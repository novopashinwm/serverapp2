﻿using System;
using System.Data;

namespace FORIS.DataAccess
{
	///<summary>
	/// Class for saving dataset into database
	///</summary>
	public class SqlDatasetAdapter : IDatasetAdapter
	{
		#region Dataadapters
		public TssDataAdapter adapterBData;
		public TssDataAdapter adapterBlading;
		public TssDataAdapter adapterBladingType;
		public TssDataAdapter adapterBrigade;
		public TssDataAdapter adapterBrigadeDriver;
		public TssDataAdapter adapterBusstop;
		public TssDataAdapter adapterCal;
		public TssDataAdapter adapterCalendarDay;
		public TssDataAdapter adapterConstants;
		public TssDataAdapter adapterContractor;
		public TssDataAdapter adapterContractorCalendar;
		public TssDataAdapter adapterController;
		public TssDataAdapter adapterControllerInfo;
		public TssDataAdapter adapterControllerSensor;
		public TssDataAdapter adapterControllerSensorLegend;
		public TssDataAdapter adapterControllerSensorMap;
		public TssDataAdapter adapterControllerSensorType;
		public TssDataAdapter adapterControllerStat;
		public TssDataAdapter adapterControllerTime;
		public TssDataAdapter adapterControllerType;
		public TssDataAdapter adapterCountGprs;
		public TssDataAdapter adapterCountGsm;
		public TssDataAdapter adapterCountSms;
		public TssDataAdapter adapterCustomer;
		public TssDataAdapter adapterDay;
		public TssDataAdapter adapterDayKind;
		public TssDataAdapter adapterDayTime;
		public TssDataAdapter adapterDayType;
		public TssDataAdapter adapterDecade;
		public TssDataAdapter adapterDepartment;
		public TssDataAdapter adapterDkSet;
		public TssDataAdapter adapterDotnetType;
		public TssDataAdapter adapterDriver;
		public TssDataAdapter adapterDriverBonus;
		public TssDataAdapter adapterDriverMsgTemplate;
		public TssDataAdapter adapterDriverStatus;
		public TssDataAdapter adapterDriverVehicle;
		public TssDataAdapter adapterDrivergroup;
		public TssDataAdapter adapterDrivergroupDriver;
		public TssDataAdapter adapterEmail;
		public TssDataAdapter adapterEmailSchedulerevent;
		public TssDataAdapter adapterEmailSchedulerqueue;
		public TssDataAdapter adapterFactorValues;
		public TssDataAdapter adapterFactors;
		public TssDataAdapter adapterGeoSegment;
		public TssDataAdapter adapterGeoSegmentRuntime;
		public TssDataAdapter adapterGeoSegmentVertex;
		public TssDataAdapter adapterGeoTrip;
		public TssDataAdapter adapterGeoZone;
		public TssDataAdapter adapterGeoZonePrimitive;
		public TssDataAdapter adapterGoods;
		public TssDataAdapter adapterGoodsLogisticOrder;
		public TssDataAdapter adapterGoodsType;
		public TssDataAdapter adapterGoodsTypeVehicle;
		public TssDataAdapter adapterGraphic;
		public TssDataAdapter adapterGraphicShift;
		public TssDataAdapter adapterGuardEvents;
		public TssDataAdapter adapterInfoSizeFiledata;
		public TssDataAdapter adapterJobDelbadxy;
		public TssDataAdapter adapterJournal;
		public TssDataAdapter adapterJournalDay;
		public TssDataAdapter adapterJournalDriver;
		public TssDataAdapter adapterJournalVehicle;
		public TssDataAdapter adapterKolomnaPoints;
		public TssDataAdapter adapterKolomnaPoints2;
		public TssDataAdapter adapterKolomnaPoints3;
		public TssDataAdapter adapterLogGuardEvents;
		public TssDataAdapter adapterLogOpsEvents;
		public TssDataAdapter adapterLogVehicleStatus;
		public TssDataAdapter adapterLogWaybillHeaderStatus;
		public TssDataAdapter adapterLogisticAddress;
		public TssDataAdapter adapterLogisticOrder;
		public TssDataAdapter adapterMapVertex;
		public TssDataAdapter adapterMaps;
		public TssDataAdapter adapterMedia;
		public TssDataAdapter adapterMediaAcceptors;
		public TssDataAdapter adapterMediaType;
		public TssDataAdapter adapterMessage;
		public TssDataAdapter adapterMessageBoard;
		public TssDataAdapter adapterMessageField;
		public TssDataAdapter adapterMessageOperator;
		public TssDataAdapter adapterMessageTemplate;
		public TssDataAdapter adapterMessageTemplateField;
		public TssDataAdapter adapterMonitoreeLog;
		public TssDataAdapter adapterOperator;
		public TssDataAdapter adapterOperatorDepartment;
		public TssDataAdapter adapterOperatorDriver;
		public TssDataAdapter adapterOperatorDrivergroup;
		public TssDataAdapter adapterOperatorProfile;
		public TssDataAdapter adapterOperatorReport;
		public TssDataAdapter adapterOperatorRoute;
		public TssDataAdapter adapterOperatorRoutegroup;
		public TssDataAdapter adapterOperatorVehicle;
		public TssDataAdapter adapterOperatorVehiclegroup;
		public TssDataAdapter adapterOperatorZone;
		public TssDataAdapter adapterOperatorZonegroup;
		public TssDataAdapter adapterOperatorgroup;
		public TssDataAdapter adapterOperatorgroupDepartment;
		public TssDataAdapter adapterOperatorgroupDriver;
		public TssDataAdapter adapterOperatorgroupDrivergroup;
		public TssDataAdapter adapterOperatorgroupOperator;
		public TssDataAdapter adapterOperatorgroupReport;
		public TssDataAdapter adapterOperatorgroupRoute;
		public TssDataAdapter adapterOperatorgroupRoutegroup;
		public TssDataAdapter adapterOperatorgroupVehicle;
		public TssDataAdapter adapterOperatorgroupVehiclegroup;
		public TssDataAdapter adapterOperatorgroupZone;
		public TssDataAdapter adapterOperatorgroupZonegroup;
		public TssDataAdapter adapterOpsEvent;
		public TssDataAdapter adapterOrder;
		public TssDataAdapter adapterOrderTrip;
		public TssDataAdapter adapterOrderType;
		public TssDataAdapter adapterOwner;
		public TssDataAdapter adapterParameter;
		public TssDataAdapter adapterPeriod;
		public TssDataAdapter adapterPoint;
		public TssDataAdapter adapterPointKind;
		public TssDataAdapter adapterReport;
		public TssDataAdapter adapterReprintReason;
		public TssDataAdapter adapterReserv;
		public TssDataAdapter adapterRight;
		public TssDataAdapter adapterRightOperator;
		public TssDataAdapter adapterRightOperatorgroup;
		public TssDataAdapter adapterRoute;
		public TssDataAdapter adapterRouteGeozone;
		public TssDataAdapter adapterRoutePoint;
		public TssDataAdapter adapterRouteTrip;
		public TssDataAdapter adapterRoutegroup;
		public TssDataAdapter adapterRoutegroupRoute;
		public TssDataAdapter adapterRs;
		public TssDataAdapter adapterRsNumber;
		public TssDataAdapter adapterRsOperationstype;
		public TssDataAdapter adapterRsOperationtype;
		public TssDataAdapter adapterRsPeriod;
		public TssDataAdapter adapterRsPoint;
		public TssDataAdapter adapterRsRoundTrip;
		public TssDataAdapter adapterRsRuntime;
		public TssDataAdapter adapterRsShift;
		public TssDataAdapter adapterRsStep;
		public TssDataAdapter adapterRsTrip;
		public TssDataAdapter adapterRsTripstype;
		public TssDataAdapter adapterRsTriptype;
		public TssDataAdapter adapterRsType;
		public TssDataAdapter adapterRsVariant;
		public TssDataAdapter adapterRsWayout;
		public TssDataAdapter adapterRule;
		public TssDataAdapter adapterSchedule;
		public TssDataAdapter adapterScheduleDetail;
		public TssDataAdapter adapterScheduleDetailInfo;
		public TssDataAdapter adapterScheduleGeozone;
		public TssDataAdapter adapterSchedulePassage;
		public TssDataAdapter adapterSchedulePoint;
		public TssDataAdapter adapterSchedulerevent;
		public TssDataAdapter adapterSchedulerqueue;
		public TssDataAdapter adapterSeason;
		public TssDataAdapter adapterSeattype;
		public TssDataAdapter adapterServers;
		public TssDataAdapter adapterSession;
		public TssDataAdapter adapterSmsLog;
		public TssDataAdapter adapterSmsType;
		public TssDataAdapter adapterSysdiagrams;
		public TssDataAdapter adapterTaskProcessorLog;
		public TssDataAdapter adapterTempTss;
		public TssDataAdapter adapterTmpdrivername;
		public TssDataAdapter adapterTrail;
		public TssDataAdapter adapterTrip;
		public TssDataAdapter adapterTripKind;
		public TssDataAdapter adapterVehicle;
		public TssDataAdapter adapterVehicleKind;
		public TssDataAdapter adapterVehicleOwner;
		public TssDataAdapter adapterVehiclePicture;
		public TssDataAdapter adapterVehicleRule;
		public TssDataAdapter adapterVehicleStatus;
		public TssDataAdapter adapterVehiclegroup;
		public TssDataAdapter adapterVehiclegroupRule;
		public TssDataAdapter adapterVehiclegroupVehicle;
		public TssDataAdapter adapterVersion;
		public TssDataAdapter adapterWaybill;
		public TssDataAdapter adapterWaybillHeader;
		public TssDataAdapter adapterWaybillheaderWaybillmark;
		public TssDataAdapter adapterWaybillmark;
		public TssDataAdapter adapterWayout;
		public TssDataAdapter adapterWbTrip;
		public TssDataAdapter adapterWebLine;
		public TssDataAdapter adapterWebLineVertex;
		public TssDataAdapter adapterWebPoint;
		public TssDataAdapter adapterWebPointType;
		public TssDataAdapter adapterWorkTime;
		public TssDataAdapter adapterWorkplaceConfig;
		public TssDataAdapter adapterWorkstation;
		public TssDataAdapter adapterWorktimeReason;
		public TssDataAdapter adapterWorktimeStatusType;
		public TssDataAdapter adapterZonePrimitive;
		public TssDataAdapter adapterZonePrimitiveVertex;
		public TssDataAdapter adapterZoneType;
		public TssDataAdapter adapterZoneVehicle;
		public TssDataAdapter adapterZoneVehiclegroup;
		public TssDataAdapter adapterZonegroup;
		public TssDataAdapter adapterZonegroupZone;
		#endregion
		protected int trailID = 0;
		protected IDbDataAdapterCollection adapters = new IDbDataAdapterCollection();
		public event BeforeDeleteHandler BeforeDelete = null;
		protected void FireBeforeDelete()
		{
			if (BeforeDelete != null)
			{
				BeforeDelete();
			}
		}
		protected int sessionID = 0;
		public int SessionID
		{
			get
			{
				return sessionID;
			}
			set
			{
				sessionID = value;
			}
		}
		public int TrailID
		{
			get
			{
				return trailID;
			}
			set
			{
				trailID = value;
			}
		}
		public IDbDataAdapterCollection Adapters
		{
			get
			{
				return adapters;
			}
		}
		public int CreateTransactionInDatabase(IDbConnection connection, IDbTransaction transaction)
		{
			using (IDbCommand command = Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("CreateTransaction"))
			{
				System.Data.SqlClient.SqlParameter session = ((System.Data.SqlClient.SqlParameter)command.Parameters["@SESSION_ID"]);
				if (this.SessionID != 0)
				{
					session.Value = this.SessionID;
				}
				else
				{
					session.Value = DBNull.Value;
				}
				System.Data.SqlClient.SqlParameter time = ((System.Data.SqlClient.SqlParameter)command.Parameters["@TRAIL_TIME"]);
				time.Value = DateTime.Now.ToUniversalTime();
				command.Connection = connection;
				command.Transaction = transaction;
				object res = command.ExecuteScalar();
				this.TrailID = (int)((System.Decimal)res);
			}
			SetupParameters();
			return this.TrailID;
		}
		protected void SetupParameters()
		{
			foreach (IDbDataAdapter adapter in this.Adapters)
			{
				if (adapter.UpdateCommand != null)
				{
					if (adapter.UpdateCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.UpdateCommand.Parameters["@TRAIL_ID"];
						par.Value = this.TrailID;
					}
				}
				if (adapter.InsertCommand != null)
				{
					if (adapter.InsertCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.InsertCommand.Parameters["@TRAIL_ID"];
						par.Value = this.TrailID;
					}
				}
				if (adapter.DeleteCommand != null)
				{
					if (adapter.DeleteCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.DeleteCommand.Parameters["@TRAIL_ID"];
						par.Value = this.TrailID;
					}
				}
			}
		}
		public SqlDatasetAdapter()
		{
			#region Create dataadapters
			this.adapterBData = new TssDataAdapter();
			this.Adapters.Add(this.adapterBData);
			UpdateSql uBData = UpdateSql.BData();
			this.adapterBData.InsertCommand = uBData.InsertCommand;
			this.adapterBData.UpdateCommand = uBData.UpdateCommand;
			this.adapterBData.DeleteCommand = uBData.DeleteCommand;
			this.adapterBladingType = new TssDataAdapter();
			this.Adapters.Add(this.adapterBladingType);
			UpdateSql uBladingType = UpdateSql.BladingType();
			this.adapterBladingType.InsertCommand = uBladingType.InsertCommand;
			this.adapterBladingType.UpdateCommand = uBladingType.UpdateCommand;
			this.adapterBladingType.DeleteCommand = uBladingType.DeleteCommand;
			this.adapterBusstop = new TssDataAdapter();
			this.Adapters.Add(this.adapterBusstop);
			UpdateSql uBusstop = UpdateSql.Busstop();
			this.adapterBusstop.InsertCommand = uBusstop.InsertCommand;
			this.adapterBusstop.UpdateCommand = uBusstop.UpdateCommand;
			this.adapterBusstop.DeleteCommand = uBusstop.DeleteCommand;
			this.adapterCalendarDay = new TssDataAdapter();
			this.Adapters.Add(this.adapterCalendarDay);
			UpdateSql uCalendarDay = UpdateSql.CalendarDay();
			this.adapterCalendarDay.InsertCommand = uCalendarDay.InsertCommand;
			this.adapterCalendarDay.UpdateCommand = uCalendarDay.UpdateCommand;
			this.adapterCalendarDay.DeleteCommand = uCalendarDay.DeleteCommand;
			this.adapterConstants = new TssDataAdapter();
			this.Adapters.Add(this.adapterConstants);
			UpdateSql uConstants = UpdateSql.Constants();
			this.adapterConstants.InsertCommand = uConstants.InsertCommand;
			this.adapterConstants.UpdateCommand = uConstants.UpdateCommand;
			this.adapterConstants.DeleteCommand = uConstants.DeleteCommand;
			this.adapterContractor = new TssDataAdapter();
			this.Adapters.Add(this.adapterContractor);
			UpdateSql uContractor = UpdateSql.Contractor();
			this.adapterContractor.InsertCommand = uContractor.InsertCommand;
			this.adapterContractor.UpdateCommand = uContractor.UpdateCommand;
			this.adapterContractor.DeleteCommand = uContractor.DeleteCommand;
			this.adapterContractorCalendar = new TssDataAdapter();
			this.Adapters.Add(this.adapterContractorCalendar);
			UpdateSql uContractorCalendar = UpdateSql.ContractorCalendar();
			this.adapterContractorCalendar.InsertCommand = uContractorCalendar.InsertCommand;
			this.adapterContractorCalendar.UpdateCommand = uContractorCalendar.UpdateCommand;
			this.adapterContractorCalendar.DeleteCommand = uContractorCalendar.DeleteCommand;
			this.adapterControllerSensorType = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerSensorType);
			UpdateSql uControllerSensorType = UpdateSql.ControllerSensorType();
			this.adapterControllerSensorType.InsertCommand = uControllerSensorType.InsertCommand;
			this.adapterControllerSensorType.UpdateCommand = uControllerSensorType.UpdateCommand;
			this.adapterControllerSensorType.DeleteCommand = uControllerSensorType.DeleteCommand;
			this.adapterControllerSensorLegend = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerSensorLegend);
			UpdateSql uControllerSensorLegend = UpdateSql.ControllerSensorLegend();
			this.adapterControllerSensorLegend.InsertCommand = uControllerSensorLegend.InsertCommand;
			this.adapterControllerSensorLegend.UpdateCommand = uControllerSensorLegend.UpdateCommand;
			this.adapterControllerSensorLegend.DeleteCommand = uControllerSensorLegend.DeleteCommand;
			this.adapterControllerType = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerType);
			UpdateSql uControllerType = UpdateSql.ControllerType();
			this.adapterControllerType.InsertCommand = uControllerType.InsertCommand;
			this.adapterControllerType.UpdateCommand = uControllerType.UpdateCommand;
			this.adapterControllerType.DeleteCommand = uControllerType.DeleteCommand;
			this.adapterControllerSensor = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerSensor);
			UpdateSql uControllerSensor = UpdateSql.ControllerSensor();
			this.adapterControllerSensor.InsertCommand = uControllerSensor.InsertCommand;
			this.adapterControllerSensor.UpdateCommand = uControllerSensor.UpdateCommand;
			this.adapterControllerSensor.DeleteCommand = uControllerSensor.DeleteCommand;
			this.adapterCountGprs = new TssDataAdapter();
			this.Adapters.Add(this.adapterCountGprs);
			UpdateSql uCountGprs = UpdateSql.CountGprs();
			this.adapterCountGprs.InsertCommand = uCountGprs.InsertCommand;
			this.adapterCountGprs.UpdateCommand = uCountGprs.UpdateCommand;
			this.adapterCountGprs.DeleteCommand = uCountGprs.DeleteCommand;
			this.adapterCustomer = new TssDataAdapter();
			this.Adapters.Add(this.adapterCustomer);
			UpdateSql uCustomer = UpdateSql.Customer();
			this.adapterCustomer.InsertCommand = uCustomer.InsertCommand;
			this.adapterCustomer.UpdateCommand = uCustomer.UpdateCommand;
			this.adapterCustomer.DeleteCommand = uCustomer.DeleteCommand;
			this.adapterDay = new TssDataAdapter();
			this.Adapters.Add(this.adapterDay);
			UpdateSql uDay = UpdateSql.Day();
			this.adapterDay.InsertCommand = uDay.InsertCommand;
			this.adapterDay.UpdateCommand = uDay.UpdateCommand;
			this.adapterDay.DeleteCommand = uDay.DeleteCommand;
			this.adapterDayType = new TssDataAdapter();
			this.Adapters.Add(this.adapterDayType);
			UpdateSql uDayType = UpdateSql.DayType();
			this.adapterDayType.InsertCommand = uDayType.InsertCommand;
			this.adapterDayType.UpdateCommand = uDayType.UpdateCommand;
			this.adapterDayType.DeleteCommand = uDayType.DeleteCommand;
			this.adapterDayTime = new TssDataAdapter();
			this.Adapters.Add(this.adapterDayTime);
			UpdateSql uDayTime = UpdateSql.DayTime();
			this.adapterDayTime.InsertCommand = uDayTime.InsertCommand;
			this.adapterDayTime.UpdateCommand = uDayTime.UpdateCommand;
			this.adapterDayTime.DeleteCommand = uDayTime.DeleteCommand;
			this.adapterDepartment = new TssDataAdapter();
			this.Adapters.Add(this.adapterDepartment);
			UpdateSql uDepartment = UpdateSql.Department();
			this.adapterDepartment.InsertCommand = uDepartment.InsertCommand;
			this.adapterDepartment.UpdateCommand = uDepartment.UpdateCommand;
			this.adapterDepartment.DeleteCommand = uDepartment.DeleteCommand;
			this.adapterDecade = new TssDataAdapter();
			this.Adapters.Add(this.adapterDecade);
			UpdateSql uDecade = UpdateSql.Decade();
			this.adapterDecade.InsertCommand = uDecade.InsertCommand;
			this.adapterDecade.UpdateCommand = uDecade.UpdateCommand;
			this.adapterDecade.DeleteCommand = uDecade.DeleteCommand;
			this.adapterDkSet = new TssDataAdapter();
			this.Adapters.Add(this.adapterDkSet);
			UpdateSql uDkSet = UpdateSql.DkSet();
			this.adapterDkSet.InsertCommand = uDkSet.InsertCommand;
			this.adapterDkSet.UpdateCommand = uDkSet.UpdateCommand;
			this.adapterDkSet.DeleteCommand = uDkSet.DeleteCommand;
			this.adapterDayKind = new TssDataAdapter();
			this.Adapters.Add(this.adapterDayKind);
			UpdateSql uDayKind = UpdateSql.DayKind();
			this.adapterDayKind.InsertCommand = uDayKind.InsertCommand;
			this.adapterDayKind.UpdateCommand = uDayKind.UpdateCommand;
			this.adapterDayKind.DeleteCommand = uDayKind.DeleteCommand;
			this.adapterCal = new TssDataAdapter();
			this.Adapters.Add(this.adapterCal);
			UpdateSql uCal = UpdateSql.Cal();
			this.adapterCal.InsertCommand = uCal.InsertCommand;
			this.adapterCal.UpdateCommand = uCal.UpdateCommand;
			this.adapterCal.DeleteCommand = uCal.DeleteCommand;
			this.adapterDotnetType = new TssDataAdapter();
			this.Adapters.Add(this.adapterDotnetType);
			UpdateSql uDotnetType = UpdateSql.DotnetType();
			this.adapterDotnetType.InsertCommand = uDotnetType.InsertCommand;
			this.adapterDotnetType.UpdateCommand = uDotnetType.UpdateCommand;
			this.adapterDotnetType.DeleteCommand = uDotnetType.DeleteCommand;
			this.adapterDriverBonus = new TssDataAdapter();
			this.Adapters.Add(this.adapterDriverBonus);
			UpdateSql uDriverBonus = UpdateSql.DriverBonus();
			this.adapterDriverBonus.InsertCommand = uDriverBonus.InsertCommand;
			this.adapterDriverBonus.UpdateCommand = uDriverBonus.UpdateCommand;
			this.adapterDriverBonus.DeleteCommand = uDriverBonus.DeleteCommand;
			this.adapterDriverMsgTemplate = new TssDataAdapter();
			this.Adapters.Add(this.adapterDriverMsgTemplate);
			UpdateSql uDriverMsgTemplate = UpdateSql.DriverMsgTemplate();
			this.adapterDriverMsgTemplate.InsertCommand = uDriverMsgTemplate.InsertCommand;
			this.adapterDriverMsgTemplate.UpdateCommand = uDriverMsgTemplate.UpdateCommand;
			this.adapterDriverMsgTemplate.DeleteCommand = uDriverMsgTemplate.DeleteCommand;
			this.adapterDriverStatus = new TssDataAdapter();
			this.Adapters.Add(this.adapterDriverStatus);
			UpdateSql uDriverStatus = UpdateSql.DriverStatus();
			this.adapterDriverStatus.InsertCommand = uDriverStatus.InsertCommand;
			this.adapterDriverStatus.UpdateCommand = uDriverStatus.UpdateCommand;
			this.adapterDriverStatus.DeleteCommand = uDriverStatus.DeleteCommand;
			this.adapterDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterDriver);
			UpdateSql uDriver = UpdateSql.Driver();
			this.adapterDriver.InsertCommand = uDriver.InsertCommand;
			this.adapterDriver.UpdateCommand = uDriver.UpdateCommand;
			this.adapterDriver.DeleteCommand = uDriver.DeleteCommand;
			this.adapterDrivergroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterDrivergroup);
			UpdateSql uDrivergroup = UpdateSql.Drivergroup();
			this.adapterDrivergroup.InsertCommand = uDrivergroup.InsertCommand;
			this.adapterDrivergroup.UpdateCommand = uDrivergroup.UpdateCommand;
			this.adapterDrivergroup.DeleteCommand = uDrivergroup.DeleteCommand;
			this.adapterDrivergroupDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterDrivergroupDriver);
			UpdateSql uDrivergroupDriver = UpdateSql.DrivergroupDriver();
			this.adapterDrivergroupDriver.InsertCommand = uDrivergroupDriver.InsertCommand;
			this.adapterDrivergroupDriver.UpdateCommand = uDrivergroupDriver.UpdateCommand;
			this.adapterDrivergroupDriver.DeleteCommand = uDrivergroupDriver.DeleteCommand;
			this.adapterEmail = new TssDataAdapter();
			this.Adapters.Add(this.adapterEmail);
			UpdateSql uEmail = UpdateSql.Email();
			this.adapterEmail.InsertCommand = uEmail.InsertCommand;
			this.adapterEmail.UpdateCommand = uEmail.UpdateCommand;
			this.adapterEmail.DeleteCommand = uEmail.DeleteCommand;
			this.adapterFactors = new TssDataAdapter();
			this.Adapters.Add(this.adapterFactors);
			UpdateSql uFactors = UpdateSql.Factors();
			this.adapterFactors.InsertCommand = uFactors.InsertCommand;
			this.adapterFactors.UpdateCommand = uFactors.UpdateCommand;
			this.adapterFactors.DeleteCommand = uFactors.DeleteCommand;
			this.adapterGeoZone = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoZone);
			UpdateSql uGeoZone = UpdateSql.GeoZone();
			this.adapterGeoZone.InsertCommand = uGeoZone.InsertCommand;
			this.adapterGeoZone.UpdateCommand = uGeoZone.UpdateCommand;
			this.adapterGeoZone.DeleteCommand = uGeoZone.DeleteCommand;
			this.adapterGoodsType = new TssDataAdapter();
			this.Adapters.Add(this.adapterGoodsType);
			UpdateSql uGoodsType = UpdateSql.GoodsType();
			this.adapterGoodsType.InsertCommand = uGoodsType.InsertCommand;
			this.adapterGoodsType.UpdateCommand = uGoodsType.UpdateCommand;
			this.adapterGoodsType.DeleteCommand = uGoodsType.DeleteCommand;
			this.adapterGoods = new TssDataAdapter();
			this.Adapters.Add(this.adapterGoods);
			UpdateSql uGoods = UpdateSql.Goods();
			this.adapterGoods.InsertCommand = uGoods.InsertCommand;
			this.adapterGoods.UpdateCommand = uGoods.UpdateCommand;
			this.adapterGoods.DeleteCommand = uGoods.DeleteCommand;
			this.adapterGraphic = new TssDataAdapter();
			this.Adapters.Add(this.adapterGraphic);
			UpdateSql uGraphic = UpdateSql.Graphic();
			this.adapterGraphic.InsertCommand = uGraphic.InsertCommand;
			this.adapterGraphic.UpdateCommand = uGraphic.UpdateCommand;
			this.adapterGraphic.DeleteCommand = uGraphic.DeleteCommand;
			this.adapterGraphicShift = new TssDataAdapter();
			this.Adapters.Add(this.adapterGraphicShift);
			UpdateSql uGraphicShift = UpdateSql.GraphicShift();
			this.adapterGraphicShift.InsertCommand = uGraphicShift.InsertCommand;
			this.adapterGraphicShift.UpdateCommand = uGraphicShift.UpdateCommand;
			this.adapterGraphicShift.DeleteCommand = uGraphicShift.DeleteCommand;
			this.adapterGuardEvents = new TssDataAdapter();
			this.Adapters.Add(this.adapterGuardEvents);
			UpdateSql uGuardEvents = UpdateSql.GuardEvents();
			this.adapterGuardEvents.InsertCommand = uGuardEvents.InsertCommand;
			this.adapterGuardEvents.UpdateCommand = uGuardEvents.UpdateCommand;
			this.adapterGuardEvents.DeleteCommand = uGuardEvents.DeleteCommand;
			this.adapterInfoSizeFiledata = new TssDataAdapter();
			this.Adapters.Add(this.adapterInfoSizeFiledata);
			UpdateSql uInfoSizeFiledata = UpdateSql.InfoSizeFiledata();
			this.adapterInfoSizeFiledata.InsertCommand = uInfoSizeFiledata.InsertCommand;
			this.adapterInfoSizeFiledata.UpdateCommand = uInfoSizeFiledata.UpdateCommand;
			this.adapterInfoSizeFiledata.DeleteCommand = uInfoSizeFiledata.DeleteCommand;
			this.adapterJobDelbadxy = new TssDataAdapter();
			this.Adapters.Add(this.adapterJobDelbadxy);
			UpdateSql uJobDelbadxy = UpdateSql.JobDelbadxy();
			this.adapterJobDelbadxy.InsertCommand = uJobDelbadxy.InsertCommand;
			this.adapterJobDelbadxy.UpdateCommand = uJobDelbadxy.UpdateCommand;
			this.adapterJobDelbadxy.DeleteCommand = uJobDelbadxy.DeleteCommand;
			this.adapterJournalDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterJournalDriver);
			UpdateSql uJournalDriver = UpdateSql.JournalDriver();
			this.adapterJournalDriver.InsertCommand = uJournalDriver.InsertCommand;
			this.adapterJournalDriver.UpdateCommand = uJournalDriver.UpdateCommand;
			this.adapterJournalDriver.DeleteCommand = uJournalDriver.DeleteCommand;
			this.adapterKolomnaPoints = new TssDataAdapter();
			this.Adapters.Add(this.adapterKolomnaPoints);
			UpdateSql uKolomnaPoints = UpdateSql.KolomnaPoints();
			this.adapterKolomnaPoints.InsertCommand = uKolomnaPoints.InsertCommand;
			this.adapterKolomnaPoints.UpdateCommand = uKolomnaPoints.UpdateCommand;
			this.adapterKolomnaPoints.DeleteCommand = uKolomnaPoints.DeleteCommand;
			this.adapterKolomnaPoints2 = new TssDataAdapter();
			this.Adapters.Add(this.adapterKolomnaPoints2);
			UpdateSql uKolomnaPoints2 = UpdateSql.KolomnaPoints2();
			this.adapterKolomnaPoints2.InsertCommand = uKolomnaPoints2.InsertCommand;
			this.adapterKolomnaPoints2.UpdateCommand = uKolomnaPoints2.UpdateCommand;
			this.adapterKolomnaPoints2.DeleteCommand = uKolomnaPoints2.DeleteCommand;
			this.adapterKolomnaPoints3 = new TssDataAdapter();
			this.Adapters.Add(this.adapterKolomnaPoints3);
			UpdateSql uKolomnaPoints3 = UpdateSql.KolomnaPoints3();
			this.adapterKolomnaPoints3.InsertCommand = uKolomnaPoints3.InsertCommand;
			this.adapterKolomnaPoints3.UpdateCommand = uKolomnaPoints3.UpdateCommand;
			this.adapterKolomnaPoints3.DeleteCommand = uKolomnaPoints3.DeleteCommand;
			this.adapterLogisticAddress = new TssDataAdapter();
			this.Adapters.Add(this.adapterLogisticAddress);
			UpdateSql uLogisticAddress = UpdateSql.LogisticAddress();
			this.adapterLogisticAddress.InsertCommand = uLogisticAddress.InsertCommand;
			this.adapterLogisticAddress.UpdateCommand = uLogisticAddress.UpdateCommand;
			this.adapterLogisticAddress.DeleteCommand = uLogisticAddress.DeleteCommand;
			this.adapterLogisticOrder = new TssDataAdapter();
			this.Adapters.Add(this.adapterLogisticOrder);
			UpdateSql uLogisticOrder = UpdateSql.LogisticOrder();
			this.adapterLogisticOrder.InsertCommand = uLogisticOrder.InsertCommand;
			this.adapterLogisticOrder.UpdateCommand = uLogisticOrder.UpdateCommand;
			this.adapterLogisticOrder.DeleteCommand = uLogisticOrder.DeleteCommand;
			this.adapterGoodsLogisticOrder = new TssDataAdapter();
			this.Adapters.Add(this.adapterGoodsLogisticOrder);
			UpdateSql uGoodsLogisticOrder = UpdateSql.GoodsLogisticOrder();
			this.adapterGoodsLogisticOrder.InsertCommand = uGoodsLogisticOrder.InsertCommand;
			this.adapterGoodsLogisticOrder.UpdateCommand = uGoodsLogisticOrder.UpdateCommand;
			this.adapterGoodsLogisticOrder.DeleteCommand = uGoodsLogisticOrder.DeleteCommand;
			this.adapterMaps = new TssDataAdapter();
			this.Adapters.Add(this.adapterMaps);
			UpdateSql uMaps = UpdateSql.Maps();
			this.adapterMaps.InsertCommand = uMaps.InsertCommand;
			this.adapterMaps.UpdateCommand = uMaps.UpdateCommand;
			this.adapterMaps.DeleteCommand = uMaps.DeleteCommand;
			this.adapterMapVertex = new TssDataAdapter();
			this.Adapters.Add(this.adapterMapVertex);
			UpdateSql uMapVertex = UpdateSql.MapVertex();
			this.adapterMapVertex.InsertCommand = uMapVertex.InsertCommand;
			this.adapterMapVertex.UpdateCommand = uMapVertex.UpdateCommand;
			this.adapterMapVertex.DeleteCommand = uMapVertex.DeleteCommand;
			this.adapterMediaType = new TssDataAdapter();
			this.Adapters.Add(this.adapterMediaType);
			UpdateSql uMediaType = UpdateSql.MediaType();
			this.adapterMediaType.InsertCommand = uMediaType.InsertCommand;
			this.adapterMediaType.UpdateCommand = uMediaType.UpdateCommand;
			this.adapterMediaType.DeleteCommand = uMediaType.DeleteCommand;
			this.adapterMedia = new TssDataAdapter();
			this.Adapters.Add(this.adapterMedia);
			UpdateSql uMedia = UpdateSql.Media();
			this.adapterMedia.InsertCommand = uMedia.InsertCommand;
			this.adapterMedia.UpdateCommand = uMedia.UpdateCommand;
			this.adapterMedia.DeleteCommand = uMedia.DeleteCommand;
			this.adapterMediaAcceptors = new TssDataAdapter();
			this.Adapters.Add(this.adapterMediaAcceptors);
			UpdateSql uMediaAcceptors = UpdateSql.MediaAcceptors();
			this.adapterMediaAcceptors.InsertCommand = uMediaAcceptors.InsertCommand;
			this.adapterMediaAcceptors.UpdateCommand = uMediaAcceptors.UpdateCommand;
			this.adapterMediaAcceptors.DeleteCommand = uMediaAcceptors.DeleteCommand;
			this.adapterMessageBoard = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessageBoard);
			UpdateSql uMessageBoard = UpdateSql.MessageBoard();
			this.adapterMessageBoard.InsertCommand = uMessageBoard.InsertCommand;
			this.adapterMessageBoard.UpdateCommand = uMessageBoard.UpdateCommand;
			this.adapterMessageBoard.DeleteCommand = uMessageBoard.DeleteCommand;
			this.adapterMessageTemplate = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessageTemplate);
			UpdateSql uMessageTemplate = UpdateSql.MessageTemplate();
			this.adapterMessageTemplate.InsertCommand = uMessageTemplate.InsertCommand;
			this.adapterMessageTemplate.UpdateCommand = uMessageTemplate.UpdateCommand;
			this.adapterMessageTemplate.DeleteCommand = uMessageTemplate.DeleteCommand;
			this.adapterMessage = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessage);
			UpdateSql uMessage = UpdateSql.Message();
			this.adapterMessage.InsertCommand = uMessage.InsertCommand;
			this.adapterMessage.UpdateCommand = uMessage.UpdateCommand;
			this.adapterMessage.DeleteCommand = uMessage.DeleteCommand;
			this.adapterMessageTemplateField = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessageTemplateField);
			UpdateSql uMessageTemplateField = UpdateSql.MessageTemplateField();
			this.adapterMessageTemplateField.InsertCommand = uMessageTemplateField.InsertCommand;
			this.adapterMessageTemplateField.UpdateCommand = uMessageTemplateField.UpdateCommand;
			this.adapterMessageTemplateField.DeleteCommand = uMessageTemplateField.DeleteCommand;
			this.adapterMessageField = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessageField);
			UpdateSql uMessageField = UpdateSql.MessageField();
			this.adapterMessageField.InsertCommand = uMessageField.InsertCommand;
			this.adapterMessageField.UpdateCommand = uMessageField.UpdateCommand;
			this.adapterMessageField.DeleteCommand = uMessageField.DeleteCommand;
			this.adapterOperator = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperator);
			UpdateSql uOperator = UpdateSql.Operator();
			this.adapterOperator.InsertCommand = uOperator.InsertCommand;
			this.adapterOperator.UpdateCommand = uOperator.UpdateCommand;
			this.adapterOperator.DeleteCommand = uOperator.DeleteCommand;
			this.adapterMessageOperator = new TssDataAdapter();
			this.Adapters.Add(this.adapterMessageOperator);
			UpdateSql uMessageOperator = UpdateSql.MessageOperator();
			this.adapterMessageOperator.InsertCommand = uMessageOperator.InsertCommand;
			this.adapterMessageOperator.UpdateCommand = uMessageOperator.UpdateCommand;
			this.adapterMessageOperator.DeleteCommand = uMessageOperator.DeleteCommand;
			this.adapterOperatorProfile = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorProfile);
			UpdateSql uOperatorProfile = UpdateSql.OperatorProfile();
			this.adapterOperatorProfile.InsertCommand = uOperatorProfile.InsertCommand;
			this.adapterOperatorProfile.UpdateCommand = uOperatorProfile.UpdateCommand;
			this.adapterOperatorProfile.DeleteCommand = uOperatorProfile.DeleteCommand;
			this.adapterOperatorgroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroup);
			UpdateSql uOperatorgroup = UpdateSql.Operatorgroup();
			this.adapterOperatorgroup.InsertCommand = uOperatorgroup.InsertCommand;
			this.adapterOperatorgroup.UpdateCommand = uOperatorgroup.UpdateCommand;
			this.adapterOperatorgroup.DeleteCommand = uOperatorgroup.DeleteCommand;
			this.adapterOperatorgroupOperator = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupOperator);
			UpdateSql uOperatorgroupOperator = UpdateSql.OperatorgroupOperator();
			this.adapterOperatorgroupOperator.InsertCommand = uOperatorgroupOperator.InsertCommand;
			this.adapterOperatorgroupOperator.UpdateCommand = uOperatorgroupOperator.UpdateCommand;
			this.adapterOperatorgroupOperator.DeleteCommand = uOperatorgroupOperator.DeleteCommand;
			this.adapterOpsEvent = new TssDataAdapter();
			this.Adapters.Add(this.adapterOpsEvent);
			UpdateSql uOpsEvent = UpdateSql.OpsEvent();
			this.adapterOpsEvent.InsertCommand = uOpsEvent.InsertCommand;
			this.adapterOpsEvent.UpdateCommand = uOpsEvent.UpdateCommand;
			this.adapterOpsEvent.DeleteCommand = uOpsEvent.DeleteCommand;
			this.adapterLogOpsEvents = new TssDataAdapter();
			this.Adapters.Add(this.adapterLogOpsEvents);
			UpdateSql uLogOpsEvents = UpdateSql.LogOpsEvents();
			this.adapterLogOpsEvents.InsertCommand = uLogOpsEvents.InsertCommand;
			this.adapterLogOpsEvents.UpdateCommand = uLogOpsEvents.UpdateCommand;
			this.adapterLogOpsEvents.DeleteCommand = uLogOpsEvents.DeleteCommand;
			this.adapterOrderType = new TssDataAdapter();
			this.Adapters.Add(this.adapterOrderType);
			UpdateSql uOrderType = UpdateSql.OrderType();
			this.adapterOrderType.InsertCommand = uOrderType.InsertCommand;
			this.adapterOrderType.UpdateCommand = uOrderType.UpdateCommand;
			this.adapterOrderType.DeleteCommand = uOrderType.DeleteCommand;
			this.adapterOrder = new TssDataAdapter();
			this.Adapters.Add(this.adapterOrder);
			UpdateSql uOrder = UpdateSql.Order();
			this.adapterOrder.InsertCommand = uOrder.InsertCommand;
			this.adapterOrder.UpdateCommand = uOrder.UpdateCommand;
			this.adapterOrder.DeleteCommand = uOrder.DeleteCommand;
			this.adapterOwner = new TssDataAdapter();
			this.Adapters.Add(this.adapterOwner);
			UpdateSql uOwner = UpdateSql.Owner();
			this.adapterOwner.InsertCommand = uOwner.InsertCommand;
			this.adapterOwner.UpdateCommand = uOwner.UpdateCommand;
			this.adapterOwner.DeleteCommand = uOwner.DeleteCommand;
			this.adapterParameter = new TssDataAdapter();
			this.Adapters.Add(this.adapterParameter);
			UpdateSql uParameter = UpdateSql.Parameter();
			this.adapterParameter.InsertCommand = uParameter.InsertCommand;
			this.adapterParameter.UpdateCommand = uParameter.UpdateCommand;
			this.adapterParameter.DeleteCommand = uParameter.DeleteCommand;
			this.adapterPointKind = new TssDataAdapter();
			this.Adapters.Add(this.adapterPointKind);
			UpdateSql uPointKind = UpdateSql.PointKind();
			this.adapterPointKind.InsertCommand = uPointKind.InsertCommand;
			this.adapterPointKind.UpdateCommand = uPointKind.UpdateCommand;
			this.adapterPointKind.DeleteCommand = uPointKind.DeleteCommand;
			this.adapterPoint = new TssDataAdapter();
			this.Adapters.Add(this.adapterPoint);
			UpdateSql uPoint = UpdateSql.Point();
			this.adapterPoint.InsertCommand = uPoint.InsertCommand;
			this.adapterPoint.UpdateCommand = uPoint.UpdateCommand;
			this.adapterPoint.DeleteCommand = uPoint.DeleteCommand;
			this.adapterReport = new TssDataAdapter();
			this.Adapters.Add(this.adapterReport);
			UpdateSql uReport = UpdateSql.Report();
			this.adapterReport.InsertCommand = uReport.InsertCommand;
			this.adapterReport.UpdateCommand = uReport.UpdateCommand;
			this.adapterReport.DeleteCommand = uReport.DeleteCommand;
			this.adapterReprintReason = new TssDataAdapter();
			this.Adapters.Add(this.adapterReprintReason);
			UpdateSql uReprintReason = UpdateSql.ReprintReason();
			this.adapterReprintReason.InsertCommand = uReprintReason.InsertCommand;
			this.adapterReprintReason.UpdateCommand = uReprintReason.UpdateCommand;
			this.adapterReprintReason.DeleteCommand = uReprintReason.DeleteCommand;
			this.adapterReserv = new TssDataAdapter();
			this.Adapters.Add(this.adapterReserv);
			UpdateSql uReserv = UpdateSql.Reserv();
			this.adapterReserv.InsertCommand = uReserv.InsertCommand;
			this.adapterReserv.UpdateCommand = uReserv.UpdateCommand;
			this.adapterReserv.DeleteCommand = uReserv.DeleteCommand;
			this.adapterRight = new TssDataAdapter();
			this.Adapters.Add(this.adapterRight);
			UpdateSql uRight = UpdateSql.Right();
			this.adapterRight.InsertCommand = uRight.InsertCommand;
			this.adapterRight.UpdateCommand = uRight.UpdateCommand;
			this.adapterRight.DeleteCommand = uRight.DeleteCommand;
			this.adapterOperatorDepartment = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorDepartment);
			UpdateSql uOperatorDepartment = UpdateSql.OperatorDepartment();
			this.adapterOperatorDepartment.InsertCommand = uOperatorDepartment.InsertCommand;
			this.adapterOperatorDepartment.UpdateCommand = uOperatorDepartment.UpdateCommand;
			this.adapterOperatorDepartment.DeleteCommand = uOperatorDepartment.DeleteCommand;
			this.adapterOperatorDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorDriver);
			UpdateSql uOperatorDriver = UpdateSql.OperatorDriver();
			this.adapterOperatorDriver.InsertCommand = uOperatorDriver.InsertCommand;
			this.adapterOperatorDriver.UpdateCommand = uOperatorDriver.UpdateCommand;
			this.adapterOperatorDriver.DeleteCommand = uOperatorDriver.DeleteCommand;
			this.adapterOperatorDrivergroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorDrivergroup);
			UpdateSql uOperatorDrivergroup = UpdateSql.OperatorDrivergroup();
			this.adapterOperatorDrivergroup.InsertCommand = uOperatorDrivergroup.InsertCommand;
			this.adapterOperatorDrivergroup.UpdateCommand = uOperatorDrivergroup.UpdateCommand;
			this.adapterOperatorDrivergroup.DeleteCommand = uOperatorDrivergroup.DeleteCommand;
			this.adapterOperatorReport = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorReport);
			UpdateSql uOperatorReport = UpdateSql.OperatorReport();
			this.adapterOperatorReport.InsertCommand = uOperatorReport.InsertCommand;
			this.adapterOperatorReport.UpdateCommand = uOperatorReport.UpdateCommand;
			this.adapterOperatorReport.DeleteCommand = uOperatorReport.DeleteCommand;
			this.adapterOperatorZone = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorZone);
			UpdateSql uOperatorZone = UpdateSql.OperatorZone();
			this.adapterOperatorZone.InsertCommand = uOperatorZone.InsertCommand;
			this.adapterOperatorZone.UpdateCommand = uOperatorZone.UpdateCommand;
			this.adapterOperatorZone.DeleteCommand = uOperatorZone.DeleteCommand;
			this.adapterOperatorgroupDepartment = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupDepartment);
			UpdateSql uOperatorgroupDepartment = UpdateSql.OperatorgroupDepartment();
			this.adapterOperatorgroupDepartment.InsertCommand = uOperatorgroupDepartment.InsertCommand;
			this.adapterOperatorgroupDepartment.UpdateCommand = uOperatorgroupDepartment.UpdateCommand;
			this.adapterOperatorgroupDepartment.DeleteCommand = uOperatorgroupDepartment.DeleteCommand;
			this.adapterOperatorgroupDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupDriver);
			UpdateSql uOperatorgroupDriver = UpdateSql.OperatorgroupDriver();
			this.adapterOperatorgroupDriver.InsertCommand = uOperatorgroupDriver.InsertCommand;
			this.adapterOperatorgroupDriver.UpdateCommand = uOperatorgroupDriver.UpdateCommand;
			this.adapterOperatorgroupDriver.DeleteCommand = uOperatorgroupDriver.DeleteCommand;
			this.adapterOperatorgroupDrivergroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupDrivergroup);
			UpdateSql uOperatorgroupDrivergroup = UpdateSql.OperatorgroupDrivergroup();
			this.adapterOperatorgroupDrivergroup.InsertCommand = uOperatorgroupDrivergroup.InsertCommand;
			this.adapterOperatorgroupDrivergroup.UpdateCommand = uOperatorgroupDrivergroup.UpdateCommand;
			this.adapterOperatorgroupDrivergroup.DeleteCommand = uOperatorgroupDrivergroup.DeleteCommand;
			this.adapterOperatorgroupReport = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupReport);
			UpdateSql uOperatorgroupReport = UpdateSql.OperatorgroupReport();
			this.adapterOperatorgroupReport.InsertCommand = uOperatorgroupReport.InsertCommand;
			this.adapterOperatorgroupReport.UpdateCommand = uOperatorgroupReport.UpdateCommand;
			this.adapterOperatorgroupReport.DeleteCommand = uOperatorgroupReport.DeleteCommand;
			this.adapterOperatorgroupZone = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupZone);
			UpdateSql uOperatorgroupZone = UpdateSql.OperatorgroupZone();
			this.adapterOperatorgroupZone.InsertCommand = uOperatorgroupZone.InsertCommand;
			this.adapterOperatorgroupZone.UpdateCommand = uOperatorgroupZone.UpdateCommand;
			this.adapterOperatorgroupZone.DeleteCommand = uOperatorgroupZone.DeleteCommand;
			this.adapterRightOperator = new TssDataAdapter();
			this.Adapters.Add(this.adapterRightOperator);
			UpdateSql uRightOperator = UpdateSql.RightOperator();
			this.adapterRightOperator.InsertCommand = uRightOperator.InsertCommand;
			this.adapterRightOperator.UpdateCommand = uRightOperator.UpdateCommand;
			this.adapterRightOperator.DeleteCommand = uRightOperator.DeleteCommand;
			this.adapterRightOperatorgroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterRightOperatorgroup);
			UpdateSql uRightOperatorgroup = UpdateSql.RightOperatorgroup();
			this.adapterRightOperatorgroup.InsertCommand = uRightOperatorgroup.InsertCommand;
			this.adapterRightOperatorgroup.UpdateCommand = uRightOperatorgroup.UpdateCommand;
			this.adapterRightOperatorgroup.DeleteCommand = uRightOperatorgroup.DeleteCommand;
			this.adapterRoute = new TssDataAdapter();
			this.Adapters.Add(this.adapterRoute);
			UpdateSql uRoute = UpdateSql.Route();
			this.adapterRoute.InsertCommand = uRoute.InsertCommand;
			this.adapterRoute.UpdateCommand = uRoute.UpdateCommand;
			this.adapterRoute.DeleteCommand = uRoute.DeleteCommand;
			this.adapterGeoTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoTrip);
			UpdateSql uGeoTrip = UpdateSql.GeoTrip();
			this.adapterGeoTrip.InsertCommand = uGeoTrip.InsertCommand;
			this.adapterGeoTrip.UpdateCommand = uGeoTrip.UpdateCommand;
			this.adapterGeoTrip.DeleteCommand = uGeoTrip.DeleteCommand;
			this.adapterGeoSegment = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoSegment);
			UpdateSql uGeoSegment = UpdateSql.GeoSegment();
			this.adapterGeoSegment.InsertCommand = uGeoSegment.InsertCommand;
			this.adapterGeoSegment.UpdateCommand = uGeoSegment.UpdateCommand;
			this.adapterGeoSegment.DeleteCommand = uGeoSegment.DeleteCommand;
			this.adapterGeoSegmentRuntime = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoSegmentRuntime);
			UpdateSql uGeoSegmentRuntime = UpdateSql.GeoSegmentRuntime();
			this.adapterGeoSegmentRuntime.InsertCommand = uGeoSegmentRuntime.InsertCommand;
			this.adapterGeoSegmentRuntime.UpdateCommand = uGeoSegmentRuntime.UpdateCommand;
			this.adapterGeoSegmentRuntime.DeleteCommand = uGeoSegmentRuntime.DeleteCommand;
			this.adapterGeoSegmentVertex = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoSegmentVertex);
			UpdateSql uGeoSegmentVertex = UpdateSql.GeoSegmentVertex();
			this.adapterGeoSegmentVertex.InsertCommand = uGeoSegmentVertex.InsertCommand;
			this.adapterGeoSegmentVertex.UpdateCommand = uGeoSegmentVertex.UpdateCommand;
			this.adapterGeoSegmentVertex.DeleteCommand = uGeoSegmentVertex.DeleteCommand;
			this.adapterOperatorRoute = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorRoute);
			UpdateSql uOperatorRoute = UpdateSql.OperatorRoute();
			this.adapterOperatorRoute.InsertCommand = uOperatorRoute.InsertCommand;
			this.adapterOperatorRoute.UpdateCommand = uOperatorRoute.UpdateCommand;
			this.adapterOperatorRoute.DeleteCommand = uOperatorRoute.DeleteCommand;
			this.adapterOperatorgroupRoute = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupRoute);
			UpdateSql uOperatorgroupRoute = UpdateSql.OperatorgroupRoute();
			this.adapterOperatorgroupRoute.InsertCommand = uOperatorgroupRoute.InsertCommand;
			this.adapterOperatorgroupRoute.UpdateCommand = uOperatorgroupRoute.UpdateCommand;
			this.adapterOperatorgroupRoute.DeleteCommand = uOperatorgroupRoute.DeleteCommand;
			this.adapterRouteGeozone = new TssDataAdapter();
			this.Adapters.Add(this.adapterRouteGeozone);
			UpdateSql uRouteGeozone = UpdateSql.RouteGeozone();
			this.adapterRouteGeozone.InsertCommand = uRouteGeozone.InsertCommand;
			this.adapterRouteGeozone.UpdateCommand = uRouteGeozone.UpdateCommand;
			this.adapterRouteGeozone.DeleteCommand = uRouteGeozone.DeleteCommand;
			this.adapterRoutePoint = new TssDataAdapter();
			this.Adapters.Add(this.adapterRoutePoint);
			UpdateSql uRoutePoint = UpdateSql.RoutePoint();
			this.adapterRoutePoint.InsertCommand = uRoutePoint.InsertCommand;
			this.adapterRoutePoint.UpdateCommand = uRoutePoint.UpdateCommand;
			this.adapterRoutePoint.DeleteCommand = uRoutePoint.DeleteCommand;
			this.adapterRouteTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterRouteTrip);
			UpdateSql uRouteTrip = UpdateSql.RouteTrip();
			this.adapterRouteTrip.InsertCommand = uRouteTrip.InsertCommand;
			this.adapterRouteTrip.UpdateCommand = uRouteTrip.UpdateCommand;
			this.adapterRouteTrip.DeleteCommand = uRouteTrip.DeleteCommand;
			this.adapterRoutegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterRoutegroup);
			UpdateSql uRoutegroup = UpdateSql.Routegroup();
			this.adapterRoutegroup.InsertCommand = uRoutegroup.InsertCommand;
			this.adapterRoutegroup.UpdateCommand = uRoutegroup.UpdateCommand;
			this.adapterRoutegroup.DeleteCommand = uRoutegroup.DeleteCommand;
			this.adapterOperatorRoutegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorRoutegroup);
			UpdateSql uOperatorRoutegroup = UpdateSql.OperatorRoutegroup();
			this.adapterOperatorRoutegroup.InsertCommand = uOperatorRoutegroup.InsertCommand;
			this.adapterOperatorRoutegroup.UpdateCommand = uOperatorRoutegroup.UpdateCommand;
			this.adapterOperatorRoutegroup.DeleteCommand = uOperatorRoutegroup.DeleteCommand;
			this.adapterOperatorgroupRoutegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupRoutegroup);
			UpdateSql uOperatorgroupRoutegroup = UpdateSql.OperatorgroupRoutegroup();
			this.adapterOperatorgroupRoutegroup.InsertCommand = uOperatorgroupRoutegroup.InsertCommand;
			this.adapterOperatorgroupRoutegroup.UpdateCommand = uOperatorgroupRoutegroup.UpdateCommand;
			this.adapterOperatorgroupRoutegroup.DeleteCommand = uOperatorgroupRoutegroup.DeleteCommand;
			this.adapterRoutegroupRoute = new TssDataAdapter();
			this.Adapters.Add(this.adapterRoutegroupRoute);
			UpdateSql uRoutegroupRoute = UpdateSql.RoutegroupRoute();
			this.adapterRoutegroupRoute.InsertCommand = uRoutegroupRoute.InsertCommand;
			this.adapterRoutegroupRoute.UpdateCommand = uRoutegroupRoute.UpdateCommand;
			this.adapterRoutegroupRoute.DeleteCommand = uRoutegroupRoute.DeleteCommand;
			this.adapterRs = new TssDataAdapter();
			this.Adapters.Add(this.adapterRs);
			UpdateSql uRs = UpdateSql.Rs();
			this.adapterRs.InsertCommand = uRs.InsertCommand;
			this.adapterRs.UpdateCommand = uRs.UpdateCommand;
			this.adapterRs.DeleteCommand = uRs.DeleteCommand;
			this.adapterRsOperationstype = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsOperationstype);
			UpdateSql uRsOperationstype = UpdateSql.RsOperationstype();
			this.adapterRsOperationstype.InsertCommand = uRsOperationstype.InsertCommand;
			this.adapterRsOperationstype.UpdateCommand = uRsOperationstype.UpdateCommand;
			this.adapterRsOperationstype.DeleteCommand = uRsOperationstype.DeleteCommand;
			this.adapterRsTripstype = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsTripstype);
			UpdateSql uRsTripstype = UpdateSql.RsTripstype();
			this.adapterRsTripstype.InsertCommand = uRsTripstype.InsertCommand;
			this.adapterRsTripstype.UpdateCommand = uRsTripstype.UpdateCommand;
			this.adapterRsTripstype.DeleteCommand = uRsTripstype.DeleteCommand;
			this.adapterRsType = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsType);
			UpdateSql uRsType = UpdateSql.RsType();
			this.adapterRsType.InsertCommand = uRsType.InsertCommand;
			this.adapterRsType.UpdateCommand = uRsType.UpdateCommand;
			this.adapterRsType.DeleteCommand = uRsType.DeleteCommand;
			this.adapterRsVariant = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsVariant);
			UpdateSql uRsVariant = UpdateSql.RsVariant();
			this.adapterRsVariant.InsertCommand = uRsVariant.InsertCommand;
			this.adapterRsVariant.UpdateCommand = uRsVariant.UpdateCommand;
			this.adapterRsVariant.DeleteCommand = uRsVariant.DeleteCommand;
			this.adapterRsPeriod = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsPeriod);
			UpdateSql uRsPeriod = UpdateSql.RsPeriod();
			this.adapterRsPeriod.InsertCommand = uRsPeriod.InsertCommand;
			this.adapterRsPeriod.UpdateCommand = uRsPeriod.UpdateCommand;
			this.adapterRsPeriod.DeleteCommand = uRsPeriod.DeleteCommand;
			this.adapterRsRoundTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsRoundTrip);
			UpdateSql uRsRoundTrip = UpdateSql.RsRoundTrip();
			this.adapterRsRoundTrip.InsertCommand = uRsRoundTrip.InsertCommand;
			this.adapterRsRoundTrip.UpdateCommand = uRsRoundTrip.UpdateCommand;
			this.adapterRsRoundTrip.DeleteCommand = uRsRoundTrip.DeleteCommand;
			this.adapterRsStep = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsStep);
			UpdateSql uRsStep = UpdateSql.RsStep();
			this.adapterRsStep.InsertCommand = uRsStep.InsertCommand;
			this.adapterRsStep.UpdateCommand = uRsStep.UpdateCommand;
			this.adapterRsStep.DeleteCommand = uRsStep.DeleteCommand;
			this.adapterRsNumber = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsNumber);
			UpdateSql uRsNumber = UpdateSql.RsNumber();
			this.adapterRsNumber.InsertCommand = uRsNumber.InsertCommand;
			this.adapterRsNumber.UpdateCommand = uRsNumber.UpdateCommand;
			this.adapterRsNumber.DeleteCommand = uRsNumber.DeleteCommand;
			this.adapterRsTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsTrip);
			UpdateSql uRsTrip = UpdateSql.RsTrip();
			this.adapterRsTrip.InsertCommand = uRsTrip.InsertCommand;
			this.adapterRsTrip.UpdateCommand = uRsTrip.UpdateCommand;
			this.adapterRsTrip.DeleteCommand = uRsTrip.DeleteCommand;
			this.adapterRsOperationtype = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsOperationtype);
			UpdateSql uRsOperationtype = UpdateSql.RsOperationtype();
			this.adapterRsOperationtype.InsertCommand = uRsOperationtype.InsertCommand;
			this.adapterRsOperationtype.UpdateCommand = uRsOperationtype.UpdateCommand;
			this.adapterRsOperationtype.DeleteCommand = uRsOperationtype.DeleteCommand;
			this.adapterRsRuntime = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsRuntime);
			UpdateSql uRsRuntime = UpdateSql.RsRuntime();
			this.adapterRsRuntime.InsertCommand = uRsRuntime.InsertCommand;
			this.adapterRsRuntime.UpdateCommand = uRsRuntime.UpdateCommand;
			this.adapterRsRuntime.DeleteCommand = uRsRuntime.DeleteCommand;
			this.adapterRsTriptype = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsTriptype);
			UpdateSql uRsTriptype = UpdateSql.RsTriptype();
			this.adapterRsTriptype.InsertCommand = uRsTriptype.InsertCommand;
			this.adapterRsTriptype.UpdateCommand = uRsTriptype.UpdateCommand;
			this.adapterRsTriptype.DeleteCommand = uRsTriptype.DeleteCommand;
			this.adapterRule = new TssDataAdapter();
			this.Adapters.Add(this.adapterRule);
			UpdateSql uRule = UpdateSql.Rule();
			this.adapterRule.InsertCommand = uRule.InsertCommand;
			this.adapterRule.UpdateCommand = uRule.UpdateCommand;
			this.adapterRule.DeleteCommand = uRule.DeleteCommand;
			this.adapterSchedulerevent = new TssDataAdapter();
			this.Adapters.Add(this.adapterSchedulerevent);
			UpdateSql uSchedulerevent = UpdateSql.Schedulerevent();
			this.adapterSchedulerevent.InsertCommand = uSchedulerevent.InsertCommand;
			this.adapterSchedulerevent.UpdateCommand = uSchedulerevent.UpdateCommand;
			this.adapterSchedulerevent.DeleteCommand = uSchedulerevent.DeleteCommand;
			this.adapterEmailSchedulerevent = new TssDataAdapter();
			this.Adapters.Add(this.adapterEmailSchedulerevent);
			UpdateSql uEmailSchedulerevent = UpdateSql.EmailSchedulerevent();
			this.adapterEmailSchedulerevent.InsertCommand = uEmailSchedulerevent.InsertCommand;
			this.adapterEmailSchedulerevent.UpdateCommand = uEmailSchedulerevent.UpdateCommand;
			this.adapterEmailSchedulerevent.DeleteCommand = uEmailSchedulerevent.DeleteCommand;
			this.adapterSchedulerqueue = new TssDataAdapter();
			this.Adapters.Add(this.adapterSchedulerqueue);
			UpdateSql uSchedulerqueue = UpdateSql.Schedulerqueue();
			this.adapterSchedulerqueue.InsertCommand = uSchedulerqueue.InsertCommand;
			this.adapterSchedulerqueue.UpdateCommand = uSchedulerqueue.UpdateCommand;
			this.adapterSchedulerqueue.DeleteCommand = uSchedulerqueue.DeleteCommand;
			this.adapterEmailSchedulerqueue = new TssDataAdapter();
			this.Adapters.Add(this.adapterEmailSchedulerqueue);
			UpdateSql uEmailSchedulerqueue = UpdateSql.EmailSchedulerqueue();
			this.adapterEmailSchedulerqueue.InsertCommand = uEmailSchedulerqueue.InsertCommand;
			this.adapterEmailSchedulerqueue.UpdateCommand = uEmailSchedulerqueue.UpdateCommand;
			this.adapterEmailSchedulerqueue.DeleteCommand = uEmailSchedulerqueue.DeleteCommand;
			this.adapterSeason = new TssDataAdapter();
			this.Adapters.Add(this.adapterSeason);
			UpdateSql uSeason = UpdateSql.Season();
			this.adapterSeason.InsertCommand = uSeason.InsertCommand;
			this.adapterSeason.UpdateCommand = uSeason.UpdateCommand;
			this.adapterSeason.DeleteCommand = uSeason.DeleteCommand;
			this.adapterFactorValues = new TssDataAdapter();
			this.Adapters.Add(this.adapterFactorValues);
			UpdateSql uFactorValues = UpdateSql.FactorValues();
			this.adapterFactorValues.InsertCommand = uFactorValues.InsertCommand;
			this.adapterFactorValues.UpdateCommand = uFactorValues.UpdateCommand;
			this.adapterFactorValues.DeleteCommand = uFactorValues.DeleteCommand;
			this.adapterPeriod = new TssDataAdapter();
			this.Adapters.Add(this.adapterPeriod);
			UpdateSql uPeriod = UpdateSql.Period();
			this.adapterPeriod.InsertCommand = uPeriod.InsertCommand;
			this.adapterPeriod.UpdateCommand = uPeriod.UpdateCommand;
			this.adapterPeriod.DeleteCommand = uPeriod.DeleteCommand;
			this.adapterSeattype = new TssDataAdapter();
			this.Adapters.Add(this.adapterSeattype);
			UpdateSql uSeattype = UpdateSql.Seattype();
			this.adapterSeattype.InsertCommand = uSeattype.InsertCommand;
			this.adapterSeattype.UpdateCommand = uSeattype.UpdateCommand;
			this.adapterSeattype.DeleteCommand = uSeattype.DeleteCommand;
			this.adapterServers = new TssDataAdapter();
			this.Adapters.Add(this.adapterServers);
			UpdateSql uServers = UpdateSql.Servers();
			this.adapterServers.InsertCommand = uServers.InsertCommand;
			this.adapterServers.UpdateCommand = uServers.UpdateCommand;
			this.adapterServers.DeleteCommand = uServers.DeleteCommand;
			this.adapterSession = new TssDataAdapter();
			this.Adapters.Add(this.adapterSession);
			UpdateSql uSession = UpdateSql.Session();
			this.adapterSession.InsertCommand = uSession.InsertCommand;
			this.adapterSession.UpdateCommand = uSession.UpdateCommand;
			this.adapterSession.DeleteCommand = uSession.DeleteCommand;
			this.adapterSmsType = new TssDataAdapter();
			this.Adapters.Add(this.adapterSmsType);
			UpdateSql uSmsType = UpdateSql.SmsType();
			this.adapterSmsType.InsertCommand = uSmsType.InsertCommand;
			this.adapterSmsType.UpdateCommand = uSmsType.UpdateCommand;
			this.adapterSmsType.DeleteCommand = uSmsType.DeleteCommand;
			this.adapterTaskProcessorLog = new TssDataAdapter();
			this.Adapters.Add(this.adapterTaskProcessorLog);
			UpdateSql uTaskProcessorLog = UpdateSql.TaskProcessorLog();
			this.adapterTaskProcessorLog.InsertCommand = uTaskProcessorLog.InsertCommand;
			this.adapterTaskProcessorLog.UpdateCommand = uTaskProcessorLog.UpdateCommand;
			this.adapterTaskProcessorLog.DeleteCommand = uTaskProcessorLog.DeleteCommand;
			this.adapterTempTss = new TssDataAdapter();
			this.Adapters.Add(this.adapterTempTss);
			UpdateSql uTempTss = UpdateSql.TempTss();
			this.adapterTempTss.InsertCommand = uTempTss.InsertCommand;
			this.adapterTempTss.UpdateCommand = uTempTss.UpdateCommand;
			this.adapterTempTss.DeleteCommand = uTempTss.DeleteCommand;
			this.adapterTmpdrivername = new TssDataAdapter();
			this.Adapters.Add(this.adapterTmpdrivername);
			UpdateSql uTmpdrivername = UpdateSql.Tmpdrivername();
			this.adapterTmpdrivername.InsertCommand = uTmpdrivername.InsertCommand;
			this.adapterTmpdrivername.UpdateCommand = uTmpdrivername.UpdateCommand;
			this.adapterTmpdrivername.DeleteCommand = uTmpdrivername.DeleteCommand;
			this.adapterTrail = new TssDataAdapter();
			this.Adapters.Add(this.adapterTrail);
			UpdateSql uTrail = UpdateSql.Trail();
			this.adapterTrail.InsertCommand = uTrail.InsertCommand;
			this.adapterTrail.UpdateCommand = uTrail.UpdateCommand;
			this.adapterTrail.DeleteCommand = uTrail.DeleteCommand;
			this.adapterVehicleKind = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehicleKind);
			UpdateSql uVehicleKind = UpdateSql.VehicleKind();
			this.adapterVehicleKind.InsertCommand = uVehicleKind.InsertCommand;
			this.adapterVehicleKind.UpdateCommand = uVehicleKind.UpdateCommand;
			this.adapterVehicleKind.DeleteCommand = uVehicleKind.DeleteCommand;
			this.adapterVehicleStatus = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehicleStatus);
			UpdateSql uVehicleStatus = UpdateSql.VehicleStatus();
			this.adapterVehicleStatus.InsertCommand = uVehicleStatus.InsertCommand;
			this.adapterVehicleStatus.UpdateCommand = uVehicleStatus.UpdateCommand;
			this.adapterVehicleStatus.DeleteCommand = uVehicleStatus.DeleteCommand;
			this.adapterVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehicle);
			UpdateSql uVehicle = UpdateSql.Vehicle();
			this.adapterVehicle.InsertCommand = uVehicle.InsertCommand;
			this.adapterVehicle.UpdateCommand = uVehicle.UpdateCommand;
			this.adapterVehicle.DeleteCommand = uVehicle.DeleteCommand;
			this.adapterBrigade = new TssDataAdapter();
			this.Adapters.Add(this.adapterBrigade);
			UpdateSql uBrigade = UpdateSql.Brigade();
			this.adapterBrigade.InsertCommand = uBrigade.InsertCommand;
			this.adapterBrigade.UpdateCommand = uBrigade.UpdateCommand;
			this.adapterBrigade.DeleteCommand = uBrigade.DeleteCommand;
			this.adapterBrigadeDriver = new TssDataAdapter();
			this.Adapters.Add(this.adapterBrigadeDriver);
			UpdateSql uBrigadeDriver = UpdateSql.BrigadeDriver();
			this.adapterBrigadeDriver.InsertCommand = uBrigadeDriver.InsertCommand;
			this.adapterBrigadeDriver.UpdateCommand = uBrigadeDriver.UpdateCommand;
			this.adapterBrigadeDriver.DeleteCommand = uBrigadeDriver.DeleteCommand;
			this.adapterController = new TssDataAdapter();
			this.Adapters.Add(this.adapterController);
			UpdateSql uController = UpdateSql.Controller();
			this.adapterController.InsertCommand = uController.InsertCommand;
			this.adapterController.UpdateCommand = uController.UpdateCommand;
			this.adapterController.DeleteCommand = uController.DeleteCommand;
			this.adapterControllerInfo = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerInfo);
			UpdateSql uControllerInfo = UpdateSql.ControllerInfo();
			this.adapterControllerInfo.InsertCommand = uControllerInfo.InsertCommand;
			this.adapterControllerInfo.UpdateCommand = uControllerInfo.UpdateCommand;
			this.adapterControllerInfo.DeleteCommand = uControllerInfo.DeleteCommand;
			this.adapterControllerSensorMap = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerSensorMap);
			UpdateSql uControllerSensorMap = UpdateSql.ControllerSensorMap();
			this.adapterControllerSensorMap.InsertCommand = uControllerSensorMap.InsertCommand;
			this.adapterControllerSensorMap.UpdateCommand = uControllerSensorMap.UpdateCommand;
			this.adapterControllerSensorMap.DeleteCommand = uControllerSensorMap.DeleteCommand;
			this.adapterControllerTime = new TssDataAdapter();
			this.Adapters.Add(this.adapterControllerTime);
			UpdateSql uControllerTime = UpdateSql.ControllerTime();
			this.adapterControllerTime.InsertCommand = uControllerTime.InsertCommand;
			this.adapterControllerTime.UpdateCommand = uControllerTime.UpdateCommand;
			this.adapterControllerTime.DeleteCommand = uControllerTime.DeleteCommand;
			this.adapterDriverVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterDriverVehicle);
			UpdateSql uDriverVehicle = UpdateSql.DriverVehicle();
			this.adapterDriverVehicle.InsertCommand = uDriverVehicle.InsertCommand;
			this.adapterDriverVehicle.UpdateCommand = uDriverVehicle.UpdateCommand;
			this.adapterDriverVehicle.DeleteCommand = uDriverVehicle.DeleteCommand;
			this.adapterGoodsTypeVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterGoodsTypeVehicle);
			UpdateSql uGoodsTypeVehicle = UpdateSql.GoodsTypeVehicle();
			this.adapterGoodsTypeVehicle.InsertCommand = uGoodsTypeVehicle.InsertCommand;
			this.adapterGoodsTypeVehicle.UpdateCommand = uGoodsTypeVehicle.UpdateCommand;
			this.adapterGoodsTypeVehicle.DeleteCommand = uGoodsTypeVehicle.DeleteCommand;
			this.adapterJournalVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterJournalVehicle);
			UpdateSql uJournalVehicle = UpdateSql.JournalVehicle();
			this.adapterJournalVehicle.InsertCommand = uJournalVehicle.InsertCommand;
			this.adapterJournalVehicle.UpdateCommand = uJournalVehicle.UpdateCommand;
			this.adapterJournalVehicle.DeleteCommand = uJournalVehicle.DeleteCommand;
			this.adapterLogVehicleStatus = new TssDataAdapter();
			this.Adapters.Add(this.adapterLogVehicleStatus);
			UpdateSql uLogVehicleStatus = UpdateSql.LogVehicleStatus();
			this.adapterLogVehicleStatus.InsertCommand = uLogVehicleStatus.InsertCommand;
			this.adapterLogVehicleStatus.UpdateCommand = uLogVehicleStatus.UpdateCommand;
			this.adapterLogVehicleStatus.DeleteCommand = uLogVehicleStatus.DeleteCommand;
			this.adapterOperatorVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorVehicle);
			UpdateSql uOperatorVehicle = UpdateSql.OperatorVehicle();
			this.adapterOperatorVehicle.InsertCommand = uOperatorVehicle.InsertCommand;
			this.adapterOperatorVehicle.UpdateCommand = uOperatorVehicle.UpdateCommand;
			this.adapterOperatorVehicle.DeleteCommand = uOperatorVehicle.DeleteCommand;
			this.adapterOperatorgroupVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupVehicle);
			UpdateSql uOperatorgroupVehicle = UpdateSql.OperatorgroupVehicle();
			this.adapterOperatorgroupVehicle.InsertCommand = uOperatorgroupVehicle.InsertCommand;
			this.adapterOperatorgroupVehicle.UpdateCommand = uOperatorgroupVehicle.UpdateCommand;
			this.adapterOperatorgroupVehicle.DeleteCommand = uOperatorgroupVehicle.DeleteCommand;
			this.adapterVehicleOwner = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehicleOwner);
			UpdateSql uVehicleOwner = UpdateSql.VehicleOwner();
			this.adapterVehicleOwner.InsertCommand = uVehicleOwner.InsertCommand;
			this.adapterVehicleOwner.UpdateCommand = uVehicleOwner.UpdateCommand;
			this.adapterVehicleOwner.DeleteCommand = uVehicleOwner.DeleteCommand;
			this.adapterVehiclePicture = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehiclePicture);
			UpdateSql uVehiclePicture = UpdateSql.VehiclePicture();
			this.adapterVehiclePicture.InsertCommand = uVehiclePicture.InsertCommand;
			this.adapterVehiclePicture.UpdateCommand = uVehiclePicture.UpdateCommand;
			this.adapterVehiclePicture.DeleteCommand = uVehiclePicture.DeleteCommand;
			this.adapterVehicleRule = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehicleRule);
			UpdateSql uVehicleRule = UpdateSql.VehicleRule();
			this.adapterVehicleRule.InsertCommand = uVehicleRule.InsertCommand;
			this.adapterVehicleRule.UpdateCommand = uVehicleRule.UpdateCommand;
			this.adapterVehicleRule.DeleteCommand = uVehicleRule.DeleteCommand;
			this.adapterVehiclegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehiclegroup);
			UpdateSql uVehiclegroup = UpdateSql.Vehiclegroup();
			this.adapterVehiclegroup.InsertCommand = uVehiclegroup.InsertCommand;
			this.adapterVehiclegroup.UpdateCommand = uVehiclegroup.UpdateCommand;
			this.adapterVehiclegroup.DeleteCommand = uVehiclegroup.DeleteCommand;
			this.adapterOperatorVehiclegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorVehiclegroup);
			UpdateSql uOperatorVehiclegroup = UpdateSql.OperatorVehiclegroup();
			this.adapterOperatorVehiclegroup.InsertCommand = uOperatorVehiclegroup.InsertCommand;
			this.adapterOperatorVehiclegroup.UpdateCommand = uOperatorVehiclegroup.UpdateCommand;
			this.adapterOperatorVehiclegroup.DeleteCommand = uOperatorVehiclegroup.DeleteCommand;
			this.adapterOperatorgroupVehiclegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupVehiclegroup);
			UpdateSql uOperatorgroupVehiclegroup = UpdateSql.OperatorgroupVehiclegroup();
			this.adapterOperatorgroupVehiclegroup.InsertCommand = uOperatorgroupVehiclegroup.InsertCommand;
			this.adapterOperatorgroupVehiclegroup.UpdateCommand = uOperatorgroupVehiclegroup.UpdateCommand;
			this.adapterOperatorgroupVehiclegroup.DeleteCommand = uOperatorgroupVehiclegroup.DeleteCommand;
			this.adapterVehiclegroupRule = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehiclegroupRule);
			UpdateSql uVehiclegroupRule = UpdateSql.VehiclegroupRule();
			this.adapterVehiclegroupRule.InsertCommand = uVehiclegroupRule.InsertCommand;
			this.adapterVehiclegroupRule.UpdateCommand = uVehiclegroupRule.UpdateCommand;
			this.adapterVehiclegroupRule.DeleteCommand = uVehiclegroupRule.DeleteCommand;
			this.adapterVehiclegroupVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterVehiclegroupVehicle);
			UpdateSql uVehiclegroupVehicle = UpdateSql.VehiclegroupVehicle();
			this.adapterVehiclegroupVehicle.InsertCommand = uVehiclegroupVehicle.InsertCommand;
			this.adapterVehiclegroupVehicle.UpdateCommand = uVehiclegroupVehicle.UpdateCommand;
			this.adapterVehiclegroupVehicle.DeleteCommand = uVehiclegroupVehicle.DeleteCommand;
			this.adapterVersion = new TssDataAdapter();
			this.Adapters.Add(this.adapterVersion);
			UpdateSql uVersion = UpdateSql.Version();
			this.adapterVersion.InsertCommand = uVersion.InsertCommand;
			this.adapterVersion.UpdateCommand = uVersion.UpdateCommand;
			this.adapterVersion.DeleteCommand = uVersion.DeleteCommand;
			this.adapterWaybill = new TssDataAdapter();
			this.Adapters.Add(this.adapterWaybill);
			UpdateSql uWaybill = UpdateSql.Waybill();
			this.adapterWaybill.InsertCommand = uWaybill.InsertCommand;
			this.adapterWaybill.UpdateCommand = uWaybill.UpdateCommand;
			this.adapterWaybill.DeleteCommand = uWaybill.DeleteCommand;
			this.adapterBlading = new TssDataAdapter();
			this.Adapters.Add(this.adapterBlading);
			UpdateSql uBlading = UpdateSql.Blading();
			this.adapterBlading.InsertCommand = uBlading.InsertCommand;
			this.adapterBlading.UpdateCommand = uBlading.UpdateCommand;
			this.adapterBlading.DeleteCommand = uBlading.DeleteCommand;
			this.adapterWaybillmark = new TssDataAdapter();
			this.Adapters.Add(this.adapterWaybillmark);
			UpdateSql uWaybillmark = UpdateSql.Waybillmark();
			this.adapterWaybillmark.InsertCommand = uWaybillmark.InsertCommand;
			this.adapterWaybillmark.UpdateCommand = uWaybillmark.UpdateCommand;
			this.adapterWaybillmark.DeleteCommand = uWaybillmark.DeleteCommand;
			this.adapterWayout = new TssDataAdapter();
			this.Adapters.Add(this.adapterWayout);
			UpdateSql uWayout = UpdateSql.Wayout();
			this.adapterWayout.InsertCommand = uWayout.InsertCommand;
			this.adapterWayout.UpdateCommand = uWayout.UpdateCommand;
			this.adapterWayout.DeleteCommand = uWayout.DeleteCommand;
			this.adapterJournal = new TssDataAdapter();
			this.Adapters.Add(this.adapterJournal);
			UpdateSql uJournal = UpdateSql.Journal();
			this.adapterJournal.InsertCommand = uJournal.InsertCommand;
			this.adapterJournal.UpdateCommand = uJournal.UpdateCommand;
			this.adapterJournal.DeleteCommand = uJournal.DeleteCommand;
			this.adapterJournalDay = new TssDataAdapter();
			this.Adapters.Add(this.adapterJournalDay);
			UpdateSql uJournalDay = UpdateSql.JournalDay();
			this.adapterJournalDay.InsertCommand = uJournalDay.InsertCommand;
			this.adapterJournalDay.UpdateCommand = uJournalDay.UpdateCommand;
			this.adapterJournalDay.DeleteCommand = uJournalDay.DeleteCommand;
			this.adapterOrderTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterOrderTrip);
			UpdateSql uOrderTrip = UpdateSql.OrderTrip();
			this.adapterOrderTrip.InsertCommand = uOrderTrip.InsertCommand;
			this.adapterOrderTrip.UpdateCommand = uOrderTrip.UpdateCommand;
			this.adapterOrderTrip.DeleteCommand = uOrderTrip.DeleteCommand;
			this.adapterRsWayout = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsWayout);
			UpdateSql uRsWayout = UpdateSql.RsWayout();
			this.adapterRsWayout.InsertCommand = uRsWayout.InsertCommand;
			this.adapterRsWayout.UpdateCommand = uRsWayout.UpdateCommand;
			this.adapterRsWayout.DeleteCommand = uRsWayout.DeleteCommand;
			this.adapterRsShift = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsShift);
			UpdateSql uRsShift = UpdateSql.RsShift();
			this.adapterRsShift.InsertCommand = uRsShift.InsertCommand;
			this.adapterRsShift.UpdateCommand = uRsShift.UpdateCommand;
			this.adapterRsShift.DeleteCommand = uRsShift.DeleteCommand;
			this.adapterRsPoint = new TssDataAdapter();
			this.Adapters.Add(this.adapterRsPoint);
			UpdateSql uRsPoint = UpdateSql.RsPoint();
			this.adapterRsPoint.InsertCommand = uRsPoint.InsertCommand;
			this.adapterRsPoint.UpdateCommand = uRsPoint.UpdateCommand;
			this.adapterRsPoint.DeleteCommand = uRsPoint.DeleteCommand;
			this.adapterSchedule = new TssDataAdapter();
			this.Adapters.Add(this.adapterSchedule);
			UpdateSql uSchedule = UpdateSql.Schedule();
			this.adapterSchedule.InsertCommand = uSchedule.InsertCommand;
			this.adapterSchedule.UpdateCommand = uSchedule.UpdateCommand;
			this.adapterSchedule.DeleteCommand = uSchedule.DeleteCommand;
			this.adapterLogWaybillHeaderStatus = new TssDataAdapter();
			this.Adapters.Add(this.adapterLogWaybillHeaderStatus);
			UpdateSql uLogWaybillHeaderStatus = UpdateSql.LogWaybillHeaderStatus();
			this.adapterLogWaybillHeaderStatus.InsertCommand = uLogWaybillHeaderStatus.InsertCommand;
			this.adapterLogWaybillHeaderStatus.UpdateCommand = uLogWaybillHeaderStatus.UpdateCommand;
			this.adapterLogWaybillHeaderStatus.DeleteCommand = uLogWaybillHeaderStatus.DeleteCommand;
			this.adapterScheduleDetail = new TssDataAdapter();
			this.Adapters.Add(this.adapterScheduleDetail);
			UpdateSql uScheduleDetail = UpdateSql.ScheduleDetail();
			this.adapterScheduleDetail.InsertCommand = uScheduleDetail.InsertCommand;
			this.adapterScheduleDetail.UpdateCommand = uScheduleDetail.UpdateCommand;
			this.adapterScheduleDetail.DeleteCommand = uScheduleDetail.DeleteCommand;
			this.adapterScheduleDetailInfo = new TssDataAdapter();
			this.Adapters.Add(this.adapterScheduleDetailInfo);
			UpdateSql uScheduleDetailInfo = UpdateSql.ScheduleDetailInfo();
			this.adapterScheduleDetailInfo.InsertCommand = uScheduleDetailInfo.InsertCommand;
			this.adapterScheduleDetailInfo.UpdateCommand = uScheduleDetailInfo.UpdateCommand;
			this.adapterScheduleDetailInfo.DeleteCommand = uScheduleDetailInfo.DeleteCommand;
			this.adapterWebLine = new TssDataAdapter();
			this.Adapters.Add(this.adapterWebLine);
			UpdateSql uWebLine = UpdateSql.WebLine();
			this.adapterWebLine.InsertCommand = uWebLine.InsertCommand;
			this.adapterWebLine.UpdateCommand = uWebLine.UpdateCommand;
			this.adapterWebLine.DeleteCommand = uWebLine.DeleteCommand;
			this.adapterWebLineVertex = new TssDataAdapter();
			this.Adapters.Add(this.adapterWebLineVertex);
			UpdateSql uWebLineVertex = UpdateSql.WebLineVertex();
			this.adapterWebLineVertex.InsertCommand = uWebLineVertex.InsertCommand;
			this.adapterWebLineVertex.UpdateCommand = uWebLineVertex.UpdateCommand;
			this.adapterWebLineVertex.DeleteCommand = uWebLineVertex.DeleteCommand;
			this.adapterWebPointType = new TssDataAdapter();
			this.Adapters.Add(this.adapterWebPointType);
			UpdateSql uWebPointType = UpdateSql.WebPointType();
			this.adapterWebPointType.InsertCommand = uWebPointType.InsertCommand;
			this.adapterWebPointType.UpdateCommand = uWebPointType.UpdateCommand;
			this.adapterWebPointType.DeleteCommand = uWebPointType.DeleteCommand;
			this.adapterWebPoint = new TssDataAdapter();
			this.Adapters.Add(this.adapterWebPoint);
			UpdateSql uWebPoint = UpdateSql.WebPoint();
			this.adapterWebPoint.InsertCommand = uWebPoint.InsertCommand;
			this.adapterWebPoint.UpdateCommand = uWebPoint.UpdateCommand;
			this.adapterWebPoint.DeleteCommand = uWebPoint.DeleteCommand;
			this.adapterWorkTime = new TssDataAdapter();
			this.Adapters.Add(this.adapterWorkTime);
			UpdateSql uWorkTime = UpdateSql.WorkTime();
			this.adapterWorkTime.InsertCommand = uWorkTime.InsertCommand;
			this.adapterWorkTime.UpdateCommand = uWorkTime.UpdateCommand;
			this.adapterWorkTime.DeleteCommand = uWorkTime.DeleteCommand;
			this.adapterWorkplaceConfig = new TssDataAdapter();
			this.Adapters.Add(this.adapterWorkplaceConfig);
			UpdateSql uWorkplaceConfig = UpdateSql.WorkplaceConfig();
			this.adapterWorkplaceConfig.InsertCommand = uWorkplaceConfig.InsertCommand;
			this.adapterWorkplaceConfig.UpdateCommand = uWorkplaceConfig.UpdateCommand;
			this.adapterWorkplaceConfig.DeleteCommand = uWorkplaceConfig.DeleteCommand;
			this.adapterWorkstation = new TssDataAdapter();
			this.Adapters.Add(this.adapterWorkstation);
			UpdateSql uWorkstation = UpdateSql.Workstation();
			this.adapterWorkstation.InsertCommand = uWorkstation.InsertCommand;
			this.adapterWorkstation.UpdateCommand = uWorkstation.UpdateCommand;
			this.adapterWorkstation.DeleteCommand = uWorkstation.DeleteCommand;
			this.adapterWorktimeStatusType = new TssDataAdapter();
			this.Adapters.Add(this.adapterWorktimeStatusType);
			UpdateSql uWorktimeStatusType = UpdateSql.WorktimeStatusType();
			this.adapterWorktimeStatusType.InsertCommand = uWorktimeStatusType.InsertCommand;
			this.adapterWorktimeStatusType.UpdateCommand = uWorktimeStatusType.UpdateCommand;
			this.adapterWorktimeStatusType.DeleteCommand = uWorktimeStatusType.DeleteCommand;
			this.adapterTripKind = new TssDataAdapter();
			this.Adapters.Add(this.adapterTripKind);
			UpdateSql uTripKind = UpdateSql.TripKind();
			this.adapterTripKind.InsertCommand = uTripKind.InsertCommand;
			this.adapterTripKind.UpdateCommand = uTripKind.UpdateCommand;
			this.adapterTripKind.DeleteCommand = uTripKind.DeleteCommand;
			this.adapterTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterTrip);
			UpdateSql uTrip = UpdateSql.Trip();
			this.adapterTrip.InsertCommand = uTrip.InsertCommand;
			this.adapterTrip.UpdateCommand = uTrip.UpdateCommand;
			this.adapterTrip.DeleteCommand = uTrip.DeleteCommand;
			this.adapterScheduleGeozone = new TssDataAdapter();
			this.Adapters.Add(this.adapterScheduleGeozone);
			UpdateSql uScheduleGeozone = UpdateSql.ScheduleGeozone();
			this.adapterScheduleGeozone.InsertCommand = uScheduleGeozone.InsertCommand;
			this.adapterScheduleGeozone.UpdateCommand = uScheduleGeozone.UpdateCommand;
			this.adapterScheduleGeozone.DeleteCommand = uScheduleGeozone.DeleteCommand;
			this.adapterSchedulePoint = new TssDataAdapter();
			this.Adapters.Add(this.adapterSchedulePoint);
			UpdateSql uSchedulePoint = UpdateSql.SchedulePoint();
			this.adapterSchedulePoint.InsertCommand = uSchedulePoint.InsertCommand;
			this.adapterSchedulePoint.UpdateCommand = uSchedulePoint.UpdateCommand;
			this.adapterSchedulePoint.DeleteCommand = uSchedulePoint.DeleteCommand;
			this.adapterWaybillHeader = new TssDataAdapter();
			this.Adapters.Add(this.adapterWaybillHeader);
			UpdateSql uWaybillHeader = UpdateSql.WaybillHeader();
			this.adapterWaybillHeader.InsertCommand = uWaybillHeader.InsertCommand;
			this.adapterWaybillHeader.UpdateCommand = uWaybillHeader.UpdateCommand;
			this.adapterWaybillHeader.DeleteCommand = uWaybillHeader.DeleteCommand;
			this.adapterSchedulePassage = new TssDataAdapter();
			this.Adapters.Add(this.adapterSchedulePassage);
			UpdateSql uSchedulePassage = UpdateSql.SchedulePassage();
			this.adapterSchedulePassage.InsertCommand = uSchedulePassage.InsertCommand;
			this.adapterSchedulePassage.UpdateCommand = uSchedulePassage.UpdateCommand;
			this.adapterSchedulePassage.DeleteCommand = uSchedulePassage.DeleteCommand;
			this.adapterWaybillheaderWaybillmark = new TssDataAdapter();
			this.Adapters.Add(this.adapterWaybillheaderWaybillmark);
			UpdateSql uWaybillheaderWaybillmark = UpdateSql.WaybillheaderWaybillmark();
			this.adapterWaybillheaderWaybillmark.InsertCommand = uWaybillheaderWaybillmark.InsertCommand;
			this.adapterWaybillheaderWaybillmark.UpdateCommand = uWaybillheaderWaybillmark.UpdateCommand;
			this.adapterWaybillheaderWaybillmark.DeleteCommand = uWaybillheaderWaybillmark.DeleteCommand;
			this.adapterWorktimeReason = new TssDataAdapter();
			this.Adapters.Add(this.adapterWorktimeReason);
			UpdateSql uWorktimeReason = UpdateSql.WorktimeReason();
			this.adapterWorktimeReason.InsertCommand = uWorktimeReason.InsertCommand;
			this.adapterWorktimeReason.UpdateCommand = uWorktimeReason.UpdateCommand;
			this.adapterWorktimeReason.DeleteCommand = uWorktimeReason.DeleteCommand;
			this.adapterWbTrip = new TssDataAdapter();
			this.Adapters.Add(this.adapterWbTrip);
			UpdateSql uWbTrip = UpdateSql.WbTrip();
			this.adapterWbTrip.InsertCommand = uWbTrip.InsertCommand;
			this.adapterWbTrip.UpdateCommand = uWbTrip.UpdateCommand;
			this.adapterWbTrip.DeleteCommand = uWbTrip.DeleteCommand;
			this.adapterZoneType = new TssDataAdapter();
			this.Adapters.Add(this.adapterZoneType);
			UpdateSql uZoneType = UpdateSql.ZoneType();
			this.adapterZoneType.InsertCommand = uZoneType.InsertCommand;
			this.adapterZoneType.UpdateCommand = uZoneType.UpdateCommand;
			this.adapterZoneType.DeleteCommand = uZoneType.DeleteCommand;
			this.adapterZonePrimitive = new TssDataAdapter();
			this.Adapters.Add(this.adapterZonePrimitive);
			UpdateSql uZonePrimitive = UpdateSql.ZonePrimitive();
			this.adapterZonePrimitive.InsertCommand = uZonePrimitive.InsertCommand;
			this.adapterZonePrimitive.UpdateCommand = uZonePrimitive.UpdateCommand;
			this.adapterZonePrimitive.DeleteCommand = uZonePrimitive.DeleteCommand;
			this.adapterGeoZonePrimitive = new TssDataAdapter();
			this.Adapters.Add(this.adapterGeoZonePrimitive);
			UpdateSql uGeoZonePrimitive = UpdateSql.GeoZonePrimitive();
			this.adapterGeoZonePrimitive.InsertCommand = uGeoZonePrimitive.InsertCommand;
			this.adapterGeoZonePrimitive.UpdateCommand = uGeoZonePrimitive.UpdateCommand;
			this.adapterGeoZonePrimitive.DeleteCommand = uGeoZonePrimitive.DeleteCommand;
			this.adapterZonePrimitiveVertex = new TssDataAdapter();
			this.Adapters.Add(this.adapterZonePrimitiveVertex);
			UpdateSql uZonePrimitiveVertex = UpdateSql.ZonePrimitiveVertex();
			this.adapterZonePrimitiveVertex.InsertCommand = uZonePrimitiveVertex.InsertCommand;
			this.adapterZonePrimitiveVertex.UpdateCommand = uZonePrimitiveVertex.UpdateCommand;
			this.adapterZonePrimitiveVertex.DeleteCommand = uZonePrimitiveVertex.DeleteCommand;
			this.adapterZoneVehicle = new TssDataAdapter();
			this.Adapters.Add(this.adapterZoneVehicle);
			UpdateSql uZoneVehicle = UpdateSql.ZoneVehicle();
			this.adapterZoneVehicle.InsertCommand = uZoneVehicle.InsertCommand;
			this.adapterZoneVehicle.UpdateCommand = uZoneVehicle.UpdateCommand;
			this.adapterZoneVehicle.DeleteCommand = uZoneVehicle.DeleteCommand;
			this.adapterZoneVehiclegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterZoneVehiclegroup);
			UpdateSql uZoneVehiclegroup = UpdateSql.ZoneVehiclegroup();
			this.adapterZoneVehiclegroup.InsertCommand = uZoneVehiclegroup.InsertCommand;
			this.adapterZoneVehiclegroup.UpdateCommand = uZoneVehiclegroup.UpdateCommand;
			this.adapterZoneVehiclegroup.DeleteCommand = uZoneVehiclegroup.DeleteCommand;
			this.adapterZonegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterZonegroup);
			UpdateSql uZonegroup = UpdateSql.Zonegroup();
			this.adapterZonegroup.InsertCommand = uZonegroup.InsertCommand;
			this.adapterZonegroup.UpdateCommand = uZonegroup.UpdateCommand;
			this.adapterZonegroup.DeleteCommand = uZonegroup.DeleteCommand;
			this.adapterOperatorZonegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorZonegroup);
			UpdateSql uOperatorZonegroup = UpdateSql.OperatorZonegroup();
			this.adapterOperatorZonegroup.InsertCommand = uOperatorZonegroup.InsertCommand;
			this.adapterOperatorZonegroup.UpdateCommand = uOperatorZonegroup.UpdateCommand;
			this.adapterOperatorZonegroup.DeleteCommand = uOperatorZonegroup.DeleteCommand;
			this.adapterOperatorgroupZonegroup = new TssDataAdapter();
			this.Adapters.Add(this.adapterOperatorgroupZonegroup);
			UpdateSql uOperatorgroupZonegroup = UpdateSql.OperatorgroupZonegroup();
			this.adapterOperatorgroupZonegroup.InsertCommand = uOperatorgroupZonegroup.InsertCommand;
			this.adapterOperatorgroupZonegroup.UpdateCommand = uOperatorgroupZonegroup.UpdateCommand;
			this.adapterOperatorgroupZonegroup.DeleteCommand = uOperatorgroupZonegroup.DeleteCommand;
			this.adapterZonegroupZone = new TssDataAdapter();
			this.Adapters.Add(this.adapterZonegroupZone);
			UpdateSql uZonegroupZone = UpdateSql.ZonegroupZone();
			this.adapterZonegroupZone.InsertCommand = uZonegroupZone.InsertCommand;
			this.adapterZonegroupZone.UpdateCommand = uZonegroupZone.UpdateCommand;
			this.adapterZonegroupZone.DeleteCommand = uZonegroupZone.DeleteCommand;
			#endregion
		}
		public void Update(DataSet dataset, IDbConnection connection, IDbTransaction transaction)
		{
			int trail_id = CreateTransactionInDatabase(connection, transaction);
			Cascades cascades = new Cascades(connection, transaction);
			cascades.TrailID = trail_id;
			cascades.CheckDatasetProperties(dataset);

			#region Setup dataadapters
			GenericHelpers.SetConnectionTransaction(this.adapterBData, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterBlading, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterBladingType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterBrigade, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterBrigadeDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterBusstop, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterCal, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterCalendarDay, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterConstants, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterContractor, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterContractorCalendar, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterController, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerInfo, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerSensor, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerSensorLegend, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerSensorMap, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerSensorType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerTime, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterControllerType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterCountGprs, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterCustomer, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDay, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDayKind, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDayTime, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDayType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDecade, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDepartment, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDkSet, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDotnetType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDriverBonus, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDriverMsgTemplate, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDriverStatus, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDriverVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDrivergroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterDrivergroupDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterEmail, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterEmailSchedulerevent, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterEmailSchedulerqueue, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterFactorValues, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterFactors, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoSegment, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoSegmentRuntime, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoSegmentVertex, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoZone, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGeoZonePrimitive, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGoods, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGoodsLogisticOrder, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGoodsType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGoodsTypeVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGraphic, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGraphicShift, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterGuardEvents, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterInfoSizeFiledata, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterJobDelbadxy, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterJournal, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterJournalDay, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterJournalDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterJournalVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterKolomnaPoints, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterKolomnaPoints2, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterKolomnaPoints3, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterLogOpsEvents, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterLogVehicleStatus, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterLogWaybillHeaderStatus, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterLogisticAddress, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterLogisticOrder, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMapVertex, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMaps, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMedia, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMediaAcceptors, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMediaType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessage, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessageBoard, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessageField, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessageOperator, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessageTemplate, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterMessageTemplateField, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperator, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorDepartment, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorDrivergroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorProfile, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorReport, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorRoute, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorRoutegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorVehiclegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorZone, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorZonegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupDepartment, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupDriver, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupDrivergroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupOperator, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupReport, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupRoute, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupRoutegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupVehiclegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupZone, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOperatorgroupZonegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOpsEvent, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOrder, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOrderTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOrderType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterOwner, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterParameter, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterPeriod, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterPoint, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterPointKind, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterReport, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterReprintReason, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterReserv, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRight, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRightOperator, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRightOperatorgroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRoute, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRouteGeozone, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRoutePoint, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRouteTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRoutegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRoutegroupRoute, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRs, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsNumber, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsOperationstype, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsOperationtype, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsPeriod, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsPoint, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsRoundTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsRuntime, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsShift, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsStep, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsTripstype, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsTriptype, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsVariant, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRsWayout, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterRule, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSchedule, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterScheduleDetail, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterScheduleDetailInfo, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterScheduleGeozone, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSchedulePassage, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSchedulePoint, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSchedulerevent, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSchedulerqueue, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSeason, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSeattype, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterServers, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSession, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterSmsType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTaskProcessorLog, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTempTss, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTmpdrivername, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTrail, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterTripKind, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehicleKind, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehicleOwner, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehiclePicture, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehicleRule, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehicleStatus, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehiclegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehiclegroupRule, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVehiclegroupVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterVersion, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWaybill, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWaybillHeader, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWaybillheaderWaybillmark, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWaybillmark, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWayout, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWbTrip, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWebLine, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWebLineVertex, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWebPoint, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWebPointType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWorkTime, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWorkplaceConfig, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWorkstation, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWorktimeReason, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterWorktimeStatusType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZonePrimitive, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZonePrimitiveVertex, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZoneType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZoneVehicle, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZoneVehiclegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZonegroup, connection, transaction);
			GenericHelpers.SetConnectionTransaction(this.adapterZonegroupZone, connection, transaction);
			#endregion
			// perform update
			SetupParameters();

			bool oldValue = dataset.EnforceConstraints;
			dataset.EnforceConstraints = false;
			try
			{
				ExecuteInsertCommands(dataset);
				ExecuteUpdateCommands(dataset);
				cascades.RunCascades();
			}
			finally
			{
				//dataset.EnforceConstraints = oldValue;
			}
		}
		public void ExecuteInsertCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Insert records to tables in insert order
			table = dataset.Tables["B_data"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBData.Update(rows);
				}
			}
			table = dataset.Tables["BLADING_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBladingType.Update(rows);
				}
			}
			table = dataset.Tables["BUSSTOP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBusstop.Update(rows);
				}
			}
			table = dataset.Tables["CALENDAR_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCalendarDay.Update(rows);
				}
			}
			table = dataset.Tables["CONSTANTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterConstants.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractor.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR_CALENDAR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractorCalendar.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorLegend.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensor.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GPRS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGprs.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_SMS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountSms.Update(rows);
				}
			}
			table = dataset.Tables["CUSTOMER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCustomer.Update(rows);
				}
			}
			table = dataset.Tables["DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDay.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayType.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayTime.Update(rows);
				}
			}
			table = dataset.Tables["DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDepartment.Update(rows);
				}
			}
			table = dataset.Tables["DECADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDecade.Update(rows);
				}
			}
			table = dataset.Tables["DK_SET"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDkSet.Update(rows);
				}
			}
			table = dataset.Tables["DAY_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayKind.Update(rows);
				}
			}
			table = dataset.Tables["CAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCal.Update(rows);
				}
			}
			table = dataset.Tables["DOTNET_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDotnetType.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_BONUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverBonus.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_MSG_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverMsgTemplate.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverStatus.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriver.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmail.Update(rows);
				}
			}
			table = dataset.Tables["FACTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactors.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZone.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsType.Update(rows);
				}
			}
			table = dataset.Tables["GOODS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoods.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphic.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphicShift.Update(rows);
				}
			}
			table = dataset.Tables["GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["Info_size_fileData"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterInfoSizeFiledata.Update(rows);
				}
			}
			table = dataset.Tables["Job_DelBadXY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJobDelbadxy.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDriver.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points2"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints2.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points3"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints3.Update(rows);
				}
			}
			table = dataset.Tables["LOG_GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ADDRESS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticAddress.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["MAPS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMaps.Update(rows);
				}
			}
			table = dataset.Tables["MAP_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMapVertex.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_BOARD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageBoard.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplate.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessage.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplateField.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageField.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_PROFILE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorProfile.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPS_EVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOpsEvent.Update(rows);
				}
			}
			table = dataset.Tables["LOG_OPS_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogOpsEvents.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderType.Update(rows);
				}
			}
			table = dataset.Tables["ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrder.Update(rows);
				}
			}
			table = dataset.Tables["OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOwner.Update(rows);
				}
			}
			table = dataset.Tables["PARAMETER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterParameter.Update(rows);
				}
			}
			table = dataset.Tables["POINT_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPointKind.Update(rows);
				}
			}
			table = dataset.Tables["POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPoint.Update(rows);
				}
			}
			table = dataset.Tables["REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReport.Update(rows);
				}
			}
			table = dataset.Tables["REPRINT_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReprintReason.Update(rows);
				}
			}
			table = dataset.Tables["RESERV"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReserv.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRight.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDepartment.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDepartment.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZone.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperator.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoute.Update(rows);
				}
			}
			table = dataset.Tables["GEO_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoTrip.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegment.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentRuntime.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentVertex.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoute.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteGeozone.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutePoint.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteTrip.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["RS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRs.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationstype.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTripstype.Update(rows);
				}
			}
			table = dataset.Tables["RS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsType.Update(rows);
				}
			}
			table = dataset.Tables["RS_VARIANT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsVariant.Update(rows);
				}
			}
			table = dataset.Tables["RS_PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPeriod.Update(rows);
				}
			}
			table = dataset.Tables["RS_ROUND_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRoundTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_STEP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsStep.Update(rows);
				}
			}
			table = dataset.Tables["RS_NUMBER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsNumber.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationtype.Update(rows);
				}
			}
			table = dataset.Tables["RS_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRuntime.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTriptype.Update(rows);
				}
			}
			table = dataset.Tables["RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRule.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["SEASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeason.Update(rows);
				}
			}
			table = dataset.Tables["FACTOR_VALUES"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactorValues.Update(rows);
				}
			}
			table = dataset.Tables["PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPeriod.Update(rows);
				}
			}
			table = dataset.Tables["SEATTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeattype.Update(rows);
				}
			}
			table = dataset.Tables["Servers"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterServers.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["SMS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsType.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["TASK_PROCESSOR_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTaskProcessorLog.Update(rows);
				}
			}
			table = dataset.Tables["Temp_TSS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTempTss.Update(rows);
				}
			}
			table = dataset.Tables["TmpDriverName"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTmpdrivername.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleKind.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigade.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigadeDriver.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterController.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerInfo.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorMap.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_STAT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerStat.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerTime.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GSM"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGsm.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverVehicle.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsTypeVehicle.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalVehicle.Update(rows);
				}
			}
			table = dataset.Tables["LOG_VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehicle.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["SMS_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsLog.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleOwner.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_PICTURE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclePicture.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleRule.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupRule.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["Version"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVersion.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybill.Update(rows);
				}
			}
			table = dataset.Tables["BLADING"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBlading.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWayout.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournal.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDay.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsWayout.Update(rows);
				}
			}
			table = dataset.Tables["RS_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsShift.Update(rows);
				}
			}
			table = dataset.Tables["RS_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPoint.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedule.Update(rows);
				}
			}
			table = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogWaybillHeaderStatus.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetail.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetailInfo.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLine.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLineVertex.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPointType.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPoint.Update(rows);
				}
			}
			table = dataset.Tables["WORK_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkTime.Update(rows);
				}
			}
			table = dataset.Tables["WORKPLACE_CONFIG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkplaceConfig.Update(rows);
				}
			}
			table = dataset.Tables["WORKSTATION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkstation.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_STATUS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeStatusType.Update(rows);
				}
			}
			table = dataset.Tables["TRIP_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTripKind.Update(rows);
				}
			}
			table = dataset.Tables["TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrip.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleGeozone.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePoint.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL_HEADER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillHeader.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_PASSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePassage.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillheaderWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeReason.Update(rows);
				}
			}
			table = dataset.Tables["WB_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWbTrip.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneType.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitiveVertex.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehicle.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONEGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroupZone.Update(rows);
				}
			}
			#endregion
		}
		public void ExecuteUpdateCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Update records in tables in insert order
			table = dataset.Tables["B_data"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBData.Update(rows);
				}
			}
			table = dataset.Tables["BLADING_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBladingType.Update(rows);
				}
			}
			table = dataset.Tables["BUSSTOP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBusstop.Update(rows);
				}
			}
			table = dataset.Tables["CALENDAR_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCalendarDay.Update(rows);
				}
			}
			table = dataset.Tables["CONSTANTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterConstants.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractor.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR_CALENDAR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractorCalendar.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorLegend.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensor.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GPRS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGprs.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_SMS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountSms.Update(rows);
				}
			}
			table = dataset.Tables["CUSTOMER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCustomer.Update(rows);
				}
			}
			table = dataset.Tables["DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDay.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayType.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayTime.Update(rows);
				}
			}
			table = dataset.Tables["DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDepartment.Update(rows);
				}
			}
			table = dataset.Tables["DECADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDecade.Update(rows);
				}
			}
			table = dataset.Tables["DK_SET"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDkSet.Update(rows);
				}
			}
			table = dataset.Tables["DAY_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayKind.Update(rows);
				}
			}
			table = dataset.Tables["CAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCal.Update(rows);
				}
			}
			table = dataset.Tables["DOTNET_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDotnetType.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_BONUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverBonus.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_MSG_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverMsgTemplate.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverStatus.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriver.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmail.Update(rows);
				}
			}
			table = dataset.Tables["FACTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactors.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZone.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsType.Update(rows);
				}
			}
			table = dataset.Tables["GOODS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoods.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphic.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphicShift.Update(rows);
				}
			}
			table = dataset.Tables["GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["Info_size_fileData"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterInfoSizeFiledata.Update(rows);
				}
			}
			table = dataset.Tables["Job_DelBadXY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJobDelbadxy.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDriver.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points2"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints2.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points3"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints3.Update(rows);
				}
			}
			table = dataset.Tables["LOG_GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ADDRESS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticAddress.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["MAPS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMaps.Update(rows);
				}
			}
			table = dataset.Tables["MAP_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMapVertex.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_BOARD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageBoard.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplate.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessage.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplateField.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageField.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_PROFILE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorProfile.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPS_EVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOpsEvent.Update(rows);
				}
			}
			table = dataset.Tables["LOG_OPS_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogOpsEvents.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderType.Update(rows);
				}
			}
			table = dataset.Tables["ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrder.Update(rows);
				}
			}
			table = dataset.Tables["OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOwner.Update(rows);
				}
			}
			table = dataset.Tables["PARAMETER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterParameter.Update(rows);
				}
			}
			table = dataset.Tables["POINT_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPointKind.Update(rows);
				}
			}
			table = dataset.Tables["POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPoint.Update(rows);
				}
			}
			table = dataset.Tables["REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReport.Update(rows);
				}
			}
			table = dataset.Tables["REPRINT_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReprintReason.Update(rows);
				}
			}
			table = dataset.Tables["RESERV"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReserv.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRight.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDepartment.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDepartment.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZone.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperator.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoute.Update(rows);
				}
			}
			table = dataset.Tables["GEO_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoTrip.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegment.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentRuntime.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentVertex.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoute.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteGeozone.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutePoint.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteTrip.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["RS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRs.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationstype.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTripstype.Update(rows);
				}
			}
			table = dataset.Tables["RS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsType.Update(rows);
				}
			}
			table = dataset.Tables["RS_VARIANT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsVariant.Update(rows);
				}
			}
			table = dataset.Tables["RS_PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPeriod.Update(rows);
				}
			}
			table = dataset.Tables["RS_ROUND_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRoundTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_STEP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsStep.Update(rows);
				}
			}
			table = dataset.Tables["RS_NUMBER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsNumber.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationtype.Update(rows);
				}
			}
			table = dataset.Tables["RS_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRuntime.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTriptype.Update(rows);
				}
			}
			table = dataset.Tables["RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRule.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["SEASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeason.Update(rows);
				}
			}
			table = dataset.Tables["FACTOR_VALUES"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactorValues.Update(rows);
				}
			}
			table = dataset.Tables["PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPeriod.Update(rows);
				}
			}
			table = dataset.Tables["SEATTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeattype.Update(rows);
				}
			}
			table = dataset.Tables["Servers"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterServers.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["SMS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsType.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["TASK_PROCESSOR_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTaskProcessorLog.Update(rows);
				}
			}
			table = dataset.Tables["Temp_TSS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTempTss.Update(rows);
				}
			}
			table = dataset.Tables["TmpDriverName"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTmpdrivername.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleKind.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigade.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigadeDriver.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterController.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerInfo.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorMap.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_STAT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerStat.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerTime.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GSM"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGsm.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverVehicle.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsTypeVehicle.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalVehicle.Update(rows);
				}
			}
			table = dataset.Tables["LOG_VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehicle.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["SMS_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsLog.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleOwner.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_PICTURE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclePicture.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleRule.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupRule.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["Version"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVersion.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybill.Update(rows);
				}
			}
			table = dataset.Tables["BLADING"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBlading.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWayout.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournal.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDay.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsWayout.Update(rows);
				}
			}
			table = dataset.Tables["RS_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsShift.Update(rows);
				}
			}
			table = dataset.Tables["RS_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPoint.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedule.Update(rows);
				}
			}
			table = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogWaybillHeaderStatus.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetail.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetailInfo.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLine.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLineVertex.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPointType.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPoint.Update(rows);
				}
			}
			table = dataset.Tables["WORK_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkTime.Update(rows);
				}
			}
			table = dataset.Tables["WORKPLACE_CONFIG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkplaceConfig.Update(rows);
				}
			}
			table = dataset.Tables["WORKSTATION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkstation.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_STATUS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeStatusType.Update(rows);
				}
			}
			table = dataset.Tables["TRIP_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTripKind.Update(rows);
				}
			}
			table = dataset.Tables["TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrip.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleGeozone.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePoint.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL_HEADER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillHeader.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_PASSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePassage.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillheaderWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeReason.Update(rows);
				}
			}
			table = dataset.Tables["WB_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWbTrip.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneType.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitiveVertex.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehicle.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONEGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroupZone.Update(rows);
				}
			}
			#endregion
		}
		public void ExecuteDeleteCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Update records in tables in delete order
			table = dataset.Tables["ZONEGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroupZone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneVehicle.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitiveVertex.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_PRIMITIVE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZonePrimitive.Update(rows);
				}
			}
			table = dataset.Tables["ZONE_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterZoneType.Update(rows);
				}
			}
			table = dataset.Tables["WB_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWbTrip.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeReason.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillheaderWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_PASSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePassage.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL_HEADER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillHeader.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulePoint.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleGeozone.Update(rows);
				}
			}
			table = dataset.Tables["TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrip.Update(rows);
				}
			}
			table = dataset.Tables["TRIP_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTripKind.Update(rows);
				}
			}
			table = dataset.Tables["WORKTIME_STATUS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorktimeStatusType.Update(rows);
				}
			}
			table = dataset.Tables["WORKSTATION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkstation.Update(rows);
				}
			}
			table = dataset.Tables["WORKPLACE_CONFIG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkplaceConfig.Update(rows);
				}
			}
			table = dataset.Tables["WORK_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWorkTime.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPoint.Update(rows);
				}
			}
			table = dataset.Tables["WEB_POINT_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebPointType.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLineVertex.Update(rows);
				}
			}
			table = dataset.Tables["WEB_LINE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWebLine.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetailInfo.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE_DETAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterScheduleDetail.Update(rows);
				}
			}
			table = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogWaybillHeaderStatus.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedule.Update(rows);
				}
			}
			table = dataset.Tables["RS_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPoint.Update(rows);
				}
			}
			table = dataset.Tables["RS_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsShift.Update(rows);
				}
			}
			table = dataset.Tables["RS_WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsWayout.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderTrip.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDay.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournal.Update(rows);
				}
			}
			table = dataset.Tables["WAYOUT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWayout.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILLMARK"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybillmark.Update(rows);
				}
			}
			table = dataset.Tables["BLADING"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBlading.Update(rows);
				}
			}
			table = dataset.Tables["WAYBILL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterWaybill.Update(rows);
				}
			}
			table = dataset.Tables["Version"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVersion.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroupRule.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclegroup.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleRule.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_PICTURE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehiclePicture.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleOwner.Update(rows);
				}
			}
			table = dataset.Tables["SMS_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsLog.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupVehicle.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorVehicle.Update(rows);
				}
			}
			table = dataset.Tables["LOG_VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalVehicle.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsTypeVehicle.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverVehicle.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GSM"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGsm.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerTime.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_STAT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerStat.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorMap.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerInfo.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterController.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigadeDriver.Update(rows);
				}
			}
			table = dataset.Tables["BRIGADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBrigade.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleStatus.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterVehicleKind.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["TmpDriverName"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTmpdrivername.Update(rows);
				}
			}
			table = dataset.Tables["Temp_TSS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTempTss.Update(rows);
				}
			}
			table = dataset.Tables["TASK_PROCESSOR_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterTaskProcessorLog.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["SMS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSmsType.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["Servers"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterServers.Update(rows);
				}
			}
			table = dataset.Tables["SEATTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeattype.Update(rows);
				}
			}
			table = dataset.Tables["PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPeriod.Update(rows);
				}
			}
			table = dataset.Tables["FACTOR_VALUES"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactorValues.Update(rows);
				}
			}
			table = dataset.Tables["SEASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSeason.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULERQUEUE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerqueue.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL_SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmailSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["SCHEDULEREVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterSchedulerevent.Update(rows);
				}
			}
			table = dataset.Tables["RULE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRule.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTriptype.Update(rows);
				}
			}
			table = dataset.Tables["RS_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRuntime.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationtype.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_NUMBER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsNumber.Update(rows);
				}
			}
			table = dataset.Tables["RS_STEP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsStep.Update(rows);
				}
			}
			table = dataset.Tables["RS_ROUND_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsRoundTrip.Update(rows);
				}
			}
			table = dataset.Tables["RS_PERIOD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsPeriod.Update(rows);
				}
			}
			table = dataset.Tables["RS_VARIANT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsVariant.Update(rows);
				}
			}
			table = dataset.Tables["RS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsType.Update(rows);
				}
			}
			table = dataset.Tables["RS_TRIPSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsTripstype.Update(rows);
				}
			}
			table = dataset.Tables["RS_OPERATIONSTYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRsOperationstype.Update(rows);
				}
			}
			table = dataset.Tables["RS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRs.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTEGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutegroup.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteTrip.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoutePoint.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE_GEOZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRouteGeozone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupRoute.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorRoute.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentVertex.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT_RUNTIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegmentRuntime.Update(rows);
				}
			}
			table = dataset.Tables["GEO_SEGMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoSegment.Update(rows);
				}
			}
			table = dataset.Tables["GEO_TRIP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoTrip.Update(rows);
				}
			}
			table = dataset.Tables["ROUTE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRoute.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRightOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupZone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupDepartment.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorZone.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorReport.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDriver.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorDepartment.Update(rows);
				}
			}
			table = dataset.Tables["RIGHT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterRight.Update(rows);
				}
			}
			table = dataset.Tables["RESERV"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReserv.Update(rows);
				}
			}
			table = dataset.Tables["REPRINT_REASON"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReprintReason.Update(rows);
				}
			}
			table = dataset.Tables["REPORT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterReport.Update(rows);
				}
			}
			table = dataset.Tables["POINT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPoint.Update(rows);
				}
			}
			table = dataset.Tables["POINT_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterPointKind.Update(rows);
				}
			}
			table = dataset.Tables["PARAMETER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterParameter.Update(rows);
				}
			}
			table = dataset.Tables["OWNER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOwner.Update(rows);
				}
			}
			table = dataset.Tables["ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrder.Update(rows);
				}
			}
			table = dataset.Tables["ORDER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOrderType.Update(rows);
				}
			}
			table = dataset.Tables["LOG_OPS_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogOpsEvents.Update(rows);
				}
			}
			table = dataset.Tables["OPS_EVENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOpsEvent.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroupOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPERATORGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorgroup.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR_PROFILE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperatorProfile.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageOperator.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageField.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE_FIELD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplateField.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessage.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageTemplate.Update(rows);
				}
			}
			table = dataset.Tables["MESSAGE_BOARD"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMessageBoard.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["MAP_VERTEX"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMapVertex.Update(rows);
				}
			}
			table = dataset.Tables["MAPS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterMaps.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ORDER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticOrder.Update(rows);
				}
			}
			table = dataset.Tables["LOGISTIC_ADDRESS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogisticAddress.Update(rows);
				}
			}
			table = dataset.Tables["LOG_GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterLogGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points3"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints3.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points2"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints2.Update(rows);
				}
			}
			table = dataset.Tables["kolomna_points"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterKolomnaPoints.Update(rows);
				}
			}
			table = dataset.Tables["JOURNAL_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJournalDriver.Update(rows);
				}
			}
			table = dataset.Tables["Job_DelBadXY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterJobDelbadxy.Update(rows);
				}
			}
			table = dataset.Tables["Info_size_fileData"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterInfoSizeFiledata.Update(rows);
				}
			}
			table = dataset.Tables["GUARD_EVENTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGuardEvents.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC_SHIFT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphicShift.Update(rows);
				}
			}
			table = dataset.Tables["GRAPHIC"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGraphic.Update(rows);
				}
			}
			table = dataset.Tables["GOODS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoods.Update(rows);
				}
			}
			table = dataset.Tables["GOODS_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGoodsType.Update(rows);
				}
			}
			table = dataset.Tables["GEO_ZONE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterGeoZone.Update(rows);
				}
			}
			table = dataset.Tables["FACTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterFactors.Update(rows);
				}
			}
			table = dataset.Tables["EMAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterEmail.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP_DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroupDriver.Update(rows);
				}
			}
			table = dataset.Tables["DRIVERGROUP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDrivergroup.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriver.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_STATUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverStatus.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_MSG_TEMPLATE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverMsgTemplate.Update(rows);
				}
			}
			table = dataset.Tables["DRIVER_BONUS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDriverBonus.Update(rows);
				}
			}
			table = dataset.Tables["DOTNET_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDotnetType.Update(rows);
				}
			}
			table = dataset.Tables["CAL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCal.Update(rows);
				}
			}
			table = dataset.Tables["DAY_KIND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayKind.Update(rows);
				}
			}
			table = dataset.Tables["DK_SET"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDkSet.Update(rows);
				}
			}
			table = dataset.Tables["DECADE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDecade.Update(rows);
				}
			}
			table = dataset.Tables["DEPARTMENT"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDepartment.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TIME"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayTime.Update(rows);
				}
			}
			table = dataset.Tables["DAY_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDayType.Update(rows);
				}
			}
			table = dataset.Tables["DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterDay.Update(rows);
				}
			}
			table = dataset.Tables["CUSTOMER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCustomer.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_SMS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountSms.Update(rows);
				}
			}
			table = dataset.Tables["COUNT_GPRS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCountGprs.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensor.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorLegend.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterControllerSensorType.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR_CALENDAR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractorCalendar.Update(rows);
				}
			}
			table = dataset.Tables["CONTRACTOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterContractor.Update(rows);
				}
			}
			table = dataset.Tables["CONSTANTS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterConstants.Update(rows);
				}
			}
			table = dataset.Tables["CALENDAR_DAY"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterCalendarDay.Update(rows);
				}
			}
			table = dataset.Tables["BUSSTOP"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBusstop.Update(rows);
				}
			}
			table = dataset.Tables["BLADING_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBladingType.Update(rows);
				}
			}
			table = dataset.Tables["B_data"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					this.adapterBData.Update(rows);
				}
			}
			#endregion
		}
	}
}