using FORIS.DataAccess;

namespace FORIS.DataAccess
{
    using System.Data.SqlClient;
    using System.Data.SqlTypes;
    
    
    /// <summary>
    /// <P>Allows to build SQL commands</P>
    /// </summary>
    public class UpdateSql
    {
        
        /// <summary>
        /// Backer for InsertCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theInsertCommand;
        
        /// <summary>
        /// Backer for UpdateCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theUpdateCommand;
        
        /// <summary>
        /// Backer for DeleteCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theDeleteCommand;
        
        private UpdateSql()
        {
        }
        
        /// <summary>
        /// Accessor for InsertCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand InsertCommand
        {
            get
            {
return theInsertCommand;
            }
        }
        
        /// <summary>
        /// Accessor for UpdateCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand UpdateCommand
        {
            get
            {
return theUpdateCommand;
            }
        }
        
        /// <summary>
        /// Accessor for DeleteCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand DeleteCommand
        {
            get
            {
return theDeleteCommand;
            }
        }
        
        public static UpdateSql BData()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBData");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBData");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBData");
return u;
        }
        
        public static UpdateSql Blading()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBlading");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBlading");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBlading");
return u;
        }
        
        public static UpdateSql BladingType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBladingType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBladingType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBladingType");
return u;
        }
        
        public static UpdateSql Brigade()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBrigade");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBrigade");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBrigade");
return u;
        }
        
        public static UpdateSql BrigadeDriver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBrigadeDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBrigadeDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBrigadeDriver");
return u;
        }
        
        public static UpdateSql Busstop()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertBusstop");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateBusstop");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteBusstop");
return u;
        }
        
        public static UpdateSql Cal()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertCal");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateCal");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteCal");
return u;
        }
        
        public static UpdateSql CalendarDay()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertCalendarDay");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateCalendarDay");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteCalendarDay");
return u;
        }
        
        public static UpdateSql Constants()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertConstants");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateConstants");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteConstants");
return u;
        }
        
        public static UpdateSql Contractor()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertContractor");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateContractor");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteContractor");
return u;
        }
        
        public static UpdateSql ContractorCalendar()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertContractorCalendar");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateContractorCalendar");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteContractorCalendar");
return u;
        }
        
        public static UpdateSql Controller()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertController");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateController");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteController");
return u;
        }
        
        public static UpdateSql ControllerInfo()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerInfo");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerInfo");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerInfo");
return u;
        }
        
        public static UpdateSql ControllerSensor()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerSensor");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerSensor");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerSensor");
return u;
        }
        
        public static UpdateSql ControllerSensorLegend()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerSensorLegend");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerSensorLegend");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerSensorLegend");
return u;
        }
        
        public static UpdateSql ControllerSensorMap()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerSensorMap");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerSensorMap");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerSensorMap");
return u;
        }
        
        public static UpdateSql ControllerSensorType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerSensorType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerSensorType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerSensorType");
return u;
        }
        
        public static UpdateSql ControllerTime()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerTime");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerTime");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerTime");
return u;
        }
        
        public static UpdateSql ControllerType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerType");
return u;
        }
        
        public static UpdateSql CountGprs()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertCountGprs");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateCountGprs");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteCountGprs");
return u;
        }
        
        public static UpdateSql Customer()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertCustomer");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateCustomer");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteCustomer");
return u;
        }
        
        public static UpdateSql Day()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDay");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDay");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDay");
return u;
        }
        
        public static UpdateSql DayKind()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDayKind");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDayKind");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDayKind");
return u;
        }
        
        public static UpdateSql DayTime()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDayTime");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDayTime");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDayTime");
return u;
        }
        
        public static UpdateSql DayType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDayType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDayType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDayType");
return u;
        }
        
        public static UpdateSql Decade()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDecade");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDecade");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDecade");
return u;
        }
        
        public static UpdateSql Department()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDepartment");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDepartment");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDepartment");
return u;
        }
        
        public static UpdateSql DkSet()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDkSet");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDkSet");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDkSet");
return u;
        }
        
        public static UpdateSql DotnetType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDotnetType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDotnetType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDotnetType");
return u;
        }
        
        public static UpdateSql Driver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDriver");
return u;
        }
        
        public static UpdateSql DriverBonus()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDriverBonus");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDriverBonus");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDriverBonus");
return u;
        }
        
        public static UpdateSql DriverMsgTemplate()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDriverMsgTemplate");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDriverMsgTemplate");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDriverMsgTemplate");
return u;
        }
        
        public static UpdateSql DriverStatus()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDriverStatus");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDriverStatus");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDriverStatus");
return u;
        }
        
        public static UpdateSql DriverVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDriverVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDriverVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDriverVehicle");
return u;
        }
        
        public static UpdateSql Drivergroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDrivergroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDrivergroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDrivergroup");
return u;
        }
        
        public static UpdateSql DrivergroupDriver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertDrivergroupDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateDrivergroupDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteDrivergroupDriver");
return u;
        }
        
        public static UpdateSql Email()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertEmail");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateEmail");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteEmail");
return u;
        }
        
        public static UpdateSql EmailSchedulerevent()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertEmailSchedulerevent");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateEmailSchedulerevent");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteEmailSchedulerevent");
return u;
        }
        
        public static UpdateSql EmailSchedulerqueue()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertEmailSchedulerqueue");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateEmailSchedulerqueue");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteEmailSchedulerqueue");
return u;
        }
        
        public static UpdateSql FactorValues()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertFactorValues");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateFactorValues");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteFactorValues");
return u;
        }
        
        public static UpdateSql Factors()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertFactors");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateFactors");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteFactors");
return u;
        }
        
        public static UpdateSql GeoSegment()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoSegment");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoSegment");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoSegment");
return u;
        }
        
        public static UpdateSql GeoSegmentRuntime()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoSegmentRuntime");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoSegmentRuntime");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoSegmentRuntime");
return u;
        }
        
        public static UpdateSql GeoSegmentVertex()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoSegmentVertex");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoSegmentVertex");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoSegmentVertex");
return u;
        }
        
        public static UpdateSql GeoTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoTrip");
return u;
        }
        
        public static UpdateSql GeoZone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoZone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoZone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoZone");
return u;
        }
        
        public static UpdateSql GeoZonePrimitive()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGeoZonePrimitive");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGeoZonePrimitive");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGeoZonePrimitive");
return u;
        }
        
        public static UpdateSql Goods()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGoods");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGoods");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGoods");
return u;
        }
        
        public static UpdateSql GoodsLogisticOrder()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGoodsLogisticOrder");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGoodsLogisticOrder");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGoodsLogisticOrder");
return u;
        }
        
        public static UpdateSql GoodsType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGoodsType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGoodsType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGoodsType");
return u;
        }
        
        public static UpdateSql GoodsTypeVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGoodsTypeVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGoodsTypeVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGoodsTypeVehicle");
return u;
        }
        
        public static UpdateSql Graphic()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGraphic");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGraphic");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGraphic");
return u;
        }
        
        public static UpdateSql GraphicShift()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGraphicShift");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGraphicShift");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGraphicShift");
return u;
        }
        
        public static UpdateSql GuardEvents()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertGuardEvents");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateGuardEvents");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteGuardEvents");
return u;
        }
        
        public static UpdateSql InfoSizeFiledata()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertInfoSizeFiledata");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateInfoSizeFiledata");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteInfoSizeFiledata");
return u;
        }
        
        public static UpdateSql JobDelbadxy()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertJobDelbadxy");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateJobDelbadxy");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteJobDelbadxy");
return u;
        }
        
        public static UpdateSql Journal()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertJournal");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateJournal");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteJournal");
return u;
        }
        
        public static UpdateSql JournalDay()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertJournalDay");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateJournalDay");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteJournalDay");
return u;
        }
        
        public static UpdateSql JournalDriver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertJournalDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateJournalDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteJournalDriver");
return u;
        }
        
        public static UpdateSql JournalVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertJournalVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateJournalVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteJournalVehicle");
return u;
        }
        
        public static UpdateSql KolomnaPoints()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertKolomnaPoints");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateKolomnaPoints");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteKolomnaPoints");
return u;
        }
        
        public static UpdateSql KolomnaPoints2()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertKolomnaPoints2");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateKolomnaPoints2");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteKolomnaPoints2");
return u;
        }
        
        public static UpdateSql KolomnaPoints3()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertKolomnaPoints3");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateKolomnaPoints3");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteKolomnaPoints3");
return u;
        }
        
        public static UpdateSql LogOpsEvents()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertLogOpsEvents");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateLogOpsEvents");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteLogOpsEvents");
return u;
        }
        
        public static UpdateSql LogVehicleStatus()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertLogVehicleStatus");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateLogVehicleStatus");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteLogVehicleStatus");
return u;
        }
        
        public static UpdateSql LogWaybillHeaderStatus()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertLogWaybillHeaderStatus");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateLogWaybillHeaderStatus");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteLogWaybillHeaderStatus");
return u;
        }
        
        public static UpdateSql LogisticAddress()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertLogisticAddress");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateLogisticAddress");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteLogisticAddress");
return u;
        }
        
        public static UpdateSql LogisticOrder()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertLogisticOrder");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateLogisticOrder");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteLogisticOrder");
return u;
        }
        
        public static UpdateSql MapVertex()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMapVertex");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMapVertex");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMapVertex");
return u;
        }
        
        public static UpdateSql Maps()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMaps");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMaps");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMaps");
return u;
        }
        
        public static UpdateSql Media()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMedia");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMedia");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMedia");
return u;
        }
        
        public static UpdateSql MediaAcceptors()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMediaAcceptors");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMediaAcceptors");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMediaAcceptors");
return u;
        }
        
        public static UpdateSql MediaType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMediaType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMediaType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMediaType");
return u;
        }
        
        public static UpdateSql Message()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessage");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessage");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessage");
return u;
        }
        
        public static UpdateSql MessageBoard()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessageBoard");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessageBoard");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessageBoard");
return u;
        }
        
        public static UpdateSql MessageField()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessageField");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessageField");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessageField");
return u;
        }
        
        public static UpdateSql MessageOperator()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessageOperator");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessageOperator");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessageOperator");
return u;
        }
        
        public static UpdateSql MessageTemplate()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessageTemplate");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessageTemplate");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessageTemplate");
return u;
        }
        
        public static UpdateSql MessageTemplateField()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMessageTemplateField");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMessageTemplateField");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMessageTemplateField");
return u;
        }
        
        public static UpdateSql Operator()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperator");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperator");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperator");
return u;
        }
        
        public static UpdateSql OperatorDepartment()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorDepartment");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorDepartment");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorDepartment");
return u;
        }
        
        public static UpdateSql OperatorDriver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorDriver");
return u;
        }
        
        public static UpdateSql OperatorDrivergroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorDrivergroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorDrivergroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorDrivergroup");
return u;
        }
        
        public static UpdateSql OperatorProfile()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorProfile");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorProfile");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorProfile");
return u;
        }
        
        public static UpdateSql OperatorReport()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorReport");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorReport");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorReport");
return u;
        }
        
        public static UpdateSql OperatorRoute()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorRoute");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorRoute");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorRoute");
return u;
        }
        
        public static UpdateSql OperatorRoutegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorRoutegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorRoutegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorRoutegroup");
return u;
        }
        
        public static UpdateSql OperatorVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorVehicle");
return u;
        }
        
        public static UpdateSql OperatorVehiclegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorVehiclegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorVehiclegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorVehiclegroup");
return u;
        }
        
        public static UpdateSql OperatorZone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorZone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorZone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorZone");
return u;
        }
        
        public static UpdateSql OperatorZonegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorZonegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorZonegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorZonegroup");
return u;
        }
        
        public static UpdateSql Operatorgroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroup");
return u;
        }
        
        public static UpdateSql OperatorgroupDepartment()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupDepartment");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupDepartment");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupDepartment");
return u;
        }
        
        public static UpdateSql OperatorgroupDriver()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupDriver");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupDriver");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupDriver");
return u;
        }
        
        public static UpdateSql OperatorgroupDrivergroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupDrivergroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupDrivergroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupDrivergroup");
return u;
        }
        
        public static UpdateSql OperatorgroupOperator()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupOperator");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupOperator");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupOperator");
return u;
        }
        
        public static UpdateSql OperatorgroupReport()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupReport");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupReport");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupReport");
return u;
        }
        
        public static UpdateSql OperatorgroupRoute()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupRoute");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupRoute");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupRoute");
return u;
        }
        
        public static UpdateSql OperatorgroupRoutegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupRoutegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupRoutegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupRoutegroup");
return u;
        }
        
        public static UpdateSql OperatorgroupVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupVehicle");
return u;
        }
        
        public static UpdateSql OperatorgroupVehiclegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupVehiclegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupVehiclegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupVehiclegroup");
return u;
        }
        
        public static UpdateSql OperatorgroupZone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupZone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupZone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupZone");
return u;
        }
        
        public static UpdateSql OperatorgroupZonegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperatorgroupZonegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperatorgroupZonegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperatorgroupZonegroup");
return u;
        }
        
        public static UpdateSql OpsEvent()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOpsEvent");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOpsEvent");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOpsEvent");
return u;
        }
        
        public static UpdateSql Order()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOrder");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOrder");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOrder");
return u;
        }
        
        public static UpdateSql OrderTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOrderTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOrderTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOrderTrip");
return u;
        }
        
        public static UpdateSql OrderType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOrderType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOrderType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOrderType");
return u;
        }
        
        public static UpdateSql Owner()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOwner");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOwner");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOwner");
return u;
        }
        
        public static UpdateSql Parameter()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertParameter");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateParameter");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteParameter");
return u;
        }
        
        public static UpdateSql Period()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertPeriod");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdatePeriod");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeletePeriod");
return u;
        }
        
        public static UpdateSql Point()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertPoint");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdatePoint");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeletePoint");
return u;
        }
        
        public static UpdateSql PointKind()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertPointKind");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdatePointKind");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeletePointKind");
return u;
        }
        
        public static UpdateSql Report()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertReport");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateReport");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteReport");
return u;
        }
        
        public static UpdateSql ReprintReason()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertReprintReason");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateReprintReason");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteReprintReason");
return u;
        }
        
        public static UpdateSql Reserv()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertReserv");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateReserv");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteReserv");
return u;
        }
        
        public static UpdateSql Right()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRight");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRight");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRight");
return u;
        }
        
        public static UpdateSql RightOperator()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRightOperator");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRightOperator");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRightOperator");
return u;
        }
        
        public static UpdateSql RightOperatorgroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRightOperatorgroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRightOperatorgroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRightOperatorgroup");
return u;
        }
        
        public static UpdateSql Route()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRoute");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRoute");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRoute");
return u;
        }
        
        public static UpdateSql RouteGeozone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRouteGeozone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRouteGeozone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRouteGeozone");
return u;
        }
        
        public static UpdateSql RoutePoint()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRoutePoint");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRoutePoint");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRoutePoint");
return u;
        }
        
        public static UpdateSql RouteTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRouteTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRouteTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRouteTrip");
return u;
        }
        
        public static UpdateSql Routegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRoutegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRoutegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRoutegroup");
return u;
        }
        
        public static UpdateSql RoutegroupRoute()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRoutegroupRoute");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRoutegroupRoute");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRoutegroupRoute");
return u;
        }
        
        public static UpdateSql Rs()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRs");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRs");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRs");
return u;
        }
        
        public static UpdateSql RsNumber()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsNumber");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsNumber");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsNumber");
return u;
        }
        
        public static UpdateSql RsOperationstype()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsOperationstype");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsOperationstype");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsOperationstype");
return u;
        }
        
        public static UpdateSql RsOperationtype()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsOperationtype");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsOperationtype");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsOperationtype");
return u;
        }
        
        public static UpdateSql RsPeriod()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsPeriod");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsPeriod");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsPeriod");
return u;
        }
        
        public static UpdateSql RsPoint()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsPoint");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsPoint");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsPoint");
return u;
        }
        
        public static UpdateSql RsRoundTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsRoundTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsRoundTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsRoundTrip");
return u;
        }
        
        public static UpdateSql RsRuntime()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsRuntime");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsRuntime");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsRuntime");
return u;
        }
        
        public static UpdateSql RsShift()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsShift");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsShift");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsShift");
return u;
        }
        
        public static UpdateSql RsStep()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsStep");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsStep");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsStep");
return u;
        }
        
        public static UpdateSql RsTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsTrip");
return u;
        }
        
        public static UpdateSql RsTripstype()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsTripstype");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsTripstype");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsTripstype");
return u;
        }
        
        public static UpdateSql RsTriptype()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsTriptype");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsTriptype");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsTriptype");
return u;
        }
        
        public static UpdateSql RsType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsType");
return u;
        }
        
        public static UpdateSql RsVariant()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsVariant");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsVariant");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsVariant");
return u;
        }
        
        public static UpdateSql RsWayout()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRsWayout");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRsWayout");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRsWayout");
return u;
        }
        
        public static UpdateSql Rule()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertRule");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateRule");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteRule");
return u;
        }
        
        public static UpdateSql Schedule()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSchedule");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSchedule");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSchedule");
return u;
        }
        
        public static UpdateSql ScheduleDetail()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertScheduleDetail");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateScheduleDetail");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteScheduleDetail");
return u;
        }
        
        public static UpdateSql ScheduleDetailInfo()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertScheduleDetailInfo");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateScheduleDetailInfo");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteScheduleDetailInfo");
return u;
        }
        
        public static UpdateSql ScheduleGeozone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertScheduleGeozone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateScheduleGeozone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteScheduleGeozone");
return u;
        }
        
        public static UpdateSql SchedulePassage()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSchedulePassage");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSchedulePassage");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSchedulePassage");
return u;
        }
        
        public static UpdateSql SchedulePoint()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSchedulePoint");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSchedulePoint");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSchedulePoint");
return u;
        }
        
        public static UpdateSql Schedulerevent()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSchedulerevent");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSchedulerevent");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSchedulerevent");
return u;
        }
        
        public static UpdateSql Schedulerqueue()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSchedulerqueue");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSchedulerqueue");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSchedulerqueue");
return u;
        }
        
        public static UpdateSql Season()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSeason");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSeason");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSeason");
return u;
        }
        
        public static UpdateSql Seattype()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSeattype");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSeattype");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSeattype");
return u;
        }
        
        public static UpdateSql Servers()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertServers");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateServers");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteServers");
return u;
        }
        
        public static UpdateSql Session()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSession");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSession");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSession");
return u;
        }
        
        public static UpdateSql SmsType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSmsType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSmsType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSmsType");
return u;
        }
        
        public static UpdateSql TaskProcessorLog()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTaskProcessorLog");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTaskProcessorLog");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTaskProcessorLog");
return u;
        }
        
        public static UpdateSql TempTss()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTempTss");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTempTss");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTempTss");
return u;
        }
        
        public static UpdateSql Tmpdrivername()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTmpdrivername");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTmpdrivername");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTmpdrivername");
return u;
        }
        
        public static UpdateSql Trail()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTrail");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTrail");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTrail");
return u;
        }
        
        public static UpdateSql Trip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTrip");
return u;
        }
        
        public static UpdateSql TripKind()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTripKind");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTripKind");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTripKind");
return u;
        }
        
        public static UpdateSql Vehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicle");
return u;
        }
        
        public static UpdateSql VehicleKind()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicleKind");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicleKind");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicleKind");
return u;
        }
        
        public static UpdateSql VehicleOwner()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicleOwner");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicleOwner");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicleOwner");
return u;
        }
        
        public static UpdateSql VehiclePicture()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehiclePicture");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehiclePicture");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehiclePicture");
return u;
        }
        
        public static UpdateSql VehicleRule()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicleRule");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicleRule");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicleRule");
return u;
        }
        
        public static UpdateSql VehicleStatus()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicleStatus");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicleStatus");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicleStatus");
return u;
        }
        
        public static UpdateSql Vehiclegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehiclegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehiclegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehiclegroup");
return u;
        }
        
        public static UpdateSql VehiclegroupRule()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehiclegroupRule");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehiclegroupRule");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehiclegroupRule");
return u;
        }
        
        public static UpdateSql VehiclegroupVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehiclegroupVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehiclegroupVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehiclegroupVehicle");
return u;
        }
        
        public static UpdateSql Version()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVersion");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVersion");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVersion");
return u;
        }
        
        public static UpdateSql Waybill()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWaybill");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWaybill");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWaybill");
return u;
        }
        
        public static UpdateSql WaybillHeader()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWaybillHeader");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWaybillHeader");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWaybillHeader");
return u;
        }
        
        public static UpdateSql WaybillheaderWaybillmark()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWaybillheaderWaybillmark");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWaybillheaderWaybillmark");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWaybillheaderWaybillmark");
return u;
        }
        
        public static UpdateSql Waybillmark()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWaybillmark");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWaybillmark");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWaybillmark");
return u;
        }
        
        public static UpdateSql Wayout()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWayout");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWayout");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWayout");
return u;
        }
        
        public static UpdateSql WbTrip()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWbTrip");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWbTrip");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWbTrip");
return u;
        }
        
        public static UpdateSql WebLine()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWebLine");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWebLine");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWebLine");
return u;
        }
        
        public static UpdateSql WebLineVertex()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWebLineVertex");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWebLineVertex");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWebLineVertex");
return u;
        }
        
        public static UpdateSql WebPoint()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWebPoint");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWebPoint");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWebPoint");
return u;
        }
        
        public static UpdateSql WebPointType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWebPointType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWebPointType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWebPointType");
return u;
        }
        
        public static UpdateSql WorkTime()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWorkTime");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWorkTime");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWorkTime");
return u;
        }
        
        public static UpdateSql WorkplaceConfig()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWorkplaceConfig");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWorkplaceConfig");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWorkplaceConfig");
return u;
        }
        
        public static UpdateSql Workstation()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWorkstation");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWorkstation");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWorkstation");
return u;
        }
        
        public static UpdateSql WorktimeReason()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWorktimeReason");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWorktimeReason");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWorktimeReason");
return u;
        }
        
        public static UpdateSql WorktimeStatusType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertWorktimeStatusType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateWorktimeStatusType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteWorktimeStatusType");
return u;
        }
        
        public static UpdateSql ZonePrimitive()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZonePrimitive");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZonePrimitive");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZonePrimitive");
return u;
        }
        
        public static UpdateSql ZonePrimitiveVertex()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZonePrimitiveVertex");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZonePrimitiveVertex");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZonePrimitiveVertex");
return u;
        }
        
        public static UpdateSql ZoneType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZoneType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZoneType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZoneType");
return u;
        }
        
        public static UpdateSql ZoneVehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZoneVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZoneVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZoneVehicle");
return u;
        }
        
        public static UpdateSql ZoneVehiclegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZoneVehiclegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZoneVehiclegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZoneVehiclegroup");
return u;
        }
        
        public static UpdateSql Zonegroup()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZonegroup");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZonegroup");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZonegroup");
return u;
        }
        
        public static UpdateSql ZonegroupZone()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertZonegroupZone");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateZonegroupZone");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteZonegroupZone");
return u;
        }
    }
}
