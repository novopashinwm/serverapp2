﻿using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.DataAccess
{
	/// <summary> Implements tables for the cascade deletion </summary>
	public class Cascades : CascadesBase
	{

		private System.Data.IDbConnection connection;

		private System.Data.IDbTransaction transaction;

		public Hashtable hashKey_BData = new Hashtable();

		public Hashtable hashKey_Blading = new Hashtable();

		public Hashtable hashKey_BladingType = new Hashtable();

		public Hashtable hashKey_Brigade = new Hashtable();

		public Hashtable hashKey_BrigadeDriver = new Hashtable();

		public Hashtable hashKey_Busstop = new Hashtable();

		public Hashtable hashKey_Cal = new Hashtable();

		public Hashtable hashKey_CalendarDay = new Hashtable();

		public Hashtable hashKey_Constants = new Hashtable();

		public Hashtable hashKey_Contractor = new Hashtable();

		public Hashtable hashKey_ContractorCalendar = new Hashtable();

		public Hashtable hashKey_Controller = new Hashtable();

		public Hashtable hashKey_ControllerInfo = new Hashtable();

		public Hashtable hashKey_ControllerSensor = new Hashtable();

		public Hashtable hashKey_ControllerSensorLegend = new Hashtable();

		public Hashtable hashKey_ControllerSensorMap = new Hashtable();

		public Hashtable hashKey_ControllerSensorType = new Hashtable();

		public Hashtable hashKey_ControllerStat = new Hashtable();

		public Hashtable hashKey_ControllerTime = new Hashtable();

		public Hashtable hashKey_ControllerType = new Hashtable();

		public Hashtable hashKey_CountGprs = new Hashtable();

		public Hashtable hashKey_CountGsm = new Hashtable();

		public Hashtable hashKey_CountSms = new Hashtable();

		public Hashtable hashKey_Customer = new Hashtable();

		public Hashtable hashKey_Day = new Hashtable();

		public Hashtable hashKey_DayKind = new Hashtable();

		public Hashtable hashKey_DayTime = new Hashtable();

		public Hashtable hashKey_DayType = new Hashtable();

		public Hashtable hashKey_Decade = new Hashtable();

		public Hashtable hashKey_Department = new Hashtable();

		public Hashtable hashKey_DkSet = new Hashtable();

		public Hashtable hashKey_DotnetType = new Hashtable();

		public Hashtable hashKey_Driver = new Hashtable();

		public Hashtable hashKey_DriverBonus = new Hashtable();

		public Hashtable hashKey_DriverMsgTemplate = new Hashtable();

		public Hashtable hashKey_DriverStatus = new Hashtable();

		public Hashtable hashKey_DriverVehicle = new Hashtable();

		public Hashtable hashKey_Drivergroup = new Hashtable();

		public Hashtable hashKey_DrivergroupDriver = new Hashtable();

		public Hashtable hashKey_Email = new Hashtable();

		public Hashtable hashKey_EmailSchedulerevent = new Hashtable();

		public Hashtable hashKey_EmailSchedulerqueue = new Hashtable();

		public Hashtable hashKey_FactorValues = new Hashtable();

		public Hashtable hashKey_Factors = new Hashtable();

		public Hashtable hashKey_GeoSegment = new Hashtable();

		public Hashtable hashKey_GeoSegmentRuntime = new Hashtable();

		public Hashtable hashKey_GeoSegmentVertex = new Hashtable();

		public Hashtable hashKey_GeoTrip = new Hashtable();

		public Hashtable hashKey_GeoZone = new Hashtable();

		public Hashtable hashKey_GeoZonePrimitive = new Hashtable();

		public Hashtable hashKey_Goods = new Hashtable();

		public Hashtable hashKey_GoodsLogisticOrder = new Hashtable();

		public Hashtable hashKey_GoodsType = new Hashtable();

		public Hashtable hashKey_GoodsTypeVehicle = new Hashtable();

		public Hashtable hashKey_Graphic = new Hashtable();

		public Hashtable hashKey_GraphicShift = new Hashtable();

		public Hashtable hashKey_GuardEvents = new Hashtable();

		public Hashtable hashKey_InfoSizeFiledata = new Hashtable();

		public Hashtable hashKey_JobDelbadxy = new Hashtable();

		public Hashtable hashKey_Journal = new Hashtable();

		public Hashtable hashKey_JournalDay = new Hashtable();

		public Hashtable hashKey_JournalDriver = new Hashtable();

		public Hashtable hashKey_JournalVehicle = new Hashtable();

		public Hashtable hashKey_KolomnaPoints = new Hashtable();

		public Hashtable hashKey_KolomnaPoints2 = new Hashtable();

		public Hashtable hashKey_KolomnaPoints3 = new Hashtable();

		public Hashtable hashKey_LogGuardEvents = new Hashtable();

		public Hashtable hashKey_LogOpsEvents = new Hashtable();

		public Hashtable hashKey_LogVehicleStatus = new Hashtable();

		public Hashtable hashKey_LogWaybillHeaderStatus = new Hashtable();

		public Hashtable hashKey_LogisticAddress = new Hashtable();

		public Hashtable hashKey_LogisticOrder = new Hashtable();

		public Hashtable hashKey_MapVertex = new Hashtable();

		public Hashtable hashKey_Maps = new Hashtable();

		public Hashtable hashKey_Media = new Hashtable();

		public Hashtable hashKey_MediaAcceptors = new Hashtable();

		public Hashtable hashKey_MediaType = new Hashtable();

		public Hashtable hashKey_Message = new Hashtable();

		public Hashtable hashKey_MessageBoard = new Hashtable();

		public Hashtable hashKey_MessageField = new Hashtable();

		public Hashtable hashKey_MessageOperator = new Hashtable();

		public Hashtable hashKey_MessageTemplate = new Hashtable();

		public Hashtable hashKey_MessageTemplateField = new Hashtable();

		public Hashtable hashKey_MonitoreeLog = new Hashtable();

		public Hashtable hashKey_Operator = new Hashtable();

		public Hashtable hashKey_OperatorDepartment = new Hashtable();

		public Hashtable hashKey_OperatorDriver = new Hashtable();

		public Hashtable hashKey_OperatorDrivergroup = new Hashtable();

		public Hashtable hashKey_OperatorProfile = new Hashtable();

		public Hashtable hashKey_OperatorReport = new Hashtable();

		public Hashtable hashKey_OperatorRoute = new Hashtable();

		public Hashtable hashKey_OperatorRoutegroup = new Hashtable();

		public Hashtable hashKey_OperatorVehicle = new Hashtable();

		public Hashtable hashKey_OperatorVehiclegroup = new Hashtable();

		public Hashtable hashKey_OperatorZone = new Hashtable();

		public Hashtable hashKey_OperatorZonegroup = new Hashtable();

		public Hashtable hashKey_Operatorgroup = new Hashtable();

		public Hashtable hashKey_OperatorgroupDepartment = new Hashtable();

		public Hashtable hashKey_OperatorgroupDriver = new Hashtable();

		public Hashtable hashKey_OperatorgroupDrivergroup = new Hashtable();

		public Hashtable hashKey_OperatorgroupOperator = new Hashtable();

		public Hashtable hashKey_OperatorgroupReport = new Hashtable();

		public Hashtable hashKey_OperatorgroupRoute = new Hashtable();

		public Hashtable hashKey_OperatorgroupRoutegroup = new Hashtable();

		public Hashtable hashKey_OperatorgroupVehicle = new Hashtable();

		public Hashtable hashKey_OperatorgroupVehiclegroup = new Hashtable();

		public Hashtable hashKey_OperatorgroupZone = new Hashtable();

		public Hashtable hashKey_OperatorgroupZonegroup = new Hashtable();

		public Hashtable hashKey_OpsEvent = new Hashtable();

		public Hashtable hashKey_Order = new Hashtable();

		public Hashtable hashKey_OrderTrip = new Hashtable();

		public Hashtable hashKey_OrderType = new Hashtable();

		public Hashtable hashKey_Owner = new Hashtable();

		public Hashtable hashKey_Parameter = new Hashtable();

		public Hashtable hashKey_Period = new Hashtable();

		public Hashtable hashKey_Point = new Hashtable();

		public Hashtable hashKey_PointKind = new Hashtable();

		public Hashtable hashKey_Report = new Hashtable();

		public Hashtable hashKey_ReprintReason = new Hashtable();

		public Hashtable hashKey_Reserv = new Hashtable();

		public Hashtable hashKey_Right = new Hashtable();

		public Hashtable hashKey_RightOperator = new Hashtable();

		public Hashtable hashKey_RightOperatorgroup = new Hashtable();

		public Hashtable hashKey_Route = new Hashtable();

		public Hashtable hashKey_RouteGeozone = new Hashtable();

		public Hashtable hashKey_RoutePoint = new Hashtable();

		public Hashtable hashKey_RouteTrip = new Hashtable();

		public Hashtable hashKey_Routegroup = new Hashtable();

		public Hashtable hashKey_RoutegroupRoute = new Hashtable();

		public Hashtable hashKey_Rs = new Hashtable();

		public Hashtable hashKey_RsNumber = new Hashtable();

		public Hashtable hashKey_RsOperationstype = new Hashtable();

		public Hashtable hashKey_RsOperationtype = new Hashtable();

		public Hashtable hashKey_RsPeriod = new Hashtable();

		public Hashtable hashKey_RsPoint = new Hashtable();

		public Hashtable hashKey_RsRoundTrip = new Hashtable();

		public Hashtable hashKey_RsRuntime = new Hashtable();

		public Hashtable hashKey_RsShift = new Hashtable();

		public Hashtable hashKey_RsStep = new Hashtable();

		public Hashtable hashKey_RsTrip = new Hashtable();

		public Hashtable hashKey_RsTripstype = new Hashtable();

		public Hashtable hashKey_RsTriptype = new Hashtable();

		public Hashtable hashKey_RsType = new Hashtable();

		public Hashtable hashKey_RsVariant = new Hashtable();

		public Hashtable hashKey_RsWayout = new Hashtable();

		public Hashtable hashKey_Rule = new Hashtable();

		public Hashtable hashKey_Schedule = new Hashtable();

		public Hashtable hashKey_ScheduleDetail = new Hashtable();

		public Hashtable hashKey_ScheduleDetailInfo = new Hashtable();

		public Hashtable hashKey_ScheduleGeozone = new Hashtable();

		public Hashtable hashKey_SchedulePassage = new Hashtable();

		public Hashtable hashKey_SchedulePoint = new Hashtable();

		public Hashtable hashKey_Schedulerevent = new Hashtable();

		public Hashtable hashKey_Schedulerqueue = new Hashtable();

		public Hashtable hashKey_Season = new Hashtable();

		public Hashtable hashKey_Seattype = new Hashtable();

		public Hashtable hashKey_Servers = new Hashtable();

		public Hashtable hashKey_Session = new Hashtable();

		public Hashtable hashKey_SmsLog = new Hashtable();

		public Hashtable hashKey_SmsType = new Hashtable();

		public Hashtable hashKey_Sysdiagrams = new Hashtable();

		public Hashtable hashKey_TaskProcessorLog = new Hashtable();

		public Hashtable hashKey_TempTss = new Hashtable();

		public Hashtable hashKey_Tmpdrivername = new Hashtable();

		public Hashtable hashKey_Trail = new Hashtable();

		public Hashtable hashKey_Trip = new Hashtable();

		public Hashtable hashKey_TripKind = new Hashtable();

		public Hashtable hashKey_Vehicle = new Hashtable();

		public Hashtable hashKey_VehicleKind = new Hashtable();

		public Hashtable hashKey_VehicleOwner = new Hashtable();

		public Hashtable hashKey_VehiclePicture = new Hashtable();

		public Hashtable hashKey_VehicleRule = new Hashtable();

		public Hashtable hashKey_VehicleStatus = new Hashtable();

		public Hashtable hashKey_Vehiclegroup = new Hashtable();

		public Hashtable hashKey_VehiclegroupRule = new Hashtable();

		public Hashtable hashKey_VehiclegroupVehicle = new Hashtable();

		public Hashtable hashKey_Version = new Hashtable();

		public Hashtable hashKey_Waybill = new Hashtable();

		public Hashtable hashKey_WaybillHeader = new Hashtable();

		public Hashtable hashKey_WaybillheaderWaybillmark = new Hashtable();

		public Hashtable hashKey_Waybillmark = new Hashtable();

		public Hashtable hashKey_Wayout = new Hashtable();

		public Hashtable hashKey_WbTrip = new Hashtable();

		public Hashtable hashKey_WebLine = new Hashtable();

		public Hashtable hashKey_WebLineVertex = new Hashtable();

		public Hashtable hashKey_WebPoint = new Hashtable();

		public Hashtable hashKey_WebPointType = new Hashtable();

		public Hashtable hashKey_WorkTime = new Hashtable();

		public Hashtable hashKey_WorkplaceConfig = new Hashtable();

		public Hashtable hashKey_Workstation = new Hashtable();

		public Hashtable hashKey_WorktimeReason = new Hashtable();

		public Hashtable hashKey_WorktimeStatusType = new Hashtable();

		public Hashtable hashKey_ZonePrimitive = new Hashtable();

		public Hashtable hashKey_ZonePrimitiveVertex = new Hashtable();

		public Hashtable hashKey_ZoneType = new Hashtable();

		public Hashtable hashKey_ZoneVehicle = new Hashtable();

		public Hashtable hashKey_ZoneVehiclegroup = new Hashtable();

		public Hashtable hashKey_Zonegroup = new Hashtable();

		public Hashtable hashKey_ZonegroupZone = new Hashtable();

		public Cascades(System.Data.IDbConnection connection, System.Data.IDbTransaction transaction)
		{
			this.connection = connection;
			this.transaction = transaction;

		}

		public virtual void RunCascades()
		{
			RunCascades(connection, transaction);

		}

		public virtual void CheckDatasetProperties(System.Data.DataSet dataset)
		{

			if (dataset.Tables.Contains("BLADING"))
			{
				CheckBladingProperties(dataset.Tables["BLADING"]);
			}
			if (dataset.Tables.Contains("BLADING_TYPE"))
			{
				CheckBladingTypeProperties(dataset.Tables["BLADING_TYPE"]);
			}
			if (dataset.Tables.Contains("BRIGADE"))
			{
				CheckBrigadeProperties(dataset.Tables["BRIGADE"]);
			}
			if (dataset.Tables.Contains("BRIGADE_DRIVER"))
			{
				CheckBrigadeDriverProperties(dataset.Tables["BRIGADE_DRIVER"]);
			}
			if (dataset.Tables.Contains("BUSSTOP"))
			{
				CheckBusstopProperties(dataset.Tables["BUSSTOP"]);
			}
			if (dataset.Tables.Contains("CAL"))
			{
				CheckCalProperties(dataset.Tables["CAL"]);
			}
			if (dataset.Tables.Contains("CALENDAR_DAY"))
			{
				CheckCalendarDayProperties(dataset.Tables["CALENDAR_DAY"]);
			}
			if (dataset.Tables.Contains("CONSTANTS"))
			{
				CheckConstantsProperties(dataset.Tables["CONSTANTS"]);
			}
			if (dataset.Tables.Contains("CONTRACTOR"))
			{
				CheckContractorProperties(dataset.Tables["CONTRACTOR"]);
			}
			if (dataset.Tables.Contains("CONTRACTOR_CALENDAR"))
			{
				CheckContractorCalendarProperties(dataset.Tables["CONTRACTOR_CALENDAR"]);
			}
			if (dataset.Tables.Contains("CONTROLLER"))
			{
				CheckControllerProperties(dataset.Tables["CONTROLLER"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_INFO"))
			{
				CheckControllerInfoProperties(dataset.Tables["CONTROLLER_INFO"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_SENSOR"))
			{
				CheckControllerSensorProperties(dataset.Tables["CONTROLLER_SENSOR"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"))
			{
				CheckControllerSensorLegendProperties(dataset.Tables["CONTROLLER_SENSOR_LEGEND"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"))
			{
				CheckControllerSensorMapProperties(dataset.Tables["CONTROLLER_SENSOR_MAP"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
			{
				CheckControllerSensorTypeProperties(dataset.Tables["CONTROLLER_SENSOR_TYPE"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_TIME"))
			{
				CheckControllerTimeProperties(dataset.Tables["CONTROLLER_TIME"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_TYPE"))
			{
				CheckControllerTypeProperties(dataset.Tables["CONTROLLER_TYPE"]);
			}
			if (dataset.Tables.Contains("COUNT_GPRS"))
			{
				CheckCountGprsProperties(dataset.Tables["COUNT_GPRS"]);
			}
			if (dataset.Tables.Contains("COUNT_SMS"))
			{
				CheckCountSmsProperties(dataset.Tables["COUNT_SMS"]);
			}
			if (dataset.Tables.Contains("CUSTOMER"))
			{
				CheckCustomerProperties(dataset.Tables["CUSTOMER"]);
			}
			if (dataset.Tables.Contains("DAY"))
			{
				CheckDayProperties(dataset.Tables["DAY"]);
			}
			if (dataset.Tables.Contains("DAY_KIND"))
			{
				CheckDayKindProperties(dataset.Tables["DAY_KIND"]);
			}
			if (dataset.Tables.Contains("DAY_TIME"))
			{
				CheckDayTimeProperties(dataset.Tables["DAY_TIME"]);
			}
			if (dataset.Tables.Contains("DAY_TYPE"))
			{
				CheckDayTypeProperties(dataset.Tables["DAY_TYPE"]);
			}
			if (dataset.Tables.Contains("DECADE"))
			{
				CheckDecadeProperties(dataset.Tables["DECADE"]);
			}
			if (dataset.Tables.Contains("DEPARTMENT"))
			{
				CheckDepartmentProperties(dataset.Tables["DEPARTMENT"]);
			}
			if (dataset.Tables.Contains("DK_SET"))
			{
				CheckDkSetProperties(dataset.Tables["DK_SET"]);
			}
			if (dataset.Tables.Contains("DOTNET_TYPE"))
			{
				CheckDotnetTypeProperties(dataset.Tables["DOTNET_TYPE"]);
			}
			if (dataset.Tables.Contains("DRIVER"))
			{
				CheckDriverProperties(dataset.Tables["DRIVER"]);
			}
			if (dataset.Tables.Contains("DRIVER_BONUS"))
			{
				CheckDriverBonusProperties(dataset.Tables["DRIVER_BONUS"]);
			}
			if (dataset.Tables.Contains("DRIVER_MSG_TEMPLATE"))
			{
				CheckDriverMsgTemplateProperties(dataset.Tables["DRIVER_MSG_TEMPLATE"]);
			}
			if (dataset.Tables.Contains("DRIVER_STATUS"))
			{
				CheckDriverStatusProperties(dataset.Tables["DRIVER_STATUS"]);
			}
			if (dataset.Tables.Contains("DRIVER_VEHICLE"))
			{
				CheckDriverVehicleProperties(dataset.Tables["DRIVER_VEHICLE"]);
			}
			if (dataset.Tables.Contains("DRIVERGROUP"))
			{
				CheckDrivergroupProperties(dataset.Tables["DRIVERGROUP"]);
			}
			if (dataset.Tables.Contains("DRIVERGROUP_DRIVER"))
			{
				CheckDrivergroupDriverProperties(dataset.Tables["DRIVERGROUP_DRIVER"]);
			}
			if (dataset.Tables.Contains("EMAIL"))
			{
				CheckEmailProperties(dataset.Tables["EMAIL"]);
			}
			if (dataset.Tables.Contains("EMAIL_SCHEDULEREVENT"))
			{
				CheckEmailSchedulereventProperties(dataset.Tables["EMAIL_SCHEDULEREVENT"]);
			}
			if (dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE"))
			{
				CheckEmailSchedulerqueueProperties(dataset.Tables["EMAIL_SCHEDULERQUEUE"]);
			}
			if (dataset.Tables.Contains("FACTOR_VALUES"))
			{
				CheckFactorValuesProperties(dataset.Tables["FACTOR_VALUES"]);
			}
			if (dataset.Tables.Contains("FACTORS"))
			{
				CheckFactorsProperties(dataset.Tables["FACTORS"]);
			}
			if (dataset.Tables.Contains("GEO_SEGMENT"))
			{
				CheckGeoSegmentProperties(dataset.Tables["GEO_SEGMENT"]);
			}
			if (dataset.Tables.Contains("GEO_SEGMENT_RUNTIME"))
			{
				CheckGeoSegmentRuntimeProperties(dataset.Tables["GEO_SEGMENT_RUNTIME"]);
			}
			if (dataset.Tables.Contains("GEO_SEGMENT_VERTEX"))
			{
				CheckGeoSegmentVertexProperties(dataset.Tables["GEO_SEGMENT_VERTEX"]);
			}
			if (dataset.Tables.Contains("GEO_TRIP"))
			{
				CheckGeoTripProperties(dataset.Tables["GEO_TRIP"]);
			}
			if (dataset.Tables.Contains("GEO_ZONE"))
			{
				CheckGeoZoneProperties(dataset.Tables["GEO_ZONE"]);
			}
			if (dataset.Tables.Contains("GEO_ZONE_PRIMITIVE"))
			{
				CheckGeoZonePrimitiveProperties(dataset.Tables["GEO_ZONE_PRIMITIVE"]);
			}
			if (dataset.Tables.Contains("GOODS"))
			{
				CheckGoodsProperties(dataset.Tables["GOODS"]);
			}
			if (dataset.Tables.Contains("GOODS_LOGISTIC_ORDER"))
			{
				CheckGoodsLogisticOrderProperties(dataset.Tables["GOODS_LOGISTIC_ORDER"]);
			}
			if (dataset.Tables.Contains("GOODS_TYPE"))
			{
				CheckGoodsTypeProperties(dataset.Tables["GOODS_TYPE"]);
			}
			if (dataset.Tables.Contains("GOODS_TYPE_VEHICLE"))
			{
				CheckGoodsTypeVehicleProperties(dataset.Tables["GOODS_TYPE_VEHICLE"]);
			}
			if (dataset.Tables.Contains("GRAPHIC"))
			{
				CheckGraphicProperties(dataset.Tables["GRAPHIC"]);
			}
			if (dataset.Tables.Contains("GRAPHIC_SHIFT"))
			{
				CheckGraphicShiftProperties(dataset.Tables["GRAPHIC_SHIFT"]);
			}
			if (dataset.Tables.Contains("GUARD_EVENTS"))
			{
				CheckGuardEventsProperties(dataset.Tables["GUARD_EVENTS"]);
			}
			if (dataset.Tables.Contains("Job_DelBadXY"))
			{
				CheckJobDelbadxyProperties(dataset.Tables["Job_DelBadXY"]);
			}
			if (dataset.Tables.Contains("JOURNAL"))
			{
				CheckJournalProperties(dataset.Tables["JOURNAL"]);
			}
			if (dataset.Tables.Contains("JOURNAL_DAY"))
			{
				CheckJournalDayProperties(dataset.Tables["JOURNAL_DAY"]);
			}
			if (dataset.Tables.Contains("JOURNAL_DRIVER"))
			{
				CheckJournalDriverProperties(dataset.Tables["JOURNAL_DRIVER"]);
			}
			if (dataset.Tables.Contains("JOURNAL_VEHICLE"))
			{
				CheckJournalVehicleProperties(dataset.Tables["JOURNAL_VEHICLE"]);
			}
			if (dataset.Tables.Contains("LOG_OPS_EVENTS"))
			{
				CheckLogOpsEventsProperties(dataset.Tables["LOG_OPS_EVENTS"]);
			}
			if (dataset.Tables.Contains("LOG_VEHICLE_STATUS"))
			{
				CheckLogVehicleStatusProperties(dataset.Tables["LOG_VEHICLE_STATUS"]);
			}
			if (dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS"))
			{
				CheckLogWaybillHeaderStatusProperties(dataset.Tables["LOG_WAYBILL_HEADER_STATUS"]);
			}
			if (dataset.Tables.Contains("LOGISTIC_ADDRESS"))
			{
				CheckLogisticAddressProperties(dataset.Tables["LOGISTIC_ADDRESS"]);
			}
			if (dataset.Tables.Contains("LOGISTIC_ORDER"))
			{
				CheckLogisticOrderProperties(dataset.Tables["LOGISTIC_ORDER"]);
			}
			if (dataset.Tables.Contains("MAP_VERTEX"))
			{
				CheckMapVertexProperties(dataset.Tables["MAP_VERTEX"]);
			}
			if (dataset.Tables.Contains("MAPS"))
			{
				CheckMapsProperties(dataset.Tables["MAPS"]);
			}
			if (dataset.Tables.Contains("MEDIA"))
			{
				CheckMediaProperties(dataset.Tables["MEDIA"]);
			}
			if (dataset.Tables.Contains("MEDIA_ACCEPTORS"))
			{
				CheckMediaAcceptorsProperties(dataset.Tables["MEDIA_ACCEPTORS"]);
			}
			if (dataset.Tables.Contains("MEDIA_TYPE"))
			{
				CheckMediaTypeProperties(dataset.Tables["MEDIA_TYPE"]);
			}
			if (dataset.Tables.Contains("MESSAGE"))
			{
				CheckMessageProperties(dataset.Tables["MESSAGE"]);
			}
			if (dataset.Tables.Contains("MESSAGE_BOARD"))
			{
				CheckMessageBoardProperties(dataset.Tables["MESSAGE_BOARD"]);
			}
			if (dataset.Tables.Contains("MESSAGE_FIELD"))
			{
				CheckMessageFieldProperties(dataset.Tables["MESSAGE_FIELD"]);
			}
			if (dataset.Tables.Contains("MESSAGE_OPERATOR"))
			{
				CheckMessageOperatorProperties(dataset.Tables["MESSAGE_OPERATOR"]);
			}
			if (dataset.Tables.Contains("MESSAGE_TEMPLATE"))
			{
				CheckMessageTemplateProperties(dataset.Tables["MESSAGE_TEMPLATE"]);
			}
			if (dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"))
			{
				CheckMessageTemplateFieldProperties(dataset.Tables["MESSAGE_TEMPLATE_FIELD"]);
			}
			if (dataset.Tables.Contains("OPERATOR"))
			{
				CheckOperatorProperties(dataset.Tables["OPERATOR"]);
			}
			if (dataset.Tables.Contains("OPERATOR_DEPARTMENT"))
			{
				CheckOperatorDepartmentProperties(dataset.Tables["OPERATOR_DEPARTMENT"]);
			}
			if (dataset.Tables.Contains("OPERATOR_DRIVER"))
			{
				CheckOperatorDriverProperties(dataset.Tables["OPERATOR_DRIVER"]);
			}
			if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP"))
			{
				CheckOperatorDrivergroupProperties(dataset.Tables["OPERATOR_DRIVERGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATOR_PROFILE"))
			{
				CheckOperatorProfileProperties(dataset.Tables["OPERATOR_PROFILE"]);
			}
			if (dataset.Tables.Contains("OPERATOR_REPORT"))
			{
				CheckOperatorReportProperties(dataset.Tables["OPERATOR_REPORT"]);
			}
			if (dataset.Tables.Contains("OPERATOR_ROUTE"))
			{
				CheckOperatorRouteProperties(dataset.Tables["OPERATOR_ROUTE"]);
			}
			if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP"))
			{
				CheckOperatorRoutegroupProperties(dataset.Tables["OPERATOR_ROUTEGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATOR_VEHICLE"))
			{
				CheckOperatorVehicleProperties(dataset.Tables["OPERATOR_VEHICLE"]);
			}
			if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP"))
			{
				CheckOperatorVehiclegroupProperties(dataset.Tables["OPERATOR_VEHICLEGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATOR_ZONE"))
			{
				CheckOperatorZoneProperties(dataset.Tables["OPERATOR_ZONE"]);
			}
			if (dataset.Tables.Contains("OPERATOR_ZONEGROUP"))
			{
				CheckOperatorZonegroupProperties(dataset.Tables["OPERATOR_ZONEGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP"))
			{
				CheckOperatorgroupProperties(dataset.Tables["OPERATORGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT"))
			{
				CheckOperatorgroupDepartmentProperties(dataset.Tables["OPERATORGROUP_DEPARTMENT"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_DRIVER"))
			{
				CheckOperatorgroupDriverProperties(dataset.Tables["OPERATORGROUP_DRIVER"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP"))
			{
				CheckOperatorgroupDrivergroupProperties(dataset.Tables["OPERATORGROUP_DRIVERGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR"))
			{
				CheckOperatorgroupOperatorProperties(dataset.Tables["OPERATORGROUP_OPERATOR"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_REPORT"))
			{
				CheckOperatorgroupReportProperties(dataset.Tables["OPERATORGROUP_REPORT"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_ROUTE"))
			{
				CheckOperatorgroupRouteProperties(dataset.Tables["OPERATORGROUP_ROUTE"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP"))
			{
				CheckOperatorgroupRoutegroupProperties(dataset.Tables["OPERATORGROUP_ROUTEGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE"))
			{
				CheckOperatorgroupVehicleProperties(dataset.Tables["OPERATORGROUP_VEHICLE"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP"))
			{
				CheckOperatorgroupVehiclegroupProperties(dataset.Tables["OPERATORGROUP_VEHICLEGROUP"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_ZONE"))
			{
				CheckOperatorgroupZoneProperties(dataset.Tables["OPERATORGROUP_ZONE"]);
			}
			if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP"))
			{
				CheckOperatorgroupZonegroupProperties(dataset.Tables["OPERATORGROUP_ZONEGROUP"]);
			}
			if (dataset.Tables.Contains("OPS_EVENT"))
			{
				CheckOpsEventProperties(dataset.Tables["OPS_EVENT"]);
			}
			if (dataset.Tables.Contains("ORDER"))
			{
				CheckOrderProperties(dataset.Tables["ORDER"]);
			}
			if (dataset.Tables.Contains("ORDER_TRIP"))
			{
				CheckOrderTripProperties(dataset.Tables["ORDER_TRIP"]);
			}
			if (dataset.Tables.Contains("ORDER_TYPE"))
			{
				CheckOrderTypeProperties(dataset.Tables["ORDER_TYPE"]);
			}
			if (dataset.Tables.Contains("OWNER"))
			{
				CheckOwnerProperties(dataset.Tables["OWNER"]);
			}
			if (dataset.Tables.Contains("PERIOD"))
			{
				CheckPeriodProperties(dataset.Tables["PERIOD"]);
			}
			if (dataset.Tables.Contains("POINT"))
			{
				CheckPointProperties(dataset.Tables["POINT"]);
			}
			if (dataset.Tables.Contains("POINT_KIND"))
			{
				CheckPointKindProperties(dataset.Tables["POINT_KIND"]);
			}
			if (dataset.Tables.Contains("REPORT"))
			{
				CheckReportProperties(dataset.Tables["REPORT"]);
			}
			if (dataset.Tables.Contains("REPRINT_REASON"))
			{
				CheckReprintReasonProperties(dataset.Tables["REPRINT_REASON"]);
			}
			if (dataset.Tables.Contains("RIGHT"))
			{
				CheckRightProperties(dataset.Tables["RIGHT"]);
			}
			if (dataset.Tables.Contains("RIGHT_OPERATOR"))
			{
				CheckRightOperatorProperties(dataset.Tables["RIGHT_OPERATOR"]);
			}
			if (dataset.Tables.Contains("RIGHT_OPERATORGROUP"))
			{
				CheckRightOperatorgroupProperties(dataset.Tables["RIGHT_OPERATORGROUP"]);
			}
			if (dataset.Tables.Contains("ROUTE"))
			{
				CheckRouteProperties(dataset.Tables["ROUTE"]);
			}
			if (dataset.Tables.Contains("ROUTE_GEOZONE"))
			{
				CheckRouteGeozoneProperties(dataset.Tables["ROUTE_GEOZONE"]);
			}
			if (dataset.Tables.Contains("ROUTE_POINT"))
			{
				CheckRoutePointProperties(dataset.Tables["ROUTE_POINT"]);
			}
			if (dataset.Tables.Contains("ROUTE_TRIP"))
			{
				CheckRouteTripProperties(dataset.Tables["ROUTE_TRIP"]);
			}
			if (dataset.Tables.Contains("ROUTEGROUP"))
			{
				CheckRoutegroupProperties(dataset.Tables["ROUTEGROUP"]);
			}
			if (dataset.Tables.Contains("ROUTEGROUP_ROUTE"))
			{
				CheckRoutegroupRouteProperties(dataset.Tables["ROUTEGROUP_ROUTE"]);
			}
			if (dataset.Tables.Contains("RS"))
			{
				CheckRsProperties(dataset.Tables["RS"]);
			}
			if (dataset.Tables.Contains("RS_NUMBER"))
			{
				CheckRsNumberProperties(dataset.Tables["RS_NUMBER"]);
			}
			if (dataset.Tables.Contains("RS_OPERATIONSTYPE"))
			{
				CheckRsOperationstypeProperties(dataset.Tables["RS_OPERATIONSTYPE"]);
			}
			if (dataset.Tables.Contains("RS_OPERATIONTYPE"))
			{
				CheckRsOperationtypeProperties(dataset.Tables["RS_OPERATIONTYPE"]);
			}
			if (dataset.Tables.Contains("RS_PERIOD"))
			{
				CheckRsPeriodProperties(dataset.Tables["RS_PERIOD"]);
			}
			if (dataset.Tables.Contains("RS_POINT"))
			{
				CheckRsPointProperties(dataset.Tables["RS_POINT"]);
			}
			if (dataset.Tables.Contains("RS_ROUND_TRIP"))
			{
				CheckRsRoundTripProperties(dataset.Tables["RS_ROUND_TRIP"]);
			}
			if (dataset.Tables.Contains("RS_RUNTIME"))
			{
				CheckRsRuntimeProperties(dataset.Tables["RS_RUNTIME"]);
			}
			if (dataset.Tables.Contains("RS_SHIFT"))
			{
				CheckRsShiftProperties(dataset.Tables["RS_SHIFT"]);
			}
			if (dataset.Tables.Contains("RS_STEP"))
			{
				CheckRsStepProperties(dataset.Tables["RS_STEP"]);
			}
			if (dataset.Tables.Contains("RS_TRIP"))
			{
				CheckRsTripProperties(dataset.Tables["RS_TRIP"]);
			}
			if (dataset.Tables.Contains("RS_TRIPSTYPE"))
			{
				CheckRsTripstypeProperties(dataset.Tables["RS_TRIPSTYPE"]);
			}
			if (dataset.Tables.Contains("RS_TRIPTYPE"))
			{
				CheckRsTriptypeProperties(dataset.Tables["RS_TRIPTYPE"]);
			}
			if (dataset.Tables.Contains("RS_TYPE"))
			{
				CheckRsTypeProperties(dataset.Tables["RS_TYPE"]);
			}
			if (dataset.Tables.Contains("RS_VARIANT"))
			{
				CheckRsVariantProperties(dataset.Tables["RS_VARIANT"]);
			}
			if (dataset.Tables.Contains("RS_WAYOUT"))
			{
				CheckRsWayoutProperties(dataset.Tables["RS_WAYOUT"]);
			}
			if (dataset.Tables.Contains("RULE"))
			{
				CheckRuleProperties(dataset.Tables["RULE"]);
			}
			if (dataset.Tables.Contains("SCHEDULE"))
			{
				CheckScheduleProperties(dataset.Tables["SCHEDULE"]);
			}
			if (dataset.Tables.Contains("SCHEDULE_DETAIL"))
			{
				CheckScheduleDetailProperties(dataset.Tables["SCHEDULE_DETAIL"]);
			}
			if (dataset.Tables.Contains("SCHEDULE_DETAIL_INFO"))
			{
				CheckScheduleDetailInfoProperties(dataset.Tables["SCHEDULE_DETAIL_INFO"]);
			}
			if (dataset.Tables.Contains("SCHEDULE_GEOZONE"))
			{
				CheckScheduleGeozoneProperties(dataset.Tables["SCHEDULE_GEOZONE"]);
			}
			if (dataset.Tables.Contains("SCHEDULE_PASSAGE"))
			{
				CheckSchedulePassageProperties(dataset.Tables["SCHEDULE_PASSAGE"]);
			}
			if (dataset.Tables.Contains("SCHEDULE_POINT"))
			{
				CheckSchedulePointProperties(dataset.Tables["SCHEDULE_POINT"]);
			}
			if (dataset.Tables.Contains("SCHEDULEREVENT"))
			{
				CheckSchedulereventProperties(dataset.Tables["SCHEDULEREVENT"]);
			}
			if (dataset.Tables.Contains("SCHEDULERQUEUE"))
			{
				CheckSchedulerqueueProperties(dataset.Tables["SCHEDULERQUEUE"]);
			}
			if (dataset.Tables.Contains("SEASON"))
			{
				CheckSeasonProperties(dataset.Tables["SEASON"]);
			}
			if (dataset.Tables.Contains("SEATTYPE"))
			{
				CheckSeattypeProperties(dataset.Tables["SEATTYPE"]);
			}
			if (dataset.Tables.Contains("SESSION"))
			{
				CheckSessionProperties(dataset.Tables["SESSION"]);
			}
			if (dataset.Tables.Contains("SMS_TYPE"))
			{
				CheckSmsTypeProperties(dataset.Tables["SMS_TYPE"]);
			}
			if (dataset.Tables.Contains("sysdiagrams"))
			{
				CheckSysdiagramsProperties(dataset.Tables["sysdiagrams"]);
			}
			if (dataset.Tables.Contains("TASK_PROCESSOR_LOG"))
			{
				CheckTaskProcessorLogProperties(dataset.Tables["TASK_PROCESSOR_LOG"]);
			}
			if (dataset.Tables.Contains("TRAIL"))
			{
				CheckTrailProperties(dataset.Tables["TRAIL"]);
			}
			if (dataset.Tables.Contains("TRIP"))
			{
				CheckTripProperties(dataset.Tables["TRIP"]);
			}
			if (dataset.Tables.Contains("TRIP_KIND"))
			{
				CheckTripKindProperties(dataset.Tables["TRIP_KIND"]);
			}
			if (dataset.Tables.Contains("VEHICLE"))
			{
				CheckVehicleProperties(dataset.Tables["VEHICLE"]);
			}
			if (dataset.Tables.Contains("VEHICLE_KIND"))
			{
				CheckVehicleKindProperties(dataset.Tables["VEHICLE_KIND"]);
			}
			if (dataset.Tables.Contains("VEHICLE_OWNER"))
			{
				CheckVehicleOwnerProperties(dataset.Tables["VEHICLE_OWNER"]);
			}
			if (dataset.Tables.Contains("VEHICLE_PICTURE"))
			{
				CheckVehiclePictureProperties(dataset.Tables["VEHICLE_PICTURE"]);
			}
			if (dataset.Tables.Contains("VEHICLE_RULE"))
			{
				CheckVehicleRuleProperties(dataset.Tables["VEHICLE_RULE"]);
			}
			if (dataset.Tables.Contains("VEHICLE_STATUS"))
			{
				CheckVehicleStatusProperties(dataset.Tables["VEHICLE_STATUS"]);
			}
			if (dataset.Tables.Contains("VEHICLEGROUP"))
			{
				CheckVehiclegroupProperties(dataset.Tables["VEHICLEGROUP"]);
			}
			if (dataset.Tables.Contains("VEHICLEGROUP_RULE"))
			{
				CheckVehiclegroupRuleProperties(dataset.Tables["VEHICLEGROUP_RULE"]);
			}
			if (dataset.Tables.Contains("VEHICLEGROUP_VEHICLE"))
			{
				CheckVehiclegroupVehicleProperties(dataset.Tables["VEHICLEGROUP_VEHICLE"]);
			}
			if (dataset.Tables.Contains("WAYBILL"))
			{
				CheckWaybillProperties(dataset.Tables["WAYBILL"]);
			}
			if (dataset.Tables.Contains("WAYBILL_HEADER"))
			{
				CheckWaybillHeaderProperties(dataset.Tables["WAYBILL_HEADER"]);
			}
			if (dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK"))
			{
				CheckWaybillheaderWaybillmarkProperties(dataset.Tables["WAYBILLHEADER_WAYBILLMARK"]);
			}
			if (dataset.Tables.Contains("WAYBILLMARK"))
			{
				CheckWaybillmarkProperties(dataset.Tables["WAYBILLMARK"]);
			}
			if (dataset.Tables.Contains("WAYOUT"))
			{
				CheckWayoutProperties(dataset.Tables["WAYOUT"]);
			}
			if (dataset.Tables.Contains("WB_TRIP"))
			{
				CheckWbTripProperties(dataset.Tables["WB_TRIP"]);
			}
			if (dataset.Tables.Contains("WEB_LINE"))
			{
				CheckWebLineProperties(dataset.Tables["WEB_LINE"]);
			}
			if (dataset.Tables.Contains("WEB_LINE_VERTEX"))
			{
				CheckWebLineVertexProperties(dataset.Tables["WEB_LINE_VERTEX"]);
			}
			if (dataset.Tables.Contains("WEB_POINT"))
			{
				CheckWebPointProperties(dataset.Tables["WEB_POINT"]);
			}
			if (dataset.Tables.Contains("WEB_POINT_TYPE"))
			{
				CheckWebPointTypeProperties(dataset.Tables["WEB_POINT_TYPE"]);
			}
			if (dataset.Tables.Contains("WORK_TIME"))
			{
				CheckWorkTimeProperties(dataset.Tables["WORK_TIME"]);
			}
			if (dataset.Tables.Contains("WORKPLACE_CONFIG"))
			{
				CheckWorkplaceConfigProperties(dataset.Tables["WORKPLACE_CONFIG"]);
			}
			if (dataset.Tables.Contains("WORKSTATION"))
			{
				CheckWorkstationProperties(dataset.Tables["WORKSTATION"]);
			}
			if (dataset.Tables.Contains("WORKTIME_REASON"))
			{
				CheckWorktimeReasonProperties(dataset.Tables["WORKTIME_REASON"]);
			}
			if (dataset.Tables.Contains("WORKTIME_STATUS_TYPE"))
			{
				CheckWorktimeStatusTypeProperties(dataset.Tables["WORKTIME_STATUS_TYPE"]);
			}
			if (dataset.Tables.Contains("ZONE_PRIMITIVE"))
			{
				CheckZonePrimitiveProperties(dataset.Tables["ZONE_PRIMITIVE"]);
			}
			if (dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX"))
			{
				CheckZonePrimitiveVertexProperties(dataset.Tables["ZONE_PRIMITIVE_VERTEX"]);
			}
			if (dataset.Tables.Contains("ZONE_TYPE"))
			{
				CheckZoneTypeProperties(dataset.Tables["ZONE_TYPE"]);
			}
			if (dataset.Tables.Contains("ZONE_VEHICLE"))
			{
				CheckZoneVehicleProperties(dataset.Tables["ZONE_VEHICLE"]);
			}
			if (dataset.Tables.Contains("ZONE_VEHICLEGROUP"))
			{
				CheckZoneVehiclegroupProperties(dataset.Tables["ZONE_VEHICLEGROUP"]);
			}
			if (dataset.Tables.Contains("ZONEGROUP"))
			{
				CheckZonegroupProperties(dataset.Tables["ZONEGROUP"]);
			}
			if (dataset.Tables.Contains("ZONEGROUP_ZONE"))
			{
				CheckZonegroupZoneProperties(dataset.Tables["ZONEGROUP_ZONE"]);
			}

		}

		public virtual void RunCascades(IDbConnection connection, IDbTransaction transaction)
		{

			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_BladingType.Keys.Count > 0)
			{
				LoadRecords_FK_BLADING_BLADING_TYPE(connection, transaction);
				LoadRecords_FK_LOGISTIC_ORDER_BLADING_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Busstop.Keys.Count > 0)
			{
				LoadRecords_Point_Busstop(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Contractor.Keys.Count > 0)
			{
				LoadRecords_FK_BLADING_CONTRACTOR(connection, transaction);
				LoadRecords_FK_LOGISTIC_ADDRESS_CONTRACTOR(connection, transaction);
				LoadRecords_FK_LOGISTIC_ORDER_CONTRACTOR(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ContractorCalendar.Keys.Count > 0)
			{
				LoadRecords_FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerSensorType.Keys.Count > 0)
			{
				LoadRecords_FK_ControllerSensor_ControllerSensorType(connection, transaction);
				LoadRecords_FK_ControllerSensorLegend_ControllerSensorType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerSensorLegend.Keys.Count > 0)
			{
				LoadRecords_FK_ControllerSensorMap_ControllerSensorLegend(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerType.Keys.Count > 0)
			{
				LoadRecords_Controller_Controller_Type(connection, transaction);
				LoadRecords_FK_ControllerSensor_ControllerType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerSensor.Keys.Count > 0)
			{
				LoadRecords_FK_ControllerSensorMap_ControllerSensor(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Customer.Keys.Count > 0)
			{
				LoadRecords_Order_Customer(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DayType.Keys.Count > 0)
			{
				LoadRecords_FK_DAY_TIME_DAY_TYPE(connection, transaction);
				LoadRecords_FK_RS_DAY_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DayTime.Keys.Count > 0)
			{
				LoadRecords_FK_GEO_SEGMENT_RUNTIME_DAY_TIME(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Department.Keys.Count > 0)
			{
				LoadRecords_FK_DECADE_DEPARTMENT(connection, transaction);
				LoadRecords_FK_DRIVER_DEPARTMENT(connection, transaction);
				LoadRecords_FK_VEHICLE_DEPARTMENT(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Decade.Keys.Count > 0)
			{
				LoadRecords_FK_BRIGADE_DECADE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DkSet.Keys.Count > 0)
			{
				LoadRecords_DayKind_DkSet(connection, transaction);
				LoadRecords_Period_DkSet(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DayKind.Keys.Count > 0)
			{
				LoadRecords_Cal_DayKind(connection, transaction);
				LoadRecords_Period_DayKind(connection, transaction);
				LoadRecords_Schedule_DayKind(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DotnetType.Keys.Count > 0)
			{
				LoadRecords_MessageTemplateField_DotnetType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_DriverStatus.Keys.Count > 0)
			{
				LoadRecords_Driver_DriverStatus(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Driver.Keys.Count > 0)
			{
				LoadRecords_DriverGroupDriver_Driver(connection, transaction);
				LoadRecords_DriverVehicle_Driver(connection, transaction);
				LoadRecords_FK_BRIGADE_DRIVER_DRIVER(connection, transaction);
				LoadRecords_FK_JOURNAL_DRIVER(connection, transaction);
				LoadRecords_FK_JOURNAL_DRIVER_DRIVER(connection, transaction);
				LoadRecords_FK_WORK_TIME_DRIVER(connection, transaction);
				LoadRecords_Journal_Driver1(connection, transaction);
				LoadRecords_OperatorDriver_Driver(connection, transaction);
				LoadRecords_OperatorGroupDriver_Driver(connection, transaction);
				LoadRecords_Waybill_Driver(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Drivergroup.Keys.Count > 0)
			{
				LoadRecords_DriverGroupDriver_DriverGroup(connection, transaction);
				LoadRecords_OoperatorDriverGroup_DriverGroup(connection, transaction);
				LoadRecords_OperatorGroupDriverGroup_DriverGroup(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Email.Keys.Count > 0)
			{
				LoadRecords_FK_EMAIL_SCHEDULEREVENT_EMAIL(connection, transaction);
				LoadRecords_FK_EMAIL_SCHEDULERQUEUE_EMAIL(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Factors.Keys.Count > 0)
			{
				LoadRecords_FactorValues_Factors(connection, transaction);
				LoadRecords_GeoSegment_Factors(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_GeoZone.Keys.Count > 0)
			{
				LoadRecords_FK_LOGISTIC_ADDRESS_GEO_ZONE(connection, transaction);
				LoadRecords_FK_ROUTE_GEOZONE_GEO_ZONE(connection, transaction);
				LoadRecords_GeoZonePrimitive_GeoZone(connection, transaction);
				LoadRecords_OperatorGroupZone_Zone(connection, transaction);
				LoadRecords_OperatorZone_Zone(connection, transaction);
				LoadRecords_ZoneGroupZone_Zone(connection, transaction);
				LoadRecords_ZoneVehicle_Zone(connection, transaction);
				LoadRecords_ZoneVehicleGroup_Zone(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_GoodsType.Keys.Count > 0)
			{
				LoadRecords_FK_GOODS_GOODS_TYPE(connection, transaction);
				LoadRecords_FK_GOODS_TYPE_VEHICLE_GOODS_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Goods.Keys.Count > 0)
			{
				LoadRecords_FK_GOODS_LOGISTIC_ORDER_GOODS(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Graphic.Keys.Count > 0)
			{
				LoadRecords_FK_BRIGADE_GRAPHIC(connection, transaction);
				LoadRecords_FK_RS_WAYOUT_GRAPHIC(connection, transaction);
				LoadRecords_GraphicShift_Graphic(connection, transaction);
				LoadRecords_Vehicle_Graphic(connection, transaction);
				LoadRecords_Wayout_Graphic(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_GuardEvents.Keys.Count > 0)
			{
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_LogisticAddress.Keys.Count > 0)
			{
				LoadRecords_FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_LogisticOrder.Keys.Count > 0)
			{
				LoadRecords_FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Maps.Keys.Count > 0)
			{
				LoadRecords_MapVeretx_Maps(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_MapVertex.Keys.Count > 0)
			{
				LoadRecords_GeoSegmentVeretx_MapVeretx(connection, transaction);
				LoadRecords_Point_MapVeretx(connection, transaction);
				LoadRecords_WebLineVertex_Vertex(connection, transaction);
				LoadRecords_WebPoint_Vertex(connection, transaction);
				LoadRecords_ZonePrimitiveVertex_MapVeretx(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_MediaType.Keys.Count > 0)
			{
				LoadRecords_Media_MediaType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Media.Keys.Count > 0)
			{
				LoadRecords_MediaAcceptors_Media(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_MessageTemplate.Keys.Count > 0)
			{
				LoadRecords_Message_MessageTemplate(connection, transaction);
				LoadRecords_MessageTemplateField_MessageTemplate(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Message.Keys.Count > 0)
			{
				LoadRecords_MessageField_Message(connection, transaction);
				LoadRecords_MessageOperator_Message(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_MessageTemplateField.Keys.Count > 0)
			{
				LoadRecords_MessageField_MessageTemplateField(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Operator.Keys.Count > 0)
			{
				LoadRecords_FK_OPERATOR_PROFILE_OPERATOR(connection, transaction);
				LoadRecords_LogOpsEvents_Operator(connection, transaction);
				LoadRecords_LogVehicleStatus_Operator(connection, transaction);
				LoadRecords_LogWaybillHeaderStatus_Operator(connection, transaction);
				LoadRecords_MessageOperator_Operator(connection, transaction);
				LoadRecords_OperatorDepartment_Operator(connection, transaction);
				LoadRecords_OperatorDriver_Operator(connection, transaction);
				LoadRecords_OperatorDriverGroup_Operator(connection, transaction);
				LoadRecords_OperatorGroupOperator_Operator(connection, transaction);
				LoadRecords_OperatorReport_Operator(connection, transaction);
				LoadRecords_OperatorRoute_Operator(connection, transaction);
				LoadRecords_OperatorRouteGroup_Operator(connection, transaction);
				LoadRecords_OperatorVehicle_Operator(connection, transaction);
				LoadRecords_OperatorVehicleGroup_Operator(connection, transaction);
				LoadRecords_OperatorZone_Operator(connection, transaction);
				LoadRecords_OperatorZoneGroup_Operator(connection, transaction);
				LoadRecords_RightOperator_Operator(connection, transaction);
				LoadRecords_WebLine_Operator(connection, transaction);
				LoadRecords_WebPoint_Operator(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Operatorgroup.Keys.Count > 0)
			{
				LoadRecords_OperatorGroupDepartment_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupDriver_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupDriverGroup_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupOperator_OperatorGroup(connection, transaction);
				LoadRecords_OperatorgroupReport_Operator(connection, transaction);
				LoadRecords_OperatorGroupRoute_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupRouteGroup_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupVehicle_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupVehicleGroup_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupZone_OperatorGroup(connection, transaction);
				LoadRecords_OperatorGroupZoneGroup_OperatorGroup(connection, transaction);
				LoadRecords_RightOperatorGroup_OperatorGroup(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_OpsEvent.Keys.Count > 0)
			{
				LoadRecords_LogOpsEvents_OpsEvent(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_OrderType.Keys.Count > 0)
			{
				LoadRecords_Order_OrderType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Order.Keys.Count > 0)
			{
				LoadRecords_FK_ORDER_TRIP_ORDER(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Owner.Keys.Count > 0)
			{
				LoadRecords_FK_VEHICLE_OWNER_OWNER(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_PointKind.Keys.Count > 0)
			{
				LoadRecords_Point_PointKind(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Point.Keys.Count > 0)
			{
				LoadRecords_GeoSegment_Point(connection, transaction);
				LoadRecords_GeoSegment_PointTo(connection, transaction);
				LoadRecords_GeoTrip_PointA(connection, transaction);
				LoadRecords_GeoTrip_PointB(connection, transaction);
				LoadRecords_RoutePoint_Point(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Report.Keys.Count > 0)
			{
				LoadRecords_OperatorgroupReport_Report(connection, transaction);
				LoadRecords_OperatorReport_Report(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Right.Keys.Count > 0)
			{
				LoadRecords_OperatorDepartment_Right(connection, transaction);
				LoadRecords_OperatorDriver_Right(connection, transaction);
				LoadRecords_OperatorDriverGroup_Right(connection, transaction);
				LoadRecords_OperatorGroupDepartment_Right(connection, transaction);
				LoadRecords_OperatorGroupDriver_Right(connection, transaction);
				LoadRecords_OperatorGroupDriverGroup_Right(connection, transaction);
				LoadRecords_OperatorgroupReport_Right(connection, transaction);
				LoadRecords_OperatorGroupRoute_Right(connection, transaction);
				LoadRecords_OperatorGroupRouteGroup_Right(connection, transaction);
				LoadRecords_OperatorGroupVehicle_Right(connection, transaction);
				LoadRecords_OperatorGroupVehicleGroup_Right(connection, transaction);
				LoadRecords_OperatorGroupZone_Right(connection, transaction);
				LoadRecords_OperatorGroupZoneGroup_Right(connection, transaction);
				LoadRecords_OperatorReport_Right(connection, transaction);
				LoadRecords_OperatorRoute_Right(connection, transaction);
				LoadRecords_OperatorRouteGroup_Right(connection, transaction);
				LoadRecords_OperatorVehicle_Right(connection, transaction);
				LoadRecords_OperatorVehicleGroup_Right(connection, transaction);
				LoadRecords_OperatorZone_Right(connection, transaction);
				LoadRecords_OperatorZoneGroup_Right(connection, transaction);
				LoadRecords_RightOperator_Right(connection, transaction);
				LoadRecords_RightOperatorGroup_Right(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Route.Keys.Count > 0)
			{
				LoadRecords_FK_ROUTE_GEOZONE_ROUTE(connection, transaction);
				LoadRecords_FK_RS_ROUTE(connection, transaction);
				LoadRecords_GeoTrip_Route(connection, transaction);
				LoadRecords_OperatorGroupRoute_Route(connection, transaction);
				LoadRecords_OperatorRoute_Route(connection, transaction);
				LoadRecords_Period_Route(connection, transaction);
				LoadRecords_RouteGroupRoute_Route(connection, transaction);
				LoadRecords_RoutePoint_Route(connection, transaction);
				LoadRecords_RouteTrip_Route(connection, transaction);
				LoadRecords_Schedule_Route(connection, transaction);
				LoadRecords_Wayout_Route(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_GeoTrip.Keys.Count > 0)
			{
				LoadRecords_FK_Wb_Trip_Geo_Trip(connection, transaction);
				LoadRecords_GeoSegment_GeoTrip(connection, transaction);
				LoadRecords_RouteTrip_GeoTrip(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_GeoSegment.Keys.Count > 0)
			{
				LoadRecords_FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT(connection, transaction);
				LoadRecords_GeoSegmentVeretx_GeoSegment(connection, transaction);
				LoadRecords_SchedulePoint_GeoSegment(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RouteGeozone.Keys.Count > 0)
			{
				LoadRecords_FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RoutePoint.Keys.Count > 0)
			{
				LoadRecords_SchedulePoint_RoutePoint(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RouteTrip.Keys.Count > 0)
			{
				LoadRecords_FK_RS_TRIP0_ROUTE_TRIP(connection, transaction);
				LoadRecords_Trip_RouteTrip(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Routegroup.Keys.Count > 0)
			{
				LoadRecords_OperatorGroupRouteGroup_RouteGroup(connection, transaction);
				LoadRecords_OperatorRouteGroup_RouteGroup(connection, transaction);
				LoadRecords_RouteGroupRoute_RouteGroup(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Rs.Keys.Count > 0)
			{
				LoadRecords_FK_RS_VARIANT_RS(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsOperationstype.Keys.Count > 0)
			{
				LoadRecords_FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsTripstype.Keys.Count > 0)
			{
				LoadRecords_FK_RS_TRIPTYPE_RS_TRIPSTYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsType.Keys.Count > 0)
			{
				LoadRecords_FK_RS_VARIANT_RS_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsVariant.Keys.Count > 0)
			{
				LoadRecords_FK_RS_PERIOD_RS_VARIANT(connection, transaction);
				LoadRecords_FK_RS_ROUND_TRIP_RS_VARIANT(connection, transaction);
				LoadRecords_FK_RS_STEP_RS_VARIANT(connection, transaction);
				LoadRecords_FK_RS_TRIP_RS_VARIANT(connection, transaction);
				LoadRecords_FK_RS_TRIPTYPE_RS_VARIANT(connection, transaction);
				LoadRecords_FK_RS_WAYOUT_RS_VARIANT(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsPeriod.Keys.Count > 0)
			{
				LoadRecords_FK_RS_RUNTIME_RS_PERIOD(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsRoundTrip.Keys.Count > 0)
			{
				LoadRecords_FK_RS_POINT_RS_TRIP(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsStep.Keys.Count > 0)
			{
				LoadRecords_FK_RS_NUMBER_RS_STEP(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsTrip.Keys.Count > 0)
			{
				LoadRecords_FK_RS_RUNTIME_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSOT_TRIPIN_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSOT_TRIPIN0_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSOT_TRIPOUT_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSOT_TRIPOUT0_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSSHIFT_TRIPIN_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSSHIFT_TRIPIN0_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSSHIFT_TRIPOUT_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSSHIFT_TRIPOUT0_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSTT_TRIPA_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSTT_TRIPB_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSW_TRIPIN_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSW_TRIPIN0_RS_TRIP0(connection, transaction);
				LoadRecords_FK_RSW_TRIPOUT_RS_TRIP(connection, transaction);
				LoadRecords_FK_RSW_TRIPOUT0_RS_TRIP0(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsOperationtype.Keys.Count > 0)
			{
				LoadRecords_FK_RS_POINT_RS_OPERATIONTYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsTriptype.Keys.Count > 0)
			{
				LoadRecords_FK_RS_POINT_RS_TRIPTYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Rule.Keys.Count > 0)
			{
				LoadRecords_VehiclegroupRule_Rule(connection, transaction);
				LoadRecords_VehicleRule_Rule(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Schedulerevent.Keys.Count > 0)
			{
				LoadRecords_FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT(connection, transaction);
				LoadRecords_SchedulerQueue_SchedulerEvent(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Schedulerqueue.Keys.Count > 0)
			{
				LoadRecords_FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Season.Keys.Count > 0)
			{
				LoadRecords_FactorValues_Season(connection, transaction);
				LoadRecords_Period_Season(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Session.Keys.Count > 0)
			{
				LoadRecords_Trail_Session(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_SmsType.Keys.Count > 0)
			{
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_VehicleKind.Keys.Count > 0)
			{
				LoadRecords_Vehicle_VehicleKind(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_VehicleStatus.Keys.Count > 0)
			{
				LoadRecords_LogVehicleStatus_VehicleStatus(connection, transaction);
				LoadRecords_Vehicle_VehicleStatus(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Vehicle.Keys.Count > 0)
			{
				LoadRecords_Controller_Vehicle(connection, transaction);
				LoadRecords_DriverVehicle_Vehicle(connection, transaction);
				LoadRecords_FK_BRIGADE_VEHICLE(connection, transaction);
				LoadRecords_FK_GOODS_TYPE_VEHICLE_VEHICLE(connection, transaction);
				LoadRecords_FK_JOURNAL_DAY_VEHICLE(connection, transaction);
				LoadRecords_FK_JOURNAL_VEHICLE(connection, transaction);
				LoadRecords_FK_JOURNAL_VEHICLE_VEHICLE(connection, transaction);
				LoadRecords_FK_VEHICLE_OWNER_VEHICLE(connection, transaction);
				LoadRecords_FK_VEHICLE_PICTURE_VEHICLE(connection, transaction);
				LoadRecords_LogVehicleStatus_Vehicle(connection, transaction);
				LoadRecords_OperatorGroupVehicle_Vehicle(connection, transaction);
				LoadRecords_OperatorVehicle_Vehicle(connection, transaction);
				LoadRecords_VehicleGroupVehicle_Vehicle(connection, transaction);
				LoadRecords_VehicleRule_Vehicle(connection, transaction);
				LoadRecords_WaybillHeader_Trailor1(connection, transaction);
				LoadRecords_WaybillHeader_Trailor2(connection, transaction);
				LoadRecords_WaybillHeader_Vehicle(connection, transaction);
				LoadRecords_ZoneVehicle_Vehicle(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Brigade.Keys.Count > 0)
			{
				LoadRecords_FK_BRIGADE_DRIVER_BRIGADE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_BrigadeDriver.Keys.Count > 0)
			{
				LoadRecords_FK_JOURNAL_DAY_BRIGADE_DRIVER(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Controller.Keys.Count > 0)
			{
				LoadRecords_ControllerTime_Controller(connection, transaction);
				LoadRecords_FK_CONTROLLER_INFO_CONTROLLER(connection, transaction);
				LoadRecords_FK_ControllerSensorMap_Controller(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Vehiclegroup.Keys.Count > 0)
			{
				LoadRecords_OperatorGroupVehicleGroup_VehicleGroup(connection, transaction);
				LoadRecords_OperatorVehicleGroup_VehicleGroup(connection, transaction);
				LoadRecords_VehiclegroupRule_Vehicle(connection, transaction);
				LoadRecords_VehicleGroupVehicle_VehicleGroup(connection, transaction);
				LoadRecords_ZoneVehicleGroup_Vehicle(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Waybill.Keys.Count > 0)
			{
				LoadRecords_FK_BLADING_WAYBILL(connection, transaction);
				LoadRecords_WbTrip_Waybill(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Waybillmark.Keys.Count > 0)
			{
				LoadRecords_Waybillheader_Waybillmark_Waybillmark(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Wayout.Keys.Count > 0)
			{
				LoadRecords_FK_JOURNAL_DAY_WAYOUT(connection, transaction);
				LoadRecords_FK_RS_WAYOUT_WAYOUT(connection, transaction);
				LoadRecords_Journal_Wayout(connection, transaction);
				LoadRecords_Schedule_Wayout(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_JournalDay.Keys.Count > 0)
			{
				LoadRecords_FK_ORDER_TRIP_JOURNAL_DAY(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_OrderTrip.Keys.Count > 0)
			{
				LoadRecords_FK_WB_TRIP_ORDER_TRIP(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsWayout.Keys.Count > 0)
			{
				LoadRecords_FK_RS_POINT_RS_WAYOUT(connection, transaction);
				LoadRecords_FK_RS_SHIFT_RS_WAYOUT(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_RsShift.Keys.Count > 0)
			{
				LoadRecords_FK_RS_POINT_RS_SHIFT(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Schedule.Keys.Count > 0)
			{
				LoadRecords_LogWaybillHeaderStatus_Schedule(connection, transaction);
				LoadRecords_ScheduleDetail_Schedule(connection, transaction);
				LoadRecords_WaybillHeader_Schedule(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ScheduleDetail.Keys.Count > 0)
			{
				LoadRecords_FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL(connection, transaction);
				LoadRecords_Trip_ScheduleDetail(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_WebLine.Keys.Count > 0)
			{
				LoadRecords_WebLineVertex_WebLine(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_WebPointType.Keys.Count > 0)
			{
				LoadRecords_WebPoint_Type(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_WorktimeStatusType.Keys.Count > 0)
			{
				LoadRecords_TripKind_WorktimeStatusType(connection, transaction);
				LoadRecords_WorktimeReason_WorktimeStatusType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_TripKind.Keys.Count > 0)
			{
				LoadRecords_Trip_TripKind(connection, transaction);
				LoadRecords_WaybillHeader_TripKind(connection, transaction);
				LoadRecords_WbTrip_TripKind(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Trip.Keys.Count > 0)
			{
				LoadRecords_FK_SCHEDULE_GEOZONE_TRIP(connection, transaction);
				LoadRecords_SchedulePoint_Trip(connection, transaction);
				LoadRecords_WbTrip_Trip(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_SchedulePoint.Keys.Count > 0)
			{
				LoadRecords_SchedulePassage_SchedulePoint(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_WaybillHeader.Keys.Count > 0)
			{
				LoadRecords_SchedulePassage_WaybillHeader(connection, transaction);
				LoadRecords_WaybillheaderWaybillmark_WaybillHeader(connection, transaction);
				LoadRecords_WbTrip_WaybillHeader(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_WorktimeReason.Keys.Count > 0)
			{
				LoadRecords_WbTrip_WorktimeReason(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ZoneType.Keys.Count > 0)
			{
				LoadRecords_ZonePrimitive_ZoneType(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ZonePrimitive.Keys.Count > 0)
			{
				LoadRecords_GeoZonePrimitive_ZonePrimitive(connection, transaction);
				LoadRecords_ZonePrimitiveVertex_ZonePrimitive(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Zonegroup.Keys.Count > 0)
			{
				LoadRecords_OperatorGroupZoneGroup_ZoneGroup(connection, transaction);
				LoadRecords_OperatorZoneGroup_ZoneGroup(connection, transaction);
				LoadRecords_ZoneGroupZone_ZoneGroup(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ZonegroupZone.Keys.Count > 0) RemoveRecords_ZonegroupZone(connection, transaction);
			if (this.hashKey_OperatorgroupZonegroup.Keys.Count > 0) RemoveRecords_OperatorgroupZonegroup(connection, transaction);
			if (this.hashKey_OperatorZonegroup.Keys.Count > 0) RemoveRecords_OperatorZonegroup(connection, transaction);
			if (this.hashKey_Zonegroup.Keys.Count > 0) RemoveRecords_Zonegroup(connection, transaction);
			if (this.hashKey_ZoneVehiclegroup.Keys.Count > 0) RemoveRecords_ZoneVehiclegroup(connection, transaction);
			if (this.hashKey_ZoneVehicle.Keys.Count > 0) RemoveRecords_ZoneVehicle(connection, transaction);
			if (this.hashKey_ZonePrimitiveVertex.Keys.Count > 0) RemoveRecords_ZonePrimitiveVertex(connection, transaction);
			if (this.hashKey_GeoZonePrimitive.Keys.Count > 0) RemoveRecords_GeoZonePrimitive(connection, transaction);
			if (this.hashKey_ZonePrimitive.Keys.Count > 0) RemoveRecords_ZonePrimitive(connection, transaction);
			if (this.hashKey_ZoneType.Keys.Count > 0) RemoveRecords_ZoneType(connection, transaction);
			if (this.hashKey_WbTrip.Keys.Count > 0) RemoveRecords_WbTrip(connection, transaction);
			if (this.hashKey_WorktimeReason.Keys.Count > 0) RemoveRecords_WorktimeReason(connection, transaction);
			if (this.hashKey_WaybillheaderWaybillmark.Keys.Count > 0) RemoveRecords_WaybillheaderWaybillmark(connection, transaction);
			if (this.hashKey_SchedulePassage.Keys.Count > 0) RemoveRecords_SchedulePassage(connection, transaction);
			if (this.hashKey_WaybillHeader.Keys.Count > 0) RemoveRecords_WaybillHeader(connection, transaction);
			if (this.hashKey_SchedulePoint.Keys.Count > 0) RemoveRecords_SchedulePoint(connection, transaction);
			if (this.hashKey_ScheduleGeozone.Keys.Count > 0) RemoveRecords_ScheduleGeozone(connection, transaction);
			if (this.hashKey_Trip.Keys.Count > 0) RemoveRecords_Trip(connection, transaction);
			if (this.hashKey_TripKind.Keys.Count > 0) RemoveRecords_TripKind(connection, transaction);
			if (this.hashKey_WorktimeStatusType.Keys.Count > 0) RemoveRecords_WorktimeStatusType(connection, transaction);
			if (this.hashKey_Workstation.Keys.Count > 0) RemoveRecords_Workstation(connection, transaction);
			if (this.hashKey_WorkplaceConfig.Keys.Count > 0) RemoveRecords_WorkplaceConfig(connection, transaction);
			if (this.hashKey_WorkTime.Keys.Count > 0) RemoveRecords_WorkTime(connection, transaction);
			if (this.hashKey_WebPoint.Keys.Count > 0) RemoveRecords_WebPoint(connection, transaction);
			if (this.hashKey_WebPointType.Keys.Count > 0) RemoveRecords_WebPointType(connection, transaction);
			if (this.hashKey_WebLineVertex.Keys.Count > 0) RemoveRecords_WebLineVertex(connection, transaction);
			if (this.hashKey_WebLine.Keys.Count > 0) RemoveRecords_WebLine(connection, transaction);
			if (this.hashKey_ScheduleDetailInfo.Keys.Count > 0) RemoveRecords_ScheduleDetailInfo(connection, transaction);
			if (this.hashKey_ScheduleDetail.Keys.Count > 0) RemoveRecords_ScheduleDetail(connection, transaction);
			if (this.hashKey_LogWaybillHeaderStatus.Keys.Count > 0) RemoveRecords_LogWaybillHeaderStatus(connection, transaction);
			if (this.hashKey_Schedule.Keys.Count > 0) RemoveRecords_Schedule(connection, transaction);
			if (this.hashKey_RsPoint.Keys.Count > 0) RemoveRecords_RsPoint(connection, transaction);
			if (this.hashKey_RsShift.Keys.Count > 0) RemoveRecords_RsShift(connection, transaction);
			if (this.hashKey_RsWayout.Keys.Count > 0) RemoveRecords_RsWayout(connection, transaction);
			if (this.hashKey_OrderTrip.Keys.Count > 0) RemoveRecords_OrderTrip(connection, transaction);
			if (this.hashKey_JournalDay.Keys.Count > 0) RemoveRecords_JournalDay(connection, transaction);
			if (this.hashKey_Journal.Keys.Count > 0) RemoveRecords_Journal(connection, transaction);
			if (this.hashKey_Wayout.Keys.Count > 0) RemoveRecords_Wayout(connection, transaction);
			if (this.hashKey_Waybillmark.Keys.Count > 0) RemoveRecords_Waybillmark(connection, transaction);
			if (this.hashKey_Blading.Keys.Count > 0) RemoveRecords_Blading(connection, transaction);
			if (this.hashKey_Waybill.Keys.Count > 0) RemoveRecords_Waybill(connection, transaction);
			if (this.hashKey_VehiclegroupVehicle.Keys.Count > 0) RemoveRecords_VehiclegroupVehicle(connection, transaction);
			if (this.hashKey_VehiclegroupRule.Keys.Count > 0) RemoveRecords_VehiclegroupRule(connection, transaction);
			if (this.hashKey_OperatorgroupVehiclegroup.Keys.Count > 0) RemoveRecords_OperatorgroupVehiclegroup(connection, transaction);
			if (this.hashKey_OperatorVehiclegroup.Keys.Count > 0) RemoveRecords_OperatorVehiclegroup(connection, transaction);
			if (this.hashKey_Vehiclegroup.Keys.Count > 0) RemoveRecords_Vehiclegroup(connection, transaction);
			if (this.hashKey_VehicleRule.Keys.Count > 0) RemoveRecords_VehicleRule(connection, transaction);
			if (this.hashKey_VehiclePicture.Keys.Count > 0) RemoveRecords_VehiclePicture(connection, transaction);
			if (this.hashKey_VehicleOwner.Keys.Count > 0) RemoveRecords_VehicleOwner(connection, transaction);
			if (this.hashKey_OperatorgroupVehicle.Keys.Count > 0) RemoveRecords_OperatorgroupVehicle(connection, transaction);
			if (this.hashKey_OperatorVehicle.Keys.Count > 0) RemoveRecords_OperatorVehicle(connection, transaction);
			if (this.hashKey_LogVehicleStatus.Keys.Count > 0) RemoveRecords_LogVehicleStatus(connection, transaction);
			if (this.hashKey_JournalVehicle.Keys.Count > 0) RemoveRecords_JournalVehicle(connection, transaction);
			if (this.hashKey_GoodsTypeVehicle.Keys.Count > 0) RemoveRecords_GoodsTypeVehicle(connection, transaction);
			if (this.hashKey_DriverVehicle.Keys.Count > 0) RemoveRecords_DriverVehicle(connection, transaction);
			if (this.hashKey_ControllerTime.Keys.Count > 0) RemoveRecords_ControllerTime(connection, transaction);
			if (this.hashKey_ControllerSensorMap.Keys.Count > 0) RemoveRecords_ControllerSensorMap(connection, transaction);
			if (this.hashKey_ControllerInfo.Keys.Count > 0) RemoveRecords_ControllerInfo(connection, transaction);
			if (this.hashKey_Controller.Keys.Count > 0) RemoveRecords_Controller(connection, transaction);
			if (this.hashKey_BrigadeDriver.Keys.Count > 0) RemoveRecords_BrigadeDriver(connection, transaction);
			if (this.hashKey_Brigade.Keys.Count > 0) RemoveRecords_Brigade(connection, transaction);
			if (this.hashKey_Vehicle.Keys.Count > 0) RemoveRecords_Vehicle(connection, transaction);
			if (this.hashKey_VehicleStatus.Keys.Count > 0) RemoveRecords_VehicleStatus(connection, transaction);
			if (this.hashKey_VehicleKind.Keys.Count > 0) RemoveRecords_VehicleKind(connection, transaction);
			if (this.hashKey_Trail.Keys.Count > 0) RemoveRecords_Trail(connection, transaction);
			if (this.hashKey_TaskProcessorLog.Keys.Count > 0) RemoveRecords_TaskProcessorLog(connection, transaction);
			if (this.hashKey_SmsType.Keys.Count > 0) RemoveRecords_SmsType(connection, transaction);
			if (this.hashKey_Session.Keys.Count > 0) RemoveRecords_Session(connection, transaction);
			if (this.hashKey_Seattype.Keys.Count > 0) RemoveRecords_Seattype(connection, transaction);
			if (this.hashKey_Period.Keys.Count > 0) RemoveRecords_Period(connection, transaction);
			if (this.hashKey_FactorValues.Keys.Count > 0) RemoveRecords_FactorValues(connection, transaction);
			if (this.hashKey_Season.Keys.Count > 0) RemoveRecords_Season(connection, transaction);
			if (this.hashKey_EmailSchedulerqueue.Keys.Count > 0) RemoveRecords_EmailSchedulerqueue(connection, transaction);
			if (this.hashKey_Schedulerqueue.Keys.Count > 0) RemoveRecords_Schedulerqueue(connection, transaction);
			if (this.hashKey_EmailSchedulerevent.Keys.Count > 0) RemoveRecords_EmailSchedulerevent(connection, transaction);
			if (this.hashKey_Schedulerevent.Keys.Count > 0) RemoveRecords_Schedulerevent(connection, transaction);
			if (this.hashKey_Rule.Keys.Count > 0) RemoveRecords_Rule(connection, transaction);
			if (this.hashKey_RsTriptype.Keys.Count > 0) RemoveRecords_RsTriptype(connection, transaction);
			if (this.hashKey_RsRuntime.Keys.Count > 0) RemoveRecords_RsRuntime(connection, transaction);
			if (this.hashKey_RsOperationtype.Keys.Count > 0) RemoveRecords_RsOperationtype(connection, transaction);
			if (this.hashKey_RsTrip.Keys.Count > 0) RemoveRecords_RsTrip(connection, transaction);
			if (this.hashKey_RsNumber.Keys.Count > 0) RemoveRecords_RsNumber(connection, transaction);
			if (this.hashKey_RsStep.Keys.Count > 0) RemoveRecords_RsStep(connection, transaction);
			if (this.hashKey_RsRoundTrip.Keys.Count > 0) RemoveRecords_RsRoundTrip(connection, transaction);
			if (this.hashKey_RsPeriod.Keys.Count > 0) RemoveRecords_RsPeriod(connection, transaction);
			if (this.hashKey_RsVariant.Keys.Count > 0) RemoveRecords_RsVariant(connection, transaction);
			if (this.hashKey_RsType.Keys.Count > 0) RemoveRecords_RsType(connection, transaction);
			if (this.hashKey_RsTripstype.Keys.Count > 0) RemoveRecords_RsTripstype(connection, transaction);
			if (this.hashKey_RsOperationstype.Keys.Count > 0) RemoveRecords_RsOperationstype(connection, transaction);
			if (this.hashKey_Rs.Keys.Count > 0) RemoveRecords_Rs(connection, transaction);
			if (this.hashKey_RoutegroupRoute.Keys.Count > 0) RemoveRecords_RoutegroupRoute(connection, transaction);
			if (this.hashKey_OperatorgroupRoutegroup.Keys.Count > 0) RemoveRecords_OperatorgroupRoutegroup(connection, transaction);
			if (this.hashKey_OperatorRoutegroup.Keys.Count > 0) RemoveRecords_OperatorRoutegroup(connection, transaction);
			if (this.hashKey_Routegroup.Keys.Count > 0) RemoveRecords_Routegroup(connection, transaction);
			if (this.hashKey_RouteTrip.Keys.Count > 0) RemoveRecords_RouteTrip(connection, transaction);
			if (this.hashKey_RoutePoint.Keys.Count > 0) RemoveRecords_RoutePoint(connection, transaction);
			if (this.hashKey_RouteGeozone.Keys.Count > 0) RemoveRecords_RouteGeozone(connection, transaction);
			if (this.hashKey_OperatorgroupRoute.Keys.Count > 0) RemoveRecords_OperatorgroupRoute(connection, transaction);
			if (this.hashKey_OperatorRoute.Keys.Count > 0) RemoveRecords_OperatorRoute(connection, transaction);
			if (this.hashKey_GeoSegmentVertex.Keys.Count > 0) RemoveRecords_GeoSegmentVertex(connection, transaction);
			if (this.hashKey_GeoSegmentRuntime.Keys.Count > 0) RemoveRecords_GeoSegmentRuntime(connection, transaction);
			if (this.hashKey_GeoSegment.Keys.Count > 0) RemoveRecords_GeoSegment(connection, transaction);
			if (this.hashKey_GeoTrip.Keys.Count > 0) RemoveRecords_GeoTrip(connection, transaction);
			if (this.hashKey_Route.Keys.Count > 0) RemoveRecords_Route(connection, transaction);
			if (this.hashKey_RightOperatorgroup.Keys.Count > 0) RemoveRecords_RightOperatorgroup(connection, transaction);
			if (this.hashKey_RightOperator.Keys.Count > 0) RemoveRecords_RightOperator(connection, transaction);
			if (this.hashKey_OperatorgroupZone.Keys.Count > 0) RemoveRecords_OperatorgroupZone(connection, transaction);
			if (this.hashKey_OperatorgroupReport.Keys.Count > 0) RemoveRecords_OperatorgroupReport(connection, transaction);
			if (this.hashKey_OperatorgroupDrivergroup.Keys.Count > 0) RemoveRecords_OperatorgroupDrivergroup(connection, transaction);
			if (this.hashKey_OperatorgroupDriver.Keys.Count > 0) RemoveRecords_OperatorgroupDriver(connection, transaction);
			if (this.hashKey_OperatorgroupDepartment.Keys.Count > 0) RemoveRecords_OperatorgroupDepartment(connection, transaction);
			if (this.hashKey_OperatorZone.Keys.Count > 0) RemoveRecords_OperatorZone(connection, transaction);
			if (this.hashKey_OperatorReport.Keys.Count > 0) RemoveRecords_OperatorReport(connection, transaction);
			if (this.hashKey_OperatorDrivergroup.Keys.Count > 0) RemoveRecords_OperatorDrivergroup(connection, transaction);
			if (this.hashKey_OperatorDriver.Keys.Count > 0) RemoveRecords_OperatorDriver(connection, transaction);
			if (this.hashKey_OperatorDepartment.Keys.Count > 0) RemoveRecords_OperatorDepartment(connection, transaction);
			if (this.hashKey_Right.Keys.Count > 0) RemoveRecords_Right(connection, transaction);
			if (this.hashKey_ReprintReason.Keys.Count > 0) RemoveRecords_ReprintReason(connection, transaction);
			if (this.hashKey_Report.Keys.Count > 0) RemoveRecords_Report(connection, transaction);
			if (this.hashKey_Point.Keys.Count > 0) RemoveRecords_Point(connection, transaction);
			if (this.hashKey_PointKind.Keys.Count > 0) RemoveRecords_PointKind(connection, transaction);
			if (this.hashKey_Owner.Keys.Count > 0) RemoveRecords_Owner(connection, transaction);
			if (this.hashKey_Order.Keys.Count > 0) RemoveRecords_Order(connection, transaction);
			if (this.hashKey_OrderType.Keys.Count > 0) RemoveRecords_OrderType(connection, transaction);
			if (this.hashKey_LogOpsEvents.Keys.Count > 0) RemoveRecords_LogOpsEvents(connection, transaction);
			if (this.hashKey_OpsEvent.Keys.Count > 0) RemoveRecords_OpsEvent(connection, transaction);
			if (this.hashKey_OperatorgroupOperator.Keys.Count > 0) RemoveRecords_OperatorgroupOperator(connection, transaction);
			if (this.hashKey_Operatorgroup.Keys.Count > 0) RemoveRecords_Operatorgroup(connection, transaction);
			if (this.hashKey_OperatorProfile.Keys.Count > 0) RemoveRecords_OperatorProfile(connection, transaction);
			if (this.hashKey_MessageOperator.Keys.Count > 0) RemoveRecords_MessageOperator(connection, transaction);
			if (this.hashKey_Operator.Keys.Count > 0) RemoveRecords_Operator(connection, transaction);
			if (this.hashKey_MessageField.Keys.Count > 0) RemoveRecords_MessageField(connection, transaction);
			if (this.hashKey_MessageTemplateField.Keys.Count > 0) RemoveRecords_MessageTemplateField(connection, transaction);
			if (this.hashKey_Message.Keys.Count > 0) RemoveRecords_Message(connection, transaction);
			if (this.hashKey_MessageTemplate.Keys.Count > 0) RemoveRecords_MessageTemplate(connection, transaction);
			if (this.hashKey_MessageBoard.Keys.Count > 0) RemoveRecords_MessageBoard(connection, transaction);
			if (this.hashKey_MediaAcceptors.Keys.Count > 0) RemoveRecords_MediaAcceptors(connection, transaction);
			if (this.hashKey_Media.Keys.Count > 0) RemoveRecords_Media(connection, transaction);
			if (this.hashKey_MediaType.Keys.Count > 0) RemoveRecords_MediaType(connection, transaction);
			if (this.hashKey_MapVertex.Keys.Count > 0) RemoveRecords_MapVertex(connection, transaction);
			if (this.hashKey_Maps.Keys.Count > 0) RemoveRecords_Maps(connection, transaction);
			if (this.hashKey_GoodsLogisticOrder.Keys.Count > 0) RemoveRecords_GoodsLogisticOrder(connection, transaction);
			if (this.hashKey_LogisticOrder.Keys.Count > 0) RemoveRecords_LogisticOrder(connection, transaction);
			if (this.hashKey_LogisticAddress.Keys.Count > 0) RemoveRecords_LogisticAddress(connection, transaction);
			if (this.hashKey_JournalDriver.Keys.Count > 0) RemoveRecords_JournalDriver(connection, transaction);
			if (this.hashKey_JobDelbadxy.Keys.Count > 0) RemoveRecords_JobDelbadxy(connection, transaction);
			if (this.hashKey_GuardEvents.Keys.Count > 0) RemoveRecords_GuardEvents(connection, transaction);
			if (this.hashKey_GraphicShift.Keys.Count > 0) RemoveRecords_GraphicShift(connection, transaction);
			if (this.hashKey_Graphic.Keys.Count > 0) RemoveRecords_Graphic(connection, transaction);
			if (this.hashKey_Goods.Keys.Count > 0) RemoveRecords_Goods(connection, transaction);
			if (this.hashKey_GoodsType.Keys.Count > 0) RemoveRecords_GoodsType(connection, transaction);
			if (this.hashKey_GeoZone.Keys.Count > 0) RemoveRecords_GeoZone(connection, transaction);
			if (this.hashKey_Factors.Keys.Count > 0) RemoveRecords_Factors(connection, transaction);
			if (this.hashKey_Email.Keys.Count > 0) RemoveRecords_Email(connection, transaction);
			if (this.hashKey_DrivergroupDriver.Keys.Count > 0) RemoveRecords_DrivergroupDriver(connection, transaction);
			if (this.hashKey_Drivergroup.Keys.Count > 0) RemoveRecords_Drivergroup(connection, transaction);
			if (this.hashKey_Driver.Keys.Count > 0) RemoveRecords_Driver(connection, transaction);
			if (this.hashKey_DriverStatus.Keys.Count > 0) RemoveRecords_DriverStatus(connection, transaction);
			if (this.hashKey_DriverMsgTemplate.Keys.Count > 0) RemoveRecords_DriverMsgTemplate(connection, transaction);
			if (this.hashKey_DriverBonus.Keys.Count > 0) RemoveRecords_DriverBonus(connection, transaction);
			if (this.hashKey_DotnetType.Keys.Count > 0) RemoveRecords_DotnetType(connection, transaction);
			if (this.hashKey_Cal.Keys.Count > 0) RemoveRecords_Cal(connection, transaction);
			if (this.hashKey_DayKind.Keys.Count > 0) RemoveRecords_DayKind(connection, transaction);
			if (this.hashKey_DkSet.Keys.Count > 0) RemoveRecords_DkSet(connection, transaction);
			if (this.hashKey_Decade.Keys.Count > 0) RemoveRecords_Decade(connection, transaction);
			if (this.hashKey_Department.Keys.Count > 0) RemoveRecords_Department(connection, transaction);
			if (this.hashKey_DayTime.Keys.Count > 0) RemoveRecords_DayTime(connection, transaction);
			if (this.hashKey_DayType.Keys.Count > 0) RemoveRecords_DayType(connection, transaction);
			if (this.hashKey_Day.Keys.Count > 0) RemoveRecords_Day(connection, transaction);
			if (this.hashKey_Customer.Keys.Count > 0) RemoveRecords_Customer(connection, transaction);
			if (this.hashKey_CountGprs.Keys.Count > 0) RemoveRecords_CountGprs(connection, transaction);
			if (this.hashKey_ControllerSensor.Keys.Count > 0) RemoveRecords_ControllerSensor(connection, transaction);
			if (this.hashKey_ControllerType.Keys.Count > 0) RemoveRecords_ControllerType(connection, transaction);
			if (this.hashKey_ControllerSensorLegend.Keys.Count > 0) RemoveRecords_ControllerSensorLegend(connection, transaction);
			if (this.hashKey_ControllerSensorType.Keys.Count > 0) RemoveRecords_ControllerSensorType(connection, transaction);
			if (this.hashKey_ContractorCalendar.Keys.Count > 0) RemoveRecords_ContractorCalendar(connection, transaction);
			if (this.hashKey_Contractor.Keys.Count > 0) RemoveRecords_Contractor(connection, transaction);
			if (this.hashKey_Constants.Keys.Count > 0) RemoveRecords_Constants(connection, transaction);
			if (this.hashKey_CalendarDay.Keys.Count > 0) RemoveRecords_CalendarDay(connection, transaction);
			if (this.hashKey_Busstop.Keys.Count > 0) RemoveRecords_Busstop(connection, transaction);
			if (this.hashKey_BladingType.Keys.Count > 0) RemoveRecords_BladingType(connection, transaction);

		}

		public virtual void CheckBladingProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							BladingPrimaryKey key = new BladingPrimaryKey();
							key.BladingId = (System.Int32)row["BLADING_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Blading.Add(key, key.BladingId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Blading(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteBlading", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (BladingPrimaryKey pk in hashKey_Blading.Keys)
			{
				sp["@BLADING_ID"].Value = pk.BladingId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckBladingTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							BladingTypePrimaryKey key = new BladingTypePrimaryKey();
							key.BladingTypeId = (System.Int32)row["BLADING_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_BladingType.Add(key, key.BladingTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_BladingType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteBladingType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (BladingTypePrimaryKey pk in hashKey_BladingType.Keys)
			{
				sp["@BLADING_TYPE_ID"].Value = pk.BladingTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckBrigadeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							BrigadePrimaryKey key = new BrigadePrimaryKey();
							key.BrigadeId = (System.Int32)row["BRIGADE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Brigade.Add(key, key.BrigadeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Brigade(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteBrigade", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (BrigadePrimaryKey pk in hashKey_Brigade.Keys)
			{
				sp["@BRIGADE_ID"].Value = pk.BrigadeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckBrigadeDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							BrigadeDriverPrimaryKey key = new BrigadeDriverPrimaryKey();
							key.BrigadeDriverId = (System.Int32)row["BRIGADE_DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_BrigadeDriver.Add(key, key.BrigadeDriverId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_BrigadeDriver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteBrigadeDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (BrigadeDriverPrimaryKey pk in hashKey_BrigadeDriver.Keys)
			{
				sp["@BRIGADE_DRIVER_ID"].Value = pk.BrigadeDriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckBusstopProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							BusstopPrimaryKey key = new BusstopPrimaryKey();
							key.BusstopId = (System.Int32)row["BUSSTOP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Busstop.Add(key, key.BusstopId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Busstop(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteBusstop", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (BusstopPrimaryKey pk in hashKey_Busstop.Keys)
			{
				sp["@BUSSTOP_ID"].Value = pk.BusstopId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckCalProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							CalPrimaryKey key = new CalPrimaryKey();
							key.Date = (System.DateTime)row["DATE", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Cal.Add(key, key.Date);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Cal(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteCal", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (CalPrimaryKey pk in hashKey_Cal.Keys)
			{
				sp["@DATE"].Value = pk.Date;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckCalendarDayProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							CalendarDayPrimaryKey key = new CalendarDayPrimaryKey();
							key.CalendarDayId = (System.Int32)row["CALENDAR_DAY_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_CalendarDay.Add(key, key.CalendarDayId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_CalendarDay(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteCalendarDay", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (CalendarDayPrimaryKey pk in hashKey_CalendarDay.Keys)
			{
				sp["@CALENDAR_DAY_ID"].Value = pk.CalendarDayId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckConstantsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ConstantsPrimaryKey key = new ConstantsPrimaryKey();
							key.Name = (System.String)row["NAME", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Constants.Add(key, key.Name);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Constants(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteConstants", connection, transaction);
			foreach (ConstantsPrimaryKey pk in hashKey_Constants.Keys)
			{
				sp["@NAME"].Value = pk.Name;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckContractorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ContractorPrimaryKey key = new ContractorPrimaryKey();
							key.ContractorId = (System.Int32)row["CONTRACTOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Contractor.Add(key, key.ContractorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Contractor(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteContractor", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ContractorPrimaryKey pk in hashKey_Contractor.Keys)
			{
				sp["@CONTRACTOR_ID"].Value = pk.ContractorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckContractorCalendarProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ContractorCalendarPrimaryKey key = new ContractorCalendarPrimaryKey();
							key.ContractorCalendarId = (System.Int32)row["CONTRACTOR_CALENDAR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ContractorCalendar.Add(key, key.ContractorCalendarId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ContractorCalendar(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteContractorCalendar", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ContractorCalendarPrimaryKey pk in hashKey_ContractorCalendar.Keys)
			{
				sp["@CONTRACTOR_CALENDAR_ID"].Value = pk.ContractorCalendarId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerPrimaryKey key = new ControllerPrimaryKey();
							key.ControllerId = (System.Int32)row["CONTROLLER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Controller.Add(key, key.ControllerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Controller(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteController", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerPrimaryKey pk in hashKey_Controller.Keys)
			{
				sp["@CONTROLLER_ID"].Value = pk.ControllerId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerInfoProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerInfoPrimaryKey key = new ControllerInfoPrimaryKey();
							key.ControllerId = (System.Int32)row["CONTROLLER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerInfo.Add(key, key.ControllerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerInfo(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerInfo", connection, transaction);
			foreach (ControllerInfoPrimaryKey pk in hashKey_ControllerInfo.Keys)
			{
				sp["@CONTROLLER_ID"].Value = pk.ControllerId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerSensorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerSensorPrimaryKey key = new ControllerSensorPrimaryKey();
							key.ControllerSensorId = (System.Int32)row["CONTROLLER_SENSOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerSensor.Add(key, key.ControllerSensorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerSensor(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerSensor", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerSensorPrimaryKey pk in hashKey_ControllerSensor.Keys)
			{
				sp["@CONTROLLER_SENSOR_ID"].Value = pk.ControllerSensorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerSensorLegendProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerSensorLegendPrimaryKey key = new ControllerSensorLegendPrimaryKey();
							key.ControllerSensorLegendId = (System.Int32)row["CONTROLLER_SENSOR_LEGEND_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerSensorLegend.Add(key, key.ControllerSensorLegendId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerSensorLegend(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerSensorLegend", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerSensorLegendPrimaryKey pk in hashKey_ControllerSensorLegend.Keys)
			{
				sp["@CONTROLLER_SENSOR_LEGEND_ID"].Value = pk.ControllerSensorLegendId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerSensorMapProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerSensorMapPrimaryKey key = new ControllerSensorMapPrimaryKey();
							key.ControllerSensorMapId = (System.Int32)row["CONTROLLER_SENSOR_MAP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerSensorMap.Add(key, key.ControllerSensorMapId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerSensorMap(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerSensorMap", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerSensorMapPrimaryKey pk in hashKey_ControllerSensorMap.Keys)
			{
				sp["@CONTROLLER_SENSOR_MAP_ID"].Value = pk.ControllerSensorMapId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerSensorTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerSensorTypePrimaryKey key = new ControllerSensorTypePrimaryKey();
							key.ControllerSensorTypeId = (System.Int32)row["CONTROLLER_SENSOR_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerSensorType.Add(key, key.ControllerSensorTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerSensorType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerSensorType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerSensorTypePrimaryKey pk in hashKey_ControllerSensorType.Keys)
			{
				sp["@CONTROLLER_SENSOR_TYPE_ID"].Value = pk.ControllerSensorTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerTimeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerTimePrimaryKey key = new ControllerTimePrimaryKey();
							key.ControllerTimeId = (System.Int32)row["CONTROLLER_TIME_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerTime.Add(key, key.ControllerTimeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerTime(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerTime", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerTimePrimaryKey pk in hashKey_ControllerTime.Keys)
			{
				sp["@CONTROLLER_TIME_ID"].Value = pk.ControllerTimeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckControllerTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerTypePrimaryKey key = new ControllerTypePrimaryKey();
							key.ControllerTypeId = (System.Int32)row["CONTROLLER_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerType.Add(key, key.ControllerTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ControllerType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerTypePrimaryKey pk in hashKey_ControllerType.Keys)
			{
				sp["@CONTROLLER_TYPE_ID"].Value = pk.ControllerTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckCountGprsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							CountGprsPrimaryKey key = new CountGprsPrimaryKey();
							key.TimeBegin = (System.DateTime)row["TIME_BEGIN", DataRowVersion.Original];
							key.Duration = (System.Int32)row["DURATION", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_CountGprs.Add(key, key.TimeBegin);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_CountGprs(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteCountGprs", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (CountGprsPrimaryKey pk in hashKey_CountGprs.Keys)
			{
				sp["@TIME_BEGIN"].Value = pk.TimeBegin;
				sp["@DURATION"].Value = pk.Duration;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckCountSmsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							CountSmsPrimaryKey key = new CountSmsPrimaryKey();
							key.TimeBegin = (System.DateTime)row["TIME_BEGIN", DataRowVersion.Original];
							key.Duration = (System.Int32)row["DURATION", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_CountSms.Add(key, key.TimeBegin);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_CountSms(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteCountSms", connection, transaction);
			foreach (CountSmsPrimaryKey pk in hashKey_CountSms.Keys)
			{
				sp["@TIME_BEGIN"].Value = pk.TimeBegin;
				sp["@DURATION"].Value = pk.Duration;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckCustomerProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							CustomerPrimaryKey key = new CustomerPrimaryKey();
							key.CustomerId = (System.Int32)row["CUSTOMER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Customer.Add(key, key.CustomerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Customer(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteCustomer", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (CustomerPrimaryKey pk in hashKey_Customer.Keys)
			{
				sp["@CUSTOMER_ID"].Value = pk.CustomerId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDayProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DayPrimaryKey key = new DayPrimaryKey();
							key.DayId = (System.Int32)row["DAY_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Day.Add(key, key.DayId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Day(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDay", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DayPrimaryKey pk in hashKey_Day.Keys)
			{
				sp["@DAY_ID"].Value = pk.DayId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDayKindProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DayKindPrimaryKey key = new DayKindPrimaryKey();
							key.DayKindId = (System.Int32)row["DAY_KIND_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DayKind.Add(key, key.DayKindId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DayKind(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDayKind", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DayKindPrimaryKey pk in hashKey_DayKind.Keys)
			{
				sp["@DAY_KIND_ID"].Value = pk.DayKindId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDayTimeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DayTimePrimaryKey key = new DayTimePrimaryKey();
							key.DayTimeId = (System.Int32)row["DAY_TIME_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DayTime.Add(key, key.DayTimeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DayTime(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDayTime", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DayTimePrimaryKey pk in hashKey_DayTime.Keys)
			{
				sp["@DAY_TIME_ID"].Value = pk.DayTimeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDayTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DayTypePrimaryKey key = new DayTypePrimaryKey();
							key.DayTypeId = (System.Int32)row["DAY_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DayType.Add(key, key.DayTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DayType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDayType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DayTypePrimaryKey pk in hashKey_DayType.Keys)
			{
				sp["@DAY_TYPE_ID"].Value = pk.DayTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDecadeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DecadePrimaryKey key = new DecadePrimaryKey();
							key.DecadeId = (System.Int32)row["DECADE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Decade.Add(key, key.DecadeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Decade(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDecade", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DecadePrimaryKey pk in hashKey_Decade.Keys)
			{
				sp["@DECADE_ID"].Value = pk.DecadeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDepartmentProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DepartmentPrimaryKey key = new DepartmentPrimaryKey();
							key.DepartmentId = (System.Int32)row["DEPARTMENT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Department.Add(key, key.DepartmentId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Department(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDepartment", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DepartmentPrimaryKey pk in hashKey_Department.Keys)
			{
				sp["@DEPARTMENT_ID"].Value = pk.DepartmentId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDkSetProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DkSetPrimaryKey key = new DkSetPrimaryKey();
							key.DkSetId = (System.Int32)row["DK_SET_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DkSet.Add(key, key.DkSetId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DkSet(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDkSet", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DkSetPrimaryKey pk in hashKey_DkSet.Keys)
			{
				sp["@DK_SET_ID"].Value = pk.DkSetId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDotnetTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DotnetTypePrimaryKey key = new DotnetTypePrimaryKey();
							key.DotnetTypeId = (System.Int32)row["DOTNET_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DotnetType.Add(key, key.DotnetTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DotnetType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDotnetType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DotnetTypePrimaryKey pk in hashKey_DotnetType.Keys)
			{
				sp["@DOTNET_TYPE_ID"].Value = pk.DotnetTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DriverPrimaryKey key = new DriverPrimaryKey();
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Driver.Add(key, key.DriverId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DriverPrimaryKey pk in hashKey_Driver.Keys)
			{
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDriverBonusProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DriverBonusPrimaryKey key = new DriverBonusPrimaryKey();
							key.DriverBonusId = (System.Int32)row["driver_bonus_id", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DriverBonus.Add(key, key.DriverBonusId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DriverBonus(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDriverBonus", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DriverBonusPrimaryKey pk in hashKey_DriverBonus.Keys)
			{
				sp["@driver_bonus_id"].Value = pk.DriverBonusId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDriverMsgTemplateProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DriverMsgTemplatePrimaryKey key = new DriverMsgTemplatePrimaryKey();
							key.DriverMsgTemplateId = (System.Int32)row["DRIVER_MSG_TEMPLATE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DriverMsgTemplate.Add(key, key.DriverMsgTemplateId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DriverMsgTemplate(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDriverMsgTemplate", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DriverMsgTemplatePrimaryKey pk in hashKey_DriverMsgTemplate.Keys)
			{
				sp["@DRIVER_MSG_TEMPLATE_ID"].Value = pk.DriverMsgTemplateId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDriverStatusProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DriverStatusPrimaryKey key = new DriverStatusPrimaryKey();
							key.DriverStatusId = (System.Int32)row["DRIVER_STATUS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DriverStatus.Add(key, key.DriverStatusId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DriverStatus(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDriverStatus", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DriverStatusPrimaryKey pk in hashKey_DriverStatus.Keys)
			{
				sp["@DRIVER_STATUS_ID"].Value = pk.DriverStatusId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDriverVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DriverVehiclePrimaryKey key = new DriverVehiclePrimaryKey();
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DriverVehicle.Add(key, key.DriverId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DriverVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDriverVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DriverVehiclePrimaryKey pk in hashKey_DriverVehicle.Keys)
			{
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDrivergroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DrivergroupPrimaryKey key = new DrivergroupPrimaryKey();
							key.DrivergroupId = (System.Int32)row["DRIVERGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Drivergroup.Add(key, key.DrivergroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Drivergroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDrivergroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DrivergroupPrimaryKey pk in hashKey_Drivergroup.Keys)
			{
				sp["@DRIVERGROUP_ID"].Value = pk.DrivergroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckDrivergroupDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							DrivergroupDriverPrimaryKey key = new DrivergroupDriverPrimaryKey();
							key.DrivergroupId = (System.Int32)row["DRIVERGROUP_ID", DataRowVersion.Original];
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_DrivergroupDriver.Add(key, key.DrivergroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_DrivergroupDriver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteDrivergroupDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (DrivergroupDriverPrimaryKey pk in hashKey_DrivergroupDriver.Keys)
			{
				sp["@DRIVERGROUP_ID"].Value = pk.DrivergroupId;
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckEmailProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							EmailPrimaryKey key = new EmailPrimaryKey();
							key.EmailId = (System.Int32)row["EMAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Email.Add(key, key.EmailId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Email(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteEmail", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (EmailPrimaryKey pk in hashKey_Email.Keys)
			{
				sp["@EMAIL_ID"].Value = pk.EmailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckEmailSchedulereventProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							EmailSchedulereventPrimaryKey key = new EmailSchedulereventPrimaryKey();
							key.SchedulereventId = (System.Int32)row["SCHEDULEREVENT_ID", DataRowVersion.Original];
							key.EmailId = (System.Int32)row["EMAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_EmailSchedulerevent.Add(key, key.SchedulereventId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_EmailSchedulerevent(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteEmailSchedulerevent", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (EmailSchedulereventPrimaryKey pk in hashKey_EmailSchedulerevent.Keys)
			{
				sp["@SCHEDULEREVENT_ID"].Value = pk.SchedulereventId;
				sp["@EMAIL_ID"].Value = pk.EmailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckEmailSchedulerqueueProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							EmailSchedulerqueuePrimaryKey key = new EmailSchedulerqueuePrimaryKey();
							key.SchedulerqueueId = (System.Int32)row["SCHEDULERQUEUE_ID", DataRowVersion.Original];
							key.EmailId = (System.Int32)row["EMAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_EmailSchedulerqueue.Add(key, key.SchedulerqueueId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_EmailSchedulerqueue(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteEmailSchedulerqueue", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (EmailSchedulerqueuePrimaryKey pk in hashKey_EmailSchedulerqueue.Keys)
			{
				sp["@SCHEDULERQUEUE_ID"].Value = pk.SchedulerqueueId;
				sp["@EMAIL_ID"].Value = pk.EmailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckFactorValuesProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							FactorValuesPrimaryKey key = new FactorValuesPrimaryKey();
							key.FactorValuesId = (System.Int32)row["FACTOR_VALUES_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_FactorValues.Add(key, key.FactorValuesId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_FactorValues(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteFactorValues", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (FactorValuesPrimaryKey pk in hashKey_FactorValues.Keys)
			{
				sp["@FACTOR_VALUES_ID"].Value = pk.FactorValuesId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckFactorsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							FactorsPrimaryKey key = new FactorsPrimaryKey();
							key.FactorId = (System.Int32)row["FACTOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Factors.Add(key, key.FactorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Factors(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteFactors", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (FactorsPrimaryKey pk in hashKey_Factors.Keys)
			{
				sp["@FACTOR_ID"].Value = pk.FactorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoSegmentProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoSegmentPrimaryKey key = new GeoSegmentPrimaryKey();
							key.GeoSegmentId = (System.Int32)row["GEO_SEGMENT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoSegment.Add(key, key.GeoSegmentId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoSegment(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoSegment", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GeoSegmentPrimaryKey pk in hashKey_GeoSegment.Keys)
			{
				sp["@GEO_SEGMENT_ID"].Value = pk.GeoSegmentId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoSegmentRuntimeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoSegmentRuntimePrimaryKey key = new GeoSegmentRuntimePrimaryKey();
							key.GeoSegmentRuntimeId = (System.Int32)row["GEO_SEGMENT_RUNTIME_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoSegmentRuntime.Add(key, key.GeoSegmentRuntimeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoSegmentRuntime(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoSegmentRuntime", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GeoSegmentRuntimePrimaryKey pk in hashKey_GeoSegmentRuntime.Keys)
			{
				sp["@GEO_SEGMENT_RUNTIME_ID"].Value = pk.GeoSegmentRuntimeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoSegmentVertexProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoSegmentVertexPrimaryKey key = new GeoSegmentVertexPrimaryKey();
							key.GeoSegmentVertexId = (System.Int32)row["GEO_SEGMENT_VERTEX_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoSegmentVertex.Add(key, key.GeoSegmentVertexId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoSegmentVertex(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoSegmentVertex", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GeoSegmentVertexPrimaryKey pk in hashKey_GeoSegmentVertex.Keys)
			{
				sp["@GEO_SEGMENT_VERTEX_ID"].Value = pk.GeoSegmentVertexId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoTripPrimaryKey key = new GeoTripPrimaryKey();
							key.GeoTripId = (System.Int32)row["GEO_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoTrip.Add(key, key.GeoTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoTrip", connection, transaction);
			foreach (GeoTripPrimaryKey pk in hashKey_GeoTrip.Keys)
			{
				sp["@GEO_TRIP_ID"].Value = pk.GeoTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoZoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoZonePrimaryKey key = new GeoZonePrimaryKey();
							key.ZoneId = (System.Int32)row["ZONE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoZone.Add(key, key.ZoneId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoZone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoZone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GeoZonePrimaryKey pk in hashKey_GeoZone.Keys)
			{
				sp["@ZONE_ID"].Value = pk.ZoneId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGeoZonePrimitiveProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GeoZonePrimitivePrimaryKey key = new GeoZonePrimitivePrimaryKey();
							key.GeoZonePrimitiveId = (System.Int32)row["GEO_ZONE_PRIMITIVE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GeoZonePrimitive.Add(key, key.GeoZonePrimitiveId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GeoZonePrimitive(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGeoZonePrimitive", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GeoZonePrimitivePrimaryKey pk in hashKey_GeoZonePrimitive.Keys)
			{
				sp["@GEO_ZONE_PRIMITIVE_ID"].Value = pk.GeoZonePrimitiveId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGoodsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GoodsPrimaryKey key = new GoodsPrimaryKey();
							key.GoodsId = (System.Int32)row["GOODS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Goods.Add(key, key.GoodsId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Goods(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGoods", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GoodsPrimaryKey pk in hashKey_Goods.Keys)
			{
				sp["@GOODS_ID"].Value = pk.GoodsId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGoodsLogisticOrderProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GoodsLogisticOrderPrimaryKey key = new GoodsLogisticOrderPrimaryKey();
							key.GoodsLogisticOrderId = (System.Int32)row["GOODS_LOGISTIC_ORDER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GoodsLogisticOrder.Add(key, key.GoodsLogisticOrderId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GoodsLogisticOrder(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGoodsLogisticOrder", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GoodsLogisticOrderPrimaryKey pk in hashKey_GoodsLogisticOrder.Keys)
			{
				sp["@GOODS_LOGISTIC_ORDER_ID"].Value = pk.GoodsLogisticOrderId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGoodsTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GoodsTypePrimaryKey key = new GoodsTypePrimaryKey();
							key.GoodsTypeId = (System.Int32)row["GOODS_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GoodsType.Add(key, key.GoodsTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GoodsType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGoodsType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GoodsTypePrimaryKey pk in hashKey_GoodsType.Keys)
			{
				sp["@GOODS_TYPE_ID"].Value = pk.GoodsTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGoodsTypeVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GoodsTypeVehiclePrimaryKey key = new GoodsTypeVehiclePrimaryKey();
							key.GoodsTypeVehicleId = (System.Int32)row["GOODS_TYPE_VEHICLE_id", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GoodsTypeVehicle.Add(key, key.GoodsTypeVehicleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GoodsTypeVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGoodsTypeVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GoodsTypeVehiclePrimaryKey pk in hashKey_GoodsTypeVehicle.Keys)
			{
				sp["@GOODS_TYPE_VEHICLE_id"].Value = pk.GoodsTypeVehicleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGraphicProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GraphicPrimaryKey key = new GraphicPrimaryKey();
							key.GraphicId = (System.Int32)row["GRAPHIC_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Graphic.Add(key, key.GraphicId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Graphic(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGraphic", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GraphicPrimaryKey pk in hashKey_Graphic.Keys)
			{
				sp["@GRAPHIC_ID"].Value = pk.GraphicId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGraphicShiftProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GraphicShiftPrimaryKey key = new GraphicShiftPrimaryKey();
							key.Id = (System.Int32)row["ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GraphicShift.Add(key, key.Id);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GraphicShift(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGraphicShift", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GraphicShiftPrimaryKey pk in hashKey_GraphicShift.Keys)
			{
				sp["@ID"].Value = pk.Id;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckGuardEventsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							GuardEventsPrimaryKey key = new GuardEventsPrimaryKey();
							key.GuardEventId = (System.Int32)row["GUARD_EVENT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_GuardEvents.Add(key, key.GuardEventId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_GuardEvents(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteGuardEvents", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (GuardEventsPrimaryKey pk in hashKey_GuardEvents.Keys)
			{
				sp["@GUARD_EVENT_ID"].Value = pk.GuardEventId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckJobDelbadxyProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							JobDelbadxyPrimaryKey key = new JobDelbadxyPrimaryKey();
							key.JobDelbadxyId = (System.Int32)row["Job_DelBadXY_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_JobDelbadxy.Add(key, key.JobDelbadxyId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_JobDelbadxy(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteJobDelbadxy", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (JobDelbadxyPrimaryKey pk in hashKey_JobDelbadxy.Keys)
			{
				sp["@Job_DelBadXY_ID"].Value = pk.JobDelbadxyId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckJournalProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							JournalPrimaryKey key = new JournalPrimaryKey();
							key.JournalId = (System.Int32)row["JOURNAL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Journal.Add(key, key.JournalId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Journal(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteJournal", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (JournalPrimaryKey pk in hashKey_Journal.Keys)
			{
				sp["@JOURNAL_ID"].Value = pk.JournalId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckJournalDayProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							JournalDayPrimaryKey key = new JournalDayPrimaryKey();
							key.JournalDayId = (System.Int32)row["JOURNAL_DAY_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_JournalDay.Add(key, key.JournalDayId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_JournalDay(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteJournalDay", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (JournalDayPrimaryKey pk in hashKey_JournalDay.Keys)
			{
				sp["@JOURNAL_DAY_ID"].Value = pk.JournalDayId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckJournalDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							JournalDriverPrimaryKey key = new JournalDriverPrimaryKey();
							key.JournalDriverId = (System.Int32)row["JOURNAL_DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_JournalDriver.Add(key, key.JournalDriverId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_JournalDriver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteJournalDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (JournalDriverPrimaryKey pk in hashKey_JournalDriver.Keys)
			{
				sp["@JOURNAL_DRIVER_ID"].Value = pk.JournalDriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckJournalVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							JournalVehiclePrimaryKey key = new JournalVehiclePrimaryKey();
							key.JournalVehicleId = (System.Int32)row["JOURNAL_VEHICLE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_JournalVehicle.Add(key, key.JournalVehicleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_JournalVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteJournalVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (JournalVehiclePrimaryKey pk in hashKey_JournalVehicle.Keys)
			{
				sp["@JOURNAL_VEHICLE_ID"].Value = pk.JournalVehicleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckLogOpsEventsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							LogOpsEventsPrimaryKey key = new LogOpsEventsPrimaryKey();
							key.RecordId = (System.Int32)row["RECORD_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_LogOpsEvents.Add(key, key.RecordId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_LogOpsEvents(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteLogOpsEvents", connection, transaction);
			foreach (LogOpsEventsPrimaryKey pk in hashKey_LogOpsEvents.Keys)
			{
				sp["@RECORD_ID"].Value = pk.RecordId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckLogVehicleStatusProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							LogVehicleStatusPrimaryKey key = new LogVehicleStatusPrimaryKey();
							key.LogVehicleStatusId = (System.Int32)row["LOG_VEHICLE_STATUS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_LogVehicleStatus.Add(key, key.LogVehicleStatusId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_LogVehicleStatus(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteLogVehicleStatus", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (LogVehicleStatusPrimaryKey pk in hashKey_LogVehicleStatus.Keys)
			{
				sp["@LOG_VEHICLE_STATUS_ID"].Value = pk.LogVehicleStatusId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckLogWaybillHeaderStatusProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							LogWaybillHeaderStatusPrimaryKey key = new LogWaybillHeaderStatusPrimaryKey();
							key.LogWaybillHeaderStatusId = (System.Int32)row["LOG_WAYBILL_HEADER_STATUS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_LogWaybillHeaderStatus.Add(key, key.LogWaybillHeaderStatusId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_LogWaybillHeaderStatus(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteLogWaybillHeaderStatus", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (LogWaybillHeaderStatusPrimaryKey pk in hashKey_LogWaybillHeaderStatus.Keys)
			{
				sp["@LOG_WAYBILL_HEADER_STATUS_ID"].Value = pk.LogWaybillHeaderStatusId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckLogisticAddressProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							LogisticAddressPrimaryKey key = new LogisticAddressPrimaryKey();
							key.LogisticAddressId = (System.Int32)row["LOGISTIC_ADDRESS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_LogisticAddress.Add(key, key.LogisticAddressId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_LogisticAddress(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteLogisticAddress", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (LogisticAddressPrimaryKey pk in hashKey_LogisticAddress.Keys)
			{
				sp["@LOGISTIC_ADDRESS_ID"].Value = pk.LogisticAddressId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckLogisticOrderProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							LogisticOrderPrimaryKey key = new LogisticOrderPrimaryKey();
							key.LogisticOrderId = (System.Int32)row["LOGISTIC_ORDER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_LogisticOrder.Add(key, key.LogisticOrderId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_LogisticOrder(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteLogisticOrder", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (LogisticOrderPrimaryKey pk in hashKey_LogisticOrder.Keys)
			{
				sp["@LOGISTIC_ORDER_ID"].Value = pk.LogisticOrderId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMapVertexProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MapVertexPrimaryKey key = new MapVertexPrimaryKey();
							key.VertexId = (System.Int32)row["VERTEX_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MapVertex.Add(key, key.VertexId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MapVertex(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMapVertex", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MapVertexPrimaryKey pk in hashKey_MapVertex.Keys)
			{
				sp["@VERTEX_ID"].Value = pk.VertexId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMapsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MapsPrimaryKey key = new MapsPrimaryKey();
							key.MapId = (System.Int32)row["MAP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Maps.Add(key, key.MapId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Maps(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMaps", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MapsPrimaryKey pk in hashKey_Maps.Keys)
			{
				sp["@MAP_ID"].Value = pk.MapId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMediaProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaPrimaryKey key = new MediaPrimaryKey();
							key.MediaId = (System.Int32)row["MEDIA_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Media.Add(key, key.MediaId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Media(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMedia", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaPrimaryKey pk in hashKey_Media.Keys)
			{
				sp["@MEDIA_ID"].Value = pk.MediaId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMediaAcceptorsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaAcceptorsPrimaryKey key = new MediaAcceptorsPrimaryKey();
							key.MaId = (System.Int32)row["MA_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MediaAcceptors.Add(key, key.MaId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MediaAcceptors(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMediaAcceptors", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaAcceptorsPrimaryKey pk in hashKey_MediaAcceptors.Keys)
			{
				sp["@MA_ID"].Value = pk.MaId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMediaTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaTypePrimaryKey key = new MediaTypePrimaryKey();
							key.Id = (System.Byte)row["ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MediaType.Add(key, key.Id);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MediaType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMediaType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaTypePrimaryKey pk in hashKey_MediaType.Keys)
			{
				sp["@ID"].Value = pk.Id;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessagePrimaryKey key = new MessagePrimaryKey();
							key.MessageId = (System.Int32)row["MESSAGE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Message.Add(key, key.MessageId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Message(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessage", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessagePrimaryKey pk in hashKey_Message.Keys)
			{
				sp["@MESSAGE_ID"].Value = pk.MessageId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageBoardProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessageBoardPrimaryKey key = new MessageBoardPrimaryKey();
							key.Id = (System.Int32)row["ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MessageBoard.Add(key, key.Id);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MessageBoard(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessageBoard", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessageBoardPrimaryKey pk in hashKey_MessageBoard.Keys)
			{
				sp["@ID"].Value = pk.Id;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageFieldProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessageFieldPrimaryKey key = new MessageFieldPrimaryKey();
							key.MessageFieldId = (System.Int32)row["MESSAGE_FIELD_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MessageField.Add(key, key.MessageFieldId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MessageField(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessageField", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessageFieldPrimaryKey pk in hashKey_MessageField.Keys)
			{
				sp["@MESSAGE_FIELD_ID"].Value = pk.MessageFieldId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageOperatorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessageOperatorPrimaryKey key = new MessageOperatorPrimaryKey();
							key.MessageOperatorId = (System.Int32)row["MESSAGE_OPERATOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MessageOperator.Add(key, key.MessageOperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MessageOperator(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessageOperator", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessageOperatorPrimaryKey pk in hashKey_MessageOperator.Keys)
			{
				sp["@MESSAGE_OPERATOR_ID"].Value = pk.MessageOperatorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageTemplateProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessageTemplatePrimaryKey key = new MessageTemplatePrimaryKey();
							key.MessageTemplateId = (System.Int32)row["MESSAGE_TEMPLATE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MessageTemplate.Add(key, key.MessageTemplateId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MessageTemplate(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessageTemplate", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessageTemplatePrimaryKey pk in hashKey_MessageTemplate.Keys)
			{
				sp["@MESSAGE_TEMPLATE_ID"].Value = pk.MessageTemplateId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckMessageTemplateFieldProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MessageTemplateFieldPrimaryKey key = new MessageTemplateFieldPrimaryKey();
							key.MessageTemplateFieldId = (System.Int32)row["MESSAGE_TEMPLATE_FIELD_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MessageTemplateField.Add(key, key.MessageTemplateFieldId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_MessageTemplateField(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMessageTemplateField", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MessageTemplateFieldPrimaryKey pk in hashKey_MessageTemplateField.Keys)
			{
				sp["@MESSAGE_TEMPLATE_FIELD_ID"].Value = pk.MessageTemplateFieldId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorPrimaryKey key = new OperatorPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Operator.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperator", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorPrimaryKey pk in hashKey_Operator.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorDepartmentProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorDepartmentPrimaryKey key = new OperatorDepartmentPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.DepartmentId = (System.Int32)row["DEPARTMENT_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorDepartment.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorDepartment(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorDepartment", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorDepartmentPrimaryKey pk in hashKey_OperatorDepartment.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@DEPARTMENT_ID"].Value = pk.DepartmentId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorDriverPrimaryKey key = new OperatorDriverPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorDriver.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorDriver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorDriverPrimaryKey pk in hashKey_OperatorDriver.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorDrivergroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorDrivergroupPrimaryKey key = new OperatorDrivergroupPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.DrivergroupId = (System.Int32)row["DRIVERGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorDrivergroup.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorDrivergroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorDrivergroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorDrivergroupPrimaryKey pk in hashKey_OperatorDrivergroup.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@DRIVERGROUP_ID"].Value = pk.DrivergroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorProfileProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorProfilePrimaryKey key = new OperatorProfilePrimaryKey();
							key.OperatorProfileId = (System.Int32)row["OPERATOR_PROFILE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorProfile.Add(key, key.OperatorProfileId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorProfile(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorProfile", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorProfilePrimaryKey pk in hashKey_OperatorProfile.Keys)
			{
				sp["@OPERATOR_PROFILE_ID"].Value = pk.OperatorProfileId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorReportProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorReportPrimaryKey key = new OperatorReportPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.ReportId = (System.Int32)row["REPORT_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorReport.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorReport(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorReport", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorReportPrimaryKey pk in hashKey_OperatorReport.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@REPORT_ID"].Value = pk.ReportId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorRouteProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorRoutePrimaryKey key = new OperatorRoutePrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.RouteId = (System.Int32)row["ROUTE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorRoute.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorRoute(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorRoute", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorRoutePrimaryKey pk in hashKey_OperatorRoute.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@ROUTE_ID"].Value = pk.RouteId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorRoutegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorRoutegroupPrimaryKey key = new OperatorRoutegroupPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.RoutegroupId = (System.Int32)row["ROUTEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorRoutegroup.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorRoutegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorRoutegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorRoutegroupPrimaryKey pk in hashKey_OperatorRoutegroup.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@ROUTEGROUP_ID"].Value = pk.RoutegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorVehiclePrimaryKey key = new OperatorVehiclePrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.VehicleId = (System.Int32)row["VEHICLE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorVehicle.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorVehiclePrimaryKey pk in hashKey_OperatorVehicle.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@VEHICLE_ID"].Value = pk.VehicleId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorVehiclegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorVehiclegroupPrimaryKey key = new OperatorVehiclegroupPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.VehiclegroupId = (System.Int32)row["VEHICLEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorVehiclegroup.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorVehiclegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorVehiclegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorVehiclegroupPrimaryKey pk in hashKey_OperatorVehiclegroup.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@VEHICLEGROUP_ID"].Value = pk.VehiclegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorZoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorZonePrimaryKey key = new OperatorZonePrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.ZoneId = (System.Int32)row["ZONE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorZone.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorZone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorZone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorZonePrimaryKey pk in hashKey_OperatorZone.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@ZONE_ID"].Value = pk.ZoneId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorZonegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorZonegroupPrimaryKey key = new OperatorZonegroupPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							key.ZonegroupId = (System.Int32)row["ZONEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorZonegroup.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorZonegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorZonegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorZonegroupPrimaryKey pk in hashKey_OperatorZonegroup.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp["@ZONEGROUP_ID"].Value = pk.ZonegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupPrimaryKey key = new OperatorgroupPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Operatorgroup.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Operatorgroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupPrimaryKey pk in hashKey_Operatorgroup.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupDepartmentProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupDepartmentPrimaryKey key = new OperatorgroupDepartmentPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.DepartmentId = (System.Int32)row["DEPARTMENT_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupDepartment.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupDepartment(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupDepartment", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupDepartmentPrimaryKey pk in hashKey_OperatorgroupDepartment.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@DEPARTMENT_ID"].Value = pk.DepartmentId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupDriverProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupDriverPrimaryKey key = new OperatorgroupDriverPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupDriver.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupDriver(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupDriver", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupDriverPrimaryKey pk in hashKey_OperatorgroupDriver.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupDrivergroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupDrivergroupPrimaryKey key = new OperatorgroupDrivergroupPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.DrivergroupId = (System.Int32)row["DRIVERGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupDrivergroup.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupDrivergroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupDrivergroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupDrivergroupPrimaryKey pk in hashKey_OperatorgroupDrivergroup.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@DRIVERGROUP_ID"].Value = pk.DrivergroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupOperatorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupOperatorPrimaryKey key = new OperatorgroupOperatorPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupOperator.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupOperator(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupOperator", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupOperatorPrimaryKey pk in hashKey_OperatorgroupOperator.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupReportProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupReportPrimaryKey key = new OperatorgroupReportPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.ReportId = (System.Int32)row["REPORT_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupReport.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupReport(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupReport", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupReportPrimaryKey pk in hashKey_OperatorgroupReport.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@REPORT_ID"].Value = pk.ReportId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupRouteProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupRoutePrimaryKey key = new OperatorgroupRoutePrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.RouteId = (System.Int32)row["ROUTE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupRoute.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupRoute(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupRoute", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupRoutePrimaryKey pk in hashKey_OperatorgroupRoute.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@ROUTE_ID"].Value = pk.RouteId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupRoutegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupRoutegroupPrimaryKey key = new OperatorgroupRoutegroupPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.RoutegroupId = (System.Int32)row["ROUTEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupRoutegroup.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupRoutegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupRoutegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupRoutegroupPrimaryKey pk in hashKey_OperatorgroupRoutegroup.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@ROUTEGROUP_ID"].Value = pk.RoutegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupVehiclePrimaryKey key = new OperatorgroupVehiclePrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.VehicleId = (System.Int32)row["VEHICLE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupVehicle.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupVehiclePrimaryKey pk in hashKey_OperatorgroupVehicle.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@VEHICLE_ID"].Value = pk.VehicleId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupVehiclegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupVehiclegroupPrimaryKey key = new OperatorgroupVehiclegroupPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.VehiclegroupId = (System.Int32)row["VEHICLEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupVehiclegroup.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupVehiclegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupVehiclegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupVehiclegroupPrimaryKey pk in hashKey_OperatorgroupVehiclegroup.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@VEHICLEGROUP_ID"].Value = pk.VehiclegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupZoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupZonePrimaryKey key = new OperatorgroupZonePrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.ZoneId = (System.Int32)row["ZONE_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupZone.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupZone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupZone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupZonePrimaryKey pk in hashKey_OperatorgroupZone.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@ZONE_ID"].Value = pk.ZoneId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOperatorgroupZonegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorgroupZonegroupPrimaryKey key = new OperatorgroupZonegroupPrimaryKey();
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							key.ZonegroupId = (System.Int32)row["ZONEGROUP_ID", DataRowVersion.Original];
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OperatorgroupZonegroup.Add(key, key.OperatorgroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OperatorgroupZonegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperatorgroupZonegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorgroupZonegroupPrimaryKey pk in hashKey_OperatorgroupZonegroup.Keys)
			{
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp["@ZONEGROUP_ID"].Value = pk.ZonegroupId;
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOpsEventProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OpsEventPrimaryKey key = new OpsEventPrimaryKey();
							key.OpsEventId = (System.Int32)row["OPS_EVENT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OpsEvent.Add(key, key.OpsEventId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OpsEvent(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOpsEvent", connection, transaction);
			foreach (OpsEventPrimaryKey pk in hashKey_OpsEvent.Keys)
			{
				sp["@OPS_EVENT_ID"].Value = pk.OpsEventId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOrderProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OrderPrimaryKey key = new OrderPrimaryKey();
							key.OrderId = (System.Int32)row["ORDER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Order.Add(key, key.OrderId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Order(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOrder", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OrderPrimaryKey pk in hashKey_Order.Keys)
			{
				sp["@ORDER_ID"].Value = pk.OrderId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOrderTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OrderTripPrimaryKey key = new OrderTripPrimaryKey();
							key.OrderTripId = (System.Int32)row["ORDER_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OrderTrip.Add(key, key.OrderTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OrderTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOrderTrip", connection, transaction);
			foreach (OrderTripPrimaryKey pk in hashKey_OrderTrip.Keys)
			{
				sp["@ORDER_TRIP_ID"].Value = pk.OrderTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOrderTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OrderTypePrimaryKey key = new OrderTypePrimaryKey();
							key.OrderTypeId = (System.Int32)row["ORDER_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_OrderType.Add(key, key.OrderTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_OrderType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOrderType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OrderTypePrimaryKey pk in hashKey_OrderType.Keys)
			{
				sp["@ORDER_TYPE_ID"].Value = pk.OrderTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckOwnerProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OwnerPrimaryKey key = new OwnerPrimaryKey();
							key.OwnerId = (System.Int32)row["OWNER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Owner.Add(key, key.OwnerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Owner(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOwner", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OwnerPrimaryKey pk in hashKey_Owner.Keys)
			{
				sp["@OWNER_ID"].Value = pk.OwnerId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckPeriodProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							PeriodPrimaryKey key = new PeriodPrimaryKey();
							key.PeriodId = (System.Int32)row["PERIOD_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Period.Add(key, key.PeriodId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Period(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeletePeriod", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (PeriodPrimaryKey pk in hashKey_Period.Keys)
			{
				sp["@PERIOD_ID"].Value = pk.PeriodId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckPointProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							PointPrimaryKey key = new PointPrimaryKey();
							key.PointId = (System.Int32)row["POINT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Point.Add(key, key.PointId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Point(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeletePoint", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (PointPrimaryKey pk in hashKey_Point.Keys)
			{
				sp["@POINT_ID"].Value = pk.PointId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckPointKindProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							PointKindPrimaryKey key = new PointKindPrimaryKey();
							key.PointKindId = (System.Int32)row["POINT_KIND_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_PointKind.Add(key, key.PointKindId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_PointKind(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeletePointKind", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (PointKindPrimaryKey pk in hashKey_PointKind.Keys)
			{
				sp["@POINT_KIND_ID"].Value = pk.PointKindId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckReportProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ReportPrimaryKey key = new ReportPrimaryKey();
							key.ReportId = (System.Int32)row["REPORT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Report.Add(key, key.ReportId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Report(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteReport", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ReportPrimaryKey pk in hashKey_Report.Keys)
			{
				sp["@REPORT_ID"].Value = pk.ReportId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckReprintReasonProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ReprintReasonPrimaryKey key = new ReprintReasonPrimaryKey();
							key.ReprintReasonId = (System.Int32)row["REPRINT_REASON_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ReprintReason.Add(key, key.ReprintReasonId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ReprintReason(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteReprintReason", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ReprintReasonPrimaryKey pk in hashKey_ReprintReason.Keys)
			{
				sp["@REPRINT_REASON_ID"].Value = pk.ReprintReasonId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRightProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RightPrimaryKey key = new RightPrimaryKey();
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Right.Add(key, key.RightId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Right(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRight", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RightPrimaryKey pk in hashKey_Right.Keys)
			{
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRightOperatorProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RightOperatorPrimaryKey key = new RightOperatorPrimaryKey();
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RightOperator.Add(key, key.RightId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RightOperator(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRightOperator", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RightOperatorPrimaryKey pk in hashKey_RightOperator.Keys)
			{
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRightOperatorgroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RightOperatorgroupPrimaryKey key = new RightOperatorgroupPrimaryKey();
							key.RightId = (System.Int32)row["RIGHT_ID", DataRowVersion.Original];
							key.OperatorgroupId = (System.Int32)row["OPERATORGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RightOperatorgroup.Add(key, key.RightId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RightOperatorgroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRightOperatorgroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RightOperatorgroupPrimaryKey pk in hashKey_RightOperatorgroup.Keys)
			{
				sp["@RIGHT_ID"].Value = pk.RightId;
				sp["@OPERATORGROUP_ID"].Value = pk.OperatorgroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRouteProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RoutePrimaryKey key = new RoutePrimaryKey();
							key.RouteId = (System.Int32)row["ROUTE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Route.Add(key, key.RouteId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Route(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRoute", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RoutePrimaryKey pk in hashKey_Route.Keys)
			{
				sp["@ROUTE_ID"].Value = pk.RouteId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRouteGeozoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RouteGeozonePrimaryKey key = new RouteGeozonePrimaryKey();
							key.RouteGeozoneId = (System.Int32)row["ROUTE_GEOZONE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RouteGeozone.Add(key, key.RouteGeozoneId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RouteGeozone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRouteGeozone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RouteGeozonePrimaryKey pk in hashKey_RouteGeozone.Keys)
			{
				sp["@ROUTE_GEOZONE_ID"].Value = pk.RouteGeozoneId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRoutePointProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RoutePointPrimaryKey key = new RoutePointPrimaryKey();
							key.RoutePointId = (System.Int32)row["ROUTE_POINT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RoutePoint.Add(key, key.RoutePointId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RoutePoint(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRoutePoint", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RoutePointPrimaryKey pk in hashKey_RoutePoint.Keys)
			{
				sp["@ROUTE_POINT_ID"].Value = pk.RoutePointId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRouteTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RouteTripPrimaryKey key = new RouteTripPrimaryKey();
							key.RouteTripId = (System.Int32)row["ROUTE_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RouteTrip.Add(key, key.RouteTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RouteTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRouteTrip", connection, transaction);
			foreach (RouteTripPrimaryKey pk in hashKey_RouteTrip.Keys)
			{
				sp["@ROUTE_TRIP_ID"].Value = pk.RouteTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRoutegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RoutegroupPrimaryKey key = new RoutegroupPrimaryKey();
							key.RoutegroupId = (System.Int32)row["ROUTEGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Routegroup.Add(key, key.RoutegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Routegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRoutegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RoutegroupPrimaryKey pk in hashKey_Routegroup.Keys)
			{
				sp["@ROUTEGROUP_ID"].Value = pk.RoutegroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRoutegroupRouteProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RoutegroupRoutePrimaryKey key = new RoutegroupRoutePrimaryKey();
							key.RoutegroupId = (System.Int32)row["ROUTEGROUP_ID", DataRowVersion.Original];
							key.RouteId = (System.Int32)row["ROUTE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RoutegroupRoute.Add(key, key.RoutegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RoutegroupRoute(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRoutegroupRoute", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RoutegroupRoutePrimaryKey pk in hashKey_RoutegroupRoute.Keys)
			{
				sp["@ROUTEGROUP_ID"].Value = pk.RoutegroupId;
				sp["@ROUTE_ID"].Value = pk.RouteId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsPrimaryKey key = new RsPrimaryKey();
							key.RsId = (System.Int32)row["RS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Rs.Add(key, key.RsId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Rs(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRs", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsPrimaryKey pk in hashKey_Rs.Keys)
			{
				sp["@RS_ID"].Value = pk.RsId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsNumberProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsNumberPrimaryKey key = new RsNumberPrimaryKey();
							key.RsNumberId = (System.Int32)row["RS_NUMBER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsNumber.Add(key, key.RsNumberId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsNumber(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsNumber", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsNumberPrimaryKey pk in hashKey_RsNumber.Keys)
			{
				sp["@RS_NUMBER_ID"].Value = pk.RsNumberId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsOperationstypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsOperationstypePrimaryKey key = new RsOperationstypePrimaryKey();
							key.RsOperationstypeId = (System.Int32)row["RS_OPERATIONSTYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsOperationstype.Add(key, key.RsOperationstypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsOperationstype(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsOperationstype", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsOperationstypePrimaryKey pk in hashKey_RsOperationstype.Keys)
			{
				sp["@RS_OPERATIONSTYPE_ID"].Value = pk.RsOperationstypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsOperationtypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey();
							key.RsOperationtypeId = (System.Int32)row["RS_OPERATIONTYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsOperationtype(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsOperationtype", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsOperationtypePrimaryKey pk in hashKey_RsOperationtype.Keys)
			{
				sp["@RS_OPERATIONTYPE_ID"].Value = pk.RsOperationtypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsPeriodProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsPeriodPrimaryKey key = new RsPeriodPrimaryKey();
							key.PeriodId = (System.Int32)row["PERIOD_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsPeriod.Add(key, key.PeriodId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsPeriod(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsPeriod", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsPeriodPrimaryKey pk in hashKey_RsPeriod.Keys)
			{
				sp["@PERIOD_ID"].Value = pk.PeriodId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsPointProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsPointPrimaryKey key = new RsPointPrimaryKey();
							key.RsPointId = (System.Int32)row["RS_POINT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsPoint.Add(key, key.RsPointId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsPoint(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsPoint", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsPointPrimaryKey pk in hashKey_RsPoint.Keys)
			{
				sp["@RS_POINT_ID"].Value = pk.RsPointId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsRoundTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsRoundTripPrimaryKey key = new RsRoundTripPrimaryKey();
							key.RsRoundTripId = (System.Int32)row["RS_ROUND_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsRoundTrip.Add(key, key.RsRoundTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsRoundTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsRoundTrip", connection, transaction);
			foreach (RsRoundTripPrimaryKey pk in hashKey_RsRoundTrip.Keys)
			{
				sp["@RS_ROUND_TRIP_ID"].Value = pk.RsRoundTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsRuntimeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsRuntimePrimaryKey key = new RsRuntimePrimaryKey();
							key.RsRuntimeId = (System.Int32)row["RS_RUNTIME_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsRuntime.Add(key, key.RsRuntimeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsRuntime(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsRuntime", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsRuntimePrimaryKey pk in hashKey_RsRuntime.Keys)
			{
				sp["@RS_RUNTIME_ID"].Value = pk.RsRuntimeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsShiftProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsShiftPrimaryKey key = new RsShiftPrimaryKey();
							key.RsShiftId = (System.Int32)row["RS_SHIFT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsShift.Add(key, key.RsShiftId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsShift(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsShift", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsShiftPrimaryKey pk in hashKey_RsShift.Keys)
			{
				sp["@RS_SHIFT_ID"].Value = pk.RsShiftId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsStepProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsStepPrimaryKey key = new RsStepPrimaryKey();
							key.RsStepId = (System.Int32)row["RS_STEP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsStep.Add(key, key.RsStepId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsStep(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsStep", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsStepPrimaryKey pk in hashKey_RsStep.Keys)
			{
				sp["@RS_STEP_ID"].Value = pk.RsStepId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsTripPrimaryKey key = new RsTripPrimaryKey();
							key.RsTripId = (System.Int32)row["RS_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsTrip.Add(key, key.RsTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsTrip", connection, transaction);
			foreach (RsTripPrimaryKey pk in hashKey_RsTrip.Keys)
			{
				sp["@RS_TRIP_ID"].Value = pk.RsTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsTripstypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsTripstypePrimaryKey key = new RsTripstypePrimaryKey();
							key.RsTripstypeId = (System.Int32)row["RS_TRIPSTYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsTripstype.Add(key, key.RsTripstypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsTripstype(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsTripstype", connection, transaction);
			foreach (RsTripstypePrimaryKey pk in hashKey_RsTripstype.Keys)
			{
				sp["@RS_TRIPSTYPE_ID"].Value = pk.RsTripstypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsTriptypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsTriptypePrimaryKey key = new RsTriptypePrimaryKey();
							key.RsTriptypeId = (System.Int32)row["RS_TRIPTYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsTriptype.Add(key, key.RsTriptypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsTriptype(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsTriptype", connection, transaction);
			foreach (RsTriptypePrimaryKey pk in hashKey_RsTriptype.Keys)
			{
				sp["@RS_TRIPTYPE_ID"].Value = pk.RsTriptypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsTypePrimaryKey key = new RsTypePrimaryKey();
							key.RsTypeId = (System.Int32)row["RS_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsType.Add(key, key.RsTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsTypePrimaryKey pk in hashKey_RsType.Keys)
			{
				sp["@RS_TYPE_ID"].Value = pk.RsTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsVariantProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsVariantPrimaryKey key = new RsVariantPrimaryKey();
							key.RsVariantId = (System.Int32)row["RS_VARIANT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsVariant.Add(key, key.RsVariantId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsVariant(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsVariant", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsVariantPrimaryKey pk in hashKey_RsVariant.Keys)
			{
				sp["@RS_VARIANT_ID"].Value = pk.RsVariantId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRsWayoutProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RsWayoutPrimaryKey key = new RsWayoutPrimaryKey();
							key.RsWayoutId = (System.Int32)row["RS_WAYOUT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_RsWayout.Add(key, key.RsWayoutId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_RsWayout(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRsWayout", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RsWayoutPrimaryKey pk in hashKey_RsWayout.Keys)
			{
				sp["@RS_WAYOUT_ID"].Value = pk.RsWayoutId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckRuleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							RulePrimaryKey key = new RulePrimaryKey();
							key.RuleId = (System.Int32)row["RULE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Rule.Add(key, key.RuleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Rule(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteRule", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (RulePrimaryKey pk in hashKey_Rule.Keys)
			{
				sp["@RULE_ID"].Value = pk.RuleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckScheduleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SchedulePrimaryKey key = new SchedulePrimaryKey();
							key.ScheduleId = (System.Int32)row["SCHEDULE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Schedule.Add(key, key.ScheduleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Schedule(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSchedule", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SchedulePrimaryKey pk in hashKey_Schedule.Keys)
			{
				sp["@SCHEDULE_ID"].Value = pk.ScheduleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckScheduleDetailProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ScheduleDetailPrimaryKey key = new ScheduleDetailPrimaryKey();
							key.ScheduleDetailId = (System.Int32)row["SCHEDULE_DETAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ScheduleDetail.Add(key, key.ScheduleDetailId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ScheduleDetail(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteScheduleDetail", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ScheduleDetailPrimaryKey pk in hashKey_ScheduleDetail.Keys)
			{
				sp["@SCHEDULE_DETAIL_ID"].Value = pk.ScheduleDetailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckScheduleDetailInfoProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ScheduleDetailInfoPrimaryKey key = new ScheduleDetailInfoPrimaryKey();
							key.ScheduleDetailId = (System.Int32)row["SCHEDULE_DETAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ScheduleDetailInfo.Add(key, key.ScheduleDetailId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ScheduleDetailInfo(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteScheduleDetailInfo", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ScheduleDetailInfoPrimaryKey pk in hashKey_ScheduleDetailInfo.Keys)
			{
				sp["@SCHEDULE_DETAIL_ID"].Value = pk.ScheduleDetailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckScheduleGeozoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ScheduleGeozonePrimaryKey key = new ScheduleGeozonePrimaryKey();
							key.ScheduleGeozoneId = (System.Int32)row["SCHEDULE_GEOZONE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ScheduleGeozone.Add(key, key.ScheduleGeozoneId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ScheduleGeozone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteScheduleGeozone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ScheduleGeozonePrimaryKey pk in hashKey_ScheduleGeozone.Keys)
			{
				sp["@SCHEDULE_GEOZONE_ID"].Value = pk.ScheduleGeozoneId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSchedulePassageProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SchedulePassagePrimaryKey key = new SchedulePassagePrimaryKey();
							key.WhId = (System.Int32)row["WH_ID", DataRowVersion.Original];
							key.SpId = (System.Int32)row["SP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_SchedulePassage.Add(key, key.WhId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_SchedulePassage(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSchedulePassage", connection, transaction);
			foreach (SchedulePassagePrimaryKey pk in hashKey_SchedulePassage.Keys)
			{
				sp["@WH_ID"].Value = pk.WhId;
				sp["@SP_ID"].Value = pk.SpId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSchedulePointProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SchedulePointPrimaryKey key = new SchedulePointPrimaryKey();
							key.SchedulePointId = (System.Int32)row["SCHEDULE_POINT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_SchedulePoint.Add(key, key.SchedulePointId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_SchedulePoint(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSchedulePoint", connection, transaction);
			foreach (SchedulePointPrimaryKey pk in hashKey_SchedulePoint.Keys)
			{
				sp["@SCHEDULE_POINT_ID"].Value = pk.SchedulePointId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSchedulereventProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SchedulereventPrimaryKey key = new SchedulereventPrimaryKey();
							key.SchedulereventId = (System.Int32)row["SCHEDULEREVENT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Schedulerevent.Add(key, key.SchedulereventId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Schedulerevent(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSchedulerevent", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SchedulereventPrimaryKey pk in hashKey_Schedulerevent.Keys)
			{
				sp["@SCHEDULEREVENT_ID"].Value = pk.SchedulereventId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSchedulerqueueProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SchedulerqueuePrimaryKey key = new SchedulerqueuePrimaryKey();
							key.SchedulerqueueId = (System.Int32)row["SCHEDULERQUEUE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Schedulerqueue.Add(key, key.SchedulerqueueId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Schedulerqueue(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSchedulerqueue", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SchedulerqueuePrimaryKey pk in hashKey_Schedulerqueue.Keys)
			{
				sp["@SCHEDULERQUEUE_ID"].Value = pk.SchedulerqueueId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSeasonProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SeasonPrimaryKey key = new SeasonPrimaryKey();
							key.SeasonId = (System.Int32)row["SEASON_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Season.Add(key, key.SeasonId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Season(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSeason", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SeasonPrimaryKey pk in hashKey_Season.Keys)
			{
				sp["@SEASON_ID"].Value = pk.SeasonId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSeattypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SeattypePrimaryKey key = new SeattypePrimaryKey();
							key.SeattypeId = (System.Int32)row["SEATTYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Seattype.Add(key, key.SeattypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Seattype(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSeattype", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SeattypePrimaryKey pk in hashKey_Seattype.Keys)
			{
				sp["@SEATTYPE_ID"].Value = pk.SeattypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSessionProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SessionPrimaryKey key = new SessionPrimaryKey();
							key.SessionId = (System.Int32)row["SESSION_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Session.Add(key, key.SessionId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Session(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSession", connection, transaction);
			foreach (SessionPrimaryKey pk in hashKey_Session.Keys)
			{
				sp["@SESSION_ID"].Value = pk.SessionId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSmsTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SmsTypePrimaryKey key = new SmsTypePrimaryKey();
							key.SmsTypeId = (System.Int32)row["SMS_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_SmsType.Add(key, key.SmsTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_SmsType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSmsType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SmsTypePrimaryKey pk in hashKey_SmsType.Keys)
			{
				sp["@SMS_TYPE_ID"].Value = pk.SmsTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckSysdiagramsProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SysdiagramsPrimaryKey key = new SysdiagramsPrimaryKey();
							key.DiagramId = (System.Int32)row["diagram_id", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Sysdiagrams.Add(key, key.DiagramId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Sysdiagrams(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSysdiagrams", connection, transaction);
			foreach (SysdiagramsPrimaryKey pk in hashKey_Sysdiagrams.Keys)
			{
				sp["@diagram_id"].Value = pk.DiagramId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckTaskProcessorLogProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							TaskProcessorLogPrimaryKey key = new TaskProcessorLogPrimaryKey();
							key.TaskProcessorLogId = (System.Int32)row["TASK_PROCESSOR_LOG_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_TaskProcessorLog.Add(key, key.TaskProcessorLogId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_TaskProcessorLog(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteTaskProcessorLog", connection, transaction);
			foreach (TaskProcessorLogPrimaryKey pk in hashKey_TaskProcessorLog.Keys)
			{
				sp["@TASK_PROCESSOR_LOG_ID"].Value = pk.TaskProcessorLogId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckTrailProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							TrailPrimaryKey key = new TrailPrimaryKey();
							key.TrailId = (System.Int32)row["TRAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Trail.Add(key, key.TrailId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Trail(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteTrail", connection, transaction);
			foreach (TrailPrimaryKey pk in hashKey_Trail.Keys)
			{
				sp["@TRAIL_ID"].Value = pk.TrailId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							TripPrimaryKey key = new TripPrimaryKey();
							key.TripId = (System.Int32)row["TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Trip.Add(key, key.TripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Trip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteTrip", connection, transaction);
			foreach (TripPrimaryKey pk in hashKey_Trip.Keys)
			{
				sp["@TRIP_ID"].Value = pk.TripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckTripKindProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							TripKindPrimaryKey key = new TripKindPrimaryKey();
							key.TripKindId = (System.Int32)row["TRIP_KIND_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_TripKind.Add(key, key.TripKindId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_TripKind(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteTripKind", connection, transaction);
			foreach (TripKindPrimaryKey pk in hashKey_TripKind.Keys)
			{
				sp["@TRIP_KIND_ID"].Value = pk.TripKindId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclePrimaryKey key = new VehiclePrimaryKey();
							key.VehicleId = (System.Int32)row["VEHICLE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Vehicle.Add(key, key.VehicleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehiclePrimaryKey pk in hashKey_Vehicle.Keys)
			{
				sp["@VEHICLE_ID"].Value = pk.VehicleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehicleKindProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehicleKindPrimaryKey key = new VehicleKindPrimaryKey();
							key.VehicleKindId = (System.Int32)row["VEHICLE_KIND_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehicleKind.Add(key, key.VehicleKindId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehicleKind(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicleKind", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehicleKindPrimaryKey pk in hashKey_VehicleKind.Keys)
			{
				sp["@VEHICLE_KIND_ID"].Value = pk.VehicleKindId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehicleOwnerProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehicleOwnerPrimaryKey key = new VehicleOwnerPrimaryKey();
							key.VehicleOwnerId = (System.Int32)row["VEHICLE_OWNER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehicleOwner.Add(key, key.VehicleOwnerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehicleOwner(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicleOwner", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehicleOwnerPrimaryKey pk in hashKey_VehicleOwner.Keys)
			{
				sp["@VEHICLE_OWNER_ID"].Value = pk.VehicleOwnerId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehiclePictureProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclePicturePrimaryKey key = new VehiclePicturePrimaryKey();
							key.VehiclePictureId = (System.Int32)row["VEHICLE_PICTURE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehiclePicture.Add(key, key.VehiclePictureId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehiclePicture(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehiclePicture", connection, transaction);
			foreach (VehiclePicturePrimaryKey pk in hashKey_VehiclePicture.Keys)
			{
				sp["@VEHICLE_PICTURE_ID"].Value = pk.VehiclePictureId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehicleRuleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehicleRulePrimaryKey key = new VehicleRulePrimaryKey();
							key.VehicleRuleId = (System.Int32)row["VEHICLE_RULE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehicleRule.Add(key, key.VehicleRuleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehicleRule(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicleRule", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehicleRulePrimaryKey pk in hashKey_VehicleRule.Keys)
			{
				sp["@VEHICLE_RULE_ID"].Value = pk.VehicleRuleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehicleStatusProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehicleStatusPrimaryKey key = new VehicleStatusPrimaryKey();
							key.VehicleStatusId = (System.Int32)row["VEHICLE_STATUS_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehicleStatus.Add(key, key.VehicleStatusId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehicleStatus(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicleStatus", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehicleStatusPrimaryKey pk in hashKey_VehicleStatus.Keys)
			{
				sp["@VEHICLE_STATUS_ID"].Value = pk.VehicleStatusId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehiclegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclegroupPrimaryKey key = new VehiclegroupPrimaryKey();
							key.VehiclegroupId = (System.Int32)row["VEHICLEGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Vehiclegroup.Add(key, key.VehiclegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Vehiclegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehiclegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehiclegroupPrimaryKey pk in hashKey_Vehiclegroup.Keys)
			{
				sp["@VEHICLEGROUP_ID"].Value = pk.VehiclegroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehiclegroupRuleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclegroupRulePrimaryKey key = new VehiclegroupRulePrimaryKey();
							key.VehiclegroupRuleId = (System.Int32)row["VEHICLEGROUP_RULE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehiclegroupRule.Add(key, key.VehiclegroupRuleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehiclegroupRule(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehiclegroupRule", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehiclegroupRulePrimaryKey pk in hashKey_VehiclegroupRule.Keys)
			{
				sp["@VEHICLEGROUP_RULE_ID"].Value = pk.VehiclegroupRuleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckVehiclegroupVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclegroupVehiclePrimaryKey key = new VehiclegroupVehiclePrimaryKey();
							key.VehiclegroupId = (System.Int32)row["VEHICLEGROUP_ID", DataRowVersion.Original];
							key.VehicleId = (System.Int32)row["VEHICLE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_VehiclegroupVehicle.Add(key, key.VehiclegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_VehiclegroupVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehiclegroupVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehiclegroupVehiclePrimaryKey pk in hashKey_VehiclegroupVehicle.Keys)
			{
				sp["@VEHICLEGROUP_ID"].Value = pk.VehiclegroupId;
				sp["@VEHICLE_ID"].Value = pk.VehicleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWaybillProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WaybillPrimaryKey key = new WaybillPrimaryKey();
							key.WaybillId = (System.Int32)row["WAYBILL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Waybill.Add(key, key.WaybillId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Waybill(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWaybill", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WaybillPrimaryKey pk in hashKey_Waybill.Keys)
			{
				sp["@WAYBILL_ID"].Value = pk.WaybillId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWaybillHeaderProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey();
							key.WaybillHeaderId = (System.Int32)row["WAYBILL_HEADER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WaybillHeader(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWaybillHeader", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WaybillHeaderPrimaryKey pk in hashKey_WaybillHeader.Keys)
			{
				sp["@WAYBILL_HEADER_ID"].Value = pk.WaybillHeaderId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWaybillheaderWaybillmarkProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WaybillheaderWaybillmarkPrimaryKey key = new WaybillheaderWaybillmarkPrimaryKey();
							key.WaybillheaderId = (System.Int32)row["WAYBILLHEADER_ID", DataRowVersion.Original];
							key.WaybillmarkId = (System.Int32)row["WAYBILLMARK_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WaybillheaderWaybillmark.Add(key, key.WaybillheaderId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WaybillheaderWaybillmark(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWaybillheaderWaybillmark", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WaybillheaderWaybillmarkPrimaryKey pk in hashKey_WaybillheaderWaybillmark.Keys)
			{
				sp["@WAYBILLHEADER_ID"].Value = pk.WaybillheaderId;
				sp["@WAYBILLMARK_ID"].Value = pk.WaybillmarkId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWaybillmarkProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WaybillmarkPrimaryKey key = new WaybillmarkPrimaryKey();
							key.WaybillmarkId = (System.Int32)row["WAYBILLMARK_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Waybillmark.Add(key, key.WaybillmarkId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Waybillmark(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWaybillmark", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WaybillmarkPrimaryKey pk in hashKey_Waybillmark.Keys)
			{
				sp["@WAYBILLMARK_ID"].Value = pk.WaybillmarkId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWayoutProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WayoutPrimaryKey key = new WayoutPrimaryKey();
							key.WayoutId = (System.Int32)row["WAYOUT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Wayout.Add(key, key.WayoutId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Wayout(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWayout", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WayoutPrimaryKey pk in hashKey_Wayout.Keys)
			{
				sp["@WAYOUT_ID"].Value = pk.WayoutId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWbTripProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WbTripPrimaryKey key = new WbTripPrimaryKey();
							key.WbTripId = (System.Int32)row["WB_TRIP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WbTrip.Add(key, key.WbTripId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WbTrip(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWbTrip", connection, transaction);
			foreach (WbTripPrimaryKey pk in hashKey_WbTrip.Keys)
			{
				sp["@WB_TRIP_ID"].Value = pk.WbTripId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWebLineProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WebLinePrimaryKey key = new WebLinePrimaryKey();
							key.LineId = (System.Int32)row["LINE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WebLine.Add(key, key.LineId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WebLine(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWebLine", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WebLinePrimaryKey pk in hashKey_WebLine.Keys)
			{
				sp["@LINE_ID"].Value = pk.LineId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWebLineVertexProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WebLineVertexPrimaryKey key = new WebLineVertexPrimaryKey();
							key.LineVertexId = (System.Int32)row["LINE_VERTEX_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WebLineVertex.Add(key, key.LineVertexId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WebLineVertex(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWebLineVertex", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WebLineVertexPrimaryKey pk in hashKey_WebLineVertex.Keys)
			{
				sp["@LINE_VERTEX_ID"].Value = pk.LineVertexId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWebPointProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WebPointPrimaryKey key = new WebPointPrimaryKey();
							key.PointId = (System.Int32)row["POINT_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WebPoint.Add(key, key.PointId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WebPoint(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWebPoint", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WebPointPrimaryKey pk in hashKey_WebPoint.Keys)
			{
				sp["@POINT_ID"].Value = pk.PointId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWebPointTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WebPointTypePrimaryKey key = new WebPointTypePrimaryKey();
							key.TypeId = (System.Int32)row["TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WebPointType.Add(key, key.TypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WebPointType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWebPointType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WebPointTypePrimaryKey pk in hashKey_WebPointType.Keys)
			{
				sp["@TYPE_ID"].Value = pk.TypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWorkTimeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WorkTimePrimaryKey key = new WorkTimePrimaryKey();
							key.DriverId = (System.Int32)row["DRIVER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WorkTime.Add(key, key.DriverId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WorkTime(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWorkTime", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WorkTimePrimaryKey pk in hashKey_WorkTime.Keys)
			{
				sp["@DRIVER_ID"].Value = pk.DriverId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWorkplaceConfigProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WorkplaceConfigPrimaryKey key = new WorkplaceConfigPrimaryKey();
							key.Id = (System.Int32)row["ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WorkplaceConfig.Add(key, key.Id);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WorkplaceConfig(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWorkplaceConfig", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WorkplaceConfigPrimaryKey pk in hashKey_WorkplaceConfig.Keys)
			{
				sp["@ID"].Value = pk.Id;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWorkstationProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WorkstationPrimaryKey key = new WorkstationPrimaryKey();
							key.WorkstationId = (System.Int32)row["WORKSTATION_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Workstation.Add(key, key.WorkstationId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Workstation(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWorkstation", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WorkstationPrimaryKey pk in hashKey_Workstation.Keys)
			{
				sp["@WORKSTATION_ID"].Value = pk.WorkstationId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWorktimeReasonProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WorktimeReasonPrimaryKey key = new WorktimeReasonPrimaryKey();
							key.WorktimeReasonId = (System.Int32)row["WORKTIME_REASON_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WorktimeReason.Add(key, key.WorktimeReasonId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WorktimeReason(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWorktimeReason", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WorktimeReasonPrimaryKey pk in hashKey_WorktimeReason.Keys)
			{
				sp["@WORKTIME_REASON_ID"].Value = pk.WorktimeReasonId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckWorktimeStatusTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							WorktimeStatusTypePrimaryKey key = new WorktimeStatusTypePrimaryKey();
							key.WorktimeStatusTypeId = (System.Int32)row["WORKTIME_STATUS_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_WorktimeStatusType.Add(key, key.WorktimeStatusTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_WorktimeStatusType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteWorktimeStatusType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (WorktimeStatusTypePrimaryKey pk in hashKey_WorktimeStatusType.Keys)
			{
				sp["@WORKTIME_STATUS_TYPE_ID"].Value = pk.WorktimeStatusTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZonePrimitiveProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZonePrimitivePrimaryKey key = new ZonePrimitivePrimaryKey();
							key.PrimitiveId = (System.Int32)row["PRIMITIVE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZonePrimitive.Add(key, key.PrimitiveId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZonePrimitive(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZonePrimitive", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZonePrimitivePrimaryKey pk in hashKey_ZonePrimitive.Keys)
			{
				sp["@PRIMITIVE_ID"].Value = pk.PrimitiveId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZonePrimitiveVertexProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZonePrimitiveVertexPrimaryKey key = new ZonePrimitiveVertexPrimaryKey();
							key.PrimitiveVertexId = (System.Int32)row["PRIMITIVE_VERTEX_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZonePrimitiveVertex.Add(key, key.PrimitiveVertexId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZonePrimitiveVertex(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZonePrimitiveVertex", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZonePrimitiveVertexPrimaryKey pk in hashKey_ZonePrimitiveVertex.Keys)
			{
				sp["@PRIMITIVE_VERTEX_ID"].Value = pk.PrimitiveVertexId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZoneTypeProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZoneTypePrimaryKey key = new ZoneTypePrimaryKey();
							key.ZoneTypeId = (System.Int32)row["ZONE_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZoneType.Add(key, key.ZoneTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZoneType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZoneType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZoneTypePrimaryKey pk in hashKey_ZoneType.Keys)
			{
				sp["@ZONE_TYPE_ID"].Value = pk.ZoneTypeId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZoneVehicleProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZoneVehiclePrimaryKey key = new ZoneVehiclePrimaryKey();
							key.ZoneVehicleId = (System.Int32)row["ZONE_VEHICLE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZoneVehicle.Add(key, key.ZoneVehicleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZoneVehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZoneVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZoneVehiclePrimaryKey pk in hashKey_ZoneVehicle.Keys)
			{
				sp["@ZONE_VEHICLE_ID"].Value = pk.ZoneVehicleId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZoneVehiclegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZoneVehiclegroupPrimaryKey key = new ZoneVehiclegroupPrimaryKey();
							key.ZoneVehiclegroupId = (System.Int32)row["ZONE_VEHICLEGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZoneVehiclegroup.Add(key, key.ZoneVehiclegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZoneVehiclegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZoneVehiclegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZoneVehiclegroupPrimaryKey pk in hashKey_ZoneVehiclegroup.Keys)
			{
				sp["@ZONE_VEHICLEGROUP_ID"].Value = pk.ZoneVehiclegroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZonegroupProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZonegroupPrimaryKey key = new ZonegroupPrimaryKey();
							key.ZonegroupId = (System.Int32)row["ZONEGROUP_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Zonegroup.Add(key, key.ZonegroupId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_Zonegroup(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZonegroup", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZonegroupPrimaryKey pk in hashKey_Zonegroup.Keys)
			{
				sp["@ZONEGROUP_ID"].Value = pk.ZonegroupId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void CheckZonegroupZoneProperties(System.Data.DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ZonegroupZonePrimaryKey key = new ZonegroupZonePrimaryKey();
							key.ZonegroupZoneId = (System.Int32)row["ZONEGROUP_ZONE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ZonegroupZone.Add(key, key.ZonegroupZoneId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}

		public virtual void RemoveRecords_ZonegroupZone(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteZonegroupZone", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ZonegroupZonePrimaryKey pk in hashKey_ZonegroupZone.Keys)
			{
				sp["@ZONEGROUP_ZONE_ID"].Value = pk.ZonegroupZoneId;
				sp.ExecuteNonQuery();
			}

		}

		public virtual void LoadRecords_Cal_DayKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DATE] FROM [dbo].[CAL] t1 inner join [dbo].[DAY_KIND] t2 on t1.[DAY_KIND]=t2.[DAY_KIND_ID] WHERE t2.[DAY_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						CalPrimaryKey key = new CalPrimaryKey((System.DateTime)reader["DATE"]);
						if (hashKey_Cal.ContainsKey(key) == false)
						{
							hashKey_Cal.Add(key, key.Date);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Controller_Controller_Type(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER] t1 inner join [dbo].[CONTROLLER_TYPE] t2 on t1.[CONTROLLER_TYPE_ID]=t2.[CONTROLLER_TYPE_ID] WHERE t2.[CONTROLLER_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerPrimaryKey key = new ControllerPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_Controller.ContainsKey(key) == false)
						{
							hashKey_Controller.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Controller_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerPrimaryKey key = new ControllerPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_Controller.ContainsKey(key) == false)
						{
							hashKey_Controller.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ControllerTime_Controller(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_TIME_ID] FROM [dbo].[CONTROLLER_TIME] t1 inner join [dbo].[CONTROLLER] t2 on t1.[CONTROLLER_ID]=t2.[CONTROLLER_ID] WHERE t2.[CONTROLLER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Controller.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerTimePrimaryKey key = new ControllerTimePrimaryKey((System.Int32)reader["CONTROLLER_TIME_ID"]);
						if (hashKey_ControllerTime.ContainsKey(key) == false)
						{
							hashKey_ControllerTime.Add(key, key.ControllerTimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_DayKind_DkSet(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DAY_KIND_ID] FROM [dbo].[DAY_KIND] t1 inner join [dbo].[DK_SET] t2 on t1.[DK_SET]=t2.[DK_SET_ID] WHERE t2.[DK_SET_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DkSet.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DayKindPrimaryKey key = new DayKindPrimaryKey((System.Int32)reader["DAY_KIND_ID"]);
						if (hashKey_DayKind.ContainsKey(key) == false)
						{
							hashKey_DayKind.Add(key, key.DayKindId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Driver_DriverStatus(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVER_ID] FROM [dbo].[DRIVER] t1 inner join [dbo].[DRIVER_STATUS] t2 on t1.[DRIVER_STATUS_ID]=t2.[DRIVER_STATUS_ID] WHERE t2.[DRIVER_STATUS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DriverStatus.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DriverPrimaryKey key = new DriverPrimaryKey((System.Int32)reader["DRIVER_ID"]);
						if (hashKey_Driver.ContainsKey(key) == false)
						{
							hashKey_Driver.Add(key, key.DriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_DriverGroupDriver_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVERGROUP_ID], t1.[DRIVER_ID] FROM [dbo].[DRIVERGROUP_DRIVER] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DrivergroupDriverPrimaryKey key = new DrivergroupDriverPrimaryKey((System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["DRIVER_ID"]);
						if (hashKey_DrivergroupDriver.ContainsKey(key) == false)
						{
							hashKey_DrivergroupDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_DriverGroupDriver_DriverGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVERGROUP_ID], t1.[DRIVER_ID] FROM [dbo].[DRIVERGROUP_DRIVER] t1 inner join [dbo].[DRIVERGROUP] t2 on t1.[DRIVERGROUP_ID]=t2.[DRIVERGROUP_ID] WHERE t2.[DRIVERGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Drivergroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DrivergroupDriverPrimaryKey key = new DrivergroupDriverPrimaryKey((System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["DRIVER_ID"]);
						if (hashKey_DrivergroupDriver.ContainsKey(key) == false)
						{
							hashKey_DrivergroupDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_DriverVehicle_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVER_ID] FROM [dbo].[DRIVER_VEHICLE] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DriverVehiclePrimaryKey key = new DriverVehiclePrimaryKey((System.Int32)reader["DRIVER_ID"]);
						if (hashKey_DriverVehicle.ContainsKey(key) == false)
						{
							hashKey_DriverVehicle.Add(key, key.DriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_DriverVehicle_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVER_ID] FROM [dbo].[DRIVER_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DriverVehiclePrimaryKey key = new DriverVehiclePrimaryKey((System.Int32)reader["DRIVER_ID"]);
						if (hashKey_DriverVehicle.ContainsKey(key) == false)
						{
							hashKey_DriverVehicle.Add(key, key.DriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FactorValues_Factors(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[FACTOR_VALUES_ID] FROM [dbo].[FACTOR_VALUES] t1 inner join [dbo].[FACTORS] t2 on t1.[FACTOR_ID]=t2.[FACTOR_ID] WHERE t2.[FACTOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Factors.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						FactorValuesPrimaryKey key = new FactorValuesPrimaryKey((System.Int32)reader["FACTOR_VALUES_ID"]);
						if (hashKey_FactorValues.ContainsKey(key) == false)
						{
							hashKey_FactorValues.Add(key, key.FactorValuesId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FactorValues_Season(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[FACTOR_VALUES_ID] FROM [dbo].[FACTOR_VALUES] t1 inner join [dbo].[SEASON] t2 on t1.[SEASON_ID]=t2.[SEASON_ID] WHERE t2.[SEASON_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Season.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						FactorValuesPrimaryKey key = new FactorValuesPrimaryKey((System.Int32)reader["FACTOR_VALUES_ID"]);
						if (hashKey_FactorValues.ContainsKey(key) == false)
						{
							hashKey_FactorValues.Add(key, key.FactorValuesId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BLADING_BLADING_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BLADING_ID] FROM [dbo].[BLADING] t1 inner join [dbo].[BLADING_TYPE] t2 on t1.[BLADING_TYPE_ID]=t2.[BLADING_TYPE_ID] WHERE t2.[BLADING_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_BladingType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BladingPrimaryKey key = new BladingPrimaryKey((System.Int32)reader["BLADING_ID"]);
						if (hashKey_Blading.ContainsKey(key) == false)
						{
							hashKey_Blading.Add(key, key.BladingId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BLADING_CONTRACTOR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BLADING_ID] FROM [dbo].[BLADING] t1 inner join [dbo].[CONTRACTOR] t2 on t1.[CONTRACTOR_ID]=t2.[CONTRACTOR_ID] WHERE t2.[CONTRACTOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Contractor.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BladingPrimaryKey key = new BladingPrimaryKey((System.Int32)reader["BLADING_ID"]);
						if (hashKey_Blading.ContainsKey(key) == false)
						{
							hashKey_Blading.Add(key, key.BladingId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BLADING_WAYBILL(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BLADING_ID] FROM [dbo].[BLADING] t1 inner join [dbo].[WAYBILL] t2 on t1.[WAYBILL_ID]=t2.[WAYBILL_ID] WHERE t2.[WAYBILL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Waybill.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BladingPrimaryKey key = new BladingPrimaryKey((System.Int32)reader["BLADING_ID"]);
						if (hashKey_Blading.ContainsKey(key) == false)
						{
							hashKey_Blading.Add(key, key.BladingId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BRIGADE_DECADE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BRIGADE_ID] FROM [dbo].[BRIGADE] t1 inner join [dbo].[DECADE] t2 on t1.[DECADE]=t2.[DECADE_ID] WHERE t2.[DECADE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Decade.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BrigadePrimaryKey key = new BrigadePrimaryKey((System.Int32)reader["BRIGADE_ID"]);
						if (hashKey_Brigade.ContainsKey(key) == false)
						{
							hashKey_Brigade.Add(key, key.BrigadeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BRIGADE_DRIVER_BRIGADE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BRIGADE_DRIVER_ID] FROM [dbo].[BRIGADE_DRIVER] t1 inner join [dbo].[BRIGADE] t2 on t1.[BRIGADE]=t2.[BRIGADE_ID] WHERE t2.[BRIGADE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Brigade.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BrigadeDriverPrimaryKey key = new BrigadeDriverPrimaryKey((System.Int32)reader["BRIGADE_DRIVER_ID"]);
						if (hashKey_BrigadeDriver.ContainsKey(key) == false)
						{
							hashKey_BrigadeDriver.Add(key, key.BrigadeDriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BRIGADE_DRIVER_DRIVER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BRIGADE_DRIVER_ID] FROM [dbo].[BRIGADE_DRIVER] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BrigadeDriverPrimaryKey key = new BrigadeDriverPrimaryKey((System.Int32)reader["BRIGADE_DRIVER_ID"]);
						if (hashKey_BrigadeDriver.ContainsKey(key) == false)
						{
							hashKey_BrigadeDriver.Add(key, key.BrigadeDriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BRIGADE_GRAPHIC(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BRIGADE_ID] FROM [dbo].[BRIGADE] t1 inner join [dbo].[GRAPHIC] t2 on t1.[GRAPHIC]=t2.[GRAPHIC_ID] WHERE t2.[GRAPHIC_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Graphic.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BrigadePrimaryKey key = new BrigadePrimaryKey((System.Int32)reader["BRIGADE_ID"]);
						if (hashKey_Brigade.ContainsKey(key) == false)
						{
							hashKey_Brigade.Add(key, key.BrigadeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_BRIGADE_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[BRIGADE_ID] FROM [dbo].[BRIGADE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						BrigadePrimaryKey key = new BrigadePrimaryKey((System.Int32)reader["BRIGADE_ID"]);
						if (hashKey_Brigade.ContainsKey(key) == false)
						{
							hashKey_Brigade.Add(key, key.BrigadeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_CONTROLLER_INFO_CONTROLLER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER_INFO] t1 inner join [dbo].[CONTROLLER] t2 on t1.[CONTROLLER_ID]=t2.[CONTROLLER_ID] WHERE t2.[CONTROLLER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Controller.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerInfoPrimaryKey key = new ControllerInfoPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_ControllerInfo.ContainsKey(key) == false)
						{
							hashKey_ControllerInfo.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensor_ControllerSensorType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_ID] FROM [dbo].[CONTROLLER_SENSOR] t1 inner join [dbo].[CONTROLLER_SENSOR_TYPE] t2 on t1.[CONTROLLER_SENSOR_TYPE_ID]=t2.[CONTROLLER_SENSOR_TYPE_ID] WHERE t2.[CONTROLLER_SENSOR_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerSensorType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorPrimaryKey key = new ControllerSensorPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_ID"]);
						if (hashKey_ControllerSensor.ContainsKey(key) == false)
						{
							hashKey_ControllerSensor.Add(key, key.ControllerSensorId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensor_ControllerType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_ID] FROM [dbo].[CONTROLLER_SENSOR] t1 inner join [dbo].[CONTROLLER_TYPE] t2 on t1.[CONTROLLER_TYPE_ID]=t2.[CONTROLLER_TYPE_ID] WHERE t2.[CONTROLLER_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorPrimaryKey key = new ControllerSensorPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_ID"]);
						if (hashKey_ControllerSensor.ContainsKey(key) == false)
						{
							hashKey_ControllerSensor.Add(key, key.ControllerSensorId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensorLegend_ControllerSensorType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_LEGEND_ID] FROM [dbo].[CONTROLLER_SENSOR_LEGEND] t1 inner join [dbo].[CONTROLLER_SENSOR_TYPE] t2 on t1.[CONTROLLER_SENSOR_TYPE_ID]=t2.[CONTROLLER_SENSOR_TYPE_ID] WHERE t2.[CONTROLLER_SENSOR_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerSensorType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorLegendPrimaryKey key = new ControllerSensorLegendPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_LEGEND_ID"]);
						if (hashKey_ControllerSensorLegend.ContainsKey(key) == false)
						{
							hashKey_ControllerSensorLegend.Add(key, key.ControllerSensorLegendId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensorMap_Controller(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_MAP_ID] FROM [dbo].[CONTROLLER_SENSOR_MAP] t1 inner join [dbo].[CONTROLLER] t2 on t1.[CONTROLLER_ID]=t2.[CONTROLLER_ID] WHERE t2.[CONTROLLER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Controller.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorMapPrimaryKey key = new ControllerSensorMapPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_MAP_ID"]);
						if (hashKey_ControllerSensorMap.ContainsKey(key) == false)
						{
							hashKey_ControllerSensorMap.Add(key, key.ControllerSensorMapId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensorMap_ControllerSensor(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_MAP_ID] FROM [dbo].[CONTROLLER_SENSOR_MAP] t1 inner join [dbo].[CONTROLLER_SENSOR] t2 on t1.[CONTROLLER_SENSOR_ID]=t2.[CONTROLLER_SENSOR_ID] WHERE t2.[CONTROLLER_SENSOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerSensor.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorMapPrimaryKey key = new ControllerSensorMapPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_MAP_ID"]);
						if (hashKey_ControllerSensorMap.ContainsKey(key) == false)
						{
							hashKey_ControllerSensorMap.Add(key, key.ControllerSensorMapId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ControllerSensorMap_ControllerSensorLegend(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_SENSOR_MAP_ID] FROM [dbo].[CONTROLLER_SENSOR_MAP] t1 inner join [dbo].[CONTROLLER_SENSOR_LEGEND] t2 on t1.[CONTROLLER_SENSOR_LEGEND_ID]=t2.[CONTROLLER_SENSOR_LEGEND_ID] WHERE t2.[CONTROLLER_SENSOR_LEGEND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerSensorLegend.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerSensorMapPrimaryKey key = new ControllerSensorMapPrimaryKey((System.Int32)reader["CONTROLLER_SENSOR_MAP_ID"]);
						if (hashKey_ControllerSensorMap.ContainsKey(key) == false)
						{
							hashKey_ControllerSensorMap.Add(key, key.ControllerSensorMapId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_DAY_TIME_DAY_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DAY_TIME_ID] FROM [dbo].[DAY_TIME] t1 inner join [dbo].[DAY_TYPE] t2 on t1.[DAY_TYPE_ID]=t2.[DAY_TYPE_ID] WHERE t2.[DAY_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DayTimePrimaryKey key = new DayTimePrimaryKey((System.Int32)reader["DAY_TIME_ID"]);
						if (hashKey_DayTime.ContainsKey(key) == false)
						{
							hashKey_DayTime.Add(key, key.DayTimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_DECADE_DEPARTMENT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DECADE_ID] FROM [dbo].[DECADE] t1 inner join [dbo].[DEPARTMENT] t2 on t1.[DEPARTMENT]=t2.[DEPARTMENT_ID] WHERE t2.[DEPARTMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Department.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DecadePrimaryKey key = new DecadePrimaryKey((System.Int32)reader["DECADE_ID"]);
						if (hashKey_Decade.ContainsKey(key) == false)
						{
							hashKey_Decade.Add(key, key.DecadeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_DRIVER_DEPARTMENT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVER_ID] FROM [dbo].[DRIVER] t1 inner join [dbo].[DEPARTMENT] t2 on t1.[DEPARTMENT]=t2.[DEPARTMENT_ID] WHERE t2.[DEPARTMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Department.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						DriverPrimaryKey key = new DriverPrimaryKey((System.Int32)reader["DRIVER_ID"]);
						if (hashKey_Driver.ContainsKey(key) == false)
						{
							hashKey_Driver.Add(key, key.DriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_EMAIL_SCHEDULEREVENT_EMAIL(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULEREVENT_ID], t1.[EMAIL_ID] FROM [dbo].[EMAIL_SCHEDULEREVENT] t1 inner join [dbo].[EMAIL] t2 on t1.[EMAIL_ID]=t2.[EMAIL_ID] WHERE t2.[EMAIL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Email.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						EmailSchedulereventPrimaryKey key = new EmailSchedulereventPrimaryKey((System.Int32)reader["SCHEDULEREVENT_ID"], (System.Int32)reader["EMAIL_ID"]);
						if (hashKey_EmailSchedulerevent.ContainsKey(key) == false)
						{
							hashKey_EmailSchedulerevent.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULEREVENT_ID], t1.[EMAIL_ID] FROM [dbo].[EMAIL_SCHEDULEREVENT] t1 inner join [dbo].[SCHEDULEREVENT] t2 on t1.[SCHEDULEREVENT_ID]=t2.[SCHEDULEREVENT_ID] WHERE t2.[SCHEDULEREVENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedulerevent.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						EmailSchedulereventPrimaryKey key = new EmailSchedulereventPrimaryKey((System.Int32)reader["SCHEDULEREVENT_ID"], (System.Int32)reader["EMAIL_ID"]);
						if (hashKey_EmailSchedulerevent.ContainsKey(key) == false)
						{
							hashKey_EmailSchedulerevent.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_EMAIL_SCHEDULERQUEUE_EMAIL(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULERQUEUE_ID], t1.[EMAIL_ID] FROM [dbo].[EMAIL_SCHEDULERQUEUE] t1 inner join [dbo].[EMAIL] t2 on t1.[EMAIL_ID]=t2.[EMAIL_ID] WHERE t2.[EMAIL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Email.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						EmailSchedulerqueuePrimaryKey key = new EmailSchedulerqueuePrimaryKey((System.Int32)reader["SCHEDULERQUEUE_ID"], (System.Int32)reader["EMAIL_ID"]);
						if (hashKey_EmailSchedulerqueue.ContainsKey(key) == false)
						{
							hashKey_EmailSchedulerqueue.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULERQUEUE_ID], t1.[EMAIL_ID] FROM [dbo].[EMAIL_SCHEDULERQUEUE] t1 inner join [dbo].[SCHEDULERQUEUE] t2 on t1.[SCHEDULERQUEUE_ID]=t2.[SCHEDULERQUEUE_ID] WHERE t2.[SCHEDULERQUEUE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedulerqueue.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						EmailSchedulerqueuePrimaryKey key = new EmailSchedulerqueuePrimaryKey((System.Int32)reader["SCHEDULERQUEUE_ID"], (System.Int32)reader["EMAIL_ID"]);
						if (hashKey_EmailSchedulerqueue.ContainsKey(key) == false)
						{
							hashKey_EmailSchedulerqueue.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GEO_SEGMENT_RUNTIME_DAY_TIME(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_RUNTIME_ID] FROM [dbo].[GEO_SEGMENT_RUNTIME] t1 inner join [dbo].[DAY_TIME] t2 on t1.[DAY_TIME_ID]=t2.[DAY_TIME_ID] WHERE t2.[DAY_TIME_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayTime.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentRuntimePrimaryKey key = new GeoSegmentRuntimePrimaryKey((System.Int32)reader["GEO_SEGMENT_RUNTIME_ID"]);
						if (hashKey_GeoSegmentRuntime.ContainsKey(key) == false)
						{
							hashKey_GeoSegmentRuntime.Add(key, key.GeoSegmentRuntimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_RUNTIME_ID] FROM [dbo].[GEO_SEGMENT_RUNTIME] t1 inner join [dbo].[GEO_SEGMENT] t2 on t1.[GEO_SEGMENT_ID]=t2.[GEO_SEGMENT_ID] WHERE t2.[GEO_SEGMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoSegment.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentRuntimePrimaryKey key = new GeoSegmentRuntimePrimaryKey((System.Int32)reader["GEO_SEGMENT_RUNTIME_ID"]);
						if (hashKey_GeoSegmentRuntime.ContainsKey(key) == false)
						{
							hashKey_GeoSegmentRuntime.Add(key, key.GeoSegmentRuntimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GOODS_GOODS_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GOODS_ID] FROM [dbo].[GOODS] t1 inner join [dbo].[GOODS_TYPE] t2 on t1.[GOODS_TYPE_ID]=t2.[GOODS_TYPE_ID] WHERE t2.[GOODS_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GoodsType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GoodsPrimaryKey key = new GoodsPrimaryKey((System.Int32)reader["GOODS_ID"]);
						if (hashKey_Goods.ContainsKey(key) == false)
						{
							hashKey_Goods.Add(key, key.GoodsId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GOODS_LOGISTIC_ORDER_GOODS(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GOODS_LOGISTIC_ORDER_ID] FROM [dbo].[GOODS_LOGISTIC_ORDER] t1 inner join [dbo].[GOODS] t2 on t1.[GOODS_ID]=t2.[GOODS_ID] WHERE t2.[GOODS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Goods.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GoodsLogisticOrderPrimaryKey key = new GoodsLogisticOrderPrimaryKey((System.Int32)reader["GOODS_LOGISTIC_ORDER_ID"]);
						if (hashKey_GoodsLogisticOrder.ContainsKey(key) == false)
						{
							hashKey_GoodsLogisticOrder.Add(key, key.GoodsLogisticOrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GOODS_LOGISTIC_ORDER_ID] FROM [dbo].[GOODS_LOGISTIC_ORDER] t1 inner join [dbo].[LOGISTIC_ORDER] t2 on t1.[LOGISTIC_ORDER_ID]=t2.[LOGISTIC_ORDER_ID] WHERE t2.[LOGISTIC_ORDER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_LogisticOrder.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GoodsLogisticOrderPrimaryKey key = new GoodsLogisticOrderPrimaryKey((System.Int32)reader["GOODS_LOGISTIC_ORDER_ID"]);
						if (hashKey_GoodsLogisticOrder.ContainsKey(key) == false)
						{
							hashKey_GoodsLogisticOrder.Add(key, key.GoodsLogisticOrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GOODS_TYPE_VEHICLE_GOODS_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GOODS_TYPE_VEHICLE_id] FROM [dbo].[GOODS_TYPE_VEHICLE] t1 inner join [dbo].[GOODS_TYPE] t2 on t1.[GOODS_TYPE_ID]=t2.[GOODS_TYPE_ID] WHERE t2.[GOODS_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GoodsType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GoodsTypeVehiclePrimaryKey key = new GoodsTypeVehiclePrimaryKey((System.Int32)reader["GOODS_TYPE_VEHICLE_id"]);
						if (hashKey_GoodsTypeVehicle.ContainsKey(key) == false)
						{
							hashKey_GoodsTypeVehicle.Add(key, key.GoodsTypeVehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_GOODS_TYPE_VEHICLE_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GOODS_TYPE_VEHICLE_id] FROM [dbo].[GOODS_TYPE_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GoodsTypeVehiclePrimaryKey key = new GoodsTypeVehiclePrimaryKey((System.Int32)reader["GOODS_TYPE_VEHICLE_id"]);
						if (hashKey_GoodsTypeVehicle.ContainsKey(key) == false)
						{
							hashKey_GoodsTypeVehicle.Add(key, key.GoodsTypeVehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_DAY_BRIGADE_DRIVER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_DAY_ID] FROM [dbo].[JOURNAL_DAY] t1 inner join [dbo].[BRIGADE_DRIVER] t2 on t1.[BRIGADE_DRIVER]=t2.[BRIGADE_DRIVER_ID] WHERE t2.[BRIGADE_DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_BrigadeDriver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalDayPrimaryKey key = new JournalDayPrimaryKey((System.Int32)reader["JOURNAL_DAY_ID"]);
						if (hashKey_JournalDay.ContainsKey(key) == false)
						{
							hashKey_JournalDay.Add(key, key.JournalDayId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_DAY_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_DAY_ID] FROM [dbo].[JOURNAL_DAY] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalDayPrimaryKey key = new JournalDayPrimaryKey((System.Int32)reader["JOURNAL_DAY_ID"]);
						if (hashKey_JournalDay.ContainsKey(key) == false)
						{
							hashKey_JournalDay.Add(key, key.JournalDayId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_DAY_WAYOUT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_DAY_ID] FROM [dbo].[JOURNAL_DAY] t1 inner join [dbo].[WAYOUT] t2 on t1.[WAYOUT]=t2.[WAYOUT_ID] WHERE t2.[ROUTE] in (");
			bool first = true;
			foreach (int pk in hashKey_Wayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalDayPrimaryKey key = new JournalDayPrimaryKey((System.Int32)reader["JOURNAL_DAY_ID"]);
						if (hashKey_JournalDay.ContainsKey(key) == false)
						{
							hashKey_JournalDay.Add(key, key.JournalDayId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_DRIVER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_ID] FROM [dbo].[JOURNAL] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalPrimaryKey key = new JournalPrimaryKey((System.Int32)reader["JOURNAL_ID"]);
						if (hashKey_Journal.ContainsKey(key) == false)
						{
							hashKey_Journal.Add(key, key.JournalId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_DRIVER_DRIVER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_DRIVER_ID] FROM [dbo].[JOURNAL_DRIVER] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalDriverPrimaryKey key = new JournalDriverPrimaryKey((System.Int32)reader["JOURNAL_DRIVER_ID"]);
						if (hashKey_JournalDriver.ContainsKey(key) == false)
						{
							hashKey_JournalDriver.Add(key, key.JournalDriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_ID] FROM [dbo].[JOURNAL] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalPrimaryKey key = new JournalPrimaryKey((System.Int32)reader["JOURNAL_ID"]);
						if (hashKey_Journal.ContainsKey(key) == false)
						{
							hashKey_Journal.Add(key, key.JournalId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_JOURNAL_VEHICLE_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_VEHICLE_ID] FROM [dbo].[JOURNAL_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalVehiclePrimaryKey key = new JournalVehiclePrimaryKey((System.Int32)reader["JOURNAL_VEHICLE_ID"]);
						if (hashKey_JournalVehicle.ContainsKey(key) == false)
						{
							hashKey_JournalVehicle.Add(key, key.JournalVehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ADDRESS_CONTRACTOR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ADDRESS_ID] FROM [dbo].[LOGISTIC_ADDRESS] t1 inner join [dbo].[CONTRACTOR] t2 on t1.[CONTRACTOR_ID]=t2.[CONTRACTOR_ID] WHERE t2.[CONTRACTOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Contractor.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticAddressPrimaryKey key = new LogisticAddressPrimaryKey((System.Int32)reader["LOGISTIC_ADDRESS_ID"]);
						if (hashKey_LogisticAddress.ContainsKey(key) == false)
						{
							hashKey_LogisticAddress.Add(key, key.LogisticAddressId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ADDRESS_ID] FROM [dbo].[LOGISTIC_ADDRESS] t1 inner join [dbo].[CONTRACTOR_CALENDAR] t2 on t1.[CONTRACTOR_CALENDAR_ID]=t2.[CONTRACTOR_CALENDAR_ID] WHERE t2.[CONTRACTOR_CALENDAR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ContractorCalendar.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticAddressPrimaryKey key = new LogisticAddressPrimaryKey((System.Int32)reader["LOGISTIC_ADDRESS_ID"]);
						if (hashKey_LogisticAddress.ContainsKey(key) == false)
						{
							hashKey_LogisticAddress.Add(key, key.LogisticAddressId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ADDRESS_GEO_ZONE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ADDRESS_ID] FROM [dbo].[LOGISTIC_ADDRESS] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[GEOZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticAddressPrimaryKey key = new LogisticAddressPrimaryKey((System.Int32)reader["LOGISTIC_ADDRESS_ID"]);
						if (hashKey_LogisticAddress.ContainsKey(key) == false)
						{
							hashKey_LogisticAddress.Add(key, key.LogisticAddressId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ORDER_BLADING_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ORDER_ID] FROM [dbo].[LOGISTIC_ORDER] t1 inner join [dbo].[BLADING_TYPE] t2 on t1.[BLADING_TYPE_ID]=t2.[BLADING_TYPE_ID] WHERE t2.[BLADING_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_BladingType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticOrderPrimaryKey key = new LogisticOrderPrimaryKey((System.Int32)reader["LOGISTIC_ORDER_ID"]);
						if (hashKey_LogisticOrder.ContainsKey(key) == false)
						{
							hashKey_LogisticOrder.Add(key, key.LogisticOrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ORDER_CONTRACTOR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ORDER_ID] FROM [dbo].[LOGISTIC_ORDER] t1 inner join [dbo].[CONTRACTOR] t2 on t1.[CONTRACTOR_ID]=t2.[CONTRACTOR_ID] WHERE t2.[CONTRACTOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Contractor.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticOrderPrimaryKey key = new LogisticOrderPrimaryKey((System.Int32)reader["LOGISTIC_ORDER_ID"]);
						if (hashKey_LogisticOrder.ContainsKey(key) == false)
						{
							hashKey_LogisticOrder.Add(key, key.LogisticOrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOGISTIC_ORDER_ID] FROM [dbo].[LOGISTIC_ORDER] t1 inner join [dbo].[LOGISTIC_ADDRESS] t2 on t1.[LOGISTIC_ADDRESS_ID]=t2.[LOGISTIC_ADDRESS_ID] WHERE t2.[LOGISTIC_ADDRESS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_LogisticAddress.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogisticOrderPrimaryKey key = new LogisticOrderPrimaryKey((System.Int32)reader["LOGISTIC_ORDER_ID"]);
						if (hashKey_LogisticOrder.ContainsKey(key) == false)
						{
							hashKey_LogisticOrder.Add(key, key.LogisticOrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_OPERATOR_PROFILE_OPERATOR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_PROFILE_ID] FROM [dbo].[OPERATOR_PROFILE] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorProfilePrimaryKey key = new OperatorProfilePrimaryKey((System.Int32)reader["OPERATOR_PROFILE_ID"]);
						if (hashKey_OperatorProfile.ContainsKey(key) == false)
						{
							hashKey_OperatorProfile.Add(key, key.OperatorProfileId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ORDER_TRIP_JOURNAL_DAY(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ORDER_TRIP_ID] FROM [dbo].[ORDER_TRIP] t1 inner join [dbo].[JOURNAL_DAY] t2 on t1.[JOURNAL_DAY]=t2.[JOURNAL_DAY_ID] WHERE t2.[JOURNAL_DAY_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_JournalDay.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderTripPrimaryKey key = new OrderTripPrimaryKey((System.Int32)reader["ORDER_TRIP_ID"]);
						if (hashKey_OrderTrip.ContainsKey(key) == false)
						{
							hashKey_OrderTrip.Add(key, key.OrderTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ORDER_TRIP_ORDER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ORDER_TRIP_ID] FROM [dbo].[ORDER_TRIP] t1 inner join [dbo].[ORDER] t2 on t1.[ORDER]=t2.[ORDER_ID] WHERE t2.[ORDER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Order.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderTripPrimaryKey key = new OrderTripPrimaryKey((System.Int32)reader["ORDER_TRIP_ID"]);
						if (hashKey_OrderTrip.ContainsKey(key) == false)
						{
							hashKey_OrderTrip.Add(key, key.OrderTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ROUTE_GEOZONE_GEO_ZONE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_GEOZONE_ID] FROM [dbo].[ROUTE_GEOZONE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[GEOZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RouteGeozonePrimaryKey key = new RouteGeozonePrimaryKey((System.Int32)reader["ROUTE_GEOZONE_ID"]);
						if (hashKey_RouteGeozone.ContainsKey(key) == false)
						{
							hashKey_RouteGeozone.Add(key, key.RouteGeozoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_ROUTE_GEOZONE_ROUTE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_GEOZONE_ID] FROM [dbo].[ROUTE_GEOZONE] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RouteGeozonePrimaryKey key = new RouteGeozonePrimaryKey((System.Int32)reader["ROUTE_GEOZONE_ID"]);
						if (hashKey_RouteGeozone.ContainsKey(key) == false)
						{
							hashKey_RouteGeozone.Add(key, key.RouteGeozoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_DAY_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_ID] FROM [dbo].[RS] t1 inner join [dbo].[DAY_TYPE] t2 on t1.[DAY_TYPE_ID]=t2.[DAY_TYPE_ID] WHERE t2.[DAY_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPrimaryKey key = new RsPrimaryKey((System.Int32)reader["RS_ID"]);
						if (hashKey_Rs.ContainsKey(key) == false)
						{
							hashKey_Rs.Add(key, key.RsId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_NUMBER_RS_STEP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_NUMBER_ID] FROM [dbo].[RS_NUMBER] t1 inner join [dbo].[RS_STEP] t2 on t1.[RS_STEP]=t2.[RS_STEP_ID] WHERE t2.[RS_STEP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsStep.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsNumberPrimaryKey key = new RsNumberPrimaryKey((System.Int32)reader["RS_NUMBER_ID"]);
						if (hashKey_RsNumber.ContainsKey(key) == false)
						{
							hashKey_RsNumber.Add(key, key.RsNumberId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_OPERATIONTYPE_ID] FROM [dbo].[RS_OPERATIONTYPE] t1 inner join [dbo].[RS_OPERATIONSTYPE] t2 on t1.[RS_OPERATIONSTYPE_ID]=t2.[RS_OPERATIONSTYPE_ID] WHERE t2.[RS_OPERATIONSTYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsOperationstype.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey((System.Int32)reader["RS_OPERATIONTYPE_ID"]);
						if (hashKey_RsOperationtype.ContainsKey(key) == false)
						{
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_PERIOD_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PERIOD_ID] FROM [dbo].[RS_PERIOD] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT_ID]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPeriodPrimaryKey key = new RsPeriodPrimaryKey((System.Int32)reader["PERIOD_ID"]);
						if (hashKey_RsPeriod.ContainsKey(key) == false)
						{
							hashKey_RsPeriod.Add(key, key.PeriodId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_POINT_RS_OPERATIONTYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_POINT_ID] FROM [dbo].[RS_POINT] t1 inner join [dbo].[RS_OPERATIONTYPE] t2 on t1.[RS_OPERATIONTYPE_ID]=t2.[RS_OPERATIONTYPE_ID] WHERE t2.[RS_OPERATIONTYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsOperationtype.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPointPrimaryKey key = new RsPointPrimaryKey((System.Int32)reader["RS_POINT_ID"]);
						if (hashKey_RsPoint.ContainsKey(key) == false)
						{
							hashKey_RsPoint.Add(key, key.RsPointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_POINT_RS_SHIFT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_POINT_ID] FROM [dbo].[RS_POINT] t1 inner join [dbo].[RS_SHIFT] t2 on t1.[RS_SHIFT_ID]=t2.[RS_SHIFT_ID] WHERE t2.[RS_SHIFT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsShift.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPointPrimaryKey key = new RsPointPrimaryKey((System.Int32)reader["RS_POINT_ID"]);
						if (hashKey_RsPoint.ContainsKey(key) == false)
						{
							hashKey_RsPoint.Add(key, key.RsPointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_POINT_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_POINT_ID] FROM [dbo].[RS_POINT] t1 inner join [dbo].[RS_ROUND_TRIP] t2 on t1.[RS_ROUND_TRIP_ID]=t2.[RS_ROUND_TRIP_ID] WHERE t2.[RS_ROUND_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsRoundTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPointPrimaryKey key = new RsPointPrimaryKey((System.Int32)reader["RS_POINT_ID"]);
						if (hashKey_RsPoint.ContainsKey(key) == false)
						{
							hashKey_RsPoint.Add(key, key.RsPointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_POINT_RS_TRIPTYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_POINT_ID] FROM [dbo].[RS_POINT] t1 inner join [dbo].[RS_TRIPTYPE] t2 on t1.[RS_TRIPTYPE_ID]=t2.[RS_TRIPTYPE_ID] WHERE t2.[RS_TRIPTYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTriptype.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPointPrimaryKey key = new RsPointPrimaryKey((System.Int32)reader["RS_POINT_ID"]);
						if (hashKey_RsPoint.ContainsKey(key) == false)
						{
							hashKey_RsPoint.Add(key, key.RsPointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_POINT_RS_WAYOUT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_POINT_ID] FROM [dbo].[RS_POINT] t1 inner join [dbo].[RS_WAYOUT] t2 on t1.[RS_WAYOUT_ID]=t2.[RS_WAYOUT_ID] WHERE t2.[RS_WAYOUT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsWayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPointPrimaryKey key = new RsPointPrimaryKey((System.Int32)reader["RS_POINT_ID"]);
						if (hashKey_RsPoint.ContainsKey(key) == false)
						{
							hashKey_RsPoint.Add(key, key.RsPointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_ROUND_TRIP_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_ROUND_TRIP_ID] FROM [dbo].[RS_ROUND_TRIP] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT_ID]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsRoundTripPrimaryKey key = new RsRoundTripPrimaryKey((System.Int32)reader["RS_ROUND_TRIP_ID"]);
						if (hashKey_RsRoundTrip.ContainsKey(key) == false)
						{
							hashKey_RsRoundTrip.Add(key, key.RsRoundTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_ROUTE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_ID] FROM [dbo].[RS] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsPrimaryKey key = new RsPrimaryKey((System.Int32)reader["RS_ID"]);
						if (hashKey_Rs.ContainsKey(key) == false)
						{
							hashKey_Rs.Add(key, key.RsId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_RUNTIME_RS_PERIOD(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_RUNTIME_ID] FROM [dbo].[RS_RUNTIME] t1 inner join [dbo].[RS_PERIOD] t2 on t1.[PERIOD_ID]=t2.[PERIOD_ID] WHERE t2.[PERIOD_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsPeriod.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsRuntimePrimaryKey key = new RsRuntimePrimaryKey((System.Int32)reader["RS_RUNTIME_ID"]);
						if (hashKey_RsRuntime.ContainsKey(key) == false)
						{
							hashKey_RsRuntime.Add(key, key.RsRuntimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_RUNTIME_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_RUNTIME_ID] FROM [dbo].[RS_RUNTIME] t1 inner join [dbo].[RS_TRIP] t2 on t1.[RS_TRIP_ID]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsRuntimePrimaryKey key = new RsRuntimePrimaryKey((System.Int32)reader["RS_RUNTIME_ID"]);
						if (hashKey_RsRuntime.ContainsKey(key) == false)
						{
							hashKey_RsRuntime.Add(key, key.RsRuntimeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_SHIFT_RS_WAYOUT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_SHIFT_ID] FROM [dbo].[RS_SHIFT] t1 inner join [dbo].[RS_WAYOUT] t2 on t1.[RS_WAYOUT_ID]=t2.[RS_WAYOUT_ID] WHERE t2.[RS_WAYOUT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsWayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsShiftPrimaryKey key = new RsShiftPrimaryKey((System.Int32)reader["RS_SHIFT_ID"]);
						if (hashKey_RsShift.ContainsKey(key) == false)
						{
							hashKey_RsShift.Add(key, key.RsShiftId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_STEP_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_STEP_ID] FROM [dbo].[RS_STEP] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsStepPrimaryKey key = new RsStepPrimaryKey((System.Int32)reader["RS_STEP_ID"]);
						if (hashKey_RsStep.ContainsKey(key) == false)
						{
							hashKey_RsStep.Add(key, key.RsStepId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_TRIP_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIP_ID] FROM [dbo].[RS_TRIP] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT_ID]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTripPrimaryKey key = new RsTripPrimaryKey((System.Int32)reader["RS_TRIP_ID"]);
						if (hashKey_RsTrip.ContainsKey(key) == false)
						{
							hashKey_RsTrip.Add(key, key.RsTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_TRIP0_ROUTE_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIP_ID] FROM [dbo].[RS_TRIP] t1 inner join [dbo].[ROUTE_TRIP] t2 on t1.[ROUTE_TRIP_ID]=t2.[ROUTE_TRIP_ID] WHERE t2.[ROUTE_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RouteTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTripPrimaryKey key = new RsTripPrimaryKey((System.Int32)reader["RS_TRIP_ID"]);
						if (hashKey_RsTrip.ContainsKey(key) == false)
						{
							hashKey_RsTrip.Add(key, key.RsTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_TRIPTYPE_RS_TRIPSTYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIPTYPE_ID] FROM [dbo].[RS_TRIPTYPE] t1 inner join [dbo].[RS_TRIPSTYPE] t2 on t1.[RS_TRIPSTYPE_ID]=t2.[RS_TRIPSTYPE_ID] WHERE t2.[RS_TRIPSTYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTripstype.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTriptypePrimaryKey key = new RsTriptypePrimaryKey((System.Int32)reader["RS_TRIPTYPE_ID"]);
						if (hashKey_RsTriptype.ContainsKey(key) == false)
						{
							hashKey_RsTriptype.Add(key, key.RsTriptypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_TRIPTYPE_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIPTYPE_ID] FROM [dbo].[RS_TRIPTYPE] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT_ID]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTriptypePrimaryKey key = new RsTriptypePrimaryKey((System.Int32)reader["RS_TRIPTYPE_ID"]);
						if (hashKey_RsTriptype.ContainsKey(key) == false)
						{
							hashKey_RsTriptype.Add(key, key.RsTriptypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_VARIANT_RS(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_VARIANT_ID] FROM [dbo].[RS_VARIANT] t1 inner join [dbo].[RS] t2 on t1.[RS_ID]=t2.[RS_ID] WHERE t2.[RS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Rs.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsVariantPrimaryKey key = new RsVariantPrimaryKey((System.Int32)reader["RS_VARIANT_ID"]);
						if (hashKey_RsVariant.ContainsKey(key) == false)
						{
							hashKey_RsVariant.Add(key, key.RsVariantId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_VARIANT_RS_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_VARIANT_ID] FROM [dbo].[RS_VARIANT] t1 inner join [dbo].[RS_TYPE] t2 on t1.[RS_TYPE_ID]=t2.[RS_TYPE_ID] WHERE t2.[RS_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsVariantPrimaryKey key = new RsVariantPrimaryKey((System.Int32)reader["RS_VARIANT_ID"]);
						if (hashKey_RsVariant.ContainsKey(key) == false)
						{
							hashKey_RsVariant.Add(key, key.RsVariantId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_WAYOUT_GRAPHIC(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[GRAPHIC] t2 on t1.[GRAPHIC_ID]=t2.[GRAPHIC_ID] WHERE t2.[GRAPHIC_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Graphic.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_WAYOUT_RS_VARIANT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[RS_VARIANT] t2 on t1.[RS_VARIANT_ID]=t2.[RS_VARIANT_ID] WHERE t2.[RS_VARIANT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsVariant.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RS_WAYOUT_WAYOUT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[WAYOUT] t2 on t1.[WAYOUT_ID]=t2.[WAYOUT_ID] WHERE t2.[ROUTE] in (");
			bool first = true;
			foreach (int pk in hashKey_Wayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSOT_TRIPIN_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_OPERATIONTYPE_ID] FROM [dbo].[RS_OPERATIONTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey((System.Int32)reader["RS_OPERATIONTYPE_ID"]);
						if (hashKey_RsOperationtype.ContainsKey(key) == false)
						{
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSOT_TRIPIN0_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_OPERATIONTYPE_ID] FROM [dbo].[RS_OPERATIONTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey((System.Int32)reader["RS_OPERATIONTYPE_ID"]);
						if (hashKey_RsOperationtype.ContainsKey(key) == false)
						{
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSOT_TRIPOUT_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_OPERATIONTYPE_ID] FROM [dbo].[RS_OPERATIONTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey((System.Int32)reader["RS_OPERATIONTYPE_ID"]);
						if (hashKey_RsOperationtype.ContainsKey(key) == false)
						{
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSOT_TRIPOUT0_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_OPERATIONTYPE_ID] FROM [dbo].[RS_OPERATIONTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsOperationtypePrimaryKey key = new RsOperationtypePrimaryKey((System.Int32)reader["RS_OPERATIONTYPE_ID"]);
						if (hashKey_RsOperationtype.ContainsKey(key) == false)
						{
							hashKey_RsOperationtype.Add(key, key.RsOperationtypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSSHIFT_TRIPIN_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_SHIFT_ID] FROM [dbo].[RS_SHIFT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsShiftPrimaryKey key = new RsShiftPrimaryKey((System.Int32)reader["RS_SHIFT_ID"]);
						if (hashKey_RsShift.ContainsKey(key) == false)
						{
							hashKey_RsShift.Add(key, key.RsShiftId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSSHIFT_TRIPIN0_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_SHIFT_ID] FROM [dbo].[RS_SHIFT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsShiftPrimaryKey key = new RsShiftPrimaryKey((System.Int32)reader["RS_SHIFT_ID"]);
						if (hashKey_RsShift.ContainsKey(key) == false)
						{
							hashKey_RsShift.Add(key, key.RsShiftId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSSHIFT_TRIPOUT_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_SHIFT_ID] FROM [dbo].[RS_SHIFT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsShiftPrimaryKey key = new RsShiftPrimaryKey((System.Int32)reader["RS_SHIFT_ID"]);
						if (hashKey_RsShift.ContainsKey(key) == false)
						{
							hashKey_RsShift.Add(key, key.RsShiftId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSSHIFT_TRIPOUT0_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_SHIFT_ID] FROM [dbo].[RS_SHIFT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsShiftPrimaryKey key = new RsShiftPrimaryKey((System.Int32)reader["RS_SHIFT_ID"]);
						if (hashKey_RsShift.ContainsKey(key) == false)
						{
							hashKey_RsShift.Add(key, key.RsShiftId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSTT_TRIPA_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIPTYPE_ID] FROM [dbo].[RS_TRIPTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_A]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTriptypePrimaryKey key = new RsTriptypePrimaryKey((System.Int32)reader["RS_TRIPTYPE_ID"]);
						if (hashKey_RsTriptype.ContainsKey(key) == false)
						{
							hashKey_RsTriptype.Add(key, key.RsTriptypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSTT_TRIPB_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_TRIPTYPE_ID] FROM [dbo].[RS_TRIPTYPE] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_B]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsTriptypePrimaryKey key = new RsTriptypePrimaryKey((System.Int32)reader["RS_TRIPTYPE_ID"]);
						if (hashKey_RsTriptype.ContainsKey(key) == false)
						{
							hashKey_RsTriptype.Add(key, key.RsTriptypeId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSW_TRIPIN_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSW_TRIPIN0_RS_TRIP0(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_IN_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSW_TRIPOUT_RS_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_RSW_TRIPOUT0_RS_TRIP0(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RS_WAYOUT_ID] FROM [dbo].[RS_WAYOUT] t1 inner join [dbo].[RS_TRIP] t2 on t1.[TRIP_OUT_0]=t2.[RS_TRIP_ID] WHERE t2.[RS_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RsTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RsWayoutPrimaryKey key = new RsWayoutPrimaryKey((System.Int32)reader["RS_WAYOUT_ID"]);
						if (hashKey_RsWayout.ContainsKey(key) == false)
						{
							hashKey_RsWayout.Add(key, key.RsWayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_DETAIL_ID] FROM [dbo].[SCHEDULE_DETAIL_INFO] t1 inner join [dbo].[SCHEDULE_DETAIL] t2 on t1.[SCHEDULE_DETAIL_ID]=t2.[SCHEDULE_DETAIL_ID] WHERE t2.[SCHEDULE_DETAIL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ScheduleDetail.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ScheduleDetailInfoPrimaryKey key = new ScheduleDetailInfoPrimaryKey((System.Int32)reader["SCHEDULE_DETAIL_ID"]);
						if (hashKey_ScheduleDetailInfo.ContainsKey(key) == false)
						{
							hashKey_ScheduleDetailInfo.Add(key, key.ScheduleDetailId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_GEOZONE_ID] FROM [dbo].[SCHEDULE_GEOZONE] t1 inner join [dbo].[ROUTE_GEOZONE] t2 on t1.[ROUTE_GEOZONE_ID]=t2.[ROUTE_GEOZONE_ID] WHERE t2.[ROUTE_GEOZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RouteGeozone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ScheduleGeozonePrimaryKey key = new ScheduleGeozonePrimaryKey((System.Int32)reader["SCHEDULE_GEOZONE_ID"]);
						if (hashKey_ScheduleGeozone.ContainsKey(key) == false)
						{
							hashKey_ScheduleGeozone.Add(key, key.ScheduleGeozoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_SCHEDULE_GEOZONE_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_GEOZONE_ID] FROM [dbo].[SCHEDULE_GEOZONE] t1 inner join [dbo].[TRIP] t2 on t1.[TRIP_ID]=t2.[TRIP_ID] WHERE t2.[TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Trip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ScheduleGeozonePrimaryKey key = new ScheduleGeozonePrimaryKey((System.Int32)reader["SCHEDULE_GEOZONE_ID"]);
						if (hashKey_ScheduleGeozone.ContainsKey(key) == false)
						{
							hashKey_ScheduleGeozone.Add(key, key.ScheduleGeozoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_VEHICLE_DEPARTMENT(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_ID] FROM [dbo].[VEHICLE] t1 inner join [dbo].[DEPARTMENT] t2 on t1.[DEPARTMENT]=t2.[DEPARTMENT_ID] WHERE t2.[DEPARTMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Department.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclePrimaryKey key = new VehiclePrimaryKey((System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_Vehicle.ContainsKey(key) == false)
						{
							hashKey_Vehicle.Add(key, key.VehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_VEHICLE_OWNER_OWNER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_OWNER_ID] FROM [dbo].[VEHICLE_OWNER] t1 inner join [dbo].[OWNER] t2 on t1.[OWNER_ID]=t2.[OWNER_ID] WHERE t2.[OWNER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Owner.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehicleOwnerPrimaryKey key = new VehicleOwnerPrimaryKey((System.Int32)reader["VEHICLE_OWNER_ID"]);
						if (hashKey_VehicleOwner.ContainsKey(key) == false)
						{
							hashKey_VehicleOwner.Add(key, key.VehicleOwnerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_VEHICLE_OWNER_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_OWNER_ID] FROM [dbo].[VEHICLE_OWNER] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehicleOwnerPrimaryKey key = new VehicleOwnerPrimaryKey((System.Int32)reader["VEHICLE_OWNER_ID"]);
						if (hashKey_VehicleOwner.ContainsKey(key) == false)
						{
							hashKey_VehicleOwner.Add(key, key.VehicleOwnerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_VEHICLE_PICTURE_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_PICTURE_ID] FROM [dbo].[VEHICLE_PICTURE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclePicturePrimaryKey key = new VehiclePicturePrimaryKey((System.Int32)reader["VEHICLE_PICTURE_ID"]);
						if (hashKey_VehiclePicture.ContainsKey(key) == false)
						{
							hashKey_VehiclePicture.Add(key, key.VehiclePictureId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_Wb_Trip_Geo_Trip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[GEO_TRIP] t2 on t1.[GEO_TRIP_ID]=t2.[GEO_TRIP_ID] WHERE t2.[GEO_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_WB_TRIP_ORDER_TRIP(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[ORDER_TRIP] t2 on t1.[ORDER_TRIP_ID]=t2.[ORDER_TRIP_ID] WHERE t2.[ORDER_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_OrderTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_FK_WORK_TIME_DRIVER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[DRIVER_ID] FROM [dbo].[WORK_TIME] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WorkTimePrimaryKey key = new WorkTimePrimaryKey((System.Int32)reader["DRIVER_ID"]);
						if (hashKey_WorkTime.ContainsKey(key) == false)
						{
							hashKey_WorkTime.Add(key, key.DriverId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegment_Factors(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_ID] FROM [dbo].[GEO_SEGMENT] t1 inner join [dbo].[FACTORS] t2 on t1.[FACTOR_ID]=t2.[FACTOR_ID] WHERE t2.[FACTOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Factors.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentPrimaryKey key = new GeoSegmentPrimaryKey((System.Int32)reader["GEO_SEGMENT_ID"]);
						if (hashKey_GeoSegment.ContainsKey(key) == false)
						{
							hashKey_GeoSegment.Add(key, key.GeoSegmentId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegment_GeoTrip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_ID] FROM [dbo].[GEO_SEGMENT] t1 inner join [dbo].[GEO_TRIP] t2 on t1.[GEO_TRIP_ID]=t2.[GEO_TRIP_ID] WHERE t2.[GEO_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentPrimaryKey key = new GeoSegmentPrimaryKey((System.Int32)reader["GEO_SEGMENT_ID"]);
						if (hashKey_GeoSegment.ContainsKey(key) == false)
						{
							hashKey_GeoSegment.Add(key, key.GeoSegmentId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegment_Point(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_ID] FROM [dbo].[GEO_SEGMENT] t1 inner join [dbo].[POINT] t2 on t1.[POINT]=t2.[POINT_ID] WHERE t2.[POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Point.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentPrimaryKey key = new GeoSegmentPrimaryKey((System.Int32)reader["GEO_SEGMENT_ID"]);
						if (hashKey_GeoSegment.ContainsKey(key) == false)
						{
							hashKey_GeoSegment.Add(key, key.GeoSegmentId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegment_PointTo(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_ID] FROM [dbo].[GEO_SEGMENT] t1 inner join [dbo].[POINT] t2 on t1.[POINTTO]=t2.[POINT_ID] WHERE t2.[POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Point.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentPrimaryKey key = new GeoSegmentPrimaryKey((System.Int32)reader["GEO_SEGMENT_ID"]);
						if (hashKey_GeoSegment.ContainsKey(key) == false)
						{
							hashKey_GeoSegment.Add(key, key.GeoSegmentId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegmentVeretx_GeoSegment(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_VERTEX_ID] FROM [dbo].[GEO_SEGMENT_VERTEX] t1 inner join [dbo].[GEO_SEGMENT] t2 on t1.[GEO_SEGMENT_ID]=t2.[GEO_SEGMENT_ID] WHERE t2.[GEO_SEGMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoSegment.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentVertexPrimaryKey key = new GeoSegmentVertexPrimaryKey((System.Int32)reader["GEO_SEGMENT_VERTEX_ID"]);
						if (hashKey_GeoSegmentVertex.ContainsKey(key) == false)
						{
							hashKey_GeoSegmentVertex.Add(key, key.GeoSegmentVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoSegmentVeretx_MapVeretx(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_SEGMENT_VERTEX_ID] FROM [dbo].[GEO_SEGMENT_VERTEX] t1 inner join [dbo].[MAP_VERTEX] t2 on t1.[VERTEX_ID]=t2.[VERTEX_ID] WHERE t2.[VERTEX_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MapVertex.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoSegmentVertexPrimaryKey key = new GeoSegmentVertexPrimaryKey((System.Int32)reader["GEO_SEGMENT_VERTEX_ID"]);
						if (hashKey_GeoSegmentVertex.ContainsKey(key) == false)
						{
							hashKey_GeoSegmentVertex.Add(key, key.GeoSegmentVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoTrip_PointA(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_TRIP_ID] FROM [dbo].[GEO_TRIP] t1 inner join [dbo].[POINT] t2 on t1.[POINTA]=t2.[POINT_ID] WHERE t2.[POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Point.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoTripPrimaryKey key = new GeoTripPrimaryKey((System.Int32)reader["GEO_TRIP_ID"]);
						if (hashKey_GeoTrip.ContainsKey(key) == false)
						{
							hashKey_GeoTrip.Add(key, key.GeoTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoTrip_PointB(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_TRIP_ID] FROM [dbo].[GEO_TRIP] t1 inner join [dbo].[POINT] t2 on t1.[POINTB]=t2.[POINT_ID] WHERE t2.[POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Point.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoTripPrimaryKey key = new GeoTripPrimaryKey((System.Int32)reader["GEO_TRIP_ID"]);
						if (hashKey_GeoTrip.ContainsKey(key) == false)
						{
							hashKey_GeoTrip.Add(key, key.GeoTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoTrip_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_TRIP_ID] FROM [dbo].[GEO_TRIP] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoTripPrimaryKey key = new GeoTripPrimaryKey((System.Int32)reader["GEO_TRIP_ID"]);
						if (hashKey_GeoTrip.ContainsKey(key) == false)
						{
							hashKey_GeoTrip.Add(key, key.GeoTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoZonePrimitive_GeoZone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_ZONE_PRIMITIVE_ID] FROM [dbo].[GEO_ZONE_PRIMITIVE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoZonePrimitivePrimaryKey key = new GeoZonePrimitivePrimaryKey((System.Int32)reader["GEO_ZONE_PRIMITIVE_ID"]);
						if (hashKey_GeoZonePrimitive.ContainsKey(key) == false)
						{
							hashKey_GeoZonePrimitive.Add(key, key.GeoZonePrimitiveId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GeoZonePrimitive_ZonePrimitive(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[GEO_ZONE_PRIMITIVE_ID] FROM [dbo].[GEO_ZONE_PRIMITIVE] t1 inner join [dbo].[ZONE_PRIMITIVE] t2 on t1.[PRIMITIVE_ID]=t2.[PRIMITIVE_ID] WHERE t2.[PRIMITIVE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ZonePrimitive.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GeoZonePrimitivePrimaryKey key = new GeoZonePrimitivePrimaryKey((System.Int32)reader["GEO_ZONE_PRIMITIVE_ID"]);
						if (hashKey_GeoZonePrimitive.ContainsKey(key) == false)
						{
							hashKey_GeoZonePrimitive.Add(key, key.GeoZonePrimitiveId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_GraphicShift_Graphic(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ID] FROM [dbo].[GRAPHIC_SHIFT] t1 inner join [dbo].[GRAPHIC] t2 on t1.[GRAPHIC]=t2.[GRAPHIC_ID] WHERE t2.[GRAPHIC_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Graphic.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						GraphicShiftPrimaryKey key = new GraphicShiftPrimaryKey((System.Int32)reader["ID"]);
						if (hashKey_GraphicShift.ContainsKey(key) == false)
						{
							hashKey_GraphicShift.Add(key, key.Id);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Journal_Driver1(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_ID] FROM [dbo].[JOURNAL] t1 inner join [dbo].[DRIVER] t2 on t1.[REPLACED_DRIVER]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalPrimaryKey key = new JournalPrimaryKey((System.Int32)reader["JOURNAL_ID"]);
						if (hashKey_Journal.ContainsKey(key) == false)
						{
							hashKey_Journal.Add(key, key.JournalId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Journal_Wayout(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[JOURNAL_ID] FROM [dbo].[JOURNAL] t1 inner join [dbo].[WAYOUT] t2 on t1.[WAYOUT]=t2.[WAYOUT_ID] WHERE t2.[ROUTE] in (");
			bool first = true;
			foreach (int pk in hashKey_Wayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						JournalPrimaryKey key = new JournalPrimaryKey((System.Int32)reader["JOURNAL_ID"]);
						if (hashKey_Journal.ContainsKey(key) == false)
						{
							hashKey_Journal.Add(key, key.JournalId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogOpsEvents_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RECORD_ID] FROM [dbo].[LOG_OPS_EVENTS] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogOpsEventsPrimaryKey key = new LogOpsEventsPrimaryKey((System.Int32)reader["RECORD_ID"]);
						if (hashKey_LogOpsEvents.ContainsKey(key) == false)
						{
							hashKey_LogOpsEvents.Add(key, key.RecordId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogOpsEvents_OpsEvent(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RECORD_ID] FROM [dbo].[LOG_OPS_EVENTS] t1 inner join [dbo].[OPS_EVENT] t2 on t1.[OPS_EVENT_ID]=t2.[OPS_EVENT_ID] WHERE t2.[OPS_EVENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_OpsEvent.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogOpsEventsPrimaryKey key = new LogOpsEventsPrimaryKey((System.Int32)reader["RECORD_ID"]);
						if (hashKey_LogOpsEvents.ContainsKey(key) == false)
						{
							hashKey_LogOpsEvents.Add(key, key.RecordId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogVehicleStatus_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOG_VEHICLE_STATUS_ID] FROM [dbo].[LOG_VEHICLE_STATUS] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogVehicleStatusPrimaryKey key = new LogVehicleStatusPrimaryKey((System.Int32)reader["LOG_VEHICLE_STATUS_ID"]);
						if (hashKey_LogVehicleStatus.ContainsKey(key) == false)
						{
							hashKey_LogVehicleStatus.Add(key, key.LogVehicleStatusId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogVehicleStatus_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOG_VEHICLE_STATUS_ID] FROM [dbo].[LOG_VEHICLE_STATUS] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogVehicleStatusPrimaryKey key = new LogVehicleStatusPrimaryKey((System.Int32)reader["LOG_VEHICLE_STATUS_ID"]);
						if (hashKey_LogVehicleStatus.ContainsKey(key) == false)
						{
							hashKey_LogVehicleStatus.Add(key, key.LogVehicleStatusId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogVehicleStatus_VehicleStatus(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOG_VEHICLE_STATUS_ID] FROM [dbo].[LOG_VEHICLE_STATUS] t1 inner join [dbo].[VEHICLE_STATUS] t2 on t1.[VEHICLE_STATUS_ID]=t2.[VEHICLE_STATUS_ID] WHERE t2.[VEHICLE_STATUS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_VehicleStatus.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogVehicleStatusPrimaryKey key = new LogVehicleStatusPrimaryKey((System.Int32)reader["LOG_VEHICLE_STATUS_ID"]);
						if (hashKey_LogVehicleStatus.ContainsKey(key) == false)
						{
							hashKey_LogVehicleStatus.Add(key, key.LogVehicleStatusId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogWaybillHeaderStatus_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOG_WAYBILL_HEADER_STATUS_ID] FROM [dbo].[LOG_WAYBILL_HEADER_STATUS] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogWaybillHeaderStatusPrimaryKey key = new LogWaybillHeaderStatusPrimaryKey((System.Int32)reader["LOG_WAYBILL_HEADER_STATUS_ID"]);
						if (hashKey_LogWaybillHeaderStatus.ContainsKey(key) == false)
						{
							hashKey_LogWaybillHeaderStatus.Add(key, key.LogWaybillHeaderStatusId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_LogWaybillHeaderStatus_Schedule(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LOG_WAYBILL_HEADER_STATUS_ID] FROM [dbo].[LOG_WAYBILL_HEADER_STATUS] t1 inner join [dbo].[SCHEDULE] t2 on t1.[SCHEDULE_ID]=t2.[SCHEDULE_ID] WHERE t2.[SCHEDULE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedule.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						LogWaybillHeaderStatusPrimaryKey key = new LogWaybillHeaderStatusPrimaryKey((System.Int32)reader["LOG_WAYBILL_HEADER_STATUS_ID"]);
						if (hashKey_LogWaybillHeaderStatus.ContainsKey(key) == false)
						{
							hashKey_LogWaybillHeaderStatus.Add(key, key.LogWaybillHeaderStatusId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MapVeretx_Maps(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VERTEX_ID] FROM [dbo].[MAP_VERTEX] t1 inner join [dbo].[MAPS] t2 on t1.[MAP_ID]=t2.[MAP_ID] WHERE t2.[MAP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Maps.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MapVertexPrimaryKey key = new MapVertexPrimaryKey((System.Int32)reader["VERTEX_ID"]);
						if (hashKey_MapVertex.ContainsKey(key) == false)
						{
							hashKey_MapVertex.Add(key, key.VertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Media_MediaType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MEDIA_ID] FROM [dbo].[MEDIA] t1 inner join [dbo].[MEDIA_TYPE] t2 on t1.[TYPE]=t2.[ID] WHERE t2.[ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MediaType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MediaPrimaryKey key = new MediaPrimaryKey((System.Int32)reader["MEDIA_ID"]);
						if (hashKey_Media.ContainsKey(key) == false)
						{
							hashKey_Media.Add(key, key.MediaId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MediaAcceptors_Media(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MA_ID] FROM [dbo].[MEDIA_ACCEPTORS] t1 inner join [dbo].[MEDIA] t2 on t1.[MEDIA_ID]=t2.[MEDIA_ID] WHERE t2.[MEDIA_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Media.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MediaAcceptorsPrimaryKey key = new MediaAcceptorsPrimaryKey((System.Int32)reader["MA_ID"]);
						if (hashKey_MediaAcceptors.ContainsKey(key) == false)
						{
							hashKey_MediaAcceptors.Add(key, key.MaId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Message_MessageTemplate(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_ID] FROM [dbo].[MESSAGE] t1 inner join [dbo].[MESSAGE_TEMPLATE] t2 on t1.[TEMPLATE_ID]=t2.[MESSAGE_TEMPLATE_ID] WHERE t2.[MESSAGE_TEMPLATE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MessageTemplate.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessagePrimaryKey key = new MessagePrimaryKey((System.Int32)reader["MESSAGE_ID"]);
						if (hashKey_Message.ContainsKey(key) == false)
						{
							hashKey_Message.Add(key, key.MessageId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageField_Message(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_FIELD_ID] FROM [dbo].[MESSAGE_FIELD] t1 inner join [dbo].[MESSAGE] t2 on t1.[MESSAGE_ID]=t2.[MESSAGE_ID] WHERE t2.[MESSAGE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Message.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageFieldPrimaryKey key = new MessageFieldPrimaryKey((System.Int32)reader["MESSAGE_FIELD_ID"]);
						if (hashKey_MessageField.ContainsKey(key) == false)
						{
							hashKey_MessageField.Add(key, key.MessageFieldId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageField_MessageTemplateField(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_FIELD_ID] FROM [dbo].[MESSAGE_FIELD] t1 inner join [dbo].[MESSAGE_TEMPLATE_FIELD] t2 on t1.[MESSAGE_TEMPLATE_FIELD_ID]=t2.[MESSAGE_TEMPLATE_FIELD_ID] WHERE t2.[MESSAGE_TEMPLATE_FIELD_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MessageTemplateField.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageFieldPrimaryKey key = new MessageFieldPrimaryKey((System.Int32)reader["MESSAGE_FIELD_ID"]);
						if (hashKey_MessageField.ContainsKey(key) == false)
						{
							hashKey_MessageField.Add(key, key.MessageFieldId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageOperator_Message(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_OPERATOR_ID] FROM [dbo].[MESSAGE_OPERATOR] t1 inner join [dbo].[MESSAGE] t2 on t1.[MESSAGE_ID]=t2.[MESSAGE_ID] WHERE t2.[MESSAGE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Message.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageOperatorPrimaryKey key = new MessageOperatorPrimaryKey((System.Int32)reader["MESSAGE_OPERATOR_ID"]);
						if (hashKey_MessageOperator.ContainsKey(key) == false)
						{
							hashKey_MessageOperator.Add(key, key.MessageOperatorId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageOperator_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_OPERATOR_ID] FROM [dbo].[MESSAGE_OPERATOR] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageOperatorPrimaryKey key = new MessageOperatorPrimaryKey((System.Int32)reader["MESSAGE_OPERATOR_ID"]);
						if (hashKey_MessageOperator.ContainsKey(key) == false)
						{
							hashKey_MessageOperator.Add(key, key.MessageOperatorId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageTemplateField_DotnetType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_TEMPLATE_FIELD_ID] FROM [dbo].[MESSAGE_TEMPLATE_FIELD] t1 inner join [dbo].[DOTNET_TYPE] t2 on t1.[DOTNET_TYPE_ID]=t2.[DOTNET_TYPE_ID] WHERE t2.[DOTNET_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DotnetType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageTemplateFieldPrimaryKey key = new MessageTemplateFieldPrimaryKey((System.Int32)reader["MESSAGE_TEMPLATE_FIELD_ID"]);
						if (hashKey_MessageTemplateField.ContainsKey(key) == false)
						{
							hashKey_MessageTemplateField.Add(key, key.MessageTemplateFieldId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_MessageTemplateField_MessageTemplate(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MESSAGE_TEMPLATE_FIELD_ID] FROM [dbo].[MESSAGE_TEMPLATE_FIELD] t1 inner join [dbo].[MESSAGE_TEMPLATE] t2 on t1.[MESSAGE_TEMPLATE_ID]=t2.[MESSAGE_TEMPLATE_ID] WHERE t2.[MESSAGE_TEMPLATE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MessageTemplate.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MessageTemplateFieldPrimaryKey key = new MessageTemplateFieldPrimaryKey((System.Int32)reader["MESSAGE_TEMPLATE_FIELD_ID"]);
						if (hashKey_MessageTemplateField.ContainsKey(key) == false)
						{
							hashKey_MessageTemplateField.Add(key, key.MessageTemplateFieldId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OoperatorDriverGroup_DriverGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVERGROUP] t1 inner join [dbo].[DRIVERGROUP] t2 on t1.[DRIVERGROUP_ID]=t2.[DRIVERGROUP_ID] WHERE t2.[DRIVERGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Drivergroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDrivergroupPrimaryKey key = new OperatorDrivergroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDepartment_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DEPARTMENT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DEPARTMENT] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDepartmentPrimaryKey key = new OperatorDepartmentPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DEPARTMENT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDepartment.ContainsKey(key) == false)
						{
							hashKey_OperatorDepartment.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDepartment_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DEPARTMENT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DEPARTMENT] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDepartmentPrimaryKey key = new OperatorDepartmentPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DEPARTMENT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDepartment.ContainsKey(key) == false)
						{
							hashKey_OperatorDepartment.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDriver_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVER] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDriverPrimaryKey key = new OperatorDriverPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDriver_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVER] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDriverPrimaryKey key = new OperatorDriverPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDriver_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVER] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDriverPrimaryKey key = new OperatorDriverPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDriverGroup_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVERGROUP] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDrivergroupPrimaryKey key = new OperatorDrivergroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorDriverGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_DRIVERGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorDrivergroupPrimaryKey key = new OperatorDrivergroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDepartment_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DEPARTMENT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DEPARTMENT] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDepartmentPrimaryKey key = new OperatorgroupDepartmentPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DEPARTMENT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDepartment.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDepartment.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDepartment_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DEPARTMENT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DEPARTMENT] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDepartmentPrimaryKey key = new OperatorgroupDepartmentPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DEPARTMENT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDepartment.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDepartment.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriver_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVER] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDriverPrimaryKey key = new OperatorgroupDriverPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriver_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVER] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDriverPrimaryKey key = new OperatorgroupDriverPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriver_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVER_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVER] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDriverPrimaryKey key = new OperatorgroupDriverPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVER_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDriver.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDriver.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriverGroup_DriverGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVERGROUP] t1 inner join [dbo].[DRIVERGROUP] t2 on t1.[DRIVERGROUP_ID]=t2.[DRIVERGROUP_ID] WHERE t2.[DRIVERGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Drivergroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDrivergroupPrimaryKey key = new OperatorgroupDrivergroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriverGroup_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVERGROUP] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDrivergroupPrimaryKey key = new OperatorgroupDrivergroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupDriverGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[DRIVERGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_DRIVERGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupDrivergroupPrimaryKey key = new OperatorgroupDrivergroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["DRIVERGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupDrivergroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupDrivergroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupOperator_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[OPERATOR_ID] FROM [dbo].[OPERATORGROUP_OPERATOR] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupOperatorPrimaryKey key = new OperatorgroupOperatorPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["OPERATOR_ID"]);
						if (hashKey_OperatorgroupOperator.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupOperator.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupOperator_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[OPERATOR_ID] FROM [dbo].[OPERATORGROUP_OPERATOR] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupOperatorPrimaryKey key = new OperatorgroupOperatorPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["OPERATOR_ID"]);
						if (hashKey_OperatorgroupOperator.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupOperator.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorgroupReport_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_REPORT] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupReportPrimaryKey key = new OperatorgroupReportPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupReport.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorgroupReport_Report(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_REPORT] t1 inner join [dbo].[REPORT] t2 on t1.[REPORT_ID]=t2.[REPORT_ID] WHERE t2.[REPORT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Report.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupReportPrimaryKey key = new OperatorgroupReportPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupReport.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorgroupReport_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_REPORT] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupReportPrimaryKey key = new OperatorgroupReportPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupReport.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRoute_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTE] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutePrimaryKey key = new OperatorgroupRoutePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRoute_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutePrimaryKey key = new OperatorgroupRoutePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRoute_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTE] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutePrimaryKey key = new OperatorgroupRoutePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRouteGroup_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTEGROUP] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutegroupPrimaryKey key = new OperatorgroupRoutegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRouteGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutegroupPrimaryKey key = new OperatorgroupRoutegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupRouteGroup_RouteGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ROUTEGROUP] t1 inner join [dbo].[ROUTEGROUP] t2 on t1.[ROUTEGROUP_ID]=t2.[ROUTEGROUP_ID] WHERE t2.[ROUTEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Routegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupRoutegroupPrimaryKey key = new OperatorgroupRoutegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicle_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLE] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclePrimaryKey key = new OperatorgroupVehiclePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicle_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclePrimaryKey key = new OperatorgroupVehiclePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicle_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclePrimaryKey key = new OperatorgroupVehiclePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicleGroup_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLEGROUP] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclegroupPrimaryKey key = new OperatorgroupVehiclegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicleGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclegroupPrimaryKey key = new OperatorgroupVehiclegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupVehicleGroup_VehicleGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_VEHICLEGROUP] t1 inner join [dbo].[VEHICLEGROUP] t2 on t1.[VEHICLEGROUP_ID]=t2.[VEHICLEGROUP_ID] WHERE t2.[VEHICLEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehiclegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupVehiclegroupPrimaryKey key = new OperatorgroupVehiclegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZone_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONE] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonePrimaryKey key = new OperatorgroupZonePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZone.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZone_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonePrimaryKey key = new OperatorgroupZonePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZone.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZone_Zone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonePrimaryKey key = new OperatorgroupZonePrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZone.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZoneGroup_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONEGROUP] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonegroupPrimaryKey key = new OperatorgroupZonegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZoneGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonegroupPrimaryKey key = new OperatorgroupZonegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorGroupZoneGroup_ZoneGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATORGROUP_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATORGROUP_ZONEGROUP] t1 inner join [dbo].[ZONEGROUP] t2 on t1.[ZONEGROUP_ID]=t2.[ZONEGROUP_ID] WHERE t2.[ZONEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Zonegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorgroupZonegroupPrimaryKey key = new OperatorgroupZonegroupPrimaryKey((System.Int32)reader["OPERATORGROUP_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorgroupZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorgroupZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorReport_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_REPORT] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorReportPrimaryKey key = new OperatorReportPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorReport.ContainsKey(key) == false)
						{
							hashKey_OperatorReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorReport_Report(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_REPORT] t1 inner join [dbo].[REPORT] t2 on t1.[REPORT_ID]=t2.[REPORT_ID] WHERE t2.[REPORT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Report.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorReportPrimaryKey key = new OperatorReportPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorReport.ContainsKey(key) == false)
						{
							hashKey_OperatorReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorReport_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[REPORT_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_REPORT] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorReportPrimaryKey key = new OperatorReportPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["REPORT_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorReport.ContainsKey(key) == false)
						{
							hashKey_OperatorReport.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRoute_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTE] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutePrimaryKey key = new OperatorRoutePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRoute_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutePrimaryKey key = new OperatorRoutePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRoute_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTE] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutePrimaryKey key = new OperatorRoutePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoute.ContainsKey(key) == false)
						{
							hashKey_OperatorRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRouteGroup_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTEGROUP] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutegroupPrimaryKey key = new OperatorRoutegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRouteGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutegroupPrimaryKey key = new OperatorRoutegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorRouteGroup_RouteGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ROUTEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ROUTEGROUP] t1 inner join [dbo].[ROUTEGROUP] t2 on t1.[ROUTEGROUP_ID]=t2.[ROUTEGROUP_ID] WHERE t2.[ROUTEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Routegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorRoutegroupPrimaryKey key = new OperatorRoutegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorRoutegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorRoutegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicle_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLE] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclePrimaryKey key = new OperatorVehiclePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicle_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclePrimaryKey key = new OperatorVehiclePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicle_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclePrimaryKey key = new OperatorVehiclePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehicle.ContainsKey(key) == false)
						{
							hashKey_OperatorVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicleGroup_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLEGROUP] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclegroupPrimaryKey key = new OperatorVehiclegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicleGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclegroupPrimaryKey key = new OperatorVehiclegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorVehicleGroup_VehicleGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[VEHICLEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_VEHICLEGROUP] t1 inner join [dbo].[VEHICLEGROUP] t2 on t1.[VEHICLEGROUP_ID]=t2.[VEHICLEGROUP_ID] WHERE t2.[VEHICLEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehiclegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorVehiclegroupPrimaryKey key = new OperatorVehiclegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorVehiclegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZone_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONE] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonePrimaryKey key = new OperatorZonePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZone.ContainsKey(key) == false)
						{
							hashKey_OperatorZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZone_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONE] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonePrimaryKey key = new OperatorZonePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZone.ContainsKey(key) == false)
						{
							hashKey_OperatorZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZone_Zone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONE_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonePrimaryKey key = new OperatorZonePrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONE_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZone.ContainsKey(key) == false)
						{
							hashKey_OperatorZone.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZoneGroup_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONEGROUP] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonegroupPrimaryKey key = new OperatorZonegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZoneGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONEGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonegroupPrimaryKey key = new OperatorZonegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_OperatorZoneGroup_ZoneGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[OPERATOR_ID], t1.[ZONEGROUP_ID], t1.[RIGHT_ID] FROM [dbo].[OPERATOR_ZONEGROUP] t1 inner join [dbo].[ZONEGROUP] t2 on t1.[ZONEGROUP_ID]=t2.[ZONEGROUP_ID] WHERE t2.[ZONEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Zonegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OperatorZonegroupPrimaryKey key = new OperatorZonegroupPrimaryKey((System.Int32)reader["OPERATOR_ID"], (System.Int32)reader["ZONEGROUP_ID"], (System.Int32)reader["RIGHT_ID"]);
						if (hashKey_OperatorZonegroup.ContainsKey(key) == false)
						{
							hashKey_OperatorZonegroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Order_Customer(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ORDER_ID] FROM [dbo].[ORDER] t1 inner join [dbo].[CUSTOMER] t2 on t1.[CUSTOMER]=t2.[CUSTOMER_ID] WHERE t2.[CUSTOMER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Customer.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderPrimaryKey key = new OrderPrimaryKey((System.Int32)reader["ORDER_ID"]);
						if (hashKey_Order.ContainsKey(key) == false)
						{
							hashKey_Order.Add(key, key.OrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Order_OrderType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ORDER_ID] FROM [dbo].[ORDER] t1 inner join [dbo].[ORDER_TYPE] t2 on t1.[ORDER_TYPE]=t2.[ORDER_TYPE_ID] WHERE t2.[ORDER_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_OrderType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderPrimaryKey key = new OrderPrimaryKey((System.Int32)reader["ORDER_ID"]);
						if (hashKey_Order.ContainsKey(key) == false)
						{
							hashKey_Order.Add(key, key.OrderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Period_DayKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PERIOD_ID] FROM [dbo].[PERIOD] t1 inner join [dbo].[DAY_KIND] t2 on t1.[DAY_KIND]=t2.[DAY_KIND_ID] WHERE t2.[DAY_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PeriodPrimaryKey key = new PeriodPrimaryKey((System.Int32)reader["PERIOD_ID"]);
						if (hashKey_Period.ContainsKey(key) == false)
						{
							hashKey_Period.Add(key, key.PeriodId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Period_DkSet(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PERIOD_ID] FROM [dbo].[PERIOD] t1 inner join [dbo].[DK_SET] t2 on t1.[DK_SET]=t2.[DK_SET_ID] WHERE t2.[DK_SET_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DkSet.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PeriodPrimaryKey key = new PeriodPrimaryKey((System.Int32)reader["PERIOD_ID"]);
						if (hashKey_Period.ContainsKey(key) == false)
						{
							hashKey_Period.Add(key, key.PeriodId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Period_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PERIOD_ID] FROM [dbo].[PERIOD] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PeriodPrimaryKey key = new PeriodPrimaryKey((System.Int32)reader["PERIOD_ID"]);
						if (hashKey_Period.ContainsKey(key) == false)
						{
							hashKey_Period.Add(key, key.PeriodId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Period_Season(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PERIOD_ID] FROM [dbo].[PERIOD] t1 inner join [dbo].[SEASON] t2 on t1.[SEASON_ID]=t2.[SEASON_ID] WHERE t2.[SEASON_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Season.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PeriodPrimaryKey key = new PeriodPrimaryKey((System.Int32)reader["PERIOD_ID"]);
						if (hashKey_Period.ContainsKey(key) == false)
						{
							hashKey_Period.Add(key, key.PeriodId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Point_Busstop(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[POINT] t1 inner join [dbo].[BUSSTOP] t2 on t1.[BUSSTOP_ID]=t2.[BUSSTOP_ID] WHERE t2.[BUSSTOP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Busstop.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PointPrimaryKey key = new PointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_Point.ContainsKey(key) == false)
						{
							hashKey_Point.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Point_MapVeretx(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[POINT] t1 inner join [dbo].[MAP_VERTEX] t2 on t1.[VERTEX_ID]=t2.[VERTEX_ID] WHERE t2.[VERTEX_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MapVertex.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PointPrimaryKey key = new PointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_Point.ContainsKey(key) == false)
						{
							hashKey_Point.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Point_PointKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[POINT] t1 inner join [dbo].[POINT_KIND] t2 on t1.[POINT_KIND_ID]=t2.[POINT_KIND_ID] WHERE t2.[POINT_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_PointKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						PointPrimaryKey key = new PointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_Point.ContainsKey(key) == false)
						{
							hashKey_Point.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RightOperator_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RIGHT_ID], t1.[OPERATOR_ID] FROM [dbo].[RIGHT_OPERATOR] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RightOperatorPrimaryKey key = new RightOperatorPrimaryKey((System.Int32)reader["RIGHT_ID"], (System.Int32)reader["OPERATOR_ID"]);
						if (hashKey_RightOperator.ContainsKey(key) == false)
						{
							hashKey_RightOperator.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RightOperator_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RIGHT_ID], t1.[OPERATOR_ID] FROM [dbo].[RIGHT_OPERATOR] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RightOperatorPrimaryKey key = new RightOperatorPrimaryKey((System.Int32)reader["RIGHT_ID"], (System.Int32)reader["OPERATOR_ID"]);
						if (hashKey_RightOperator.ContainsKey(key) == false)
						{
							hashKey_RightOperator.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RightOperatorGroup_OperatorGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RIGHT_ID], t1.[OPERATORGROUP_ID] FROM [dbo].[RIGHT_OPERATORGROUP] t1 inner join [dbo].[OPERATORGROUP] t2 on t1.[OPERATORGROUP_ID]=t2.[OPERATORGROUP_ID] WHERE t2.[OPERATORGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operatorgroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RightOperatorgroupPrimaryKey key = new RightOperatorgroupPrimaryKey((System.Int32)reader["RIGHT_ID"], (System.Int32)reader["OPERATORGROUP_ID"]);
						if (hashKey_RightOperatorgroup.ContainsKey(key) == false)
						{
							hashKey_RightOperatorgroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RightOperatorGroup_Right(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[RIGHT_ID], t1.[OPERATORGROUP_ID] FROM [dbo].[RIGHT_OPERATORGROUP] t1 inner join [dbo].[RIGHT] t2 on t1.[RIGHT_ID]=t2.[RIGHT_ID] WHERE t2.[RIGHT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Right.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RightOperatorgroupPrimaryKey key = new RightOperatorgroupPrimaryKey((System.Int32)reader["RIGHT_ID"], (System.Int32)reader["OPERATORGROUP_ID"]);
						if (hashKey_RightOperatorgroup.ContainsKey(key) == false)
						{
							hashKey_RightOperatorgroup.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RouteGroupRoute_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTEGROUP_ID], t1.[ROUTE_ID] FROM [dbo].[ROUTEGROUP_ROUTE] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RoutegroupRoutePrimaryKey key = new RoutegroupRoutePrimaryKey((System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["ROUTE_ID"]);
						if (hashKey_RoutegroupRoute.ContainsKey(key) == false)
						{
							hashKey_RoutegroupRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RouteGroupRoute_RouteGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTEGROUP_ID], t1.[ROUTE_ID] FROM [dbo].[ROUTEGROUP_ROUTE] t1 inner join [dbo].[ROUTEGROUP] t2 on t1.[ROUTEGROUP_ID]=t2.[ROUTEGROUP_ID] WHERE t2.[ROUTEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Routegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RoutegroupRoutePrimaryKey key = new RoutegroupRoutePrimaryKey((System.Int32)reader["ROUTEGROUP_ID"], (System.Int32)reader["ROUTE_ID"]);
						if (hashKey_RoutegroupRoute.ContainsKey(key) == false)
						{
							hashKey_RoutegroupRoute.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RoutePoint_Point(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_POINT_ID] FROM [dbo].[ROUTE_POINT] t1 inner join [dbo].[POINT] t2 on t1.[POINT_ID]=t2.[POINT_ID] WHERE t2.[POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Point.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RoutePointPrimaryKey key = new RoutePointPrimaryKey((System.Int32)reader["ROUTE_POINT_ID"]);
						if (hashKey_RoutePoint.ContainsKey(key) == false)
						{
							hashKey_RoutePoint.Add(key, key.RoutePointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RoutePoint_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_POINT_ID] FROM [dbo].[ROUTE_POINT] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RoutePointPrimaryKey key = new RoutePointPrimaryKey((System.Int32)reader["ROUTE_POINT_ID"]);
						if (hashKey_RoutePoint.ContainsKey(key) == false)
						{
							hashKey_RoutePoint.Add(key, key.RoutePointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RouteTrip_GeoTrip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_TRIP_ID] FROM [dbo].[ROUTE_TRIP] t1 inner join [dbo].[GEO_TRIP] t2 on t1.[GEO_TRIP_ID]=t2.[GEO_TRIP_ID] WHERE t2.[GEO_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RouteTripPrimaryKey key = new RouteTripPrimaryKey((System.Int32)reader["ROUTE_TRIP_ID"]);
						if (hashKey_RouteTrip.ContainsKey(key) == false)
						{
							hashKey_RouteTrip.Add(key, key.RouteTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_RouteTrip_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ROUTE_TRIP_ID] FROM [dbo].[ROUTE_TRIP] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						RouteTripPrimaryKey key = new RouteTripPrimaryKey((System.Int32)reader["ROUTE_TRIP_ID"]);
						if (hashKey_RouteTrip.ContainsKey(key) == false)
						{
							hashKey_RouteTrip.Add(key, key.RouteTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Schedule_DayKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_ID] FROM [dbo].[SCHEDULE] t1 inner join [dbo].[DAY_KIND] t2 on t1.[DAY_KIND_ID]=t2.[DAY_KIND_ID] WHERE t2.[DAY_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_DayKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePrimaryKey key = new SchedulePrimaryKey((System.Int32)reader["SCHEDULE_ID"]);
						if (hashKey_Schedule.ContainsKey(key) == false)
						{
							hashKey_Schedule.Add(key, key.ScheduleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Schedule_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_ID] FROM [dbo].[SCHEDULE] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE_ID]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePrimaryKey key = new SchedulePrimaryKey((System.Int32)reader["SCHEDULE_ID"]);
						if (hashKey_Schedule.ContainsKey(key) == false)
						{
							hashKey_Schedule.Add(key, key.ScheduleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Schedule_Wayout(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_ID] FROM [dbo].[SCHEDULE] t1 inner join [dbo].[WAYOUT] t2 on t1.[WAYOUT]=t2.[WAYOUT_ID] WHERE t2.[ROUTE] in (");
			bool first = true;
			foreach (int pk in hashKey_Wayout.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePrimaryKey key = new SchedulePrimaryKey((System.Int32)reader["SCHEDULE_ID"]);
						if (hashKey_Schedule.ContainsKey(key) == false)
						{
							hashKey_Schedule.Add(key, key.ScheduleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ScheduleDetail_Schedule(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_DETAIL_ID] FROM [dbo].[SCHEDULE_DETAIL] t1 inner join [dbo].[SCHEDULE] t2 on t1.[SCHEDULE_ID]=t2.[SCHEDULE_ID] WHERE t2.[SCHEDULE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedule.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ScheduleDetailPrimaryKey key = new ScheduleDetailPrimaryKey((System.Int32)reader["SCHEDULE_DETAIL_ID"]);
						if (hashKey_ScheduleDetail.ContainsKey(key) == false)
						{
							hashKey_ScheduleDetail.Add(key, key.ScheduleDetailId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulePassage_SchedulePoint(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WH_ID], t1.[SP_ID] FROM [dbo].[SCHEDULE_PASSAGE] t1 inner join [dbo].[SCHEDULE_POINT] t2 on t1.[SP_ID]=t2.[SCHEDULE_POINT_ID] WHERE t2.[SCHEDULE_POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_SchedulePoint.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePassagePrimaryKey key = new SchedulePassagePrimaryKey((System.Int32)reader["WH_ID"], (System.Int32)reader["SP_ID"]);
						if (hashKey_SchedulePassage.ContainsKey(key) == false)
						{
							hashKey_SchedulePassage.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulePassage_WaybillHeader(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WH_ID], t1.[SP_ID] FROM [dbo].[SCHEDULE_PASSAGE] t1 inner join [dbo].[WAYBILL_HEADER] t2 on t1.[WH_ID]=t2.[WAYBILL_HEADER_ID] WHERE t2.[WAYBILL_HEADER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WaybillHeader.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePassagePrimaryKey key = new SchedulePassagePrimaryKey((System.Int32)reader["WH_ID"], (System.Int32)reader["SP_ID"]);
						if (hashKey_SchedulePassage.ContainsKey(key) == false)
						{
							hashKey_SchedulePassage.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulePoint_GeoSegment(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_POINT_ID] FROM [dbo].[SCHEDULE_POINT] t1 inner join [dbo].[GEO_SEGMENT] t2 on t1.[GEO_SEGMENT_ID]=t2.[GEO_SEGMENT_ID] WHERE t2.[GEO_SEGMENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoSegment.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePointPrimaryKey key = new SchedulePointPrimaryKey((System.Int32)reader["SCHEDULE_POINT_ID"]);
						if (hashKey_SchedulePoint.ContainsKey(key) == false)
						{
							hashKey_SchedulePoint.Add(key, key.SchedulePointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulePoint_RoutePoint(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_POINT_ID] FROM [dbo].[SCHEDULE_POINT] t1 inner join [dbo].[ROUTE_POINT] t2 on t1.[ROUTE_POINT_ID]=t2.[ROUTE_POINT_ID] WHERE t2.[ROUTE_POINT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RoutePoint.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePointPrimaryKey key = new SchedulePointPrimaryKey((System.Int32)reader["SCHEDULE_POINT_ID"]);
						if (hashKey_SchedulePoint.ContainsKey(key) == false)
						{
							hashKey_SchedulePoint.Add(key, key.SchedulePointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulePoint_Trip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULE_POINT_ID] FROM [dbo].[SCHEDULE_POINT] t1 inner join [dbo].[TRIP] t2 on t1.[TRIP_ID]=t2.[TRIP_ID] WHERE t2.[TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Trip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulePointPrimaryKey key = new SchedulePointPrimaryKey((System.Int32)reader["SCHEDULE_POINT_ID"]);
						if (hashKey_SchedulePoint.ContainsKey(key) == false)
						{
							hashKey_SchedulePoint.Add(key, key.SchedulePointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_SchedulerQueue_SchedulerEvent(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SCHEDULERQUEUE_ID] FROM [dbo].[SCHEDULERQUEUE] t1 inner join [dbo].[SCHEDULEREVENT] t2 on t1.[SCHEDULEREVENT_ID]=t2.[SCHEDULEREVENT_ID] WHERE t2.[SCHEDULEREVENT_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedulerevent.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SchedulerqueuePrimaryKey key = new SchedulerqueuePrimaryKey((System.Int32)reader["SCHEDULERQUEUE_ID"]);
						if (hashKey_Schedulerqueue.ContainsKey(key) == false)
						{
							hashKey_Schedulerqueue.Add(key, key.SchedulerqueueId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Trail_Session(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRAIL_ID] FROM [dbo].[TRAIL] t1 inner join [dbo].[SESSION] t2 on t1.[SESSION_ID]=t2.[SESSION_ID] WHERE t2.[SESSION_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Session.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TrailPrimaryKey key = new TrailPrimaryKey((System.Int32)reader["TRAIL_ID"]);
						if (hashKey_Trail.ContainsKey(key) == false)
						{
							hashKey_Trail.Add(key, key.TrailId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Trip_RouteTrip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRIP_ID] FROM [dbo].[TRIP] t1 inner join [dbo].[ROUTE_TRIP] t2 on t1.[ROUTE_TRIP_ID]=t2.[ROUTE_TRIP_ID] WHERE t2.[ROUTE_TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_RouteTrip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TripPrimaryKey key = new TripPrimaryKey((System.Int32)reader["TRIP_ID"]);
						if (hashKey_Trip.ContainsKey(key) == false)
						{
							hashKey_Trip.Add(key, key.TripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Trip_ScheduleDetail(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRIP_ID] FROM [dbo].[TRIP] t1 inner join [dbo].[SCHEDULE_DETAIL] t2 on t1.[SCHEDULE_DETAIL_ID]=t2.[SCHEDULE_DETAIL_ID] WHERE t2.[SCHEDULE_DETAIL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ScheduleDetail.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TripPrimaryKey key = new TripPrimaryKey((System.Int32)reader["TRIP_ID"]);
						if (hashKey_Trip.ContainsKey(key) == false)
						{
							hashKey_Trip.Add(key, key.TripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Trip_TripKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRIP_ID] FROM [dbo].[TRIP] t1 inner join [dbo].[TRIP_KIND] t2 on t1.[TRIP_KIND_ID]=t2.[TRIP_KIND_ID] WHERE t2.[TRIP_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_TripKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TripPrimaryKey key = new TripPrimaryKey((System.Int32)reader["TRIP_ID"]);
						if (hashKey_Trip.ContainsKey(key) == false)
						{
							hashKey_Trip.Add(key, key.TripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_TripKind_WorktimeStatusType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRIP_KIND_ID] FROM [dbo].[TRIP_KIND] t1 inner join [dbo].[WORKTIME_STATUS_TYPE] t2 on t1.[WORKTIME_STATUS_TYPE_ID]=t2.[WORKTIME_STATUS_TYPE_ID] WHERE t2.[WORKTIME_STATUS_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WorktimeStatusType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TripKindPrimaryKey key = new TripKindPrimaryKey((System.Int32)reader["TRIP_KIND_ID"]);
						if (hashKey_TripKind.ContainsKey(key) == false)
						{
							hashKey_TripKind.Add(key, key.TripKindId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Vehicle_Graphic(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_ID] FROM [dbo].[VEHICLE] t1 inner join [dbo].[GRAPHIC] t2 on t1.[GRAPHIC]=t2.[GRAPHIC_ID] WHERE t2.[GRAPHIC_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Graphic.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclePrimaryKey key = new VehiclePrimaryKey((System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_Vehicle.ContainsKey(key) == false)
						{
							hashKey_Vehicle.Add(key, key.VehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Vehicle_VehicleKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_ID] FROM [dbo].[VEHICLE] t1 inner join [dbo].[VEHICLE_KIND] t2 on t1.[VEHICLE_KIND_ID]=t2.[VEHICLE_KIND_ID] WHERE t2.[VEHICLE_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_VehicleKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclePrimaryKey key = new VehiclePrimaryKey((System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_Vehicle.ContainsKey(key) == false)
						{
							hashKey_Vehicle.Add(key, key.VehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Vehicle_VehicleStatus(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_ID] FROM [dbo].[VEHICLE] t1 inner join [dbo].[VEHICLE_STATUS] t2 on t1.[VEHICLE_STATUS_ID]=t2.[VEHICLE_STATUS_ID] WHERE t2.[VEHICLE_STATUS_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_VehicleStatus.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclePrimaryKey key = new VehiclePrimaryKey((System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_Vehicle.ContainsKey(key) == false)
						{
							hashKey_Vehicle.Add(key, key.VehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehiclegroupRule_Rule(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLEGROUP_RULE_ID] FROM [dbo].[VEHICLEGROUP_RULE] t1 inner join [dbo].[RULE] t2 on t1.[RULE_ID]=t2.[RULE_ID] WHERE t2.[RULE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Rule.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclegroupRulePrimaryKey key = new VehiclegroupRulePrimaryKey((System.Int32)reader["VEHICLEGROUP_RULE_ID"]);
						if (hashKey_VehiclegroupRule.ContainsKey(key) == false)
						{
							hashKey_VehiclegroupRule.Add(key, key.VehiclegroupRuleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehiclegroupRule_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLEGROUP_RULE_ID] FROM [dbo].[VEHICLEGROUP_RULE] t1 inner join [dbo].[VEHICLEGROUP] t2 on t1.[VEHICLEGROUP_ID]=t2.[VEHICLEGROUP_ID] WHERE t2.[VEHICLEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehiclegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclegroupRulePrimaryKey key = new VehiclegroupRulePrimaryKey((System.Int32)reader["VEHICLEGROUP_RULE_ID"]);
						if (hashKey_VehiclegroupRule.ContainsKey(key) == false)
						{
							hashKey_VehiclegroupRule.Add(key, key.VehiclegroupRuleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehicleGroupVehicle_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLEGROUP_ID], t1.[VEHICLE_ID] FROM [dbo].[VEHICLEGROUP_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclegroupVehiclePrimaryKey key = new VehiclegroupVehiclePrimaryKey((System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_VehiclegroupVehicle.ContainsKey(key) == false)
						{
							hashKey_VehiclegroupVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehicleGroupVehicle_VehicleGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLEGROUP_ID], t1.[VEHICLE_ID] FROM [dbo].[VEHICLEGROUP_VEHICLE] t1 inner join [dbo].[VEHICLEGROUP] t2 on t1.[VEHICLEGROUP_ID]=t2.[VEHICLEGROUP_ID] WHERE t2.[VEHICLEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehiclegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehiclegroupVehiclePrimaryKey key = new VehiclegroupVehiclePrimaryKey((System.Int32)reader["VEHICLEGROUP_ID"], (System.Int32)reader["VEHICLE_ID"]);
						if (hashKey_VehiclegroupVehicle.ContainsKey(key) == false)
						{
							hashKey_VehiclegroupVehicle.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehicleRule_Rule(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_RULE_ID] FROM [dbo].[VEHICLE_RULE] t1 inner join [dbo].[RULE] t2 on t1.[RULE_ID]=t2.[RULE_ID] WHERE t2.[RULE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Rule.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehicleRulePrimaryKey key = new VehicleRulePrimaryKey((System.Int32)reader["VEHICLE_RULE_ID"]);
						if (hashKey_VehicleRule.ContainsKey(key) == false)
						{
							hashKey_VehicleRule.Add(key, key.VehicleRuleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_VehicleRule_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[VEHICLE_RULE_ID] FROM [dbo].[VEHICLE_RULE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						VehicleRulePrimaryKey key = new VehicleRulePrimaryKey((System.Int32)reader["VEHICLE_RULE_ID"]);
						if (hashKey_VehicleRule.ContainsKey(key) == false)
						{
							hashKey_VehicleRule.Add(key, key.VehicleRuleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Waybill_Driver(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_ID] FROM [dbo].[WAYBILL] t1 inner join [dbo].[DRIVER] t2 on t1.[DRIVER_ID]=t2.[DRIVER_ID] WHERE t2.[DRIVER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Driver.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillPrimaryKey key = new WaybillPrimaryKey((System.Int32)reader["WAYBILL_ID"]);
						if (hashKey_Waybill.ContainsKey(key) == false)
						{
							hashKey_Waybill.Add(key, key.WaybillId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillHeader_Schedule(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] t1 inner join [dbo].[SCHEDULE] t2 on t1.[SCHEDULE_ID]=t2.[SCHEDULE_ID] WHERE t2.[SCHEDULE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Schedule.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey((System.Int32)reader["WAYBILL_HEADER_ID"]);
						if (hashKey_WaybillHeader.ContainsKey(key) == false)
						{
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillHeader_Trailor1(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] t1 inner join [dbo].[VEHICLE] t2 on t1.[TRAILOR1]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey((System.Int32)reader["WAYBILL_HEADER_ID"]);
						if (hashKey_WaybillHeader.ContainsKey(key) == false)
						{
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillHeader_Trailor2(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] t1 inner join [dbo].[VEHICLE] t2 on t1.[TRAILOR2]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey((System.Int32)reader["WAYBILL_HEADER_ID"]);
						if (hashKey_WaybillHeader.ContainsKey(key) == false)
						{
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillHeader_TripKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] t1 inner join [dbo].[TRIP_KIND] t2 on t1.[INITIAL_TRIP_KIND]=t2.[TRIP_KIND_ID] WHERE t2.[TRIP_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_TripKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey((System.Int32)reader["WAYBILL_HEADER_ID"]);
						if (hashKey_WaybillHeader.ContainsKey(key) == false)
						{
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillHeader_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillHeaderPrimaryKey key = new WaybillHeaderPrimaryKey((System.Int32)reader["WAYBILL_HEADER_ID"]);
						if (hashKey_WaybillHeader.ContainsKey(key) == false)
						{
							hashKey_WaybillHeader.Add(key, key.WaybillHeaderId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Waybillheader_Waybillmark_Waybillmark(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILLHEADER_ID], t1.[WAYBILLMARK_ID] FROM [dbo].[WAYBILLHEADER_WAYBILLMARK] t1 inner join [dbo].[WAYBILLMARK] t2 on t1.[WAYBILLMARK_ID]=t2.[WAYBILLMARK_ID] WHERE t2.[WAYBILLMARK_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Waybillmark.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillheaderWaybillmarkPrimaryKey key = new WaybillheaderWaybillmarkPrimaryKey((System.Int32)reader["WAYBILLHEADER_ID"], (System.Int32)reader["WAYBILLMARK_ID"]);
						if (hashKey_WaybillheaderWaybillmark.ContainsKey(key) == false)
						{
							hashKey_WaybillheaderWaybillmark.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WaybillheaderWaybillmark_WaybillHeader(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYBILLHEADER_ID], t1.[WAYBILLMARK_ID] FROM [dbo].[WAYBILLHEADER_WAYBILLMARK] t1 inner join [dbo].[WAYBILL_HEADER] t2 on t1.[WAYBILLHEADER_ID]=t2.[WAYBILL_HEADER_ID] WHERE t2.[WAYBILL_HEADER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WaybillHeader.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WaybillheaderWaybillmarkPrimaryKey key = new WaybillheaderWaybillmarkPrimaryKey((System.Int32)reader["WAYBILLHEADER_ID"], (System.Int32)reader["WAYBILLMARK_ID"]);
						if (hashKey_WaybillheaderWaybillmark.ContainsKey(key) == false)
						{
							hashKey_WaybillheaderWaybillmark.Add(key, null);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Wayout_Graphic(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYOUT_ID] FROM [dbo].[WAYOUT] t1 inner join [dbo].[GRAPHIC] t2 on t1.[graphic]=t2.[GRAPHIC_ID] WHERE t2.[GRAPHIC_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Graphic.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WayoutPrimaryKey key = new WayoutPrimaryKey((System.Int32)reader["WAYOUT_ID"]);
						if (hashKey_Wayout.ContainsKey(key) == false)
						{
							hashKey_Wayout.Add(key, key.WayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_Wayout_Route(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WAYOUT_ID] FROM [dbo].[WAYOUT] t1 inner join [dbo].[ROUTE] t2 on t1.[ROUTE]=t2.[ROUTE_ID] WHERE t2.[ROUTE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Route.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WayoutPrimaryKey key = new WayoutPrimaryKey((System.Int32)reader["WAYOUT_ID"]);
						if (hashKey_Wayout.ContainsKey(key) == false)
						{
							hashKey_Wayout.Add(key, key.WayoutId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WbTrip_Trip(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[TRIP] t2 on t1.[TRIP_ID]=t2.[TRIP_ID] WHERE t2.[TRIP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Trip.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WbTrip_TripKind(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[TRIP_KIND] t2 on t1.[TRIP_KIND_ID]=t2.[TRIP_KIND_ID] WHERE t2.[TRIP_KIND_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_TripKind.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WbTrip_Waybill(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[WAYBILL] t2 on t1.[WAYBILL_ID]=t2.[WAYBILL_ID] WHERE t2.[WAYBILL_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Waybill.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WbTrip_WaybillHeader(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[WAYBILL_HEADER] t2 on t1.[WAYBILL_HEADER_ID]=t2.[WAYBILL_HEADER_ID] WHERE t2.[WAYBILL_HEADER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WaybillHeader.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WbTrip_WorktimeReason(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WB_TRIP_ID] FROM [dbo].[WB_TRIP] t1 inner join [dbo].[WORKTIME_REASON] t2 on t1.[WORKTIME_REASON_ID]=t2.[WORKTIME_REASON_ID] WHERE t2.[WORKTIME_REASON_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WorktimeReason.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WbTripPrimaryKey key = new WbTripPrimaryKey((System.Int32)reader["WB_TRIP_ID"]);
						if (hashKey_WbTrip.ContainsKey(key) == false)
						{
							hashKey_WbTrip.Add(key, key.WbTripId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebLine_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LINE_ID] FROM [dbo].[WEB_LINE] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebLinePrimaryKey key = new WebLinePrimaryKey((System.Int32)reader["LINE_ID"]);
						if (hashKey_WebLine.ContainsKey(key) == false)
						{
							hashKey_WebLine.Add(key, key.LineId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebLineVertex_Vertex(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LINE_VERTEX_ID] FROM [dbo].[WEB_LINE_VERTEX] t1 inner join [dbo].[MAP_VERTEX] t2 on t1.[VERTEX_ID]=t2.[VERTEX_ID] WHERE t2.[VERTEX_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MapVertex.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebLineVertexPrimaryKey key = new WebLineVertexPrimaryKey((System.Int32)reader["LINE_VERTEX_ID"]);
						if (hashKey_WebLineVertex.ContainsKey(key) == false)
						{
							hashKey_WebLineVertex.Add(key, key.LineVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebLineVertex_WebLine(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[LINE_VERTEX_ID] FROM [dbo].[WEB_LINE_VERTEX] t1 inner join [dbo].[WEB_LINE] t2 on t1.[LINE_ID]=t2.[LINE_ID] WHERE t2.[LINE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WebLine.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebLineVertexPrimaryKey key = new WebLineVertexPrimaryKey((System.Int32)reader["LINE_VERTEX_ID"]);
						if (hashKey_WebLineVertex.ContainsKey(key) == false)
						{
							hashKey_WebLineVertex.Add(key, key.LineVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebPoint_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[WEB_POINT] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebPointPrimaryKey key = new WebPointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_WebPoint.ContainsKey(key) == false)
						{
							hashKey_WebPoint.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebPoint_Type(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[WEB_POINT] t1 inner join [dbo].[WEB_POINT_TYPE] t2 on t1.[TYPE_ID]=t2.[TYPE_ID] WHERE t2.[TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WebPointType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebPointPrimaryKey key = new WebPointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_WebPoint.ContainsKey(key) == false)
						{
							hashKey_WebPoint.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WebPoint_Vertex(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[POINT_ID] FROM [dbo].[WEB_POINT] t1 inner join [dbo].[MAP_VERTEX] t2 on t1.[VERTEX_ID]=t2.[VERTEX_ID] WHERE t2.[VERTEX_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MapVertex.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WebPointPrimaryKey key = new WebPointPrimaryKey((System.Int32)reader["POINT_ID"]);
						if (hashKey_WebPoint.ContainsKey(key) == false)
						{
							hashKey_WebPoint.Add(key, key.PointId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_WorktimeReason_WorktimeStatusType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[WORKTIME_REASON_ID] FROM [dbo].[WORKTIME_REASON] t1 inner join [dbo].[WORKTIME_STATUS_TYPE] t2 on t1.[WORKTIME_STATUS_TYPE_ID]=t2.[WORKTIME_STATUS_TYPE_ID] WHERE t2.[WORKTIME_STATUS_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_WorktimeStatusType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						WorktimeReasonPrimaryKey key = new WorktimeReasonPrimaryKey((System.Int32)reader["WORKTIME_REASON_ID"]);
						if (hashKey_WorktimeReason.ContainsKey(key) == false)
						{
							hashKey_WorktimeReason.Add(key, key.WorktimeReasonId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneGroupZone_Zone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONEGROUP_ZONE_ID] FROM [dbo].[ZONEGROUP_ZONE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZonegroupZonePrimaryKey key = new ZonegroupZonePrimaryKey((System.Int32)reader["ZONEGROUP_ZONE_ID"]);
						if (hashKey_ZonegroupZone.ContainsKey(key) == false)
						{
							hashKey_ZonegroupZone.Add(key, key.ZonegroupZoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneGroupZone_ZoneGroup(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONEGROUP_ZONE_ID] FROM [dbo].[ZONEGROUP_ZONE] t1 inner join [dbo].[ZONEGROUP] t2 on t1.[ZONEGROUP_ID]=t2.[ZONEGROUP_ID] WHERE t2.[ZONEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Zonegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZonegroupZonePrimaryKey key = new ZonegroupZonePrimaryKey((System.Int32)reader["ZONEGROUP_ZONE_ID"]);
						if (hashKey_ZonegroupZone.ContainsKey(key) == false)
						{
							hashKey_ZonegroupZone.Add(key, key.ZonegroupZoneId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZonePrimitive_ZoneType(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PRIMITIVE_ID] FROM [dbo].[ZONE_PRIMITIVE] t1 inner join [dbo].[ZONE_TYPE] t2 on t1.[PRIMITIVE_TYPE]=t2.[ZONE_TYPE_ID] WHERE t2.[ZONE_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ZoneType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZonePrimitivePrimaryKey key = new ZonePrimitivePrimaryKey((System.Int32)reader["PRIMITIVE_ID"]);
						if (hashKey_ZonePrimitive.ContainsKey(key) == false)
						{
							hashKey_ZonePrimitive.Add(key, key.PrimitiveId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZonePrimitiveVertex_MapVeretx(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PRIMITIVE_VERTEX_ID] FROM [dbo].[ZONE_PRIMITIVE_VERTEX] t1 inner join [dbo].[MAP_VERTEX] t2 on t1.[VERTEX_ID]=t2.[VERTEX_ID] WHERE t2.[VERTEX_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MapVertex.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZonePrimitiveVertexPrimaryKey key = new ZonePrimitiveVertexPrimaryKey((System.Int32)reader["PRIMITIVE_VERTEX_ID"]);
						if (hashKey_ZonePrimitiveVertex.ContainsKey(key) == false)
						{
							hashKey_ZonePrimitiveVertex.Add(key, key.PrimitiveVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZonePrimitiveVertex_ZonePrimitive(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[PRIMITIVE_VERTEX_ID] FROM [dbo].[ZONE_PRIMITIVE_VERTEX] t1 inner join [dbo].[ZONE_PRIMITIVE] t2 on t1.[PRIMITIVE_ID]=t2.[PRIMITIVE_ID] WHERE t2.[PRIMITIVE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ZonePrimitive.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZonePrimitiveVertexPrimaryKey key = new ZonePrimitiveVertexPrimaryKey((System.Int32)reader["PRIMITIVE_VERTEX_ID"]);
						if (hashKey_ZonePrimitiveVertex.ContainsKey(key) == false)
						{
							hashKey_ZonePrimitiveVertex.Add(key, key.PrimitiveVertexId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneVehicle_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONE_VEHICLE_ID] FROM [dbo].[ZONE_VEHICLE] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZoneVehiclePrimaryKey key = new ZoneVehiclePrimaryKey((System.Int32)reader["ZONE_VEHICLE_ID"]);
						if (hashKey_ZoneVehicle.ContainsKey(key) == false)
						{
							hashKey_ZoneVehicle.Add(key, key.ZoneVehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneVehicle_Zone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONE_VEHICLE_ID] FROM [dbo].[ZONE_VEHICLE] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZoneVehiclePrimaryKey key = new ZoneVehiclePrimaryKey((System.Int32)reader["ZONE_VEHICLE_ID"]);
						if (hashKey_ZoneVehicle.ContainsKey(key) == false)
						{
							hashKey_ZoneVehicle.Add(key, key.ZoneVehicleId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneVehicleGroup_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONE_VEHICLEGROUP_ID] FROM [dbo].[ZONE_VEHICLEGROUP] t1 inner join [dbo].[VEHICLEGROUP] t2 on t1.[VEHICLEGROUP_ID]=t2.[VEHICLEGROUP_ID] WHERE t2.[VEHICLEGROUP_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehiclegroup.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZoneVehiclegroupPrimaryKey key = new ZoneVehiclegroupPrimaryKey((System.Int32)reader["ZONE_VEHICLEGROUP_ID"]);
						if (hashKey_ZoneVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_ZoneVehiclegroup.Add(key, key.ZoneVehiclegroupId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}

		public virtual void LoadRecords_ZoneVehicleGroup_Zone(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[ZONE_VEHICLEGROUP_ID] FROM [dbo].[ZONE_VEHICLEGROUP] t1 inner join [dbo].[GEO_ZONE] t2 on t1.[ZONE_ID]=t2.[ZONE_ID] WHERE t2.[ZONE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_GeoZone.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ZoneVehiclegroupPrimaryKey key = new ZoneVehiclegroupPrimaryKey((System.Int32)reader["ZONE_VEHICLEGROUP_ID"]);
						if (hashKey_ZoneVehiclegroup.ContainsKey(key) == false)
						{
							hashKey_ZoneVehiclegroup.Add(key, key.ZoneVehiclegroupId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
	}
}