﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Interfaces.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AddTrackerResult = FORIS.TSS.BusinessLogic.ResultCodes.AddTrackerResult;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class ServerTests
	{
		[TestMethod]
		public void AddBadTracker()
		{
			var server = Helper.CreateNewPhysicalUser();
			var result = server.AddTrackerByPhoneNumber("+7(926)731-54-59 доваор Ж");
			Assert.AreEqual(result.Code, AddTrackerResult.InvalidPhoneNumber);
		}

		[TestMethod]
		public void RegisterMsisdn()
		{
			Register(Helper._msisdnPrefixes.First());
		}


		private static void Register(string prefix)
		{
			var msisdn = Helper.FindFreeMsisdn(prefix);

			//Вначале регистрация
			int operatorId = Helper.GetOrCreateUserByMsisdn(msisdn).OperatorId;
			//Затем восстановление доступа
			Assert.AreEqual(operatorId, Helper.GetOrCreateUserByMsisdn(msisdn).OperatorId);
		}

		[TestMethod]
		public void AskPositionOfSmartphones()
		{
			RunPositionRequests();
		}

		[TestMethod]
		public void AskPositionOfSmartphonesScheduled()
		{
			RunPositionRequests(true);
		}

		private void RunPositionRequests(bool scheduled = false)
		{
			//TODO: добавить проверку текстов сообщений на соответствие требованиям

			var androidUser = RegisterAndroidUser();
			var androidVehicle = androidUser.GetVehicles().Single();

			var appleUser = RegisterAppleUser();
			var appleVehicle = appleUser.GetVehicles().Single();
			SetPhoneBook(androidUser, appleUser, "Эпловод");
			SetPhoneBook(appleUser, androidUser, "Дроидовод");

			appleUser.ProposeFriendship(androidUser.GetMsisdn());
			androidUser.SetReplacedAccessToVehicle(appleUser.OperatorId, androidVehicle.id, SystemRight.VehicleAccess,
				true);

			var commandDto = appleUser.RegisterCommand(CreatesAskPositionCommand(androidVehicle, scheduled));
			Assert.AreEqual(CmdResult.Received, commandDto.Result);

			var commandProcessorHelper = new CommandProcessorHelper(delegate { return null; });
			using (var entities = new Entities())
			{
				var command = entities.Command.First(c => c.ID == commandDto.Id);
				Assert.IsNotNull(command.Type.ExecutionTimeout);
				command.Status = (int)CmdStatus.Processing;
				command.Date_Received = DateTime.UtcNow.AddSeconds(-command.Type.ExecutionTimeout.Value - 1);
				entities.SaveChanges();

				commandProcessorHelper.ProcessLateCommands(entities);
				Assert.AreEqual(CmdStatus.Completed, (CmdStatus)command.Status);
			}

			Assert.AreEqual(
				SetAccessToVehicleResult.AccessGranted,
				appleUser.SetReplacedAccessToVehicle(androidUser.GetMsisdn(), appleVehicle.id, SystemRight.VehicleAccess,
					true));

			if (scheduled)
			{
				var now = DateTime.Now;
				var today = now.Date;
				var startTime = now.TimeOfDay;
				var schedule = new RepeatEveryTime
				{
					Start = today,
					End = today.AddDays(2),
					StartTime = startTime,
					EndTime = startTime.Add(TimeSpan.FromSeconds(-1))
				};
				schedule.SetInterval(TimeSpan.FromMinutes(3));

				androidUser.SetMlpSchedule(IdType.Vehicle, appleVehicle.id, schedule);
			}

			commandDto = androidUser.RegisterCommand(CreatesAskPositionCommand(appleVehicle, scheduled));
			Assert.AreEqual(CmdResult.Received, commandDto.Result);
			var commandId = commandDto.Id;

			using (var entities = new Entities())
			{
				var command = entities.Command.First(c => c.ID == commandId);
				Assert.AreEqual(CmdStatus.Received, (CmdStatus)command.Status);
				commandProcessorHelper.CheckoutCommandForProcessing(command);
				Assert.IsTrue(entities.SaveUnsavedChanges());
				commandProcessorHelper.ProcessCommand(entities, command);
				Assert.IsTrue(entities.SaveUnsavedChanges());

				Assert.IsNotNull(command.Type.ExecutionTimeout);
				command.Status = (int)CmdStatus.Processing;
				command.Date_Received = DateTime.UtcNow.AddSeconds(-command.Type.ExecutionTimeout.Value - 1);
				Assert.IsTrue(entities.SaveUnsavedChanges());

				commandProcessorHelper.ProcessLateCommands(entities);
				Assert.AreEqual(CmdStatus.Completed, (CmdStatus)command.Status);
			}

			commandDto = androidUser.RegisterCommand(CreatesAskPositionCommand(appleVehicle, scheduled));
			Assert.AreEqual(CmdResult.Received, commandDto.Result);
			commandId = commandDto.Id;

			using (var entities = new Entities())
			{
				var command = entities.Command.First(c => c.ID == commandId);
				Assert.AreEqual(CmdStatus.Received, (CmdStatus)command.Status);
				commandProcessorHelper.CheckoutCommandForProcessing(command);
				Assert.IsTrue(entities.SaveUnsavedChanges());
				commandProcessorHelper.ProcessCommand(entities, command);
				Assert.IsTrue(entities.SaveUnsavedChanges());
				Assert.AreEqual(CmdStatus.Processing, (CmdStatus)command.Status);

				commandProcessorHelper.ProcessReceiveEvent(
					new ReceiveEventArgs(new MobilUnit
					{
						Unique = appleVehicle.id,
						Time = TimeHelper.GetSecondsFromBase(),
						Latitude = 12.34,
						Longitude = 56.78,
						CorrectGPS = true
					}));
			}

			if (scheduled)
				androidUser.SetMlpSchedule(IdType.Vehicle, appleVehicle.id, null);
		}

		private static void SetPhoneBook(IWebPersonalServer me, IWebPersonalServer friend, string name)
		{
			me.SavePhoneBook(
				new PhoneBook
				{
					Contacts =
						new[]
						{
							new PhoneBookContact
							{
								Type = ContactType.Phone,
								Value = friend.GetMsisdn(),
								Name = name
							}
						}
				});
		}

		private static Command CreatesAskPositionCommand(Vehicle targetVehicle, bool scheduled)
		{
			var result = new Command
			{
				TargetID = targetVehicle.id,
				Type = CmdType.AskPosition
			};

			if (scheduled)
				result.SetParameter(CommandParameter.ScheduledRequest, null);

			return result;
		}

		public IWebPersonalServer RegisterAndroidUser()
		{
			var server = Helper.CreateNewPhysicalUser();
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);
			return server;
		}

		public IWebPersonalServer RegisterAppleUser()
		{
			var server = Helper.CreateNewPhysicalUser();
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), null, Guid.NewGuid().ToString("N"));
			return server;
		}
	}
}