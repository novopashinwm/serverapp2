﻿using System.Linq;
using FORIS.TSS.EntityModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class EntitiesTests
	{
		[TestMethod]
		public void IncreaseRenderedQuantity()
		{
			using (var entities = new Entities())
			{
				var bs = entities.Billing_Service.First();

				var quantity =
					entities.Rendered_Service
							.Where(rs => rs.Billing_Service_ID == bs.ID)
							.Select(rs => rs.Quantity)
							.FirstOrDefault();

				var result = entities.IncreaseRenderedQuantity(bs.ID, "SMS", null).First();

				Assert.IsNotNull(result);

				Assert.AreEqual(result.Value, quantity + 1);
			}
		}
	}
}