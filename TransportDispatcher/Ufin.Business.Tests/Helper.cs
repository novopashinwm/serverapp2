﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication;
using Interfaces.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Contact = FORIS.TSS.EntityModel.Contact;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace Compass.Ufin.Business.Tests
{
	static class Helper
	{
		public static string GenerateNewImei()
		{
			var result = IdGenerator.GenerateTerminalDeviceNumber().ToString(CultureInfo.InvariantCulture);
			if ((ImeiHelper.ImeiLength - 1) < result.Length)
				result = result.Substring(result.Length - (ImeiHelper.ImeiLength - 1));
			else
				result = result.PadRight(ImeiHelper.ImeiLength - 1, '0');

			return result + ImeiHelper.GetImeiControlDigit(result);
		}
		public static IWebPersonalServer GetServerByLogin(string login)
		{
			int operatorId;
			using (var entities = new Entities())
			{
				var @operator =
					entities.OPERATOR.Where(o => o.LOGIN == login)
							.Select(o => new {ID = o.OPERATOR_ID})
							.FirstOrDefault();
				if (@operator == null)
					throw new ArgumentOutOfRangeException("login", login, "Login does not exists");


				operatorId = @operator.ID;
			}

			return GetServer(operatorId);
		}
		public static IWebPersonalServer GetServerByMsisdn(string msisdn)
		{
			int operatorId;
			using (var entities = new Entities())
			{
				var @operator = entities.Operator_Contact
					.Where(oc => oc.Contact.Value == msisdn && oc.Confirmed && !oc.Removable)
					.Select(oc => new { ID = oc.Operator_ID })
					.FirstOrDefault();
				if(@operator == null)
					throw new ArgumentException("msisdn not exists", "msisdn");

				operatorId = @operator.ID;
			}

			return GetServer(operatorId);
		}
		public static IWebPersonalServer GetServer(int operatorId)
		{
			var tdSessionManager = new TDSessionManager();
			var tdServer = new TDServer(tdSessionManager, ConfigurationManager.AppSettings);
			tdSessionManager.Server = tdServer;

			var application =
				(IWebPersonalServer)
				tdSessionManager.CreateSession(operatorId, Guid.NewGuid(), typeof(Helper).Assembly.ImageRuntimeVersion, IPAddress.Loopback);
			return application;
		}
		public static IWebPersonalServer GetSuperAdminServer()
		{
			var operatorId = GetSuperAdmin().id;
			return GetServer(operatorId);
		}
		public static Operator GetSuperAdmin()
		{
			using (var entities = new Entities())
			{
				var config = entities.CONSTANTS.Where(c => c.NAME == "SalesOperatorGroupID").Select(c => c.VALUE).First();
				var operatorGroupId = int.Parse(config);
				return entities.OPERATOR.First(o => o.ContainingGroups.Any(og => og.OPERATORGROUP_ID == operatorGroupId)).ToDto();

			}
		}
		public static Department GetDepartmentsByOperator(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities.GetVehiclesByRight(operatorId, SystemRight.VehicleAccess).Where(item => item.Department_ID.HasValue).Select(item => item.DEPARTMENT).First().ToDto();
			}
		}
		internal static readonly string[] _msisdnPrefixes = {"985", "916", "916"};
		public static string FindFreeMsisdn(int length, params string[] prefixes)
		{
			var baseNumber = (long)(DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds;
			if (prefixes == null || prefixes.Length == 0)
				prefixes = _msisdnPrefixes;
			using (var entities = new Entities())
			{
				foreach (var prefix in prefixes)
				{
					while (true)
					{
						++baseNumber;

						var msisdn = baseNumber.ToString(CultureInfo.InvariantCulture);
						if (msisdn.Length + prefix.Length < length)
							msisdn = msisdn.PadLeft(length, '0');

						msisdn = prefix + msisdn.Substring(msisdn.Length + prefix.Length - length);


						if (entities.Contact.Any(c => c.Type == (int) ContactType.Phone && c.Value == msisdn))
							continue;

						Contact msisdnContact = entities.GetContact(ContactType.Phone, msisdn);
						return msisdnContact.Value;
					}
				}

				throw new ArgumentException("Unable to find free msisdn with specified prefix: " + prefixes.Join(", "));
			}
		}
		public static string FindFreeMsisdn(params string[] prefixes)
		{
			var baseNumber = (long)(DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds;
			if (prefixes == null || prefixes.Length == 0)
				prefixes = _msisdnPrefixes;
			using (var entities = new Entities())
			{
				foreach (var prefix in prefixes)
				{
					while (true)
					{
						++baseNumber;

						string msisdn;

						if (prefix.Length == 0)
						{
							msisdn = baseNumber.ToString(CultureInfo.InvariantCulture);
							if (msisdn.Length + prefix.Length < 9)
								msisdn = msisdn.PadLeft(9, '0');

							msisdn = "79" + msisdn.Substring(msisdn.Length + prefix.Length - 9);
						}
						else
						{
							msisdn = baseNumber.ToString(CultureInfo.InvariantCulture);
							if (msisdn.Length + prefix.Length < 10)
								msisdn = msisdn.PadLeft(10, '0');

							msisdn = "7" + prefix + msisdn.Substring(msisdn.Length + prefix.Length - 10);
						}


						if (entities.Contact.Any(c => c.Type == (int)ContactType.Phone && c.Value == msisdn))
							continue;

						Contact msisdnContact = entities.GetContact(ContactType.Phone, msisdn);
						return msisdnContact.Value;
					}
				}

				throw new ArgumentException("Unable to find free msisdn with specified prefix: " + prefixes.Join(", "));
			}
		}
		private static string GetDefaultContractNumber(long baseNumber)
		{
			return ("contract_" + baseNumber);
		}
		private static int? GetOperatorIdByTerminalDevicNumber(long terminalDeviceNumber)
		{
			using (var entities = new Entities())
			{
				var result = entities
					.OPERATOR
					.Where(o => o.Asid.Terminal_Device_Number == terminalDeviceNumber)
					.Select(o => new { o.OPERATOR_ID })
					.FirstOrDefault();

				return result != null ? result.OPERATOR_ID : (int?) null;
			}
		}
		public static IWebPersonalServer GetPersonalServerByMsisdn(string msisdn)
		{
			int operatorID;
			using (var entities = new Entities())
			{
				operatorID =
					entities.OPERATOR.Where(
						o =>
							o.Contacts.Any(
								oc =>
									oc.Contact.Value == msisdn && oc.Confirmed &&
									oc.Contact.Type == (int) ContactType.Phone)
						)
						.Select(o => o.OPERATOR_ID)
						.First();
			}

			return GetServer(operatorID);
		}
		public static IWebPersonalServer RegisterMsisdn(string msisdn)
		{
			var server = GetOrCreateUserByMsisdn(msisdn);
			var appId = Guid.NewGuid().ToString();
			var apnDeviceToken = Guid.NewGuid().ToByteArray().ToHexString();
			server.UpdateClientRegistration(appId, null, apnDeviceToken);

			var gcmRegistrationId = Guid.NewGuid().ToByteArray().ToHexString();
			server.UpdateClientRegistration(appId, gcmRegistrationId, null);

			Assert.AreEqual(VehicleKind.Smartphone, server.GetVehicles().Single().vehicleKind);

			Assert.AreEqual("Я " + msisdn, server.GetVehicles().Single().Name);
			if (string.IsNullOrEmpty(msisdn))
				return server;

			using (var entities = new Entities())
			{
				var @operator =
					entities.OPERATOR.Where(o => o.OPERATOR_ID == server.OperatorId)
						.Select(o => new { o.LOGIN, o.PASSWORD }).First();
				var expectedLogin = entities.GetConstant(CONSTANTS.NewLoginPrefix) + msisdn;
				Assert.AreEqual(expectedLogin, @operator.LOGIN);
				Assert.IsFalse(string.IsNullOrWhiteSpace(@operator.PASSWORD));
			}

			return server;
		}
		public static IWebPersonalServer GetPersonalServerBySimId(string simId)
		{
			var operatorID = GetOperatorIdBySimId(simId);
			if (operatorID == null)
			{
				var tdSessionManager = new TDSessionManager();
				var tdServer = new TDServer(tdSessionManager, ConfigurationManager.AppSettings);
				tdSessionManager.Server = tdServer;
				operatorID = tdSessionManager.GetOperatorIdBySimId(simId, string.Empty);
				if (operatorID == null)
					throw new ArgumentOutOfRangeException("simId", simId, @"Unable to retrieve operator id");
			}

			return GetServer(operatorID.Value);
		}
		private static int? GetOperatorIdBySimId(string simId)
		{
			using (var entities = new Entities())
			{
				var result =
					entities.Asid.Where(a => a.SimID == simId)
						.Select(a => (int?)a.OPERATOR.OPERATOR_ID)
						.FirstOrDefault();

				return result;
			}
		}
		public static string FindFreePhone(params string[] prefixes)
		{
			if (prefixes.Length == 0)
				prefixes = _msisdnPrefixes;

			using (var entities = new Entities())
			{
				var baseNumber = (long)(DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds;
				foreach (var prefix in prefixes)
				{
					while (true)
					{
						++baseNumber;

						var msisdn = baseNumber.ToString(CultureInfo.InvariantCulture);
						if (msisdn.Length < 10 - prefix.Length)
							msisdn = msisdn.PadLeft(10, '0');
						msisdn = "7" + prefix + msisdn.Substring(msisdn.Length - (10 - prefix.Length));

						if (!entities.Contact.Any(c => c.Value == msisdn && c.Type == (int) ContactType.Phone))
							return msisdn;
					}
				}

				throw new ArgumentException("Unable to find free msisdn for prefixes: " + prefixes.Join(", "));
			}
		}
		public static void AddConfirmedContact(this IWebPersonalServer server, ContactType type, string value)
		{
			var result = server.AddContact(type, value);
			Assert.AreEqual(result.Result, SetNewContactResult.Success);
			Assert.IsNotNull(result.Value.Id);
			var contactId = result.Value.Id.Value;
			Assert.AreEqual(server.SendConfirmationCode(contactId, string.Empty, string.Empty).Code, SetNewContactResult.Success);
			var confirmMessage = Helper.GetLastMessage();
			Assert.IsNotNull(confirmMessage);
			var confirmationCode = confirmMessage.MESSAGE_FIELD
				.FirstOrDefault(mf => mf.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.ConfirmationCode)
				?.CONTENT;
			Assert.IsNotNull(confirmationCode);
			using (var entities = new Entities())
			{
				entities.Attach(confirmMessage);
				confirmMessage.STATUS = (int) MessageState.Done;
				entities.SaveChanges();
			}

			Assert.AreEqual(SetNewContactResult.Success, server.VerifyContact(contactId, confirmationCode).Code);
		}
		public static int GetLastMessageId()
		{
			using (var entities = new Entities())
			{
				return entities.MESSAGE.Max(item => item.MESSAGE_ID);
			}
		}
		public static MESSAGE GetLastMessage()
		{
			return GetLastMessages(1).FirstOrDefault();
		}
		public static List<MESSAGE> GetLastMessages(int count, string templateName = null)
		{
			using (var entities = new Entities())
			{
				IQueryable<MESSAGE> query = entities.MESSAGE
					.Include(MESSAGE.ContactsIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath + "." + MESSAGE_FIELD.MESSAGE_TEMPLATE_FIELDIncludePath)
					.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
					.Include(MESSAGE.MESSAGE_TEMPLATEIncludePath);

				if (templateName != null)
					query = query.Where(m => m.MESSAGE_TEMPLATE.NAME == templateName);

				return query
						.OrderByDescending(m => m.MESSAGE_ID)
						.Take(count)
						.ToList();
			}
		}
		public static int GetMessageCount(int id)
		{
			using (var entities = new Entities())
			{
				return entities.MESSAGE.Count(item => item.MESSAGE_ID > id);
			}
		}
		public static List<MESSAGE> GetMessagesAfter(int id)
		{
			using (var entities = new Entities())
			{
				return entities.MESSAGE
					.Include(MESSAGE.ContactsIncludePath)
					.Include(MESSAGE.OwnerOperatorIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath + "." + MESSAGE_FIELD.MESSAGE_TEMPLATE_FIELDIncludePath)
					.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
					.Include(MESSAGE.MESSAGE_TEMPLATEIncludePath)
					.Where(item => item.MESSAGE_ID > id)
					.ToList();
			}
		}
		public static string GetConstant(Constant constant)
		{
			using (var entities = new Entities())
			{
				return entities.GetConstant(constant);
			}
		}
		public static string GenerateNewLogin()
		{
			string loginPrefix;
			int maxOperatorId;
			using (var entities = new Entities())
			{
				loginPrefix = entities.GetConstant(CONSTANTS.NewLoginPrefix);
				if (string.IsNullOrWhiteSpace(loginPrefix))
					loginPrefix = "L";
				maxOperatorId = entities.OPERATOR.Max(o => o.OPERATOR_ID);
			}
			return loginPrefix + (maxOperatorId + 1);
		}
		public static int GetOperatorIdByLogin(string userLogin)
		{
			if (string.IsNullOrWhiteSpace(userLogin))
				throw new ArgumentOutOfRangeException("userLogin", userLogin, @"Value cannot be null or whitespace");

			using (var entities = new Entities())
			{
				return entities.OPERATOR.Where(o => o.LOGIN == userLogin).Select(o => o.OPERATOR_ID).First();
			}
		}
		public static string GetAsidByMsisdn(string msisdn)
		{
			using (var entities = new Entities())
			{
				return entities.Contact.Where(c => c.DemaskedPhone.Value == msisdn).Select(c => c.Value).FirstOrDefault();
			}
		}
		public static int GetLastCommandId()
		{
			using (var entities = new Entities())
			{
				return entities.Command.Select(c => c.ID).OrderByDescending(x => x).First();
			}
		}
		public static MESSAGE GetLastMessageToPhone(Entities entities, string msisdn)
		{
			return entities.Message_Contact
				.Where(
					mc =>
						mc.Type == (int) MessageContactType.Destination &&
						(mc.Contact.Type == (int) ContactType.Phone &&
						 mc.Contact.Value == msisdn ||
						 mc.Contact.DemaskedPhone.Value == msisdn))
				.OrderByDescending(mc => mc.Message_ID)
				.Select(mc => mc.MESSAGE)
				.FirstOrDefault();
		}
		public static IWebPersonalServer CreateNewCorporateUser(string adminLogin = null)
		{
			var superAdminServer = GetSuperAdminServer();
			Assert.IsNotNull(superAdminServer);
			var departmentName = Guid.NewGuid().ToString("N");
			int departmentId;
			Assert.AreEqual(
				CreateDepartmentResult.Success,
				superAdminServer.CreateDepartment(
					departmentName,
					adminLogin ?? departmentName,
					null,
					out departmentId));
			return GetServerByLogin(adminLogin ?? departmentName);
		}
		public static string FindUsedMsisdn()
		{
			using (var entities = new Entities())
			{
				var msisdn = entities.VEHICLE
					.Where(v => v.CONTROLLER.PhoneContactID != null && entities.Log_Time.Any(l => l.Vehicle_ID == v.VEHICLE_ID))
					.Select(v => v.CONTROLLER.PhoneContact.Value)
					.FirstOrDefault();

				return msisdn;
			}
		}
		public static IWebPersonalServer CreateNewPhysicalUser()
		{
			return GetOrCreateUserByMsisdn(FindFreeMsisdn(_msisdnPrefixes));
		}
		public static string GetMsisdnByTerminalDeviceNumber(long number)
		{
			using (var entities = new Entities())
			{
				return entities.Asid
					.Where(a => a.Terminal_Device_Number == number)
					.Select(a => a.Contact.DemaskedPhone.Value)
					.FirstOrDefault();
			}
		}
		public static IWebPersonalServer GetOrCreateUserByMsisdn(string msisdn)
		{
			Assert.AreEqual(
				SetNewContactResult.Success,
				Server.Instance()
					.SendConfirmationCode(ContactType.Phone, msisdn, CultureInfo.InvariantCulture,
						IPAddress.Loopback.ToString())
					.Code);

			var regex = new Regex("\\d+");
			var message = GetLastMessages(1, Msg::MessageTemplate.GenericConfirmation).First();
			var code = regex.Match(message.BODY).Value;
			//Вначале отправляем неверный код подтверждения
			var verifyMsisndResult = Server.Instance()
				.VerifyContact(ContactType.Phone, msisdn, code + "0", IPAddress.Loopback.ToString());
			Assert.AreEqual(SetNewContactResult.InvalidConfirmationCode, verifyMsisndResult.Code);
			verifyMsisndResult = Server.Instance()
				.VerifyContact(ContactType.Phone, msisdn, code, IPAddress.Loopback.ToString());
			Assert.AreEqual(SetNewContactResult.Success, verifyMsisndResult.Code);
			var simId = verifyMsisndResult.SimId;
			Assert.IsNotNull(simId);

			var server = GetPersonalServerBySimId(simId);
			Assert.IsNotNull(server);
			Assert.AreEqual(
				ContactHelper.GetNormalizedPhone(msisdn),
				server.GetContacts(ContactType.Phone).Single(c => c.IsConfirmed ?? false).Value);

			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);

			return server;
		}
	}
}