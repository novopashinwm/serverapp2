﻿using System;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.Resources;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using Interfaces.Web.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Command = FORIS.TSS.EntityModel.Command;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class BasicFunctionSetTests
	{
		[TestMethod]
		public void ReportAddRemoveServices()
		{
			foreach (var isDetailed in new[] { false, true })
			{
				ReportAddRemoveServices(DateTime.UtcNow.AddMonths(-2), DateTime.UtcNow.AddMonths(-1), isDetailed);

				ReportAddRemoveServices(new DateTime(2013, 10, 24), new DateTime(2013, 11, 23), isDetailed);

			}
		}

		private static void ReportAddRemoveServices(DateTime @from, DateTime to, bool isDetailed)
		{
			var server = Server.Instance();
			var operatorId = Helper.GetSuperAdmin().id;
			var report = server.ReportAddRemoveServices(@from, to, isDetailed, operatorId);

			Assert.IsNotNull(report);
			Assert.IsNotNull(report.SummaryRecords);

			foreach (var r in report.SummaryRecords)
			{
				Assert.AreEqual(r.CountOnStart + r.CountOfAdded - r.CountOfRemoved, r.CountOnEnd);
			}

			Assert.AreEqual(isDetailed, report.DetailRecords != null);
		}

		[TestMethod]
		public void Bug967235()
		{
			var disallowReason = CapabilityDisallowingReason.VehicleIsBlocked;
			var capability = DeviceCapability.AllowAskPosition;
			var resourceContainer = ResourceContainers.Get(CultureInfo.CurrentCulture);
			Assert.AreEqual(resourceContainer[disallowReason], resourceContainer[disallowReason, capability]);
		}

		[TestMethod]
		public void TestPhoneBook()
		{
			var msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var server = Helper.RegisterMsisdn(msisdn);

			const string value = @"[{'type':'Phone','value':'70386642724'},{'type':'Phone','value':'8 (985) 432-17-23'},{'type':'Phone','value':' 89166147992'},{'type':'Phone','value':' 46769439432'},{'type':'Phone','value':' 4759440401'},{'type':'Phone','value':'8 (916) 127-48-55'},{'type':'Phone','value':'7917 502-88-51'},{'type':'Phone','value':'903 555-44-33'},{'type':'Phone','value':' 7 963 915-06-54'},{'type':'Phone','value':'79167082588'},{'type':'Phone','value':'903 111-22-22'},{'type':'Phone','value':' 7 963 641-81-69'},{'type':'Phone','value':' 7 985 432-17-32'},{'type':'Phone','value':' 7 929 666-61-18'},{'type':'Phone','value':' 89852579565'}]";
			var contacts = JsonHelper.DeserializeObjectFromJson<PhoneBookContact[]>(value);
			var phoneBook = new PhoneBook
			{
				Contacts = contacts
			};

			Assert.AreEqual(server.SavePhoneBook(phoneBook), SavePhoneBookResult.Success);
			using (var entities = new Entities())
			{
				var operatorPhoneBook = entities.Operator_PhoneBook.FirstOrDefault(b => b.OperatorId == server.OperatorId);
				Assert.IsNotNull(operatorPhoneBook);
				Assert.IsTrue(operatorPhoneBook.IsNotified);
			}

			var operators = Server.Instance().GetOperatorsByPhoneBookContact("79854321723");
			Assert.IsTrue(operators.Any());
		}

		[TestMethod]
		public void TestPhoneBookNotifications()
		{
			var msisdn1 = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var msisdn2 = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var msisdn3 = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var msisdn4 = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var msisdn5 = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());

			var phoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact { Type = ContactType.Phone, Value = msisdn1 },
					new PhoneBookContact { Type = ContactType.Phone, Value = msisdn2 },
					new PhoneBookContact { Type = ContactType.Phone, Value = msisdn3 },
					new PhoneBookContact { Type = ContactType.Phone, Value = msisdn4 },
					new PhoneBookContact { Type = ContactType.Phone, Value = msisdn5 },
				}
			};

			var server1 = Helper.RegisterMsisdn(msisdn1);
			var lastMessageId = Helper.GetLastMessageId();
			server1.SavePhoneBook(phoneBook);
			var messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.IsTrue(messages.All(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification));
			Assert.AreEqual(messages.Count(), 0);

			var server2 = Helper.RegisterMsisdn(msisdn2);
			lastMessageId = Helper.GetLastMessageId();
			server2.SavePhoneBook(phoneBook);
			messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.IsTrue(messages.All(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification));
			Assert.AreEqual(messages.Count(), 1);

			var server3 = Helper.RegisterMsisdn(msisdn3);
			lastMessageId = Helper.GetLastMessageId();
			server3.SavePhoneBook(phoneBook);
			messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.IsTrue(messages.All(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification));
			Assert.AreEqual(messages.Count(), 2);

			var server4 = Helper.RegisterMsisdn(msisdn4);
			lastMessageId = Helper.GetLastMessageId();
			server4.SavePhoneBook(phoneBook);
			messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.IsTrue(messages.All(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification));
			Assert.AreEqual(messages.Count(), 3);

			var server5 = Helper.RegisterMsisdn(msisdn5);
			lastMessageId = Helper.GetLastMessageId();
			server5.SavePhoneBook(phoneBook);
			messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.IsTrue(messages.All(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification));
			Assert.AreEqual(messages.Count(), 4);
		}

		[TestMethod]
		public void TestPhoneBookContact()
		{
			var existMsisdn1 = Helper.FindFreeMsisdn();
			var existMsisdn2 = Helper.FindFreeMsisdn();
			var existMsisdn3 = Helper.FindFreeMsisdn();
			var existMsisdn4 = Helper.FindFreeMsisdn();
			var name1 = Guid.NewGuid().ToString("N");
			var name2 = Guid.NewGuid().ToString("N");
			var deletedName = Guid.NewGuid().ToString("N");
			var newname = Guid.NewGuid().ToString("N");
			var phoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact { Type = ContactType.Phone, Value = existMsisdn1, Name = name1 },
					new PhoneBookContact { Type = ContactType.Phone, Value = existMsisdn2, Name = name2 },
					new PhoneBookContact { Type = ContactType.Phone, Value = existMsisdn3, Name = deletedName },
					new PhoneBookContact { Type = ContactType.Phone, Value = existMsisdn4, Name = string.Empty },
				}
			};

			var msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var server = Helper.RegisterMsisdn(msisdn);
			server.SavePhoneBook(phoneBook);
			var phones = Server.Instance().SearchPhoneBook(server.OperatorId, new[] { existMsisdn1, existMsisdn2, existMsisdn3, existMsisdn4 });
			Assert.AreEqual(phones[existMsisdn1], name1);
			Assert.AreEqual(phones[existMsisdn2], name2);
			Assert.AreEqual(phones[existMsisdn3], deletedName);
			Assert.AreEqual(phones[existMsisdn4], String.Empty);

			var notExistMsisdn1 = Helper.FindFreeMsisdn();
			var notExistMsisdn2 = Helper.FindFreeMsisdn();

			server.UpdatePhoneBookContactName(existMsisdn1, newname);
			server.UpdatePhoneBookContactName(existMsisdn4, newname);
			server.UpdatePhoneBookContactName(notExistMsisdn1, newname);
			phones = Server.Instance().SearchPhoneBook(server.OperatorId, new[] { existMsisdn1, existMsisdn2, existMsisdn3, existMsisdn4, notExistMsisdn1, notExistMsisdn2 });
			Assert.AreEqual(phones[existMsisdn1], newname);
			Assert.AreEqual(phones[existMsisdn2], name2);
			Assert.AreEqual(phones[existMsisdn3], deletedName);
			Assert.AreEqual(phones[existMsisdn4], newname);
			Assert.AreEqual(phones[notExistMsisdn1], newname);

			var updatedName = Guid.Empty.ToString("N");
			var updatedPhoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact {Type = ContactType.Phone, Value = existMsisdn1, Name = updatedName},
					new PhoneBookContact {Type = ContactType.Phone, Value = existMsisdn2, Name = updatedName},
					new PhoneBookContact {Type = ContactType.Phone, Value = existMsisdn4, Name = updatedName },
					new PhoneBookContact {Type = ContactType.Phone, Value = notExistMsisdn1, Name = updatedName},
					new PhoneBookContact {Type = ContactType.Phone, Value = notExistMsisdn2, Name = updatedName},
			   }
			};
			server.SavePhoneBook(updatedPhoneBook);

			phones = Server.Instance().SearchPhoneBook(server.OperatorId, new[] { existMsisdn1, existMsisdn2, existMsisdn3, existMsisdn4, notExistMsisdn1, notExistMsisdn2 });
			Assert.AreEqual(phones[existMsisdn1], updatedName);
			Assert.AreEqual(phones[existMsisdn2], updatedName);
			Assert.IsTrue(phones.ContainsKey(existMsisdn3));
			Assert.AreEqual(phones[existMsisdn4], updatedName);
			Assert.AreEqual(phones[notExistMsisdn1], updatedName);
			Assert.AreEqual(phones[notExistMsisdn2], updatedName);
		}

		[TestMethod]
		public void SetMessageStatus()
		{
			var msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var server = Helper.RegisterMsisdn(msisdn);
			var vehicle = server.GetVehicles().First();
			var superAdmin = Helper.GetSuperAdmin();

			using (var entities = new Entities())
			{
				Assert.IsTrue(
					CommandProcessorHelper.TrySendCloudPush(
						entities, new Command
						{
							Type_ID = (int)CmdType.AskPosition,
							Target_ID = vehicle.id,
							Sender_ID = superAdmin.id
						}));
				entities.SaveUnsavedChanges();
			}

			var pushId = Helper.GetLastMessageId();
			var updateIds = server.UpdateMessageStatus(new[] { pushId }, MessageProcessingResult.Delivered);
			Assert.IsTrue(updateIds.Contains(pushId));
			Assert.AreEqual(MessageProcessingResult.Delivered, GetMessageProcessingResult(pushId));

			Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Sent, pushId);
			Assert.AreEqual(MessageProcessingResult.Delivered, GetMessageProcessingResult(pushId));

			updateIds = server.UpdateMessageStatus(new[] { pushId }, MessageProcessingResult.Read);
			Assert.IsTrue(updateIds.Contains(pushId));
			Assert.AreEqual(MessageProcessingResult.Read, GetMessageProcessingResult(pushId));

			updateIds = server.UpdateMessageStatus(new[] { pushId }, MessageProcessingResult.Delivered);
			Assert.IsTrue(updateIds.Contains(pushId));
			Assert.AreEqual(MessageProcessingResult.Delivered, GetMessageProcessingResult(pushId));
		}

		private static MessageProcessingResult GetMessageProcessingResult(int messageId)
		{
			using (var entities = new Entities())
			{
				return entities.MESSAGE
					.Where(m => m.MESSAGE_ID == messageId)
					.Select(m => (MessageProcessingResult)m.ProcessingResult)
					.First();
			}
		}
	}
}