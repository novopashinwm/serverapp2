﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.EntityModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class ReportsTests
	{
		[TestMethod]
		public void MoveReportParametersDeserialization()
		{
			const string configXml = "<SOAP-ENV:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:clr=\"http://schemas.microsoft.com/soap/encoding/clr/1.0\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><SOAP-ENV:Body><a1:MoveReportParameters xmlns:a1=\"http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportWebGisMoveHistory\" id=\"ref-1\"><showAddress>false</showAddress><mapGuid id=\"ref-7\">00000000-0000-0000-0000-000000000000</mapGuid><mblnAlwaysCurrentDate>false</mblnAlwaysCurrentDate><mstrReportTimeFrom id=\"ref-8\">18:32</mstrReportTimeFrom><mstrReportTimeTo id=\"ref-9\">18:32</mstrReportTimeTo><vehicleProp href=\"#ref-10\" /><count>1000</count><interval>60</interval><parkingInterval>300</parkingInterval><_workingHoursInterval href=\"#ref-12\" /><_x003C_OperatorID_x003E_k__BackingField>293</_x003C_OperatorID_x003E_k__BackingField><dtDateReport>2013-10-19T00:00:00.0000000+04:00</dtDateReport><dtDateFrom>2012-10-25T00:00:00.0000000+04:00</dtDateFrom><dtDateTo>2012-10-26T00:00:00.0000000+04:00</dtDateTo><_operatorId>293</_operatorId><ReportParameters_x002B__applicationPath id=\"ref-13\">nika-glonass.local/WebMap</ReportParameters_x002B__applicationPath><ReportParameters_x002B_dtDateReport>2013-10-19T00:00:00.0000000+04:00</ReportParameters_x002B_dtDateReport><ReportParameters_x002B_dtDateFrom>2012-10-25T00:00:00.0000000+04:00</ReportParameters_x002B_dtDateFrom><ReportParameters_x002B_dtDateTo>2012-10-26T00:00:00.0000000+04:00</ReportParameters_x002B_dtDateTo><ReportParameters_x002B_mblnShowBlankReport>false</ReportParameters_x002B_mblnShowBlankReport><ReportParameters_x002B__operatorId>293</ReportParameters_x002B__operatorId><ReportParameters_x002B__culture href=\"#ref-14\" /><ReportParameters_x002B__timeZoneInfoString id=\"ref-15\">Arabian Standard Time;240;(UTC+04:00) РђР±Сѓ-Р”Р°Р±Рё, РњСѓСЃРєР°С‚;РђСЂР°Р±СЃРєРѕРµ РІСЂРµРјСЏ (Р·РёРјР°);РђСЂР°Р±СЃРєРѕРµ РІСЂРµРјСЏ (Р»РµС‚Рѕ);;</ReportParameters_x002B__timeZoneInfoString><ReportParameters_x002B__mapGuid><_a>89119490</_a><_b>-4975</_b><_c>17161</_c><_d>160</_d><_e>170</_e><_f>219</_f><_g>254</_g><_h>197</_h><_i>110</_i><_j>78</_j><_k>228</_k></ReportParameters_x002B__mapGuid><ReportParameters_x002B__deviceCapabilities>None</ReportParameters_x002B__deviceCapabilities><ReportParameters_x002B__interval href=\"#ref-16\" /></a1:MoveReportParameters><a2:TagListBoxItem xmlns:a2=\"http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher/FORIS.TSS.Commands\" id=\"ref-10\"><mobjTagElement xsi:type=\"xsd:int\">821</mobjTagElement><mtxtTextElement id=\"ref-17\" /></a2:TagListBoxItem><a4:WorkingHoursInterval xmlns:a4=\"http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/FORIS.TSS.Common.Utils\" id=\"ref-12\"><_x003C_WorkFrom_x003E_k__BackingField>-P0Y0M0DT0H0M0S</_x003C_WorkFrom_x003E_k__BackingField><_x003C_WorkTo_x003E_k__BackingField>-P0Y0M0DT0H0M0S</_x003C_WorkTo_x003E_k__BackingField></a4:WorkingHoursInterval><a5:CultureInfo xmlns:a5=\"http://schemas.microsoft.com/clr/ns/System.Globalization\" id=\"ref-14\"><m_isReadOnly>false</m_isReadOnly><compareInfo href=\"#ref-21\" /><textInfo xsi:null=\"1\" /><numInfo xsi:null=\"1\" /><dateTimeInfo xsi:null=\"1\" /><calendar xsi:null=\"1\" /><m_dataItem>0</m_dataItem><cultureID>1049</cultureID><m_name id=\"ref-22\">ru-RU</m_name><m_useUserOverride>true</m_useUserOverride></a5:CultureInfo><a4:DateTimeInterval xmlns:a4=\"http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/FORIS.TSS.Common.Utils\" id=\"ref-16\"><_x003C_DateFrom_x003E_k__BackingField>2012-12-05T09:00:00.0000000+04:00</_x003C_DateFrom_x003E_k__BackingField><_x003C_DateTo_x003E_k__BackingField>2012-12-06T15:00:00.0000000+04:00</_x003C_DateTo_x003E_k__BackingField><_x003C_Accuracy_x003E_k__BackingField>ToMinute</_x003C_Accuracy_x003E_k__BackingField></a4:DateTimeInterval><a6:Version xmlns:a6=\"http://schemas.microsoft.com/clr/ns/System\" id=\"ref-18\"><_Major>2</_Major><_Minor>0</_Minor><_Build>-1</_Build><_Revision>-1</_Revision></a6:Version><a5:CompareInfo xmlns:a5=\"http://schemas.microsoft.com/clr/ns/System.Globalization\" id=\"ref-21\"><m_name id=\"ref-23\">ru-RU</m_name><win32LCID>0</win32LCID><culture>1049</culture><m_SortVersion xsi:null=\"1\" /></a5:CompareInfo></SOAP-ENV:Body></SOAP-ENV:Envelope>";
			var moveReportParameters = SerializationHelper.FromXML(configXml);
			Assert.IsNotNull(moveReportParameters);
		}

		[TestMethod]
		public void ZoneOverTssReportParameters()
		{
			using (var entities = new Entities())
			{
				var configs = entities.SCHEDULERQUEUE.Where(q => q.SCHEDULEREVENT.REPORT.REPORT_GUID == new Guid("6E2DE7AD-2656-4812-BC1C-0EEA32613662")).ToArray();
				foreach (var config in configs)
				{
					Assert.IsNotNull(config);
					var cfgParameters = SerializationHelper.FromXML(config.CONFIG_XML);
					Assert.IsNotNull(cfgParameters);
				}
			}
		}

		[TestMethod]
		public void SerializeRepeatEveryWeek()
		{
			var repeatEveryWeek = new RepeatEveryWeek();
			var xml = SerializationHelper.ToXML(repeatEveryWeek);
			Assert.IsNotNull(xml);
		}

		[TestMethod]
		public void SerializeRepeatOnNthDay()
		{
			var repeat = new RepeatOnNthDay();
			repeat.N = 1;
			var xml = SerializationHelper.ToXML(repeat);
			Assert.IsNotNull(xml);
		}

		[TestMethod]
		public void DeserializeRepetitionXml()
		{
			var xmlList = new[]
			{
 @"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatEveryWeek id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""><n>1</n><Monday>false</Monday><Tuesday>false</Tuesday><Wednesday>false</Wednesday><Thursday>false</Thursday><Friday>false</Friday><Saturday>true</Saturday><Sunday>false</Sunday><comment xsi:null=""1""/></a1:RepeatEveryWeek></SOAP-ENV:Body></SOAP-ENV:Envelope>"
,@"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatEveryWeekGivenTimes id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""><_isWeekDayEnabled href=""#ref-3""/><_times href=""#ref-4""/><Start>2016-03-16T00:00:00.0000000+03:00</Start><End>9999-12-31T23:59:59.9999999+03:00</End><TimeZoneOffset>P0Y0M0DT3H0M0S</TimeZoneOffset><Comment xsi:null=""1""/></a1:RepeatEveryWeekGivenTimes><SOAP-ENC:Array id=""ref-3"" SOAP-ENC:arrayType=""xsd:boolean[7]""><item>False</item><item>True</item><item>True</item><item>True</item><item>True</item><item>True</item><item>False</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-4"" SOAP-ENC:arrayType=""xsd:duration[2]""><item>P0Y0M0DT7H0M0S</item><item>P0Y0M0DT19H0M0S</item></SOAP-ENC:Array></SOAP-ENV:Body></SOAP-ENV:Envelope>"
,@"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatEveryTime id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""><tsInterval>P0Y0M0DT0H1M0S</tsInterval><StartTime>-P0Y0M0DT0H0M0S</StartTime><EndTime>P0Y0M0DT23H59M0S</EndTime><Start>2016-03-13T19:00:00.0000000+03:00</Start><End>9999-12-31T23:59:59.9999999+03:00</End><TimeZoneOffset>P0Y0M0DT3H0M0S</TimeZoneOffset><Comment xsi:null=""1""/></a1:RepeatEveryTime></SOAP-ENV:Body></SOAP-ENV:Envelope>"
,@"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatOnNthDay id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces""><n>1</n><comment xsi:null=""1""/></a1:RepeatOnNthDay></SOAP-ENV:Body></SOAP-ENV:Envelope>"
,@"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatCustomWeekDayCustomTime id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""><_schedule href=""#ref-3""/><Start>2015-08-13T00:00:00.0000000+03:00</Start><End>9999-12-31T23:59:59.9999999+03:00</End><TimeZoneOffset>P0Y0M0DT3H0M0S</TimeZoneOffset><Comment xsi:null=""1""/></a1:RepeatCustomWeekDayCustomTime><SOAP-ENC:Array id=""ref-3"" SOAP-ENC:arrayType=""a2:TimeSpan[][7]"" xmlns:a2=""http://schemas.microsoft.com/clr/ns/System""><item href=""#ref-4""/><item href=""#ref-5""/><item href=""#ref-6""/><item href=""#ref-7""/><item href=""#ref-8""/><item href=""#ref-9""/><item href=""#ref-10""/></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-4"" SOAP-ENC:arrayType=""xsd:duration[0]""></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-5"" SOAP-ENC:arrayType=""xsd:duration[19]""><item>P0Y0M0DT9H30M0S</item><item>P0Y0M0DT10H0M0S</item><item>P0Y0M0DT10H30M0S</item><item>P0Y0M0DT11H0M0S</item><item>P0Y0M0DT11H30M0S</item><item>P0Y0M0DT12H0M0S</item><item>P0Y0M0DT12H30M0S</item><item>P0Y0M0DT13H0M0S</item><item>P0Y0M0DT13H30M0S</item><item>P0Y0M0DT14H0M0S</item><item>P0Y0M0DT14H30M0S</item><item>P0Y0M0DT15H0M0S</item><item>P0Y0M0DT15H30M0S</item><item>P0Y0M0DT16H0M0S</item><item>P0Y0M0DT16H30M0S</item><item>P0Y0M0DT17H0M0S</item><item>P0Y0M0DT17H30M0S</item><item>P0Y0M0DT18H0M0S</item><item>P0Y0M0DT18H30M0S</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-6"" SOAP-ENC:arrayType=""xsd:duration[19]""><item>P0Y0M0DT9H30M0S</item><item>P0Y0M0DT10H0M0S</item><item>P0Y0M0DT10H30M0S</item><item>P0Y0M0DT11H0M0S</item><item>P0Y0M0DT11H30M0S</item><item>P0Y0M0DT12H0M0S</item><item>P0Y0M0DT12H30M0S</item><item>P0Y0M0DT13H0M0S</item><item>P0Y0M0DT13H30M0S</item><item>P0Y0M0DT14H0M0S</item><item>P0Y0M0DT14H30M0S</item><item>P0Y0M0DT15H0M0S</item><item>P0Y0M0DT15H30M0S</item><item>P0Y0M0DT16H0M0S</item><item>P0Y0M0DT16H30M0S</item><item>P0Y0M0DT17H0M0S</item><item>P0Y0M0DT17H30M0S</item><item>P0Y0M0DT18H0M0S</item><item>P0Y0M0DT18H30M0S</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-7"" SOAP-ENC:arrayType=""xsd:duration[19]""><item>P0Y0M0DT9H30M0S</item><item>P0Y0M0DT10H0M0S</item><item>P0Y0M0DT10H30M0S</item><item>P0Y0M0DT11H0M0S</item><item>P0Y0M0DT11H30M0S</item><item>P0Y0M0DT12H0M0S</item><item>P0Y0M0DT12H30M0S</item><item>P0Y0M0DT13H0M0S</item><item>P0Y0M0DT13H30M0S</item><item>P0Y0M0DT14H0M0S</item><item>P0Y0M0DT14H30M0S</item><item>P0Y0M0DT15H0M0S</item><item>P0Y0M0DT15H30M0S</item><item>P0Y0M0DT16H0M0S</item><item>P0Y0M0DT16H30M0S</item><item>P0Y0M0DT17H0M0S</item><item>P0Y0M0DT17H30M0S</item><item>P0Y0M0DT18H0M0S</item><item>P0Y0M0DT18H30M0S</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-8"" SOAP-ENC:arrayType=""xsd:duration[19]""><item>P0Y0M0DT9H30M0S</item><item>P0Y0M0DT10H0M0S</item><item>P0Y0M0DT10H30M0S</item><item>P0Y0M0DT11H0M0S</item><item>P0Y0M0DT11H30M0S</item><item>P0Y0M0DT12H0M0S</item><item>P0Y0M0DT12H30M0S</item><item>P0Y0M0DT13H0M0S</item><item>P0Y0M0DT13H30M0S</item><item>P0Y0M0DT14H0M0S</item><item>P0Y0M0DT14H30M0S</item><item>P0Y0M0DT15H0M0S</item><item>P0Y0M0DT15H30M0S</item><item>P0Y0M0DT16H0M0S</item><item>P0Y0M0DT16H30M0S</item><item>P0Y0M0DT17H0M0S</item><item>P0Y0M0DT17H30M0S</item><item>P0Y0M0DT18H0M0S</item><item>P0Y0M0DT18H30M0S</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-9"" SOAP-ENC:arrayType=""xsd:duration[19]""><item>P0Y0M0DT9H30M0S</item><item>P0Y0M0DT10H0M0S</item><item>P0Y0M0DT10H30M0S</item><item>P0Y0M0DT11H0M0S</item><item>P0Y0M0DT11H30M0S</item><item>P0Y0M0DT12H0M0S</item><item>P0Y0M0DT12H30M0S</item><item>P0Y0M0DT13H0M0S</item><item>P0Y0M0DT13H30M0S</item><item>P0Y0M0DT14H0M0S</item><item>P0Y0M0DT14H30M0S</item><item>P0Y0M0DT15H0M0S</item><item>P0Y0M0DT15H30M0S</item><item>P0Y0M0DT16H0M0S</item><item>P0Y0M0DT16H30M0S</item><item>P0Y0M0DT17H0M0S</item><item>P0Y0M0DT17H30M0S</item><item>P0Y0M0DT18H0M0S</item><item>P0Y0M0DT18H30M0S</item></SOAP-ENC:Array><SOAP-ENC:Array id=""ref-10"" SOAP-ENC:arrayType=""xsd:duration[0]""></SOAP-ENC:Array></SOAP-ENV:Body></SOAP-ENV:Envelope>"
,@"<SOAP-ENV:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:clr=""http://schemas.microsoft.com/soap/encoding/clr/1.0"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><SOAP-ENV:Body><a1:RepeatOnNthDayOfMonth id=""ref-1"" xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces""><n>18</n><month>0</month><comment xsi:null=""1""/></a1:RepeatOnNthDayOfMonth></SOAP-ENV:Body></SOAP-ENV:Envelope>"
			};

			foreach (var xml in xmlList)
			{
				var alteredXml =
					xml.Replace(
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""",
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/FORIS.TSS.Common.Utils"""
						)
						.Replace(
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces""",
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/FORIS.TSS.Common.Utils"""
						);


				var deserializedObject = SerializationHelper.FromXmlOrThrowException(alteredXml);
				Assert.IsNotNull(deserializedObject);
			}
		}
	}
}