﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.ServerApplication.Mail;
using Interfaces.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class AppStoreBillingTests
	{
		[TestMethod]
		public void Android()
		{
			var billingService = TestAttachService(AttachAndroidService, type => new AndroidPublisherService(), SchedulerExecutionResult.Success);
			Assert.IsNull(billingService.EndDate);
			Assert.IsNotNull(billingService.MaxLogDepthHours);
			Assert.IsNotNull(billingService.TrackingSchedule);
		}

		[TestMethod]
		public void Apple()
		{
			var billingService = TestAttachService(AttachAppleService, type => new AppleVerifyReceiptService(), SchedulerExecutionResult.Cancel);
			Assert.IsNotNull(billingService.EndDate);
			Assert.IsNotNull(billingService.MaxLogDepthHours);
			Assert.IsNotNull(billingService.TrackingSchedule);
		}

		private static BillingService TestAttachService(Action<IWebPersonalServer> attachService, Func<Type, object> resolveDependency, SchedulerExecutionResult schedulerResult)
		{
			var server = Helper.CreateNewPhysicalUser();
			var billingServicesBefore = server.GetBillingServices();

			Assert.IsNotNull(server.OwnVehicleId);
			Assert.IsNotNull(server.GetMsisdn());

			attachService(server);

			using (var entities = new Entities())
			{
				var validator = entities.SCHEDULERQUEUE
					.Where(sq => sq.Operator_ID == server.OperatorId)
					.OrderByDescending(sq => sq.SCHEDULERQUEUE_ID)
					.First();

				Assert.AreEqual(
					schedulerResult,
					new SchedulerHelper(resolveDependency).ProcessEvent(entities, validator).Code);

				entities.SaveChanges();
			}

			var billingServicesAfter = server.GetBillingServices();
			Assert.AreEqual(billingServicesBefore.Count + 1, billingServicesAfter.Count);

			return billingServicesAfter.Single(bs => bs.Type == ServiceTypeCategory.Generic);
		}

		private static void AttachAppleService(IWebPersonalServer server)
		{
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), null, Guid.NewGuid().ToString("N"), true);
			server.TryAttachAppleSubscription(Convert.ToBase64String(Guid.NewGuid().ToByteArray()));
		}

		private static void AttachAndroidService(IWebPersonalServer server)
		{
			var json =
				@"{""packageName"":""ru.good4.project1"",""productId"":""premium"",""purchaseTime"":1456485671129,""purchaseState"":0,""developerPayload"":""premium.payload"",""purchaseToken"":""fplcbjhoaedkedcecdebhajd.AO-J1OxySticzJAyIcL-x4PV2px4hqG66vwgFHnR-HbdwKU5UneXZM7cb44gVw_uuyN81KmYI0GcXIcldF6bMRUkOdOydh5xKONaiVVwWv_dTkQWJUeUkrE"",""autoRenewing"":true}";

			using (var entities = new Entities())
			{
				var billingService = entities.Billing_Service.FirstOrDefault(
					bs => bs.PurchaseToken == "fplcbjhoaedkedcecdebhajd.AO-J1OxySticzJAyIcL-x4PV2px4hqG66vwgFHnR-HbdwKU5UneXZM7cb44gVw_uuyN81KmYI0GcXIcldF6bMRUkOdOydh5xKONaiVVwWv_dTkQWJUeUkrE");
				if (billingService != null)
				{
					billingService.PurchaseToken = null;
					entities.SaveChanges();
				}
			}

			var signatureBase64String =
				"fu4r0cPIfpT7CWx0rnMZM3D7Fm0SqO955vr6M08EN8TqbjdgYXf4HVqVA2FGYOjjEyReGAQSVNs+b8dxDKLdv6gmNdohLnA5hXC11aFWK8PdCnZc1bznEiGlHJJolldqeqJk13R9lBLpggZ/LimmdFNJNaq4QX5tgIMXgCDWu/1EKfY1sZlW1+ySiLo7XvaRPjIuODOfAv8RnEPmycn2L/jYWkItlRWA328zpz/8J6l20nDY7FyqjlRCf2DgXCGvJnzwo77OudG20zIGuxbjLErtd9HgLHWTOAaPO1BzOZChzHhO1yccY0juvK97p9AUzYtVQNJ2SKURauHPCIU4dA==";
			var signature = Convert.FromBase64String(signatureBase64String);
			server.TryAttachAndroidSubscription(json, signature);
		}

		private class AndroidPublisherService : IAndroidPublisherService
		{
			public SubscriptionPurchase GetSubscriptionPurchase(string package, string product, string purchaseToken)
			{
				return new SubscriptionPurchase
				{
					AutoRenewing = true,
					ExpiryTimeMillis = DateTime.UtcNow.AddDays(1).ToLogTimeMs()
				};
			}
		}

		private class AppleVerifyReceiptService : IAppleVerifyReceiptService
		{
			public ApplePurchase VerifyReceipt(bool sandbox, string purchaseBase64)
			{
				return new ApplePurchase
				{
					status = 0,
					environment = sandbox ? "Sandbox" : "Production",
					receipt = new ApplePurchase.Receipt
					{
						bundle_id = "localhost.apple",
						receipt_type = "ProductionSandbox",
						in_app = new[]
						{
							new ApplePurchase.Receipt.InApp
							{
								quantity = "1",
								product_id = "6RS",
								transaction_id = Guid.NewGuid().ToString("N"),
								purchase_date_ms = DateTime.UtcNow.AddMinutes(-15).ToLogTimeMs(),
								original_purchase_date_ms = DateTime.UtcNow.AddMinutes(-15).ToLogTimeMs(),
								expires_date_ms = DateTime.UtcNow.AddMinutes(-10).ToLogTimeMs(),
								web_order_line_item_id = Guid.NewGuid().ToString("N"),
								is_trial_period = "false"
							},
							new ApplePurchase.Receipt.InApp
							{
								quantity = "1",
								product_id = "6RS",
								transaction_id = Guid.NewGuid().ToString("N"),
								purchase_date_ms = DateTime.UtcNow.AddMinutes(-5).ToLogTimeMs(),
								original_purchase_date_ms = DateTime.UtcNow.AddMinutes(-5).ToLogTimeMs(),
								expires_date_ms = DateTime.UtcNow.AddMinutes(5).ToLogTimeMs(),
								web_order_line_item_id = Guid.NewGuid().ToString("N"),
								is_trial_period = "false"
							}
						}
					}
				};
			}
		}
	}
}