﻿using System;
using System.Text;
using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class HelpersTests
	{
		[TestMethod]
		public void TestConvertToSafeForUrlString()
		{
			const int number = 0x12345678;

			var bytes = BitConverter.GetBytes(number);
			if (!BitConverter.IsLittleEndian)
				Array.Reverse(bytes);

			var stringFromTestedMethod = StringHelper.ConvertToSafeForUrlString("01234567", bytes);

			var reversedString = new StringBuilder(stringFromTestedMethod.Length);
			for (var i = 0; i != stringFromTestedMethod.Length; ++i)
				reversedString.Append(stringFromTestedMethod[stringFromTestedMethod.Length - i - 1]);

			Assert.AreEqual(number, Convert.ToInt32(reversedString.ToString(), 8));
		}
	}
}