﻿using System;
using System.Globalization;

namespace Compass.Ufin.Business.Tests
{
	static class IdGenerator
	{
		private static long _generatedNumber = DateTime.UtcNow.Ticks;
		private static readonly object GeneratedNumberLock = new object();

		public static long GenerateTerminalDeviceNumber()
		{
			lock (GeneratedNumberLock)
			{
				++_generatedNumber;
			}
			return _generatedNumber;
		}

		private static readonly object UniqueLongNumberLock = new object();
		private static long _uniqueLongNumber;

		public static long GenerateUniqueLongNumber()
		{
			lock (UniqueLongNumberLock)
			{
				if (_uniqueLongNumber == 0)
					_uniqueLongNumber = (long)(DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds * 1000;
				else
					++_uniqueLongNumber;
			}

			return _uniqueLongNumber;
		}
	}
}