﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common.Filtering;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests.Filtering
{
	[TestClass]
	public class ErrorFilterTests
	{
		[TestMethod]
		public void EqualityOfMeans_FilterTest()
		{
			const decimal RelativeFuelSensorError = 0.05m;
			var absoluteFuelSensorError = 600 * RelativeFuelSensorError;
			var logTimeDelta = 5 * 60;
			var valueError = absoluteFuelSensorError;
			var errorFilter = new ErrorFilter<SensorLogRecord>(logTimeDelta, valueError, r => r.Value);

			var value1 = 377.6m;
			var value2 = value1 - 1.005m * valueError;
			var result = default(List<SensorLogRecord>);
			// Одно значение выходит за границы возможных ошибок - фильтруем
			result = errorFilter.Filter(new SensorLogRecord[]
			{
				new SensorLogRecord{ LogTime = 1633218876, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219056, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219207, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219209, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219389, Value = value2 },
				new SensorLogRecord{ LogTime = 1633219488, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219490, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219670, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219800, Value = value1 },
			})
				.ToList();
			Assert.AreNotEqual(value2, result.Min(x => x.Value));
			// Одно значение на границе возможных ошибок - оставляем
			value2 = value1 - 1.000m * valueError;
			result = errorFilter.Filter(new SensorLogRecord[]
			{
				new SensorLogRecord{ LogTime = 1633218876, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219056, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219207, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219209, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219389, Value = value2 },
				new SensorLogRecord{ LogTime = 1633219488, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219490, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219670, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219800, Value = value1 },
			})
				.ToList();
			Assert.AreEqual(value2, result.Min(x => x.Value));
			// Одно значение внутри границ возможных ошибок - оставляем
			value2 = value1 - 0.995m *valueError;
			result = errorFilter.Filter(new SensorLogRecord[]
			{
				new SensorLogRecord{ LogTime = 1633218876, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219056, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219207, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219209, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219389, Value = value2 },
				new SensorLogRecord{ LogTime = 1633219488, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219490, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219670, Value = value1 },
				new SensorLogRecord{ LogTime = 1633219800, Value = value1 },
			})
				.ToList();
			Assert.AreEqual(value2, result.Min(x => x.Value));
		}
	}
}