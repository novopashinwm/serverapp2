﻿using System;
using System.Linq;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class TimeZoneDstTests
	{
		private const string _dst = "{\"dstChanges\":[{\"on\":\"2017-01-03T21:00:00.000Z\",\"off\":\"2017-10-28T22:00:00.000Z\",\"delta\":60},{\"on\":\"2016-01-05T21:00:00.000Z\",\"off\":\"2016-10-29T22:00:00.000Z\",\"delta\":60},{\"on\":\"2015-01-06T21:00:00.000Z\",\"off\":\"2015-10-24T22:00:00.000Z\",\"delta\":60},{\"on\":\"2013-12-31T21:00:00.000Z\",\"off\":\"2014-10-25T22:00:00.000Z\",\"delta\":60},{\"on\":\"2013-01-01T21:00:00.000Z\",\"off\":\"2013-10-26T22:00:00.000Z\",\"delta\":60},{\"on\":\"2012-01-03T21:00:00.000Z\",\"off\":\"2012-10-27T22:00:00.000Z\",\"delta\":60}],\"baseOffset\":180}";

		private const string _rDst = "{\"dstChanges\":[{\"on\":\"2017-01-03T21:00:00.000Z\",\"off\":\"2017-10-28T22:00:00.000Z\",\"delta\":60},{\"on\":\"2016-01-05T21:00:00.000Z\",\"off\":\"2016-10-29T22:00:00.000Z\",\"delta\":60},{\"on\":\"2015-01-06T21:00:00.000Z\",\"off\":\"2015-10-24T22:00:00.000Z\",\"delta\":60},{\"on\":null,\"off\":\"2014-10-25T22:00:00.000Z\",\"delta\":60},{\"on\":\"2013-01-01T21:00:00.000Z\",\"off\":\"2013-10-26T22:00:00.000Z\",\"delta\":60},{\"on\":\"2012-01-03T21:00:00.000Z\",\"off\":\"2012-10-27T22:00:00.000Z\",\"delta\":60}],\"baseOffset\":180}";

		private const string _mDst = "{\"dstChanges\":[{\"on\":\"2017-01-03T21:00:00.000Z\",\"off\":\"2017-10-28T22:00:00.000Z\",\"delta\":60},{\"on\":\"2016-01-05T21:00:00.000Z\",\"off\":\"2016-10-29T22:00:00.000Z\",\"delta\":60},{\"on\":\"2015-01-06T21:00:00.000Z\",\"off\":\"2015-10-24T22:00:00.000Z\",\"delta\":60},{\"on\":null,\"off\":\"2014-10-25T22:00:00.000Z\",\"delta\":60},{\"on\":\"2013-01-01T21:00:00.000Z\",\"off\":\"2013-10-26T22:00:00.000Z\",\"delta\":60},{\"on\":\"2012-01-03T21:00:00.000Z\",\"off\":\"2012-10-27T22:00:00.000Z\",\"delta\":60}],\"baseOffset\":180}";

		private const string _mDstIE = "{\"dstChanges\":[{\"on\":\"2017-01-03T21:00:00.000Z\",\"off\":\"2017-10-28T22:00:00.000Z\",\"delta\":60},{\"on\":\"2016-01-05T21:00:00.000Z\",\"off\":\"2016-10-29T22:00:00.000Z\",\"delta\":60},{\"on\":\"2015-01-06T21:00:00.000Z\",\"off\":\"2015-10-24T22:00:00.000Z\",\"delta\":60},{\"on\":\"2014-01-01T00:00:00.000Z\",\"off\":\"2014-10-25T22:00:00.000Z\",\"delta\":60},{\"on\":\"2013-01-01T21:00:00.000Z\",\"off\":\"2013-10-26T22:00:00.000Z\",\"delta\":60},{\"on\":\"2012-01-03T21:00:00.000Z\",\"off\":\"2012-10-27T22:00:00.000Z\",\"delta\":60}],\"baseOffset\":180}";

		private const string _rDst2013 = "{\"dstChanges\":[],\"baseOffset\":240}";

		private const string _serStr = "Russian Standard Time;180;(UTC+03:00) Moscow, St. Petersburg, Volgograd (RTZ 2);Russia TZ 2 Standard Time;Russia TZ 2 Daylight Time;[01:01:0001;12:31:2010;60;[0;02:00:00;3;5;0;];[0;03:00:00;10;5;0;];][01:01:2011;12:31:2011;60;[0;02:00:00;3;5;0;];[0;00:00:00;1;1;6;];][01:01:2014;12:31:2014;60;[0;00:00:00;1;1;3;];[0;02:00:00;10;5;0;];];";

		private const string _st = "0ff62a3b-9b18-4f51-97d5-3a5ab6b23700;180;Custom Timezone;Standart;Daylight saving;[01:03:2012;10:27:2012;60;[1;21:00:00;1;3;];[1;22:00:00;10;27;];][01:01:2013;10:26:2013;60;[1;21:00:00;1;1;];[1;22:00:00;10;26;];][01:01:2014;10:25:2014;60;[1;00:00:00;1;1;];[1;22:00:00;10;25;];][01:06:2015;10:24:2015;60;[1;21:00:00;1;6;];[1;22:00:00;10;24;];][01:05:2016;10:29:2016;60;[1;21:00:00;1;5;];[1;22:00:00;10;29;];][01:03:2017;10:28:2017;60;[1;21:00:00;1;3;];[1;22:00:00;10;28;];];";

		private const string _mozilla = "f536d6fb-7f9a-4c35-bab9-5043c4d07537;180;Custom Timezone;Standart;Daylight saving;[01:03:2012;10:27:2012;60;[1;21:00:00;1;3;];[1;22:00:00;10;27;];][01:01:2013;10:26:2013;60;[1;21:00:00;1;1;];[1;22:00:00;10;26;];][12:31:2013;10:25:2014;60;[1;21:00:00;12;31;];[1;22:00:00;10;25;];][01:06:2015;10:24:2015;60;[1;21:00:00;1;6;];[1;22:00:00;10;24;];][01:05:2016;10:29:2016;60;[1;21:00:00;1;5;];[1;22:00:00;10;29;];][01:03:2017;10:28:2017;60;[1;21:00:00;1;3;];[1;22:00:00;10;28;];];";

		[TestMethod]
		public void TestTimeZones()
		{
			var timeZone = TimeZoneHelper.ParseDstString(_dst);
			var testInput = @"2011-01-01T20:00:00=>2011-01-01T23:00:00
2012-01-03T17:59:59=>2012-01-03T20:59:59
2012-01-03T18:00:00=>2012-01-03T22:00:00
2012-10-27T17:59:59=>2012-10-27T21:59:59
2012-10-27T18:00:00=>2012-10-27T21:00:00
2013-12-31T17:59:59=>2013-12-31T20:59:59
2013-12-31T18:00:00=>2013-12-31T22:00:00
2013-12-31T20:00:01=>2014-01-01T00:00:01
2014-10-25T17:59:59=>2014-10-25T21:59:59
2014-10-25T18:00:00=>2014-10-25T21:00:00
2013-01-01T17:59:59=>2013-01-01T20:59:59
2013-01-01T18:00:00=>2013-01-01T22:00:00
2013-10-26T17:59:59=>2013-10-26T21:59:59
2013-10-26T18:00:00=>2013-10-26T21:00:00
2015-02-02T15:00:00=>2015-02-02T19:00:00";
			TestTimeZone(timeZone, testInput);
		}

		[TestMethod]
		public void TestTimeZonesForRussia2014()
		{
			var timeZone = TimeZoneHelper.ParseDstString(_rDst);
			var testInput = @"2014-01-01T00:00:00=>2014-01-01T04:00:00
2014-10-25T00:00:00=>2014-10-25T04:00:00
2014-10-27T00:00:00=>2014-10-27T03:00:00";
			TestTimeZone(timeZone, testInput);
			var currentTimeZone = TimeZoneInfo.Local;
			TestTimeZone(currentTimeZone, testInput);
		}

		[TestMethod]
		public void TestMoscowTimeZone()
		{
			// Chrome 
			var timeZone = TimeZoneHelper.ParseDstString(_mDst);
			var timeZones = TimeZoneHelper.GetEqualTimeZoneInfos(timeZone);
			Assert.IsTrue(timeZones.Any());

			// IE
			var ieTimeZone = TimeZoneHelper.ParseDstString(_mDstIE);
			var ieTimeZones = TimeZoneHelper.GetEqualTimeZoneInfos(ieTimeZone);
			Assert.IsTrue(ieTimeZones.Any());

			// Обновление часового пояса не установлено.
			var noUpdateTimeZone = TimeZoneHelper.ParseDstString(_rDst2013);
			var noUpdateTimeZones = TimeZoneHelper.GetEqualTimeZoneInfos(noUpdateTimeZone).ToList();
			Assert.IsTrue(noUpdateTimeZones.Any());

			// Таймзона при смене языка системы.
			timeZone = TimeZoneInfo.FromSerializedString(_serStr);
			var tz = TimeZoneHelper.GetEqualTimeZoneInfos(timeZone).ToList().FirstOrDefault();
			Assert.IsNotNull(tz);

			timeZone = TimeZoneInfo.FromSerializedString(_st);
			tz = TimeZoneHelper.GetEqualTimeZoneInfos(timeZone).ToList().FirstOrDefault();
			Assert.IsNotNull(tz);

			timeZone = TimeZoneInfo.FromSerializedString(_mozilla);
			tz = TimeZoneHelper.GetEqualTimeZoneInfos(timeZone).ToList().FirstOrDefault();
			Assert.IsNotNull(tz);
		}

		private void TestTimeZone(TimeZoneInfo timeZone, string testInput)
		{
			var tests = testInput.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var test in tests)
			{
				var parts = test.Split(new[] { "=>" }, StringSplitOptions.RemoveEmptyEntries);
				var utcDate = DateTime.Parse(parts[0]);
				var convertToLocalTime = TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZone);
				var localDate = DateTime.Parse(parts[1]);
				Assert.AreEqual(convertToLocalTime, localDate);
			}
		}

		[TestMethod]
		public void GetFingerprintForMsk()
		{
			var sourceTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
			var fingerprint = sourceTimeZone.ToFingerprint(2014);

			var fingerprintJson = JsonHelper.SerializeObjectToJson(fingerprint);
			var resolvedTimeZone = TimeZoneHelper.GetEqualTimeZoneInfos(TimeZoneHelper.ParseDstString(fingerprintJson)).First();

			Assert.AreEqual(3, resolvedTimeZone.BaseUtcOffset.TotalHours);
		}
	}
}