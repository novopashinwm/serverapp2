﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.Infrastructure.Interfaces.UserName;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.Billing;
using Interfaces.Web;
using Interfaces.Web.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AddTrackerResult = FORIS.TSS.BusinessLogic.ResultCodes.AddTrackerResult;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;
using TrackingSchedule = FORIS.TSS.BusinessLogic.DTO.TrackingSchedule;
using Vehicle = FORIS.TSS.BusinessLogic.DTO.Vehicle;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class WebPersonalServerTests
	{
		private const string UserLogin = "mt2changed";
		[TestMethod]
		public void CreateDepartment()
		{
			var login = IdGenerator.GenerateTerminalDeviceNumber().ToString(CultureInfo.InvariantCulture);

			var server = GetPersonalServerByLogin(UserLogin);
			int departmentId;
			var result = server.CreateDepartment(login, login, null, out departmentId);

			Assert.AreEqual(CreateDepartmentResult.Success, result);
		}
		[TestMethod]
		public void CreateDepartmentByEmail()
		{
			var login = IdGenerator.GenerateTerminalDeviceNumber().ToString(CultureInfo.InvariantCulture);

			var server = GetPersonalServerByLogin(UserLogin);
			int departmentId;
			var result = server.CreateDepartment(login, login + "@test.local", null, out departmentId);

			Assert.AreEqual(CreateDepartmentResult.Success, result);
		}
		[TestMethod]
		public void SetVehicleDeviceType()
		{
			var server = Helper.CreateNewCorporateUser();
			var addTrackerResult = server.AddTrackerByPhoneNumber(Helper.FindFreeMsisdn());
			Assert.AreEqual(AddTrackerResult.Success, addTrackerResult.Code);
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var vehicleToChange = server.GetVehicles(
				new GetVehiclesArgs { IncludeAttributes = true, VehicleIDs = new[] { addTrackerResult.VehicleId.Value } }).Single();

			Assert.IsNotNull(vehicleToChange.controllerType);
			Assert.IsTrue(vehicleToChange.rights.Contains(SystemRight.ChangeDeviceConfigBySMS));

			ControllerType newControllerType = server.GetControllerTypeAllowedToAdd().First(ct => ct.Name == "AvtoGrafCan");
			server.SetVehicleDeviceType(vehicleToChange.id, newControllerType.Name);


			var vehicleAfterChange = server.GetVehicles(
				new GetVehiclesArgs { IncludeAttributes = true })
										   .First(v => v.id == vehicleToChange.id);
			Assert.AreEqual(vehicleAfterChange.controllerType.Name, newControllerType.Name);
		}
		private IWebPersonalServer GetPersonalServerByLogin(string userName)
		{
			var tdSessionManager = new TDSessionManager();
			var tdServer = new TDServer(tdSessionManager, ConfigurationManager.AppSettings);
			tdSessionManager.Server = tdServer;

			var gi = new GenericIdentity(userName);
			IPAddress ipAddress = IPAddress.None;

			Thread.CurrentPrincipal = new CustomPrincipal(gi, ipAddress);

			var application =
				(IWebPersonalServer)
				tdSessionManager.CreateSession(Guid.NewGuid(), GetType().Assembly.ImageRuntimeVersion);
			return application;
		}
		private IWebPersonalServer GetPersonalServerById(int operatorId)
		{
			var tdSessionManager = new TDSessionManager();
			var tdServer = new TDServer(tdSessionManager, ConfigurationManager.AppSettings);
			tdSessionManager.Server = tdServer;

			var application = (IWebPersonalServer)tdSessionManager.CreateSession(operatorId, Guid.NewGuid(), GetType().Assembly.ImageRuntimeVersion, IPAddress.None);
			return application;
		}
		[TestMethod]
		public void CommandCreated()
		{
			var server = Helper.CreateNewPhysicalUser();
			var registeredCommand = server.RegisterCommand(
				new Command
				{
					TargetID = server.GetVehicles().First().id,
					Type = CmdType.AskPosition
				});

			Assert.IsNotNull(registeredCommand.Created);
			var vehicle = server.GetVehicles(new GetVehiclesArgs { IncludeLastData = true, IncludeAttributes = true }).First();

			using (var entities = new Entities())
			{
				Assert.AreEqual(
					server.SessionInfo.SessionId.ToString(),
					entities.Command_Parameter.Where(cp => cp.Command.ID == registeredCommand.Id && cp.Key == Command_Parameter.SessionID).Select(cp => cp.Value).First());
			}

			Assert.IsNotNull(vehicle.commands.First(c => c.Type == CmdType.AskPosition).Created);
		}
		[TestMethod]
		public void CancelCommand()
		{
			var server = Helper.CreateNewPhysicalUser();
			var addTrackerResult = server.AddTrackerByPhoneNumber(Helper.FindFreePhone());
			Assert.IsNotNull(addTrackerResult.VehicleId);
			Assert.AreEqual(SetVehicleControllerFieldResult.Success,
				server.SetVehicleDeviceType(addTrackerResult.VehicleId.Value, ControllerType.Names.AvtoGraf));

			var vehicleIDs = new[] { addTrackerResult.VehicleId.Value };
			var vehicle = server
				.GetVehicles(new GetVehiclesArgs { VehicleIDs = vehicleIDs })
				.First(v => server.GetCommandTypes(v.id).Any());

			var cmdType = CmdType.Setup;

			server.RegisterCommand(
				new Command
				{
					TargetID = vehicle.id,
					Type = cmdType
				});

			vehicle = server.GetVehicles(new GetVehiclesArgs { VehicleIDs = vehicleIDs, IncludeLastData = true }).First();

			var command = vehicle.commands.First(c => c.Type == cmdType);

			var commandIsCancelled = server.CancelCommand(command.Id);

			Assert.IsTrue(commandIsCancelled);
		}
		[TestMethod]
		public void AppClient()
		{
			var server = Helper.CreateNewPhysicalUser();

			var appId = Guid.NewGuid().ToString().ToUpper();
			var gcmId = Guid.NewGuid().ToString().ToUpper();
			server.UpdateClientRegistration(appId, gcmId, null);
			var lowerCaseAppId = appId.ToLower();
			server.UpdateClientRegistration(lowerCaseAppId, null, lowerCaseAppId);
			server.UpdateClientRegistration(appId, null, null);

			using (var entities = new Entities())
			{
				var appClient = entities.AppClient.First(x => x.AppId == appId);
				Assert.AreEqual(appClient.Operator_ID, server.OperatorId);
				//Проверка регистрозависимости
				Assert.IsFalse(entities.AppClient.Any(x => x.AppId == lowerCaseAppId));
			}
		}
		[TestMethod]
		public void WrongMessageDestination()
		{
			var server = Helper.CreateNewPhysicalUser();
			var appId = Guid.NewGuid().ToString().ToUpper();
			var gcmId = Guid.NewGuid().ToString().ToUpper();
			server.UpdateClientRegistration(appId, gcmId, null);

			var server2 = Helper.CreateNewPhysicalUser();
			var appId2 = Guid.NewGuid().ToString().ToUpper();
			var gcmId2 = Guid.NewGuid().ToString().ToUpper();
			server2.UpdateClientRegistration(appId2, gcmId2, null);

			using (var entities = new Entities())
			{
				var singlePushToAppDestinationPerUser = entities.GetConstantAsBool(Constant.SinglePushToAppDestinationPerUser);

				var ormOperator = entities.GetOperator(server.OperatorId);
				var messages = entities.CreateAppNotification(ormOperator.OPERATOR_ID, "Hello", "World");
				Assert.AreEqual(singlePushToAppDestinationPerUser ? 1 : 2, messages.Count);
				entities.SaveChangesBySession(server2.SessionInfo.SessionId);

				var message = messages.First();
				server2.WrongMessageDestination(message.MESSAGE_ID, appId);
				messages = entities.CreateAppNotification(ormOperator.OPERATOR_ID, "Hello", "World");
				Assert.AreEqual(singlePushToAppDestinationPerUser ? 0 : 1, messages.Count);
				entities.SaveChangesBySession(server2.SessionInfo.SessionId);
			}
		}
		[TestMethod]
		public void AppClientAndroid()
		{
			var server = GetPersonalServerByLogin(UserLogin);

			var appId = Guid.NewGuid().ToString();
			var gcmRegistrationId = Guid.NewGuid().ToString();
			server.UpdateClientRegistration(appId, gcmRegistrationId, null);

			using (var entities = new Entities())
			{
				var appClientCount = entities.AppClient.Count(
					x => x.Operator_ID == server.OperatorId &&
						 x.AppId == appId &&
						 x.Contact.Type == (int)ContactType.Android &&
						 x.Contact.Value == gcmRegistrationId);
				Assert.AreEqual(appClientCount, 1);
			}
		}
		[TestMethod]
		public void AppClientApple()
		{
			var server = GetPersonalServerByLogin(UserLogin);

			//Проверка регистронезависимости для AppId
			var appId = Guid.NewGuid().ToString();
			var apnDeviceToken = Guid.NewGuid().ToByteArray().ToHexString();
			server.UpdateClientRegistration(appId, null, apnDeviceToken);

			using (var entities = new Entities())
			{
				var appClientCount = entities.AppClient.Count(
					x => x.Operator_ID == server.OperatorId &&
						 x.AppId == appId &&
						 x.Contact.Type == (int)ContactType.Apple &&
						 x.Contact.Value == apnDeviceToken);
				Assert.AreEqual(1, appClientCount);
			}
		}
		[TestMethod]
		public void VehicleAccess()
		{
			var server = GetPersonalServerByLogin(UserLogin);
			var login = IdGenerator.GenerateTerminalDeviceNumber().ToString(CultureInfo.InvariantCulture);
			int departmentId;
			var resultDepartment = server.CreateDepartment(login, login, null, out departmentId);
			Assert.AreEqual(CreateDepartmentResult.Success, resultDepartment);

			var userServer = GetPersonalServerByLogin(login);
			var msisdn = Helper.FindFreeMsisdn();
			var resultTracker = userServer.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.Success, resultTracker.Code);
			Assert.IsNotNull(resultTracker.VehicleId);

			var vehiclesArgs = new GetVehiclesArgs
			{
				VehicleIDs = new[] { resultTracker.VehicleId.Value }
			};
			var vehicles = userServer.GetVehicles(vehiclesArgs);
			Assert.IsTrue(vehicles.Any(v => v.id == resultTracker.VehicleId.Value));
			var departments = server.SearchAdminDepartments(msisdn);
			Assert.IsTrue(departments.Any());
			var department = departments.FirstOrDefault(item => item.id == departmentId);
			Assert.IsNotNull(department);
			server.SelectDepartment(department.id);

			vehicles = server.GetVehicles();
			Assert.IsTrue(vehicles.Any(v => v.id == resultTracker.VehicleId.Value));
		}
		[TestMethod]
		public void CheckVehicleOnlineStatus()
		{
			var server = GetPersonalServerByLogin(UserLogin);
			var args = new GetVehiclesArgs
			{
				IncludeAttributes = true,
				IncludeLastData = true,
			};
			var vehicles = server.GetVehicles(args);
			Assert.IsTrue(vehicles.All(item => item.OnlineStatus != null));
		}
		private IWebPersonalServer GetSuperAdminServer()
		{
			string login;
			using (var entities = new Entities())
			{
				var config =
					entities.CONSTANTS.Where(c => c.NAME == "SalesOperatorGroupID").Select(c => c.VALUE).First();
				var operatorGroupId = int.Parse(config);
				login =
					entities.OPERATOR.Where(o => o.ContainingGroups.Any(og => og.OPERATORGROUP_ID == operatorGroupId))
							.Select(o => o.LOGIN)
							.First();

			}

			return GetPersonalServerByLogin(login);
		}
		[TestMethod]
		public void CreateTracker()
		{
			var server = Helper.CreateNewPhysicalUser();
			var msisdn = "111111111";
			var result = server.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.InvalidPhoneNumber, result.Code);

			msisdn = Helper.FindFreeMsisdn();
			result = server.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.Success, result.Code);
			Assert.IsNotNull(result.VehicleId);
			Assert.IsTrue(server.GetVehicles().Any(v => v.id == result.VehicleId.Value));

			using (var entities = new Entities())
			{
				entities.Log_Time.AddObject(new Log_Time
				{
					Vehicle_ID = result.VehicleId.Value,
					InsertTime = DateTime.UtcNow,
					Log_Time1 = TimeHelper.GetSecondsFromBase()
				});
				entities.SaveChanges();
			}

			result = server.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.PhoneAlreadyExists, result.Code);

			server = Helper.CreateNewPhysicalUser();
			result = server.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.PhoneAlreadyExists, result.Code);
			result = server.AddTrackerByPhoneNumber(msisdn + msisdn);
			Assert.AreEqual(AddTrackerResult.InvalidPhoneNumber, result.Code);
		}
		[TestMethod]
		public void AddAndInviteMtsFriend()
		{
			var server = Helper.CreateNewPhysicalUser();

			var friendPhone = Helper.FindFreePhone("910");

			Assert.AreEqual(0, server.FindUsers(friendPhone).Count);
		}
		[TestMethod]
		public void SimIdByLoginPassword()
		{
			var superAdmin = GetSuperAdminServer();
			var number = IdGenerator.GenerateUniqueLongNumber();
			int departmentId;
			var login = "Login" + number;
			Assert.AreEqual(CreateDepartmentResult.Success,
							superAdmin.CreateDepartment("Name" + number, login, null, out departmentId));

			var server = GetPersonalServerByLogin(login);
			var userName = Guid.NewGuid().ToString("N");
			server.SetUserName(userName);

			var simId = server.GetOrCreateSimId();
			Assert.IsNotNull(simId);

			Assert.AreEqual(
				1,
				server.GetVehicles().Count(v => v.Supports(DeviceCapability.AllowAskPosition)));

			var vehicle = server.GetVehicles().Single();

			var command = server.RegisterCommand(new Command { TargetID = vehicle.id, Type = CmdType.AskPosition });
			Assert.AreEqual(CmdStatus.Received, command.Status);
		}
		[TestMethod]
		public void NewIndiaVehicleSupportsLimitedSmsQuantity()
		{
			int maxSmsQuantity = 50;

			var server = Helper.CreateNewCorporateUser();

			using (var entities = new Entities())
			{
				var department = entities.DEPARTMENT.First(d => d.DEPARTMENT_ID == server.Department.id);
				department.Country = entities.Country.First(c => c.Name == Country.India);
				entities.SaveChanges();
			}

			var addTrackerResult = server.AddTrackerByPhoneNumber(Helper.FindFreeMsisdn());
			Assert.AreEqual(AddTrackerResult.Success, addTrackerResult.Code);
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var args = new GetVehiclesArgs
			{
				IncludeLastData = true,
				VehicleIDs = new[] { addTrackerResult.VehicleId.Value }
			};
			var vehicle = server.GetVehicles(args).Single();

			Assert.AreEqual(maxSmsQuantity, vehicle.billingServices.Single().Status[RenderedServiceType.SMS].QuantityLeft);

			using (var entities = new Entities())
			{
				var billingServer = new BillingServer(entities);
				for (var i = 0; i != maxSmsQuantity; ++i)
				{
					Assert.AreEqual(
						BillingResult.Success,
						billingServer.AllowRenderService(server.OperatorId, addTrackerResult.VehicleId.Value,
														 PaidService.SendNotificationSms));

					var payerVehicleId = billingServer.ChargeServiceRendering(server.OperatorId,
																			  addTrackerResult.VehicleId.Value,
																			  PaidService.SendNotificationSms);
					Assert.AreEqual(addTrackerResult.VehicleId, payerVehicleId);
					vehicle = server.GetVehicles(args).Single();
					Assert.AreEqual(maxSmsQuantity - i - 1, vehicle.billingServices.Single().Status[RenderedServiceType.SMS].QuantityLeft);
				}
			}
		}
		[TestMethod]
		public void DeleteOperatorWithConfirmedContact()
		{
			var adminServer = GetSuperAdminServer();
			var email = IdGenerator.GenerateUniqueLongNumber() + "@" + IdGenerator.GenerateUniqueLongNumber() + ".ru";

			Operator @operator1;
			var login1 = Guid.NewGuid().ToString("N");
			Assert.AreEqual(adminServer.CreateDepartmentOperator(login1, login1, login1, out @operator1), CreateOperatorResult.Success);

			var operatorServer1 = Helper.GetServer(@operator1.id);
			operatorServer1.AddConfirmedContact(ContactType.Email, email);

			adminServer.DeleteOperator(@operator1.id);

			Operator @operator2;
			var login2 = Guid.NewGuid().ToString("N");
			Assert.AreEqual(adminServer.CreateDepartmentOperator(login2, login2, login2, out @operator2), CreateOperatorResult.Success);

			var operatorServer2 = Helper.GetServer(@operator2.id);
			operatorServer2.AddConfirmedContact(ContactType.Email, email);
		}
		[TestMethod]
		public void ChangeControllerType()
		{
			var server = Helper.CreateNewPhysicalUser();
			var vehicle = server.GetVehicles().FirstOrDefault();
			Assert.IsNotNull(vehicle);

			using (var entities = new Entities())
			{
				if (!entities.GetConstantAsBool(Constant.AddDefaultRules))
					return;

				var controllerTypeWithNoAlarm = entities.CONTROLLER_TYPE.FirstOrDefault(
					t => t.CONTROLLER_SENSOR.All(s => s.CONTROLLER_SENSOR_LEGEND.Number != (int)SensorLegend.Alarm));
				Assert.IsNotNull(controllerTypeWithNoAlarm);
				server.SetVehicleDeviceType(vehicle.id, controllerTypeWithNoAlarm.TYPE_NAME);

				var owners = entities.GetVehicleOwnersQuery(vehicle.id).Select(o => o.OPERATOR_ID).ToList();
				Assert.IsTrue(owners.Contains(server.OperatorId));

				bool addDefaultRules = entities.GetConstantAsBool(Constant.AddDefaultRules);

				if (addDefaultRules)
				{
					foreach (var ownerId in owners)
						Assert.IsFalse(entities.AlarmRuleExists(ownerId, vehicle.id));
				}

				var controllerType =
					entities.CONTROLLER_TYPE.FirstOrDefault(
						t => t.CONTROLLER_SENSOR.Any(s => s.CONTROLLER_SENSOR_LEGEND.Number == (int)SensorLegend.Alarm));
				Assert.IsNotNull(controllerType);

				server.SetVehicleDeviceType(vehicle.id, controllerType.TYPE_NAME);

				if (addDefaultRules)
				{
					foreach (var ownerId in owners)
						Assert.IsTrue(entities.AlarmRuleExists(ownerId, vehicle.id));
				}
			}
		}
		[TestMethod]
		public void AddEmptyTracker()
		{
			var server = Helper.CreateNewPhysicalUser();

			var addTrackerResult = server.AddTrackerAsEmpty();
			Assert.AreEqual(AddTrackerResult.Success, addTrackerResult.Code);
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var vehicleId = addTrackerResult.VehicleId.Value;
			var attributes = server.GetVehicleAttributes(vehicleId);

			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.TrackerType));

			Assert.AreEqual(
				SetVehicleControllerFieldResult.Success,
				server.SetVehicleAttribute(vehicleId, VehicleAttributeName.TrackerType, ControllerType.Names.AvtoGraf));

			attributes = server.GetVehicleAttributes(vehicleId);
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.Phone));
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.DeviceId));
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.Password));
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.ApnName));
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.ApnUser));
			Assert.IsTrue(attributes.Any(a => a.Name == VehicleAttributeName.ApnPassword));

			var newAttributeValues = new Dictionary<VehicleAttributeName, string>
			{
				{VehicleAttributeName.Phone, Helper.FindFreeMsisdn()},
				{VehicleAttributeName.DeviceId, IdGenerator.GenerateUniqueLongNumber().ToString()},
				{VehicleAttributeName.Password, "defpassw"},
				{VehicleAttributeName.ApnName, "apnName"},
				{VehicleAttributeName.ApnUser, "apnUser"},
				{VehicleAttributeName.ApnPassword, "apnPassword"}
			};
			var setVehicleAttributesResult =
				server.SetVehicleAttributes(vehicleId,
				newAttributeValues);

			Assert.AreEqual(SetVehicleDevicePhoneResult.Success, setVehicleAttributesResult[VehicleAttributeName.Phone]);
			Assert.AreEqual(SetVehicleDeviceIDResult.Success, setVehicleAttributesResult[VehicleAttributeName.DeviceId]);
			Assert.AreEqual(SetVehicleDevicePasswordResult.Success, setVehicleAttributesResult[VehicleAttributeName.Password]);
			Assert.AreEqual(SetVehicleFieldResult.Success, setVehicleAttributesResult[VehicleAttributeName.ApnName]);
			Assert.AreEqual(SetVehicleFieldResult.Success, setVehicleAttributesResult[VehicleAttributeName.ApnUser]);
			Assert.AreEqual(SetVehicleFieldResult.Success, setVehicleAttributesResult[VehicleAttributeName.ApnPassword]);

			var changedAttributes = server.GetVehicleAttributes(vehicleId)
				.ToDictionary(a => a.Name, a => a.Value);
			foreach (var attributeName in newAttributeValues.Keys)
			{
				Assert.AreEqual(newAttributeValues[attributeName], changedAttributes[attributeName]);
			}
		}
		[TestMethod]
		public void AddAndDeleteTracker()
		{
			var server = Helper.CreateNewPhysicalUser();

			Assert.IsFalse(server.DeleteVehicle(server.GetVehicles().Single().id) == DeleteVehicleResult.Success);

			var msisdn = Helper.FindFreeMsisdn();

			for (var i = 0; i != 2; ++i)
			{
				var addTrackerResult = server.AddTrackerByPhoneNumber(msisdn);
				var code = addTrackerResult.Code;
				Assert.AreEqual(AddTrackerResult.Success, code);
				Assert.IsNotNull(addTrackerResult.VehicleId);

				var vehicle = server.GetVehicles().First(v => v.id == addTrackerResult.VehicleId.Value);
				Assert.IsTrue(vehicle.Removable ?? false);

				if (i != 0)
					AddVehicleLog(vehicle);

				Assert.IsTrue(server.DeleteVehicle(addTrackerResult.VehicleId.Value) == DeleteVehicleResult.Success);
			}
		}
		private static void AddVehicleLog(Vehicle vehicle)
		{
			using (var entities = new Entities())
			{
				entities.Log_Time.AddObject(
					new Log_Time
					{
						Vehicle_ID = vehicle.id,
						Log_Time1 = TimeHelper.GetSecondsFromBase(),
						InsertTime = DateTime.UtcNow,
						Media = null
					});
				entities.SaveChanges();
			}
		}
		[TestMethod]
		public void RestoreVehicleAfterDelete()
		{
			var adminLogin = IdGenerator.GenerateTerminalDeviceNumber().ToString(CultureInfo.InvariantCulture);

			var superAdminServer = Helper.GetSuperAdminServer();
			int departmentId;
			var result = superAdminServer.CreateDepartment(adminLogin, adminLogin, null, out departmentId);
			Assert.AreEqual(CreateDepartmentResult.Success, result);

			var adminOneServer = Helper.GetServerByLogin(adminLogin);
			Assert.AreEqual(0, adminOneServer.GetVehicles().Count);
			var msisdn = Helper.FindFreeMsisdn();
			var addTrackerResult = adminOneServer.AddTrackerByPhoneNumber(msisdn);
			Assert.AreEqual(AddTrackerResult.Success, addTrackerResult.Code);
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var deviceId = adminLogin;
			var vehicleId = addTrackerResult.VehicleId.Value;
			Assert.AreEqual(
				SetVehicleDeviceIDResult.Success,
				adminOneServer.SetVehicleDeviceId(vehicleId, deviceId));

			AddVehicleLog(adminOneServer.GetVehicles().Single());

			Assert.IsTrue(adminOneServer.DeleteVehicle(vehicleId) == DeleteVehicleResult.Success);
			Assert.AreEqual(0, adminOneServer.GetVehicles().Count);

			using (var entities = new Entities())
			{
				var removedDepartmentExtId = entities.CONSTANTS.Where(c => c.NAME == CONSTANTS.RemovedDepartmentExtId).Select(c => c.VALUE).FirstOrDefault();
				Assert.IsNotNull(removedDepartmentExtId);
				var department = entities.DEPARTMENT.FirstOrDefault(d => d.ExtID == removedDepartmentExtId);
				Assert.IsNotNull(department);

				superAdminServer.SelectDepartment(department.DEPARTMENT_ID);
			}

			var vehicle = superAdminServer.GetVehicles().FirstOrDefault(v => v.id == vehicleId);
			Assert.IsNotNull(vehicle);
			var note = superAdminServer.GetVehicleAttributes(vehicleId, VehicleAttributeName.Notes).Single().Value;
			Assert.IsTrue(note.Contains(msisdn));
			Assert.IsTrue(note.Contains(deviceId));

			//Восстановление
			superAdminServer.MoveVehicle(vehicleId, departmentId);
			Assert.AreEqual(1, adminOneServer.GetVehicles().Count);
		}
		[TestMethod]
		public void CrossNetworkOperatorFriendship()
		{
			var firstMsisdn = Helper.FindFreePhone("916");
			var firstServer = Helper.RegisterMsisdn(firstMsisdn);
			var secondMsisdn = Helper.FindFreePhone("926");

			Assert.AreEqual(ProposeFriendshipResult.InvitationSent, firstServer.ProposeFriendship(secondMsisdn));

			var secondServer = Helper.RegisterMsisdn(secondMsisdn);
			var secondUserVehicelId = secondServer.GetVehicles().Single().id;
			Assert.IsFalse(firstServer.GetVehicles().Any(v => v.id == secondUserVehicelId));

			secondServer.SetReplacedAccessToVehicle(firstMsisdn, secondUserVehicelId, SystemRight.VehicleAccess, true);

			Assert.IsTrue(firstServer.GetVehicles().Any(v => v.id == secondUserVehicelId));

			var lastMessageId = Helper.GetLastMessageId();
			var thirdMsisdn = Helper.FindFreePhone("926");
			secondServer.SetReplacedAccessToVehicle(thirdMsisdn, secondUserVehicelId, SystemRight.VehicleAccess, true);

			Assert.AreNotEqual(lastMessageId, Helper.GetLastMessageId());

			var thirdServer = Helper.GetPersonalServerByMsisdn(thirdMsisdn);
			Assert.IsTrue(thirdServer.GetVehicles().Any(v => v.id == secondUserVehicelId));
		}
		[TestMethod]
		public void LockDepartment()
		{
			var server = Helper.CreateNewCorporateUser();
			string login;
			string password;
			using (var entities = new Entities())
			{
				var adminOperator = entities.OPERATOR.First(o => o.OPERATOR_ID == server.OperatorId);
				login = adminOperator.LOGIN;
				password = adminOperator.PASSWORD;
			}

			var adminServer = Helper.GetSuperAdminServer();
			adminServer.LockDepartment(server.Department.id);

			var tdSessionManager = new TDSessionManager();
			var loginResult = tdSessionManager.CheckPasswordForWeb(login, password);
			Assert.IsTrue(loginResult.IsLocked);

			adminServer.UnlockDepartment(server.Department.id);
			loginResult = tdSessionManager.CheckPasswordForWeb(login, password);
			Assert.IsFalse(loginResult.IsLocked);
		}
		[TestMethod]
		public void TestVehicleFields()
		{
			var server = Helper.CreateNewPhysicalUser();
			var vehicle = server.GetVehicles().FirstOrDefault();
			using (var entities = new Entities())
			{
				var department = new DEPARTMENT
				{
					NAME = Guid.NewGuid().ToString("N")
				};
				entities.DEPARTMENT.AddObject(department);
				var entity = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicle.id);
				entity.DEPARTMENT = department;

				entities.Allow(entities.GetOperator(server.OperatorId),
								department,
								SystemRight.SecurityAdministration);
				entities.SaveChangesBySession(server.SessionInfo.SessionId);
			}


			const string ipAddress = "127.0.0.1";
			const VehicleKind vehicleKind = VehicleKind.Bus;
			const decimal fuelSpentStandart = 10.12m;
			const decimal fuelSpentStandartIndia = 10.13m;
			const decimal fuelSpentStandartMh = 20.12m;
			const decimal fuelSpentStandartGas = 1.12m;
			const int maxAllowedSpeed = 90;
			const string garageNumber = "123";
			const string publicNumber = "234";
			const string vehicleType = "supecar";
			const int fuelTankVolume = 100;
			const int fuelType = 1;
			const string ownerFirstName = "ownerFirstName";
			const string ownerLastName = "ownerLastName";
			const string ownerSecondName = "ownerSecondName";
			const string ownerPhone1 = "79166147991";
			const string ownerPhone2 = "79166147992";
			const string ownerPhone3 = "79166147993";
			const string image = "/9j/4AAQSkZJRgABAQEAYABgAAD/4QBoRXhpZgAASUkqAAgAAAAEABoBBQABAAAAPgAAABsBBQABAAAARgAAACgBAwABAAAAAwAAADEBAgARAAAATgAAAAAAAACjkwAA6AMAAKOTAADoAwAAUGFpbnQuTkVUIHYzLjUuNQAA/9sAQwABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/9sAQwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/8AAEQgAFAAUAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+Xf29f8AgvH+2H4d/bZ/ao+HOg/Gfxr4b0b4Z/tFfGH4T+FfDXg/W7zRNLsdB+HfxE8QeCNCtYNOsJEWfUJrLRbaS/vGRrnUdQlnnkJaRVCfEH/gpb/wUo+Aeh+LdS/ap+Nnx/8A2btZsNE0m8+G/gn4iHxra+LfjLrOuWWnataWngeyllsrKbw1o2i6pYat4z8bTagdI8Mpf6Zo5i1DxPqlloUv5b6ppnjy/wD+C7n7ZPiD4eaZ8LNd8SfCv9rj/goT8bYdD+Mt7p2neBtYsvgr4h+OXxO1HR2vtSuLeLTvE2rWvheTTPBOrxGSXQPGF3oniCaM2WlXbx8Z/wAFYf8Agq9cf8FDx8IfA/hnwOPDHwt+Dlpdax4av/F+naLe/E6HxP4x0vT28Z+H7XxLYNczaX8ObHULWGLS/DFneNY6pqdi/i68t7S8v4NN0xWfe/y/4P8Alv6WXs6l/jV99tEnZa+as9vM/uR/4Ie/8FTfjf8AtQfsh+LvF/xR1eH4g6z4O+PPi34e6T4h8QSXE+tnw/YeBPhl4qtbLUtQhkSTU5rXUPF+qeVe3ZkuhaSW9m0jQ2kIUr8i/wDg2EZm/YI+LpYlj/w194+5PJ/5Ix8AaKY7NaPVpK77vv8AM/Tr/goj/wAG4H/BPf8AaN+P3xK/aS1vVfj78PPHnxb8Q33jLx1pnwu8a+BNG8H6r4v1aQ3Gu+JY9E8SfC7xbNY6p4hv2l1XW2s9Rhtb/Vrm81N7Vby8uppvz08K/wDBrf8A8E99Y1SOzvPip+18YXmVG8r4ifCFG2lucN/woZsH3AoooHd939/9dkf1ef8ABPL/AIJ4/s3f8E9f2cdM+AH7PWi61D4Qk8S6z451zWPGl3o/iXxh4r8Y+ILTSbDU/EXiPWU0HTba5vn0vQ9E0i3jstN0+ytNL0jT7O2tIo4BkoooEf/Z";
			const string icon = "default";

			using (var entities = new Entities())
			{
				var entity = entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicle.id);
				var owner =
					entities.VEHICLE_OWNER.Where(o => o.VEHICLE.VEHICLE_ID == vehicle.id)
						.Select(o => o.OWNER)
						.FirstOrDefault();

				Assert.IsNotNull(entity);
				Assert.IsNotNull(vehicle);
				Assert.IsNull(owner);

				Assert.IsFalse(entities.VEHICLE_PICTURE.Any(p => p.VEHICLE_ID == vehicle.id));
				Assert.IsFalse(vehicle.HasIP.HasValue && vehicle.HasIP.Value);
				Assert.AreNotEqual(vehicle.vehicleKind, vehicleKind);
				Assert.AreNotEqual(entity.FuelSpendStandardLiter, fuelSpentStandart * 100);
				Assert.AreNotEqual(vehicle.MaxAllowedSpeed, maxAllowedSpeed);
				Assert.AreNotEqual(vehicle.garageNum, garageNumber);
				Assert.AreNotEqual(entity.PUBLIC_NUMBER, publicNumber);
				Assert.AreNotEqual(entity.VEHICLE_TYPE, vehicleType);
				Assert.AreNotEqual(vehicle.fuelTankVolume, fuelTankVolume);
				if (entity.Fuel_Type != null)
					Assert.AreNotEqual(entity.Fuel_Type.ID, fuelType);
			}

			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.Image, image), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.IpAddress, ipAddress), SetVehicleFieldResult.IncorrectFieldName);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.VehicleKind, vehicleKind.ToString()), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.MaxAllowedSpeed, maxAllowedSpeed.ToString()), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.Name, garageNumber), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.PublicNumber, publicNumber), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.VehicleType, vehicleType), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelTankVolume, fuelTankVolume.ToString()), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelType, fuelType.ToString()), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerFirstName, ownerFirstName), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerLastName, ownerLastName), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerSecondName, ownerSecondName), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerPhoneNumber1, ownerPhone1), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerPhoneNumber2, ownerPhone2), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.OwnerPhoneNumber3, ownerPhone3), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.Icon, icon), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.Notes, "not setted"), SetVehicleFieldResult.IncorrectFieldName);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelSpentStandartMH, fuelSpentStandartMh.ToString(CultureInfo.InvariantCulture)), SetVehicleFieldResult.Success);
			Assert.AreEqual(server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelSpentStandartGas, fuelSpentStandartGas.ToString(CultureInfo.InvariantCulture)), SetVehicleFieldResult.IncorrectFieldName);

			var attributes = server.GetVehicleAttributes(vehicle.id);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.Image).Value, image);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.VehicleKind).Value, vehicleKind.ToString());
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.MaxAllowedSpeed).Value, maxAllowedSpeed.ToString());
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.Name).Value, garageNumber);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.PublicNumber).Value, publicNumber);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.VehicleType).Value, vehicleType);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.FuelTankVolume).Value, fuelTankVolume.ToString());
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.FuelType).Value, fuelType.ToString());
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerFirstName).Value, ownerFirstName);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerLastName).Value, ownerLastName);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerSecondName).Value, ownerSecondName);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerPhoneNumber1).Value, ownerPhone1);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerPhoneNumber2).Value, ownerPhone2);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.OwnerPhoneNumber3).Value, ownerPhone3);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.Icon).Value, string.Empty);
			Assert.AreEqual(attributes.First(a => a.Name == VehicleAttributeName.FuelSpentStandartMH).Value, fuelSpentStandartMh.ToString("##0.####", CultureInfo.InvariantCulture));
			Assert.IsNull(attributes.FirstOrDefault(a => a.Name == VehicleAttributeName.FuelSpentStandartGas));

			using (var entities = new Entities())
			{
				var entity = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicle.id);
				entity.DEPARTMENT.Country = entities.Country.FirstOrDefault(c => c.Name == "Russia");
				entities.SaveChanges();

				Assert.AreEqual(
					server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelSpentStandart,
						fuelSpentStandart.ToString(CultureInfo.InvariantCulture)), SetVehicleFieldResult.Success);
				Assert.AreEqual(
					server.GetVehicleAttributes(vehicle.id)
						.First(a => a.Name == VehicleAttributeName.FuelSpentStandart).Value, fuelSpentStandart.ToString("##0.####", CultureInfo.InvariantCulture));
			}

			using (var entities = new Entities())
			{
				var entity = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicle.id);
				entity.DEPARTMENT.Country = entities.Country.First(c => c.Name == Country.India);
				entities.SaveChanges();

				Assert.AreEqual(
					server.SetVehicleAttribute(vehicle.id, VehicleAttributeName.FuelSpentStandart,
						fuelSpentStandartIndia.ToString(CultureInfo.InvariantCulture)), SetVehicleFieldResult.Success);
				Assert.AreEqual(
					server.GetVehicleAttributes(vehicle.id)
						.First(a => a.Name == VehicleAttributeName.FuelSpentStandart).Value, fuelSpentStandartIndia.ToString("##0.####", CultureInfo.InvariantCulture));
			}

			const string notes = "test notes";
			var adminServer = Helper.GetSuperAdminServer();
			using (var entities = new Entities())
			{
				var entity = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicle.id);
				entity.DEPARTMENT = entities.DEPARTMENT.First(c => c.DEPARTMENT_ID == adminServer.Department.id);
				entities.OPERATOR_VEHICLE.AddObject(new OPERATOR_VEHICLE
				{
					OPERATOR_ID = adminServer.OperatorId,
					RIGHT_ID = (int)SystemRight.VehicleAccess,
					VEHICLE_ID = vehicle.id,
					ALLOWED = true
				});
				entities.SaveUnsavedChanges();
			}
			Assert.AreEqual(adminServer.SetVehicleAttribute(vehicle.id, VehicleAttributeName.Notes, notes), SetVehicleFieldResult.Success);
			Assert.AreEqual(adminServer.GetVehicleAttributes(vehicle.id, VehicleAttributeName.Notes)[0].Value, notes);
		}
		[TestMethod]
		public void GetVehiclesOnlyLastData()
		{
			var server = Helper.CreateNewPhysicalUser();
			int operatorId = server.OperatorId;

			var arguments = new GetVehiclesArgs
			{
				IncludeLastData = true,
				IncludeAttributes = false
			};

			var vehicles = Server.Instance().GetVehiclesWithPositions(operatorId, null, arguments);

			Assert.AreEqual(1, vehicles.Count);
		}
		[TestMethod]
		public void GetVehiclesNullArguments()
		{
			var server = Helper.CreateNewPhysicalUser();
			int operatorId = server.OperatorId;
			var vehicles = Server.Instance().GetVehiclesWithPositions(operatorId, null, null);
			Assert.AreEqual(1, vehicles.Count);
		}
		[TestMethod]
		public void AddFriendIgnoreAndRestore()
		{
			var server = Helper.CreateNewPhysicalUser();
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);
			var friendServer = Helper.CreateNewPhysicalUser();
			Assert.AreEqual(
				SetAccessToVehicleResult.AccessGranted,
				friendServer.SetReplacedAccessToVehicle(server.GetMsisdn(), friendServer.OwnVehicleId.Value, SystemRight.VehicleAccess, true));
			Assert.AreEqual(2, server.GetVehicles().Count);
			var friendVehicle = server.GetVehicles().First(v => v.Owner.id != server.OperatorId);
			server.DeleteVehicle(friendVehicle.id);
			Assert.IsFalse(server.GetVehicles().Any(v => v.id == friendVehicle.id));
			Assert.IsTrue(server.GetVehicles(new GetVehiclesArgs { ObjectsFilter = VehicleObjectsFilter.Ignored }).Any(v => v.id == friendVehicle.id));
			server.IgnoreVehicle(friendVehicle.id, false);
			Assert.IsTrue(server.GetVehicles().Any(v => v.id == friendVehicle.id));
		}
		[TestMethod]
		public void SaveGeoZone()
		{
			var server = Helper.CreateNewPhysicalUser();

			const string circleColor = "#cccccc";
			const string circleDescription = "circle description";
			const string circleName = "circle";
			const float circleLat = 35.55f;
			const float circleLng = 55.37f;
			const float circleRadius = 100;
			var circle = new GeoZone
			{
				Color = circleColor,
				Description = circleDescription,
				Name = circleName,
				Points = new[]
				{
					new GeoZonePoint
					{
						Lat = circleLat,
						Lng = circleLng,
						Radius = circleRadius
					},
				},
				Type = ZoneTypes.Circle
			};

			const string polygonColor = "#cccccc";
			const string polygonDescription = "polygon description";
			const string polygonName = "polygon";
			const float polygonLat = 35.55f;
			const float polygonLng = 55.37f;
			var polygon = new GeoZone
			{
				Color = polygonColor,
				Description = polygonDescription,
				Name = polygonName,
				Points = new[]
				{
					new GeoZonePoint
					{
						Lat = polygonLat,
						Lng = polygonLng,
					},
					new GeoZonePoint
					{
						Lat = polygonLat + 1,
						Lng = polygonLng,
					},
					new GeoZonePoint
					{
						Lat = polygonLat + 1,
						Lng = polygonLng + 1,
					},
				},
				Type = ZoneTypes.Polygon
			};

			int zoneId;
			Assert.AreEqual(server.AddZoneWebForOperator(circle, out zoneId), SaveZoneResult.Success);
			var result = server.GetGeoZones(true, zoneId, true, true).Single();
			var point = result.Points.FirstOrDefault();
			Assert.IsNotNull(result);
			Assert.AreEqual(string.Compare(result.Color, circleColor, StringComparison.InvariantCultureIgnoreCase), 0);
			Assert.AreEqual(result.Description, circleDescription);
			Assert.AreEqual(result.Name, circleName);
			Assert.IsNotNull(point);
			Assert.AreEqual(point.Radius, circleRadius);
			Assert.AreEqual(point.Lat, circleLat);
			Assert.AreEqual(point.Lng, circleLng);
			Assert.AreEqual(result.Type, ZoneTypes.Circle);

			Assert.AreEqual(server.EdtZoneWebForOperator(polygon), SaveZoneResult.Success);
			result = server.GetGeoZones(true, zoneId, true, true).Single();
			point = result.Points.FirstOrDefault();
			Assert.IsNotNull(result);
			Assert.AreEqual(string.Compare(result.Color, polygonColor, StringComparison.InvariantCultureIgnoreCase), 0);
			Assert.AreEqual(result.Description, polygonDescription);
			Assert.AreEqual(result.Name, polygonName);
			Assert.IsNotNull(point);
			Assert.AreEqual(point.Lat, polygonLat);
			Assert.AreEqual(point.Lng, polygonLng);
			Assert.AreEqual(result.Type, ZoneTypes.Polygon);

			Assert.AreEqual(server.EdtZoneWebForOperator(circle), SaveZoneResult.Success);
			result = server.GetGeoZones(true, zoneId, true, true).Single();
			point = result.Points.FirstOrDefault();
			Assert.IsNotNull(result);
			Assert.AreEqual(string.Compare(result.Color, circleColor, StringComparison.InvariantCultureIgnoreCase), 0);
			Assert.AreEqual(result.Description, circleDescription);
			Assert.AreEqual(result.Name, circleName);
			Assert.IsNotNull(point);
			Assert.AreEqual(point.Radius, circleRadius);
			Assert.AreEqual(point.Lat, circleLat);
			Assert.AreEqual(point.Lng, circleLng);
			Assert.AreEqual(result.Type, ZoneTypes.Circle);

			server.DelZoneWebForOperator(zoneId);
		}
		[TestMethod]
		public void SaveRule()
		{
			var server = Helper.CreateNewPhysicalUser();

			var vehicle = server.GetVehicles().FirstOrDefault();
			Assert.IsNotNull(vehicle);

			var rule2 = server.SaveRule(new Rule
			{
				ObjectId = vehicle.id,
				ObjectIdType = IdType.Vehicle,
				Enabled = true,
				MotionStart = true,
				Subscriptions = new[]
				{
					new RuleSubscription
					{
						OperatorId = server.OperatorId
					}
				}
			});
			Assert.IsNotNull(rule2);

			var msisdn = Helper.FindFreePhone();
			var email = IdGenerator.GenerateUniqueLongNumber() + "@asid.ru";
			var rule3 = server.SaveRule(new Rule
			{
				ObjectId = vehicle.id,
				ObjectIdType = IdType.Vehicle,
				Enabled = true,
				MotionStart = true,
				Subscriptions = new[]
				{
					new RuleSubscription
					{
						OperatorId = server.OperatorId,
						Phones = new [] { msisdn },
						Emails = new [] { email }
					}
				}
			});
			Assert.IsNotNull(rule3);

			Assert.IsTrue(rule3.Subscriptions.Any(s => s.Phones.Contains(msisdn)));
			Assert.IsTrue(rule3.Subscriptions.Any(s => s.Emails.Contains(email)));
		}
		[TestMethod]
		public void TestPushNotificationForAccess()
		{
			var server = Helper.CreateNewPhysicalUser();

			var friendServer = Helper.CreateNewPhysicalUser();
			var friendVehicle = friendServer.GetVehicles().First();

			var lastMessageId = Helper.GetLastMessageId();

			friendServer.SetReplacedAccessToVehicle(
				server.OperatorId, friendVehicle.id, SystemRight.VehicleAccess, true);
			var messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.AreEqual(messages.Count, 1);
			var notificationMessage = messages.First();
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Source));
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Destination));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.PushMessage));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.VehicleID));
			Assert.IsNotNull(notificationMessage.Owner_Operator_ID);

			// Добавляем мобильное приложение на сервер
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);
			friendServer.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);

			// Убираем доступ
			lastMessageId = Helper.GetLastMessageId();
			friendServer.SetReplacedAccessToVehicle(server.OperatorId, friendVehicle.id, SystemRight.VehicleAccess,
				false);
			messages = Helper.GetMessagesAfter(lastMessageId);
			Assert.AreEqual(messages.Count, 1);
			notificationMessage = messages.First();
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Source));
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Destination));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.PushMessage));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.VehicleID));
			Assert.IsNotNull(notificationMessage.Owner_Operator_ID);


			//Снова разрешаем доступ
			friendServer.SetReplacedAccessToVehicle(server.OperatorId, friendVehicle.id, SystemRight.VehicleAccess,
				true);
			messages = Helper.GetMessagesAfter(notificationMessage.MESSAGE_ID);
			Assert.AreEqual(messages.Count, 1);
			notificationMessage = messages.First();
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Source));
			Assert.IsTrue(notificationMessage.Contacts.Any(c => c.Type == (int)MessageContactType.Destination));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.PushMessage));
			Assert.IsNotNull(notificationMessage.GetFieldValue(MESSAGE_TEMPLATE_FIELD.VehicleID));
			Assert.IsNotNull(notificationMessage.Owner_Operator_ID);
		}
		[TestMethod]
		public void GetGarageNumber()
		{
			var friendMsisdn = Helper.FindFreePhone(Helper._msisdnPrefixes.Last());
			var friendServer = Helper.RegisterMsisdn(friendMsisdn);
			var friendVehicle = friendServer.GetVehicles().FirstOrDefault();
			Assert.IsNotNull(friendVehicle);

			var msisdn = Helper.FindFreePhone(Helper._msisdnPrefixes.First());
			var server = Helper.RegisterMsisdn(msisdn);
			friendServer.SetReplacedAccessToVehicle(server.OperatorId, friendVehicle.id, SystemRight.VehicleAccess, true);

			var vehicleName = Guid.NewGuid().ToString("N");
			server.SetVehicleAttribute(friendVehicle.id, VehicleAttributeName.Name, vehicleName);

			friendVehicle = server.GetVehicles().FirstOrDefault(v => v.id == friendVehicle.id);
			Assert.IsNotNull(friendVehicle);
			Assert.AreEqual(friendVehicle.phone, friendMsisdn);
			Assert.AreEqual(friendVehicle.garageNum, vehicleName);

			var contactName = IdGenerator.GenerateUniqueLongNumber().ToString();
			var result = server.AddContact(ContactType.Phone, friendMsisdn);
			Assert.IsNotNull(result.Value.Id);
			server.SetContactName(result.Value.Id.Value, contactName);
			friendVehicle = server.GetVehicles().FirstOrDefault(v => v.id == friendVehicle.id);
			Assert.IsNotNull(friendVehicle);
			Assert.AreEqual(friendMsisdn, friendVehicle.phone);
			Assert.AreEqual(contactName, friendVehicle.garageNum);

			var phoneBookName = Guid.NewGuid().ToString("N");
			var phoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact
					{
						Type = ContactType.Phone,
						Value = friendMsisdn
					}
				}
			};
			Assert.AreEqual(server.SavePhoneBook(phoneBook), SavePhoneBookResult.Success);

			friendVehicle = server.GetVehicles().FirstOrDefault(v => v.id == friendVehicle.id);
			Assert.IsNotNull(friendVehicle);
			Assert.AreEqual(friendVehicle.garageNum, contactName);

			var vehicle = server.GetVehicles().FirstOrDefault(v => v.id != friendVehicle.id);
			Assert.IsNotNull(vehicle);
			server.SetReplacedAccessToVehicle(friendServer.OperatorId, vehicle.id, SystemRight.VehicleAccess, true);
			var friendOperator = server.GetAccessesToVehicle(vehicle.id).FirstOrDefault(o => o.id == friendServer.OperatorId);
			Assert.IsNotNull(friendOperator);
			Assert.AreEqual(friendOperator.name, contactName);

			server.AddFriend(friendServer.OperatorId);
			friendOperator = server.GetFriends().FirstOrDefault(o => o.id == friendServer.OperatorId);
			Assert.IsNotNull(friendOperator);
			Assert.AreEqual(friendOperator.name, contactName);

			phoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact
					{
						Type = ContactType.Phone,
						Value = friendMsisdn,
						Name = phoneBookName
					}
				}
			};
			Assert.AreEqual(server.SavePhoneBook(phoneBook), SavePhoneBookResult.Success);

			friendVehicle = server.GetVehicles().FirstOrDefault(v => v.id == friendVehicle.id);
			Assert.IsNotNull(friendVehicle);
			Assert.AreEqual(friendVehicle.garageNum, phoneBookName);

			vehicle = server.GetVehicles().FirstOrDefault(v => v.id != friendVehicle.id);
			Assert.IsNotNull(vehicle);
			server.SetReplacedAccessToVehicle(friendServer.OperatorId, vehicle.id, SystemRight.VehicleAccess, true);
			friendOperator = server.GetAccessesToVehicle(vehicle.id).FirstOrDefault(o => o.id == friendServer.OperatorId);
			Assert.IsNotNull(friendOperator);
			Assert.AreEqual(friendOperator.name, phoneBookName);

			server.AddFriend(friendServer.OperatorId);
			friendOperator = server.GetFriends().FirstOrDefault(o => o.id == friendServer.OperatorId);
			Assert.IsNotNull(friendOperator);
			Assert.AreEqual(friendOperator.name, phoneBookName);
		}
		/// <summary>
		/// Пользователь добавляет себе трекер по номеру телефона,
		/// а затем этот номер телефона используется для регистрации пользователя -
		/// истинного владельца этого номера телефона
		/// </summary>
		[TestMethod]
		public void AddTrackerThenRegisterUser()
		{
			var firstUser = Helper.RegisterMsisdn(Helper.FindFreeMsisdn());

			var trackerMsisdn = Helper.FindFreeMsisdn();
			var addTrackerResult = firstUser.AddTrackerByPhoneNumber(trackerMsisdn);
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var vehicle = firstUser.GetVehicles().First(v => v.id == addTrackerResult.VehicleId.Value);
			Assert.IsTrue(vehicle.rights.Contains(SystemRight.SecurityAdministration));

			Assert.AreEqual(ProposeFriendshipResult.InvitationSent, firstUser.ProposeFriendship(trackerMsisdn));

			var secondUser = Helper.RegisterMsisdn(trackerMsisdn);
			var secondUserVehicle = secondUser.GetVehicles().Single().id;
			secondUser.SetReplacedAccessToVehicle(firstUser.OperatorId, secondUserVehicle, SystemRight.VehicleAccess, true);

			Assert.IsFalse(
				firstUser.GetVehicles()
					.Single(v => v.phone == trackerMsisdn)
					.rights.Contains(SystemRight.SecurityAdministration));

			Assert.IsTrue(firstUser.GetVehicles().Any(v => v.phone == trackerMsisdn));
		}
		[TestMethod]
		public void SearchOperators()
		{
			var adminServer = Helper.GetSuperAdminServer();
			var msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());
			var server = Helper.RegisterMsisdn(msisdn);

			var deviceId = new Random().Next().ToString();
			int vehicleId;
			Assert.AreEqual(server.AddTrackerByType(ControllerType.Names.Unspecified, deviceId, out vehicleId), AddTrackerResult.Success);

			var operators = adminServer.SearchOperators(msisdn);
			Assert.IsTrue(operators.Any(o => o.id == server.OperatorId));

			operators = adminServer.SearchOperators(deviceId);
			Assert.IsTrue(operators.Any(o => o.id == server.OperatorId));

			using (var entites = new Entities())
			{
				var @operator = entites.OPERATOR.FirstOrDefault(o => o.OPERATOR_ID == server.OperatorId);
				Assert.IsNotNull(@operator);

				var login = IdGenerator.GenerateUniqueLongNumber().ToString();
				var name = IdGenerator.GenerateUniqueLongNumber().ToString();
				@operator.LOGIN = login;
				@operator.NAME = name;

				entites.SaveUnsavedChanges();

				operators = adminServer.SearchOperators(login);
				Assert.IsTrue(operators.Any(o => o.id == server.OperatorId));

				operators = adminServer.SearchOperators(name);
				Assert.IsTrue(operators.Any(o => o.id == server.OperatorId));
			}
		}
		[TestMethod]
		public void SendConfirmation()
		{
			var server = Helper.CreateNewCorporateUser();
			var msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes.First());

			Assert.AreEqual(server.AddContact(ContactType.Phone, msisdn).Result, SetNewContactResult.Success);
			var contactId = server.GetContactId(ContactType.Phone, msisdn);
			var messageLifeTimeMinutes = 15;
			var limitName = BanLimit.NoLimit.ToString();
			using (var entities = new Entities())
			{
				var constantName = Constant.ConfirmationLifetimeMinutes.ToString();
				var constant = entities.CONSTANTS.FirstOrDefault(c => c.NAME == constantName);
				if (constant == null)
				{
					constant = new CONSTANTS { NAME = Constant.ConfirmationLifetimeMinutes.ToString() };
					entities.CONSTANTS.AddObject(constant);
				}

				constant.VALUE = messageLifeTimeMinutes.ToString();
				entities.RequestFreqLimit.AddObject(new RequestFreqLimit
				{
					Name = limitName,
					MaxCount = 3,
					Seconds = 3600
				});

				entities.SaveUnsavedChanges();
			}

			for (var i = 0; i < 4; i++)
			{
				Assert.AreEqual(server.SendConfirmationCode(contactId, "", "").Code, SetNewContactResult.Success);
				try
				{
					Server.Instance().AcceptRequestWithLimit(BanLimit.NoLimit, "192.168.1.2");
				}
				catch (OperationBanException)
				{
					Assert.IsTrue(i == 3);
				}
				using (var entities = new Entities())
				{
					var lastConfirmationCodeMessage = entities.MESSAGE
						.Where(m =>
								m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.GenericConfirmation &&
								m.Owner_Operator_ID == server.OperatorId &&
								m.Contacts.Any(c =>
									c.Type == (int)MessageContactType.Destination &&
									c.Contact_ID == contactId))
						.OrderByDescending(m => m.MESSAGE_ID)
						.FirstOrDefault();
					Assert.IsNotNull(lastConfirmationCodeMessage);

					var operatorContact = entities.Operator_Contact
						.FirstOrDefault(c =>
							c.Contact_ID == contactId &&
							c.Operator_ID == server.OperatorId);
					Assert.IsNotNull(operatorContact);
					Assert.IsNotNull(operatorContact.ConfirmationSentDate);
					operatorContact.ConfirmationSentDate = operatorContact.ConfirmationSentDate.Value.AddMinutes(-messageLifeTimeMinutes);
					lastConfirmationCodeMessage.STATUS = (int)MessageState.Done;

					entities.SaveUnsavedChanges();
				}
			}
		}
		/// <summary>
		/// Проверяет, как работает наследование прав через группу объектов и группу геозон
		/// </summary>
		[TestMethod]
		public void TestAccessPropagation()
		{
			var superAdmin = Helper.GetSuperAdminServer();
			int departmentId;

			var userLogin = Guid.NewGuid().ToString("N");
			Assert.AreEqual(
				CreateDepartmentResult.Success,
				superAdmin.CreateDepartment(userLogin, userLogin, null, out departmentId));

			var user = Helper.GetServerByLogin(userLogin);
			var addTrackerResult = user.AddTrackerByPhoneNumber(Helper.FindFreeMsisdn());
			Assert.IsNotNull(addTrackerResult.VehicleId);

			var vehicleGroupId = user.AddVehicleGroup(userLogin);
			user.AddVehicleToGroup(addTrackerResult.VehicleId.Value, vehicleGroupId);

			Operator subUser;
			Assert.AreEqual(
				CreateOperatorResult.Success,
				user.CreateDepartmentOperator(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), out subUser));

			var subUserServer = Helper.GetServerByLogin(subUser.login);
			Assert.AreEqual(0, subUserServer.GetVehicles().Count);

			user.SetReplacedAccessToVehicleGroup(subUserServer.OperatorId, vehicleGroupId, SystemRight.VehicleAccess, true);

			Assert.AreEqual(1, subUserServer.GetVehicleGroups().Count);
			Assert.AreEqual(0, subUserServer.GetVehicles().Count);

			user.SetReplacedAccessToVehicle(subUserServer.OperatorId, addTrackerResult.VehicleId.Value, SystemRight.VehicleAccess, true);

			Assert.AreEqual(1, subUserServer.GetVehicleGroups().Count);
			Assert.AreEqual(1, subUserServer.GetVehicles().Count);


			var circle = new GeoZone
			{
				Points = new[]
				{
					new GeoZonePoint
					{
						Lat = 0,
						Lng = 0,
						Radius = 100
					},
				},
				Type = ZoneTypes.Circle
			};

			int zoneId;
			Assert.AreEqual(SaveZoneResult.Success, user.AddZoneWebForOperator(circle, out zoneId));
			Assert.AreEqual(0, subUserServer.GetGeoZones().Count);

			var zoneGroup = user.ZoneGroupCreate(Guid.NewGuid().ToString("N"));
			user.ZoneGroupIncludeZone(zoneGroup.Id, zoneId);
			Assert.AreEqual(0, subUserServer.GetGeoZoneGroups().Length);
			user.SetAccessToZoneGroup(subUser.id, zoneGroup.Id, SystemRight.ZoneAccess, true);
			Assert.AreEqual(1, subUserServer.GetGeoZoneGroups().Length);
			Assert.AreEqual(0, subUserServer.GetGeoZones().Count);

			user.SetAccessToZone(subUser.id, zoneId, SystemRight.ZoneAccess, true);
			Assert.AreEqual(1, subUserServer.GetGeoZoneGroups().Length);
			Assert.AreEqual(1, subUserServer.GetGeoZones().Count);

			//Теперь проверяем, есть ли доступ через группу, если включить флаг AccessPropagation

			using (var entities = new Entities())
			{
				var vehicleGroup = entities.VEHICLEGROUP.First(vg => vg.VEHICLEGROUP_ID == vehicleGroupId);
				Assert.IsFalse(vehicleGroup.PropagateAccess);
				vehicleGroup.PropagateAccess = true;

				var zoneGroupEntity = entities.ZONEGROUP.First(zg => zg.ZONEGROUP_ID == zoneGroup.Id);
				Assert.IsFalse(zoneGroupEntity.PropagateAccess);
				zoneGroupEntity.PropagateAccess = true;

				entities.SaveChangesBySession(superAdmin.SessionInfo.SessionId);
			}

			Assert.IsFalse(subUserServer.GetVehicles().Single().rights.Contains(SystemRight.EditVehicles));
			user.SetReplacedAccessToVehicleGroup(subUserServer.OperatorId, vehicleGroupId, SystemRight.EditVehicles, true);
			Assert.IsTrue(subUserServer.GetVehicles().Single().rights.Contains(SystemRight.EditVehicles));

			Assert.IsFalse(subUserServer.GetGeoZones().Single().Editable);
			user.SetAccessToZoneGroup(subUser.id, zoneGroup.Id, SystemRight.EditZone, true);
			Assert.IsTrue(subUserServer.GetGeoZones().Single().Editable);
		}
		[TestMethod]
		public void TestPhoneBookMerge()
		{
			var server = Helper.CreateNewPhysicalUser();
			server.SavePhoneBook(new PhoneBook
			{
				Contacts = new[] { new PhoneBookContact() { Name = "A", Type = ContactType.Phone, Value = "79160000000" } }
			});
			server.SavePhoneBook(new PhoneBook
			{
				Contacts = new[] { new PhoneBookContact() { Name = "B", Type = ContactType.Phone, Value = "79160000001" } }
			});
			server.SavePhoneBook(new PhoneBook
			{
				Contacts = new[] { new PhoneBookContact() { Name = "AChanged", Type = ContactType.Phone, Value = "79160000000" } }
			});

			using (var entities = new Entities())
			{
				var phoneBook =
					SerializationHelper.DeserializeXml<PhoneBook>(
						entities.Operator_PhoneBook.First(pb => pb.OperatorId == server.OperatorId).PhoneBook);
				Assert.AreEqual(2, phoneBook.Contacts.Length);
				Assert.AreEqual("AChanged", phoneBook.Contacts.Where(x => x.Value == "79160000000").Select(x => x.Name).First());
				Assert.AreEqual("B", phoneBook.Contacts.Where(x => x.Value == "79160000001").Select(x => x.Name).First());
			}
		}
		[TestMethod]
		public void TestAddressBookNameOverride()
		{
			var superAdmin = Helper.GetSuperAdminServer();
			int departmentId;

			var userLogin = Guid.NewGuid().ToString("N");
			Assert.AreEqual(
				CreateDepartmentResult.Success,
				superAdmin.CreateDepartment(userLogin, userLogin, null, out departmentId));

			var user = Helper.GetServerByLogin(userLogin);
			int vehicleId;
			var trackerNumber = IdGenerator.GenerateUniqueLongNumber().ToString();
			var addTrackerResult = user.AddTrackerByType(ControllerType.Names.Unspecified, trackerNumber, out vehicleId);
			Assert.AreEqual(AddTrackerResult.Success, addTrackerResult);

			Assert.AreEqual(trackerNumber, user.GetVehicles().Single().Name);

			var phoneBook = new PhoneBook
			{
				Contacts = new[]
				{
					new PhoneBookContact
					{
						Type = ContactType.Phone,
						Name = "Wrong name"
					}
				}
			};

			Assert.AreEqual(user.SavePhoneBook(phoneBook), SavePhoneBookResult.Success);

			using (var entities = new Entities())
			{
				var controller = entities.CONTROLLER.First(c => c.VEHICLE.VEHICLE_ID == vehicleId);
				controller.PHONE = string.Empty;
				entities.SaveChangesBySession(user.SessionInfo.SessionId);
			}

			Assert.AreEqual(trackerNumber, user.GetVehicles().Single().Name);
		}
		[TestMethod]
		public void CreateWebNotification()
		{
			using (var entities = new Entities())
			{
				var message = entities.CreateWebNotification(187, "test", "test2");
				message.MESSAGE_TEMPLATE = entities.MESSAGE_TEMPLATE.FirstOrDefault(t => t.NAME == Msg::MessageTemplate.GenericNotification);
				entities.AddMessageField(message, "test-field", "test value");
				entities.SaveChanges();
			}
		}
		[TestMethod]
		public void SetupTracker()
		{
			var server = Helper.GetSuperAdminServer();
			int departmentId;

			var userLogin = Guid.NewGuid().ToString("N");
			Assert.AreEqual(
				CreateDepartmentResult.Success,
				server.CreateDepartment(userLogin, userLogin, null, out departmentId));

			server.SelectDepartment(departmentId);

			var addTrackerResult = server.AddTrackerAsEmpty();
			Assert.IsNotNull(addTrackerResult.VehicleId);
			var vehicleId = addTrackerResult.VehicleId.Value;
			Assert.AreEqual(SetVehicleControllerFieldResult.Success,
				server.SetVehicleDeviceType(vehicleId, ControllerType.Names.AvtoGraf));

			//Пытаемся установить уже зарегистрированный номер телефона
			var usedPhoneNumber = Helper.FindUsedMsisdn();
			Assert.AreEqual(
				SetVehicleDevicePhoneResult.AlreadyExists,
				server.SetVehicleAttribute(vehicleId, VehicleAttributeName.Phone, usedPhoneNumber));

			var freeMsisdn = Helper.FindFreeMsisdn();
			Assert.AreEqual(
				SetVehicleDevicePhoneResult.Success,
				server.SetVehicleAttribute(vehicleId, VehicleAttributeName.Phone, freeMsisdn));

			var apnAttributes = server.GetVehicleAttributes(
				vehicleId,
				VehicleAttributeName.ApnName, VehicleAttributeName.ApnUser, VehicleAttributeName.ApnPassword);
			Assert.AreEqual(3, apnAttributes.Count);
			Assert.IsTrue(apnAttributes.All(a => a.Editable && string.IsNullOrWhiteSpace(a.Value)));

			server.SetVehicleAttribute(vehicleId, VehicleAttributeName.ApnName, "attributeApnName");
			server.SetVehicleAttribute(vehicleId, VehicleAttributeName.ApnUser, "attributeApnUser");
			server.SetVehicleAttribute(vehicleId, VehicleAttributeName.ApnPassword, "attributeApnPass");

			apnAttributes = server.GetVehicleAttributes(
				vehicleId,
				VehicleAttributeName.ApnName, VehicleAttributeName.ApnUser, VehicleAttributeName.ApnPassword);

			Assert.AreEqual("attributeApnName", apnAttributes.Single(a => a.Name == VehicleAttributeName.ApnName).Value);
			Assert.AreEqual("attributeApnUser", apnAttributes.Single(a => a.Name == VehicleAttributeName.ApnUser).Value);
			Assert.AreEqual("attributeApnPass", apnAttributes.Single(a => a.Name == VehicleAttributeName.ApnPassword).Value);

			var command =
				server.RegisterCommand(new Command
				{
					TargetID = vehicleId,
					Type = CmdType.Setup
				});

			Assert.AreEqual("attributeApnName", command.Parameters[PARAMS.Keys.InternetApn.Name]);
			Assert.AreEqual("attributeApnUser", command.Parameters[PARAMS.Keys.InternetApn.User]);
			Assert.AreEqual("attributeApnPass", command.Parameters[PARAMS.Keys.InternetApn.Password]);

			using (var entities = new Entities())
			{
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Name));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.User));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Password));
			}

			server.CancelCommand(command.Id);

			command =
				server.RegisterCommand(new Command
				{
					TargetID = vehicleId,
					Type = CmdType.Setup,
					Parameters = new Dictionary<string, string>
					{
						{"apnName", "customAPNName"},
						{"apnUser", "customAPNUser"},
						{"apnPassword", "customAPNPass"}
					}
				});

			using (var entities = new Entities())
			{
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Name));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.User));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Password));
			}

			var vehicle = server.GetVehicles(new GetVehiclesArgs { IncludeLastData = true }).Single();
			Assert.AreEqual(addTrackerResult.VehicleId, vehicle.id);
			command = vehicle.commands.Single();

			using (var entities = new Entities())
			{
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Name));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.User));
				Assert.IsTrue(entities.Command_Parameter.Any(cp => cp.Command.ID == command.Id && cp.Key == PARAMS.Keys.InternetApn.Password));
			}
		}
		[TestMethod]
		public void QuestionToSupport()
		{
			var server = Helper.CreateNewCorporateUser();
			var email = VerifyEmail(server);

			server.SendQuestionToSupport(email, Guid.NewGuid().ToString("N"), "Test question");
		}
		private static string VerifyEmail(IWebPersonalServer server)
		{
			var email = Guid.NewGuid().ToString("N") + "@" + Guid.NewGuid().ToString("N") + ".ru";
			Assert.IsTrue(ContactHelper.IsValidEmail(email));
			var addContactResult = server.AddContact(ContactType.Email, email);
			Assert.AreEqual(SetNewContactResult.Success, addContactResult.Result);
			Assert.IsNotNull(addContactResult.Value.Id);
			server.SendConfirmationCode(addContactResult.Value.Id.Value, "Confirmation subject", "{0}");
			var lastMessage = Helper.GetLastMessage();
			Assert.AreEqual(
				SetNewContactResult.Success,
				server.VerifyContact(addContactResult.Value.Id.Value, lastMessage.BODY).Code);
			return email;
		}
		[TestMethod]
		public void ForbidReportAccessForSubOperator()
		{
			var server = Helper.CreateNewCorporateUser();

			Operator operInfo;
			server.CreateDepartmentOperator(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), out operInfo);

			operInfo = server.GetOperatorRights(operInfo.id);

			Assert.IsTrue(operInfo.rights.Any(r => r.right == SystemRight.ReportsAccess && (r.allow ?? false)));
			Assert.IsTrue(operInfo.rights.Any(r => r.right == SystemRight.PathAccess && (r.allow ?? false)));

			var subServer = Helper.GetServerByLogin(operInfo.login);
			var rights = subServer.GetOperatorSystemRights(subServer.OperatorId);
			Assert.IsTrue(rights.Contains(SystemRight.ReportsAccess));
			Assert.IsTrue(rights.Contains(SystemRight.PathAccess));

			//Выборочно отключаем доступ к Пути
			operInfo.rights.Single(r => r.right == SystemRight.PathAccess).allow = null;
			server.SaveOperatorRights(operInfo);
			rights = subServer.GetOperatorSystemRights(subServer.OperatorId);
			Assert.IsFalse(rights.Contains(SystemRight.PathAccess));
		}
		[TestMethod]
		public void AllowRemoveConfirmedContact()
		{
			var server = Helper.CreateNewCorporateUser();
			var email = VerifyEmail(server);

			var contact = server.GetContacts(ContactType.Email).Single();
			Assert.AreEqual(email, contact.Value);
			Assert.IsTrue(contact.IsConfirmed ?? false);
			Assert.IsTrue(contact.IsRemovable ?? false);
		}
		[TestMethod]
		public void VehicleTrackingInterval()
		{
			var server = Helper.CreateNewPhysicalUser();
			server.UpdateClientRegistration(Guid.NewGuid().ToString("N"), Guid.NewGuid().ToString("N"), null);

			var vehicleId = server.GetVehicles().Single().id;
			Assert.AreEqual(0, server.GetVehicleSchedules(vehicleId).Count);
			var schedule = server.SetVehicleSchedule(vehicleId, new TrackingSchedule
			{
				Enabled = true,
				Days = new[] { DayOfWeek.Monday, DayOfWeek.Friday },
				From = new TimeOfDay(9, 30),
				To = new TimeOfDay(18, 45)
			});
			Assert.AreEqual(schedule.Owner.OperatorId, server.OperatorId);
			Assert.IsTrue(0 < schedule.Id);
			CheckScheduleEquality(schedule, server, vehicleId);

			schedule.From = new TimeOfDay(21, 0);
			schedule.To = new TimeOfDay(9, 5);
			schedule.Enabled = false;
			schedule.Days = new[] { DayOfWeek.Sunday };
			server.SetVehicleSchedule(vehicleId, schedule);
			CheckScheduleEquality(schedule, server, vehicleId);

			var otherUser = Helper.CreateNewPhysicalUser();
			Assert.AreEqual(
				SetAccessToVehicleResult.AccessGranted,
				server.SetReplacedAccessToVehicle(otherUser.GetMsisdn(), vehicleId, SystemRight.VehicleAccess, true));

			Assert.AreEqual(0, otherUser.GetVehicleSchedules(vehicleId).Count);
			var otherUserSchedule = otherUser.SetVehicleSchedule(
				vehicleId, new TrackingSchedule
				{
					Enabled = true,
					Days = new[] { DayOfWeek.Wednesday, DayOfWeek.Thursday },
					From = new TimeOfDay(0, 0),
					To = new TimeOfDay(0, 0)
				});

			Assert.AreEqual(otherUserSchedule.Owner.OperatorId, otherUser.OperatorId);
			Assert.AreEqual(1, otherUser.GetVehicleSchedules(vehicleId).Count);
			Assert.AreEqual(2, server.GetVehicleSchedules(vehicleId).Count);

			server.DelReplacedAccessToVehicle(otherUser.GetMsisdn(), vehicleId, SystemRight.VehicleAccess);

			Assert.AreEqual(1, server.GetVehicleSchedules(vehicleId).Count);
		}
		private static void CheckScheduleEquality(TrackingSchedule schedule, IWebPersonalServer server, int vehicleId)
		{
			var savedSchedule = server.GetVehicleSchedules(vehicleId).Single();
			Assert.AreEqual(schedule.Id, savedSchedule.Id);
			Assert.AreEqual(schedule.Enabled, savedSchedule.Enabled);
			Assert.IsTrue(savedSchedule.Days.All(x => schedule.Days.Contains(x)) &&
						  schedule.Days.All(x => savedSchedule.Days.Contains(x)));
			Assert.AreEqual(schedule.From, savedSchedule.From);
			Assert.AreEqual(schedule.To, savedSchedule.To);
			Assert.AreEqual(server.OperatorId, savedSchedule.Owner.OperatorId);
			Assert.AreEqual(ContactType.Phone, savedSchedule.Owner.Contact.Type);
			Assert.AreEqual(server.GetMsisdn(), savedSchedule.Owner.Contact.Value);
		}
		/// <summary>
		/// Вначале один пользователь неявно создаёт другого пользователя в системе, предоставляя ему доступ к одному из своих объектов.
		/// Затем этот абонент авторизуется в системе по номеру телефона с мобильного приложения.
		/// Проверяется, корректно ли проставляется номер телефона второго пользователя в его собственном объекте наблюдения.
		/// </summary>
		[TestMethod]
		public void CheckVehicleMsisdnAssigning()
		{
			var user1 = Helper.CreateNewPhysicalUser();

			var user2Msisdn = Helper.FindFreeMsisdn(Helper._msisdnPrefixes);
			user1.SetReplacedAccessToVehicle(user2Msisdn, user1.GetVehicles().Single().id, SystemRight.VehicleAccess, true);

			var user2 = Helper.GetPersonalServerByMsisdn(user2Msisdn);
			Assert.IsNotNull(user2.GetOrCreateSimId());
			var user2Vehicles = user2.GetVehicles();
			Assert.AreEqual(2, user2Vehicles.Count);
			Assert.AreEqual(user2Msisdn, user2Vehicles.OrderBy(v => v.id).Last().phone);
		}
		[TestMethod]
		public void MexicanFriend()
		{
			var user = Helper.CreateNewPhysicalUser();

			var mexicanMsisdn = "+" + Helper.FindFreeMsisdn(13, "52");
			Assert.AreEqual(1 + 13, mexicanMsisdn.Length);
			var result = user.ProposeFriendship(mexicanMsisdn);

			Assert.AreEqual(ProposeFriendshipResult.UnsupportedNetworkProvider, result);

			var mexicanUser = Helper.GetOrCreateUserByMsisdn(mexicanMsisdn);
			Assert.IsNotNull(mexicanUser.OwnVehicleId);

			var mexicanVehicleId = mexicanUser.OwnVehicleId.Value;
			Assert.AreEqual(
				SetAccessToVehicleResult.AccessGranted,
				mexicanUser.SetReplacedAccessToVehicle(user.GetMsisdn(), mexicanVehicleId, SystemRight.VehicleAccess, true));

			Assert.IsTrue(user.GetVehicles().Any(v => v.id == mexicanVehicleId));

			Assert.IsNotNull(user.OwnVehicleId);
			var userVehicleId = user.OwnVehicleId.Value;
			Assert.AreEqual(
				SetAccessToVehicleResult.AccessGranted,
				user.SetReplacedAccessToVehicle(mexicanMsisdn, userVehicleId, SystemRight.VehicleAccess, true));

			Assert.IsTrue(mexicanUser.GetVehicles().Any(v => v.id == userVehicleId));
		}
		[TestMethod]
		public void IndianConfirmation()
		{
			var indianMsisdn = "+" + Helper.FindFreeMsisdn(12, "91");
			Assert.AreEqual(1 + 12, indianMsisdn.Length);

			Assert.AreEqual(
				SetNewContactResult.Success,
				Server.Instance()
					.SendConfirmationCode(ContactType.Phone, indianMsisdn, CultureInfo.InvariantCulture,
					IPAddress.Loopback.ToString())
				.Code);
		}
	}
}