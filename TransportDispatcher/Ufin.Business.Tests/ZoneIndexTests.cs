﻿using Compass.Ufin.Gis;
using FORIS.TSS.BusinessLogic.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class ZoneIndexTests
	{
		[TestMethod]
		public void TestZoneIndex1()
		{
			var zonePoints = new[]
			{
				new GeoZonePoint {Lng = (float) 37.2, Lat = (float) 54.41},
				new GeoZonePoint {Lng = (float) 37.2, Lat = (float) 55.62},
				new GeoZonePoint {Lng = (float) 38.2, Lat = (float) 55.62},
				new GeoZonePoint {Lng = (float) 38.2, Lat = (float) 54.41},
				new GeoZonePoint {Lng = (float) 37.2, Lat = (float) 54.41},
			};
			var nodes = zonePoints.GetMatrixForPolygon();
			Assert.IsNotNull(nodes);
			Assert.IsTrue(nodes.Count > 0);
		}
	}
}