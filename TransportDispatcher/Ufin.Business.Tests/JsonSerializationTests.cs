﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Business.Tests
{
	[TestClass]
	public class JsonSerializationTests
	{
		[TestMethod]
		public void CommandParameters()
		{
			var command = JsonHelper.DeserializeObjectFromJson<Command>(
				@"{ 'targetID' : 77271, 'type' : 'Setup', parameters: { apnName: 'internet.mts.ru', apnUser: 'mts', apnPassword: 'mts' } }");

			Assert.AreEqual(77271, command.TargetID);
			Assert.AreEqual(CmdType.Setup, command.Type);
			Assert.AreEqual(3, command.Parameters.Count);
			Assert.AreEqual("internet.mts.ru", command.Parameters["apnName"]);
		}

		[TestMethod]
		public void DictionaryStringToObject()
		{
			var dictionary = JsonHelper.DeserializeObjectFromJson<Dictionary<string, string>>(
				@"{ apnName: 'internet.mts.ru', apnUser: 'mts', apnPassword: 'mts' }");

			Assert.AreEqual(3, dictionary.Count);
		}

		[TestMethod]
		public void EnumKeyDictionaryToObject()
		{
			var dictionary = JsonHelper.DeserializeObjectFromJson<Dictionary<VehicleAttributeName, string>>(
				@"{'Phone':'89270142565'}");

			Assert.AreEqual(1, dictionary.Count);
		}

		[TestMethod]
		public void TimeOfDay()
		{
			var original = new TimeOfDay(12, 34);
			var json = JsonHelper.SerializeObjectToJson(original);
			Assert.AreEqual("[\"12:34\"]", json);
			var deserialized = JsonHelper.DeserializeObjectFromJson<TimeOfDay[]>(json);
			Assert.AreEqual(original.Hours, deserialized[0].Hours);
			Assert.AreEqual(original.Minutes, deserialized[0].Minutes);
		}
	}
}