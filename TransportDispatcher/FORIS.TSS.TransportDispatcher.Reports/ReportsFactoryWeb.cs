﻿using System;
using System.Globalization;
using System.IO;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Reports;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Используется на стороне сервера для формирования отчетов для WEB </summary>
	public class ReportsFactoryWeb
	{
		/// <summary> Директория для хранения отчётов (результирующих файлов) </summary>
		private readonly DirectoryInfo     _outputDirectoryInfo;
		/// <summary> Ссылка на персональный сервер </summary>
		private readonly IBasicFunctionSet _mclsInstance;
		/// <summary> Идентификатор оператора </summary>
		private readonly int               _operatorId;
		/// <summary> Репозиторий отчетов </summary>
		private readonly IReportRepository _repository;
		public string ReportsPath
		{
			get
			{
				return _outputDirectoryInfo.FullName;
			}
		}
		public ReportsFactoryWeb(IBasicFunctionSet mclsInstance, IReportRepository repository, int operatorId)
		{
			if (mclsInstance == null)
				throw new ArgumentNullException(nameof(mclsInstance));
			if (repository == null)
				throw new ArgumentNullException(nameof(repository));

			_mclsInstance = mclsInstance;
			_operatorId   = operatorId;
			_repository   = repository;

			var reportsPath = GetReportsPath(operatorId);
			_outputDirectoryInfo = new DirectoryInfo(reportsPath);
			if(!_outputDirectoryInfo.Exists)
				_outputDirectoryInfo.Create();
		}
		/// <summary> Формирует, печатает в файл и возвращает сформированный отчет </summary>
		/// <returns></returns>
		public string CreateReportAsFile(Guid reportId, PARAMS reportParams, ReportTypeEnum formatType)
		{
			var fileName = BuildReportFileName(reportId, reportParams, formatType);
			var filePathName = ReportsPath + @"\" + fileName;
			try
			{
				ReportsFactory
					.Instance(_mclsInstance, _repository)
					.CreateReportAsFile(reportId, reportParams, _operatorId, formatType, filePathName);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
				return null;
			}
			return fileName;
		}
		/// <summary> Создает отчёт как структурированный объект </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Параметры отчёта </param>
		public object CreateReportAsDto(Guid reportId, PARAMS reportParams)
		{
			try
			{
				return ReportsFactory
					.Instance(_mclsInstance, _repository)
					.CreateReportAsDto(reportId, reportParams, _operatorId);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
				return null;
			}
		}
		/// <summary> Создает отчет </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Параметры отчёта </param>
		/// <remarks> Возвращает либо текстовую строку отчёта HTML или DataSet с данными отчёта </remarks>
		public object CreateReportAsObj(Guid reportId, PARAMS reportParams)
		{
			try
			{
				return ReportsFactory
					.Instance(_mclsInstance, _repository)
					.CreateReportAsObj(reportId, reportParams, _operatorId);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
				return null;
			}
		}
		/// <summary> Возвращает параметры отчета </summary>
		/// <param name="reportId"></param>
		/// <param name="reportParams"></param>
		public ReportParameters GetReportParameters(Guid reportId, PARAMS reportParams)
		{
			var result = ReportsFactory
				.Instance(_mclsInstance, _repository)
				.GetReportParameters(reportId, reportParams, _operatorId);
			return result;
		}
		private string BuildReportFileName(Guid reportId, PARAMS reportParams, ReportTypeEnum formatType)
		{
			var report = _repository[reportId];
			var reportParameters = GetReportParameters(reportId, reportParams);
			return report.GetReportFileName(reportParameters) + "." + ReportHelper.GetExtension(formatType);
		}
		private string GetReportsPath(int operatorId)
		{
			var reportsPath = GlobalsConfig.AppSettings["PathWebGis"];
			// Если директория указана в конфиге
			if (string.IsNullOrEmpty(reportsPath))
			{
				reportsPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
			}
			else
			{
				reportsPath += @"\Reports\";
			}
			// Сразу создаем на случай, если нет такой директории
			reportsPath = Path.Combine(reportsPath, operatorId.ToString(CultureInfo.InvariantCulture));
			return reportsPath;
		}
	}
}