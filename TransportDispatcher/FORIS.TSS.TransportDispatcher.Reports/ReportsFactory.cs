﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Reports;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Класс "Фабрика отчетов" </summary>
	public class ReportsFactory
	{
		/// <summary> Ссылка на объект, содержащий интерфейсы для клиента или сервера </summary>
		private readonly IBasicFunctionSet _mclsInstance;
		private readonly IReportRepository _repository;
		public TssReportBase[] ReportsList { get; }
		/// <summary> Счетчик полученных отчетов </summary>
		public int Count => ReportsList?.Length ?? 0;

		private static volatile ReportsFactory instance = null;

		//TODO: Бред какой-то, если вызвать с разными параметрами, то создастся только первый. В чем смысл?
		public static ReportsFactory Instance(IBasicFunctionSet mclsInstance, IReportRepository repository)
		{
			if (instance == null)
			{
				lock (typeof(ReportsFactory))
				{
					if (instance == null)
					{
						instance = new ReportsFactory(mclsInstance, repository);
					}
				}
			}
			return instance;
		}
		/// <summary> Приватный конструктор </summary>
		/// <param name="mclsInstance"></param>
		/// <param name="repository"></param>
		private ReportsFactory(IBasicFunctionSet mclsInstance, IReportRepository repository)
		{
			_mclsInstance = mclsInstance;
			_repository   = repository;

			// Предварительный список созданных отчетов для сортировки
			// Загрузка отчетов
			var reports = LoadReports();

			Trace.TraceInformation((reports != null ? reports.Count : 0) + " reports loaded");

			ReportsList = reports?.ToArray() ?? Array.Empty<TssReportBase>();
		}
		/// <summary> Функция для поиска и загрузки отчетов из текущей директории </summary>
		/// <returns></returns>
		private List<TssReportBase> LoadReports()
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Load reports".CallTraceMessage());
			try
			{
				HashSet<Guid> reportGuids;
				using (var entities = new Entities())
				{
					//TODO: извлекать только те отчёты, на которые имеются права у текущего пользователя
					reportGuids = new HashSet<Guid>(entities.REPORT.Select(r => r.REPORT_GUID));
				}

				var result = new List<TssReportBase>(reportGuids.Count);
				foreach (var guid in reportGuids)
				{
					var report = _repository[guid];
					if (report == null)
						continue;
					report.IBasicFunctionSetInstanceSet(_mclsInstance);
					result.Add(report);
				}

				return result;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return null;
		}
		/// <summary> Функция создания отчёта в файл заданного формата (*.pdf, *.xls) конкретного отчета по его GUID и заданным параметрам </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Параметры отчёта </param>
		/// <param name="operatorId"> Идентификатор оператора </param>
		/// <param name="formatType"> Формат отчёта </param>
		/// <param name="fileName"> Имя файла отчёта </param>
		public void CreateReportAsFile(Guid reportId, PARAMS reportParams, int operatorId, ReportTypeEnum formatType, string fileName)
		{
			var tssReport = _repository[reportId];
			if (tssReport == null)
				return;

			using var clsReport = tssReport.Create(GetReportParameters(reportId, reportParams, operatorId));
			// Отчет был подготовлен (экземпляр отчета существует)
			if (!string.IsNullOrEmpty(fileName))
				clsReport?.ExportToDisk(formatType, fileName);

		}
		/// <summary> Функция создает отчёт и возвращает его объект </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Параметры отчёта </param>
		/// <param name="operatorId"> Идентификатор пользователя, от имени которого создаётся отчёт </param>
		public object CreateReportAsObj(Guid reportId, PARAMS reportParams, int operatorId)
		{
			if (reportParams == null)
				throw new ArgumentNullException(nameof(reportParams));

			try
			{
				var report = _repository[reportId];
				var parameters = GetReportParameters(reportId, reportParams, operatorId);

				var result = report.GetReportAsObj(parameters);

				return result;
			}
			catch (Exception ex)
			{
				$"Unable to build report {reportId} with params {reportParams} for operator '{operatorId}'"
					.WithException(ex, true)
					.CallTraceError();
			}

			return null;
		}
		public object CreateReportAsDto(Guid reportId, PARAMS reportParams, int operatorId)
		{
			if (reportParams == null)
				throw new ArgumentNullException(nameof(reportParams));

			try
			{
				var report     = _repository[reportId];
				var parameters = GetReportParameters(reportId, reportParams, operatorId);
				var result     = report.GetReportAsDto(parameters);

				return result;
			}
			catch (Exception ex)
			{
				$"Unable to build report {reportId} with params {reportParams} for operator '{operatorId}'"
					.WithException(ex, true)
					.CallTraceError();
			}

			return null;
		}
		/// <summary> Возвращает параметры отчёта </summary>
		/// <param name="reportId"> Уникальный идентификатор отчёта </param>
		/// <param name="reportParams">Коллекция параметров</param>
		/// <param name="operatorId">Идентификатор пользователя, от имени которого строится отчёт</param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="ArgumentException"></exception>
		public ReportParameters GetReportParameters(Guid reportId, PARAMS reportParams, int operatorId)
		{

			var report = ReportsList.FirstOrDefault(r =>
				{
					var attributeReportGuid = Attribute.GetCustomAttribute(r.GetType(), typeof (GuidAttribute));
					Guid rGuid;
					if (!Guid.TryParse(((GuidAttribute) attributeReportGuid).Value, out rGuid))
						return false;

					return rGuid == reportId;
				});

			if (report == null)
				return null;

			// Получение класса с набором параметров для отчета.
			var reportParametersSettings = GetReportParametersSettings(reportParams, operatorId);
			var clsReportParameters = report.ReportParametersInstanceGet(reportParametersSettings);
			if (clsReportParameters == null)
				throw new ArgumentException("ReportParametersInstanceGet returns null");
			clsReportParameters.OperatorId = reportParametersSettings.OperatorId;

			// Поиск заданных параметров в классе отчета и задание им полученных значений из переданного словаря
			if (reportParams != null)
			{
				IDictionaryEnumerator dictionaryEnumerator = reportParams.GetEnumerator();
				var properties = clsReportParameters.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
				while (dictionaryEnumerator.MoveNext())
				{
					// Поиск заданного параметра
					var parameterName = dictionaryEnumerator.Key.ToString();
					var propertyInfos = properties.Where(p => p.Name == parameterName).ToArray();
					var propertyInfo = propertyInfos.Length > 1 ? propertyInfos.First(p => p.DeclaringType != typeof(ReportParameters)) : propertyInfos.FirstOrDefault();

					// Если данный параметр существует, задаем значение
					if (propertyInfo == null)
					{
						Trace.TraceWarning("Parameter {0} is not supported by report {1}",
										   parameterName, report.GetType());
						continue;
					}

					propertyInfo.SetValue(clsReportParameters, dictionaryEnumerator.Value, null);
				}
			}

			clsReportParameters = report.ReportParametersInstanceRefresh(clsReportParameters);
			var checkResult = report.ReportParametersInstanceCheck(clsReportParameters);
			if (checkResult == null || !checkResult.Ok)
			{
				var errorMessage =
					string.Format("{0}.ReportParametersInstanceCheck did not approve parameters: \n{1},\nreason:{2} ",
						report.GetType(), reportParams, checkResult != null ? JsonHelper.SerializeObjectToJson(checkResult.WrongArguments) : null);
				throw new ArgumentException(errorMessage, "paramsArray");
			}
			return clsReportParameters;
		}
		private TssReportParametersSettings GetReportParametersSettings(PARAMS reportParams, int operatorId)
		{
			var departmentId   = (int?)reportParams["DepartmentId"];
			var departmentType = (DepartmentType?)reportParams["DepartmentType"];
			var reportParametersSettings = new TssReportParametersSettings
			{
				DepartmentId   = departmentId,
				DepartmentType = departmentType,
				OperatorId     = operatorId
			};

			return reportParametersSettings;
		}
	}
}