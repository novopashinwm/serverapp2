﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class ReportParametersInstanceCheckResult
	{
		public class WrongArgument
		{
			public string Key;
			public string Name;
			public string Reason;
		}

		public bool Ok = true;
		public List<WrongArgument> WrongArguments;

		public static implicit operator ReportParametersInstanceCheckResult(bool ok)
		{
			return new ReportParametersInstanceCheckResult { Ok = ok };
		}

		public static implicit operator bool(ReportParametersInstanceCheckResult x)
		{
			return x != null && x.Ok;
		}

		public void AddWrongArgument(string key, string reason)
		{
			if (WrongArguments == null)
				WrongArguments = new List<WrongArgument>();

			WrongArguments.Add(new WrongArgument { Key = key, Reason = reason });

			Ok = false;
		}

		public override string ToString()
		{
			if (Ok)
				return "Ok";

			if (WrongArguments == null || WrongArguments.Count == 0)
				return "Some argument are wrong";

			return string.Join(", ", WrongArguments.Select(wa => wa.Key + ": " + wa.Reason));
		}
	}
}