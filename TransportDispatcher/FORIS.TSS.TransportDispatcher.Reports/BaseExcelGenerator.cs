﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public abstract class BaseExcelGenerator
	{
		/// <summary> Заполняет таблицу с данными отчёта </summary>
		protected abstract void FillTable();
		/// <summary> Возвращает список колонок отчёта </summary>
		protected abstract IEnumerable<CellInfo> Cells { get; }
		protected abstract string WorksheetName { get; }
		protected virtual void FillHeader()
		{
		}
		private   const string StyleDateTimeFormat = "yyyy\\-mm\\-dd HH:MM:ss";
		private   const string StyleTimeSpanFormat = "[h]:mm:ss";

		protected const string DateTimeCellStyle   = "DateTimeStyle";
		protected const string TimeSpanCellStyle   = "TimeSpanStyle";

		/// <summary> Пространство имён spreadsheet </summary>
		protected static readonly XNamespace SpreadsheetNS = "urn:schemas-microsoft-com:office:spreadsheet";
		/// <summary> Пространство имён excel </summary>
		protected static readonly XNamespace ExcelNS       = "urn:schemas-microsoft-com:office:excel";
		/// <summary> Другие необходимые пространства имен </summary>
		private static readonly XNamespace X2              = "http://schemas.microsoft.com/office/excel/2003/xml";
		private static readonly XNamespace C               = "urn:schemas-microsoft-com:office:component:spreadsheet";
		private static readonly XNamespace Xsi             = "http://www.w3.org/2001/XMLSchema-instance";
		private static readonly XNamespace O               = "urn:schemas-microsoft-com:office:office";
		private static readonly XNamespace HtmlNS          = "http://www.w3.org/TR/REC-html40";

		private int      _rowsAdded;
		private XElement _table;
		private XElement _workBook;

		public string Create(bool autoFilter = true)
		{
			_workBook = CreateWorkbook();
			var document = new XDocument(
				new XDeclaration("1.0", "utf-8", "yes"),
				new XProcessingInstruction("mso-application", "progid=\"Excel.Sheet\""),
				_workBook);

			_table = CreateTableInNewWorksheet(Cells, WorksheetName);
			FillHeader();
			CreateTableHeader(autoFilter: autoFilter);
			FillTable();

			return "<?xml version=\"1.0\"?>" + document.ToString().Replace("xmlns=\"\"", "");
		}
		protected XElement CreateTableInNewWorksheet(IEnumerable<CellInfo> cells, string worksheetName)
		{
			var worksheet = new XElement("Worksheet", new XAttribute(SpreadsheetNS + "Name", worksheetName));
			_workBook.Add(worksheet);
			var table = new XElement("Table",
				new XAttribute(ExcelNS + "FullColumns", "1"),
				new XAttribute(ExcelNS + "FullRows", "1"),
				new XAttribute(SpreadsheetNS + "DefaultRowHeight", "15"));

			_rowsAdded = 0;
			worksheet.Add(table);

			foreach (var cell in cells)
			{
				var columnElement = new XElement("Column",
					new XAttribute(SpreadsheetNS + "AutoFitWidth", cell.Width != null ? 0 : 1));
				if (cell.Width != null)
				{
					columnElement.Add(
						new XAttribute(SpreadsheetNS + "Width", 135 * cell.Width.Value));
				}
				table.Add(columnElement);
			}
			return table;
		}
		/// <summary> Добавляет строку в листе </summary>
		/// <param name="reportNameRow"> Строка для добавления </param>
		/// <param name="table"> Элемент, представляющий таблицу </param>
		protected void AddRow(XElement reportNameRow, XElement table = null)
		{
			(table ?? _table).Add(reportNameRow);
			++_rowsAdded;
		}
		protected XElement CreateWorkbook()
		{
			var result = new XElement(SpreadsheetNS + "Workbook",
				new XAttribute("xmlns", SpreadsheetNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "xsi", Xsi.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "x", ExcelNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "x2", X2.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "ss", SpreadsheetNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "o", O.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "html", HtmlNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "c", C.NamespaceName),
				CreateExcelWorkbookSettings(),
				CreateStyles());
			return result;
		}

		private XElement CreateExcelWorkbookSettings()
		{
			return new XElement(SpreadsheetNS + "ExcelWorkbook",
				new XElement("WindowHeight", 10005),
				new XElement("WindowWidth", 10005),
				new XElement("WindowTopX", 120),
				new XElement("WindowTopY", 135),
				new XElement("RefModeR1C1"),
				new XElement("ProtectStructure", "False"),
				new XElement("ProtectWindows", "False"));
		}
		private XElement CreateStyles()
		{
			return new XElement(SpreadsheetNS + "Styles",
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "Default"), new XAttribute(SpreadsheetNS + "Name", "Normal"),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement("Borders"),
					new XElement("Font", new XAttribute(SpreadsheetNS + "FontName", "Calibri"), new XAttribute(SpreadsheetNS + "CharSet", "204"), new XAttribute(SpreadsheetNS + "Family", "Swiss"), new XAttribute(SpreadsheetNS + "Size", "11"), new XAttribute(SpreadsheetNS + "Color", "#000000")),
					new XElement("Interior"),
					new XElement("NumberFormat"),
					new XElement("Protection")
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s62"),
					new XElement("Font", new XAttribute(SpreadsheetNS + "FontName", "Calibri"), new XAttribute(SpreadsheetNS + "CharSet", "204"), new XAttribute(SpreadsheetNS + "Family", "Swiss"), new XAttribute(SpreadsheetNS + "Size", "11"), new XAttribute(SpreadsheetNS + "Color", "#000000"), new XAttribute(SpreadsheetNS + "Bold", "1"))
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s64"),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Vertical", "Bottom"))
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", DateTimeCellStyle),
					CreateBordersXElement(),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement("NumberFormat", new XAttribute(SpreadsheetNS + "Format", StyleDateTimeFormat))
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", TimeSpanCellStyle),
					CreateBordersXElement(),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement("NumberFormat", new XAttribute(SpreadsheetNS + "Format", StyleTimeSpanFormat))
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s66"),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Horizontal", "Left"), new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement("NumberFormat", new XAttribute(SpreadsheetNS + "Format", "General Date"))
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s70"),
					CreateBordersXElement()
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s75"),
					new XElement("Borders",
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Bottom"), new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Left"),   new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Right"),  new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1"))
					)
				),
				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s92"),
					new XElement("Borders",
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Bottom"), new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Left"),   new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Right"),  new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
						new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Top"),    new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1"))
					),
					new XElement("Alignment", new XAttribute(SpreadsheetNS + "Horizontal", "Center"), new XAttribute(SpreadsheetNS + "Vertical", "Top"), new XAttribute(SpreadsheetNS + "WrapText", "1")),
					new XElement("Interior",  new XAttribute(SpreadsheetNS + "Color", "#D8D8D8"),     new XAttribute(SpreadsheetNS + "Pattern", "Solid"))
				),

				new XElement("Style", new XAttribute(SpreadsheetNS + "ID", "s69"),
					new XElement("Font", new XAttribute(SpreadsheetNS + "Color", "#000000"), new XAttribute(SpreadsheetNS + "Bold", "1"), new XAttribute(SpreadsheetNS + "Size", "11"))
				),
				new XElement("Style",
					new XAttribute(SpreadsheetNS + "ID", "s76"),

					new XElement("Font",
						new XAttribute(SpreadsheetNS + "FontName", "Calibri"),
						new XAttribute(ExcelNS       + "CharSet",  "204"),
						new XAttribute(ExcelNS       + "Family",   "Swiss"),
						new XAttribute(SpreadsheetNS + "Size",     "11"),
						new XAttribute(SpreadsheetNS + "Color",    "#FFFFFF")),
					new XElement("Interior",
						new XAttribute(SpreadsheetNS + "Color",   "#FF0000"),
						new XAttribute(SpreadsheetNS + "Pattern", "Solid")))
			);
		}
		private static XElement CreateBordersXElement()
		{
			return new XElement("Borders",
				new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Bottom"), new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
				new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Left"),   new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
				new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Right"),  new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")),
				new XElement("Border", new XAttribute(SpreadsheetNS + "Position", "Top"),    new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1"))
				);
		}
		protected XElement CreateCellTitleHead(string content, int mergeAcross = 0)
		{
			return new XElement("Cell", new XAttribute(SpreadsheetNS + "StyleID", "s62"), new XAttribute(SpreadsheetNS + "MergeAcross", mergeAcross),
				new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"), content)
				);
		}
		protected XElement CreateCellValueHead(string content, int mergeAcross = 5)
		{
			return new XElement("Cell", new XAttribute(SpreadsheetNS + "StyleID", "s64"), new XAttribute(SpreadsheetNS + "MergeAcross", mergeAcross),
				new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"), content));
		}
		protected static XElement CreateDataCell(CellInfo cellInfo, string value)
		{
			string style;

			if (value != null && value.Contains((char)0))
				throw new ArgumentOutOfRangeException(nameof(value), value, $@"Value should not contain 0x00; cellInfo.Key: {cellInfo.Key}");

			string typeValue = cellInfo.ExcelType;
			if (string.IsNullOrWhiteSpace(value) && cellInfo.WarnWhenEmpty)
			{
				style     = "s76";
				typeValue = "String";
			}
			else
			{
				switch (cellInfo.ExcelType)
				{
					case CellInfo.ExcelTypes.DateTime:
						style = DateTimeCellStyle;
						break;
					case CellInfo.ExcelTypes.TimeSpan:
						style = TimeSpanCellStyle;
						typeValue = CellInfo.ExcelTypes.DateTime;
						break;
					case CellInfo.ExcelTypes.Warning:
						style = string.IsNullOrWhiteSpace(value) ? "s75" : "s76";
						typeValue = "String";
						break;
					//case CellInfo.ExcelTypes.HyperLink:
					//	style = string.IsNullOrWhiteSpace(value) ? "s75" : "s76";
					//	typeValue = "String";
					//	break;
					default:
						style = "s75";
						break;
				}
			}

			var cellElement = new XElement(
				"Cell",
				new XAttribute(SpreadsheetNS + "StyleID", style));

			if (!string.IsNullOrWhiteSpace(value))
				cellElement.Add(
					new XElement("Data",
						new XAttribute(SpreadsheetNS + "Type", typeValue),
						value));
			return cellElement;
		}
		/// <summary> Заполняет заголовок таблицы отчёта </summary>
		protected virtual void CreateTableHeader(XElement table = null, IEnumerable<CellInfo> cells = null, bool autoFilter = true)
		{
			if (table == null)
				table = _table;
			if (cells == null)
				cells = Cells;

			var row = new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"),
				new XAttribute(SpreadsheetNS + "Height", "30"));

			foreach (var cellInfo in cells)
			{
				row.Add(
					new XElement("Cell", new XAttribute(SpreadsheetNS + "StyleID", "s92"),
						new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"), cellInfo.Name ?? cellInfo.Key)));
			}

			AddRow(row, table);

			if (autoFilter)
			{
				var filterExpression = "R" + _rowsAdded + "C1:R" + _rowsAdded + "C" + cells.Count();
				var filterNode = new XElement(ExcelNS + "AutoFilter",
					new XAttribute("Range", filterExpression));
				table.AddAfterSelf(filterNode);
			}
		}
		protected XElement CreateCellGroupHead(string content, int mergeAcross)
		{
			return new XElement("Cell", new XAttribute(SpreadsheetNS + "StyleID", "s75"), new XAttribute(SpreadsheetNS + "MergeAcross", mergeAcross),
				new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"), content));
		}
	}
}