﻿using System;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Интерфейс для фабричного метода для каждого отчёта </summary>
	public interface ITssReport
	{
		/// <summary> Возвращает наименование отчёта </summary>
		/// <param name="culture"> Культура, для которой следует вернуть наименование </param>
		string GetReportName(CultureInfo culture);
		/// <summary> Возвращает описание, что именно должен вернуть отчёт, в том числе важные замечания </summary>
		/// <param name="culture"> Культура, для которой следует вернуть описание </param>
		string GetReportDesc(CultureInfo culture);
		/// <summary> Возвращает контейнер строковых ресурсов для перевода строк </summary>
		/// <param name="culture"> Культура, для которой следует вернуть контейнер строковых ресурсов </param>
		/// <returns> Контейнер строковых ресурсов </returns>
		ResourceStringContainer GetResourceStringContainer(CultureInfo culture);
		/// <summary>
		/// Невозможно создать ссылку на экземпляр класса интерфейсов напрямую, поэтому приходится передавать ее в класс и хранить в нем.
		/// Ссылка на экземпляр класса интерфейсов используется в отчётах, вызывающих для получения данных хранимые процедуры.
		/// </summary>
		/// <param name="iInstance"> Ссылка на объект, содержащий интерфейсы </param>
		void IBasicFunctionSetInstanceSet(IBasicFunctionSet iInstance);
		/// <summary> Возвращает экземпляр класса, содержащий набор параметров для данного отчёта </summary>
		/// <returns></returns>
		ReportParameters ReportParametersInstanceGet(int operatorId);
		/// <summary> Возвращает экземпляр класса, содержащий набор параметров для данного отчёта </summary>
		/// <param name="settings"></param>
		/// <returns></returns>
		ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings);
		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчёта.
		/// Например, если один параметр отчёта зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters"> Частично заполненный набор параметров отчёта </param>
		/// <returns> Скорректированный набор параметров </returns>
		ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters);
		/// <summary>
		/// Функция проверяет переданный набор параметров на корректность заполнения.
		/// После изменения любого из параметров весь набор надо проверять на корректность заполнения.
		/// Если параметры заполнены некорректно, то кнопку "Запуск" следует вообще оставлять недоступной.
		/// </summary>
		/// <param name="iReportParameters"> Заполненный набор параметров отчёта </param>
		/// <returns> В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false </returns>
		ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters);
		/// <summary> Возвращает данные, на основе которых строится отчет, но без построения отчёта </summary>
		object GetReportAsObj(ReportParameters reportParameters);
		/// <summary> Возвращает данные отчёта для форматирования </summary>
		/// <param name="reportParameters"></param>
		/// <returns></returns>
		object GetReportAsDto(ReportParameters reportParameters);
		/// <summary> Создает отчет, используя переданные параметры </summary>
		/// <param name="iReportParameters"> Ссылка на экземпляр параметров </param>
		/// <returns></returns>
		IReportClass Create(ReportParameters iReportParameters);
		/// <summary> Возвращает уникальный код отчёта </summary>
		/// <returns></returns>
		Guid GetGuid();
		/// <summary> Возвращает список поддерживаемых отчётом форматов </summary>
		IEnumerable<ReportTypeEnum> GetSupportedFormats();
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		string GetGroupName();
		/// <summary> Возвращает название файла отчёта  </summary>
		string GetReportFileName(ReportParameters reportParameters);
		/// <summary> Возвращает тему для письма с отчётом  </summary>
		string GetReportSubject(ReportParameters reportParameters);
	}
}