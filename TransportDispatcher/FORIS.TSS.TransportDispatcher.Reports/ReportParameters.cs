﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Enums;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Параметры для создания отчетов </summary>
	[Serializable]
	public class ReportParameters
	{
		/// <summary> Путь к приложению </summary>
		private string _applicationPath;
		[DisplayName("ApplicationPath")]
		[Browsable(false)]
		public string ApplicationPath
		{
			get { return _applicationPath; }
			set { _applicationPath = value; }
		}
		/// <summary> Дата, на которую формируется отчет (если отчет формируется за один день) </summary>
		protected DateTime dtDateReport = DateTime.Now.Date;
		[DisplayName("DateDN"), Category("ReportParsCat"), Description("DateDesc")]
		[Browsable(false)]
		public virtual DateTime DateReport
		{
			get
			{
				return dtDateReport;
			}
			set
			{
				dtDateReport = value;
			}
		}

		/// <summary> Интервал времени </summary>
		private DateTimeInterval _interval = new DateTimeInterval(IntervalAccuracy.ToDay);
		[DisplayName("DateTimeInterval")]
		[Type(typeof(DateTimeInterval))]
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod")]
		[Options("Accuracy:1")]
		[Order(255)]
		public virtual DateTimeInterval DateTimeInterval
		{
			get { return _interval; }
			set
			{
				_interval = value;
				DateFrom = _interval.DateFrom;
				DateTo   = _interval.DateTo;
			}
		}

		/// <summary> Дата начала периода, на который формируется отчет (если отчет формируется за период) </summary>
		protected DateTime dtDateFrom = DateTime.Now.AddDays(-2).Date;
		[DisplayName("PeriodBeginDN"), Category("ReportParsCat"), Description("PeriodBeginDesc")]
		[Browsable(false)]
		public virtual DateTime DateFrom
		{
			get
			{
				return dtDateFrom;
			}
			set
			{
				dtDateFrom = value;
			}
		}

		/// <summary> Дата окончания периода, на который формируется отчет (если отчет формируется за период) </summary>
		protected DateTime dtDateTo = DateTime.Now.Date;
		[DisplayName("PeriodEndDN"), Category("ReportParsCat"), Description("PeriodEndDesc")]
		[Browsable(false)]
		public virtual DateTime DateTo
		{
			get
			{
				return dtDateTo;
			}
			set
			{
				dtDateTo = value;
			}
		}

		/// <summary> Дата окончания периода в секундах от 01-01-1970 </summary>
		[Browsable(false)]
		public int DateToInt
		{
			get
			{
				var dateTime = DateTimeInterval.DateTo;

				if (dateTime.Kind != DateTimeKind.Utc)
				{
					if (dateTime.Kind != DateTimeKind.Unspecified)
					{
						Trace.TraceWarning("Wrong dateTime kind {0} at {1}", dateTime.Kind, Environment.StackTrace);
						dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
						TimeHelper.GetUtcTime(dateTime, TimeZoneInfo);
					}
					dateTime = TimeHelper.GetUtcTime(dateTime, TimeZoneInfo);
				}

				return TimeHelper.GetSecondsFromBase(dateTime);
			}
		}

		/// <summary> Дата начала периода в секундах от 01-01-1970 </summary>
		[Browsable(false)]
		public int DateFromInt
		{
			get
			{
				var dateTime = DateTimeInterval.DateFrom;

				if (dateTime.Kind != DateTimeKind.Utc)
				{
					if (dateTime.Kind != DateTimeKind.Unspecified)
					{
						Trace.TraceWarning("Wrong dateTime kind {0} at {1}", dateTime.Kind, Environment.StackTrace);
						dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
					}
					dateTime = TimeHelper.GetUtcTime(dateTime, TimeZoneInfo);
				}
				return TimeHelper.GetSecondsFromBase(dateTime);
			}
		}
		/// <summary> Флаг, определяет отображается ли отчет заполненным данными, или он будет отображаться пустым </summary>
		bool mblnShowBlankReport = false;
		[DisplayName("EmpryReportDN", true), Category("ReportParsCat"), Description("EmpryReportDesc")]
		public bool ShowBlankReportProp
		{
			get
			{
				return mblnShowBlankReport;
			}
			set
			{
				mblnShowBlankReport = value;
			}
		}
		/// <summary> Идентификатор оператора </summary>
		protected int _operatorId;
		[Browsable(false)]
		public int OperatorId
		{
			get { return _operatorId; }
			set { _operatorId = value; }
		}
		/// <summary> Культура </summary>
		private CultureInfo _culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
		[Browsable(false)]
		public CultureInfo Culture
		{
			get { return _culture; }
			set { _culture = value; }
		}
		/// <summary> Тайм зона </summary>
		private string _timeZoneInfoString;
		[XmlIgnore]
		[NonSerialized]
		[SoapIgnore]
		private TimeZoneInfo _timeZoneInfo;
		[Browsable(false)]
		public TimeZoneInfo TimeZoneInfo
		{
			get
			{
				if (_timeZoneInfo == null && _timeZoneInfoString != null)
					_timeZoneInfo = TimeZoneInfo.FromSerializedString(_timeZoneInfoString);
				return _timeZoneInfo;
			}
			set
			{
				_timeZoneInfo       = value;
				_timeZoneInfoString = _timeZoneInfo != null ? _timeZoneInfo.ToSerializedString() : null;
			}
		}
		[NonSerialized]
		[XmlIgnore]
		[SoapIgnore]
		private Guid? _mapGuid = null;
		[Browsable(false)]
		public Guid? MapGuid
		{
			get { return _mapGuid; }
			set { _mapGuid = value; }
		}
		private DeviceCapability _deviceCapabilities;
		[Browsable(false)]
		public DeviceCapability DeviceCapabilities
		{
			get { return _deviceCapabilities; }
			set { _deviceCapabilities = value; }
		}
		/// <summary> Идентификатор клиента </summary>
		[NonSerialized]
		[XmlIgnore]
		[SoapIgnore]
		private int _departmentId;
		[Browsable(false)]
		public int? DepartmentId
		{
			get { return _departmentId > 0 ? _departmentId : (int?)null; }
			set { _departmentId = value ?? 0; }
		}
		/// <summary> Идентификатор клиента </summary>
		[NonSerialized]
		[XmlIgnore]
		[SoapIgnore]
		private DepartmentType _departmentType = DepartmentType.Physical;
		[Browsable(false)]
		public DepartmentType DepartmentType
		{
			get { return _departmentType; }
			set { _departmentType = value; }
		}
		/// <summary> Все отображаемые свойства </summary>
		[NonSerialized]
		[XmlIgnore]
		[SoapIgnore]
		private IEnumerable<string> _allParametersNames;
		protected IEnumerable<string> AllParameterNames
		{
			get
			{
				if (null == _allParametersNames)
				{
					_allParametersNames = GetType()
						.GetProperties()
						.Where(p =>
						{
							var name = Attribute.GetCustomAttribute(p, typeof(DisplayNameAttribute), false) as DisplayNameAttribute ?? new DisplayNameAttribute(p.Name);
							var type = Attribute.GetCustomAttribute(p, typeof(TypeAttribute),        false) as TypeAttribute        ?? new TypeAttribute(p.PropertyType);
							var ctrl = Attribute.GetCustomAttribute(p, typeof(ControlTypeAttribute), false) as ControlTypeAttribute;
							return name != null || type != null || ctrl != null;
						})
						.Select(p => p.Name);
				}
				return _allParametersNames;
			}
		}

		#region Methods

		public DateTime FromUtc(DateTime value)
		{
			switch (value.Kind)
			{
				case DateTimeKind.Local:
				case DateTimeKind.Unspecified:
					value = DateTime.SpecifyKind(value, DateTimeKind.Utc);
					break;
			}
			return value == DateTime.MinValue
				? value
				: TimeZoneInfo.ConvertTime(value, TimeZoneInfo.Utc, TimeZoneInfo);
		}

		public DateTime ToUtc(DateTime value)
		{
			switch (value.Kind)
			{
				case DateTimeKind.Local:
					value = DateTime.SpecifyKind(value, DateTimeKind.Unspecified);
					break;
				case DateTimeKind.Utc:
					return value;
			}
			return TimeZoneInfo.ConvertTime(value, TimeZoneInfo, TimeZoneInfo.Utc);
		}

		public string FormatUtcAsLocal(DateTime value)
		{
			//TODO: использовать информацию о локали оператора (тоже должна быть в параметрах)
			return FromUtc(value).ToString("dd.MM.yyyy HH:mm:ss");
		}

		public string FormatUtcAsLocalDate(DateTime value)
		{
			//TODO: использовать информацию о локали оператора (тоже должна быть в параметрах)
			return FromUtc(value).ToString("dd.MM.yyyy");
		}

		public string FormatUtcAsLocalTime(DateTime value)
		{
			//TODO: использовать информацию о локали оператора (тоже должна быть в параметрах)
			return FromUtc(value).ToString("HH:mm:ss");
		}

		/// <summary> Функция проверки и преобразования вводимого пользователем значения времени в свойствах отчета </summary>
		/// <param name="iCheckingTimeValue"> Проверяемое значение введенного времени </param>
		/// <returns> Скорректированное значение времени </returns>
		public static string TimeInputCheck(string iCheckingTimeValue)
		{
			//Получение времени на текущий момент для показа примера верного значения.
			string strCurrentTimeValue = DateTime.Now.ToShortTimeString();
			//Переменная для хранения результирующего значения функции.
			string strReturnValue = iCheckingTimeValue;
			try
			{
				//Переменная для хранения результатов проверки введенного значения.
				bool blnWrongTimePartValue = false;
				//Переменная для хранения введенного количества часов.
				int intTimePartHoursValue = 0;
				//Переменная для хранения введенного количества минут.
				int intTimePartMinutesValue = 0;

				//Проверка того, что значение было вообще введено.
				if (iCheckingTimeValue == null)
				{
					//Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
					blnWrongTimePartValue = true;
				}

				//Выделение из введенного значения времени количества часов и минут.
				string[] astrTimeParst = iCheckingTimeValue.ToString().Split(':');

				//Во введенном значении был найден разделитель-двоеточие?
				if (astrTimeParst.Length == 2)
				{
					//Получение введенного количества часов.
					intTimePartHoursValue = Convert.ToInt32(astrTimeParst[0]);
					//Получение введенного количества минут.
					intTimePartMinutesValue = Convert.ToInt32(astrTimeParst[1]);
				}
				else
				{
					//Получение введенного значения в виде строки.
					string strEnteredString = astrTimeParst[0];
					//Вычисление количества символов во введенной строке.
					int intSymbolsCount = strEnteredString.Length;

					//Определение введенного времени.
					switch (intSymbolsCount)
					{
						case 3:
							//[Введено три цифры (кол-во часов может оказаться меньше 10)].
							//Получение количества часов в веденном значении времени.
							intTimePartHoursValue = Convert.ToInt32(strEnteredString.Substring(0, 1));
							//Получение количества минут в веденном значении времени.
							intTimePartMinutesValue = Convert.ToInt32(strEnteredString.Substring(1, 2));
							break;
						case 4:
							//[Введено четыре цифры (две - количество часов, две - количество минут)].
							//Получение количества часов в веденном значении времени.
							intTimePartHoursValue = Convert.ToInt32(strEnteredString.Substring(0, 2));
							//Получение количества часов в веденном значении времени.
							intTimePartMinutesValue = Convert.ToInt32(strEnteredString.Substring(2, 2));
							break;
						default:
							//Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
							blnWrongTimePartValue = true;
							break;
					}

					//В строку времени добавляется опущенное пользователем двоеточие.
					strReturnValue = intTimePartHoursValue + ":" + intTimePartMinutesValue;
				}

				//Проверка корректности введенного количества часов.
				if (intTimePartHoursValue < 0 || intTimePartHoursValue > 23)
				{
					//Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
					blnWrongTimePartValue = true;
				}

				//Проверка корректности введенного количества минут.
				if (intTimePartMinutesValue < 0 || intTimePartMinutesValue > 59)
				{
					//Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
					blnWrongTimePartValue = true;
				}
				else
				{
					//Пользователь ввел одну цифру в значение минут?
					if (astrTimeParst[1].Length == 1)
					{
						//[Пользователь ввел одну цифру в значение минут].
						//Следует добавить ноль в значение минут в конец или в начало?
						if (intTimePartMinutesValue < 6)
						{
							//[Следует добавить ноль в значение минут в конец].
							//Корректировка введенного пользователем значения минут: к возвращаемому результату добавляется ноль (например, 16:2 превращается в 16:20).
							strReturnValue += "0";
						}
						else
						{
							//[Следует добавить ноль в значение минут в конец].
							//Корректировка введенного пользователем значения минут: к возвращаемому результату добавляется ноль (например, 16:7 превращается в 16:07).
							strReturnValue = intTimePartHoursValue + ":0" + intTimePartMinutesValue; ;
						}
					}
				}

				//Значение времени введено корректно?
				if (blnWrongTimePartValue)
				{
					//[Значение времени введено некорректно].
					//Возврат старого неизменного значения времени.
					strReturnValue = strCurrentTimeValue;
					//Сообщение для пользователя о некорректном вводе значения времени.
					Trace.TraceError("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
				}

				//Функция возвращает скорректированное значение.
				return strReturnValue;
			}
			catch
			{
				//[Значение времени введено некорректно].
				//Сообщение для пользователя о некорректном вводе значения времени.
				Trace.TraceError("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
				//Возврат старого неизменного значения времени.
				return strCurrentTimeValue;
			}
		}

		public virtual IEnumerable<string> GetParametersForView(IBasicFunctionSet basicFunctionSet)
		{
			return null;
		}

		#endregion
	}
}