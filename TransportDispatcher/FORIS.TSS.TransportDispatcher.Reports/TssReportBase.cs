﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Класс содержит функции, общие для всех отчетов </summary>
	public abstract class TssReportBase : ITssReport
	{
		static TssReportBase()
		{
			InvalidFileNameChars = new HashSet<char>();
			foreach (var @char in Path.GetInvalidFileNameChars())
				InvalidFileNameChars.Add(@char);
			//Запятые не разрешены для совместимости с заголовком HTTP Content-Disposition
			InvalidFileNameChars.Add(',');
			// Регистрация директорий со шрифтами, для отчетов iTextSharp (генерация PDF)
			iTextSharp.text.FontFactory.RegisterDirectories();
		}
		/// <summary> Порядок отчета (по нему осуществляется сортировка отчетов в группах) </summary>
		public abstract int Order { get; }
		/// <summary> Свойство видимости элемента управления подписками для этого отчета </summary>
		public virtual bool IsSubscriptionVisible { get; set; } = true;
		public virtual IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			return new[] { ReportTypeEnum.Acrobat, ReportTypeEnum.Excel, ReportTypeEnum.Html };
		}

		#region statics

		/// <summary>
		/// Функция заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
		/// </summary>
		/// <param name="iInstance">Ссылка на экземпляр класса интерфейсов.</param>
		/// <returns></returns>
		public static ConstantsListDataset ReportEstablishmentAttributesFill(IBasicFunctionSet iInstance)
		{
			//Переменная для хранения ссылки на экземпляр класса интерфейсов.
			IBasicFunctionSet clsInstance = iInstance;

			//Создание датасета со структурой таблицы с данными предприятия.
			var dsEstablishmentAttributes = new ConstantsListDataset();

			try
			{
				//Создание строки, имеющей структуру данных как у таблицы ConstantsList.
				ConstantsListDataset.ConstantsListRow rowConstantsData = dsEstablishmentAttributes.ConstantsList.NewConstantsListRow();
				//Установка значений для полей отчета с атрибутами предприятия.
				rowConstantsData.EstablishmentName      = clsInstance.GetConstant(Constant.ESTABLISHMENT_NAME);
				rowConstantsData.EstablishmentAddress   = clsInstance.GetConstant(Constant.ESTABLISHMENT_ADDRESS);
				rowConstantsData.EstablishmentPhone     = clsInstance.GetConstant(Constant.ESTABLISHMENT_PHONE);
				rowConstantsData.ReportCreatorSignature = clsInstance.GetConstant(Constant.REPORT_CREATOR_SIGNATURE);
				//Отправка данных в датасет.
				dsEstablishmentAttributes.ConstantsList.AddConstantsListRow(rowConstantsData);

				//Функция возвращает сформированный датасет с атрибутами предприятия.
				return dsEstablishmentAttributes;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.

				Trace.TraceError("{0}", ex);

				return null;
			}
			finally
			{
				//Датасет еще не уничтожен?
				if (dsEstablishmentAttributes != null)
				//[Датасет еще не уничтожен].
				{
					//Уничтожение датасета.
					dsEstablishmentAttributes.Dispose();
					// нет смысла переменная локальная
					// dsEstablishmentAttributes = null;
				}
			}
		}

		#endregion statics

		/// <summary> Переменная для хранения ссылки на экземпляр класса интерфейсов </summary>
		/// <remarks> Ссылка на экземпляр класса интерфейсов используется в отчетах, вызывающих для получения данных хранимые процедуры </remarks>
		protected IBasicFunctionSet mclsInstance;

		#region ITssReport members

		public void IBasicFunctionSetInstanceSet(IBasicFunctionSet iInstance)
		{
			mclsInstance = iInstance;
		}
		public ReportParameters ReportParametersInstanceGet(int operatorId)
		{
			return ReportParametersInstanceGet(new TssReportParametersSettings { OperatorId = operatorId });
		}
		public virtual ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			return iReportParameters;
		}
		public virtual ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var result = new ReportParametersInstanceCheckResult();

			var dateFrom = iReportParameters.DateTimeInterval.DateFrom;
			var dateTo   = iReportParameters.DateTimeInterval.DateTo;

			if (dateFrom > dateTo)
				result.AddWrongArgument("DateTimeInterval", "DateFrom cannot be greater than DateTo");

			const int maxReportAge = 10;
			if (iReportParameters.DateFrom < DateTime.UtcNow.AddYears(-maxReportAge))
				result.AddWrongArgument("DateTimeInterval", string.Format("DateFrom cannot be older than {0} years", maxReportAge));

			if (DateTime.UtcNow.AddDays(1) < iReportParameters.DateTo)
				result.AddWrongArgument("DateTimeInterval", "DateTo cannot belong to the future");

			return result;
		}
		public abstract IReportClass Create(ReportParameters iReportParameters);
		public abstract ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings);
		public abstract string GetReportName(CultureInfo culture);
		public virtual string GetGroupName()
		{
			return null;
		}

		#region NotImplemented

		public virtual object GetReportAsObj(ReportParameters reportParameters)
		{
			throw new NotImplementedException("GetReportData(ReportParameters reportParameters) is not implemented");
		}
		public virtual object GetReportAsDto(ReportParameters reportParameters)
		{
			throw new NotImplementedException("GetReportDTO(ReportParameters reportParameters) is not implemented");
		}

		#endregion NotImplemented

		#endregion ITSS members

		public Guid GetGuid()
		{
			var attribute = Attribute.GetCustomAttribute(GetType(), typeof(GuidAttribute));
			return attribute == null
				? Guid.Empty
				: new Guid(((GuidAttribute)attribute).Value);
		}
		public static void TranslateReport<T>(ReportClassWrapper report, CultureInfo culture)
			where T : TssReportBase, new()
		{
			if (default == report)
				return;
			if (default == culture)
				return;
			var resourceStringContainer = new T().GetResourceStringContainer(culture);
			TranslateReport<T>(report, resourceStringContainer);
		}
		public static void TranslateReport<T>(ReportClassWrapper report, ResourceStringContainer resourceStringContainer)
			where T : TssReportBase
		{
			if (default == report)
				return;
			if (default == resourceStringContainer)
				return;
			TranslateReportObjects(report.ReportDocument.ReportDefinition.ReportObjects, resourceStringContainer);
			foreach (ReportDocument subReport in report.ReportDocument.Subreports)
				TranslateReportObjects(subReport.ReportDefinition.ReportObjects, resourceStringContainer);
		}
		private static void TranslateReportObjects(ReportObjects reportObjects, ResourceStringContainer resourceStringContainer)
		{
			foreach (var repObj in reportObjects)
			{
				var textObj = repObj as TextObject;
				if (textObj == null)
					continue;
				var text = resourceStringContainer.GetStringOrDefault(textObj.Name);
				if (text != null)
					textObj.Text = text;
			}
		}
		public virtual string GetReportDesc(CultureInfo culture)
		{
			return string.Empty;
		}
		public virtual string GetReportSubject(ReportParameters reportParameters)
		{
			if (reportParameters == null)
				throw new ArgumentNullException(nameof(reportParameters));

			var sb = new StringBuilder();

			var objectName =
				GetVehicleName(reportParameters)
				?? GetVehicleGroupName(reportParameters);

			if (objectName != null)
				sb.Append(objectName).Append(": ");

			var reportName = GetReportName(reportParameters.Culture);
			sb.Append(reportName).Append(", ");

			var dateFrom = reportParameters.DateTimeInterval.DateFrom;
			var dateFromString = FormatDateTimeForFileName(dateFrom);
			sb.Append(dateFromString);

			var dateTo = reportParameters.DateTimeInterval.DateTo;
			if (dateFrom != dateTo)
			{
				var dateToString = FormatDateTimeForFileName(dateTo);
				sb.Append("-").Append(dateToString);
			}

			return sb.ToString();
		}
		public virtual string GetReportFileName(ReportParameters reportParameters)
		{
			if (reportParameters == null)
				throw new ArgumentNullException(nameof(reportParameters));

			var reportName = GetReportName(reportParameters.Culture);
			PrepareStringToFileName(reportName);

			var objectName = GetVehicleName(reportParameters)
				?? GetVehicleGroupName(reportParameters);

			if (objectName != null)
				objectName = PrepareStringToFileName(objectName);

			var dateFrom = reportParameters.DateTimeInterval.DateFrom;
			var dateFromString = FormatDateTimeForFileName(dateFrom);

			var sb = new StringBuilder();
			if (!string.IsNullOrWhiteSpace(objectName))
			{
				sb.Append(objectName).Append(".");
			}
			sb.Append(reportName);
			sb.Append(".");
			sb.Append(dateFromString);
			var dateTo = reportParameters.DateTimeInterval.DateTo;
			if (dateFrom != dateTo)
			{
				var dateToString = FormatDateTimeForFileName(dateTo);
				sb.Append("-").Append(dateToString);
			}

			return sb.ToString().Replace(' ', '_');
		}

		private static readonly HashSet<char> InvalidFileNameChars;
		protected static string PrepareStringToFileName(string s)
		{
			var sb = new StringBuilder(s.Length);
			foreach (var c in s)
			{
				if (InvalidFileNameChars.Contains(c))
					continue;
				sb.Append(c);
			}
			return sb.ToString();
		}
		protected string FormatDateTimeForFileName(DateTime dateTime)
		{
			if (dateTime.Date == dateTime)
				return dateTime.ToString("yyyy.MM.dd");
			return dateTime.ToString("yyyy.MM.dd HH-mm");
		}
		public string GetVehicleGroupName(ReportParameters reportParameters)
		{
			int? id = GetIdProperty(reportParameters, ReportParametersUtils.VehicleGroupPicker);
			if (id == null)
				return null;
			var vehicleGroup = mclsInstance.GetVehicleGroupById(id.Value);
			if (vehicleGroup == null)
				return null;
			return vehicleGroup.name;
		}
		public string GetVehicleName(ReportParameters reportParameters)
		{
			int? id = GetIdProperty(reportParameters, ReportParametersUtils.VehiclePicker);
			if (id == null)
				return null;
			var vehicle = mclsInstance.GetVehicleById(reportParameters.OperatorId, id.Value, reportParameters.Culture);
			if (vehicle == null)
				return null;
			return vehicle.Name;
		}
		private static int? GetIdProperty(ReportParameters reportParameters, string controlType)
		{
			var property = reportParameters.GetType()
				.GetProperties()
				.FirstOrDefault(p =>
					p.GetCustomAttributes(typeof(ControlTypeAttribute), true)
						.Any(a => ((ControlTypeAttribute)a).Type == controlType));
			if (property == null)
				return null;
			return property.GetValue(reportParameters, null) as int?;
		}
		public virtual ResourceStringContainer GetResourceStringContainer(CultureInfo culture)
		{
			return new ResourceStringContainer(culture, GetType().Namespace + ".Strings", GetType().Assembly);
		}
	}
}