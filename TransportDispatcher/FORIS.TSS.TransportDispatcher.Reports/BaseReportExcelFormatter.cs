﻿using System;
using System.IO;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public abstract class BaseReportExcelFormatter : IReportClass
	{
		public virtual void Dispose()
		{
			//nothing to do
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			var bytes = GetBytes(formatType);

			File.WriteAllBytes(fileName, bytes);
		}
		private byte[] GetBytes(ReportTypeEnum formatType)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					{
						var xmlString = GetExcelXmlString();

						return Encoding.UTF8.GetBytes(xmlString);
					}
				default:
					throw new NotSupportedException("Report format " + formatType + " is not supported");
			}
		}
		protected abstract string GetExcelXmlString();
	}
}