﻿using System.IO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public abstract class BaseReportFormatter : IReportClass
	{
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			File.WriteAllBytes(fileName, GetBytes(formatType));
		}
		public virtual void Dispose() { }
		protected abstract byte[] GetBytes(ReportTypeEnum formatType);
	}
}