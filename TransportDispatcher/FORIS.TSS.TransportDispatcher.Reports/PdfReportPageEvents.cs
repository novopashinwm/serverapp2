﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class PdfReportPageEvents : PdfPageEventHelper
	{
		protected BaseFont    ArialBaseFont = ReportHelper.ArialBaseFont;
		protected PdfTemplate PageNumbersTemplate;
		protected float       PageNumbersTextSize = 8;
		protected string      PageNumbersSeparator = "/";
		private   PdfPTable   headerPdfTable;
		private   float       DefaultPadding     = 2f;
		private   float       DefaultBorderWidth = 1f;

		public override void OnOpenDocument(PdfWriter writer, Document document)
		{
			// Таблица заголовка страницы
			headerPdfTable = GetPageHeaderTable(writer, document);
			// Шаблон для указания общего числа страниц
			var templateW = ArialBaseFont.GetWidthPoint(string.Empty.PadRight(5, '0'), PageNumbersTextSize);
			var templateH = ArialBaseFont.GetAscentPoint(PageNumbersSeparator, PageNumbersTextSize) -
				ArialBaseFont.GetDescentPoint(PageNumbersSeparator, PageNumbersTextSize);
			PageNumbersTemplate = writer.DirectContent.CreateTemplate(templateW, templateH);
			// Установка границ страницы
			document.SetMargins(
				document.LeftMargin,
				document.RightMargin,
				document.TopMargin    + 1f * DefaultPadding + headerPdfTable?.CalculateHeights() ?? 0f,
				document.BottomMargin + 2f * DefaultPadding + PageNumbersTemplate.Height);
		}
		public override void OnCloseDocument(PdfWriter writer, Document document)
		{
			var pageNumberT = (writer.PageNumber).ToString();
			var pageNumberH = ArialBaseFont.GetAscentPoint(pageNumberT, PageNumbersTextSize) -
				ArialBaseFont.GetDescentPoint(pageNumberT, PageNumbersTextSize);
			var pageNumberX = 0f;
			var pageNumberY = - ArialBaseFont.GetDescentPoint(PageNumbersSeparator, PageNumbersTextSize);
			ColumnText.ShowTextAligned(
				PageNumbersTemplate,
				Element.ALIGN_LEFT,
		 		new Phrase((writer.PageNumber).ToString(), new Font(ArialBaseFont, PageNumbersTextSize, Font.NORMAL)),
		 		pageNumberX, pageNumberY, 0f);
		}
		public override void OnEndPage(PdfWriter writer, Document document)
		{
			AddPageHeader(writer, document);
			AddPageFooter(writer, document);
			//AddPageMarginsAndGrid(writer, document);
		}
		private PdfPTable GetPageHeaderTable(PdfWriter writer, Document document)
		{
			// Создаем таблицу заголовка страницы
			var pdfTab = new PdfPTable(3);
			// Текст ячеек заголовка страницы
			//var txtCell1 = "Ufin"; // Пока убираем заголовки, пока не придумаем как делать заголовки из настроек
			var txtCell1 = string.Empty;
			var txtCell2 = writer.Info.GetAsString(PdfName.TITLE)?.ToUnicodeString() ?? "";
			//var txtCell3 = "на контроле"; // Пока убираем заголовки, пока не придумаем как делать заголовки из настроек
			var txtCell3 = string.Empty;
			// Фразы ячеек заголовка страницы
			var phrCell1 = new Phrase(txtCell1, new Font(ReportHelper.LucidaBaseFont, 18, Font.BOLD, new BaseColor(27, 127, 109)));
			var phrCell2 = new Phrase(txtCell2, new Font(ReportHelper.ArialBaseFont,  12, Font.BOLD, new BaseColor(00, 000, 000)));
			var phrCell3 = new Phrase(txtCell3, new Font(ReportHelper.LucidaBaseFont, 12, Font.BOLD, new BaseColor(27, 127, 109)));
			// Ячейки таблицы заголовка страницы
			var pdfCell1 = new PdfPCell(phrCell1) {
				Padding = 2f * DefaultPadding, HorizontalAlignment = Element.ALIGN_LEFT,   VerticalAlignment = Element.ALIGN_BOTTOM, Border = Rectangle.BOTTOM_BORDER, BorderWidth = DefaultBorderWidth };
			var pdfCell2 = new PdfPCell(phrCell2) {
				Padding = 2f * DefaultPadding, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, Border = Rectangle.BOTTOM_BORDER, BorderWidth = DefaultBorderWidth };
			var pdfCell3 = new PdfPCell(phrCell3) {
				Padding = 2f * DefaultPadding, HorizontalAlignment = Element.ALIGN_RIGHT,  VerticalAlignment = Element.ALIGN_BOTTOM, Border = Rectangle.BOTTOM_BORDER, BorderWidth = DefaultBorderWidth };
			// Добавить ячейки заголовка страницы в таблицу
			pdfTab.AddCell(pdfCell1);
			pdfTab.AddCell(pdfCell2);
			pdfTab.AddCell(pdfCell3);
			// Указываем полную ширину таблицы заголовка страницы
			pdfTab.TotalWidth =
				document.PageSize.Width -
				document.LeftMargin     -
				document.RightMargin;
			// Рассчитываем ширину каждой колонки в зависимости от наполнения
			var wdhCell1 = ColumnText.GetWidth(phrCell1) + pdfCell1.PaddingLeft + pdfCell1.PaddingRight + 2 * DefaultPadding;
			var wdhCell3 = ColumnText.GetWidth(phrCell3) + pdfCell3.PaddingLeft + pdfCell3.PaddingRight + 2 * DefaultPadding;
			var wdhCell2 = pdfTab.TotalWidth - wdhCell1 - wdhCell3;
			// Указываем ширину каждой колонки в зависимости от наполнения
			pdfTab.SetWidths(new [] { wdhCell1, wdhCell2, wdhCell3 });
			return pdfTab;
		}
		private void AddPageHeader(PdfWriter writer, Document document)
		{
			headerPdfTable
				.WriteSelectedRows(0, -1,
					document.LeftMargin, document.PageSize.Height - document.TopMargin + DefaultPadding + (headerPdfTable?.CalculateHeights() ?? 0f),
					writer.DirectContent);
		}
		private void AddPageFooter(PdfWriter writer, Document document)
		{
			var pageNumberT = writer.PageNumber.ToString() + PageNumbersSeparator;
			var pageNumberW = ArialBaseFont.GetWidthPoint(pageNumberT, PageNumbersTextSize);
			var pageNumberH = ArialBaseFont.GetAscentPoint(pageNumberT, PageNumbersTextSize) -
				ArialBaseFont.GetDescentPoint(pageNumberT, PageNumbersTextSize);
			var pageNumberX = document.PageSize.GetRight(document.RightMargin   + pageNumberW + PageNumbersTemplate.Width);
			var pageNumberY = document.PageSize.GetBottom(document.BottomMargin - pageNumberH - 1.5f * DefaultPadding);

			var cb = writer.DirectContent;
			ColumnText.ShowTextAligned(
				cb,
				Element.ALIGN_LEFT,
				new Phrase(pageNumberT, new Font(ArialBaseFont, PageNumbersTextSize, Font.NORMAL)),
				pageNumberX, pageNumberY - ArialBaseFont.GetDescentPoint(pageNumberT, PageNumbersTextSize), 0f);
			cb.AddTemplate(
				PageNumbersTemplate,
				pageNumberX + pageNumberW,
				pageNumberY);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(document.PageSize.GetLeft (document.LeftMargin),  document.PageSize.GetBottom(document.BottomMargin - 0.5f * DefaultPadding));
			cb.LineTo(document.PageSize.GetRight(document.RightMargin), document.PageSize.GetBottom(document.BottomMargin - 0.5f * DefaultPadding));
			cb.Stroke();
		}
		private void AddPageMarginsAndGrid(PdfWriter writer, Document document)
		{
			var cb = writer.DirectContent;
			/////////////////////////////////////////////////////////////////////////////////
			// Каждые 5мм вертикальные
			var stepWSize = (document.PageSize.Width / 210f) * 5f;
			for (float stepV = stepWSize; stepV < document.PageSize.Height; stepV += stepWSize)
			{
				cb.SetLineWidth(0);
				cb.SetColorStroke(BaseColor.GREEN);
				cb.MoveTo(stepV, 0);
				cb.LineTo(stepV, document.PageSize.Height);
				cb.Stroke();
			}
			// 20f от края левая
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(document.PageSize.GetLeft(20f),                       0f);
			cb.LineTo(document.PageSize.GetLeft(20f), document.PageSize.Height);
			cb.Stroke();
			// LeftMargin
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(document.PageSize.GetLeft(document.LeftMargin), 0f);
			cb.LineTo(document.PageSize.GetLeft(document.LeftMargin), document.PageSize.Height);
			cb.Stroke();
			// 20f от края правая
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(document.PageSize.GetRight(20f),                       0f);
			cb.LineTo(document.PageSize.GetRight(20f), document.PageSize.Height);
			cb.Stroke();
			// RightMargin
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(document.PageSize.GetRight(document.RightMargin), 0f);
			cb.LineTo(document.PageSize.GetRight(document.RightMargin), document.PageSize.Height);
			cb.Stroke();
			// Каждые 5мм горизонтальные
			var stepHSize = (document.PageSize.Height / 297f) * 5f;
			for (float stepH = stepWSize; stepH < document.PageSize.Height; stepH += stepWSize)
			{
				cb.SetLineWidth(0);
				cb.SetColorStroke(BaseColor.GREEN);
				cb.MoveTo(0,                       document.PageSize.Height - stepH);
				cb.LineTo(document.PageSize.Width, document.PageSize.Height - stepH);
				cb.Stroke();
			}
			// 20f от края верхняя
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(0,                       document.PageSize.GetTop(20f));
			cb.LineTo(document.PageSize.Width, document.PageSize.GetTop(20f));
			cb.Stroke();
			// TopMargin
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(0,                       document.PageSize.GetTop(document.TopMargin));
			cb.LineTo(document.PageSize.Width, document.PageSize.GetTop(document.TopMargin));
			cb.Stroke();
			// 20f от края нижняя
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(0,                       document.PageSize.GetBottom(20f));
			cb.LineTo(document.PageSize.Width, document.PageSize.GetBottom(20f));
			cb.Stroke();
			// BottomMargin
			cb.SetLineWidth(0);
			cb.SetColorStroke(BaseColor.BLACK);
			cb.MoveTo(0,                       document.PageSize.GetBottom(document.BottomMargin));
			cb.LineTo(document.PageSize.Width, document.PageSize.GetBottom(document.BottomMargin));
			cb.Stroke();
			/////////////////////////////////////////////////////////////////////////////////
		}
	}
}