﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class WorkbookEngine
	{
		static string CreateWorkbook(string xml, string templateFileName)
		{
			Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

			var xmlSource = new XmlTextReader(new StringReader(xml));
			var xpathDoc = new XPathDocument(xmlSource);

			var templatesPath = Path.Combine(
				   ConfigurationManager.AppSettings["WorkbookEngine.XslTemplatesPath"]
				?? ConfigurationManager.AppSettings["ReportAssembliesPath"]
				?? Environment.CurrentDirectory
				, templateFileName);

			using (var xslSource = new XmlTextReader(templatesPath))
			{
				var xsltDoc = new XslCompiledTransform();

				xsltDoc.Load(xslSource);

				var sb = new StringBuilder();
				var sw = new StringWriter(sb);

				xsltDoc.Transform(xpathDoc, null, sw);

				return sw.ToString();
			}
		}
		public static string CreateWorkbook(DataSet ds, string templateFileName)
		{
			return CreateWorkbook(ds.GetXml(), templateFileName);
		}
		/// <summary>
		///
		/// </summary>
		/// <param name="templateFileName"></param>
		/// <param name="objects">Передаваемые объекты должны иметь метод GetXml или быть коллекциями таких объектов</param>
		/// <returns></returns>
		public static string CreateWorkbook(string templateFileName, params object[] objects)
		{
			using (var textWriter = new StringWriter())
			{
				using (var writer = new XmlTextWriter(textWriter))
				{
					writer.WriteStartElement("response");
					foreach (var @object in objects)
					{
						if (@object != null)
						{
							if (@object.GetType().IsValueType)
								throw new NotSupportedException("Only class or collection can be serialize to xml");

							if (@object is IList)
							{
								writer.WriteStartElement(((IList)@object)[0].GetType().Name + "s");
								foreach (var o in (IList)@object)
								{
									InvokeWriteXml(o, writer);
								}
								writer.WriteEndElement();
							}
							else
							{
								InvokeWriteXml(@object, writer);
							}
						}
					}
					writer.WriteEndElement();
				}
				return CreateWorkbook("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + textWriter.ToString(), templateFileName);
			}
		}
		static void InvokeWriteXml(object @object, XmlWriter writer)
		{
			var methods = @object.GetType().GetMethods();
			if (!methods.Any(m => m.Name == "WriteXml"))
				throw new NotSupportedException("Object has not method WriteXml");
			var method = methods.First(m => m.Name == "WriteXml");
			method.Invoke(@object, parameters: new[] { writer });
		}
	}
}