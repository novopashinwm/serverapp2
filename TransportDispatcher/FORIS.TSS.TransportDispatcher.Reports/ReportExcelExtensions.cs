﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public static class ReportExcelExtensions
	{
		/// <summary> Пространство имён spreadsheet </summary>
		private static readonly XNamespace SpreadsheetNS = "urn:schemas-microsoft-com:office:spreadsheet";
		/// <summary> Пространство имён excel </summary>
		private static readonly XNamespace ExcelNS       = "urn:schemas-microsoft-com:office:excel";
		/// <summary> Другие необходимые пространства имен </summary>
		private static readonly XNamespace X2            = "http://schemas.microsoft.com/office/excel/2003/xml";
		private static readonly XNamespace C             = "urn:schemas-microsoft-com:office:component:spreadsheet";
		private static readonly XNamespace Xsi           = "http://www.w3.org/2001/XMLSchema-instance";
		private static readonly XNamespace O             = "urn:schemas-microsoft-com:office:office";
		private static readonly XNamespace HtmlNS        = "http://www.w3.org/TR/REC-html40";

		private const string StyleDateTimeFormat = "yyyy\\-mm\\-dd HH:MM:ss";
		private const string StyleTimeSpanFormat = "[h]:mm:ss";

		private const string DateTimeCellStyle = "DateTimeStyle";
		private const string TimeSpanCellStyle = "TimeSpanStyle";
		private const string NumberCellStyle   = "NumberStyle";
		private const string HyperLinkStyle    = "HyperLinkStyle";

		public static XDocument Build(this XElement xElement)
		{
			return new XDocument(
				new XDeclaration("1.0", "utf-8", "yes"),
				new XProcessingInstruction("mso-application", "progid=\"Excel.Sheet\""),
				xElement);
		}
		public static XElement Root(this XElement xElement)
		{
			if (xElement == default)
				return default;
			var root = xElement;
			while (root.Parent != default)
				root = root.Parent;
			return root;
		}
		public static XElement WithContent(this XElement xElement, object content)
		{
			xElement.Add(content);
			return xElement;
		}
		public static XElement With(this XElement xElement, Action<XElement> action)
		{
			action?.Invoke(xElement);
			return xElement;
		}
		public static XElement WithContent(this XElement xElement, params object[] content)
		{
			xElement.Add(content);
			return xElement;
		}
		public static XElement WithContent(this XElement xElement, Func<object[]> contentGetter)
		{
			return xElement.WithContent(contentGetter?.Invoke());
		}
		public static XElement CreateWorkbook()
		{
			return new XElement(SpreadsheetNS + "Workbook",
				new XAttribute("xmlns", SpreadsheetNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "xsi", Xsi.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "x", ExcelNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "x2", X2.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "ss", SpreadsheetNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "o", O.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "html", HtmlNS.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "c", C.NamespaceName),
				new XElement(SpreadsheetNS + "ExcelWorkbook",
					new XElement(SpreadsheetNS + "RefModeR1C1")
				)
			);
		}
		public static XElement WithStyles(this XElement xWorkbook)
		{
			xWorkbook.Add(new XElement(SpreadsheetNS + "Styles",
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "Default"),
					new XAttribute(SpreadsheetNS + "Name", "Normal"),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Vertical", "Bottom")
					),
					new XElement(SpreadsheetNS + "Font",
						new XAttribute(SpreadsheetNS + "FontName", "Calibri"),
						new XAttribute(SpreadsheetNS + "CharSet", "204"),
						new XAttribute(SpreadsheetNS + "Family", "Swiss"),
						new XAttribute(SpreadsheetNS + "Size", "11"),
						new XAttribute(SpreadsheetNS + "Color", "#000000")
					),
					new XElement(SpreadsheetNS + "Borders"),
					new XElement(SpreadsheetNS + "Interior"),
					new XElement(SpreadsheetNS + "NumberFormat"),
					new XElement(SpreadsheetNS + "Protection")
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", DateTimeCellStyle),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement(SpreadsheetNS + "NumberFormat",
						new XAttribute(SpreadsheetNS + "Format", StyleDateTimeFormat))
				)
					.WithBorders(true, true, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", TimeSpanCellStyle),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Vertical", "Bottom")),
					new XElement(SpreadsheetNS + "NumberFormat",
						new XAttribute(SpreadsheetNS + "Format", StyleTimeSpanFormat))
				)
					.WithBorders(true, true, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", NumberCellStyle),
					new XElement(SpreadsheetNS + "NumberFormat",
						new XAttribute(SpreadsheetNS + "Format", "Fixed")
					)
				)
					.WithBorders(true, true, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", HyperLinkStyle),
					new XElement(SpreadsheetNS + "Font",
						new XAttribute(SpreadsheetNS + "FontName",  "Calibri"),
						new XAttribute(SpreadsheetNS + "CharSet",   "204"),
						new XAttribute(SpreadsheetNS + "Family",    "Swiss"),
						new XAttribute(SpreadsheetNS + "Size",      "11"),
						new XAttribute(SpreadsheetNS + "Color",     "#0066CC"),
						new XAttribute(SpreadsheetNS + "Underline", "Single")
					)
				)
					.WithBorders(true, true, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s62"),
					new XElement(SpreadsheetNS + "Font",
						new XAttribute(SpreadsheetNS + "FontName", "Calibri"),
						new XAttribute(SpreadsheetNS + "CharSet", "204"),
						new XAttribute(SpreadsheetNS + "Family", "Swiss"),
						new XAttribute(SpreadsheetNS + "Size", "11"),
						new XAttribute(SpreadsheetNS + "Color", "#000000"),
						new XAttribute(SpreadsheetNS + "Bold", "1")
					)
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s64"),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Vertical", "Bottom")
					)
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s66"),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Horizontal", "Left"),
						new XAttribute(SpreadsheetNS + "Vertical", "Bottom")
					),
					new XElement(SpreadsheetNS + "NumberFormat",
						new XAttribute(SpreadsheetNS + "Format", "General Date")
					)
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s69"),
					new XElement(SpreadsheetNS + "Font",
						new XAttribute(SpreadsheetNS + "Color", "#000000"),
						new XAttribute(SpreadsheetNS + "Bold", "1"),
						new XAttribute(SpreadsheetNS + "Size", "11"))
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s70")
				)
					.WithBorders(true, true, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s75")
				)
					.WithBorders(true, false, true, true),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s76"),
					new XElement(SpreadsheetNS + "Font",
						new XAttribute(SpreadsheetNS + "FontName", "Calibri"),
						new XAttribute(ExcelNS + "CharSet", "204"),
						new XAttribute(ExcelNS + "Family", "Swiss"),
						new XAttribute(SpreadsheetNS + "Size", "11"),
						new XAttribute(SpreadsheetNS + "Color", "#FFFFFF")
					),
					new XElement(SpreadsheetNS + "Interior",
						new XAttribute(SpreadsheetNS + "Color", "#FF0000"),
						new XAttribute(SpreadsheetNS + "Pattern", "Solid")
					)
				),
				new XElement(SpreadsheetNS + "Style",
					new XAttribute(SpreadsheetNS + "ID", "s92"),
					new XElement(SpreadsheetNS + "Alignment",
						new XAttribute(SpreadsheetNS + "Horizontal", "Center"),
						new XAttribute(SpreadsheetNS + "Vertical", "Top"),
						new XAttribute(SpreadsheetNS + "WrapText", "1")
					),
					new XElement(SpreadsheetNS + "Interior",
						new XAttribute(SpreadsheetNS + "Color", "#D8D8D8"),
						new XAttribute(SpreadsheetNS + "Pattern", "Solid")
					)
				)
					.WithBorders(true, true, true, true)
			));
			return xWorkbook;
		}
		public static XElement WithBorders(this XElement xElement, bool left = true, bool top = true, bool right = true, bool bottom = true)
		{
			var borders = new XElement(SpreadsheetNS + "Borders");
			if (left)
				borders.Add(new XElement(SpreadsheetNS + "Border", new XAttribute(SpreadsheetNS + "Position", "Left"),   new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")));
			if (top)
				borders.Add(new XElement(SpreadsheetNS + "Border", new XAttribute(SpreadsheetNS + "Position", "Top"),    new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")));
			if (right)
				borders.Add(new XElement(SpreadsheetNS + "Border", new XAttribute(SpreadsheetNS + "Position", "Right"),  new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")));
			if (bottom)
				borders.Add(new XElement(SpreadsheetNS + "Border", new XAttribute(SpreadsheetNS + "Position", "Bottom"), new XAttribute(SpreadsheetNS + "LineStyle", "Continuous"), new XAttribute(SpreadsheetNS + "Weight", "1")));
			if (left || top || right || bottom)
				xElement.Add(borders);
			return xElement;
		}
		public static XElement CreateWorksheet(this XElement xElement, string worksheetName)
		{
			var worksheet = new XElement(SpreadsheetNS + "Worksheet", new XAttribute(SpreadsheetNS + "Name", worksheetName));
			xElement.Add(worksheet);
			return worksheet;
		}
		public static XElement CreateTable(this XElement xWorksheet, IEnumerable<CellInfo> cells)
		{
			var xTable = new XElement(SpreadsheetNS + "Table",
				new XAttribute(ExcelNS + "FullColumns", "1"),
				new XAttribute(ExcelNS + "FullRows", "1"),
				new XAttribute(SpreadsheetNS + "DefaultRowHeight", "15"));

			xWorksheet.Add(xTable);

			foreach (var cell in cells)
			{
				var xCol = new XElement(SpreadsheetNS + "Column",
					new XAttribute(SpreadsheetNS + "AutoFitWidth", cell.Width != null ? 0 : 1));
				if (cell.Width != null)
					xCol.Add(new XAttribute(SpreadsheetNS + "Width", 135 * cell.Width.Value));
				xTable.Add(xCol);
			}

			return xTable;
		}
		public static XElement CreateRow(this XElement xTable)
		{
			var xRow = new XElement(SpreadsheetNS + "Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0));
			xTable.Add(xRow);
			return xRow;
		}
		public static XElement CreateCellTitleHead(this XElement xRow, string content, int mergeAcross = 0)
		{
			var xCell = new XElement(SpreadsheetNS + "Cell",
				new XAttribute(SpreadsheetNS + "StyleID", "s62"),
				new XAttribute(SpreadsheetNS + "MergeAcross", mergeAcross),
				new XElement(SpreadsheetNS + "Data",
					new XAttribute(SpreadsheetNS + "Type", "String"), content));
			xRow.Add(xCell);
			return xCell;
		}
		public static XElement CreateCellValueHead(this XElement xRow, string content, int mergeAcross = 0)
		{
			var xCell = new XElement(SpreadsheetNS + "Cell",
				new XAttribute(SpreadsheetNS + "StyleID", "s64"),
				new XAttribute(SpreadsheetNS + "MergeAcross", mergeAcross),
				new XElement(SpreadsheetNS + "Data",
					new XAttribute(SpreadsheetNS + "Type", "String"), content));
			xRow.Add(xCell);
			return xCell;
		}
		public static XElement WithDataCell(this XElement xRow, CellInfo cellInfo, string value)
		{

			if (value != null && value.Contains((char)0))
				throw new ArgumentOutOfRangeException(nameof(value), value, $@"Value should not contain 0x00; cellInfo.Key: {cellInfo.Key}");

			var style     = default(string);
			var typeValue = cellInfo.ExcelType;
			if (string.IsNullOrWhiteSpace(value) && cellInfo.WarnWhenEmpty)
			{
				style     = "s76";
				typeValue = "String";
			}
			else
			{
				switch (cellInfo.ExcelType)
				{
					case CellInfo.ExcelTypes.DateTime:
						style     = DateTimeCellStyle;
						typeValue = CellInfo.ExcelTypes.DateTime;
						break;
					case CellInfo.ExcelTypes.TimeSpan:
						style     = TimeSpanCellStyle;
						typeValue = CellInfo.ExcelTypes.DateTime;
						break;
					case CellInfo.ExcelTypes.Warning:
						style     = string.IsNullOrWhiteSpace(value) ? "s75" : "s76";
						typeValue = CellInfo.ExcelTypes.String;
						break;
					case CellInfo.ExcelTypes.Number:
						style     = NumberCellStyle;
						typeValue = CellInfo.ExcelTypes.Number;
						break;
					case CellInfo.ExcelTypes.HyperLink:
						style     = HyperLinkStyle;
						typeValue = CellInfo.ExcelTypes.String;
						break;
					default:
						style     = "s75";
						break;
				}
			}

			var xCell = new XElement(SpreadsheetNS + "Cell",
				new XAttribute(SpreadsheetNS + "StyleID", style));

			if (cellInfo.ExcelType == CellInfo.ExcelTypes.HyperLink)
				xCell.Add(new XAttribute(SpreadsheetNS + "HRef", value));

			if (!string.IsNullOrWhiteSpace(value))
				xCell.Add(
					new XElement(SpreadsheetNS + "Data",
						new XAttribute(SpreadsheetNS + "Type", typeValue),
						value));
			xRow.Add(xCell);
			return xRow;
		}
		public static XElement WithReportHeaderRow(this XElement xTable, IEnumerable<CellInfo> cells, string title, string value)
		{
			var colCount = cells?.Count() ?? 0;
			return xTable
				.CreateRow()
					.CreateCellTitleHead(title, 0)
					.Parent
					.CreateCellValueHead(value, 2 < colCount ? colCount - 2 : 0)
					.Parent
				.Parent;
		}
		public static XElement WithTableHeadRow(this XElement xTable, IEnumerable<CellInfo> cells, bool autoFilter = true)
		{
			if (cells == default)
				return xTable;

			var xRow = xTable
				.CreateRow()
				.WithContent(new XAttribute(SpreadsheetNS + "Height", "30"));

			foreach (var cellInfo in cells)
			{
				xRow.Add(
					new XElement(SpreadsheetNS + "Cell",
						new XAttribute(SpreadsheetNS + "StyleID", "s92"),
						new XElement(SpreadsheetNS + "Data",
							new XAttribute(SpreadsheetNS + "Type", "String"), cellInfo.Name ?? cellInfo.Key)));
			}

			if (autoFilter)
			{
				var rowCount = xTable.Elements(SpreadsheetNS + "Row")?.Count() ?? 0;
				xTable.AddAfterSelf(new XElement(ExcelNS + "AutoFilter",
					new XAttribute("Range", $"R{rowCount}C1:R{rowCount}C{cells.Count()}")));
			}

			return xTable;
		}
		public static XElement WithTableBodyRow(this XElement xTable, IEnumerable<CellInfo> cells, object row)
		{
			if (cells == default)
				return xTable;

			var xRow = xTable.CreateRow();
			foreach (var cellInfo in cells)
				xRow.WithDataCell(cellInfo, cellInfo.GetValue(row));

			return xTable;
		}
	}
}