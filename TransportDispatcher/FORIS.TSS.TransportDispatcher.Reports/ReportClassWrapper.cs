﻿using System;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class ReportClassWrapper : IReportClass
	{
		private readonly ReportClass _reportClass;
		public ReportClassWrapper()
		{
			_reportClass = new ReportClass();
		}
		public ReportClassWrapper(ReportClass reportClass)
		{
			if (reportClass == null)
				throw new ArgumentNullException(nameof(reportClass));

			_reportClass = reportClass;
		}
		public static implicit operator ReportClassWrapper(ReportClass reportClass)
		{
			return new ReportClassWrapper(reportClass);
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			ExportFormatType reportType;
			switch (formatType)
			{
				case ReportTypeEnum.Acrobat:
					reportType = ExportFormatType.PortableDocFormat;
					break;
				case ReportTypeEnum.Excel:
					var options = _reportClass.ExportOptions;
					options.ExportDestinationType = ExportDestinationType.DiskFile;
					options.ExportFormatType = ExportFormatType.Excel;
					options.DestinationOptions =
						new DiskFileDestinationOptions
						{
							DiskFileName = fileName
						};
					options.ExportFormatOptions = new ExcelFormatOptions {ShowGridLines = true};
					_reportClass.Export(options);
					return;
				case ReportTypeEnum.Html:
					reportType = ExportFormatType.HTML40;
					break;
				case ReportTypeEnum.Xml:
					reportType = ExportFormatType.Xml;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(formatType));
			}
			_reportClass.ExportToDisk(reportType, fileName);
		}
		public ExportOptions ExportOptions
		{
			get { return _reportClass.ExportOptions; }
		}
		public PrintOptions PrintOptions
		{
			get { return _reportClass.PrintOptions; }
		}
		public ReportDefinition ReportDefinition
		{
			get { return _reportClass.ReportDefinition; }
		}
		public ReportDocument ReportDocument
		{
			get { return _reportClass; }
		}
		public void Export()
		{
			_reportClass.Export();
		}
		public void PrintToPrinter(int nCopies, bool collated, int startPageN, int endPageN)
		{
			_reportClass.PrintToPrinter(nCopies, collated, startPageN, endPageN);
		}
		public void Dispose()
		{
			var reportClass = _reportClass;
			if (reportClass == null)
				return;
			_reportClass.Close();
			_reportClass.Dispose();
		}
		public void SetDataSource(DataSet dataSet)
		{
			_reportClass.SetDataSource(dataSet);
		}
	}
}