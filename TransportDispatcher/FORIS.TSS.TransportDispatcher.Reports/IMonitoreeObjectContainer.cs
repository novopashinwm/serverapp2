﻿using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public interface IMonitoreeObjectContainer
	{
		IdType MonitoreeObjectIdType { get; }
		int    MonitoreeObjectId     { get; }
	}
}