﻿using System;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public abstract class BaseReportExcelGenerator : BaseExcelGenerator
	{
		private   readonly ITssReport              _report;
		private   readonly ResourceStringContainer _strings;
		protected readonly ReportParameters        Parameters;
		protected BaseReportExcelGenerator(ITssReport report, ReportParameters parameters)
		{
			Parameters = parameters;
			_report    = report;
			_strings   = report.GetResourceStringContainer(Parameters.Culture);
		}
		/// <summary> Возвращает имя Листа (в терминах Excel) </summary>
		protected override string WorksheetName
		{
			get { return _strings["Report"]; }
		}

		/// <summary> Заполняет заголовок отчёта в таблице </summary>
		protected override void FillHeader()
		{
			var reportNameRow = new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
				CreateCellTitleHead(_strings["Report"]),
				CreateCellValueHead(_report.GetReportName(Parameters.Culture))
			);
			AddRow(reportNameRow);
		}
		protected void FillDateIntervalHeader()
		{
			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
				CreateCellTitleHead(_strings["dateFrom"]),
				CreateCellValueHead(Parameters.DateFrom.ToString(TimeHelper.DefaultTimeFormatUpToDays, Parameters.Culture))
			));
			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
				CreateCellTitleHead(_strings["dateTo"]),
				CreateCellValueHead(Parameters.DateTo.ToString(TimeHelper.DefaultTimeFormatUpToDays, Parameters.Culture))
			));
		}

		private const string DateTimeFormat = "yyyy-MM-dd\\THH:mm:ss\\Z";
		protected string ToString(DateTime? dateTime)
		{
			if (dateTime == null)
				return string.Empty;

			return Parameters.FromUtc(dateTime.Value).ToString(DateTimeFormat);
		}
		/// <summary> Базовая дата для отправки в Excel-Xml значений TimeStamp (передаются в виде даты) </summary>
		private static readonly DateTime ExcelTimeSpanBasicDateTime = new DateTime(1899, 12, 31, 00, 00, 00, DateTimeKind.Utc);
		/// <summary> Форматирование TimeSpan для передачи в Excel-Xml </summary>
		/// <param name="timeSpan"></param>
		/// <returns></returns>
		public static string FormatTimeSpan(TimeSpan? timeSpan)
		{
			if (timeSpan == null)
				return string.Empty;
			return FormatDateTime(ExcelTimeSpanBasicDateTime + timeSpan.Value);
		}
		/// <summary> Форматирование DateTime для передачи в Excel-Xml </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public static string FormatDateTime(DateTime? dateTime)
		{
			if (dateTime == null)
				return string.Empty;
			return XmlConvert.ToString(dateTime.Value,
				XmlDateTimeSerializationMode.Unspecified);
		}
	}
}