﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public static class ReportHelper
	{
		public const           string   ARIAL  = "Arial";
		public static readonly BaseFont ArialBaseFont;
		public const           string   Lucida = "Lucida Sans Unicode";
		public static readonly BaseFont LucidaBaseFont;
		static ReportHelper()
		{
			// Регистрация директорий со шрифтами, для отчетов iTextSharp (генерация PDF)
			FontFactory.RegisterDirectories();
			// Создать базовый шрифт Arial Regular
			ArialBaseFont  = FontFactory.GetFont(ARIAL, BaseFont.IDENTITY_H).BaseFont;
			// Создать базовый шрифт Lucida Sans Regular
			LucidaBaseFont = FontFactory.GetFont(Lucida, BaseFont.IDENTITY_H).BaseFont;
		}
		/// <summary>
		/// Export report to file
		/// </summary>
		/// <param name="crReportDocument">ReportDocument</param>
		/// <param name="exportPath">Export Path (physical path 
		/// on the disk were exported document will be stored on)</param>
		/// <param name="filename">File name (file name without 
		/// extension f.e. "MyReport1")</param>
		/// <param name="reportType"></param>
		/// < returns>returns true if export was succesfull</returns>
		public static string ExportReport(IReportClass crReportDocument, string exportPath, string filename, ReportTypeEnum reportType)
		{
			if (crReportDocument == null)
				throw new ArgumentNullException(nameof(crReportDocument));

			//pdf, xls, doc, rpt, htm

			var fileExt = GetExtension(reportType);

			//creating full report file name 
			//for example if the filename was "MyReport1"
			//and ExpType was "pdf", full file name will be "MyReport1.pdf"
			filename = filename + "." + fileExt;

			//creating storage directory if not exists
			if (!Directory.Exists(exportPath)) 
				Directory.CreateDirectory(exportPath);

			var crystalReport = crReportDocument as ReportClassWrapper;
			if (crystalReport != null)
				return ExportCrystalReport(exportPath, filename, crystalReport, reportType);

			var tempFileName = exportPath + filename;

			crReportDocument.ExportToDisk(reportType, tempFileName);
			return tempFileName;
		}

		public static string GetExtension(ReportTypeEnum reportType)
		{
			switch (reportType)
			{
				case ReportTypeEnum.Acrobat:        return "pdf";
				case ReportTypeEnum.Crystal:        return "rpt";
				case ReportTypeEnum.Excel:          return "xls";
				case ReportTypeEnum.Html:           return "htm";
				case ReportTypeEnum.RichTextFormat: return "rtf";
				case ReportTypeEnum.Word:           return "doc";
				case ReportTypeEnum.Xml:            return "xml";
				default:
					throw new ArgumentOutOfRangeException(nameof(reportType), reportType, @"Unable to resolve file extension for reportType provided");
			}
		}
		public static ReportTypeEnum GetReportType(string expType)
		{
			switch (expType)
			{
				case "rtf": return ReportTypeEnum.RichTextFormat;
				case "pdf": return ReportTypeEnum.Acrobat;
				case "doc": return ReportTypeEnum.Word;
				case "xls": return ReportTypeEnum.Excel;
				case "rpt": return ReportTypeEnum.Crystal;
				case "htm": return ReportTypeEnum.Html;
				case "xml": return ReportTypeEnum.Xml;
				default:
					throw new ArgumentOutOfRangeException(nameof(expType), expType, "Unknown export type setting");
			}
		}
		private static string ExportCrystalReport(string exportPath, string filename, ReportClassWrapper crystalReport, ReportTypeEnum reportType)
		{
			//creating new instance representing disk file destination 
			//options such as filename, export type etc.
			var crDiskFileDestinationOptions = new DiskFileDestinationOptions();
			ExportOptions crExportOptions = crystalReport.ExportOptions;

			switch (reportType)
			{
				case ReportTypeEnum.RichTextFormat:
					{
						//setting disk file name 
						crDiskFileDestinationOptions.DiskFileName =
							exportPath + filename;
						//setting destination type in our case disk file
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						//setuing export format type
						crExportOptions.ExportFormatType = ExportFormatType.RichText;
						//setting previously defined destination 
						//opions to our input report document
						crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
						break;
					}
					//NOTE following code is similar to previous, so I want comment it again
				case ReportTypeEnum.Acrobat:
					{
						crDiskFileDestinationOptions.DiskFileName =
							exportPath + filename;
						crExportOptions.DestinationOptions =
							crDiskFileDestinationOptions;
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						crExportOptions.ExportFormatType =
							ExportFormatType.PortableDocFormat;
						break;
					}
				case ReportTypeEnum.Word:
					{
						crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						crExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
						crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
						break;
					}
				case ReportTypeEnum.Excel:
					{
						crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						crExportOptions.ExportFormatType = ExportFormatType.Excel;
						crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
						break;
					}
				case ReportTypeEnum.Crystal:
					{
						crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						crExportOptions.ExportFormatType = ExportFormatType.CrystalReport;
						crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
						break;
					}
				case ReportTypeEnum.Html:
					{
						var html40Formatopts = new HTMLFormatOptions();
						crExportOptions.ExportDestinationType =
							ExportDestinationType.DiskFile;
						crExportOptions.ExportFormatType = ExportFormatType.HTML40;
						html40Formatopts.HTMLBaseFolderName = exportPath + filename;
						html40Formatopts.HTMLFileName = "HTML40.html";
						html40Formatopts.HTMLEnableSeparatedPages = true;
						html40Formatopts.HTMLHasPageNavigator = true;
						html40Formatopts.FirstPageNumber = 1;
						html40Formatopts.LastPageNumber = 3;
						crExportOptions.FormatOptions = html40Formatopts;
						break;
					}
				default:
					throw new ArgumentOutOfRangeException("reportType", reportType, "Unable to format crystal report");
			}
			//trying to export input report document, 
			//and if success returns true
			crystalReport.Export();
			return crDiskFileDestinationOptions.DiskFileName;
		}
		public static string GetPathUrl(string applicationPath, int vehicleId, DateTime @from, DateTime @to)
		{
			return
				string.Format(
					"///{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=5"
					, applicationPath
					, vehicleId
					, @from.ToString("dd.MM.yyyy+HH:mm:ss")
					, @to.ToString("dd.MM.yyyy+HH:mm:ss")
					);
		}
		public static void WritePathLink(this XmlWriter writer,
			ReportParameters reportParameters,
			int              vehicleId,
			DateTime         @from,
			DateTime         @to,
			string           text)
		{
			writer.WriteStartElement("a");
			writer.WriteAttributeString("href", GetPathUrl(reportParameters.ApplicationPath, vehicleId, @from, @to));
			writer.WriteAttributeString("title", GetResourceStringContainer(reportParameters.Culture)["ShowOnMap"]);
			writer.WriteString(text);
			writer.WriteEndElement();
		}
		/// <summary> Возвращает контейнер строковых ресурсов для перевода строк общий для всех отчетов </summary>
		/// <param name="culture"> Культура, для которой следует вернуть контейнер строковых ресурсов </param>
		/// <returns> Контейнер строковых ресурсов </returns>
		public static ResourceStringContainer GetResourceStringContainer(CultureInfo culture)
		{
			return new ResourceStringContainer(
				culture,
				typeof(ReportHelper).Namespace + ".Strings",
				typeof(ReportHelper).Assembly);
		}
	}
}