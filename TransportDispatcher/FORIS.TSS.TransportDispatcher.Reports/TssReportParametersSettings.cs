﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class TssReportParametersSettings
	{
		public int             OperatorId     { get; set; }
		public int?            DepartmentId   { get; set; }
		public DepartmentType? DepartmentType { get; set; }
	}
}