﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public interface IReportClass : IDisposable
	{
		void ExportToDisk(ReportTypeEnum formatType, string fileName);
	}
}