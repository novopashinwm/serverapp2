﻿using System;
using System.Collections.Generic;
using FORIS.TSS.TransportDispatcher.Reports;

namespace FORIS.TSS.BusinessLogic.Reports
{
	public interface IReportRepository
	{
		TssReportBase           this[Guid guid] { get; }
		IEnumerable<ITssReport> Reports         { get; }
	}
}