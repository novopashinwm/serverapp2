﻿using System;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class CellInfo
	{
		public CellInfo(float width = 1.0f, bool warnWhenEmpty = false)
		{
			Width         = width;
			WarnWhenEmpty = warnWhenEmpty;
		}
		public static class ExcelTypes
		{
			public const string String    = "String";
			public const string Number    = "Number";
			public const string DateTime  = "DateTime";
			public const string TimeSpan  = "TimeSpan";
			public const string Warning   = "Warning";
			public const string HyperLink = "HyperLink";
		}
		/// <summary> Ключ для идентификации </summary>
		public string Key           { get; set; }
		/// <summary> Название, которое будет отображаться в шапке таблицы </summary>
		public string Name          { get; set; }
		/// <summary> Выделить цветом если пустое </summary>
		public bool   WarnWhenEmpty { get; }
		/// <summary> Ширина столбца (если null, то автоподбор в Excel, если в коде <code>Width = 10F/25F, // 10 в Excel</code>) </summary>
		/// <remarks> Максимальная ширина столбца Excel 255 (255F/25F) </remarks>
		public float? Width         { get; set; }
		/// <summary> Указатель на функцию получения значения в виде строки </summary>
		public Func<object, string> GetValue;
		private string _excelType;
		/// <summary> Тип колонки, для Excel (но большего для рисования в другие форматы и не нужно) </summary>
		public string ExcelType
		{
			get { return string.IsNullOrEmpty(_excelType) ? ExcelTypes.String : _excelType; }
			set { _excelType = value; }
		}
	}
}