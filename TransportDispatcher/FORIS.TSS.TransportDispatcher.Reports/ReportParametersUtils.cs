﻿using System.Globalization;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public static class ReportParametersUtils
	{
		public const string SpeedInput                  = "SpeedInput";
		public const string ShortIfSingleVehicle        = "ShortIfSingleVehicle";
		public const string ZonePicker                  = "ZonePicker";
		public const string ZonesPicker                 = "ZonesPicker";
		public const string ZoneGroupPicker             = "ZoneGroupPicker";
		public const string VehiclePicker               = "VehiclePicker";
		public const string VehiclesPicker              = "VehiclesPicker";
		public const string VehicleGroupPicker          = "VehicleGroupPicker";
		public const string SensorPicker                = "SensorPicker";
		public const string Boolean                     = "IsDetailed";
		public const string IntervalPicker              = "IntervalPicker";
		public const string ParkingIntervalPicker       = "ParkingIntervalPicker";
		public const string FuelSpendStandardTypePicker = "FuelSpendStandardTypePicker";
		public const string FuelSensorTypePicker        = "FuelSensorTypePicker";
		public const string FuelCostsPicker             = "FuelCostsPicker";
		public const string WorkingHoursInterval        = "WorkingHoursInterval";
		public const string DatesPicker                 = "DatesPicker";
		public const string WeekDaysPicker              = "WeekDaysPicker";
		public const string MileageFilePicker           = "MileageFilePicker";
		public const string DateTimeFromToPicker        = "DateTimeFromToPicker";
	}
}