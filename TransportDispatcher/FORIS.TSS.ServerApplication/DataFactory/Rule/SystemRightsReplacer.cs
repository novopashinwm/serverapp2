﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication.DataFactory.Rule
{
	public class SystemRightsReplacer
	{
		private readonly Dictionary<DepartmentType, Dictionary<IdType, List<SystemRightReplacer>>> _replacedSystemRights = new Dictionary<DepartmentType, Dictionary<IdType, List<SystemRightReplacer>>>();
		public IEnumerable<SystemRight> GetSystemRights(DepartmentType departmentType, IdType type)
		{
			return _replacedSystemRights.ContainsKey(departmentType)
				? _replacedSystemRights[departmentType].ContainsKey(type)
					? _replacedSystemRights[departmentType][type].SelectMany(item => item.Rights)
					: Enumerable.Empty<SystemRight>()
				: Enumerable.Empty<SystemRight>();
		}
		public IEnumerable<SystemRight> GetReplacedRights(DepartmentType departmentType, IdType type)
		{
			return _replacedSystemRights.ContainsKey(departmentType)
				? _replacedSystemRights[departmentType].ContainsKey(type)
					? _replacedSystemRights[departmentType][type].Select(item => item.Replaced)
					: Enumerable.Empty<SystemRight>()
				: Enumerable.Empty<SystemRight>();
		}
		public SystemRightReplacer GetRightReplacer(DepartmentType departmentType, IdType type, SystemRight right)
		{
			return _replacedSystemRights.ContainsKey(departmentType)
				? _replacedSystemRights[departmentType].ContainsKey(type)
					? _replacedSystemRights[departmentType][type].FirstOrDefault(item => item.Replaced == right)
					: null
				: null;
		}
		public SystemRightsReplacer()
		{
		}
		public SystemRightsReplacer(IEnumerable<SystemRightReplacer> rules)
		{
			AddRules(rules);
		}
		private void ValidateRule(SystemRightReplacer rule, Dictionary<DepartmentType, Dictionary<IdType, List<SystemRight>>> validator)
		{
			foreach (var departmentType in rule.DepartmentTypes)
			{
				if (!validator.ContainsKey(departmentType))
				{
					validator.Add(departmentType, new Dictionary<IdType, List<SystemRight>>());
				}

				foreach (var target in rule.Targets)
				{
					if (!validator[departmentType].ContainsKey(target))
					{
						validator[departmentType][target] = new List<SystemRight>();
					}

					foreach (var usedRight in rule.Rights)
					{
						if (validator[departmentType][target].Contains(usedRight))
						{
							throw new ArgumentException("Access rules have not unique system rights");
						}

						validator[departmentType][target].Add(usedRight);
					}
				}
			}
		}
		private void AddRule(SystemRightReplacer rule, Dictionary<DepartmentType, Dictionary<IdType, List<SystemRight>>> validator)
		{
			ValidateRule(rule, validator);
			foreach (var departmentType in rule.DepartmentTypes)
			{
				if (!_replacedSystemRights.ContainsKey(departmentType))
				{
					_replacedSystemRights.Add(departmentType, new Dictionary<IdType, List<SystemRightReplacer>>());
				}

				foreach (var target in rule.Targets)
				{
					if (!_replacedSystemRights[departmentType].ContainsKey(target))
					{
						_replacedSystemRights[departmentType][target] = new List<SystemRightReplacer>();
					}

					_replacedSystemRights[departmentType][target].Add(rule);
				}
			}
		}
		private void AddRules(IEnumerable<SystemRightReplacer> rules)
		{
			var validator = new Dictionary<DepartmentType, Dictionary<IdType, List<SystemRight>>>();
			foreach (var rule in rules)
			{
				AddRule(rule, validator);
			}
		}
	}
}