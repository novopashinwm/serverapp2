﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication.DataFactory.Rule
{
	[Serializable]
	public class SystemRightReplacer
	{
		public SystemRight      Replaced        { get; private set; }
		public DepartmentType[] DepartmentTypes { get; private set; }
		public IdType[]         Targets         { get; private set; }
		public SystemRight[]    Rights          { get; private set; }
		public static SystemRightReplacer ForVehicle(SystemRight right, DepartmentType departmentType)
		{
			return new SystemRightReplacer(IdType.Vehicle, departmentType, right);
		}
		public static SystemRightReplacer ForVehicle(SystemRight right, DepartmentType departmentType, SystemRight[] rights)
		{
			return new SystemRightReplacer(IdType.Vehicle, right, departmentType, rights);
		}
		public static SystemRightReplacer ForVehicle(SystemRight right, DepartmentType[] departmentTypes)
		{
			return new SystemRightReplacer(IdType.Vehicle, departmentTypes, right);
		}
		public static SystemRightReplacer ForVehicle(SystemRight right, DepartmentType[] departmentTypes, SystemRight[] rights)
		{
			return new SystemRightReplacer(IdType.Vehicle, right, departmentTypes, rights);
		}
		public static SystemRightReplacer ForVehicleAndVehicleGroup(SystemRight right, DepartmentType departmentType)
		{
			return new SystemRightReplacer(new[] { IdType.Vehicle , IdType.VehicleGroup }, departmentType, right);
		}
		public static SystemRightReplacer ForVehicleAndVehicleGroup(SystemRight right, DepartmentType departmentType, SystemRight[] rights)
		{
			return new SystemRightReplacer(new[] { IdType.Vehicle, IdType.VehicleGroup }, right, departmentType, rights);
		}
		public static SystemRightReplacer ForVehicleAndVehicleGroup(SystemRight right, DepartmentType[] departmentTypes)
		{
			return new SystemRightReplacer(new[] { IdType.Vehicle, IdType.VehicleGroup }, departmentTypes, right);
		}
		public static SystemRightReplacer ForVehicleAndVehicleGroup(SystemRight right, DepartmentType[] departmentTypes, SystemRight[] rights)
		{
			return new SystemRightReplacer(new[] { IdType.Vehicle, IdType.VehicleGroup }, right, departmentTypes, rights);
		}
		public static SystemRightReplacer ForVehicleGroup(SystemRight right, DepartmentType departmentType)
		{
			return new SystemRightReplacer(IdType.VehicleGroup, departmentType, right);
		}
		public static SystemRightReplacer ForVehicleGroup(SystemRight right, DepartmentType departmentType, SystemRight[] rights)
		{
			return new SystemRightReplacer(IdType.VehicleGroup, right, departmentType, rights);
		}
		public static SystemRightReplacer ForVehicleGroup(SystemRight right, DepartmentType[] departmentTypes)
		{
			return new SystemRightReplacer(IdType.VehicleGroup, departmentTypes, right);
		}
		public static SystemRightReplacer ForVehicleGroup(SystemRight right, DepartmentType[] departmentTypes, SystemRight[] rights)
		{
			return new SystemRightReplacer(IdType.VehicleGroup, right, departmentTypes, rights);
		}
		private SystemRightReplacer(IdType target, DepartmentType departmentType, SystemRight right)
			: this(target, right, departmentType, new[] { right })
		{
		}
		private SystemRightReplacer(IdType target, DepartmentType[] departmentTypes, SystemRight right)
			: this(target, right, departmentTypes, new[] { right })
		{
		}
		private SystemRightReplacer(IdType target, SystemRight replacedRight, DepartmentType departmentType, IEnumerable<SystemRight> rights)
			: this(target, replacedRight, new[] { departmentType }, rights)
		{
		}
		private SystemRightReplacer(IdType target, SystemRight replacedRight, DepartmentType[] departmentTypes, IEnumerable<SystemRight> rights)
			: this(new[] { target }, replacedRight, departmentTypes, rights)
		{
		}
		private SystemRightReplacer(IEnumerable<IdType> targets, DepartmentType departmentType, SystemRight right)
			: this(targets, right, departmentType, new[] { right })
		{
		}
		private SystemRightReplacer(IEnumerable<IdType> targets, DepartmentType[] departmentTypes, SystemRight right)
			: this(targets, right, departmentTypes, new[] { right })
		{
		}
		private SystemRightReplacer(IEnumerable<IdType> targets, SystemRight replacedRight, DepartmentType departmentType, IEnumerable<SystemRight> rights)
			: this(targets, replacedRight, new[] { departmentType }, rights)
		{
		}
		private SystemRightReplacer(IEnumerable<IdType> targets, SystemRight replacedRight, DepartmentType[] departmentTypes, IEnumerable<SystemRight> rights)
		{
			Targets         = targets.Distinct().ToArray();
			Replaced        = replacedRight;
			DepartmentTypes = departmentTypes;
			Rights          = new HashSet<SystemRight>(rights).ToArray();
		}
	}
}