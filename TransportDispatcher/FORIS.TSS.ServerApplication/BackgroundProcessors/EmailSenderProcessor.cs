﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Mail;
using FORIS.TSS.ServerApplication.SmsService;
using Attachment = System.Net.Mail.Attachment;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	sealed class EmailSenderProcessor : MessageSender
	{
		protected override MessageProcessingResult SendMessage(Entities entities, MessageTuple messageTuple)
		{
			try
			{
				using (var mailmessage = new MailMessage())
				{
					mailmessage.Subject    = messageTuple.Message.SUBJECT;
					mailmessage.Body       = messageTuple.Message.BODY;
					mailmessage.IsBodyHtml = true;

					foreach (var messageContact in messageTuple.Message.Contacts.Where(
						mc => mc.Contact.Type == (int)ContactType.Email))
					{
						var address = GetAddress(messageContact.Contact);
						if (address == null)
							continue;

						var messageContactType = (MessageContactType)messageContact.Type;
						switch (messageContactType)
						{
							case MessageContactType.ReplyTo:
								mailmessage.ReplyToList.Add(address);
								break;
							case MessageContactType.Destination:
								mailmessage.To.Add(address);
								break;
							case MessageContactType.Copy:
								mailmessage.CC.Add(address);
								break;
							case MessageContactType.HiddenCopy:
								mailmessage.Bcc.Add(address);
								break;
							default:
								Trace.TraceWarning("{0}: MessageContactType.{1} is not supported", this, messageContactType);
								break;
						}
					}

					if (mailmessage.To.Count == 0)
						return MessageProcessingResult.InvalidDestinationAddress;

					var pictureAttachment = GetPictureAttacment(messageTuple.Message);
					if (pictureAttachment != null)
						mailmessage.Attachments.Add(pictureAttachment);

					var reportAttachment = GetReportAttachment(messageTuple.Message);
					if (reportAttachment != null)
						mailmessage.Attachments.Add(reportAttachment);

					foreach (var attachment in GetAttachments(messageTuple.Message))
					{
						mailmessage.Attachments.Add(attachment);
					}

					MailService.SendEmail(mailmessage);
					return MessageProcessingResult.Sent;
				}
			}
			catch (SmtpException ex)
			{
				if (ex.StatusCode == SmtpStatusCode.SyntaxError)
				{
					Trace.TraceError("Unable to send email [{0}] to address {1}, email will be canceled, status code {2}, error: {3}",
						messageTuple.Id, messageTuple.Destination, ex.StatusCode, ex.Message);
					//Адрес указан с ошибкой
					return MessageProcessingResult.InvalidDestinationAddress;
				}

				Trace.TraceError("Unable to send email [{0}] to address {1}, email will be canceled, status code {2}, error: {3}",
					messageTuple.Id, messageTuple.Destination, ex.StatusCode, ex);
				return MessageProcessingResult.Error;
			}
		}
		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int)ContactType.Email;
		}
		private MailAddress GetAddress(Contact contact)
		{
			MailAddress result;
			try
			{
				result = new MailAddress(contact.Value);
			}
			catch (FormatException)
			{
				++contact.LockedCount;
				contact.LockedUntil = Forever;
				return null;
			}

			return result;
		}
		private IEnumerable<Attachment> GetAttachments(MESSAGE message)
		{
			if (!message.Attachment.IsLoaded)
				message.Attachment.Load();

			foreach (var storedAttachment in message.Attachment)
			{
				var memoryStream = new MemoryStream(storedAttachment.Data);

				yield return new Attachment(memoryStream, storedAttachment.Name);
			}
		}
		private Attachment GetPictureAttacment(MESSAGE message)
		{
			if (!message.MESSAGE_FIELD.IsLoaded)
				message.MESSAGE_FIELD.Load();
			foreach (var field in message.MESSAGE_FIELD)
			{
				if (!field.MESSAGE_TEMPLATE_FIELDReference.IsLoaded)
					field.MESSAGE_TEMPLATE_FIELDReference.Load();
			}

			var logTimeString =
				message.MESSAGE_FIELD.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.PictureLogTime)
					   .Select(f => f.CONTENT)
					   .FirstOrDefault();

			int logTime;
			if (!int.TryParse(logTimeString, out logTime))
				return null;

			var vehicleIDString =
				message.MESSAGE_FIELD.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.PictureVehicleID)
					   .Select(f => f.CONTENT)
					   .FirstOrDefault();
			int vehicleID;
			if (!int.TryParse(vehicleIDString, out vehicleID))
				return null;

			var fileName =
				message.MESSAGE_FIELD.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.PictureName)
					   .Select(f => f.CONTENT)
					   .FirstOrDefault();
			if (string.IsNullOrWhiteSpace(fileName))
				return null;

			var pictureBytes = Server.Instance().GetPictureThumbnail(vehicleID, logTime);
			if (pictureBytes == null)
				return null;

			var memoryStream = new MemoryStream(pictureBytes);

			var pictureAttacment = new Attachment(memoryStream, fileName);

			return pictureAttacment;
		}
		private Attachment GetReportAttachment(MESSAGE message)
		{
			if (!message.MESSAGE_FIELD.IsLoaded)
				message.MESSAGE_FIELD.Load();
			foreach (var field in message.MESSAGE_FIELD)
			{
				if (!field.MESSAGE_TEMPLATE_FIELDReference.IsLoaded)
					field.MESSAGE_TEMPLATE_FIELDReference.Load();
			}

			var filePath =
				message.MESSAGE_FIELD.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.ReportFilePath)
					   .Select(f => f.CONTENT)
					   .FirstOrDefault();
			if (string.IsNullOrWhiteSpace(filePath))
				return null;

			var reportName =
				message.MESSAGE_FIELD.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.ReportName)
					   .Select(f => f.CONTENT)
					   .FirstOrDefault();
			if (string.IsNullOrWhiteSpace(reportName))
				return null;

			var reportBytes = File.ReadAllBytes(filePath);
			var memoryStream = new MemoryStream(reportBytes);

			var reportAttachment = new Attachment(memoryStream, reportName);
			return reportAttachment;
		}
		public EmailSenderProcessor()
			: base(MessageDestinationType.Email, 100)
		{
		}
		public override void Initialize()
		{
		}
		private static readonly object InstanceLock = new object();
		private static EmailSenderProcessor _instance;
		public static EmailSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new EmailSenderProcessor());
			}
		}
	}
}