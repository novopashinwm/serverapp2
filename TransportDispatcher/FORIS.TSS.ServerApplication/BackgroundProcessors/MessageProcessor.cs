﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	sealed class MessageProcessor : BackgroundProcessorSingleton<MessageProcessor>
	{
		private List<MessageSender> _senders;
		private readonly bool _enabled;

		public MessageProcessor() : base(TimeSpan.FromSeconds(1))
		{
			_enabled = string.Equals(ConfigurationManager.AppSettings["MessageSenderFactory.Cycle.Enabled"], "true", StringComparison.OrdinalIgnoreCase);

			BeforeStart += delegate
				{
					InitializeSenders();
					foreach (var sender in _senders)
					{
						try
						{
							if (sender.Enabled)
							{
								sender.Initialize();
								sender.Start();
							}
						}
						catch (Exception ex)
						{
							Trace.TraceError("{0} Error in message sender {1}: {2}", this, sender, ex);
						}
					}
				};

			AfterStop += delegate
				{
					foreach (var sender in _senders)
					{
						if (sender.Enabled)
						{
							sender.Stop();
						}
					}
				};
		}

		private void InitializeSenders()
		{
			if (_senders == null)
				_senders = new List<MessageSender>();
			/// Phone
			_senders.Add(SmppMessageSenderProcessor.Instance);
			_senders.Add(RedHatHttpSmsSenderProcessor.Instance);
			_senders.Add(ModemSenderProcessor.Instance);
			/// Android
			_senders.Add(AndroidSenderProcessor.Instance);
			/// Apple
			_senders.Add(AppleSenderProcessor.Instance);
			_senders.Add(AppleDebugSenderProcessor.Instance);
			/// Email
			_senders.Add(EmailSenderProcessor.Instance);

			foreach (var sender in _senders)
			{
				if (sender.Enabled)
					PendingMessagesToWaiting(sender.MessageDestinationType);
			}
		}

		protected override bool Do()
		{
			if (!_enabled)
				return false;

			using (var entities = new Entities())
			{
				var messages = entities.MESSAGE
					.Include(MESSAGE.ContactsIncludePath)
					.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
					.Where(m =>
						m.DestinationType_ID == (int)MessageDestinationType.Unknown &&
						m.STATUS             == (int)MessageState.Wait)
					.OrderBy(m => m.TIME)
					.Take(100)
					.ToList();

				if (messages.Count == 0)
					return false;

				foreach (var message in messages)
				{
					var gatewayId = entities.GetMessageGatewayId(message);

					if (gatewayId != null)
					{
						message.STATUS             = (int)MessageState.Wait;
						message.DestinationType_ID = gatewayId.Value;
						continue;
					}

					var contact = message.Contacts
						.Where(mc => mc.Type == (int)MessageContactType.Destination)
						.Select(mc => mc.Contact)
						.FirstOrDefault();

					var approriateSenders = contact != null ? _senders.Where(s => s.CanSendTo(contact)) : null;
					MessageSender approriateSender = null;
					if (approriateSenders != null)
						approriateSender = approriateSenders.FirstOrDefault(s => s.Enabled);

					if (approriateSender == null)
					{
						message.ProcessingResult = (int)MessageProcessingResult.NoApproriateProcessor;
						message.STATUS           = (int)MessageState.Done;
						continue;
					}

					message.STATUS             = (int)MessageState.Wait;
					message.DestinationType_ID = (int)approriateSender.MessageDestinationType;
				}

				entities.SaveChanges();
				return true;
			}

		}

		public static void PendingMessagesToWaiting(MessageDestinationType messageDestinationType)
		{
			while (true)
			{
				using (var entities = new Entities())
				{
					var messages = entities.MESSAGE
						.Where(m =>
							m.STATUS             == (int)MessageState.Pending &&
							m.DestinationType_ID == (int)messageDestinationType)
						.OrderBy(m => m.TIME)
						.Take(100)
						.ToList();

					if (messages.Count == 0)
						break;

					foreach (var message in messages)
					{
						message.STATUS = (int)MessageState.Wait;
					}

					entities.SaveChanges();
				}
			}
		}
	}
}