﻿using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary>
	/// Разбирает очередь исходящих сообщений на Apple и отправляет сообщения через Apple
	/// </summary>
	sealed class AppleDebugSenderProcessor : AppleSenderProcessor
	{
		public AppleDebugSenderProcessor() : base(
			typeof(AppleDebugSenderProcessor).GetAppSettingAsBoolean("Enabled"),
			MessageDestinationType.AppleiOSDebug,
			ContactType.iOsDebug,
			true)
		{
		}

		private static readonly object InstanceLock = new object();
		private static AppleDebugSenderProcessor _instance;
		/// <summary> Экземпляр класса </summary>
		public new static AppleDebugSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new AppleDebugSenderProcessor());
			}
		}
	}
}