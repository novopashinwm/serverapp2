﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Exceptions;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary> Фоновый обработчик команд </summary>
	public sealed class CommandProcessor : BackgroundProcessor
	{
		private readonly ConcurrentQueue<NotifyEventArgs>  _notifications = new ConcurrentQueue<NotifyEventArgs>();
		private readonly ConcurrentQueue<ReceiveEventArgs> _receiveEvents = new ConcurrentQueue<ReceiveEventArgs>();
		private readonly CommandProcessorHelper            _helper;
		private readonly Server                            _server;
		private CommandProcessor()
		{
			_helper = new CommandProcessorHelper(SendCommandToTerminalServer);
			_server = Server.Instance();

			BeforeStart += SubscribeToServerEvents;
			AfterStop   += UnsubscribeFromServerEvents;
		}
		private CmdResult? SendCommandToTerminalServer(IStdCommand cmd)
		{
			try
			{
				_server.SendCommand(cmd);
				return null;
			}
			catch (DeviceDisconnectedException ex)
			{
				Trace.TraceError("{0}", ex);
				return CmdResult.DeviceNotFoundInReceversList;
			}
			catch (CommandSendingFailureException ex)
			{
				Trace.TraceError("{0}", ex);
				return CmdResult.SendCommandDeviceException;
			}
			catch (TerminalServerConnectionException ex)
			{
				Trace.TraceError("{0}", ex);
				return CmdResult.TerminalManagerIsNull;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return CmdResult.SendCommandDeviceException;
			}
		}
		private void SubscribeToServerEvents(object sender, EventArgs e)
		{
			_server.NotifyEvent  += OnNotified;
			_server.ReceiveEvent += TerminalManagerReceiveEvent;
		}
		private void UnsubscribeFromServerEvents(object sender, EventArgs e)
		{
			_server.NotifyEvent  -= OnNotified;
			_server.ReceiveEvent -= TerminalManagerReceiveEvent;
		}
		private void OnNotified(NotifyEventArgs args)
		{
			_notifications.Enqueue(args);
		}
		protected override bool Do()
		{
			bool somethingWasProcessed = ProcessEvents();

			using (var entities = new Entities())
			{
				_helper.ProcessLateCommands(entities);
				var commands = _helper.CheckoutCommandsForProcessing(entities);
				if (commands.Count == 0)
					return somethingWasProcessed;
				
				foreach (var command in commands)
				{
					using (new Stopwatcher("CommandProcessorHelper.ProcessCommand"))
					_helper.ProcessCommand(entities, command);
				}
				entities.SaveChanges();
			}

			return somethingWasProcessed;
		}
		private TimeSpan _maxEventProcessingTime = TimeSpan.Zero;
		private static readonly TimeSpan CriticalEventProcessingTime = TimeSpan.FromMinutes(1);
		private bool ProcessEvents()
		{
			var stopWatch = new Stopwatch();
			stopWatch.Start();

			var somethingWasProcessed = false;

			try
			{
				NotifyEventArgs notifyEventArgs;
				while (_notifications.TryDequeue(out notifyEventArgs))
				{
					_helper.ProcessNotifyEvent(notifyEventArgs);
					CheckEventProcessingTime(stopWatch.Elapsed);
					somethingWasProcessed = true;
				}

				ReceiveEventArgs receiveEventArgs;
				while (_receiveEvents.TryDequeue(out receiveEventArgs))
				{
					_helper.ProcessReceiveEvent(receiveEventArgs);
					CheckEventProcessingTime(stopWatch.Elapsed);
					somethingWasProcessed = true;
				}

				return somethingWasProcessed;
			}
			finally
			{
				stopWatch.Stop();
				if (CriticalEventProcessingTime < stopWatch.Elapsed)
					Trace.TraceWarning("{0}.ProcessEvents was too long: {1}", GetType(), stopWatch.Elapsed);
			}
		}
		private void CheckEventProcessingTime(TimeSpan elapsed)
		{
			//Время выполнения увеличилось более чем вдвое по сравнению с предыдущим разом
			if (_maxEventProcessingTime < elapsed - _maxEventProcessingTime)
			{
				Trace.TraceWarning("{0}.Peak event processing time is {1}", GetType(), elapsed);
				_maxEventProcessingTime = elapsed;
			}
		}
		private void TerminalManagerReceiveEvent(ReceiveEventArgs args)
		{
			_receiveEvents.Enqueue(args);
		}
		private static readonly object InstanceLock = new object();
		private static CommandProcessor _instance;
		/// <summary> Экземпляр синглтона </summary>
		public static CommandProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new CommandProcessor());
			}
		}
	}
}