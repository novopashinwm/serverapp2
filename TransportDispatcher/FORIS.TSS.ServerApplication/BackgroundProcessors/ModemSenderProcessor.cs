﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Exceptions;
using FORIS.TSS.ServerApplication.SmsService;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	sealed class ModemSenderProcessor : MessageSender
	{
		private static ModemSenderProcessor _instance;

		private static readonly object Lock = new object();

		public static ModemSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock(Lock)
				{
					Thread.MemoryBarrier();
					if(_instance == null)
						_instance = new ModemSenderProcessor();
					
				}
				return _instance;
			}
		}

		private ModemSenderProcessor()
			: base(MessageDestinationType.Modem, 1, ConfigHelper.GetAppSettingAsBoolean("ModemMessageSender.Enabled"))
		{
		}

		public override void Initialize()
		{
			if(Server.Instance().TerminalManager == null)
				throw new TerminalServerConnectionException("Cannot connect to terminal server");
		}

		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{
			try
			{
				var msisdn = messageTuple.Destination.Value;

				Server.Instance().TerminalManager.SendSMS(msisdn, messageTuple.Body);
				return MessageProcessingResult.Sent;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}: {1}", this, ex);
				return MessageProcessingResult.Error;
			}
		}

		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int) ContactType.Phone;
		}
	}
}