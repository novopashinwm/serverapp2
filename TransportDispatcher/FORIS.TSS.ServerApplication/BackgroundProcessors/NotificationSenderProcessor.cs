﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.EntityModel;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	public class NotificationSenderProcessor : BackgroundProcessorSingleton<NotificationSenderProcessor>
	{
		private readonly TimingCache<Constant, int> _cacheConstants = new TimingCache<Constant, int>(TimeSpan.FromSeconds(10),
			constant =>
			{
				using (var entities = new Entities())
				{
					var value = entities.GetConstantAsInt(constant);
					var result = value ?? 120;
					return result;
				}
			});

		public int OutDateSeconds
		{
			get { return _cacheConstants[Constant.OutdateSeconds]; }
		}

		public NotificationSenderProcessor() : base(TimeSpan.FromSeconds(1))
		{
			BeforeStart += (sender, args) =>
			{
				using (var entities = new Entities())
					PendingMessageToWaiting(entities);
			};
		}

		protected override bool Do()
		{
			using (var entities = new Entities())
			{
				var somethingWasChanged = ResetOutdatedMessages(entities);
				var messages            = PendingNotifications(entities);
				foreach (var message in messages)
				{
					SendNotification(entities, message);
					entities.SaveUnsavedChanges();
				}
				return somethingWasChanged || messages.Count != 0;
			}
		}

		/// <summary> Переводим сообщения, которые не были выполнены в прошлый раз в состояние ожидания </summary>
		/// <param name="entities"></param>
		private void PendingMessageToWaiting(Entities entities)
		{
			var utcNow = DateTime.UtcNow;
			var notifications = entities.MESSAGE
				.Where(m =>
					m.DestinationType_ID == (int)MessageDestinationType.Notification &&
					m.STATUS == (int)MessageState.Pending &&
					m.TIME <= utcNow
				);
			foreach (var notification in notifications)
			{
				notification.STATUS = (int)MessageState.Wait;
			}

			entities.SaveUnsavedChanges();
		}

		/// <summary> Пометка уведомлений для обработки, которые были начаты обрабатываться, но не были обработаны </summary>
		/// <param name="entities"></param>
		/// <returns></returns>
		private List<MESSAGE> PendingNotifications(Entities entities)
		{
			using (var transaction = new Transaction())
			{
				var messages = entities.MESSAGE
					.Where(m =>
						m.DestinationType_ID == (int)MessageDestinationType.Notification &&
						m.STATUS             == (int)MessageState.Wait)
					.OrderBy(m => m.TIME)
					.Take(100)
					.ToList();

				if (messages.Any())
				{
					foreach (var message in messages)
						message.STATUS = (int)MessageState.Pending;

					entities.SaveUnsavedChanges();
				}

				transaction.Complete();
				return messages;
			}
		}

		/// <summary> Помечает просроченные сообщения для выполнения еще раз </summary>
		private bool ResetOutdatedMessages(Entities entities)
		{
			//TODO: исправить на хранимую процедуру
			var time = DateTime.UtcNow.AddSeconds(-OutDateSeconds);
			var messages = entities.MESSAGE
				.Where(m =>
					m.STATUS             == (int)MessageState.Pending &&
					m.DestinationType_ID == (int)MessageDestinationType.Notification &&
					m.ProcessingResult   == (int)MessageProcessingResult.PushSent &&
					m.TIME <= time)
				.ToList();
			if (!messages.Any())
				return false;
			foreach (var message in messages)
			{
				message.STATUS = (int)MessageState.Wait;
			}

			entities.SaveUnsavedChanges();
			return true;
		}

		/// <summary> Отправка уведомления </summary>
		/// <param name="entities"></param>
		/// <param name="message"></param>
		private void SendNotification(Entities entities, MESSAGE message)
		{
			var notifications = new List<MESSAGE>();
			var pushWasSent = false;
			var smsWasSent  = false;
			if (message.ProcessingResult != (int)MessageProcessingResult.PushSent)
			{
				notifications.AddRange(SendPushNotification(entities, message));
				pushWasSent  = notifications.Any();
				message.TIME = DateTime.UtcNow;
			}

			if (!pushWasSent)
			{
				notifications.AddRange(SendSmsNotification(entities, message));
				smsWasSent     = notifications.Any();
				message.STATUS = (int)MessageState.Done;
			}

			if (!notifications.Any())
				message.STATUS = (int)MessageState.Done;

			foreach (var notification in notifications)
				notification.GROUP = message.MESSAGE_ID;

			if (pushWasSent)
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.PushSent, message.MESSAGE_ID);
			else if (smsWasSent)
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Sent, message.MESSAGE_ID);
			else
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.ContactDoesNotExist, message.MESSAGE_ID);
		}

		/// <summary> Отправка SMS-уведомления </summary>
		private IEnumerable<MESSAGE> SendSmsNotification(Entities entities, MESSAGE message)
		{
			var destination = message.Contacts
				.Where(c => c.Type == (int)MessageContactType.Destination)
				.Select(c => c.Contact)
				.FirstOrDefault();
			if (destination == null)
				return Enumerable.Empty<MESSAGE>();

			var source = message.Contacts
				.Where(c => c.Type == (int)MessageContactType.Source)
				.Select(c => c.Contact)
				.FirstOrDefault();

			var smsNotifications = source != null
				? entities.SendSmsToUser(destination, source, message.MESSAGE_TEMPLATE, message.BODY)
				: entities.SendSmsToUser(destination,         message.MESSAGE_TEMPLATE, message.BODY);
			return smsNotifications;
		}

		/// <summary> Отправка push-уведомления </summary>
		/// <param name="entities"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		private IEnumerable<MESSAGE> SendPushNotification(Entities entities, MESSAGE message)
		{
			int temp;
			var pushOperatorId = int.TryParse(message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.OperatorId), out temp) ? temp : (int?)null;
			if (!pushOperatorId.HasValue)
				return Enumerable.Empty<MESSAGE>();

			var vehicleId = int.TryParse(message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.VehicleID), out temp) ? temp : (int?)null;
			var vehicleName = string.Empty;
			if(vehicleId.HasValue)
				vehicleName = Server.Instance().GetVehicleName(pushOperatorId, vehicleId.Value);
			var msisdn = message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.Msisdn);
			var subject = string.IsNullOrEmpty(message.SUBJECT) ? vehicleName : message.SUBJECT;
			var body = message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.PushMessage) ?? message.BODY;
			if (string.IsNullOrEmpty(body))
				return Enumerable.Empty<MESSAGE>();

			var pushOperator = entities.OPERATOR.First(o => o.OPERATOR_ID == pushOperatorId);
			var pushNotifications = entities.CreateAppNotification(pushOperator.OPERATOR_ID, subject, body);
			var notificationTemplate = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.GenericNotification);
			foreach (var pushNotification in pushNotifications)
			{
				pushNotification.MESSAGE_TEMPLATE = notificationTemplate;
				if(vehicleId.HasValue)
					entities.AddMessageField(pushNotification, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId.Value);
				if(!string.IsNullOrEmpty(msisdn))
					entities.AddMessageField(pushNotification, MESSAGE_TEMPLATE_FIELD.Msisdn, msisdn);
			}

			return pushNotifications;
		}
	}
}