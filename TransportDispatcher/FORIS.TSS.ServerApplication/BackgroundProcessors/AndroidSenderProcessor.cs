﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request;
using FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Response;
using FORIS.TSS.ServerApplication.SmsService;
using Newtonsoft.Json;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary> Разбирает очередь исходящих сообщений на Android и отправляет сообщения через Google Cloud Messaging (GCM) </summary>
	sealed class AndroidSenderProcessor : MessageSender
	{
		private AndroidSenderProcessor()
			: base(MessageDestinationType.Android, 5)
		{
		}

		private string   _baseUrl;
		private string   _authKey;
		private TimeSpan _timeOut;
		public override void Initialize()
		{
			_baseUrl = GetType().GetAppSettingAsString  ("Url")           ?? "https://fcm.googleapis.com/fcm/send";
			_authKey = GetType().GetAppSettingAsString  ("Authorization") ?? string.Empty;
			_timeOut = GetType().GetAppSettingAsTimeSpan("Timeout")       ?? TimeSpan.FromSeconds(5);
		}

		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{

			var appId =
				context.AppClient
					.Where(app => app.Contact_ID == messageTuple.Destination.ID)
					.Select(app => app.AppId)
					.FirstOrDefault();

			if (string.IsNullOrWhiteSpace(appId))
				return MessageProcessingResult.MissingAppId;

			var request = CreateFcmRequest(context, messageTuple, appId);
			if (request.Status != FcmRequestStatus.OK)
				return MessageProcessingResult.Error;

			var requestJson = JsonConvert.SerializeObject(request);
			return SendMessageViaFcm(context, messageTuple, requestJson);

			//TODO: Использовать логику из Messages.Message
			var data = GetMessageAttributes(context, messageTuple);

			data.Add("appId", appId);

			var pushType = GetPushTypeByTemplateName(messageTuple.TemplateName);

			switch (pushType)
			{
				case PushType.PositionRequest:
					if (messageTuple.Message.Owner_Operator_ID != null)
						data.Add("operator_id", messageTuple.Message.Owner_Operator_ID.Value);
					break;
				case PushType.ProposeFriendship:
					if (!data.ContainsKey(MESSAGE_TEMPLATE_FIELD.FriendMsisdn))
					{
						TraceError("Field {0} not found", MESSAGE_TEMPLATE_FIELD.FriendMsisdn);
						return MessageProcessingResult.Error;
					}
					break;
				case PushType.DataUpdate:
					if (!data.ContainsKey(MESSAGE_TEMPLATE_FIELD.DataType))
					{
						TraceError("Field {0} not found", MESSAGE_TEMPLATE_FIELD.DataType);
						return MessageProcessingResult.Error;
					}
					if (!data.ContainsKey(MESSAGE_TEMPLATE_FIELD.PushIsVisible))
						data.Add(MESSAGE_TEMPLATE_FIELD.PushIsVisible, false);
					break;
			}

			data.Add("id", messageTuple.Id);
			data.Add("type", pushType);

			if (messageTuple.Message.SUBJECT != null)
				data["text"] = messageTuple.Message.SUBJECT;
			else if (messageTuple.Body != null)
				data["text"] = messageTuple.Body;

			if (messageTuple.Body != null)
				data.Add("description", messageTuple.Body);

			var attributes = GetMessageAttributes(context, messageTuple);
			object vehicleId;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.VehicleID, out vehicleId))
				data.Add("vehicle_id", vehicleId);

			var json = JsonConvert.SerializeObject(new Dictionary<string, object>
			{
				{"data", data},
				{"registration_ids", new[] {messageTuple.Destination.Value}}
			});

			return SendMessageViaFcm(context, messageTuple, json);
		}
		/// <summary> Создать класс запроса к Google Cloud Messaging (FCM) </summary>
		/// <param name="context"> Контекст БД </param>
		/// <param name="messageTuple"> Параметры сообщения </param>
		/// <returns></returns>
		private FcmRequest<FcmAndroidLocalizedNotification> CreateFcmRequest(Entities context, MessageTuple messageTuple, string appId)
		{
			var result = new FcmRequest<FcmAndroidLocalizedNotification>()
			{
				Status = FcmRequestStatus.OK
			};
			var notification = new FcmAndroidLocalizedNotification
			{
				// Показываемые в центре уведомлений Android, только для видимых сообщений
				Title = messageTuple?.Message?.SUBJECT ?? messageTuple?.Body,
				Body  = messageTuple?.Body
			};

			var attributes = GetMessageAttributes(context, messageTuple);

			var pushIsVisibleVal = default(bool?);
			var pushIsVisibleFld = attributes
				.FirstOrDefault(a => a.Key == MESSAGE_TEMPLATE_FIELD.PushIsVisible)
				.Value;
			if (null != pushIsVisibleFld)
			{
				var pushIsVisibleObj = default(bool);
				if (bool.TryParse(pushIsVisibleFld.ToString(), out pushIsVisibleObj))
					pushIsVisibleVal = pushIsVisibleObj;
			}
			pushIsVisibleVal = pushIsVisibleVal.HasValue && pushIsVisibleVal.Value;

			// Добавляем appId
			attributes["appId"] = appId;

			// Добавляем идентификатор сообщения
			attributes["id"] = new object[] { messageTuple.Message.MESSAGE_ID };

			// Добавляем текст сообщения
			if (!string.IsNullOrWhiteSpace(messageTuple.Body))
			{
				object messageText;
				if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.MessageText, out messageText))
					attributes["text"] = new object[] { messageTuple.Body };
			}
			// Добавляем дату
			object date;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.Date, out date) && date is DateTime)
				attributes["date"] = new object[] { (DateTime)date };

			// Добавляем идентификатор объекта наблюдения
			object vehicleId;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.VehicleID, out vehicleId))
				attributes["vid"] = new[] { vehicleId };

			// Определяем тип push-уведомления
			var pushType = GetPushTypeByTemplateName(messageTuple.TemplateName);
			// Добавляем параметры в соответствии с типом push-уведомления
			switch (pushType)
			{
				case PushType.Notification:
					// Если явно не указано, то push видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue ? pushIsVisibleVal.Value : true;
					TryAddField(attributes, MESSAGE_TEMPLATE_FIELD.Msisdn, "msisdn");
					notification.Sound = "chime";
					break;
				case PushType.PositionRequest:
					// Если явно не указано, то push не видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue && pushIsVisibleVal.Value;
					TryAddFriendMsisdn(attributes);
					if (messageTuple.Message.Owner_Operator_ID != null)
						attributes["oid"] = new object[] { messageTuple.Message.Owner_Operator_ID.Value };
					// Звук для запросов местоположения не должно быть, если запрос невидимый
					if (pushIsVisibleVal.Value)
						notification.Sound = "chime";
					break;
				case PushType.ProposeFriendship:
					// Если явно не указано, то push видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue ? pushIsVisibleVal.Value : true;
					if (!TryAddFriendMsisdn(attributes))
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					notification.Sound = "chime";
					break;
				case PushType.Alarm:
					// Если явно не указано, то push видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue ? pushIsVisibleVal.Value : true;
					TryAddLastPosition(attributes);
					notification.Sound = "alarm.mp3";
					break;
				case PushType.FriendWasRegistered:
					// Если явно не указано, то push видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue ? pushIsVisibleVal.Value : true;
					if (!TryAddFriendMsisdn(attributes))
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					notification.Sound = "chime";
					break;
				case PushType.DataUpdate:
					// Если явно не указано, то push не видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue && pushIsVisibleVal.Value;
					object dataTypeObject;
					if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.DataType, out dataTypeObject))
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					var dataTypeString = dataTypeObject as string;
					if (dataTypeString == null)
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					UpdatedDataType dataType;
					if (!Enum.TryParse(dataTypeString, out dataType))
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					attributes["dataType"] = (int)dataType;
					break;
				case PushType.RequestResult:
					// Если явно не указано, то push видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue ? pushIsVisibleVal.Value : true;
					if (!TryAddFriendMsisdn(attributes))
					{
						result.Status = FcmRequestStatus.INVALID_REQUEST;
						return result;
					}
					//Идентификатор команды - необязательный параметр, т.к. RequestResult также приходит тому, кого запросили
					TryAddFriendCommandId(attributes);
					TryAddLastPosition(attributes);
					notification.Sound = "chime";
					break;
				default:
					// Если явно не указано, то push не видимый
					pushIsVisibleVal = pushIsVisibleVal.HasValue && pushIsVisibleVal.Value;
					break;
			}

			attributes["id"]   = messageTuple.Id;
			attributes["type"] = pushType.ToString();

			if (messageTuple.Message.SUBJECT != null)
				attributes["text"] = messageTuple.Message.SUBJECT;
			else if (messageTuple.Body != null)
				attributes["text"] = messageTuple.Body;

			if (messageTuple.Body != null)
				attributes["description"] = messageTuple.Body;

			// Прореживание не заполненных атрибутов
			result.Data = attributes
				.Where(k => !string.IsNullOrWhiteSpace(k.Key) && null != k.Value)
				.ToDictionary(k => k.Key, k => k.Value);

			// Если явно не указано, то push не видимый
			pushIsVisibleVal = pushIsVisibleVal.HasValue && pushIsVisibleVal.Value;
			// Только для видимых сообщений добавляем эту секцию
			result.Notification = pushIsVisibleVal.Value ? notification : null;
			// Добавляем сам атрибут видимости
			attributes[MESSAGE_TEMPLATE_FIELD.PushIsVisible] = pushIsVisibleVal.Value ? 1 : 0;

			result.RegistrationIDs = new[] { messageTuple.Destination.Value };
			return result;
		}
		private void TryAddLastPosition(Dictionary<string, object> attributes)
		{
			object vehicleIdObject;
			if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.VehicleID, out vehicleIdObject))
				return;
			var vehicleId = vehicleIdObject as int?;
			if (vehicleId == null)
				return;

			var mu = Server.Instance().GetLastPosition(vehicleId.Value);
			if (mu == null || !mu.ValidPosition)
				return;

			attributes["time"] = mu.CorrectTime;
			attributes["lat"]  = mu.Latitude;
			attributes["lng"]  = mu.Longitude;
			if (null != mu.Speed)
				attributes["speed"] = mu.Speed;
			if (null != mu.Course)
				attributes["course"] = mu.Course;
			if (null != mu.Radius)
				attributes["radius"] = mu.Radius;

			//TODO: отправлять адрес
		}
		private bool TryAddFriendMsisdn(Dictionary<string, object> attributes)
		{
			return TryAddField(attributes, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, "msisdn");
		}
		private bool TryAddField(Dictionary<string, object> attributes, string messageFieldName, string payloadFieldName)
		{
			object friendMsisdn;
			if (!attributes.TryGetValue(messageFieldName, out friendMsisdn))
				return false;

			if (friendMsisdn == null)
				return false;

			attributes[payloadFieldName] = new object[] { friendMsisdn };
			return true;
		}
		private bool TryAddFriendCommandId(Dictionary<string, object> attributes)
		{
			object @object;
			if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.CommandId, out @object))
			{
				TraceError("Field {0} not found at {1}", MESSAGE_TEMPLATE_FIELD.CommandId, Environment.StackTrace);
				return false;
			}

			if (@object == null)
			{
				TraceError("Field {0} cannot be null at {1}", MESSAGE_TEMPLATE_FIELD.CommandId, Environment.StackTrace);
				return false;
			}

			var stringValue = @object as string;
			if (stringValue == null)
			{
				TraceError(
					"Unable to cast field {0} (type={1}) as string at {2}",
					MESSAGE_TEMPLATE_FIELD.CommandId, @object.GetType(), Environment.StackTrace);
				return false;
			}

			int intValue;
			if (!int.TryParse(stringValue, out intValue))
			{
				TraceError(
					"Field {0} cannot be parsed (value={1}) as int at {2}",
					MESSAGE_TEMPLATE_FIELD.CommandId, stringValue, Environment.StackTrace);
				return false;
			}

			attributes["cmdId"] = new object[] { intValue };
			return true;
		}
		/// <summary> Отправить сообщение с помощью механизма Google Cloud Messaging Fcm </summary>
		/// <param name="context">Контекст БД.</param>
		/// <param name="messageTuple">Параметры сообщения.</param>
		/// <param name="json"></param>
		/// <returns></returns>
		private MessageProcessingResult SendMessageViaFcm(Entities context, MessageTuple messageTuple, string json)
		{
			using (var client = new HttpClient())
			{
				client.BaseAddress = new Uri(_baseUrl);
				client.Timeout     = _timeOut;
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				if (!string.IsNullOrWhiteSpace(_authKey))
					client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization",
						_authKey.ToLowerInvariant().StartsWith("key=") ? _authKey : $"key={_authKey}");
				var bodyRequestContent = new StringContent(
					json,
					Encoding.UTF8,
					"application/json");

				TraceInformation("Sending message {0} through FCM to {1}, request is:\n{2}",
					messageTuple.Id, messageTuple.Destination, json);

				using (var response = client.PostAsync("", bodyRequestContent).Result)
				{
					try
					{
						if (!response.IsSuccessStatusCode && null != response.Headers.RetryAfter && response.Headers.RetryAfter.Delta.HasValue)
						{
							Throttled(response.Headers.RetryAfter.Delta.Value);
							return MessageProcessingResult.Throttled;
						}
						response.EnsureSuccessStatusCode();
						if (response.IsSuccessStatusCode)
						{
							var fcmResponse = default(FcmResponse);
							using (var content = response.Content)
							{
								var jsonResponseContent = content.ReadAsStringAsync().Result;
								try
								{
									fcmResponse = Newtonsoft.Json.JsonConvert
										.DeserializeObject<FcmResponse>(jsonResponseContent, new Newtonsoft.Json.JsonSerializerSettings() {  });
								}
								catch (IOException ex)
								{
									TraceWarning("Sending message {0} through FCM to {1} was unsuccessful, response is:\n{2}\n{3}\n{4}",
										messageTuple.Id, messageTuple.Destination, jsonResponseContent, ex.Message, ex.StackTrace);
									return MessageProcessingResult.HttpError;
								}
								catch (Newtonsoft.Json.JsonException ex)
								{
									TraceWarning("Sending message {0} through FCM to {1} was unsuccessful, response is:\n{2}\n{3}\n{4}",
										messageTuple.Id, messageTuple.Destination, jsonResponseContent, ex.Message, ex.StackTrace);
									return MessageProcessingResult.HttpError;
								}

								// Новый идентификатор пришел
								if (1 == fcmResponse.CanonicalIDs && 1 == fcmResponse.Results?.Count())
								{
									var newRegistrationId = fcmResponse.Results
										.FirstOrDefault()?.RegistrationId;
									context.UpdateContactForAppClient(messageTuple.Destination, newRegistrationId);
								}

								if (1 != fcmResponse.Success)
								{
									if (0 != fcmResponse.Results?.Count())
									{
										var error = fcmResponse.Results
											.FirstOrDefault()?.Error;

										if (error == FcmResponseResultError.MissingRegistration ||
											error == FcmResponseResultError.InvalidRegistration ||
											error == FcmResponseResultError.NotRegistered       ||
											error == FcmResponseResultError.MismatchSenderId)
										{
											TraceError("Sending message {0} through FCM to {1} was unsuccessful, response is:\n{2}",
												messageTuple.Id, messageTuple.Destination, jsonResponseContent);
											messageTuple.Destination.LockedUntil = Forever;
											return MessageProcessingResult.InvalidDestinationAddress;
										}
									}
									TraceError("Sending message {0} through FCM to {1} was unsuccessful, response is:\n{2}",
										messageTuple.Id, messageTuple.Destination, jsonResponseContent);
									return MessageProcessingResult.Error;
								}

								TraceInformation("Sending message {0} through FCM to {1}, response is:\n{2}",
									messageTuple.Id, messageTuple.Destination, jsonResponseContent);
							}
						}
					}
					catch (HttpRequestException ex)
					{
						TraceError("Sending message {0} through FCM to {1} was unsuccessful:\n{2}\n{3}",
							messageTuple.Id, messageTuple.Destination, ex.Message, ex.StackTrace);
						return MessageProcessingResult.HttpError;
					}
				}
				return MessageProcessingResult.Sent;
			}
		}

		private static readonly object InstanceLock = new object();
		private static AndroidSenderProcessor _instance;

		/// <summary> Экземпляр класса </summary>
		public static AndroidSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new AndroidSenderProcessor());
			}
		}
		/// <summary> Проверка возможности отправки контакту. </summary>
		/// <param name="contact"></param>
		/// <returns></returns>
		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int) ContactType.Android;
		}
	}
}