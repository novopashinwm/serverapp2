﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.SmsService;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary> Разбирает очередь исходящих сообщений на Android и отправляет сообщения через Google Cloud Messaging </summary>
	internal class AppleSenderProcessor : MessageSenderWithLock
	{
		enum SendingResultType
		{
			LockDeviceId,
			Sent,
			Failed
		}
		class SendingResult
		{
			public int?   MessageId;
			public string DeviceId;
			public SendingResultType Code;
		}
		private readonly ConcurrentQueue<SendingResult> _resultQueue = new ConcurrentQueue<SendingResult>();
		private readonly ContactType                    _contactType;
		private readonly bool                           _sandbox;

		public AppleSenderProcessor()
			: this(
				typeof(AppleSenderProcessor).GetAppSettingAsBoolean("Enabled"),
				MessageDestinationType.AppleiOS,
				ContactType.Apple,
				typeof(AppleSenderProcessor).GetAppSettingAsBoolean("Sandbox"))
		{
		}
		protected AppleSenderProcessor(bool enabled, MessageDestinationType destinationType, ContactType contactType, bool sandbox)
			: base(destinationType, 1, enabled)
		{
			_contactType = contactType;
			_sandbox     = sandbox;
		}
		public override void Initialize()
		{
			if (!Enabled)
				return;
			var p12File     = GetType().GetAppSettingAsString("CertFile");
			var p12Password = GetType().GetAppSettingAsString("CertPassword");

			if (!File.Exists(p12File))
			{
				TraceInformation("File does not exists: {0}", p12File);
				return;
			}

			_service = new PushBroker();

			_service.OnDeviceSubscriptionExpired += ServiceOnDeviceSubscriptionExpired;
			_service.OnNotificationFailed        += ServiceOnNotificationFailed;
			_service.OnNotificationSent          += ServiceOnNotificationSent;
			_service.OnServiceException          += ServiceOnServiceException;
			_service.OnChannelException          += ServiceOnChannelException;

			var pushChannelSettings = new ApplePushChannelSettings(!_sandbox, p12File, p12Password)
			{
				FeedbackIntervalMinutes = GetType().GetAppSettingAsInt32("FeedbackIntervalMinutes", 0),
				ConnectionTimeout       = 5000
			};
			_service.RegisterAppleService(pushChannelSettings) ;
		}
		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{
			//TODO: Использовать логику из Messages.Message
			var attributes = GetMessageAttributes(context, messageTuple);

			var payload = new AppleNotificationPayload(messageTuple.Body);
			payload.AddCustom("id", new object[] { messageTuple.Message.MESSAGE_ID });

			var appId =
				context.AppClient
					.Where(app => app.Contact_ID == messageTuple.Destination.ID)
					.Select(app => app.AppId)
					.FirstOrDefault();

			if (string.IsNullOrWhiteSpace(appId))
				return MessageProcessingResult.MissingAppId;

			if (!string.IsNullOrWhiteSpace(messageTuple.Body))
			{
				object messageText;
				if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.MessageText, out messageText))
					payload.AddCustom("text", new object[] {messageText as string});
				else
					payload.AddCustom("text", new object[] { messageTuple.Body });
			}

			object date;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.Date, out date) && date is DateTime)
				payload.AddCustom("date", new object[] {(DateTime)date});

			payload.AddCustom("appId", new object[] { appId });

			var templateName = messageTuple.Message.MESSAGE_TEMPLATE != null ? messageTuple.Message.MESSAGE_TEMPLATE.NAME : null;

			object vehicleId;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.VehicleID, out vehicleId))
				payload.AddCustom("vid", new[] { vehicleId });
			
			object pushIsVisible;
			bool pushIsVisibleAdded = false;
			if (attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.PushIsVisible, out pushIsVisible))
			{
				payload.AddCustom(MESSAGE_TEMPLATE_FIELD.PushIsVisible, "true".Equals(pushIsVisible) ? 1 : 0);
				pushIsVisibleAdded = true;
			}

			payload.ContentAvailable = 1;

			PushType pushType = GetPushTypeByTemplateName(templateName);

			switch (pushType)
			{
				case PushType.Notification:
					TryAddField(attributes, payload, MESSAGE_TEMPLATE_FIELD.Msisdn, "msisdn");
					payload.Sound = "chime";
					break;
				case PushType.PositionRequest:
					TryAddFriendMsisdn(attributes, payload);
					if (messageTuple.Message.Owner_Operator_ID != null)
						payload.AddCustom("oid", new object[] { messageTuple.Message.Owner_Operator_ID.Value });
					//Звука для запросов местоположения не должно быть, если запрос невидимый
					if ("true".Equals(pushIsVisible))
						payload.Sound = "chime";
					//А в общем случае должно быть в настройках
					break;
				case PushType.ProposeFriendship:
					if (!TryAddFriendMsisdn(attributes, payload))
						return MessageProcessingResult.Error;
					payload.Sound = "chime";
					break;
				case PushType.Alarm:
					payload.Sound = "alarm.mp3";
					TryAddPosition(attributes, payload);
					break;
				case PushType.FriendWasRegistered:
					if (!TryAddFriendMsisdn(attributes, payload))
						return MessageProcessingResult.Error;
					payload.Sound = "chime";
					break;
				case PushType.DataUpdate:
					object dataTypeObject;
					if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.DataType, out dataTypeObject))
						return MessageProcessingResult.Error;
					var dataTypeString = dataTypeObject as string;
					if (dataTypeString == null)
						return MessageProcessingResult.Error;
					UpdatedDataType dataType;
					if (!Enum.TryParse(dataTypeString, out dataType))
						return MessageProcessingResult.Error;
					payload.AddCustom("dataType", (int)dataType);
					if (!pushIsVisibleAdded)
						payload.AddCustom(MESSAGE_TEMPLATE_FIELD.PushIsVisible, 0);
						
					break;
				case PushType.RequestResult:
					if (!TryAddFriendMsisdn(attributes, payload))
						return MessageProcessingResult.Error;
					payload.Sound = "chime";
					//Идентификатор команды - необязательный параметр, т.к. RequestResult также приходит тому, кого запросили
					TryAddFriendCommandId(attributes, payload);
					TryAddPosition(attributes, payload);
					break;
			}

			payload.AddCustom("type", new object[] { (int)pushType });

			var notification = new AppleNotification(messageTuple.Destination.Value, payload) {Tag = messageTuple.Id};

			TraceInformation("Message through apple cloud to {0}: {1}", messageTuple.Destination.Value, payload);

			_service.QueueNotification(notification);

			return MessageProcessingResult.Queued;
		}
		private void TryAddPosition(Dictionary<string, object> attributes, AppleNotificationPayload payload)
		{
			object vehicleIdObject;
			if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.VehicleID, out vehicleIdObject))
				return;
			var vehicleId = vehicleIdObject as int?;
			if (vehicleId == null)
				return;

			var mu = Server.Instance().GetLastPosition(vehicleId.Value);
			if (mu == null || !mu.ValidPosition)
				return;

			payload.AddCustom("time", mu.CorrectTime);
			payload.AddCustom("lat", mu.Latitude);
			payload.AddCustom("lng", mu.Longitude);
			if (mu.Speed != null)
				payload.AddCustom("speed", mu.Speed);
			if (mu.Course != null)
				payload.AddCustom("course", mu.Course);
			if (mu.Radius != null)
				payload.AddCustom("radius", mu.Radius);

			//TODO: отправлять адрес
		}
		private bool TryAddFriendMsisdn(Dictionary<string, object> attributes, AppleNotificationPayload payload)
		{
			return TryAddField(attributes, payload, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, "msisdn");
		}
		private bool TryAddField(Dictionary<string, object> attributes, AppleNotificationPayload payload, string messageFieldName, string payloadFieldName)
		{
			object friendMsisdn;
			if (!attributes.TryGetValue(messageFieldName, out friendMsisdn))
				return false;

			if (friendMsisdn == null)
				return false;

			payload.AddCustom(payloadFieldName, new object[] {friendMsisdn});
			return true;
		}
		private bool TryAddFriendCommandId(Dictionary<string, object> attributes, AppleNotificationPayload payload)
		{
			object @object;
			if (!attributes.TryGetValue(MESSAGE_TEMPLATE_FIELD.CommandId, out @object))
			{
				TraceError("Field {0} not found at {1}", MESSAGE_TEMPLATE_FIELD.CommandId, Environment.StackTrace);
				return false;
			}

			if (@object == null)
			{
				TraceError("Field {0} cannot be null at {1}", MESSAGE_TEMPLATE_FIELD.CommandId, Environment.StackTrace);
				return false;
			}

			var stringValue = @object as string;
			if (stringValue == null)
			{
				TraceError(
					"Unable to cast field {0} (type={1}) as string at {2}",
					MESSAGE_TEMPLATE_FIELD.CommandId, @object.GetType(), Environment.StackTrace);
				return false;
			}

			int intValue;
			if (!int.TryParse(stringValue, out intValue))
			{
				TraceError(
					"Field {0} cannot be parsed (value={1}) as int at {2}", 
					MESSAGE_TEMPLATE_FIELD.CommandId, stringValue, Environment.StackTrace);
				return false;
			}

			payload.AddCustom("cmdId", new object[] {intValue});
			return true;
		}
		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int)_contactType;
		}
		private PushBroker _service;
		private static readonly object InstanceLock = new object();
		private static AppleSenderProcessor _instance;
		/// <summary> Экземпляр класса </summary>
		public static AppleSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new AppleSenderProcessor());
			}
		}
		protected override bool Do()
		{
			var somethingWasDone = base.Do();

			if (ProcessSendingResults())
				somethingWasDone = true;

			if (ResendUnsentMessages())
				somethingWasDone = true;

			return somethingWasDone;
		}
		private bool ResendUnsentMessages()
		{
			if (!ConfigHelper.GetAppSettingAsBoolean("AppleSenderProcessor.ResendUnsentMessages"))
				return false;

			if (MessageDestinationType != MessageDestinationType.AppleiOSDebug)
				return false;

			var deadLine = DateTime.UtcNow.AddMinutes(-1);
			var oldest = DateTime.UtcNow.AddDays(-1);
			using (var entities = new Entities())
			{
				var messages =
					entities
						.MESSAGE
						.Where(m =>
							m.DestinationType_ID == (int)MessageDestinationType &&
							m.STATUS == (int) MessageState.Done &&
							m.ProcessingResult == (int) MessageProcessingResult.Sent &&
							m.TIME < deadLine &&
							m.TIME > oldest
						)
						.Take(100)
						.ToList();

				if (messages.Count == 0)
					return false;

				foreach (var message in messages)
				{
					message.STATUS = (int) MessageState.Wait;
					message.ProcessingResult = (int) MessageProcessingResult.Received;
					message.TIME = DateTime.UtcNow;
				}
				entities.SaveChanges();
			}

			return true;
		}

		#region Service Events

		private void ServiceOnChannelException(object sender, IPushChannel pushchannel, Exception error)
		{
			TraceError("{0}: {1}", pushchannel, error);
		}
		private void ServiceOnServiceException(object sender, Exception error)
		{
			TraceError("{0}: {1}", sender, error);
		}
		private void ServiceOnNotificationSent(object sender, INotification notification)
		{
			EnqueueNotification(notification, SendingResultType.Sent);
		}
		private void ServiceOnNotificationFailed(object sender, INotification notification, Exception error)
		{
			TraceInformation(
				"Notification failed, tag = {0}, notification.IsValidRegistrationId() = {1}, error: {2}",
				notification.Tag, notification.IsValidDeviceRegistrationId(), error);

			EnqueueNotification(notification, SendingResultType.Failed);
		}
		private void ServiceOnDeviceSubscriptionExpired(object sender, string expiredsubscriptionid, DateTime expirationdateutc, INotification notification)
		{
			TraceInformation("DeviceId expired, deviceId = {0}, expirationdateutc = {1}", expiredsubscriptionid, expirationdateutc);
			
			EnqueueExpiredDeviceId(notification, expiredsubscriptionid);
		}

		#endregion

		private bool ProcessSendingResults()
		{
			if (_resultQueue.IsEmpty)
				return false;

			using (var entities = new Entities())
			{
				SendingResult result;
				while (_resultQueue.TryDequeue(out result))
				{
					if (result.Code == SendingResultType.LockDeviceId)
					{
						var contact =
							entities.Contact
									.FirstOrDefault(
										c =>
										c.Value == result.DeviceId &&
										c.Type == (int)_contactType);

						if (contact != null)
						{
							contact.LockedUntil = DateTime.UtcNow.Add(LockSpan);
							contact.LockedCount++;
						}
						else
							TraceError("ProcessSendingResults: unable to find contact by deviceId: {0}",
									   result.DeviceId);
					}

					if (result.MessageId == null)
					{
						//Уведомление от Feedback Service только с DeviceToken
						continue;
					}

					var message = entities.MESSAGE.FirstOrDefault(m => m.MESSAGE_ID == result.MessageId.Value);
					if (message == null)
					{
						TraceError("ProcessSendingResults: unable to find message by id: {0}", result.MessageId.Value);
						continue;
					}

					message.TIME = DateTime.UtcNow;
					switch (result.Code)
					{
						case SendingResultType.Sent:
							Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Sent, message.MESSAGE_ID);
							message.STATUS = (int) MessageState.Done;
							break;
						case SendingResultType.Failed:
							ProcessMessageError(message);
							break;
						case SendingResultType.LockDeviceId:
							Server.Instance().SetMessageProcessingResult(MessageProcessingResult.DestinationContactIsLocked, message.MESSAGE_ID);
							message.STATUS = (int) MessageState.Done;
							break;
					}
				}

				entities.SaveChanges();

				return true;
			}
		}
		private void EnqueueNotification(INotification notification, SendingResultType result)
		{
			var appleNotification = (AppleNotification) notification;
			var messageId = (int) appleNotification.Tag;
			var deviceToken = appleNotification.DeviceToken;
			EnqueueNotificationResult(messageId, deviceToken, result);
		}
		private void EnqueueExpiredDeviceId(INotification notification, string deviceId)
		{
			var messageId = notification != null ? (int?) notification.Tag : null;
			EnqueueNotificationResult(messageId, deviceId, SendingResultType.LockDeviceId);
		}
		private void EnqueueNotificationResult(int? messageId, string deviceToken, SendingResultType result)
		{
			var sendingResult = new SendingResult
				{
					MessageId = messageId,
					DeviceId = deviceToken,
					Code = result,
				};
			_resultQueue.Enqueue(sendingResult);
		}
	}
}