﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	internal sealed partial class RuleProcessor
	{
		private class QuestionAnswerer
		{
			private readonly List<int>                                           _logTimes;
			private readonly ILookup<int, int>                                   _geoZones;
			private readonly Dictionary<int, CoreServerDatabaseManager.Position> _positions;
			private readonly int[]                                               _positionLogTimes;
			private readonly ILookup<SensorLegend, CONTROLLER_SENSOR_MAP>        _sensorMaps;
			private readonly Dictionary<int, int>                                _speeds;
			private readonly int[]                                               _speedLogTimes;
			private readonly Dictionary<int, InputSample>                        _inputSamples;
			private readonly HashSet<int>                                        _stopLogTimes;

			public QuestionAnswerer(
				IEnumerable<InputLogRecord>                             inputValuesForPosition,
				IEnumerable<GeoZoneForPosition>                         geoZonesForPosition,
				IEnumerable<CoreServerDatabaseManager.Position>         positions,
				IEnumerable<CoreServerDatabaseManager.SpeedForPosition> speeds,
				IEnumerable<CONTROLLER_SENSOR_MAP>                      vehicleSensorMap,
				IEnumerable<int>                                        newLogTimes,
				IEnumerable<int>                                        stopLogTimes,
				int?                                                    lastLogTime)
			{
				var inputLogRecordLookup = inputValuesForPosition.ToLookup(item => item.Number);
				_inputSamples = new Dictionary<int, InputSample>();
				foreach (var grouping in inputLogRecordLookup)
					_inputSamples.Add(grouping.Key,
						new InputSample(
							inputLogRecordLookup[grouping.Key]
								.Select(inputLogRecord =>
									new InputSample.InputRecord(
										inputLogRecord.LogTime,
										inputLogRecord.Value)
								)
								.ToList()
						)
					);

				_geoZones  = geoZonesForPosition.ToLookup(item => item.LogTime, item => item.ZoneId);
				_positions = positions.ToDictionary(item => item.LogTime);
				_positionLogTimes = _positions.Keys.ToArray();
				Array.Sort(_positionLogTimes);
				_speeds = speeds.ToDictionary(item => item.LogTime, item => item.Speed);
				_speedLogTimes = _speeds.Keys.ToArray();
				Array.Sort(_speedLogTimes);
				_sensorMaps = vehicleSensorMap.ToLookup(item => (SensorLegend)item.CONTROLLER_SENSOR_LEGEND.Number);

				_stopLogTimes = new HashSet<int>(stopLogTimes);

				var allLogTimes = new SortedSet<int>();
				foreach (var logTime in _positions.Keys)
					allLogTimes.Add(logTime);
				foreach (var logTime in _inputSamples.Values.SelectMany(inputSample => inputSample.LogTimes))
					allLogTimes.Add(logTime);
				foreach (var logTime in _speeds.Keys)
					allLogTimes.Add(logTime);
				foreach (var logTime in newLogTimes)
					allLogTimes.Add(logTime);
				if (lastLogTime != null)
					allLogTimes.Add(lastLogTime.Value);
				_logTimes = allLogTimes.ToList();
			}
			public bool HasPosition(int logTime)
			{
				return _positions.ContainsKey(logTime);
			}
			public CoreServerDatabaseManager.Position? GetPosition(int logTime)
			{
				CoreServerDatabaseManager.Position result;
				if (_positions.TryGetValue(logTime, out result))
					return result;
				var bsr = _positionLogTimes.BinarySearch(logTime.CompareTo);

				return bsr.HasFrom ? _positions[_positionLogTimes[bsr.From]] : (CoreServerDatabaseManager.Position?)null;
			}
			public bool IsInZone(int logTime, int zoneId)
			{
				return _geoZones[logTime].Contains(zoneId);
			}
			public decimal? GetSensorValue(int logTime, SensorLegend sensorLegend)
			{
				if (!_sensorMaps.Contains(sensorLegend))
					return null;

				var maps = _sensorMaps[sensorLegend];
				decimal? mappedValue = null;
				foreach (var m in maps)
				{
					InputSample currentSensorValues;

					if (!_inputSamples.TryGetValue(m.CONTROLLER_SENSOR.NUMBER, out currentSensorValues))
						continue; //Вообще нет данных по этому входу
					var value = currentSensorValues.Get(logTime, m.CONTROLLER_SENSOR.VALUE_EXPIRED ?? m.CONTROLLER_SENSOR_LEGEND.VALUE_EXPIRED);
					if (value == null || value < m.Min_Value || m.Max_Value < value)
						continue;
					if (mappedValue == null)
						mappedValue = 0;
					mappedValue += m.Multiplier * value + m.Constant;
				}
				return mappedValue;
			}
			internal int? GetSpeed(int logTime)
			{
				int speed;
				if (_speeds.TryGetValue(logTime, out speed))
					return speed;

				var bsr = _speedLogTimes.BinarySearch(logTime.CompareTo);

				return bsr.HasFrom ? _speeds[_speedLogTimes[bsr.From]] : (int?)null;
			}
			internal int? GetPreviousGeoLogTime(int logTime)
			{
				--logTime;
				var bsr = _positionLogTimes.BinarySearch(logTime.CompareTo);
				return bsr.HasFrom ? _positionLogTimes[bsr.From] : (int?)null;
			}
			public int? GetPreviousLogTime(int logTime)
			{
				--logTime;

				var bsr = _logTimes.BinarySearch(x => logTime.CompareTo(x));
				return bsr.HasFrom ? _logTimes[bsr.From] : (int?)null;
			}
			public bool HasPicture(int logTime)
			{
				return
					Server.VideoAnalyticsSensors.Select(
						videoAnalyticsSensor => GetSensorValue(logTime, videoAnalyticsSensor))
						  .Any(value => value != null && value.Value != 0);
			}
			public bool IsStop(int logTime)
			{
				return _stopLogTimes.Contains(logTime);
			}
			public override string ToString()
			{
				return
					string.Format(
						"_logTimes = {0}, _positionLogTimes = {1}, _speedLogTimes = {2}, _stopLogTimes = {3}",
						_logTimes.Join(", "), _positionLogTimes.Join(", "), _speedLogTimes.Join(", "), _stopLogTimes.Join(", "));
			}
		}
	}
}