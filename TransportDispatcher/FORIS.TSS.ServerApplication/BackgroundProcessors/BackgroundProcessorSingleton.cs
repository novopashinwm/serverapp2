﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	public abstract class BackgroundProcessorSingleton<T>
		: BackgroundProcessor
		where T : BackgroundProcessorSingleton<T>, new()
	{
		private class Lock
		{
		}
		private static readonly Lock InstanceLock = new Lock();
		private static volatile T _instance;

		public static T Instance
		{
			get
			{
				if (_instance != null)
					return _instance;
				lock (InstanceLock)
				{
					if (_instance != null)
						return _instance;
					_instance = new T();
				}
				return _instance;
			}
		}
		protected BackgroundProcessorSingleton(TimeSpan? sleepInterval) : base(sleepInterval)
		{
		}
		/// <summary> Возвращает настройку: включен или выключен фоновый обработчик </summary>
		public bool Enabled => GetType().GetAppSettingAsBoolean("Enabled");
	}
}