﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	class ExpiredTrialServiceRemover : BackgroundProcessorSingleton<ExpiredTrialServiceRemover>
	{
		private readonly DateTime? _overridenTrialDate;

		public ExpiredTrialServiceRemover() : base(TimeSpan.FromMinutes(5))
		{
			var trialDateString = ConfigurationManager.AppSettings["ExpiredTrialServiceRemover.OverridenTrialDate"];
			if (!string.IsNullOrWhiteSpace(trialDateString))
			{
				_overridenTrialDate = XmlConvert.ToDateTime(
					trialDateString,
					XmlDateTimeSerializationMode.Utc);
			}
		}

		protected override bool Do()
		{
			bool somethingWasDone = false;
			using (var entities = new Entities())
			{
				if (_overridenTrialDate != null)
				{
					while (true)
					{
						var service = entities.Billing_Service.FirstOrDefault(
							bs => bs.EndDate < _overridenTrialDate.Value && 
								  bs.Type.PeriodDays != null);
						if (service == null)
							break;
						service.EndDate = _overridenTrialDate;
						entities.SaveChangesWithHistory();
						somethingWasDone = true;
					}
				}
				
				while (true)
				{
					var expiredService = entities
						.Billing_Service
						.Include(Billing_Service.AsidIncludePath)
						.Include(Billing_Service.AsidIncludePath + "." + Asid.ContactIncludePath)
						.Include(Billing_Service.AsidIncludePath + "." + Asid.DEPARTMENTIncludePath)
						.Include(Billing_Service.AsidIncludePath + "." + Asid.OPERATORIncludePath)
						.Include(Billing_Service.TypeReferencePath)
						.FirstOrDefault(bs => bs.EndDate < DateTime.UtcNow);
					if (expiredService == null)
						break;

					if (expiredService.Asid != null &&
						expiredService.Asid.Contact == null &&
						expiredService.Type.PeriodDays != null &&
						expiredService.Asid.OPERATOR != null)
					{
						var operatorPhone = entities.GetOperatorConfirmedPhone(expiredService.Asid.OPERATOR.OPERATOR_ID);

						if (operatorPhone != null)
						{
							//Услуга для неМТС
							expiredService.EndDate = DateTime.UtcNow.AddDays(expiredService.Type.PeriodDays.Value);
							somethingWasDone = true;
							entities.CreateAppNotificationAboutOperatorDataChange(
								expiredService.Asid.OPERATOR.OPERATOR_ID, UpdatedDataType.BillingServices);

							entities.SaveChangesWithHistory();
							continue;
						}
					}

					Trace.TraceInformation("{0}: Delete expired trial service id = {1}",
										   GetType(), expiredService.ID);

					if ((expiredService.Type.MinPrice ?? 0) == 0 &&
						(expiredService.Type.MaxPrice ?? 0) == 0 &&
						expiredService.Asid != null &&
						expiredService.Asid.Contact != null)
					{
						var adminAsids = entities.GetAdminAsids(expiredService.Asid).ToList();
					
						foreach (var adminAsid in adminAsids)
						{
							if (adminAsid.Contact == null)
								continue;

							if (adminAsid.OPERATOR != null)
								entities.CreateAppNotificationAboutOperatorDataChange(
									adminAsid.OPERATOR.OPERATOR_ID, UpdatedDataType.BillingServices);
						}
					}

					entities.DeleteBillingService(expiredService);

					entities.SaveChangesWithHistory();
					somethingWasDone = true;
				}
			}

			return somethingWasDone;
		}
	}
}
