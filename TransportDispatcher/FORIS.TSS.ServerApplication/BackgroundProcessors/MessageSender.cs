﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.ServerApplication.SmsService;
using FORIS.TSS.TerminalService.Interfaces;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	abstract class MessageSender : BackgroundProcessor, IInitializable
	{
		protected static readonly DateTime Forever = new DateTime(2099, 1, 1);

		private readonly bool _enabled;

		private readonly TimeSpan[] _failedRetryDelays = new[]
		{
			TimeSpan.FromSeconds(30),
			TimeSpan.FromMinutes(1),
			TimeSpan.FromMinutes(5),
			TimeSpan.FromMinutes(30),
			TimeSpan.FromHours(1),
			TimeSpan.FromDays(1)
		};

		private readonly MessageDestinationType _messageDestinationType;
		private readonly Func<Entities, IQueryable<MessageTuple>> _getMesages;
		private readonly int _maxTps;
		private TimeSpan _retryThrottlingTime = TimeSpan.FromMinutes(1);
		private readonly TimeSpan _retryWaitingTime = TimeSpan.FromMinutes(1);

		private DateTime _throttleTime = DateTime.MinValue;

		protected MessageSender(
			MessageDestinationType messageDestinationType,
			int maxTps,
			bool? enabled = null,
			TimeSpan? sleepInterval = null)
			: base(sleepInterval)
		{
			_messageDestinationType = messageDestinationType;
			_getMesages = ctx => GetMessageBasicQuery(messageDestinationType)(ctx).Select(
				m => new MessageTuple
					{
						Message = m
					});
			_maxTps  = maxTps;
			_enabled = enabled ?? GetType().GetAppSettingAsBoolean("Enabled");
		}

		protected static Func<Entities, IQueryable<MESSAGE>> GetMessageBasicQuery(MessageDestinationType transport)
		{
			var utcNow = DateTime.UtcNow;
			return ctx => ctx.MESSAGE
				.Include(MESSAGE.MessageTemplateIncludePath)
				.Include(MESSAGE.MessageFieldIncludePath)
				.Include(MESSAGE.MessageFieldIncludePath + "." + MESSAGE_FIELD.MessageTemplateFieldIncludePath)
				.Include(MESSAGE.ContactsIncludePath)
				.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
				.Where(m =>
					m.DestinationType_ID == (int)transport &&
					m.STATUS             == (int) MessageState.Wait &&
					m.TIME               <= utcNow)
				.OrderBy(m => m.TIME);
		}
		public MessageDestinationType Type { get; set; }
		/// <summary> Метод, вызываемый для инициализации объекта перед использованием других методов </summary>
		public abstract void Initialize();
		/// <summary> Получить платный сервис по имени шаблона сообщения </summary>
		/// <param name="templateName"> Имя шаблона сообщения </param>
		/// <returns> Платный сервис <seealso cref="PaidService"/></returns>
		public static PaidService GetPaidServiceByTemplate(string templateName)
		{
			//TODO: вынести в справочник, по которому возможно определение PaidService <->Message_Template.Name в обе стороны
			if (string.Equals(templateName, Msg::MessageTemplate.GenericNotification, StringComparison.OrdinalIgnoreCase))
				return PaidService.SendNotificationSms;

			if (!string.IsNullOrWhiteSpace(templateName) && templateName.StartsWith("Configuration."))
				return PaidService.SendConfigurationSms; //NOTE: может, смотреть, не адресовано ли сообщение контроллеру?

			//TODO: может, различать ещё командные сообщения? Например, "вибрировать" для Sonim.

			return PaidService.SendInformationalSms;
		}
		protected bool CheckBilling(BillingServer billingServer, MessageTuple messageTuple)
		{
			if (messageTuple.OperatorId == null)
				return true;

			var paidService = GetPaidServiceByTemplate(messageTuple.TemplateName);

			if (paidService == PaidService.SendInformationalSms ||
				paidService == PaidService.SendConfigurationSms)
				return true;

			var billingResult =
				messageTuple.VehicleId == null
					? billingServer.AllowRenderService(messageTuple.OperatorId.Value, paidService)
					: billingServer.AllowRenderService(messageTuple.OperatorId.Value, messageTuple.VehicleId.Value, paidService);

			if (billingResult != BillingResult.Success)
			{
				TraceInformation("Message {0} has failed because of billing result {1}", messageTuple.Message.MESSAGE_ID, billingResult);
				return false;
			}

			return true;
		}
		protected void ChargeBilling(BillingServer billingServer, MessageTuple messageTuple)
		{
			if (messageTuple.OperatorId == null)
				return;

			var paidService = GetPaidServiceByTemplate(messageTuple.TemplateName);

			if (paidService == PaidService.SendInformationalSms ||
				paidService == PaidService.SendConfigurationSms)
				return;

			if (messageTuple.VehicleId == null)
			{
				billingServer.ChargeServiceRendering(messageTuple.OperatorId.Value, paidService);
			}
			else
			{
				billingServer.ChargeServiceRendering(messageTuple.OperatorId.Value, messageTuple.VehicleId.Value, paidService, messageId:messageTuple.Id);
			}
		}
		protected void CloseNotificationMessage(Entities entities, MESSAGE message)
		{
			if(!message.GROUP.HasValue)
				return;

			var groupMessage = entities.MESSAGE
				.Where(m => m.DestinationType_ID    == (int)MessageDestinationType.Notification)
				.Where(m => m.MESSAGE_ID             < message.MESSAGE_ID)
				.Where(m => m.TIME                   < message.TIME)
				.Where(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.GenericNotification)
				.Where(m => m.Owner_Operator_ID     == message.Owner_Operator_ID)
				.Where(m => m.STATUS                == (int)MessageState.Pending)
				.FirstOrDefault();
			if(groupMessage == null)
				return;

			groupMessage.STATUS = (int) MessageState.Done;
			entities.SaveUnsavedChanges();
		}
		protected MessageProcessingResult GetProcessingResult(SendSmsResult sendSmsResult)
		{
			switch (sendSmsResult)
			{
				case SendSmsResult.Success:
					return MessageProcessingResult.Sent;
				case SendSmsResult.InvalidMessageLength:
					return MessageProcessingResult.InvalidMessageLength;
				case SendSmsResult.InvalidDestinationAddress:
					return MessageProcessingResult.InvalidDestinationAddress;
				case SendSmsResult.NoConnection:
					return MessageProcessingResult.NoConnectionWithGateway;
				case SendSmsResult.MessageQueueFull:
					return MessageProcessingResult.MessageQueueFull;
				case SendSmsResult.UnspecifiedError:
					return MessageProcessingResult.Error;
				case SendSmsResult.HttpError:
					return MessageProcessingResult.HttpError;
				case SendSmsResult.Timeout:
					return MessageProcessingResult.Timeout;
				case SendSmsResult.ServiceTurnedOff:
					return MessageProcessingResult.ServiceTurnedOff;
				case SendSmsResult.ThrottlingError:
					return MessageProcessingResult.Throttled;
				default:
					return MessageProcessingResult.Unknown;
			}
		}
		protected static bool CheckOutMessageToProcessing(IEnumerable<MessageTuple> messageTuples, Entities context)
		{
			bool anyMessageRequiresToBeSaved = false;
			foreach (var message in messageTuples.Select(messageTuple => messageTuple.Message))
			{
				message.STATUS = (int) MessageState.Pending;
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Processing, message.MESSAGE_ID);
				anyMessageRequiresToBeSaved = true;
			}
			if (!anyMessageRequiresToBeSaved)
				return false;
			context.SaveChanges();
			return true;
		}
		protected override bool Do()
		{
			if (!_enabled)
				return false;

			if (IsThrottled)
			{
				var timeSpanToSleep = DateTime.UtcNow - _throttleTime.Add(_retryThrottlingTime);
				if (1 < timeSpanToSleep.TotalSeconds)
					Thread.Sleep(timeSpanToSleep);
				return false;
			}
			return Process();
		}
		protected bool IsThrottled
		{
			get { return DateTime.UtcNow < _throttleTime.Add(_retryThrottlingTime); }
		}
		private bool Process()
		{
			using (var context = new Entities())
			{
				var sw = new Stopwatch();
				sw.Start();
				var messageTuples = _getMesages(context).Take(_maxTps == 0 ? 1 : _maxTps).ToList();
				sw.Stop();
				if (TimeSpan.FromSeconds(1) < sw.Elapsed)
					TraceWarning("_getMessages is too slow - {0}", sw.Elapsed);

				if (!CheckOutMessageToProcessing(messageTuples, context))
					return false;

				foreach (var messageTuple in messageTuples)
				{
					if (!ProcessMessage(context, messageTuple))
						break;
				}

				if (messageTuples.Count == 0)
					return false;

				context.SaveChanges();

				return true;
			}
		}
		protected virtual bool ProcessMessage(Entities context, MessageTuple messageTuple)
		{
			var message = messageTuple.Message;

			if (IsThrottled)
			{
				message.STATUS = (int)MessageState.Wait;
				message.TIME = DateTime.UtcNow.Add(_retryThrottlingTime);
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Throttled, message.MESSAGE_ID);
				return false;
			}

			if (messageTuple.Destination == null || !messageTuple.Destination.Valid)
			{
				message.STATUS = (int)MessageState.Done;
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.DestinationContactIsNotValid, message.MESSAGE_ID);
				return true;
			}

			//TODO: рассмотреть случай, когда у сообщения более одного получателя
			var lockedContact = message.Contacts.FirstOrDefault(c =>
				(c.Type == (int)MessageContactType.Destination) &&
				(c.Contact.LockedUntil != null && c.Contact.LockedUntil.Value >= DateTime.UtcNow));

			if (lockedContact != null && lockedContact.Contact.LockedUntil != null && lockedContact.Contact.LockedUntil != Forever)
			{
				message.STATUS = (int)MessageState.Done;
				Server.Instance().SetMessageProcessingResult(MessageProcessingResult.DestinationContactIsLocked, message.MESSAGE_ID);
				return true;
			}

			if (messageTuple.CommandId.HasValue)
			{
				// Проверяем не была ли отменена команда.
				var tuple = messageTuple;
				var commandResult = context.Command
					.Where(c => c.ID == tuple.CommandId)
					.Select(c => c.Result_Type_ID)
					.Cast<CmdResult>()
					.FirstOrDefault();
				if (commandResult == CmdResult.Cancelled)
				{
					message.STATUS = (int?)MessageState.Done;
					Server.Instance().SetMessageProcessingResult(MessageProcessingResult.CancelByCommand, message.MESSAGE_ID);
					return false;
				}
			}

			return ProcessSendMessage(context, messageTuple);
		}
		protected bool ProcessSendMessage(Entities context, MessageTuple messageTuple)
		{
			var message = messageTuple.Message;
			try
			{
				MessageProcessingResult messageResult = SendMessage(context, messageTuple);
				message.TIME = DateTime.UtcNow;

				switch (messageResult)
				{
					case MessageProcessingResult.Sent:
					case MessageProcessingResult.Queued:
						message.STATUS = (int) MessageState.Done;
						CloseNotificationMessage(context, message);
						break;
					case MessageProcessingResult.MessageQueueFull:
						message.TIME = messageTuple.Destination.LockedUntil ?? DateTime.UtcNow.Add(_retryWaitingTime);
						message.STATUS = (int)MessageState.Wait;
						break;
					case MessageProcessingResult.NoConnectionWithGateway:
					case MessageProcessingResult.Throttled:
						var throttledUntil = DateTime.UtcNow.Add(_retryThrottlingTime);

						TraceWarning("Throttled until {0} because of result {1} when sending message {2}",
							throttledUntil.ToLocalTime(), messageResult, message.MESSAGE_ID);

						message.TIME   = throttledUntil;
						message.STATUS = (int)MessageState.Wait;
						_throttleTime  = DateTime.UtcNow;
						break;
					default:
						message.STATUS = (int)MessageState.Done;
						break;
				}

				Server.Instance()
					.SetMessageProcessingResult(messageResult, message.MESSAGE_ID);
				Server.Instance()
					.NotifyMessageStateChanged(new MessageStateItem
					{
						MessageId        = messageTuple.Id,
						State            = (MessageState)message.STATUS,
						ProcessingResult = messageResult
					});
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception ex)
			{
				TraceError("Unable to send message [{0}] to {1} error: {2}",
					messageTuple.Id, messageTuple.Destination, ex);
				ProcessMessageError(message);
			}

			return true;
		}
		protected void ProcessMessageError(MESSAGE message)
		{
			if (message.ErrorCount < 0)
				message.ErrorCount = 1;
			else
				++message.ErrorCount;
			Server.Instance().SetMessageProcessingResult(MessageProcessingResult.Error, message.MESSAGE_ID);

			if (_failedRetryDelays.Length < message.ErrorCount)
			{
				message.STATUS = (int) MessageState.Done;
			}
			else
			{
				message.STATUS = (int) MessageState.Wait;
				message.TIME = DateTime.UtcNow.Add(_failedRetryDelays[message.ErrorCount - 1]);
			}
		}
		/// <summary> Отправка сообщения адресату </summary>
		/// <param name="context"></param>
		/// <param name="messageTuple"></param>
		/// <returns></returns>
		protected abstract MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple);
		protected void Throttled(TimeSpan timeSpan)
		{
			_throttleTime = DateTime.UtcNow;
			_retryThrottlingTime = timeSpan;

			TraceWarning("Throttled, command processing will be suspended till {0}",
				timeSpan + _retryThrottlingTime);
		}
		public bool Enabled { get { return _enabled; } }
		protected void TraceInformation(string format, params object[] @params)
		{
			Trace.TraceInformation(GetType().Name + ": " + format, @params);
		}
		protected void TraceWarning(string format, params object[] @params)
		{
			Trace.TraceWarning(GetType().Name + ": " + format, @params);
		}
		protected void TraceError(string format, params object[] @params)
		{
			Trace.TraceError(GetType().Name + ": " + format, @params);
		}
		protected Dictionary<string, object> GetMessageAttributes(Entities context, MessageTuple messageTuple)
		{
			var result = context
				.MESSAGE_FIELD
				.Include(MESSAGE_FIELD.MESSAGEIncludePath)
				.Include(MESSAGE_FIELD.MESSAGEIncludePath + "." + MESSAGE.MESSAGE_TEMPLATEIncludePath)
				.Include(MESSAGE_FIELD.MESSAGE_TEMPLATE_FIELDIncludePath)
				.Where(f => f.MESSAGE.MESSAGE_ID == messageTuple.Message.MESSAGE_ID)
				.ToList()
				.Select(f => f.ToDTO())
				.ToDictionary(f => f.Template.Name, f => f.StronglyTypeValue);
			return result;
		}
		/// <summary> Тип адресатов сообщений для этого отправителя. </summary>
		public MessageDestinationType MessageDestinationType
		{
			get { return _messageDestinationType; }
		}
		/// <summary> Проверка возможности отправки контакту </summary>
		/// <param name="contact"></param>
		/// <returns></returns>
		public abstract bool CanSendTo(Contact contact);
		/// <summary> Получить тип push-уведомления по имени шаблона сообщения, по умолчанию "уведомление" </summary>
		/// <param name="templateName">Имя шаблона сообщения.</param>
		/// <returns>Тип push-уведомления<seealso cref="PushType"/></returns>
		internal static PushType GetPushTypeByTemplateName(string templateName)
		{
			switch (templateName)
			{
				case Msg::MessageTemplate.GenericNotification:
					return PushType.Notification;
				case Msg::MessageTemplate.PositionRequest:
					return PushType.PositionRequest;
				case Msg::MessageTemplate.ProposeFriendship:
					return PushType.ProposeFriendship;
				case Msg::MessageTemplate.AlarmNotification:
					return PushType.Alarm;
				case Msg::MessageTemplate.FriendWasRegisteredNotification:
					return PushType.FriendWasRegistered;
				case Msg::MessageTemplate.DataUpdate:
					return PushType.DataUpdate;
				case Msg::MessageTemplate.RequestResult:
					return PushType.RequestResult;
				case Msg::MessageTemplate.WakeUp:
					return PushType.WakeUp;
				case Msg::MessageTemplate.BillingServiceExhaust:
					return PushType.BillingServiceExhaust;
				default:
					Trace.TraceWarning("GetPushTypeByTemplateName: unknown templateName: {0}", templateName);
					return PushType.Notification;
			}
		}
	}
}