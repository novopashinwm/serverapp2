﻿using System;
using System.Configuration;
using System.Diagnostics;
using EasySMPP;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.ServerApplication.SmsService;
using FORIS.TSS.ServerApplication.SmsService.Configuration;
using FORIS.TSS.ServerApplication.Utilities.SmsService;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	sealed class SmppMessageSenderProcessor : MessageSender
	{
		private TimeSpan _retryTimeOnMQF;
		private static readonly object InstanceLock = new object();
		private static SmppMessageSenderProcessor _instance;
		private SmppSmsSender _smppSmsSender;

		public SmppMessageSenderProcessor()
			: base(MessageDestinationType.Smpp, 1, ConfigHelper.GetAppSettingAsBoolean("SmppSenderProcessor.Enabled"))
		{
		}

		private bool _autoReply;

		public override void Initialize()
		{
			var configuration = (SmsProviderSmppSectionHandler)ConfigurationManager.GetSection("SmsProviderSmpp");
			_retryTimeOnMQF = TimeSpan.FromSeconds(60);
			_autoReply = configuration.AutoReply;

			SmppSmsSender.Instance.Initialize();

			_smppSmsSender = SmppSmsSender.Instance;
			_smppSmsSender.OnReceiveSms   += SmppSmsSenderOnReceiveSms;
			_smppSmsSender.OnDeliveredSms += SmppSmsSenderOnDeliveredSms;
		}

		void SmppSmsSenderOnDeliveredSms(object sender, DeliveredSmsEventHandlerArgs args)
		{
			Trace.TraceInformation("{0}: Delivery report received from SMPP, sender: {1}, MessageId: {2}, State: {3}",
				GetType(), sender, args.MessageId, args.State);
			var messageProcessingResult = ToMessageProcessingResult(args.State);
			if (messageProcessingResult == null)
			{
				Trace.TraceWarning(
					"{0}: Unknown delivery state from SMPP, sender: {1}, MessageId: {2}, State: {3}",
					GetType(), sender, args.MessageId, args.State);
				return;
			}
			Server.Instance().SetMessageProcessingResult(messageProcessingResult.Value, args.MessageId);
		}

		private static MessageProcessingResult? ToMessageProcessingResult(MessageStates value)
		{
			switch (value)
			{
				case MessageStates.ACCEPTED:
				case MessageStates.DELIVERED:
					return MessageProcessingResult.Delivered;
				case MessageStates.UNDELIVERABLE:
					return MessageProcessingResult.Undeliverable;
				case MessageStates.DELETED:
					return MessageProcessingResult.Deleted;
				case MessageStates.UNKNOWN:
					return MessageProcessingResult.Unknown;
				case MessageStates.REJECTED:
					return MessageProcessingResult.Rejected;
				default:
					return null;
			}
		}

		void SmppSmsSenderOnReceiveSms(object sender, RecieveSmsEventHandlerArgs args)
		{
			var from = args.From;
			if (string.IsNullOrWhiteSpace(from))
			{
				Trace.TraceWarning("{0}: Message received from SMPP, sender {1}, message body: {2}", GetType(), "<Empty>", args.Text);
				// Защищаемся от спама
				return;
			}
			Trace.TraceInformation("{0}: Message received from SMPP, sender {1}, message body: {2}", GetType(), args.From, args.Text);

			using (var entities = new Entities())
			{
				var message = new MESSAGE
				{
					BODY             = args.Text,
					ProcessingResult = (int)MessageProcessingResult.Received,
					//TODO: Помещать в очередь SMS для обработки, тогда состояние будет Wait
					STATUS           = (int)MessageState.Done
				};
				message.AddSource(entities.GetContact(ContactType.Phone, from));
				entities.MESSAGE.AddObject(message);

				if (_autoReply)
					entities.CreateOutgoingSmsToMsisdn(from, args.Text);

				entities.SaveChanges();
			}
		}

		public static SmppMessageSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new SmppMessageSenderProcessor());
			}
		}

		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{
			var message = messageTuple.Message;

			var billingServer = new BillingServer(context);
			if (!CheckBilling(billingServer, messageTuple))
				return MessageProcessingResult.UnsuccessfulBillingResult;

			var sendSmsResult = _smppSmsSender.SendSms(messageTuple);

			var result = GetProcessingResult(sendSmsResult);

			switch (sendSmsResult)
			{
				case SendSmsResult.Success:
					messageTuple.Destination.LockedCount = 0;
					ChargeBilling(billingServer, messageTuple);
					break;
				case SendSmsResult.MessageQueueFull:
					++messageTuple.Destination.LockedCount;
					messageTuple.Destination.LockedUntil = GetUnlockTime(messageTuple.Destination.LockedCount);
					break;
				case SendSmsResult.InvalidDestinationAddress:
					++messageTuple.Destination.LockedCount;
					messageTuple.Destination.LockedUntil = Forever;

					Trace.TraceWarning(
						"{0}: Message [{1}] has received InvalidDestinationAddress, so phone [{2}] will be blocked forever",
						GetType(), message.MESSAGE_ID, messageTuple.Destination);
					break;
			}

			return result;
		}

		private DateTime GetUnlockTime(int mqfCount)
		{
			TimeSpan interval;

			if (10 < mqfCount)
			{
				interval = TimeSpan.FromDays(1);
			}
			else
			{
				interval = TimeSpan.FromSeconds((2L ^ (mqfCount - 1)) * _retryTimeOnMQF.TotalSeconds);
				if (1 <= interval.TotalDays)
					interval = TimeSpan.FromDays(1);
			}

			return DateTime.UtcNow.Add(interval);
		}

		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int) ContactType.Phone;
		}
	}
}