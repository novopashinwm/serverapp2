﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.SmsService;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	abstract class MessageSenderWithLock : MessageSender
	{
		private readonly TimingCache<string, string> _cache = new TimingCache<string, string>(TimeSpan.FromMinutes(1), input =>
		{
			using (var entities = new Entities())
			{
				var value = entities.CONSTANTS.Where(c => c.NAME == input).Select(c => c.VALUE).FirstOrDefault();
				return value ?? string.Empty;
			}
		});

		protected TimeSpan LockSpan
		{
			get
			{
				TimeSpan span;
				var value = TimeSpan.TryParse(_cache[CONSTANTS.PushLockTimeSpan], out span) ? span : TimeSpan.FromDays(1);
				return value;
			}
		}

		protected MessageSenderWithLock(
			MessageDestinationType messageDestinationType,
			int maxTps,
			bool enabled,
			TimeSpan? sleepInterval = null) :
				base(messageDestinationType, maxTps, enabled, sleepInterval)
		{
		}

		protected override bool ProcessMessage(Entities context, MessageTuple messageTuple)
		{
			DateTime? lockedUntil = null;
			var lockedCount = 0;
			if (messageTuple.Destination != null)
			{
				var destinationIsLocked = DateTime.UtcNow < messageTuple.Destination.LockedUntil;

				lockedUntil = destinationIsLocked
					? messageTuple.Destination.LockedUntil
					: null;
				lockedCount = destinationIsLocked
					? messageTuple.Destination.LockedCount + 1
					: 0;
			}

			var result = base.ProcessMessage(context, messageTuple);

			if (messageTuple.Destination == null) 
				return result;

			if (lockedUntil.HasValue)
			{
				if (messageTuple.Destination.LockedUntil > lockedUntil)
					return result;

				messageTuple.Destination.LockedUntil = lockedUntil;
				messageTuple.Destination.LockedCount = lockedCount;
			}

			return false;
		}
	}
}