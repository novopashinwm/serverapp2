﻿namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO
{
	public class GoogleCloudMessagingGcmResult
	{
		// ReSharper disable InconsistentNaming
#pragma warning disable 649
		public string error;
		public string registration_id;
#pragma warning restore 649
		// ReSharper restore InconsistentNaming

		public static readonly string InvalidRegistration = "InvalidRegistration";
		public static readonly string NotRegistered       = "NotRegistered";
		public static readonly string MismatchSenderId    = "MismatchSenderId";
	}
}