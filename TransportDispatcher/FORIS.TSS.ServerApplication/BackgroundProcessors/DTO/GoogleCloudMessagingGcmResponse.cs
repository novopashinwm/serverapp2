﻿namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO
{
	public class GoogleCloudMessagingGcmResponse
	{
		// ReSharper disable InconsistentNaming
#pragma warning disable 649
		public int success;
		public int canonical_ids;

		public GoogleCloudMessagingGcmResult[] results;

#pragma warning restore 649
		// ReSharper restore InconsistentNaming
	}
}