﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Response
{
	[DataContract]
	public class FcmResponseResult
	{
		/// <summary>
		/// Gets or sets the unique ID for each successfully processed message.
		/// </summary>
		[DataMember(Name = "message_id", IsRequired = false)]
		public string MessageId { get; set; }
		/// <summary> Gets or sets the registration ID of the result. </summary>
		[DataMember(Name = "registration_id", IsRequired = false)]
		public string RegistrationId { get; set; }
		[DataMember(Name = "error")]
		public string ErrorStr
		{
			get
			{
				return Error.ToString();
			}
			set
			{
				Error = (FcmResponseResultError)Enum.Parse(typeof(FcmResponseResultError), value);
			}
		}
		/// <summary> Gets or sets the type of error that occurred. </summary>
		//[IgnoreDataMember]
		//[JsonIgnore]
		public FcmResponseResultError Error { get; set; }
		/// <summary> Gets whether the result contains an error. </summary>
		public bool HasError => Error != FcmResponseResultError.None;
	}
}