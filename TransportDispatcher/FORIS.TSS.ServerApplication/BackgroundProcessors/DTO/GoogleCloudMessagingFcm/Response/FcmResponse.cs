﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Response
{
	/// <summary> Represents a Firebase Cloud Messaging message response object. </summary>
	[DataContract]
	public class FcmResponse
	{
		/// <summary> Gets or sets the unique ID identifying the multicast message. </summary>
		[DataMember(Name = "multicast_id",  IsRequired = true)]
		public long MulticastID { get; set; }
		/// <summary> Gets or sets the number of messages that were processed without an error. </summary>
		[DataMember(Name = "success",       IsRequired = true)]
		public int Success      { get; set; }
		/// <summary> Gets or sets the number of messages that could not be processed. </summary>
		[DataMember(Name = "failure",       IsRequired = true)]
		public int Failure      { get; set; }
		/// <summary> Gets or sets the number of results that contain a canonical registration token. </summary>
		[DataMember(Name = "canonical_ids", IsRequired = true)]
		public int  CanonicalIDs { get; set; }
		/// <summary> Gets or sets the list of results representing the status of the messages processed. </summary>
		[DataMember(Name = "results")]
		public IEnumerable<FcmResponseResult> Results { get; set; }
	}
}