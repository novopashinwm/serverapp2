﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Response
{
	[DataContract]
	public enum FcmResponseResultError
	{
		/// <summary>No error occurred.</summary>
		[EnumMember, Description("No error")]
		None = 0,
		/// <summary></summary>
		[EnumMember, Description("Missing Registration Token")]
		MissingRegistration,
		/// <summary></summary>
		[EnumMember, Description("Invalid Registration Token")]
		InvalidRegistration,
		/// <summary></summary>
		[EnumMember, Description("Unregistered Device")]
		NotRegistered,
		/// <summary></summary>
		[EnumMember, Description("Invalid Package Name")]
		InvalidPackageName,
		/// <summary></summary>
		[EnumMember, Description("Mismatched Sender")]
		MismatchSenderId,
		/// <summary></summary>
		[EnumMember, Description("Message Too Big")]
		MessageTooBig,
		/// <summary></summary>
		[EnumMember, Description("Invalid Data Key")]
		InvalidDataKey,
		/// <summary></summary>
		[EnumMember, Description("Invalid Time to Live")]
		InvalidTtl,
		/// <summary></summary>
		[EnumMember, Description("Timeout")]
		Unavailable,
		/// <summary></summary>
		[EnumMember, Description("Internal Server Error")]
		InternalServerError,
		/// <summary></summary>
		[EnumMember, Description("Device Message Rate Exceeded")]
		DeviceMessageRateExceeded,
		/// <summary></summary>
		[EnumMember, Description("Topics Message Rate Exceeded")]
		TopicsMessageRateExceeded
	}
}