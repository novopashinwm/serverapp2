﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Статус запроса.
	/// </summary>
	[DataContract]
	public enum FcmRequestStatus
	{
		[EnumMember]
		OK,             // indicates the request contains a valid result.
		[EnumMember]
		INVALID_REQUEST // indicates that the provided request was invalid.
	}
}