﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Запрос для отправки сообщения через FireBase Cloud Messaging (FCM).
	/// </summary>
	[DataContract]
	public class FcmRequest<T> where T : FcmNotification
	{
		/// <summary> specifies the recipient of a multicast message, a message sent to more than one registration token. </summary>
		[DataMember(Name = "registration_ids", IsRequired = false, EmitDefaultValue = false)]
		public string[] RegistrationIDs { get; set; }
		#region Payload
		/// <summary> Specifies the custom key-value pairs of the message's payload. </summary>
		[DataMember(Name = "data",             IsRequired = false, EmitDefaultValue = false)]
		public Dictionary<string, object> Data { get; set; }
		/// <summary> Specifies the custom key-value pairs of the message's payload. </summary>
		[DataMember(Name = "notification",     IsRequired = false, EmitDefaultValue = false)]
		public T Notification { get; set; }
		#endregion Payload
		/// <summary> Статус запроса. </summary>
		public FcmRequestStatus Status { get; set; }
	}
}