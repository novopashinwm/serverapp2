﻿using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Represents a Firebase Cloud Messaging notification object for Android devices.
	/// </summary>
	/// <remarks> See https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support </remarks>
	[DataContract]
	public class FcmAppleLocalizedNotification : FcmLocalizedNotification
	{
		/// <summary> Gets or sets the badge on the client app home icon. </summary>
		[DataMember(Name = "badge", IsRequired = false, EmitDefaultValue = false)]
		public string Badge { get; set; }
	}
}