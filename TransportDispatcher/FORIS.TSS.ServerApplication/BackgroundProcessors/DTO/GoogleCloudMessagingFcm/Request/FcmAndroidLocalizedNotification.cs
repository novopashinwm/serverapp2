﻿using System.Drawing;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Represents a Firebase Cloud Messaging notification object for Android devices.
	/// </summary>
	/// <remarks> See https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support </remarks>
	[DataContract]
	public class FcmAndroidLocalizedNotification : FcmLocalizedNotification
	{
		/// <summary> The app must create a channel with this ID before any notification with this key is received. </summary>
		[DataMember(Name = "android_channel_id", IsRequired = false, EmitDefaultValue = false)]
		public string AndroidChannelId { get; set; }
		/// <summary> Gets or sets whether each notification results in a new entry in the notification drawer. Notifications with the same tag replace the existing one. Example: #FF0033 </summary>
		[DataMember(Name = "color", IsRequired = false, EmitDefaultValue = false)]
		public string ColorStr
		{
			get
			{
				return Color.HasValue ? Color.ToString() : null;
			}
			set
			{
				var color = ColorTranslator.FromHtml(value);
				if (!color.IsEmpty)
					Color = ColorTranslator.FromHtml(value);
				else
					Color = null;
			}
		}
		public Color? Color { get; set; }
		/// <summary> Gets or sets the URL or name of the notification's icon. </summary>
		[DataMember(Name = "icon", IsRequired = false, EmitDefaultValue = false)]
		public string Icon { get; set; }
		/// <summary> Gets or sets whether each notification results in a new entry in the notification drawer. Notifications with the same tag replace the existing one. </summary>
		[DataMember(Name = "tag", IsRequired = false, EmitDefaultValue = false)]
		public string Tag { get; set; }
	}
}