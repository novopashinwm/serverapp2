﻿using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Represents a Firebase Cloud Messaging notification object with localization properties.
	/// </summary>
	/// <remarks> See https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support </remarks>
	[DataContract]
	public abstract class FcmLocalizedNotification : FcmNotification
	{
		/// <summary> Gets or sets the key for the body string localization. </summary>
		[DataMember(Name = "body_loc_key",   IsRequired = false, EmitDefaultValue = false)]
		public string BodyLocalizationKey   { get; set; }
		/// <summary> Gets or sets the string value to replace format specifiers in the body string for localization. </summary>
		[DataMember(Name = "body_loc_args",  IsRequired = false, EmitDefaultValue = false)]
		public string BodyLocalizationArgs  { get; set; }
		/// <summary> Gets or sets the key for the title string localization. </summary>
		[DataMember(Name = "title_loc_key",  IsRequired = false, EmitDefaultValue = false)]
		public string TitleLocalizationKey  { get; set; }
		/// <summary> Gets or sets the string value to replace format specifiers in the title string for localization. </summary>
		[DataMember(Name = "title_loc_args", IsRequired = false, EmitDefaultValue = false)]
		public string TitleLocalizationArgs { get; set; }
	}
}