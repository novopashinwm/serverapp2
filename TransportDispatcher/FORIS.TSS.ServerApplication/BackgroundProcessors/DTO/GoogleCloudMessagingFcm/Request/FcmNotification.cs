﻿using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors.DTO.GoogleCloudMessagingFcm.Request
{
	/// <summary>
	/// Represents a Firebase Cloud Messaging notification object.
	/// </summary>
	/// <remarks> See https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support </remarks>
	[DataContract]
	public abstract class FcmNotification
	{
		/// <summary> Gets or sets the title of the notification. </summary>
		[DataMember(Name = "title", IsRequired = false, EmitDefaultValue = false)]
		public string Title { get; set; }
		/// <summary> Gets or sets the body text of the notification. </summary>
		[DataMember(Name = "body", IsRequired = false, EmitDefaultValue = false)]
		public string Body { get; set; }
		/// <summary> Gets or sets the sound that is played when the device receives a notification. </summary>
		[DataMember(Name = "sound", IsRequired = false, EmitDefaultValue = false)]
		public string Sound { get; set; }
		/// <summary> Gets or sets the action associated with a user click on the notification. </summary>
		[DataMember(Name = "click_action", IsRequired = false, EmitDefaultValue = false)]
		public string ClickAction { get; set; }
	}
}