﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	internal sealed partial class RuleProcessor : BackgroundProcessor
	{
#if DEBUG
		private const int StopDurationSeconds = 30; //5*60;
#else
		private const int StopDurationSeconds = 5*60;
#endif

		private RuleProcessor()
		{
			_operatorLogin = Environment.UserDomainName + @"\" + Environment.UserName;

			if (!int.TryParse(ConfigurationManager.AppSettings["RuleProcessor.MaxVehicleCountToProcessByOneStep"], out _maxVehicleCountToProcessByOneStep))
				_maxVehicleCountToProcessByOneStep = 100;

			if (!int.TryParse(ConfigurationManager.AppSettings["RuleProcessor.MaxMinutesToProcess"], out _maxMinutesToProcess))
				_maxMinutesToProcess = 30;

			_allowanceEntities = new Entities();
			_accessCache = new TimingCache<OperatorVehicleAccess, bool>(TimeSpan.FromMinutes(5), IsAllowedInDb);
		}

		private readonly string        _operatorLogin;
		private static RuleProcessor   _instance;
		private static readonly object _instanceLock = new object();
		private readonly int           _maxMinutesToProcess;
		private readonly int           _maxVehicleCountToProcessByOneStep;
		public static RuleProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (_instanceLock)
					return _instance ?? (_instance = new RuleProcessor());
			}
		}
		private class VehicleQuery
		{
			/// <summary> Идентификатор объекта наблюдения  </summary>
			public int                     VehicleId;
			/// <summary> Сортированный набор категорий датчиков, по которым есть условия проверки </summary>
			public SortedSet<SensorLegend> SensorLegends;
			/// <summary> Есть ли условия проверок скорости </summary>
			public bool                    Speed;
			/// <summary> Сортированный набор геозон, по которым есть условия проверки </summary>
			public SortedSet<int>          GeoZones;
			/// <summary> Есть ли условия проверок начала движения/остановок </summary>
			public bool                    MotionStart;
		}
		private readonly TimingCache<Tuple<int, int>, string> _vehicleNameCache =
			new TimingCache<Tuple<int, int>, string>(
				TimeSpan.FromMinutes(20), x => Server.Instance().GetVehicleName(x.Item1, x.Item2));
		/// <summary> Класс сработавшего условия по группе геозон </summary>
		private class SatisfiedZoneGroupCondition
		{
			public readonly CompoundRule_ZoneGroup ZoneGroupCondition;
			public readonly GEO_ZONE               Zone;
			public SatisfiedZoneGroupCondition(CompoundRule_ZoneGroup zoneGroupCondition, GEO_ZONE zone)
			{
				ZoneGroupCondition = zoneGroupCondition;
				Zone               = zone;
			}
		}
		/// <summary> Класс сработавшего составного условия </summary>
		private class SatisfiedCompoundCondition
		{
			public CompoundRule                 CompoundRule;
			public VEHICLE                      Vehicle;
			public CompoundRule_Sensor[]        Sensors;
			public CompoundRule_Speed           Speed;
			public CompoundRule_Time            Time;
			public CompoundRule_TimePoint       TimePoint;
			public CompoundRule_Zone            Zone;
			public SatisfiedZoneGroupCondition  ZoneGroup;
			public CompoundRule_DataAbsence     DataAbsence;
			public CompoundRule_PositionAbsence PositionAbsence;
			public bool                         IsAlarm
			{
				get
				{
					return Sensors != null && Sensors
						.Any(item =>
							item.CONTROLLER_SENSOR_LEGEND.Number == (int)SensorLegend.Alarm &&
							item.Value != 0);
				}
			}
		}
		/// <summary> Поиск объектов для обработки </summary>
		private static readonly Func<Entities, string, DateTime, int, IQueryable<VEHICLE>> GetVehiclesToProcess =
				CompiledQuery.Compile<Entities, string, DateTime, int, IQueryable<VEHICLE>>(
					(entities, hostName, time, maxVehicleCountToProcessByOneStep) => entities.Vehicle_Rule_Processing
						.Include($"{Vehicle_Rule_Processing.VEHICLEIncludePath}")
						.Include($"{Vehicle_Rule_Processing.VEHICLEIncludePath}.{VEHICLE.CONTROLLERIncludePath}")
						.Include($"{Vehicle_Rule_Processing.VEHICLEIncludePath}.{VEHICLE.CONTROLLERIncludePath}.{nameof(CONTROLLER.CONTROLLER_SENSOR_MAP)}")
						.Where(vrp =>
							(vrp.Host == null || vrp.Host.Name == hostName) &&
							(vrp.LastTime < time))
						.OrderByDescending(vrp => vrp.Host.Host_ID)
						.ThenBy(vrp => vrp.LastTime)
						.Select(vrp => vrp.VEHICLE)
						.Take(maxVehicleCountToProcessByOneStep));
		/// <summary> Поиск правил для обработки </summary>
		private static readonly Func<Entities, string, int, IQueryable<CompoundRule>> GetRulesToProcess =
			CompiledQuery.Compile<Entities, string, int, IQueryable<CompoundRule>>(
				(entities, hostName, logTime) => (from cr in entities.CompoundRule
					.Include("CompoundRule_Sensor")
					.Include("CompoundRule_Sensor.CONTROLLER_SENSOR_LEGEND")
					.Include("CompoundRule_Speed")
					.Include("CompoundRule_Zone")
					.Include("CompoundRule_Zone.GEO_ZONE")
					.Include("CompoundRule_ZoneGroup")
					.Include("CompoundRule_ZoneGroup.ZONEGROUP")
					.Include("CompoundRule_ZoneGroup.ZONEGROUP." + ZONEGROUP.MembersIncludePath)
					.Include("CompoundRule_ZoneGroup.ZONEGROUP." + ZONEGROUP.MembersIncludePath + "." + ZONEGROUP_ZONE.ZoneIncludePath)
					.Include("CompoundRule_Time")
					.Include("CompoundRule_Time.TimeZone")
					.Include("CompoundRule_Message_Template")
					.Include("CompoundRule_Message_Template.Operator")
					.Include("CompoundRule_Message_Template.Operator.TimeZone")
					.Include("CompoundRule_DataAbsence")
					.Include("CompoundRule_PositionAbsence")
						where cr.Active
							&&
							(
								cr.CompoundRule_Vehicle.Any(v => v.Vehicle_Rule_Processing.Host.Name == hostName)
								||
								cr.CompoundRule_VehicleGroup.Any(vg => vg.VEHICLE.Any(v => v.Vehicle_Rule_Processing.Host.Name == hostName))
							)
							&&
							(!cr.ProcessingBegAt.HasValue || cr.ProcessingBegAt.Value <= logTime)
							&&
							(!cr.ProcessingEndAt.HasValue || cr.ProcessingEndAt.Value >= logTime)
						select cr));
		/// <summary> Поиск записей Vehicle_Rule_Processing для которых нет действующих правил (активность определяется временем жизни и флагом активности) </summary>
		private static readonly Func<Entities, int, IQueryable<Vehicle_Rule_Processing>> GetVrpsToDelete =
			CompiledQuery.Compile<Entities, int, IQueryable<Vehicle_Rule_Processing>>(
				(entities, logTime) => entities.Vehicle_Rule_Processing
					.Where(vrp => !entities.CompoundRule
						.Any(cr =>
							cr.Active
							&&
							(
								cr.CompoundRule_Vehicle.Contains(vrp.VEHICLE)
								||
								cr.CompoundRule_VehicleGroup.Any(vg => vg.VEHICLE.Contains(vrp.VEHICLE))
							)
							&&
							(!cr.ProcessingBegAt.HasValue || cr.ProcessingBegAt.Value <= logTime)
							&&
							(!cr.ProcessingEndAt.HasValue || cr.ProcessingEndAt.Value >= logTime)
							&&
							cr.CompoundRule_Message_Template
								.Any(crmt =>
									vrp.VEHICLE.Accesses
										.Any(a =>
											a.right_id    == (int)SystemRight.VehicleAccess &&
											a.operator_id == crmt.OPERATOR.OPERATOR_ID
										)
								)
						)
					)
			);
		/// <summary> Поиск объектов наблюдения для которых есть действующие правила (активность определяется временем жизни и флагом активности) </summary>
		private static readonly Func<Entities, int, IQueryable<VEHICLE>> GetVehiclesToAddVrps =
			CompiledQuery.Compile<Entities, int, IQueryable<VEHICLE>>(
				(entities, logTime) => entities.VEHICLE
					.Where(v =>
						v.Vehicle_Rule_Processing == null
						&&
						(
							v.CompoundRule
								.Any(cr =>
									cr.Active
									&&
									cr.CompoundRule_Message_Template
										.Any(mt =>
											mt.OPERATOR.v_operator_vehicle_right
												.Any(a =>
													a.right_id   == (int)SystemRight.VehicleAccess &&
													a.vehicle_id == v.VEHICLE_ID
												)
										)
									&&
									(!cr.ProcessingBegAt.HasValue || cr.ProcessingBegAt.Value <= logTime)
									&&
									(!cr.ProcessingEndAt.HasValue || cr.ProcessingEndAt.Value >= logTime)
								)
							||
							v.VEHICLEGROUP
								.Any(vg =>
									vg.CompoundRule
										.Any(cr =>
											cr.Active
											&&
											cr.CompoundRule_Message_Template
												.Any(mt =>
													mt.OPERATOR.v_operator_vehicle_right
														.Any(a =>
															a.right_id   == (int)SystemRight.VehicleAccess &&
															a.vehicle_id == v.VEHICLE_ID
														)
													&&
													mt.OPERATOR.v_operator_vehicle_groups_right
														.Any(a =>
															a.right_id        == (int)SystemRight.VehicleAccess &&
															a.vehiclegroup_id == vg.VEHICLEGROUP_ID
														)
												)
											&&
											(!cr.ProcessingBegAt.HasValue || cr.ProcessingBegAt.Value <= logTime)
											&&
											(!cr.ProcessingEndAt.HasValue || cr.ProcessingEndAt.Value >= logTime)
										)
								)
						)
					)
				);
		private class Event
		{
			public QuestionAnswerer                               QuestionAnswerer;
			public SatisfiedCompoundCondition                     SatisfiedConditions;
			public int                                            LogTime;
			public CoreServerDatabaseManager.CompoundRuleLogEntry LogEntry
			{
				get
				{
					return new CoreServerDatabaseManager.CompoundRuleLogEntry(
						SatisfiedConditions.Vehicle.VEHICLE_ID,
						LogTime,
						SatisfiedConditions.CompoundRule.CompoundRule_ID);
				}
			}
		}
		protected override bool Do()
		{
			using var entities = new Entities();
			// Текущее UTC/GMT время на сервере БД
			var currCheckTime = Server.Instance().DbManager.GetUtcDate();

			// 1. От объектов с Vehicle_Rule_Processing, но без активных правил отрезать Vehicle_Rule_Processing
			// Это решает следующую ситуацию:
			// У объекта наблюдения были правила, потом были удалены или отключены.
			// Объекты без активных правил не участвуют в обработке и даты в таблице Vehicle_Rule_Processing не обновляются.
			// При добавлении/включении правила, в таблице Vehicle_Rule_Processing указана старая дата
			// и система попытается обработать только что добавленное/включенное правило за все время с момента выключения/удаления правила
			var vrpsToDelete = GetVrpsToDelete(entities, currCheckTime.ToLogTime()).ToArray();
			if (vrpsToDelete.Any())
			{
				foreach (var vrpToDelete in vrpsToDelete)
					entities.Vehicle_Rule_Processing.DeleteObject(vrpToDelete);
				entities.SaveChanges(); // Нет привязки к сессии, т.к. исторические данные не изменяются
			}

			// Минимальное время обработки по всем машинам (самая старая обработка из действующих, т.к. не действующие удалены ранее)
			var lastCheckTime = Server.Instance().DbManager.GetLastRuleCheckDate();

			// В транзакции дополняем Vehicle_Rule_Processing
			using (var transaction = new Transaction())
			{
				var vehiclesToAddVrps = GetVehiclesToAddVrps(entities, currCheckTime.ToLogTime()).ToArray();
				foreach (var vehicle in vehiclesToAddVrps)
				{
					vehicle.Vehicle_Rule_Processing = new Vehicle_Rule_Processing
					{
						LastTime = currCheckTime,
						VEHICLE  = vehicle
					};
					entities.Vehicle_Rule_Processing.AddObject(vehicle.Vehicle_Rule_Processing);
				}
				entities.SaveChanges();
				transaction.Complete();
			}
			////////////////////////////////////////////////////////////////////////////////////////////////
			/// Если необходимо меняем время текущей проверки отталкиваясь от последней проверки и максимального интервала обработки
			/// Догоняем если есть отставание
			if (lastCheckTime != null && (currCheckTime - lastCheckTime.Value).TotalMinutes > _maxMinutesToProcess)
				currCheckTime = lastCheckTime.Value.AddMinutes(_maxMinutesToProcess);
			////////////////////////////////////////////////////////////////////////////////////////////////
			/// Трассируем время текущей проверки
			$"CheckTime={currCheckTime:yyMMdd-HHmmss.fff}(UTC)"
				.CallTraceInformation();
			////////////////////////////////////////////////////////////////////////////////////////////////
			var currentHostName = Environment.MachineName;
			var vehicles        = default(VEHICLE[]);
			using (var transaction = new Transaction())
			{
				// Поиск объектов наблюдения для которых есть записи в Vehicle_Rule_Processing
				vehicles = GetVehiclesToProcess(entities, currentHostName, currCheckTime, _maxVehicleCountToProcessByOneStep).ToArray();
				// Если объектов нет, то нечего и обрабатывать
				if (vehicles.Length == 0)
				{
					transaction.Complete();
					return false;
				}

				// Обновляем Vehicle_Rule_Processing, параметрами Host и текущего времени обработки
				var host = entities.GetOrCreateHostByName(currentHostName);
				foreach (var vehicle in vehicles)
				{
					var vrp = vehicle.Vehicle_Rule_Processing;
					if (vrp == null)
						continue;
					vrp.CurrentTime = currCheckTime;
					vrp.Host        = host;
				}

				entities.SaveChanges(); // Нет привязки к сессии, т.к. исторические данные не изменяются
				transaction.Complete();
			}

			// Получаем правила, которые должны исполняться на этом host
			var rules = GetRulesToProcess(entities, currentHostName, currCheckTime.ToLogTime()).ToArray();
			if (rules.Length == 0)
				return false;

			// Справочник сортированных списков правил по идентификатору объекта наблюдения
			var vidToRuleIds = new Dictionary<int, SortedSet<int>>();
			// Справочник ORM VEHICLE по идентификатору объекта наблюдения
			var vidToVehicle = new Dictionary<int, VEHICLE>();
			// Заполняем словари vidToRuleIds и vidToVehicle
			foreach (var rule in rules)
			{
				// Обход правил для объектов наблюдения
				foreach (var vehicle in rule.CompoundRule_Vehicle)
				{
					SortedSet<int> ruleIds;
					if (!vidToRuleIds.TryGetValue(vehicle.VEHICLE_ID, out ruleIds))
						vidToRuleIds.Add(vehicle.VEHICLE_ID, ruleIds = new SortedSet<int>());
					if (!ruleIds.Contains(rule.CompoundRule_ID))
						ruleIds.Add(rule.CompoundRule_ID);
					if (!vidToVehicle.ContainsKey(vehicle.VEHICLE_ID))
						vidToVehicle.Add(vehicle.VEHICLE_ID, vehicle);
				}
				// Обход правил для групп объектов наблюдения
				foreach (var group in rule.CompoundRule_VehicleGroup)
				{
					foreach (var vehicle in group.VEHICLE)
					{
						SortedSet<int> ruleIds;
						if (!vidToRuleIds.TryGetValue(vehicle.VEHICLE_ID, out ruleIds))
							vidToRuleIds.Add(vehicle.VEHICLE_ID, ruleIds = new SortedSet<int>());
						if (!ruleIds.Contains(rule.CompoundRule_ID))
							ruleIds.Add(rule.CompoundRule_ID);
						if (!vidToVehicle.ContainsKey(vehicle.VEHICLE_ID))
							vidToVehicle.Add(vehicle.VEHICLE_ID, vehicle);
					}
				}
			}
			// Список правил для исполнения, превращаем в справочник по идентификатору правила
			var rulesById = rules.ToDictionary(r => r.CompoundRule_ID);
			// Заполняем список запросов (правил), которые нужно вычислить
			var queries = new List<VehicleQuery>(vidToRuleIds.Count);
			foreach (var pair in vidToRuleIds)
			{
				var vehicleId = pair.Key;
				var query = new VehicleQuery
				{
					VehicleId = vehicleId
				};
				queries.Add(query);
				var ruleIds = pair.Value;

				foreach (var ruleId in ruleIds)
				{
					var rule = rulesById[ruleId];

					if (rule.CompoundRule_Sensor.Any() && query.SensorLegends == null)
						query.SensorLegends = new SortedSet<SensorLegend>();

					foreach (var sensorCondition in rule.CompoundRule_Sensor)
						query.SensorLegends.Add((SensorLegend)sensorCondition.CONTROLLER_SENSOR_LEGEND.Number);

					if (rule.CompoundRule_Speed.Any())
						query.Speed = true;

					if (rule.MotionStart)
						query.MotionStart = true;

					if ((rule.CompoundRule_Zone.Any() || rule.CompoundRule_ZoneGroup.Any()) && query.GeoZones == null)
						query.GeoZones = new SortedSet<int>();

					foreach (var zoneCondition in rule.CompoundRule_Zone)
						query.GeoZones.Add(zoneCondition.GEO_ZONE.ZONE_ID);
					foreach (var zoneGroupCondition in rule.CompoundRule_ZoneGroup)
					{
						foreach (var zone in zoneGroupCondition.ZONEGROUP.Members)
							query.GeoZones.Add(zone.Zone.ZONE_ID);
					}
				}
			}

			// Получаем по каждому обрабатываемому объекту наблюдения массив временных точек, которые ещё не обрабатывались
			var logTimeInfos = Server.Instance().DbManager
				.GetNewLogTimes(vidToRuleIds.Keys.ToArray(), currCheckTime)
				.ToDictionary(lti => lti.VehicleId);
			// Текущее время проверки, переводим в LogTime
			var currCheckLogTime = TimeHelper.GetSecondsFromBase(currCheckTime);

			// Перечень объектов наблюдения, для которых новых данных не поступило
			// Для этих объектов проверяется только правило "Отсутствие данных более N минут"
			var dataAbsenceVehicleIds = new HashSet<int>();
			foreach (var vehicle in vehicles)
			{
				if (logTimeInfos.ContainsKey(vehicle.VEHICLE_ID) ||
					!vidToRuleIds[vehicle.VEHICLE_ID]
						.Any(rid => rulesById[rid].CompoundRule_DataAbsence.Any()))
					continue;

				if (vehicle.Vehicle_Rule_Processing == null)
					continue;

				// Для формирования оповещений "Нет данных более N минут"
				var prevCheckTime = vehicle.Vehicle_Rule_Processing.LastTime;
				if (prevCheckTime != currCheckTime)
				{
					var logTimeInfo = new CoreServerDatabaseManager.LogTimeInfo(vehicle.VEHICLE_ID);
					logTimeInfo.NewLogTimes.Add(currCheckLogTime);
					logTimeInfos.Add(vehicle.VEHICLE_ID, logTimeInfo);
					dataAbsenceVehicleIds.Add(vehicle.VEHICLE_ID);
				}
			}

			// Готовим для обработки пары VehicleId и LogTime в виде VehicleLogTimeParam из logTimeInfos
			var logTimeParams = new List<VehicleLogTimeParam>();
			foreach (var logTimeInfo in logTimeInfos.Values)
			{
				if (!logTimeInfo.NewLogTimes.Any())
					continue;

				logTimeParams.AddRange(logTimeInfo.NewLogTimes.Select(logTime => new VehicleLogTimeParam(logTimeInfo.VehicleId, logTime)));
			}
			/////////////////////////////////////////////////////////////////////////////////
			//("logTimeParams:" + logTimeParams.ToJson()).CallTraceInformation();
			/////////////////////////////////////////////////////////////////////////////////
			// ОБРАБОТКА, если такие пары VehicleID и LogTime в виде VehicleLogTimeParam есть
			if (logTimeParams.Any()) // Обрабатываем, только если есть данные
			{
				#region Подготовка
				// Массив событий (результат обработки)
				var events = new List<Event>();
				// Пары VehicleId и ZoneId в виде VehicleZoneParam
				var geoZonesParams = new List<VehicleZoneParam>();
				// Пары VehicleId и SensorNumber в виде VehicleSensorParam
				var sensorParams = new List<CoreServerDatabaseManager.VehicleSensorParam>();
				// Массив VehicleId для которых есть условия с датчиками
				var havingSensorsVehicleIds = queries
					.Where(q => q.SensorLegends != null && q.SensorLegends.Any())
					.Select(q => q.VehicleId)
					.ToArray();
				// Справочник маппинга датчиков по VehicleId
				var vehicleSensorMaps =
					(from v in entities.VEHICLE
							.Include("CONTROLLER")
							.Include("CONTROLLER.CONTROLLER_SENSOR_MAP")
							.Include("CONTROLLER.CONTROLLER_SENSOR_MAP.CONTROLLER_SENSOR")
						from csm in v.CONTROLLER.CONTROLLER_SENSOR_MAP
						where havingSensorsVehicleIds.Contains(v.VEHICLE_ID)
						select new { v.VEHICLE_ID, csm })
						.ToLookup(item => item.VEHICLE_ID, item => item.csm);
				// Заполняем параметры из конкретных запросов, полученных из набора правил
				foreach (var query in queries)
				{
					var vehicleId = query.VehicleId;
					// По набору зон правила, заполняем список пар VehicleId и ZoneId
					if (query.GeoZones != null)
						geoZonesParams.AddRange(query.GeoZones.Select(zoneId => new VehicleZoneParam(vehicleId, zoneId)));
					// По набору датчиков правила, заполняем список пар VehicleId и SensorNumber
					if (query.SensorLegends != null && query.SensorLegends.Any())
					{
						var inputNumbers = new SortedSet<int>();
						foreach (var mapping in vehicleSensorMaps[vehicleId])
							inputNumbers.Add(mapping.CONTROLLER_SENSOR.NUMBER);
						sensorParams.AddRange(inputNumbers.Select(inputNumber => new CoreServerDatabaseManager.VehicleSensorParam(vehicleId, inputNumber)));
					}
				}
				// Набор VehicleId для которых есть условие Начала движения/остановок
				var vehicleIdsWithStopControl = new HashSet<int>(queries
					.Where(q => q.MotionStart)
					.Select(q => q.VehicleId));
				// Набор VehicleId для которых есть условие проверок скорости
				var vehicleIdsWithSpeedControl = new SortedSet<int>(queries
					.Where(q => q.Speed)
					.Select(q => q.VehicleId));
				// Справочник значений датчиков по VehicleId
				var inputValuesForPositions =Server.Instance().DbManager
					.GetInputValuesForPositions(sensorParams, logTimeParams, true)
					.ToLookup(item => item.VehicleId, item => item.Input);
				// Справочник позиций по VehicleId
				var positions = Server.Instance().DbManager
					.GetPositions(logTimeParams, true)
					.ToLookup(item => item.VehicleId);
				// Список пар VehicleId и LogTime для позиций
				var positionLogTimes = new List<VehicleLogTimeParam>();
				foreach (var group in positions)
				{
					foreach (var position in group)
						positionLogTimes.Add(new VehicleLogTimeParam(group.Key, position.LogTime));
				}
				// Справочник комбинаций VehicleId, LogTime, ZoneId по VehicleId (нахождение в зоне объекта в момент времени)
				var geoZonesForPositions = Server.Instance().DbManager
					.GetGeoZonesForVehicleLogTime(geoZonesParams, positionLogTimes)
					.ToLookup(item => item.VehicleId);
				// Справочник комбинаций VehicleId, LogTime, Speed по VehicleId (скорость объекта в момент времени)
				var speeds = Server.Instance().DbManager
					.GetSpeedForPositions(logTimeParams.Where(ltp => vehicleIdsWithSpeedControl.Contains(ltp.VehicleId)), true)
					.ToLookup(item => item.VehicleId);
				// Справочник LogTime по VehicleId (моменты времени остановок)
				var stops = Server.Instance().DbManager
					.GetStopsForPosition(positionLogTimes.Where(ltp => vehicleIdsWithStopControl.Contains(ltp.VehicleId)), StopDurationSeconds)
					.ToLookup(item => item.VehicleId, item => item.LogTime);
				// Список идентификаторов объектов для которых есть CompoundRule_TimePoint
				var withTimePointConditionVehicleIds = vidToRuleIds
					.Where(x => x.Value.Any(rid => rulesById[rid].CompoundRule_TimePoint.Count > 0))
					.Select(x => x.Key)
					.ToHashSet();
				/////////////////////////////////////////////////////////////////////////////////
				//("withTimePointConditionVehicleIds:" + withTimePointConditionVehicleIds.ToJson()).CallTraceInformation();
				/////////////////////////////////////////////////////////////////////////////////
				// Справочник(изменяемый) LogTime по VehicleId (моменты времени последней передачи данных для объектов с условиями отсутствия данных)
				var lastLogTimes = Server.Instance().DbManager
					.GetLastLogTimes(dataAbsenceVehicleIds.Union(withTimePointConditionVehicleIds), currCheckTime)
					.ToDictionary(item => item.VehicleId, item => item.LogTime);
				/////////////////////////////////////////////////////////////////////////////////
				//("lastLogTimes:" + lastLogTimes.Select(l => new { vid = l.Key, logTime = l.Value, locTime = l.Value.ToUtcDateTime().ToLocalTime() }).ToJson()).CallTraceInformation();
				/////////////////////////////////////////////////////////////////////////////////
				#endregion Подготовка
				// Перебор объектов для которых есть правила и их вычисления
				foreach (var vehicleId in vidToRuleIds.Keys)
				{
					CoreServerDatabaseManager.LogTimeInfo logTimeInfo;
					if (!logTimeInfos.TryGetValue(vehicleId, out logTimeInfo) || vidToVehicle[vehicleId].Vehicle_Rule_Processing == null)
						continue; //Вообще нет никаких записей по объекту наблюдения, проверять ничего не нужно

					// Предыдущее время обработки правил объекта наблюдения (дата)
					var vehicleRuleProcessingLastTime = vidToVehicle[vehicleId].Vehicle_Rule_Processing.LastTime;
					// Предыдущее время обработки правил объекта наблюдения (LogTime)
					var prevCheckLogTime = TimeHelper.GetSecondsFromBase(vehicleRuleProcessingLastTime);
					// Создание объекта вычислителя условий правил (заполняя его всей необходимой информацией)
					var questionAnswerer = new QuestionAnswerer(
						inputValuesForPositions[vehicleId],
						geoZonesForPositions[vehicleId],
						positions[vehicleId],
						speeds[vehicleId],
						vehicleSensorMaps[vehicleId],
						logTimeInfo.NewLogTimes,
						stops[vehicleId],
						lastLogTimes.ContainsKey(vehicleId) ? lastLogTimes[vehicleId] : (int?)null);
					/////////////////////////////////////////////////////////////////////////////////
					//("logTimeInfo:" + logTimeInfo.NewLogTimes.Select(l => l.ToUtcDateTime().ToLocalTime()).ToJson()).CallTraceInformation();
					/////////////////////////////////////////////////////////////////////////////////
					// Перебор новых моментов времени для объекта наблюдения
					foreach (var currLogTime in logTimeInfo.NewLogTimes)
					{
						// Перебор правил для объекта наблюдения и вычисления для момента времени
						foreach (var ruleId in vidToRuleIds[vehicleId])
						{
							// Текущее правило
							var compoundRule = rulesById[ruleId];

							// Для объектов наблюдения, которые проверяются в этот раз без новых данных, можно проверять только правило "Отсутствие данных"
							// И наоборот, правила, содержащие "Отсутствие данных" не должны проверяться вообще, если данные есть
							if (( dataAbsenceVehicleIds.Contains(vehicleId) && compoundRule.CompoundRule_DataAbsence.Count == 0) ||
								(!dataAbsenceVehicleIds.Contains(vehicleId) && compoundRule.CompoundRule_DataAbsence.Count != 0))
								continue;
							// Проверка отсутствия в правиле условий, если нет нечего проверять
							if (compoundRule.CompoundRule_Sensor.Count          == 0 &&
								compoundRule.CompoundRule_Speed.Count           == 0 &&
								compoundRule.CompoundRule_Time.Count            == 0 &&
								compoundRule.CompoundRule_TimePoint.Count       == 0 &&
								compoundRule.CompoundRule_Zone.Count            == 0 &&
								compoundRule.CompoundRule_ZoneGroup.Count       == 0 &&
								compoundRule.CompoundRule_DataAbsence.Count     == 0 &&
								compoundRule.CompoundRule_PositionAbsence.Count == 0 &&
								!compoundRule.MotionStart)
								continue;

							// Проверяем условие для времени текущей проверки
							var satisfiedConditions = GetSatisfiedCompoundCondition(compoundRule, questionAnswerer, currLogTime);
							if (satisfiedConditions == null)
								continue;

							// Все условия выполнены, нужно проверить, выполнялись ли эти условия в предыдущий момент времени
							var prevLogTime = satisfiedConditions.DataAbsence != null
								? prevCheckLogTime
								: questionAnswerer.GetPreviousLogTime(currLogTime);
							// TODO: Странное условие
							if (prevLogTime == null || GetSatisfiedCompoundCondition(compoundRule, questionAnswerer, prevLogTime.Value) != null)
								continue; //выполнимость правила не изменилась, значит, событие уже было проверено
							// Заполняем информацию об объекте наблюдения
							satisfiedConditions.Vehicle = vidToVehicle[vehicleId];
							// Добавляем событие
							events.Add(new Event
							{
								QuestionAnswerer    = questionAnswerer,
								SatisfiedConditions = satisfiedConditions,
								LogTime             = currLogTime
							});
						}
					}
				}

				if (events.Count != 0)
				{
					// Фильтруем события, убираем объекты на которые у оператора правила нет прав
					events = events
						.Where(e => e.SatisfiedConditions
							.CompoundRule
							.CompoundRule_Message_Template
							.Any(mt => IsAllowed(mt.OPERATOR.OPERATOR_ID, e.SatisfiedConditions.Vehicle.VEHICLE_ID))
						)
						.ToList();

					// Значение в БД, стоит 1 секунда, расчет ведется с частотой 1 секунда (точность LogTime)
					var eventDelta = entities.GetConstantAsInt(Constant.RuleEventIntervalSeconds) ?? 20*60;
					var unregisteredEvents = Server.Instance().DbManager
						.GetUnregisteredCompoundRuleEvents(events.Select(e => e.LogEntry), eventDelta);

					// Прореживаем события, чтобы появлялись не чаще eventDelta (бесполезное, eventDelta в базе стоит 1 секунда равна точности расчета)
					// в итоге sparsedEvents будет рано unregisteredEvents
					//TODO: оптимизировать, чтобы избежать сложности o(n^2)
					var sparsedEvents = new List<CoreServerDatabaseManager.CompoundRuleLogEntry>();
					foreach (var @event in unregisteredEvents)
					{
						if (sparsedEvents.Any(e =>
							e.VehicleID      == @event.VehicleID      &&
							e.CompoundRuleID == @event.CompoundRuleID &&
							Math.Abs(e.LogTime - @event.LogTime) <= eventDelta))
							continue;
						sparsedEvents.Add(@event);
					}
					// TODO: Похоже, что нужно использовать не events, а unregisteredEvents (при обработке на одном сервере, роли не играет)
					if (sparsedEvents.Count != 0 || events.Any(e => e.SatisfiedConditions.IsAlarm))
					{
						foreach (var @event in events.Where(e => sparsedEvents.Contains(e.LogEntry) || e.SatisfiedConditions.IsAlarm))
						{
							// Сообщения формируем только если дата события не старше суток от текущего момента времени
							if((currCheckTime - @event.LogTime.ToUtcDateTime()) < TimeSpan.FromDays(1))
								CreateNotificationMessages(@event.QuestionAnswerer, entities, @event.SatisfiedConditions, @event.LogTime);
						}

						// Регистрация событий в БД
						Server.Instance().DbManager.RegisterCompoundRuleEvents(sparsedEvents);
					}
				}
			}
			// Обновление Vehicle_Rule_Processing, завершение обработки
			foreach (var vehicle in vehicles)
			{
				if (vehicle.Vehicle_Rule_Processing == null)
					continue;
				vehicle.Vehicle_Rule_Processing.Host     = null;
				vehicle.Vehicle_Rule_Processing.LastTime = currCheckTime;
			}

			entities.SaveChanges(entities.GetOperator(_operatorLogin));

			return logTimeInfos.Count > 0;
		}

		/// <summary> Получить сработавшие условия правила в момент времени </summary>
		/// <param name="compoundRule"> Правило(ORM) </param>
		/// <param name="questionAnswerer"> Калькулятор условий </param>
		/// <param name="logTime"> Момент времени </param>
		/// <returns></returns>
		private static SatisfiedCompoundCondition GetSatisfiedCompoundCondition(
			CompoundRule compoundRule,
			QuestionAnswerer questionAnswerer,
			int logTime)
		{
			var dataAbsenceCondition = GetSatisfiedConditions(questionAnswerer, compoundRule.CompoundRule_DataAbsence, logTime).FirstOrDefault();
			if (compoundRule.CompoundRule_DataAbsence.Count != 0 && dataAbsenceCondition == null)
				return null;

			var positionAbsenceCondition = GetSatisfiedConditions(questionAnswerer, compoundRule.CompoundRule_PositionAbsence, logTime).FirstOrDefault();
			if (compoundRule.CompoundRule_PositionAbsence.Count != 0 && positionAbsenceCondition == null)
				return null;

			var motionStartCondition = GetMotionStartSatisfiedConditions(questionAnswerer, compoundRule, logTime).FirstOrDefault();
			if (compoundRule.MotionStart && motionStartCondition == null)
				return null;

			var sensorConditions = GetSatisfiedConditions(questionAnswerer, compoundRule.CompoundRule_Sensor, logTime).ToArray();
			if (compoundRule.CompoundRule_Sensor.Count != sensorConditions.Length)
				return null;

			var speedCondition = GetSatisfiedConditions(questionAnswerer, compoundRule.CompoundRule_Speed, logTime).FirstOrDefault();
			if (compoundRule.CompoundRule_Speed.Count != 0 && speedCondition == null)
				return null;

			var timeCondition = GetSatisfiedConditions(compoundRule.CompoundRule_Time, logTime).FirstOrDefault();
			if (compoundRule.CompoundRule_Time.Count != 0 && timeCondition == null)
				return null;

			var timePointCondition = GetSatisfiedConditions(questionAnswerer, compoundRule.CompoundRule_TimePoint, logTime).FirstOrDefault();
			if (compoundRule.CompoundRule_TimePoint.Count != 0 && timePointCondition == null)
				return null;

			var commonZoneConditions = compoundRule.CompoundRule_Zone.Where(zc => zc.Type != (int)ZoneEventType.OutOfZone).ToArray();
			var commonZoneGroupConditions = compoundRule.CompoundRule_ZoneGroup.Where(zgc => zgc.Type != (int)ZoneEventType.OutOfZone).ToArray();

			var outOfZoneConditions = compoundRule.CompoundRule_Zone.Where(zc => zc.Type == (int)ZoneEventType.OutOfZone).ToArray();
			var outOfZoneGroupConditions = compoundRule.CompoundRule_ZoneGroup.Where(zgc => zgc.Type == (int)ZoneEventType.OutOfZone).ToArray();

			CompoundRule_Zone satisfiedZoneCondition = null;
			SatisfiedZoneGroupCondition satisfiedZoneGroupCondition = null;

			if (outOfZoneConditions.Any() || outOfZoneGroupConditions.Any())
			{
				var zoneIds = new SortedSet<int>();
				foreach (var zoneId in outOfZoneConditions.Select(zc => zc.GEO_ZONE.ZONE_ID))
					zoneIds.Add(zoneId);
				foreach (var zoneId in outOfZoneGroupConditions
					.Select(zgc => zgc.ZONEGROUP)
					.SelectMany(zoneGroup => zoneGroup.Members.Select(zgz => zgz.Zone.ZONE_ID)))
				{
					zoneIds.Add(zoneId);
				}

				var positionLogTime = questionAnswerer.HasPosition(logTime)
					? logTime
					: questionAnswerer.GetPreviousGeoLogTime(logTime);

				if (positionLogTime != null && !zoneIds.Any(zoneID => questionAnswerer.IsInZone(positionLogTime.Value, zoneID)))
				{
					satisfiedZoneCondition = outOfZoneConditions.FirstOrDefault();
					if (satisfiedZoneCondition == null)
					{
						var zoneGroupCondition = outOfZoneGroupConditions.First(zgc => zgc.ZONEGROUP.Members.Any());
						var geoZone = zoneGroupCondition.ZONEGROUP.Members.Count == 1
							? zoneGroupCondition.ZONEGROUP.Members.Single()
							: null;
						satisfiedZoneGroupCondition = new SatisfiedZoneGroupCondition(
							zoneGroupCondition, geoZone != null ? geoZone.Zone : null);
					}
				}
			}

			// Не отработало ни одно условие "Вне геозоны" и есть общие условия по геозонам, нужно проверить выполнение общих условий
			if (satisfiedZoneCondition == null && satisfiedZoneGroupCondition == null &&
				(commonZoneConditions.Any() || commonZoneGroupConditions.Any()))
			{
				satisfiedZoneCondition = GetSatisfiedConditions(commonZoneConditions, questionAnswerer, logTime).FirstOrDefault();
				if (satisfiedZoneCondition == null)
					satisfiedZoneGroupCondition = GetSatisfiedConditions(commonZoneGroupConditions, questionAnswerer, logTime).FirstOrDefault();
			}

			// Есть условия по геозонам, но ни одно не выполнилось, ни через группу зон, ни через зону
			if ((compoundRule.CompoundRule_Zone.Any() || compoundRule.CompoundRule_ZoneGroup.Any())
				&& satisfiedZoneCondition == null && satisfiedZoneGroupCondition == null)
			{
				return null;
			}

			return new SatisfiedCompoundCondition
			{
				CompoundRule    = compoundRule,
				Sensors         = sensorConditions,
				Speed           = speedCondition,
				Time            = timeCondition,
				TimePoint       = timePointCondition,
				Zone            = satisfiedZoneCondition,
				ZoneGroup       = satisfiedZoneGroupCondition,
				DataAbsence     = dataAbsenceCondition,
				PositionAbsence = positionAbsenceCondition
			};
		}
		private static IEnumerable<CompoundRule_PositionAbsence> GetSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			IEnumerable<CompoundRule_PositionAbsence> conditions,
			int logTime)
		{
			foreach (var condition in conditions)
			{
				var previuosGeoLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);
				var positionAbsenceSeconds = logTime - previuosGeoLogTime;
				if (condition.Minutes * 60 < positionAbsenceSeconds)
					yield return condition;
			}
		}
		private static IEnumerable<CompoundRule> GetMotionStartSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			CompoundRule condition,
			int logTime)
		{
			if (!condition.MotionStart)
				yield break;

			var previuosGeoLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);
			if (previuosGeoLogTime == null)
				yield break;
			var previousPosition = questionAnswerer.GetPosition(previuosGeoLogTime.Value);
			if (previousPosition == null)
				yield break;

			if (!questionAnswerer.IsStop(previousPosition.Value.LogTime))
				yield break;

			var currentPosition = questionAnswerer.GetPosition(logTime);
			if (currentPosition == null)
				yield break;
			if (questionAnswerer.IsStop(currentPosition.Value.LogTime))
				yield break;

			yield return condition;
		}
		private static IEnumerable<CompoundRule_DataAbsence> GetSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			IEnumerable<CompoundRule_DataAbsence> conditions,
			int logTime)
		{
			foreach (var condition in conditions)
			{
				var previuosLogTime = questionAnswerer.GetPreviousLogTime(logTime);
				var dataAbsenceSeconds = logTime - previuosLogTime;
				if (condition.Minutes * 60 < dataAbsenceSeconds)
					yield return condition;
			}
		}
		private static IEnumerable<CompoundRule_Sensor> GetSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			IEnumerable<CompoundRule_Sensor> conditions,
			int logTime)
		{
			return (from sensorRule in conditions
					let value = questionAnswerer.GetSensorValue(logTime, (SensorLegend)sensorRule.CONTROLLER_SENSOR_LEGEND.Number)
					where value != null
					where ComparsionIsSucceeded((ScalarComparisonType)sensorRule.Comparison_Type, value.Value, sensorRule.Value)
					select sensorRule);
		}
		private static IEnumerable<CompoundRule_Speed> GetSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			IEnumerable<CompoundRule_Speed> conditions,
			int logTime)
		{
			return (from speedRule in conditions
					let speed = questionAnswerer.GetSpeed(logTime)
					where speed != null
					where ComparsionIsSucceeded((ScalarComparisonType)speedRule.Type, speed.Value, speedRule.Value)
					select speedRule);
		}
		private static IEnumerable<CompoundRule_Zone> GetSatisfiedConditions(
			IEnumerable<CompoundRule_Zone> zoneRules,
			QuestionAnswerer questionAnswerer,
			int logTime)
		{
			if (!questionAnswerer.HasPosition(logTime))
			{
				var assumedLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);
				if (assumedLogTime == null)
					yield break;
				logTime = assumedLogTime.Value;
			}

			var previousLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);

			foreach (var zr in
				(from zoneRule in zoneRules
				 let zoneId = zoneRule.GEO_ZONE.ZONE_ID
				 let zoneEventType = (ZoneEventType)zoneRule.Type
				 where CheckZoneEventType(questionAnswerer, zoneEventType, zoneId, logTime, previousLogTime)
				 select zoneRule))
			{
				yield return zr;
			}
		}
		private static IEnumerable<SatisfiedZoneGroupCondition> GetSatisfiedConditions(
			IEnumerable<CompoundRule_ZoneGroup> zoneGroupRules,
			QuestionAnswerer questionAnswerer,
			int logTime)
		{
			if (!questionAnswerer.HasPosition(logTime))
			{
				var assumedLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);
				if (assumedLogTime == null)
					yield break;
				logTime = assumedLogTime.Value;
			}

			var previousLogTime = questionAnswerer.GetPreviousGeoLogTime(logTime);

			foreach (var zoneGroupRule in zoneGroupRules)
			{
				foreach (var zone in zoneGroupRule.ZONEGROUP.Members)
				{
					if (CheckZoneEventType(
							questionAnswerer,
							(ZoneEventType)zoneGroupRule.Type,
							zone.Zone.ZONE_ID,
							logTime,
							previousLogTime))
						yield return new SatisfiedZoneGroupCondition(zoneGroupRule, zone.Zone);
				}
			}
		}
		private static IEnumerable<CompoundRule_Time> GetSatisfiedConditions(IEnumerable<CompoundRule_Time> conditions, int logTime)
		{
			foreach (var timeRule in conditions)
			{
				var timeZoneCode = timeRule.TimeZone.Code;
				var messageTemplate = timeRule.CompoundRule.CompoundRule_Message_Template.FirstOrDefault();
				if (messageTemplate != null && messageTemplate.OPERATOR != null &&
					messageTemplate.OPERATOR.TimeZone != null)
				{
					timeZoneCode = messageTemplate.OPERATOR.TimeZone.Code;
				}

				var localTimeOfDay = TimeHelper.GetLocalTime(logTime, TimeZoneInfo.FindSystemTimeZoneById(timeZoneCode)).TimeOfDay;
				var from = TimeSpan.FromHours(timeRule.Hours_From).Add(TimeSpan.FromMinutes(timeRule.Minutes_From));
				var to = TimeSpan.FromHours(timeRule.Hours_To).Add(TimeSpan.FromMinutes(timeRule.Minutes_To + 1)); //Минута целиком, далее строгое неравенство

				if (to < from)
				{
					if (localTimeOfDay < to || from <= localTimeOfDay)
						yield return timeRule;
					yield break;
				}

				if (from <= localTimeOfDay && localTimeOfDay < to)
					yield return timeRule;
			}
		}
		private static IEnumerable<CompoundRule_TimePoint> GetSatisfiedConditions(
			QuestionAnswerer questionAnswerer,
			IEnumerable<CompoundRule_TimePoint> conditions,
			int logTime)
		{
			var beg = questionAnswerer.GetPreviousLogTime(logTime);
			var end = logTime;
			foreach (var timePointRule in conditions)
			{
				var rul = timePointRule.LogTime;
				/////////////////////////////////////////////////////////////////////////////////
				//("1: " + (beg: beg, end: end, rul: rul).ToJson()).CallTraceInformation();
				//("1: " + (new[] { beg.Value, end, rul }).Select(l => l.ToUtcDateTime().ToLocalTime()).ToJson()).CallTraceInformation();
				//("1: " + questionAnswerer.ToString()).CallTraceInformation();
				/////////////////////////////////////////////////////////////////////////////////
				if (beg < rul && rul <= end)
				{
					/////////////////////////////////////////////////////////////////////////////////
					//("2: " + (beg: beg, end: end, rul: rul).ToJson()).CallTraceInformation();
					//("2: " + (new[] { beg.Value, end, rul }).Select(l => l.ToUtcDateTime().ToLocalTime()).ToJson()).CallTraceInformation();
					//("2: " + questionAnswerer.ToString()).CallTraceInformation();
					/////////////////////////////////////////////////////////////////////////////////
					yield return timePointRule;
				}
			}
		}
		private void CreateNotificationMessages(
			QuestionAnswerer questionAnswerer,
			Entities entities, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime)
		{
			foreach (var messageOption in satisfiedCompoundRule.CompoundRule.CompoundRule_Message_Template)
			{
				if (!messageOption.OPERATORReference.IsLoaded)
					messageOption.OPERATORReference.Load();

				if (messageOption.OPERATOR == null)
					continue;

				var eventText   = GetEventDescription(questionAnswerer, messageOption.OPERATOR, satisfiedCompoundRule, logTime);
				var eventDate   = GetEventDate(logTime, messageOption.OPERATOR);
				var vehicleName = GetVehicleName(messageOption.OPERATOR, satisfiedCompoundRule);
				var eventDesc   = eventText;

				GenerateWebClientNotifications(entities, messageOption, vehicleName, eventDesc, satisfiedCompoundRule, logTime, eventText, eventDate);
				GenerateAppClientNotifications(entities, messageOption, vehicleName, eventDesc, satisfiedCompoundRule, logTime, eventText, eventDate);

				var subject = eventDate + " " + vehicleName + ": " + eventDesc;
				GenerateEmails(entities, satisfiedCompoundRule, messageOption, subject, questionAnswerer, logTime);
				GenerateSMS   (entities, satisfiedCompoundRule, messageOption, subject, questionAnswerer, logTime);
			}
		}
		private struct OperatorVehicleAccess
		{
			public readonly int OperatorId;
			public readonly int VehicleId;
			public OperatorVehicleAccess(int operatorId, int vehicleId)
			{
				OperatorId = operatorId;
				VehicleId  = vehicleId;
			}
			public override bool Equals(object obj)
			{
				if (obj == null) return false;
				if (obj.GetType() != GetType())
					return false;
				var other = (OperatorVehicleAccess) obj;
				return Equals(other);
			}
			public bool Equals(OperatorVehicleAccess other)
			{
				return OperatorId == other.OperatorId && VehicleId == other.VehicleId;
			}
			public override int GetHashCode()
			{
				unchecked
				{
					return (OperatorId*397) ^ VehicleId;
				}
			}
		}
		private bool IsAllowed(int operatorId, int vehicleId)
		{
			var key = new OperatorVehicleAccess(operatorId, vehicleId);
			return _accessCache[key];
		}
		private readonly Entities                                 _allowanceEntities;
		private readonly TimingCache<OperatorVehicleAccess, bool> _accessCache;
		private bool IsAllowedInDb(OperatorVehicleAccess key)
		{
			return _allowanceEntities.VEHICLE.Any(
				v => v.VEHICLE_ID == key.VehicleId &&
					 v.Accesses.Any(a => a.operator_id == key.OperatorId &&
										 a.right_id == (int) SystemRight.VehicleAccess) &&
					 !v.Accesses.Any(a => a.operator_id == key.OperatorId &&
										  a.right_id == (int) SystemRight.Ignore));
		}
		private MESSAGE GenerateWebClientNotifications(Entities entities, CompoundRule_Message_Template messageOption, string vehicleName, string eventText, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime, string text, string eventDate)
		{
			var message = entities.CreateWebNotification(messageOption.OPERATOR.OPERATOR_ID, vehicleName, eventText);
			var templateName = Msg::MessageTemplate.GenericNotification;
			if (satisfiedCompoundRule.IsAlarm)
				templateName = Msg::MessageTemplate.AlarmNotification;
			var template = entities.MESSAGE_TEMPLATE.FirstOrDefault(mt => mt.NAME == templateName);

			AddVehicleId(entities, message, satisfiedCompoundRule.Vehicle.VEHICLE_ID);
			if (null != template)
				message.MESSAGE_TEMPLATE = template;
			entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.Date, TimeHelper.GetDateTimeUTC(logTime));
			entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.MessageText, text);

			return message;
		}
		private static void GenerateAppClientNotifications(Entities entities, CompoundRule_Message_Template messageOption, string vehicleName, string eventText, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime, string text, string eventDate)
		{
			if (!messageOption.ToAppClient)
				return;

			var ormOperator = messageOption.OPERATOR;
			var messages = entities.CreateAppNotification(ormOperator.OPERATOR_ID, vehicleName, eventText, eventDate);

			if (messages.Count == 0)
				return;

			var templateName = Msg::MessageTemplate.GenericNotification;
			if (satisfiedCompoundRule.IsAlarm)
				templateName = Msg::MessageTemplate.AlarmNotification;

			var template = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == templateName);
			foreach (var message in messages)
			{
				AddVehicleId(entities, message, satisfiedCompoundRule.Vehicle.VEHICLE_ID);
				message.MESSAGE_TEMPLATE = template;
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.Date, TimeHelper.GetDateTimeUTC(logTime));
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.MessageText, text);
			}
		}
		private static void GenerateSMS(Entities entities, SatisfiedCompoundCondition satisfiedCompoundRule, CompoundRule_Message_Template messageOption, string subject, QuestionAnswerer questionAnswerer, int logTime)
		{
			if (!messageOption.OPERATOR.Contacts.IsLoaded)
				messageOption.OPERATOR.Contacts.Load();

			var smsBodyBuilder = new StringBuilder(subject);
			var operatorId = messageOption.OPERATOR.OPERATOR_ID;
			var vehicleID = satisfiedCompoundRule.Vehicle.VEHICLE_ID;
			if (questionAnswerer.HasPicture(logTime))
			{
				smsBodyBuilder.Append(" ");
				smsBodyBuilder.Append(GetPictureUrl(logTime, vehicleID));
			}

			var smsBody = smsBodyBuilder.ToString();
			var template = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.GenericNotification);

			var contacts = messageOption.CompoundRule.Contacts
				.Where(c => c.Contact.Type == (int)ContactType.Phone)
				.Select(c => c.Contact).ToList();
			if (messageOption.ToOwnPhone)
			{
				var ownContact = entities.GetOwnAsidContact(operatorId) ?? entities.GetOwnPhoneContact(operatorId);
				if (ownContact != null)
					contacts.Add(ownContact);
			}

			foreach (var contact in contacts)
			{
				var smsMessages = entities.CreateOutgoingSms(contact, smsBody);
				foreach (var sms in smsMessages)
				{
					sms.Owner_Operator_ID = operatorId;
					sms.MESSAGE_TEMPLATE  = template;
					AddVehicleId(entities, sms, vehicleID);
				}

				Trace.TraceInformation(
					"{0}: Sending notification to {1} - {2}",
					satisfiedCompoundRule.CompoundRule.CompoundRule_ID,
					contact,
					smsBody);
			}
		}
		private static void AddVehicleId(Entities entities, MESSAGE sms, int vehicleId)
		{
			entities.AddMessageField(sms, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId);
		}
		private static void GenerateEmails(
			Entities entities,
			SatisfiedCompoundCondition satisfiedCompoundRule,
			CompoundRule_Message_Template messageOption, string subject, QuestionAnswerer questionAnswerer, int logTime)
		{
			var emails = satisfiedCompoundRule.CompoundRule.Contacts
				.Where(c => c.Contact.Type == (int)ContactType.Email)
				.Select(c => c.Contact)
				.ToList();

			if (messageOption.ToOwnEmail)
			{
				var operatorId = messageOption.OPERATOR.OPERATOR_ID;
				var ownEmails =
					entities.Operator_Contact.Where(
						c =>
							c.Confirmed && c.Operator_ID == operatorId &&
							c.Contact.Type == (int)ContactType.Email)
					.Select(c => c.Contact)
					.ToArray();
				emails.AddRange(ownEmails);
			}

			if (emails.Count == 0)
				return;

			List<KeyValuePair<string, string>> fields;
			var body = GetEmailMessageBodyHtml(
				questionAnswerer,
				messageOption.OPERATOR,
				satisfiedCompoundRule,
				logTime,
				subject,
				out fields);


			foreach (var contact in emails)
			{
				var message = entities.CreateOutgoingMessage(contact, subject, body);
				message.OwnerOperator = messageOption.OPERATOR;
				if (fields != null)
				{
					foreach (var pair in fields)
					{
						var field = new MESSAGE_FIELD
							{
								MESSAGE = message,
								MESSAGE_TEMPLATE_FIELD =
									entities.MESSAGE_TEMPLATE_FIELD.First(tf => tf.NAME == pair.Key)
							};

						field.SetContent(pair.Value);

						entities.MESSAGE_FIELD.AddObject(field);
					}
				}

				Trace.TraceInformation(
					"{0}: Sending notification to {1} - {2}",
					satisfiedCompoundRule.CompoundRule.CompoundRule_ID,
					contact,
					subject);
			}
		}
		private static IEnumerable<string> GetMessageSubjectParts(QuestionAnswerer questionAnswerer, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime, ResourceContainers resourceContainers)
		{

			if (satisfiedCompoundRule.IsAlarm)
			{
				yield return resourceContainers.GetLocalizedMessagePart("Alarm").ToUpper();
			}

			if (satisfiedCompoundRule.DataAbsence != null)
			{
				yield return string.Format(resourceContainers.GetLocalizedMessagePart("NoDataNMinutes"), satisfiedCompoundRule.DataAbsence.Minutes);
			}

			if (satisfiedCompoundRule.PositionAbsence != null)
			{
				yield return string.Format(resourceContainers.GetLocalizedMessagePart("NoPositionNMinutes"), satisfiedCompoundRule.PositionAbsence.Minutes);
			}

			if (satisfiedCompoundRule.CompoundRule.MotionStart)
			{
				yield return resourceContainers.GetLocalizedMessagePart("MotionStart");
			}

			if (satisfiedCompoundRule.Speed != null)
			{
				string formatString;
				switch ((ScalarComparisonType)satisfiedCompoundRule.Speed.Type)
				{
					case ScalarComparisonType.More:
						formatString = resourceContainers.GetLocalizedMessagePart("OverspeedingFormatString");
						break;
					case ScalarComparisonType.Less:
						formatString = resourceContainers.GetLocalizedMessagePart("UnderspeedingFormatString");
						break;
					default:
						formatString = resourceContainers.GetLocalizedMessagePart("CommonSpeedViolationFormatString");
						break;
				}
				yield return string.Format(formatString, satisfiedCompoundRule.Speed.Value, questionAnswerer.GetSpeed(logTime));
			}

			var usedSensorLegends = new SortedSet<SensorLegend>();
			foreach (var sensor in satisfiedCompoundRule.Sensors)
			{
				var sensorLegend = (SensorLegend)sensor.CONTROLLER_SENSOR_LEGEND.Number;
				if (sensorLegend == SensorLegend.Alarm && sensor.Value == 1)
					continue; // Тревожное сообщение имеет наивысший приоритет и отображается всегда в начале сообщения
				if (usedSensorLegends.Contains(sensorLegend))
					continue;
				usedSensorLegends.Add(sensorLegend);

				var sensorType = (SensorType)sensor.CONTROLLER_SENSOR_LEGEND.CONTROLLER_SENSOR_TYPE.CONTROLLER_SENSOR_TYPE_ID;
				var sensorModel = new Sensor
				{
					Number = sensorLegend,
					Type   = sensorType,
				}
					.LocalizeSensorMeta(resourceContainers);
				yield return sensorModel.Name + " " + sensorModel.GetDisplayedValue(sensor.Value, resourceContainers);
			}

			if (satisfiedCompoundRule.Zone != null)
				yield return
					GetZoneEventTypeName(resourceContainers, (ZoneEventType)satisfiedCompoundRule.Zone.Type) + " " +
					satisfiedCompoundRule.Zone.GEO_ZONE.NAME;

			if (satisfiedCompoundRule.ZoneGroup != null)
			{
				var sb = new StringBuilder();
				sb.Append(GetZoneEventTypeName(resourceContainers, (ZoneEventType)satisfiedCompoundRule.ZoneGroup.ZoneGroupCondition.Type));
				sb.Append(" ");
				if (satisfiedCompoundRule.ZoneGroup.Zone != null)
				{
					sb.Append(satisfiedCompoundRule.ZoneGroup.Zone.NAME);
					sb.Append(" ");
				}
				sb.Append(resourceContainers.GetLocalizedMessagePart("OfTheGroup"));
				sb.Append(" ");
				sb.Append(satisfiedCompoundRule.ZoneGroup.ZoneGroupCondition.ZONEGROUP.NAME);
				yield return sb.ToString();
			}

			var time = satisfiedCompoundRule.Time;
			if (time != null)
			{
				var fromTime =
					TimeSpan.FromHours(time.Hours_From)
					.Add(TimeSpan.FromMinutes(time.Minutes_From));
				var toTime = TimeSpan.FromHours(time.Hours_To)
					.Add(TimeSpan.FromMinutes(time.Minutes_To));
				yield return
					resourceContainers.GetLocalizedMessagePart("FromTime") + " " +
					fromTime.FormatTimeOfDay()                             + " " +
					resourceContainers.GetLocalizedMessagePart("ToTime")   + " " +
					toTime.FormatTimeOfDay();
			}
		}
		private static string GetZoneEventTypeName(ResourceContainers resourceContainers, ZoneEventType zoneEventType)
		{
			switch (zoneEventType)
			{
				case ZoneEventType.Incoming:  return resourceContainers.GetLocalizedMessagePart("IncomingToZone");
				case ZoneEventType.Outgoing:  return resourceContainers.GetLocalizedMessagePart("OutgoingFromZone");
				case ZoneEventType.InZone:    return resourceContainers.GetLocalizedMessagePart("InZone");
				case ZoneEventType.OutOfZone: return resourceContainers.GetLocalizedMessagePart("OutOfZone");
				default:
					throw new NotSupportedException(zoneEventType.ToString());
			}
		}
		private string GetVehicleName(OPERATOR @operator, SatisfiedCompoundCondition satisfiedCompoundRule)
		{
			var resourcesContainers = ResourceContainers.Get(
				GetCultureInfo(@operator, satisfiedCompoundRule));

			var sb = new StringBuilder();

			if (!satisfiedCompoundRule.Vehicle.DEPARTMENTReference.IsLoaded)
				satisfiedCompoundRule.Vehicle.DEPARTMENTReference.Load();
			if (satisfiedCompoundRule.Vehicle.DEPARTMENT != null)
			{
				if (!satisfiedCompoundRule.Vehicle.DEPARTMENT.CountryReference.IsLoaded)
					satisfiedCompoundRule.Vehicle.DEPARTMENT.CountryReference.Load();
				if (satisfiedCompoundRule.Vehicle.DEPARTMENT.Country != null &&
					satisfiedCompoundRule.Vehicle.DEPARTMENT.Country.Is(Country.India))
				{
					sb.Append(resourcesContainers.GetLocalizedMessagePart("VehicleNo"));
					sb.Append(" ");
				}
			}

			//Объект наблюдения
			sb.Append(_vehicleNameCache[new Tuple<int, int>(@operator.OPERATOR_ID, satisfiedCompoundRule.Vehicle.VEHICLE_ID)]);

			return sb.ToString();
		}
		private static string GetEventDate(int logTime, OPERATOR ormOperator)
		{
			//Дата события
			return TimeHelper.GetLocalTime(logTime, ormOperator.TimeZoneInfoInstance)
				.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
		}
		private static string GetEventDescription(
			QuestionAnswerer questionAnswerer,
			OPERATOR ormOperator, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime)
		{
			var resourcesContainers = ResourceContainers.Get(
				GetCultureInfo(ormOperator, satisfiedCompoundRule));

			var sb = new StringBuilder(256);

			var firstPart = true;
			foreach (var part in GetMessageSubjectParts(questionAnswerer, satisfiedCompoundRule, logTime, resourcesContainers))
			{
				if (firstPart)
				{
					firstPart = false;
				}
				else
				{
					sb.Append(", ");
				}
				sb.Append(part);
			}

			//Для тревоги по возможности указать место, где произошло событие
			if (satisfiedCompoundRule.IsAlarm)
			{
				AppendPosition(questionAnswerer, logTime, sb, resourcesContainers);
			}

			return sb.ToString();
		}
		private static CultureInfo GetCultureInfo(OPERATOR ormOperator, SatisfiedCompoundCondition satisfiedCompoundRule)
		{
			CultureInfo culture = null;
			if (!ormOperator.CultureReference.IsLoaded)
				ormOperator.CultureReference.Load();
			if (ormOperator.Culture != null)
			{
				culture = CultureInfo.GetCultureInfo(ormOperator.Culture.Code);
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentCulture;

				Trace.TraceWarning("{0}: Unable to determine operator's [{1}] culture, using current system's one {2}",
					satisfiedCompoundRule.CompoundRule.CompoundRule_ID, ormOperator.OPERATOR_ID, culture);
			}
			return culture;
		}
		private static void AppendPosition(QuestionAnswerer questionAnswerer,
			int logTime, StringBuilder sb, ResourceContainers resourceContainers)
		{
			var position = questionAnswerer.GetPosition(logTime);
			sb.Append(" ");
			if (position == null)
			{
				sb.Append(resourceContainers.GetLocalizedMessagePart("UnknownLocation"));
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(resourceContainers.GetLocalizedMessagePart("FromLocation")))
				{
					sb.Append(resourceContainers.GetLocalizedMessagePart("FromLocation"));
					sb.Append(" ");
				}
				sb
					.Append(position.Value.Lat.ToString(NumberHelper.FormatInfo))
					.Append(" ")
					.Append(position.Value.Lng.ToString(NumberHelper.FormatInfo));

				var addressString = GeoHelper.GetAddressString(
					lat:      (double)position.Value.Lat,
					lng:      (double)position.Value.Lng,
					language: resourceContainers.CultureInfo.TwoLetterISOLanguageName);
				if (!string.IsNullOrWhiteSpace(addressString))
				{
					sb.Append(" ")
					  .Append(addressString);
				}

				if (60 <= logTime - position.Value.LogTime)
				{
					sb.Append(" (")
						.AppendTimeInterval(TimeSpan.FromSeconds(logTime - position.Value.LogTime), resourceContainers.CultureInfo)
						.Append(" ")
						.Append(resourceContainers.GetLocalizedMessagePart("TimeSpanBefore"))
						.Append(")");
				}
			}
		}
		private static string GetEmailMessageBodyHtml(
			QuestionAnswerer questionAnswerer,
			OPERATOR ormOperator, SatisfiedCompoundCondition satisfiedCompoundRule, int logTime, string subject, out List<KeyValuePair<string, string>> fields)
		{
			fields = null;

			var body = new XElement("div");

			body.Add(new XElement("p", subject));

			if (questionAnswerer.HasPicture(logTime))
			{
				fields = new List<KeyValuePair<string, string>>();

				var vehicleID = satisfiedCompoundRule.Vehicle.VEHICLE_ID;
				var fileName = string.Format(
					"camera{0}.{1}.jpg",
					vehicleID,
					TimeHelper.GetLocalTime(logTime, ormOperator.TimeZoneInfoInstance)
							  .ToString(TimeHelper.FileCompatibleTimeFormat));

				fields.Add(new KeyValuePair<string, string>(
					MESSAGE_TEMPLATE_FIELD.PictureName,
					fileName));
				fields.Add(new KeyValuePair<string, string>(
					MESSAGE_TEMPLATE_FIELD.PictureVehicleID,
					vehicleID.ToString(CultureInfo.InvariantCulture)));
				fields.Add(new KeyValuePair<string, string>(
					MESSAGE_TEMPLATE_FIELD.PictureLogTime,
					logTime.ToString(CultureInfo.InvariantCulture)));

				//TODO: брать адрес сервера из настроек пользователя
				body.Add(
					new XElement("a",
								 new XAttribute("href",
												GetPictureUrl(logTime, vehicleID))),

					new XElement("img", new XAttribute("src", "cid:" + fileName)));
			}

			return body.ToString();
		}
		private static string GetPictureUrl(int logTime, int vehicleID)
		{
			return Server.Instance().UrlWebGis + "/Picture.aspx?objectId=" + vehicleID + "&logTime=" + logTime;
		}
		private static bool CheckZoneEventType(QuestionAnswerer questionAnswerer, ZoneEventType zoneEventType, int zoneId, int logTime, int? previousLogTime)
		{
			switch (zoneEventType)
			{
				case ZoneEventType.Incoming:
					if (previousLogTime == null)
					{
						Trace.TraceWarning("Checking {0} without previous position on {1}", zoneEventType, logTime);
						return false;
					}

					return questionAnswerer.IsInZone(logTime, zoneId) && !questionAnswerer.IsInZone(previousLogTime.Value, zoneId);
				case ZoneEventType.Outgoing:
					if (previousLogTime == null)
					{
						Trace.TraceWarning("Checking {0} without previous position on {1}", zoneEventType, logTime);
						return false;
					}

					return !questionAnswerer.IsInZone(logTime, zoneId) && questionAnswerer.IsInZone(previousLogTime.Value, zoneId);
				case ZoneEventType.InZone:
					return questionAnswerer.IsInZone(logTime, zoneId);
				case ZoneEventType.OutOfZone:
					return !questionAnswerer.IsInZone(logTime, zoneId);
				default:
					return false;
			}
		}
		private static bool ComparsionIsSucceeded(ScalarComparisonType scalarComparisonType, decimal x, decimal y)
		{
			switch (scalarComparisonType)
			{
				case ScalarComparisonType.Equal:
					return x == y;
				case ScalarComparisonType.Less:
					return x < y;
				case ScalarComparisonType.EqualOrLess:
					return x <= y;
				case ScalarComparisonType.More:
					return x > y;
				case ScalarComparisonType.EqualOrMore:
					return x >= y;
				case ScalarComparisonType.NotEqual:
					return x != y;
			}
			return false;
		}
	}
}