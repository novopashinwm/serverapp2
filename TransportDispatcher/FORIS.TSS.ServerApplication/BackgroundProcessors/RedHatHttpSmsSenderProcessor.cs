﻿using System.Configuration;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.SmsService;
using FORIS.TSS.ServerApplication.SmsService.Configuration;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	sealed class RedHatHttpSmsSenderProcessor : MessageSender
	{
		public RedHatHttpSmsSenderProcessor()
			: base(MessageDestinationType.RedHatSmsGateway, 1, ConfigHelper.GetAppSettingAsBoolean("RedHatSenderProcessor.Enabled"))
		{
			BeforeStart += delegate
			{
				if (!Enabled)
					return;
				if (Config == null)
					return;
				if (!string.IsNullOrWhiteSpace(Config.ReceiveSmsUri))
				{
					_receiver = new RedHatHttpSmsReceiver(Config.ReceiveSmsUri);
					_receiver.Start();
				}
			};

			AfterStop += delegate
			{
				if (_receiver == null)
					return;
				_receiver.Stop();
				_receiver = null;
			};
		}

		private static SmsProviderRedHatSectionHandler Config
		{
			get { return ConfigurationManager.GetSection("SmsProviderRedHat") as SmsProviderRedHatSectionHandler; }
		}

		public override void Initialize()
		{
		}

		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{
			var smsSender     = RedHatHttpSmsSender.Instance;
			var sendSmsResult = smsSender.SendSms(messageTuple);
			var result        = GetProcessingResult(sendSmsResult);
			return result;
		}

		public override bool CanSendTo(Contact contact)
		{
			return contact.Type == (int)ContactType.Phone;
		}

		private static readonly object InstanceLock = new object();
		private static RedHatHttpSmsSenderProcessor _instance;
		private RedHatHttpSmsReceiver _receiver;

		public static RedHatHttpSmsSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new RedHatHttpSmsSenderProcessor());
			}
		}
	}
}