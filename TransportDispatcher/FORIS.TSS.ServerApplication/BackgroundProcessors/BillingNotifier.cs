﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.Terminal;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary> Обеспечивает генерацию уведомлений об исчерпании пакетов услуг </summary>
	class BillingNotifier : BackgroundProcessorSingleton<BillingNotifier>
	{
		private long _lastRenderedServiceItemID;
		private readonly RenderedServiceType _renderedServiceType;
		private readonly string _renderedServiceTypeString;

		public BillingNotifier() : base(TimeSpan.FromMinutes(1))
		{
			_renderedServiceType       = RenderedServiceType.ServiceDays;
			_renderedServiceTypeString = _renderedServiceType.ToString();

			BeforeStart += delegate
			{
				_lastRenderedServiceItemID = Server.Instance().GetConstantAsLongInt(Constant.LastRenderedServiceItemID) ?? 0;

				Server.Instance().NotifyEvent += OnVehicleNotified;
			};

			AfterStop += delegate
			{
				Server.Instance().SetConstant(Constant.LastRenderedServiceItemID, _lastRenderedServiceItemID);
			};
		}

		class Item
		{
			public long ID;
			public int? OperatorID;
			public string CultureCode;
			public int? RenderedQuantity;
			public int? MaxQuantity;
		}

		protected override bool Do()
		{
			using (var entities = new Entities())
			{
				var renderedServiceItem =
					entities.RenderedServiceItem
						.OrderBy(rsi => rsi.ID)
						.Where(rsi =>
							rsi.ID > _lastRenderedServiceItemID &&
							rsi.Billing_Service_ID != null &&
							rsi.Rendered_Service_Type.Name == _renderedServiceTypeString &&
							rsi.Status == (int) RenderedServiceItemStatus.Payed)
						.Select(rsi => new Item
						{
							ID               = rsi.ID,
							OperatorID       = rsi.Billing_Service.Operator_ID,
							CultureCode      = rsi.Billing_Service.OPERATOR.Culture.Code,
							RenderedQuantity = rsi.RenderedQuantity,
							//MaxQuantity зависит от _renderedServiceType
							MaxQuantity      = rsi.Billing_Service.Type.ServiceDays
						})
						.FirstOrDefault();


				if (renderedServiceItem == null)
					return false; //Задачи закончились, можно сделать перерыв
				_lastRenderedServiceItemID = renderedServiceItem.ID;

				return ProcessItem(entities, renderedServiceItem);
			}
		}

		private bool ProcessItem(Entities entities, Item renderedServiceItem)
		{
			if (renderedServiceItem.RenderedQuantity == null || renderedServiceItem.MaxQuantity == null)
				return true;

			//Проверяем остатки, только для пользователей
			if (renderedServiceItem.OperatorID == null)
				return true;

			//Проводим дальнейшую обработку, только если списали последнюю единицу услуги
			if (renderedServiceItem.RenderedQuantity != renderedServiceItem.MaxQuantity)
				return true;

			//TODO: обеспечить проверку множественных порогов

			var services = Server.Instance().DbManager.GetOperatorServices(renderedServiceItem.OperatorID.Value, null);
			var remainingQuantity =
				services.Sum(
					s =>
						s.Status != null &&
						s.Status.ContainsKey(_renderedServiceType)
							? s.Status[_renderedServiceType].QuantityLeft ?? 0
							: 0);

			if (remainingQuantity != 0)
				return true;

			var @strings = ResourceContainers.Get(renderedServiceItem.CultureCode);

			OPERATOR @operator = entities.GetOperator(renderedServiceItem.OperatorID.Value);
			var shrtText = @strings["MachineDaysPacketIsExhausted"];
			var fullText = @strings["ClickToBuyMoreMachineDays"];
			var messages =
				entities.CreateAppNotification(@operator.OPERATOR_ID, shrtText, fullText);
			messages.Add(entities.CreateWebNotification(@operator.OPERATOR_ID, shrtText, fullText));

			foreach (var message in messages)
			{
				message.MESSAGE_TEMPLATE = entities.GetMessageTemplate(Msg::MessageTemplate.BillingServiceExhaust);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.RenderedServiceType, _renderedServiceTypeString);
			}

			if (messages.Count != 0)
				entities.SaveChanges();

			return true;
		}

		private void OnVehicleNotified(NotifyEventArgs args)
		{
			if (args.Event != TerminalEventMessage.VehicleIsUnpaid)
				return;

			var vehicleId =
				args.Tag != null && 0 < args.Tag.Length
					? args.Tag[0] as int?
					: null;

			if (vehicleId != null)
				NotifyUserAboutUnpaidVehicle(vehicleId.Value);
		}

		private readonly ConcurrentDictionary<int, DateTime> _lastUnpaidVehicleNotificationDateTime =
			new ConcurrentDictionary<int, DateTime>();
		private void NotifyUserAboutUnpaidVehicle(int vehicleId)
		{
			var now = DateTime.UtcNow;
			if (!_lastUnpaidVehicleNotificationDateTime.TryAdd(vehicleId, now))
			{
				DateTime oldDate;
				if (!_lastUnpaidVehicleNotificationDateTime.TryGetValue(vehicleId, out oldDate))
					return;
				if (now - oldDate < TimeSpan.FromDays(1))
					return;
				if (!_lastUnpaidVehicleNotificationDateTime.TryUpdate(vehicleId, now, oldDate))
					return;
			}

			using (var entities = new Entities())
			{
				var payers =
					entities.GetVehiclePayers(vehicleId)
						.Select(o => new {o.OPERATOR_ID, CultureCode = o.Culture.Code})
						.ToList();

				bool saveChanges = false;
				foreach (var payer in payers)
				{
					var @strings = ResourceContainers.Get(payer.CultureCode ?? CultureInfo.InvariantCulture.IetfLanguageTag);

					var shrtText = string.Format(@strings["VehicleServiceIsUnpaid"], Server.Instance().GetVehicleName(payer.OPERATOR_ID, vehicleId));
					var fullText = @strings["ClickToBuyMoreMachineDays"];
					var messages =
						entities.CreateAppNotification(payer.OPERATOR_ID, shrtText, fullText);
					messages.Add(entities.CreateWebNotification(payer.OPERATOR_ID, shrtText, fullText));
					foreach (var message in messages)
					{
						message.MESSAGE_TEMPLATE = entities.GetMessageTemplate(Msg::MessageTemplate.BillingServiceExhaust);
						entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.RenderedServiceType, _renderedServiceTypeString);
						entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId);
					}

					if (messages.Count != 0)
						saveChanges = true;
				}
				if (saveChanges)
					entities.SaveChanges();
			}
		}
	}
}
