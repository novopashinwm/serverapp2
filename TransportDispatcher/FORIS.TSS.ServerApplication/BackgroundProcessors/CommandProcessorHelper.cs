﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Command = FORIS.TSS.EntityModel.Command;
using Contact = FORIS.TSS.EntityModel.Contact;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary> Функции обработки команд </summary>
	public class CommandProcessorHelper
	{
		/// <summary> Словарь ожидающих определения местоположения (команды AskPosition) объектов наблюдения </summary>
		private readonly Dictionary<int, int> _waitingForPositionVehicleIds = new Dictionary<int, int>();
		/// <summary> Набор ожидающих результатов команд объектов наблюдения </summary>
		private readonly HashSet<int> _waitingForAnyDataVehicleIds = new HashSet<int>();
		/// <summary> Указатель на функцию пересылки команд серверу терминалов </summary>
		private readonly Func<IStdCommand, CmdResult?> _sendCommandToTerminalServer;
		/// <summary> Уведомлять абонентов о запросах местоположения по расписанию </summary>
		private readonly bool _notifyAboutScheduledRequests;
		/// <summary> Интервал, после которого другу отправляется видимый push-запрос с просьбой активировать приложение </summary>
		private readonly TimeSpan? _cloudPositionRequestTimeout;

		/// <summary> Конструктор </summary>
		public CommandProcessorHelper(Func<IStdCommand, CmdResult?> sendCommandToTerminalServer)
		{
			_notifyAboutScheduledRequests = ConfigHelper.GetAppSettingAsBoolean("CommandProcessor.NotifyAboutScheduledRequests");
			_cloudPositionRequestTimeout = ConfigHelper.GetAppSettingAsTimeSpan("CommandProcessor.CloudPositionRequestTimeout");
			_sendCommandToTerminalServer = sendCommandToTerminalServer;
			FillWaitingForPositionVehicleIds();
		}

		/// <summary> Заполнить при старте команды AskPosition, ожидающие завершения </summary>
		private void FillWaitingForPositionVehicleIds()
		{
			using (var entities = new Entities())
			{
				entities.ExecuteGuarantly(context =>
				{
					// ReSharper disable once AccessToDisposedClosure
					var waitingVehicleIds = entities.Command
						.Where(c =>
							c.Status  == (int) CmdStatus.Processing &&
							c.Type_ID == (int) CmdType.AskPosition)
						.Select(c => c.Target_ID)
						.ToList();

					foreach (var vehicleId in waitingVehicleIds)
					{
						var lastPosition = Server.Instance().GetLastPosition(vehicleId);
						_waitingForPositionVehicleIds[vehicleId] = lastPosition != null ? lastPosition.CorrectTime : 0;
					}
				});
			}
		}

		/// <summary> Обрабатывает событие типа Notify ("Сообщение терминала") </summary>
		public void ProcessNotifyEvent(NotifyEventArgs args)
		{
			var commandTags = args.Tag;
			if (commandTags == null)
				return;

			if (args.Event == TerminalEventMessage.CommandCompleted)
			{
				var commandResult = (CommandResult)commandTags[0];

				ProcessCommandCompletion(commandResult, commandResult.Tag as IUnitPosition);
				return;
			}

			if (args.Event == TerminalEventMessage.Picture)
			{
				ProcessPictureEvent(commandTags);
			}
		}

		/// <summary> Обрабатывает событие типа TerminalEventMessage.Picture </summary>
		/// <param name="commandTags"></param>
		private void ProcessPictureEvent(object[] commandTags)
		{
			if (commandTags.Length < 4)
				return;

			var unitInfo = commandTags[0] as IUnitInfo;
			if (unitInfo == null)
				return;
			var photoPacketLength = commandTags[3] as int?;
			using (var entities = new Entities())
			{
				//Необходимо продлить время выполнения активных команд получения изображения

				List<Command> commands;
				try
				{
					commands = entities.Command
						.Where(c =>
							c.Target_ID == unitInfo.Unique             &&
							c.Type_ID   == (int)CmdType.CapturePicture &&
							c.Status    == (int)CmdStatus.Processing)
						.ToList();
				}
				catch (EntityCommandExecutionException e)
				{
					var innerSqlException = e.InnerException as SqlException;
					if (innerSqlException != null &&
						innerSqlException.Message.EndsWith("Rerun the transaction."))
					{
						return;
					}

					Trace.TraceError("{0}", e);
					return;
				}

				if (commands.Count == 0)
					return;

				foreach (var c in commands)
				{
					if (photoPacketLength == null || photoPacketLength.Value < 1)
					{
						c.Result_Type_ID = (int)CmdResult.NotSupported;
						c.Status = (int)CmdStatus.Completed;
					}

					c.Result_Date_Received = DateTime.UtcNow;
				}

				entities.SaveChanges();
			}
		}

		/// <summary> Обрабатывает команду </summary>
		public void ProcessCommand(Entities entities, Command command)
		{
			var cmdType = (CmdType)command.Type_ID;

			if (!command.Command_Parameter.IsLoaded)
				command.Command_Parameter.Load();

			if (!command.Target.CONTROLLERReference.IsLoaded)
				command.Target.CONTROLLERReference.Load();

			// Завершаем команду для объекта наблюдения при отсутствии оборудования мониторинга
			var controller = command.Target.CONTROLLER;
			if (controller == null)
			{
				$"Vehicle {command.Target.VEHICLE_ID} has no controller".CallTraceWarning();
				command.Result_Date_Received = DateTime.UtcNow;
				command.Result_Type_ID       = (int)CmdResult.NoDeviceInstalled;
				command.Status               = (int)CmdStatus.Completed;
				return;
			}

			// Обработка команды AskPosition ("Запросить местоположение")
			if (cmdType == CmdType.AskPosition)
			{
				var commandNotificationInfo = new CommandNotificationInfo(entities, command);

				// Под вопросом, нужно ли это показывать в системном центре уведомлений или в разделе Уведомления мобильного приложения
				if (entities.GetConstantAsBool(Constant.SendNotificationAboutCommandStartToSender))
					SendNotificationAboutCommandStartToSender(entities, commandNotificationInfo);

				// На случай, если местоположение известно, а пользователь просто хочет запросить позицию трекера по SMS
				var lastPosition = Server.Instance().GetLastPosition(command.Target_ID);
				_waitingForPositionVehicleIds[command.Target_ID] = lastPosition != null ? lastPosition.CorrectTime : 0;

				// Запрос (подготовка сообщений к MessageSender) к смартфону(ам) используя Push уведомления
				var pushSent = SendPositionRequestPush(entities, commandNotificationInfo);
				if (!pushSent)
				{
					command.Result_Type_ID = (int)CmdResult.MobileAppNotInstalled;
					command.Status         = (int)CmdStatus.Completed;
					return;
				}
				return;
			}

			SendCommandToTerminalServer(command);
		}

		/// <summary> Формирует уведомление вида: Вы запросили местоположение Г. Поттер </summary>
		private void SendNotificationAboutCommandStartToSender(Entities entities, CommandNotificationInfo info)
		{
			if (info.Sender == null ||
				info.ScheduledRequest ||
				info.Sender.CloudContacts == null ||
				info.Sender.CloudContacts.Count == 0 ||
				info.Target != null &&
				info.Target.OperatorId == info.Sender.OperatorId)
				return;

			SendNotification(entities, info.Sender, new MESSAGE.Blank(info.Sender.VehicleName, info.Sender.Strings["weAreSearchingForHim"]));
		}

		private List<MESSAGE> SendNotification(Entities entities, OperatorNotificationInfo operatorInfo, MESSAGE.Blank blank, DateTime? date = null)
		{
			if (operatorInfo.CloudContacts == null)
				return null;

			var result = new List<MESSAGE>();
			foreach (var contact in operatorInfo.CloudContacts)
			{
				var message = entities.CreateAppNotification(contact, null, null);
				result.Add(message);
				message.Fill(blank);

				message.Owner_Operator_ID = operatorInfo.OperatorId;
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, operatorInfo.VehicleId);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.Date, date ?? DateTime.UtcNow);
			}
			return result;
		}

		/// <summary> Опорные данные для рассылки уведомлений о команде </summary>
		private class OperatorNotificationInfo
		{
			public readonly List<Contact>      CloudContacts;
			public readonly ResourceContainers Strings;
			public readonly string             FriendName;
			public readonly string             VehicleName;
			public readonly int?               OperatorId;
			/// <summary> Идентификатор объекта наблюдения, на который отправлена команда </summary>
			public readonly int                VehicleId;
			public readonly string             Msisdn;
			public readonly string             CultureCode;

			public OperatorNotificationInfo(Entities entities, int? operatorId, int vehicleId,
				int? friendOperatorId = null)
			{
				OperatorId = operatorId;
				VehicleId = vehicleId;
				if (operatorId != null)
					CloudContacts = entities.GetCloudContactsForOperator(operatorId.Value);

				var operatorCulture = CultureInfo.InvariantCulture;
				if (operatorId != null)
					operatorCulture = entities.GetOperatorCulture(operatorId.Value);

				CultureCode = operatorCulture.TwoLetterISOLanguageName;
				Strings = ResourceContainers.Get(operatorCulture);
				if (friendOperatorId != null && operatorId != null)
					FriendName = Server.Instance().GetFriendName(operatorId.Value, friendOperatorId.Value);

				VehicleName = Server.Instance().GetVehicleName(operatorId, vehicleId);
				if (string.IsNullOrWhiteSpace(VehicleName))
					VehicleName = Strings["Noname"];

				if (operatorId != null)
				{
					using (new Stopwatcher("entities.GetOwnMsisdn(operatorId.Value)"))
					Msisdn = entities.GetOwnMsisdn(operatorId.Value);
				}
				else
				{
					Contact phoneContact;
					using (new Stopwatcher("entities.GetVehiclePhone"))
					phoneContact = entities.GetVehiclePhone(vehicleId);
					if (phoneContact != null)
						Msisdn = phoneContact.Value;
				}
			}
		}

		class CommandNotificationInfo
		{
			public readonly OperatorNotificationInfo Sender;
			public readonly OperatorNotificationInfo Target;
			public readonly bool ScheduledRequest;
			public readonly int CommandId;
			public readonly bool SendResultOverSms;
			public readonly bool WarnAboutLocation;
			public readonly DateTime? Time;
			public readonly TimeSpan? CommandAge;
			public bool ActivateAppRequest;

			public CommandNotificationInfo(Entities entities, Command command)
			{
				CommandId = command.ID;
				ActivateAppRequest = command.HasParameter(CommandParameter.ActivateAppRequest);

				Sender = command.Sender_ID != null
					? new OperatorNotificationInfo(entities, command.Sender_ID.Value, command.Target_ID)
					: null;

				var targetOperatorId = entities.GetOperatorByVehicleId(command.Target_ID)
					.Select(o => o.OPERATOR_ID)
					.FirstOrDefault();
				Target            = new OperatorNotificationInfo(entities, targetOperatorId, command.Target_ID, command.Sender_ID);
				ScheduledRequest  = command.HasParameter(CommandParameter.ScheduledRequest);
				SendResultOverSms = command.HasParameter(CommandParameter.SendResultOver.Key, CommandParameter.SendResultOver.Values.Sms);

				WarnAboutLocation =
					targetOperatorId != default(int) &&
					entities.Asid
						.Where(a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == command.Target_ID)
						.Select(a => a.WarnAboutLocation)
						.FirstOrDefault();

				Time = command.Log_Time != null ? TimeHelper.GetDateTimeUTC(command.Log_Time.Value) : (DateTime?) null;

				CommandAge = GetCommandAge(command);
			}
		}

		/// <summary> Отправляет push-запрос местоположения на смартфоны </summary>
		public static bool TrySendCloudPush(Entities entities, Command command)
		{
			var commandNotificationInfo = new CommandNotificationInfo(entities, command);

			return SendPositionRequestPush(entities, commandNotificationInfo);
		}

		private static bool SendPositionRequestPush(Entities entities, CommandNotificationInfo info)
		{
			if (info.Target == null ||
				info.Target.CloudContacts == null ||
				info.Target.CloudContacts.Count == 0)
				return false;

			MESSAGE.Blank blank = null;
			if (info.ActivateAppRequest &&
				entities.GetConstantAsBool(Constant.SendNotificationAboutCommandStartToTarget))
			{
				var friendName =
					info.Sender != null &&
					info.Sender.OperatorId != null &&
					info.Target.OperatorId != null
						? Server.Instance().GetFriendName(info.Target.OperatorId.Value, info.Sender.OperatorId.Value)
						: null;

				//TODO: возможно, следует отправлять запрос местоположения и уведомление о запросе двумя разными push'ами
				//TODO: при этом запрос местоположения - невидимый

				blank = new MESSAGE.Blank(
					friendName,
					info.ActivateAppRequest
					? info.Target.Strings["isSearchingForYouPleaseActivateApp"]
					: info.Target.Strings["isSearchingForYou"]);
			}

			foreach (var cloudContact in info.Target.CloudContacts)
			{
				var message = new MESSAGE();

				entities.MESSAGE.AddObject(message);

				message.MESSAGE_TEMPLATE = entities.MESSAGE_TEMPLATE.First(
					mt => mt.NAME == Msg::MessageTemplate.PositionRequest);
				message.AddDestination(cloudContact);
				message.Owner_Operator_ID = info.Sender != null ? info.Sender.OperatorId : null;

				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, info.Target.VehicleId);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.CommandId, info.CommandId);

				//Запрос местоположения всегда невидимый.
				//Уведомление типа "Вас нашли" или "Вас не нашли" приходит отдельным пушем
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.PushIsVisible, blank != null);

				if (info.Sender != null && !string.IsNullOrWhiteSpace(info.Sender.Msisdn))
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, info.Sender.Msisdn);

				if (blank != null)
					message.Fill(blank);
			}

			return true;
		}

		/// <summary> Помечает команды как обрабатываемые и возвращает их список </summary>
		public List<Command> CheckoutCommandsForProcessing(Entities entities)
		{
			List<Command> commands;
			using (var transaction = new Transaction())
			{
				var dateReceived = DateTime.UtcNow;

				var cmd = entities.Command
					.Include(Command.TargetIncludePath)
					.Include(Command.SenderIncludePath)
					.Where(c => c.Status == (int)CmdStatus.Received &&
								c.Date_Received <= dateReceived)
					.OrderBy(c => c.ErrorCount)
					.ThenBy(c => c.Date_Received)
					.Take(100);

				commands = cmd.ToList();
				foreach (var command in commands)
				{
					CheckoutCommandForProcessing(command);
				}

				entities.SaveChanges();
				transaction.Complete();
			}
			return commands;
		}

		/// <summary> Помечает команду как обрабатываемую </summary>
		public void CheckoutCommandForProcessing(Command command)
		{
			command.Status = (int) CmdStatus.Processing;
			command.Result_Type_ID = (int) CmdResult.Processing;
		}

		private void SendPositionRequestResultWithCloudPush(Entities entities, CmdResult cmdResult, CommandNotificationInfo info)
		{
			if (info.Sender == null || string.IsNullOrEmpty(info.Target.Msisdn))
				return;

			var text = GetTextOfPositionRequestResultCloudPush(entities, cmdResult, info);
			if (string.IsNullOrWhiteSpace(text))
				return;

			var messages = SendNotification(entities, info.Sender, new MESSAGE.Blank(info.Sender.VehicleName, text), info.Time);
			if (messages == null)
				return;

			var template = entities.GetMessageTemplate(Msg::MessageTemplate.RequestResult);
			foreach (var message in messages)
			{
				message.MESSAGE_TEMPLATE = template;
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.CommandId, info.CommandId);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, info.Target.Msisdn);
			}
		}

		private string GetTextOfPositionRequestResultCloudPush(Entities entities, CmdResult cmdResult, CommandNotificationInfo info)
		{
			if (info.Sender == null)
				return null;

			if (info.ScheduledRequest)
			{
				if (!_notifyAboutScheduledRequests)
					return null;

				//TODO: сделать Command.Sender_ID not null
				if (info.Sender.OperatorId == null)
					return null;

				var previousCmdResult = GetPreviousCommandResult(entities, info);
				if (cmdResult != CmdResult.Completed &&
					(previousCmdResult ?? CmdResult.Completed) == CmdResult.Completed)
				{
					return info.Target.Strings["wasNotFoundBySchedule"];
				}

				if (cmdResult == CmdResult.Completed &&
					previousCmdResult != null &&
					previousCmdResult.Value != CmdResult.Completed)
				{
					return info.Target.Strings["wasFoundBySchedule"];
				}

				//Не нужно уведомлять
				return null;
			}

			switch (cmdResult)
			{
				case CmdResult.Completed:

					//Если искомого нашли в период после 2 минут прошедших с запроса (т. е. не сразу), то шлем уведомление об успехе ищущему.
					if (info.CommandAge == null || info.CommandAge < TimeSpan.FromMinutes(2))
						return null;

					return info.Sender.Strings["wasFound"];
				default:
					return null;
			}
		}

		private void SendResultOverSms(Entities context, CmdResult cmdResult, CommandNotificationInfo info, IUnitPosition position)
		{
			if (!info.SendResultOverSms)
				return;
			if (info.Sender == null || info.Sender.OperatorId == null)
				return;

			string text;
			switch (cmdResult)
			{
				case CmdResult.Completed:
					var positionString = GeoHelper.GetPositionString(
						operatorId: info.Sender.OperatorId.Value,
						lat:        position.Latitude,
						lng:        position.Longitude,
						radius:     position.Radius,
						vehicleId:  info.Sender.VehicleId,
						language:   info.Sender.CultureCode);
					text = positionString;
					break;
				default:
					text = info.Sender.Strings["wasNotFound"];
					break;
			}

			//TODO: использовать имя из SMS-запроса "ГДЕ" в качестве имени объекта наблюдения

			context.SendSmsToUser(
				context.GetContact(ContactType.Phone, info.Sender.Msisdn),
				context.GetMessageTemplate(Msg::MessageTemplate.CommandResult),
				info.Sender.VehicleName + " " + text);
		}

		/// <summary> Обрабатывает события Receive </summary>
		public void ProcessReceiveEvent(ReceiveEventArgs args)
		{
			foreach (var mu in args.MobilUnits)
			{
				ProcessReceiveEvent(mu);
			}
		}

		private void ProcessReceiveEvent(IMobilUnit mu)
		{
			PositionReceivedForAskPositionCommand(mu);
			AnyDataReceivedForSetupCommand(mu);
		}

		private void AnyDataReceivedForSetupCommand(IMobilUnit mu)
		{
			if (mu.Media != Media.GPRS &&
				mu.Media != Media.TCP
#if DEBUG
				&& mu.Media != Media.Emulator
#endif
)
				return; //Ожидаются данные только по каналу GPRS/TCP/UDP и т.п. - это говорит о том, что команда настройки трекера успешно выполнена
			//Проблема: GPS-ошейники для оленей работают исключительно по SMS и для них предположение неверно.
			//Решение: автоматическая настройка GPS-ошейников не поддерживается

			//Проблема: трекер уже мог быть настроен, а команда будет помечена как выполненная, хотя до конца ещё не дошла


			if (!_waitingForAnyDataVehicleIds.Contains(mu.Unique))
				return;

			_waitingForAnyDataVehicleIds.Remove(mu.Unique);

			using (var entities = new Entities())
			{
				var setupCommandResults =
					entities.Command.Where(
						c => c.Type_ID == (int)CmdType.Setup &&
							c.Target_ID == mu.Unique &&
							c.Status == (int)CmdStatus.Processing).ToList();


				if (setupCommandResults.Count == 0)
					return;

				foreach (var result in setupCommandResults)
				{
					result.Result_Date_Received = DateTime.UtcNow;
					result.Result_Type_ID = (int)CmdResult.Completed;
					result.Status = (int)CmdStatus.Completed;
				}

				entities.SaveChanges();
			}
		}

		private void PositionReceivedForAskPositionCommand(IMobilUnit mu)
		{
			if (!mu.ValidPosition)
				return;

			//Без этой проверки все входящие координаты начнут проверяться на существование команды, что вызовет огромное количество обращений к БД
			int commandLogTime;
			if (!_waitingForPositionVehicleIds.TryGetValue(mu.Unique, out commandLogTime))
				return;
			if (mu.CorrectTime < commandLogTime)
				return;

			_waitingForPositionVehicleIds.Remove(mu.Unique);

			using (var entities = new Entities())
			{

				var commandIds = (from c in entities.Command
								  where
									  c.Type_ID == (int)CmdType.AskPosition &&
									  c.Target_ID == mu.Unique &&
									  c.Status == (int)CmdStatus.Processing
								  select c.ID).ToList();

				foreach (var id in commandIds)
				{
					ProcessSuccessAskPositionCommand(id, mu);
				}
			}
		}

		private void ProcessSuccessAskPositionCommand(int commandID, IUnitPosition position)
		{
			if (position == null)
				throw new ArgumentNullException("position", @"position cannot be null");

			ProcessCommandCompletion(
				new CommandResult
				{
					CmdType = CmdType.AskPosition,
					CmdResult = CmdResult.Completed,
					CommandID = commandID,
					Tag = position.UnitInfo
				}, position);
		}

		private static void ChargeServiceRendering(Entities entities, Command command)
		{
			int? payerVehicleID;

			if (command.Sender_ID == null)
				return;

			var billingServer = new BillingServer(entities);
			try
			{
				payerVehicleID = billingServer.ChargeServiceRendering(
					command.Sender_ID.Value, command.Target_ID, PaidService.AskPositionOverLBS, command.ID);
			}
			catch (BillingException e)
			{
				Trace.TraceWarning("Error when charging service {0} for operator {1} on vehicle {2}: {3}",
					PaidService.AskPositionOverLBS, command.Sender_ID.Value, command.Target_ID, e.Message);
				payerVehicleID = null;
			}

			if (payerVehicleID != null)
				command.Payer_Vehicle_ID = payerVehicleID.Value;
		}

		private void ProcessCommandCompletion(CommandResult commandResult, IUnitPosition position)
		{
			using (var entities = new Entities())
			{
				var commandTarget = commandResult.Tag;
				if (commandTarget == null)
				{
					Trace.TraceWarning("{0}: command result without target was received: {1}", this, commandResult);
					return;
				}

				var query = entities.Command
					.Include(Command.Command_ParameterIncludePath)
					.Where(c => c.Target_ID == commandTarget.Unique &&
								c.Status == (int) CmdStatus.Processing);
				if (commandResult.CmdType != CmdType.Unspecified)
					query = query.Where(c => c.Type_ID == (int) commandResult.CmdType);

				Command command = query.FirstOrDefault();

				if (command == null)
					return;

				var cmdResult = commandResult.CmdResult;

				if (cmdResult == CmdResult.CapacityLimitReached ||
					cmdResult == CmdResult.TimeoutExceeded)
				{
					return;
				}

				command.Result_Date_Received = DateTime.UtcNow;
				command.Result_Type_ID = (int)cmdResult;

				if (position != null)
				{
					if (command.Type_ID == (int) CmdType.AskPosition && position.CorrectTime != default(int))
						command.Log_Time = position.CorrectTime;
					else
						command.Log_Time = position.Time;
				}

				if (command.Type_ID == (int)CmdType.AskPosition)
				{
					if (cmdResult == CmdResult.Completed)
						command.Status = (int)CmdStatus.Completed;
				}
				else if (command.Type_ID != (int)CmdType.Setup || cmdResult != CmdResult.Completed)
				{
					command.Status = (int)CmdStatus.Completed;
				}

				if (cmdResult == CmdResult.UnknownSubscriber)
				{
					var targetAsid =
						entities.Asid.FirstOrDefault(
							a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == command.Target_ID);
					if (targetAsid != null && targetAsid.Contact != null)
						targetAsid.Contact = null;
				}

				if (cmdResult == CmdResult.VehicleAccessDisallowed)
				{
					//TODO: добавить в юнит-тесты
					entities.DeleteAccess(command.Sender, command.Target);
				}

				if (command.Status == (int)CmdStatus.Completed && command.Type_ID == (int)CmdType.AskPosition)
				{
					var info = new CommandNotificationInfo(entities, command);

					SendResultOverSms(entities, (CmdResult)command.Result_Type_ID, info, position);
					SendPositionRequestResultWithCloudPush(entities, (CmdResult)command.Result_Type_ID, info);
					SendPositionRequestNotification(entities, (CmdResult)command.Result_Type_ID, info);
					ChargeServiceRendering(entities, command);
				}

				entities.SaveChanges();
			}
		}

		/// <summary> Отправить уведомление об успешном или не успешном поиске местоположения (Кого определяли) </summary>
		private void SendPositionRequestNotification(Entities entities, CmdResult cmdResult, CommandNotificationInfo info)
		{
			if (info.Target == null)
				return;

			// Отключено, дабы не напрягать клиента
			return;

			var text = GetTextOfPositionRequestNotification(entities, cmdResult, info);
			if (string.IsNullOrWhiteSpace(text))
				return;

			var blank = new MESSAGE.Blank(info.Target.FriendName, text);
			var messages = SendNotification(entities, info.Target, blank, info.Time);
			if (messages == null)
				return;

			var template = entities.GetMessageTemplate(Msg::MessageTemplate.RequestResult);

			foreach (var message in messages)
			{
				message.MESSAGE_TEMPLATE = template;
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, info.Sender.Msisdn);
			}
		}

		private string GetTextOfPositionRequestNotification(
			Entities entities, CmdResult cmdResult, CommandNotificationInfo info)
		{
			if (info.ScheduledRequest)
			{
				if (!_notifyAboutScheduledRequests)
					return null;

				//TODO: сделать Command.Sender_ID not null
				if (info.Sender.OperatorId == null)
					return null;

				var previousCmdResult = GetPreviousCommandResult(entities, info);
				if (cmdResult != CmdResult.Completed &&
					(previousCmdResult ?? CmdResult.Completed) == CmdResult.Completed)
				{
					return info.Target.Strings["youWereNotFoundBySchedule"];
				}

			   //Не нужно уведомлять
				return null;
			}

			switch (cmdResult)
			{
				case CmdResult.Completed:
					return null;
				default:
					return info.Target.Strings["youWereNotFound"];
			}
		}

		private static CmdResult? GetPreviousCommandResult(Entities entities, CommandNotificationInfo info)
		{
			var previousCommandResult =
				entities.Command.Where(
					c => c.Sender_ID == info.Sender.OperatorId.Value &&
						 c.Target_ID == info.Sender.VehicleId &&
						 c.ID < info.CommandId)
					.OrderByDescending(c => c.ID)
					.Select(c => new {CmdResult = (CmdResult) c.Result_Type_ID})
					.FirstOrDefault();
			return previousCommandResult != null ? previousCommandResult.CmdResult : (CmdResult?) null;
		}

		/// <summary>
		/// Отменяет команды, время выполнения которых превысило определенный порог
		/// </summary>
		/// <param name="entities">Контекст подключения к БД</param>
		public void ProcessLateCommands(Entities entities)
		{
			const int undoPerTransaction = 100;
			var nowDate = DateTime.UtcNow;

			while (true)
			{
				var lateCommands = (from c in entities.Command
					where (c.Status == (int) CmdStatus.Processing) &&
						  c.Date_Received < EntityFunctions.AddSeconds(nowDate, -c.Type.ExecutionTimeout)
					select c).Take(undoPerTransaction).ToArray();

				if (0 < lateCommands.Length)
				{
						foreach (var command in lateCommands)
					{
						var cmdResult = (CmdResult)command.Result_Type_ID;
						switch (cmdResult)
						{
							case CmdResult.Received:
							case CmdResult.Processing:
								command.Result_Type_ID = (int)CmdResult.TimeoutExceeded;
								break;
						}

						if (command.Type_ID == (int) CmdType.AskPosition)
						{
							TimeSpan? nextTryTimeSpan = GetNextTryTimeSpan(command);

							if (nextTryTimeSpan != null)
							{
								command.Status = (int) CmdStatus.Received;
								command.Date_Received = DateTime.UtcNow.Add(nextTryTimeSpan.Value);
							}
							else
							{
								command.Status = (int) CmdStatus.Completed;
								command.Result_Date_Received = DateTime.UtcNow;
							}

							//TODO: присылать оповещения о завершении команды не только определения местоположения, но и любой другой

							var lastNotificationSent = GetDateTimeParameter(command, Command_Parameter.NotificationSent);

							if (lastNotificationSent == null ||
								DateTime.UtcNow - lastNotificationSent.Value >= TimeSpan.FromHours(1))
							{
								entities.SetCommandParameter(command, Command_Parameter.NotificationSent, TimeHelper.ToXmlFormat(DateTime.UtcNow));

								var info = new CommandNotificationInfo(entities, command);

								SendResultOverSms(entities, cmdResult, info, null);
								SendPositionRequestResultWithCloudPush(entities, cmdResult, info);
								SendPositionRequestNotification(entities, cmdResult, info);
							}
						}
						else
						{
							command.Status = (int)CmdStatus.Completed;
							command.Result_Date_Received = DateTime.UtcNow;
						}
					}

					entities.SaveChanges();
				}

				if (lateCommands.Length < undoPerTransaction)
					break;
			}
		}

		private TimeSpan? GetNextTryTimeSpan(Command command)
		{
			var age = GetCommandAge(command);

			if (age < TimeSpan.FromHours(1))
				return TimeSpan.FromMinutes(1);

			if (age < TimeSpan.FromDays(1))
				return TimeSpan.FromMinutes(5);

			if (age < TimeSpan.FromDays(90))
				return TimeSpan.FromHours(1);

			return null;
		}

		private static TimeSpan? GetCommandAge(Command command)
		{
			var createDate = GetDateTimeParameter(command, Command_Parameter.Created);
			if (createDate == null) return null;

			var age = DateTime.UtcNow - createDate.Value;
			return age;
		}

		private static DateTime? GetDateTimeParameter(Command command, string parameterKey)
		{
			if (!command.Command_Parameter.IsLoaded)
				command.Command_Parameter.Load();

			if (!command.HasParameter(parameterKey))
				return null;
			var createDateString =
				command.Command_Parameter.Where(x => x.Key == parameterKey).Select(x => x.Value).First();
			var createDate = TimeHelper.FromXmlFormat(createDateString);
			return createDate;
		}

		private void SendCommandToTerminalServer(Command command)
		{
			var cmd = GetCommand(command);
			var cmdResult = _sendCommandToTerminalServer(cmd);

			if (cmdResult != null)
			{
				command.Result_Type_ID = (int) cmdResult.Value;
				command.Status = (int) CmdStatus.Completed;
				return;
			}

			if (cmd.Type == CmdType.Setup)
				_waitingForAnyDataVehicleIds.Add(command.Target_ID);
		}

		private static IStdCommand GetCommand(Command command)
		{
			var cmd = CommandCreator.CreateCommand((CmdType)command.Type_ID);

			cmd.Target.Unique = command.Target_ID;

			if (command.Sender != null)
			{
				cmd.Sender = new OperatorInfo(command.Sender.OPERATOR_ID, command.Sender.LOGIN, command.Sender.NAME);
			}

			var cmdParams = new PARAMS();
			foreach (var commandParameter in command.Command_Parameter)
				cmdParams.Add(commandParameter.Key, commandParameter.Value);
			cmdParams.Add(CommandParameter.CommandID, command.ID);
			cmd.Params = cmdParams;
			return cmd;
		}
	}
}