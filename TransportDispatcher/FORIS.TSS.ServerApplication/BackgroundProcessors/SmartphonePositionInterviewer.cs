﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	class SmartphonePositionInterviewer : BackgroundProcessorSingleton<SmartphonePositionInterviewer>
	{
		/// <summary> Время отправки последнего пуша запроса местоположения на смартфон </summary>
		private readonly ConcurrentDictionary<int, DateTime> _lastVehiclePushSent
			= new ConcurrentDictionary<int, DateTime>();

		/// <summary> Время, когда объект наблюдения был последний раз загружен пользователем через API </summary>
		private readonly ConcurrentDictionary<int, DateTime> _lastVehicleRequestedByUser
			= new ConcurrentDictionary<int, DateTime>();

		public SmartphonePositionInterviewer() : base(TimeSpan.FromSeconds(5))
		{
			BeforeStart += delegate { SubscribeToEvents();   };
			AfterStop   += delegate { UnsubscribeToEvents(); };
		}

		private void SubscribeToEvents()
		{
			Server.Instance().VehiclesRequested += VehiclesRequested;
		}

		private void UnsubscribeToEvents()
		{
			Server.Instance().VehiclesRequested -= VehiclesRequested;
		}

		protected override bool Do()
		{
			var somethingWasDone = false;
			foreach (var pair in _lastVehicleRequestedByUser)
			{
				if (pair.Value < DateTime.UtcNow.AddMinutes(-5))
					continue;

				// Последний запрос позиции из БД был произведен менее 5 минут назад
				if (!NeedRequestPhonePosition(pair.Key))
					continue;

				if (RequestPhonePosition(pair.Key))
					somethingWasDone = true;
			}

			return somethingWasDone;
		}

		private bool NeedRequestPhonePosition(int vehicleId)
		{
			DateTime lastPushSentTime;
			if (_lastVehiclePushSent.TryGetValue(vehicleId, out lastPushSentTime) &&
				//TODO: вынести эту константу в заглушку
				DateTime.UtcNow.AddSeconds(-5) < lastPushSentTime)
				return false; //Запрос отправлен менее 5 секунд назад

			//TODO: Уменьшить поток запросов на устройство
			//TODO: В целом нужно избежать ненужных отправок, например, когда пуш был отправлен, но не был еще доставлен
			//TODO: Также после доставки пуша может пройти некоторое время, чтобы устройство успело определить позицию
			//TODO: Т.е. нужно дождаться завершения жизненного цикла старого пуша, прежде чем снова генерить новый
			//TODO: Также полезно различать тип устройства в рамках его жизненного цикла
			//TODO: Например, у android доставка пушей более гарантирована, чем у apple (?)

			return true;
		}

		private bool RequestPhonePosition(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var friendContact = entities
					.VEHICLE.Where(x => x.VEHICLE_ID == vehicleId)
					.SelectMany(x => x.CONTROLLER.MLP_Controller.Asid.OPERATOR.AppClient)
					.Where(x => x.Contact_ID != null)
					.OrderByDescending(x => x.ID)
					.Select(x => x.Contact)
					.FirstOrDefault();

				if (friendContact == null)
					return false;

				var message = new MESSAGE();

				entities.MESSAGE.AddObject(message);

				message.MESSAGE_TEMPLATE = entities.MESSAGE_TEMPLATE.First(
					mt => mt.NAME == Msg::MessageTemplate.PositionRequest);
				message.AddDestination(friendContact);

				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId);

				// Запрос местоположения всегда невидимый.
				// Уведомление типа "Вас нашли" или "Вас не нашли" приходит отдельным push сообщением
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.PushIsVisible, false);

				entities.SaveChanges();
			}

			LastVehiclePushSent(vehicleId);

			return true;
		}

		private void VehiclesRequested(int operatorId, GetVehiclesArgs arguments, List<Vehicle> vehicles)
		{
			if ((arguments.VehicleIDs?.Length ?? 0) != 0) return;

			var activeSmartphoneVehicles = vehicles
				.Where(v =>
					v?.controllerType?.Name == ControllerType.Names.SoftTracker &&
					!(v.rights?.Contains(SystemRight.Ignore) ?? false))
				.Select(x => x.id)
				.ToArray();

			foreach (var vehicleId in activeSmartphoneVehicles)
				SmartphonePositionRequestedByUser(vehicleId);
		}

		private void SmartphonePositionRequestedByUser(int vehicleId)
		{
			_lastVehicleRequestedByUser[vehicleId] = DateTime.UtcNow;
		}

		private void LastVehiclePushSent(int vehicleId)
		{
			_lastVehiclePushSent[vehicleId] = DateTime.UtcNow;
		}
	}
}