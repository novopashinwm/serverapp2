﻿namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	interface IInitializable
	{
		void Initialize();
	}
}