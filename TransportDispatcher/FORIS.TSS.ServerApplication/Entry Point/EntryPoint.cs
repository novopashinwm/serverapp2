﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.Protection;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Корневой класс домена серверов (как объект с поддержкой удаленного взаимодействия, через границы доменов и процессов) </summary>
	/// <remarks> Обычно используется в конфигурационном файле для создания нового домена, поэтому прямых ссылок не имеет </remarks>
	public class EntryPoint : MarshalByRefObject, IEntryPoint
	{
		/// <summary> Получает объект службы времени существования для управления политикой времени существования для этого экземпляра </summary>
		/// <returns>
		/// Теоретически на этот объект существует ссылка только в инфраструктуре ремотинга.
		/// Домен приложения ланчера вызывает методы Start() и Stop() этого объекта.
		/// Если изготавливать стандартную лицензию, то этот объект по истечению лицензии в виду отсутствия спонсоров будет отключен от
		/// ремотинга и при выгрузке сервера попытка  вызова метода Stop() приведет к исключению.
		/// </returns>
		public override object InitializeLifetimeService()
		{
			/* Вечная лицензия */
			return null;
		}
		/// <summary> Конструктор </summary>
		public EntryPoint()
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Create Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
		}
		/// <summary> Запуск сервера </summary>
		public void Start(bool runLoop, bool activate, bool staticServer)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Start Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			try
			{
				// К событиям домена подключаемся только если сервис стартует
				AppDomain.CurrentDomain.DomainUnload       += CurrentDomain_DomainUnload;
				AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

				// Настройка Remoting
				RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);
				RemotingServices.Disconnect(this);

				if (activate)
				{
					Server.Instance().Exit += Server_Exit;

					if (ProtectionKey.CanStartServer())
						Server.Instance().Start();
					else
						throw new HaspProtectionException("No protection key or license has expired");

					/* Необходимо чтоб точка входа запускала серверы,
					 * указанные в файле конфигурации приложения,
					 * а не предопределенного в коде класса.
					 * Класс сервера должен реализовывать интерфейс IServer,
					 * чтоб его можно было запустить и остановить
					 */

					var serverConfigurations = ConfigurationManager.GetSection("servers") as IDictionary<string, ServerApplicationConfiguration>
						?? new Dictionary<string, ServerApplicationConfiguration>();

					/* Используем GlobalsConfig.AppSettings
					 * вместо ConfigurationManager.AppSettings, чтоб секции
					 * машин оказывали влияние на запуск серверов
					 */
					var serverNames = GlobalsConfig.AppSettings["Server"]?.Split(',') ?? new string[0];
					if (serverNames.Length != 0)
						foreach (string serverName in serverNames)
						{
							var server = serverConfigurations.FirstOrDefault(s => s.Key == serverName).Value?.CreateInstance();
							if (server != null)
							{
								server.Exit += Server_Exit;
								if (ProtectionKey.CanStartServer())
									server.Start();
								else
									throw new Exception("No protection key or license has expired");
							}
							else
								$"В секции 'servers' файла конфигурации нет описания сервера '{serverName}'"
									.TraceWarning();
						}
					else
						$"Серверы не загружены. Не указан параметр 'Server' в секции 'appSettings' файла конфигурации"
							.TraceWarning();
				}

				#region Run loop

				if (runLoop)
				{
					Thread thread = new Thread(new ThreadStart(Application.Run));
					thread.IsBackground = false; // It isn't possible to use ThreadPool in this case
					thread.Start();
				}

				#endregion Run loop
			}
			catch (Exception ex)
			{
				$"Start Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'"
					.WithException(ex)
					.CallTraceError();
				throw;
			}
		}
		/// <summary> Остановка сервера </summary>
		public void Stop()
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Stop Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			Server.Instance().Stop();
		}
		private void Server_Exit(IServer server, string message)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Server '{server?.GetTypeName(false)}' Exit, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			Server.Instance().Stop();
			throw new HaspProtectionException(message);
		}
		[SecurityPermission(SecurityAction.LinkDemand, ControlAppDomain = true)]
		private void CurrentDomain_DomainUnload(object sender, EventArgs e)
		{
			$"Domain Unload: {AppDomain.CurrentDomain.FriendlyName}".TraceInformation();
		}
		[SecurityPermission(SecurityAction.LinkDemand, ControlAppDomain = true)]
		private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs ex)
		{
			(string.Empty
				+ $"!!!!!!!!!!!!!!!!! CurrentDomain_UnhandledException !!!!!!!!!!!!!!!!!"
				+ $"\nSender - {sender?.GetTypeName() ?? "Unknown"}")
				.WithException(ex?.ExceptionObject as Exception, true)
				.CallTraceError();
		}
	}
}