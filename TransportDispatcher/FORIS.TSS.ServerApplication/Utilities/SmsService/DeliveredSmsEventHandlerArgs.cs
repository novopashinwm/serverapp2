﻿using System;
using EasySMPP;

namespace FORIS.TSS.ServerApplication.Utilities.SmsService
{
	public class DeliveredSmsEventHandlerArgs : EventArgs
	{
		public DeliveredSmsEventHandlerArgs(int messageId, MessageStates state)
		{
			MessageId = messageId;
			State     = state;
		}

		public int MessageId { get; private set; }

		public MessageStates State { get; private set; }
	}
}