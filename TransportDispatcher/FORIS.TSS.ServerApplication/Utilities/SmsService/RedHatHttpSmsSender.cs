﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using FORIS.TSS.ServerApplication.SmsService.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public class RedHatHttpSmsSender : AbstractGenericSingleton<RedHatHttpSmsSender>, ISmsSender
	{
		private readonly bool     _enabled;
		private readonly string   _urlTemplate;
		private readonly string   _replyOnSucceded;
		private readonly TimeSpan _timeout;
		private readonly string   _countryCodeToCut;

		public RedHatHttpSmsSender()
		{
			var configuration = (SmsProviderRedHatSectionHandler)ConfigurationManager.GetSection("SmsProviderRedHat");
			_enabled          = configuration.Enabled;
			_urlTemplate      = configuration.SendSmsUrl;
			_replyOnSucceded  = configuration.ReplyOnSucceded;
			_timeout          = configuration.RequestTimeout;
			_countryCodeToCut = configuration.CountryCodeToCut;
		}

		public SendSmsResult SendSms(MessageTuple smsMessage)
		{
			if (!_enabled)
				return SendSmsResult.ServiceTurnedOff;

			// Отправляем сообщение
			var queryUrl = _urlTemplate
				.Replace("{to}", GetPhone(smsMessage))
				.Replace("{message}", Uri.EscapeDataString(smsMessage.Body));

			// Тассируем отправку сообщения
			Trace.TraceInformation(GetType().Name + ": Message: {0}, From: [{1}], To: [{2}], Text: {3}, Url: {4}",
				smsMessage.Id, smsMessage.Sender, smsMessage.Destination, smsMessage.Body, queryUrl);

			var httpRequest = (HttpWebRequest)WebRequest.Create(queryUrl);
			httpRequest.Method  = "GET";
			httpRequest.Timeout = (int)_timeout.TotalMilliseconds;

			try
			{
				var response = (HttpWebResponse)httpRequest.GetResponse();

				string xResultString;
				var responseStream = response.GetResponseStream();
				if (responseStream == null)
				{
					Trace.TraceError(GetType().Name + ": Empty response stream when opening {0}", queryUrl);
					return SendSmsResult.HttpError;
				}

				using (var stringReader = new StreamReader(responseStream))
					xResultString = stringReader.ReadToEnd();

				if (string.IsNullOrWhiteSpace(xResultString))
				{
					Trace.TraceError(GetType().Name + ": Empty result string when opening {0}", queryUrl);
					return SendSmsResult.UnspecifiedError;
				}

				if (xResultString.IndexOf(_replyOnSucceded, StringComparison.InvariantCulture) < 0)
				{
					Trace.TraceWarning(GetType().Name + ": Invalid result string {0} when opening {1}", xResultString, queryUrl);
					return SendSmsResult.UnspecifiedError;
				}
			}
			catch (TimeoutException)
			{
				Trace.TraceWarning(GetType().Name + ": Timeout exceeded when opening {0}", queryUrl);
				return SendSmsResult.Timeout;
			}
			catch (Exception ex)
			{
				Trace.TraceError(GetType().Name + ": Error when opening {0}: {1}", queryUrl, ex);
				return SendSmsResult.HttpError;
			}

			return SendSmsResult.Success;
		}

		private string GetPhone(MessageTuple smsMessage)
		{
			var phone = smsMessage.Destination.Value;
			if (string.IsNullOrWhiteSpace(_countryCodeToCut))
				return phone;

			if (phone.StartsWith(_countryCodeToCut))
				return phone.Substring(_countryCodeToCut.Length);

			return phone;
		}
	}
}