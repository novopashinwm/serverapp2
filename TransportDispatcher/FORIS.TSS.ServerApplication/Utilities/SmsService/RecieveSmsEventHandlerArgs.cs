using System;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public class RecieveSmsEventHandlerArgs : EventArgs
	{
		public string From { get; set; }
		public string To { get; set; }
		public string Text { get; set; }

		public RecieveSmsEventHandlerArgs(string from, string to, string text)
		{
			From = from;
			To = to;
			Text = text;
		}
	}
}