﻿using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.SmsService
{
	internal delegate void SmsCommandDelegate(
		Entities context, string requestorAsid, OPERATOR_VEHICLE requestor, string[] smsParts);
}