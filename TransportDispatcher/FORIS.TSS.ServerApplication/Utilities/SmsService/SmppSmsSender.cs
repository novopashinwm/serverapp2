﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using FORIS.TSS.ServerApplication.SmsService.Configuration;
using EasySMPP;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.ServerApplication.Utilities.SmsService;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public delegate void RecieveSmsEventHandler(object sender, RecieveSmsEventHandlerArgs args);

	public delegate void DeliveredSmsEventHandler(object sender, DeliveredSmsEventHandlerArgs args);

	public class SmppSmsSender : AbstractGenericSingleton<SmppSmsSender>, ISmsSender, IDisposable
	{
		struct DeliveryKey
		{
			/// <summary> Идентификатор сообщения получившего расписку. </summary>
			private readonly string _submitMessageId;

			/// <summary> Номер телефона, на который отправлено сообщение. </summary>
			private readonly string _to;

			public DeliveryKey(string submitMessageId, string to)
			{
				_submitMessageId = submitMessageId;
				_to = to;
			}

			public override bool Equals(object obj)
			{
				if (obj == null)
					return false;
				if (obj.GetType() != typeof (DeliveryKey))
					return false;
				var x = (DeliveryKey) obj;

				return _submitMessageId == x._submitMessageId &&
					   _to == x._to;
			}

			public override int GetHashCode()
			{
				return (_submitMessageId ?? string.Empty).GetHashCode() ^
					   (_to ?? string.Empty).GetHashCode();
			}
		}

		private readonly string _description;
		private readonly string _host;
		private readonly int _port;
		private readonly AutoResetEvent _lock = new AutoResetEvent(false);
		private readonly ConcurrentDictionary<DeliveryKey, int> _waitDelivery = new ConcurrentDictionary<DeliveryKey, int>();
		private readonly ConcurrentDictionary<int, int> _deliveryMessagePartCounts = new ConcurrentDictionary<int, int>(); 

		private readonly string _systemId;
		private readonly string _password;

		private readonly string _systemType;
		private readonly byte _addrTon;
		private readonly byte _addrNpi;
		private readonly string _addressRange;
		private readonly int _sequenceNumber;

		private readonly string _defaultSender;
		private readonly Dictionary<string, Regex> _senderToTargetRegex;
		private readonly bool _testModeOn;
		private readonly bool _askDelivery;

		private static SMPPClient _client;
		private readonly bool _enabled;
		private readonly string _stubOutputFile;

		private static int _currentMessageId;
		private static int _messagePartToProcess;
		private static int _messagePartProcessed;
		private readonly X509Certificate _sslCertificate;

		public event RecieveSmsEventHandler OnReceiveSms;
		public event DeliveredSmsEventHandler OnDeliveredSms;

		public SendSmsResult SendSms(MessageTuple smsMessage)
		{
			if (!_enabled)
				return SendSmsResult.ServiceTurnedOff;

			// Тассируем отправку сообщения
			Trace.TraceInformation(GetType().Name + ": Message: {0}, From: [{1}], To: [{2}], Text: {3}",
				smsMessage.Id, smsMessage.Sender, smsMessage.Destination, smsMessage.Body);

			// Отправляем сообщение
			var clearPhone = smsMessage.Destination.Value;
			var regex = new Regex(@"\d+");
			var match = regex.Match(clearPhone);
			if (match.Success)
				clearPhone = match.Value;

			var sender = smsMessage.Sender?.Value ?? GetSender(clearPhone);

			if (!string.IsNullOrWhiteSpace(_stubOutputFile))
			{
				File.AppendAllText(_stubOutputFile,
					string.Format("Sending message {4} on behalf of {0} to phone: {1} [explicit: {2}]\nMessage: {3}\n",
						sender, smsMessage.Destination, clearPhone, smsMessage.Message.BODY, smsMessage.Id));
				smsMessage.SendMask = 1;
				return SendSmsResult.Success;
			}

			if (!_client.CanSend)
			{
				try
				{
					_client.Connect();
					if (!_client.CanSend)
						return SendSmsResult.NoConnection;
				}
				catch (Exception ex)
				{
					Trace.TraceError(GetType().Name + ": {0}", ex);
					return SendSmsResult.NoConnection;
				}
			}

			_currentMessageId = smsMessage.Id & 0x00FF;
			_messagePartProcessed = 0;
			_messagePartToProcess = _client.SendSms(sender, clearPhone, smsMessage.Body, smsMessage.Id, ref smsMessage.SendMask);

			_lock.WaitOne(TimeSpan.FromMinutes(2));
			var pool = _client.MessagePool;
			var parts = pool.Where(x => x.MessageId == _currentMessageId).ToList();
			var maskBitArray = new BitArray(parts.Count);
			foreach (var s in parts.Where(s => s.Status == SubmitSmStatus.Sent))
				maskBitArray.Set((s.SegmentId ?? 1) - 1, true);
			smsMessage.SendMask = GetIntFromBitArray(maskBitArray);

			SendSmsResult result;
			if (pool.Any(x => x.MessageId == _currentMessageId && x.Status == SubmitSmStatus.Throttled))
				result = SendSmsResult.ThrottlingError;
			else if (pool.Any(x => x.MessageId == _currentMessageId && x.Status == SubmitSmStatus.MessageQueueFull))
				result = SendSmsResult.MessageQueueFull;
			else if (pool.Any(x => x.MessageId == _currentMessageId && x.Status == SubmitSmStatus.Failed))
				result = SendSmsResult.UnspecifiedError;
			else if (pool.All(x => x.MessageId == _currentMessageId && x.Status == SubmitSmStatus.Pushed))
				result = SendSmsResult.UnspecifiedError;
			else
				result = SendSmsResult.Success;

			pool.Clear();
			return result;
		}

		private string GetSender(string phone)
		{
			foreach (var pair in _senderToTargetRegex)
			{
				if (pair.Value.IsMatch(phone))
					return pair.Key;
			}
			return _defaultSender;
		}

		private static int GetIntFromBitArray(BitArray bitArray)
		{
			int[] array = new int[1];
			bitArray.CopyTo(array, 0);
			return array[0];
		}
		
		public SmppSmsSender()
		{
			var configuration = (SmsProviderSmppSectionHandler)ConfigurationManager.GetSection("SmsProviderSmpp");
			_description = configuration.Description;
			_host = configuration.Host;
			_port = configuration.Port;

			_systemId = configuration.SystemId;
			_password = configuration.Password;

			_systemType = configuration.SystemType;
			_addrTon = configuration.AddrTon;
			_addrNpi = configuration.AddrNpi;
			_addressRange = configuration.AddressRange;
			_sequenceNumber = configuration.SequenceNumber;

			_defaultSender = Server.Instance().GetConstant(Constant.ServiceName);
			_senderToTargetRegex = new Dictionary<string, Regex>();
			foreach (SmsSenderElement sender in configuration.Senders)
			{
				_senderToTargetRegex.Add(sender.Sender, new Regex(sender.Target));
			}

			_testModeOn     = configuration.TestModeOn;
			_stubOutputFile = configuration.StubOutputFile;
			_enabled        = configuration.Enabled;
			_askDelivery    = configuration.AskDelivery;

			if (!string.IsNullOrWhiteSpace(configuration.SslCertificatePath))
			{
				_sslCertificate = new X509Certificate(configuration.SslCertificatePath, configuration.SslCertificatePassword);
			}
			else
			{
				_sslCertificate = null;
			}
		}

		internal void Initialize()
		{
			if (!string.IsNullOrWhiteSpace(_stubOutputFile)) return;

			KernelParameters.AskDeliveryReceipt = _askDelivery ? 1 : 0;
			_client = new SMPPClient
				{
					LogLevel = LogLevels.LogErrors | LogLevels.LogExceptions | LogLevels.LogWarnings
				};

			_client.OnLog          += _client_OnLog;
			_client.OnSubmitSmResp += _client_OnSubmitSmResp;
			_client.OnDeliverSm    += _client_OnDeliverSm;

			var smsc = new SMSC(
				_description,
				_host,
				_port,
				_systemId,
				_password,
				_systemType,
				_addrTon,
				_addrNpi,
				_addressRange,
				_sequenceNumber,
				_testModeOn);

			smsc.SslCertificate = _sslCertificate;

			_client.AddSMSC(smsc);

			if (_enabled)
				_client.Connect();
		}

		private void _client_OnDeliverSm(DeliverSmEventArgs e)
		{
			if (e.IsDeliveryReceipt && _askDelivery)
			{
				var deliveryKey = new DeliveryKey(e.ReceiptedMessageID, e.From);

				int messageId;
				if (!_waitDelivery.TryRemove(deliveryKey, out messageId))
					return;

				int deliveredPartsCount;
				if (!_deliveryMessagePartCounts.TryRemove(messageId, out deliveredPartsCount))
					return;

				var state = (MessageStates) e.MessageState;

				OnDeliveredSms?.Invoke(this, new DeliveredSmsEventHandlerArgs(messageId, state));

				return;
			}
			OnReceiveSms?.Invoke(this, new RecieveSmsEventHandlerArgs(e.From, e.To, e.TextString));
		}

		private void _client_OnSubmitSmResp(SubmitSmRespEventArgs e)
		{
			Trace.TraceInformation(GetType().Name + ": Event message MessageId: {0}, SequenceId: {1}, Status: {2}",
				e.MessageId, e.Sequence, e.Status);

			var pool = _client.MessagePool;

			foreach (var elem in pool)
				Trace.TraceInformation(GetType().Name + ": Pool message MessageId: {0}, SequenceId: {1}, SegmentId: {2}, Added: {3}, Status: {4}, OriginalMessageId: {5}",
					elem.MessageId, elem.SequenceId, elem.SegmentId, elem.Added, elem.Status, elem.OriginalMessageId);

			var elementToUpdate = pool.FirstOrDefault(x => x.SequenceId == e.Sequence);
			if (elementToUpdate == null)
				return;

			Trace.TraceInformation(GetType().Name + ": Update message MessageId: {0}, SequenceId: {1}, SegmentId: {2}, Added: {3}, Status: {4}, OriginalMessageId: {5}",
				elementToUpdate.MessageId, elementToUpdate.SequenceId, elementToUpdate.SegmentId, elementToUpdate.Added, elementToUpdate.Status, elementToUpdate.OriginalMessageId);

			switch (e.Status)
			{
				case StatusCodes.ESME_ROK:
					elementToUpdate.Status = SubmitSmStatus.Sent; // OK
					break;
				case StatusCodes.ESME_RTHROTTLED:
					elementToUpdate.Status = SubmitSmStatus.Throttled; //THROTTLED
					break;
				case StatusCodes.ESME_RMSGQFUL:
					elementToUpdate.Status = SubmitSmStatus.MessageQueueFull; //MQF
					break;
				default:
					elementToUpdate.Status = SubmitSmStatus.Failed; //UNHANDLED
					break;
			}

			if (_askDelivery)
			{
				var deliveryKey = new DeliveryKey(e.MessageId, elementToUpdate.To);
				
				_waitDelivery.AddOrUpdate(deliveryKey, elementToUpdate.OriginalMessageId, (key, i) => elementToUpdate.OriginalMessageId);
				if (!_deliveryMessagePartCounts.ContainsKey(elementToUpdate.OriginalMessageId))
					_deliveryMessagePartCounts.TryAdd(elementToUpdate.OriginalMessageId, _messagePartToProcess);
			}

			Interlocked.Increment(ref _messagePartProcessed);
			if (_messagePartToProcess == _messagePartProcessed)
				_lock.Set();
		}

		private void _client_OnLog(LogEventArgs e)
		{
			Trace.TraceInformation(GetType().Name + ": Log message: {1}", e.Message);
		}

		public void Dispose()
		{
			_client.Disconnect();
			_client.Dispose();
		}
	}
}