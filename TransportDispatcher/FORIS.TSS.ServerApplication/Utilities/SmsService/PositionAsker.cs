﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.BusinessLogic.Server;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;

namespace FORIS.TSS.ServerApplication.SmsService
{
	/// <summary> Создаёт команды на определение объекта по данным LBS </summary>
	public class PositionAsker : ISchedulerPlugin
	{
		static PositionAsker()
		{
			ActualThreshold = ConfigHelper.GetAppSettingAsTimeSpan("PositionAsker.ActualThreshold") ?? TimeSpan.FromMinutes(10);
		}

		public string GetStaticConfig()
		{
			return null;
		}

		public void SetStaticConfig(int id, string configurationData)
		{
			// Nothing to do
		}

		public string GetDisplayName(CultureInfo locale)
		{
			return "MLP Asker";
		}

		public object GetParameters(string strConfigXML)
		{
			return null;
		}

		public BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(string xml, string commentXml, int queueID)
		{
			var paramsArray = (PARAMS)SerializationHelper.FromXML(xml);
			var idType      = (IdType)paramsArray["idType"];
			var id          = (int)paramsArray["id"];
			var operatorId  = (int)paramsArray["operatorID"];


			using (var entities = new Entities())
			{
				List<int> vehicleIDs;
				const int vehicleAccessRight = (int) SystemRight.VehicleAccess;
				if (idType == IdType.VehicleGroup)
				{
					if ((from ovgr in entities.v_operator_vehicle_groups_right 
						 where ovgr.operator_id == operatorId && 
							   ovgr.vehiclegroup_id == id &&
							   ovgr.right_id == vehicleAccessRight
							 select ovgr).Any())
					{
						vehicleIDs = (from vg in entities.VEHICLEGROUP
										   from v in vg.VEHICLE
										   from ovr in v.Accesses
										   where vg.VEHICLEGROUP_ID == id &&
												 v.CONTROLLER.MLP_Controller.Asid != null &&
												 ovr.operator_id == operatorId &&
												 ovr.right_id == (int)SystemRight.VehicleAccess
										   select v.VEHICLE_ID).ToList();

						if (!vehicleIDs.Any())
							return SchedulerExecutionResult.Success; //Нет прав на запрос местоположения ни по одному устройству в группе, значит ничего делать не нужно; 
						//задача в очереди тоже должна остаться: возможно, оператор наполняет группу устройствами прямо в этот момент
					}
					else
					{
						return SchedulerExecutionResult.Cancel; //Нет больше прав на группу, не нужно более выполнять запросы местоположения
					}
				}
				else
				{
					vehicleIDs =
					(
						from ovr in entities.v_operator_vehicle_right
						where
							ovr.operator_id == operatorId         &&
							ovr.right_id    == vehicleAccessRight &&
							ovr.vehicle_id  == id
						select ovr.vehicle_id
					)
						.ToList();
					if (!vehicleIDs.Any())
						return SchedulerExecutionResult.Cancel; //нет больше прав на запрос местоположения по устройству
				}


				var server = Server.Instance();
				foreach (var vehicleId in vehicleIDs)
				{
					server.RegisterCommand(
						entities.GetOperatorInfo(operatorId),
						new Command
						{
							TargetID   = vehicleId,
							Type       = CmdType.AskPosition,
							Parameters = new Dictionary<string, string>
							{
								{ CommandParameter.ScheduledRequest, XmlConvert.ToString(true) }
							}
						});
				}

				return SchedulerExecutionResult.Success;
			}
		}

		public object GetDefaultSettings()
		{
			return null;
		}

		private static readonly TimeSpan ActualThreshold;

		public bool IsActualTask(DateTime nearestTime)
		{
			return DateTime.UtcNow - nearestTime < ActualThreshold;
		}
	}
}
