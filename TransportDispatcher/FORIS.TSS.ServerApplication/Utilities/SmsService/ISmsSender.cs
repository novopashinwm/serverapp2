﻿namespace FORIS.TSS.ServerApplication.SmsService
{
	interface ISmsSender
	{
		/// <summary> Отправить сообщение </summary>
		/// <param name="smsMessage">Сообщение и вспомогательная информация</param>
		SendSmsResult SendSms(MessageTuple smsMessage);
	}
}