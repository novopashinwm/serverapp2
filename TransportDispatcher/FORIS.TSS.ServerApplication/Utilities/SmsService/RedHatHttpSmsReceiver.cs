﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Http;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.SmsService
{
	class RedHatHttpSmsReceiver : BasicHttpSmsReceiver
	{
		private readonly Dictionary<int, LimitedQueue<GeoZoneForPosition>> _zonePasses =
			new Dictionary<int, LimitedQueue<GeoZoneForPosition>>();

		private readonly ReaderWriterLockSlim _zonePassesLock = new ReaderWriterLockSlim();

		public RedHatHttpSmsReceiver(string httpPrefix) : base(httpPrefix)
		{
			BeforeStart += OnBeforeStart;
		}

		private void OnBeforeStart(object o, EventArgs eventArgs)
		{
			Server.Instance().ReceiveEvent += OnReceiveEvent;
		}

		private void OnReceiveEvent(ReceiveEventArgs args)
		{
			var mobilUnits = args.MobilUnits.Where(NeedToProcess).OrderBy(mu => mu.Unique).ThenBy(mu => mu.Time).ToList();
			if (mobilUnits.Count == 0)
				return;
			var vehicleIds = new HashSet<int>(mobilUnits.Select(mu => mu.Unique));
			var vehicleZoneParams = ZoneVehicles.Where(vzp => vehicleIds.Contains(vzp.VehicleId)).ToList();
			if (vehicleZoneParams.Count == 0)
				return;
			var vehiclePositionParams = mobilUnits.Select(
				mu => new VehiclePositionParam(
					mu.Unique, mu.Time, mu.Latitude, mu.Longitude));

			var positionZones = Server.Instance()
				.GetGeoZonesForPositions(vehicleZoneParams, vehiclePositionParams)
				.ToLookup(pz => new VehicleLogTimeParam(pz.VehicleId, pz.LogTime), pz => pz.ZoneId);

			_zonePassesLock.EnterWriteLock();

			try
			{
				foreach (var mu in mobilUnits)
				{
					var zoneId = positionZones[new VehicleLogTimeParam(mu.Unique, mu.Time)].FirstOrDefault();
					if (zoneId == default(int))
						continue;
					LimitedQueue<GeoZoneForPosition> zonePasses;
					if (!_zonePasses.TryGetValue(mu.Unique, out zonePasses))
					{
						zonePasses = new LimitedQueue<GeoZoneForPosition>(2);
						_zonePasses.Add(mu.Unique, zonePasses);
						zonePasses.Add(new GeoZoneForPosition(mu.Unique, mu.Time, zoneId));
						continue;
					}

					var lastZonePass = zonePasses.Last();
					if (mu.Time < lastZonePass.LogTime)
						continue;

					if (lastZonePass.ZoneId != zoneId)
						zonePasses.Add(new GeoZoneForPosition(mu.Unique, mu.Time, zoneId));
					else
						zonePasses.Update(new GeoZoneForPosition(mu.Unique, mu.Time, zoneId));
				}
			}
			finally
			{
				_zonePassesLock.ExitWriteLock();
			}
		}

		private bool NeedToProcess(IMobilUnit mu)
		{
			if (!mu.ValidPosition) return false;

			return VehicleIds.Contains(mu.Unique);
		}

		private HashSet<int> _vehicleIds = new HashSet<int>();
		private readonly object _vehicleIdsLock = new object();
		private DateTime _lastVehicleIdsTime = DateTime.MinValue;

		private HashSet<int> VehicleIds
		{
			get
			{
				if (_maxCacheAge < DateTime.UtcNow - _lastVehicleIdsTime)
				{
					lock (_vehicleIdsLock)
					{
						if (_maxCacheAge < DateTime.UtcNow - _lastVehicleIdsTime)
						{
							_lastVehicleIdsTime = DateTime.UtcNow;
							var vehicleIds = new HashSet<int>();
							using (var entities = new Entities())
							{
								var departmentId = entities.GetConstantAsInt(CONSTANTS.RedHatSmsServiceDepartmentID);
								if (departmentId != null)
								{
									foreach (
										var vehicleId in
											entities.VEHICLE
													.Where(v => v.Department_ID == departmentId.Value &&
																v.ZoneGroups.Any())
													.Select(v => v.VEHICLE_ID))
									{
										vehicleIds.Add(vehicleId);
									}
								}
							}
							_vehicleIds = vehicleIds;
						}
					}
				}

				return _vehicleIds;
			}
		}

		private readonly List<VehicleZoneParam> _zoneVehicles = new List<VehicleZoneParam>();
		private DateTime _zoneVehiclesTime = DateTime.MinValue;
		private readonly TimeSpan _maxCacheAge = TimeSpan.FromMinutes(5);

		private IEnumerable<VehicleZoneParam> ZoneVehicles
		{
			get
			{
				if (_maxCacheAge < DateTime.UtcNow - _zoneVehiclesTime)
				{
					_zoneVehiclesTime = DateTime.UtcNow;

					_zoneVehicles.Clear();

					using (var entities = new Entities())
					{
						var departmentId = entities.GetConstantAsInt(CONSTANTS.RedHatSmsServiceDepartmentID);
						if (departmentId != null)
						{
							_zoneVehicles.AddRange(
								(from v in entities.VEHICLE
								 from zg in v.ZoneGroups
								 from zgm in zg.Members
								 where v.Department_ID == departmentId.Value
								 //В LINQ to Entities поддерживаются только конструкторы без параметров и инициализаторы.
								 select new {v.VEHICLE_ID, zgm.ZONE_ID})
									.ToList().Select(x => new VehicleZoneParam(x.VEHICLE_ID, x.ZONE_ID)));
						}
					}
				}
				return _zoneVehicles;
			}
		}

		protected override string ProcessSmsHttpRequest(HttpListenerRequest request, string messageText)
		{
			var phone = request.QueryString["MOBILENO"];
			if (string.IsNullOrWhiteSpace(phone))
				return "Phone is unknown";
			if (phone.StartsWith("0"))
				phone = phone.TrimStart('0');
			phone = ContactHelper.GetNormalizedPhone(phone);

			var text = request.QueryString["KEYVALUE"] ?? string.Empty;

			LogMessage(phone, text);

			string answer;

			const string delhiMetroSmsServicesPrefix = "DMRC ";
			if (text.StartsWith(delhiMetroSmsServicesPrefix))
			{
				answer = GetNearestBuses(text.Substring(delhiMetroSmsServicesPrefix.Length));
			}
			else
			{
				answer = "Unknown command: " + text;
			}

			Trace.TraceInformation("{0}: SMS answer: {1}", this, answer);

			return answer;
		}

		private void LogMessage(string phone, string text)
		{
			using (var entities = new Entities())
			{
				var contact = entities.GetContact(ContactType.Phone, phone);

				if (contact == null)
				{
					Trace.TraceWarning("{0}: Unable to get contact object for phone: {1}", this, phone);
					return;
				}

				var message = new MESSAGE
				{
					BODY               = text,
					TIME               = DateTime.UtcNow,
					SourceType_ID      = (int)ObjectType.Phone,
					DestinationType_ID = (int)MessageDestinationType.Server,
					ProcessingResult   = (int)MessageProcessingResult.Received,
					// Сообщение обрабатывается синхронно
					STATUS             = (int)MessageState.Done
				};

				message.Contacts.Add(new Message_Contact
				{
					Type    = (int)MessageContactType.Source,
					Contact = contact
				});

				entities.MESSAGE.AddObject(message);
				entities.SaveChanges();
			}
		}

		private string GetNearestBuses(string messageText)
		{
			using (var entities = new Entities())
			{
				var busStopCode = messageText.Trim();

				if (string.IsNullOrWhiteSpace(busStopCode))
				{
					return "Bus stop code is not specified";
				}

				var departmentId = entities.GetConstantAsInt(CONSTANTS.RedHatSmsServiceDepartmentID);
				if (departmentId == null)
					return "Service is off";
				var route = entities
					.GEO_ZONE
					.Where(z => z.DESCRIPTION == busStopCode && z.Department_ID == departmentId.Value)
					.SelectMany(z => z.ZONEGROUP_ZONE)
					.Where(zgz => zgz.Group.Department_ID == departmentId.Value)
					.SelectMany(zgz => zgz.Group.Members)
					.Select(zgz => zgz.Zone)
					.OrderBy(z => z.NAME)
					.ToList();

				if (route.Count == 0)
				{
					return "Wrong bus stop code: " + busStopCode;
				}

				var currentZoneId = route.First(z => z.DESCRIPTION == busStopCode).ZONE_ID;

				var incomingBuses = VehicleIds
					.Where(vid => _zoneVehicles.Any(zvp => zvp.ZoneId == currentZoneId))
					.Select(vid => GetIncomingBusInfo(vid, currentZoneId, route))
					.Where(x => x != null)
					.ToList();

				var forwardBus = GetNearestBus(incomingBuses, Direction.Forward);
				var backwardBus = GetNearestBus(incomingBuses, Direction.Backward);

				if (forwardBus == null && backwardBus == null)
				{
					return "No bus coming to stop code " + busStopCode;
				}

				var sb = new StringBuilder(255);

				if (forwardBus != null)
					AppendIncomingBus(sb, entities, forwardBus, route);
				if (backwardBus != null)
				{
					if (forwardBus != null)
						sb.Append(" & ");
					AppendIncomingBus(sb, entities, backwardBus, route);
				}

				return sb.ToString();
			}
		}

		private void AppendIncomingBus(StringBuilder sb, Entities entities, IncomingBusInfo bus, List<GEO_ZONE> route)
		{
			var busName =
				entities.VEHICLE.Where(v => v.VEHICLE_ID == bus.VehicleId).Select(v => v.GARAGE_NUMBER).First();

			sb.Append(busName)
			  .Append(" towards ")
			  .Append(GetBusStopName((bus.Direction == Direction.Forward
										  ? route.Last(z => !string.IsNullOrWhiteSpace(z.DESCRIPTION))
										  : route.First(z => !string.IsNullOrWhiteSpace(z.DESCRIPTION))).NAME))
			  .Append(", last at ")
			  .Append(GetBusStopName(bus.LastStop.NAME))
			  .Append(", ")
			  .Append((int) Math.Round((TimeHelper.GetSecondsFromBase() - bus.LastStopTime)/60.0))
			  .Append(" min ago");
		}

		private string GetBusStopName(string name)
		{
			var index = name.IndexOf('|');
			if (index == -1)
				return name;
			return name.Substring(index + 1);
		}

		private IncomingBusInfo GetNearestBus(List<IncomingBusInfo> incomingBuses, Direction direction)
		{
			return incomingBuses.Where(b => b.Direction == direction)
								.OrderBy(b => b.Distance)
								.ThenByDescending(b => b.LastStopTime)
								.FirstOrDefault();

		}

		private enum Direction
		{
			Forward = 1,
			Backward = -1
		}

		private class IncomingBusInfo
		{
			public int VehicleId;
			public int Distance;
			public Direction Direction;
			public GEO_ZONE LastStop;
			public int LastStopTime;
		}

		private IncomingBusInfo GetIncomingBusInfo(int bus, int stop, List<GEO_ZONE> route)
		{
			List<GeoZoneForPosition> zonePass;
			_zonePassesLock.EnterReadLock();
			try
			{
				LimitedQueue<GeoZoneForPosition> zonePassBuffer;
				if (!_zonePasses.TryGetValue(bus, out zonePassBuffer) || zonePassBuffer.Count < 2)
					return null; //Нет информации о направлении движения
				zonePass = new List<GeoZoneForPosition>
					{
						zonePassBuffer.Last(1),
						zonePassBuffer.Last()
					};
			}
			finally
			{
				_zonePassesLock.ExitReadLock();
			}

			var lastZoneIndex = route.FindIndex(x => x.ZONE_ID == zonePass.Last().ZoneId);
			if (lastZoneIndex == -1)
				return null; //Автобус не двигается по данному маршруту
			var prevZoneIndex = route.FindIndex(x => x.ZONE_ID == zonePass.First().ZoneId);
			if (prevZoneIndex == -1)
				return null; //Автобус не двигается по данному маршруту

			int passedStopIndex, passedStopTime;
			if (!string.IsNullOrWhiteSpace(route[lastZoneIndex].DESCRIPTION))
			{
				passedStopIndex = lastZoneIndex;
				passedStopTime = zonePass.Last().LogTime;
			}
			else if (!string.IsNullOrWhiteSpace(route[prevZoneIndex].DESCRIPTION))
			{
				passedStopIndex = prevZoneIndex;
				passedStopTime = zonePass.First().LogTime;
			}
			else //Нет ни одной остановки, значит, автобус сошёл с маршрута
				return null;

			var stopIndex = route.FindIndex(z => z.ZONE_ID == stop);
			if (stopIndex == -1)
				return null; //Искомая остановка не находится на данном маршруте

			int stopDistance;
			Direction direction;
			if (lastZoneIndex == prevZoneIndex + 1 || lastZoneIndex == 0 && prevZoneIndex == route.Count - 1)
			{
				if (passedStopIndex < stopIndex)
				{
					direction = Direction.Forward;
					stopDistance = stopIndex - passedStopIndex;
				}
				else
				{
					direction = Direction.Backward;
					//Автобус уже прошёл данную остановку, нужно учесть разворот на конечной
					stopDistance = (route.Count - 1 - passedStopIndex) + (route.Count - 1 - stopIndex);
				}
			}
			else if (lastZoneIndex == prevZoneIndex - 1 || lastZoneIndex == route.Count - 1 && prevZoneIndex == 0)
			{
				if (passedStopIndex <= stopIndex)
				{
					direction = Direction.Forward;
					//Автобус уже прошёл данную остановку, развернётся на конечной
					stopDistance = passedStopIndex + stopIndex;
				}
				else
				{
					direction = Direction.Backward;
					stopDistance = passedStopIndex - stopIndex;
				}
			}
			else
			{
				//Нарушена последовательность прохождения зон на маршруте
				return null;
			}

			return new IncomingBusInfo
				{
					VehicleId    = bus,
					Direction    = direction,
					Distance     = stopDistance,
					LastStop     = route[passedStopIndex],
					LastStopTime = passedStopTime
				};
		}
	}
}