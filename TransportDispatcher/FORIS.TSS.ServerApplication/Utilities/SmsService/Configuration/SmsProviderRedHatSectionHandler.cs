﻿using System;
using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	public class SmsProviderRedHatSectionHandler : ConfigurationSection
	{
		private const string SendSmsUrlKey = "SendSmsUrl";

		[ConfigurationProperty(SendSmsUrlKey, IsRequired = true, DefaultValue = "http://site.com")]
		public string SendSmsUrl
		{
			get { return (string)this[SendSmsUrlKey]; }
			set { this[SendSmsUrlKey] = value; }
		}

		private const string ReplyOnSuccededKey = "ReplyOnSucceded";

		[ConfigurationProperty(ReplyOnSuccededKey, IsRequired = false, DefaultValue = "Message+Sent+Successfully")]
		public string ReplyOnSucceded
		{
			get { return (string)this[ReplyOnSuccededKey]; }
			set { this[ReplyOnSuccededKey] = value; }
		}

		private const string EnabledKey = "Enabled";

		[ConfigurationProperty(EnabledKey, IsRequired = false, DefaultValue = "false")]
		public bool Enabled
		{
			get { return (bool)this[EnabledKey]; }
			set { this[EnabledKey] = value; }
		}

		private const string RequestTimeoutKey = "RequestTimeout";

		[ConfigurationProperty(RequestTimeoutKey, IsRequired = false, DefaultValue = "0.00:00:05")]
		public TimeSpan RequestTimeout
		{
			get { return (TimeSpan)this[RequestTimeoutKey]; }
			set { this[RequestTimeoutKey] = value; }
		}

		private const string CountryCodeToCutKey = "CountryCodeToCut";

		/// <summary>Код страны, который нужно исключить из номера телефона</summary>
		[ConfigurationProperty(CountryCodeToCutKey, IsRequired = false, DefaultValue = "91")]
		public string CountryCodeToCut
		{
			get { return (string) this[CountryCodeToCutKey]; }
			set { this[CountryCodeToCutKey] = value; }
		}

		private const string ReceiveSmsUriKey = "ReceiveSmsUri";

		[ConfigurationProperty(ReceiveSmsUriKey, IsRequired = false)]
		public string ReceiveSmsUri
		{
			get { return (string)this[ReceiveSmsUriKey]; }
			set { this[ReceiveSmsUriKey] = value; }
		}
	}
}