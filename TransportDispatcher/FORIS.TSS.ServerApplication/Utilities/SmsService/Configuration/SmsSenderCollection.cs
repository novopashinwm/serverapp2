﻿using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	/// <summary> Коллекция конфигураций правил, по которым определяется, от имени какого номера будет доставлена SMS </summary>
	public class SmsSenderCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new SmsSenderElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((SmsSenderElement)element).Sender;
		}

		protected override string ElementName
		{
			get
			{
				return "Sender";
			}
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.BasicMap;
			}
		}
	}
}