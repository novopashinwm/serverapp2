﻿using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	public class AliasElement : ConfigurationElement
	{
		private const string NameKey = "Name";

		[ConfigurationProperty(NameKey, IsRequired = true)]
		public string Name
		{
			get { return (string)this[NameKey]; }
		}
	}
}