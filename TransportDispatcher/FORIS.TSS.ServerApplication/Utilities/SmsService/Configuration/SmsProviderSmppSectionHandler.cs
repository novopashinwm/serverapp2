﻿using System;
using System.ComponentModel;
using System.Configuration;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	/// <summary> Настройка SMPP-шлюза </summary>
	public class SmsProviderSmppSectionHandler : ConfigurationSection
	{
		private const string DescriptionKey = "Description";

		[ConfigurationProperty(DescriptionKey, IsRequired = false)]
		public string Description
		{
			get { return (string)this[DescriptionKey]; }
			set { this[DescriptionKey] = value; }
		}

		private const string HostKey = "Host";

		[ConfigurationProperty(HostKey, IsRequired = true, DefaultValue = "Description")]
		public string Host
		{
			get { return (string)this[HostKey]; }
			set { this[HostKey] = value; }
		}

		private const string PortKey = "Port";

		[ConfigurationProperty(PortKey, IsRequired = true, DefaultValue = 5001)]
		public int Port
		{
			get { return (int)this[PortKey]; }
			set { this[PortKey] = value; }
		}

		private const string SystemIdKey = "SystemId";

		[ConfigurationProperty(SystemIdKey, IsRequired = true, DefaultValue = "login")]
		public string SystemId
		{
			get { return (string)this[SystemIdKey]; }
			set { this[SystemIdKey] = value; }
		}

		private const string PasswordKey = "Password";

		[ConfigurationProperty(PasswordKey, IsRequired = true, DefaultValue = "password")]
		public string Password
		{
			get { return (string)this[PasswordKey]; }
			set { this[PasswordKey] = value; }
		}

		private const string SystemTypeKey = "SystemType";

		[ConfigurationProperty(SystemTypeKey, IsRequired = false)]
		public string SystemType
		{
			get { return (string)this[SystemTypeKey]; }
			set { this[SystemTypeKey] = value; }
		}

		private const string AddrTonKey = "AddrTon";

		[ConfigurationProperty(AddrTonKey, IsRequired = true)]
		public byte AddrTon
		{
			get { return (byte)this[AddrTonKey]; }
			set { this[AddrTonKey] = value; }
		}

		private const string AddrNpiKey = "AddrNpi";

		[ConfigurationProperty(AddrNpiKey, IsRequired = true)]
		public byte AddrNpi
		{
			get { return (byte)this[AddrNpiKey]; }
			set { this[AddrNpiKey] = value; }
		}

		private const string AddressRangeKey = "AddressRange";

		[ConfigurationProperty(AddressRangeKey, IsRequired = false)]
		public string AddressRange
		{
			get { return (string)this[AddressRangeKey]; }
			set { this[AddressRangeKey] = value; }
		}

		private const string SequenceNumberKey = "SequenceNumber";

		[ConfigurationProperty(SequenceNumberKey, IsRequired = false)]
		public int SequenceNumber
		{
			get { return (int)this[SequenceNumberKey]; }
			set { this[SequenceNumberKey] = value; }
		}

		private const string DefaultSenderKey = "DefaultSender";

		[Obsolete("Do not use, default sender should be stored in CONSTANTS table")]
		[ConfigurationProperty(DefaultSenderKey, IsRequired = false, DefaultValue = "NIKA")]
		public string DefaultSender
		{
			get { return (string)this[DefaultSenderKey]; }
			set { this[DefaultSenderKey] = value; }
		}

		private const string SendersKey = "Senders";

		/// <summary> Коллекция конфигураций правил, по которым определяется, от имени какого номера будет доставлена SMS </summary>
		[ConfigurationProperty(SendersKey, IsDefaultCollection = false)]
		public SmsSenderCollection Senders
		{
			get
			{
				return (SmsSenderCollection)base[SendersKey];
			}
		}


		private const string TestModeOnKey = "TestModeOn";

		[ConfigurationProperty(TestModeOnKey, IsRequired = true, DefaultValue = true)]
		public bool TestModeOn
		{
			get { return (bool)this[TestModeOnKey]; }
			set { this[TestModeOnKey] = value; }
		}

		private const string EnabledKey = "Enabled";

		[ConfigurationProperty(EnabledKey, IsRequired = false, DefaultValue = "true")]
		public bool Enabled
		{
			get { return (bool)this[EnabledKey]; }
			set { this[EnabledKey] = value; }
		}

		private const string MaxTPSKey = "MaxTPS";

		[ConfigurationProperty(MaxTPSKey, IsRequired = false, DefaultValue = 4)]
		public int MaxTPS
		{
			get { return (int)this[MaxTPSKey]; }
			set { this[MaxTPSKey] = value; }
		}

		private const string RetryWaitingTimeKey = "RetryWaitingTime";

		[ConfigurationProperty(RetryWaitingTimeKey, IsRequired = false, DefaultValue = 1800)]
		public int RetryWaitingTime
		{
			get { return (int)this[RetryWaitingTimeKey]; }
			set { this[RetryWaitingTimeKey] = value; }
		}

		private const string RetryThrottlingTimeKey = "RetryThrottlingTime";

		[ConfigurationProperty(RetryThrottlingTimeKey, IsRequired = false, DefaultValue = 60)]
		public int RetryThrottlingTime
		{
			get { return (int)this[RetryThrottlingTimeKey]; }
			set { this[RetryThrottlingTimeKey] = value; }
		}

		private const string AutoReplyKey = "AutoReply";
		[ConfigurationProperty(AutoReplyKey, IsRequired = false, DefaultValue = "false")]
		public bool AutoReply
		{
			get { return (bool)this[AutoReplyKey]; }
			set { this[AutoReplyKey] = value; }
		}

		private const string StubOutputFileKey = "StubOutputFile";

		[ConfigurationProperty(StubOutputFileKey, IsRequired = false)]
		public string StubOutputFile
		{
			get { return (string)this[StubOutputFileKey]; }
			set { this[StubOutputFileKey] = value; }
		}

		private const string AskDeliveryKey = "AskDelivery";

		[ConfigurationProperty(AskDeliveryKey, IsRequired = false, DefaultValue = false)]
		public bool AskDelivery
		{
			get { return (bool) this[AskDeliveryKey]; }
			set { this[AskDeliveryKey] = value; }
		}

		private const string SslCertificatePathKey = "SslCertificatePath";

		[ConfigurationProperty(SslCertificatePathKey, IsRequired = false, DefaultValue = null)]
		public string SslCertificatePath
		{
			get { return (string)this[SslCertificatePathKey]; }
			set { this[SslCertificatePathKey] = value; }
		}

		private const string SslCertificatePasswordKey = "SslCertificatePassword";

		[ConfigurationProperty(SslCertificatePasswordKey, IsRequired = false, DefaultValue = null)]
		public string SslCertificatePassword
		{
			get { return (string)this[SslCertificatePasswordKey]; }
			set { this[SslCertificatePasswordKey] = value; }
		}
	}
}