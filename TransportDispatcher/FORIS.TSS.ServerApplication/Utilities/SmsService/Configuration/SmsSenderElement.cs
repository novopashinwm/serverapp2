﻿using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	/// <summary> Конфигурация правила, по которому определяется, от имени какого номера будет доставлена SMS </summary>
	public class SmsSenderElement : ConfigurationElement
	{
		private const string TargetKey = "Target";

		/// <summary> Номер телефона отправителя SMS </summary>
		[ConfigurationProperty(TargetKey, IsRequired = true)]
		public string Target
		{
			get { return (string)this[TargetKey]; }
			set { this[TargetKey] = value; }
		}

		private const string SenderKey = "Sender";

		/// <summary> Номер телефона отправителя SMS </summary>
		[ConfigurationProperty(SenderKey, IsRequired = true)]
		public string Sender
		{
			get { return (string)this[SenderKey]; }
			set { this[SenderKey] = value; }
		}
	}
}