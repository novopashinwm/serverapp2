﻿using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	public class AliasElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new AliasElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((AliasElement)element).Name;
		}

		protected override string ElementName
		{
			get
			{
				return "Alias";
			}
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.BasicMap;
			}
		}
	}
}