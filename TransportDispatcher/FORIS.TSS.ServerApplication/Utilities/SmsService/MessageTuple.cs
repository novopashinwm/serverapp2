﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public class MessageTuple
	{
		public MESSAGE Message;
		public int? SendMask;
		
		public Contact Destination
		{
			get
			{
				return Message.Contacts
					.Where(mc => mc.Type == (int) MessageContactType.Destination)
					.Select(mc => mc.Contact)
					.FirstOrDefault();
			}
		}

		public Contact Sender
		{
			get
			{
				return
					Message.Contacts
						.Where(mc => mc.Type == (int) MessageContactType.ReplyTo)
						.Select(mc => mc.Contact)
						.FirstOrDefault()
					??
					Message.Contacts
						.Where(mc => mc.Type == (int) MessageContactType.Source)
						.Select(mc => mc.Contact)
						.FirstOrDefault();
			}
		}

		public string Body { get { return Message.BODY; } }

		public int? OperatorId { get { return Message.Owner_Operator_ID; } }

		public DateTime Time { get { return Message.TIME; } }

		public int Id { get { return Message.MESSAGE_ID; } }

		public string TemplateName
		{
			get
			{
				if (Message.MESSAGE_TEMPLATE == null)
					return null;
				return Message.MESSAGE_TEMPLATE.NAME;
			}
		}

		public int? VehicleId
		{
			get
			{
				var vehicleIdString = Message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.VehicleID);

				if (string.IsNullOrWhiteSpace(vehicleIdString))
					return null;
				
				int result;
				if (!int.TryParse(vehicleIdString, out result))
					throw new InvalidOperationException("Unable to parse VehicleIdString = " + vehicleIdString + " as int");

				return result;
			}
		}

		public int? CommandId
		{
			get
			{
				var commandIdString = Message.GetFieldValue(MESSAGE_TEMPLATE_FIELD.CommandId);
				if (string.IsNullOrEmpty(commandIdString))
					return null;

				int result;
				if(!int.TryParse(commandIdString, out result))
					throw new InvalidOperationException("Unable to parse CommandIdString = " + commandIdString + "as int");

				return result;
			}
		}
	}
}