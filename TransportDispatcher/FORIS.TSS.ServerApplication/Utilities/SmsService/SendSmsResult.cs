﻿namespace FORIS.TSS.ServerApplication.SmsService
{
	public enum SendSmsResult
	{
		Success,
		InvalidMessageLength,
		InvalidDestinationAddress,
		NoConnection,
		MessageQueueFull,
		UnspecifiedError,
		HttpError,
		Timeout,
		ServiceTurnedOff,
		ThrottlingError,
		ServiceUnavailable
	}
}