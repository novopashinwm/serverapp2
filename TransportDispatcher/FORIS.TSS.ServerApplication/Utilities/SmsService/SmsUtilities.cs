﻿using System.Diagnostics;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public static class SmsUtilities
	{
		public const string FeedString = "\f";

		public static SendSmsResult TranslateXResult(int xResult)
		{
			switch (xResult)
			{
				case 0: return SendSmsResult.Success;
				case 1: return SendSmsResult.NoConnection;
				case 2: return SendSmsResult.InvalidMessageLength;
				case 3: return SendSmsResult.InvalidDestinationAddress;
				case 4: return SendSmsResult.MessageQueueFull;
				case 5: return SendSmsResult.ThrottlingError;
				default:
					Trace.TraceError("Unknown x-result during sms sending: {0}", xResult);
					return SendSmsResult.UnspecifiedError;
			}
		}
	}
}