﻿namespace FORIS.TSS.ServerApplication
{
	public abstract class AbstractGenericSingleton<T> where T : AbstractGenericSingleton<T>, new()
	{
		private class Lock
		{
		}

		private static readonly Lock InstanceLock = new Lock();
		private static T _instance;

		public static T Instance
		{
			get
			{
				if (_instance != null)
					return _instance;
				lock (InstanceLock)
				{
					if (_instance != null)
						return _instance;
					_instance = new T();
				}
				return _instance;
			}
		}
	}
}