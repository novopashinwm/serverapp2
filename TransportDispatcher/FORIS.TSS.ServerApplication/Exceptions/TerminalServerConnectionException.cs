﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.Exceptions
{
	/// <summary> Исключение, которое выбрасывается при проблемах с доступом к серверу терминалов </summary>
	[Serializable]
	public class TerminalServerConnectionException : Exception
	{
		public TerminalServerConnectionException()
		{
		}

		public TerminalServerConnectionException(string message) : base(message)
		{
		}

		public TerminalServerConnectionException(string message, Exception inner) : base(message, inner)
		{
		}

		protected TerminalServerConnectionException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}