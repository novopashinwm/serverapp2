﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using FORIS.DataAccess;
using FORIS.DataAccess.Interfaces;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Database;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;
using Sensor = FORIS.TSS.BusinessLogic.DTO.Historical.Sensor;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Менеджер базы данных </summary>
	public class CoreServerDatabaseManager : DatabaseManager
	{

		#region Constructor & Dispose

		public CoreServerDatabaseManager()
			: base()
		{
		}

		/// <summary> Конструктор </summary>
		/// <param name="container"></param>
		public CoreServerDatabaseManager(IContainer container)
			: base(container)
		{
		}

		#endregion Constructor & Dispose

		// SQL script list. just keeping pigs together
		// Info: need optimization
		const string SqlGetLastPositions = "exec dbo.GetLastPositions";
		const string SqlMediaList = "exec dbo.GetMedia";
		#region GetUnitNumberByID(int [] group)
		/// <summary> Извлечение идентификаторов мобильных объектов из базы данных по списку первичных ключей </summary>
		/// <param name="group"> массив первичных ключей базы данных </param>
		/// <returns> массив идентификаторов мобильных объектов </returns>
		public int[] GetUnitNumberByID(int[] group)
		{
			var muID = new int[group.Length];

			try
			{
				using (var sp = Database.Procedure("dbo.GetUnitNumbers"))
				{
					DataSet ds = sp.ExecuteDataSet();
					DataRowCollection drs = ds.Tables[0].Rows;

					var vidToNumber = drs
						.Cast<DataRow>()
						.Where(dr => !dr.IsNull("VEHICLE_ID"))
						.ToDictionary(dr => (int) dr["VEHICLE_ID"], dr => (int) dr["NUMBER"]);

					int count = group.Length;
					for (int i = 0; i < count; i++)
					{
						int number;
						if (!vidToNumber.TryGetValue(group[i], out number))
							continue;
						muID[i] = number;
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}

			return muID;
		}
		#endregion GetUnitNumberByID(int [] group)
		/// <summary> Возвращает детальную информацию по списку объектов пользователя </summary>
		/// <param name="operatorID"> Идентификатор пользователя </param>
		/// <param name="vehicleIDs"> Список идентификаторов объектов наблюдения (опционально) </param>
		/// <param name="departmentId"> Идентификатор департамента (опционально) </param>
		/// <param name="currentDate"> Дата </param>
		/// <param name="lastData"> Заполнять ли информацию о местоположении и состоянии объекта </param>
		/// <param name="attributes"> Заполнять ли атрибуты объектов </param>
		/// <param name="includeZones"> Заполнять ли информацию о зонах, в которых находится объект </param>
		/// <returns></returns>
		public List<Vehicle> GetVehiclesWithPositions(int operatorID, int[] vehicleIDs, int? departmentId, bool lastData, bool attributes = true,
			DateTime? currentDate = null, bool includeZones = false)
		{
			using (var sp = Database.Procedure("dbo.GetVehiclesWithPositions"))
			{
				sp["@operatorID"].Value = operatorID;

				if (vehicleIDs != null && vehicleIDs.Length != 0)
				{
					var vehicleIDsTable = GetIdTable(vehicleIDs);
					sp.SetTableValuedParameter("@vehicleIDs", vehicleIDsTable);
				}

				sp["@departmentId"].Value      = departmentId;
				sp["@includeLastData"].Value   = lastData;
				sp["@includeAttributes"].Value = attributes;
				sp["@includeZone"].Value       =  includeZones;

				if (currentDate.HasValue)
				{
					sp["@currentLogTime"].Value = TimeHelper.GetSecondsFromBase(currentDate.Value);
				}

				using (new ConnectionOpener(sp.Command.Connection))
				{
					using (var reader = sp.ExecuteReader())
					{
						//ALLOW_VEHICLE: common information about vehicles
						var result = ReadVehicles(reader, attributes);

						//Useful indices
						var vidToVehicle = result.ToDictionary(v => v.id);
						var asidIdToVehicle = result.Where(a => a.asidId != null).ToLookup(a => a.asidId.Value);

						if (attributes)
						{
							if (!reader.NextResult())
								return result;
							//VEHICLE_PROFILE: url of icon
							ReadVehicleProfile(reader, vidToVehicle);

							if (!reader.NextResult())
								return result;
							//Vehicle_Right
							ReadVehicleRights(reader, vidToVehicle);

							var billingServices = GetVehicleServices(vidToVehicle.Keys.ToArray());
							AssignBillingServices(billingServices, vidToVehicle);

							if (!reader.NextResult())
								throw new InvalidOperationException("Unable to read RenderedServiceItem");
							//RenderedServiceItem
							ReadRenderedServiceItem(reader, vidToVehicle);

							if (!reader.NextResult())
								throw new InvalidOperationException("Unable to read Billing_Blocking");
							//Billing_Blocking
							ReadBillingBlockings(reader, asidIdToVehicle);
						}

						if (!reader.NextResult())
							return result;
						//SENSOR_MAPPING
						ReadSensorMapping(reader, vidToVehicle);

						if (!reader.NextResult())
							return result;
						//POSITION
						ReadPosition(reader, vidToVehicle);

						if (!reader.NextResult())
							return result;
						//STATE_LOG
						ReadStateLog(reader, vidToVehicle);

						if (!reader.NextResult())
							return result;
						//MAPPED_SENSOR
						ReadMappedSensor(reader, vidToVehicle);
						foreach (var v in result)
						{
							var ignition = v.GetSensorValue(SensorLegend.Ignition);
							v.ignition = ignition != null ? (ignition.Value != 0) : (bool?)null;
							v.fuel = v.GetSensorValue(SensorLegend.Fuel);
						}

						if (!reader.NextResult())
							return result;

						//COMMAND
						foreach (var commandLookup in ReadCommand(reader).ToLookup(c => c.TargetID))
						{
							var vid = commandLookup.Key;
							Vehicle vehicle;
							if (vidToVehicle.TryGetValue(vid, out vehicle))
								vehicle.commands = commandLookup.ToArray();
						}

						if (!reader.NextResult())
							return result;
						//PICTURE_LOG
						ReadPictures(reader, vidToVehicle);

						//ZONES
						if (!reader.NextResult())
							return result;

						if (includeZones)
						{
							ReadZones(reader, vidToVehicle);
						}

						return result;
					}
				}
			}
		}
		private void AssignBillingServices(List<BillingService> billingServices, Dictionary<int, Vehicle> vidToVehicle)
		{
			foreach (var billingService in billingServices)
			{
				if (billingService.VehicleId == null)
					continue;

				Vehicle vehicle;
				if (!vidToVehicle.TryGetValue(billingService.VehicleId.Value, out vehicle))
					continue;
				if (!vehicle.rights.Contains(SystemRight.SecurityAdministration) &&
					!vehicle.rights.Contains(SystemRight.PayForCaller))
					continue;

				if (vehicle.billingServices == null)
					vehicle.billingServices = new List<BillingService>();
				vehicle.billingServices.Add(billingService);
			}

			foreach (var vehicle in vidToVehicle.Values)
			{
				if (!vehicle.rights.Contains(SystemRight.SecurityAdministration))
					continue;

				if (vehicle.billingServices == null)
					vehicle.billingServices = new List<BillingService>();
			}
		}
		private void ReadStateLog(IDataReader reader, Dictionary<int, Vehicle> vehicles)
		{
			var vehicleIdOrdinal = reader.GetOrdinal("Vehicle_ID");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var typeOrdinal = reader.GetOrdinal("Type");
			var valueOrdinal = reader.GetOrdinal("Value");

			while (reader.Read())
			{
				var vehicleId = reader.GetInt32(vehicleIdOrdinal);
				var logTime = reader.GetInt32(logTimeOrdinal);
				var type = reader.GetByte(typeOrdinal);
				var value = reader.GetInt32(valueOrdinal);

				Vehicle vehicle;
				if(!vehicles.TryGetValue(vehicleId, out vehicle))
					continue;

				if(vehicle.State == null)
					vehicle.State = new Dictionary<VehicleStateType, VehicleState>();

				var state = new VehicleState
					{
						LogTime = logTime,
						Value = value
					};
				vehicle.State.Add((VehicleStateType)type, state);
			}
		}
		private void ReadZones(IDataReader reader, Dictionary<int, Vehicle> vehicles)
		{
			var vehicleIdOrdinal = reader.GetOrdinal("Vehicle_ID");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var zoneIdOrdinal = reader.GetOrdinal("Zone_ID");

			while (reader.Read())
			{
				var vehicleId = reader.GetInt32(vehicleIdOrdinal);
				var logTime = reader.GetInt32(logTimeOrdinal);
				var zoneId = reader.GetInt32(zoneIdOrdinal);

				Vehicle vehicle;
				if(!vehicles.TryGetValue(vehicleId, out vehicle))
					continue;

				if (vehicle.ZoneIds == null)
				{
					vehicle.ZoneIds = new List<int>();
				}

				vehicle.ZoneIds.Add(zoneId);
			}
		}
		private void ReadRenderedServiceItem(IDataReader reader, Dictionary<int, Vehicle> idToVehicle)
		{
			var vehicleIdOrdinal = reader.GetOrdinal("Vehicle_ID");
			var renderedServiceNameOrdinal = reader.GetOrdinal("Rendered_Service_Type_Name");
			var quantityOrdinal = reader.GetOrdinal("Quantity");

			while (reader.Read())
			{
				var vehicleId = reader.GetInt32(vehicleIdOrdinal);
				var renderedServiceName = reader.GetString(renderedServiceNameOrdinal);
				var quantity = reader.GetInt32(quantityOrdinal);

				Vehicle vehicle;
				if (!idToVehicle.TryGetValue(vehicleId, out vehicle))
					continue;

				RenderedServiceType type;
				if (!Enum.TryParse(renderedServiceName, out type))
					continue;

				if (vehicle.RenderedServices == null)
					vehicle.RenderedServices = new List<RenderedService>();
				vehicle.RenderedServices.Add(new RenderedService { Type = type, Quantity = quantity });
			}
		}
		private void ReadQuantityLeft(IDataReader reader, IDictionary<int, BillingService> idToBillingService)
		{
			var bsIdOrdinal = reader.GetOrdinal("Billing_Service_ID");
			var vehicleIdOrdinal = reader.GetOrdinal("Vehicle_ID");
			var renderedServiceTypeNameOrdinal = reader.GetOrdinal("Rendered_Service_Type_Name");
			var quantityLeftOrdinal = reader.GetOrdinal("Quantity_Left"); //Доступный остаток для услуг со счётчиками
			var lastTimeOrdinal = reader.GetOrdinal("LastTime");

			while (reader.Read())
			{
				var bsId = reader.GetInt32(bsIdOrdinal);
				var vehicleId = reader.GetInt32(vehicleIdOrdinal);
				var serviceTypeCategoryString = reader.GetString(renderedServiceTypeNameOrdinal);
				var quantityLeft = reader.IsDBNull(quantityLeftOrdinal)
					? (int?) null
					: reader.GetInt32(quantityLeftOrdinal);
				var lastTime = reader.IsDBNull(lastTimeOrdinal) ? (DateTime?) null : reader.GetDateTime(lastTimeOrdinal);

				RenderedServiceType renderedServiceType;
				if (!Enum.TryParse(serviceTypeCategoryString, true, out renderedServiceType))
				{
					Trace.TraceWarning("{0}: Unable to parse service type category as enum: {1}", this,
						serviceTypeCategoryString);
					continue;
				}

				BillingService billingService;
				if (!idToBillingService.TryGetValue(bsId, out billingService))
					continue;

				//Остатки
				if (billingService.Status == null)
					billingService.Status = new Dictionary<RenderedServiceType, BillingServiceStatus>();

				BillingServiceStatus status;
				if (!billingService.Status.TryGetValue(renderedServiceType, out status))
					billingService.Status.Add(renderedServiceType, status = new BillingServiceStatus());

				if (quantityLeft != null)
					status.QuantityLeft = (status.QuantityLeft ?? 0) + quantityLeft.Value;

				if (lastTime != null)
				{
					if (status.LastTime == null || status.LastTime.Value < lastTime.Value)
						status.LastTime = lastTime;
				}
			}

			foreach (var billingService in idToBillingService.Values.Where(bs => bs.Status != null))
			{
				foreach (var statusItem in billingService.Status)
				{
					var status = statusItem.Value;

					if (billingService.MinIntervalSeconds == null || status.LastTime == null)
						continue;

					status.NextTime = status.LastTime.Value.AddSeconds(billingService.MinIntervalSeconds.Value);
				}
			}
		}
		private void ReadSensorMapping(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			foreach (var vehicle in vidToVehicle.Values)
			{
				vehicle.Sensors = new List<Sensor>();
			}

			var vidOrdinal = reader.GetOrdinal("VEHICLE_ID");
			var legendNumberOrdinal = reader.GetOrdinal("LegendNumber");
			var typeOrdinal = reader.GetOrdinal("Controller_Sensor_Type_ID");

			while (reader.Read())
			{
				var vid = reader.GetInt32(vidOrdinal);
				var number = (SensorLegend)reader.GetInt32(legendNumberOrdinal);
				var type = (SensorType)reader.GetInt32(typeOrdinal);

				Vehicle vehicle;
				if (!vidToVehicle.TryGetValue(vid, out vehicle))
					continue;

				vehicle.Sensors.Add(new Sensor
					{
						Number = number,
						Type = type,
					});
			}
		}
		public static DataTable GetIdTable(IEnumerable<int> vehicleIDs)
		{
			var vehicleIDsTable = new DataTable("VehicleIDs");
			vehicleIDsTable.Columns.Add("Id", typeof (int));
			foreach (var id in vehicleIDs)
				vehicleIDsTable.Rows.Add(id);
			return vehicleIDsTable;
		}
		public static DataTable GetMsisdnTable(IEnumerable<string> msisdns)
		{
			var msisdnsTable = new DataTable("msisdns");
			msisdnsTable.Columns.Add("value", typeof(string));
			if(msisdns != null)
				foreach (var msisdn in msisdns)
					msisdnsTable.Rows.Add(msisdn);
			return msisdnsTable;
		}
		private void ReadPictures(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			var vidOrdinal = reader.GetOrdinal("Vehicle_ID");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var cameraNumberOrdinal = reader.GetOrdinal("Camera_Number");
			var photoNumberOrdinal = reader.GetOrdinal("Photo_Number");

			while (reader.Read())
			{
				if (reader.IsDBNull(logTimeOrdinal))
					continue;
				var vehicle = vidToVehicle[reader.GetInt32(vidOrdinal)];

				if (vehicle.Pictures == null)
					vehicle.Pictures = new List<Picture>();

				var picture = new Picture();
				vehicle.Pictures.Add(picture);
				picture.Date = TimeHelper.GetDateTimeUTC(reader.GetInt32(logTimeOrdinal));
				picture.CameraNumber = reader.GetInt32(cameraNumberOrdinal);
				picture.PhotoNumber = reader.GetInt32(photoNumberOrdinal);
			}
		}
		private IEnumerable<Command> ReadCommand(IDataReader reader)
		{
			var idOrdinal = reader.GetOrdinal("id");
			var targetIDOrdinal = reader.GetOrdinal("Target_ID");
			var typeOrdinal = reader.GetOrdinal("Type_ID");
			var resultOrdinal = reader.GetOrdinal("Result_Type_ID");
			var resultDateOrdinal = reader.GetOrdinal("Result_Date");
			var payerVehicleIDOrdinal = reader.GetOrdinal("Payer_Vehicle_ID");
			var statusOrdinal = reader.GetOrdinal("Status");
			var createdOrdinal  = reader.GetOrdinal("Created");
			while (reader.Read())
			{
				var id = reader.GetInt32(idOrdinal);
				var cmdType = (CmdType) reader.GetInt32(typeOrdinal);
				var targetID = reader.GetInt32(targetIDOrdinal);
				var created = reader.IsDBNull(createdOrdinal) ? (DateTime?)null : reader.GetDateTime(createdOrdinal);
				var cmdResult = reader.IsDBNull(resultOrdinal) ? (CmdResult?) null : (CmdResult) reader.GetInt32(resultOrdinal);
				var resultDate = reader.IsDBNull(resultDateOrdinal) ? (DateTime?) null : reader.GetDateTime(resultDateOrdinal);
				var payerVehicleID = reader.IsDBNull(payerVehicleIDOrdinal) ? (int?)null : reader.GetInt32(payerVehicleIDOrdinal);
				var status = (CmdStatus)reader.GetInt32(statusOrdinal);
				yield return new Command
								  {
									  Id = id,
									  Type = cmdType,
									  TargetID = targetID,
									  Created = created,
									  Status = status,
									  Result = cmdResult,
									  ResultDate = resultDate,
									  PayerVehicleID = payerVehicleID
								  };
			}
		}
		private void ReadMappedSensor(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			foreach (var vehicle in vidToVehicle.Values)
			{
				if (vehicle.Sensors == null)
					vehicle.Sensors = new List<Sensor>();
			}

			var vidOrdinal = reader.GetOrdinal("VEHICLE_ID");
			var valueOrdinal = reader.GetOrdinal("Value");
			var legendNumberOrdinal = reader.GetOrdinal("LegendNumber");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");

			while (reader.Read())
			{
				if (reader.IsDBNull(valueOrdinal) || reader.IsDBNull(logTimeOrdinal))
					continue;
				var vid = reader.GetInt32(vidOrdinal);
				var value = reader.GetDecimal(valueOrdinal);
				var number = (SensorLegend)reader.GetInt32(legendNumberOrdinal);
				var logTime = reader.GetInt32(logTimeOrdinal);

				Vehicle vehicle;
				if (!vidToVehicle.TryGetValue(vid, out vehicle))
					continue;

				Sensor sensor = vehicle.GetSensor(number);
				if (sensor == null)
					continue;

				sensor.LogRecords = new List<SensorLogRecord>
				{
					new SensorLogRecord(logTime, value)
				};
			}
		}
		private void ReadPosition(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			var vidOrdinal          = reader.GetOrdinal("Vehicle_ID");
			var latOrdinal          = reader.GetOrdinal("Lat");
			var lngOrdinal          = reader.GetOrdinal("Lng");
			var courseOrdinal       = reader.GetOrdinal("Course");
			var gpscourseOrdinal    = reader.GetOrdinal("GPS_Course");
			var speedOrdinal        = reader.GetOrdinal("Speed");
			var radiusOrdinal       = reader.GetOrdinal("Radius");
			var logTimeOrdinal      = reader.GetOrdinal("Log_Time");
			var posLogTimeOrdinal   = reader.GetOrdinal("Geo_Log_Time");
			var isLBSDataOriginal   = reader.GetOrdinal("IsLBSPosition");
			var positionTypeOrdinal = reader.GetOrdinal("PositionType");
			var dumpingOrdinal      = reader.GetOrdinal("Dumping");
			var insertDelaySOrdinal = reader.GetOrdinal("InsertDelayS");

			while (reader.Read())
			{
				var vid = reader.GetInt32(vidOrdinal);
				Vehicle vehicle;
				if (!vidToVehicle.TryGetValue(vid, out vehicle))
					continue;

				vehicle.IsLBSPosition = !reader.IsDBNull(isLBSDataOriginal) && reader.GetBoolean(isLBSDataOriginal);

				vehicle.lat = (double?)(reader.IsDBNull(latOrdinal) ? default(decimal?) : reader.GetDecimal(latOrdinal));
				vehicle.lng = (double?)(reader.IsDBNull(lngOrdinal) ? default(decimal?) : reader.GetDecimal(lngOrdinal));

				vehicle.MotionCourse = reader.IsDBNull(courseOrdinal)    ? (int?)null : reader.GetInt32(courseOrdinal);
				vehicle.gpscourse    = reader.IsDBNull(gpscourseOrdinal) ? (int?)null : Convert.ToInt32(reader.GetValue(gpscourseOrdinal));

				vehicle.course = vehicle.gpscourse ?? vehicle.MotionCourse;

				vehicle.speed = reader.IsDBNull(speedOrdinal) ? (int?)null : reader.GetByte(speedOrdinal);
				vehicle.PositionType = reader.IsDBNull(positionTypeOrdinal)
					? (PositionType?) null
					: (PositionType)reader.GetByte(positionTypeOrdinal);

				vehicle.radius = reader.IsDBNull(radiusOrdinal) ? 0 : reader.GetInt32(radiusOrdinal);
				vehicle.logTime = reader.IsDBNull(logTimeOrdinal) ? (int?)null : reader.GetInt32(logTimeOrdinal);
				if (vehicle.logTime != null)
					vehicle.Date = TimeHelper.GetDateTimeUTC(vehicle.logTime.Value);

				vehicle.posLogTime = reader.IsDBNull(posLogTimeOrdinal) ? (int?)null : reader.GetInt32(posLogTimeOrdinal);
				if (vehicle.posLogTime != null)
					vehicle.posDate = TimeHelper.GetDateTimeUTC(vehicle.posLogTime.Value);

				vehicle.isValid = (vehicle.logTime - vehicle.posLogTime) < 2 * 60; //Координата GPS отстает от сигнала не более чем 2 минуты
				vehicle.Dumping = reader.GetBoolean(dumpingOrdinal);
				vehicle.InsertDelayS = reader.IsDBNull(insertDelaySOrdinal)
					? (int?)null
					: reader.GetInt32(insertDelaySOrdinal);
			}
		}
		private void ReadBillingBlockings(IDataReader reader, ILookup<int, Vehicle> asidIdToVehicle)
		{
			var asidIdOrdinal = reader.GetOrdinal("Asid_ID");
			var blockingDateOrdinal = reader.GetOrdinal("BlockingDate");

			var asidIdToBillingBlocking = new Dictionary<int, BillingBlocking>();
			while (reader.Read())
			{
				var billingBlocking = new BillingBlocking();
				var blockingDate = reader.GetDateTime(blockingDateOrdinal);
				billingBlocking.BlockingDate = blockingDate;

				var asidId = reader.GetInt32(asidIdOrdinal);
				asidIdToBillingBlocking[asidId] = billingBlocking;
			}

			foreach (var pair in asidIdToBillingBlocking)
			{
				foreach (var vehicle in asidIdToVehicle[pair.Key])
					vehicle.BillingBlocking = pair.Value;
			}
		}
		private List<BillingService> ReadBillingServices(IDataReader reader)
		{
			var billingServices = ReadBillingServiceRows(reader);
			var idToBillingService = billingServices.Where(bs => bs.Id != null).ToDictionary(bs => bs.Id.Value);

			if (!reader.NextResult())
				throw new InvalidOperationException("Unable to read Quantity_Left");

			//Quantity_Left
			ReadQuantityLeft(reader, idToBillingService);

			return billingServices;
		}
		private List<BillingService> ReadBillingServiceRows(IDataReader reader)
		{
			var vehicleIdOrdinal           = reader.GetOrdinal("Vehicle_ID");
			var bsIdOrdinal                = reader.GetOrdinal("Billing_Service_ID");
			var nameOrdinal                = reader.GetOrdinal("Name");
			var serviceTypeCategoryOrdinal = reader.GetOrdinal("Service_Type_Category");
			var startDateOrdinal           = reader.GetOrdinal("StartDate");
			var endDateOrdinal             = reader.GetOrdinal("EndDate");
			var smsOrdinal                 = reader.GetOrdinal("SMS");
			var lbsOrdinal                 = reader.GetOrdinal("LBS");
			var isTrialOrdinal             = reader.GetOrdinal("isTrial");
			var isActivatedOrdinal         = reader.GetOrdinal("isActivated");
			var maxQuantityOrdinal         = reader.GetOrdinal("maxQuantity");
			var minIntervalSecondsOrdinal  = reader.GetOrdinal("minIntervalSeconds");
			var isBlockedOrdinal           = reader.GetOrdinal("isBlocked");
			var maxLogDepthHoursOrdinal    = reader.GetOrdinal("MaxLogDepthHours");
			var trackingScheduleOrdinal    = reader.GetOrdinal("TrackingSchedule");

			var billingServices = new List<BillingService>();
			while (reader.Read())
			{
				var bsId = reader.GetInt32(bsIdOrdinal);
				var vehicleId = reader.GetInt32(vehicleIdOrdinal);

				var serviceTypeCategoryString = reader.GetString(serviceTypeCategoryOrdinal);
				var name = reader.GetStringOrNull(nameOrdinal);
				var startDate = reader.GetDateTime(startDateOrdinal);
				var endDate = reader.IsDBNull(endDateOrdinal) ? (DateTime?) null : reader.GetDateTime(endDateOrdinal);
				var maxQuantity = reader.IsDBNull(maxQuantityOrdinal)
									  ? (int?) null
									  : reader.GetInt32(maxQuantityOrdinal);
				var minIntervalSeconds = reader.IsDBNull(minIntervalSecondsOrdinal)
											 ? (int?) null
											 : reader.GetInt32(minIntervalSecondsOrdinal);
				var isSharedOrdinal = reader.GetOrdinal("isShared");

				ServiceTypeCategory serviceTypeCategory;
				if (!Enum.TryParse(serviceTypeCategoryString, true, out serviceTypeCategory))
				{
					Trace.TraceWarning("Unable to parse service type category as enum: {0}", serviceTypeCategoryString);
					continue;
				}

				var billingService = new BillingService();
				billingServices.Add(billingService);
				billingService.Id = bsId;
				billingService.Name = name;
				billingService.Type = serviceTypeCategory;
				billingService.StartDate = startDate;
				billingService.EndDate = endDate;
				billingService.Sms = reader.GetBoolean(smsOrdinal);
				billingService.Lbs = reader.GetBoolean(lbsOrdinal);
				billingService.IsTrial = reader.GetBoolean(isTrialOrdinal);
				billingService.IsActivated = reader.GetBoolean(isActivatedOrdinal);
				billingService.MaxQuantity = maxQuantity;
				billingService.MinIntervalSeconds = minIntervalSeconds;
				billingService.IsShared = reader.GetBoolean(isSharedOrdinal);
				billingService.VehicleId = vehicleId;
				billingService.IsBlocked = reader.GetBoolean(isBlockedOrdinal);
				billingService.MaxLogDepthHours = reader.GetInt32OrNull(maxLogDepthHoursOrdinal);
				billingService.TrackingSchedule = reader.GetTrueOrNull(trackingScheduleOrdinal);
			}

			return billingServices;
		}
		private static readonly SystemRight[] EmptySystemRightsArray = new SystemRight[] {};
		private void ReadVehicleRights(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			var vehicleIdOrdinal = reader.GetOrdinal("vehicle_id");
			var rightIdColumn = reader.GetOrdinal("right_id");

			var vidToRightIds = new Dictionary<int, List<SystemRight>>();
			while (reader.Read())
			{
				var vid = reader.GetInt32(vehicleIdOrdinal);
				var right = (SystemRight) reader.GetInt32(rightIdColumn);
				List<SystemRight> rights;
				if (!vidToRightIds.TryGetValue(vid, out rights))
					vidToRightIds.Add(vid, rights = new List<SystemRight>());
				rights.Add(right);
			}

			foreach (var pair in vidToRightIds)
			{
				var vid = pair.Key;
				Vehicle v;
				if (!vidToVehicle.TryGetValue(vid, out v))
					continue;
				v.rights = pair.Value.ToArray();
			}

			foreach (var v in vidToVehicle.Values.Where(v => v.rights == null))
				v.rights = EmptySystemRightsArray;
		}
		private static List<Vehicle> ReadVehicles(IDataReader reader, bool attributes)
		{
			var result = new List<Vehicle>();
			var vehicleIdOrdinal = reader.GetOrdinal("vehicle_id");

			if (attributes)
			{
				var garageNumberOrdinal = reader.GetOrdinal("garage_number");
				var fuelTankOrdinal = reader.GetOrdinal("fuel_tank");
				var vehicleKindOrdinal = reader.GetOrdinal("vehicle_kind_id");
				var brandOrdinal = reader.GetOrdinal("vehicle_type");
				var regNumOrdinal = reader.GetOrdinal("public_number");
				var ctrlNumOrdinal = reader.GetOrdinal("Controller_Number");
				var phoneOrdinal = reader.GetOrdinal("phone");
				var phoneIsEditableOrdinal = reader.GetOrdinal("phoneIsEditable");
				var asidIdOrdinal = reader.GetOrdinal("Asid_ID");
				var departmentIdOrdinal = reader.GetOrdinal("Department_ID");
				var departmentNameOrdinal = reader.GetOrdinal("Department_Name");
				var allowMlpRequestOrdinal = reader.GetOrdinal("allowMlpRequest");
				var knownMaskedMsisdnOrdinal = reader.GetOrdinal("KnownMaskedMsisdn");
				var controllerTypeNameOrdinal = reader.GetOrdinal("CONTROLLER_TYPE_NAME");
				var controllerTypeHasInputsOrdinal = reader.GetOrdinal("CONTROLLER_TYPE_HasInputs");
				var controllerHasIPOrdinal = reader.GetOrdinal("CONTROLLER_HasIP");
				var controllerTypeSupportsPositionAskingOrdinal = reader.GetOrdinal("CONTROLLER_TYPE_SupportsPositionAsking");
				var controllerTypeDeviceIdIsImeiOrdinal = reader.GetOrdinal("CONTROLLER_TYPE_DeviceIdIsImei");

				var ownerOperatorOperatorIDOrdinal = reader.GetOrdinal("OwnerOperator_Operator_ID");
				var ownerOperatorLoginOrdinal = reader.GetOrdinal("OwnerOperator_Login");
				var ownerOperatorNameOrdinal = reader.GetOrdinal("OwnerOperator_Name");
				var ownerOperatorPhoneOrdinal = reader.GetOrdinal("OwnerOperatorPhone");
				var ownerOperatorEmailOrdinal = reader.GetOrdinal("OwnerOperatorEmail");

				var maxAllowedSpeedOrdinal = reader.GetOrdinal("MaxAllowedSpeed");
				var isFriendVehicleOrdinal = reader.GetOrdinal("isFriendVehicle");

				var isOwnVehicleOrdinal = reader.GetOrdinal("isOwnVehicle");

				while (reader.Read())
				{
					var vehicle = new Vehicle();
					vehicle.id = reader.GetInt32(vehicleIdOrdinal);
					var garageNumber = reader.IsDBNull(garageNumberOrdinal) ? null : reader.GetString(garageNumberOrdinal);
					vehicle.garageNum = garageNumber;
					vehicle.fuelTankVolume = reader.IsDBNull(fuelTankOrdinal)
												 ? (decimal?) null
												 : Convert.ToDecimal(reader.GetValue(fuelTankOrdinal));
					vehicle.MaxAllowedSpeed = reader.IsDBNull(maxAllowedSpeedOrdinal)
												  ? (int?) null
												  : reader.GetInt32(maxAllowedSpeedOrdinal);

					vehicle.vehicleKind = (VehicleKind) reader.GetInt32(vehicleKindOrdinal);
					vehicle.brand = reader.IsDBNull(brandOrdinal) ? null : reader.GetString(brandOrdinal);
					vehicle.regNum = reader.IsDBNull(regNumOrdinal) ? null : reader.GetString(regNumOrdinal);
					vehicle.status = 0;
					vehicle.ctrlNum = reader.IsDBNull(ctrlNumOrdinal) ? null : reader.GetString(ctrlNumOrdinal);
					vehicle.phone = reader.IsDBNull(phoneOrdinal) ? null : reader.GetString(phoneOrdinal);
					vehicle.PhoneIsEditable = reader.IsDBNull(phoneIsEditableOrdinal)
												  ? (bool?)null
												  : reader.GetBoolean(phoneIsEditableOrdinal);
					vehicle.asidId = reader.IsDBNull(asidIdOrdinal) ? (int?) null : reader.GetInt32(asidIdOrdinal);
					vehicle.departmentId = reader.IsDBNull(departmentIdOrdinal) ? (int?) null : reader.GetInt32(departmentIdOrdinal);
					vehicle.departmentName = reader.IsDBNull(departmentNameOrdinal) ? null : reader.GetString(departmentNameOrdinal);

					vehicle.allowMlpRequest = reader.IsDBNull(allowMlpRequestOrdinal)
												  ? (bool?) null
												  : reader.GetBoolean(allowMlpRequestOrdinal);
					vehicle.KnownMaskedMsisdn = reader.IsDBNull(knownMaskedMsisdnOrdinal)
												  ? (bool?) null
												  : reader.GetBoolean(knownMaskedMsisdnOrdinal);
					vehicle.isFriendVehicle = reader.GetBoolean(isFriendVehicleOrdinal);

					if (!reader.IsDBNull(controllerTypeNameOrdinal))
					{
						vehicle.controllerType = new ControllerType();
						vehicle.controllerType.Name = reader.GetString(controllerTypeNameOrdinal);
						vehicle.controllerType.HasInputs = reader.GetBoolean(controllerTypeHasInputsOrdinal);
						vehicle.controllerType.SupportsPositionAsking = reader.GetBoolean(controllerTypeSupportsPositionAskingOrdinal);
						vehicle.controllerType.DeviceIdIsImei = reader.GetBoolean(controllerTypeDeviceIdIsImeiOrdinal);
					}

					vehicle.HasIP = reader.IsDBNull(controllerHasIPOrdinal) ? (bool?) null : reader.GetBoolean(controllerHasIPOrdinal);

					vehicle.IsOwnVehicle = reader.GetBoolean(isOwnVehicleOrdinal);

					if (!reader.IsDBNull(ownerOperatorOperatorIDOrdinal))
					{
						vehicle.Owner       = new BusinessLogic.DTO.OperatorRights.Operator();
						vehicle.Owner.id    = reader.GetInt32(ownerOperatorOperatorIDOrdinal);
						vehicle.Owner.login = reader.IsDBNull(ownerOperatorLoginOrdinal)
							? null
							: reader.GetString(ownerOperatorLoginOrdinal);
						vehicle.Owner.name  = reader.IsDBNull(ownerOperatorNameOrdinal)
							? null
							: reader.GetString(ownerOperatorNameOrdinal);
						if (!reader.IsDBNull(ownerOperatorPhoneOrdinal))
						{
							vehicle.Owner.Phones = new[]
							{
								new Contact(ContactType.Phone, reader.GetString(ownerOperatorPhoneOrdinal))
								{
									IsConfirmed = true
								}
							};
						}
						if (!reader.IsDBNull(ownerOperatorEmailOrdinal))
						{
							vehicle.Owner.Emails = new[]
							{
								new Contact(ContactType.Email, reader.GetString(ownerOperatorEmailOrdinal))
								{
									IsConfirmed = true
								}
							};
						}
						vehicle.Owner.FormatContactInformation();
					}

					result.Add(vehicle);
				}
			}
			else
			{
				while (reader.Read())
				{
					var vehicle = new Vehicle();
					vehicle.id = reader.GetInt32(vehicleIdOrdinal);
					result.Add(vehicle);
				}
			}

			return result;
		}
		private void ReadVehicleProfile(IDataReader reader, Dictionary<int, Vehicle> vidToVehicle)
		{
			var ordinalPropVid = reader.GetOrdinal("vehicle_id");
			var ordinalPropNam = reader.GetOrdinal("property_name");
			var ordinalPropVal = reader.GetOrdinal("value");

			while (reader.Read())
			{
				// Отсутствует значение свойства, поэтому пропускаем
				if (reader.IsDBNull(ordinalPropVal))
					continue;

				var nam = reader.GetString(ordinalPropNam);
				var typ = default(VehicleProfile);
				if (!Enum.TryParse(nam, true, out typ))
					continue;
				var val = reader.GetValue(ordinalPropVal) as string;
				if (val == null)
					continue;
				var vid = reader.GetInt32(ordinalPropVid);
				var veh = default(Vehicle);
				if (!vidToVehicle.TryGetValue(vid, out veh))
					continue;
				if (veh == null)
					continue;
				switch (typ)
				{
					case VehicleProfile.Notes:
						veh.Notes = val;
						break;
					case VehicleProfile.Icon:
						veh.iconUrl = val;
						break;
					case VehicleProfile.OnlineGeocoding:
						var onlineGeocoding = default(bool);
						if (bool.TryParse(val, out onlineGeocoding))
							veh.IsOnlineGeocoding = onlineGeocoding;
						break;
					default:
						break;
				}
			}
		}
		#region GetLastPositions()
		public IDictionary<int, IMobilUnit> GetLastPositions()
		{
			Dictionary<int, IMobilUnit> Result = new Dictionary<int, IMobilUnit>(200);

			using (IDbCommand command = Database.Command(SqlGetLastPositions, System.Data.CommandType.Text))
			{
				using (IDataReader dr = Database.ExecuteReader(command))
				{
					while (dr.Read())
					{
						var mu = new MobilUnit.Unit.MobilUnit();

						mu.Unique = (int)dr["MONITOREE_ID"];

						mu.Time = (int)dr["LOG_TIME"];

						if (dr["X"] == DBNull.Value || dr["Y"] == DBNull.Value)
						{
							mu.CorrectGPS = false;
						}
						else
						{
							//TODO: include position  type
							mu.Speed = dr["SPEED"] as byte? ?? 0;
							mu.Radius = dr["radius"] as int? ?? 0;
							mu.Longitude = Convert.ToDouble(dr["X"]);
							mu.Latitude = Convert.ToDouble(dr["Y"]);

							if (!(mu.Latitude < 90f && mu.Longitude < 180f))
								mu.CorrectGPS = false;
							else
								mu.CorrectGPS = true;

							mu.TermID = dr["MEDIA"] == DBNull.Value ? 1 : (int)(byte)dr["MEDIA"];
						}

						if (Result.ContainsKey(mu.Unique))
							Result.Remove(mu.Unique);

						Result.Add(mu.Unique, mu);
					}
				}
			}

			#region заполнение номеров контроллеров

			int[] group = new int[Result.Count];

			Result.Keys.CopyTo(group, 0);

			int[] units = GetUnitNumberByID(group);

			for (int i = 0; i < units.Length; i++)
			{
				Result[group[i]].ID = units[i];
			}

			#endregion заполнение номеров контроллеров

			return Result;
		}
		#endregion GetLastPositions()
		#region FillUnitInfo(IUnitInfo unitInfo)
		/// <summary> get full info about unit </summary>
		/// <param name="ui">part filled obj</param>
		public void FillUnitInfo(IUnitInfo ui)
		{
			try
			{
				using (var sp = Database.Procedure("dbo.GetUnitInfo"))
				{
					sp["@uniqueID"].Value = ui.Unique;
					sp["@id"].Value = ui.ID;
					sp["@phone"].Value = ui.Telephone;
					sp["@device_id"].Value = ui.DeviceID;

					using (new ConnectionOpener(sp.Command.Connection))
					using (IDataReader dr = sp.ExecuteReader())
					{
						if (dr.Read())
						{
							ui.Unique = (int)dr["Unique"];
							ui.ID = (int)dr["ID"];
							ui.Telephone = (string)dr["Phone"];

							if (ui.IP.Address != IPAddress.Any) return;


							#region Fill IP Address

							var lastPositions = Server.Instance()
								.GetLastPositions(new int[] { ui.Unique });

							if (lastPositions != null && lastPositions.Count != 0)
								ui.IP = lastPositions[ui.Unique].IP;

							#endregion Fill IP Address
						}

						dr.Close();
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}
		#endregion FillUnitInfo(IUnitInfo unitInfo)
		#region GetVehicleRoutes()
		public struct RouteExtNumber
		{
			public int RouteID;
			public string ExtNumber;

			public RouteExtNumber(int routeID, string extNumber)
			{
				RouteID = routeID;
				ExtNumber = extNumber;
			}
		}
		public Hashtable GetVehicleRoutes()
		{
			Hashtable hash = new Hashtable();

			try
			{
				using (var sp = Database.Procedure("dbo.GetVehicleRoutes"))
				{
					using (new ConnectionOpener(sp.Command.Connection))
					{
						IDataReader dr = sp.ExecuteReader();

						try
						{
							while (dr.Read())
							{
								if (dr["VEHICLE_ID"] == DBNull.Value || dr["ROUTE_ID"] == DBNull.Value)
								{
									Trace.WriteLine(" GetVehicleRoutes: can't found route for vehicle. exec proc on server!");
								}
								else
								{
									int vehicleID = (int)dr["VEHICLE_ID"];
									int routeID = (int)dr["ROUTE_ID"];
									string extNumber = (string)dr["EXT_NUMBER"];
									hash.Add(vehicleID, new RouteExtNumber(routeID, extNumber));
								}
							}
						}
						finally
						{
							dr.Close();
						}
					}
				}

			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}

			return hash;
		}
		#endregion GetVehicleRoutes()
		/// <summary> Создание/редактирование норматива межзонового пробега </summary>
		/// <param name="zone_id_from">id 1-й геозоны</param>
		/// <param name="zone_id_to">id 2-й геозоны</param>
		/// <param name="distance">норматив (в метрах)</param>
		/// <param name="operatorId">ID оператора</param>
		/// <param name="sessionId">ID сессии пользователя</param>
		public void UpdateZoneDistanceStandart(int zone_id_from, int zone_id_to, int distance, int operatorId, int sessionId)
		{
			using (var sp = Database.Procedure("dbo.UpdateZoneDistanceStandart"))
			{
				sp["@zone_from"].Value  = zone_id_from;
				sp["@zone_to"].Value    = zone_id_to;
				sp["@distance"].Value   = distance;
				sp["@operatorID"].Value = operatorId;
				sp["@sessionID"].Value  = sessionId;
				//using (new ConnectionOpener(sp.Command.Connection))
				//{
				sp.ExecuteNonQuery();
				//}
			}

		}
		/// <summary> Получение списка всех нормативов межзонового пробега, доступных данному оператору </summary>
		/// <param name="operatorID">ID оператора</param>
		public DataSet GetAllZoneDistanceStandarts(int operatorID)
		{
			using (var sp = Database.Procedure("dbo.GetZoneDistanceStandarts"))
			{
				sp["@operatorID"].Value = operatorID;
				DataSet ds = sp.ExecuteDataSet();
				return ds;
			}
		}
		#region GetLog(int from, int to, int id)
		public SortedList GetLogNew(int from, int to, int id, int interval, int count, out int iLogLength, out string garageNumber)
		{
			SortedList sl = new SortedList();
			garageNumber = String.Empty;
			iLogLength = 0;
			IMobilUnit lastMU = null;

			// топливо
			bool fuelDirectSensor = false;
			float fuelFactor = 1.0f;
			int fuelMinSensorValue = 0;
			int fuelMaxSensorValue = Int32.MaxValue;

			try
			{
				using (var sp = Database.Procedure("dbo.GetLogNew"))
				{
					sp["@count"].Value = count;
					sp["@interval"].Value = interval;
					sp["@from"].Value = from;
					sp["@to"].Value = to;
					sp["@vehicle_id"].Value = id;
					using (new ConnectionOpener(sp.Command.Connection))
					{
						IDataReader dr = sp.ExecuteReader();
						try
						{
							dr.Read();
							garageNumber = (string)dr["GARAGE_NUMBER"];
							// ci.FUEL_LVAL, ci.FUEL_UVAL, ci.FUEL_FACTOR, ci.FUEL_SENSOR_DIRECTION
							fuelFactor = dr["FUEL_FACTOR"] != DBNull.Value ? (float)dr["FUEL_FACTOR"] : fuelFactor;
							fuelDirectSensor = dr["FUEL_SENSOR_DIRECTION"] != DBNull.Value ? (bool)dr["FUEL_SENSOR_DIRECTION"] : fuelDirectSensor;
							fuelMinSensorValue = dr["FUEL_LVAL"] != DBNull.Value ? (int)dr["FUEL_LVAL"] : 0;
							fuelMaxSensorValue = dr["FUEL_UVAL"] != DBNull.Value ? (int)dr["FUEL_UVAL"] : Int32.MaxValue;
							iLogLength = (int)dr["RecsCountInDB"];
							dr.NextResult();

							var radiusIndex = dr.GetOrdinal("Radius");
							var isMLPIndex = dr.GetOrdinal("IsMLP");
							for (int i = 0; dr.Read(); i++)
							{
								var mu = new FORIS.TSS.MobilUnit.Unit.MobilUnit();

								if (!dr.IsDBNull(radiusIndex))
									mu.Radius = dr.GetInt32(radiusIndex);

								var isMlp = dr.IsDBNull(isMLPIndex) || !dr.GetBoolean(isMLPIndex);
								if (isMlp)
									mu.Type = PositionType.LBS;

								mu.Unique = ((int)dr["MONITOREE_ID"]);
								mu.Time = ((int)dr["LOG_TIME"]);
								mu.Speed = ((byte)dr["SPEED"]);
								mu.Longitude = Convert.ToDouble(dr["X"]);
								mu.Latitude = Convert.ToDouble(dr["Y"]);
								mu.VoltageAN1 = (dr["T"] != DBNull.Value
													 ? (int)dr["T"]
													 : Int32.MaxValue);
								// температура
								// расход топлива
								int fuelVolume = dr["F"] != DBNull.Value ? (int)dr["F"] : 0;
								if (fuelDirectSensor)
									fuelVolume = (fuelVolume < fuelMinSensorValue || fuelVolume > fuelMaxSensorValue) ? 0 :
										(int)((fuelVolume - fuelMinSensorValue) * fuelFactor);
								else
								{
									if (fuelMaxSensorValue < Int32.MaxValue)
										fuelVolume = (fuelVolume < fuelMinSensorValue || fuelVolume > fuelMaxSensorValue) ? 0 :
											(int)((fuelMaxSensorValue - fuelVolume) * fuelFactor);
								}
								mu.VoltageAN4 = fuelVolume;

								if (!(mu.Latitude < 90f && mu.Longitude < 180f))
									mu.CorrectGPS = false;
								else
									mu.CorrectGPS = true;


								// считаем курс
								if (lastMU != null && mu.ValidPosition)
								{
									if (mu.Speed > 0 &&
										lastMU.Longitude == mu.Longitude &&
										lastMU.Latitude == mu.Latitude &&
										lastMU.Speed == mu.Speed)
										mu.CorrectGPS = false;
									else
										mu.CalcCourse(lastMU);

								}
								lastMU = mu;

								sl.Add(mu.Time, mu);
							}
						}
						catch (Exception ex)
						{
							default(string)
								.WithException(ex, true)
								.CallTraceError();
						}
						finally
						{
							dr.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return sl;
		}
		#endregion GetLog(int from, int to, int id)
		#region SetTerminals()
		/// <summary> Установка свойств терминалов сервера </summary>
		/// <returns> хэш с настройками терминалов сервера, в данной версии реализована поддержка только одного устройства (TC35i) </returns>
		public Hashtable SetTerminals()
		{
			using (IDataReader dr = Database.ExecuteReader(SqlMediaList, System.Data.CommandType.Text))
			{
				Hashtable terminals = new Hashtable();
				while (dr.Read())
				{
					terminals.Add(((string)dr["NAME"]).Trim(), ((string)dr["INITIALIZATION"]).Trim());
				}
				return terminals;
			}
		}
		#endregion SetTerminals()
		#region GetVehiclesTimeInForAllRoutes(DateTime time)
		public DataSet GetVehiclesTimeInForAllRoutes(DateTime time)
		{
			using (var sp = Database.Procedure("dbo.GetVehiclesTimeInForAllRoutes"))
			{
				sp["@local_time"].Value = time;
				DataSet dataSet = sp.ExecuteDataSet(Tables.GetVehiclesTimeInForAllRoutes);
				return dataSet;
			}
		}
		#endregion GetVehiclesTimeInForAllRoutes(DateTime time)
		#region Проверка телефона владельца при запросе адреса
		public int GetOwnerRight(int contNumber, string phone)
		{
			try
			{
				using (var sp = Database.Procedure("dbo.GetOwnerRight"))
				{
					using (new ConnectionOpener(sp.Command.Connection))
					{
						sp["@contNumber"].Value = contNumber;
						sp["@phone"].Value = phone;
						int num = (int)sp.ExecuteScalar();
						return num;
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				throw;
			}
		}
		public DataSet GetOwnersForVehicle(int contNumber)
		{
			try
			{
				using (var sp = Database.Procedure("dbo.GetOwnersForVehicle"))
				{
					using (new ConnectionOpener(sp.Command.Connection))
					{
						sp["@contNumber"].Value = contNumber;
						DataSet dataSet = sp.ExecuteDataSet();
						return dataSet;
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				throw;
			}
		}
		#endregion Проверка телефона владельца при запросе адреса

		#region GetVehicles()
		public DataSet GetVehicles()
		{
			using (var sp = Database.Procedure("dbo.GetVehicles"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetVehicle);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}
		#endregion GetVehicles()
		#region GetOperatorGroupsAndOperators()
		public DataSet GetOperatorGroupsAndOperators()
		{
			using (var sp = Database.Procedure("dbo.GetOperatorGroupsAndOperators"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOperatorGroupsAndOperators);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}
		public DataSet GetControllers()
		{
			using (var sp = Database.Procedure("dbo.GetDataForControllers"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForControllers);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}
		#endregion GetOperatorGroupsAndOperators()

		#region Messages
		public DataSet GetMessages(int iOperatorId, int iStatusId, DateTime dtTime1, DateTime dtTime2, int iSourceId)
		{
			using (var sp = Database.Procedure("dbo.GetMessages"))
			{
				sp["@op_id"].Value = iOperatorId;

				if (iStatusId > -1)
					sp["@status_id"].Value = iStatusId;
				else
					sp["@status_id"].Value = DBNull.Value;

				if (dtTime1 != DateTime.MinValue)
					sp["@time1"].Value = dtTime1;
				else
					sp["@time1"].Value = DBNull.Value;

				if (dtTime2 != DateTime.MinValue)
					sp["@time2"].Value = dtTime2;
				else
					sp["@time2"].Value = DBNull.Value;

				if (iSourceId > 0)
					sp["@source_id"].Value = iSourceId;
				else
					sp["@source_id"].Value = DBNull.Value;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMessages);
				return dataSet;
			}
		}
		public List<int> SetMessageProcessingResult(string appId, MessageProcessingResult value, params int[] messageIds)
		{
			using (var sp = Database.Procedure("dbo.UpdateMessagesResult"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@ids", GetIdTable(messageIds));
				sp["@result"].Value = (int)value;
				sp["@appId"].Value = appId;

				var list = new List<int>(messageIds.Length);
				using (var reader = sp.ExecuteReader())
				{
					while (reader.Read())
						list.Add(reader.GetInt32(0));
				}
				return list;
			}
		}
		#endregion Messages
		public ReportMenuVehiclesCapabilities GetReportMenuCapabilities(int operatorId, int? departmentId)
		{
			using (var sp = Database.Procedure("dbo.GetReportMenuVehiclesCapabilities"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@OperatorId"].Value   = operatorId;
				sp["@DepartmentId"].Value = departmentId;
				using (var reader = sp.ExecuteReader())
				{
					reader.Read();
					return new ReportMenuVehiclesCapabilities
					{
						HasDigitalSensors              = Convert.ToBoolean(reader["HasDigitalSensors"]),
						HasSpeedMonitoring             = Convert.ToBoolean(reader["HasSpeedMonitoring"]),
						IsAskPositionAvailable         = Convert.ToBoolean(reader["IsAskPositionAvailable"]),
						IsIginitionMonitoringAvailable = Convert.ToBoolean(reader["IsIginitionMonitoringAvailable"]),
						IsSalesAvailable               = Convert.ToBoolean(reader["IsSalesAvailable"]),
						IsWhoAskedMeAvailable          = Convert.ToBoolean(reader["IsWhoAskedMeAvailable"]),
					};
				}
			}
		}
		/// <summary> Добавляет новую запись в таблицу SMS_LOG </summary>
		/// <param name="sms_type_id">тип смс</param>
		/// <param name="owner_id">идентификатор владельца</param>
		/// <param name="controller_number">номер контроллера</param>
		public void InsertSmsLog(int sms_type_id, int owner_id, int controller_number)
		{
			try
			{
				using (var sp = Database.Procedure("dbo.InsertSmsLog"))
				{
					sp["@sms_type_id"].Value = sms_type_id;
					sp["@owner_id"].Value = owner_id;
					sp["@controller_number"].Value = controller_number;
					sp["@date_time"].Value = DateTime.UtcNow;
					sp.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}
		public IEnumerable<VehicleLogTimeParam> GetLastLogTimes(IEnumerable<int> vehicleIds, DateTime time)
		{
			using (var sp = Database.Procedure("dbo.GetLastLogTimes"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_ids"].Value = vehicleIds.Join(";");
				using (var reader = sp.ExecuteReader())
				{
					var vidOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");

					while (reader.Read())
					{
						yield return new VehicleLogTimeParam(
							reader.GetInt32(vidOrdinal),
							reader.GetInt32(logTimeOrdinal));
					}
				}
			}
		}
		public List<GeoZoneForPosition> GetGeoZonesForPositions(
			IEnumerable<VehicleZoneParam> vehicleZoneParams,
			IEnumerable<VehiclePositionParam> vehiclePositionParams)
		{
			using (var sp = Database.Procedure("dbo.GetGeoZonesForPositions"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				var vehicleZoneTable = new DataTable("VehicleZoneParam");
				vehicleZoneTable.Columns.Add("Vehicle_ID", typeof(int));
				vehicleZoneTable.Columns.Add("Zone_ID", typeof(int));
				foreach (var vzp in vehicleZoneParams.Distinct())
					vehicleZoneTable.Rows.Add(vzp.VehicleId, vzp.ZoneId);
				sp.SetTableValuedParameter("@vehicle_zone", vehicleZoneTable);
				sp.SetTableValuedParameter("@vehicle_position", GetVehiclePositionTable(vehiclePositionParams));


				using (new Stopwatcher("GetGeoZonesForPositions", delegate { return ToString(vehicleZoneParams, vehiclePositionParams);}))
					using (var reader = sp.ExecuteReader())
					{
						return ReadZonePositions(reader);
					}
			}
		}
		private static string ToString(
			IEnumerable<VehicleZoneParam> vehicleZoneParams,
			IEnumerable<VehiclePositionParam> vehiclePositionParams)
		{
			return vehicleZoneParams.Select(x => x.VehicleId + "x" + x.ZoneId).Join(", ") + "|" +
				   vehiclePositionParams.Select(x => x.VehicleId + "x" + x.LogTime + "x" + x.Lat.ToString(NumberHelper.FormatInfo) + "x" + x.Lng.ToString(NumberHelper.FormatInfo)).Join(", ");
		}
		public List<GeoZoneForPosition> GetGeoZonesForVehicleLogTime(
			IEnumerable<VehicleZoneParam> vehicleZoneParams,
			IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams)
		{
			using (var sp = Database.Procedure("dbo.GetGeoZonesForVehicleLogTime"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				var vehicleZoneTable = new DataTable("VehicleZoneParam");
				vehicleZoneTable.Columns.Add("Vehicle_ID", typeof(int));
				vehicleZoneTable.Columns.Add("Zone_ID", typeof(int));
				foreach (var vzp in vehicleZoneParams)
					vehicleZoneTable.Rows.Add(vzp.VehicleId, vzp.ZoneId);
				sp.SetTableValuedParameter("@vehicle_zone", vehicleZoneTable);
				sp.SetTableValuedParameter("@vehicle_log_time", GetVehicleLogTimeTable(vehicleLogTimeParams));

				using (var reader = sp.ExecuteReader())
				{
					return ReadZonePositions(reader);
				}
			}
		}
		private static List<GeoZoneForPosition> ReadZonePositions(IDataReader reader)
		{
			var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
			var logTimeOrdinal   = reader.GetOrdinal("Log_Time");
			var geoZoneIDOrdnial = reader.GetOrdinal("Zone_ID");

			var result = new List<GeoZoneForPosition>();

			while (reader.Read())
			{
				result.Add(new GeoZoneForPosition(
					reader.GetInt32(vehicleIDOrdinal),
					reader.GetInt32(logTimeOrdinal),
					reader.GetInt32(geoZoneIDOrdnial)
				));
			}
			return result;
		}
		public struct Position
		{
			public readonly int     VehicleId;
			public readonly int     LogTime;
			public readonly decimal Lat;
			public readonly decimal Lng;

			public Position(int vehicleId, int logTime, decimal lat, decimal lng)
			{
				VehicleId = vehicleId;
				LogTime   = logTime;
				Lat       = lat;
				Lng       = lng;
			}
		}
		public List<Position> GetPositions(IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams, bool includeNeighbour, CancellationToken? cancellationToken = null)
		{
			using (var sp = Database.Procedure("dbo.GetPositions"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@vehicle_log_time", GetVehicleLogTimeTable(vehicleLogTimeParams));
				sp["@include_neighbour"].Value = includeNeighbour;
				using (var reader = cancellationToken != null ? sp.ExecuteReader(cancellationToken.Value) : sp.ExecuteReader())
				{
					var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");
					var latOrdnial = reader.GetOrdinal("Lat");
					var lngOrdinal = reader.GetOrdinal("Lng");

					var result = new List<Position>();

					while (reader.Read())
					{
						result.Add(new Position
						(
							reader.GetInt32(vehicleIDOrdinal),
							reader.GetInt32(logTimeOrdinal),
							reader.GetDecimal(latOrdnial),
							reader.GetDecimal(lngOrdinal)
						));
					}
					return result;
				}
			}
		}
		public struct SpeedForPosition
		{
			public readonly int VehicleId;
			public readonly int LogTime;
			public readonly int Speed;

			public SpeedForPosition(int vehicleId, int logTime, int speed)
			{
				VehicleId = vehicleId;
				LogTime   = logTime;
				Speed     = speed;
			}
		}
		public List<SpeedForPosition> GetSpeedForPositions(
			IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams,
			bool includeNeighbour)
		{
			using (var sp = Database.Procedure("dbo.GetSpeeds"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@vehicle_log_time", GetVehicleLogTimeTable(vehicleLogTimeParams));
				sp["@include_neighbour"].Value = includeNeighbour;
				using (var reader = sp.ExecuteReader())
				{
					var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");
					var speedOrdnial = reader.GetOrdinal("Speed");

					var result = new List<SpeedForPosition>();

					while (reader.Read())
					{
						result.Add(new SpeedForPosition(
									   reader.GetInt32(vehicleIDOrdinal),
									   reader.GetInt32(logTimeOrdinal),
									   reader.GetByte(speedOrdnial)
									   ));
					}
					return result;
				}
			}
		}
		/// <summary> Возвращает только те точки из vehicleLogTimeParams, которые являются остановками более duration секунд </summary>
		/// <param name="vehicleLogTimeParams">Точки, среди которых нужно найти остановки</param>
		/// <param name="duration">Продолжительность остановки</param>
		public List<VehicleLogTimeParam> GetStopsForPosition(
			IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams,
			int duration)
		{
			using (var sp = Database.Procedure("dbo.GetStopsForPositions"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@vehicle_log_time", GetVehicleLogTimeTable(vehicleLogTimeParams));
				sp["@duration"].Value = duration;
				using (var reader = sp.ExecuteReader())
				{
					var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");

					var result = new List<VehicleLogTimeParam>();

					while (reader.Read())
					{
						result.Add(new VehicleLogTimeParam(
									   reader.GetInt32(vehicleIDOrdinal),
									   reader.GetInt32(logTimeOrdinal)
									   ));
					}
					return result;
				}
			}
		}
		private static DataTable GetVehiclePositionTable(IEnumerable<VehiclePositionParam> vehicleLogTimeParams)
		{
			var vehicleLogTimeTable = new DataTable("VehicleLogTimeParam");
			vehicleLogTimeTable.Columns.Add("Vehicle_ID", typeof(int));
			vehicleLogTimeTable.Columns.Add("Log_Time", typeof(int));
			vehicleLogTimeTable.Columns.Add("Lat", typeof(decimal));
			vehicleLogTimeTable.Columns.Add("Lng", typeof(decimal));
			foreach (var vzp in vehicleLogTimeParams.ToLookup(x => x.Id).Select(g => g.First()))
				vehicleLogTimeTable.Rows.Add(vzp.VehicleId, vzp.LogTime, vzp.Lat, vzp.Lng);
			return vehicleLogTimeTable;
		}
		private static DataTable GetVehicleLogTimeTable(IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams)
		{
			var vehicleLogTimeTable = new DataTable("VehicleLogTimeParam");
			vehicleLogTimeTable.Columns.Add("Vehicle_ID", typeof(int));
			vehicleLogTimeTable.Columns.Add("Log_Time", typeof(int));
			foreach (var vzp in vehicleLogTimeParams)
				vehicleLogTimeTable.Rows.Add(vzp.VehicleId, vzp.LogTime);
			return vehicleLogTimeTable;
		}
		public class LogTimeInfo
		{
			public readonly int       VehicleId;
			public readonly List<int> NewLogTimes;
			public LogTimeInfo(int vehicleId)
			{
				VehicleId   = vehicleId;
				NewLogTimes = new List<int>();
			}
		}
		public List<LogTimeInfo> GetNewLogTimes(int[] vehicleIds, DateTime dateTime)
		{
			using (var sp = Database.Procedure("dbo.GetNewLogTimes"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_ids"].Value = vehicleIds.Join(";");
				sp["@max_datetime"].Value = dateTime;
				using (var reader = sp.ExecuteReader())
				{
					var vehicleIdOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");
					var result = new Dictionary<int, LogTimeInfo>();
					while (reader.Read())
					{
						var vehicleId = reader.GetInt32(vehicleIdOrdinal);
						var logTime = reader.GetInt32(logTimeOrdinal);
						LogTimeInfo logTimes;
						if (!result.TryGetValue(vehicleId, out logTimes))
							result.Add(vehicleId, logTimes = new LogTimeInfo(vehicleId));
						logTimes.NewLogTimes.Add(logTime);
					}

					return result.Values.ToList();
				}
			}
		}
		public FullLog GetFullLog(int vehicleId, int logTimeFrom, int logTimeTo, FullLogOptions options)
		{
			if (options == null)
				throw new ArgumentNullException(nameof(options));

			using (var sp = Database.Procedure("dbo.GetFullLog"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_id"].Value          = vehicleId;
				sp["@from"].Value                = logTimeFrom;
				sp["@to"].Value                  = logTimeTo;
				sp["@noDataPeriod"].Value        = (int)options.NoDataPeriod.TotalSeconds;
				sp["@maxTotalPointsCount"].Value = options.MaxTotalPointCount;
				using (var reader = sp.ExecuteReader())
				{
					try
					{
						if(!options.StopPeriod.HasValue)
							options.StopPeriod = FullLogOptions.DefaultStopPeriod;
						var fullLog = new FullLog(logTimeFrom, logTimeTo, options);
						fullLog.Read(reader);
						return fullLog;
					}
					catch (Exception ex)
					{
						$"{sp.Command.CommandText}(@vehicle_id={vehicleId},@from={logTimeFrom},@to={logTimeTo})"
							.WithException(ex)
							.CallTraceError();
						throw;
					}
				}
			}
		}
		public struct InputValueForPosition
		{
			public readonly int            VehicleId;
			public readonly InputLogRecord Input;

			public InputValueForPosition(int vehicleId, InputLogRecord input)
			{
				VehicleId = vehicleId;
				Input     = input;
			}
		}
		public struct VehicleSensorParam
		{
			public int VehicleId;
			public int SensorNumber;

			public VehicleSensorParam(int vehicleId, int sensorNumber)
			{
				VehicleId    = vehicleId;
				SensorNumber = sensorNumber;
			}
		}
		public List<InputValueForPosition> GetInputValuesForPositions(
			IEnumerable<VehicleSensorParam> vehicleSensorParams,
			IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams,
			bool includeNeighbour)
		{
			using (var sp = Database.Procedure("dbo.GetSensorsForPositions"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				var vehicleSensorParamTable = new DataTable("VehicleSensor");
				vehicleSensorParamTable.Columns.Add("Vehicle_ID", typeof(int));
				vehicleSensorParamTable.Columns.Add("Sensor_Number", typeof(int));
				foreach (var vsp in vehicleSensorParams)
					vehicleSensorParamTable.Rows.Add(vsp.VehicleId, vsp.SensorNumber);
				sp.SetTableValuedParameter("@vehicle_sensor", vehicleSensorParamTable);
				sp.SetTableValuedParameter("@vehicle_log_time", GetVehicleLogTimeTable(vehicleLogTimeParams));
				sp["@include_neighbour"].Value = includeNeighbour;

				using (var reader = sp.ExecuteReader())
				{
					var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");
					var sensorNumberOrdinal = reader.GetOrdinal("Number");
					var valueOrdinal = reader.GetOrdinal("Value");

					var result = new List<InputValueForPosition>();
					while (reader.Read())
					{
						result.Add(
							new InputValueForPosition(
								reader.GetInt32(vehicleIDOrdinal),
								new InputLogRecord(
									reader.GetInt32(logTimeOrdinal),
									reader.GetInt32(sensorNumberOrdinal),
									reader.GetInt64(valueOrdinal))));
					}
					return result;
				}
			}
		}
		public List<InputLogRecord> GetRawSensorValues(int vehicleId)
		{
			using (var sp = Database.Procedure("dbo.GetRawSensorValues"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_id"].Value = vehicleId;
				using (var reader = sp.Command.ExecuteReader())
				{
					var numberOrdinal   = reader.GetOrdinal("Number");
					var logTimeOrdingal = reader.GetOrdinal("Log_Time");
					var valueOrdinal    = reader.GetOrdinal("Value");
					var result = new List<InputLogRecord>();
					while (reader.Read())
					{
						result.Add(new InputLogRecord(
							reader.GetInt32(logTimeOrdingal),
							reader.GetInt32(numberOrdinal),
							reader.GetInt64(valueOrdinal)));
					}
					return result;
				}
			}
		}
		/// <summary> Получает из БД изображение </summary>
		/// <param name="vehicleID">Идентификатор объекта наблюдения, с которым ассоциировано изображение</param>
		/// <param name="logTime">Время изображения</param>
		public Picture GetPicture(int vehicleID, int logTime)
		{
			using (var sp = Database.Procedure("dbo.GetPicture"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_id"].Value = vehicleID;
				sp["@log_time"].Value = logTime;
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;

					var cameraNumberOrdinal = reader.GetOrdinal("Camera_Number");
					var photoNumberOrdinal = reader.GetOrdinal("Photo_Number");
					var urlOrdinal = reader.GetOrdinal("Url");
					var bytesOrdinal = reader.GetOrdinal("Bytes");

					return new Picture
						{
							Date = TimeHelper.GetDateTimeUTC(logTime),
							CameraNumber = reader.GetInt32(cameraNumberOrdinal),
							PhotoNumber = reader.GetInt32(photoNumberOrdinal),
							Url = reader.IsDBNull(urlOrdinal) ? null : reader.GetString(urlOrdinal),
							Bytes = reader.IsDBNull(bytesOrdinal) ? null : reader.GetValue(bytesOrdinal) as byte[]
						};
				}
			}
		}
		public List<int> GetNearestPictures(int vehicleID, int logTime)
		{
			var result = new List<int>();
			using (var sp = Database.Procedure("dbo.GetNearestPictures"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@log_time"].Value = logTime;
				sp["@vehicle_id"].Value = vehicleID;
				using (var reader = sp.ExecuteReader())
				{
					while (reader.Read())
					{
						result.Add((int)reader[0]);
					}
					reader.Close();
				}
			}
			return result;
		}
		/// <summary> Возвращает текущую дату сервера БД (UTC/GMT) </summary>
		public DateTime GetUtcDate()
		{
			using (var sp = Database.Procedure("dbo.GetUtcDateTime"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						throw new ApplicationException("Could not read");
					return reader.GetDateTime(0);
				}
			}
		}
		/// <summary> Возвращает время наиболее старой проверки среди всех объектов наблюдения </summary>
		public DateTime? GetLastRuleCheckDate()
		{
			using (var sp = Database.Procedure("dbo.GetLastRuleCheckDate"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;
					return reader.IsDBNull(0) ? (DateTime?) null : reader.GetDateTime(0);
				}
			}
		}
		public List<RunLogRecord> GetRunLogForGroup(int vehicleGroupId, int logTimeFrom, int logTimeTo)
		{
			var result = new List<RunLogRecord>();
			using (var sp = Database.Procedure("dbo.GetRunLogForVehicleGroup"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehiclegroup_id"].Value = vehicleGroupId;
				sp["@from"].Value = logTimeFrom;
				sp["@to"].Value = logTimeTo;

				using (var reader = sp.ExecuteReader())
				{
					while (reader.Read())
					{
						result.Add(new RunLogRecord
						{
							VehicleId = (int)reader["vehicle_id"],
							LogTime = (int)reader["log_time"],
							Odometer = (long)reader["odometer"]
						});
					}
					reader.Close();
				}
			}
			return result;
		}
		public List<RunLogRecord> GetRunLogForVehicle(int vehicleId, int logTimeFrom, int logTimeTo)
		{
			var result = new List<RunLogRecord>();
			using (var sp = Database.Procedure("dbo.GetRunLogForVehicle"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_id"].Value = vehicleId;
				sp["@from"].Value = logTimeFrom;
				sp["@to"].Value = logTimeTo;

				using (var reader = sp.ExecuteReader())
				{
					var logTimeOrdinal = reader.GetOrdinal("log_time");
					var odometerOrdinal = reader.GetOrdinal("odometer");

					while (reader.Read())
					{
						result.Add(new RunLogRecord
						{
							VehicleId = vehicleId,
							LogTime = reader.GetInt32(logTimeOrdinal),
							Odometer = reader.GetInt64(odometerOrdinal)
						});
					}
					reader.Close();
				}
			}
			return result;
		}
		private static DataTable GetLogTimeSpanTable(IEnumerable<LogTimeSpanParam> logTimeSpanParams)
		{
			var vehicleLogTimeTable = new DataTable("VehicleLogTimeParam");
			vehicleLogTimeTable.Columns.Add("Log_Time_From", typeof(int));
			vehicleLogTimeTable.Columns.Add("Log_Time_To", typeof(int));
			foreach (var vzp in logTimeSpanParams)
				vehicleLogTimeTable.Rows.Add(vzp.LogTimeFrom, vzp.LogTimeTo);
			return vehicleLogTimeTable;
		}
		public DataSet GetMoveDetailHistory(int count, int interval, DateTime time_from, DateTime time_to, int vehicleID, IEnumerable<LogTimeSpanParam> workingIntervals)
		{
			using (var sp = Database.Procedure("dbo.GetMoveDetailHistory"))
			{
				sp["@count"].Value = count;
				sp["@interval"].Value = interval;
				sp["@time_from"].Value = time_from;
				sp["@time_to"].Value = time_to;
				sp["@vehicle_id"].Value = vehicleID;
				sp.SetTableValuedParameter("@workingIntervals", GetLogTimeSpanTable(workingIntervals));
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMoveDetailHistory);
				return dataSet;
			}
		}
		public DataSet GetMoveGroupDay(int vehicleID, IEnumerable<LogTimeSpanParam> workingIntervals)
		{
			using (var sp = Database.Procedure("dbo.GetMoveGroupDay"))
			{
				sp["@vehicle"].Value = vehicleID;
				sp.SetTableValuedParameter("@workingIntervals", GetLogTimeSpanTable(workingIntervals));
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMoveGroupDay);
				return dataSet;
			}
		}
		public List<RunLogRecord> GetRunLog(IEnumerable<int> vehicleIds, IEnumerable<LogTimeSpanParam> logTimeSpanParams, bool includeAdjacent)
		{
			var result = new List<RunLogRecord>();
			using (var sp = Database.Procedure("dbo.GetRunLog"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@vehicles", GetIdTable(vehicleIds));
				sp.SetTableValuedParameter("@intervals", GetLogTimeSpanTable(logTimeSpanParams));
				sp["@include_adjacent"].Value = includeAdjacent;

				using (var reader = sp.ExecuteReader())
				{
					do
					{
						while (reader.Read())
						{
							result.Add(new RunLogRecord
										   {
											   VehicleId = (int) reader["vehicle_id"],
											   LogTime = (int) reader["log_time"],
											   Odometer = (long) reader["odometer"]
										   });
						}
					} while (reader.NextResult());

					reader.Close();
				}
			}
			return result;
		}
		public struct CompoundRuleLogEntry
		{
			public readonly int VehicleID;
			public readonly int LogTime;
			public readonly int CompoundRuleID;

			public CompoundRuleLogEntry(int vehicleID, int logTime, int compoundRuleID)
			{
				VehicleID = vehicleID;
				LogTime = logTime;
				CompoundRuleID = compoundRuleID;
			}

			public override bool Equals(object obj)
			{
				if (obj == null)
					return false;
				if (obj.GetType() != GetType())
					return false;
				var other = (CompoundRuleLogEntry) obj;
				return VehicleID == other.VehicleID &&
					   LogTime == other.LogTime &&
					   CompoundRuleID == other.CompoundRuleID;
			}

			public override int GetHashCode()
			{
				return VehicleID.GetHashCode() ^ LogTime.GetHashCode() ^ CompoundRuleID.GetHashCode();
			}
		}
		public void RegisterCompoundRuleEvents(IEnumerable<CompoundRuleLogEntry> entries)
		{
			using (var sp = Database.Procedure("dbo.RegisterCompoundRuleEvents"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@compoundRuleLog", GetDataTable(entries));
				sp.ExecuteNonQuery();
			}
		}
		public List<CompoundRuleLogEntry> GetUnregisteredCompoundRuleEvents(
			IEnumerable<CompoundRuleLogEntry> entries, int delta)
		{
			using (var sp = Database.Procedure("dbo.GetUnregisteredCompoundRuleEvents"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@compoundRuleLog", GetDataTable(entries));
				sp["@delta"].Value = delta;
				using (var reader = sp.ExecuteReader())
				{
					var vehicleIDOrdinal = reader.GetOrdinal("Vehicle_ID");
					var logTimeOrdinal = reader.GetOrdinal("Log_Time");
					var compoundRuleIDOrdinal = reader.GetOrdinal("CompoundRule_ID");

					var result = new List<CompoundRuleLogEntry>();

					while (reader.Read())
					{
						result.Add(new CompoundRuleLogEntry(
									   reader.GetInt32(vehicleIDOrdinal),
									   reader.GetInt32(logTimeOrdinal),
									   reader.GetInt32(compoundRuleIDOrdinal)));
					}

					return result;
				}
			}
		}
		private DataTable GetDataTable(IEnumerable<CompoundRuleLogEntry> entries)
		{
			var result = new DataTable("CompoundRuleEvents");
			result.Columns.Add("Vehicle_ID", typeof(int));
			result.Columns.Add("Log_Time", typeof(int));
			result.Columns.Add("CompoundRule_ID", typeof(int));
			foreach (var entry in entries)
				result.Rows.Add(entry.VehicleID, entry.LogTime, entry.CompoundRuleID);
			return result;
		}
		public DataSet GetDataForWeb(int operatorID, int? departmentId, int[] vehicleIds)
		{
			using (var sp = Database.Procedure("dbo.GetDataForWeb"))
			{
				sp["@operatorID"].Value = operatorID;
				sp["@departmentID"].Value = departmentId;
				sp.SetTableValuedParameter("@vehicleIds", GetIdTable(vehicleIds));

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForWeb);
				return dataSet;
			}
		}
		public int[] GetOperatorsByPhoneBookContact(string msisdn)
		{
			using (var sp = Database.Procedure("dbo.GetOperatorsByPhoneBookContact"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				var result = new List<int>();
				sp["@msisdn"].Value = msisdn;
				using (var reader = sp.ExecuteReader())
				{
					var operatorIdOrdinal = reader.GetOrdinal("OperatorId");
					while (reader.Read())
						result.Add(reader.GetInt32(operatorIdOrdinal));
				}
				return result.ToArray();
			}
		}
		public List<PhoneBookContact> SearchPhoneBookContacts(int operatorId, string[] msisdns)
		{
			using (var sp = Database.Procedure("dbo.SearchPhoneBookContacts"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				var result = new List<PhoneBookContact>();

				sp["@operatorId"].Value = operatorId;
				sp.SetTableValuedParameter("@msisdns", GetMsisdnTable(msisdns));
				using (var reader = sp.ExecuteReader())
				{
					var nameOrdinal = reader.GetOrdinal("Name");
					var valueOrdinal = reader.GetOrdinal("Value");
					while (reader.Read())
					{
						result.Add(new PhoneBookContact
						{
							Name = reader.GetString(nameOrdinal),
							Value = reader.GetString(valueOrdinal),
							Type = ContactType.Phone
						});
					}
				}

				return result;
			}
		}
		public List<BillingService> GetOperatorServices(int operatorId, int? departmentId)
		{
			using (var sp = Database.Procedure("dbo.GetOperatorServices"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@operator_id"].Value = operatorId;
				sp["@department_id"].Value = departmentId;
				using (var reader = sp.ExecuteReader())
				{
					return ReadBillingServices(reader);
				}
			}
		}
		public List<BillingService> GetVehicleServices(params int[] vehicleIds)
		{
			using (var sp = Database.Procedure("dbo.GetVehicleServices"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@vid", GetIdTable(vehicleIds));
				using (var reader = sp.ExecuteReader())
				{
					return ReadBillingServices(reader);
				}
			}
		}
		public class OperatorAsid
		{
			public int? Id;
			public bool IsBlocked;
			public readonly List<BillingService> Services = new List<BillingService>();
		}
		public OperatorAsid GetOperatorAsid(int operatorId, int? departmentId)
		{
			var result = new OperatorAsid();
			using (var sp = Database.Procedure("dbo.GetOperatorAsid"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@operator_id"].Value = operatorId;
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return result;

					result.Id = reader.GetInt32(0);
					var blockedDate = reader.IsDBNull(1) ? (DateTime?) null : reader.GetDateTime(1);
					result.IsBlocked = blockedDate <= DateTime.UtcNow;
				}
			}
			result.Services.AddRange(GetOperatorServices(operatorId, departmentId).Where(bs => !(bs.IsBlocked ?? false)));
			return result;
		}
		public List<MessageWeb> GetUpdatedMessages(DateTime from, DateTime to)
		{
			using (var sp = Database.Procedure("dbo.GetUpdatedMessages"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				using (var reader = sp.ExecuteReader())
				{
					var messageIDOrdinal = reader.GetOrdinal("Message_ID");
					var createdOrdinal = reader.GetOrdinal("Created");
					var timeOrdinal = reader.GetOrdinal("Time");
					var toIDOrdinal = reader.GetOrdinal("ToID");
					var toTypeOrdinal = reader.GetOrdinal("ToType");
					var toValueOrdinal = reader.GetOrdinal("ToValue");
					var fromIDOrdinal = reader.GetOrdinal("FromID");
					var fromTypeOrdinal = reader.GetOrdinal("FromType");
					var fromValueOrdinal = reader.GetOrdinal("FromValue");
					var subjectOrdinal = reader.GetOrdinal("SUBJECT");
					var bodyOrdinal = reader.GetOrdinal("BODY");
					var statusOrdinal = reader.GetOrdinal("STATUS");
					var processingResultOrdinal = reader.GetOrdinal("ProcessingResult");

					var result = new List<MessageWeb>();
					while (reader.Read())
					{
						var item = new MessageWeb();
						result.Add(item);
						item.Id = reader.GetInt32(messageIDOrdinal);
						item.Created = reader.GetDateTimeOrNull(createdOrdinal);
						item.Time = reader.GetDateTime(timeOrdinal);
						if (!reader.IsDBNull(toIDOrdinal))
						{
							item.To = new MessageWeb.Contact();
							item.To.Id = reader.GetInt32(toIDOrdinal);
							item.To.Type = ((ContactType)reader.GetInt32(toTypeOrdinal)).ToString();
							item.To.Value = reader.GetStringOrNull(toValueOrdinal);
						}
						if (!reader.IsDBNull(fromIDOrdinal))
						{
							item.From = new MessageWeb.Contact();
							item.From.Id = reader.GetInt32(fromIDOrdinal);
							item.From.Type = ((ContactType)reader.GetInt32(fromTypeOrdinal)).ToString();
							item.From.Value = reader.GetStringOrNull(fromValueOrdinal);
						}
						item.Subject = reader.GetStringOrNull(subjectOrdinal);
						item.Body = reader.GetStringOrNull(bodyOrdinal);
						var status = reader.GetInt32OrNull(statusOrdinal);
						if (status != null)
							item.Status = ((MessageState)status.Value).ToString();
						var processingResult = reader.GetInt32OrNull(processingResultOrdinal);
						if (processingResult != null)
							item.Result = (MessageProcessingResult)processingResult.Value;
					}
					return result;
				}
			}
		}
	}
}