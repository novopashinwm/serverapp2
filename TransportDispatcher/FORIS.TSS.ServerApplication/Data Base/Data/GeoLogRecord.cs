﻿namespace FORIS.TSS.ServerApplication.Data_Base.Data
{
    public class GeoLogRecord
    {
        public long LogTimeID;
        public int LogTime;
        public int VehicleID;
        public decimal Lng;
        public decimal Lat;
    }
}
