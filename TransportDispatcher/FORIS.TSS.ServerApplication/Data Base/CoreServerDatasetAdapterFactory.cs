using System.ComponentModel;
using FORIS.DataAccess;
using FORIS.TSS.ServerApplication.Database;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	///
	/// </summary>
	/// <remarks>
	/// ���� ��������� ����� ������������ ������ ������
	/// ���������� ���������� (�����������, ���������������
	/// �������� � ��������� � �.�. � �.�.)
	/// </remarks>
	public class CoreServerDatasetAdapterFactory : Component, IDatasetAdapterFactory
	{
		#region Constructor &

		public CoreServerDatasetAdapterFactory()
		{
		}
		public CoreServerDatasetAdapterFactory(IContainer container)
		{
			container.Add(this);
		}

		#endregion Constructor &

		public IDatasetAdapter GetAdapter()
		{
			return new SqlDatasetAdapter();
		}
	}
}