﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Compass.Ufin.Gis;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.WorkplaceShadow.Geo;

namespace FORIS.TSS.ServerApplication
{
	public static class GeoHelper
	{
		private const int ZoneAccessRightID = (int)SystemRight.ZoneAccess;
		private class ZonePrimitive
		{
			public readonly string GeoZoneName;
			private readonly DPoint[] _points;
			private readonly double _minY;
			private readonly double _maxY;
			private readonly double _minX;
			private readonly double _maxX;
			public ZonePrimitive(GEO_ZONE_PRIMITIVE gzp)
			{
				GeoZoneName = gzp.GEO_ZONE.NAME;
				_points = Array.ConvertAll(
					gzp.ZONE_PRIMITIVE.ZONE_PRIMITIVE_VERTEX.ToArray(),
					zpv => ToDPoint(zpv.MAP_VERTEX));

				_minY = double.MaxValue;
				_maxY = double.MinValue;
				_minX = double.MaxValue;
				_maxX = double.MinValue;

				foreach (var point in _points)
				{
					var latitude = point.y;

					if (_minY > latitude)
						_minY = latitude;

					if (_maxY < latitude)
						_maxY = latitude;

					var longitude = point.x;
					if (_minX > longitude)
						_minX = longitude;

					if (_maxX < longitude)
						_maxX = longitude;
				}
			}
			public bool Contains(DPoint point)
			{
				if (point.x < _minX || _maxX < point.x ||
					point.y < _minY || _maxY < point.y)
					return false;

				var state = Math2D.isPointInСontour(point, _points);
				return (state == Math2D.PointInPolygonState.ppsInside ||
						state == Math2D.PointInPolygonState.ppsOnEdge);

			}
		}
		private class MapVertex
		{
			public double Y;
			public double X;
			public IEnumerable<string> Names;
		}
		private static IQueryable<GEO_ZONE_PRIMITIVE> GetGeoZonePrimitivesByOperatorID(this Entities entities, int operatorId)
		{
			const int polygonZoneTypeID = 3;
			return ((from gzp in
						entities.GEO_ZONE_PRIMITIVE
							.Include("ZONE_PRIMITIVE.ZONE_PRIMITIVE_VERTEX.MAP_VERTEX")
							.Include("GEO_ZONE")
					where gzp.ZONE_PRIMITIVE.ZONE_TYPE.ZONE_TYPE_ID == polygonZoneTypeID
					select gzp
				   )).Where(
						item => item.GEO_ZONE.Accesses.Count(
									ozr => ozr.operator_id == operatorId &&
										   ozr.right_id == ZoneAccessRightID
								 ) != 0);
		}
		private static readonly TimingCache<int, List<ZonePrimitive>> GeoZoneVerticesByByOperatorId =
			new TimingCache<int, List<ZonePrimitive>>(
				TimeSpan.FromSeconds(5),
				operatorId =>
				{
					using (var entities = new Entities())
						return (entities.GetGeoZonePrimitivesByOperatorID(operatorId))
							.ToList()
							.ConvertAll(gzp => new ZonePrimitive(gzp));
				});
		private static readonly TimingCache<int, List<MapVertex>> MapVerticesByOperatorID =
			new TimingCache<int, List<MapVertex>>(
				TimeSpan.FromSeconds(5),
				operatorId =>
				{
					using (var context = new Entities())
					{
						return
						(
							from p in context.WEB_POINT
							where p.OPERATOR == null || p.OPERATOR.OPERATOR_ID == operatorId
							group p.MAP_VERTEX by p.MAP_VERTEX
							into v
							select new MapVertex
							{
								Names = v.Key.WEB_POINT.Select(wp => wp.NAME),
								X     = v.Key.X,
								Y     = v.Key.Y
							}
						)
							.ToList();
					}
				});
		public static string GetPositionString(int operatorId, double lat, double lng, int? radius, string language)
		{
			return GetPositionString(operatorId, lat, lng, radius, null, language);
		}
		public static string GetPositionString(int operatorId, double lat, double lng, int? radius, int? vehicleId, string language)
		{
			var address           = TryGetAddress         (lat: lat, lng: lng, language);
			var nearestPoints     = GetNearestPointsString(lat: lat, lng: lng, radius, operatorId);
			var geoZonesString    = GetGeoZonesString     (lat: lat, lng: lng, operatorId);
			var coordinatesString = GetCoordinatesString  (lat: lat, lng: lng);

			return string.Join(
				", ",
				Array.FindAll(new[]
				{
					address,
					nearestPoints,
					geoZonesString,
					coordinatesString
				}, s => !string.IsNullOrEmpty(s)));
		}
		/// <summary> Получить текст позиции (без адреса), геозона, координаты, точность, ближайшая точка интереса </summary>
		/// <param name="operatorId"> Оператор </param>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <param name="radius"> Радиус(точность) </param>
		/// <returns></returns>
		public static string GetPositionStringWithoutAddress(int operatorId, double lat, double lng, int? radius)
		{
			var coordinatesString = GetCoordinatesString  (lat: lat, lng: lng);
			var nearestPoints     = GetNearestPointsString(lat: lat, lng: lng, radius, operatorId);
			var geozones          = GetGeoZonesString     (lat: lat, lng: lng, operatorId);

			return string.Join(
				", ",
				Array.FindAll(new[]
				{
					nearestPoints,
					geozones,
					coordinatesString
				},
				s => !string.IsNullOrEmpty(s)));
		}
		public static string GetAddressString(double lat, double lng, string language)
		{
			return TryGetAddress(lat: lat, lng: lng, language);
		}
		public static string GetNearestPointsString(double lat, double lng, int? radius, int operatorId)
		{
			if (!radius.HasValue)
				radius = 1000;

			var vertices = MapVerticesByOperatorID[operatorId]
				.FindAll(vertex =>
					Common.GeoHelper.DistanceBetweenLocations(
						lat1Degrees: vertex.Y,
						lng1Degrees: vertex.X,
						lat2Degrees: lat,
						lng2Degrees: lng) <= radius.Value);

			if (vertices.Count == 0)
				return null;

			var verticesWithPoints = vertices.FindAll(vertex => vertex.Names.Any());

			if (verticesWithPoints.Count == 0)
				return null;

			var nearestPointStringBuilder = new StringBuilder(100);

			var firstPoint = true;
			foreach (var webPointName in verticesWithPoints.SelectMany(vertex => vertex.Names))
			{
				if (firstPoint)
					firstPoint = false;
				else
					nearestPointStringBuilder.Append(", ");

				nearestPointStringBuilder.Append(webPointName);
			}

			return nearestPointStringBuilder.ToString();
		}
		/// <summary> Получить текст координат, по координатам </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <returns></returns>
		private static string GetCoordinatesString(double lat, double lng)
		{
			const string latLngFormat = "0.#####";
			return lat.ToString(latLngFormat, NumberHelper.FormatInfo) + " " + lng.ToString(latLngFormat, NumberHelper.FormatInfo);
		}
		/// <summary> Получить текст адреса по координатам или null </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <param name="language"> Язык текста, для мультиязычности </param>
		/// <returns></returns>
		private static string TryGetAddress(double lat, double lng, string language)
		{
			try
			{
				var geoClient = GeoClient.Default;
				if (geoClient == null)
					return null;
				return geoClient.GetAddressByPoint(lat: lat, lng: lng, language);
			}
			catch (Exception ex)
			{
				while (ex != null)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();
					ex = ex.InnerException;
				}
				return null;
			}
		}
		/// <summary> Получить текст геозон в которых находится позиция </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <param name="operatorId"> Оператор </param>
		/// <returns></returns>
		public static string GetGeoZonesString(double lat, double lng, int operatorId)
		{
			var zonePrimitives = GetGeoZones(lat: lat, lng: lng, operatorId);

			if (zonePrimitives.Count == 0)
				return null;
			return string.Join(", ", zonePrimitives.ConvertAll(primitive => primitive.GeoZoneName).ToArray());
		}
		/// <summary> Получить список примитивов (геозон) </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <param name="operatorId"> Оператор </param>
		/// <returns></returns>
		private static List<ZonePrimitive> GetGeoZones(double lat, double lng, int operatorId)
		{
			var primitives = GeoZoneVerticesByByOperatorId[operatorId];

			var associatedPrimitives = new List<ZonePrimitive>();

			//TODO: искать в квадродереве
			var point = ToDPoint(x: lng, y: lat);
			foreach (var primitive in primitives)
			{
				if (primitive.Contains(point))
					associatedPrimitives.Add(primitive);
			}

			return associatedPrimitives;
		}
		/// <summary> Преобразовать координаты в геоточку </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		private static DPoint ToDPoint(double x, double y)
		{
			var planar = GeoPoint.CreateGeo(geoX: x, geoY: y).Planar;
			return new DPoint(planar.x, planar.y);
		}
		/// <summary> Преобразовать вершину в геоточку </summary>
		/// <param name="v">Вершина</param>
		/// <returns></returns>
		private static DPoint ToDPoint(MAP_VERTEX v)
		{
			var planar = GeoPoint.CreateGeo(geoX: v.X, geoY: v.Y).Planar;
			return new DPoint(planar.x, planar.y);
		}
		/// <summary> Пересчет матриц геозон (индексов для быстрого поиска вхождения точки в зону) </summary>
		public static void RecalcAllZoneMatrix()
		{
			using (var entities = new Entities())
			{
				var zones = entities.GEO_ZONE;
				foreach (var zone in zones)
				{
					entities.Detach(zone);

					using (var editableEntities = new Entities())
					{
						editableEntities.Attach(zone);

						var zoneID = zone.ZONE_ID;

						var zonePrimitives =
						(
							from gzp in editableEntities.GEO_ZONE_PRIMITIVE
								.Include("ZONE_PRIMITIVE")
								.Include("ZONE_PRIMITIVE.ZONE_PRIMITIVE_VERTEX.MAP_VERTEX")
							where gzp.GEO_ZONE.ZONE_ID == zoneID
							select gzp
						)
							.ToList();

						foreach (var zonePrimitive in zonePrimitives)
						{
							var zoneTypeId = zonePrimitive.ZONE_PRIMITIVE.ZONE_TYPE.ZONE_TYPE_ID;
							var radius = zonePrimitive.ZONE_PRIMITIVE.RADIUS;
							var points = zonePrimitive.ZONE_PRIMITIVE.ZONE_PRIMITIVE_VERTEX
								.ToList()
								.ConvertAll(zpv => new GeoZonePoint
								{
									Lat    = (float)zpv.MAP_VERTEX.Y,
									Lng    = (float)zpv.MAP_VERTEX.X,
									Radius = radius
								})
								.ToArray();
							var matrixNodes = points.GetMatrixNodes((ZoneTypes)zoneTypeId);
							editableEntities.ZoneMatrixUpsert(zoneID, matrixNodes);
						}
						editableEntities.SaveChanges();
					}
				}
			}
		}
		/// <summary> Проверка координат, на допустимость значений </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <returns></returns>
		public static bool ValidPosition(double lat, double lng)
		{
			return Common.GeoHelper.ValidLocation(lat, lng);
		}
	}
}