﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.TransportDispatcher.Reports;

namespace FORIS.TSS.ServerApplication.Mail
{
	public class SchedulerHelper
	{
		public SchedulerHelper(Func<Type, object> resolveDependency)
		{
			_resolveDependency = resolveDependency;
		}

		private readonly Dictionary<int, bool> _currentErrors = new Dictionary<int, bool>();

		ISchedulerPlugin _currentPlugin;

		private readonly DateTime _schedulerStartDateTime = DateTime.UtcNow;
		private readonly Func<Type, object> _resolveDependency;

		/// <summary> Возвращает время старта службы расписаний </summary>
		public DateTime SchedulerStartDateTime
		{
			get { return _schedulerStartDateTime; }
		}
		public BusinessLogic.DTO.Results.SchedulerExecutionResult ProcessEvent(Entities entities, SCHEDULERQUEUE currentTask)
		{
			try
			{
				return ProcessEventInternal(entities, currentTask);
			}
			catch (Exception ex)
			{
				Trace.TraceError("Error during task processing id = {0}: {1}",
					currentTask.SCHEDULERQUEUE_ID, ex);
				RegisterError(entities, currentTask, ex);
				++currentTask.ErrorCount;
				return SchedulerExecutionResult.WaitForReload;
			}
		}
		private BusinessLogic.DTO.Results.SchedulerExecutionResult ProcessEventInternal(Entities entities, SCHEDULERQUEUE task)
		{
			IRepetitionClass irc = null;
			BusinessLogic.DTO.Results.SchedulerExecutionResult result;
			_currentPlugin = GetPlugin(entities, task);
			if (_currentPlugin != null)
			{
				irc = RepetitionParametersServices.FromXML(task.REPETITION_XML);
				if (irc != null && !_currentPlugin.IsActualTask(task.NEAREST_TIME))
				{
					Trace.TraceWarning("Task {0} is not actual and it was not performed", task.SCHEDULERQUEUE_ID);
					UpdateTask(irc, task, null);
					return SchedulerExecutionResult.Success;
				}

				result = ExecuteActualTask(entities, task, irc);
			}
			else
			{
				result = SchedulerExecutionResult.Cancel;
				task.ErrorCount++;
			}

			switch (result.Code)
			{
				case SchedulerExecutionResult.Cancel:
					task.Enabled = false;
					break;
				case SchedulerExecutionResult.Retry:
					task.NEAREST_TIME = DateTime.UtcNow.AddMinutes(5);
					task.ErrorCount++;
					break;
				case SchedulerExecutionResult.WaitForReload:
					task.LastSchedulerStartDate = SchedulerStartDateTime;
					task.ErrorCount++;
					break;
				case SchedulerExecutionResult.Success:
					UnregisterError(task.SCHEDULERQUEUE_ID);
					task.ErrorCount = 0;
					UpdateTask(irc, task, result);
					break;
				default:
					throw new NotImplementedException(string.Format("{0}, not implemented result: {1}", this, result.Code));
			}

			return result;
		}
		private BusinessLogic.DTO.Results.SchedulerExecutionResult ExecuteActualTask(Entities entities, SCHEDULERQUEUE task, IRepetitionClass irc)
		{
			var commentXml = irc != null ? irc.GetComment() : string.Empty;
			var configXml = task.CONFIG_XML;
			BusinessLogic.DTO.Results.SchedulerExecutionResult result;
			try
			{
				result = _currentPlugin.Execute(configXml, commentXml, task.SCHEDULERQUEUE_ID);
			}
			catch (VehicleNotFoundException ex)
			{
				Trace.TraceInformation("Subscription {0} will be disabled because of {1}", task.SCHEDULERQUEUE_ID, ex);
				result = SchedulerExecutionResult.Cancel;
			}
			catch (VehicleGroupNotFoundException ex)
			{
				Trace.TraceInformation("Subscription {0} will be disabled because of {1}", task.SCHEDULERQUEUE_ID, ex);
				result = SchedulerExecutionResult.Cancel;
			}
			catch (SystemRightAccessException ex)
			{
				Trace.TraceInformation("Subscription {0} has loss rights({1}) to {2} id={3}",
					task.SCHEDULERQUEUE_ID, string.Join(",", ex.Rights), ex.IdType, ex.Id);
				if (ex.Id > 0)
					SendSystemRightAccessErrorToOperator(entities, task, ex);

				result = SchedulerExecutionResult.Cancel;
			}
			// Исключение в результате которого IReportClass.Create() возвращает null. Наиболее вероятно, отсутствие данных в отчете.
			catch (Exception ex)
			{
				Trace.TraceError("Processing task {0} failed: {1}", task.SCHEDULERQUEUE_ID, ex);
				result = SchedulerExecutionResult.WaitForReload;
				RegisterError(entities, task, ex);
			}

			return result;
		}
		public void UpdateTask(IRepetitionClass irc, SCHEDULERQUEUE task, BusinessLogic.DTO.Results.SchedulerExecutionResult result)
		{
			DateTime? nextTime;
			var time = _currentPlugin == null || _currentPlugin.IsActualTask(task.NEAREST_TIME)
				? task.NEAREST_TIME
				: DateTime.UtcNow;

			if (result != null && result.NextTime != null)
				time = result.NextTime.Value;

			try
			{
				nextTime = time != DateTime.MaxValue.ToUniversalTime() && irc != null
					? irc.GetNextTimeInUTC(time)
					: null;
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to get next time, SchedulerQueue_ID = {0}: {1}", task.SCHEDULERQUEUE_ID, ex);
				nextTime = null;
			}

			//Задача удаляется из очереди, если:
			//расписание завершилось
			//произошла ошибка, из-за которой не удалось определить очередное время выполнения задачи
			//по результатам выполнения задачи стало понятно, что более выполнять задачу не нужно
			if (nextTime == time || nextTime == null)
			{
				task.Enabled = false;
				return;
			}
			task.NEAREST_TIME   = nextTime.Value;
			task.REPETITION_XML = RepetitionParametersServices.ToXML(irc);

			// Обновление параметров (даты) формирования отчета для отправки
			string strConfigXml = task.CONFIG_XML;
			var daysDelta = (int)(nextTime.Value.Date - time.Date).TotalDays;
			task.CONFIG_XML = UpdateConfigXml(strConfigXml, nextTime.Value.ToLocalTime(), daysDelta);
		}
		/// <summary> Обновление параметров (даты) формирования отчета для отправки в конфигурации задания </summary>
		/// <param name="strConfigXml"></param>
		/// <param name="dtNextNearestTime"></param>
		/// <param name="daysDelta"></param>
		/// <returns></returns>
		public string UpdateConfigXml(string strConfigXml, DateTime dtNextNearestTime, int daysDelta)
		{
			var reportParameters = (ReportParameters)_currentPlugin.GetParameters(strConfigXml);
			if (reportParameters != null)
			{
				DateTime dtDateReport = reportParameters.DateReport;

				// Для отчетов, которые формируются за день
				if (dtDateReport < dtNextNearestTime)
				{
					reportParameters.DateReport = dtNextNearestTime.Date;
				}

				//Сдвигаем даты начала и конца на межотчетный интервал
				reportParameters.DateTimeInterval.DateTo = reportParameters.DateTimeInterval.DateTo.AddDays(daysDelta);
				reportParameters.DateTimeInterval.DateFrom = reportParameters.DateTimeInterval.DateFrom.AddDays(daysDelta);

				strConfigXml = ReportParamsToXml(reportParameters);
			}

			_currentPlugin = null;
			return strConfigXml;
		}
		public static string ReportParamsToXml(object graph)
		{
			using (var stm = new MemoryStream())
			{
				var formatter = new SoapFormatter { AssemblyFormat = FormatterAssemblyStyle.Simple };
				formatter.Serialize(stm, graph);
				byte[] array = stm.ToArray();
				return Encoding.Default.GetString(array);
			}
		}
		private ISchedulerPlugin GetPlugin(Entities entities, SCHEDULERQUEUE task)
		{
			// Комментарий к периодике отчета
			var pluginType = Type.GetType(task.SCHEDULEREVENT.PLUGIN_NAME);
			if (pluginType == null)
			{
				var errorMessage =
					string.Format(CultureInfo.InvariantCulture,
						"Scheduler: Unable to resolve type from SchedulerEvent, SchedulerEvent_ID = {0}, type name = {1}",
						task.SCHEDULEREVENT.SCHEDULEREVENT_ID, task.SCHEDULEREVENT.PLUGIN_NAME);
				Trace.TraceError(errorMessage);
				RegisterError(entities, task, new ObjectNotFoundException(errorMessage));
				return null;
			}

			if (!pluginType.GetInterfaces().Contains(typeof(ISchedulerPlugin)))
			{
				var errorMessage =
					string.Format(CultureInfo.InvariantCulture,
						"Scheduler: Class from SchedulerEvent.Plugin_Name does not implement ISchedulerPlugin, SchedulerEvent_ID = {0}, type name = {1}",
						task.SCHEDULEREVENT.SCHEDULEREVENT_ID, task.SCHEDULEREVENT.PLUGIN_NAME);
				Trace.TraceError(errorMessage);
				RegisterError(entities, task, new ObjectNotFoundException(errorMessage));
				return null;
			}

			var plugin = (ISchedulerPlugin)Activator.CreateInstance(pluginType, new object[] { });
			var dependent = plugin as IDependent;
			if (dependent != null)
				dependent.ResolveDependies(_resolveDependency);
			plugin.SetStaticConfig(task.SCHEDULERQUEUE_ID, task.SCHEDULEREVENT.CONFIG_XML);
			return plugin;
		}
		private void RegisterError(Entities entities, SCHEDULERQUEUE task, Exception ex)
		{
			if (!_currentErrors.ContainsKey(task.SCHEDULERQUEUE_ID))
				SendThroubleToTechnicalSupport(entities, task, ex);
			_currentErrors[task.SCHEDULERQUEUE_ID] = true;
		}
		private void UnregisterError(int taskId)
		{
			if (_currentErrors.ContainsKey(taskId))
				_currentErrors.Remove(taskId);
		}
		private void SendThroubleToTechnicalSupport(Entities entities, SCHEDULERQUEUE task, Exception ex)
		{
			var subject = string.Format(CultureInfo.CurrentCulture, "Error in scheduler {1} {0}", task.SCHEDULEREVENT.COMMENT, task.SCHEDULERQUEUE_ID);
			var message = string.Format("<pre>{0}</pre>", ex);
			foreach (var contact in entities.GetTechnicalSupportEmails())
				entities.CreateOutgoingMessage(contact, subject, message);
		}
		private void SendSystemRightAccessErrorToOperator(Entities entities, SCHEDULERQUEUE task, SystemRightAccessException ex)
		{
			var recipients = task.Contact.ToList();
			var model = new
			{
				TaskId = task.SCHEDULERQUEUE_ID,
				Rights = ex.Rights,
				IdType = ex.IdType,
				Id     = ex.Id
			};
			entities.CreateOutgoingMessages(MessageTemplate.LossRightsToObject, recipients, null, null, ex.Culture, GetFieldValue, model);
		}
		private string GetFieldValue(Entities entities, string name, object obj, CultureInfo cultureInfo)
		{
			var model = (dynamic)obj;
			var rights = (SystemRight[])model.Rights;
			var vehicleId = (int)model.Id;
			var idType = (IdType)model.IdType;
			var taskId = (int)model.TaskId;
			var resourceContainer = ResourceContainers.Get(cultureInfo);
			switch (name)
			{
				case "scheduler_id":
					return taskId.ToString();
				case "scheduler_name":
					return _currentPlugin.GetDisplayName(cultureInfo);
				case "rights_names":
					var rightNames = string.Join(", ", rights.Select(r => resourceContainer[r].ToLower()));
					return rightNames;
				case "object_type":
					return resourceContainer[idType].ToLower();
				case "object_name":
					switch (idType)
					{
						case IdType.Vehicle:
							return entities.VEHICLE.Where(v => v.VEHICLE_ID == vehicleId).Select(v => v.GARAGE_NUMBER).FirstOrDefault();
						default:
							return vehicleId.ToString();
					}
				default:
					throw new NotImplementedException(name);
			}
		}
	}
}