﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using FORIS.TSS.BusinessLogic.DTO.Results;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.Mail
{
    /// <summary>
    /// Базовый класс для реализаций интерфейса <see cref="ISchedulerPlugin"/>
    /// </summary>
    public abstract class BaseSchedulerPlugin : ISchedulerPlugin
    {
        protected T ResolveDependency<T>(Func<Type, object> createInstance) where T: class
        {
            var result = createInstance(typeof (T)) as T;
            if (result == null)
                throw new InvalidOperationException("Unable to resole dependency of type " + typeof (T));
            return result;
        }

        public virtual string GetStaticConfig()
        {
            return string.Empty;
        }

        public virtual void SetStaticConfig(int id, string configurationData)
        {
        }

        public virtual string GetDisplayName(CultureInfo locale)
        {
            return GetType().AssemblyQualifiedName;
        }

        public abstract object GetParameters(string xml);

        public abstract SchedulerExecutionResult Execute(string xml, string commentXml, int queueId);

        public virtual object GetDefaultSettings()
        {
            return null;
        }

        public virtual bool IsActualTask(DateTime nearestTime)
        {
            return true;
        }

        /// <summary>
        /// Сообщить технической поддержке о возникшей неполадке
        /// </summary>
        protected void NotifySupport(string message, Exception e = null)
        {
            var subject = GetType().Name + ": " + message + " at " + XmlConvert.ToString(DateTime.UtcNow, XmlDateTimeSerializationMode.Utc);
            var body = e?.ToString() ?? Environment.StackTrace;
            Trace.TraceWarning("{0} \n {1}", subject, body);
            using (var entities = new Entities())
            {
                entities.SendMessageToSupport(subject, body);
                entities.SaveChanges();
            }
        }
    }
}
