﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;
using Contact = FORIS.TSS.BusinessLogic.DTO.Contact;
#pragma warning disable 1591

namespace FORIS.TSS.ServerApplication.Mail
{
	/// <summary> Построение и рассылка отчётов по электронной почте </summary>
	public class GenericReport_SchedulerPlugin : ISchedulerPlugin
	{
		protected ITssReport Report;
		protected int        ID;
		public GenericReport_SchedulerPlugin()
		{
			Report = null;
			ID     = 0;
		}
		public GenericReport_SchedulerPlugin(ITssReport report, int id)
		{
			Report = report;
			ID     = id;
			report.IBasicFunctionSetInstanceSet(Server.Instance());
		}
		/// <summary> User friendly name of plugin </summary>
		/// <param name="locale"> Locale of name </param>
		/// <returns> name of plugin </returns>
		public string GetDisplayName(CultureInfo locale)
		{
			if (Report == null)
				return null;
			return Report.GetReportName(locale);
		}
		public BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(string xml, string commentXml, int queueId)
		{
			// Build parameters through factory
			var reportParameters = (ReportParameters)GetParameters(xml);

			// TODO: В случае перехода на другой сериализатор, от этого решения можно отказаться,
			// TODO: так как сейчас Kind от DateTime не сериализуется SoapSerializer'ом.
			reportParameters.DateTimeInterval = new DateTimeInterval(
				DateTime.SpecifyKind(reportParameters.DateTimeInterval.DateFrom, DateTimeKind.Unspecified),
				DateTime.SpecifyKind(reportParameters.DateTimeInterval.DateTo, DateTimeKind.Unspecified),
				reportParameters.DateTimeInterval.Accuracy);

			// Execute report and obtain ReportClass
			// Send report through email
			var recipients = MailService.GetRecipients(queueId);

			if (recipients.Count == 0)
			{
				Trace.TraceInformation("No recipients on queued item {0}. The item will be disabled", queueId);
				return SchedulerExecutionResult.Cancel;
			}

			IReportClass rc = null;

			try
			{
				reportParameters = Report.ReportParametersInstanceRefresh(reportParameters);
				var checkResult = Report.ReportParametersInstanceCheck(reportParameters);
				if (!checkResult)
				{
					Trace.TraceInformation("{0}: queueId was canceled because of parameters check result = {1}", this, checkResult);
					return SchedulerExecutionResult.Cancel;
				}

				rc = Report.Create(reportParameters);
				if (rc == null)
					throw new InvalidOperationException(
						string.Format("Error during report {0} creation, queueId: {1}", Report, queueId));

				var reportType = ReportHelper.GetReportType(Config.GlobalsConfig.AppSettings["MailAttachmentFormat"]);

				var supportedFormats = Report.GetSupportedFormats().ToArray();
				if (!supportedFormats.Any())
				{
					throw new InvalidOperationException(
						string.Format("Report {0} [{1}] does not support any format",
							Report.GetType().Name, Report.GetGuid()));
				}

				if (!supportedFormats.Contains(reportType))
					reportType = supportedFormats.First();

				var reportSubject  = Report.GetReportSubject(reportParameters);
				var reportFileName = Report.GetReportFileName(reportParameters);

				var temppath = "MailTempPath".GetAppSettingAsString(Path.GetTempPath());
				var filename = "[" + queueId + "]" + reportFileName;
				var filePath = ReportHelper.ExportReport(rc, temppath, filename, reportType);
				var attachmentFileName = reportFileName + "." + ReportHelper.GetExtension(reportType);

				SendReportToRecipients(reportSubject, recipients, attachmentFileName, filePath);
			}
			catch (SerializationException ex)
			{
				Trace.TraceInformation("Subscription {0} will be disabled because of {1}", queueId, ex);
				return SchedulerExecutionResult.Cancel;
			}
			// Исключение во время неудачной попытки отправить отчет по электронной почте. Откладываем исполнение и отправку на пять минут.
			catch (SmtpException ex)
			{
				Trace.TraceError("Error during sending report. queueId = {0}, exception: {1}", queueId, ex);
				return SchedulerExecutionResult.Retry;
			}
			catch (CrystalReportsException ex)
			{
				Trace.TraceError("Error during sending report. queueId = {0}, exception: {1}", queueId, ex);
				if (ex.InnerException is COMException)
					return SchedulerExecutionResult.Retry;
				else
					throw new CrystalReportsException("No more instances", ex);
			}
			finally
			{
				if (rc != null)
					rc.Dispose();
			}

			return SchedulerExecutionResult.Success;
		}
		private void SendReportToRecipients(string reportSubject, IEnumerable<Contact> recipients, string attachmentFileName, string filePath)
		{
			using (var entities = new Entities())
			{
				foreach (var recipient in recipients)
				{
					if (recipient.Id == null)
						continue;

					var message = new MESSAGE
					{
						BODY               = reportSubject,
						SUBJECT            = reportSubject,
						DestinationType_ID = (int)MessageDestinationType.Email
						// Нет шаблона сообщения
					};

					message.Contacts.Add(new Message_Contact
					{
						Type = (int)MessageContactType.Destination,
						Contact_ID = recipient.Id.Value,
					});

					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.ReportFilePath, filePath);
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.ReportName, attachmentFileName);
				}

				entities.SaveChangesWithHistory();
			}
		}
		public object GetDefaultSettings()
		{
			ReportParameters reportParameters = Report.ReportParametersInstanceGet(-1);
			return reportParameters;
		}
		public bool IsActualTask(DateTime nearestTime)
		{
			return true;
		}
		public object GetParameters(string xml)
		{
			if (!string.IsNullOrEmpty(xml))
			{
				byte[] array = Encoding.Default.GetBytes(xml);
				using (var stm = new MemoryStream(array))
				{
					var formatter = new SoapFormatter { AssemblyFormat = FormatterAssemblyStyle.Simple };

					object graph = formatter.Deserialize(stm);

					return graph;
				}
			}

			return Report.ReportParametersInstanceGet(-1);
		}
		/// <summary>
		/// Returns static configuration data
		/// </summary>
		/// <returns>string with configuration data</returns>
		public string GetStaticConfig()
		{
			return Report.GetType().AssemblyQualifiedName;
		}
		/// <summary>
		/// Sets static configuration data
		/// </summary>
		public void SetStaticConfig(int id, string configurationData)
		{
			var strType = configurationData.Split(',');
			var tssReportBases = ReportsFactory.Instance(Server.Instance(), Repository.Instance).ReportsList;
			foreach (var reportBase in tssReportBases)
			{
				if (String.Equals(reportBase.GetType().ToString().Trim(), strType[0].Trim(), StringComparison.CurrentCultureIgnoreCase))
				{
					ID = id;
					Report = reportBase;
					return;
				}
			}
		}
	}
}