﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.ServerApplication.Mail;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Сервис рассылки отчётов и сообщений </summary>
	public class Scheduler : BackgroundProcessor
	{
		private readonly SchedulerHelper _schedulerHelper;
		private object ResolveDependices(Type type)
		{
			if (type == typeof(IAndroidPublisherService))
				return new GooglePlayAndroidPublisherService();
			if (type == typeof(IAppleVerifyReceiptService))
				return new AppleVerifyReceiptService();
			throw new ArgumentOutOfRangeException("type", type, @"Unknown dependency");
		}
		protected static readonly Scheduler SchedulerInstance = new Scheduler();
		public static Scheduler Instance
		{
			get
			{
				return SchedulerInstance;
			}
		}
		public Scheduler() : base(GetTimeout())
		{
			_schedulerHelper = new SchedulerHelper(ResolveDependices);

			BeforeStart += delegate
			{
				ReportsFactory.Instance(Server.Instance(), Repository.Instance);
			};
		}
		protected TraceSwitch TsScheduler =
			new TraceSwitch("SCHEDULER", "Controls scheduler tracing");
		/// <summary> Возвращает время старта службы расписаний </summary>
		public DateTime SchedulerStartDateTime
		{
			get { return _schedulerHelper.SchedulerStartDateTime; }
		}
		private  static TimeSpan GetTimeout()
		{
			int timeoutMilliseconds;
			if (!int.TryParse(ConfigurationManager.AppSettings["SchedulerCycle"], out timeoutMilliseconds))
				timeoutMilliseconds = 1000;

			return TimeSpan.FromMilliseconds(timeoutMilliseconds);
		}
		/// <summary> Шаг разбора очереди планировщика </summary>
		override protected bool Do()
		{
			using (var entities = new Entities())
			{
				var utcNow = DateTime.UtcNow;
				var schedulerStartDateThreshold = SchedulerStartDateTime.AddSeconds(-1);

				var currentTask = entities.SCHEDULERQUEUE
					.Where(sc =>
						sc.Enabled &&
						sc.NEAREST_TIME < utcNow &&
						(sc.LastSchedulerStartDate == null || sc.LastSchedulerStartDate < schedulerStartDateThreshold)
					)
					.OrderBy(sc => sc.ErrorCount)
					.ThenBy(sc => sc.NEAREST_TIME)
					.FirstOrDefault();

				if (currentTask == null)
					return false;

				if (!currentTask.SCHEDULEREVENTReference.IsLoaded)
					currentTask.SCHEDULEREVENTReference.Load();

				_schedulerHelper.ProcessEvent(entities, currentTask);

				entities.SaveChanges();
			}
			return true;
		}
	}
}