﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.ServerApplication.Mail
{
	public class EmailSender
	{
		private static readonly string      _smtpServer;
		private static readonly int         _smtpServerPort;
		private static readonly string      _smtpServerLogin;
		private static readonly string      _smtpServerPassword;
		private static readonly bool        _useSSL;
		private static readonly MailAddress _mailFromDefault;

		private static SmtpClient GetSmtpClient()
		{
			var client = new SmtpClient(_smtpServer, _smtpServerPort)
			{
				EnableSsl = _useSSL,
				//TODO: Ввести параметр для таймаута СМТП.
				Timeout = 3000
			};

			if (string.IsNullOrEmpty(_smtpServerLogin))
			{
				client.UseDefaultCredentials = true;
				client.Credentials           = CredentialCache.DefaultNetworkCredentials;
			}
			else
			{
				client.UseDefaultCredentials = false;
				client.Credentials           = new NetworkCredential(_smtpServerLogin, _smtpServerPassword);
			}

			return client;
		}
		static EmailSender()
		{
			_smtpServer         = ConfigHelper.GetAppSettingAsString("MailServer", "stub");
			_smtpServerPort     = ConfigHelper.GetAppSettingAsInt32("MailSmtpServerPort", 25);
			_smtpServerLogin    = ConfigHelper.GetAppSettingAsString("MailServerLogin");
			_smtpServerPassword = ConfigHelper.GetAppSettingAsString("MailServerPassword");
			_useSSL             = ConfigHelper.GetAppSettingAsInt32("MailSmtpServerSSL", 0) == 1;
			_mailFromDefault    = new MailAddress(QuoteSquareBraces(ConfigHelper.GetAppSettingAsString("MailFrom")));
		}
		public static void Send(MailMessage mailmsg)
		{
			try
			{
				if (_smtpServer == "stub")
					SendWithStub(mailmsg);
				else
					SendWithSmtpClient(mailmsg);
			}
			finally
			{
				if (mailmsg != null)
				{
					mailmsg.Dispose();
				}
			}
		}
		private static void SendWithStub(MailMessage mailmsg)
		{
			$"Sending message to stub: \n{MailMessageToString(mailmsg)}"
				.CallTraceInformation();
		}
		private static string MailMessageToString(MailMessage message)
		{
			var sb = new StringBuilder(1024);

			if (message.From != null && !string.IsNullOrWhiteSpace(message.From.Address))
				sb.AppendLine("From: " + message.From);
			if (message.To != null && message.To.Count != 0)
				sb.AppendLine("To: " + message.To);
			if (message.CC != null && message.CC.Count != 0)
				sb.AppendLine("CC: " + message.CC);
			if (message.Bcc != null && message.Bcc.Count != 0)
				sb.AppendLine("BCC: " + message.Bcc);
			if (message.ReplyToList != null && message.ReplyToList.Count != 0)
				sb.AppendLine("Reply-To: " + message.ReplyToList);

			sb.AppendLine("Subject: " + message.Subject);
			sb.AppendLine("Body: " + message.Body);

			return sb.ToString();
		}
		private static void SendWithSmtpClient(MailMessage mailmsg)
		{
			bool sent;
			var tryCount = 0;
			do
			{
				using (var client = GetSmtpClient())
				{
					try
					{
						if (mailmsg.From == null || string.IsNullOrEmpty(mailmsg.From.Address))
							mailmsg.From = _mailFromDefault;

						client.Send(mailmsg);
						sent = true;
					}
					catch (SmtpFailedRecipientsException ex)
					{
						var to = new List<MailAddress>();
						var cc = new List<MailAddress>();
						var bcc = new List<MailAddress>();
						foreach (var t in ex.InnerExceptions)
						{
							var status = t.StatusCode;
							if (status == SmtpStatusCode.MailboxBusy ||
								status == SmtpStatusCode.MailboxUnavailable)
							{
								var toAddress = mailmsg.To.FirstOrDefault(c => c.Address == t.FailedRecipient);
								var ccAddress = mailmsg.CC.FirstOrDefault(c => c.Address == t.FailedRecipient);
								var bccAddress = mailmsg.Bcc.FirstOrDefault(c => c.Address == t.FailedRecipient);
								if (toAddress != null)
									to.Add(toAddress);
								else if (ccAddress != null)
									cc.Add(ccAddress);
								else if (bccAddress != null)
									bcc.Add(bccAddress);
							}
							else
							{
								Trace.TraceError("Failed to deliver message to {0}", t.FailedRecipient);
							}
						}

						mailmsg.To.Clear();
						mailmsg.CC.Clear();
						mailmsg.Bcc.Clear();

						foreach (var mailAddress in to)
							mailmsg.To.Add(mailAddress);
						foreach (var mailAddress in cc)
							mailmsg.CC.Add(mailAddress);
						foreach (var mailAddress in bcc)
							mailmsg.Bcc.Add(mailAddress);

						sent = !(mailmsg.To.Any() || mailmsg.CC.Any() || mailmsg.Bcc.Any());
						if(!sent)
							Thread.Sleep(5000);
					}

					tryCount++;
				}
			} while (!sent && tryCount < 2);

			if(!sent)
				Trace.TraceError("Failed to deliver message From: {3}, To: {0}, CC: {1}, BCC: {2}, Subject:{4}, AttachmentsCount:{5}", string.Join(",", mailmsg.To), string.Join(",", mailmsg.CC), string.Join(",", mailmsg.Bcc), mailmsg.From, mailmsg.Subject, mailmsg.Attachments.Count);
		}
		public static void SendOrThrowException(MailMessage mailmsg)
		{
			Send(mailmsg);
		}
		public static void Send(string subject, string message, string[] recepients, string mailFrom, params Attachment[] attachments)
		{
			using (var mailmsg = new MailMessage())
			{
				mailmsg.From = string.IsNullOrWhiteSpace(mailFrom)
					? _mailFromDefault
					: new MailAddress(QuoteSquareBraces(mailFrom));

				foreach (var recepient in recepients)
				{
					mailmsg.To.Add(recepient);
				}

				mailmsg.Subject = subject;
				mailmsg.Body = message;

				Send(mailmsg);
			}
		}
		private static string QuoteSquareBraces(string str)
		{
			return str.Replace("&lt;", "<").Replace("&gt;", ">");
		}
	}
}