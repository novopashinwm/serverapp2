﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO.Results;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// This is plugin for scheduler
	/// </summary>
	public interface ISchedulerPlugin
	{
		/// <summary> Returns static configuration data </summary>
		/// <returns> string with configuration data </returns>
		string GetStaticConfig();
		/// <summary> Sets static configuration data </summary>
		void SetStaticConfig(int id, string configurationData);
		/// <summary> User friendly name of plugin </summary>
		/// <param name="locale"> Locale of name </param>
		/// <returns> name of plugin </returns>
		string GetDisplayName(CultureInfo locale);
		/// <summary> Get parameters plugin with some configuration data </summary>
		/// <param name="xml"> configuration data </param>
		object GetParameters(string xml);
		/// <summary> Executes plugin with some configuration data </summary>
		/// <param name="xml"> configuration data </param>
		/// <param name="commentXml"></param>
		/// <param name="queueId"></param>
		SchedulerExecutionResult Execute(string xml, string commentXml, int queueId);
		/// <summary> returns object with default settings </summary>
		object GetDefaultSettings();
		/// <summary> Проверяет, нужно ли выполнять задачу, назначенную сейчас на время nearestTime </summary>
		/// <param name="nearestTime"> Время, на которое была назначена задача </param>
		bool IsActualTask(DateTime nearestTime);
	}
}