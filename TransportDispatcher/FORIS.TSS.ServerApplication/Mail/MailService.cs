﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.EntityModel;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;
using Attachment = System.Net.Mail.Attachment;
using Contact = FORIS.TSS.BusinessLogic.DTO.Contact;


namespace FORIS.TSS.ServerApplication.Mail
{
	public class MailService
	{
		protected ArrayList events = new ArrayList();
		public MailService()
		{
			FindAndAddSchedulerEvents();
		}
		public void FindAndAddSchedulerEvents()
		{
			DataSet schedulerEvents = GetSchedulerEvents();
			try
			{
				if (ReportsFactory.Instance(Server.Instance(), Repository.Instance).Count > 0)
				{
					// Добавление в список отчетов новой строки.
					for (int i = 0; i < ReportsFactory.Instance(Server.Instance(), Repository.Instance).Count; i++)
					{
						int id = GetSchedulerEventID(schedulerEvents,
							ReportsFactory.Instance(Server.Instance(), Repository.Instance)
								.ReportsList[i].GetType().AssemblyQualifiedName);
						if (id != 0)
						{
							GenericReport_SchedulerPlugin wrapper =
								new GenericReport_SchedulerPlugin(
									ReportsFactory.Instance(Server.Instance(), Repository.Instance).ReportsList[i], id);
							events.Add(wrapper);
							Debug.WriteLine(wrapper.GetStaticConfig());
						}
					}
				}

			}
			catch (Exception ex)
			{
				Trace.TraceError(this.GetTypeName() + ": Error {0}", ex.ToString());
			}
		}
		protected int GetSchedulerEventID(DataSet ds, string typename)
		{
			foreach (DataRow row in ds.Tables["SCHEDULEREVENT"].Rows)
			{
				var config = row["CONFIG_XML"] as string;
				if (string.IsNullOrEmpty(config))
					continue;
				if (typename.StartsWith(config))
					return (int)row["SCHEDULEREVENT_ID"];
			}
			return 0;
		}
		public static List<Contact> GetRecipients(int queueID)
		{
			using (var entities = new Entities())
			{
				return entities.SCHEDULERQUEUE.Where(sq => sq.SCHEDULERQUEUE_ID == queueID)
					.SelectMany(sq => sq.Contact)
					.Select(oc => new Contact
						{
							Value = oc.Value,
							Id = oc.ID
						})
					.ToList();
			}
		}
		/// <summary>
		/// Return tables for subscribtions from database
		/// </summary>
		/// <returns>Dataset with tables EMAIL, SCHEDULEREVENT and SCHEDULEREVENT_EMAIL</returns>
		private static DataSet GetSubscribtionsRaw()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("[dbo].[GetSubscribtions]")
				)
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds,
					new string[]
						{
							"EMAIL", "SCHEDULEREVENT", "EMAIL_SCHEDULEREVENT", "OPERATOR",
							"SCHEDULERQUEUE", "EMAIL_SCHEDULERQUEUE"
						});
				return ds;
			}
		}
		private static void InsertReportNames(DataTable table, string columnName)
		{
			// add column to table
			if (table.Columns.Contains(columnName))
				return;

			DataColumn column = table.Columns.Add(columnName, typeof(string));

			// populate column with information about reports
			foreach (DataRow row in table.Rows)
			{
				int eventID = (int)row["SCHEDULEREVENT_ID"];
				row[column] = GetReportName(eventID);
			}
		}
		private static readonly Dictionary<int, string> pluginNames =
			new Dictionary<int, string>();
		private static void EnsureReportsWasLoaded()
		{
			if (pluginNames.Count > 0)
			{
				return;
			}
			DataSet ds = GetSchedulerEvents();
			foreach (DataRow row in ds.Tables["SCHEDULEREVENT"].Rows)
			{
				// retrieve data from table
				int eventID = (int)row["SCHEDULEREVENT_ID"];
				string typename = (string)row["PLUGIN_NAME"];
				string initdata;
				if (row["CONFIG_XML"] == DBNull.Value)
				{
					initdata = String.Empty;
				}
				else
				{
					initdata = (string)row["CONFIG_XML"];
				}
				// create plugin
				Type t = Type.GetType(typename);
				object o = Activator.CreateInstance(t);
				ISchedulerPlugin plugin = o as ISchedulerPlugin;
				if (plugin == null)
				{
					Trace.WriteLine("Wrong plugin type");
					continue;
				}
				// Initialize plugin
				plugin.SetStaticConfig(eventID, initdata);
				// Get plugin name
				string displayName = plugin.GetDisplayName(null);
				// store it
				pluginNames.Add(eventID, displayName);
			}
		}
		private static DataSet GetSchedulerEvents()
		{
			using (
				StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("[dbo].[GetSchedulerEvents]"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, new string[] { "SCHEDULEREVENT" });
				return ds;
			}
		}
		private static string GetReportName(int reportID)
		{
			// ensure what information about reports is cached
			EnsureReportsWasLoaded();

			string pluginName = pluginNames[reportID];

			if (String.IsNullOrEmpty(pluginName))
				Trace.WriteLine("report ID= " + reportID + " have not Name!");

			return pluginNames[reportID];
		}
		public static void SendReport(
			List<Contact> recipients,
			IReportClass report,
			string reportName,
			string reportFileName,
			ReportParameters reportParameters,
			int queueID,
			ReportTypeEnum reportType)
		{
			using (var attachment = GetReportAsAttachment(report, reportName, reportFileName, reportParameters.DateReport, queueID, reportType))
			{
				SendEmail(recipients, reportName, attachment);
			}
		}
		public static Attachment GetReportAsAttachment(
			IReportClass rc, string reportName, string reportFileName, DateTime dtReport, int queueID, ReportTypeEnum reportType)
		{
			if (rc == null)
				throw new ArgumentNullException(nameof(rc));

			string temppath = GlobalsConfig.AppSettings["MailTempPath"];
			if (temppath == null)
			{
				temppath = Path.GetTempPath();
			}
			string filename = "[" + queueID + "]" + reportFileName;
			
			string filePath = ReportHelper.ExportReport(rc, temppath, filename, reportType);
			var attachment = new Attachment(filePath);
			return attachment;
		}
		public static string QuoteSquareBraces(string str)
		{
			return str.Replace("&lt;", "<").Replace("&gt;", ">");
		}
		public static string BuildEmail(string name, string email)
		{
			return QuoteSquareBraces(name) + "<" + QuoteSquareBraces(email) + ">";
		}
		/// <summary>
		/// Sends message to each recipient in list
		/// </summary>
		/// <param name="recipients">pairs of (recipient-email, recipient name)</param>
		/// <param name="reportName">Локализованное название отчёта</param>
		/// <param name="attachments">Построенный отчёт в виде документов PDF или Excel</param>
		public static void SendEmail(List<Contact> recipients, string reportName, params Attachment[] attachments)
		{
			using (var mailmsg = new MailMessage())
			{
				mailmsg.IsBodyHtml = true;

				foreach (var contact in recipients)
				{
					mailmsg.Bcc.Add(string.IsNullOrWhiteSpace(contact.Name)
						? new MailAddress(contact.Value)
						: new MailAddress(contact.Value, contact.Name));
				}

				//TODO: добавить информацию об отчёте - список параметров и даты, за который построен отчёт
				mailmsg.Subject = reportName;

				//TODO: предоставить ссылку для отписки от отчёта
				mailmsg.Body = mailmsg.Subject;

				if (attachments != null)
				{
					foreach (Attachment attachment in attachments)
					{
						mailmsg.Attachments.Add(attachment);
					}
				}

				EmailSender.SendOrThrowException(mailmsg);
			}
		}
		public static MailMessage CreateMailMessage(string to, string subject, string body)
		{
			return new MailMessage(
				QuoteSquareBraces(GlobalsConfig.AppSettings["MailFrom"]),
				to,
				subject,
				body);
		}
		public static void SendEmail(MailMessage mailmsg)
		{
			EmailSender.Send(mailmsg);
		}
		public static string TechnicalSupportEmail
		{
			get
			{
				return ConfigurationManager.AppSettings["TechnicalSupportEmail"];
			}
		}
		public static string DefaultFromEmail
		{
			get
			{
				return ConfigurationManager.AppSettings["DefaultFromEmail"];
			}
		}
	}
}