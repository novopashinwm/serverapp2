﻿using System.Diagnostics.CodeAnalysis;

namespace FORIS.TSS.ServerApplication.Billing
{
	public interface IAppleVerifyReceiptService
	{
		ApplePurchase VerifyReceipt(bool sandbox, string purchaseBase64);
	}

	/// <summary> DTO-объект для десериализации JSON-результата verifyReceipt</summary>
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public class ApplePurchase
	{
		public int     status;
		public string  environment;
		public Receipt receipt;

		public class Receipt
		{
			public string  receipt_type;
			public string  bundle_id;
			public InApp[] in_app;

			public class InApp
			{
				public string quantity;
				public string product_id;
				public string transaction_id;
				public long   purchase_date_ms;
				public long   original_purchase_date_ms;
				public long   expires_date_ms;
				public long   cancellation_date_ms;
				public string web_order_line_item_id;
				public string is_trial_period;
			}
		}
	}
}