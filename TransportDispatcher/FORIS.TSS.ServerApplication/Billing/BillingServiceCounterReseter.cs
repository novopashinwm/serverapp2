﻿using System;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.Billing
{
	public class BillingServiceCounterReseter : ISchedulerPlugin
	{
		public string GetStaticConfig()
		{
			return string.Empty;
		}

		public void SetStaticConfig(int id, string configurationData)
		{
			//do nothing
		}

		public string GetDisplayName(CultureInfo locale)
		{
			return "Reset of Billing_Service.RenderedQuantity";
		}

		public object GetParameters(string strConfigXML)
		{
			return null;
		}
		
		public BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(string xml, string commentXml, int queueId)
		{
			using (var entities = new Entities())
			{
				var renderedServices =
					entities.Rendered_Service.
					Where(rs => rs.BillingService.ScheduledTask.SCHEDULERQUEUE_ID == queueId).
					Select(rs => new {rs.Billing_Service_ID, rs.Type.Name}).
					ToList();

				if (renderedServices.Count == 0)
					return SchedulerExecutionResult.Cancel;

				foreach (var renderedService in renderedServices)
				{
					entities.SetRenderedQuantity(renderedService.Billing_Service_ID, renderedService.Name, 0);
				}

				return SchedulerExecutionResult.Success;
			}
		}

		public object GetDefaultSettings()
		{
			return null;
		}

		public bool IsActualTask(DateTime nearestTime)
		{
			return true;
		}
	}
}