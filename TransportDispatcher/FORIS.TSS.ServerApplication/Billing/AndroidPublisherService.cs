﻿using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;
using Google.Apis.AndroidPublisher.v2;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;

namespace FORIS.TSS.ServerApplication.Billing
{
	class GooglePlayAndroidPublisherService : IAndroidPublisherService
	{
		public SubscriptionPurchase GetSubscriptionPurchase(string package, string product, string purchaseToken)
		{
			var privateKeyDto =
				JsonHelper.DeserializeObjectFromJson<GooglePrivateKeyDto>(
					Server.Instance().GetConstant(Constant.AndroidPrivateKey));

			var serviceAccountCredential =
				new ServiceAccountCredential(new ServiceAccountCredential.Initializer(
					privateKeyDto.client_email)
				{
					Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
				}.FromPrivateKey(privateKeyDto.private_key));

			var androidPublisherService = new AndroidPublisherService(new BaseClientService.Initializer
			{
				HttpClientInitializer = serviceAccountCredential,
				ApplicationName = "ProjectX"
			});

			var purchase = androidPublisherService.Purchases.Subscriptions.Get(package, product, purchaseToken).Execute();

			return new SubscriptionPurchase
			{
				AutoRenewing = purchase.AutoRenewing,
				ExpiryTimeMillis = purchase.ExpiryTimeMillis
			};
		}
	}
}