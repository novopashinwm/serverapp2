﻿using System;

namespace FORIS.TSS.ServerApplication.Billing
{
	//TODO: использовать фреймворк для Dependency Injection
	interface IDependent
	{
		void ResolveDependies(Func<Type, object> createInstance);
	}
}