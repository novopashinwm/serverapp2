using System.Diagnostics.CodeAnalysis;

namespace FORIS.TSS.ServerApplication.Billing
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class GooglePrivateKeyDto
    {
#pragma warning disable 649
        public string private_key;
        public string client_email;
#pragma warning restore 649
    }
}