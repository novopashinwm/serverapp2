﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Results;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Mail;
#pragma warning disable 1591

namespace FORIS.TSS.ServerApplication.Billing
{
	/// <summary>
	/// Асинхронный обработчик платежной информации - чеков (receipt) от устройств Apple
	/// </summary>
	public class AppleReceiptProcessor : BaseSchedulerPlugin, IDependent
	{
		private IAppleVerifyReceiptService _appleVerifyReceiptService;

		public void ResolveDependies(Func<Type, object> createInstance)
		{
			_appleVerifyReceiptService = ResolveDependency<IAppleVerifyReceiptService>(createInstance);
		}
		
		/// <summary> Параметры чека </summary>
		public class Parameters
		{
			/// <summary>Закодированный receipt</summary>
			public string PurchaseBase64;
			/// <summary>Идентификатор мобильного приложения, которое прислало этот чек </summary>
			public string AppId;
		}

		public override object GetParameters(string xml)
		{
			return JsonHelper.DeserializeObjectFromJson<Parameters>(xml);
		}

		public override SchedulerExecutionResult Execute(string xml, string commentXml, int queueId)
		{
			var parameters = JsonHelper.DeserializeObjectFromJson<Parameters>(xml);
			var sandbox = IsSandbox(parameters.AppId);
			if (sandbox == null)
				return BusinessLogic.ResultCodes.SchedulerExecutionResult.Cancel;

			for (var i = 0; i != 2; ++i)
			{
				ApplePurchase applePurchase;
				try
				{
					applePurchase = _appleVerifyReceiptService.VerifyReceipt(sandbox.Value, parameters.PurchaseBase64);
				}
				catch (Exception e)
				{
					NotifySupport("Error during verifing apple purchase", e);
					return BusinessLogic.ResultCodes.SchedulerExecutionResult.WaitForReload;
				}
				if (applePurchase == null)
				{
					NotifySupport("verifyReceipt returned null");
					return Delay();
				}                

				switch (applePurchase.status)
				{
					case 0: //Success
						return RefreshServices(queueId, applePurchase.receipt);
					//The receipt server is not currently available.
					case 21005:
						return Delay();
					case 21002:
					case 21003:
						NotifySupport("Unsuccessful verifyReceipt status: " + applePurchase.status);
						return BusinessLogic.ResultCodes.SchedulerExecutionResult.Cancel;
					case 21007:
					case 21008:
						sandbox = !sandbox;
						continue;
					default:
						NotifySupport("Unsuccessful verifyReceipt status: " + applePurchase.status);
						return BusinessLogic.ResultCodes.SchedulerExecutionResult.WaitForReload;
				}
			}
			return BusinessLogic.ResultCodes.SchedulerExecutionResult.WaitForReload;
		}

		private SchedulerExecutionResult RefreshServices(int queueId, ApplePurchase.Receipt receipt)
		{
			if (receipt == null || receipt.in_app == null)
			{
				NotifySupport("Empty receipt or purchases when processing queue id " + queueId);
				return BusinessLogic.ResultCodes.SchedulerExecutionResult.Cancel;
			}

			using (var entities = new Entities())
			{
				var operatorId =
					entities.SCHEDULERQUEUE
						.Where(sq => sq.SCHEDULERQUEUE_ID == queueId)
						.Select(sq => sq.Operator_ID).First();

				if (operatorId == null)
				{
					NotifySupport("Schedule " + queueId + " does not have operator attached" );
					return BusinessLogic.ResultCodes.SchedulerExecutionResult.Cancel;
				}

				//Добавляем подписки, для которых нет услуг
				foreach (var purchase in receipt.in_app)
				{
					var expireDate = (purchase.expires_date_ms != 0)
						? TimeHelper.FromLong(purchase.expires_date_ms)
						: (DateTime?) null;
					
					//Это возобновляемая подписка
					//Договорились, что невозобновляемых подписок не будет

					if (expireDate < DateTime.UtcNow)
						continue; //Возобновляемая подписка завершилась

					//Далее или возобновляемая подписка, или разовая покупка
					
					var serviceType = string.Empty
						+ "apple"
						+ Billing_Service_Type.PurchaseInfoDelimiter
						+ receipt.bundle_id
						+ Billing_Service_Type.PurchaseInfoDelimiter
						+ purchase.product_id;

					Billing_Service billingService =
						entities.Billing_Service.FirstOrDefault(
							bs => bs.Type.Service_Type == serviceType &&
								  bs.PurchaseToken == purchase.transaction_id);

					if (billingService == null)
						billingService = entities.CreateBillingService(serviceType);

					if (billingService.Operator_ID != operatorId)
						billingService.Operator_ID = operatorId;


					billingService.PurchaseToken = purchase.transaction_id;
					billingService.EndDate = expireDate;
				}

				entities.CreateAppNotificationAboutOperatorDataChange(
					operatorId.Value, UpdatedDataType.BillingServices);

				entities.SaveChangesBySchedulerQueue(queueId);
			}

			//Инициация продления срока действия услуги остается за мобильным приложением

			return BusinessLogic.ResultCodes.SchedulerExecutionResult.Cancel;
		}

		private SchedulerExecutionResult Delay()
		{
			return new SchedulerExecutionResult(DateTime.UtcNow.AddMinutes(1));
		}

		private bool? IsSandbox(string appId)
		{
			using (var entities = new Entities())
			{
				var contact =
					entities.AppClient.Where(ac => ac.AppId == appId)
						.Select(ac => new { Type = (ContactType) ac.Contact.Type })
						.FirstOrDefault();
				if (contact == null)
					return null;
				switch (contact.Type)
				{
					case ContactType.iOsDebug:
						return true;
					case ContactType.Apple:
						return false;
					default:
						return null;
				}
			}
		}
	}
}