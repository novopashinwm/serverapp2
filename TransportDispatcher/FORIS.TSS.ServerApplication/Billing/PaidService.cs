﻿namespace FORIS.TSS.ServerApplication.Billing
{
	/// <summary> Платная услуга для оказания её абоненту </summary>
	/// <remarks> Платной услугой является услуга абоненту, которая требует учёта расходов на её оказание </remarks>
	public enum PaidService
	{
		/// <summary> Отправить SMS с уведомлением о событии </summary>
		SendNotificationSms  = 1,
		/// <summary> Получить позицию по данным сотовой сети </summary>
		AskPositionOverLBS   = 2,
		/// <summary> Отправить информационное сообщение по SMS </summary>
		SendInformationalSms = 3,
		/// <summary> Отправить конфигурационное сообщение по SMS </summary>
		SendConfigurationSms = 4,
	}
}