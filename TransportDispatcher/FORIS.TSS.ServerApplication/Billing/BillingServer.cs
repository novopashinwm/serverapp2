﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication.Billing
{
	/// <summary> Содержит логику для поддержки ограничений на оказание платных услуг </summary>
	public class BillingServer
	{
		private readonly Entities _entities;

		/// <summary> Создает экземпляр сервера для контроля ограничений на оказание платных услуг и списания </summary>
		/// <param name="entities"> Контекст подключения к БД, в рамках которого работает сервер </param>
		/// <exception cref="ArgumentNullException"> Бросает исключение, если передан нулевой экземпляр entities </exception>
		public BillingServer(Entities entities)
		{
			if (entities == null)
				throw new ArgumentNullException(nameof(entities));

			_entities = entities;
		}

		private static Expression<Func<Billing_Service, bool>> GetServiceTypeCondition(PaidService paidService)
		{
			//TODO: убрать логику по услугам из БД, сосредоточив её в объектах наследниках абстрактной услуги, которые и будут знать, что может, а что нет данная услуга
			switch (paidService)
			{
				case PaidService.SendNotificationSms:
				case PaidService.SendConfigurationSms:
					return bs => bs.Type.SMS;
				case PaidService.AskPositionOverLBS:
					return bs => bs.Type.LBS;
				default:
					throw new ArgumentOutOfRangeException("paidService", paidService, @"Value is out of range, probably not supported or implemented");
			}
		}

		private static bool IsApproriate(BillingService billingService, PaidService paidService)
		{
			//TODO: убрать логику по услугам из БД, сосредоточив её в объектах наследниках абстрактной услуги, которые и будут знать, что может, а что нет данная услуга

			if (billingService.EndDate <= DateTime.UtcNow)
				return false;

			switch (paidService)
			{
				case PaidService.SendNotificationSms:
				case PaidService.SendConfigurationSms:
					return billingService.Sms ?? false;
				case PaidService.AskPositionOverLBS:
					return billingService.Lbs ?? false;
				default:
					throw new ArgumentOutOfRangeException("paidService", paidService, @"Value is out of range, probably not supported or implemented");
			}
		}

		/// <summary> Услуга подключена на аккаунт пользователя - вариант Индия </summary>
		private Expression<Func<Billing_Service, bool>> ServiceIsOnOperator(int operatorId)
		{
			return bs => bs.Operator_ID == operatorId;
		}

		/// <summary> Услуга подключена на клиента - любой абонент в системе </summary>
		private Expression<Func<Billing_Service, bool>> ServiceIsOnDepartment(int operatorId)
		{
			return bs => _entities.v_operator_department_right
				.Any(odr =>
					odr.Department_ID == bs.Department_ID &&
					odr.operator_id   == operatorId &&
					odr.right_id      == (int)SystemRight.DepartmentsAccess);
		}

		/// <summary> Услуга подключена на телефон пользователя - вариант МТС, физлица </summary>
		private Expression<Func<Billing_Service, bool>> ServiceIsOnOperatorAsid(int operatorId)
		{
			return bs => bs.Asid.OPERATOR.OPERATOR_ID == operatorId;
		}

		private Expression<Func<Billing_Service, bool>> ServiceIsOnDepartment(int operatorId, int vehicleId)
		{
			return bs =>
				bs.Department_ID == _entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId).DEPARTMENT.DEPARTMENT_ID &&
				_entities.v_operator_department_right.Count(
					odr =>
					odr.Department_ID == bs.Department_ID &&
					odr.operator_id == operatorId &&
					odr.right_id == (int)SystemRight.DepartmentsAccess) == 1;
		}

		/// <summary> Услуга подключена на устройство vehicleId, и устройству разрешено оплачивать входящие запросы - вариант МТС, корпоративные клиенты </summary>
		private Expression<Func<Billing_Service, bool>> ServiceIsOnVehiclePayingForCaller(int operatorId, int vehicleId)
		{
			return bs =>
				(bs.Vehicle_ID == vehicleId || bs.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId) &&
				_entities.v_operator_vehicle_right.Any(
					ovr =>
					ovr.operator_id == operatorId &&
					ovr.right_id == (int) SystemRight.PayForCaller &&
					ovr.vehicle_id == vehicleId);
		}

		/// <summary> Услуга-пакет, списывать с которой возможно любому пользователю в рамках департамента </summary>
		private Expression<Func<Billing_Service, bool>> SharedService(int operatorId)
		{
			return bs =>
				bs.Type.Shared &&
				bs.Asid.DEPARTMENT.Accesses.Any(
					a => a.operator_id == operatorId &&
						a.right_id     == (int)SystemRight.PayForCaller);
		}

		private IQueryable<Billing_Service> GetBillingServicesBasicQuery(PaidService paidService)
		{
			var billingServiceTypePredicate = GetServiceTypeCondition(paidService);

			return
				_entities.Billing_Service
					.Where(billingServiceTypePredicate)
					.Where(bs => bs.EndDate == null || DateTime.UtcNow < bs.EndDate);
		}

		private BillingResult AllowRenderService(ServiceToRender service)
		{
			//Проверка наличия блокировки
			if (service.AsidIsBlocked)
				return BillingResult.ServiceIsBlocked;

			var minIntervalSeconds = service.MinIntervalSeconds;
			var maxQuantity = service.MaxQuantity;

			var renderedQuantity = service.RenderedQuantity ?? 0;
			var renderedLastTime = service.RenderedLastTime;

			if (maxQuantity != null && maxQuantity.Value <= renderedQuantity)
				return BillingResult.InsufficientFunds; //Закончилось количество единиц услуги

			if (minIntervalSeconds != null && renderedLastTime != null)
			{
				var secondsSinceLastRender = (DateTime.UtcNow - renderedLastTime.Value).TotalSeconds;
				if (secondsSinceLastRender < minIntervalSeconds)
					return BillingResult.LimitPerTimeSpanReached; //Интервал оказания услуги ещё не истёк
			}

			//Ограничений нет
			return BillingResult.Success;
		}

		private static string GetRenderedServiceCategory(PaidService paidService)
		{
			switch (paidService)
			{
				case PaidService.AskPositionOverLBS:
					return "LBS";
				case PaidService.SendConfigurationSms:
				case PaidService.SendInformationalSms:
				case PaidService.SendNotificationSms:
					return "SMS";
				default:
					throw new ArgumentOutOfRangeException("paidService", paidService, @"Value is not supported yet");
			}
		}

		private BillingResult AllowRenderService(IEnumerable<ServiceToRender> billingServices)
		{
			if (billingServices == null)
				return BillingResult.NoService; // Ни один критерий не применим

			var lastKnownBillingResult = BillingResult.NoService; // Услуга не подключена
			ServiceToRender serviceForResult = null;

			foreach (var billingService in billingServices)
			{
				lastKnownBillingResult = AllowRenderService(billingService);
				serviceForResult = billingService;

				if (lastKnownBillingResult == BillingResult.Success)
					break;
			}

			switch (lastKnownBillingResult)
			{
				case BillingResult.NoService:
					if (serviceForResult == null)
						return BillingResult.NoService;
					switch (serviceForResult.WhoPays)
					{
						case WhoPays.Caller: return BillingResult.NoCallerService;
						case WhoPays.Target: return BillingResult.NoTargetService;
					}
					break;
				case BillingResult.ServiceIsBlocked:
					if (serviceForResult == null)
						return BillingResult.ServiceIsBlocked;
					switch (serviceForResult.WhoPays)
					{
						case WhoPays.Caller: return BillingResult.CallerServiceIsBlocked;
						case WhoPays.Target: return BillingResult.TargetServiceIsBlocked;
					}
					break;
			}
			return lastKnownBillingResult;
		}

		/// <summary> Проверяет, можно ли оказать услугу paidService пользователю operatorId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="paidService"> Платная услуга </param>
		/// <returns> Возвращает true, если услуга может быть оказана и false в противном случае </returns>
		public BillingResult AllowRenderService(int operatorId, PaidService paidService)
		{
			var billingServices = GetBillingServices(operatorId, paidService);

			return AllowRenderService(billingServices);
		}

		/// <summary> Проверяет, можно ли оказать услугу paidService пользователю operatorId для объекта vehicleId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения, в связи с которым оказывается услуга </param>
		/// <param name="paidService"> Платная услуга </param>
		/// <returns> Возвращает true, если услуга может быть оказана и false в противном случае </returns>
		public BillingResult AllowRenderService(int operatorId, int vehicleId, PaidService paidService)
		{
			if (_entities.GetAsidByVehicleIdQuery(vehicleId).Any(a => a.Billing_Blocking != null && a.Billing_Blocking.BlockingDate <= DateTime.UtcNow))
				return BillingResult.TargetServiceIsBlocked;

			var billingServices = GetBillingServices(operatorId, vehicleId, paidService);

			return AllowRenderService(billingServices);
		}

		private class ServiceToRender
		{
			public WhoPays WhoPays;

			public readonly bool      AsidIsBlocked;
			public readonly DateTime? RenderedLastTime;
			public readonly int?      RenderedQuantity;
			public readonly string    RenderedServiceName;
			public readonly string    ServiceType;
			public readonly int?      MinIntervalSeconds;
			public readonly int?      BillingServiceID;
			public readonly int?      MaxQuantity;
			public readonly int?      AsidId;
			public readonly int?      VehicleId;
			public ServiceToRender(PaidService paidService, Asid asid)
				: this(paidService, asid, null)
			{
			}
			public ServiceToRender(PaidService paidService, Billing_Service billingService)
				: this(paidService, billingService.Asid, billingService)
			{
			}
			public ServiceToRender(PaidService paidService, BillingService billingService, Entities entities)
			{
				RenderedServiceName = GetRenderedServiceCategory(paidService);

				VehicleId = billingService.VehicleId;
				AsidIsBlocked = billingService.IsBlocked ?? false;

				//TODO: брать из BillingService
				var renderedService = entities.Rendered_Service.FirstOrDefault(
					rs => rs.Billing_Service_ID == billingService.Id &&
						  rs.Type.Name == RenderedServiceName);

				if (renderedService != null)
				{
					RenderedLastTime = renderedService.LastTime;
					RenderedQuantity = renderedService.Quantity;
				}

				MinIntervalSeconds = billingService.MinIntervalSeconds;
				MaxQuantity = billingService.MaxQuantity;

				BillingServiceID = billingService.Id;
			}
			private ServiceToRender(PaidService paidService, Asid asid, Billing_Service billingService)
			{
				RenderedServiceName = GetRenderedServiceCategory(paidService);

				if (asid == null && billingService != null)
				{
					if (!billingService.AsidReference.IsLoaded)
						billingService.AsidReference.Load();
					asid = billingService.Asid;
				}

				if (asid != null)
				{
					AsidId = asid.ID;
					if (!asid.Billing_BlockingReference.IsLoaded)
						asid.Billing_BlockingReference.Load();
					AsidIsBlocked = asid.Billing_Blocking != null && asid.Billing_Blocking.BlockingDate <= DateTime.UtcNow;
				}

				if (billingService != null)
				{
					if (!billingService.RenderedServices.IsLoaded)
						billingService.RenderedServices.Load();
					var renderedService = billingService.RenderedServices.FirstOrDefault(
						rs => rs.Type.Name == RenderedServiceName);

					RenderedLastTime = renderedService != null ? renderedService.LastTime : (DateTime?)null;
					RenderedQuantity = renderedService != null ? renderedService.Quantity : (int?)null;

					if (!billingService.TypeReference.IsLoaded)
						billingService.TypeReference.Load();
					ServiceType = billingService.Type.Service_Type;
					MinIntervalSeconds = billingService.Type.MinIntervalSeconds;
					MaxQuantity = billingService.Type.LimitedQuantity
						? (billingService.MaxQuantity ?? billingService.Type.MaxQuantity)
						: null;

					BillingServiceID = billingService.ID;
				}
			}
		}
		private enum WhoPays
		{
			Caller,
			Target,
			OtherParty
		}
		private IQueryable<Billing_Service> Order(IQueryable<Billing_Service> billingServices)
		{
			return billingServices.OrderBy(bs => bs.StartDate);
		}

		private IEnumerable<ServiceToRender> GetBillingServices(int operatorId, PaidService paidService)
		{
			var billingServices = _entities.Billing_Service.Where(bs => bs.Type.SMS);

			foreach (var billingService in Order(billingServices.Where(ServiceIsOnDepartment(operatorId))))
				yield return new ServiceToRender(paidService, billingService) { WhoPays = WhoPays.OtherParty };

			foreach (var billingService in Order(billingServices.Where(ServiceIsOnOperator(operatorId))))
				yield return new ServiceToRender(paidService, billingService) { WhoPays = WhoPays.Caller     };
		}
		/// <summary> Получить услуги, которые можно оказать </summary>
		/// <param name="operatorId"></param>
		/// <param name="vehicleId"></param>
		/// <param name="paidService"></param>
		/// <returns></returns>
		private IEnumerable<ServiceToRender> GetBillingServices(int operatorId, int vehicleId, PaidService paidService)
		{
			if (_entities.IsAllowedVehicleAll(operatorId, vehicleId, SystemRight.PayForCaller))
			{
				var vehicleServices = Server.Instance().DbManager
					.GetVehicleServices(vehicleId)
					.Where(bs => IsApproriate(bs, paidService))
					.OrderBy(bs => bs.StartDate)
					.ToList();

				foreach (var billingService in vehicleServices)
					yield return new ServiceToRender(paidService, billingService, _entities) { WhoPays = WhoPays.Target };
			}

			var operatorServices = Server.Instance().DbManager
				.GetOperatorServices(operatorId, null)
				.Where(bs => IsApproriate(bs, paidService))
				.OrderBy(bs => bs.StartDate)
				.ToList();

			foreach (var billingService in operatorServices)
				yield return new ServiceToRender(paidService, billingService, _entities) { WhoPays = WhoPays.OtherParty };
		}

		private bool AllCdrArePayed(Asid asid)
		{
			return asid.AllowsCdrPayment;
		}

		private bool IsPhysicalCustomer(int operatorId)
		{
			return !_entities.IsCorporateCustomer(operatorId);
		}

		/// <summary> Фиксирует факт оказания платной услуги и списывает средства со счёта </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="paidService">Платная услуга</param>
		/// <param name="commandId">Идентификатор команды, отправку которой нужно оплатить</param>
		/// <param name="messageId">Идентификатор сообщения, отправку которого нужно оплатить</param>
		public int? ChargeServiceRendering(int operatorId, PaidService paidService, int? commandId = null, int? messageId = null)
		{
			var billingServiceQuery = GetBillingServices(operatorId, paidService);
			var result = ChargeServiceRendering(billingServiceQuery, commandId, messageId);
			ProcessResult(operatorId, result, paidService);
			return result.PayerVehicleId;
		}

		/// <summary> Фиксирует факт оказания платной услуги и списывает средства со счёта </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="vehicleId">Идентификатор объекта наблюдения, в связи с которым оказывается услуга</param>
		/// <param name="paidService">Платная услуга</param>
		/// <returns>Возвращает VehicleID объекта наблюдения, за чей счёт была оказана услуга</returns>
		/// <param name="commandId">Идентификатор команды, отправку которой нужно оплатить</param>
		/// <param name="messageId">Идентификатор сообщения, отправку которого нужно оплатить</param>
		public int? ChargeServiceRendering(int operatorId, int vehicleId, PaidService paidService, int? commandId = null, int? messageId = null)
		{
			var result = ChargeServiceRendering(GetBillingServices(operatorId, vehicleId, paidService), commandId, messageId);
			ProcessResult(operatorId, result, paidService);
			return result.PayerVehicleId;
		}

		private void ProcessResult(int operatorId, ChargeResult result, PaidService paidService)
		{
			if (!result.Exhausted)
				return;

			var asid = _entities.Asid.Include(Asid.ContactIncludePath).FirstOrDefault(a => a.OPERATOR.OPERATOR_ID == operatorId);

			if (asid == null || asid.Contact == null)
				return; //Некуда отправлять уведомление

			//Пакет услуг Ufin LBS Бюджет на объекте Петя исчерпан. Дождитесь 28.09.2013 17:28 или подключите безлимитный пакет.

			var sb = new StringBuilder();
			sb.Append("Пакет услуг ");
			sb.Append(result.BillingService.Type.Name);

			if (result.PayerVehicleId != null)
			{
				var vehicleName = Server.Instance().GetVehicleName(operatorId, result.PayerVehicleId.Value);

				if (!string.IsNullOrWhiteSpace(vehicleName))
				{
					sb.Append(" на объекте наблюдения ");
					sb.Append(vehicleName);
				}
			}
			sb.Append(" исчерпан.");

			var nextRefresh = _entities.SCHEDULERQUEUE.
										Where(sq => sq.Billing_Service.Any(bs => bs.ID == result.BillingService.ID)).
										Select(sq => new {Time = sq.NEAREST_TIME}).
										FirstOrDefault();

			if (nextRefresh != null)
			{
				var timeZone = _entities.TimeZone.FirstOrDefault(tz => tz.OPERATOR.Any(o => o.OPERATOR_ID == operatorId));
				var timeZoneById = timeZone != null ? TimeZoneInfo.FindSystemTimeZoneById(timeZone.Code) : null;
				var time = timeZoneById != null
							   ? TimeZoneInfo.ConvertTimeFromUtc(nextRefresh.Time, timeZoneById)
							   : nextRefresh.Time;

				sb.Append(" Дождитесь ");
				sb.Append(time.ToString(TimeHelper.DefaultTimeFormatUpToMinutes));
				sb.Append(" или подключите безлимитный пакет.");
			}
			else
			{
				sb.Append(" Подключите безлимитный пакет.");
			}
		}

		class ChargeResult
		{
			/// <summary>
			/// Название услуги
			/// </summary>
			public Billing_Service BillingService;

			/// <summary>
			/// true, если в результате тарификации услуги объём пакета услуг подошёл к концу
			/// </summary>
			public bool Exhausted;

			/// <summary>
			/// Идентификатор объекта наблюдения, за счёт которого была оказана услуга
			/// </summary>
			public int? PayerVehicleId;
		}

		private ChargeResult ChargeServiceRendering(IEnumerable<ServiceToRender> services, int? commandId = null, int? messageId = null)
		{
			var result = new ChargeResult();

			var billingResult = BillingResult.NoService;
			foreach (var service in services)
			{
				billingResult = AllowRenderService(service);

				if (billingResult != BillingResult.Success)
					continue;

				var utcNow = DateTime.UtcNow;
				if (service.BillingServiceID != null)
				{
					var renderedQuantity = _entities
							.IncreaseRenderedQuantity(service.BillingServiceID, service.RenderedServiceName, null)
							.FirstOrDefault();
					var maxQuantity = service.MaxQuantity;
					if (maxQuantity != null && renderedQuantity != null &&
						maxQuantity.Value == renderedQuantity.Value)
						result.Exhausted = true;
				}

				if (service.BillingServiceID != null)
					result.BillingService = _entities.Billing_Service.First(bs => bs.ID == service.BillingServiceID);

				var asidId = service.AsidId;

				if (asidId == null && service.VehicleId != null)
				{
					var asidIdValue = _entities.Asid
						.Where(
							a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == service.VehicleId)
						.Select(a => a.ID)
						.FirstOrDefault();
					if (asidIdValue != default(int))
						asidId = asidIdValue;
				}

				if (asidId != null)
				{
					using (var changableEntities = new Entities())
					{
						var status = service.BillingServiceID != null ? RenderedServiceItemStatus.Payed : RenderedServiceItemStatus.Created;
						var renderedServiceName = service.RenderedServiceName;
						var billingServiceId = service.BillingServiceID;
						changableEntities.ExecuteGuarantly(e =>
						{
							e.RenderedServiceItem.AddObject(
								new RenderedServiceItem
								{
									RenderDate = utcNow,
									Asid_ID = asidId.Value,
									Rendered_Service_Type =
										e.Rendered_Service_Type.First(rst => rst.Name == renderedServiceName),
									Billing_Service_ID = billingServiceId,
									Command_ID = commandId,
									Message_ID = messageId,
									Status = (int)status,
									StatusDate = utcNow
								});

							e.SaveChanges();
						});
					}
				}

				if (asidId != null)
				{
					var payerVehicle =
						_entities.VEHICLE
								 .Where(v =>
									 v.CONTROLLER.MLP_Controller.Asid.ID == asidId)
								 .Select(v => new {Id = v.VEHICLE_ID})
								 .FirstOrDefault();

					if (payerVehicle != null)
						result.PayerVehicleId = payerVehicle.Id;
				}
				else if (service.BillingServiceID != null)
				{
					var payerVehicle = _entities.VEHICLE
								 .Where(v =>
									 v.CONTROLLER.MLP_Controller.Asid.Billing_Service.Any(
										 bs => bs.ID == service.BillingServiceID))
								 .Select(v => new {Id = v.VEHICLE_ID})
								 .FirstOrDefault();

					if (payerVehicle == null)
					{
						payerVehicle = _entities.VEHICLE
									 .Where(v =>
										 v.Billing_Service.Any(bs =>
											 bs.ID == service.BillingServiceID))
									 .Select(v => new {Id = v.VEHICLE_ID})
									 .FirstOrDefault();
					}

					if (payerVehicle != null)
						result.PayerVehicleId = payerVehicle.Id;
				}

				return result;
			}

			if (billingResult != BillingResult.Success)
				throw new BillingException("Unable to charge, billing result: " + billingResult);

			return null;
		}


	}

	internal class BillingException : Exception
	{
		public BillingException(string message) : base(message)
		{
		}
	}
}
