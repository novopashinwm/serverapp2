﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Mail;

namespace FORIS.TSS.ServerApplication.Billing
{
	class AndroidSubscriptionValidator : BaseSchedulerPlugin, IDependent
	{
		private IAndroidPublisherService _androidPublisherService;

		public void ResolveDependies(Func<Type, object> createInstance)
		{
			_androidPublisherService = ResolveDependency<IAndroidPublisherService>(createInstance);
		}

		public override object GetParameters(string xml)
		{
			return null;
		}

		public override BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(string xml, string commentXml, int queueId)
		{
			using (var entities = new Entities())
			{
				var result = Execute(entities, queueId);
				entities.SaveChanges(SaveContext.Scheduler(queueId));
				return result;
			}
		}

		private BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(Entities entities, int queueId)
		{
			var billingService = entities.Billing_Service.First(bs => bs.ScheduledTask.SCHEDULERQUEUE_ID == queueId);

			var parts = billingService.Type.Service_Type.Split('|');

			var package = parts[1];
			var productId = parts[2];
			var purchaseToken = billingService.PurchaseToken;

			var subscriptionPurchase = _androidPublisherService.GetSubscriptionPurchase(package, productId, purchaseToken);

			var now = DateTime.UtcNow;
			if (subscriptionPurchase.ExpiryTimeMillis == null)
			{
				NotifySupport(string.Format("Purchase for schedulerqueue_id = {0} has no expiry time", queueId));
				billingService.ScheduledTask.NEAREST_TIME = now.AddDays(1);
				return SchedulerExecutionResult.Success;
			}

			var expiryTime = TimeHelper.GetDateTimeUTC((int)(subscriptionPurchase.ExpiryTimeMillis.Value / 1000));
			if (subscriptionPurchase.AutoRenewing ?? false)
			{
				billingService.EndDate = null;
			}
			else
			{
				if (billingService.EndDate == null || billingService.EndDate.Value < expiryTime)
					billingService.EndDate = expiryTime;

				if (expiryTime <= now)
					return SchedulerExecutionResult.Cancel;
			}

			if (!billingService.Activated)
				billingService.Activated = true;

			billingService.ScheduledTask.NEAREST_TIME = expiryTime;
			return SchedulerExecutionResult.Success;
		}
	}
}
