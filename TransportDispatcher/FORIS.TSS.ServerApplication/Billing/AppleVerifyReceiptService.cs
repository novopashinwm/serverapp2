﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Billing
{
	class AppleVerifyReceiptService : IAppleVerifyReceiptService
	{
		private class AppleServerParameters
		{
			public string Url;
			public string SharedSecret;
		}

		private AppleServerParameters GetAppleServerParameters(bool sandbox)
		{
			if (sandbox)
			{
				return new AppleServerParameters
				{
					Url = "https://sandbox.itunes.apple.com/verifyReceipt",
					SharedSecret = Server.Instance().GetConstant(Constant.AppleSandboxSharedSecret)
				};
			}

			return new AppleServerParameters
			{
				Url = "https://buy.itunes.apple.com/verifyReceipt",
				SharedSecret = Server.Instance().GetConstant(Constant.AppleSharedSecret)
			};
		}

		public ApplePurchase VerifyReceipt(bool sandbox, string purchaseBase64)
		{
			var serverParameters = GetAppleServerParameters(sandbox);

			var requestJson = JsonHelper.SerializeObjectToJson(
				new Dictionary<string, string>
				{
					{"receipt-data", purchaseBase64},
					{"password", serverParameters.SharedSecret}
				});
			var content = new StringContent(
				requestJson,
				Encoding.ASCII,
				"application/json"
				);

			string resultContent;
			try
			{
				using (var httpClient = new HttpClient())
				{
					var responseMessage = httpClient.PostAsync(serverParameters.Url, content).Result;
					resultContent = responseMessage.Content.ReadAsStringAsync().Result;
				}
			}
			catch (AggregateException e)
			{
				if (e.InnerExceptions.Any(i => i is HttpRequestException))
					return null;
				throw;
			}
			return JsonHelper.DeserializeObjectFromJson<ApplePurchase>(resultContent);
		}
	}
}