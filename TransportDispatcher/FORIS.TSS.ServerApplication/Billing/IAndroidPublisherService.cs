﻿namespace FORIS.TSS.ServerApplication.Billing
{
	public interface IAndroidPublisherService
	{
		SubscriptionPurchase GetSubscriptionPurchase(string package, string product, string purchaseToken);
	}

	public class SubscriptionPurchase
	{
		public long? ExpiryTimeMillis;
		public bool? AutoRenewing;
	}
}