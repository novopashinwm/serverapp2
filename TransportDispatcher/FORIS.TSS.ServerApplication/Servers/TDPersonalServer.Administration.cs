﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using Interfaces.Web;
using Dto = FORIS.TSS.BusinessLogic.DTO;
using Res = FORIS.TSS.BusinessLogic.ResultCodes;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		private SystemRight[] ZoneRights
		{
			get
			{
				return ComputedDepartmentType == DepartmentType.Corporate
					? UserRole.CorporateZoneRights
					: UserRole.PhysicalZoneRights;
			}
		}
		public void SaveOperatorRights(Rht::Operator oper)
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new InvalidOperationException("Department is not assigned");

			// Проверяем права текущего оператора на изменение оператора
			var departments = ((IWebPersonalServer)this).GetAvailableDepartments();
			if (departments == null || departments.FindAll(department => department.id == departmentId.Value).Count == 0)
				throw new SecurityException("No admin rights for department");

			using (var entites = new Entities())
			{
				var currentRights =
				(
					from ro in entites.RIGHT_OPERATOR
					where ro.OPERATOR_ID == oper.id
					select ro
				).ToList();

				foreach (var right in oper.rights)
				{
					// Данное прав можно менять только супер-админам, т.к. только он может назначать пользователя партнером
					if (right.right == SystemRight.ServiceManagement && !IsSuperAdministrator)
						continue;
					var toRemove = currentRights.FirstOrDefault(x => x.RIGHT_ID == right.right_id && !right.allow.HasValue);
					if (toRemove != null)
					{
						entites.RIGHT_OPERATOR.DeleteObject(toRemove);
						continue;
					}

					var changeExisting =
						currentRights.FirstOrDefault(x => x.RIGHT_ID == right.right_id && right.allow.HasValue);

					if (changeExisting != null)
					{
						changeExisting.ALLOWED = right.allow ?? false;
						continue;
					}

					if (!currentRights.Any(x => x.RIGHT_ID == right.right_id) && right.allow.HasValue)
						entites.RIGHT_OPERATOR.AddObject(new RIGHT_OPERATOR
						{
							RIGHT_ID    = right.right_id,
							OPERATOR_ID = oper.id,
							ALLOWED     = right.allow.Value
						});
				}

				entites.SaveChanges();
			}
		}
		public List<Rht::Operator> WhoCanSeeVehicle(int vehicleId)
		{
			const int rightId = (int) SystemRight.VehicleAccess;
			using (var entities = new Entities())
			{
				if (!entities.v_operator_vehicle_right
					.Any(ovr =>
						ovr.operator_id == OperatorId                              &&
						ovr.right_id    == (int)SystemRight.SecurityAdministration &&
						ovr.vehicle_id  == vehicleId))
					throw new SecurityException("No administrative rights for operator " + OperatorId + " on vehicle " + vehicleId);

				//Представление v_operator_vehicle_right не используется, чтобы не показывать пользователям,
				//что их объекты наблюдения доступны из административных аккаунтов
				var operators = entities.v_operator_vehicle_right
					.Where(ovr => ovr.vehicle_id == vehicleId &&
						ovr.operator_id != OperatorId &&
						ovr.right_id == rightId &&
						(
							ovr.priority == (int)EffectiveRightPriority.Direct
							||
							ovr.priority == (int)EffectiveRightPriority.Group
						))
					.Select(ovr => ovr.OPERATOR)
					.Select(o => new
					{
						o.OPERATOR_ID,
						o.LOGIN,
						o.NAME,
						Emails = o.Contacts.Where(e => e.Confirmed && e.Contact.Type == (int)ContactType.Email),
						Phones = o.Contacts.Where(p => p.Confirmed && p.Contact.Type == (int)ContactType.Phone)
					})
					.ToList();

				return operators.Select(
					o => new Rht::Operator
					{
						id     = o.OPERATOR_ID,
						login  = o.LOGIN,
						name   = o.NAME,
						Emails = o.Emails.Select(e => new Dto::Contact { Value = e.Value }).ToArray(),
						Phones = o.Phones.Select(e => new Dto::Contact { Value = e.Value }).ToArray(),
					}).ToList();
			}
		}
		public List<Rht::Operator> WhoCanSeeVehicleGroup(int vehicleGroupId)
		{
			const int rightId = (int)SystemRight.VehicleAccess;
			using (var entities = new Entities())
			{
				//TODO: оказывается, при создании группы административные права на неё не назначаются
				//TODO: нужно назначать, но запрещать добавлять в такую группу объекты наблюдения, на которые у пользователя нет административных прав,
				//TODO: но тогда в группу можно будет добавлять только свои объекты наблюдения
				//TODO: та же проблема с правом на редактирование - можно получить право на редактирование, добавив объект в свою группу, на которые есть права на редактирование
				//TODO: можно сделать так - наследовать права, только если department_id группы и объекта совпадают
				entities.CheckAdminOfVehicleGroup(OperatorId, vehicleGroupId);

				//Представление v_operator_vehicle_right не используется, чтобы не показывать пользователям,
				//что их объекты наблюдения доступны из административных аккаунтов
				var operators =
					entities
						.v_operator_vehicle_groups_right
						.Where(ovr => ovr.vehiclegroup_id == vehicleGroupId &&
							ovr.operator_id != OperatorId &&
							ovr.right_id == rightId &&
							ovr.priority == (int)EffectiveRightPriority.Direct)
						.Select(ovr => ovr.OPERATOR)
						.Select(o => new
						{
							o.OPERATOR_ID,
							o.LOGIN,
							o.NAME,
							Emails = o.Contacts.Where(e => e.Confirmed && e.Contact.Type == (int)ContactType.Email),
							Phones = o.Contacts.Where(p => p.Confirmed && p.Contact.Type == (int)ContactType.Phone)
						}).ToList();

				return operators.Select(
					o => new Rht::Operator
					{
						id = o.OPERATOR_ID,
						login = o.LOGIN,
						name = o.NAME,
						Emails = o.Emails.Select(e => new Dto::Contact { Value = e.Value }).ToArray(),
						Phones = o.Phones.Select(e => new Dto::Contact { Value = e.Value }).ToArray(),
					}).ToList();
			}

		}
		private IQueryable<MESSAGE> ToContact(int contactId, IQueryable<MESSAGE> messages)
		{
			return messages
				.Where(m => m.Owner_Operator_ID == OperatorId &&
							m.Contacts.Any(mc => mc.Type == (int)MessageContactType.Destination &&
												 mc.Contact_ID == contactId));
		}
		/// <summary> Сообщения от данного пользователя другому с id = friendOperatorId </summary>
		/// <param name="friendOperatorId"> Идентификатор пользователя - получателя сообщения </param>
		/// <param name="messages"> Запрос на получение сообщений </param>
		/// <returns> Запрос на получение сообщений </returns>
		private IQueryable<MESSAGE> ToFriend(int friendOperatorId, IQueryable<MESSAGE> messages)
		{
			var friendOperatorIdString = friendOperatorId.ToString(CultureInfo.InvariantCulture);
			return messages
				.Where(m => m.Owner_Operator_ID == OperatorId &&
							m.Contacts.Any(mc => mc.Type == (int) MessageContactType.Destination &&
												 mc.Contact.Type == (int) ContactType.Operator &&
												 mc.Contact.Value == friendOperatorIdString));
		}
		private IQueryable<MESSAGE> OfType(string messageTemplateName, IQueryable<MESSAGE> messages)
		{
			return messages.Where(m => m.MESSAGE_TEMPLATE.NAME == messageTemplateName);
		}
		private IQueryable<MESSAGE> AboutVehicle(int vehicleId, IQueryable<MESSAGE> messages)
		{
			var vehicleIdString = vehicleId.ToString(CultureInfo.InvariantCulture);
			return
				messages.Where(
					m => m.MESSAGE_FIELD.Any(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.VehicleID &&
												  f.CONTENT == vehicleIdString));
		}
		private IQueryable<MESSAGE> GetViewVehicleProposal(Entities entities, int friendOperatorId, int vehicleId)
		{
			return ToFriend(friendOperatorId,
				OfType(Msg::MessageTemplate.ProposeViewVehicle,
					AboutVehicle(vehicleId, entities.MESSAGE)));
		}
		public void ProposeViewVehicle(int friendOperatorId, int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!IsVehicleAllowed(entities, vehicleId, SystemRight.SecurityAdministration))
					throw new SecurityException("Operator " + OperatorId + " is not security administrator for vehicle " + vehicleId);

				//Создать сообщение типа "Приглашение", адресованное пользователю friendOperatorId (если такого сообщения ещё не существует)
				//Варианты реализации:
				//1. Сразу отправлять на все известные контакты: в чат веб-интерфейса, по SMS, по email, в мобильный клиент,
				//т.е. единовременно будет создано столько сообщений, сколько известно подтверждённых контактов пользователя
				//2. Считать сообщение "родительским", а для доставки на конкретные контактные адреса использовать дочерние сообщения
				//Реакцию пользователя на дочернее сообщение отмечать в родительском сообщении.

				var vehicleIdString = vehicleId.ToString(CultureInfo.InvariantCulture);

				var proposalAlreadyExists = GetViewVehicleProposal(entities, friendOperatorId, vehicleId).Any();

				if (proposalAlreadyExists)
					return;

				var message = new MESSAGE();
				entities.MESSAGE.AddObject(message);
				message.Owner_Operator_ID = OperatorId;
				message.MESSAGE_TEMPLATE = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.ProposeViewVehicle);
				message.DestinationType_ID = (int) MessageDestinationType.Operator;
				message.Contacts.Add(new Message_Contact
				{
					Type    = (int)MessageContactType.Destination,
					Contact = entities.GetContact(ContactType.Operator, friendOperatorId.ToString(CultureInfo.InvariantCulture))
				});
				message.TIME = DateTime.UtcNow;
				var vehicleIdMessageField = new MESSAGE_FIELD
				{
					MESSAGE = message,
					MESSAGE_TEMPLATE_FIELD = entities.MESSAGE_TEMPLATE_FIELD.First(mtf => mtf.NAME == MESSAGE_TEMPLATE_FIELD.VehicleID),

				};
				vehicleIdMessageField.SetContent(vehicleIdString);
				entities.MESSAGE_FIELD.AddObject(vehicleIdMessageField);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void RecallViewVehicleProposation(int friendOperatorId, int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!IsVehicleAllowed(entities, vehicleId, SystemRight.SecurityAdministration))
					throw new SecurityException("Operator " + OperatorId + " is not security administrator for vehicle " +
												vehicleId);

				//Удалить сообщение типа "Приглашение", адресованное пользователю friendOperatorId
				var messages = GetViewVehicleProposal(entities, friendOperatorId, vehicleId).ToList();

				foreach (var message in messages)
				{
					entities.DeleteMessage(message);
				}

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DisallowVehicleAccess(int friendOperatorId, int vehicleId)
		{
			if (friendOperatorId == OperatorId)
				return;

			using (var entities = new Entities())
			{
				if (!IsVehicleAllowed(entities, vehicleId, SystemRight.SecurityAdministration))
					throw new SecurityException("Operator " + OperatorId + " is not security administrator for vehicle " + vehicleId);


				var rights =
					entities.v_operator_vehicle_right.Where(
						v => v.operator_id == friendOperatorId && v.vehicle_id == vehicleId)
							.Select(r => (SystemRight) r.right_id).ToArray();

				entities.Disallow(entities.GetOperator(friendOperatorId), entities.GetVehicle(vehicleId), rights);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public List<Rht::Operator> FindUsers(string contact)
		{
			if (string.IsNullOrWhiteSpace(contact))
				throw new ArgumentOutOfRangeException("contact", contact, @"Value cannot be null or whitespace");

			using (var entities = new Entities())
			{
				var friendsGroupQuery = entities.FriendsGroupQuery(OperatorId);

				var phone = ContactHelper.GetNormalizedPhone(contact);

				var operatorsQuery = entities.OPERATOR
					.Where(o =>
						// Самого себя не ищем
						o.OPERATOR_ID != OperatorId &&
						//Успешный поиск по логину или одному из подтверждённых контактов
						(
							o.LOGIN == contact
							||
							o.Contacts.Any(oc =>
								oc.Confirmed &&
								(
									oc.Contact.Value == phone &&
									oc.Contact.Type  == (int) ContactType.Phone
									||
									oc.Contact.Value == contact &&
									oc.Contact.Type  == (int) ContactType.Email
								))
							||
							o.Asid.Contact.DemaskedPhone.Value == phone &&
							o.Asid.Contact.DemaskedPhone.Type  == (int) ContactType.Phone
						));

				if (ComputedDepartmentId != null && GetDepartmentRights().Contains(SystemRight.SecurityAdministration))
				{
					var departmentOperatorsQuery = entities.GetDepartmentOperatorsQuery(ComputedDepartmentId.Value);
					//Не ищем своих пользователей
					operatorsQuery = operatorsQuery.Where(o => !departmentOperatorsQuery.Contains(o));
				}

				var operators = operatorsQuery
					.GroupBy(o => o.OPERATOR_ID)
					.Select(g => g.FirstOrDefault()) //Distinct почему-то не работает
					.Select(o => new
					{
						o.OPERATOR_ID,
						o.LOGIN,
						o.NAME,
						Emails = o.Contacts
							.Where(oc =>
								oc.Confirmed &&
								oc.Contact.Type  == (int)ContactType.Email &&
								oc.Contact.Value == contact)
							.Select(oc => oc.Value),
						Phones = o.Contacts
							.Where(oc =>
								oc.Confirmed &&
								oc.Contact.Type  == (int)ContactType.Phone &&
								oc.Contact.Value == phone)
							.Select(oc => oc.Value)
							.Union(entities.Contact
								.Where(pc =>
									pc.Type == (int)ContactType.Phone &&
									pc.Value == phone &&
									pc.MaskedAsids.Any(ac =>
										ac.Asid.Any(a => a.OPERATOR.OPERATOR_ID == o.OPERATOR_ID)))
								.Select(c => c.Value))
							.Distinct(),
						IsFriend = friendsGroupQuery.Any(og => og.Members.Contains(o))
					})
					.ToList();

				return operators
					.Select(o =>
						new Rht::Operator
						{
							id     = o.OPERATOR_ID,
							login  = string.Equals(o.LOGIN, contact, StringComparison.OrdinalIgnoreCase) ? o.LOGIN : null,
							name   = o.NAME,
							Emails = o.Emails
								.Select(contactValue =>
									new Dto::Contact { Type = ContactType.Email, Value = contactValue })
								.ToArray(),
							Phones = o.Phones
								.Select(contactValue =>
									new Dto::Contact { Type = ContactType.Phone, Value = contactValue })
								.ToArray(),
							IsFriend = o.IsFriend
						})
					.ToList();
			}
		}
		public Res::ProposeFriendshipResult ProposeFriendship(string msisdn)
		{
			return Server.Instance().ProposeFriendship(OperatorId, msisdn, SaveContext.Session(SessionId));
		}
		public List<Rht::Operator> GetFriends()
		{
			return Server.Instance()
				.GetFriends(OperatorId, FilterByDepartment ? ComputedDepartmentId : null)
				.ToList();
		}
		public List<Department> GetPartners()
		{
			return Enumerable.Empty<Department>().ToList();
		}
		public void AddFriend(int friendId)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				if (entities.AddFriend(OperatorId, friendId))
					entities.SaveChangesBySession(SessionId);
			}
		}
		public void AddPartner(int departmentId)
		{
			throw new NotImplementedException();
		}
		public void DeleteFriend(int friendId)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				if (ComputedDepartmentId != null && entities.GetDepartmentOperatorsQuery(ComputedDepartmentId.Value)
						.Any(o => o.OPERATOR_ID == friendId))
					throw new ArgumentOutOfRangeException(nameof(friendId), friendId, @"Operator is not friend");

				var ormFriend = entities.GetOperator(friendId);
				// Удаляем доступы к объектам наблюдения
				foreach (var vehicle in entities.VEHICLE
					.Where(x =>
						x.Accesses.Any(a =>
							a.operator_id == OperatorId &&
							a.right_id    == (int)SystemRight.SecurityAdministration
						)
						&&
						x.Accesses.Any(a =>
							a.operator_id == friendId &&
							a.priority    == (int)EffectiveRightPriority.Direct
						)
					)
				)
				{
					entities.DeleteAccess(ormFriend, vehicle);
				}
				// Удаляем права на группы объектов наблюдения
				foreach (var vehicleGroup in entities.VEHICLEGROUP
					.Where(x =>
						x.Accesses.Any(a =>
							a.operator_id == OperatorId &&
							a.right_id    == (int)SystemRight.GroupAdministration
						)
						&&
						x.Accesses.Any(a =>
							a.operator_id == friendId &&
							a.priority    == (int)EffectiveRightPriority.Group
						)
					)
				)
				{
					entities.DeleteAccess(ormFriend, vehicleGroup);
				}
				// Удаляем права на геозоны
				foreach (var zone in entities.GEO_ZONE
					.Where(x =>
						x.Accesses.Any(a =>
							a.operator_id == OperatorId &&
							a.right_id    == (int)SystemRight.SecurityAdministration
						)
						&&
						x.Accesses.Any(a =>
							a.operator_id == friendId &&
							a.priority    == (int)EffectiveRightPriority.Direct
						)
					)
				)
				{
					entities.DeleteAccess(ormFriend, zone);
				}
				// Удаляем права на группы геозон
				foreach (var zoneGroup in entities.ZONEGROUP
					.Where(x =>
						x.Accesses.Any(a =>
							a.operator_id == OperatorId &&
							a.right_id    == (int)SystemRight.GroupAdministration
						)
						&&
						x.Accesses.Any(a =>
							a.operator_id == friendId &&
							a.priority    == (int)EffectiveRightPriority.Group
						)
					)
				)
				{
					entities.DeleteAccess(ormFriend, zoneGroup);
				}
				// Удаляем пользователя из группы друзей
				var groupOfFriends = entities.FriendsGroupQuery(OperatorId).FirstOrDefault();
				if (groupOfFriends?.Members?.Contains(ormFriend) ?? false)
					groupOfFriends?.Members?.Remove(ormFriend);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DeletePartner(int departmentId)
		{
			throw new NotImplementedException();
		}
		public SystemRight[] GetAccessibleRightsOnVehicle()
		{
			return Server.Instance()
				.VehicleSystemRightsReplacer
				.GetSystemRights(ComputedDepartmentType, IdType.Vehicle)
				// Право SystemRight.SecurityAdministration, можно выдать только для супера или партнера
				.Where(r => SystemRight.SecurityAdministration != r || IsSuperAdministrator || (IsSalesManager && !IsDefaultDepartment()))
				.ToArray();
		}
		public SystemRight[] GetReplacedAccessOnVehicle()
		{
			return Server.Instance()
				.VehicleSystemRightsReplacer
				.GetReplacedRights(ComputedDepartmentType, IdType.Vehicle)
				// Право SystemRight.SecurityAdministration, можно выдать только для супера или партнера
				.Where(r => SystemRight.SecurityAdministration != r || IsSuperAdministrator || (IsSalesManager && !IsDefaultDepartment()))
				.ToArray();
		}
		public SystemRight[] GetAccessibleRightsOnZone()
		{
			return ZoneRights;
		}
		public List<Rht::Operator> GetAccessesToVehicle(int vehicleId)
		{
			return Server.Instance()
				.GetAccessesToVehicle(OperatorId, Department, vehicleId)
				.ToList();
		}
		public List<Rht::Operator> GetAccessesToVehicleGroup(int vehicleGroupId)
		{
			using (var entities = new Entities())
			{
				entities.CheckAdminOfVehicleGroup(OperatorId, vehicleGroupId);

				var rightIds = GetAccessibleRightsOnVehicle()
					.Select(r => (int)r)
					.ToArray();

				var result = entities.OPERATOR_VEHICLEGROUP
					.Where(x => x.VEHICLEGROUP_ID == vehicleGroupId && rightIds.Contains(x.RIGHT_ID))
					.ToLookup(x => x.OPERATOR.OPERATOR_ID)
					.Select(x => new Rht::Operator
					{
						id     = x.Key,
						groups = new[]
						{
							new Rht::VehicleGroup
							{
								rights = x
									.Select(r => new Rht::Right
									{
										allow    = r.ALLOWED,
										right_id = r.RIGHT_ID,
										right    = (SystemRight)r.RIGHT_ID
									})
									.ToArray()
							}
						}
					})
					.ToList();
				return result;
			}
		}
		public void IgnoreVehicle(int vehicleId, bool ignored)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				var @operator = entities.GetOperator(OperatorId);
				var vehicle   = entities.GetVehicle(vehicleId);

				// Отменяем эту проверку, т.к. разрешаем прятать свои объекты видимо для физ. лиц
				//if (entities.v_operator_vehicle_right.Any(
				//	ovr => ovr.operator_id == OperatorID &&
				//		   ovr.vehicle_id  == vehicleId &&
				//		   ovr.right_id    == (int) SystemRight.SecurityAdministration))
				//	throw new ArgumentOutOfRangeException("vehicleId", vehicleId, @"Own vehicle may not be ignored or unignored");

				// Отменяем эту проверку, т.к. разрешаем прятать свои объекты видимо для корп. лиц
				//if (ignored)
				//{
				//	var vehicleDepartmentId = entities.VEHICLE
				//		.Where(v =>  v.VEHICLE_ID == vehicleId)
				//		.Select(v => v.Department_ID)
				//		.FirstOrDefault();
				//	if (vehicleDepartmentId != null)
				//	{
				//		if (entities.GetDepartmentOperatorsQuery(vehicleDepartmentId.Value).Any(o => o.OPERATOR_ID == OperatorID))
				//			throw new ArgumentOutOfRangeException("vehicleId", vehicleId, @"Vehicles from own department may not be ignored");
				//	}
				//}

				var systemRight = entities.GetSystemRight(SystemRight.Ignore);
				if (ignored)
					entities.SetDirectAccess(@operator, vehicle, systemRight, true);
				else
					entities.DeleteAccess(@operator, vehicle, systemRight);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void IgnoreVehicleGroup(int vehicleGroupId, bool ignored)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				if (entities.v_operator_vehicle_groups_right.Any(
					ovr => ovr.operator_id     == OperatorId &&
						   ovr.vehiclegroup_id == vehicleGroupId &&
						   ovr.right_id        == (int)SystemRight.SecurityAdministration))
					throw new ArgumentOutOfRangeException("vehicleGroupId", vehicleGroupId, @"Own vehicle group may not be ignored");

				if (ignored)
				{
					var vehicleGroupDepartmentId = entities.VEHICLEGROUP
						.Where(vg => vg.VEHICLEGROUP_ID == vehicleGroupId)
						.Select(vg => vg.Department_ID)
						.FirstOrDefault();
					if (vehicleGroupDepartmentId != null)
					{
						if (entities.GetDepartmentOperatorsQuery(vehicleGroupDepartmentId.Value).Any(o => o.OPERATOR_ID == OperatorId))
							throw new ArgumentOutOfRangeException("vehicleGroupId", vehicleGroupId, @"Vehicle groups from own department may not be ignored");
					}
				}

				var @operator    = entities.GetOperator(OperatorId);
				var vehicleGroup = entities.GetVehicleGroup(vehicleGroupId);
				var systemRight  = entities.GetSystemRight(SystemRight.Ignore);
				if (ignored)
					entities.SetDirectAccess(@operator, vehicleGroup, systemRight, true);
				else
					entities.DeleteAccess(@operator, vehicleGroup, systemRight);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void IgnoreZone(int zoneId, bool ignored)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				if (entities.v_operator_zone_right.Any(
					ovr => ovr.operator_id == OperatorId &&
						   ovr.ZONE_ID == zoneId &&
						   ovr.right_id == (int)SystemRight.SecurityAdministration))
					throw new ArgumentOutOfRangeException(nameof(zoneId), zoneId, @"Own zone may not be ignored");

				if (ignored)
				{
					var departmentId =
						entities.GEO_ZONE
								.Where(v => v.ZONE_ID == zoneId)
								.Select(v => v.Department_ID)
								.FirstOrDefault();
					if (departmentId != null)
					{
						if (entities.GetDepartmentOperatorsQuery(departmentId.Value).Any(o => o.OPERATOR_ID == OperatorId))
							throw new ArgumentOutOfRangeException(nameof(zoneId), zoneId, @"Zone from own department may not be ignored");
					}
				}

				var @operator = entities.GetOperator(OperatorId);
				var zone = entities.GetZone(OperatorId, zoneId).First();
				var systemRight = entities.GetSystemRight(SystemRight.Ignore);
				if (ignored)
					entities.SetDirectAccess(@operator, zone, systemRight, true);
				else
					entities.DeleteAccess(@operator, zone, systemRight);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void IgnoreZoneGroup(int zoneGroupId, bool ignored)
		{
			if (!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("Not allowed");

			using (var entities = new Entities())
			{
				if (entities.v_operator_zone_groups_right.Any(
					ovr => ovr.operator_id == OperatorId &&
						   ovr.zonegroup_id == zoneGroupId &&
						   ovr.right_id == (int)SystemRight.SecurityAdministration))
					throw new ArgumentOutOfRangeException(nameof(zoneGroupId), zoneGroupId, @"Own zone group may not be ignored");

				if (ignored)
				{
					var departmentId =
						entities.ZONEGROUP
								.Where(zg => zg.ZONEGROUP_ID == zoneGroupId)
								.Select(v => v.Department_ID)
								.FirstOrDefault();
					if (departmentId != null)
					{
						if (entities.GetDepartmentOperatorsQuery(departmentId.Value).Any(o => o.OPERATOR_ID == OperatorId))
							throw new ArgumentOutOfRangeException(nameof(zoneGroupId), zoneGroupId, @"Zone group from own department may not be ignored");
					}
				}

				var @operator = entities.GetOperator(OperatorId);
				var @group = entities.GetZoneGroup(OperatorId, zoneGroupId).First();
				var systemRight = entities.GetSystemRight(SystemRight.Ignore);
				if (ignored)
					entities.SetDirectAccess(@operator, @group, systemRight, true);
				else
					entities.DeleteAccess(@operator, @group, systemRight);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public Res::SetAccessToVehicleResult SetReplacedAccessToVehicle(int operatorId, int vehicleId, SystemRight right, bool allowed)
		{
			if (operatorId == OperatorId)
				throw new ArgumentException($"Managing of own rights is not allowed '{SessionInfo?.OperatorInfo?.Login}({OperatorId})'");

			return Server.Instance().SetAccessToVehicle(
				adminOperatorId: OperatorId, // Текущий оператор, который выдает право
				operatorId:      operatorId, // Оператор которому выдаем право
				vehicleId:       vehicleId,  // Объект наблюдения
				right:           right,      // Право
				allowed:         allowed,    // Состояние права установлено или снято
				saveContext:     SaveContext.Session(SessionId));
		}
		public void DelReplacedAccessToVehicle(int operatorId, int vehicleId, SystemRight? right)
		{
			if (operatorId == OperatorId)
				throw new ArgumentException($"Managing of own rights is not allowed '{SessionInfo?.OperatorInfo?.Login}({OperatorId})'");

			var departmentType = ComputedDepartmentType;

			var rights = Server.Instance().GetRemovableRightsOnVehicle(operatorId, vehicleId);
			if (right.HasValue)
			{
				rights = Server.Instance()
					?.VehicleSystemRightsReplacer
					?.GetRightReplacer(departmentType, IdType.Vehicle, right.Value)
					?.Rights;
				if (0 == (rights?.Length ?? 0))
					throw new ArgumentOutOfRangeException(nameof(right), right, @"Right does not exists");
			}
			Server.Instance().DelAccessToVehicle(
				adminOperatorId: OperatorId,
				operatorId:      operatorId,
				vehicleId:       vehicleId,
				rights:          rights,
				saveContext:     SaveContext.Session(SessionId));
		}
		private int? GetOperatorIdByConfirmedMsisdn(string msisdn)
		{
			using (var entities = new Entities())
				return entities.GetOperatorIdByConfirmedMsisdn(msisdn);
		}
		public Res::SetAccessToVehicleResult SetReplacedAccessToVehicle(string msisdn, int vehicleId, SystemRight right, bool allowed)
		{
			// Нормализуем входной номер
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);
			// Ищем идентификатор оператора по номеру нормализованному телефона
			var msisdnOperatorId = GetOperatorIdByConfirmedMsisdn(msisdn);
			if (msisdnOperatorId == null)
				throw new ArgumentException($"Operator not found by phone '{msisdn}'");
			return SetReplacedAccessToVehicle(
				operatorId: msisdnOperatorId.Value,
				vehicleId:  vehicleId,
				right:      right,
				allowed:    allowed);
		}
		public void DelReplacedAccessToVehicle(string msisdn, int vehicleId, SystemRight? right)
		{
			// Нормализуем входной номер
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);
			// Ищем идентификатор оператора по номеру нормализованному телефона
			var msisdnOperatorId = GetOperatorIdByConfirmedMsisdn(msisdn);
			if (msisdnOperatorId == null)
				return;
			DelReplacedAccessToVehicle(
				operatorId: msisdnOperatorId.Value,
				vehicleId:  vehicleId,
				right:      right);
		}
		private void SetAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight[] rights, bool allowed)
		{
			var vehicleRights = Server.Instance().VehicleSystemRightsReplacer.GetSystemRights(ComputedDepartmentType, IdType.Vehicle).ToArray();
			if (!rights.All(vehicleRights.Contains))
			{
				var failedRights = string.Join(", ", rights.Where(right => !vehicleRights.Contains(right)).Select(item => item.ToString()));
				throw new ArgumentOutOfRangeException("right", failedRights, @"Right cannot be administrated here");
			}

			using (var entities = new Entities())
			{
				entities.CheckAdminOfVehicleGroup(OperatorId, vehicleGroupId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException(nameof(operatorId), operatorId, @"Operator not found by id");

				var ormVehicleGroup = entities.GetVehicleGroup(vehicleGroupId);

				if (allowed)
					entities.Allow(ormOperator, ormVehicleGroup, rights);
				else
					entities.Disallow(ormOperator, ormVehicleGroup, rights);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void SetReplacedAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight replacedRight, bool allowed)
		{
			var departmentType = ComputedDepartmentType;
			var replacer = Server.Instance().VehicleSystemRightsReplacer.GetRightReplacer(departmentType, IdType.VehicleGroup, replacedRight);
			var rights = replacer != null ? replacer.Rights : null;
			if (rights == null)
				throw new ArgumentOutOfRangeException("replacedRight", replacedRight, "Right don't exists");
			SetAccessToVehicleGroup(operatorId, vehicleGroupId, rights, allowed);
		}
		private void DeleteAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight[] rights)
		{
			var vehicleRights = Server.Instance().VehicleSystemRightsReplacer.GetSystemRights(ComputedDepartmentType, IdType.Vehicle).ToArray();
			if (!rights.All(vehicleRights.Contains))
			{
				var failedRights = string.Join(", ", rights.Where(right => !vehicleRights.Contains(right)).Select(item => item.ToString()));
				throw new ArgumentOutOfRangeException("right", failedRights, @"Right cannot be administrated here");
			}

			using (var entities = new Entities())
			{
				entities.CheckAdminOfVehicleGroup(OperatorId, vehicleGroupId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException("operatorId", operatorId, @"Operator not found by id");

				var intRights = rights.Select(item => (int)item).ToArray();
				var access = entities.OPERATOR_VEHICLEGROUP.FirstOrDefault(
					x => x.OPERATOR_ID == operatorId &&
						 x.VEHICLEGROUP_ID == vehicleGroupId &&
						 intRights.Contains(x.RIGHT_ID));

				if (access == null)
					return;

				entities.OPERATOR_VEHICLEGROUP.DeleteObject(access);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DelReplacedAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight replacedRight)
		{
			var departmentType = ComputedDepartmentType;
			var replacer = Server.Instance().VehicleSystemRightsReplacer.GetRightReplacer(departmentType, IdType.VehicleGroup, replacedRight);
			var rights = replacer != null ? replacer.Rights : null;
			if (rights == null)
				throw new ArgumentOutOfRangeException("replacedRight", replacedRight, "Right don't exists");
			DeleteAccessToVehicleGroup(operatorId, vehicleGroupId, rights);
		}
		public void SetAccessToZone(int operatorId, int zoneId, SystemRight right, bool allowed)
		{
			if (!ZoneRights.Contains(right))
				throw new ArgumentOutOfRangeException("right", right, @"Right cannot be administrated here");

			using (var entities = new Entities())
			{
				CheckAdminOfZone(entities, zoneId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException("operatorId", operatorId, @"Operator not found by id");

				var ormGeoZone = entities.GetZone(zoneId).First();

				if (allowed)
					entities.Allow(ormOperator, ormGeoZone, right);
				else
					entities.Disallow(ormOperator, ormGeoZone, right);

				if (allowed)
					Server.Instance().OnZoneAccessGranted(OperatorId, zoneId);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DeleteAccessToZone(int operatorId, int zoneId, SystemRight right)
		{
			if (!ZoneRights.Contains(right))
				throw new ArgumentOutOfRangeException("right", right, @"Right cannot be administrated here");

			using (var entities = new Entities())
			{
				CheckAdminOfZone(entities, zoneId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException("operatorId", operatorId, @"Operator not found by id");

				var access = entities.OPERATOR_ZONE.FirstOrDefault(
					x => x.OPERATOR_ID == operatorId &&
						 x.ZONE_ID == zoneId &&
						 x.RIGHT_ID == (int)right);

				if (access == null)
					return;

				entities.OPERATOR_ZONE.DeleteObject(access);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void SetAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right, bool allowed)
		{
			if (!ZoneRights.Contains(right))
				throw new ArgumentOutOfRangeException("right", right, @"Right cannot be administrated here");

			using (var entities = new Entities())
			{
				CheckAdminOfZoneGroup(entities, zoneGroupId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException("operatorId", operatorId, @"Operator not found by id");

				var @group = entities.GetZoneGroup(zoneGroupId).First();

				if (allowed)
					entities.Allow(ormOperator, @group, right);
				else
					entities.Disallow(ormOperator, @group, right);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DeleteAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right)
		{
			if (!ZoneRights.Contains(right))
				throw new ArgumentOutOfRangeException("right", right, @"Right cannot be administrated here");

			using (var entities = new Entities())
			{
				CheckAdminOfZoneGroup(entities, zoneGroupId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException(nameof(operatorId), operatorId, @"Operator not found by id");

				var access = entities.OPERATOR_ZONEGROUP.FirstOrDefault(
					x => x.OPERATOR_ID == operatorId &&
						 x.ZONEGROUP_ID == zoneGroupId &&
						 x.RIGHT_ID == (int)right);

				if (access == null)
					return;

				entities.OPERATOR_ZONEGROUP.DeleteObject(access);

				entities.SaveChangesBySession(SessionId);
			}
		}
		private bool CheckAccessToZoneGroup(Entities entities, int zoneGroupId)
		{
			return entities.v_operator_zone_groups_right
				.Any(ovr =>
					ovr.operator_id  == OperatorId &&
					ovr.zonegroup_id == zoneGroupId &&
					 ovr.right_id    == (int)SystemRight.ZoneAccess);
		}
		private void CheckAdminOfZoneGroup(Entities entities, int zoneGroupId)
		{
			if (!entities.v_operator_zone_groups_right
				.Any(ovr =>
					ovr.operator_id  == OperatorId &&
					ovr.zonegroup_id == zoneGroupId &&
					ovr.right_id     == (int)SystemRight.GroupAdministration))
				throw new SecurityException("Disallowed: current operator " + OperatorId + " is not admin of zone group " + zoneGroupId);
		}
		private void CheckAdminOfZone(Entities entities, int zoneId)
		{
			if (!entities.v_operator_zone_right
				.Any(ovr =>
					ovr.operator_id == OperatorId &&
					ovr.ZONE_ID     == zoneId &&
					ovr.right_id    == (int)SystemRight.SecurityAdministration))
				throw new SecurityException("Disallowed: current operator " + OperatorId + " is not admin of zone " + zoneId);
		}
		/// <summary> Сохранение прав на группы машин и машины по оператору </summary>
		/// <param name="newOperator"> информации по оператору </param>
		/// <returns></returns>
		public void ChangeOperatorVehicleRights(Rht::Operator newOperator)
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new InvalidOperationException("Department is not assigned");

			//Проверяем права текущего оператора на изменение оператора
			var departments = ((IWebPersonalServer)this).GetAvailableDepartments();
			if (departments == null || departments.FindAll(department => department.id == departmentId.Value).Count == 0)
				throw new SecurityException("No admin rights for department");

			var oldOperator = GetOperatorVehicleRights(newOperator.id);

			if (oldOperator == null)
				throw new SecurityException("Operator not in Department");

			using (var entities = new Entities())
			{
				var @operator = entities.GetOperator(newOperator.id);

				var vehicleIds = newOperator.vehicles.Select(v => v.id).ToArray();

				var vehicleById = entities.VEHICLE
					.Where(
						v => vehicleIds.Contains(v.VEHICLE_ID) &&
							 v.DEPARTMENT.DEPARTMENT_ID == departmentId.Value)
					.ToDictionary(v => v.VEHICLE_ID);

				//Меняем права на машины
				var toVehicleRights = Server.Instance().VehicleSystemRightsReplacer.GetSystemRights(ComputedDepartmentType, IdType.Vehicle).ToArray();
				var rights = entities.GetSystemRights(toVehicleRights);

				if (newOperator.vehicles != null && newOperator.vehicles.Length > 0)
				{
					var vehicleRights = entities.OPERATOR_VEHICLE.Where(
						ov => ov.OPERATOR_ID == newOperator.id &&
							  vehicleIds.Contains(ov.VEHICLE_ID)).ToLookup(ov => ov.VEHICLE_ID);

					foreach (var newVehicle in newOperator.vehicles)
					{
						//Проверяем есть ли такая машина в департаменте
						VEHICLE oldVehicle;
						if (!vehicleById.TryGetValue(newVehicle.id, out oldVehicle))
							throw new ArgumentException("Vehicle not found by id " + newVehicle.id + " in department " + departmentId.Value);

						foreach (var right in rights)
						{
							var newRight = newVehicle.rights.FirstOrDefault(x => x.right_id == right.RIGHT_ID);
							var ov = vehicleRights[newVehicle.id].FirstOrDefault(x => x.RIGHT == right);
							if (newRight != null)
							{
								var allowed = newRight.allow ?? false;
								if (ov == null)
								{
									entities.OPERATOR_VEHICLE.AddObject(
										new OPERATOR_VEHICLE
											{
												OPERATOR = @operator,
												VEHICLE = oldVehicle,
												RIGHT = right,
												ALLOWED = allowed
											});
								}
								else
								{
									//Проверка, чтобы не формировались новые записи в истории изменений
									if (ov.ALLOWED != newRight.allow)
										ov.ALLOWED = allowed;
								}
							}
							else
							{
								if (ov != null)
								{
									entities.OPERATOR_VEHICLE.DeleteObject(ov);
								}
							}
						}
					}
				}

				//Меняем права на группы машин
				if (newOperator.groups != null && newOperator.groups.Length > 0)
				{
					var vgIds = newOperator.groups.Select(vg => vg.id).ToArray();
					var groupById = entities.VEHICLEGROUP.Where(vg => vgIds.Contains(vg.VEHICLEGROUP_ID)).ToDictionary(vg => vg.VEHICLEGROUP_ID);

					var vehicleGroupRights =
						entities.OPERATOR_VEHICLEGROUP
							.Where(ovg => ovg.OPERATOR_ID == newOperator.id &&
										  vgIds.Contains(ovg.VEHICLEGROUP_ID))
							.ToLookup(ovg => ovg.VEHICLEGROUP_ID);

					var allowedGroupIds = new HashSet<int>(oldOperator.groups.Select(g => g.id));

					foreach (var newGroup in newOperator.groups)
					{
						//TODO: вообще с клиента не передавать группу "Не в группе", про эту виртуальную группу должен знать только клиент
						// ОБработка виртуальной группы "не в группе". Права не могут быть выставлены.
						if (newGroup.id == -1)
							continue;
						// Проверяем доступна ли такая группа оператору
						bool groupAllowed = allowedGroupIds.Contains(newGroup.id);

						if (!groupAllowed)
							throw new SecurityException(string.Format("Group {0} is not allowed for operator {1}", newGroup.id, OperatorId));

						var group = groupById[newGroup.id];

						foreach (var right in rights)
						{
							var newRight = newGroup.rights.FirstOrDefault(x => x.right_id == right.RIGHT_ID);
							var ovg = vehicleGroupRights[newGroup.id].FirstOrDefault(x => x.RIGHT == right);
							if (newRight != null)
							{
								var allowed = newRight.allow ?? false;
								if (ovg == null)
								{
									entities.OPERATOR_VEHICLEGROUP.AddObject(
										new OPERATOR_VEHICLEGROUP
											{
												OPERATOR = @operator,
												VEHICLEGROUP = group,
												RIGHT = right,
												ALLOWED = allowed
											}
										);
								}
								else
								{
									//Проверка, чтобы не формировались новые записи в истории изменений
									if (ovg.ALLOWED != newRight.allow)
										ovg.ALLOWED = allowed;
								}
							}
							else
							{
								if (ovg != null)
									entities.OPERATOR_VEHICLEGROUP.DeleteObject(ovg);
							}
						}
					}
				}

				entities.SaveChangesBySession(SessionId);
			}
		}
		private CONTROLLER CreateControllerByPhone(Entities entities, string phone, int phoneContactId)
		{
			var result = new CONTROLLER
				{
					PHONE = phone,
					PhoneContactID = phoneContactId,
					CONTROLLER_TYPE = entities.GetControllerType(ControllerType.Names.Unspecified),
					CONTROLLER_INFO = new CONTROLLER_INFO { TIME = DateTime.UtcNow }
				};

			return result;
		}
		public AddTrackerResult AddTrackerByPhoneNumber(string phoneNumber)
		{
			if (phoneNumber.Length > 20)
				return Res::AddTrackerResult.InvalidPhoneNumber;

			var normalizedPhone = ContactHelper.GetNormalizedPhone(phoneNumber);
			if (normalizedPhone == null || !ContactHelper.IsValidPhone(normalizedPhone))
				return Res::AddTrackerResult.InvalidPhoneNumber;

			using (var entities = new Entities())
			{
				var allowedTrackerTypes = GetControllerTypeAllowedToAdd();
				if (allowedTrackerTypes == null ||
					allowedTrackerTypes.All(ct => ct.Name != ControllerType.Names.Unspecified))
					return Res::AddTrackerResult.Forbidden;

				var phoneContactId = entities.GetContactId(ContactType.Phone, normalizedPhone);

				if (entities.Operator_Contact.Any(oc => oc.Contact_ID == phoneContactId && oc.Confirmed))
					return Res::AddTrackerResult.PhoneAlreadyExists;

				var existingVehicle =
					entities.VEHICLE
					.Where(v => v.CONTROLLER.PhoneContactID == phoneContactId)
					.Select(v => new {Id = v.VEHICLE_ID})
					.FirstOrDefault();

				if (existingVehicle != null)
				{
					if (entities.Log_Time.Any(l => l.Vehicle_ID == existingVehicle.Id))
						return Res::AddTrackerResult.PhoneAlreadyExists;

					var existingVehicleController =
						entities.CONTROLLER.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == existingVehicle.Id);

					if (existingVehicleController != null)
						existingVehicleController.PhoneContactID = null;
				}

				var ormController = CreateControllerByPhone(entities, phoneNumber, phoneContactId);

				var controllerType = ormController.CONTROLLER_TYPE;
				if (!controllerType.DefaultVehicleKindReference.IsLoaded)
					controllerType.DefaultVehicleKindReference.Load();

				var ormVehicle = new VEHICLE
				{
					GARAGE_NUMBER  = ormController.PHONE,
					PUBLIC_NUMBER  = string.Empty,
					CONTROLLER     = ormController,
					VEHICLE_KIND   = controllerType.DefaultVehicleKind ?? entities.GetVehicleKind(VehicleKind.Truck), //TODO: вынести в параметр
					VEHICLE_STATUS = entities.GetDefaultVehicleStatus(),
				};

				var ormDepartment = ComputedDepartmentId != null
					? entities.GetDepartment(ComputedDepartmentId.Value)
					: null;
				ormVehicle.SetDepartment(entities, ormDepartment);

				entities.VEHICLE.AddObject(ormVehicle);

				var ormOperator = entities.GetOperator(OperatorId);
				entities.Allow(ormOperator, ormVehicle, UserRole.DefaultOwnRightsOnVehicle);
				entities.SaveChangesBySession(SessionId);

				var tryConnectServiceAppResult = entities.TryConnectServiceApp(ormVehicle, phoneContactId);
				if (tryConnectServiceAppResult == TryConnectServiceAppResult.Success)
				{
					entities.AddDefaultRules(ormController);
					entities.SaveChangesBySession(SessionId);
				}
				else
				{
					Trace.TraceWarning("{0}: unable to connect service app by phone {1} to vehicle {2}: {3}",
						this, phoneNumber, ormVehicle.VEHICLE_ID, tryConnectServiceAppResult);
				}

				return new AddTrackerResult(Res::AddTrackerResult.Success, ormVehicle.VEHICLE_ID);
			}
		}
		public AddTrackerResult AddTrackerAsEmpty()
		{
			using (var entities = new Entities())
			{
				var allowedTrackerTypes = GetControllerTypeAllowedToAdd();
				if (allowedTrackerTypes == null ||
					allowedTrackerTypes.All(ct => ct.Name != ControllerType.Names.Unspecified))
					return Res::AddTrackerResult.Forbidden;

				var ormController = new CONTROLLER
				{
					CONTROLLER_TYPE = entities.GetControllerType(ControllerType.Names.Unspecified),
					CONTROLLER_INFO = new CONTROLLER_INFO {TIME = DateTime.UtcNow}
				};

				var controllerType = ormController.CONTROLLER_TYPE;
				if (!controllerType.DefaultVehicleKindReference.IsLoaded)
					controllerType.DefaultVehicleKindReference.Load();

				var ormDepartment = ComputedDepartmentId != null ? entities.GetDepartment(ComputedDepartmentId.Value) : null;
				var ormVehicle = new VEHICLE
				{
					GARAGE_NUMBER  = ormController.PHONE,
					PUBLIC_NUMBER  = string.Empty,
					CONTROLLER     = ormController,
					VEHICLE_KIND   = controllerType.DefaultVehicleKind ?? entities.GetVehicleKind(VehicleKind.Truck),
					//TODO: вынести в параметр
					VEHICLE_STATUS = entities.GetDefaultVehicleStatus(),
				};

				ormVehicle.SetDepartment(entities, ormDepartment);

				entities.VEHICLE.AddObject(ormVehicle);

				var ormOperator = entities.GetOperator(OperatorId);
				entities.Allow(ormOperator, ormVehicle, UserRole.DefaultOwnRightsOnVehicle);
				Server.Instance().OnVehicleAccessGranted(OperatorId, ormVehicle.VEHICLE_ID);
				entities.SaveChangesBySession(SessionId);
				return new AddTrackerResult(Res::AddTrackerResult.Success, ormVehicle.VEHICLE_ID);
			}
		}
		public Department LockDepartment(int departmentId)
		{
			return DoServiceManagementWork(departmentId, d => d.LockDate = DateTime.UtcNow);
		}
		public Department UnlockDepartment(int departmentId)
		{
			return DoServiceManagementWork(departmentId, d => d.LockDate = null);
		}
		private Department DoServiceManagementWork(int departmentId, Action<DEPARTMENT> action)
		{
			if (!IsSalesManager) // Нужно быть партнером
				throw new SecurityException("Operator should be sales manager");

			using (var entities = new Entities())
			{
				// Нужно быть партнером указанного департамента или суперадмином (партнером для всех департаментов)
				if (!(entities.IsSalesManager(OperatorId, departmentId) || entities.IsSuperAdministrator(OperatorId)))
					throw new SecurityException("Operator should be sales manager");

				var department = entities.DEPARTMENT
					.FirstOrDefault(d => d.DEPARTMENT_ID == departmentId);

				if (department == null)
					throw new SecurityException("Operator should have rights on the department");

				action(department);
				entities.SaveChangesBySession(SessionId);

				return entities.DEPARTMENT.First(d => d.DEPARTMENT_ID == departmentId).ToDto();
			}
		}
	}
}