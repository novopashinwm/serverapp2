﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using Interfaces.Web.Enums;
using Contact = FORIS.TSS.BusinessLogic.DTO.Contact;
using TrackingSchedule = FORIS.TSS.BusinessLogic.DTO.TrackingSchedule;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		public override string GetAsidName(int asidId)
		{
			using (var entities = new Entities())
			{
				var asid = entities.Asid.Where(a => a.ID == asidId).Select(a => new
				{
					OperatorId = (int?)a.OPERATOR.OPERATOR_ID,
					Contact = a.Contact.DemaskedPhone.Value,
					VehicleId = (int?)a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID
				}).FirstOrDefault();

				if (asid == null)
					return null;

				return asid.VehicleId != null ?
					GetVehicleName(asid.OperatorId, asid.VehicleId.Value) :
					asid.Contact;
			}
		}
		//TODO: для определения названий нескольких объектов, реализовать функцию, которая примет список объектов
		/// <summary> Определяет название объекта наблюдения </summary>
		public override string GetVehicleName(int? operatorId, int vehicleId)
		{
			var vehicleNameString = GetVehicleNameString(operatorId, vehicleId);
			if (vehicleNameString != null)
				vehicleNameString = vehicleNameString.Trim();
			return vehicleNameString;
		}
		private string GetVehicleNameString(int? operatorId, int vehicleId)
		{
			using (var entities = new Entities())
			{
				var vehicle =
					entities
						.VEHICLE
						.Where(v => v.VEHICLE_ID == vehicleId)
						.Select(
							v =>
								new
								{
									Name = v.GARAGE_NUMBER,
									Msisdn = v.CONTROLLER.PhoneContact.Value,
									AsidMsisdn = v.CONTROLLER.MLP_Controller.Asid.Contact.DemaskedPhone.Value,
									DeviceIdBytes = (v.CONTROLLER.CONTROLLER_INFO.DEVICE_ID)
								})
						.FirstOrDefault();

				if (vehicle == null)
					return null;

				var msisdn = vehicle.Msisdn ?? vehicle.AsidMsisdn;

				if (msisdn != null && operatorId != null)
				{
					var contactName = entities.
						Operator_Contact.Where(
							oc => oc.Operator_ID == operatorId.Value &&
								  oc.Contact.Type == (int) ContactType.Phone &&
								  oc.Contact.Value == msisdn)
						.Select(oc => oc.Name)
						.FirstOrDefault();

					if (!string.IsNullOrWhiteSpace(contactName))
						return contactName;

					var pb = DbManager.SearchPhoneBookContacts(operatorId.Value, new[] {msisdn});
					if (pb != null)
						contactName = pb.Where(bp => bp.Value == msisdn).Select(bp => bp.Name).FirstOrDefault();

					if (!string.IsNullOrWhiteSpace(contactName))
						return contactName;
				}

				if (!string.IsNullOrWhiteSpace(vehicle.Name))
					return vehicle.Name;

				if (msisdn != null)
				{
					// Без + номер телефона не набирается на iPhone
					return "+" + msisdn;
				}

				return string.Empty;
			}
		}
		public SetVehicleDeviceIDResult SetVehicleDeviceId(int operatorId, int vehicleId, string deviceId, SaveContext saveContext)
		{
			if (deviceId == null)
				deviceId = string.Empty;

			using (var entities = new Entities())
			{
				var ormVehicle = entities
					?.GetVehicleQuery(VEHICLE.CONTROLLERIncludePath)
					?.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
				if (ormVehicle == null)
					return SetVehicleDeviceIDResult.InsufficientRights;

				deviceId = deviceId.Trim();

				var newDeviceId = Encoding.ASCII.GetBytes(deviceId);

				entities.EnsureControllerInfoExists(ormVehicle);

				var oldDeviceId = ormVehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID;
				if (oldDeviceId != null && oldDeviceId.SequenceEqual(newDeviceId))
					return SetVehicleDeviceIDResult.Success;

				if (newDeviceId.Length == 0)
				{
					ormVehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID = newDeviceId;
					entities.SaveChanges(saveContext);
					return SetVehicleDeviceIDResult.Success;
				}

				if (ormVehicle.CONTROLLER.CONTROLLER_TYPE.DeviceIdIsImei && !ImeiHelper.IsImeiCorrect(deviceId))
					return SetVehicleDeviceIDResult.WrongDeviceID;

				deviceId = ormVehicle.CONTROLLER.CONTROLLER_TYPE.NormalizeDeviceId(deviceId);

				using (var tran = new Transaction())
				{
					// Проверка на наличие такого идентификатора устройства
					var anyDeviceId =
						entities.CONTROLLER_INFO.Any(i => i.DEVICE_ID != null && i.DEVICE_ID == newDeviceId);
					if (anyDeviceId)
					{
						tran.Complete();
						return SetVehicleDeviceIDResult.DeviceIDAlreadyExists;
					}

					int newControllerNumber;
					if (!int.TryParse(deviceId, out newControllerNumber))
						newControllerNumber = 0;

					ormVehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID = newDeviceId;
					ormVehicle.CONTROLLER.NUMBER = newControllerNumber;
					if (string.IsNullOrEmpty(ormVehicle.GARAGE_NUMBER))
					{
						ormVehicle.GARAGE_NUMBER = Encoding.ASCII.GetString(newDeviceId);
					}

					entities.SaveChanges(saveContext);

					tran.Complete();
					return SetVehicleDeviceIDResult.Success;
				}
			}
		}
		public SetVehicleDevicePhoneResult SetVehicleDevicePhone(int operatorId, int vehicleId, string phone, SaveContext saveContext)
		{
			phone = string.IsNullOrWhiteSpace(phone) ? string.Empty : phone.Trim();
			string normalizedPhone = null;
			if (phone != string.Empty)
			{
				normalizedPhone = ContactHelper.GetNormalizedPhone(phone);
				if (normalizedPhone == null ||
					!ContactHelper.IsValidPhone(normalizedPhone))
					return SetVehicleDevicePhoneResult.InvalidFormat;
			}

			using (var entities = new Entities())
			{
				var ormVehicle = entities
					?.GetVehicleQuery(VEHICLE.CONTROLLERIncludePath)
					?.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);

				if (ormVehicle == null)
					return SetVehicleDevicePhoneResult.InsufficientRights;

				entities.EnsureControllerExists(ormVehicle);

				if (ormVehicle.CONTROLLER.PHONE == phone)
					return SetVehicleDevicePhoneResult.Success;

				/*
				1. При указании номера проверять, есть ли услуга на номере и является ли услуга свободной.
				   Свободной считается услуга, по учётной записи которой нет ни одного входа в систему, и она не привязана к корпоративному клиенту (единственная на контракте и контракт не корпоративный).
				   Если услуги нет, то просто сохранять номер в Controller.Phone
				2. При переносе нужно решить, что делать с объектом наблюдения и ссылкой на клиента.
					Следует перенаправить MLP_Controller на новый объект наблюдения
					При этом ссылку Vehicle.Department в клиенте менять не следует, т.к. она влияет на управление правами.
					Ссылку Asid.Department_ID менять также не следует, т.к. она вляет на корректность биллинга.
				3. Обработать ситуацию, когда вначале добавляют устройство, сразу указывают номер телефона, а услугу добавляют позже.
					При добавлении услуги и после демаскирования проверять, нет ли уже на данном контракте устройства с таким номером телефона.
					Если есть - безусловно привязывать. Для этого следует дать возможность указывать номер контракта МТС (только для России).
					Если нет - то устройство может быть на другом контракте - тоже привязывать, но оставлять неизменным Asid.Department.ExtID
				 */

				if (phone == string.Empty)
				{
					if (ormVehicle.CONTROLLER.PhoneContactID != null)
					{
						entities.DisconnectServiceApp(ormVehicle);
						ormVehicle.CONTROLLER.PhoneContactID = null;
						ormVehicle.CONTROLLER.PHONE = null;
						entities.SaveChanges(saveContext);
					}
					return SetVehicleDevicePhoneResult.Success;
				}

				var contactId = entities.GetContactId(ContactType.Phone, normalizedPhone);

				if (ormVehicle.CONTROLLER.PhoneContactID == contactId)
				{
					//Просто меняется представление номера телефона, например +798525796565 на +7(985)257-95-65
					ormVehicle.CONTROLLER.PHONE = phone;
					if (string.IsNullOrEmpty(ormVehicle.GARAGE_NUMBER))
					{
						ormVehicle.GARAGE_NUMBER = phone;
					}
					entities.SaveChanges(saveContext);
					return SetVehicleDevicePhoneResult.Success;
				}
				/*
				 * Отключаем проверку до полного прояснения ситуации о необходимости такой проверки.
				// Проверяем, есть ли такой телефон у другого объекта наблюдения
				var otherVehicle =
					entities.VEHICLE
							.Where(
								v => v.VEHICLE_ID != vehicleId &&
									 v.CONTROLLER.PhoneContactID == contactId)
							.Select(v => new { ID = v.VEHICLE_ID })
							.FirstOrDefault();

				if (otherVehicle != null)
				{
					if (entities.Log_Time.Any(lt => lt.Vehicle_ID == otherVehicle.ID))
						return SetVehicleDevicePhoneResult.AlreadyExists;

					var confirmedOperator =
						entities.Operator_Contact
							.Where(oc => oc.Contact_ID == contactId && oc.Confirmed)
							.Select(oc => new {ID = oc.Operator_ID})
							.FirstOrDefault();

					if (confirmedOperator != null)
					{
						using (var historical = new EntityModel.History.HistoricalEntities())
						{
							if (historical.SESSION.Any(s => s.OPERATOR_ID == confirmedOperator.ID))
								return SetVehicleDevicePhoneResult.AlreadyExists;
						}
					}
				}
				*/
				var assignServiceAppResult = entities.TryConnectServiceApp(ormVehicle, contactId);
				switch (assignServiceAppResult)
				{
					case TryConnectServiceAppResult.NoService:
						if (entities.IsSuperAdministrator(operatorId))
							break; //Чтобы можно было на "низком уровне" указывать номер телефона
						return SetVehicleDevicePhoneResult.NoService;
					case TryConnectServiceAppResult.ServiceIsAlreadyUsed:
						return SetVehicleDevicePhoneResult.ServiceIsAlreadyUsed;
					case TryConnectServiceAppResult.ServiceAppDoesNotExist:
					case TryConnectServiceAppResult.Success:
						break;
					default:
						throw new InvalidOperationException(
							"TryAssignServiceApp unexpected result: " + assignServiceAppResult);
				}

				//Устанавливаем номер телефона
				ormVehicle.CONTROLLER.PHONE = phone;
				ormVehicle.CONTROLLER.PhoneContactID = contactId;
				if (string.IsNullOrEmpty(ormVehicle.GARAGE_NUMBER))
				{
					ormVehicle.GARAGE_NUMBER = phone;
				}

				entities.SaveChanges(saveContext);

				return SetVehicleDevicePhoneResult.Success;
			}
		}
		public List<TrackingSchedule> GetVehicleSchedules(int operatorId, int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedVehicleAll(operatorId, vehicleId, SystemRight.VehicleAccess))
					return new List<TrackingSchedule>();

				var isAdmin = entities.IsAllowedVehicleAll(operatorId, vehicleId, SystemRight.SecurityAdministration);
				IQueryable<EntityModel.TrackingSchedule> query;
				if (isAdmin)
					query = entities.TrackingSchedule.Where(
						x => x.Vehicle_ID == vehicleId &&
							 x.VEHICLE.Accesses.Any(
								 a =>
									 a.operator_id == x.Operator_ID &&
									 a.right_id == (int) SystemRight.VehicleAccess));
				else
					query =
						entities.TrackingSchedule.Where(
							x =>
								x.Vehicle_ID == vehicleId &&
								x.Operator_ID == operatorId);

				return LoadVehicleSchedule(entities, operatorId, query);
			}
		}
		private List<TrackingSchedule> LoadVehicleSchedule(Entities entities, int operatorId, IQueryable<EntityModel.TrackingSchedule> query)
		{
			var dbItems =
				query.Select(x => new
				{
					x.ID,
					x.Enabled,
					x.Operator_ID,
					x.Config
				}).ToList();

			var result = new List<TrackingSchedule>(dbItems.Count);
			foreach (var item in dbItems)
			{
				var xmlReader = new XmlTextReader(new StringReader(item.Config));
				var trackingSchedule = (TrackingSchedule)TrackingScheduleSerializer.ReadObject(xmlReader);
				trackingSchedule.Id = item.ID;
				trackingSchedule.Enabled = item.Enabled;
				trackingSchedule.Owner = new Owner { OperatorId = item.Operator_ID };
				trackingSchedule.Editable = item.Operator_ID == operatorId;
				result.Add(trackingSchedule);
			}

			if (result.Count == 0)
				return result;

			var operatorIds = dbItems.Select(x => x.Operator_ID).Distinct().ToArray();
			var owners = entities.Operator_Contact
				.Where(
					x =>
						operatorIds.Contains(x.Operator_ID) &&
						x.Confirmed &&
						x.Contact.Type == (int)ContactType.Phone)
				.ToLookup(x => x.Operator_ID, x => x.Contact.Value);

			foreach (var trackingSchedule in result)
			{
				var ownerMsisdn = owners[trackingSchedule.Owner.OperatorId].FirstOrDefault();
				if (ownerMsisdn != null)
					trackingSchedule.Owner.Contact = new Contact(ContactType.Phone, ownerMsisdn);
			}

			return result;
		}
		private static readonly DataContractSerializer TrackingScheduleSerializer = new DataContractSerializer(typeof(TrackingSchedule));
		/// <summary> Устанавливает расписание для объекта наблюдения </summary>
		public TrackingSchedule SetVehicleSchedule(int operatorId, int vehicleId, TrackingSchedule schedule, SaveContext saveContext)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedVehicleAll(operatorId, vehicleId, SystemRight.VehicleAccess))
					throw new SecurityException("Vehicle access is not allowed");

				EntityModel.TrackingSchedule entity;
				if (0 < schedule.Id)
				{
					entity = entities.TrackingSchedule.FirstOrDefault(
						x => x.Operator_ID == operatorId &&
							 x.Vehicle_ID == vehicleId &&
							 x.ID == schedule.Id);
					if (entity == null)
						return null;
				}
				else
				{
					entity = new EntityModel.TrackingSchedule();
					entity.Operator_ID = operatorId;
					entity.Vehicle_ID = vehicleId;
					entities.TrackingSchedule.AddObject(entity);
				}

				entity.Enabled = schedule.Enabled;

				var sb = new StringBuilder();
				var stringWriter = new StringWriter(sb);
				var xmlWriter = new XmlTextWriter(stringWriter);
				TrackingScheduleSerializer.WriteObject(xmlWriter, schedule);
				entity.Config = sb.ToString();

				entities.CreateAppNotificationAboutVehicleDataChange(vehicleId, UpdatedDataType.Schedule);

				entities.SaveChanges(saveContext);

				return LoadVehicleSchedule(entities, operatorId, entities.TrackingSchedule.Where(x => x.ID == entity.ID)).Single();
			}
		}
		public void DeleteVehicleSchedule(int operatorId, int scheduleId, SaveContext saveContext)
		{
			using (var entities = new Entities())
			{
				var entity = entities.TrackingSchedule.FirstOrDefault(
					x => x.Operator_ID == operatorId &&
						 x.ID == scheduleId);

				if (entity == null)
					return;

				var vehicleID = entity.Vehicle_ID;
				entities.TrackingSchedule.DeleteObject(entity);
				entities.CreateAppNotificationAboutVehicleDataChange(vehicleID, UpdatedDataType.Schedule);

				entities.SaveChanges(saveContext);
			}
		}
	}
}