﻿using System;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Interfaces.Web;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public bool CancelCommand(int id)
		{
			return Server.Instance().CancelCommand(OperatorId, id);
		}
		/// <summary> Отправка текстового сообщения на контроллер </summary>
		/// <param name="command"></param>
		public Command RegisterCommand(Command command)
		{
			command.SetParameter(Command_Parameter.SessionID, SessionId.ToString());

			command = Server.Instance().RegisterCommand(SessionInfo.OperatorInfo, command, CultureInfo);

			if (command.Result != null && command.ResultText == null)
			{
				var r = ResourceContainers.Get(CultureInfo);
				command.ResultText = command.Status == CmdStatus.Completed
					? r[command.Result.Value]
					: r[command.Status];
			}

			return command;
		}
		/// <summary> Отмена команды на остановку/включения двигателя авто </summary>
		/// <param name="vehicleIds">Список объектов наблюдения, по которым следует отменить иммобилизацию</param>
		public void CancelImmobilization(int[] vehicleIds)
		{
			using (var entities = new Entities())
			{
				var commandIds =
				(
					from c in entities.Command
					where
						(c.Status == (int)CmdStatus.Received || c.Status == (int)CmdStatus.Processing)
						&& vehicleIds.Contains(c.Target_ID)
						&&
						(
							c.Type_ID == (int)CmdType.CutOffElectricity ||
							c.Type_ID == (int)CmdType.ReopenElectricity ||
							c.Type_ID == (int)CmdType.CutOffFuel        ||
							c.Type_ID == (int)CmdType.ReopenFuel
						)
					select c.ID
				)
					.ToList();
				foreach (var commandId in commandIds)
				{
					CancelCommand(commandId);
				}
			}
		}
		public void SendInstantCommand(CmdType type, int vehicleId, Action<object> ready)
		{
			using (var entities = new Entities())
			{
				if (!entities.v_operator_vehicle_right.Any(
						ovr =>
						ovr.operator_id == OperatorId &&
						ovr.vehicle_id == vehicleId &&
						ovr.right_id == (int) SystemRight.VehicleAccess))
					throw new SecurityException("Vehicle " + vehicleId + " is not accessible");
			}

			switch (type)
			{
				case CmdType.CreateReplayUri:
					var command = CommandCreator.CreateCommand(type);
					if (command == null)
						throw new InvalidOperationException("Unable to create command by type: " + type);
					command.Target.Unique = vehicleId;

					NotifyEventHandler[] handler = {null};
					Server instance = Server.Instance();
					handler[0] = args =>
						{
							var tags = args.Tag;
							CommandResult commandResult = null;
							if (tags != null && tags.Length == 1) 
								commandResult = tags[0] as CommandResult;

							if (commandResult == null)
								return;


							var target = commandResult.Tag;
							if (target == null ||
								target.Unique != vehicleId ||
								commandResult.CmdType != type)
								return;

							instance.NotifyEvent -= handler[0];
							ready(commandResult.Value);
						};

					instance.NotifyEvent += handler[0];
					instance.SendCommand(command);
					break;
				default:
					throw new NotSupportedException("Command type is not supported: " + type);
			}
		}
		public CommandScenario GetCommandScenario(int vehicleId, CmdType type)
		{
			var scenario = Server.Instance().GetCommandScenario(SessionInfo.OperatorInfo, vehicleId, type);

			if (scenario.Result != CmdResult.Completed)
				scenario.ResultText = ResourceContainers.Get(CultureInfo)[scenario.Result];

			return scenario;
		}
	}
}