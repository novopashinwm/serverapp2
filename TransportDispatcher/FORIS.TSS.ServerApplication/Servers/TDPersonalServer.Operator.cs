﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security;
using FORIS.DataAccess.Interfaces;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using Interfaces.Web;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		private string _cultureCode;
		private CultureInfo _cultureInfo;
		/// <summary> Текущая культура пользователя </summary>
		public CultureInfo CultureInfo
		{
			get
			{
				if (_cultureInfo == null)
				{
					if (_cultureCode == null)
					{
						using (var entities = new Entities())
						{
							_cultureCode = entities.OPERATOR
								.Where(o => o.OPERATOR_ID == OperatorId)
								.Select(o => o.Culture.Code)
								.FirstOrDefault() ?? CultureInfo.CurrentCulture.Name;
						}
					}

					_cultureInfo = CultureInfo.GetCultureInfo(_cultureCode);
				}

				return _cultureInfo;
			}
		}
		protected override SetNewLoginResult SetNewLogin(string login)
		{
			if (IsGuest)
				return SetNewLoginResult.Forbidden;

			return SetNewLogin(OperatorId, login);
		}
		public void SetUserName(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			using (var entities = new Entities())
			{
				if (!entities.v_operator_rights.Any(
					x => x.operator_id == OperatorId && x.right_id == (int)SystemRight.SecurityAdministration))
					throw new SecurityException("Forbidden");

				var @operator = entities.OPERATOR.First(o => o.OPERATOR_ID == OperatorId);
				if (@operator.NAME == name)
					return;
				@operator.NAME = name;
				entities.SaveChangesBySession(SessionId);
			}
		}
		private bool _ownVehicleIdSet;
		private int? _ownVehicleId;
		public int? OwnVehicleId
		{
			get
			{
				if (_ownVehicleIdSet)
					return _ownVehicleId;

				using (var entities = new Entities())
				{
					var result = entities.VEHICLE
						.Where(v => v.CONTROLLER.MLP_Controller.Asid.OPERATOR.OPERATOR_ID == OperatorId)
						.Select(v => new { v.VEHICLE_ID })
						.FirstOrDefault();
					_ownVehicleId = result?.VEHICLE_ID;
					_ownVehicleIdSet = true;
					return _ownVehicleId;
				}
			}
		}
		public string GetOrCreateSimId()
		{
			using (var entities = new Entities())
			{
				return entities.GetOrCreateSimIdByOperatorId(OperatorId, ComputedDepartmentId, SaveContext.Session(SessionId));
			}
		}
		public string GetMsisdn()
		{
			using (var entities = new Entities())
			{
				var contact = entities.GetOperatorConfirmedPhone(OperatorId);
				return contact != null ? contact.Value : null;
			}
		}
		public SetNewPasswordResult ChangePassword(string oldPassword, string newPassword)
		{
			if (string.IsNullOrWhiteSpace(oldPassword))
				throw new ArgumentException(@"Value is null or empty", "oldPassword");

			if (string.IsNullOrWhiteSpace(newPassword))
				return SetNewPasswordResult.NewPasswordIsInvalid;

			if (IsGuest)
				throw new SecurityException("Forbidden for guest");

			var login = SessionInfo.OperatorInfo.Login;
			if (string.IsNullOrWhiteSpace(login))
				throw new SecurityException("User has no login");

			Server.Instance().AcceptRequestWithLimit(BanLimit.CheckPassword, SessionInfo.IP.ToString());

			var oldPasswordCheckingResult = Server.Instance().CheckPasswordForWeb(login, oldPassword);
			if (oldPasswordCheckingResult == null)
				return SetNewPasswordResult.OldPasswordIsWrong;

			return SetPassword(newPassword, OperatorId);
		}
		public List<BillingService> GetBillingServices()
		{
			return Server.Instance()
				.DbManager
				.GetOperatorServices(OperatorId, FilterByDepartment ? ComputedDepartmentId : null);
		}
		/// <summary> Проверяет, является ли данный пользователь гостем </summary>
		public bool IsGuest
		{
			get
			{
				return string.IsNullOrWhiteSpace(SessionInfo.OperatorInfo.Login) ||
					SessionInfo.OperatorInfo.Login == ConfigHelper.GetAppSettingAsString("GuestLogin");
			}
		}
		protected override bool CheckPassword(string password)
		{
			using (IStoredProcedure sp = serverInstance.Database.Procedure("dbo.CheckPassword"))
			{
				sp["@OPERATOR_ID"].Value = SessionInfo.OperatorInfo.OperatorId;
				sp["@PASSWORD"].Value = password;
				sp["@RESULT"].Value = false;

				sp.ExecuteNonQuery();

				return (bool)sp["@RESULT"].Value;
			}
		}
		protected override SetNewPasswordResult SetPassword(string password, int operatorId)
		{
			if (IsGuest)
				throw new SecurityException("Password change is forbidden for guest");

			var departmentId = ComputedDepartmentId;
			// Пароль разрешается менять или себе, или своим подчиненным пользователям
			if (operatorId != OperatorId)
			{
				if (departmentId == null)
					throw new SecurityException("Department is not selected");

				if (!Department.Rights.Contains(SystemRight.SecurityAdministration))
					throw new SecurityException($@"Operator {OperatorId} is not department admin for {departmentId.Value}");
			}

			using (var entities = new Entities())
			{
				if (operatorId != OperatorId)
				{
					if (!entities.v_operator_department_right
						.Any(odr =>
							odr.operator_id == operatorId &&
							odr.Department_ID == departmentId.Value &&
							odr.right_id == (int)SystemRight.DepartmentsAccess))
						throw new SecurityException($@"Operator {operatorId} is not member of department {departmentId.Value}");
				}
				else
				{
					if (!entities.Operator_Contact.Any(oc =>
						(oc.Operator_ID == operatorId && oc.Confirmed) &&
						(oc.Contact.Type == (int)ContactType.Phone || oc.Contact.Type == (int)ContactType.Email)))
						return SetNewPasswordResult.NoConfirmedContact;
				}

				var ormOperator = entities.GetOperator(operatorId);
				ormOperator.PASSWORD = password;
				ormOperator.OneTimePassword = false;
				entities.SaveChangesBySession(SessionId);
				/////////////////////////////////////////////////////////////////////////////////////////////
				// Меняем SimId при смене пароля, что бы заставить приложения заново войти по имени и паролю
				ResetOperatorSimId(operatorId);
				// Закрываем все сессии пользователя, чтобы заставить браузеры выйти на форму логина
				ResetOperatorSessions();
				/////////////////////////////////////////////////////////////////////////////////////////////
				return SetNewPasswordResult.Success;
			}
		}
		private void ResetOperatorSimId(int operatorId)
		{
			using (var entities = new Entities())
			{
				var ormOperator = entities.GetOperator(operatorId, OPERATOR.AsidIncludePath);
				// Если связь с Asid не загружена загружаем
				if (!ormOperator.AsidReference.IsLoaded)
					ormOperator.AsidReference.Load();
				// Если SimID существует, то генерируем новый, для невозможности входа по SimId
				if (!string.IsNullOrWhiteSpace(ormOperator?.Asid?.SimID))
					ormOperator.Asid.SimID = Guid.NewGuid().ToString("N");
				entities.SaveChangesBySession(SessionId);
			}
		}
		private void ResetOperatorSessions()
		{
			(sessionManager as IAdminSessionManager)
				?.CloseSessionsByOperatorId(OperatorId);
		}
		private TimeZoneInfo _tz;
		TimeZoneInfo IWebPersonalServer.TimeZoneInfo
		{
			get
			{
				if (_tz == null)
				{
					using (var entities = new Entities())
					{
						var code =
						(
							from o in entities.OPERATOR
							where o.OPERATOR_ID == OperatorId
							select o.TimeZone.Code
						)
							.FirstOrDefault();
						if (code == null)
							_tz = TimeZoneInfo.Local;
						else
							_tz = TimeZoneInfo.FindSystemTimeZoneById(code);
					}
				}

				return _tz;
			}
		}
		bool IWebPersonalServer.UpdateTimeZoneInfo(TimeZoneInfo timeZoneInfo, bool isTimeZoneInfoManual)
		{
			_tz = timeZoneInfo;
			if (IsGuest)
				return false;

			var equalSystemTimeZone = TimeZoneHelper.GetEqualTimeZoneInfos(timeZoneInfo).FirstOrDefault();

			if (equalSystemTimeZone == null)
			{
				Trace.TraceWarning("Unknown time zone: " + timeZoneInfo.ToSerializedString());
				return false;
			}

			using (var entities = new Entities())
			{
				var ormOperator = entities.GetOperator(OperatorId);
				if (!ormOperator.TimeZoneReference.IsLoaded)
					ormOperator.TimeZoneReference.Load();

				var timeZoneCode = equalSystemTimeZone.Id;
				if (ormOperator.TimeZone != null && (
					ormOperator.TimeZone.Code == timeZoneCode ||
					(ormOperator.IsTimeZoneInfoManual ?? false) && !isTimeZoneInfoManual))
				{
					return false;
				}

				var timeZoneEntity = entities.TimeZone.FirstOrDefault(tz => tz.Code == timeZoneCode);
				if (timeZoneEntity == null)
				{
					Trace.TraceWarning("Time zone not in reference book: " + timeZoneCode);
					return false;
				}
				ormOperator.TimeZone = timeZoneEntity;
				ormOperator.IsTimeZoneInfoManual = isTimeZoneInfoManual;

				entities.SaveChangesBySession(SessionId);

				return true;
			}
		}
		//TODO: Optimize
		bool IWebPersonalServer.UpdateCulture(string cultureCode)
		{
			if (_cultureCode == cultureCode)
				return false;

			_cultureCode = cultureCode;
			_cultureInfo = CultureInfo.GetCultureInfo(_cultureCode);

			if (IsGuest)
				return false;

			using (var entities = new Entities())
			{
				var ormOperator = entities.GetOperator(OperatorId, OPERATOR.CultureIncludePath);

				if (ormOperator.Culture != null &&
					string.Equals(ormOperator.Culture.Code, cultureCode, StringComparison.OrdinalIgnoreCase))
					return false;

				var culture = entities.Culture.FirstOrDefault(c => c.Code == cultureCode);
				if (culture == null)
				{
					using (var transaction = new Transaction())
					{
						culture = entities.Culture.FirstOrDefault(c => c.Code == cultureCode);
						if (culture == null)
						{
							culture = new Culture { Code = cultureCode };
							entities.Culture.AddObject(culture);
							entities.SaveChanges();
						}
						transaction.Complete();
					}
				}
				ormOperator.Culture = culture;
				entities.SaveChangesBySession(SessionId);
				return true;
			}
		}
		bool IWebPersonalServer.AllowUseDialer
		{
			get
			{
				using (var entities = new Entities())
				{
					return ComputedDepartmentId != null &&
					(
						from d in entities.DEPARTMENT
						where d.DEPARTMENT_ID == ComputedDepartmentId.Value &&
							d.Country.AllowUseDialer
						select d
					)
						.Any();
				}
			}
		}
		/// <summary> Проверяет, может ли данный пользователь добавлять новые устройства </summary>
		public bool AllowAddTracker
		{
			get
			{
				if (IsGuest)
					return false;

				using (var entities = new Entities())
				{
					return CanOperatorAddTracker(entities);
				}
			}
		}
		/// <summary> Можно ли отправлять SMS </summary>
		public bool AllowSmsSend
		{
			get
			{
				if (IsGuest)
					return false;

				if (Department == null)
					return false;

				using (var entities = new Entities())
				{
					return entities.OPERATOR_DEPARTMENT
						.Any(x =>
							x.OPERATOR_ID == OperatorId &&
							x.DEPARTMENT.Country.Country_ID == 1); //India
				}
			}
		}
		/// <summary> Может ли текущий пользователь изменять свой собственный логин </summary>
		public bool AllowChangeOwnLogin
		{
			get
			{
				//TODO: брать значение из прав пользователя
				//Пока нет бизнес-процессов, которые бы требовали возможности изменять свой собственный логин
				//Для администраторов абонентов наоборот, лучше, если нет возможности по ошибке изменить логин
				return !IsGuest;
			}
		}
		void IWebPersonalServer.DeleteOperatorFromDepartment(int operatorIdToDelete)
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new SecurityException("Department is not assigned");

			//Исключаем оператора из департамента
			//Удаляем все права оператора на машины департамента и группы админа департамента
			var arParams = new ArrayList();
			arParams.Add(new ParamValue("@operatorId", OperatorId));
			arParams.Add(new ParamValue("@operatorIdToDelete", operatorIdToDelete));
			arParams.Add(new ParamValue("@departamentId", departmentId.Value));

			GetDataFromDB(arParams, "dbo.DeleteOperatorFromDepartment", null);
		}
		private void CheckAdministrativeRightsOnDepartment(Entities entities)
		{
			var currentOperator = entities.GetOperator(OperatorId);
			if (currentOperator == null)
				throw new InvalidOperationException($@"No current user operating found by id {OperatorId}");

			// Check if possible deleting operator.
			if (!entities.v_operator_rights
				.Any(x =>
					x.operator_id == OperatorId &&
					(
						x.right_id == (int)SystemRight.ServerAdministration ||
						x.right_id == (int)SystemRight.SecurityAdministration
					)))
				throw new SecurityException("No appropriate rights for current user.");

			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new InvalidOperationException("Department is not specified");

			if (!entities.v_operator_department_right
				.Any(odr =>
					odr.operator_id == OperatorId &&
					odr.Department_ID == departmentId.Value &&
					odr.right_id == (int)SystemRight.SecurityAdministration))
				throw new SecurityException($@"Operator {OperatorId} is not security admin for department {departmentId.Value}");
		}
		void IWebPersonalServer.DeleteOperator(int operatorIdToDelete)
		{
			using (var entities = new Entities())
			{
				CheckAdministrativeRightsOnDepartment(entities);

				var departmentId = ComputedDepartmentId;
				if (departmentId == null)
					throw new InvalidOperationException("Department is not specified");

				if (!entities.v_operator_department_right
					.Any(odr =>
						odr.operator_id == operatorIdToDelete &&
						odr.Department_ID == departmentId.Value &&
						odr.right_id == (int)SystemRight.DepartmentsAccess))
					throw new SecurityException($"Operator {operatorIdToDelete} could not be removed because it does not belongs department {departmentId.Value}");

				var ormOperator = entities.GetDepartmentOperatorsQuery(departmentId.Value)
					.FirstOrDefault(o => o.OPERATOR_ID == operatorIdToDelete);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException("operatorIdToDelete", operatorIdToDelete,
						$@"Operator does not exist with id specified in the department {departmentId.Value}");

				var ormDepartment = entities.GetDepartment(departmentId.Value);
				entities.DeleteOperatorFromDepartment(ormOperator, ormDepartment);

				// Excluding possible operator rights to vehicles.
				var ovs = entities.OPERATOR_VEHICLE
					.Where(x => x.OPERATOR_ID == operatorIdToDelete && x.ALLOWED);
				foreach (var ov in ovs)
					ov.ALLOWED = false;

				// Excluding possible operator rights to vehicle_groups.
				var ovgs = entities.OPERATOR_VEHICLEGROUP
					.Where(x => x.OPERATOR_ID == operatorIdToDelete && x.ALLOWED);
				foreach (var ovg in ovgs)
					ovg.ALLOWED = false;

				var ocs = entities.Operator_Contact
					.Where(oc => oc.Operator_ID == operatorIdToDelete && oc.Confirmed);
				foreach (var oc in ocs)
					oc.Confirmed = false;

				ormOperator.Asid = null;

				// deleting operator. Proudly renaming it and disabling.
				ormOperator.LOGIN = null;

				entities.SaveChangesBySession(SessionId);
			}
		}
		public Rht::Operator GetOperatorDetails(int operatorId)
		{
			using (var entities = new Entities())
			{
				if (operatorId != OperatorId)
				{
					CheckAdministrativeRightsOnDepartment(entities);
				}

				var o = entities.GetOperator(operatorId, OPERATOR.AsidIncludePath);

				if (o == null)
					return null;

				var result = new Rht::Operator
				{
					id = operatorId,
					name = o.NAME,
					login = o.LOGIN
				};
				if (operatorId == OperatorId ||
					entities.GetDepartmentOperatorsQuery(ComputedDepartmentId.Value)
						.Any(x => x.OPERATOR_ID == operatorId))
				{
					var operatorEmails = entities.GetOperatorEmails(operatorId);
					var operatorPhones = entities.GetOperatorPhones(operatorId);
					var hasMaskedPhone = o.Asid != null;

					result.HasMaskedPhone = hasMaskedPhone;
					result.mandatoryPhone = o.MandatoryPhone;
					result.mandatoryEmail = o.MandatoryEmail;
					result.Phones = operatorPhones;
					result.Emails = operatorEmails;
				}

				return result;
			}
		}
		/// <summary> Сохранение изменения информации по оператору </summary>
		/// <param name="operInfo"> информации по оператору </param>
		/// <returns></returns>
		SetNewLoginResult IWebPersonalServer.SaveOperatorInfo(Rht::Operator operInfo)
		{
			if (IsGuest)
				return SetNewLoginResult.Forbidden;

			var res = SetNewLoginResult.Success;

			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new SecurityException("Department is not assigned");

			// Проверяем права текущего оператора на изменение оператора
			var departments = ((IWebPersonalServer)this).GetAvailableDepartments();
			if (operInfo.id != OperatorId && (
				departments == null || departments.FindAll(department => department.id == departmentId.Value).Count == 0))
				throw new SecurityException("No admin rights for department");

			using (var entities = new Entities())
			{
				if (operInfo.id != OperatorId)
					CheckAdministrativeRightsOnDepartment(entities);

				var ormOperator =
					operInfo.id != OperatorId
						? entities.GetDepartmentOperatorsQuery(ComputedDepartmentId.Value)
							.FirstOrDefault(o => o.OPERATOR_ID == operInfo.id)
						: entities.GetOperator(OperatorId);

				if (ormOperator == null)
					throw new ArgumentOutOfRangeException(
						nameof(operInfo), operInfo,
						$@"Operator {operInfo.id} does not exist with id specified in the department {departmentId.Value}");

				//если поменял логин
				if (operInfo.login != ormOperator.LOGIN)
					res = SetNewLogin(operInfo.id, operInfo.login);

				//Если меняли простые атрибуты
				if (operInfo.name != ormOperator.NAME)
				{
					ormOperator.NAME = operInfo.name;
					entities.SaveChangesBySession(SessionId);
				}

				if (!string.IsNullOrEmpty(operInfo.pwd) && operInfo.pwd != ormOperator.PASSWORD)
				{
					SetPassword(operInfo.pwd, operInfo.id);
				}
			}

			return res;
		}
		/// <summary> Изменение логина оператора </summary>
		/// <param name="changeOperatorId">Оператор, которому меняют логин</param>
		/// <param name="login">новый логин</param>
		/// <param name="manual">Логин задаётся пользователем вручную</param>
		/// <returns></returns>
		public SetNewLoginResult SetNewLogin(int changeOperatorId, string login, bool manual = true)
		{
			return Server.Instance().SetNewLogin(OperatorId, changeOperatorId, login, manual, SaveContext.Session(SessionId));
		}
	}
}