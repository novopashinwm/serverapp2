﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using Contact = FORIS.TSS.EntityModel.Contact;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		public void SendNotificationsFromPhoneBook(int operatorId)
		{
			using (var entities = new Entities())
			{
				var ownMsisdn = entities.GetOwnMsisdn(operatorId);
				if (string.IsNullOrEmpty(ownMsisdn))
					return;

				var phoneBookEntity = entities.Operator_PhoneBook.FirstOrDefault(b => b.OperatorId == operatorId);
				if (phoneBookEntity == null)
					return;

				if (phoneBookEntity.IsNotified)
					return;

				var ns = new XmlSerializerNamespaces();
				ns.Add("", "");
				var serializer = new XmlSerializer(typeof(PhoneBook));
				var bytes = Encoding.UTF8.GetBytes(phoneBookEntity.PhoneBook);
				var phoneBook = (PhoneBook)serializer.Deserialize(new MemoryStream(bytes));
				if (phoneBook.Contacts == null)
					return;

				var operatorsFromMyBook =
					entities
						.Operator_Contact
						.Where(
							c =>
								c.Confirmed &&
								c.Contact.Type == (int)ContactType.Phone &&
								c.Operator_ID != operatorId)
						.Select(c => new { c.OPERATOR, Phone = c.Contact.Value })
						.ToArray();

				var phones = phoneBook.Contacts.Select(c => c.Value).ToArray();
				operatorsFromMyBook = operatorsFromMyBook.Where(x => phones.Contains(x.Phone)).ToArray();

				var operatorIds = GetOperatorsByPhoneBookContact(ownMsisdn);
				var operatorsWithMeInBook = operatorIds.Any()
					? entities.OPERATOR.Where(o => operatorIds.Contains(o.OPERATOR_ID)).ToArray()
					: new OPERATOR[0];

				var operatorsToNotify = operatorsFromMyBook.Where(myOperator =>
					operatorsWithMeInBook.Any(@operator =>
						@operator.OPERATOR_ID == myOperator.OPERATOR.OPERATOR_ID))
					.ToArray();

				if (operatorsToNotify.Any())
				{
					var notifications = operatorsToNotify.SelectMany(o =>
					{
						OPERATOR @operator = o.OPERATOR;
						var messages = entities.CreateAppNotification(@operator.OPERATOR_ID, null, null);
						messages.Add(entities.CreateWebNotification(@operator.OPERATOR_ID, null, null));
						return messages;
					}).ToArray();
					foreach (var notification in notifications)
					{
						notification.MESSAGE_TEMPLATE = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.FriendWasRegisteredNotification);
						entities.AddMessageField(notification, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, ownMsisdn);
					}
				}

				phoneBookEntity.IsNotified = true;
				entities.SaveChangesByOperator(operatorId);
			}
		}
		public string GetFriendName(int operatorId, int friendOperatorId)
		{
			using (var entities = new Entities())
			{
				var friendPhone = entities.GetOperatorConfirmedPhone(friendOperatorId);
				if (friendPhone == null)
				{
					return entities.GetOperatorOwnName(friendOperatorId);
				}

				var pb = DbManager.SearchPhoneBookContacts(operatorId, new[] { friendPhone.Value });
				if (pb == null)
					return entities.GetOperatorOwnName(friendOperatorId);

				var result = pb.Where(bp => bp.Value == friendPhone.Value).Select(bp => bp.Name).FirstOrDefault();

				if (string.IsNullOrWhiteSpace(result))
					return friendPhone.Value;

				return result;
			}
		}
		private CultureInfo GetOperatorCulture(OPERATOR @operator)
		{
			if (!@operator.CultureReference.IsLoaded)
				@operator.CultureReference.Load();
			var cultureCode = @operator.Culture != null ? @operator.Culture.Code : null;
			if (cultureCode == null)
				return CultureInfo.InvariantCulture;
			return CultureInfo.InvariantCulture;
		}
		public SetNewLoginResult SetNewLogin(int operatorId, int changeOperatorId, string login, bool manual, SaveContext saveContext)
		{
			//1. Check for login rules
			if (!IsLoginValid(login, manual))
				return SetNewLoginResult.InvalidLogin;

			OPERATOR currentOperator;
			using (var entities = new Entities())
			{
				var oldLogin =
					entities.OPERATOR.Where(o => o.OPERATOR_ID == changeOperatorId)
						.Select(o => o.LOGIN)
						.FirstOrDefault();

				using (var transaction = new Transaction())
				{
					var existingOperatorWithSameLogin = (from o in entities.OPERATOR
														 where o.LOGIN == login
														 select o).FirstOrDefault();

					if (existingOperatorWithSameLogin != null)
					{
						if (existingOperatorWithSameLogin.OPERATOR_ID != changeOperatorId)
						{
							transaction.Complete();
							return SetNewLoginResult.LoginAlreadyExists;
						}

						//Проверяем на строгое совпадение старого и нового логина одного и того же оператора.
						//Возможно, оператор пытается сменить регистр каких-то символов своего логина
						if (existingOperatorWithSameLogin.LOGIN == login)
						{
							transaction.Complete();
							return SetNewLoginResult.Success;
						}
					}

					currentOperator = (from o in entities.OPERATOR
									   where o.OPERATOR_ID == changeOperatorId
									   select o).First();

					if (ContactHelper.IsValidEmail(login))
					{
						if (!entities.Operator_Contact.Any(
							oc => oc.Confirmed &&
								  oc.Operator_ID == changeOperatorId &&
								  oc.Contact.Type == (int)ContactType.Email &&
								  oc.Contact.Value == login))
						{
							transaction.Complete();
							return SetNewLoginResult.LoginAsEmailDoesNotMatchToEmail;
						}
					}

					currentOperator.LOGIN = login;

					entities.SaveChanges(saveContext);

					transaction.Complete();
				}
			}

			return SetNewLoginResult.Success;
		}
		private static readonly Regex LoginRegex = new Regex(@"[a-zA-Zа-яА-Я0-9\-_]+");
		/// <summary> Проверяет валидность логина - соответствие правилам системы </summary>
		/// <param name="login">Логин для проверки</param>
		/// <param name="manual">Логин задаётся пользователем вручную</param>
		internal static bool IsLoginValid(string login, bool manual = true)
		{
			if (32 < login.Length)
				return false;
			if (!string.Equals(LoginRegex.Match(login).Value, login, StringComparison.OrdinalIgnoreCase))
				return false;
			// Убрано, т.к. стараемся, чтобы все регистрировались по своему номеру телефона
			//if (manual && login.All(char.IsDigit))
			//	return false;
			if (login.StartsWith("_"))
				return false;

			return true;
		}
		public string GetOperatorPhone(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities.GetOperatorConfirmedPhone(operatorId)?.Value;
			}
		}
		public void SetAppClientContact(string appId, ContactType type, string value)
		{
			using (var entities = new Entities())
			{
				Contact appContact = null;

				if (!string.IsNullOrWhiteSpace(value))
					appContact = entities.GetContact(type, value);

				if (appContact != null)
				{
					if (appContact.LockedUntil.HasValue)
					{
						appContact.LockedUntil = null;
						appContact.LockedCount = 0;
						appContact.Valid = true;
					}

					foreach (var app in entities.AppClient.Where(app => app.Contact_ID == appContact.ID && app.AppId != appId))
					{
						if (app.EntityState == EntityState.Deleted)
							continue;
						entities.AppClient.DeleteObject(app);
					}

					entities.SaveChangesWithHistory();
				}

				var appClient = entities.AppClient
					.Include(AppClient.ContactIncludePath)
					.Include(AppClient.OPERATORIncludePath)
					.FirstOrDefault(x => x.AppId == appId);
				if (appClient == null)
				{
					appClient = new AppClient
					{
						AppId = appId
					};
					entities.AppClient.AddObject(appClient);
				}

				appClient.Contact = appContact;

				entities.SaveChangesWithHistory();
			}
		}
	}
}