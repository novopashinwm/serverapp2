﻿using System;
using System.Linq;
using System.Security;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using Interfaces.Web;
using Interfaces.Web.Enums;
using Contact = FORIS.TSS.BusinessLogic.DTO.Contact;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public SendConfirmationCodeResult SendConfirmationCode(int contactId, string subjectFormatString, string messageFormatString)
		{
			return Server.Instance().SendConfirmationCode(
				OperatorId, contactId, messageFormatString, subjectFormatString, SessionInfo.IP.ToString(), SaveContext.Session(SessionId));
		}
		public Contact[] GetOperatorEmails(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities.GetOperatorEmails(operatorId);
			}
		}
		public VerifyContactResult VerifyContact(int contactId, string confirmationCode)
		{
			if (IsGuest)
				throw new SecurityException("Verifying is forbidden for guest");

			var result = Server.Instance().VerifyContact(OperatorId, contactId, confirmationCode, SessionInfo.IP.ToString(), SaveContext.Session(SessionId));

			return result;
		}
		public VerifyContactResult VerifyMsisdn(string msisdn, string confirmationCode)
		{
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);
			if (string.IsNullOrWhiteSpace(msisdn))
				return SetNewContactResult.InvalidFormat;

			int contactId;
			using (var entities = new Entities())
				contactId = entities.GetContactId(ContactType.Phone, msisdn);
			return VerifyContact(contactId, confirmationCode);
		}
		public SetNewContactResult AddContact(ContactType type, string value, out int addedContactId)
		{
			if (!Server.TypesOfUserDefinedContacts.Contains(type))
				throw new ArgumentOutOfRangeException("type", type, @"Contact types " + string.Join(", ", Server.TypesOfUserDefinedContacts) + @" are only supported");

			if (value == null)
				throw new ArgumentNullException("value");

			value = value.Trim();

			addedContactId = 0;
			if (!((type == ContactType.Email && ContactHelper.IsValidEmail(value)) ||
				(type == ContactType.Phone && ContactHelper.IsValidPhone(value))))
				return SetNewContactResult.InvalidFormat;

			var normalizedValue = type == ContactType.Phone ? ContactHelper.GetNormalizedPhone(value) : value;

			if (normalizedValue == null)
				return SetNewContactResult.InvalidFormat;

			using (var entities = new Entities())
			{
				var contactId = entities.GetContactId(type, normalizedValue);

				if (entities.Operator_Contact.Any(oc => oc.Operator_ID == OperatorId && oc.Contact_ID == contactId))
				{
					addedContactId = contactId;
					return SetNewContactResult.AlreadyExists;
				}

				var operatorContact = new Operator_Contact
					{
						Contact_ID = contactId,
						Operator_ID = OperatorId,
						Name = string.Empty,
						Description = string.Empty,
						Confirmed = false,
						Value = value
					};

				entities.Operator_Contact.AddObject(operatorContact);
				entities.SaveChangesBySession(SessionId);

				addedContactId = contactId;

				return SetNewContactResult.Success;
			}
		}
		public AddResult<Contact> AddContact(ContactType type, string value)
		{
			int addedContactId;
			var addContactResult = AddContact(type, value, out addedContactId);
			var contact = GetContacts(type).FirstOrDefault(c => c.Id == addedContactId);
			if (contact == null)
				addContactResult = SetNewContactResult.Failed;

			var result = new AddResult<Contact>
			{
				Result = addContactResult,
				ResultText = ResourceContainers.Get(CultureInfo)[addContactResult],
				Value = contact
			};
			return result;
		}
		public SetNewContactResult DeleteContact(int contactId)
		{
			using (var entities = new Entities())
			{
				var operatorContact = entities.Operator_Contact.FirstOrDefault(
					oc =>
						oc.Contact_ID == contactId &&
						oc.Operator_ID == OperatorId);

				if (operatorContact == null)
					return SetNewContactResult.Success;

				if (!operatorContact.Removable)
					return SetNewContactResult.ContactIsNotRemovable;

				entities.Delete(operatorContact);
				entities.SaveChangesBySession(SessionId);
				return SetNewContactResult.SuccessfullyDeleted;
			}
		}
		public void UpdatePhoneBookContactName(string msisdn, string value)
		{
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);
			using (var entities = new Entities())
			{
				entities.UpdatePhoneBookContact(msisdn, value, OperatorId);
			}
		}
		public void SetContactComment(int id, string value)
		{
			using (var entities = new Entities())
			{
				var ormOperatorContact = entities.Operator_Contact
					.FirstOrDefault(oc =>
						oc.Operator_ID == OperatorId &&
						oc.Contact_ID  == id);
				if (ormOperatorContact == null)
					return;
				ormOperatorContact.Description = value;
				entities.SaveChangesBySession(SessionId);
			}
		}
		public Contact[] GetContacts(ContactType contactType)
		{
			using (var entities = new Entities())
			{
				return entities.GetOperatorContacts(OperatorId, contactType);
			}
		}
		public Contact[] GetOperatorPhones(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities.GetOperatorPhones(operatorId);
			}
		}
		public SendConfirmationCodeResult SendConfirmationCode(ContactType contactType, string value, string subjectFormat, string bodyFormat)
		{
			string normalizedValue;
			switch (contactType)
			{
				case ContactType.Phone:
					normalizedValue = ContactHelper.GetNormalizedPhone(value);
					if (string.IsNullOrWhiteSpace(normalizedValue))
						return SetNewContactResult.InvalidFormat;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(contactType), contactType, @"Value is not supported here");
			}

			using (var entities = new Entities())
			{
				var ownerOperator = entities.Operator_Contact
					.Where(oc =>
						oc.Confirmed                        &&
						oc.Contact.Type == (int)contactType &&
						oc.Value        == normalizedValue)
					.Select(oc => new { oc.Operator_ID })
					.FirstOrDefault();
				if (ownerOperator != null)
				{
					if (ownerOperator.Operator_ID == OperatorId)
						return SetNewContactResult.AlreadyExists;
				}

				if (entities.Operator_Contact.Any(oc =>
					oc.Operator_ID == OperatorId && oc.Confirmed && oc.Contact.Type == (int)contactType))
					return SetNewContactResult.ConfirmedContactAlreadyExists;

				int contactId;
				var addContactResult = AddContact(contactType, normalizedValue, out contactId);
				if (addContactResult != SetNewContactResult.Success &&
					addContactResult != SetNewContactResult.AlreadyExists)
					return addContactResult;

				return SendConfirmationCode(contactId, subjectFormat, bodyFormat);
			}
		}
		public void SetContactName(int id, string value)
		{
			using (var entities = new Entities())
			{
				var ormOperatorContact = entities.Operator_Contact
					.FirstOrDefault(oc =>
						oc.Operator_ID == OperatorId &&
						oc.Contact_ID  == id);
				if (ormOperatorContact == null)
					return;
				ormOperatorContact.Name = value;
				entities.SaveChangesBySession(SessionId);
			}
		}
		public SavePhoneBookResult SavePhoneBook(PhoneBook phoneBook)
		{
			using (var entities = new Entities())
			{
				phoneBook.Filter();

				var ns = new XmlSerializerNamespaces();
				// добавлено, не добавлялся namespace
				ns.Add(string.Empty, string.Empty);
				var existing = entities.Operator_PhoneBook.FirstOrDefault(b => b.OperatorId == OperatorId);
				if (existing?.PhoneBook != null)
				{
					var existingPhoneBook = SerializationHelper.DeserializeXml<PhoneBook>(existing.PhoneBook);
					if (existingPhoneBook != null)
					{
						if (existingPhoneBook.Contacts == null)
							existingPhoneBook.Contacts = new PhoneBookContact[0];
						var missing = existingPhoneBook.Contacts.Except(phoneBook.Contacts).ToArray();
						if (missing.Length != 0)
							phoneBook = new PhoneBook { Contacts = phoneBook.Contacts.Union(missing).ToArray() };
					}
				}

				var xml = SerializationHelper.SerializeXml(phoneBook);

				entities.SavePhoneBook(OperatorId, xml);

				entities.SaveChangesBySession(SessionId);
			}

			Server.Instance().SendNotificationsFromPhoneBook(OperatorId);
			return SavePhoneBookResult.Success;
		}
		public int GetContactId(ContactType contactType, string value)
		{
			using (var entities = new Entities())
			{
				return entities.GetContactId(contactType, value);
			}
		}
	}
}