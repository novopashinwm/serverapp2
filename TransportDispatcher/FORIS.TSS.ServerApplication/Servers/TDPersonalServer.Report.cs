﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Resources;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		private static readonly HashSet<string> ReportsAllowedForPhysicals =
			new HashSet<string>(StringComparer.OrdinalIgnoreCase)
			{
				//История движения
				"F824B9A6-F435-4940-9788-1C46495BB73F",
				//Прохождение геозон
				//"6E2DE7AD-2656-4812-BC1C-0EEA32613662" - Временно убран, т.к. слишком много "автомобильного" в этом отчёте,
				//Суточный пробег
				"B4C4E567-3CA3-4252-BC9E-E9949FA9C7A7",
				//Отчет о превышении скорости
				"E3C8AAD3-9682-48FB-A2B4-F5C45927797C",
				//Отчеты по LBS
				"B939736A-7B95-4D0B-B59C-6A126D310E70",
				"873109F4-D52A-40C5-B095-6CE759A17469",
				"D2299B07-03BA-4C17-99BB-7C51F0CDEC3B",
			};

		private static readonly HashSet<string> SalesReports =
			new HashSet<string>(StringComparer.OrdinalIgnoreCase)
			{
				"6690093C-C323-450C-B23B-91EFCF404409",
				"2E0725EB-978D-439A-8112-3ECBDBCCD4D3"
			};
		public List<ReportMenuGroup> GetReportMenu()
		{
			List<ReportMenuGroup> groups;
			var reportAvailabilityParams = GetReportMenuCapabilities();
			using (var entities = new Entities())
			{
				var reportMenuItems = entities
				.REPORT
					.Where(r => r.GroupName != null && r.GroupName.Trim() != string.Empty)
					.AsEnumerable()
					// Блокируем временно отключенные в коллекции, но существующие в БД
					.Where(r => Repository.Instance[r.REPORT_GUID] != null)
					.Select(r => CreateReportMenuItem(r, reportAvailabilityParams))
					// Еще одна блокировка, если не удалось создать элемент меню
					.Where(r => null != r && r.GroupType != ReportMenuGroupType.Unknown)
					.OrderBy(r => r.Order)
					.ToList();

				groups = reportMenuItems
					.Select(item => item.GroupType)
					.Distinct()
					.Select(item => CreateReportMenuGroup(item, reportMenuItems, reportAvailabilityParams))
					.OrderBy(group => group.GroupType)
					.ToList();
			}
			return groups;
		}
		private ReportMenuItem CreateReportMenuItem(REPORT r)
		{
			var groupType = default(ReportMenuGroupType);
			if (null == Repository.Instance[r.REPORT_GUID])
				return null;
			var menuItem = new ReportMenuItem
			{
				Id                    = r.REPORT_ID,
				Name                  = r.Name,
				GroupType             = Enum.TryParse(r.GroupName, true, out groupType)
					? groupType
					: ReportMenuGroupType.Unknown,
				Guid                  = r.REPORT_GUID.ToString(),
				AssemblyName          = r.AssemblyName,
				ClassParametersName   = r.ClassParametersName,
				ClassResourcesName    = r.ClassResourcesName,
				IsAvailable           = true,
				IsSubscriptionVisible = true,
				Selected              = false,
				Order                 = Repository.Instance[r.REPORT_GUID].Order,
			};
			return menuItem;
		}
		private ReportMenuItem CreateReportMenuItem(REPORT r, ReportMenuVehiclesCapabilities reportAvailabilityParams)
		{
			var reportMenuItem = CreateReportMenuItem(r);
			reportMenuItem.IsAvailable = GetReportAvailability(reportMenuItem, reportAvailabilityParams);
			if (string.IsNullOrEmpty(reportMenuItem.AssemblyName))
				return reportMenuItem;
			var assemblyName        = reportMenuItem.AssemblyName;
			var classParametersName = reportMenuItem.ClassParametersName;

			if (string.IsNullOrWhiteSpace(assemblyName) || string.IsNullOrWhiteSpace(classParametersName))
				return reportMenuItem;
			var report = Repository.Instance[Guid.Parse(reportMenuItem.Guid)];
			if (report == null)
				return reportMenuItem;
			reportMenuItem.Name        = report.GetReportName(CultureInfo);
			reportMenuItem.Description = report.GetReportDesc(CultureInfo);

			return reportMenuItem;
		}
		private ReportMenuGroup CreateReportMenuGroup(ReportMenuGroupType groupType, IEnumerable<ReportMenuItem> reportMenuItems, ReportMenuVehiclesCapabilities reportAvailabilityParams)
		{
			var reportGroupMenuItems = reportMenuItems
				.Where(x => x.GroupType == groupType)
				.ToList();
			var reportMenuGroup = new ReportMenuGroup
			{
				//TODO: Убрать костыль, сделать группы и матрицу доступа для отчетов
				Name            = GetGroupName(groupType),
				GroupType       = groupType,
				IsAvailable     = GetReportGroupAvailability(groupType, reportAvailabilityParams) && reportGroupMenuItems.Any(r => r.IsAvailable),
				ReportMenuItems = reportGroupMenuItems
			};
			return reportMenuGroup;
		}
		/// <summary> Метод используется для локализации групп отчетов в зависимости от типа клиента </summary>
		/// <param name="groupType"></param>
		/// <returns></returns>
		private string GetGroupName(ReportMenuGroupType groupType)
		{
			var groupName = groupType.ToString();
			switch (groupType)
			{
				case ReportMenuGroupType.MlpReports:
				case ReportMenuGroupType.GpsReports:
					return groupName + ComputedDepartmentType;
			}
			return groupName;
		}
		public ReportMenuItem GetReportMenuItem(Guid reportGuid)
		{
			var report = Repository.Instance[reportGuid];
			if (report == null)
				return null;
			ReportMenuItem menuItem;
			using (var entities = new Entities())
			{
				var reportEntity = entities.REPORT.FirstOrDefault(r => r.REPORT_GUID == reportGuid);
				if (reportEntity == null)
					return null;
				menuItem = CreateReportMenuItem(reportEntity);
			}

			menuItem.Name                  = report.GetReportName(CultureInfo);
			menuItem.Description           = report.GetReportDesc(CultureInfo);
			menuItem.IsSubscriptionVisible = report.IsSubscriptionVisible;

			// Общие для всех отчетов локализованные строки.
			var resCommon = ReportHelper.GetResourceStringContainer(CultureInfo);
			// Локализованные строки отчета.
			var resReport = report.GetResourceStringContainer(CultureInfo);

			var basicFunctionSet = Server.Instance();
			report.IBasicFunctionSetInstanceSet(basicFunctionSet);
			var settings = GetReportParametersSettings();
			var reportParameters = report.ReportParametersInstanceGet(settings);
			reportParameters.OperatorId = settings.OperatorId;
			var properties = reportParameters.GetType().GetProperties();
			var acceptedPropertiesNames = reportParameters.GetParametersForView(basicFunctionSet);
			if (acceptedPropertiesNames != null)
			{
				properties = properties
					.Where(property =>
						acceptedPropertiesNames
							.Any(item => property.Name == item))
					.ToArray();
			}
			menuItem.Formats = report.GetSupportedFormats().ToArray();

			foreach (var p in properties)
			{
				var name    = Attribute.GetCustomAttribute(p, typeof(DisplayNameAttribute), false) as DisplayNameAttribute ?? new DisplayNameAttribute(p.Name);
				var type    = Attribute.GetCustomAttribute(p, typeof(TypeAttribute)       , false) as TypeAttribute        ?? new TypeAttribute(p.PropertyType);
				var control = Attribute.GetCustomAttribute(p, typeof(ControlTypeAttribute), false) as ControlTypeAttribute;
				var order   = Attribute.GetCustomAttribute(p, typeof(OrderAttribute),       false) as OrderAttribute;
				var options = Attribute.GetCustomAttribute(p, typeof(OptionsAttribute),     false) as OptionsAttribute;

				if (null == name || null == type || null == control)
					continue;

				// Добавляем элементы управления
				var rmp = new ReportMenuProperty
				{
					Name    = name.DisplayName,
					Type    = type.Type,
					Control = control.Type,
					Order   = order?.Value ?? 0
				};

				// ..их свойства..
				if (null != options)
				{
					rmp.Options = string.Empty;
					foreach (var pair in options.KeyValuePairCollection)
						rmp.Options += string.Format("{0}:{1};", pair.Key, pair.Value);
					rmp.Options = rmp.Options.TrimEnd(';');
					rmp.Rights  = options.Rights;
				}

				// ..локализация, либо описание.
				try
				{
					rmp.Description = resReport?.GetStringOrDefault(control.Description)
						?? resCommon?.GetStringOrDefault(control.Description)
							?? control.Description;

					menuItem.Properties.Add(rmp);
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);
				}
			}

			return menuItem;
		}
		public void DeleteSubscription(int subscriptionId)
		{
			using (var entities = new Entities())
			{
				var subscription = entities
					.SCHEDULERQUEUE.FirstOrDefault(
						sq => sq.SCHEDULERQUEUE_ID == subscriptionId && sq.Operator_ID == OperatorId);

				if (subscription == null)
					return;

				entities.Delete(subscription);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public ReportMenuVehiclesCapabilities GetReportMenuCapabilities()
		{
			var result = ((IBasicFunctionSet)Server.Instance())
				.GetReportMenuCapabilities(OperatorId, FilterByDepartment ? ComputedDepartmentId : null);
			var operatorSystemRights = GetOperatorSystemRights(OperatorId);
			result.IsPathAccess = operatorSystemRights.Contains(SystemRight.PathAccess);
			return result;
		}
		private bool GetReportGroupAvailability(ReportMenuGroupType groupType, ReportMenuVehiclesCapabilities availabilityParams)
		{
			var available = true;
			switch (groupType)
			{
				case ReportMenuGroupType.CommonReports:
				case ReportMenuGroupType.GeoPointReports:
				case ReportMenuGroupType.DrivingSafety:
					break;
				case ReportMenuGroupType.MlpReports:
					available &= availabilityParams.IsAskPositionAvailable;
					break;
				case ReportMenuGroupType.GpsReports:
					available &= availabilityParams.HasSpeedMonitoring;
					break;
				case ReportMenuGroupType.SensorsReports:
					available &= availabilityParams.HasDigitalSensors;
					break;
				case ReportMenuGroupType.GeocodingViaExcelReports:
					available &= availabilityParams.IsPathAccess;
					break;
			}
			return available;
		}
		private bool GetReportAvailability(ReportMenuItem reportMenuItem, ReportMenuVehiclesCapabilities availabilityParams)
		{
			var available = true;

			const string whoAskedMeGuid = "873109F4-D52A-40C5-B095-6CE759A17469";
			if (reportMenuItem.Guid == whoAskedMeGuid)
				available &= availabilityParams.IsWhoAskedMeAvailable && availabilityParams.IsAskPositionAvailable;

			var withIgnitionReports = new[] { "reportGroupFuelCost", "reportFuelCost", "fuelCostCalculation" };
			if (withIgnitionReports.Contains(reportMenuItem.Name))
				available &= availabilityParams.IsIginitionMonitoringAvailable;

			//if (IsPhysicalCustomer())
			//	available &= ReportsAllowedForPhysicals.Contains(reportMenuItem.Guid);

			if (SalesReports.Contains(reportMenuItem.Guid))
				available &= availabilityParams.IsSalesAvailable;

			return available;
		}
		private TssReportParametersSettings GetReportParametersSettings()
		{
			return new TssReportParametersSettings
			{
				OperatorId     = OperatorId,
				DepartmentId   = ComputedDepartmentId,
				DepartmentType = ComputedDepartmentType,
			};
		}
	}
}