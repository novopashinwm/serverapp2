﻿using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		public override string GetConstant(Constant constant)
		{
			using (var entities = new Entities())
			{
				return entities.GetConstant(constant);
			}
		}
		public void SetConstant(Constant constant, string value)
		{
			using (var entities = new Entities())
			{
				entities.SetConstant(constant, value);
				entities.SaveChanges();
			}
		}
		public int? GetConstantAsInt(Constant constant)
		{
			using (var entities = new Entities())
			{
				return entities.GetConstantAsInt(constant);
			}
		}
		public void SetConstant(Constant constant, int value)
		{
			using (var entities = new Entities())
			{
				entities.SetConstant(constant, value);
				entities.SaveChanges();
			}
		}
		public void SetConstant(Constant constant, long value)
		{
			using (var entities = new Entities())
			{
				entities.SetConstant(constant, value);
				entities.SaveChanges();
			}
		}
		public string[] GetConstantAsStringList(Constant constant)
		{
			var constantValue = GetConstant(constant);
			if (constantValue == null)
				return new string[0];
			return JsonHelper.DeserializeObjectFromJson<string[]>(constantValue);
		}
		public void SetConstant(Constant constant, string[] value)
		{
			SetConstant(constant, value != null ? JsonHelper.SerializeObjectToJson(value) : null);
		}
		public long? GetConstantAsLongInt(Constant constant)
		{
			using (var entities = new Entities())
			{
				return entities.GetConstantAsLong(constant);
			}
		}
		public void SetConstant(Constant constant, bool value)
		{
			using (var entities = new Entities())
			{
				entities.SetConstant(constant, value);
			}
		}
		public bool GetConstantAsBool(Constant constant)
		{
			using (var entities = new Entities())
			{
				return entities.GetConstantAsBool(constant);
			}
		}
	}
}