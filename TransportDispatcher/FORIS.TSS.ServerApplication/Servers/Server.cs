﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Xml;
using Compass.Ufin.Orders.Core;
using Compass.Ufin.Orders.Services;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.Protection;
using FORIS.TSS.Resources;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using FORIS.TSS.ServerApplication.DataFactory.Rule;
using FORIS.TSS.ServerApplication.Exceptions;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.WorkplaceShadow.Geo;
using FORIS.TSS.WorkplaceShadow.Terminal;
using FORIS.TSS.WorkplaceSharnier;
using Microsoft.Extensions.DependencyInjection;
using Res = FORIS.TSS.BusinessLogic.ResultCodes;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using Severity = FORIS.TSS.Terminal.Severity;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Сервер системы TSS </summary>
	public partial class Server :
		BasicFunctionSetImpl,
		IServer
	{
		/// <summary> Событие наличия данных у терминала сервера </summary>
		public event ReceiveEventHandler ReceiveEvent;
		/// <summary> Событие наличия сообщений у терминала </summary>
		public event NotifyEventHandler  NotifyEvent;
		/// <summary> Ссылка на экземпляр самого себя </summary>
		private static readonly Server   instance = null;

		/// <summary> Флаг активности менеджера терминалов </summary>
		public bool IsActive
		{
			get { return null != _terminalClient; }
		}

		/// <summary> Менеджер базы данных </summary>
		public CoreServerDatabaseManager DbManager
		{
			get { return databaseManager; }
		}

		#region IDatabaseProvider Members

		public IDatabaseDataSupplier Database
		{
			get { return databaseManager; }
		}

		#endregion IDatabaseProvider Members

		/// <summary> Словарь отслеживаемых мобильных объектов с последней информацией о их состоянии </summary>
		private readonly ConcurrentDictionary<int, IMobilUnit> _mobilUnits = new ConcurrentDictionary<int, IMobilUnit>();

		/// <summary> Константный набор прав на редактирование администратором прав оператора на транспортное средство </summary>
		private readonly object LockObject = new object();
		private volatile SystemRightsReplacer _vehicleAcessRules;
		public SystemRightsReplacer VehicleSystemRightsReplacer
		{
			get
			{
				if (_vehicleAcessRules == null)
				{
					lock (LockObject)
					{
						if (_vehicleAcessRules == null)
						{
							return _vehicleAcessRules = new SystemRightsReplacer(new[]
							{
								///////////////////////////////////////////////////////////////////////////////////////////////////////////////
								// Корпоративный клиент
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.VehicleAccess,            DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.PathAccess,               DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditVehicles,             DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.Immobilization,           DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ChangeDeviceConfigBySMS,  DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditGroup,                DepartmentType.Corporate),
								// Корпоративный клиент (дополнение 1)
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ViewingTrackerAttributes, DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditingTrackerAttributes, DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.CommandAccess,            DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ShareLinkAccess,          DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ManageSensors,            DepartmentType.Corporate),
								// Корпоративный клиент (дополнение 2)
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ViewingSensorValues,      DepartmentType.Corporate),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EstimatedTimeArrival,     DepartmentType.Corporate),
								// Право необходимое партнеру для передачи прав пользователю партнерского клиента, далее нужно фильтровать по оператору
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.SecurityAdministration,   DepartmentType.Corporate),
								///////////////////////////////////////////////////////////////////////////////////////////////////////////////
								// Индивидуальный клиент
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.VehicleAccess,            DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.PathAccess,               DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditVehicles,             DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.Immobilization,           DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ChangeDeviceConfigBySMS,  DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditGroup,                DepartmentType.Physical),
								// Индивидуальный клиент (дополнение 1)
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ViewingTrackerAttributes, DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EditingTrackerAttributes, DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.CommandAccess,            DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ShareLinkAccess,          DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ManageSensors,            DepartmentType.Physical),
								// Индивидуальный клиент (дополнение 2)
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.ViewingSensorValues,      DepartmentType.Physical),
								SystemRightReplacer.ForVehicleAndVehicleGroup(SystemRight.EstimatedTimeArrival,     DepartmentType.Physical),
								///////////////////////////////////////////////////////////////////////////////////////////////////////////////
							});
						}
					}
				}

				return _vehicleAcessRules;
			}
		}

		/// <summary> Массив идентификаторов ТС, которые находятся под охраной </summary>
		private new System.ComponentModel.IContainer components;

		public static readonly SensorLegend[] VideoAnalyticsSensors = new[]
		{
			SensorLegend.MotionAlarm,
			SensorLegend.LineCrossed,
			SensorLegend.FaceDetected,
			SensorLegend.AbandonedAlarm,
			SensorLegend.LoiteringAlarm
		};


		/// <summary> Название сервера </summary>
		public string Name
		{
			get { return "TSS Server"; }
		}

		/// <summary> Блокировка для доступа к серверу правил </summary>
		private object lockRuleManagerServer = new object();

		#region .ctor

		static Server()
		{
			instance = new Server();
		}
		public IServiceProvider Services { get; private set; }

		/// <summary> Конструктор TSS сервера </summary>
		public Server()
		{
			AppDomain.CurrentDomain.DomainUnload += delegate
			{
				Stop();
			};

			InitializeComponent();

			Trace.TraceInformation("CurrentDomain: " + AppDomain.CurrentDomain.FriendlyName);

			_updateTrackerPropertiesEnabled = "true".Equals(ConfigurationManager.AppSettings["Server._updateTrackerPropertiesEnabled"], StringComparison.OrdinalIgnoreCase);

			#region DependencyInjection

			// Настройка сервера (настройка функций через DependencyInjection)
			var serviceCollection = new ServiceCollection().AddOptions();
			ConfigureServices(serviceCollection);
			Services = serviceCollection.BuildServiceProvider();

			#endregion DependencyInjection

			string strTsReconnectInterval = GlobalsConfig.AppSettings["TsRecconectInterval"];
			if (!string.IsNullOrEmpty(strTsReconnectInterval))
			{
				int ri;
				if (int.TryParse(strTsReconnectInterval, out ri))
				{
					_terminalServerEventsMissingReconnectIntertval = ri * 1000;
					_terminalServerEventsMissingReconnectTimer     = new Timer(TryReconnectByTimer, "Terminal Server events have long been missing", _terminalServerEventsMissingReconnectIntertval, Timeout.Infinite);
				}
			}

			if (GlobalsConfig.AppSettings["TerminalService"] != null)
			{
				_messageStateChangeNotifier = new BulkProcessor<MessageStateItem>(NotifyTerminalManagerAboutMessageStateChange);
				_terminalServerPinger       = new TerminalServerPinger(this);
			}
		}

		private void TryReconnectByTimer(object o)
		{
			TryReconnect(o as string);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
					components.Dispose();

				if (_terminalServerEventsMissingReconnectTimer != null)
					_terminalServerEventsMissingReconnectTimer.Dispose();

				exitDelegate = null;
			}

			base.Dispose(disposing);
		}

		#endregion .ctor

		#region Instance()

		/// <summary> Создание экземпляра сервера системы TSS </summary>
		/// <returns> Экземпляр сервера системы TSS </returns>
		public static Server Instance()
		{
			return instance;
		}

		#endregion Instance()

		#region Configure()

		private void ConfigureServices(IServiceCollection serviceCollection)
		{
			try
			{
				serviceCollection.AddTransient<IOrdersManagement>(r => new OrdersService(databaseManager.Database.ConnectionString));
			}
			catch (Exception ex)
			{
				$"Unable to complete server configuration"
					.WithException(ex)
					.CallTraceError();
				Stop();
			}
		}

		#endregion Configure()

		#region Start()

		#region HASP
		private       ServerExitEventHandler exitDelegate;
		public  event ServerExitEventHandler Exit
		{
			add    { exitDelegate += value; }
			remove { exitDelegate -= value; }
		}
		private void OnExit(string message)
		{
			exitDelegate?.Invoke(this, message);
		}

		#endregion HASP

		// TODO: Переключать состояние сервера
		private ServerState state = ServerState.Undefined;

		[Browsable(false)]
		[DefaultValue(ServerState.Undefined)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ServerState State
		{
			get { return state; }
		}

		//
		private DataSet VehicleDS = null;

		/// <summary> Старт сервера </summary>
		/// <remarks>В этом методе происходит запуск таймеров, потоков, терминалов и т.д.</remarks>
		public void Start()
		{
			if (null == instance)
				throw new InvalidOperationException("Server.instance must be set to that time");
			try
			{
				#region HASP
				//Поверка количества машин в БД
				if (ProtectionKey.ValidVehiclesCount < int.MaxValue)
				{
					Trace.TraceInformation("Valid Vehicles Count: " + ProtectionKey.ValidVehiclesCount);
					VehicleDS = databaseManager.GetVehicles();
					if (VehicleDS.Tables["VEHICLE"].Rows.Count > ProtectionKey.ValidVehiclesCount)
					{
						OnExit("Protection: Max count of vehicles exceeded!\r\nMax count of vehicles: "
							+ ProtectionKey.ValidVehiclesCount);
					}
				}

				if (ProtectionKey.ValidUsersCount < int.MaxValue)
				{
					Trace.TraceInformation("Valid Users Count: " + ProtectionKey.ValidUsersCount);
					DataSet OperatorDS = databaseManager.GetOperatorGroupsAndOperators();
					if (OperatorDS.Tables["Operator"].Rows.Count > ProtectionKey.ValidUsersCount)
					{
						OnExit("Max count of operators exceeded!\r\nMax count of operators: "
							+ ProtectionKey.ValidUsersCount);
					}
				}

				if (ProtectionKey.ValidControllersCount < int.MaxValue)
				{
					DataSet ControllersDS = databaseManager.GetControllers();
					if (ControllersDS.Tables["CONTROLLER"].Rows.Count > ProtectionKey.ValidControllersCount)
					{
						OnExit("Max count of controllers exceeded!\r\nMax count of controllers: "
							+ ProtectionKey.ValidControllersCount);
					}
				}
				#endregion HASP

				// запуск потока для получения последних позиций
				// Загрузка происходит в самом начале и синхронно, т.к. остальные модули зависят от кэша позиций
				using (new Stopwatcher(CallStackHelper.GetCallerMethodFullNameMessage($"Load last positions")))
					LoadLastPositions();

				if (SmartphonePositionInterviewer.Instance.Enabled)
					SmartphonePositionInterviewer.Instance.Start();

				// launch thread for checking scheduler queue in database
				if (string.Equals(ConfigurationManager.AppSettings["SchedulerEnabled"], "true",
						StringComparison.OrdinalIgnoreCase))
					Scheduler.Instance.Start();

				if (BillingNotifier.Instance.Enabled)
					BillingNotifier.Instance.Start();

				if (string.Equals(ConfigurationManager.AppSettings["NotificationSenderProcessor.Enabled"], "true",
						StringComparison.OrdinalIgnoreCase))
					NotificationSenderProcessor.Instance.Start();

				if (string.Equals(ConfigurationManager.AppSettings["MessageSenderFactoryEnabled"], "true",
								   StringComparison.OrdinalIgnoreCase))
					MessageProcessor.Instance.Start();

				// launch thread for asynchronous command processing
				if (string.Equals(ConfigurationManager.AppSettings["CommandProcessorEnabled"], "true", StringComparison.OrdinalIgnoreCase))
					CommandProcessor.Instance.Start();

				if (string.Equals(ConfigurationManager.AppSettings["ExpiredTrialServiceRemover.Enabled"], "true", StringComparison.OrdinalIgnoreCase))
				{
					_expiredTrialServiceRemover = ExpiredTrialServiceRemover.Instance;
					_expiredTrialServiceRemover.Start();
				}

				// launch thread for rule processing
				if (string.Equals(ConfigurationManager.AppSettings["RuleProcessorEnabled"], "true",
								   StringComparison.OrdinalIgnoreCase))
					RuleProcessor.Instance.Start();

				if (string.Equals(ConfigurationManager.AppSettings["Alarm.Enabled"], "true", StringComparison.OrdinalIgnoreCase))
				{
					AlarmReceived += OnAlarmReceived;
				}

				if (_terminalServerPinger != null)
					_terminalServerPinger.Start();

				using (new Stopwatcher(
					CallStackHelper.GetCallerMethodFullNameMessage($"Connect to TerminalService"),
					(timeSpan) => _terminalClient != null ? "connected to TerminalService" : "unable to connect to TerminalService",
					TimeSpan.Zero))
					ConnectTerminalClient();

				string gpsLimitMaxSpeedSetting = ConfigurationManager.AppSettings["GPSLimitMaxSpeed"];
				if (gpsLimitMaxSpeedSetting != null)
				{
					MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed =
						int.Parse(gpsLimitMaxSpeedSetting);
				}

				UploadNewReportsMetadataToDataBase();

				if (_terminalClient != null)
					SendDiagnosticSMS(Environment.MachineName + ": Сервер TSS запущен.");

				if (_messageStateChangeNotifier != null)
					_messageStateChangeNotifier.Start();

				// лог
				Trace.TraceInformation(GetType().Name + ": Сервер TSS запущен.");
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				throw;
			}
		}

		/// <summary> Создаёт необходимые для работы отчётов записи в таблице отчётов </summary>
		private void UploadNewReportsMetadataToDataBase()
		{
			var repository = TransportDispatcher.Reports.ReportCollection.Repository.Instance;

			using (var entities = new Entities())
			{
				foreach (var report in repository.Reports)
				{
					var guid = report.GetGuid();
					if (guid == Guid.Empty)
						continue;

					var reportEntity = entities
						.REPORT
						.Include("SCHEDULEREVENT")
						.FirstOrDefault(r => r.REPORT_GUID == guid);

					var reportType                = report.GetType();
					var reportTypeName            = reportType.Name;
					var reportTypeAssemblyName    = reportType.Assembly.GetName().Name;
					var reportTypeAssemblyQualifiedNameShort =
						string.Join(", ", report.GetType().AssemblyQualifiedName.Split(',').Select(s => s.Trim()).Take(2).ToArray());
					var reportFileName            = report.GetReportName(CultureInfo.CurrentCulture);
					var reportGroupName           = report.GetGroupName();

					var reportClassParametersName = report.ReportParametersInstanceGet(0).GetType().Name;
					var reportClassResourcesName  = report.GetResourceStringContainer(CultureInfo.CurrentCulture).BaseName;
					if (reportEntity == null)
					{
						reportEntity = new REPORT
						{
							REPORT_GUID         = guid,
							REPORT_FILENAME     = reportFileName,
							GroupName           = reportGroupName,
							Name                = reportTypeName,
							AssemblyName        = reportTypeAssemblyName,
							ClassParametersName = reportClassParametersName,
							ClassResourcesName  = reportClassResourcesName,
						};

						entities.REPORT.AddObject(reportEntity);
					}
					else
					{
						if (reportEntity.REPORT_FILENAME != reportFileName)
							reportEntity.REPORT_FILENAME = reportFileName;

						if (reportEntity.GroupName != reportGroupName)
							reportEntity.GroupName = reportGroupName;

						if (reportEntity.Name != reportTypeName)
							reportEntity.Name = reportTypeName;

						if (reportEntity.AssemblyName != reportTypeAssemblyName)
							reportEntity.AssemblyName = reportTypeAssemblyName;

						if (reportEntity.ClassParametersName != reportClassParametersName)
							reportEntity.ClassParametersName = reportClassParametersName;

						if (reportEntity.ClassResourcesName != reportClassResourcesName)
							reportEntity.ClassResourcesName = reportClassResourcesName;
					}

					if (report.GetSupportedFormats().Any(f => f == ReportTypeEnum.Acrobat || f == ReportTypeEnum.Excel))
					{
						var reportPluginName = "FORIS.TSS.ServerApplication.Mail.GenericReport_SchedulerPlugin, FORIS.TSS.ServerApplication";
						var schedulerEvent = reportEntity
							.SCHEDULEREVENT
							.FirstOrDefault(e => e.PLUGIN_NAME == reportPluginName);
						if (schedulerEvent == null)
						{
							schedulerEvent = new SCHEDULEREVENT
							{
								REPORT      = reportEntity,
								PLUGIN_NAME = reportPluginName,
								COMMENT     = reportTypeName,
								CONFIG_XML  = reportTypeAssemblyQualifiedNameShort,
							};
							entities.SCHEDULEREVENT.AddObject(schedulerEvent);
						}
						else
						{
							if (schedulerEvent.COMMENT != reportTypeName)
								schedulerEvent.COMMENT = reportTypeName;

							if (schedulerEvent.CONFIG_XML != reportTypeAssemblyQualifiedNameShort)
								schedulerEvent.CONFIG_XML = reportTypeAssemblyQualifiedNameShort;

						}
					}
				}
				entities.SaveChanges();
			}
		}
		private void TryReconnect(string reason)
		{
			$"Reconnecting to terminal server because of {reason}"
				.CallTraceWarning();
			try
			{
				DisconnectTerminalClient();
			}
			catch (Exception ex)
			{
				$"Reconnect (disconnect part) exception"
					.WithException(ex)
					.CallTraceError();
			}
			try
			{
				ConnectTerminalClient();
			}
			catch (Exception ex)
			{
				$"Reconnect (connect part) exception"
					.WithException(ex)
					.CallTraceError();
			}
		}
		/// <summary> Отсылка диагностических СМС списку получателей указанному в конфиг файле </summary>
		/// <param name="text">Текст для отправки</param>
		private void SendDiagnosticSMS(string text)
		{
			try
			{
				string strSendDiagnosticSMS = GlobalsConfig.AppSettings["SendDiagnosticSMS"];
				if (string.IsNullOrEmpty(strSendDiagnosticSMS))
					strSendDiagnosticSMS = "OFF";

				bool SendDiagnosticSMS = false;
				if (strSendDiagnosticSMS.ToUpper() == "ON")
					SendDiagnosticSMS = true;
				else if (strSendDiagnosticSMS.ToUpper() == "OFF")
					SendDiagnosticSMS = false;
				if (!SendDiagnosticSMS)
					return;

				for (int i = 1; i <= 10; i++)
				{
					string phoneNikName = "Phone" + i;
					string phone = GlobalsConfig.AppSettings[phoneNikName];

					if (string.IsNullOrEmpty(phone))
						continue;
					_terminalClient.Session.SendSMS(phone, text);
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}

		#endregion Start()

		/// <summary> Остановка сервера </summary>
		public void Stop()
		{
			RuleProcessor.Instance.Stop();
			BillingNotifier.Instance.Stop();
			Scheduler.Instance.Stop();
			CommandProcessor.Instance.Stop();

			_expiredTrialServiceRemover?.Stop();

			_messageStateChangeNotifier?.Stop();

			_terminalServerPinger?.Stop();

			if (SmartphonePositionInterviewer.Instance.Enabled)
				SmartphonePositionInterviewer.Instance.Stop();

			DisconnectTerminalClient();

			Dispose();
		}

		#region Terminal_ReceiveEvent(ReceiveEventArgs args)

		/// <summary> Таймер для переподключения </summary>
		private readonly Timer _terminalServerEventsMissingReconnectTimer;
		private readonly int   _terminalServerEventsMissingReconnectIntertval;
		private readonly bool  _updateTrackerPropertiesEnabled;

		/// <summary> Обработчик события получения данных от терминалов </summary>
		/// <param name="args">данные терминалов</param>
		public void Terminal_ReceiveEvent(ReceiveEventArgs args)
		{
			try
			{
				if (0 < _terminalServerEventsMissingReconnectIntertval)
					_terminalServerEventsMissingReconnectTimer?.Change(_terminalServerEventsMissingReconnectIntertval, Timeout.Infinite);

				AddLastPosition(args.MobilUnits);
				NotifyReceiveEvents(args);
				CheckAlarmAndNotify(args.MobilUnits);

				if (_updateTrackerPropertiesEnabled)
					UpdateTrackerProperties(args.MobilUnits);

			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}
		private void UpdateTrackerProperties(IMobilUnit[] mobilUnits)
		{
			foreach (var mu in mobilUnits)
			{
				foreach (var pair in mu.Properties)
				{
					string lastUpdatedValue;
					if (_lastUpdatedPropertyValue.TryGetValue(new Tuple<int, DeviceProperty>(mu.Unique, pair.Key),
																out lastUpdatedValue) &&
						lastUpdatedValue == pair.Value)
					{
						continue;
					}

					switch (pair.Key)
					{
						case DeviceProperty.IMEI:
							UpdateDeviceId(mu.Unique, pair.Value);
							break;
						case DeviceProperty.Password:
							UpdateDevicePassword(mu.Unique, pair.Value);
							break;
						case DeviceProperty.Firmware:
							UpdateDeviceFirmwareVersion(mu.Unique, pair.Value);
							break;
						case DeviceProperty.IMSI:
							UpdateDeviceImsi(mu.Unique, pair.Value);
							break;
						case DeviceProperty.Protocol:
							UpdateDeviceProtocol(mu.Unique, pair.Value);
							break;
					}
					_lastUpdatedPropertyValue[new Tuple<int, DeviceProperty>(mu.Unique, pair.Key)] = pair.Value;
				}
			}
		}

		private readonly Dictionary<Tuple<int, DeviceProperty>, string> _lastUpdatedPropertyValue =
			new Dictionary<Tuple<int, DeviceProperty>, string>();

		private void UpdateDeviceProtocol(int unique, string value)
		{
			if (string.IsNullOrEmpty(value))
				return;

			using (var entities = new Entities())
			{
				var controller = entities.CONTROLLER.FirstOrDefault(c => c.CONTROLLER_TYPE.TYPE_NAME == ControllerType.Names.Unspecified && c.VEHICLE.VEHICLE_ID == unique);
				if (controller == null)
					return;

				var controllerTypeName = value.Split('|').First();
				var controllerType = entities.CONTROLLER_TYPE.FirstOrDefault(t => t.TYPE_NAME == controllerTypeName);
				if (controllerType == null)
					return;

				controller.CONTROLLER_TYPE = controllerType;
				entities.SaveUnsavedChanges();
			}
		}

		private void UpdateDeviceImsi(int vehicleId, string value)
		{
			using (var entities = new Entities())
			{
				var controllerInfo = entities.CONTROLLER_INFO.FirstOrDefault(
					ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);
				if (controllerInfo == null)
				{
					Trace.TraceWarning(
						"{0}: vehicle {1} cannot set new imsi {2} because controller info is not found",
						this, vehicleId, value);
					return;
				}

				if (controllerInfo.IMSI == value)
					return;

				controllerInfo.IMSI = value;
			}
		}

		private void UpdateDeviceId(int vehicleId, string value)
		{
			using (var entities = new Entities())
			{
				var deviceID = Encoding.ASCII.GetBytes(value);

				if (entities.CONTROLLER_INFO.Any(
					ci =>
					ci.CONTROLLER.VEHICLE.VEHICLE_ID != vehicleId &&
					ci.DEVICE_ID == deviceID))
				{
					Trace.TraceWarning(
						"{0}: vehicle {1} cannot set new device id {2} because it's already seized by another vehicle id",
						this, vehicleId, value);
					return;
				}

				var controllerInfo = entities.CONTROLLER_INFO.FirstOrDefault(
					ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);

				if (controllerInfo == null)
				{
					var controller = entities.CONTROLLER.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == vehicleId);
					if (controller != null)
					{
						controllerInfo = new CONTROLLER_INFO
							{
								TIME = DateTime.UtcNow,
								CONTROLLER = controller
							};
						entities.CONTROLLER_INFO.AddObject(controllerInfo);
					}
				}

				if (controllerInfo == null)
				{
					Trace.TraceWarning(
						"{0}: vehicle {1} cannot set new device id {2} because controller info is not found",
						this, vehicleId, value);
					return;
				}

				if (controllerInfo.DEVICE_ID == deviceID)
					return;

				controllerInfo.DEVICE_ID = deviceID;
				var vehicle = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicleId);
				if (string.IsNullOrWhiteSpace(vehicle.GARAGE_NUMBER))
				{
					vehicle.GARAGE_NUMBER = Encoding.ASCII.GetString(deviceID);
				}

				entities.SaveChangesByOperator(null);
			}
		}

		private void UpdateDevicePassword(int vehicleId, string value)
		{
#if REMEMBER_PASSWORD
			using (var entities = new Entities())
			{
				var controllerInfo = entities.CONTROLLER_INFO.FirstOrDefault(
					ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId &&
						  (ci.PASSWORD == null ||
						   ci.PASSWORD != value));

				if (controllerInfo == null)
					return;

				controllerInfo.PASSWORD = value;

				entities.SaveChangesWithHistory();
			}
#endif
		}

		private void UpdateDeviceFirmwareVersion(int vehicleId, string value)
		{
			if (value == null)
				value = string.Empty;

			Trace.TraceInformation("{0}.UpdateDeviceFirmwareVersion ({1}, {2})",
								   this, vehicleId, value);

			using (var entities = new Entities())
			{
				var valueBytes = Encoding.ASCII.GetBytes(value);

				var controllerInfo = entities.CONTROLLER_INFO.FirstOrDefault(
					ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);

				if (controllerInfo == null)
				{
					Trace.TraceWarning(
						"{0}.UpdateDeviceFirmwareVersion: unable to find controller info by vehicleId {1}",
						this, vehicleId);
					return;
				}

				if (controllerInfo.DEVICE_ID != null &&
					controllerInfo.DEVICE_ID.SequenceEqual(valueBytes))
				{
					Trace.TraceInformation(
						"{0}.UpdateDeviceFirmwareVersion: value will not be updated because it does not change, vehicleId = {1}",
						this, vehicleId);
					return;
				}

				controllerInfo.FIRMWARE = valueBytes;

				entities.SaveChangesByOperator(null);
			}
		}

		#endregion Terminal_ReceiveEvent(ReceiveEventArgs args)

		private bool CheckMobileUnit(IMobilUnit newPositions, IMobilUnit lastPosition)
		{
			return newPositions.Time > lastPosition.Time;
		}
		private IMobilUnit JoinLastAndPrevPositions(IMobilUnit lastPosition, IMobilUnit prevPosition)
		{
			if (null == prevPosition)
				return lastPosition;

			// Считаем курс
			if (prevPosition.ValidPosition && lastPosition.ValidPosition && lastPosition.Course == null)
				lastPosition.Course = lastPosition.CalcCourse(prevPosition.Latitude, prevPosition.Longitude);
			// Если пришла более старая позиция или наоборот позиция из будущего, то игнорируем ее
			if (null == lastPosition
				|| lastPosition.Time < prevPosition.Time
				|| TimeHelper.year1970.AddSeconds(lastPosition.Time) > DateTime.UtcNow.AddHours(1))
			{
				return prevPosition;
			}
			// Если предыдущая позиция слишком давнишняя (> 15 минут), то просто используем новую
			if ((lastPosition.Time - prevPosition.Time) > CommandHelper.LbsRewriteInterval)
			{
				return lastPosition;
			}
			// Если пришла позиция LBS и имеется позиция GPS в пределах 5 минут, возвращаем позицию GPS
			if (lastPosition.Time - prevPosition.Time > 5 * 60 && prevPosition.Type == PositionType.GPS &&
				lastPosition.Type == PositionType.LBS)
			{
				return prevPosition;
			}
			// Если пришли невалидные координаты, то берем предыдущие в пределах 15 минут
			if (!lastPosition.ValidPosition)
			{

				lastPosition.Latitude   = prevPosition.Latitude;
				lastPosition.Longitude  = prevPosition.Longitude;
				lastPosition.Satellites = prevPosition.Satellites;
				lastPosition.Speed      = prevPosition.Speed;

				// Ставим признак валидности позиции
				if (prevPosition.ValidPosition)
				{
					lastPosition.Course = prevPosition.Course;
					if(prevPosition.CorrectTime > 0)
						lastPosition.CorrectTime = prevPosition.CorrectTime;

					var lastPositionMU = lastPosition as MobilUnit.Unit.MobilUnit;
					if (lastPositionMU != null)
					{
						lastPositionMU.CorrectGPS = true;
					}
				}
			}

			if (lastPosition.IP.Address == IPAddress.Any)
				lastPosition.IP = prevPosition.IP;

			lastPosition.SensorValues = JoinSensorValues(lastPosition.SensorValues, prevPosition.SensorValues);

			return lastPosition;
		}
		private Dictionary<int, long> JoinSensorValues(Dictionary<int, long> lastSensorValues, Dictionary<int, long> prevSensorValues)
		{
			if (lastSensorValues == null)
				return prevSensorValues;

			if (prevSensorValues != null && prevSensorValues.Count > 0)
			{
				foreach (var prev in prevSensorValues)
				{
					if (!lastSensorValues.ContainsKey(prev.Key))
						lastSensorValues.Add(prev.Key, prev.Value);
				}
			}
			return lastSensorValues;
		}

		/// <summary> Добавление последней позиции в хэш последних позиций </summary>
		/// <param name="lastPositions"> Последние позиции </param>
		public void AddLastPosition(params IMobilUnit[] lastPositions)
		{
			foreach (IMobilUnit lastPosition in lastPositions)
			{
				if (Equals(lastPosition.cmdType, CmdType.GetLog) || lastPosition.Unique == 0)
					continue;

				if (TimeHelper.year1970.AddSeconds(lastPosition.Time) > DateTime.UtcNow.AddHours(1))
					continue;
			}

			foreach (IMobilUnit lastPosition in lastPositions)
			{
				if (lastPosition.Unique == 0)
					continue;

				if (TimeHelper.year1970.AddSeconds(lastPosition.Time) > DateTime.UtcNow.AddHours(1))
					continue;

				// предпоследняя позиция
				IMobilUnit prevPosition = null;

				if (_mobilUnits.ContainsKey(lastPosition.Unique))
				{
					/*
						Теперь уже предпоследняя позиция - используется
						для расчета направления движения мобильного объекта
						и получения изменений состояния датчиков
					*/
					prevPosition = (IMobilUnit)_mobilUnits[lastPosition.Unique];

					_mobilUnits[lastPosition.Unique] = JoinLastAndPrevPositions(lastPosition, prevPosition);
				}
				else
				{
					_mobilUnits.TryAdd(lastPosition.Unique, lastPosition);
				}
			}
		}

		#region NotifyReceiveEvents(ReceiveEventArgs args)

		/// <summary> Уведомление клиентов о событии наличия принятых сервером данных </summary>
		/// <param name="args">принятые данные</param>
		private void NotifyReceiveEvents(ReceiveEventArgs args)
		{

			if (ReceiveEvent == null)
				return;

			Delegate[] invkList = ReceiveEvent.GetInvocationList();
			IEnumerator ie = invkList.GetEnumerator();
			while (ie.MoveNext())
			{
				ReceiveEventHandler handler = (ReceiveEventHandler)ie.Current;

				try
				{
					handler.BeginInvoke(args, new AsyncCallback(CallBackReceiveEvents), handler); //null, null);
				}
				catch (Exception e)
				{
					throw e;
				}
			}
		}

		/// <summary> Проверка асинхронного вызова уведомления клиентов о событии наличия принятых сервером данных </summary>
		/// <param name="ar"></param>
		private void CallBackReceiveEvents(IAsyncResult ar)
		{
			// попытка завершить метод синхронно
			ReceiveEventHandler handler = (ReceiveEventHandler)ar.AsyncState;
			try
			{
				handler.EndInvoke(ar);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				ReceiveEvent -= handler;
			}
		}

		#endregion NotifyReceiveEvents(ReceiveEventArgs args)

		#region NotifyNotifyEvents(NotifyEventArgs args)

		/// <summary> Уведомление клиентов о событии наличия принятых сервером данных </summary>
		/// <param name="args">принятые данные</param>
		private void NotifyNotifyEvents(NotifyEventArgs args)
		{
			if (NotifyEvent == null)
				return;

			Delegate[] invkList = NotifyEvent.GetInvocationList();
			IEnumerator ie = invkList.GetEnumerator();
			//
			while (ie.MoveNext())
			{
				NotifyEventHandler handler = (NotifyEventHandler)ie.Current;

				try
				{
					handler.BeginInvoke(args, new AsyncCallback(CallBackNotifyEvents), handler); //null, null);
				}
				catch (Exception e)
				{
					throw e;
				}
			}
		}

		/// <summary> Проверка асинхронного вызова уведомления клиентов о событии наличия принятых сервером данных </summary>
		/// <param name="ar"></param>
		private void CallBackNotifyEvents(IAsyncResult ar)
		{
			// попытка завершить метод синхронно
			var handler = (NotifyEventHandler)ar.AsyncState;
			try
			{
				handler.EndInvoke(ar);
			}
			catch (Exception ex)
			{
				if (ex is SocketException || ex is RemotingException)
				{
					$"Notification will be unsubscribed"
						.WithException(ex)
						.CallTraceError();
					NotifyEvent -= handler;
				}
				else
				{
					$"Exception received when firing NotifyEvent, delegate will be unsubscribed"
						.WithException(ex)
						.CallTraceError();
				}
			}
		}

		#endregion NotifyNotifyEvents(NotifyEventArgs args)

		/// <summary> Поток для получения последних позиций </summary>
		/// <remarks>Запуск такого потока позволяет сервису успешно стартовать, т.к. для запуска сервиса отводится не более 30 сек.
		/// <p>TODO: необходимо создать отдельный кеш последних позиций в БД:</p>
		/// <p>- уменьшится время загрузки последних позиций ТС в несколько раз;</p>
		/// <p>- отпадёт необходимость в алгоритмах по определению интервала между последними позициями.</p>
		/// </remarks>
		private void LoadLastPositions()
		{
			IDictionary<int, IMobilUnit> databaseLastPositions = databaseManager.GetLastPositions();

			// восстановление последних позиций из базы данных
			/* Необходимо аккуратное слияние, так как в течение
			* длительного процесса получения последних позиций из БД
			* уже могли прилететь более свежие позиции непосредственно с
			* контроллеров в хэш последних позиций
			*/
			foreach (IMobilUnit mobilUnit in databaseLastPositions.Values)
			{
				if (!_mobilUnits.ContainsKey(mobilUnit.Unique))
				{
					_mobilUnits.TryAdd(mobilUnit.Unique, mobilUnit);
				}
				else
				{
					/* Теоретически конечно в хэше могут находиться
					* только более свежие позиции, чем извлеченные из БД,
					* однако нет уверенности, что метод не будет
					* вызываться лишний раз
					*/

					if (mobilUnit.Time < _mobilUnits[mobilUnit.Unique].Time)
					{
						_mobilUnits[mobilUnit.Unique] = mobilUnit;
					}
				}
			}
		}
		/// <summary> Создание/редактирование норматива межзонового пробега </summary>
		/// <param name="zone_id_from">id 1-й геозоны</param>
		/// <param name="zone_id_to">id 2-й геозоны</param>
		/// <param name="distance">норматив (в метрах)</param>
		/// <param name="operatorID">ID оператора</param>
		/// <param name="sessionID">ID сессии пользователя</param>
		public void UpdateZoneDistanceStandart(
			int zone_id_from, int zone_id_to, int distance, int operatorID, int sessionID)
		{
			databaseManager.UpdateZoneDistanceStandart(zone_id_from, zone_id_to, distance, operatorID, sessionID);
		}

		#region TerminalClient
		private const string              _terminalServerName = "TerminalService";
		private readonly object           _terminalClientLock = new object();
		private volatile TerminalClient   _terminalClient;
		private volatile ITerminalManager _terminalManager;

		[Browsable(false)]
		public ITerminalManager TerminalManager
		{
			get
			{
				if (_terminalManager != null)
					return _terminalManager;

				TryReconnect("TerminalManager is null");

				return _terminalManager;
			}
		}
		private void ConnectTerminalClient()
		{
			var terminalServerSessionManagerUrl = GlobalsConfig.AppSettings[_terminalServerName];
			if (terminalServerSessionManagerUrl == null)
			{
				$"{_terminalServerName} not configured in configuration file"
					.CallTraceWarning();
				return;
			}

			try
			{
				// Ждем блокировку и блокируем
				lock (_terminalClientLock)
				{
					// Во время ожидания блокировки еще не присвоили значение
					if (_terminalClient != null)
						return;
					// Создаем клиента
					var terminalClient = new TerminalClient(ConfigurationManager.AppSettings);
					terminalClient.Connect(terminalServerSessionManagerUrl, false);
					((IClient)terminalClient).Start();

					terminalClient.Receive       += Terminal_ReceiveEvent;
					terminalClient.Notify        += Terminal_NotifyEvent;
					terminalClient.NeedReConnect += TerminalClientNeedReConnect;
					// Присваиваем только после прохождения всех процедур подключения
					_terminalClient  = terminalClient;
					// Получаем внутреннее свойство сервера терминалов, от которого надо потом избавиться
					_terminalManager = _terminalClient.Session.GetTerminalManager();
					if (_terminalManager == null)
					{
						$"TerminalClient.Session.GetTerminalManager() returned null"
							.CallTraceWarning();
					}
				}
			}
			catch (Exception ex)
			{
				$"Unable to connect to terminal server '{terminalServerSessionManagerUrl}'"
					.WithException(ex)
					.CallTraceError();
				_terminalManager = null;
			}
			finally
			{
				_terminalServerEventsMissingReconnectTimer?.Change(_terminalServerEventsMissingReconnectIntertval, Timeout.Infinite);
			}

			if (_terminalManager != null)
				$"Successfully connected to terminal server '{terminalServerSessionManagerUrl}'"
					.CallTraceInformation();
		}
		private void TerminalClientNeedReConnect(object sender, EventArgs e)
		{
			TryReconnect("TerminalClientNeedReConnect event");
		}
		private void DisconnectTerminalClient()
		{
			lock (_terminalClientLock)
			{
				if (_terminalClient != null)
				{
					try
					{
						_terminalClient.Notify -= Terminal_NotifyEvent;
					}
					catch (Exception ex)
					{
						$"Notify unsubscribing"
							.WithException(ex)
							.CallTraceError();
					}
					try
					{
						_terminalClient.Receive -= Terminal_ReceiveEvent;
					}
					catch (Exception ex)
					{
						$"Receive unsubscribing"
							.WithException(ex)
							.CallTraceError();
					}
					try
					{
						((IClient)_terminalClient).Stop();
					}
					catch (Exception ex)
					{
						$"TerminalClient.Stop()"
							.WithException(ex)
							.CallTraceError();
					}
					try
					{
						_terminalClient.Dispose();
					}
					catch (Exception ex)
					{
						$"TerminalClient.Dispose()"
							.WithException(ex)
							.CallTraceError();
					}
					_terminalClient = null;
				}
			}
		}

		#endregion TerminalClient

		#region Получение последнего адреса MobilUnit по номеру контроллера

		public string GetLastAddress(int ctrlNum, string language)
		{
			try
			{
				var geoClient = GeoClient.Default;
				if (geoClient == null)
					throw new ApplicationException($"No connection to map server");
				foreach (IMobilUnit mu in _mobilUnits.Values)
				{
					if (mu.ID == ctrlNum)
					{
						string time = TimeHelper.GetLocalTime(mu.Time, "HH:mm dd/MM/yy");
						string address = geoClient.GetAddressByPoint(lat: mu.Latitude, lng: mu.Longitude, language);
						return string.Format("{0} {1}", time, address);
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return "Адрес не найден!";
		}
		public string GetLastAddress(int ctrlNum, out double lat, out double lng, string language)
		{
			lat = 0;
			lng = 0;
			try
			{
				var geoClient = GeoClient.Default;
				if (geoClient == null)
					throw new ApplicationException($"No connection to map server");

				foreach (IMobilUnit mu in _mobilUnits.Values)
				{
					if (mu.ID == ctrlNum)
					{
						lat = mu.Latitude;
						lng = mu.Longitude;
						string time = TimeHelper.GetLocalTime(mu.Time, "HH:mm dd/MM/yy");
						string address = geoClient.GetAddressByPoint(lat: mu.Latitude, lng: mu.Longitude, language);
						return string.Format("{0} {1}", time, address);
					}
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return "Адрес не найден!";
		}

		#endregion Получение последнего адреса MobilUnit по номеру контроллера

		/// <summary> notification from terminal. events and messages to be logged or resend clients. alarm, text and so on </summary>
		/// <param name="args">notification info</param>
		/// <remarks>доделать Аркадию</remarks>
		public void Terminal_NotifyEvent(BulkNotifyEventArgs bulkArgs)
		{
			if (0 < _terminalServerEventsMissingReconnectIntertval)
				_terminalServerEventsMissingReconnectTimer?.Change(_terminalServerEventsMissingReconnectIntertval, Timeout.Infinite);

			foreach (var receivedArgs in bulkArgs.SingleEventArgs)
			{
				var args = receivedArgs;
				try
				{
					switch (args.Event)
					{
						case TerminalEventMessage.NE_ALARM:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[1] as string;
								if (ui != null)
									databaseManager.FillUnitInfo(ui);
								$"'{args.Message}' from {ui}. original text '{text}'"
									.CallTraceWarning();
								break;
							}
						case TerminalEventMessage.NE_MESSAGE:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[2].ToString();

								#region Заплатка для SANAV

								// TODO: Заплатка для SANAV
								// Чтобы сообщение о тревоге проходило быстрее, надо сначала получить SMS от SANAV!
								// Номер модема зашит в памяти SANAV. Дело в том, что SANAV сначала посылает SMS на 3 номера, а потом
								// посылает позиции с пометкой SOS. В данной версии SANAV формирует тревогу только по получении позиции.
								// Если в тексте есть слово "Emergency!!", то формируем сигнал Тревога!
								if (text.IndexOf("Emergency!!") > 0)
								{
									args = new NotifyEventArgs(args.Time, TerminalEventMessage.NE_ALARM, null,
										Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
										args.Tag);

									//if (ui != null)
									//    databaseManager.FillUnitInfo(ui);
									$"'{"Тревога!"}' from {ui}. original text '{text}'"
										.CallTraceWarning();
									break;
								}

								#endregion Заплатка для SANAV

								if (ui != null)
								{
									//databaseManager.FillUnitInfo(ui);
									$"'{args.Message}' from {ui}. original text '{text}'"
										.CallTraceWarning();

									ProcessSMSMessage(ui, text);
								}
								break;
							}
						case TerminalEventMessage.NE_CONFIRM:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[1] as string;
								if (ui != null)
									databaseManager.FillUnitInfo(ui);
								$"Confirm from {ui}. original text '{text}'"
									.CallTraceWarning();
								break;
							}

						#region Статусы скорой

						case TerminalEventMessage.NE_FREE:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[1] as string;
								if (ui != null)
									databaseManager.FillUnitInfo(ui);
								$"'{args.Message}' from {ui}. original text '{text}'"
									.CallTraceWarning();
							}
							break;
						case TerminalEventMessage.NE_TO_ORDER:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[1] as string;
								if (ui != null)
									databaseManager.FillUnitInfo(ui);
								$"'{args.Message}' from {ui}. original text '{text}'"
									.CallTraceWarning();
							}
							break;
						case TerminalEventMessage.NE_ORDER:
							{
								object[] par = (object[])args.Tag;
								IUnitInfo ui = par[0] as IUnitInfo;
								string text  = par[1] as string;
								if (ui != null)
									databaseManager.FillUnitInfo(ui);
								$"'{args.Message}' from {ui}. original text '{text}'"
									.CallTraceWarning();
							}
							break;

						#endregion Статусы скорой
					}
					NotifyNotifyEvents(args);
				}
				catch (Exception ex)
				{
					$"Error during forming and dispatching notification message"
						.WithException(ex, true)
						.CallTraceError();
				}
			}
		}

		#region Обработка SMS

		private void ProcessSMSMessage(IUnitInfo ui, string text)
		{
			if (text == null || text == String.Empty)
				return;

			// первый симаол SMS
			string startSimbol = text.Substring(0, 1);

			switch (startSimbol.ToUpper())
			{
				// поиск адреса
				case "A":
				case "А":
					ProcessSMSMessageA(ui, text);
					break;
				// парковка
				case "P":
				case "Р":
					ProcessSMSMessageP(ui, text);
					break;
				// снятие парковки
				case "M":
				case "М":
					ProcessSMSMessageM(ui, text);
					break;
				// неизвестная SMS
				default:
					ProcessSMSMessageDefault(ui, text);
					break;
			}
		}
		/// <summary> Обработка SMS "A xxx" - получение адреса "где моя машина?" </summary>
		/// <param name="ui"></param>
		/// <param name="text"></param>
		private void ProcessSMSMessageA(IUnitInfo ui, string text)
		{
			#region Запрос позиции

			//Номер контроллера
			int num;
			string address = string.Empty;
			text = text.Remove(0, 1);
			string sendText = "Запрос не обработан!";

			if (int.TryParse(text, out num))
			{
				int i = databaseManager.GetOwnerRight(num, ui.Telephone);
				//Если у оператора есть право на машину, то i д.б. > 0
				switch (i)
				{
					case 0:
						sendText = string.Format("Нет прав на машину {0}", num);
						_terminalManager.SendSMS(ui.Telephone, sendText);
						break;

					default:
						address = GetLastAddress(num, "");
						sendText = string.Format("{0}: {1}", num, address);
						_terminalManager.SendSMS(ui.Telephone, sendText);
						ProcessSMSLog(ui, 1, num);
						break;
				}
			}

			#endregion Запрос позиции
		}
		/// <summary> Обработка SMS "M xxx" - снятие сигнала ПАРКОВКА </summary>
		/// <param name="ui"></param>
		/// <param name="text"></param>
		private void ProcessSMSMessageM(IUnitInfo ui, string text)
		{
			#region Парковка

			//Номер контроллера
			int num;
			text = text.Remove(0, 1);
			string sendText = "Запрос не обработан!";

			if (int.TryParse(text, out num))
			{
				int i = databaseManager.GetOwnerRight(num, ui.Telephone);
				//Если у оператора есть право на машину, то i д.б. > 0
				switch (i)
				{
					case 0:
						sendText = String.Format("Нет прав на машину {0}", num);
						_terminalManager.SendSMS(ui.Telephone, sendText);
						break;

					default:
						ProcessSMSLog(ui, 3, num);
						break;
				}
			}

			#endregion Парковка
		}
		/// <summary> Обработка SMS "P xxx" - сигнал ПАРКОВКА </summary>
		/// <param name="ui"></param>
		/// <param name="text"></param>
		private void ProcessSMSMessageP(IUnitInfo ui, string text)
		{
			#region Парковка

			//Номер контроллера
			int num;
			text = text.Remove(0, 1);
			string sendText = "Запрос не обработан!";

			if (int.TryParse(text, out num))
			{
				int i = databaseManager.GetOwnerRight(num, ui.Telephone);
				//Если у оператора есть право на машину, то i д.б. > 0
				switch (i)
				{
					case 0:
						sendText = String.Format("Нет прав на машину {0}", num);
						_terminalManager.SendSMS(ui.Telephone, sendText);
						break;

					default:
						ProcessSMSLog(ui, 2, num);
						break;
				}
			}

			#endregion Парковка
		}
		/// <summary> Обработка неопределённого SMS сообщения </summary>
		/// <param name="ui"></param>
		/// <param name="text"></param>
		private void ProcessSMSMessageDefault(IUnitInfo ui, string text)
		{
			Trace.TraceInformation("Unknown SMS!  Text: " + text);
			string telephone = "+7???????";
			if (ui != null && ui.Telephone != null)
				telephone = ui.Telephone;
			Trace.TraceInformation("from telephone: " + telephone);
		}
		private void ProcessSMSLog(IUnitInfo ui, int sms_type, int controller_number)
		{
			try
			{
				int owner_id = GetOwnerByTelephone(ui.Telephone);

				if (owner_id > 0)
					databaseManager.InsertSmsLog(sms_type, owner_id, controller_number);
				else
					Trace.TraceInformation(
						String.Format(
							"Can't write sms to log: sms-{0} phone-{1}",
							sms_type.ToString(),
							ui.Telephone
							)
						);
			}
			catch (Exception e)
			{
				Trace.TraceError(e.ToString());
			}
		}
		private int GetOwnerByTelephone(string telephone)
		{
			int owner_id = 0;

			try
			{
				ArrayList pars = new ArrayList();
				pars.Add(new ParamValue("@telephone", telephone));
				DataSet owners = GetDataFromDB(
					pars,
					"GetOwnerByTelephone",
					new string[] { "OWNER" }
					);
				if (owners != null && owners.Tables["OWNER"] != null && owners.Tables["OWNER"].Rows.Count > 0)
				{
					DataRow opRow = owners.Tables["OWNER"].Rows[0];
					owner_id = (int)opRow["OWNER_ID"];
				}

			}
			catch (Exception e)
			{
				Trace.TraceError(e.ToString());
			}

			return owner_id;
		}

		#endregion Обработка SMS

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}

		#endregion Component Designer generated code

		#region IMessageServerDataProvider Members

		#endregion IMessageServerDataProvider Members

		/// <summary> Возвращает список vehicleId трекеров, которые могут быть дизактивированы </summary>
		/// <param name="operatorID"></param>
		public List<int> GetTrackersCanBeDisactivated(int operatorID)
		{
			List<int> res;
			using (var entities = new Entities())
			{
				const int rightID = (int)SystemRight.SecurityAdministration;

				res = (from vr in entities.v_operator_vehicle_right
					   where vr.operator_id == operatorID
							 && vr.right_id == rightID
							 && vr.VEHICLE.ActivatedTracker != null
							 && vr.VEHICLE.ActivatedTracker.Activated
					   select vr.VEHICLE.VEHICLE_ID).ToList();
			}

			return res;
		}

		/// <summary> Возвращает трекер в набор неактивированных трекеров </summary>
		/// <param name="operatorID"></param>
		/// <param name="vehicleID"></param>
		public DisactivateTrackerResult DisactivateTracker(int operatorID, int vehicleID)
		{
			using (var entities = new Entities())
			{
				var vehicle = entities.GetVehicle(vehicleID);
				if (vehicle == null)
					return DisactivateTrackerResult.Forbidden;

				//Проверяем есть ли права на этот трекер
				var owner = entities.GetOwner(vehicle);
				if (owner == null || owner.OPERATOR_ID != operatorID)
					return DisactivateTrackerResult.Forbidden;

				//Проверяем, есть ли в Controller_Info Device_id is not null
				var controllerInfo = (from v in entities.VEHICLE
									  where
										  v.VEHICLE_ID == vehicleID
										  && v.CONTROLLER.CONTROLLER_INFO.DEVICE_ID != null
									  select v.CONTROLLER.CONTROLLER_INFO).FirstOrDefault();

				if (controllerInfo == null)
					return DisactivateTrackerResult.TrackerIsNotActivatable;

				if (!vehicle.ActivatedTrackerReference.IsLoaded)
					vehicle.ActivatedTrackerReference.Load();

				//Проверяем есть ли запись таблице Tracker
				var tracker = vehicle.ActivatedTracker;

				if (tracker == null || !tracker.Activated)
					return DisactivateTrackerResult.TrackerIsNotActivatable;

				//Изменяем Controller_Info для трекера (делаем Device_id = null)
				controllerInfo.DEVICE_ID = null;
				tracker.Activated = false;
				tracker.Vehicle = entities.CreateVehicle(tracker, null);

				var @operator = entities.GetOperator(operatorID);

				var ovrs = (from ovr in entities.v_operator_vehicle_right
							where ovr.vehicle_id == vehicle.VEHICLE_ID
							select new { ovr.OPERATOR, ovr.RIGHT }).ToList();

				foreach (var ovr in ovrs)
					entities.Disallow(ovr.OPERATOR, vehicle, ovr.RIGHT);

				entities.SaveChangesByOperator(operatorID);

				return DisactivateTrackerResult.Success;
			}
		}

		private static int? _trackerMaxAttemptsCount;

		private static int TrackerMaxAttemptsCount
		{
			get
			{
				if (_trackerMaxAttemptsCount == null)
				{
					var configString = ConfigurationManager.AppSettings["TrackerMaxAttemptsCount"];
					_trackerMaxAttemptsCount = 5;
					try
					{
						_trackerMaxAttemptsCount = XmlConvert.ToInt32(configString);
					}
					catch (ThreadAbortException)
					{
						throw;
					}
					catch (Exception ex)
					{
						Trace.TraceError(ex.ToString());
					}
				}
				return _trackerMaxAttemptsCount.Value;
			}
		}

		private static TimeSpan? _trackerBlockingTimespan;

		private TimeSpan TrackerBlockingTimespan
		{
			get
			{
				if (_trackerBlockingTimespan == null)
				{
					var configString = ConfigurationManager.AppSettings["TrackerBlockingTimespan"];
					try
					{
						_trackerBlockingTimespan = XmlConvert.ToTimeSpan(configString);
					}
					catch (ThreadAbortException)
					{
						throw;
					}
					catch (Exception ex)
					{
						Trace.TraceError(ex.ToString());
						_trackerBlockingTimespan = TimeSpan.FromHours(1);
					}
				}
				return _trackerBlockingTimespan.Value;
			}
		}

		/// <summary> Создает новый трекер на основании номера трекера и контрольного числа. Привязывает трекер оператору, полученного вызовом делегата getOperator </summary>
		/// <param name="getOperator">Делегат для получения оператора, который будет считаться владельцем нового трекера</param>
		/// <param name="alias">Название устройства</param>
		/// <param name="number">Номер контроллера</param>
		/// <param name="controlCode">Контрольный код</param>
		public Res::AddTrackerResult AddTracker(
			Func<Entities, OPERATOR> getOperator, string alias, string number, string controlCode, IPAddress clientIP, int? departmentId)
		{
			var addTrackerResult = AddTrackerInternal(getOperator, alias, number, controlCode, clientIP, departmentId);

			Trace.TraceError("Adding tracker with params: alias = " + alias
				+ ", number = "      + number
				+ ", controlCode = " + controlCode
				+ ", clientIP = "    + clientIP
				+ ", result = "      + addTrackerResult);

			return
				(addTrackerResult == Res::AddTrackerResult.InvalidNumber ||
				 addTrackerResult == Res::AddTrackerResult.InvalidControlCode)
					? Res::AddTrackerResult.InvalidNumberOrControlCode
					: addTrackerResult;
		}

		private static readonly SystemRight[] DefaultTrackerOwnerRights = new[]
		{
			SystemRight.SecurityAdministration,
			SystemRight.VehicleAccess,
			SystemRight.EditVehicles,
		};

		private ExpiredTrialServiceRemover _expiredTrialServiceRemover;

		private Res::AddTrackerResult AddTrackerInternal(
			Func<Entities, OPERATOR> getOperator, string alias, string number, string controlCode, IPAddress clientIP, int? departmentId)
		{
			using (var entities = new Entities())
			using (var transaction = new Transaction())
			{
				var trackers =
					from t in entities.Tracker
					where t.Number == number && t.Vehicle != null && !t.Activated
					select t;

				if (trackers.Count() == 0)
				{
					transaction.Complete();
					return Res::AddTrackerResult.InvalidNumber;
				}

				var clientIPString = clientIP.ToString();
				var attempt = (from a in entities.Tracker_Attempt
							   where a.Number == number &&
									 a.IP == clientIPString
							   select a).FirstOrDefault();

				if (attempt == null)
				{
					attempt = new Tracker_Attempt
					{
						Number = number,
						IP = clientIPString,
						Last_Time = DateTime.UtcNow,
						Count = 1
					};
					entities.AddToTracker_Attempt(attempt);
				}
				else
				{
					if (TrackerMaxAttemptsCount < attempt.Count)
					{
						if (DateTime.UtcNow < attempt.Last_Time.Add(TrackerBlockingTimespan))
						{
							transaction.Complete();
							return Res::AddTrackerResult.MaxAttemptsExceeded;
						}
						attempt.Count = 1;
					}
					else
					{
						++attempt.Count;
					}
					attempt.Last_Time = DateTime.UtcNow;
				}
				entities.SaveChanges();

				var tracker = (from t in trackers where t.Control_Code == controlCode select t).FirstOrDefault();

				if (tracker == null)
				{
					transaction.Complete();
					return Res::AddTrackerResult.InvalidControlCode;
				}

				var @operator = getOperator(entities);

				if (@operator == null)
					return Res::AddTrackerResult.NoOperator;

				tracker.VehicleReference.Load();
				var vehicle = tracker.Vehicle;

				//Если указан департамент, привязываем объект наблюдения к нему
				if (departmentId != null)
					vehicle.SetDepartment(entities, entities.DEPARTMENT.First(d => d.DEPARTMENT_ID == departmentId.Value));

				vehicle.GARAGE_NUMBER = alias;

				//5. Привязка трекера к оператору.
				entities.Allow(
					@operator,
					vehicle,
					entities.GetSystemRights(DefaultTrackerOwnerRights).ToArray());

				//6. Помечаем трекер как активированный
				tracker.Activated = true;

				//7. Сохранение изменений
				entities.SaveChangesByOperator(@operator.OPERATOR_ID);

				transaction.Complete();
			}

			return Res::AddTrackerResult.Success;
		}
		/// <summary> Создает в системе новый трекер и привязывает его указанному оператору. </summary>
		/// <param name="operatorID">ID оператора</param>
		/// <param name="alias">Название устройства</param>
		/// <param name="number">Номер контроллера</param>
		/// <param name="controlCode">Контрольный код</param>
		/// <param name="clientIP"></param>
		/// <param name="departmentId">Идентификатор департамента</param>
		public Res::AddTrackerResult AddTracker(
			int operatorID, string alias, string number, string controlCode, IPAddress clientIP, int? departmentId)
		{
			return AddTracker(
				entities => (from o in entities.OPERATOR where o.OPERATOR_ID == operatorID select o).First(),
				alias, number, controlCode, clientIP, departmentId
				);
		}
		public List<Message> GetMessages(int? operatorID, int[] vehicleIDs, DateTime? @from, DateTime? to)
		{
			using (var entities = new Entities())
			{
				IQueryable<MESSAGE> messagesQuery =
					entities.MESSAGE
					.Include(MESSAGE.MESSAGE_TEMPLATEIncludePath)
					.Include(MESSAGE.ContactsIncludePath)
					.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
					.Include(MESSAGE.OwnerOperatorIncludePath);

				if (@from != null)
					messagesQuery = (from m in messagesQuery
									 where @from.Value <= m.TIME
									 select m);

				if (@to != null)
					messagesQuery = (from m in messagesQuery
									 where m.TIME <= @to.Value
									 select m);

				if (operatorID != null)
				{
					messagesQuery = messagesQuery.Where(m => m.Owner_Operator_ID == operatorID);
				}

				if (vehicleIDs != null && vehicleIDs.Length != 0)
				{
					var controllerIds =
						entities.VEHICLE
								.Where(v => vehicleIDs.Contains(v.VEHICLE_ID) && v.CONTROLLER != null)
								.Select(v => v.CONTROLLER.CONTROLLER_ID)
								.ToList()
								.Select(i => i.ToString(CultureInfo.InvariantCulture))
								.ToArray();

					var contactIds =
						entities.VEHICLE.Where(v => vehicleIDs.Contains(v.VEHICLE_ID))
								.Select(v => v.CONTROLLER.MLP_Controller.Asid.Contact)
								.Union(
									entities.Contact.Where(
										c => c.Type == (int) ContactType.Vehicle && controllerIds.Contains(c.Value)))
								.Select(c => c.ID)
								.Distinct()
								.ToList();

					if (contactIds.Count == 0)
						return new List<Message>();

					messagesQuery = messagesQuery.Where(m => m.Contacts.Any(mc => contactIds.Contains(mc.Contact.ID)));
				}

				var messages = messagesQuery.ToList();

				var operatorIDs =
					messages.Where(m => m.Owner_Operator_ID != null)
							.Select(m => m.Owner_Operator_ID ?? 0)
							.ToList();

				var objectNameByVehicleID =
					(from vehicle in
						 entities.VEHICLE.Where(v => vehicleIDs.Contains(v.VEHICLE_ID))
					 select new {ID = vehicle.VEHICLE_ID, vehicle.GARAGE_NUMBER})
						.ToDictionary(item => item.ID, item => item.GARAGE_NUMBER);

				var operatorIDsArray = operatorIDs.ToArray();
				var objectNameByOperatorID =
					(from o in
						 entities.OPERATOR.Where(o => operatorIDsArray.Contains(o.OPERATOR_ID))
					 select new {ID = o.OPERATOR_ID, o.NAME, o.LOGIN})
						.ToDictionary(item => item.ID, item => string.IsNullOrEmpty(item.NAME) ? item.LOGIN : item.NAME);

				return messages.ConvertAll(
					delegate(MESSAGE m)
						{
							var result = m.ToDto();

							var sourceMessageContact = m.Contacts.FirstOrDefault(mc => mc.Type == (int) MessageContactType.Source);

							if (sourceMessageContact != null)
							{
								string source;
								switch ((ContactType) sourceMessageContact.Contact.Type)
								{
									case ContactType.Vehicle:
										var vehicleID = int.Parse(sourceMessageContact.Contact.Value);
										if (objectNameByVehicleID.TryGetValue(vehicleID, out source))
											result.Source = source;
										break;
								}
							}
							else if (m.Owner_Operator_ID != null)
							{
								string source;
								if (objectNameByOperatorID.TryGetValue(m.Owner_Operator_ID.Value, out source))
									result.Source = source;
							}

							MessageType type =
								operatorID != null &&
								m.Owner_Operator_ID == operatorID
									? MessageType.Outgoing
									: MessageType.Incoming;

							result.Type = type;
							result.Template = m.MESSAGE_TEMPLATE != null ? m.MESSAGE_TEMPLATE.NAME : null;

							return result;
						});
			}
		}
		public List<InputLogRecord> GetRawSensorValues(int vehicleId)
		{
			var log = databaseManager.GetRawSensorValues(vehicleId) ?? new List<InputLogRecord>();
			var mev = GetLastPosition(vehicleId);
			if (0 < (mev?.SensorValues?.Count ?? 0))
				log.AddRange(mev.SensorValues.Select(mu => new InputLogRecord(mev.Time, mu.Key, mu.Value)));
			return log
				.GroupBy(l => l.Number)
				.Select(g => g.OrderBy(l => l.LogTime).FirstOrDefault())
				.ToList();
		}

		private const string RandomKeySymbolsString = "1234567890QWERTYUIOPASDFGHJKLZXCVBNM";

		public string GenerateRandomKey(int length)
		{
			if (length < 0)
				throw new ArgumentOutOfRangeException("length", length, "Value cannot be negative");

			var random = new Random();
			var sb = new StringBuilder(length);
			for (var i = 0; i != length; ++i)
				sb.Append(RandomKeySymbolsString[random.Next(RandomKeySymbolsString.Length)]);
			return sb.ToString();
		}
		public string GenerateRandomKey(string predefinedPart, int randomPartLength)
		{
			//Ключ составляется из двух частей: случайной и предопределенной
			//Предопределенная часть включается в ключ, чтобы исключить вероятность того,
			//что для разных predefinedPart будут получены одинаковые ключи

			//Генерируем случайный ключ
			var key = GenerateRandomKey(randomPartLength);
			var sb = new StringBuilder(key);

			//XORим байты предопределенной части байтами случайной части,
			//чтобы в двух разных ключах по одной предопределенной части
			//не бросалось в глаза общая часть ключа
			var randomPartBytes = Encoding.ASCII.GetBytes(sb.ToString());
			var predefinedPartBytes = Encoding.Unicode.GetBytes(predefinedPart);
			for (var i = 0; i != predefinedPartBytes.Length; ++i)
				predefinedPartBytes[i] = (byte)(predefinedPartBytes[i] ^ randomPartBytes[i % randomPartBytes.Length]);

			var codedPredefinedPart = StringHelper.ConvertToSafeForUrlString(RandomKeySymbolsString, predefinedPartBytes);

			return codedPredefinedPart + Encoding.ASCII.GetString(randomPartBytes);
		}
		public string GenerateSmsKey()
		{
			var rnd = new Random().Next(1000000);
			return string.Format(rnd.ToString("D6"));
		}
		public void SendCommand(IStdCommand cmd)
		{
			var terminalManager = TerminalManager;

			if (terminalManager == null)
				throw new TerminalServerConnectionException("Terminal manager is null");

			try
			{
				terminalManager.SendCommand(cmd);
			}
			catch (SocketException e)
			{
				throw new TerminalServerConnectionException("Socket exception when trying to connect to terminal manager", e);
			}
			catch (RemotingException e)
			{
				throw new TerminalServerConnectionException("Remoting exception when trying to connect to terminal manager", e);
			}
		}
		public Picture GetPicture(int vehicleID, int logTime)
		{
			var picture = DbManager.GetPicture(vehicleID, logTime);

			if (picture == null)
				return null;

			if (picture.Bytes == null && !string.IsNullOrWhiteSpace(picture.Url))
			{
				try
				{
					using (var webClient = new WebClient())
						picture.Bytes = webClient.DownloadData(picture.Url);
				}
				catch (Exception e)
				{
					Trace.TraceError("Unable to load picture from {0}: {1}", picture.Url, e);
					return null;
				}

				picture.Url = null;
			}

			return picture;
		}
		public byte[] GetPictureThumbnail(int vehicleID, int logTime, int? width = null, int? height = 100)
		{
			var picture = GetPicture(vehicleID, logTime);

			if (picture == null)
				return null;

			var originalPictureBytes = picture.Bytes;
			if (originalPictureBytes == null)
				return null;

			var ms = new MemoryStream(originalPictureBytes);
			var image = Image.FromStream(ms);
			int thumbnailHeight, thumbnailWidth;
			if (height.HasValue)
			{
				thumbnailHeight = height.Value > image.Height ? image.Height : height.Value;
				thumbnailWidth = (int)Math.Round(thumbnailHeight * (image.Width / (double)image.Height), MidpointRounding.ToEven);
			}
			else if (width.HasValue)
			{
				thumbnailWidth = width.Value > image.Width ? image.Width : width.Value;
				thumbnailHeight = (int) Math.Round(thumbnailWidth*(image.Height/(double) image.Width), MidpointRounding.ToEven);
			}
			else
			{
				thumbnailHeight = 100 > image.Height ? image.Height : 100;
				thumbnailWidth = (int)Math.Round(thumbnailHeight * (image.Width / (double)image.Height), MidpointRounding.ToEven);
			}

			var thumbnail = image.GetThumbnailImage(
				thumbnailWidth, thumbnailHeight,
				() => true,
				IntPtr.Zero);
			var thumbStream = new MemoryStream();
			// put the image into the memory stream
			thumbnail.Save(thumbStream, ImageFormat.Jpeg);
			// make byte array the same size as the image
			var thumbContent = new Byte[thumbStream.Length];
			// rewind the memory stream
			thumbStream.Position = 0;
			// load the byte array with the image
			thumbStream.Read(thumbContent, 0, (int)thumbStream.Length);
			return thumbContent;
		}

		public string UrlWebGis
		{
			get { return ConfigurationManager.AppSettings["UrlWebGis"] ?? "https://web.ufin.online"; }
		}

		public DepartmentType DefaultDepartmentType
		{
			get
			{
				return DepartmentType.Corporate;
			}
		}
		public IMobilUnit GetLastPosition(int vehicleId)
		{
			IMobilUnit result;
			if (!_mobilUnits.TryGetValue(vehicleId, out result))
				return null;
			return result;
		}
		/// <summary> Возвращает список последних позиций ТС </summary>
		/// <name="group"> массив int[] идентификаторов ТС, позиции которых необходим получить </param>
		/// <returns>список последних позиций для указанных ТС LastPosition[]</returns>
		/// <remarks><see cref="LoadLastPositions"/></remarks>
		public IDictionary<int, IMobilUnit> GetLastPositions(int[] uniques)
		{
			var result = new Dictionary<int, IMobilUnit>(uniques.Length);
			foreach (var unique in uniques)
			{
				if (!_mobilUnits.ContainsKey(unique))
					continue;

				IMobilUnit position;
				if (_mobilUnits.TryGetValue(unique, out position))
					result.Add(unique, position);
			}
			return result;
		}
		/// <summary> Отправляет запрос на дружбу </summary>
		/// <param name="operatorID">Оператор, который хочет получить разрешение на определение местоположения абонента с номером msisdn</param>
		/// <param name="msisdn">Номер телефона абонента, который должен ответить согласием</param>
		/// <param name="context">Контекст изменений</param>
		/// <returns>Результат запроса</returns>
		public ProposeFriendshipResult ProposeFriendship(int operatorID, string msisdn, SaveContext context)
		{
			using (var entities = new Entities())
			{
				msisdn = ContactHelper.GetNormalizedPhone(msisdn);
				if (msisdn == null)
					return ProposeFriendshipResult.InvalidMsisdn;

				var friendContact = entities.GetContact(ContactType.Phone, msisdn);
				if (friendContact == null)
					return ProposeFriendshipResult.InvalidMsisdn;

				var friendMsisdn = entities.GetMsisdnContact(friendContact);
				if (entities.Operator_Contact.Any(oc =>
					oc.Operator_ID == operatorID      &&
					oc.Contact_ID  == friendMsisdn.ID &&
					oc.Confirmed))
					return ProposeFriendshipResult.UnableToInviteSelf; //Самому себе приглашение не отправляем

				var operatorMsisdn = entities.GetOwnPhoneContact(operatorID);
				if (null == operatorMsisdn)
					return ProposeFriendshipResult.OwnMsisdnIsNotSpecified;

				IQueryable<VEHICLE> friendVehicleQuery = entities.GetVehicleQuery();
				friendVehicleQuery = friendVehicleQuery.Where(v => v.CONTROLLER.PhoneContactID == friendContact.ID);

				friendVehicleQuery = friendVehicleQuery.Where(v => v.CONTROLLER.MLP_Controller.Asid.OPERATOR != null);

				var friendSharedVehicle = friendVehicleQuery.FirstOrDefault(
					v => v.OPERATOR_VEHICLE.Any(access =>
						access.OPERATOR_ID == operatorID &&
						access.RIGHT_ID    == (int) SystemRight.VehicleAccess &&
						access.ALLOWED));
				if (friendSharedVehicle != null)
				{
					if (entities.DeleteAccess(entities.GetOperator(operatorID), friendSharedVehicle, SystemRight.Ignore))
					{
						entities.SaveChanges(context);
					}

					return ProposeFriendshipResult.AlreadyHasAccess;
				}

				var friendshipRequest =
					entities.GetFrienshipRequestsBetween(operatorID, friendMsisdn.Value).FirstOrDefault();
				if (friendshipRequest != null)
				{
					Trace.TraceInformation(
						"{0}.ProposeFriendship: InvitationWasSentEarlier, operatorMsisdn = {1}, friendMsisdn = {2}",
						this, operatorMsisdn.Value, friendMsisdn.Value);
					var notificationAlreadyExists = entities.GetFriendshipRequestNotifications(friendshipRequest).Any();
					return notificationAlreadyExists
						? ProposeFriendshipResult.InvitationSent
						: ProposeFriendshipResult.InvitationRegisteredAndWaitingForAppOnFriendDevice;
				}

				friendshipRequest = entities.RegisterFriendshipRequest(operatorID, friendMsisdn);
				entities.SaveChanges(context);

				var messages = new List<MESSAGE>();

				// 1. Отправляем Push, т.к. это самый дешёвый канал отправки сообщений
				var msisdnUser = entities.GetOperatorByConfirmedMsisdn(msisdn);
				if (msisdnUser != null)
				{
					var operatorNameAsFriendOfMsisdn = GetFriendName(msisdnUser.OPERATOR_ID, operatorID);
					var culture = GetOperatorCulture(msisdnUser);
					var invitationText = ResourceContainers.Get(culture)["heWantsLocateYou"];
					messages.AddRange(entities.CreateAppNotification(msisdnUser.OPERATOR_ID, operatorNameAsFriendOfMsisdn, invitationText));
					messages.Add     (entities.CreateWebNotification(msisdnUser.OPERATOR_ID, operatorNameAsFriendOfMsisdn, invitationText));
				}

				// 2. Не удалось отправить Push, значит, отправляем SMS или запрос на интерворкинг
				if (messages.Count == 0)
				{
					var messageGatewayId = entities.GetMessageGatewayId((ContactType)friendContact.Type,
						friendContact.Value, Msg::MessageTemplate.ProposeFriendship);
					if (messageGatewayId == null)
					{
						// Проверяем, сможет ли друг зарегистрироваться в системе
						var registrationIsPossibleForFriend =
							entities.GetMessageGatewayId((ContactType)friendContact.Type, friendContact.Value,
								Msg::MessageTemplate.GenericConfirmation) != null;
						return registrationIsPossibleForFriend
							? ProposeFriendshipResult.InvitationRegisteredAndWaitingForAppOnFriendDevice
							: ProposeFriendshipResult.UnsupportedNetworkProvider;
					}

					var smsMessages = new List<MESSAGE>();

					smsMessages.AddRange(
						entities.SendSmsToUser(
							friendContact,
							entities.GetMessageTemplate(Msg::MessageTemplate.ProposeFriendship),
							$@"Здравствуйте, я хочу определять ваше местоположение. ",
							$@"Установите мобильное приложение Ufin, чтобы дать мне соответствующее разрешение. ",
							$@"С уважением, +{operatorMsisdn.Value}."
					));
					// Убираем отправку данного СМС
					/*
					smsMessages.AddRange(
						entities.SendSmsToUser(
							friendContact,
							entities.GetMessageTemplate(MESSAGE_TEMPLATE.ProposeFriendship),
							"Режим или запрет предоставления информации вы сможете задать непосредственно в приложении."));
					*/
					foreach (var smsMessage in smsMessages)
						smsMessage.DestinationType_ID = messageGatewayId.Value;
					messages.AddRange(smsMessages);
				}

				foreach (var message in messages)
				{
					message.Owner_Operator_ID = operatorID;
					message.MESSAGE_TEMPLATE  = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.ProposeFriendship);
					message.GROUP             = friendshipRequest.MESSAGE_ID;
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, operatorMsisdn.Value);
				}

				entities.SaveChanges(context);

				return ProposeFriendshipResult.InvitationSent;
			}
		}
		/// <summary> Уведомляет пользователя об изменении права </summary>
		/// <param name="adminOperatorId"> Идентификатор пользователя, который выполнил операцию изменения прав. </param>
		/// <param name="operatorId"> Идентификатор пользователя, чьи права были изменены. </param>
		/// <param name="vehicleId"> Идентификатор ТС права, на которое было изменено. </param>
		/// <param name="allowed"> Было ли право разрешено или запрещено </param>
		/// <param name="saveContext"> Контекст для привязки изменений. </param>
		public void NotifyUserAboutRightChange(int? adminOperatorId, int operatorId, int vehicleId, bool allowed, SaveContext saveContext)
		{
			using (var entities = new Entities())
			{
				if (entities.TrackingSchedule.Any(
						ts => ts.Enabled && ts.Operator_ID == operatorId && ts.Vehicle_ID == vehicleId))
				{
					entities.CreateAppNotificationAboutVehicleDataChange(vehicleId, UpdatedDataType.Schedule);
					entities.SaveChanges(saveContext);
				}

				var msisdn = entities.GetOwnMsisdn(operatorId);
				if (msisdn == null)
					return;
				// Отключаем отправку СМС при изменении прав
				if (0 == 1)
					NotifyUserAboutRightChange(adminOperatorId, msisdn, vehicleId, allowed, saveContext);
			}
		}
		/// <summary> Уведомляет пользователя о получении или утрате прав на объект </summary>
		/// <param name="adminOperatorId">Идентификатор пользователя владельца ТС</param>
		/// <param name="msisdn">Телефон пользователя, которому предоставляется право</param>
		/// <param name="vehicleId">Идентификатор ТС, права на который меняются</param>
		/// <param name="allowed">Флаг разрешения</param>
		/// <param name="saveContext">Контекст для сохранения истории изменений</param>
		public void NotifyUserAboutRightChange(int? adminOperatorId, string msisdn, int vehicleId, bool allowed, SaveContext saveContext)
		{
			if (msisdn == null)
				throw new ArgumentNullException("msisdn");

			using (var entities = new Entities())
			{
				var operatorByMsisdn = entities.GetOperatorByConfirmedMsisdn(msisdn);
				var vehicle = entities
					.VEHICLE
					.Where(v => v.VEHICLE_ID == vehicleId)
					.Select(v => new { Id = v.VEHICLE_ID, Msisdn = v.CONTROLLER.PhoneContact.Value })
					.First();

				var ownMsisdn = adminOperatorId != null ? entities.GetOwnMsisdn(adminOperatorId.Value) : null;
				var pushMessage = GetNotifyAboutRightChangeMessage(entities, operatorByMsisdn, vehicle, SystemRight.VehicleAccess, allowed);
				var notifyMessage = new MESSAGE
				{
					//Делаем subject пустым, чтобы при отправке не происходило добавления имени объекта в заголовок уведомления,
					//т.к. имя объекта уже содержится в теле сообщения
					SUBJECT            = string.Empty,
					BODY               = pushMessage,
					DestinationType_ID = (int)MessageDestinationType.Notification,
					Owner_Operator_ID  = adminOperatorId,
					TIME               = DateTime.UtcNow
				};

				entities.AddMessageField(notifyMessage, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicle.Id);
				if (ownMsisdn != null)
					entities.AddMessageField(notifyMessage, MESSAGE_TEMPLATE_FIELD.PushMessage, pushMessage);
				var vehicleMsisdn = string.IsNullOrEmpty(vehicle.Msisdn) ? ownMsisdn : vehicle.Msisdn;
				if(!string.IsNullOrEmpty(vehicleMsisdn))
					entities.AddMessageField(notifyMessage, MESSAGE_TEMPLATE_FIELD.Msisdn, vehicleMsisdn);
				if(operatorByMsisdn != null)
					entities.AddMessageField(notifyMessage, MESSAGE_TEMPLATE_FIELD.OperatorId, operatorByMsisdn.OPERATOR_ID);

				////////////////////////////////////////////////
				/// TODO: Проверить нужно ли, пока убираем???
				/// Отправляется СМС от имени выдающего права в данном случае это неправильно
				//Contact sourceContact = null;
				//if (!string.IsNullOrEmpty(ownMsisdn))
				//	sourceContact = entities.GetContact(ContactType.Phone, ownMsisdn);
				//else if (!string.IsNullOrEmpty(vehicle.Msisdn))
				//	sourceContact = entities.GetContact(ContactType.Phone, vehicle.Msisdn);
				//if(sourceContact != null)
				//	notifyMessage.AddSource(sourceContact);
				////////////////////////////////////////////////
				if (!string.IsNullOrWhiteSpace(msisdn))
					notifyMessage.AddDestination(entities.GetContact(ContactType.Phone, msisdn));

				entities.SaveChanges(saveContext);
			}
		}
		private string GetNotifyAboutRightChangeMessage(Entities entities, OPERATOR @operator, dynamic vehicle, SystemRight right, bool allowed)
		{
			var cultureInfo = @operator != null
				? entities.GetOperatorCulture(@operator.OPERATOR_ID)
				: CultureInfo.CurrentCulture;
			var strings = ResourceContainers.Get(cultureInfo);
			var message = string.Empty;
			var operatorId = @operator != null ? @operator.OPERATOR_ID : (int?)null;
			var vehicleName = GetVehicleName(operatorId, vehicle.Id);
			switch (right)
			{
				case SystemRight.VehicleAccess:
					var format = allowed
						? strings["YouCanDetectSubscriberLocation"]
						: strings["YouCantDetectSubscriberLocation"];

					message = string.Format(format, vehicleName);
					break;
			}

			return message;
		}
		/// <summary> Устанавливает доступ к ТС </summary>
		/// <param name="adminOperatorId"> Идентификатор оператора-владельца ТС </param>
		/// <param name="operatorId"> Идентификатор оператора, для кого меняется набор прав </param>
		/// <param name="vehicleId"> Идентификатор ТС, на которое меняется набор прав </param>
		/// <param name="right"> Набор прав </param>
		/// <param name="allowed"> Флаг разрешить или запретить права </param>
		/// <param name="saveContext"> Контекст для привязки изменений </param>
		public SetAccessToVehicleResult SetAccessToVehicle(int adminOperatorId, int operatorId, int vehicleId, SystemRight right, bool allowed, SaveContext saveContext)
		{
			bool notify;
			using (var entities = new Entities())
			{
				var ormVehicle = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicleId);

				var departmentType = ormVehicle.DEPARTMENT != null
					? ormVehicle.DEPARTMENT.Type.HasValue
						? (DepartmentType)ormVehicle.DEPARTMENT.Type
						: DepartmentType.Corporate
					: DepartmentType.Physical;
				var replacer = VehicleSystemRightsReplacer.GetRightReplacer(departmentType, IdType.Vehicle, right);
				var rights = replacer != null ? replacer.Rights : null;
				if (rights == null)
					throw new ArgumentOutOfRangeException("right", right, @"Right don't exists");

				entities.CheckAdminOfVehicle(adminOperatorId, vehicleId);
				entities.SetAccessToVehicle(operatorId, ormVehicle, rights, allowed);

				entities.SaveChanges(saveContext);

				if (rights.Contains(SystemRight.VehicleAccess))
					entities.UnregisterFriendshipRequest(operatorId, adminOperatorId);

				notify = rights.Contains(SystemRight.VehicleAccess);

				if (notify && entities.TrackingSchedule.Any(ts => ts.Enabled && ts.Operator_ID == operatorId && ts.Vehicle_ID == vehicleId))
					entities.CreateAppNotificationAboutVehicleDataChange(vehicleId, UpdatedDataType.Schedule);

				entities.SaveChanges(saveContext);
			}

			if (notify)
				NotifyUserAboutRightChange(adminOperatorId, operatorId, vehicleId, allowed, saveContext);

			if (right == SystemRight.VehicleAccess)
				OnVehicleAccessGranted(operatorId, vehicleId);

			return SetAccessToVehicleResult.AccessGranted;
		}
		public bool DelAccessToVehicle(int? adminOperatorId, int operatorId, int vehicleId, SystemRight[] rights, SaveContext saveContext)
		{
			bool notify;
			using (var entities = new Entities())
			{
				if (adminOperatorId != null)
					entities.CheckAdminOfVehicle(adminOperatorId.Value, vehicleId);

				var ormOperator = entities.GetOperator(operatorId);
				if (ormOperator == null)
					throw new ArgumentOutOfRangeException(nameof(operatorId), operatorId, @"Operator not found by id");

				var intRights = rights.Select(item => (int)item).ToArray();
				var query = entities.OPERATOR_VEHICLE
					.Where(x =>
						x.OPERATOR_ID == operatorId &&
						x.VEHICLE_ID  == vehicleId  &&
						intRights.Contains(x.RIGHT_ID));

				var accesses = query.ToList();

				if (accesses.Count == 0)
					return false;

				foreach (var access in accesses)
					entities.OPERATOR_VEHICLE.DeleteObject(access);

				notify = rights.Contains(SystemRight.VehicleAccess);

				entities.SaveChanges(saveContext);
			}

			if (notify)
				NotifyUserAboutRightChange(adminOperatorId, operatorId, vehicleId, false, saveContext);

			return true;
		}
		public SystemRight[] GetRemovableRightsOnVehicle(int operatorId, int vehicleId)
		{
			SystemRight[] rights;
			using (var entities = new Entities())
			{
				rights = entities.OPERATOR_VEHICLE
					.Where(ov =>
						ov.OPERATOR_ID == operatorId                              &&
						ov.VEHICLE_ID  == vehicleId                               &&
						ov.RIGHT_ID    != (int)SystemRight.SecurityAdministration &&
						ov.RIGHT_ID    != (int)SystemRight.Ignore)
					.Select(ov => (SystemRight)ov.RIGHT_ID)
					.ToArray();
			}
			return rights;
		}
		public bool IsSuperAdministrator(int operatorId)
		{
			using (var entities = new Entities())
				return entities.IsSuperAdministrator(operatorId);
		}
		public bool IsSalesManager(int operatorId, int? departmentId)
		{
			using (var entities = new Entities())
				return entities.IsSalesManager(operatorId, departmentId);
		}
		public bool IsDefaultDepartment(int operatorId, int? departmentId)
		{
			using (var entities = new Entities())
				return entities.IsDefaultDepartment(operatorId, departmentId);
		}
		public int[] GetOperatorsByPhoneBookContact(string msisdn)
		{
			return DbManager.GetOperatorsByPhoneBookContact(msisdn);
		}
		private readonly ConcurrentDictionary<CultureInfo, Dictionary<string, BusinessLogic.DTO.Icon>> _icons
			= new ConcurrentDictionary<CultureInfo, Dictionary<string, BusinessLogic.DTO.Icon>>();
		public Dictionary<string, BusinessLogic.DTO.Icon> GetIcons(CultureInfo culture = null)
		{
			culture = culture ?? CultureInfo.InvariantCulture;
			var dic = _icons.GetOrAdd(culture, c => GetVehicleIcons(c).ToDictionary(i => i.Name));
			return dic;
		}
		private BusinessLogic.DTO.Icon[] GetVehicleIcons(CultureInfo culture = null)
		{
			var container = ResourceContainers.Get(culture ?? CultureInfo.InvariantCulture);
			var icons = container.GetVehicleIcons();
			var result = new List<BusinessLogic.DTO.Icon>(icons.Count);
			foreach (var iconPair in icons)
			{
				var icon = new BusinessLogic.DTO.Icon
				{
					Name = iconPair.Key
				};
				if (string.IsNullOrEmpty(VehicleIconPath))
				{
					var stream = new MemoryStream();
					iconPair.Value.Save(stream, ImageFormat.Png);
					icon.Value = Convert.ToBase64String(stream.ToArray());
				}
				else
				{
					var fileName = string.Format("{0}.png", iconPair.Key);
					icon.Value = Path.Combine(VehicleIconPath, fileName);
				}

				result.Add(icon);
			}

			return result.ToArray();
		}
		private string _vehicleIconPath;
		public string VehicleIconPath
		{
			get { return _vehicleIconPath; }
		}
		public void ImportVehicleIcons(string path, CultureInfo culture = null)
		{
			var container = ResourceContainers.Get(culture ?? CultureInfo.InvariantCulture);
			var icons = container.GetVehicleIcons();
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);
			foreach (var icon in icons)
			{
				var fileName = string.Format("{0}.png", icon.Key);
				var filePath = Path.Combine(path, fileName);
				if (!File.Exists(filePath))
				{
					icon.Value.Save(filePath, ImageFormat.Png);
				}
			}

			_vehicleIconPath = path;
		}
		public string GetVehicleIcon(string icon, VehicleKind vehicleKind, CultureInfo culture = null)
		{
			icon = icon ?? string.Empty;
			var icons = GetIcons(culture);
			if (icons.ContainsKey(icon))
				icon = icons[icon].Value.Replace(_vehicleIconPath + "\\", string.Empty);
			return icon;
		}
		protected static HashSet<string> LoadIgnoredFreqLimitIps()
		{
			using (var entities = new Entities())
				return new HashSet<string>(entities.Ip
					.Where(ip => !ip.FreqLimitEnabled)
					.Select(ip => ip.Value), StringComparer.OrdinalIgnoreCase);
		}
		protected HashSet<string> IgnoredFreqLimitIps = LoadIgnoredFreqLimitIps();
		public void AcceptRequestWithLimit(BanLimit limit, string ip)
		{
			if (IgnoredFreqLimitIps.Contains(ip))
				return;

			using (var entities = new Entities())
			{
				var banDate = entities.AcceptRequestWithLimit(limit.ToString(), ip).First();
				if (!banDate.HasValue)
					return;

				var date = TimeHelper.GetDateTimeUTC(banDate.Value);
				throw new OperationBanException(date, limit);
			}
		}
		private readonly TerminalServerPinger _terminalServerPinger;
		private class TerminalServerPinger : BackgroundProcessor
		{
			public TerminalServerPinger(Server server) : base(TimeSpan.FromSeconds(1))
			{
				_server = server;
			}
			private readonly Server _server;
			protected override bool Do()
			{
				bool terminalClientIsAlive = false;
				try
				{
					var client = _server._terminalClient;
					if (client != null)
					{
						var session = client.Session;
						if (session != null)
							terminalClientIsAlive = session.IsAlive();
					}
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();
					return false;
				}

				if (!terminalClientIsAlive)
					$"Terminal client is not alive now"
						.CallTraceWarning();
				return false;
			}
		}
		public IEnumerable<Rht::Operator> GetFriends(int operatorId, int? departmentId)
		{
			using (var entities = new Entities())
			{
				// Готовим запрос машин на которые у operatorId есть право SecurityAdministration
				var vehiclesQuery = entities
					.v_operator_vehicle_right
					.Where(v =>
						v.operator_id == operatorId &&
						v.right_id    == (int)SystemRight.SecurityAdministration)
					.Select(my => my.VEHICLE);
				// Если есть департамент, то накладываем на запрос дополнительное условие
				if (departmentId != null)
					vehiclesQuery = vehiclesQuery
						.Where(v => v.Department_ID == departmentId.Value);

				var friends = vehiclesQuery
					// Выбираем у машин на которые у пользователя есть право SecurityAdministration всех операторов
					.SelectMany(v => v.Accesses) // Accesses это элементы представления v_operator_vehicle_right
					// Выбираем только тех пользователей у которых право пришло из группы или напрямую
					.Where(friend =>
						friend.right_id    == (int)SystemRight.VehicleAccess &&
						friend.operator_id != operatorId                     &&
						(
							friend.priority == (int)EffectiveRightPriority.Direct
							||
							friend.priority == (int)EffectiveRightPriority.Group)
						)
					// У каждого элемента v_operator_vehicle_right выбираем записи таблицы OPERATOR
					.Select(friend => friend.OPERATOR)
					// Объединяем полученный список со списком друзей из группы друзей (у группы право Friendship)
					.Union(entities.FriendsGroupQuery(operatorId)
						// Выбираем операторов
						.SelectMany(g => g.Members))
					// Возвращаем только уникальных операторов из объединенного набора друзей
					.Distinct()
					// Результатом будет анонимный тип содержащий друзей
					.Select(o => new
					{
						o.OPERATOR_ID,
						o.LOGIN,
						o.NAME,
						AsidPhone = o.Asid.Contact.DemaskedPhone.Value,
						Phones = o.Contacts
							.Where(oc =>
								oc.Confirmed &&
								oc.Contact.Type == (int) ContactType.Phone)
							.Select(c => c.Contact.Value),
						Emails = o.Contacts
							.Where(oc =>
								oc.Confirmed &&
								oc.Contact.Type == (int) ContactType.Email)
							.Select(c => c.Contact.Value)
					})
					.ToList();
				// Видимо Distinct не работает, поэтому группируем по OPERATOR_ID
				var friendsLookup = friends
					.ToLookup(x => x.OPERATOR_ID)
					.ToList();
				// Дальнейшее заполнение элементов результирующего набора операторов
				var msisdns      = friends.SelectMany(f => f.Phones).ToArray();
				var msisdnToName = entities.GetOperatorMsisdnToNameDictionary(operatorId);
				var phoneBook    = SearchPhoneBook(operatorId, msisdns);

				var result = new List<Rht::Operator>(friends.Count);
				foreach (var g in friendsLookup)
				{
					var friend = g.First();

					var friendDto = new Rht::Operator
					{
						id    = friend.OPERATOR_ID,
						login = friend.LOGIN,
						name  = friend.NAME
					};
					result.Add(friendDto);
					friendDto.Emails = friend.Emails
						.Select(x => new BusinessLogic.DTO.Contact(ContactType.Email, x))
						.ToArray();
					var phones = new List<BusinessLogic.DTO.Contact>();
					if (friend.AsidPhone != null)
						phones.Add(new BusinessLogic.DTO.Contact(ContactType.Phone, friend.AsidPhone));
					phones.AddRange(friend.Phones.Select(x => new BusinessLogic.DTO.Contact(ContactType.Phone, x)));
					friendDto.Phones = phones.ToArray();

					friendDto.FormatContactInformation();

					AssignUserNameFromContacts(friendDto, msisdnToName, phoneBook);
				}

				return result;
			}
		}
		public IEnumerable<Rht::Operator> GetAccessesToVehicle(int operatorId, Department department, int vehicleId)
		{
			using (var entities = new Entities())
			{
				entities.CheckAdminOfVehicle(operatorId, vehicleId);

				var operatorsQuery = entities.OPERATOR
					.Where(o =>
						o.OPERATOR_ID != operatorId &&
						o.OPERATOR_VEHICLE
							.Any(ov =>
								ov.VEHICLE_ID == vehicleId &&
								ov.RIGHT_ID   == (int)SystemRight.VehicleAccess &&
								ov.ALLOWED));

				var departmentType = department != null ? department.Type : DepartmentType.Physical;
				var rightIds = VehicleSystemRightsReplacer
					.GetSystemRights(departmentType, IdType.Vehicle)
					.Select(r => (int)r)
					.ToArray();

				var rights = operatorsQuery
					.SelectMany(o => o.OPERATOR_VEHICLE)
					.Where(ov => ov.VEHICLE_ID == vehicleId && rightIds.Contains(ov.RIGHT_ID))
					.ToLookup(ov => ov.OPERATOR_ID,
						ov => new Rht::Right
						{
							allow    = ov.ALLOWED,
							right_id = ov.RIGHT_ID,
							right    = (SystemRight)ov.RIGHT_ID
						});

				var operators =
					operatorsQuery
						.Select(x => new Rht::Operator
						{
							id = x.OPERATOR_ID,
							name = x.NAME,
							login = x.LOGIN,
						}).ToList();

				var operatorContacts = operatorsQuery
					.SelectMany(o => o.Contacts)
					.Where(
						oc => oc.Confirmed &&
							  oc.Contact.Type == (int)ContactType.Phone)
					.ToList();
				var operatorContactsLookup = operatorContacts
					.ToLookup(
						oc => oc.Operator_ID,
						oc =>
							new BusinessLogic.DTO.Contact
							{
								Type = (ContactType)oc.Contact.Type,
								Value = oc.Contact.Value
							});
				var msisdns = operatorContacts.Select(c => c.Contact.Value).ToArray();


				var msisdnToName = entities.GetOperatorMsisdnToNameDictionary(operatorId);
				var phoneBook = SearchPhoneBook(operatorId, msisdns);

				foreach (var @operator in operators)
				{
					@operator.vehicles = new[]
					{
						new Rht::Vehicle
						{
							id = vehicleId,
							rights = rights[@operator.id].ToArray()
						}
					};
					@operator.Phones = operatorContactsLookup[@operator.id].ToArray();

					AssignUserNameFromContacts(@operator, msisdnToName, phoneBook);
				}

				return operators;
			}
		}
		public IEnumerable<Rht::Operator> GetAllBelongOperators(int operatorId, int departmentId)
		{
			using (var entities = new Entities())
			{
				// Получаем список зависимых операторов.
				return entities
					.GetAllBelongOperators(operatorId, departmentId)
					.Select(o =>
						new Rht::Operator
						{
							id    = o.OPERATOR_ID,
							name  = o.NAME,
							login = o.LOGIN
						})
					.OrderBy(x => x.name)
					.ToList();
			}
		}
		private static void AssignUserNameFromContacts(Rht::Operator @operator, Dictionary<string, string> msisdnToName, Dictionary<string, string> phoneBook)
		{
			if (@operator.Phones == null)
				return;

			if (phoneBook != null)
			{
				foreach (var phone in @operator.Phones)
				{
					string name;
					if (!phoneBook.TryGetValue(phone.Value, out name)
						|| string.IsNullOrEmpty(name))
						continue;
					@operator.name = name;
					return;
				}
			}

			if (msisdnToName != null)
			{
				foreach (var phone in @operator.Phones)
				{
					string name;
					if (!msisdnToName.TryGetValue(phone.Value, out name)
						|| string.IsNullOrEmpty(name))
						continue;
					@operator.name = name;
					return;
				}
			}
		}
	}
}