﻿using System;
using System.Collections.Generic;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public IEnumerable<ShareLink> GetShareLinks(Pagination pagination, CancellationToken cancellationToken)
		{
			return Server.Instance()
				.GetShareLinks(pagination, cancellationToken, OperatorId);
		}
		public ShareLink ShareLinkCreate(ShareLink dtoShareLink)
		{
			dtoShareLink.CreatorId = OperatorId;
			return Server.Instance()
				.ShareLinkCreate(dtoShareLink);
		}
		public ShareLink ShareLinkRead(Guid shareLinkGuid)
		{
			var dtoShareLink = new ShareLink { LinkGuid = shareLinkGuid, CreatorId = OperatorId };
			return Server.Instance()
				.ShareLinkRead(dtoShareLink);
		}
		public ShareLink ShareLinkUpdate(ShareLink dtoShareLink)
		{
			dtoShareLink.CreatorId = OperatorId;
			return Server.Instance()
				.ShareLinkUpdate(dtoShareLink);
		}
		public void ShareLinkDelete(Guid shareLinkGuid)
		{
			var dtoShareLink = new ShareLink { LinkGuid = shareLinkGuid, CreatorId = OperatorId };
			Server.Instance()
				.ShareLinkDelete(dtoShareLink);
		}
	}
}