﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using Interfaces.Web.Enums;
using Contact = FORIS.TSS.EntityModel.Contact;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		internal static readonly ContactType[] TypesOfUserDefinedContacts = { ContactType.Email, ContactType.Phone };

		public SendConfirmationCodeResult SendConfirmationCode(ContactType type, string value, CultureInfo culture, string ipAddress)
		{
			value = CheckContact(type, value);

			int contactId;
			using (var entities = new Entities())
				contactId = entities.GetContactId(type, value);

			var messageFormatString = ResourceContainers
				.Get(culture)
				.GetLocalizedMessagePart("BeelineConfirmationSmsMessageFormat");
			return SendConfirmationCode(null, contactId, messageFormatString, null, ipAddress, SaveContext.None());
		}

		private static string CheckContact(ContactType type, string value)
		{
			switch (type)
			{
				case ContactType.Phone:
					var normalizedValue = ContactHelper.GetNormalizedPhone(value);
					if (normalizedValue == null)
						throw new ArgumentOutOfRangeException("value", value, @"Value is not valid msisdn");
					value = normalizedValue;
					break;
				default:
					throw new NotImplementedException(type.ToString());
			}
			return value;
		}

		public SendConfirmationCodeResult SendConfirmationCode(int? operatorId, int contactId, string messageFormatString, string subjectFormatString, string ipAddress, SaveContext saveContext)
		{
			using (var entities = new Entities())
			{
				Contact contact;
				Operator_Contact operatorContact = null;
				if (operatorId != null)
				{
					operatorContact = entities.Operator_Contact
						.Include(Operator_Contact.ContactIncludePath)
						.FirstOrDefault(oc => oc.Contact_ID == contactId && oc.Operator_ID == operatorId.Value);
					if (operatorContact == null)
						return SetNewContactResult.ContactDoesNotExist;
					contact = operatorContact.Contact;
				}
				else
				{
					contact = entities.Contact.FirstOrDefault(c => c.ID == contactId);
				}

				if (contact == null)
					return SetNewContactResult.ContactDoesNotExist;

				if (entities.Operator_Contact.Any(
					oc => oc.Operator_ID == operatorId.Value && oc.Confirmed && oc.Contact.Type == contact.Type))
					return SetNewContactResult.ConfirmedContactAlreadyExists;

				var type = (ContactType)contact.Type;
				if (!TypesOfUserDefinedContacts.Contains(type))
					throw new ArgumentOutOfRangeException("contactId", contactId,
						@"Contact types " + string.Join(", ", TypesOfUserDefinedContacts) + @" are only supported");

				var lastConfirmationCodeMessage = entities.MESSAGE
					.Where(
						m =>
							m.MESSAGE_TEMPLATE.NAME ==  Msg::MessageTemplate.GenericConfirmation &&
							m.Contacts.Any(
								c => c.Type == (int)MessageContactType.Destination && c.Contact_ID == contactId))
					.OrderByDescending(m => m.MESSAGE_ID)
					.Select(
						m =>
							new
							{
								m.Created,
								Sent = m.TIME,
								State = (MessageState)m.STATUS,
								RemainingQuantityString =
									m.MESSAGE_FIELD.Where(
										mf =>
											mf.MESSAGE_TEMPLATE_FIELD.NAME ==
											MESSAGE_TEMPLATE_FIELD.RemainingAttemptCount)
										.Select(mf => mf.CONTENT)
										.FirstOrDefault()
							})
					.FirstOrDefault();

				if (lastConfirmationCodeMessage != null)
				{
					if (lastConfirmationCodeMessage.State != MessageState.Done)
					{
						return new SendConfirmationCodeResult
						{
							Code    = SetNewContactResult.Success,
							EndDate = DateTime.UtcNow.AddMinutes(entities.GetConfirmationLifetimeMinutes())
						};
					}

					var confirmationValidDate = DateTime.UtcNow.AddMinutes(-entities.GetConfirmationLifetimeMinutes());
					var confirmationSentDate = lastConfirmationCodeMessage.Created;
					var confirmationValid = confirmationSentDate.HasValue &&
											confirmationSentDate > confirmationValidDate;
					if (confirmationValid)
					{
						int remainingAttemptQuantity;
						if (!int.TryParse(lastConfirmationCodeMessage.RemainingQuantityString,
							out remainingAttemptQuantity) ||
							remainingAttemptQuantity == 0)
						{
							return new SendConfirmationCodeResult
							{
								Code = SetNewContactResult.Failed,
								EndDate = confirmationSentDate.Value.AddMinutes(
									entities.GetConfirmationLifetimeMinutes())
							};
						}

						var plannedConfirmationEnd =
							confirmationSentDate.Value.AddMinutes(
								entities.GetConfirmationLifetimeMinutes());

						//Код подтверждения уже отправлен
						if (DateTime.UtcNow < plannedConfirmationEnd)
						{
							return new SendConfirmationCodeResult
							{
								Code = SetNewContactResult.Success,
								EndDate = plannedConfirmationEnd
							};
						}
					}
				}

				AcceptRequestWithLimit(BanLimit.SmsConfirmation, ipAddress);

				string confirmationKey;
				switch (type)
				{
					case ContactType.Email:
						confirmationKey = GenerateSmsKey(); //GenerateRandomKey(16);
						break;
					case ContactType.Phone:
						confirmationKey = GenerateSmsKey();
						break;
					default:
						throw new ArgumentOutOfRangeException("contactId", type,
							@"Contact types " + string.Join(", ", TypesOfUserDefinedContacts) + @" are only supported");
				}

				var subject = subjectFormatString != null ? string.Format(subjectFormatString, confirmationKey) : null;

				var body = string.Format(messageFormatString, confirmationKey);

				var message = entities.CreateOutgoingMessage(contact, subject, body);
				message.MESSAGE_TEMPLATE =
					entities.MESSAGE_TEMPLATE.First(t => t.NAME == Msg::MessageTemplate.GenericConfirmation);
				message.Owner_Operator_ID = operatorId;
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.ConfirmationCode, confirmationKey);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.RemainingAttemptCount, entities.GetConstantAsInt(Constant.VerifyAttemptCount) ?? 5);
				if (operatorContact != null)
					operatorContact.ConfirmationSentDate = DateTime.UtcNow;
				entities.SaveChanges(saveContext);

				return new SendConfirmationCodeResult
				{
					Code = SetNewContactResult.Success,
					EndDate = message.TIME.AddMinutes(entities.GetConfirmationLifetimeMinutes())
				};
			}
		}

		public VerifyContactResult VerifyContact(ContactType type, string value, string code, string ip)
		{
			value = CheckContact(type, value);

			int contactId;
			using (var entities = new Entities())
				contactId = entities.GetContactId(type, value);

			return VerifyContact(null, contactId, code, ip, SaveContext.None());
		}

		public VerifyContactResult VerifyContact(int? operatorId, int contactId, string confirmationCode, string ip, SaveContext saveContext)
		{
			if (string.IsNullOrWhiteSpace(confirmationCode))
				return SetNewContactResult.Failed;

			string contactValue;
			int currentOwnVehicleId;

			string loginPrefix;
			string currentLogin;
			ContactType contactType;
			using (var entities = new Entities())
			{
				var ormContact = entities.Contact.FirstOrDefault(c => c.ID == contactId);
				if (ormContact == null)
					return SetNewContactResult.Failed;
				contactValue = ormContact.Value;
				contactType  = (ContactType)ormContact.Type;

				Operator_Contact operatorContact;
				if (operatorId != null)
				{
					operatorContact = entities
						.Operator_Contact
						.FirstOrDefault(oc => oc.Contact_ID == contactId && oc.Operator_ID == operatorId.Value);

					if (operatorContact == null) //Контакта не существует
						return SetNewContactResult.Failed;

					if (operatorContact.Confirmed)
						return SetNewContactResult.Failed;

					//Разрешается подтверждать не более одного контакта данного типа на пользователя
					if (entities.Operator_Contact
							.Any(oc =>
								oc.Operator_ID == operatorId.Value &&
								oc.Confirmed &&
								oc.Contact.Type == (int) contactType))
						return SetNewContactResult.ConfirmedContactAlreadyExists;
				}

				AcceptRequestWithLimit(BanLimit.MsisdnVerification, ip);

				var lastConfirmationCode = entities.MESSAGE
					.Where(m => m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.GenericConfirmation &&
						m.Contacts.Any(c =>
							c.Type == (int) MessageContactType.Destination &&
							c.Contact_ID == contactId))
					.OrderByDescending(m => m.MESSAGE_ID)
					.Select(m => new
					{
						State = (MessageState) m.STATUS,
						Code = m.MESSAGE_FIELD
							.Where(mf => mf.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.ConfirmationCode)
							.Select(mf => mf.CONTENT)
							.FirstOrDefault(),
						RemainingAttemptCountId = m.MESSAGE_FIELD
							.Where(mf => mf.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.RemainingAttemptCount)
							.Select(mf => mf.MESSAGE_FIELD_ID)
							.FirstOrDefault(),
						Time = m.Created
						//TODO: обдумать, возможно, следует указывать поле TIME, а не дату создания,
					})
					.FirstOrDefault();

				if (lastConfirmationCode == null)
					return SetNewContactResult.Failed;

				// Контроль времени жизни подтверждения
				var confirmationValidDate = DateTime.UtcNow.AddMinutes(-entities.GetConfirmationLifetimeMinutes());
				if (lastConfirmationCode.State == MessageState.Done && lastConfirmationCode.Time < confirmationValidDate)
					return SetNewContactResult.ConfirmationCodeExpired;

				//Контроль количества попыток
				int remainingAttemptCountValue;
				using (var transaction = new Transaction())
				{
					var remainingAttemptCountField = entities.MESSAGE_FIELD
						.FirstOrDefault(mf => mf.MESSAGE_FIELD_ID == lastConfirmationCode.RemainingAttemptCountId);

					if (remainingAttemptCountField == null)
						return SetNewContactResult.Failed;

					if (!int.TryParse(remainingAttemptCountField.CONTENT, out remainingAttemptCountValue))
						return SetNewContactResult.Failed;

					if (remainingAttemptCountValue == 0)
					{
						return new VerifyContactResult
						{
							Code = SetNewContactResult.AttemptsQuantityIsExhausted,
							RemainingAttemptCount = remainingAttemptCountValue
						};
					}

					--remainingAttemptCountValue;

					remainingAttemptCountField.SetContent(remainingAttemptCountValue.ToString());

					entities.SaveChanges(saveContext);
					transaction.Complete();
				}

				if (lastConfirmationCode.Code != confirmationCode)
				{
					return new VerifyContactResult
					{
						Code = SetNewContactResult.InvalidConfirmationCode,
						RemainingAttemptCount = remainingAttemptCountValue
					};
				}

				var otherOperatorContactConfirmedQuery = entities.Operator_Contact
					.Where(oc =>
						oc.Contact_ID == contactId &&
						oc.Confirmed);

				if (operatorId != null)
				{
					var operatorIdClosure = operatorId.Value;
					otherOperatorContactConfirmedQuery = otherOperatorContactConfirmedQuery
						.Where(oc => oc.Operator_ID != operatorIdClosure);
				}

				var otherOperator = otherOperatorContactConfirmedQuery
					.Select(oc => new {oc.Operator_ID})
					.FirstOrDefault();

				if (otherOperator != null)
				{
					if (operatorId == null)
						return new VerifyContactResult
						{
							Code  = SetNewContactResult.Success,
							SimId = entities.GetOrCreateSimIdByOperatorId(otherOperator.Operator_ID, null, saveContext)
						};

					//TODO: удалить подтверждение другого пользователя к этому телефону
					foreach (var otherOperatorContact in otherOperatorContactConfirmedQuery.ToList())
						otherOperatorContact.Confirmed = false;

					entities.SaveChanges(saveContext);

				}

				if (operatorId == null)
				{
					// Создаётся новый пользователь
					var ormOperator = entities.GetOrCreateOperatorByContact(ormContact, saveContext);
					if (ormOperator == null)
						throw new InvalidOperationException("Unable to create operator");
					operatorId      = ormOperator.OPERATOR_ID;
					operatorContact = entities.Operator_Contact
						.First(oc =>
							oc.Confirmed &&
							oc.Operator_ID == operatorId &&
							oc.Contact_ID  == ormContact.ID);
				}
				else
				{
					operatorContact = entities.Operator_Contact
						.FirstOrDefault(oc =>
							oc.Operator_ID == operatorId.Value &&
							oc.Contact_ID  == contactId);
				}

				if (operatorContact == null)
				{
					return SetNewContactResult.Failed;
				}

				operatorContact.Confirmed = true;
				operatorContact.Removable = entities.IsCorporateCustomer(operatorId.Value);
				entities.SaveChanges(saveContext);
				Trace.TraceInformation("{0}: Contact {1} confirmed by VerifyContact", this, operatorContact);

				// Ищем актуальные запросы дружбы и пересылаем их на клиент
				//TODO: то же самое нужно сделать при появлении нового контакта на устройстве

				entities.ResendFriendshipRequests(operatorId.Value, saveContext,
					entities.GetCloudContactsForOperator(operatorId.Value));

				loginPrefix = entities.GetConstant(CONSTANTS.NewLoginPrefix) ?? string.Empty;

				currentLogin = entities.OPERATOR
					.Where(o => o.OPERATOR_ID == operatorId)
					.Select(o => o.LOGIN)
					.FirstOrDefault();
				currentOwnVehicleId = entities.VEHICLE
					.Where(v =>
						v.CONTROLLER.MLP_Controller.Asid.OPERATOR.OPERATOR_ID == operatorId &&
						 v.CONTROLLER.PhoneContact == null)
					.Select(v => v.VEHICLE_ID)
					.FirstOrDefault();
			}
			if (contactType == ContactType.Phone)
			{
				if (string.IsNullOrWhiteSpace(currentLogin))
				{
					//Назначение логина и пароля
					var setNewLoginResult = SetNewLogin(operatorId.Value, operatorId.Value, loginPrefix + contactValue,
						false, saveContext);
					if (setNewLoginResult != SetNewLoginResult.Success)
						Trace.TraceWarning("{0}.VerifyContact({1}, ..) unable to assign login", this, contactId);
				}

				if (currentOwnVehicleId != default(int))
				{
					using (var entities = new Entities())
					{
						var controllers = entities.CONTROLLER.Where(
							c => c.VEHICLE.VEHICLE_ID != currentOwnVehicleId &&
								 c.PhoneContactID == contactId)
							.ToList();

						if (controllers.Count != 0)
						{
							foreach (var controller in controllers)
							{
								controller.PHONE = string.Empty;
								controller.PhoneContactID = null;
							}

							entities.SaveChanges(saveContext);
						}
					}

					var setVehicleDevicePhoneResult = SetVehicleDevicePhone(operatorId.Value, currentOwnVehicleId,
						contactValue, saveContext);
					if (setVehicleDevicePhoneResult != SetVehicleDevicePhoneResult.Success)
					{
						Trace.TraceWarning(
							"VerifyContact(contactId={0}), operatorId = {1}, setVehicleDevicePhoneResult = {2}",
							contactId, operatorId, setVehicleDevicePhoneResult);
					}
				}
			}
			SendNotificationsFromPhoneBook(operatorId.Value);

			var result = new VerifyContactResult();
			result.Code = SetNewContactResult.Success;
			result.SimId = GetOrCreateSimId(operatorId.Value, null /*TODO: получить departmentId*/);
			return result;
		}

		private string GetOrCreateSimId(int operatorID, int? departmentId)
		{
			using (var entities = new Entities())
			{
				return entities.GetOrCreateSimIdByOperatorId(operatorID, departmentId);
			}
		}
	}
}