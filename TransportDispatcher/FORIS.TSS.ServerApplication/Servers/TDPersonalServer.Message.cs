﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Billing;
using Interfaces.Web;
using Dto = FORIS.TSS.BusinessLogic.DTO;
using Enm = FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		List<Dto::Message> IWebPersonalServer.GetMessages(int[] vehicleIDs, DateTime? @from, DateTime? to)
		{
			return Server.Instance().GetMessages(OperatorId, vehicleIDs, @from, to);
		}
		public List<Dto::Message> GetMessages(MessageType messageDirection, Enm::MessageTemplate template)
		{
			using (var entities = new Entities())
			{
				var templateName = template.ToString();
				var messageQuery = entities.MESSAGE.Where(m => m.MESSAGE_TEMPLATE.NAME == templateName);

				switch (messageDirection)
				{
					case MessageType.Outgoing:
						messageQuery = messageQuery.Where(m => m.Owner_Operator_ID == OperatorId);
						break;
					case MessageType.Incoming:
						throw new NotImplementedException("messageDirection " + messageDirection + " is not implemented");
					default:
						throw new ArgumentOutOfRangeException(nameof(messageDirection), messageDirection, @"Value is not supported");
				}

				return messageQuery.ToList().Select(m => m.ToDto()).ToList();
			}
		}
		/// <summary> Возвращает все сообщения текущего пользователя мобильного клиента по идентификатору пользователя и AppId сессии </summary>
		/// <returns></returns>
		public Dto::Messages.Message[] GetMessagesForAppClient(int? lastMessageId, DateTime? from, DateTime? to, Pagination pagination)
		{
			if (string.IsNullOrEmpty(AppId))
				throw new ArgumentException("AppId is empty. Client must be login with mobile application to use this method");

			var messages = Server.Instance().GetMessagesByAppId(OperatorId, AppId, lastMessageId, from, to, pagination);
			return messages;
		}
		/// <summary> Возвращает все сообщения текущего пользователя по идентификатору пользователя и набору объектов наблюдения </summary>
		/// <returns></returns>
		public Dto::Messages.Message[] GetMessagesForOperator(int? lastMessageId, DateTime? @from, DateTime? to, Pagination pagination, int[] vehicleIds)
		{
			var messages = Server.Instance()
				.GetMessagesByOperator(OperatorId, lastMessageId, @from, to, pagination, vehicleIds);
			return messages;
		}
		public Dto::Historical.HistoryMarked<Dto::Messages.MessageWeb> GetShortMessagesByMsisdn(string msisdn, int? firstMessageId, Pagination pagination)
		{
			if(!IsSuperAdministrator)
				throw new SecurityException("not allowed");
			return new Dto::Historical.HistoryMarked<Dto::Messages.MessageWeb>
			{
				HistoryId = TimeHelper.GetSecondsFromBase(DateTime.UtcNow),
				Items     = Server.Instance()
					.GetMessagesByMsisdn(msisdn, firstMessageId, pagination)
			};
		}
		public Dto::Historical.HistoryMarked<Dto::Messages.MessageWeb> GetShortMessagesByVehicle(int vehicleId, int? firstMessageId, Pagination pagination)
		{
			var items = new List<Dto::Messages.MessageWeb>();
			using (var entities = new Entities())
			{
				if (!IsSuperAdministrator &&
					!IsSalesManager       &&
					!entities.IsAllowedVehicleAny(OperatorId, vehicleId,
						SystemRight.ChangeDeviceConfigBySMS,
						SystemRight.Immobilization,
						SystemRight.CommandAccess))
					throw new SecurityException("not allowed");

				var vehiclePhone = entities.VEHICLE
					?.Where(v => v.VEHICLE_ID == vehicleId)
					?.Select(v => new
					{
						v.VEHICLE_ID,
						v.CONTROLLER.PHONE
					})
					?.FirstOrDefault()
					?.PHONE;
				if (!string.IsNullOrWhiteSpace(vehiclePhone))
					items = Server.Instance().GetMessagesByMsisdn(vehiclePhone, firstMessageId, pagination);
				return new Dto::Historical.HistoryMarked<Dto::Messages.MessageWeb>
				{
					HistoryId = TimeHelper.GetSecondsFromBase(DateTime.UtcNow),
					Items     = items
				};
			}
		}
		public void SendSms(string dstMsisdn, string srcMsisdn, string body)
		{
			if(!IsSuperAdministrator)
				throw new SecurityException("not allowed");

			using (var entities = new Entities())
			{
				var dstContact = entities.GetContact(Enm::ContactType.Phone, dstMsisdn);
				if (dstContact == null)
					throw new SecurityException($@"No destination contact {dstMsisdn}");

				var srcContact = entities.GetContact(Enm::ContactType.Phone, srcMsisdn);
				if (srcContact == null)
					throw new SecurityException($@"No source contact {srcMsisdn}");

				var messages = entities.CreateOutgoingSms(dstContact, body);
				foreach (var message in messages)
					message.AddSource(srcContact);

				entities.SaveChangesByOperator(OperatorId);
			}
		}
		public void SendConfigurationSmsToVehicle(int vehicleId, string message)
		{
			using (var entities = new Entities())
			{
				if (!IsSuperAdministrator &&
					!IsSalesManager       &&
					!entities.IsAllowedVehicleAny(OperatorId, vehicleId,
						SystemRight.ChangeDeviceConfigBySMS,
						SystemRight.Immobilization,
						SystemRight.CommandAccess))
					throw new SecurityException("not allowed");
				SendShortMessageToVehicle(entities, vehicleId, message, PaidService.SendConfigurationSms);
			}
		}
		public void SendInformationalSmsToVehicle(int vehicleId, string message)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedVehicleAll(OperatorId, vehicleId, SystemRight.VehicleAccess))
					throw new SecurityException("not allowed");
				SendShortMessageToVehicle(entities, vehicleId, message, PaidService.SendInformationalSms);
			}
		}
		public void SendShortMessageToVehicle(Entities entities, int vehicleId, string messageBody, PaidService? paidService = null)
		{
			if (string.IsNullOrWhiteSpace(messageBody))
				return; // Нечего отправлять

			var vehicle = entities.VEHICLE
				?.Where(v => v.VEHICLE_ID == vehicleId)
				?.Select(v => new
				{
					v.VEHICLE_ID,
					v.CONTROLLER.PHONE
				})
				?.FirstOrDefault();
			if (string.IsNullOrWhiteSpace(vehicle?.PHONE))
				return;

			var dstContact = entities.GetContact(Enm::ContactType.Phone, vehicle.PHONE);
			var srvContact = entities.GetContact(Enm::ContactType.Phone, entities.GetConstant(Enm::Constant.ServiceInternationalPhoneNumber));
			var messages = entities
				.CreateOutgoingSms(dstContact, messageBody);
			foreach (var message in messages)
			{
				message.Owner_Operator_ID = OperatorId;
				if (paidService.HasValue && paidService.Value == PaidService.SendConfigurationSms)
					message.AddSource(srvContact);
				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId);
			}
			entities.SaveChangesBySession(SessionId);
		}
		void IWebPersonalServer.SendQuestionToSupport(string contacts, string trackerNumber, string question)
		{
			Server.Instance().SendQuestionToSupport(
				SessionInfo, Department?.name, trackerNumber, contacts, question);
		}
		public void SendCalculationRequest(CalculationRequest customerForm)
		{
			Server.Instance().SendCalculationRequest(SessionInfo, customerForm);
		}
		public int[] UpdateMessageStatus(int[] messageIds, Enm::MessageProcessingResult messageProcessingResult)
		{
			if (messageIds == null)
				throw new ArgumentNullException(nameof(messageIds));
			if (messageIds.Length == 0)
				return new int[0];

			//Через этот вызов можно менять статус доставки только собственных сообщений
			var messageIdsWithApp = FilterAppMessagesByOperatorId(messageIds);
			var messageIdsByOwner = FilterMessagesByOwner(messageIds);

			messageIds = messageIdsByOwner.Union(messageIdsWithApp).ToArray();
			if (messageIds.Length == 0)
				return new int[0];

			Server.Instance().SetMessageProcessingResult(messageProcessingResult, messageIds);

			if (!messageIdsWithApp.Any())
				return messageIds;
			// Разблокируем контакты и помечаем их годными
			using (var entities = new Entities())
			{
				foreach (var ids in messageIdsWithApp.SplitByCount(100))
				{
					var contactIds = entities.Message_Contact
						.Where(mc =>
							mc.Type == (int)Enm::MessageContactType.Destination &&
							ids.Contains(mc.Message_ID) &&
							mc.Contact.LockedUntil.HasValue)
						.Select(c => c.Contact.ID)
						.Distinct()
						.ToList();

					if (contactIds.Any())
					{
						var contacts = entities.Contact.Where(c => contactIds.Contains(c.ID));
						foreach (var contact in contacts)
						{
							contact.LockedUntil = null;
							contact.LockedCount = 0;
							contact.Valid = true;
						}
						entities.SaveChanges();
					}

					// Все групповые сообщения помечаем выполненными
					var groupIds = entities.MESSAGE
						.Where(m => ids.Contains(m.MESSAGE_ID))
						.Where(m => m.GROUP != null && m.STATUS == (int)Enm::MessageState.Done)
						.Select(m => m.GROUP)
						.Distinct()
						.ToArray();

					if (groupIds.Any())
					{
						var groupMessages = entities.MESSAGE.Where(m => groupIds.Contains(m.MESSAGE_ID)).ToArray();
						foreach (var groupMessage in groupMessages)
							groupMessage.STATUS = (int)Enm::MessageState.Done;

						entities.SaveChanges();
					}
				}
			}

			return messageIds;
		}
		private int[] FilterAppMessagesByOperatorId(int[] messageIds)
		{
			using (var entities = new Entities())
			{
				var filteredByOperatorId = new List<int>(messageIds.Length);
				foreach (var part in messageIds.SplitByCount(100))
				{
					filteredByOperatorId.AddRange(
						entities
							.Message_Contact
							.Where(mc => part.Contains(mc.Message_ID) &&
										 ContactTypeHelper.AppContactTypeIds.Contains(mc.Contact.Type) &&
										 mc.Contact.AppClient.Any(app => app.Operator_ID == OperatorId))
							.Select(mc => mc.Message_ID));
				}

				messageIds = filteredByOperatorId.ToArray();
			}
			return messageIds;
		}
		private int[] FilterMessagesByOwner(int[] messageIds)
		{
			using (var entities = new Entities())
			{
				var filteredByOperatorId = new List<int>(messageIds.Length);
				foreach (var part in messageIds.SplitByCount(100))
				{
					filteredByOperatorId.AddRange(
						entities.MESSAGE
							.Where(m => part.Contains(m.MESSAGE_ID) &&
										m.Owner_Operator_ID == OperatorId)
							.Select(m => m.MESSAGE_ID));

				}

				messageIds = filteredByOperatorId.ToArray();
			}

			return messageIds;
		}
		public void WrongMessageDestination(int messageId, string wrongAppId)
		{
			using (var entities = new Entities())
			{
				var contactId =
					entities.Message_Contact
						.Where(mc => mc.Message_ID == messageId && mc.Type == (int)Enm::MessageContactType.Destination)
						.Select(mc => mc.Contact_ID)
						.FirstOrDefault();

				if (contactId == default(int))
					return;

				Server.Instance().SetMessageProcessingResult(Enm::MessageProcessingResult.MismatchAppId, messageId);

				//Проверка без блокировки
				if (!entities.AppClient.Any(
					app => app.Contact_ID == contactId &&
						   app.AppId == wrongAppId))
					return;

				using (var transaction = new Transaction())
				{
					var appClient = entities.AppClient
						.FirstOrDefault(
							app => app.Contact_ID == contactId &&
								   app.AppId == wrongAppId);

					if (appClient != null)
					{
						appClient.Contact_ID = null;
						entities.SaveChangesBySession(SessionId);
					}

					transaction.Complete();
				}
			}
		}
		Dto::Message IWebPersonalServer.SendCustomerTypeChangeRequest()
		{
			if (!(Department?.TypeIsChangeable ?? false))
				throw new InvalidOperationException();

			var messages = Server.Instance().SendMessageToSupport(
				SessionInfo, Enm::MessageTemplate.SwitchToCorporativeRequest, null, null, GetFieldValue);

			return messages != null ? messages.FirstOrDefault() : null;
		}
		private string GetFieldValue(Entities entities, string name, object model, CultureInfo cultureInfo)
		{
			switch (name)
			{
				case "Department.Name":
					return Department?.name ?? string.Empty;
				case "AppSettings.UrlWebGis":
					return ConfigHelper.GetAppSettingAsString("UrlWebGis", string.Empty);
				case "Department.Department_ID":
					//return Department != null ? Department.id.ToString(CultureInfo) : string.Empty;
					return Department?.id.ToString(CultureInfo) ?? string.Empty;
				case "Department.Region":
					if (Department?.extID == null)
						return string.Empty;
					return
						entities.Asid
							.Where(a =>
								a.OPERATOR.OPERATOR_ID     == OperatorId &&
								a.Billing_Service_Provider != null)
							.Select(a => a.Billing_Service_Provider.Name)
							.FirstOrDefault() ?? string.Empty;
				default:
					return name;
			}
		}
	}
}