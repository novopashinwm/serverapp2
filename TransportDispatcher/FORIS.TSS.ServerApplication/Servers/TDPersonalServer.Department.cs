﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using Interfaces.Web;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		private int?           ComputedDepartmentId   => Department?.id;
		private DepartmentType ComputedDepartmentType => Department?.Type ?? DepartmentType.Physical;
		public Department      Department { get; private set; }
		/// <summary> Текущий клиент юридическое лицо? </summary>
		/// <returns> Да/Нет </returns>
		private bool IsCorporateCustomer()
		{
			return ComputedDepartmentType == DepartmentType.Corporate;
		}
		/// <summary> Текущий клиент это физическое лицо? </summary>
		/// <returns> Да/Нет </returns>
		private bool IsPhysicalCustomer()
		{
			return ComputedDepartmentType == DepartmentType.Physical;
		}
		private bool IsDefaultDepartment()
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				return false; // Пользователя можно создать только в департаменте

			using (var entities = new Entities())
				return entities.IsDefaultDepartment(OperatorId, departmentId.Value);
		}
		/// <summary> Разрешено ли текущему оператору создавать пользователей </summary>
		public bool AllowAddOperator
		{
			get
			{
				var departmentId = ComputedDepartmentId;
				if (departmentId == null)
					return false; // Пользователя можно создать только в департаменте

				if (IsPhysicalCustomer() && !IsSuperAdministrator)
					return false;

				using (var entities = new Entities())
				{
					if (!entities.v_operator_department_right
						.Any(odr =>
							odr.operator_id   == OperatorId &&
							odr.Department_ID == departmentId.Value &&
							odr.right_id      == (int)SystemRight.SecurityAdministration))
						return false;
				}
				return true;
			}
		}
		/// <summary> Разрешен ли номер договора, используется только для России </summary>
		public bool DepartmentExtIDIsAllowed
		{
			get { return IsSuperAdministrator && Department != null && Department.countryName == Country.Russia; }
		}

		void IWebPersonalServer.SelectDepartment(int? departmentId)
		{
			if (departmentId.HasValue)
			{
				using (var entities = new Entities())
				{
					// Ищем среди департаментов к которым у текущего пользователя есть доступ (право DepartmentsAccess)
					var ormDepartment = entities.GetAvailableDepartmentsByOperatorIdQuery(OperatorId)
						.FirstOrDefault(d => d.DEPARTMENT_ID == departmentId.Value);
					// Если указанный департамент не доступен для текущего пользователя, выдаем исключение нарушения безопасности
					if (ormDepartment == null)
						throw new SecurityException("Access to department with id " + departmentId.Value + " is denied");
					// Если указанный департамент доступен текущему пользователю, то переключаем департамент
					SelectDepartmentInternal(entities, ormDepartment);
				}
			}
			else
				Department = null;
		}
		private void SelectDepartmentInternal(Entities entities, DEPARTMENT ormDepartment)
		{
			Department = ormDepartment?.ToDto();
			if (null == Department)
				return;
			// При смене департамента необходимо сбрасывать иначе некорректно работает, т.к. зависит от пользователя и департамента
			_isSalesManager = null;
			Department.Rights = entities.v_operator_department_right
				.Where(odr =>
					odr.operator_id   == OperatorId &&
					odr.Department_ID == Department.id)
				.Select(odr => (SystemRight)odr.right_id)
				.Distinct()
				.ToArray();

			Department.TypeIsChangeable =
				Department.Type == DepartmentType.Physical &&
				(
					IsSuperAdministrator ||
					//Администратор сервисов Ufin
					entities.v_operator_department_right
						.Any(odr =>
							odr.operator_id   == OperatorId    &&
							odr.Department_ID == Department.id &&
							odr.right_id      == (int)SystemRight.SecurityAdministration)
				);
		}
		public CreateDepartmentResult CreateDepartment(string name, string adminLogin, string departmentExtID, out int departmentId)
		{
			if (!IsSuperAdministrator && !IsSalesManager)
				throw new SecurityException("Operator should have ServiceManagement right to perform this operation");

			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentException(@"Parameter cannot be empty", nameof(name));

			if (departmentExtID != null && !IsSuperAdministrator && !IsSalesManager)
				throw new SecurityException("Creating customer using DepartmentExtID is not allowed for current user");

			// Создаем департамент
			using (var entities = new Entities())
			{
				// Убираем лишние пробелы в имени пользователя администратора, если оно не отсутствует
				if (adminLogin != null)
					adminLogin = adminLogin.Trim();
				// Создаем транзакцию
				using (var transaction = new Transaction())
				{
					// Проверяем наличие такого же департамента по имени
					if (entities.DEPARTMENT.Any(d => d.NAME == name))
					{
						transaction.Complete();
						departmentId = -1;
						return CreateDepartmentResult.DepartmentAlreadyExists;
					}
					// Проверяем наличие такого же департамента по номеру договора
					if (departmentExtID != null)
					{
						if (entities.DEPARTMENT.Any(d => d.ExtID == departmentExtID))
						{
							transaction.Complete();
							departmentId = -1;
							return CreateDepartmentResult.DepartmentAlreadyExists;
						}
					}
					// Создаем пользователя администратора, для нового департамента
					var ormAdminOperator = default(OPERATOR);
					if (!string.IsNullOrWhiteSpace(adminLogin))
					{
						// Логин пользователя не может быть почтой
						if (ContactHelper.IsValidEmail(adminLogin))
						{
							transaction.Complete();
							departmentId = -1;
							return CreateDepartmentResult.DepartmentAdminLoginNotAllowedAsEmail;
						}
						// Логин пользователя не может быть номером телефона
						if (ContactHelper.IsValidPhone(adminLogin))
						{
							transaction.Complete();
							departmentId = -1;
							return CreateDepartmentResult.DepartmentAdminLoginNotAllowedAsPhone;
						}
						// Проверяем существование логина
						if (entities.OPERATOR.Any(o => o.LOGIN == adminLogin))
						{
							transaction.Complete();
							departmentId = -1;
							return CreateDepartmentResult.DepartmentAdminAlreadyExists;
						}
						// Создаем оператора
						ormAdminOperator = new OPERATOR
						{
							LOGIN    = adminLogin,
							PASSWORD = adminLogin,
							NAME     = adminLogin
						};
						entities.OPERATOR.AddObject(ormAdminOperator);
						// Создание группы друзей
						entities.CreateFriendGroup(ormAdminOperator);
						// Сохраняем пользователя
						entities.SaveChangesBySession(SessionId);
					}

					// Создаем департамент
					var ormDepartment = new DEPARTMENT { NAME = name, ExtID = departmentExtID };
					entities.DEPARTMENT.AddObject(ormDepartment);

					// Страну департамента ставим такую же как для текущего департамента
					if (Department != null && !string.IsNullOrWhiteSpace(Department.countryName))
						ormDepartment.Country = entities.Country.FirstOrDefault(c => c.Name == Department.countryName);

					// Создаем для департамента группу операторов
					var ormOperatorGroup = new OPERATORGROUP { NAME = name };
					entities.OPERATORGROUP.AddObject(ormOperatorGroup);

					// Выдаем права на группу операторов департамента
					foreach (var systemRight in UserRole.UfinCorpUser.DepartmentRights)
					{
						entities.OPERATORGROUP_DEPARTMENT.AddObject(
							new OPERATORGROUP_DEPARTMENT
							{
								OPERATORGROUP = ormOperatorGroup,
								DEPARTMENT    = ormDepartment,
								RIGHT         = entities.GetSystemRight(systemRight),
								ALLOWED       = true
							});
					}
					// Если департамент создается с администратором, выдаем права администратора департамента
					if (ormAdminOperator != null)
					{
						// Добавляем в группу пользователей департамента
						ormOperatorGroup.Members.Add(ormAdminOperator);
						// Создаваемому администратору департамента, выдаем права в системе для роли администратора
						entities.Allow(ormAdminOperator,                UserRole.UfinCorpAdmin.UserRights);
						// Создаваемому администратору департамента, выдаем права в департаменте для роли администратора
						entities.Allow(ormAdminOperator, ormDepartment, UserRole.UfinCorpAdmin.DepartmentRights);
					}

					var ormCurrentOperator = entities.GetOperator(OperatorId);
					// Оператору создателю департамента выдаем права супер-администратора на создаваемый департамент
					entities.Allow(ormCurrentOperator, ormDepartment, UserRole.UfinSuperAdmin.DepartmentRights);

					// Группе супер-администраторов выдаем права супер-администратора на создаваемый департамент
					var salesOperatorGroupId = entities.SalesOperatorGroupID;
					if (salesOperatorGroupId != null)
					{
						var salesOperatorGroup = entities.OPERATORGROUP
							.First(og => og.OPERATORGROUP_ID == salesOperatorGroupId.Value);
						entities.Allow(salesOperatorGroup, ormDepartment, UserRole.UfinSuperAdmin.DepartmentRights);
					}
					// Сохраняем изменения
					entities.SaveChangesBySession(SessionId);
					// Применяем транзакции
					transaction.Complete();

					departmentId = ormDepartment.DEPARTMENT_ID;

					return CreateDepartmentResult.Success;
				}
			}
		}
		/// <summary> Создает в системе нового оператора </summary>
		/// <param name="login"></param>
		/// <param name="password"></param>
		/// <param name="nickname"> Имя оператора </param>
		/// <param name="operInfo"> созданный оператор </param>
		/// <returns></returns>
		public CreateOperatorResult CreateDepartmentOperator(string login, string password, string nickname, out Rht::Operator operInfo)
		{
			operInfo = null;
			if (string.IsNullOrEmpty(password))
				return CreateOperatorResult.InvalidPassword;

			if (!Server.IsLoginValid(login))
				return CreateOperatorResult.InvalidLogin;

			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new SecurityException("Department is not assigned");

			using (var entities = new Entities())
			{
				if (!AllowAddOperator)
					throw new SecurityException("Creating operators is forbidden");

				CheckAdministrativeRightsOnDepartment(entities);

				CreateOperatorResult res;

				OPERATOR newOperator;
				using (var transaction = new Transaction())
				{
					res = entities.CreateNewOperator(login, password, nickname, string.Empty, out newOperator);
					entities.SaveChangesBySession(SessionId);
					transaction.Complete();
				}

				if (res != CreateOperatorResult.Success)
					return res;

				if (newOperator == null)
					throw new InvalidOperationException("newOperator == null");

				if (departmentId.Value > 0)
				{
					entities.AddOperatorToDepartment(newOperator, entities.GetDepartment(departmentId.Value));
					entities.SaveChangesBySession(SessionId);
				}

				operInfo = new Rht::Operator
				{
					id    = newOperator.OPERATOR_ID,
					login = login,
					name  = nickname
				};

				return res;
			}
		}
		/// <summary> Возвращает список департаментов к которым текущий пользователь имеет доступ </summary>
		/// <returns></returns>
		List<Department> IWebPersonalServer.GetAvailableDepartments()
		{
			using (var entities = new Entities())
			{
				return entities.GetAvailableDepartmentsByOperatorIdQuery(OperatorId)
					.ToList()
					.ConvertAll(d => d.ToDto());
			}
		}
		/// <summary> Получить количество департаментов к которым текущий пользователь имеет доступ </summary>
		/// <returns></returns>
		int IWebPersonalServer.GetAvailableDepartmentsCount()
		{
			using (var entities = new Entities())
			{
				return entities.GetAvailableDepartmentsByOperatorIdQuery(OperatorId)
					.Count();
			}
		}
		/// <summary> Поиск департаментов которые позволено администрировать оператору </summary>
		/// <param name="searchText"></param>
		/// <returns></returns>
		List<Department> IWebPersonalServer.SearchAdminDepartments(string searchText)
		{
			using (var entities = new Entities())
			{
				return entities.SearchDepartments(searchText, OperatorId)
					?.Select(d => new Department
					{
						id          = d.DEPARTMENT_ID,
						name        = d.NAME          ?? string.Empty,
						description = d.searchDevices ?? string.Empty,
						countryName = d.countryName   ?? string.Empty,
						extID       = string.Empty,
					})
					?.ToList();
			}
		}
		List<Rht::Operator> IWebPersonalServer.SearchOperators(string searchText)
		{
			if (!IsSuperAdministrator)
				return null;

			var searchByPhone =
				ContactHelper.MinPhoneLength <= searchText.Length && searchText.Length <= ContactHelper.MaxPhoneLength &&
				searchText.All(char.IsDigit);

			var searchByDeviceId = DeviceNumberHelper.ValidTrackerNumber(searchText);
			using(var entities = new Entities())
			{
				return entities.SearchOperators(searchText, searchByPhone, searchByDeviceId)
					.Select(o =>
					{
						var @operator = o.ToDto();
						@operator.description = o.EMAIL;
						return @operator;
					})
					.ToList();
			}
		}
		/// <summary> Возвращает список всех операторов на управление которыми есть права у текущего пользователя </summary>
		List<Rht::Operator> IWebPersonalServer.GetAllBelongOperators()
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				return new List<Rht::Operator>();
			return Server.Instance()
				.GetAllBelongOperators(OperatorId, departmentId.Value)
				.ToList();
		}
		void IWebPersonalServer.ChangeDepartmentType(DepartmentType departmentType)
		{
			if (ComputedDepartmentId == null)
				throw new InvalidOperationException("Department is not selected");

			if (ComputedDepartmentType == departmentType)
				return;

			if (departmentType != DepartmentType.Corporate)
				throw new NotImplementedException("Changing type to " + departmentType + " is not implemented");

			using (var entities = new Entities())
			{
				if (!entities.IsAllowed(OperatorId, SystemRight.ServiceManagement))
					throw new SecurityException("Access to department is denied");
				var ormDepartment = entities.DEPARTMENT
					.FirstOrDefault(d => d.DEPARTMENT_ID == ComputedDepartmentId.Value);
				if (ormDepartment != null)
				{
					entities.ChangeDepartmentType(ormDepartment, departmentType);
					entities.SaveChangesBySession(SessionId);
					SelectDepartmentInternal(entities, ormDepartment);
				}
			}
		}
		public List<SystemRight> GetDepartmentRights()
		{
			var result = new List<SystemRight>();
			if (IsGuest)
				return result;
			//if (IsCorporateCustomer()) т.к. разрешили физикам редактировать группы
				result.Add(SystemRight.EditGroup);
			return result;
		}
		public void SetDepartmentName(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
				throw new ArgumentException("Value cannot be null or whitespace");

			value = value.Trim();

			UpdateDepartmentAttribute(
				entities => !entities.DEPARTMENT.Any(d => d.NAME == value),
				d => d.NAME = value);
		}
		public void SetDepartmentExtID(string value)
		{
			if (Department?.countryName != Country.Russia)
				throw new InvalidOperationException("ExtID editing is allowed only for Russia");

			if (string.IsNullOrWhiteSpace(value))
				value = null;

			UpdateDepartmentAttribute(
				entities => value == null || !entities.DEPARTMENT.Any(d => d.ExtID == value),
				d => d.ExtID = value);
		}
		public void SetDepartmentDescription(string value)
		{
			if (value == null)
				value = string.Empty;

			UpdateDepartmentAttribute(x => true, d => d.Description = value);
		}
		private void UpdateDepartmentAttribute(Predicate<Entities> checkPredicate, Action<DEPARTMENT> modifyAction)
		{
			if (ComputedDepartmentId == null)
				throw new InvalidOperationException("Department is not selected");

			using (var entities = new Entities())
			{
				CheckAdministrativeRightsOnDepartment(entities);
				if (!entities.v_operator_rights
					.Any(odr =>
						odr.operator_id == OperatorId &&
						odr.right_id    == (int)SystemRight.ServiceManagement))
					throw new SecurityException("Operator " + OperatorId + " has no " + nameof(SystemRight.ServiceManagement) + " right");

				if (!checkPredicate(entities))
					throw new ArgumentException("Argument validation failed");

				var ormDepartment = entities.GetDepartment(ComputedDepartmentId.Value);
				if (ormDepartment != null)
				{
					modifyAction(ormDepartment);
					entities.SaveChangesBySession(SessionId);
					SelectDepartmentInternal(entities, ormDepartment);
				}
			}
		}
		public List<Rht::Operator> GetDepartmentOperators(int departmentId)
		{
			using (var entities = new Entities())
			{
				return entities
					.GetDepartmentOperatorsQuery(departmentId)
					.ToList()
					.Select(o => o.ToDto())
					.ToList();
			}
		}
		public List<Rht::Operator> GetDepartmentAdministrators(int departmentId)
		{
			using (var entities = new Entities())
			{
				return entities
					.GetDepartmentAdministratorsQuery(departmentId)
					.ToList()
					.Select(o => o.ToDto())
					.ToList();
			}
		}
	}
}