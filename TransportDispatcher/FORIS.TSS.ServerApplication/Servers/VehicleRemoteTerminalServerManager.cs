﻿using System;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.EntityModel;
using Interfaces.Web;
using RemoteTerminalServerDTO = FORIS.TSS.BusinessLogic.Terminal.Model.RemoteTerminalServer;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Менеджер работы с удаленным терминалом для отправки информации об объекте наблюдения </summary>
	class VehicleRemoteTerminalServerManager : IRemoteTerminalServerManager
	{
		private readonly int          _vehicleId;
		private readonly ISessionInfo _sessionInfo;
		private readonly bool         _isAllowed;
		/// <summary> Конструктор </summary>
		/// <param name="vehicleId"></param>
		/// <param name="sessionInfo"></param>
		public VehicleRemoteTerminalServerManager(int vehicleId, ISessionInfo sessionInfo)
		{
			if (sessionInfo == null)
				throw new ArgumentNullException(nameof(sessionInfo));

			using (var entities = new Entities())
			{
				//_isAllowed = entities.IsAllowedVehicleAll(sessionInfo.OperatorInfo.OperatorId, vehicleId, SystemRight.ServiceManagement);
				_isAllowed = entities.IsSuperAdministrator(sessionInfo.OperatorInfo.OperatorId);
			}

			_vehicleId   = vehicleId;
			_sessionInfo = sessionInfo;
		}
		/// <summary> Получить массив состояния серверов ретрансляции для данного объекта наблюдения </summary>
		/// <returns></returns>
		public RemoteTerminalServerDTO[] GetRemoteTerminalServers()
		{
			if (!_isAllowed)
				return new RemoteTerminalServerDTO[] { };

			using (var entities = new Entities())
			{
				var result = entities
					.Vehicle_RemoteTerminalServer
					.Where(x => x.Vehicle_ID == _vehicleId)
					.Select(x => new RemoteTerminalServerDTO
					{
						Id       = x.RemoteTerminalServer_ID,
						Name     = x.RemoteTerminalServer.Name,
						Enabled  = x.Enabled,
						DeviceId = x.DeviceID
					})
					.ToList();

				result.AddRange(
					entities.RemoteTerminalServer
						.Where(x => !x.Vehicle_RemoteTerminalServer.Any(v => v.Vehicle_ID == _vehicleId))
						.Select(x => new RemoteTerminalServerDTO
						{
							Id       = x.RemoteTerminalServer_ID,
							Name     = x.Name,
							Enabled  = false,
							DeviceId = null
						}));

				result.Sort((x,y)=>string.Compare(x?.Name, y?.Name, StringComparison.OrdinalIgnoreCase));
				return result.ToArray();
			}
		}
		/// <summary> Сохранить настройку ретрансляции объекта наблюдения </summary>
		/// <param name="remoteTerminalServer"></param>
		public void SetRemoteTerminalServers(RemoteTerminalServerDTO remoteTerminalServer)
		{
			if (remoteTerminalServer == null)
				throw new ArgumentNullException(nameof(remoteTerminalServer));

			if (!_isAllowed)
				throw new SecurityException($"Vehicle {_vehicleId} access is denied");

			using (var entities = new Entities())
			{
				var record =
					entities
						.Vehicle_RemoteTerminalServer
						.FirstOrDefault(x =>
							x.Vehicle_ID              == _vehicleId &&
							x.RemoteTerminalServer_ID == remoteTerminalServer.Id);

				if (!remoteTerminalServer.Enabled && string.IsNullOrWhiteSpace(remoteTerminalServer.DeviceId))
				{
					if (record != null)
					{
						entities.Vehicle_RemoteTerminalServer.DeleteObject(record);
						entities.SaveChangesBySession(_sessionInfo.SessionId);
					}
					return;
				}

				if (record == null)
				{
					record = new Vehicle_RemoteTerminalServer
					{
						Vehicle_ID              = _vehicleId,
						RemoteTerminalServer_ID = remoteTerminalServer.Id
					};
					entities.Vehicle_RemoteTerminalServer.AddObject(record);
				}
				record.Enabled  = remoteTerminalServer.Enabled;
				record.DeviceID = remoteTerminalServer.DeviceId;

				entities.SaveChangesBySession(_sessionInfo.SessionId);
			}
		}
	}
}