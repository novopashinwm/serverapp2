﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using Compass.Ufin.Gis;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using Interfaces.Web.Enums;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public List<GeoZone> GetGeoZones(bool includeVertices, int? zoneId = null, bool includeIgnored = false, bool loadSingle = false)
		{
			var departmentId = FilterByDepartment ? ComputedDepartmentId : null;
			return Server.Instance()
				.GetGeoZones(OperatorId, departmentId, includeVertices, zoneId, includeIgnored, loadSingle);
		}
		public DataSet GetZonesWebForOperator(bool vertexGet, int zoneID, bool includeIgnored = false, bool loadSingle = false)
		{
			var departmentId = FilterByDepartment ? ComputedDepartmentId : null;
			return Server.Instance()
				.GetZonesWebForOperator(OperatorId, departmentId, vertexGet, zoneID, includeIgnored, loadSingle);
		}
		public SaveZoneResult AddZoneWebForOperator(GeoZone dtoGeoZone, out int zoneId)
		{
			zoneId = 0;
			// Убираем дубликаты точек
			dtoGeoZone.Points = dtoGeoZone.Points.ExcludeNeighborDuplicates().ToArray();
			// Проверяем правильность примитива
			if (!dtoGeoZone.CheckZonePrimitive())
				return SaveZoneResult.NotValid;

			using (var entities = new Entities())
			{
				try
				{
					var ormGeoZone = entities.GeoZoneCreate(OperatorId, dtoGeoZone, ComputedDepartmentId);
					entities.SaveChangesBySession(SessionId);

					zoneId = ormGeoZone.ZONE_ID;
					Server.Instance().OnZoneAccessGranted(OperatorId, zoneId);
					entities.ZoneMatrixUpsert(zoneId, dtoGeoZone.GetMatrixNodes());
					entities.SaveChangesBySession(SessionId);
				}
				catch (SecurityException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					return SaveZoneResult.Denied;
				}
				catch (KeyNotFoundException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					return SaveZoneResult.ZoneNotFound;
				}
				catch(Exception ex)
				{
					default(string)
						.WithException(ex, true)
						.CallTraceError();
					return SaveZoneResult.UnknownError;
				}
			}

			return SaveZoneResult.Success;
		}
		public SaveZoneResult EdtZoneWebForOperator(GeoZone dtoGeoZone)
		{
			// Убираем дубликаты точек
			dtoGeoZone.Points = dtoGeoZone.Points.ExcludeNeighborDuplicates().ToArray();
			// Проверяем правильность примитива
			if (!dtoGeoZone.CheckZonePrimitive())
				return SaveZoneResult.NotValid;

			using (var entities = new Entities())
			{
				try
				{
					entities.GeoZoneUpdate(OperatorId, dtoGeoZone);
					entities.SaveChangesBySession(SessionId);

					entities.ZoneMatrixUpsert(dtoGeoZone.Id, dtoGeoZone.GetMatrixNodes());
					entities.SaveChangesBySession(SessionId);
				}
				catch (SecurityException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					return SaveZoneResult.Denied;
				}
				catch (KeyNotFoundException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					return SaveZoneResult.ZoneNotFound;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex, true)
						.CallTraceError();
					return SaveZoneResult.UnknownError;
				}
			}

			return SaveZoneResult.Success;
		}
		public void DelZoneWebForOperator(int zoneId)
		{
			using (var entities = new Entities())
			{
				try
				{
					entities.GeoZoneDelete(OperatorId, zoneId);
					entities.SaveChangesBySession(SessionId);
					entities.ZoneMatrixDelete(zoneId);
					entities.SaveChangesBySession(SessionId);
				}
				catch (SecurityException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					throw;
				}
				catch (KeyNotFoundException ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceWarning();
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex, true)
						.CallTraceError();
					throw;
				}
			}
		}
		public Group ZoneGroupCreate(string name)
		{
			using var entities = new Entities();
			var ormZoneGroup = entities.ZoneGroupCreate(OperatorId, name, ComputedDepartmentId);
			entities.SaveChangesBySession(SessionId);
			return new Group
			{
				Id               = ormZoneGroup.ZONEGROUP_ID,
				Name             = ormZoneGroup.NAME,
				ObjectIds        = Array.Empty<int>(),
				Description      = ormZoneGroup.DESCRIPTION,
				AllowAddToGroup  = UserRole.DefaultZoneGroupCreatorRights.Contains(SystemRight.AddZoneToGroup),
				AllowEditContent = UserRole.DefaultZoneGroupCreatorRights.Contains(SystemRight.EditZone)
			};
		}
		public Group ZoneGroupUpdate(string name, int zoneGroupId)
		{
			using var entities = new Entities();
			var ormZoneGroup = entities.ZoneGroupUpdate(OperatorId, zoneGroupId, name);
			entities.SaveChangesBySession(SessionId);

			var dtoZoneGroup = ormZoneGroup.ToDto();
			dtoZoneGroup.AllowAddToGroup  = entities.IsAllowedZoneGroupAll(OperatorId, zoneGroupId, SystemRight.AddZoneToGroup);
			dtoZoneGroup.AllowEditContent = entities.IsAllowedZoneGroupAll(OperatorId, zoneGroupId, SystemRight.EditGroup);
			return dtoZoneGroup;
		}
		public void ZoneGroupDelete(int zoneGroupId)
		{
			using var entities = new Entities();
			entities.ZoneGroupDelete(OperatorId, zoneGroupId);
			entities.SaveChangesBySession(SessionId);
		}
		public void ZoneGroupIncludeZone(int zoneGroupId, int zoneId)
		{
			using var entities = new Entities();
			entities.ZoneGroupIncludeZone(OperatorId, zoneGroupId, zoneId);
			entities.SaveChangesBySession(SessionId);
		}
		public void ZoneGroupExcludeZone(int zoneGroupId, int zoneId)
		{
			using var entities = new Entities();
			entities.ZoneGroupExcludeZone(OperatorId, zoneGroupId, zoneId);
			entities.SaveChangesBySession(SessionId);
		}
	}
}