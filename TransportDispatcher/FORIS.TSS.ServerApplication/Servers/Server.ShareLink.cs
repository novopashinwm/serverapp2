﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		public ShareLink GetShareLinkByGuid(Guid linkGuid)
		{
			using (var entities = new Entities())
			{
				return entities
					?.GetQuery4ShareLinks(linkGuid: linkGuid)
					?.FirstOrDefault()
					?.ToDTO();
			}
		}
		public IEnumerable<ShareLink> GetShareLinks(Pagination pagination, CancellationToken cancellationToken, int operatorId)
		{
			using (var entities = new Entities())
			{
				var queryShareLinks = entities
					.GetQuery4ShareLinks(creatorId: operatorId);
				// Подсчет сколько всего записей
				if (null != pagination && pagination.ComputeTotal)
					pagination.TotalRecords = queryShareLinks.Count();
				// Изменить запрос на выборку части данных
				queryShareLinks = queryShareLinks.OrderBy(e => e.VEHICLE_SHARE_LINK_ID);
				if (null != pagination)
					queryShareLinks = queryShareLinks
						.Skip(pagination.Skip)
						.Take(pagination.RecordsPerPage);
				// Сделать выборку
				var results = queryShareLinks
					.ToList()
					.Select(e => e.ToDTO())
					// Если убрать, то могут ошибки связей
					.ToList();
				// Заполнить количество выбранных данных
				if (null != pagination)
					pagination.TotalPageRecords = results.Count();
				return results;
			}
		}
		public ShareLink ShareLinkCreate(ShareLink dtoShareLink)
		{
			return GetOrCreateShareLink(
				dtoShareLink.CreatorId,
				dtoShareLink.LogTimeBeg,
				dtoShareLink.LogTimeEnd,
				dtoShareLink.VehicleId,
				dtoShareLink.VehicleName,
				dtoShareLink.VehicleDesc);
		}
		public ShareLink ShareLinkRead(ShareLink dtoShareLink)
		{
			using (var entities = new Entities())
			{
				// Поиск общей ссылки
				var ormShareLink = entities
					.GetQuery4ShareLinks(linkGuid: dtoShareLink.LinkGuid, creatorId: dtoShareLink.CreatorId)
					.FirstOrDefault();
				// Общая ссылка не найдена, выдаем исключение
				if (null == ormShareLink)
					throw new KeyNotFoundException(
						$"{nameof(ShareLink)} with Id={dtoShareLink.LinkGuid} not found for OperatorId={dtoShareLink.CreatorId}");
				return ormShareLink.ToDTO();
			}
		}
		public ShareLink ShareLinkUpdate(ShareLink dtoShareLink)
		{
			using (var entities = new Entities())
			{
				if (ShareLink.EmptyGuid == dtoShareLink.LinkGuid)
					throw new ArgumentException(nameof(dtoShareLink.LinkGuid));
				// Поиск общей ссылки
				var ormShareLink = entities
					.GetQuery4ShareLinks(linkGuid: dtoShareLink.LinkGuid, creatorId: dtoShareLink.CreatorId)
					.FirstOrDefault();
				// Если не найдено, выдаем исключение
				if (null == ormShareLink)
					throw new KeyNotFoundException(
						$"{nameof(ShareLink)} with Id={dtoShareLink.LinkGuid} not found for OperatorId={dtoShareLink.CreatorId}");
				// Обновление объекта ORM из объекта DTO
				ormShareLink.UpdateFromDTO(dtoShareLink, entities);
				// Сохранение изменений с использованием оператора для сохранения истории
				entities.SaveChangesByOperator(dtoShareLink.CreatorId);
				// Чтение DTO из БД
				return ShareLinkRead(dtoShareLink);
			}
		}
		public void ShareLinkDelete(ShareLink dtoShareLink)
		{
			using (var entities = new Entities())
			{
				if (ShareLink.EmptyGuid == dtoShareLink.LinkGuid)
					throw new ArgumentException(nameof(dtoShareLink.LinkGuid));
				// Поиск общей ссылки
				var ormShareLink = entities
					.GetQuery4ShareLinks(linkGuid: dtoShareLink.LinkGuid, creatorId: dtoShareLink.CreatorId)
					.FirstOrDefault();
				// Если не найдено, выдаем исключение
				if (null == ormShareLink)
					throw new KeyNotFoundException(
						$"{nameof(ShareLink)} with Id={dtoShareLink.LinkGuid} not found for OperatorId={dtoShareLink.CreatorId}");
				// Удаление общей ссылки
				entities.DeleteObject(ormShareLink);
				// Сохранение изменений с использованием оператора для сохранения истории
				entities.SaveChangesByOperator(dtoShareLink.CreatorId);
			}
		}
	}
}