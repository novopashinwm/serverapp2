﻿using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		public void OnZoneAccessGranted(int operatorId, int zoneId)
		{
			// Отключено принудительное создание неактивных правил на все машины входа/выхода в зону при установке доступа к зоне
			return;
			int[] vehicleIds;
			using (var entities = new Entities())
			{
				vehicleIds = entities.v_operator_vehicle_right
					.Where(x =>
						x.operator_id == operatorId &&
						x.right_id    == (int) SystemRight.VehicleAccess)
					.Select(x => x.vehicle_id)
					.Except(entities.v_operator_vehicle_right
						.Where(x =>
							x.operator_id == operatorId &&
							x.right_id    == (int) SystemRight.Ignore)
						.Select(x => x.vehicle_id))
					.ToArray();
				foreach (var vehicleId in vehicleIds)
				{
					entities.AddVehicleZoneCompoundRule(operatorId, vehicleId, zoneId, ZoneEventType.Incoming);
					entities.AddVehicleZoneCompoundRule(operatorId, vehicleId, zoneId, ZoneEventType.Outgoing);
				}
			}
		}
		public void OnVehicleAccessGranted(int operatorId, int vehicleId)
		{
			// Отключено принудительное создание неактивных правил на все доступные зоны входа/выхода при установке доступа к машине
			return;

			int[] zoneIds;

			using (var entities = new Entities())
			{
				zoneIds = entities.v_operator_zone_right
					.Where(x =>
						x.operator_id == operatorId &&
						x.right_id    == (int)SystemRight.ZoneAccess)
					.Select(x => x.ZONE_ID)
					.Except(entities.v_operator_zone_right
						.Where(x =>
							x.operator_id == operatorId &&
							x.right_id    == (int)SystemRight.Ignore)
						.Select(x => x.ZONE_ID))
					.ToArray();
				foreach (var zoneId in zoneIds)
				{
					entities.AddVehicleZoneCompoundRule(operatorId, vehicleId, zoneId, ZoneEventType.Incoming);
					entities.AddVehicleZoneCompoundRule(operatorId, vehicleId, zoneId, ZoneEventType.Outgoing);
				}
			}
		}
	}
}