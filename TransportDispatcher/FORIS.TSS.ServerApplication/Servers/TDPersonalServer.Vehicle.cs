﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.EntityModel.History;
using FORIS.TSS.Resources;
using Interfaces.Web;
using Interfaces.Web.Enums;
using CommandType = FORIS.TSS.BusinessLogic.DTO.CommandType;
using TrackingSchedule = FORIS.TSS.BusinessLogic.DTO.TrackingSchedule;
using VehicleControlDate = FORIS.TSS.BusinessLogic.DTO.VehicleControlDate;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Функционал, связанный с объектами наблюдения </summary>
	public partial class TDPersonalServer
	{
		private static readonly List<VehicleKind> Mobilekinds = new List<VehicleKind>
		{
			VehicleKind.MobilePhone,
			VehicleKind.Tracker,
			VehicleKind.Smartphone,
			VehicleKind.WebCamera,
		};
		public SetVehicleDeviceIDResult SetVehicleDeviceId(int vehicleId, string deviceId)
		{
			return Server.Instance().SetVehicleDeviceId(OperatorId, vehicleId, deviceId, SaveContext.Session(SessionId));
		}
		public SetVehicleDevicePhoneResult SetVehicleDevicePhone(int vehicleId, string phone)
		{
			return Server.Instance().SetVehicleDevicePhone(OperatorId, vehicleId, phone, SaveContext.Session(SessionId));
		}
		public SetVehicleDevicePasswordResult SetVehicleDevicePassword(int vehicleId, string password)
		{
			if (password == null)
				throw new ArgumentNullException("password");

			using (var entities = new Entities())
			{
				var vehicle =
					entities.GetVehicle(OperatorId, vehicleId, SystemRight.EditingTrackerAttributes).FirstOrDefault();

				if (vehicle == null)
					return SetVehicleDevicePasswordResult.InsufficientRights;

				entities.EnsureControllerInfoExists(vehicle);

				vehicle.CONTROLLER.CONTROLLER_INFO.PASSWORD = password;

				entities.SaveChangesBySession(SessionId);

				return SetVehicleDevicePasswordResult.Success;
			}
		}
		public SetVehicleControllerFieldResult SetVehicleDeviceType(int vehicleId, string trackerType)
		{
			using (var entities = new Entities())
			{
				const SystemRight requiredRight = SystemRight.ChangeDeviceConfigBySMS; // Заменяет EditControllerPhone и/или EditControllerDeviceID
				//const SystemRight requiredRight = SystemRight.EditControllerDeviceID;

				if (!entities.v_operator_vehicle_right.Any(
					ovr => ovr.operator_id == OperatorId &&
						   ovr.vehicle_id == vehicleId &&
						   ovr.right_id == (int) requiredRight))
					return SetVehicleControllerFieldResult.InsufficientRights;

				var newControllerType = entities.GetControllerType(trackerType);

				var controller = entities.CONTROLLER.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == vehicleId);

				if (controller == null)
					return SetVehicleControllerFieldResult.ControllerInfoNotFound;

				if (controller.CONTROLLER_TYPE == newControllerType)
					return SetVehicleControllerFieldResult.Success;

				entities.ChangeControllerType(controller, newControllerType);

				entities.SaveChangesBySession(SessionId);
				return SetVehicleControllerFieldResult.Success;
			}
		}
		public void SetVehiclePosition(int vehicleId, double lat, double lng)
		{
			if (lat < -90 || 90 < lat)
				throw new ArgumentOutOfRangeException("lat", lat, @"Latitude cannot be lesser than -90 or greater than 90");
			if (lng < -180 || 180 < lng)
				throw new ArgumentOutOfRangeException("lng", lng, @"Longitude cannot be lesser than -180 or greater than 180");

			using (var entities = new Entities())
			{
				if (!entities.v_operator_vehicle_right.Any(
					ovr =>
					ovr.operator_id == OperatorId && ovr.vehicle_id == vehicleId &&
					ovr.right_id == (int) SystemRight.SecurityAdministration))
					throw new SecurityException("Operation is forbidden");

				var logTime = TimeHelper.GetSecondsFromBase();

				entities.Log_Time.AddObject(new Log_Time
					{
						Vehicle_ID = vehicleId,
						Log_Time1 = logTime,
						InsertTime = DateTime.UtcNow,
						Media = 0
					});

				entities.Geo_Log.AddObject(new Geo_Log
					{
						Vehicle_ID = vehicleId,
						LOG_TIME = logTime,
						Lat = (decimal) lat,
						Lng = (decimal) lng
					});

				entities.Position_Radius.AddObject(new Position_Radius
					{
						Vehicle_ID = vehicleId,
						Log_Time = logTime,
						Type = (int) PositionType.UserDefined
					});

				entities.GPS_Log.AddObject(new GPS_Log
					{
						Vehicle_ID = vehicleId,
						Log_Time = logTime,
						Speed = null,
						Course = null,
						Satellites = 0,
						Firmware = null
					});

				entities.SaveChanges();
			}
		}
		public SetVehicleControllerFieldResult SetVehicleDeviceIPAddress(int vehicleId, string ip)
		{
			using (var entities = new Entities())
			{
				if (!entities.v_operator_vehicle_right.Any(
					ovr =>
					ovr.operator_id == OperatorId && ovr.vehicle_id == vehicleId &&
					ovr.right_id == (int)SystemRight.ChangeDeviceConfigBySMS)) // Заменяет EditControllerPhone и/или EditControllerDeviceID
					//ovr.right_id == (int) SystemRight.EditControllerDeviceID))
					return SetVehicleControllerFieldResult.InsufficientRights;

				var controllerInfo =
					entities.CONTROLLER_INFO.FirstOrDefault(ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);

				if (controllerInfo == null)
					return SetVehicleControllerFieldResult.ControllerInfoNotFound;

				if (string.Equals(controllerInfo.IP, ip, StringComparison.OrdinalIgnoreCase))
					return SetVehicleControllerFieldResult.Success;
				controllerInfo.IP = ip;
				entities.SaveChangesBySession(SessionId);
				return SetVehicleControllerFieldResult.Success;
			}
		}
		public Group GetVehicleZoneGroup(int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return null;

				var g =
					entities.ZONEGROUP.Where(zg => zg.VEHICLE.Any(v => v.VEHICLE_ID == vehicleId))
							.OrderBy(zg => zg.ZONEGROUP_ID)
							.FirstOrDefault();
				return g != null ? g.ToDto() : null;
			}
		}
		public SetVehicleFieldResult SetVehicleName(int vehicleId, string value)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);

				if (vehicle.GARAGE_NUMBER == value)
					return SetVehicleFieldResult.Success;
				vehicle.GARAGE_NUMBER = value;
				entities.SaveChangesBySession(SessionId);
				return SetVehicleFieldResult.Success;
			}
		}
		public SetVehicleFieldResult SetMaxAllowedSpeed(int vehicleId, int? value)
		{
			if (value < 0)
				return SetVehicleFieldResult.IncorrectValue;

			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				vehicle.MaxAllowedSpeed = value;
				entities.SaveChangesBySession(SessionId);

				return SetVehicleFieldResult.Success;
			}
		}
		public List<CommandType> GetCommandTypes(int vehicleId, bool includeScenario)
		{
			var result = Server.Instance().GetCommandTypes(SessionInfo.OperatorInfo, vehicleId, includeScenario);

			if (result.Count == 0)
				return result;

			var resourceContainers = ResourceContainers.Get(CultureInfo);
			foreach (var type in result)
			{
				type.Name        = resourceContainers.GetLocalizedText<CommandType>($"{type.Id}");
				type.Description = resourceContainers.GetLocalizedText<CommandType>($"{type.Id}Description");
				type.Warning     = resourceContainers.GetLocalizedText<CommandType>($"{type.Id}Warning");
			}
			result.Sort(CommandType.CompareByName);

			return result;
		}
		public string GetVehiclePassword(int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!entities.v_operator_vehicle_right.Any(ovr =>
					ovr.operator_id == OperatorId &&
					ovr.vehicle_id  == vehicleId &&
					ovr.right_id    == (int)SystemRight.ChangeDeviceConfigBySMS))
					return null;

				return entities.CONTROLLER_INFO
					.Where(ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId)
					.Select(ci => ci.PASSWORD)
					.FirstOrDefault();
			}
		}
		public ControllerType GetVehicleControllerType(int vehicleId)
		{
			using (var entities = new Entities())
			{
				return entities.VEHICLE
					?.Where(v =>
						v.VEHICLE_ID == vehicleId &&
						v.Accesses
							.Any(a =>
								a.operator_id == OperatorId &&
								a.right_id    == (int)SystemRight.VehicleAccess))
					?.Select(v => v.CONTROLLER.CONTROLLER_TYPE)
					?.FirstOrDefault()
					?.ToDto(ResourceContainers.Get(CultureInfo));
			}
		}
		public DeleteVehicleResult DeleteVehicle(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var isOwnedVehicle = entities.v_operator_vehicle_right.Any(
					access => access.right_id    == (int) SystemRight.SecurityAdministration &&
							  access.vehicle_id  == vehicleId &&
							  access.operator_id == OperatorId);

				if (!isOwnedVehicle)
				{
					var vehicleBelongsToCurrentDepartment = entities.v_operator_department_right.Any(
						odr => odr.operator_id == OperatorId &&
							   odr.right_id    == (int)SystemRight.DepartmentsAccess &&
							   odr.Department.VEHICLE.Any(v => v.VEHICLE_ID == vehicleId));
					if (vehicleBelongsToCurrentDepartment)
						return DeleteVehicleResult.VehicleNotInDepartment;

					IgnoreVehicle(vehicleId, true);
					return DeleteVehicleResult.Success;
				}

				var isUserVehicle = entities.IsUserVehicle(vehicleId);
				//Объект наблюдения представляет пользователя системы, удалять такой нельзя
				//TODO: рассмотреть случай подключения услуги Ufin-ГЛОНАСС корпоративному клиенту
				if (isUserVehicle)
					return DeleteVehicleResult.VehicleIsUser;

				if (!entities.Log_Time.Any(lt => lt.Vehicle_ID == vehicleId))
				{
					//перемещать объект в Удаленные только если есть данные по нему, в противном случае удалять объект насовсем
					entities.DeleteVehicle(vehicleId);
					return DeleteVehicleResult.Success;
				}

				//Остался последний вариант - объект наблюдения представляет добавленный пользователем трекер
				//Удаляются все связи объекта, IMEI, номер телефона и приложение обслуживания, переместить объект в Department удаленные - для возможности поиска техподдержкой и восстановления
				var removedDepartmentId = GetRemovedDepartmentId();
				var vehicle = entities.GetVehicle(vehicleId,
					VEHICLE.DEPARTMENTIncludePath,
					VEHICLE.CONTROLLERIncludePath,
					VEHICLE.CONTROLLERIncludePath + "." + CONTROLLER.CONTROLLER_INFOIncludePath,
					VEHICLE.CONTROLLERIncludePath + "." + CONTROLLER.MLP_ControllerIncludePath);
				vehicle.Department_ID = removedDepartmentId;
				entities.ClearCollection(vehicle.CompoundRule);
				entities.ClearCollection(vehicle.VEHICLEGROUP);
				entities.ClearCollection(vehicle.SCHEDULERQUEUE);
				entities.ClearCollection(vehicle.OPERATOR_VEHICLE);
				entities.ClearCollection(vehicle.OPERATORGROUP_VEHICLE);

				if (vehicle.CONTROLLER.MLP_Controller != null)
					entities.MLP_Controller.DeleteObject(vehicle.CONTROLLER.MLP_Controller);

				var removedAttributes = new StringBuilder(255);

				if (vehicle.CONTROLLER.CONTROLLER_INFO != null && vehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID != null)
				{
					var deviceId = vehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID;
					vehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID = null;

					var deviceIdString = Encoding.ASCII.GetString(deviceId);
					removedAttributes.AppendLine("device_id = " + deviceIdString);
				}
				vehicle.CONTROLLER.PHONE = null;

				if (vehicle.CONTROLLER.PhoneContactID != null)
				{
					if (!vehicle.CONTROLLER.PhoneContactReference.IsLoaded)
						vehicle.CONTROLLER.PhoneContactReference.Load();
					var phone = vehicle.CONTROLLER.PhoneContact.Value;
					vehicle.CONTROLLER.PhoneContactID = null;
					removedAttributes.AppendLine("phone = " + phone);
				}

				if (removedAttributes.Length != 0)
				{
					var vehicleNotes = GetVehicleAttributes(vehicleId, VehicleAttributeName.Notes)
						.Select(a => a.Value)
						.FirstOrDefault();
					removedAttributes.AppendLine(vehicleNotes ?? string.Empty);
					SetVehicleAttribute(vehicleId, VehicleAttributeName.Notes, removedAttributes.ToString());
				}

				entities.SaveChangesBySession(SessionId);
				return DeleteVehicleResult.Success;
			}
		}
		public void MoveVehicle(int vehicleId, int targetDepartmentId)
		{
			using (var entities = new Entities())
			{
				var isUserVehicle = entities.IsUserVehicle(vehicleId);
				if (isUserVehicle)
					throw new ArgumentOutOfRangeException("vehicleId", vehicleId, @"Vehicle cannot be moved because it represent user position");

				var vehicle = entities.GetVehicle(vehicleId,
					VEHICLE.DEPARTMENTIncludePath,
					VEHICLE.CONTROLLERIncludePath,
					VEHICLE.CONTROLLERIncludePath + "." + CONTROLLER.MLP_ControllerIncludePath);

				// Этот объект и так уже в нужном департаменте ничего делать не нужно, выходим
				if (vehicle.Department_ID == targetDepartmentId)
					return;

				// Если пользователь не суперадмин и не партнер обоих департаментов, то у него нет прав
				if (!entities.IsSuperAdministrator(OperatorId) && (!entities.IsSalesManager(OperatorId, vehicle.Department_ID) || !entities.IsSalesManager(OperatorId, targetDepartmentId)))
					throw new SecurityException("Forbidden");

				vehicle.Department_ID = targetDepartmentId;

				entities.ClearCollection(vehicle.CompoundRule);
				entities.ClearCollection(vehicle.VEHICLEGROUP);
				entities.ClearCollection(vehicle.SCHEDULERQUEUE);
				entities.ClearCollection(vehicle.OPERATOR_VEHICLE);
				entities.ClearCollection(vehicle.OPERATORGROUP_VEHICLE);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public VehicleProfileHistory[] GetVehicleProfileHistory(int vehicleId, string name)
		{
			if(!GetOperatorSystemRights(OperatorId).Contains(SystemRight.SecurityAdministration))
				throw new SecurityException("not allowed");

			using (var entities = new HistoricalEntities())
			{
				var profileHistory = entities.H_VEHICLE_PROFILE
					.Where(p => p.VEHICLE_ID == vehicleId && p.PROPERTY_NAME == name)
					.OrderBy(p => p.ACTUAL_TIME)
					.ToList();
				return profileHistory.Select(p => p.ToDTO()).ToArray();
			}
		}
		private string GetVehicleProfileField(int vehicleId, VehicleProfile fieldName)
		{
			using (var entities = new Entities())
				return entities.GetVehicleProfileField(vehicleId, fieldName.ToString().ToLowerInvariant());
		}
		private SetVehicleFieldResult SetVehicleProfileField(int vehicleId, VehicleProfile fieldName, string fieldValue)
		{
			using (var entities = new Entities())
			{
				entities.SetVehicleProfileField(vehicleId, fieldName.ToString().ToLowerInvariant(), fieldValue);
				entities.SaveChangesBySession(SessionId);
				return SetVehicleFieldResult.Success;
			}
		}
		public List<VehicleAttribute> GetVehicleAttributes(int vehicleId, params VehicleAttributeName[] attributesNames)
		{
			return GetVehicleAttributes(vehicleId, null, true, false, attributesNames);
		}
		public List<VehicleAttributeValue> GetVehicleAttributeValues(int vehicleId, VehicleAttributeName attributeName, VehicleKind? vehicleKind)
		{
			var attributes = GetVehicleAttributes(vehicleId, vehicleKind, false, true, attributeName);
			var attribute = attributes.FirstOrDefault(a => a.Name == attributeName);
			if (attribute == null)
				return null;

			return (attribute.Values ?? new List<VehicleAttributeValue>()).ToList();
		}
		private List<VehicleAttribute> GetVehicleAttributes(int vehicleId, VehicleKind? vehicleKind, bool fillAttributes, bool fillPossibleValues, params VehicleAttributeName[] attributeNames)
		{
			using (var entities = new Entities())
			{
				var arguments = new GetVehiclesArgs
				{
					IncludeAddresses   = false,
					VehicleIDs         = new[] { vehicleId },
					IncludeLastData    = false,
					IncludeAttributes  = true,
					FilterByDepartment = FilterByDepartment,
					Culture            = CultureInfo
				};
				var dtoVehicle = ((IBasicFunctionSet)Server.Instance())
					.GetVehiclesWithPositions(OperatorId, Department, arguments)
					.FirstOrDefault(v => v.id == vehicleId);
				if (dtoVehicle == null)
					return new List<VehicleAttribute>();
				var ormVehicle = entities.GetVehicle(dtoVehicle.id);
				var kind       = vehicleKind ?? dtoVehicle.vehicleKind;
				var result     = GetVehicleAttributes(entities, dtoVehicle, ormVehicle, kind);
				if(attributeNames != null && attributeNames.Any())
					result = result.Where(a => attributeNames.Contains(a.Name)).ToList();
				if(fillAttributes)
					FillAttributeValues(result, entities, dtoVehicle, ormVehicle, kind);
				if(fillPossibleValues)
					FillPossibleValues(result, entities, dtoVehicle, ormVehicle, kind);
				return result;
			}
		}
		private List<VehicleAttribute> GetVehicleAttributes(Entities entities, Vehicle dtoVehicle, VEHICLE ormVehicle, VehicleKind vehicleKind)
		{
			// Результат
			var result    = new List<VehicleAttribute>();
			// Ресурсы
			var resources = ResourceContainers.Get(CultureInfo);
			/////////////////////////////////////////////////////////////////////////////////////////////////
			#region Закладка Object
			// Вычисляем редактируемость атрибутов на закладке
			var editableObjectAttributes = IsSalesManager || IsSuperAdministrator || dtoVehicle.rights.Contains(SystemRight.EditVehicles);

			result.Add(new VehicleAttribute { Name = VehicleAttributeName.Name,              Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.VehicleKind,       Type = AttributeType.Enum,     Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.Image,             Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerFirstName,    Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerLastName,     Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerSecondName,   Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerPhoneNumber1, Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerPhoneNumber2, Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.OwnerPhoneNumber3, Type = AttributeType.String,   Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.LicenseExpiry,     Type = AttributeType.DateTime, Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.InsuranceExpiry,   Type = AttributeType.DateTime, Editable = editableObjectAttributes });
			result.Add(new VehicleAttribute { Name = VehicleAttributeName.PermitExpiry,      Type = AttributeType.DateTime, Editable = editableObjectAttributes });
			if (dtoVehicle.IsVehicle)
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.PublicNumber,  Type = AttributeType.String,   Editable = editableObjectAttributes });

			if (!Mobilekinds.Contains(vehicleKind))
			{
				var fuelTypeUnit = ormVehicle.Fuel_Type != null
					? (UnitOfMeasure)ormVehicle.Fuel_Type.Unit_Id
					: UnitOfMeasure.LitrePer100Km;
				var fuelTankUnit = ormVehicle.Fuel_Type != null
					? (UnitOfMeasure) ormVehicle.Fuel_Type.Tank_Unit_Id
					: UnitOfMeasure.Litre;
				if (fuelTypeUnit == UnitOfMeasure.KmPerM3)
				{
					result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelSpentStandartGas, Type = AttributeType.Number, Editable = editableObjectAttributes, Unit = resources[fuelTypeUnit] });
					result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelTankGasVolume,    Type = AttributeType.Number, Editable = editableObjectAttributes, Unit = resources[fuelTankUnit] });
				}
				else
				{
					result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelSpentStandart,    Type = AttributeType.Number, Editable = editableObjectAttributes, Unit = resources[fuelTypeUnit] });
					result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelTankVolume,       Type = AttributeType.Number, Editable = editableObjectAttributes, Unit = resources[fuelTankUnit] });
				}
				var spentUnit = fuelTankUnit == UnitOfMeasure.Cbm
					? UnitOfMeasure.M3PerMotohour
					: UnitOfMeasure.LitrePerHour;
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelSpentStandartMH,      Type = AttributeType.Number, Editable = editableObjectAttributes, Unit = resources[spentUnit] });
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.MaxAllowedSpeed,          Type = AttributeType.Number, Editable = editableObjectAttributes });
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.VehicleType,              Type = AttributeType.String, Editable = editableObjectAttributes });
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.FuelType,                 Type = AttributeType.Enum,   Editable = editableObjectAttributes });
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.VIN,                      Type = AttributeType.String, Editable = editableObjectAttributes });
			}

			if(vehicleKind == VehicleKind.Bus)
				result.Add(new VehicleAttribute { Name = VehicleAttributeName.Route, Type = AttributeType.Enum, Editable = editableObjectAttributes });

			// Иконка объекта
			result.Add(new VehicleAttribute
			{
				Name     = VehicleAttributeName.Icon,
				Type     = AttributeType.Enum,
				Editable = editableObjectAttributes,
			});
			// Атрибут Notes доступен только для суперадминов и партнеров
			if (IsSuperAdministrator || IsSalesManager)
			{
				result.Add(new VehicleAttribute
				{
					Name     = VehicleAttributeName.Notes,
					Type     = AttributeType.String,
					Editable = true,
				});
			}
			// Атрибут OnlineGeocoding доступен для суперадминов, но редактировать его можно только если не включено для всех
			if (IsSuperAdministrator)
			{
				result.Add(new VehicleAttribute
				{
					Name     = VehicleAttributeName.OnlineGeocoding,
					Type     = AttributeType.Boolean,
					Editable = IsSuperAdministrator && !Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All),
				});
			}
			#endregion Закладка Object
			/////////////////////////////////////////////////////////////////////////////////////////////////
			#region Закладка Tracker
			// Запрашиваем дополнительно, т.к. поле SupportsPassword не заполняется стандартно
			dtoVehicle.controllerType = GetVehicleControllerType(dtoVehicle.id) ?? dtoVehicle.controllerType;
			// Вычисляем видимость и редактируемость атрибутов на закладке
			var viewableTrackerAttributes = IsSalesManager || IsSuperAdministrator || dtoVehicle.rights.Intersect(UserRole.TrackerViewRightsAny).Any();
			var editableTrackerAttributes = IsSalesManager || IsSuperAdministrator || dtoVehicle.rights.Intersect(UserRole.TrackerEditRightsAny).Any();
			if (viewableTrackerAttributes)
			{
				// Тип трекера
				result.Add(new VehicleAttribute         { Name = VehicleAttributeName.TrackerType, Type = AttributeType.Enum,   Editable = editableTrackerAttributes });
				// Идентификатор трекера
				if (dtoVehicle.controllerType.Name != ControllerType.Names.SoftTracker)
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.DeviceId,    Type = AttributeType.String, Editable = editableTrackerAttributes });
				// Телефон трекера
				result.Add(new VehicleAttribute         { Name = VehicleAttributeName.Phone,       Type = AttributeType.String, Editable = editableTrackerAttributes });
				// IP адрес камера
				if (dtoVehicle.controllerType.Name == ControllerType.Names.FtpWebCam)
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.IpAddress,   Type = AttributeType.String, Editable = editableTrackerAttributes });
				// Свойства используемые для настройки трекера
				if (IsSalesManager || IsSuperAdministrator || dtoVehicle.rights.Contains(SystemRight.ChangeDeviceConfigBySMS))
				{
					// Свойства APN
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.ApnName,     Type = AttributeType.String, Editable = editableTrackerAttributes });
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.ApnUser,     Type = AttributeType.String, Editable = editableTrackerAttributes });
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.ApnPassword, Type = AttributeType.String, Editable = editableTrackerAttributes });
					// Пароль трекера, выдаем только при редактировании, т.к. при просмотре оно скрыто точками
					if (editableTrackerAttributes && (dtoVehicle.controllerType?.SupportsPassword ?? false))
						result.Add(new VehicleAttribute { Name = VehicleAttributeName.Password,    Type = AttributeType.String, Editable = editableTrackerAttributes });
				}
				// Протокол указываем только для продавцов и суперадминов
				if (IsSalesManager || IsSuperAdministrator)
					result.Add(new VehicleAttribute     { Name = VehicleAttributeName.Protocol,    Type = AttributeType.String, Editable = false });
			}
			#endregion Закладка Tracker
			/////////////////////////////////////////////////////////////////////////////////////////////////
			foreach (var vehicleAttribute in result)
			{
				switch (vehicleAttribute.Name)
				{
					case VehicleAttributeName.DeviceId:
						if (dtoVehicle.controllerType != null &&
							(dtoVehicle.controllerType.DeviceIdIsImei ?? false))
						{
							vehicleAttribute.UserFriendlyName = resources["DeviceImei"];
							continue;
						}
						break;
				}
				vehicleAttribute.UserFriendlyName = resources[vehicleAttribute.Name];
			}
			return result;
		}
		private void FillAttributeValues(List<VehicleAttribute> result, Entities entities, Vehicle dtoVehicle, VEHICLE ormVehicle, VehicleKind vehicleKind)
		{
			var r = ResourceContainers.Get(CultureInfo);
			var owner = ormVehicle
				.VEHICLE_OWNER
				.Where(o => o.VEHICLE.VEHICLE_ID == dtoVehicle.id)
				.Select(o => o.OWNER)
				.FirstOrDefault();
			var vehicleId = dtoVehicle.id;
			foreach (var vehicleAttribute in result)
			{
				var value = string.Empty;
				string friendlyValue = null;
				switch (vehicleAttribute.Name)
				{
					case VehicleAttributeName.DeviceId:
						value = dtoVehicle.ctrlNum;
						if (string.IsNullOrWhiteSpace(value))
						{
							var deviceIdBytes = ormVehicle
								?.CONTROLLER
								?.CONTROLLER_INFO
								?.DEVICE_ID;
							if (0 < (deviceIdBytes?.Length ?? 0))
								value = Encoding.ASCII.GetString(deviceIdBytes);
						}
						break;
					case VehicleAttributeName.FuelSpentStandartGas:
					case VehicleAttributeName.FuelSpentStandart:
						if (ormVehicle.FuelSpendStandardKilometer.HasValue && ormVehicle.FuelSpendStandardLiter.HasValue)
						{
							var spentUnit = (ormVehicle.Fuel_Type != null ? (UnitOfMeasure?)ormVehicle.Fuel_Type.Unit_Id : null) ?? UnitOfMeasure.LitrePer100Km;
							switch (spentUnit)
							{
								case UnitOfMeasure.LitrePer100Km:
									value = ormVehicle.FuelSpendStandardLiter.Value.ToString("##0.####", CultureInfo.InvariantCulture);
									break;
								case UnitOfMeasure.KmPerLitre:
								case UnitOfMeasure.KmPerM3:
									value = ormVehicle.FuelSpendStandardKilometer.Value.ToString("##0.####", CultureInfo.InvariantCulture);
									break;
								default:
									throw new NotImplementedException("Cannot convert spentUnit to " + spentUnit);
							}
						}
						else
							value = string.Empty;
						break;
					case VehicleAttributeName.FuelSpentStandartMH:
						var fuelSpentStandartPerMh = entities.GetRuleValue(vehicleId, RuleNumber.FuelSpendStandardPerMH);
						if (fuelSpentStandartPerMh.HasValue)
						{
							var unit = (ormVehicle.Fuel_Type != null ? (UnitOfMeasure?)ormVehicle.Fuel_Type.Tank_Unit_Id : null) ?? UnitOfMeasure.Litre;
							fuelSpentStandartPerMh = MeasureUnitHelper.ConvertTo(fuelSpentStandartPerMh.Value, UnitOfMeasure.Litre, unit);
						}

						value = fuelSpentStandartPerMh.HasValue
							? fuelSpentStandartPerMh.Value.ToString("##0.####", CultureInfo.InvariantCulture)
							: string.Empty;
						break;
					case VehicleAttributeName.FuelTankGasVolume:
					case VehicleAttributeName.FuelTankVolume:
						var tankUnit = (ormVehicle.Fuel_Type != null ? (UnitOfMeasure?)ormVehicle.Fuel_Type.Tank_Unit_Id : null) ?? UnitOfMeasure.Litre;
						value = ormVehicle.FUEL_TANK != null ? MeasureUnitHelper.ConvertTo(ormVehicle.FUEL_TANK.Value, UnitOfMeasure.Litre, tankUnit).ToString("##0.####", CultureInfo.InvariantCulture) : string.Empty;
						break;
					case VehicleAttributeName.FuelType:
						if (ormVehicle.Fuel_Type != null)
						{
							value = ormVehicle.Fuel_Type.ID.ToString();
							friendlyValue = ormVehicle.Fuel_Type.Name;
						}
						break;
					case VehicleAttributeName.Image:
						var picture = ormVehicle.VEHICLE_PICTURE.FirstOrDefault();
						value = picture != null ? Convert.ToBase64String(picture.PICTURE) : string.Empty;
						break;
					case VehicleAttributeName.InsuranceExpiry:
						var insuranceExpiry = GetVehicleControlDate(vehicleId, VehicleControlDateTypes.InsuranceExpiry);
						value = insuranceExpiry != null ? insuranceExpiry.Value.ToShortDateString() : string.Empty;
						break;
					case VehicleAttributeName.IpAddress:
						value = ormVehicle.CONTROLLER.CONTROLLER_INFO.IP;
						break;
					case VehicleAttributeName.LicenseExpiry:
						var licenseExpiry = GetVehicleControlDate(vehicleId, VehicleControlDateTypes.LicenseExpiry);
						value = licenseExpiry != null ? licenseExpiry.Value.ToShortDateString() : string.Empty;
						break;
					case VehicleAttributeName.MaxAllowedSpeed:
						value = dtoVehicle.MaxAllowedSpeed.HasValue ? dtoVehicle.MaxAllowedSpeed.ToString() : string.Empty;
						break;
					case VehicleAttributeName.Name:
						value = ormVehicle.GARAGE_NUMBER;
						break;
					case VehicleAttributeName.OwnerFirstName:
						value = owner != null ? owner.FIRST_NAME : string.Empty;
						break;
					case VehicleAttributeName.OwnerLastName:
						value = owner != null ? owner.LAST_NAME : string.Empty;
						break;
					case VehicleAttributeName.OwnerPhoneNumber1:
						value = owner != null ? owner.PHONE_NUMBER1 : string.Empty;
						break;
					case VehicleAttributeName.OwnerPhoneNumber2:
						value = owner != null ? owner.PHONE_NUMBER2 : string.Empty;
						break;
					case VehicleAttributeName.OwnerPhoneNumber3:
						value = owner != null ? owner.PHONE_NUMBER3 : string.Empty;
						break;
					case VehicleAttributeName.OwnerSecondName:
						value = owner != null ? owner.SECOND_NAME : string.Empty;
						break;
					case VehicleAttributeName.Password:
						value = GetVehiclePassword(vehicleId);
						break;
					case VehicleAttributeName.PermitExpiry:
						var permitExpiry = GetVehicleControlDate(vehicleId, VehicleControlDateTypes.PermitExpiry);
						value = permitExpiry != null ? permitExpiry.Value.ToShortDateString() : string.Empty;
						break;
					case VehicleAttributeName.Phone:
						value = dtoVehicle.phone;
						if (string.IsNullOrWhiteSpace(value))
							value = ormVehicle
								?.CONTROLLER
								?.PHONE;
						if (string.IsNullOrWhiteSpace(value))
							value = ormVehicle
								?.CONTROLLER
								?.PhoneContact
								?.Value;
						break;
					case VehicleAttributeName.PublicNumber:
						value = dtoVehicle.regNum;
						break;
					case VehicleAttributeName.Route:
						var zoneGroupId = GetVehicleZoneGroupId(vehicleId);
						value = zoneGroupId.HasValue ? zoneGroupId.Value.ToString() : string.Empty;
						break;
					case VehicleAttributeName.TrackerType:
						if (dtoVehicle.controllerType != null)
						{
							value         = dtoVehicle.controllerType.Name;
							friendlyValue = dtoVehicle.controllerType.UserFriendlyName;
						}
						break;
					case VehicleAttributeName.VehicleKind:
						value = vehicleKind.ToString();
						friendlyValue = r[vehicleKind];
						break;
					case VehicleAttributeName.VehicleType:
						value = ormVehicle.VEHICLE_TYPE;
						break;
					case VehicleAttributeName.Protocol:
						value = GetVehicleProtocol(vehicleId);
						break;
					case VehicleAttributeName.ApnName:
						value = ormVehicle.CONTROLLER != null &&
								ormVehicle.CONTROLLER.CONTROLLER_INFO != null
							? ormVehicle.CONTROLLER.CONTROLLER_INFO.INET_ENTRY_POINT
							: null;
						break;
					case VehicleAttributeName.ApnUser:
						value = ormVehicle.CONTROLLER != null &&
								ormVehicle.CONTROLLER.CONTROLLER_INFO != null
							? ormVehicle.CONTROLLER.CONTROLLER_INFO.LOGIN
							: null;
						break;
					case VehicleAttributeName.ApnPassword:
						value = ormVehicle.CONTROLLER != null &&
								ormVehicle.CONTROLLER.CONTROLLER_INFO != null
							? ormVehicle.CONTROLLER.CONTROLLER_INFO.ApnPassword
							: null;
						break;
					case VehicleAttributeName.VIN:
						value = ormVehicle.VIN;
						break;
					case VehicleAttributeName.Icon:
						value = GetVehicleProfileField(vehicleId, VehicleProfile.Icon);
						if (value == "default")
							value = string.Empty;
						friendlyValue = Server.Instance().GetVehicleIcon(value, dtoVehicle.vehicleKind);
						break;
					case VehicleAttributeName.Notes:
						value = GetVehicleProfileField(vehicleId, VehicleProfile.Notes);
						break;
					case VehicleAttributeName.OnlineGeocoding:
						if (!Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All))
						{ // Если данный атрибут не разрешен для всех в конфигурации, то ищем его значение в профиле
							var valBool = default(bool);
							value = bool.TryParse(GetVehicleProfileField(vehicleId, VehicleProfile.OnlineGeocoding), out valBool)
								? valBool.ToString()
								: bool.FalseString;
						}
						else
						{ // Если данный атрибут разрешен для всех в конфигурации, делаем его недоступным для редактирования и значение True
							vehicleAttribute.Editable = false;
							value                     = bool.TrueString;
						}
						break;
				}

				friendlyValue                      = friendlyValue ?? value;
				vehicleAttribute.Value             = value         ?? string.Empty;
				vehicleAttribute.UserFriendlyValue = friendlyValue ?? string.Empty;
			}
		}
		private void FillPossibleValues(List<VehicleAttribute> result, Entities entities, Vehicle dtoVehicle, VEHICLE ormVehicle, VehicleKind vehicleKind)
		{
			var r = ResourceContainers.Get(CultureInfo);
			foreach (var vehicleAttribute in result)
			{
				if(vehicleAttribute.Type != AttributeType.Enum)
					continue;

				switch (vehicleAttribute.Name)
				{
					case VehicleAttributeName.VehicleKind:
						vehicleAttribute.Values = ((IWebPersonalServer)this)
								.GetAccessibleVehicleKinds(dtoVehicle.id)
								.Select(v => new VehicleAttributeValue { Id = v.ToString(), Name = r[v] })
								.OrderBy(v => v.Name)
								.ToList();
						break;
					case VehicleAttributeName.FuelType:
						vehicleAttribute.Values = ((IWebPersonalServer)this)
								.GetFuelTypes()
								.Select(v => new VehicleAttributeValue { Id = v.ID.ToString(), Name = v.Name })
								.OrderBy(v => v.Name)
								.ToList();
						break;
					case VehicleAttributeName.TrackerType:
						vehicleAttribute.Values = ((IWebPersonalServer)this)
								.GetControllerTypeAllowedToAssign(dtoVehicle.id)
								.Select(v => new VehicleAttributeValue { Id = v.Name, Icon = v.ImagePath, Name = v.UserFriendlyName })
								.ToList();
						break;
					case VehicleAttributeName.Route:
						vehicleAttribute.Values = ((IWebPersonalServer) this)
								.GetGeoZoneGroups()
								.Select(v => new VehicleAttributeValue { Id = v.Id.ToString(), Name = v.Name })
								.OrderBy(v => v.Name)
								.ToList();
						break;
					case VehicleAttributeName.Icon:
						vehicleAttribute.Values = Server.Instance()
							.GetIcons()
							.Values
							.Where(i => !i.Name.StartsWith("default"))
							.Select(i => new VehicleAttributeValue
							{
								Id   = i.Name,
								Icon = i.Value,
								Name = Server.Instance().GetVehicleIcon(i.Name, dtoVehicle.vehicleKind)
							})
							.ToList();
						break;
				}
			}
		}
		public Enum SetVehicleAttribute(int vehicleId, VehicleAttributeName attributeName, string value)
		{
			var vehicleAttribute = GetVehicleAttributes(vehicleId, null, false, false, attributeName)
				?.FirstOrDefault();
			if (vehicleAttribute == null)
				return SetVehicleFieldResult.IncorrectFieldName;
			if (!vehicleAttribute.Editable)
				return SetVehicleFieldResult.InsufficientRights;
			try
			{
				switch (attributeName)
				{
					case VehicleAttributeName.Name:
						return SetVehicleName(vehicleId, value);
					case VehicleAttributeName.DeviceId:
						return SetVehicleDeviceId(vehicleId, value);
					case VehicleAttributeName.Phone:
						return SetVehicleDevicePhone(vehicleId, value);
					case VehicleAttributeName.TrackerType:
						return SetVehicleDeviceType(vehicleId, value);
					case VehicleAttributeName.Password:
						return SetVehicleDevicePassword(vehicleId, value ?? string.Empty);
					case VehicleAttributeName.IpAddress:
						return SetVehicleDeviceIPAddress(vehicleId, value);
					case VehicleAttributeName.Route:
						int zoneGroupIdBuffer;
						var zoneGroupId = int.TryParse(value, out zoneGroupIdBuffer) ? zoneGroupIdBuffer : (int?)null;
						return SetVehicleZoneGroup(vehicleId, zoneGroupId);
					case VehicleAttributeName.FuelSpentStandart:
					case VehicleAttributeName.FuelSpentStandartGas:
						return SetVehicleFuelSpentStandart(vehicleId, value);
					case VehicleAttributeName.FuelSpentStandartMH:
						return SetVehicleFuelSpentStandartMh(vehicleId, value);
					case VehicleAttributeName.VehicleKind:
						VehicleKind vehicleKind;
						if (Enum.TryParse(value, out vehicleKind) &&
							Enum.IsDefined(vehicleKind.GetType(), (int) vehicleKind))
							value = ((int)vehicleKind).ToString();
						else
							return SetVehicleFieldResult.IncorrectValue;
						return SetVehicleAttributeValue<VEHICLE_KIND>(vehicleId,  "VEHICLE_KIND_ID", value);
					case VehicleAttributeName.MaxAllowedSpeed:
						return SetMaxAllowedSpeed(vehicleId, string.IsNullOrWhiteSpace(value) ? (int?)null : int.Parse(value));
					case VehicleAttributeName.PublicNumber:
						return SetVehicleAttributeValue(vehicleId, "PUBLIC_NUMBER", value);
					case VehicleAttributeName.VehicleType:
						return SetVehicleAttributeValue(vehicleId, "VEHICLE_TYPE", value);
					case VehicleAttributeName.FuelTankVolume:
					case VehicleAttributeName.FuelTankGasVolume:
						return SetFuelTank(vehicleId, value);
					case VehicleAttributeName.FuelType:
						return SetVehicleAttributeValue<Fuel_Type>(vehicleId, "ID", value);
					case VehicleAttributeName.OwnerFirstName:
						return SetVehicleOwnerValue(vehicleId, "FIRST_NAME", value);
					case VehicleAttributeName.OwnerLastName:
						return SetVehicleOwnerValue(vehicleId, "LAST_NAME", value);
					case VehicleAttributeName.OwnerSecondName:
						return SetVehicleOwnerValue(vehicleId, "SECOND_NAME", value);
					case VehicleAttributeName.OwnerPhoneNumber1:
						value = value ?? string.Empty;
						return string.IsNullOrWhiteSpace(value) || ContactHelper.IsValidPhone(value)
							? SetVehicleOwnerValue(vehicleId, "PHONE_NUMBER1", value)
							: SetVehicleFieldResult.IncorrectValue;
					case VehicleAttributeName.OwnerPhoneNumber2:
						value = value ?? string.Empty;
						return string.IsNullOrWhiteSpace(value) || ContactHelper.IsValidPhone(value)
							? SetVehicleOwnerValue(vehicleId, "PHONE_NUMBER2", value)
							: SetVehicleFieldResult.IncorrectValue;
					case VehicleAttributeName.OwnerPhoneNumber3:
						value = value ?? string.Empty;
						return string.IsNullOrWhiteSpace(value) || ContactHelper.IsValidPhone(value)
							? SetVehicleOwnerValue(vehicleId, "PHONE_NUMBER3", value)
							: SetVehicleFieldResult.IncorrectValue;
					case VehicleAttributeName.Notes:
						if (IsSuperAdministrator || IsSalesManager)
							return SetVehicleProfileField(vehicleId, VehicleProfile.Notes, value);
						else
							return SetVehicleFieldResult.InsufficientRights;
					case VehicleAttributeName.Icon:
						var iconUrlParts = value.Split('/');
						value = iconUrlParts[iconUrlParts.Length - 1];
						if (value == "default")
							value = string.Empty;
						return SetVehicleProfileField(vehicleId, VehicleProfile.Icon, value);
					case VehicleAttributeName.Image:
						return SetVehicleImage(vehicleId, value);
					case VehicleAttributeName.InsuranceExpiry:
						return SetVehicleControlDate(vehicleId, VehicleControlDateTypes.InsuranceExpiry, value);
					case VehicleAttributeName.LicenseExpiry:
						return SetVehicleControlDate(vehicleId, VehicleControlDateTypes.LicenseExpiry, value);
					case VehicleAttributeName.PermitExpiry:
						return SetVehicleControlDate(vehicleId, VehicleControlDateTypes.PermitExpiry, value);
					case VehicleAttributeName.ApnName:
					case VehicleAttributeName.ApnUser:
					case VehicleAttributeName.ApnPassword:
						return SetControllerInfoValue(vehicleId, attributeName, value);
					case VehicleAttributeName.VIN:
						return SetVehicleAttributeValue(vehicleId, "VIN", value);
					case VehicleAttributeName.OnlineGeocoding:
						var onlineGeocoding4All = Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All);
						if (!onlineGeocoding4All)
							return SetVehicleProfileField(vehicleId, VehicleProfile.OnlineGeocoding, value);
						else
							return SetVehicleFieldResult.InsufficientRights;
				}
			}
			catch (UpdateException e)
			{
				var argumentException = e.InnerException as ArgumentException;
				if (argumentException != null)
					return SetVehicleFieldResult.IncorrectValue;
				Trace.TraceError("{0}.SetVehicleFuelSpentStandart {1}, {2}", this, vehicleId, value);
				return SetVehicleFieldResult.Error;
			}

			return SetVehicleFieldResult.IncorrectFieldName;
		}
		public Dictionary<VehicleAttributeName, Enum> SetVehicleAttributes(int vehicleId, Dictionary<VehicleAttributeName, string> values)
		{
			if (values == null)
				throw new ArgumentNullException("values");

			var result = new Dictionary<VehicleAttributeName, Enum>();

			foreach (var pair in values)
			{
				result[pair.Key] = SetVehicleAttribute(vehicleId, pair.Key, pair.Value);
			}

			return result;
		}
		public List<TrackingSchedule> GetVehicleSchedules(int vehicleId)
		{
			return Server.Instance().GetVehicleSchedules(OperatorId, vehicleId);
		}
		public TrackingSchedule SetVehicleSchedule(int vehicleId, TrackingSchedule schedule)
		{
			return Server.Instance().SetVehicleSchedule(OperatorId, vehicleId, schedule, SaveContext.Session(SessionId));
		}
		public void DeleteVehicleSchedule(int scheduleId)
		{
			Server.Instance().DeleteVehicleSchedule(OperatorId, scheduleId, SaveContext.Session(SessionId));
		}
		public string GetVehicleName(int vehicleId)
		{
			return Server.Instance().GetVehicleName(OperatorId, vehicleId);
		}
		private SetVehicleFieldResult SetControllerInfoValue(int vehicleId, VehicleAttributeName name, string value)
		{
			using (var entities = new Entities())
			{
				var controllerInfo = entities.CONTROLLER_INFO.FirstOrDefault(ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);
				if (controllerInfo == null)
					return SetVehicleFieldResult.InsufficientRights;

				switch (name)
				{
					case VehicleAttributeName.ApnName:
						controllerInfo.INET_ENTRY_POINT = value;
						break;
					case VehicleAttributeName.ApnUser:
						controllerInfo.LOGIN = value;
						break;
					case VehicleAttributeName.ApnPassword:
						controllerInfo.ApnPassword = value;
						break;
					default:
						throw new ArgumentOutOfRangeException("name", name, @"Value is not supported");
				}

				entities.SaveChangesBySession(SessionId);
				return SetVehicleFieldResult.Success;
			}
		}
		public VehicleControlDate GetVehicleControlDate(int vehicleId, VehicleControlDateTypes type)
		{
			using (var entities = new Entities())
			{
				var controlDate = entities.VehicleControlDate.FirstOrDefault(d =>
							d.Type_ID == (int)type &&
							d.Vehicle_ID == vehicleId);
				if (controlDate == null)
					return null;

				return controlDate.ToDto();
			}
		}
		private string GetVehicleProtocol(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var protocol = entities.IP_Log
					.Where(i => i.Vehicle_ID == vehicleId)
					.Select(ip => ip.Protocol)
					.FirstOrDefault();
				return protocol ?? string.Empty;
			}
		}
		private SetVehicleControlDateResult SetVehicleControlDate(int vehicleId, VehicleControlDateTypes type, string value)
		{
			DateTime? date = null;
			if (!string.IsNullOrEmpty(value))
			{
				DateTime dateValue;
				if (DateTime.TryParse(value, out dateValue))
					date = dateValue;
				else
					return SetVehicleControlDateResult.InvalidDate;
			}

			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleControlDateResult.InsufficientRights;

				var controlDate = entities.VehicleControlDate.FirstOrDefault(d =>
							d.Type_ID == (int) type &&
							d.Vehicle_ID == vehicleId);
				if (date.HasValue)
				{
					if (controlDate == null)
					{
						controlDate = new EntityModel.VehicleControlDate
						{
							Vehicle_ID = vehicleId,
							Type_ID = (int) type
						};
						entities.VehicleControlDate.AddObject(controlDate);
					}

					controlDate.Value = date.Value;
				}
				else
				{
					if(controlDate != null)
						entities.DeleteObject(controlDate);
				}

				entities.SaveChangesBySession(SessionId);
				return SetVehicleControlDateResult.Success;
			}
		}
		private SetVehicleFieldResult SetVehicleFuelSpentStandart(int vehicleId, string value)
		{
			decimal? fuelSpentStandart = null;
			if (!string.IsNullOrWhiteSpace(value))
			{
				decimal fuelSpendStandartValue;
				value = value.Replace(",", ".");
				if (!decimal.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out fuelSpendStandartValue))
					return SetVehicleFieldResult.IncorrectValue;
				fuelSpentStandart = fuelSpendStandartValue;
			}

			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				if (fuelSpentStandart > 0)
				{
					var measureUnit = vehicle.Fuel_Type != null ? (UnitOfMeasure)vehicle.Fuel_Type.Unit_Id : UnitOfMeasure.LitrePer100Km;
					switch (measureUnit)
					{
						case UnitOfMeasure.KmPerM3:
							vehicle.FuelSpendStandardKilometer = fuelSpentStandart.Value;
							vehicle.FuelSpendStandardLiter = MeasureUnitHelper.ConvertTo(1, UnitOfMeasure.Cbm, UnitOfMeasure.Litre);
							break;
						case UnitOfMeasure.LitrePer100Km:
							vehicle.FuelSpendStandardKilometer = 100;
							vehicle.FuelSpendStandardLiter = fuelSpentStandart.Value;
							break;
						case UnitOfMeasure.KmPerLitre:
							vehicle.FuelSpendStandardKilometer = fuelSpentStandart.Value;
							vehicle.FuelSpendStandardLiter = 1;
							break;
						default:
							throw new NotImplementedException("Cannot convert FuelSpendStandard to " + measureUnit);
					}
				}
				else
				{
					vehicle.FuelSpendStandardKilometer = null;
					vehicle.FuelSpendStandardLiter = null;
				}

				entities.SaveChangesBySession(SessionId);
			}

			return SetVehicleFieldResult.Success;
		}
		private SetVehicleFieldResult SetFuelTank(int vehicleId, string value)
		{
			decimal? fuelTank = null;
			if (!string.IsNullOrWhiteSpace(value))
			{
				value = value.Replace(",", ".");
				decimal fuelTankValue;
				if (!decimal.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out fuelTankValue))
					return SetVehicleFieldResult.IncorrectValue;
				fuelTank = fuelTankValue;
			}


			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				if (fuelTank > 0)
				{
					var measureUnit = vehicle.Fuel_Type != null
						? (UnitOfMeasure) vehicle.Fuel_Type.Tank_Unit_Id
						: UnitOfMeasure.Litre;
					vehicle.FUEL_TANK = MeasureUnitHelper.ConvertTo(fuelTank.Value, measureUnit, UnitOfMeasure.Litre);
				}
				else
				{
					vehicle.FUEL_TANK = 0;
				}

				entities.SaveChangesBySession(SessionId);
			}

			return SetVehicleFieldResult.Success;
		}
		private SetVehicleFieldResult SetVehicleFuelSpentStandartMh(int vehicleId, string value)
		{
			var fuelSpentStandardMh = 0m;
			if (!string.IsNullOrWhiteSpace(value))
			{
				value = value.Replace(",", ".");
				decimal fuelSpentStandartMhValue;
				if (!decimal.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out fuelSpentStandartMhValue))
					return SetVehicleFieldResult.IncorrectValue;
				fuelSpentStandardMh = fuelSpentStandartMhValue;
			}

			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				var unit = vehicle.Fuel_Type != null
					? (UnitOfMeasure)vehicle.Fuel_Type.Tank_Unit_Id
					: UnitOfMeasure.Litre;
				fuelSpentStandardMh = MeasureUnitHelper.ConvertTo(fuelSpentStandardMh, unit, UnitOfMeasure.Litre);

				SetVehicleRuleValue(vehicleId, RuleNumber.FuelSpendStandardPerMH, fuelSpentStandardMh, entities);
				entities.SaveChangesBySession(SessionId);
				return SetVehicleFieldResult.Success;
			}
		}
		private SetVehicleFieldResult SetVehicleAttributeValue(int vehicleId, string field, string value)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;
				var vehicle = entities.GetVehicle(vehicleId);
				var result = SetPropertyValue(vehicle, field, value);
				if(result == SetVehicleFieldResult.Success)
					entities.SaveChangesBySession(SessionId);

				return result;
			}
		}
		private SetVehicleFieldResult SetVehicleAttributeValue(int vehicleId, string field, string value, params VehicleKind[] vehicleKinds)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				if (vehicle.VEHICLE_KIND == null || !vehicleKinds.All(k => vehicle.VEHICLE_KIND.VEHICLE_KIND_ID != (int) k))
					return SetVehicleFieldResult.IncorrectFieldName;

				var result = SetPropertyValue(vehicle, field, value);
				if (result == SetVehicleFieldResult.Success)
					entities.SaveChangesBySession(SessionId);

				return result;
			}
		}
		private SetVehicleFieldResult SetVehicleOwnerValue(int vehicleId, string field, string value)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				var owner = entities.GetVehicleOwner(vehicle);
				var result = SetPropertyValue(owner, field, value);
				if (result == SetVehicleFieldResult.Success)
					entities.SaveChangesBySession(SessionId);

				return result;
			}
		}
		private SetVehicleFieldResult SetVehicleAttributeValue<T>(int vehicleId, string field, string value)
			where T : class
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				var result = SetReferenceValue<T>(entities, vehicle, field, value);
				if(result == SetVehicleFieldResult.Success)
					entities.SaveChangesBySession(SessionId);
				return result;
			}
		}
		private SetVehicleFieldResult SetVehicleAttributeValue<T>(int vehicleId, string field, string value, params VehicleKind[] vehicleKinds) where T : class
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				if (vehicle.VEHICLE_KIND == null || !vehicleKinds.All(k => vehicle.VEHICLE_KIND.VEHICLE_KIND_ID != (int)k))
					return SetVehicleFieldResult.IncorrectFieldName;

				var result = SetReferenceValue<T>(entities, vehicle, field, value);
				if (result == SetVehicleFieldResult.Success)
					entities.SaveChangesBySession(SessionId);
				return result;
			}
		}
		private SetVehicleFieldResult SetVehicleImage(int vehicleId, string base64Img)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;
				//////////////////////////////////////////////////////////////////////
				var imgBytes = default(byte[]);
				if (!string.IsNullOrWhiteSpace(base64Img))
					imgBytes = Convert.FromBase64String(base64Img
						.Replace(" ", string.Empty).Replace("\r\n", string.Empty));
				//////////////////////////////////////////////////////////////////////
				UpdateVehicleImage(imgBytes, vehicleId, entities);
				entities.SaveChangesBySession(SessionId);
				return SetVehicleFieldResult.Success;
			}
		}
		/// <summary> Установка значения свойства объекта </summary>
		/// <param name="obj"></param>
		/// <param name="field"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		private SetVehicleFieldResult SetPropertyValue(object obj, string field, object value)
		{
			var property = obj.GetType().GetProperty(field, BindingFlags.Instance | BindingFlags.Public);
			if (property == null)
				return SetVehicleFieldResult.IncorrectFieldName;

			var propertyType = property.PropertyType;
			var isNullable = propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>);
			if (isNullable)
				propertyType = propertyType.GetGenericArguments()[0];

			var fieldValue = value;
			if (value != null && propertyType != value.GetType() && value is string)
			{
				var stringValue = (string) value;
				if (!string.IsNullOrWhiteSpace(stringValue))
				{
					var typeConverter = TypeDescriptor.GetConverter(propertyType);
					if (typeConverter.CanConvertFrom(stringValue.GetType()))
						try
						{
							fieldValue = typeConverter.ConvertFromInvariantString(stringValue);
						}
						catch (Exception)
						{
							return SetVehicleFieldResult.IncorrectValue;
						}
				}
				else if (isNullable)
					fieldValue = null;
				else if (propertyType.IsValueType)
					fieldValue = Activator.CreateInstance(propertyType);
				else
					fieldValue = null;
			}

			var propertyValue = property.GetValue(obj, null);
			if (propertyValue == null && fieldValue != null || (propertyValue != null && !propertyValue.Equals(fieldValue)))
				property.SetValue(obj, fieldValue, null);
			return SetVehicleFieldResult.Success;
		}
		private SetVehicleFieldResult SetReferenceValue<T>(Entities entities, VEHICLE vehicle, string field, string value) where T : class
		{
			var entitiesObjectSetProperty = entities.GetType()
				.GetProperties()
				.Where(p => p.PropertyType.IsGenericType)
				.FirstOrDefault(p => p.PropertyType.GetGenericArguments()[0] == typeof(T));
			if (entitiesObjectSetProperty == null)
				return SetVehicleFieldResult.IncorrectFieldName;

			object obj = null;
			int id;
			if (int.TryParse(value, out id))
			{
				var entitiesSet = (ObjectSet<T>)entitiesObjectSetProperty.GetValue(entities, null);
				var parameter = Expression.Parameter(typeof(T), "p");
				var property = Expression.Property(parameter, field);
				var constantValue = Expression.Constant(id);
				var comparison = Expression.Equal(property, constantValue);
				var lambda = Expression.Lambda<Func<T, bool>>(comparison, parameter);
				obj = entitiesSet.FirstOrDefault(lambda);
			}

			SetPropertyValue(vehicle, entitiesObjectSetProperty.Name, obj);

			return SetVehicleFieldResult.Success;
		}
		private int GetRemovedDepartmentId()
		{
			using (var entities = new Entities())
			{
				var removedDepartmentExtId = entities.GetConstant(Constant.RemovedDepartmentExtId);
				if (removedDepartmentExtId == null)
				{
					removedDepartmentExtId = Guid.NewGuid().ToString("N");
					var department = new DEPARTMENT
					{
						NAME        = "Удаленные",
						Description = "Все удаленные объекты наблюдения",
						ExtID       = removedDepartmentExtId
					};
					entities.DEPARTMENT.AddObject(department);
					var salesGroup = entities.SalesOperatorGroupID;
					if (salesGroup == null)
						throw new InvalidOperationException("entities.SalesOperatorGroupID is null");

					foreach (var right in UserRole.UfinSuperAdmin.DepartmentRights)
					{
						department.OPERATORGROUP_DEPARTMENT.Add(new OPERATORGROUP_DEPARTMENT
						{
							DEPARTMENT       = department,
							OPERATORGROUP_ID = salesGroup.Value,
							ALLOWED          = true,
							RIGHT_ID         = (int)right
						});
					}
					entities.CONSTANTS.AddObject(new CONSTANTS
					{
						NAME  = Constant.RemovedDepartmentExtId.ToString(),
						VALUE = removedDepartmentExtId
					});
					entities.SaveChangesBySession(SessionId);
					return department.DEPARTMENT_ID;
				}

				return entities.DEPARTMENT.Where(d => d.ExtID == removedDepartmentExtId).Select(d => d.DEPARTMENT_ID).First();
			}
		}
		public SetVehicleFieldResult SetVehicleZoneGroup(int vehicleId, int? zoneGroupId)
		{
			using (var entities = new Entities())
			{
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					return SetVehicleFieldResult.InsufficientRights;
				if (zoneGroupId != null && !CheckAccessToZoneGroup(entities, zoneGroupId.Value))
					return SetVehicleFieldResult.InsufficientRights;

				var vehicle = entities.GetVehicle(vehicleId);
				if (!vehicle.ZoneGroups.IsLoaded)
					vehicle.ZoneGroups.Load();
				var existingZoneGroups = vehicle.ZoneGroups.Reverse().ToArray();
				foreach (var zoneGroup in existingZoneGroups)
					entities.DeleteVehicleZoneGroup(vehicle, zoneGroup);
				if (zoneGroupId != null)
					entities.Add(vehicle, entities.GetZoneGroup(zoneGroupId.Value).FirstOrDefault());
				entities.SaveChangesBySession(SessionId);

				return SetVehicleFieldResult.Success;
			}
		}
		public int? GetVehicleZoneGroupId(int vehicleId)
		{
			using (var entities = new Entities())
			{
				if (!IsVehicleAllowed(entities, vehicleId, SystemRight.VehicleAccess))
					return null;

				var vehicle = entities.GetVehicle(vehicleId);
				if(vehicle.ZoneGroups.IsLoaded)
					vehicle.ZoneGroups.Load();

				var existingZoneGroup = vehicle.ZoneGroups.FirstOrDefault();
				if (existingZoneGroup == null)
					return null;

				if (!CheckAccessToZoneGroup(entities, existingZoneGroup.ZONEGROUP_ID))
					return null;

				return existingZoneGroup.ZONEGROUP_ID;
			}
		}
		VehicleKind[] IWebPersonalServer.GetAccessibleVehicleKinds(int vehicleId)
		{
			VehicleKind currentVehicleKind;
			using (var entities = new Entities())
			{
				var vehicleKindEntity = entities.VEHICLE
						.Where(v =>
							v.VEHICLE_ID == vehicleId &&
							v.Accesses.Any(
								ovr =>
								ovr.operator_id == OperatorId && ovr.right_id == (int)SystemRight.VehicleAccess))
						.Select(v => v.VEHICLE_KIND).FirstOrDefault();
				if (vehicleKindEntity == null)
					return new VehicleKind[0];
				currentVehicleKind = (VehicleKind)vehicleKindEntity.VEHICLE_KIND_ID;
			}

			var accessibleKinds = IsPhysicalCustomer()
				? new[] { VehicleKind.Tracker, VehicleKind.Vehicle, VehicleKind.MobilePhone, VehicleKind.Smartphone }
				: (VehicleKind[])Enum.GetValues(typeof(VehicleKind));

			//if (!accessibleKinds.Contains(currentVehicleKind))
			//	return new[] { currentVehicleKind };

			return accessibleKinds;
		}
		public bool IsAllowedVehicleAll(int vehicleId, params SystemRight[] rights)
		{
			return Server.Instance()
				.IsAllowedVehicleAll(OperatorId, vehicleId, rights);
		}
		public bool IsAllowedVehicleAny(int vehicleId, params SystemRight[] rights)
		{
			return Server.Instance()
				.IsAllowedVehicleAny(OperatorId, vehicleId, rights);
		}
	}
}