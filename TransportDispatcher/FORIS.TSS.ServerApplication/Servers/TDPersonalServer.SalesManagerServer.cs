﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using AddTrackerResultCode = FORIS.TSS.BusinessLogic.ResultCodes.AddTrackerResult;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		#region Implement ISalesManagerServer
		public LoginInfo ResetAdministratorPassword(string login)
		{
			if (ComputedDepartmentId == null)
				throw new InvalidOperationException("Department is not set");

			if (string.IsNullOrWhiteSpace(login))
				throw new ArgumentException(@"Argument cannot be null or empty or whitespace", "login");

			using (var entities = new Entities())
			{
				var ormDepartment = entities.GetDepartment(ComputedDepartmentId.Value);
				if (ormDepartment == null)
					return null; //No such department

				var administrator = entities.GetOperator(login);
				if (!entities.v_operator_department_right.Any(
					odr => odr.operator_id == administrator.OPERATOR_ID &&
						odr.Department_ID  == ComputedDepartmentId.Value &&
						odr.right_id       == (int)SystemRight.SecurityAdministration))
					throw new ArgumentOutOfRangeException("login", login,
						@"Operator is not security administrator of department id = " + ComputedDepartmentId);

				SetPassword(OPERATOR.GeneratePassword(), administrator.OPERATOR_ID);

				entities.SaveChangesBySession(SessionId);

				return new LoginInfo { Login = administrator.LOGIN, Password = administrator.PASSWORD };
			}
		}
		public AddTrackerResultCode CheckTrackerToAddByType(string trackerType, string trackerNumber)
		{
			using (var entities = new Entities())
			{
				var query = entities.CONTROLLER_TYPE.Where(ct => ct.TYPE_NAME == trackerType);
				if (!IsSuperAdministrator)
					query = query.Where(ct => ct.AllowedToAddByCustomer == true);

				var controllerType = query.FirstOrDefault();

				if (controllerType == null)
					return AddTrackerResultCode.Forbidden;

				var deviceIdIsImei = controllerType.DeviceIdIsImei;

				if (deviceIdIsImei)
				{
					if (!ImeiHelper.IsImeiCorrect(trackerNumber))
						return AddTrackerResultCode.InvalidNumber;
				}

				trackerNumber = controllerType.NormalizeDeviceId(trackerNumber);
				if (!DeviceNumberHelper.ValidTrackerNumber(trackerNumber))
					return AddTrackerResultCode.InvalidNumber;

				var deviceID = Encoding.ASCII.GetBytes(trackerNumber);

				if (entities.CONTROLLER_INFO.Any(ci => ci.DEVICE_ID == deviceID && ci.CONTROLLER.VEHICLE != null))
					return AddTrackerResultCode.DeviceIDAlreadyExists;

				var allowedTrackerTypes = GetControllerTypeAllowedToAdd();
				if (allowedTrackerTypes.All(t => t.Id != controllerType.CONTROLLER_TYPE_ID))
					return AddTrackerResultCode.Forbidden;
			}
			return AddTrackerResultCode.Success;
		}
		public AddTrackerResultCode AddTrackerByType(string trackerType, string trackerNumber, out int vehicleId)
		{
			vehicleId = -1;

			using (var entities = new Entities())
			{
				var query = entities.CONTROLLER_TYPE
					.Include("CONTROLLER_SENSOR")
					.Include("CONTROLLER_SENSOR.CONTROLLER_SENSOR_LEGEND")
					.Where(ct => ct.TYPE_NAME == trackerType);

				if (!IsSuperAdministrator)
					query = query.Where(ct => ct.AllowedToAddByCustomer == true);
				var controllerType = query.FirstOrDefault();

				if (controllerType == null)
					return AddTrackerResultCode.Forbidden;

				var allowedTrackerTypes = GetControllerTypeAllowedToAdd();
				if (allowedTrackerTypes.All(t => t.Id != controllerType.CONTROLLER_TYPE_ID))
					return AddTrackerResultCode.Forbidden;

				var department = ComputedDepartmentId != null ? entities.GetDepartment(ComputedDepartmentId.Value) : null;

				trackerNumber = controllerType.NormalizeDeviceId(trackerNumber);
				if (!DeviceNumberHelper.ValidTrackerNumber(trackerNumber))
					return AddTrackerResultCode.InvalidNumber;

				var ormController = GetOrCreateController(entities, Encoding.ASCII.GetBytes(trackerNumber));
				if (ormController == null)
					return AddTrackerResultCode.DeviceIDAlreadyExists;

				ormController.CONTROLLER_TYPE = controllerType;

				// Обязательные датчики по умолчанию
				entities.FillDefaultSensors(ormController);

				if (!controllerType.DefaultVehicleKindReference.IsLoaded)
					controllerType.DefaultVehicleKindReference.Load();

				var ormVehicle = new VEHICLE
				{
					GARAGE_NUMBER  = trackerNumber,
					PUBLIC_NUMBER  = string.Empty,
					CONTROLLER     = ormController,
					VEHICLE_KIND   = controllerType.DefaultVehicleKind ?? entities.GetVehicleKind(VehicleKind.Truck),
					//TODO: вынести в параметр
					VEHICLE_STATUS = entities.GetDefaultVehicleStatus(),
				};

				ormVehicle.SetDepartment(entities, department);
				entities.VEHICLE.AddObject(ormVehicle);

				int controllerNumber;
				if (int.TryParse(trackerNumber, out controllerNumber))
					ormVehicle.CONTROLLER.NUMBER = controllerNumber;

				entities.SaveChangesBySession(SessionId);

				var ormOperator = entities.GetOperator(OperatorId);
				entities.Allow(ormOperator, ormVehicle, UserRole.DefaultOwnRightsOnVehicle);
				entities.SaveChangesBySession(SessionId);

				vehicleId = ormVehicle.VEHICLE_ID;

				return AddTrackerResultCode.Success;
			}
		}
		public AddTrackerResultCode CheckTrackerToAddByTemplate(int templateVehicleId, string trackerNumber)
		{
			string templateTrackerTypeName;
			using (var entities = new Entities())
			{
				var patternVehicle = entities.GetVehicle(templateVehicleId);
				if (patternVehicle == null)
					throw new ArgumentOutOfRangeException(nameof(templateVehicleId), templateVehicleId,
						@"Template vehicle not found");

				templateTrackerTypeName = entities.CONTROLLER
					.Where(c => c.VEHICLE.VEHICLE_ID == templateVehicleId)
					.Select(c => c.CONTROLLER_TYPE.TYPE_NAME)
					.FirstOrDefault();
			}

			var allowedTrackerTypes = GetControllerTypeAllowedToAdd();
			if (templateTrackerTypeName == null || allowedTrackerTypes.All(c => c.Name != templateTrackerTypeName))
				return AddTrackerResultCode.Forbidden;
			return CheckTrackerToAddByType(templateTrackerTypeName, trackerNumber);
		}
		public AddTrackerResultCode AddTrackerByTemplate(int templateVehicleId, string trackerNumber, out int vehicleId)
		{
			vehicleId = -1;

			var checkResult = CheckTrackerToAddByTemplate(templateVehicleId, trackerNumber);
			if (checkResult != AddTrackerResultCode.Success)
				return checkResult;

			using (var entities = new Entities())
			{
				var ormVehicleSrc = entities.GetVehicle(templateVehicleId);

				var ormDepartment = ComputedDepartmentId != null
					? entities.GetDepartment(ComputedDepartmentId.Value)
					: null;

				var deviceId = Encoding.ASCII.GetBytes(trackerNumber);

				var ormVehicleDst = entities.Copy(ormVehicleSrc);
				ormVehicleDst.GARAGE_NUMBER = trackerNumber;
				ormVehicleDst.SetDepartment(entities, ormDepartment);
				ormVehicleDst.CONTROLLER.CONTROLLER_INFO.DEVICE_ID = deviceId;

				entities.VEHICLE.AddObject(ormVehicleDst);

				int controllerNumber;
				if (int.TryParse(trackerNumber, out controllerNumber))
					ormVehicleDst.CONTROLLER.NUMBER = controllerNumber;

				var ormControllerExisting = entities.CONTROLLER
					.Include("CONTROLLER_INFO")
					.FirstOrDefault(c => c.CONTROLLER_INFO.DEVICE_ID == deviceId);
				if (ormControllerExisting != null)
				{
					ormVehicleDst.CONTROLLER.PHONE                    = ormControllerExisting.PHONE;
					ormVehicleDst.CONTROLLER.CONTROLLER_INFO.PASSWORD = ormControllerExisting.CONTROLLER_INFO.PASSWORD;
					entities.DeleteController(ormControllerExisting);
				}

				entities.SaveChangesBySession(SessionId);

				var ormOperator = entities.GetOperator(OperatorId);
				entities.Allow(ormOperator, ormVehicleDst, UserRole.DefaultOwnRightsOnVehicle);
				entities.SaveChangesBySession(SessionId);

				vehicleId = ormVehicleDst.VEHICLE_ID;

				Server.Instance().OnVehicleAccessGranted(OperatorId, ormVehicleDst.VEHICLE_ID);

				return AddTrackerResultCode.Success;
			}
		}
		#endregion Implement ISalesManagerServer
		#region Implement IWebPersonalServer.SalesManagerServer
		/// <summary> Переменная сохраняющая значение является ли пользователь партнером департамента </summary>
		/// <remarks> При смене департамента необходимо сбрасывать иначе некорректно работает, т.к. зависит от пользователя и департамента </remarks>
		private bool? _isSalesManager = null;
		public bool IsSalesManager
		{
			get
			{
				if (!_isSalesManager.HasValue)
				{
					using (var entities = new Entities())
						_isSalesManager = entities
							.IsSalesManager(OperatorId, ComputedDepartmentId);
				}
				return _isSalesManager.Value;
			}
		}
		public List<BillingServiceType> GetServiceTypesAllowedToAdd(int vehicleId)
		{
			if (!IsSalesManager)
				return null;

			using (var entities = new Entities())
			{
				var result = entities.Billing_Service_Type
					.Where(bst =>
						bst.IsManual &&
						!bst.Billing_Service.Any(bs =>
							bs.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId))
					.Select(bst => new
					{
						Id       = bst.ID,
						Name     = bst.Name,
						Lbs      = bst.LBS,
						Sms      = bst.SMS,
						Category = bst.Service_Type_Category
					})
					.ToList()
					.ConvertAll(bst => new BillingServiceType
					{
						Id       = bst.Id,
						Name     = bst.Name,
						Lbs      = bst.Lbs,
						Sms      = bst.Sms,
						Category = (ServiceTypeCategory)Enum.Parse(typeof(ServiceTypeCategory), bst.Category)
					});
				return result;
			}
		}
		public void AddService(int vehicleId, int billingServiceTypeId)
		{
			if (!IsSalesManager)
				throw new SecurityException("Operator should be sales manager");

			AddServiceToVehicle(vehicleId, billingServiceTypeId);
		}
		public void DelService(int billingServiceId)
		{
			if (!IsSalesManager)
				throw new SecurityException("Operator should be sales manager");
			using (var entities = new Entities())
			{
				var service = entities.Billing_Service
					.FirstOrDefault(bs => bs.ID == billingServiceId &&
						bs.Type.IsManual &&
						bs.Asid.MLP_Controller.CONTROLLER.VEHICLE.Accesses.Any(a =>
						a.operator_id == OperatorId &&
						a.right_id    == (int)SystemRight.ServiceManagement));

				if (service == null)
					return;

				entities.DeleteBillingService(service);
			}
		}
		public void AddBlocking(int vehicleId)
		{
			if (!IsSalesManager)
				throw new SecurityException("Operator should be sales manager");

			using (var entities = new Entities())
			{
				if (entities.Billing_Blocking.Any(
					bb => bb.Type == (int)BlockingType.Manual &&
						  bb.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId))
					return;

				var asid = GetOrCreateAsidByVehicleId(entities, vehicleId);
				if (asid == null)
					throw new ArgumentOutOfRangeException(nameof(vehicleId), vehicleId, @"User " + OperatorId + @" is unable to retrieve asid for adding lock");

				//TODO: блокировать услуги во Вьюге

				entities.Billing_Blocking.AddObject(new Billing_Blocking
				{
					Asid         = asid,
					Type         = (int)BlockingType.Manual,
					BlockingDate = DateTime.UtcNow
				});
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DelBlocking(int vehicleId)
		{
			if (!IsSalesManager)
				throw new SecurityException("Operator should be sales manager");

			using (var entities = new Entities())
			{
				var billingBlocking = entities.Billing_Blocking
					.FirstOrDefault(bb =>
						bb.Type == (int)BlockingType.Manual &&
						bb.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId &&
						bb.Asid.MLP_Controller.CONTROLLER.VEHICLE.Accesses.Any(a =>
							a.operator_id == OperatorId &&
							a.right_id == (int)SystemRight.ServiceManagement));

				if (billingBlocking == null)
					return;

				entities.Billing_Blocking.DeleteObject(billingBlocking);
				entities.SaveChangesBySession(SessionId);
			}
		}
		#endregion Implement IWebPersonalServer.SalesManagerServer
		/// <summary>
		/// <para>Возвращает или создаёт новый экземпляр контроллера по его DEVICE_ID</para>
		/// <para>Возвращает null, если DEVICE_ID принадлежит другому контроллеру, которому уже назначен объект наблюдения</para>
		/// </summary>
		/// <remarks>Бывает так, что перед отгрузкой клиенту в блоки вставляется сим-карта, а сам блок запечатывается;
		/// после чего блок отправляется клиенту.
		/// В БД заливается информация о CONTROLLER и CONTROLLER_INFO, информация о VEHICLE появится позднее. </remarks>
		private CONTROLLER GetOrCreateController(Entities entities, byte[] deviceId)
		{
			var controllerInfo = entities.CONTROLLER_INFO
				.Include("CONTROLLER")
				.Include("CONTROLLER.VEHICLE")
				.FirstOrDefault(ci => ci.DEVICE_ID == deviceId);

			if (controllerInfo                    != null &&
				controllerInfo.CONTROLLER         != null &&
				controllerInfo.CONTROLLER.VEHICLE != null)
			{
				return null;
			}

			if (controllerInfo == null)
			{
				controllerInfo = new CONTROLLER_INFO
				{
					DEVICE_ID = deviceId,
					TIME      = DateTime.UtcNow,
				};
				entities.CONTROLLER_INFO.AddObject(controllerInfo);
			}

			if (controllerInfo.CONTROLLER == null)
			{
				controllerInfo.CONTROLLER =
					new CONTROLLER
					{
						CONTROLLER_INFO = controllerInfo,
					};
				entities.CONTROLLER.AddObject(controllerInfo.CONTROLLER);
			}

			return controllerInfo.CONTROLLER;
		}
		private Asid GetOrCreateAsidByVehicleId(Entities entities, int vehicleId)
		{
			var vehicle =
				entities.GetVehicle(
					OperatorId, vehicleId, SystemRight.ServiceManagement,
					VEHICLE.CONTROLLERIncludePath, VEHICLE.DEPARTMENTIncludePath)
					.FirstOrDefault();

			if (vehicle == null || vehicle.CONTROLLER == null)
				return null;

			var asid = entities.GetAsidByVehicleIdQuery(vehicleId).FirstOrDefault();
			if (asid == null)
			{
				asid = new Asid();
				asid.DEPARTMENT = vehicle.DEPARTMENT;
				entities.MLP_Controller.AddObject(new MLP_Controller {Asid = asid, CONTROLLER = vehicle.CONTROLLER});
			}

			if (string.IsNullOrWhiteSpace(asid.SimID))
				asid.SimID = Guid.NewGuid().ToString("N");

			return asid;
		}
		private void AddServiceToVehicle(int vehicleId, int billingServiceTypeId)
		{
			string simId;
			string serviceType;

			using (var entities = new Entities())
			{
				serviceType = entities.Billing_Service_Type
					.Where(bst => bst.ID == billingServiceTypeId &&
								  bst.IsManual)
					.Select(bst => bst.Service_Type)
					.FirstOrDefault();

				if (serviceType == null)
					throw new ArgumentOutOfRangeException("billingServiceTypeId", billingServiceTypeId,
						@"No such service");

				if (entities.Billing_Service.Any(
					bs => bs.Type.ID == billingServiceTypeId &&
						  bs.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId))
					return;

				var asid = GetOrCreateAsidByVehicleId(entities, vehicleId);
				if (asid == null)
					throw new ArgumentOutOfRangeException("vehicleId", vehicleId,
						@"User " + OperatorId + @" is unable to retrieve asid for adding service");

				entities.SaveChangesBySession(SessionId);

				simId = asid.SimID;
				if (string.IsNullOrWhiteSpace(simId))
					throw new InvalidOperationException("Asid of vehicle " + vehicleId + " does not contain simId");

				entities.CreateBillingService(asid, entities.Billing_Service_Type.First(bs => bs.Service_Type == serviceType), null);
			}
		}
	}
}