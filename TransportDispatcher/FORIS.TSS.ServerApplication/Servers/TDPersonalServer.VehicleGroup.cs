﻿using System.Collections.Generic;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using Interfaces.Web;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		List<Group> IWebPersonalServer.GetVehicleGroups(GetGroupsArguments arguments)
		{
			if(arguments == null)
				arguments = new GetGroupsArguments();

			arguments.Culture            = CultureInfo;
			arguments.FilterByDepartment = FilterByDepartment;
			return Server.Instance()
				.GetVehicleGroupsWithVehicles(
					OperatorId,
					Department,
					arguments);
		}
		public int AddVehicleGroup(string name)
		{
			if (IsGuest)
				throw new SecurityException("Group creation is forbidden");
			if (!GetDepartmentRights().Contains(SystemRight.EditGroup))
				throw new SecurityException("Group creation is forbidden");

			using (var entities = new Entities())
			{
				var operatorId = OperatorId;
				var ormOperator = entities.OPERATOR
					.Where(o => o.OPERATOR_ID == operatorId)
					.First();

				var ormVehicleGroup = new VEHICLEGROUP
				{
					NAME          = name,
					COMMENT       = string.Empty,
					Department_ID = ComputedDepartmentId,
				};

				entities.VEHICLEGROUP.AddObject(ormVehicleGroup);

				foreach (var rightEnum in UserRole.DefaultOprRightsOnVehicleGroup)
				{
					var right = entities.GetSystemRight(rightEnum);
					var ormOvg   = new OPERATOR_VEHICLEGROUP
					{
						ALLOWED      = true,
						RIGHT        = right,
						OPERATOR     = ormOperator,
						VEHICLEGROUP = ormVehicleGroup,
					};
					entities.OPERATOR_VEHICLEGROUP.AddObject(ormOvg);
				}
				entities.SaveChangesBySession(SessionId);
				return ormVehicleGroup.VEHICLEGROUP_ID;
			}
		}
		public void RemoveVehicleGroup(int groupId)
		{
			using (var entities = new Entities())
			{
				var vehicleGroup = entities.VEHICLEGROUP
					.Where(vg => vg.VEHICLEGROUP_ID == groupId)
					.FirstOrDefault();
				if (vehicleGroup == null)
					return;

				// Есть ли права на редактирование группы?
				if (!CheckEditRightOnGroup(entities, vehicleGroup.VEHICLEGROUP_ID))
					throw new SecurityException("No rights for vehiclegroup delete.");

				entities.DeleteVehicleGroup(vehicleGroup);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void AddVehicleToGroup(int vehicleId, int groupId)
		{
			using (var entities = new Entities())
			{

				// Есть ли права на редактирование группы?
				if (!CheckEditRightOnGroup(entities, groupId))
					throw new SecurityException("No edit rights to the vehiclegroup.");

				// Есть ли права на машину?
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					throw new SecurityException("No access to vehicle.");

				var alreadyExists = entities.VEHICLEGROUP
					.Where(g => g.VEHICLEGROUP_ID == groupId)
					.SelectMany(g => g.VEHICLE.Select(v => v.VEHICLE_ID))
					.Where(v => v == vehicleId);
				// Добавлена ли уже машина в данную группу?
				if (0 != alreadyExists.Count())
					return;

				var vehicleGroup = entities.VEHICLEGROUP
					.Where(vg => vg.VEHICLEGROUP_ID == groupId)
					.FirstOrDefault();
				if (vehicleGroup == null)
					return;

				var vehicle = entities.VEHICLE
					.Where(v => v.VEHICLE_ID == vehicleId)
					.FirstOrDefault();
				if (vehicle == null)
					return;

				entities.Add(vehicleGroup, vehicle);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void RemoveVehicleFromGroup(int vehicleId, int groupId)
		{
			using (var entities = new Entities())
			{
				// Есть ли права на редактирование группы?
				if (!CheckEditRightOnGroup(entities, groupId))
					throw new SecurityException("No edit rights to the vehiclegroup.");

				// Есть ли права на машину?
				if (!CheckEditRightOnVehicle(entities, vehicleId))
					throw new SecurityException("No access to vehicle.");

				var vehicleGroup = entities.VEHICLEGROUP
					.Where(vg => vg.VEHICLEGROUP_ID == groupId)
					.FirstOrDefault();
				if (vehicleGroup == null)
					return;

				var vehicle = entities.VEHICLE
					.Where(v => v.VEHICLE_ID == vehicleId)
					.FirstOrDefault();
				if (vehicle == null)
					return;

				entities.Remove(vehicleGroup, vehicle);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void UpdateVehicleGroup(Group @group)
		{
			using (var entities = new Entities())
			{
				// Есть ли права на редактирование группы?
				if (!CheckEditRightOnGroup(entities, @group.Id))
					throw new SecurityException("No edit rights to the vehiclegroup.");

				var vehicleGroup = entities.VEHICLEGROUP
					.Where(vg => vg.VEHICLEGROUP_ID == @group.Id)
					.FirstOrDefault();
				if (vehicleGroup == null)
					return;

				vehicleGroup.NAME    = @group.Name;
				vehicleGroup.COMMENT = @group.Description;
				entities.SaveChangesBySession(SessionId);
				var vehicleIdsSrc = new HashSet<int>(vehicleGroup.VEHICLE.Select(v => v.VEHICLE_ID));
				var vehicleIdsDst = new HashSet<int>(@group.ObjectIds);
				var vehicleIdsDel = vehicleIdsSrc.Except(vehicleIdsDst);
				foreach(var del in vehicleIdsDel)
					RemoveVehicleFromGroup(del, @group.Id);
				var vehicleIdsAdd = vehicleIdsDst.Except(vehicleIdsSrc);
				foreach (var add in vehicleIdsAdd)
					AddVehicleToGroup(add, @group.Id);
				entities.SaveChangesBySession(SessionId);
			}
		}
		private bool CheckEditRightOnGroup(Entities entities, int groupId)
		{
			if (IsGuest)
				return false;

			// 1. Есть ли права на редактирование группы
			if ((from ovgr in entities.v_operator_vehicle_groups_right
				 where ovgr.operator_id     == OperatorId &&
					   ovgr.vehiclegroup_id == groupId &&
					   ovgr.right_id == (int)SystemRight.EditGroup
				 select ovgr).Any())
			{
				return true;
			}

			// 2. Есть ли административные права хотя бы на один объект из группы
			if (entities.VEHICLEGROUP.Any(
				vg => vg.VEHICLEGROUP_ID == groupId &&
					  vg.VEHICLE.Any(
						  v =>
						  v.Accesses.Any(
							  ovr =>
							  ovr.operator_id == OperatorId && ovr.right_id == (int)SystemRight.SecurityAdministration))))
			{
				return true;
			}

			return false;
		}
		private bool CheckEditRightOnVehicle(Entities entities, int vehicleId)
		{
			if (!IsVehicleAllowed(entities, vehicleId, SystemRight.VehicleAccess))
				return false;

			return true;
		}
	}
}