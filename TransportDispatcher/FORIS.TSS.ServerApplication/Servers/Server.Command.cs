﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		/// <summary> Регистрирует команду на выполнение и назначает ей ID </summary>
		/// <param name="sender"> Инициатор команды </param>
		/// <param name="command"> Команда </param>
		/// <param name="cultureInfo"> Культура, с которой должна обрабатываться команда </param>
		/// <returns> Возвращает команду с назначенным ID (если назначен) и результатом </returns>
		public Command RegisterCommand(IOperatorInfo sender, Command command, CultureInfo cultureInfo = null)
		{
			var operatorId = sender.OperatorId;

			command.ResultDate = DateTime.UtcNow;

			using (var entities = new Entities())
			{
				// Проверка доступности команды для оператора на объекте наблюдения
				var isAllowed = IsCommandAllowed(operatorId, command.TargetID, command.Type, entities);
				if (!isAllowed)
				{
					command.SetResult(CmdResult.VehicleAccessDisallowed);
					return command;
				}

				if (command.Type == CmdType.AskPosition)
				{
					var args = new GetVehiclesArgs
					{
						Culture            = cultureInfo,
						FilterByDepartment = false,
						VehicleIDs         = new[] { command.TargetID }
					};
					var vehicle = GetVehiclesWithPositions(operatorId, null, args)
							.FirstOrDefault(v => v.id == command.TargetID);
					if (vehicle == null)
						throw new VehicleNotFoundException(string.Format("Vehicle not found by id {0} for operator {1}",
							command.TargetID, operatorId));

					var deviceCapability = GetRequiredCapability(command.Type);

					if (deviceCapability != null)
					{
						DeviceCapabilityOption deviceCapabilityOption;
						if (!vehicle.capabilities.TryGetValue(deviceCapability.Value, out deviceCapabilityOption))
						{
							command.SetResult(CmdResult.NotSupported);
							return command;
						}

						if (!deviceCapabilityOption.Allowed)
						{
							command.SetResult(CmdResult.NotSupported);
							command.ResultText = deviceCapabilityOption.DisallowingReasonText;
							return command;
						}
					}
				}
				else
				{
					var commandType = GetCommandTypes(sender, command.TargetID, false)
						.FirstOrDefault(ct => ct.Id == command.Type);
					if (commandType == null)
					{
						command.SetResult(CmdResult.NotSupported);
						return command;
					}
				}

				// Eсли есть невыполненные активные команды того же типа, нужно вернуть эту же команду в качестве результата
				EntityModel.Command alreadyPendingCommandOfTheSameType;

				using (new Stopwatcher("RegisterCommand.alreadyPendingCommandOfTheSameType"))
					alreadyPendingCommandOfTheSameType = entities.Command
						.Include(EntityModel.Command.Command_ParameterIncludePath)
						.Where(c =>
							c.Type_ID == (int) command.Type &&
							c.Target_ID == command.TargetID &&
							c.Status != (int) CmdStatus.Completed)
						.OrderByDescending(c => c.ID)
						.FirstOrDefault();

				if (alreadyPendingCommandOfTheSameType != null)
				{
					Command_Parameter scheduledRequest;
					using (new Stopwatcher("RegisterCommand.scheduledRequest"))
						scheduledRequest = alreadyPendingCommandOfTheSameType.Command_Parameter
							.FirstOrDefault(cp => cp.Key == CommandParameter.ScheduledRequest);
					if (scheduledRequest != null && !command.HasParameter(CommandParameter.ScheduledRequest))
					{
						entities.Command_Parameter.DeleteObject(scheduledRequest);
						entities.SaveChanges();
					}

					return alreadyPendingCommandOfTheSameType.ToDto();
				}

				var blockingCommandType = GetBlockingCommand(command.Type);
				if (blockingCommandType != null)
				{
					var blockingCmdTypeInt = (int) blockingCommandType.Value;
					if (entities.Command.Any(
						c => c.Type_ID == blockingCmdTypeInt
							 && c.Target_ID == command.TargetID
							 && c.Status != (int) CmdStatus.Completed
						))
					{
						command.SetResult(CmdResult.OtherCommandIsPending);
						return command;
					}
				}

				if (command.Type == CmdType.AskPosition)
				{
					bool allowLbsCommand;
					using (new Stopwatcher("RegisterCommand.PreprocessLbsCommand"))
						allowLbsCommand = PreprocessLbsCommand(entities, command, operatorId);
					if (!allowLbsCommand)
						return command;
				}

				if (command.Type == CmdType.Callback)
				{
					// Определяем, на какой номер звонить
					var phone = entities.OPERATOR
						.Where(o => o.OPERATOR_ID == operatorId)
						.Select(o => o.Asid.Contact.DemaskedPhone.Value)
						.FirstOrDefault(); // Номер оператора по контракту
					if (phone == null)
					{
						phone = entities.Operator_Contact
							.Where(oc =>
								oc.Operator_ID == operatorId &&
								oc.Contact.Type == (int) ContactType.Phone &&
								oc.Confirmed)
							.Select(oc => oc.Contact.Value)
							.OrderBy(value => value)
							.FirstOrDefault(); // Подтвержденный номер оператора
					}
					if (phone == null)
					{
						command.SetResult(CmdResult.PhoneNumberIsUnknown);
						return command;
					}
					// Для Callback заполняем номер телефона оператора выполняющего команду для звонка
					command.SetParameter(PARAMS.Keys.Phone, phone);
				}

				if (command.Type == CmdType.Setup && !command.HasParameter(PARAMS.Keys.InternetApn.Name))
				{
					var controllerInfo = entities.CONTROLLER_INFO.First(ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == command.TargetID);
					command.SetParameter(PARAMS.Keys.InternetApn.Name, controllerInfo.INET_ENTRY_POINT);
					command.SetParameter(PARAMS.Keys.InternetApn.User, controllerInfo.LOGIN);
					command.SetParameter(PARAMS.Keys.InternetApn.Password, controllerInfo.ApnPassword);
				}

				var createdCommand = entities.CreateNewCommand(
					entities.OPERATOR.First(o => o.OPERATOR_ID == operatorId),
					entities.VEHICLE.First(v => v.VEHICLE_ID == command.TargetID),
					(int) command.Type,
					command.Parameters
					);

				if (command.Result == null)
					command.Result = CmdResult.Received;

				createdCommand.Result_Type_ID = (int) command.Result.Value;
				createdCommand.Status = (int) command.Status;

				using (new Stopwatcher("RegisterCommand.SaveChanges"))
					entities.SaveChanges();

				command.Id = createdCommand.ID;
				command.Created = createdCommand.Date_Received;
			}

			return command;
		}
		private static readonly TimingCache<CmdType, SystemRight?> _requiredRightForCommandTypesCache =
			new TimingCache<CmdType, SystemRight?>(TimeSpan.FromSeconds(60), cmdType =>
			{
				using (var entities = new Entities())
				{
					return (SystemRight?)entities
						?.CommandTypes
						?.FirstOrDefault(t => t.id == (int)cmdType)
						?.Right_ID;
				}
			});
		/// <summary> Право необходимое для выполнения команды типа (матрица в коде тип команды, право) </summary>
		/// <param name="cmdType"> Тип команды</param>
		/// <returns> Право или null </returns>
		private static SystemRight? GetRequiredRightForCommandType(CmdType cmdType)
		{
			return _requiredRightForCommandTypesCache[cmdType];
		}
		/// <summary> Возможность устройства необходимая для выполнения команды типа (матрица в коде тип команды, возможность устройства) </summary>
		/// <param name="cmdType"> Тип команды </param>
		/// <returns> Возможность или null </returns>
		private static DeviceCapability? GetRequiredCapability(CmdType cmdType)
		{
			switch (cmdType)
			{
				case CmdType.CapturePicture:
					return DeviceCapability.CapturePicture;
				case CmdType.AskPosition:
					return DeviceCapability.AllowAskPosition;
				case CmdType.CancelAlarm:
				case CmdType.ChangeMode:
				case CmdType.ReloadDevice:
				case CmdType.VibrateRequest:
				case CmdType.ShutdownDevice:
					return DeviceCapability.ChangeDeviceSettings;
				case CmdType.Callback:
					return DeviceCapability.PhoneCallBack;
				default:
					return null;
			}
		}
		private bool PreprocessLbsCommand(Entities entities, Command command, int operatorID)
		{
			CmdResult? prerequisitesCmdResult;
			var threshold = TimeSpan.FromMilliseconds(5);
			using (new Stopwatcher("PreprocessLbsCommand:CheckCommandPrerequisites"))
				prerequisitesCmdResult = CheckCommandPrerequisites(
					entities, new BillingServer(entities), command.Type, operatorID, command.TargetID);
			if (prerequisitesCmdResult.HasValue)
			{
				command.SetResult(prerequisitesCmdResult.Value);
			}

			using (new Stopwatcher("PreprocessLbsCommand:lastCommandResult"))
			{
				var lastCommandResult =
					entities.Command
						.Where(c => c.Sender_ID == operatorID &&
									c.Target_ID == command.TargetID &&
									c.Type_ID == (int) command.Type)
						.OrderByDescending(c => c.ID)
						.Select(c => new
						{
							ReceivedDate = c.Date_Received,
							c.Result_Type_ID,
							c.Type.MinIntervalSeconds
						})
						.Take(1)
						.FirstOrDefault();

				if (lastCommandResult != null)
				{
					if (command.Status == CmdStatus.Received)
					{
						var prevCommandDate = lastCommandResult.ReceivedDate;
						if ((DateTime.UtcNow - prevCommandDate).TotalSeconds < lastCommandResult.MinIntervalSeconds)
						{
							//Команда не должна создаваться чаще, чем указанный интервал
							command.SetResult(CmdResult.Throttled);
						}
					}
					else
					{
						var prevResult = (CmdResult) lastCommandResult.Result_Type_ID;
						var timeSincePrevResult = DateTime.UtcNow - lastCommandResult.ReceivedDate;
						if (prevResult == command.Result &&
							timeSincePrevResult.TotalSeconds < lastCommandResult.MinIntervalSeconds)
						{
							return false;
						}
					}
				}
			}
			return true;
		}
		private static CmdType? GetBlockingCommand(CmdType cmdType)
		{
			switch (cmdType)
			{
				case CmdType.CutOffElectricity:
					return CmdType.ReopenElectricity;
				case CmdType.ReopenElectricity:
					return CmdType.CutOffElectricity;
				case CmdType.CutOffFuel:
					return CmdType.ReopenFuel;
				default:
					return null;
			}
		}
		/// <summary> Проверяет, выполнены ли необходимые условия для выполнения команды. </summary>
		/// <returns> Возвращает null, если нет причин, по которым команду нельзя выполнить. </returns>
		private static CmdResult? CheckCommandPrerequisites(
			Entities entities, BillingServer billingServer, CmdType cmdType, int operatorId, int vehicleId)
		{
			var positionRequest = cmdType == CmdType.AskPosition;

			if (positionRequest)
			{
				using (new Stopwatcher("CheckCommandPrerequisites:asid", threshold: TimeSpan.FromMilliseconds(1)))
				{
					var controllerSupportsAskPosition =
						entities.CONTROLLER.Any(
							c =>
								c.VEHICLE.VEHICLE_ID == vehicleId &&
								(c.PhoneContactID != null &&
								 c.CONTROLLER_TYPE.CommandTypes.Any(t => t.id == (int) CmdType.AskPosition) ||
								 c.CONTROLLER_TYPE.TYPE_NAME == ControllerType.Names.SoftTracker));

					if (!controllerSupportsAskPosition)
						return CmdResult.Inaccessible;
				}

				BillingResult billingResult;
				using (
					new Stopwatcher("CheckCommandPrerequisites:AllowRenderService",
						threshold: TimeSpan.FromMilliseconds(1)))
					billingResult = billingServer.AllowRenderService(
						operatorId, vehicleId, PaidService.AskPositionOverLBS);

				switch (billingResult)
				{
					case BillingResult.Success:
						break;
					case BillingResult.LimitPerTimeSpanReached:
						return CmdResult.MLPRequestCountPerTimespanExceeded;
					case BillingResult.InsufficientFunds:
						return CmdResult.InsufficientFunds;
					case BillingResult.NoCallerService:
						return CmdResult.NoCallerLBSService;
					case BillingResult.NoTargetService:
						return CmdResult.NoTargetLBSService;
					case BillingResult.CallerServiceIsBlocked:
						return CmdResult.CallerLBSServiceIsBlocked;
					case BillingResult.TargetServiceIsBlocked:
						return CmdResult.TargetLBSServiceIsBlocked;
					case BillingResult.NoService:
						return CmdResult.NoTarifficationService;
					case BillingResult.ServiceIsBlocked:
						return CmdResult.ServiceIsBlocked;
					default:
						throw new NotSupportedException(
							string.Format("Unknown billing result {0} when calling AllowRenderService({1}, {2}, {3})",
								billingResult, operatorId, vehicleId, PaidService.AskPositionOverLBS));
				}
			}

			return null;
		}
		/// <summary> Отменяет команду </summary>
		/// <param name="operatorId"> Идентификатор пользователя, от имени которого отменяется команда </param>
		/// <param name="id"> Идентификатор команды </param>
		/// <returns> Возвращает true, если команда отменена и false, если нет </returns>
		public bool CancelCommand(int operatorId, int id)
		{
			using (var entities = new Entities())
			{
				var command =
					entities.Command
						.Include(EntityModel.Command.TargetIncludePath)
						.FirstOrDefault(c => c.ID == id);

				if (command == null)
					return true; //Нет такой команды, значит команда остановлена

				var cmdType = (CmdType) command.Type_ID;

				var requiredRight = GetRequiredRightForCommandType(cmdType);
				if (requiredRight == null)
					return false; //Отмена не предусмотрена

				var requiredRightId = (int) requiredRight.Value;
				if (
					!entities.v_operator_vehicle_right.Any(
						ovr =>
							ovr.operator_id == operatorId &&
							ovr.vehicle_id == command.Target_ID &&
							ovr.right_id == requiredRightId))
				{
					return false; //Нет прав
				}

				var status = (CmdStatus) command.Status;
				if (status != CmdStatus.Received &&
					status != CmdStatus.Processing)
					return false; //Команда уже в процессе выполнения

				//TODO: отменять выполнение команды через службу терминалов
				command.Result_Type_ID = (int)CmdResult.Cancelled;
				command.Date_Received  = DateTime.UtcNow;
				command.Status         = (int)CmdStatus.Completed;

				entities.SaveChanges();

				return true;
			}
		}
		private static readonly TimingCache<int, List<CmdType>> _commandTypesForVehicleCache =
			new TimingCache<int, List<CmdType>>(TimeSpan.FromSeconds(10), vehicleId =>
			{
				using (var entities = new Entities())
				{
					return entities.CONTROLLER
						.Where(c => c.VEHICLE.VEHICLE_ID == vehicleId)
						.SelectMany(c => c.CONTROLLER_TYPE.CommandTypes)
						.Select(ct => (CmdType)ct.id)
						.ToList();
				}

			});
		public List<CommandType> GetCommandTypes(IOperatorInfo sender, int vehicleId, bool includeScenario)
		{
			var result = new List<CommandType>();

			using (var entities = new Entities())
			{
				// Используем небольшой 10 секундный кэш для снятия нагрузки,
				// тем более, что изменение типа трекера происходит не часто.
				var cmdTypes = _commandTypesForVehicleCache[vehicleId];

				// TODO: вместо опрашивания типов трекеров, узнавать у TerminalManager'а о том, что доступно, а что нет
				// Пока не понятно, как это сделать, т.к. TerminalManager тоже обращается к БД за идентификацией типа трекера
				// TODO: также для команд группы "Иммобилизация" проверять настройки подключений управляющих выходов

				if (cmdTypes.Count == 0)
					return result;

				var rights = new HashSet<SystemRight>(entities
					.v_operator_vehicle_right.Where(ovr =>
						ovr.operator_id == sender.OperatorId &&
						ovr.vehicle_id  == vehicleId)
					.Select(ovr => (SystemRight)ovr.right_id));

				result.AddRange(cmdTypes
					.Where(cmdType =>
					{
						var right = GetRequiredRightForCommandType(cmdType);
						return right == null || rights.Contains(right.Value);
					})
					.Select(cmdType => new CommandType {Id = cmdType}));

				var setup = result.FirstOrDefault(x => x.Id == CmdType.Setup);
				if (setup != null && !entities.IsSuperAdministrator(sender.OperatorId))
				{
					// Команду "Настроить трекер" не нужно показывать, если есть актуальные данные по трекеру за последние сутки
					var lastGpsLogTime =
						entities.GPS_Log.Where(x => x.Vehicle_ID == vehicleId)
							.OrderByDescending(x => x.Log_Time)
							.Select(x => x.Log_Time)
							.FirstOrDefault();

					if (TimeHelper.GetSecondsFromBase() < lastGpsLogTime + 24*60*60)
						result.Remove(setup);
				}

				if (includeScenario)
				{
					try
					{
						foreach (var commandType in result)
							commandType.Scenario = GetCommandScenario(sender, vehicleId, commandType.Id);
					}
					catch (Exception ex)
					{
						default(string)
							.WithException(ex, true)
							.CallTraceError();
					}
				}

				return result;
			}
		}
		public CommandScenario GetCommandScenario(IOperatorInfo sender, int vehicleId, CmdType type)
		{
			if (!IsCommandAllowed(sender.OperatorId, vehicleId, type))
				return new CommandScenario { Result = CmdResult.VehicleAccessDisallowed };

			var command = CommandCreator.CreateCommand(type);
			if (command == null)
				throw new InvalidOperationException("Unable to create command by type: " + type);

			command.Target.Unique = vehicleId;
			command.Sender        = sender;

			using (var entities = new Entities())
			{
				// Получаем номер сервера по умолчанию
				var replyToPhone = entities.GetConstant(Constant.ServiceInternationalPhoneNumber);
				if (!entities.IsCorporateCustomer(sender.OperatorId))
				{
					// Для физика получаем информацию о логине и пробуем привести к номеру телефона
					var operatorInfo = entities.GetOperatorInfo(sender.OperatorId);
					if (null != operatorInfo && 0 < operatorInfo.OperatorId)
					{
						var loginToPhone = ContactHelper.GetNormalizedPhone(operatorInfo.Login);
						if (ContactHelper.IsValidPhone(loginToPhone))
							// Номер телефона из логина
							replyToPhone = loginToPhone;
						else
						{
							// Подтвержденный номер телефона оператора
							var confirmedPhone = GetOperatorPhone(sender.OperatorId);
							if (!string.IsNullOrWhiteSpace(confirmedPhone))
								replyToPhone = confirmedPhone;
						}
					}
				}
				if (string.IsNullOrWhiteSpace(replyToPhone))
					return new CommandScenario { Result = CmdResult.OperatorPhoneIsNotSpecified };
				command.SetParamValue(PARAMS.Keys.ReplyToPhone, replyToPhone);

				// Дополнительные параметры для команды Setup
				if (type == CmdType.Setup)
				{
					var apnParameters = entities.CONTROLLER_INFO
						.Where(ci => ci.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId)
						.Select(ci => new { ApnName = ci.INET_ENTRY_POINT, ApnUser = ci.LOGIN, ci.ApnPassword })
						.FirstOrDefault();

					if (apnParameters != null && !string.IsNullOrWhiteSpace(apnParameters.ApnName))
					{
						command.SetParamValue(PARAMS.Keys.InternetApn.Name, apnParameters.ApnName);
						command.SetParamValue(PARAMS.Keys.InternetApn.User, apnParameters.ApnUser);
						command.SetParamValue(PARAMS.Keys.InternetApn.Password, apnParameters.ApnPassword);
					}
				}
			}

			var terminalManager = TerminalManager;
			if (terminalManager == null)
				return new CommandScenario { Result = CmdResult.TerminalManagerIsNull };

			return terminalManager.GetScenario(command);
		}
		/// <summary> Проверка возможности выполнения команды указанного типа на объекте наблюдения оператором </summary>
		/// <param name="operatorId"> Оператор </param>
		/// <param name="vehicleId"> Объект наблюдения </param>
		/// <param name="type"> Тип команды </param>
		/// <param name="entities"> Модель данных </param>
		/// <returns>Да/Нет</returns>
		private bool IsCommandAllowed(int operatorId, int vehicleId, CmdType type, Entities entities = null)
		{
			Entities usedEntities = entities ?? new Entities();
			try
			{
				// Получить право на выполнение указанной команды
				var requiredRight = GetRequiredRightForCommandType(type);
				// Для выполнения команды на объекте наблюдения, у пользователя помимо права на команду, должно быть право на объект наблюдения
				var rights = new List<int> { (int)SystemRight.VehicleAccess };
				// Добавляем к необходимому VehicleAccess, право необходимое на команду (если оно есть и не равно VehicleAccess)
				if (requiredRight != null && requiredRight != SystemRight.VehicleAccess)
					rights.Add((int)requiredRight.Value);
				// Получить права оператора на объект наблюдения
				var operatorVehicleRights =
				(
					from r in usedEntities.v_operator_vehicle_right
					where
						r.operator_id == operatorId &&
						r.vehicle_id  == vehicleId
					select r.right_id
				)
					.ToList();

				var isAllowed =
					rights.All(operatorVehicleRights.Contains) &&
					operatorVehicleRights.All(r => r != (int)SystemRight.Ignore);

				//или нет прав на управление автомобилем
				return isAllowed;
			}
			finally
			{
				if (entities == null)
					usedEntities.Dispose();
			}
		}
	}
}