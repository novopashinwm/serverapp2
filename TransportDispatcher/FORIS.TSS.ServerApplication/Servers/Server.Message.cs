﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Exceptions;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;
using Attachment = FORIS.TSS.EntityModel.Attachment;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{

		public List<BusinessLogic.DTO.Message> SendQuestionToSupport(
			ISessionInfo sessionInfo, string clientName, string trackerNumber, string clientContacts, string question)
		{
			var @operator = sessionInfo != null ? sessionInfo.OperatorInfo : null;

			var subject = new StringBuilder();

			subject.Append("Вопрос в тех поддержку от");

			if (@operator != null)
			{
				subject.Append(" ")
					.Append(@operator.Name);

				if (@operator.Name != @operator.Login)
				{
					subject
						.Append(" (")
						.Append(@operator.Login)
						.Append(")");
				}
			}

			if (!string.IsNullOrWhiteSpace(clientName))
			{
				subject.Append(" ")
					.Append(clientName);
			}
			var body = new XElement("div");

			IEnumerable<string> emailContacts = null;
			if (!string.IsNullOrWhiteSpace(clientContacts))
			{
				body.Add(new XElement("p", "Контактная информация, указанная пользователем: ", clientContacts));
				emailContacts = ContactHelper.ParseEmails(clientContacts);
			}

			if (@operator != null)
			{
				using (var entities = new Entities())
				{
					var confirmedContacts =
						entities.GetOperatorEmails(@operator.OperatorId)
							.Where(e => e.IsConfirmed ?? false)
							.Select(e => e.Value)
							.Union(
								entities.GetOperatorPhones(@operator.OperatorId)
									.Where(e => e.IsConfirmed ?? false)
									.Select(p => p.Value))
							.Join(", ");

					if (!string.IsNullOrWhiteSpace(confirmedContacts))
						body.Add(new XElement("p", "Подтверждённая контактная информация: ", confirmedContacts));
				}
			}

			if (!string.IsNullOrWhiteSpace(trackerNumber))
				body.Add(new XElement("p", "Номер трекера: ", trackerNumber));

			body.Add(new XElement("p", "Вопрос: ", question));

			return SendMessageToSupport(sessionInfo, MessageTemplate.QuestionToSupport, subject.ToString(), body.ToString(), null, emailContacts);
		}

		public List<BusinessLogic.DTO.Message> SendMessageToSupport(
			ISessionInfo sessionInfo,
			MessageTemplate template, string subject, string body, Func<Entities, string, object, CultureInfo, string> getFieldValue = null, IEnumerable<string> emailContacts = null)
		{
			using (var entities = new Entities())
			{
				var emails = entities.GetTechnicalSupportEmails();
				if (emails == null || emails.Count == 0)
					return null;

				var operatorId = sessionInfo != null && sessionInfo.OperatorInfo != null ? sessionInfo.OperatorInfo.OperatorId : (int?)null;

				var operatorEmailIds = new HashSet<int>();
				if (operatorId.HasValue)
				{
					operatorEmailIds.AddRange(
						entities.Operator_Contact
							.Where(c => c.OPERATOR.OPERATOR_ID == operatorId.Value)
							.Where(c => c.Contact.Type == (int)ContactType.Email)
							.Where(c => c.Confirmed && c.Contact.Valid)
							.Select(c => c.Contact_ID));
				}

				if (emailContacts != null)
				{
					foreach (var emailContact in emailContacts.Where(ContactHelper.IsValidEmail))
						operatorEmailIds.Add(entities.GetContactId(ContactType.Email, emailContact));
				}

				var messages = entities.CreateOutgoingMessages(template, emails, subject, body, null, getFieldValue);
				foreach (var message in messages)
				{
					if (operatorId.HasValue)
						message.Owner_Operator_ID = operatorId.Value;

					if (operatorEmailIds.Any())
						foreach (var operatorEmailId in operatorEmailIds)
						{
							message.Contacts.Add(new Message_Contact
							{
								Contact_ID = operatorEmailId,
								Type = (int)MessageContactType.ReplyTo
							});
						}
				}

				if (sessionInfo != null)
					entities.SaveChangesBySession(sessionInfo.SessionId);
				else
					entities.SaveChangesByOperator(null);


				return messages.ConvertAll(m => m.ToDto());
			}
		}

		public void SendCalculationRequest(ISessionInfo sessionInfo, CalculationRequest calculationRequest)
		{
			var subject = "Запрос на расчёт стоимости от компании " + calculationRequest.CompanyName;
			var body = GetRegistrationEmailBody(calculationRequest);
			using (var entities = new Entities())
			{
				var emails = entities.GetCalculatorEmails();
				if (emails == null || emails.Count == 0)
					return;

				Attachment attachment;
				if (calculationRequest.Attachment != null)
				{
					attachment = new Attachment
					{
						Name = calculationRequest.Attachment.Name,
						Data = calculationRequest.Attachment.Bytes,
					};
					entities.Attachment.AddObject(attachment);
				}
				else
				{
					attachment = null;
				}

				var messages = entities.CreateOutgoingMessages(MessageTemplate.CalculationRequest, emails, subject, body, null);
				foreach (var message in messages)
				{
					if (sessionInfo != null && sessionInfo.OperatorInfo != null)
					{
						message.SourceType_ID = (int)ObjectType.Operator;
						message.Owner_Operator_ID = sessionInfo.OperatorInfo.OperatorId;
					}

					entities.AddMessageField(message, "VehicleQuantity", calculationRequest.VehicleQuantity);
					entities.AddMessageField(message, "EmployeeQuantity", calculationRequest.EmployeeQuantity);
					entities.AddMessageField(message, "Vehicles", calculationRequest.Vehicles);
					entities.AddMessageField(message, "InstallByMTSPartner", calculationRequest.InstallByMTSPartner);
					entities.AddMessageField(message, "Glonass", calculationRequest.Glonass);
					entities.AddMessageField(message, "FuelLevelSensor", calculationRequest.FuelLevelSensor);
					entities.AddMessageField(message, "IsAlreadyMTSCustomer", calculationRequest.IsAlreadyMTSCustomer);
					entities.AddMessageField(message, "City", calculationRequest.City);
					entities.AddMessageField(message, "CompanyName", calculationRequest.CompanyName);
					entities.AddMessageField(message, "UserName", calculationRequest.UserName);
					entities.AddMessageField(message, "Email", calculationRequest.Email);
					entities.AddMessageField(message, "Phone", calculationRequest.Phone);
					entities.AddMessageField(message, "AdditionalInformation", calculationRequest.AdditionalInformation);

					if (calculationRequest.Attachment != null)
						entities.Add(message, attachment);
				}

				if (sessionInfo != null)
					entities.SaveChangesBySession(sessionInfo.SessionId);
				else
					entities.SaveChangesByOperator(null);
			}
		}

		public void SetMessageProcessingResult(MessageProcessingResult value, params int[] messageIds)
		{
			DbManager.SetMessageProcessingResult(null, value, messageIds);
		}

		/// <summary> Обновляет статусы сообщений </summary>
		/// <param name="messageIds">идентификаторы сообщений</param>
		/// <param name="appId">Идентификатор установленного приложения, на который пришли идентификаторы сообщений</param>
		/// <param name="status">устанавливаемый статус</param>
		public List<int> SetMessageProcessingResult(string appId, MessageProcessingResult status, params int[] messageIds)
		{
			return DbManager.SetMessageProcessingResult(appId, status, messageIds);
		}

		private static string GetRegistrationEmailBody(CalculationRequest calculationRequest)
		{
			var body = new StringBuilder(1000);
			using (XmlWriter writer = XmlWriter.Create(body, new XmlWriterSettings { OmitXmlDeclaration = true, ConformanceLevel = ConformanceLevel.Fragment }))
			{
				writer.WriteStartElement("div");

				WriteHeader(writer, "Данные для расчёта стоимости");

				writer.WriteStartElement("br");
				writer.WriteEndElement();

				WriteRow(writer, "Количество автомобилей", calculationRequest.VehicleQuantity);
				WriteRow(writer, "Количество сотрудников для контроля", calculationRequest.EmployeeQuantity);
				WriteRow(writer, "Списочный состав автомобилей", calculationRequest.Vehicles);
				WriteRow(writer, "Установка трекеров на автомобили в техцентре-партнере МТС", calculationRequest.InstallByMTSPartner);
				WriteRow(writer, "Использование ГЛОНАСС", calculationRequest.Glonass);
				WriteRow(writer, "Контроль уровня топлива в баке", calculationRequest.FuelLevelSensor);

				WriteHeader(writer, "Контактная информация");

				WriteRow(writer, "Компания уже является корпоративным абонентом МТС", calculationRequest.IsAlreadyMTSCustomer);
				WriteRow(writer, "Город", calculationRequest.City);
				WriteRow(writer, "Название компании", calculationRequest.CompanyName);
				WriteRow(writer, "Контактное лицо", calculationRequest.UserName);
				WriteRow(writer, "Адрес электронной почты", calculationRequest.Email);
				WriteRow(writer, "Телефон: ", calculationRequest.Phone);
				WriteRow(writer, "Дополнительная информация", calculationRequest.AdditionalInformation);

				writer.WriteEndElement();
			}
			return body.ToString();
		}

		private static void WriteHeader(XmlWriter writer, string text)
		{
			if (writer.WriteState != WriteState.Start)
			{
				writer.WriteElementString("br", string.Empty);
				writer.WriteElementString("br", string.Empty);
			}

			writer.WriteStartElement("b");
			writer.WriteString(text);
			writer.WriteEndElement();

			writer.WriteElementString("br", string.Empty);
		}

		private static void WriteRow(XmlWriter writer, string name, int value)
		{
			WriteRow(writer, name, value.ToString(CultureInfo.InvariantCulture));
		}

		private static void WriteRow(XmlWriter writer, string name, bool value)
		{
			WriteRow(writer, name, value ? "Да" : "Нет");
		}

		private static void WriteRow(XmlWriter writer, string name, string value)
		{
			writer.WriteElementString("i", name + ": ");
			writer.WriteElementString("span", value);
			writer.WriteElementString("br", string.Empty);
		}

		private readonly BulkProcessor<MessageStateItem> _messageStateChangeNotifier;

		private void NotifyTerminalManagerAboutMessageStateChange(MessageStateItem[] items)
		{
			var terminalManager = TerminalManager;

			if (terminalManager == null)
				throw new TerminalServerConnectionException("Terminal manager is null");
			try
			{
				TerminalManager.NotifyMessageState(items);
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}: {1} messages were not notified because of error: {2}",
					this, items.Length, ex);
			}
		}

		public void NotifyMessageStateChanged(MessageStateItem item)
		{
			if (_messageStateChangeNotifier != null)
				_messageStateChangeNotifier.Enqueue(item);
		}

		private readonly ConcurrentDictionary<int, DateTime> _requestFriendPositions =
			new ConcurrentDictionary<int, DateTime>();

		private readonly ConcurrentDictionary<int, DateTime> _lastRequestedContact =
			new ConcurrentDictionary<int, DateTime>();

		//TODO: вынести в класс и параметризовать задержкой, сейчас захардкоженой в 5 минут
		private static bool WasProcessedInFiveMinutes(int id, ConcurrentDictionary<int, DateTime> lastTime)
		{
			var utcNow = DateTime.UtcNow;
			if (lastTime.TryAdd(id, utcNow))
				return false;

			var last = lastTime[id];
			if (utcNow - last < TimeSpan.FromMinutes(5))
				return true;
			return !lastTime.TryUpdate(id, utcNow, last);
		}
	}
}