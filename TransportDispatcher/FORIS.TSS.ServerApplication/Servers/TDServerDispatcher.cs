﻿using System.ComponentModel;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication
{
	public class TDServerDispatcher :
		ServerDispatcher<TDServer>
	{
		#region Constructor & Dispose

		public TDServerDispatcher(IContainer container)
		{
			container.Add(this);
		}
		public TDServerDispatcher()
		{
		}

		#endregion Constructor & Dispose

		#region Ambassadors

		private readonly TDServerAmbassadorCollection ambassadors =
			new TDServerAmbassadorCollection();
		protected override AmbassadorCollection<IServerItem<TDServer>> GetAmbassadorCollection()
		{
			return ambassadors;
		}

		#endregion Ambassadors
	}
	public class TDServerAmbassadorCollection :
		ServerAmbassadorCollection<TDServer>
	{
		public new TDServerAmbassador this[int index]
		{
			get { return (TDServerAmbassador)base[index]; }
		}
	}
	public class TDServerAmbassador :
		ServerAmbassador<TDServer>
	{
	}
}