﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using Interfaces.Web;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		/// <summary> Поиск клиентов по тексту </summary>
		/// <param name="searchText"> Искомый текст </param>
		/// <param name="pagination"> Параметр постраничной выдачи </param>
		/// <param name="cancellationToken"> Маркер отмены операции </param>
		/// <returns></returns>
		public List<SearchHits<CustomerItem>> SearchCustomers(string searchText, Pagination pagination, CancellationToken cancellationToken)
		{
			var items = new List<SearchHits<CustomerItem>>();
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SearchCustomers"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@searchText"].Value = searchText;
				sp["@operatorId"].Value = OperatorId;
				if (pagination != null)
				{
					sp["@skipCount"].Value = pagination.Skip;
					sp["@takeCount"].Value = pagination.RecordsPerPage;
				}
				using (var spReader = sp.ExecuteReader(cancellationToken))
				{
					var reader = spReader as DbDataReader;
					if (null == reader)
						return items;
					if (!reader.HasRows)
						return items;
					// Чтение статистики выборки
					var ordTotalFind = reader.GetOrdinal("TOTALFIND");
					var ordTotalPage = reader.GetOrdinal("TOTALPAGE");
					if (reader.ReadAsync(cancellationToken).Result)
					{
						if (null != pagination && pagination.ComputeTotal)
							pagination.TotalRecords = reader.GetInt32(ordTotalFind);
						pagination.TotalPageRecords = reader.GetInt32(ordTotalPage);
					}
					// Переход к выборке элементов поиска
					if (!reader.NextResultAsync(cancellationToken).Result)
						return items;
					var ord_id       = reader.GetOrdinal("id");
					var ord_name     = reader.GetOrdinal("name");
					var ord_desc     = reader.GetOrdinal("desc");
					var ord_type     = reader.GetOrdinal("type");
					var ord_kind     = reader.GetOrdinal("kind");
					var ord_contract = reader.GetOrdinal("contract");
					while (reader.ReadAsync(cancellationToken).Result)
					{
						var searchHitsItem = new SearchHits<CustomerItem>
						{
							Item = new CustomerItem
							{
								id       = reader.GetInt32       (ord_id),
								name     = reader.GetStringOrNull(ord_name),
								desc     = reader.GetStringOrNull(ord_desc),
								type     = reader.GetFieldValue<DepartmentType>(ord_type),
								kind     = reader.GetFieldValue<CustomerKind>  (ord_kind),
								contract = reader.GetStringOrNull(ord_contract),
							},
							Hits = new List<HitItem>()
						};
						items.Add(searchHitsItem);
					}
					// Переход к выборке вхождений в элементы поиска
					if (!reader.NextResultAsync(cancellationToken).Result)
						return items;
					var ord_ItmId   = reader.GetOrdinal("ItmId");
					var ord_ItmKind = reader.GetOrdinal("ItmKind");
					var ord_HitType = reader.GetOrdinal("HitType");
					var ord_HitText = reader.GetOrdinal("HitText");

					while (reader.ReadAsync(cancellationToken).Result)
					{
						var itmId   = reader.GetFieldValue<int>(ord_ItmId);
						var itmKind = reader.GetFieldValue<int>(ord_ItmKind);
						var item = items.FirstOrDefault(i => i.Item.id == itmId && (int)i.Item.kind == itmKind);
						if (null != item)
						{
							item.Hits.Add(
								new HitItem
								{
									HitType = reader.GetStringOrNull(ord_HitType),
									HitText = reader.GetStringOrNull(ord_HitText)
								});
						}
					}
					if (pagination.TotalPageRecords != items.Count)
						pagination.TotalPageRecords = items.Count;
				}
				return items.ToList();
			}
		}
	}
}