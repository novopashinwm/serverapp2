﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public bool IsRuleServerAllowed { get { return !IsGuest; } }
		List<Rule> IRuleServer.GetRules(IdType idType, int id)
		{
			if (!IsRuleServerAllowed)
				throw new SecurityException("RuleServices are disallowed");

			using (var entities = new Entities())
			{
				IQueryable<CompoundRule> queryableCompoundRules;

				switch (idType)
				{
					case IdType.Vehicle:
						//Правила по объекту наблюдения

						//Права на объект
						if (!entities.IsAllowedVehicleAll(OperatorId, id, SystemRight.VehicleAccess))
							return new List<Rule>();

						queryableCompoundRules =
						(
							from v in entities.VEHICLE
							from cr in v.CompoundRule
							where v.VEHICLE_ID == id
							select cr
						);
						break;
					case IdType.VehicleGroup:
						//Права на группу объектов
						if (!entities.IsAllowedVehicleGroupAll(OperatorId, id, SystemRight.VehicleAccess))
							return new List<Rule>();

						queryableCompoundRules =
						(
							from vg in entities.VEHICLEGROUP
							from cr in vg.CompoundRule
							where vg.VEHICLEGROUP_ID == id
							select cr
						);
						break;
					default:
						throw new NotSupportedException(idType.ToString());
				}

				var compoundRules = queryableCompoundRules
					.Where(cr => cr.CompoundRule_Message_Template
						.Any(mt => mt.OPERATOR.OPERATOR_ID == OperatorId))
					.ToList();

				return entities.GetRulesDTOs(compoundRules, idType, id);
			}
		}
		void IRuleServer.RemoveRule(int ruleId)
		{
			if (!IsRuleServerAllowed)
				throw new SecurityException("RuleServices are disallowed");

			using (var entities = new Entities())
			{
				var compoundRule =
				(
					from cr in entities.CompoundRule
						.Include(nameof(CompoundRule.CompoundRule_Vehicle))
						.Include(nameof(CompoundRule.CompoundRule_VehicleGroup))
					where cr.CompoundRule_ID == ruleId
					select cr
				)
					 .FirstOrDefault();

				if (compoundRule == null)
					return;

				var vehicles = compoundRule.CompoundRule_Vehicle.ToArray();
				foreach (var vehicle in vehicles)
				{
					if (!entities.IsAllowedVehicleAll(OperatorId, vehicle.VEHICLE_ID, SystemRight.VehicleAccess))
						continue;
					entities.Remove(compoundRule, vehicle);
				}

				var vehicleGroups = compoundRule.CompoundRule_VehicleGroup.ToArray();
				foreach (var vehicleGroup in vehicleGroups)
				{
					if (!entities.IsAllowedVehicleGroupAll(OperatorId, vehicleGroup.VEHICLEGROUP_ID, SystemRight.VehicleAccess))
						continue;
					entities.Remove(compoundRule, vehicleGroup);
				}

				if (compoundRule.CompoundRule_Vehicle.Count == 0 && compoundRule.CompoundRule_VehicleGroup.Count == 0)
					entities.DeleteCompoundRule(compoundRule);
				entities.SaveChangesBySession(SessionId);
			}
		}
		Rule IRuleServer.SaveRule(Rule rule)
		{
			if (!IsRuleServerAllowed)
				throw new SecurityException("RuleServices are disallowed");

			using (var entities = new Entities())
			{
				switch (rule.ObjectIdType)
				{
					case IdType.Vehicle:
						if (!entities.IsAllowedVehicleAll(OperatorId, rule.ObjectId, SystemRight.VehicleAccess))
							return null;
						break;
					case IdType.VehicleGroup:
						if (!entities.IsAllowedVehicleGroupAll(OperatorId, rule.ObjectId, SystemRight.VehicleAccess))
							return null;
						break;
					default:
						throw new NotSupportedException(rule.ObjectIdType.ToString());
				}

				var validationErrors = entities.CheckRule(OperatorId, rule);
				if (validationErrors.Length > 0)
				{
					var strings = ResourceContainers.Get(CultureInfo ?? CultureInfo.CurrentCulture);
					var validationError = validationErrors.First();
					var message = string.Join(Environment.NewLine, string.Format(CultureInfo, strings[validationError.Message], validationError.Value));
					throw new ValidationException(message)
					{
						Value = validationError.Value,
						Type  = validationError.ValidationGroup
					};
				}

				entities.ProcessRuleSubscriptions(rule.Subscriptions);
				entities.SaveChangesBySession(SessionId);

				var compoundRule = entities.AddOrUpdateRule(OperatorId, rule);
				entities.SaveChangesBySession(SessionId);

				return entities.GetRulesDTOs(new[] { compoundRule }, rule.ObjectIdType, rule.ObjectId)
					.FirstOrDefault();
			}
		}
		List<BusinessLogic.DTO.OperatorRights.Operator> IRuleServer.GetOperators(IdType idType, int id)
		{
			if (!IsRuleServerAllowed)
				throw new SecurityException("RuleServices are disallowed");

			using (var entities = new Entities())
			{
				switch (idType)
				{
					case IdType.Vehicle:
						if (!entities.IsAllowedVehicleAll(OperatorId, id, SystemRight.VehicleAccess))
							return null;
						break;
					case IdType.VehicleGroup:
						if (!entities.IsAllowedVehicleGroupAll(OperatorId, id, SystemRight.VehicleAccess))
							return null;
						break;
					default:
						throw new NotSupportedException(idType.ToString());
				}

				return new List<BusinessLogic.DTO.OperatorRights.Operator>
				{
					GetOperatorDetails(OperatorId)
				};
			}
		}
	}
}