﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Config;
using FORIS.TSS.EntityModel;
using Interfaces.Web;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	public class TDSessionManager : SessionManager<TDServer>, IWebLoginProvider
	{
		private readonly TDServerDispatcher _serverDispatcher = new TDServerDispatcher();
		protected override ServerDispatcher<TDServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}
		/// <summary> Метод создает объект сессии на сервере </summary>
		/// <returns> объект сессии - персональный сервер </returns>
		/// <remarks>
		/// Возможно следует передавать какие-нибудь параметры
		/// в этот метод, чтоб можно было создавать объекты
		/// сессий различных типов в зависимости от запроса клиента
		/// </remarks>
		protected override PersonalServerBase<TDServer> OnCreatePersonalServer()
		{
			return new TDPersonalServer();
		}
		public CheckPasswordResult CheckPasswordForWeb(string login, string password)
		{
			return ServerApplication.Server.Instance().CheckPasswordForWeb(login, password);
		}
		public void SendConfirmationEmail(string emailAddressTo, string subjectFormat, string messageFormatString, bool encodeAsUri)
		{
			//TODO: проверить валидность emailAddressTo

			using (var entities = new Entities())
			{
				var emails = entities.Operator_Contact
					.Include(Operator_Contact.ContactIncludePath)
					.Include(Operator_Contact.OPERATORIncludePath)
					.Where(oc =>
						oc.Confirmed                       &&
						oc.Contact.Value == emailAddressTo &&
						oc.Contact.Type  == (int) ContactType.Email)
					.ToList();

				if (emails.Count == 0)
					throw new SecurityException("Current email does not have enough permissions to restore password.");

				foreach (var email in emails)
				{
					var confirmationKey = ServerApplication.Server.Instance().GenerateRandomKey(
						email.OPERATOR.OPERATOR_ID.ToString(CultureInfo.InvariantCulture),
						32);

					var confirmationKeyString = encodeAsUri
						? Uri.EscapeDataString(confirmationKey)
						: confirmationKey;

					var message = entities.CreateOutgoingMessage(
						email.Contact,
						string.Format(subjectFormat, email.OPERATOR.LOGIN),
						string.Format(messageFormatString, confirmationKeyString, email.OPERATOR.LOGIN));

					message.MESSAGE_TEMPLATE =
						entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.GenericConfirmation);
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.ConfirmationCode, confirmationKey);

				}
				entities.SaveChangesByOperator(null);
			}
		}
		string IWebLoginProvider.GetEmailByConfirmationKey(string confirmationKey)
		{
			using (var entities = new Entities())
			{
				return entities
					?.GetEmailByConfirmationKey(confirmationKey)
					?.FirstOrDefault()
					?.Value;
			}
		}
		int? IWebLoginProvider.GetOperatorIdByConfirmationKey(string confirmationKey)
		{
			using (var entities = new Entities())
			{
				var minMessageTime = DateTime.UtcNow.AddHours(-1);
				var message = entities.MESSAGE_FIELD
					.Where(f =>
						f.MESSAGE_TEMPLATE_FIELD.NAME                  == MESSAGE_TEMPLATE_FIELD.ConfirmationCode &&
						f.MESSAGE_TEMPLATE_FIELD.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.GenericConfirmation &&
						f.CONTENT == confirmationKey)
					.Select(f => f.MESSAGE)
					.FirstOrDefault(m =>
						minMessageTime < m.TIME &&
						m.Owner_Operator_ID == null &&
						!m.MESSAGE_FIELD.Any(f => f.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.ConfirmationIsUsed));
				if (message == null)
					return null;

				entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.ConfirmationIsUsed, true);

				var @operator = entities.MESSAGE
					.Where(m => m.MESSAGE_ID == message.MESSAGE_ID)
					.SelectMany(m => m.Contacts
						.Where(mc =>
							mc.Type == (int) MessageContactType.Destination &&
							mc.Contact.Type == (int) ContactType.Email)
						.Select(mc => mc.Contact))
					.SelectMany(c => c.Operators.Where(oc => oc.Confirmed).Select(o => o.OPERATOR))
					.FirstOrDefault();

				entities.SaveChangesByOperator(null);

				return @operator != null ? @operator.OPERATOR_ID : (int?) null;
			}
		}
		string IWebLoginProvider.GetLoginByEmail(string email)
		{
			using (var entities = new Entities())
			{
				return entities.Operator_Contact
					.Where(oc =>
						oc.Confirmed &&
						oc.Contact.Type == (int)ContactType.Email &&
						oc.Value        == email)
					.Select(oc => oc.OPERATOR.LOGIN)
					.FirstOrDefault();
			}
		}
		bool IWebLoginProvider.CheckCaptcha(string remoteIP, string challenge, string response)
		{
			var privateKey = GlobalsConfig.AppSettings["recaptcha-private-key"];

			var httpRequestBody =
				"privatekey=" + privateKey +
				"&remoteip="  + remoteIP   +
				"&challenge=" + challenge  +
				"&response="  + Uri.EscapeDataString(response);

			byte[] buffer = Encoding.ASCII.GetBytes(httpRequestBody);

			var httpRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri("http://www.google.com/recaptcha/api/verify?" + httpRequestBody));
			httpRequest.Proxy = GlobalProxySelection.GetEmptyWebProxy();
			httpRequest.ContentType = "text/plain";
			httpRequest.Method = "POST";
			httpRequest.Timeout = 3000;
			httpRequest.ContentLength = buffer.Length;

			using (var requestStream = httpRequest.GetRequestStream())
			{
				requestStream.Write(buffer, 0, buffer.Length);
				requestStream.Close();
				var loWebResponse = (HttpWebResponse)httpRequest.GetResponse();

				using (var stringReader = new StreamReader(loWebResponse.GetResponseStream()))
				{
					var firstLine = stringReader.ReadLine();
					return string.Equals(firstLine, "true", StringComparison.OrdinalIgnoreCase);
				}
			}
		}
		void IWebLoginProvider.SendEmail(string email, string subject, string body)
		{
			if (email == null)
				throw new ArgumentNullException("email");
			email = email.Trim();

			try
			{
				using (var entities = new Entities())
				{
					entities.CreateOutgoingEmail(email, subject, body);
					entities.SaveChangesByOperator(null);
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
		}
		public void SendQuestionToSupport(string clientName, string trackerNumber, string clientContacts, string question)
		{
			ServerApplication.Server.Instance().SendQuestionToSupport(null, clientName, trackerNumber, clientContacts, question);
		}
		public int? GetOperatorIdBySimId(string simId, string name)
		{
			using (var entities = new Entities())
			{
				var defaultOperatorAlias = string.Empty;
				var asid = entities.GetAsidBySimId(simId);
				var ormOperator = entities.GetOrCreateOperatorByAsid(asid, defaultOperatorAlias);
				if (ormOperator == null)
					return null;

				return ormOperator.OPERATOR_ID;
			}
		}
		/// <summary> Создает оператора по идентификатору объекта наблюдения для общих ссылок </summary>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		public int? GetOperatorId4ShareLink(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var operatorShareLink = entities.GetOrCreateOperator4ShareLink(vehicleId);
				if (null == operatorShareLink)
					return null;
				return operatorShareLink.OPERATOR_ID;
			}
		}
		public void SendCalculationRequest(CalculationRequest customerForm)
		{
			ServerApplication.Server.Instance().SendCalculationRequest(null, customerForm);
		}
		public bool SimIdExists(string simId)
		{
			using (var entities = new Entities())
			{
				return entities.Asid.Any(a => a.SimID == simId);
			}
		}
		private static string GetStringFromConfig(string key, int minAllowedLength)
		{
			var s = ConfigurationManager.AppSettings[key];
			if (s == null || s.Length < minAllowedLength)
				throw new InvalidOperationException("Invalid configuration: " + key + " " + s);
			return s;
		}
		private static int GetNumberFromConfig(string key, int minAllowedValue)
		{
			var s = ConfigurationManager.AppSettings[key];
			int i;
			if (!int.TryParse(s, out i) || i < minAllowedValue)
				throw new InvalidOperationException("Invalid configuration: " + key + " " + s);
			return i;
		}
		private static string GenerateDecimalSequence(int length)
		{
			var random = new Random();
			var result = new StringBuilder(length);
			for (var i = 0; i != length; ++i)
				result.Append(random.Next(0, 9));
			return result.ToString();
		}
	}
}