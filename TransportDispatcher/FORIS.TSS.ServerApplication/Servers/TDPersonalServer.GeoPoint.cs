﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;

//Реализация функционала по работе с точками интереса

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		#region WebPointsNew
		public List<WebPoint> GetPointsWeb()
		{
			using (var entities = new Entities())
			{
				return entities
					.GetPointsWeb(null, OperatorId)
					.Select(p => p.ToDto())
					.ToList();
			}
		}
		private WEB_POINT GetOrmWebPoint(int webPointId, Entities entities = null)
		{
			// Поиск точки
			var ormWebPoint = entities
				?.WEB_POINT
				?.Include("OPERATOR")
				?.Include("WEB_POINT_TYPE")
				?.Include("MAP_VERTEX")
				?.FirstOrDefault(i => i.POINT_ID == webPointId && i.OPERATOR_ID == OperatorId);
			// Точка не найдена, выдаем исключение
			if (null == ormWebPoint)
				throw new KeyNotFoundException(nameof(WebPoint) + $" with Id={webPointId} not found");
			return ormWebPoint;
		}
		public WebPoint WebPointRead(int webPointId)
		{
			using (var entities = new Entities())
				return GetOrmWebPoint(webPointId, entities)
					?.ToDto();
		}
		public WebPoint WebPointCreate(WebPoint dtoWebPoint)
		{
			using (var entities = new Entities())
			{
				// Преобразование DTO в объект ORM
				var ormWebPoint = WEB_POINT.CreateFromDto(dtoWebPoint, entities);
				// Добавляем текущего оператора
				ormWebPoint.OPERATOR_ID = OperatorId;
				ormWebPoint.OPERATOR    = entities.GetOperator(OperatorId);
				// Добавление объекта ORM в контекст
				entities.AddToWEB_POINT(ormWebPoint);
				// Сохранение изменений с использованием сессии для сохранения истории
				entities.SaveChangesBySession(SessionId);
				// Чтение точки
				return WebPointRead(ormWebPoint.POINT_ID);
			}
		}
		public WebPoint WebPointUpdate(WebPoint dtoWebPoint)
		{
			using (var entities = new Entities())
			{
				var ormWebPoint = GetOrmWebPoint(dtoWebPoint.Id, entities);
				ormWebPoint.UpdateFromDto(dtoWebPoint, entities);
				// Сохранение изменений с использованием сессии для сохранения истории
				entities.SaveChangesBySession(SessionId);
				// Повторное чтение из БД (возможно тут и не нужно, но база главней)
				return WebPointRead(ormWebPoint.POINT_ID);
			}
		}
		public void WebPointDelete(int webPointId)
		{
			using (var entities = new Entities())
			{
				var ormWebPoint = GetOrmWebPoint(webPointId, entities);
				if (!ormWebPoint.OPERATORReference.IsLoaded)
					ormWebPoint.OPERATORReference.Load();
				if (!ormWebPoint.WEB_POINT_TYPEReference.IsLoaded)
					ormWebPoint.WEB_POINT_TYPEReference.Load();
				if (!ormWebPoint.MAP_VERTEXReference.IsLoaded)
					ormWebPoint.MAP_VERTEXReference.Load();

				entities.DeleteWebPoint(ormWebPoint);
				entities.SaveChangesBySession(SessionId);
			}
		}
		#endregion WebPointsNew
		public DataSet GetPointsWebForOperator()
		{
			// Используем EF вместо IDbDataAdapter
			using (var entities = new Entities())
			{
				return new DataSet()
				{
					Tables =
					{
						entities.GetPointsWeb(null, OperatorId).ToDataTable("PointsWeb")
					}
				};
			}
		}
		public void AddPointWebForOperator(PointF point, string pointName, string description, int pointType)
		{
			using (var entities = new Entities())
			{
				//////////////////////////////////////////////////
				var vertex = new MAP_VERTEX
				{
					ENABLE = true,
					X      = point.X,
					Y      = point.Y
				};
				entities.MAP_VERTEX.AddObject(vertex);
				//////////////////////////////////////////////////
				var webPoint = new WEB_POINT
				{
					NAME           = pointName,
					DESCRIPTION    = description,
					WEB_POINT_TYPE = entities.GetWebPointType((WebPointType) pointType),
					OPERATOR       = entities.GetOperator(OperatorId),
					MAP_VERTEX     = vertex,
				};
				entities.WEB_POINT.AddObject(webPoint);
				//////////////////////////////////////////////////
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void EdtPointWebForOperator(int pointID, PointF point, string pointName, string description, int pointType)
		{
			using (var entities = new Entities())
			{
				var webPoint = GetPointForEdit(entities, pointID);
				//////////////////////////////////////////////////
				webPoint.NAME           = pointName;
				webPoint.DESCRIPTION    = description;
				webPoint.WEB_POINT_TYPE = entities.WEB_POINT_TYPE.FirstOrDefault(wpt => wpt.TYPE_ID == pointType);
				webPoint.MAP_VERTEX.X   = point.X;
				webPoint.MAP_VERTEX.Y   = point.Y;
				//////////////////////////////////////////////////
				entities.SaveChangesBySession(SessionId);
			}
		}
		public void DelPointWebForOperator(int pointID)
		{
			using (var entities = new Entities())
			{
				var webPoint = GetPointForEdit(entities, pointID);
				entities.DeleteWebPoint(webPoint);
				entities.SaveChangesBySession(SessionId);
			}
		}
		private WEB_POINT GetPointForEdit(Entities entities, int pointID)
		{
			var ormWebPoint = entities.WEB_POINT
				.Include("OPERATOR")
				.Include("WEB_POINT_TYPE")
				.Include("MAP_VERTEX")
				.FirstOrDefault(p => p.POINT_ID == pointID);

			if (ormWebPoint == null)
				throw new ArgumentOutOfRangeException("pointID", pointID, "Point was not found");
			if (ormWebPoint.OPERATOR != null && ormWebPoint.OPERATOR.OPERATOR_ID != OperatorId)
				throw new SecurityException("Point is owned by other operator");

			if (ormWebPoint.OPERATOR == null && !entities.IsAllowed(OperatorId, SystemRight.EditPublicPoints))
				throw new SecurityException("Insufficient rights to edit public points");

			return ormWebPoint;
		}
	}
}