﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;
using Interfaces.Web;

namespace FORIS.TSS.ServerApplication
{
	public class TDServer : ServerBase<TDServer>
	{
		#region Controls & Components

		private IContainer                _components;
		private TDServerDispatcher        _serverDispatcher;

		#endregion  Controls & Components

		#region Constructor & Dispose

		public TDServer(SessionManager<TDServer> sessionManager, NameValueCollection properties)
			: base(sessionManager, properties)
		{
			InitializeComponent();
			Init();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				_components?.Dispose();
			base.Dispose(disposing);
		}

		#endregion  Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			_components       = new Container();
			_serverDispatcher = new TDServerDispatcher(_components);
		}

		#endregion Component Designer generated code

		protected override ServerDispatcher<TDServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}

		protected override IDatabaseDataSupplier GetDatabaseDataSupplier()
		{
			return Server.Instance().DbManager;
		}

		#region Start & Stop

		protected override void Start()
		{
		}
		protected override void Stop()
		{
		}

		#endregion Start & Stop

		/// <summary> Создание/редактирование норматива межзонового пробега </summary>
		/// <param name="zone_id_from"> id 1-й геозоны </param>
		/// <param name="zone_id_to"> id 2-й геозоны </param>
		/// <param name="distance"> норматив (в метрах) </param>
		/// <param name="operatorID"> ID оператора </param>
		/// <param name="sessionID"> ID сессии пользователя </param>
		public void UpdateZoneDistanceStandart(int zone_id_from, int zone_id_to, int distance, int operatorID, int sessionID)
		{
			Server.Instance().UpdateZoneDistanceStandart(zone_id_from, zone_id_to, distance, operatorID, sessionID);
		}
		/// <summary> Получение списка всех нормативов межзонового пробега, доступных данному оператору </summary>
		/// <param name="operatorID"> ID оператора </param>
		public DataSet GetAllZoneDistanceStandarts( int operatorID)
		{
			return Server.Instance().GetAllZoneDistanceStandarts(operatorID);
		}
		public SortedList<int, IWebMobileInfoCP> GetMobileHistory(int from, int to, int id, int interval, int count)
		{
			int iLogLength;
			var res = GetMobileHistory(from, to, id, interval, count, out iLogLength);
			return res; 
		}
		public SortedList<int, IWebMobileInfoCP> GetMobileHistory(int from, int to, int id, int interval, int count, out int iLogLength)
		{
			string garageNumber;

			SortedList log = Server.Instance().DbManager.GetLogNew(from, to, id, interval, count, out iLogLength, out garageNumber);
			var result = new SortedList<int, IWebMobileInfoCP>(log.Count);
			int i = 0;
			foreach (MobilUnit.Unit.MobilUnit unit in log.Values)
			{
				var webMobileInfo = 
					new WebMobileInfoCP(
						unit.Unique,
						unit.ID,
						garageNumber,
						0,
						unit.Time,
						unit.Speed,
						unit.Course ?? 0,
						unit.Latitude,
						unit.Longitude,
						unit.ValidPosition,
						(int)unit.Status,
						unit.CorrectTime,
						unit.Latitude,
						unit.Longitude,
						unit.VoltageAN4,
						unit.VoltageAN1
						);

				webMobileInfo.Radius = unit.Radius;
				
				result[i] = webMobileInfo;
				i++;
			}

			return result;
		}
		public DataSet GetDataFromDB(ArrayList arParams, string strStoredProcedureName, string[] strTablesNames)
		{
			return Server.Instance().GetDataFromDB(arParams, strStoredProcedureName, strTablesNames);
		}
		/// <summary> Получает информацию об объектах наблюдения для веб-клиента </summary>
		/// <remarks>Не следует увеличивать использование метода</remarks>
		/// <param name="departmentId">Идентификатор клиента, которым следует ограничить выборку</param>
		/// <param name="vehicleIds">Необязательный параметр: список объектов наблюдения, которыми следует ограничить выборку</param>
		/// <param name="operatorID">Идентификатор пользователя, от имени которого производится выборка</param>
		public DataSet GetDataForWeb(int operatorID, int? departmentId, int[] vehicleIds)
		{
			return Server.Instance().DbManager.GetDataForWeb(operatorID, departmentId, vehicleIds);
		}
		public IDictionary<int, IMobilUnit> GetLastPositions(int[] uniques)
		{
			return Server.Instance().GetLastPositions(uniques);
		}
	}
}