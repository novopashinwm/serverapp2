﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO.PlatformSpecific.Android;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Billing;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		public void TryAttachAndroidSubscription(string purchase, byte[] signature)
		{
			if (!VerifyAndroidSignature(purchase, signature))
			{
				Trace.TraceWarning("Purchase not verified");
				return;
			}

			var androidPurchase = JsonHelper.DeserializeObjectFromJson<Purchase>(purchase);
			
			var serviceType = string.Empty
				+"android"
				+ Billing_Service_Type.PurchaseInfoDelimiter
				+ androidPurchase.packageName
				+ Billing_Service_Type.PurchaseInfoDelimiter
				+ androidPurchase.productId;

			using (var entities = new Entities())
			{
				Billing_Service billingService =
					entities.Billing_Service.FirstOrDefault(
						bs => bs.Type.Service_Type == serviceType &&
							  bs.PurchaseToken == androidPurchase.purchaseToken);

				if (billingService != null)
				{
					if (billingService.Operator_ID == OperatorId)
						return;
					billingService.Operator_ID = OperatorId;
					entities.CreateAppNotificationAboutOperatorDataChange(OperatorId, UpdatedDataType.BillingServices);
					entities.SaveChangesBySession(SessionId);
					Trace.TraceWarning("Purchase was belonged to another user");
					return;
				}

				billingService = entities.CreateBillingService(serviceType);
				billingService.Operator_ID = OperatorId;
				billingService.PurchaseToken = androidPurchase.purchaseToken;

				if (androidPurchase.autoRenewing)
				{
					var scheduledTask = entities.SCHEDULERQUEUE.CreateObject();
					billingService.ScheduledTask = scheduledTask;
					scheduledTask.Enabled = true;
					scheduledTask.Operator_ID = OperatorId;
					scheduledTask.SCHEDULEREVENT = entities.GetOrCreateSchedulerEvent(typeof(AndroidSubscriptionValidator));
					scheduledTask.ErrorCount = 0;
					scheduledTask.REPETITION_XML = null;
					scheduledTask.NEAREST_TIME = DateTime.UtcNow;
				}

				entities.CreateAppNotificationAboutOperatorDataChange(OperatorId, UpdatedDataType.BillingServices);
				entities.SaveChangesBySession(SessionId);

				Trace.TraceInformation("Purchase registered");
			}
		}
		private bool VerifyAndroidSignature(string s, byte[] signature)
		{
			var apiKeyModulus = Server.Instance().GetConstant(Constant.AndroidAppKeyModulus);
			var apiKeyExponent = Server.Instance().GetConstant(Constant.AndroidAppKeyExponent);

			try
			{
				using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
				{
					RSAParameters rsaKeyInfo = new RSAParameters
					{
						Exponent = Convert.FromBase64String(apiKeyExponent),
						Modulus = Convert.FromBase64String(apiKeyModulus)
					};
					rsa.ImportParameters(rsaKeyInfo);

					return rsa.VerifyData(Encoding.ASCII.GetBytes(s), "SHA1", signature);

				}
			}
			catch (CryptographicException e)
			{
				Trace.TraceError("{0}", e);
				return false;
			}
		}
		public void TryAttachAppleSubscription(string purchaseBase64)
		{
			using (var entities = new Entities())
			{
				var schedulerQueue = entities.SCHEDULERQUEUE.CreateObject();
				schedulerQueue.Enabled = true;
				schedulerQueue.SCHEDULEREVENT = entities.GetOrCreateSchedulerEvent(typeof (AppleReceiptProcessor));
				schedulerQueue.ErrorCount = 0;
				schedulerQueue.REPETITION_XML = null;
				schedulerQueue.CONFIG_XML = JsonHelper.SerializeObjectToJson(
					new AppleReceiptProcessor.Parameters
					{
						PurchaseBase64 = purchaseBase64,
						AppId = AppId
					});
				schedulerQueue.NEAREST_TIME = DateTime.UtcNow;
				schedulerQueue.Operator_ID = OperatorId;
				entities.SaveChangesBySession(SessionId);
			}
		}
	}
}