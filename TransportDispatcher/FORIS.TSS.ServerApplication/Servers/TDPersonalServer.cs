﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.Resources;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;
using FORIS.TSS.WorkplaceShadow.Geo;
using Interfaces.Geo;
using Interfaces.Web;
using Orm = FORIS.TSS.EntityModel;
using Dto = FORIS.TSS.BusinessLogic.DTO;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer : PersonalServerBase<TDServer>, IWebPersonalServer
	{
		/// <summary> Фабрика (ленивая, создается только по запросу) отчетов для web </summary>
		private Lazy<ReportsFactoryWeb> _reportsFactoryWebLazy;

		private bool? _isSuperAdministrator;
		public bool IsSuperAdministrator
		{
			get
			{
				if (!_isSuperAdministrator.HasValue)
					using (var entities = new Entities())
						_isSuperAdministrator = entities
							.IsSuperAdministrator(OperatorId);
				return _isSuperAdministrator.Value;
			}
		}
		private bool? _filterVehiclesByDepartment = null;
		private bool FilterByDepartment
		{
			get
			{
				if (_filterVehiclesByDepartment == null)
				{
					_filterVehiclesByDepartment = ((IBasicFunctionSet)Server.Instance()).FilterByDepartment(OperatorId);
				}
				return _filterVehiclesByDepartment.Value;
			}
		}
		public override void Init(TDServer serverInstance, SessionManager<TDServer> sessionManager, ISessionInfo sessionInfo)
		{
			base.Init(serverInstance, sessionManager, sessionInfo);
			Init();
		}
		public void Init()
		{
			using (var entities = new Entities())
			{
				SelectDepartmentInternal(entities, entities?.GetDefaultDepartmentByOperatorId(OperatorId));
			}
			_reportsFactoryWebLazy = new Lazy<ReportsFactoryWeb>(() =>
				new ReportsFactoryWeb(Server.Instance(), Repository.Instance, OperatorId));
		}
		protected override void OnClosing()
		{
			base.OnClosing();
			CleanupBeforeLogout();
		}
		protected override void OnClosed()
		{
			base.OnClosed();
		}
		#region Properties

		public override string ApplicationName
		{
			get { throw new NotImplementedException(); }
		}

		#endregion Properties

		#region PersonalServerBase<TDServer> Members

		/// <summary> Подготавливает список параметров для вызова процедуры смены пароля </summary>
		/// <param name="operator_id"></param>
		/// <param name="new_password"></param>
		/// <returns></returns>
		private ArrayList ParamsPreparingForChangePassword(int operator_id, string new_password)
		{
			var arParams = new ArrayList();

			arParams.Add(new ParamValue("@operator_id", operator_id));
			arParams.Add(new ParamValue("@new_password", new_password));

			return arParams;
		}
		/// <summary> Returns tables for calculating access from Operators to Vehicles and Routes </summary>
		/// <returns></returns>
		internal DataSet GetSecurity(IDbTransaction transaction)
		{
			IDbConnection connection;
			if (transaction == null)
			{
				connection = null;
			}
			else
			{
				connection = transaction.Connection;
			}

			using (StoredProcedure sp = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_SECURITY", connection, transaction))
			{
				sp["@oid"].Value = OperatorId;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_SECURITY);
				return dataSet;
			}
		}

		#endregion PersonalServerBase<TDServer> Members

		#region IPersonalServer Members

		public override TssPrincipalInfo GetPrincipal()
		{
			return new TssPrincipalInfo("", "OPERATOR", OperatorId, TssPrincipalType.Operator);
		}

		#endregion IPersonalServer Members

		#region IWebPersonalServer Members
		public IRemoteTerminalServerManager GetRemoteTerminalServerManager(int vehicleId)
		{
			return new VehicleRemoteTerminalServerManager(vehicleId, SessionInfo);
		}
		/// <summary> Создание/редактирование норматива межзонового пробега </summary>
		/// <param name="zone_id_from">id 1-й геозоны</param>
		/// <param name="zone_id_to">id 2-й геозоны</param>
		/// <param name="distance">норматив (в метрах)</param>
		public void UpdateZoneDistanceStandart(int zone_id_from, int zone_id_to, int distance)
		{
			serverInstance.UpdateZoneDistanceStandart(zone_id_from, zone_id_to, distance, SessionInfo.OperatorInfo.OperatorId, SessionInfo.SessionId);
		}
		/// <summary> Получение списка всех нормативов межзонового пробега, доступных данному оператору </summary>
		public DataSet GetAllZoneDistanceStandarts()
		{
			return serverInstance.GetAllZoneDistanceStandarts(SessionInfo.OperatorInfo.OperatorId);
		}
		/// <summary> Получение расстояния в метрах для указанной машины за указанный диапазон времени </summary>
		/// <param name="from">время начала (UTC)</param>
		/// <param name="to">время окончания (UTC)</param>
		/// <param name="vehicle_id">id машины</param>
		/// <returns>расстояние в метрах</returns>
		public Single GetDistance(int from, int to, int vehicle_id)
		{
			//exec CALCULATEDISTANCE @from=1242030426,@to=1242064461,@vehicle_id=426
			var arParams = new ArrayList();
			arParams.Add(new ParamValue("@from", from));
			arParams.Add(new ParamValue("@to", to));
			arParams.Add(new ParamValue("@vehicle_id", vehicle_id));

			DataSet ds = GetDataFromDB(arParams, "dbo.CALCULATEDISTANCE", new string[] { "distance" });

			return (Single)ds.Tables["distance"].Rows[0]["distance"];
		}
		private IWebMobileHistory GetMobileHistory(int from, int to, int id, int interval, int count, bool getGrouping, bool getAllRecords)
		{
			int logLength;
			// отсортированный по времени массив исторических позиций за указанный интервал времени
			SortedList<int, IWebMobileInfoCP> mobileHistory = serverInstance.GetMobileHistory(from, to, id, interval, count, out logLength);

			// массив для хранения параметров группировки
			List<GroupingMobileUnit> grouping = null;

			if (getGrouping)
			{
				#region Группировка для WEB-клиента

				grouping = new List<GroupingMobileUnit>();

				// текущие параметры
				GroupingMobileUnit curGrouping = null;

				foreach (var entry in mobileHistory)
				{
					IWebMobileInfoCP curMU = entry.Value;

					// определяем к какому типу группировки надо причислить текущую позицию
					GroupingType groupType = GetGroupType(curMU);

					// формирование текущей группировки
					if (curGrouping == null || curGrouping.GroupingType != groupType)
					{
						curGrouping = new GroupingMobileUnit(groupType);
						grouping.Add(curGrouping);
					}

					// формирование узла
					var node = new GroupingMobileUnitNode(curMU.Time, null, curMU);

					// добавление узла в текущую группировку
					curGrouping.Add(node);
				}

				#endregion // Группировка для WEB-клиента
			}

			if (!getAllRecords)
			{
				mobileHistory = null;
			}

			return new WebMobileHistory(mobileHistory, grouping, GetDistance(from, to, id));
		}
		private static GroupingType GetGroupType(IWebMobileInfoCP curMU)
		{
			const int slowSpeed = 5; // минимальная скорость движения

			if (0 < curMU.Radius)
				return GroupingType.MLP;

			if (slowSpeed <= curMU.Speed)
				return GroupingType.Motion;

			if (0 < curMU.Speed && curMU.Speed < slowSpeed)
				return GroupingType.Slow;

			return GroupingType.Stop;
		}
		public IWebMobileHistory GetMobileHistory(int from, int to, int id, int interval, int count)
		{
			return GetMobileHistory(from, to, id, interval, count, true, true);
		}
		public IWebMobileHistory GetMobileHistory(int from, int to, int id, int interval, int count, bool groupingOnly)
		{
			if (groupingOnly)
			{
				return GetMobileHistory(from, to, id, interval, count, true, false);
			}
			else
			{
				return GetMobileHistory(from, to, id, interval, count, false, true);
			}
		}
		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="from">начало периода</param>
		/// <param name="to">конец периода</param>
		/// <param name="id">идентификатор машины</param>
		/// <param name="interval">интервал между позициями в секундах</param>
		/// <param name="count">количество запрашиваемых позиций</param>
		/// <returns>отсортированный по времени список последних позиций ТС</returns>
		IWebMobileHistory IWebPersonalServer.GetMobileHistory(int from, int to, int id, int interval, int count)
		{
			return GetMobileHistory(from, to, id, interval, count);
		}
		public DataSet GetDataFromDB(ArrayList arParams, string strStoredProcedureName, string[] strTablesNames)
		{
			return serverInstance.GetDataFromDB(arParams, strStoredProcedureName, strTablesNames);
		}
		public DataSet GetDataForWeb(params int[] vehicleIds)
		{
			return serverInstance.GetDataForWeb(
				OperatorId,
				(vehicleIds == null || vehicleIds.Length == 0) && FilterByDepartment ? ComputedDepartmentId : null,
				vehicleIds);
		}
		IDictionary<int, IWebMobileInfoCP> IWebPersonalServer.GetMobilesInfo(int[] iArrayGroup)
		{
			return GetMobilesInfo(iArrayGroup);
		}
		/// <summary> Заполняем поля данными об объекте </summary>
		/// <param name="objects"></param>
		private void FillMobileObjectInfo(IDictionary<int, IWebMobileInfoCP> objects)
		{
			var ds = GetDataForWeb(objects.Keys.ToArray());
			var vTable = ds.Tables["ALLOW_VEHICLE"];
			foreach (var obj in objects.Values)
			{
				var rows = vTable.Select("VEHICLE_ID=" + obj.Unique);
				if (rows.Length > 0)
				{
					obj.ObjectName = rows[0]["GARAGE_NUMBER"].ToString().Trim();
				}
			}
		}
		public IDictionary<int, IWebMobileInfoCP> GetMobilesInfo(int[] iArrayGroup)
		{
			IDictionary<int, IMobilUnit> positions = serverInstance.GetLastPositions(iArrayGroup);

			var result = new Dictionary<int, IWebMobileInfoCP>();
			foreach (KeyValuePair<int, IMobilUnit> pair in positions)
			{
				var webMobileInfo =
					new WebMobileInfoCP(
						pair.Value.Unique,
						pair.Value.ID,
						0,
						pair.Value.Time,
						pair.Value.Speed,
						pair.Value.Course ?? 0,
						pair.Value.Latitude,
						pair.Value.Longitude,
						pair.Value.ValidPosition,
						(int)pair.Value.Status,
						pair.Value.CorrectTime,
						pair.Value.Latitude,
						pair.Value.Longitude,
						pair.Value.VoltageAN4,
						pair.Value.VoltageAN1);

				result.Add(pair.Key, webMobileInfo);
			}

			FillMobileObjectInfo(result);

			return result;
		}
		public string CreateReportAsFile(Guid guid, PARAMS reportParams, ReportTypeEnum reportType)
		{
			AddDefaultParametrsForWebReport(reportParams);

			return _reportsFactoryWebLazy.Value.CreateReportAsFile(guid, reportParams, reportType);
		}
		/// <summary> Создается отчет, возвращается заполненный датасет/строка с версткой в html </summary>
		/// <param name="reportId"></param>
		/// <param name="reportParams"></param>
		/// <returns></returns>
		public object CreateReportAsObj(Guid reportId, PARAMS reportParams)
		{
			AddDefaultParametrsForWebReport(reportParams);

			return _reportsFactoryWebLazy.Value.CreateReportAsObj(reportId, reportParams);
		}
		public void ReportToSchedulerQueue(Guid reportId, PARAMS reportParams, IRepetitionClass repetition, DateTime time, List<int> emailIds)
		{
			if (repetition == null)
				throw new ArgumentNullException(nameof(repetition));

			AddDefaultParametrsForWebReport(reportParams);
			using (var entities = new Entities())
			{
				var emails = entities.Contact
					.Where(c => c.Type == (int)ContactType.Email && emailIds.Contains(c.ID))
					.ToArray();

				if (emails.Length == 0)
				{
					throw new ArgumentException(
						string.Format("No email found by ids {0}, operator {1}", emailIds.Join(", "), OperatorId), nameof(emailIds));
				}

				var reportParameters = _reportsFactoryWebLazy.Value.GetReportParameters(reportId, reportParams);
				if (reportParameters == null)
					throw new ArgumentException(@"Report not found by GUID " + reportId, "guid");

				var schedulerevent = entities.SCHEDULEREVENT.FirstOrDefault(x => x.REPORT.REPORT_GUID == reportId);
				if (schedulerevent == null)
					throw new InvalidOperationException("SchedulerEvent not found by report guid " + reportId);

				var repetitionXML = RepetitionParametersServices.ToXML(repetition);
				var configXML = SerializationHelper.ToXML(reportParameters);
				var schedulerQueueElement = new SCHEDULERQUEUE
				{
					Operator_ID    = OperatorId,
					NEAREST_TIME   = repetition.GetNextTimeInUTC(time) ?? DateTime.MinValue,
					REPETITION_XML = repetitionXML,
					CONFIG_XML     = configXML,
					SCHEDULEREVENT = schedulerevent,
					Enabled        = true
				};

				if (reportParams.ContainsKey("idType") && reportParams.ContainsKey("id"))
				{
					var idType = (IdType)reportParams["idType"];
					var entityId = (int)reportParams["id"];

					switch (idType)
					{
						case IdType.Vehicle:
							schedulerQueueElement.VEHICLE =
								entities.VEHICLE.FirstOrDefault(x => x.VEHICLE_ID == entityId);
							break;
						case IdType.VehicleGroup:
							schedulerQueueElement.VEHICLEGROUP =
								entities.VEHICLEGROUP.FirstOrDefault(x => x.VEHICLEGROUP_ID == entityId);
							break;
					}
				}

				entities.SCHEDULERQUEUE.AddObject(schedulerQueueElement);

				foreach (var email in emails)
				{
					schedulerQueueElement.Contact.Add(email);
				}

				entities.SaveChangesBySession(SessionId);
			}
		}
		private void AddDefaultParametrsForWebReport(PARAMS reportParams)
		{
			if (IsSuperAdministrator && FilterByDepartment && !reportParams.ContainsKey("DepartmentId"))
				reportParams.Add("DepartmentId", ComputedDepartmentId);
			if (!reportParams.ContainsKey("TimeZoneInfo"))
				reportParams.Add("TimeZoneInfo", ((IWebPersonalServer)this).TimeZoneInfo);
			if (!reportParams.ContainsKey("Culture"))
				reportParams.Add("Culture", CultureInfo);
			if (!reportParams.ContainsKey("OperatorId"))
				reportParams.Add("OperatorId", OperatorId);
			if (!reportParams.ContainsKey("DepartmentType"))
				reportParams.Add("DepartmentType", ComputedDepartmentType);
			if (!reportParams.ContainsKey("Currency"))
				reportParams.Add("Currency", ((IWebPersonalServer)this).GetCurrency());
		}
		public List<ReportSubscription> GetSubscriptions()
		{
			using (var entities = new Entities())
			{
				var schedulerqueues = entities.SCHEDULERQUEUE
					.Include(SCHEDULERQUEUE.ContactIncludePath)
					.Include(SCHEDULERQUEUE.SchedulerEventIncludePath)
					.Include(SCHEDULERQUEUE.SchedulerEventIncludePath + "." + SCHEDULEREVENT.ReportIncludePath)
					.Where(sq =>
						sq.SCHEDULEREVENT.REPORT != null &&
						sq.Operator_ID == OperatorId)
					.ToList();

				var subscriptions = new List<ReportSubscription>(schedulerqueues.Count);

				foreach (var schedulerqueue in schedulerqueues)
				{
					var schedulerEventID    = schedulerqueue.SCHEDULERQUEUE_ID;
					var reportParametersXml = schedulerqueue.CONFIG_XML;
					var scheduleXml         = schedulerqueue.REPETITION_XML;

					var schedule    = (IRepetitionClass)SerializationHelper.FromXML(scheduleXml);
					var nearestTime = schedulerqueue.NEAREST_TIME;

					var reportParamsObj = SerializationHelper.FromXML(reportParametersXml);
					var monitoreeObjectContainer = reportParamsObj as IMonitoreeObjectContainer;
					var reportParams = reportParamsObj as ReportParameters;

					var schedulerEventRow = schedulerqueue.SCHEDULEREVENT;
					var reportName = schedulerEventRow.COMMENT;
					var reportGuid = schedulerEventRow.REPORT.REPORT_GUID;

					var report = Repository.Instance[reportGuid];
					if (report != null)
						reportName = report.GetReportName(CultureInfo);

					int? objectId;
					IdType? objectIdType;

					if (monitoreeObjectContainer != null)
					{
						try
						{
							objectId     = monitoreeObjectContainer.MonitoreeObjectId;
							objectIdType = monitoreeObjectContainer.MonitoreeObjectIdType;
						}
						catch (Exception e)
						{
							Trace.TraceWarning("{0}", e);
							objectId = null;
							objectIdType = null;
						}
					}
					else
					{
						objectId = null;
						objectIdType = null;
					}

					var subscription = new ReportSubscription
					{
						Id                  = schedulerqueue.SCHEDULERQUEUE_ID,
						Enabled             = schedulerqueue.Enabled,
						ObjectId            = objectId,
						ObjectIdType        = objectIdType,
						ScheduleDescription = schedule.ToString(),
						Name                = reportName,
						ScheduleEventId     = schedulerEventID,
						Schedule            = schedule,
						NearestTime         = nearestTime == DateTime.MaxValue
							? (int?)null
							: TimeHelper.GetSecondsFromBase(nearestTime),
					};

					subscription.EmailIds = schedulerqueue.Contact.Select(oc => oc.ID).ToList();
					subscription.Emails   = schedulerqueue.Contact.Select(oc => oc.Value).ToList();

					var operatorContacts = entities.Operator_Contact
						.Where(oc =>
							oc.Operator_ID == OperatorId &&
							subscription.EmailIds.Contains(oc.Contact_ID))
						.ToDictionary(oc => oc.Contact.Value, oc => oc.Name, StringComparer.OrdinalIgnoreCase);

					for (var i = 0; i != subscription.Emails.Count; ++i)
					{
						string emailName;
						if (!operatorContacts.TryGetValue(subscription.Emails[i], out emailName))
							continue;
						if (string.IsNullOrWhiteSpace(emailName))
							continue;
						subscription.Emails[i] = emailName + " <" + subscription.Emails[i] + ">";
					}

					if (reportParams != null)
					{
						subscription.ReportFrom = reportParams.DateFromInt;
						subscription.ReportTo = reportParams.DateToInt;
					}
					subscriptions.Add(subscription);
				}

				return subscriptions;
			}
		}
		/// <summary> Получение списка объектов доступных для мониторинга </summary>
		/// <returns></returns>
		protected IDictionary<int, IWebMobileInfoCP> GetAllowedVehicles()
		{
			IDictionary<int, IWebMobileInfoCP> result = new Dictionary<int, IWebMobileInfoCP>();

			DataSet ds = GetDataForWeb();

			foreach (DataRow row in ds.Tables["ALLOW_VEHICLE"].Rows)
			{
				int vId = Convert.ToInt32(row["VEHICLE_ID"]);
				WebMobileInfoCP newMobileInfo =
					new WebMobileInfoCP(
						vId,
						0,
						row["GARAGE_NUMBER"].ToString().Trim(),
						0,
						0,
						0,
						0,
						-181,
						-181,
						false,
						0,
						0,
						-181,
						-181,
						int.MinValue,
						int.MinValue
						);

				result.Add(vId, newMobileInfo);
			}

			return result;
		}
		/// <summary> Получение списка последних позиций для всех объектов доступных пользователю </summary>
		/// <returns></returns>
		public IDictionary<int, IWebMobileInfoCP> GetLastPositions()
		{
			var allVehicles = GetAllowedVehicles();
			if (allVehicles == null || allVehicles.Count == 0)
				return null;

			var uniques = allVehicles.Keys.ToArray();

			var positions = GetLastPositions(uniques);
			if (positions != null && positions.Count > 0)
			{
				foreach (var pos in positions.Values)
				{
					var vehicle = allVehicles[pos.Unique];
					pos.ObjectName = vehicle.ObjectName;
					allVehicles[pos.Unique] = pos;
				}
			}

			return allVehicles;
		}
		/// <summary> Получение списка последних позиций для заданных объектов </summary>
		/// <param name="uniques"></param>
		/// <returns></returns>
		public IDictionary<int, IWebMobileInfoCP> GetLastPositions(int[] uniques)
		{
			IDictionary<int, IMobilUnit> positions = serverInstance.GetLastPositions(uniques);
			IDictionary<int, IWebMobileInfoCP> result = new Dictionary<int, IWebMobileInfoCP>();

			foreach (KeyValuePair<int, IMobilUnit> pair in positions)
			{
				IMobilUnit unit = pair.Value;
				WebMobileInfoCP newMobileInfo =
					new WebMobileInfoCP(
						unit.Unique,
						unit.ID,
						0,
						unit.Time,
						unit.Speed,
						unit.Course ?? 0,
						unit.Latitude,
						unit.Longitude,
						unit.ValidPosition,
						(int)unit.Status,
						unit.CorrectTime,
						unit.Latitude,
						unit.Longitude,
						unit.VoltageAN4,
						unit.VoltageAN1);

				newMobileInfo.Radius = unit.Radius;

				result.Add(pair.Key, newMobileInfo);
			}

			FillMobileObjectInfo(result);

			return result;
		}
		public DataSet GetAddressesListFromDB(Guid mapGuid, string strFind)
		{
			throw new System.NotImplementedException();
		}
		IDictionary<int, IWebMobileInfoCP> IWebPersonalServer.GetLastPositions(int[] uniques)
		{
			return GetLastPositions(uniques);
		}

		#region Functions for Web Points

		public DataSet GetLinesWebForOperator(Guid mapGuid, bool vertexGet, int lineID)
		{
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@operator_id", OperatorId));
			arParams.Add(new ParamValue("@vertexGet", vertexGet));
			arParams.Add(new ParamValue("@line_id", lineID));

			DataSet ds = serverInstance.GetDataFromDB(arParams, "GetLinesWeb", new string[] { "LinesWeb" });

			return ds;
		}

		#endregion Functions for Web Points

		public int OperatorId
		{
			get { return SessionInfo.OperatorInfo.OperatorId; }
		}
		public void AddLineWebForOperator(Guid mapGuid, PointF[] points, string lineName, string description)
		{
			StringBuilder vertex = new StringBuilder();
			foreach (PointF p in points)
			{
				vertex.Append(p.X.ToString());
				vertex.Append(":");
				vertex.Append(p.Y.ToString());
				vertex.Append(";");
			}
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@vertex", vertex.ToString()));
			arParams.Add(new ParamValue("@name", lineName));
			arParams.Add(new ParamValue("@description", description));
			arParams.Add(new ParamValue("@operator_id", OperatorId));

			serverInstance.GetDataFromDB(arParams, "AddLinesWeb", null);
		}
		public void EdtLineWebForOperator(int lineID, PointF[] points, string lineName, string description)
		{
			StringBuilder vertex = new StringBuilder();
			foreach (PointF p in points)
			{
				vertex.Append(p.X.ToString());
				vertex.Append(":");
				vertex.Append(p.Y.ToString());
				vertex.Append(";");
			}
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@line_id", lineID));
			arParams.Add(new ParamValue("@vertex", vertex.ToString()));
			arParams.Add(new ParamValue("@name", lineName));
			arParams.Add(new ParamValue("@description", description));

			serverInstance.GetDataFromDB(arParams, "EditLinesWeb", null);
		}
		public void DelLineWebForOperator(int lineID)
		{
			ArrayList arParams = new ArrayList();
			//arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@point_id", lineID));

			serverInstance.GetDataFromDB(arParams, "DelPointsWeb", null);
		}

		#endregion IWebPersonalServer Members
		private bool IsVehicleAllowed(Entities entities, int vehicleId, SystemRight right)
		{
			var rightID = (int)right;
			// Есть ли права на доступ к машине
			return ((from ovr in entities.v_operator_vehicle_right
					 where ovr.operator_id == OperatorId &&
						   ovr.vehicle_id == vehicleId &&
						   ovr.right_id == rightID
					 select ovr).Count() != 0);
		}
		private bool IsVehicleGroupAllowed(Entities entities, int vehicleGroupID, SystemRight right)
		{
			var rightID = (int)right;
			// Есть ли права на доступ к группе
			return ((from ovgr in entities.v_operator_vehicle_groups_right
					 where ovgr.operator_id == OperatorId &&
						   ovgr.vehiclegroup_id == vehicleGroupID &&
						   ovgr.right_id == rightID
					 select ovgr).Count() != 0);
		}
		public IRepetitionClass GetMlpSchedule(IdType idType, int id)
		{
			using (var entities = new Entities())
			{
				var queue = entities.GetMlpSchedules(OperatorId, idType, id).FirstOrDefault();
				if (queue == null)
					return null;

				return (IRepetitionClass)SerializationHelper.FromXML(queue.REPETITION_XML);
			}
		}
		void IWebPersonalServer.SetScheduleEnabled(int id, bool value)
		{
			using (var entities = new Entities())
			{
				var task = entities.SCHEDULERQUEUE
								   .FirstOrDefault(sq => sq.SCHEDULERQUEUE_ID == id &&
														 sq.Operator_ID == OperatorId);
				if (task == null)
					throw new ArgumentOutOfRangeException("id", @"SCHEDULERQUEUE with id " + id + @" does not exists or Operator " + OperatorId + @" does not own it");

				task.Enabled = value;
				entities.SaveChangesBySession(SessionId);
			}
		}
		public object GetEntity(IdType idType, int id)
		{
			using (var entities = new Entities())
			{
				return entities.GetEntity(OperatorId, idType, id);
			}
		}
		void IWebPersonalServer.SetMlpSchedule(IdType idType, int id, IRepetitionClass repetition)
		{
			using (var entities = new Entities())
			{
				var target = entities.GetEntity(OperatorId, idType, id);
				var schedule = entities.GetMlpSchedules(OperatorId, idType, id).FirstOrDefault();
				if (repetition == null)
				{
					if (schedule != null)
					{
						entities.Delete(schedule);
						//отправить push-уведомление о прекращении слежения по расписанию
						SendPushNotificationAboutLbsScheduleChange(entities, idType, id, false);
					}
				}
				else
				{
					if (schedule == null)
					{
						schedule = new SCHEDULERQUEUE
						{
							CONFIG_XML = SerializationHelper.ToXML(
								new PARAMS
								{
									{ "idType",     idType     },
									{ "id",         id         },
									{ "operatorID", OperatorId },
								}),
							SCHEDULEREVENT =
							(
								from se in entities.SCHEDULEREVENT
								where se.SCHEDULEREVENT_ID == Entities.SchedulerEventPositionAsker
								select se
							)
								.FirstOrDefault(),
							VEHICLE = target as VEHICLE,
							VEHICLEGROUP = target as VEHICLEGROUP,
							Owner = entities.GetOperator(OperatorId)
						};
						entities.AddToSCHEDULERQUEUE(schedule);
						//отправить push-уведомление о начале слежения по расписанию
						SendPushNotificationAboutLbsScheduleChange(entities, idType, id, true);
					}
					schedule.Enabled = true;
					schedule.ErrorCount = 0;
					schedule.REPETITION_XML = SerializationHelper.ToXML(repetition);
					schedule.NEAREST_TIME = repetition.GetNextTimeInUTC(DateTime.UtcNow)
						?? DateTime.MaxValue;
					//MaxValue эквивалентно тому, что событие, для которого составлено расписание, не произойдет никогда.
					//При этом сохранять такие расписания возможно, поскольку составление расписания - трудоемкая операция для оператора
				}
				entities.SaveChangesBySession(SessionInfo.SessionId);
			}
		}
		private void SendPushNotificationAboutLbsScheduleChange(Entities entities, IdType idType, int id, bool scheduleAdded)
		{
			if (idType != IdType.Vehicle)
				return;
			if (!entities.IsPositionRequestVisible(OperatorId, id))
				return;
			var vehicleOperator =
				entities.GetOperatorByVehicleId(id)
					.Select(o => new { ID = o.OPERATOR_ID, CultureCode = o.Culture.Code })
					.FirstOrDefault();
			if (vehicleOperator == null)
				return;

			var cloudContacts = entities.GetCloudContactsForOperator(vehicleOperator.ID);
			if (cloudContacts.Count == 0)
				return;
			var friendName = Server.Instance().GetFriendName(vehicleOperator.ID, OperatorId);
			if (string.IsNullOrWhiteSpace(friendName))
				return;

			var vehicleOperatorCulture =
				vehicleOperator.CultureCode != null
				? System.Globalization.CultureInfo.GetCultureInfo(vehicleOperator.CultureCode)
				: System.Globalization.CultureInfo.InvariantCulture;
			var text = ResourceContainers.Get(vehicleOperatorCulture)[scheduleAdded ? "youAreScheduledFromNow" : "youAreNotScheduledNoMore"];
			var blank = new MESSAGE.Blank(friendName, text);
			var operatorMsisdn = GetMsisdn();

			foreach (var cloudContact in cloudContacts)
			{
				var message = entities.CreateAppNotification(cloudContact, null, null);
				message.Fill(blank);
				if (!string.IsNullOrWhiteSpace(operatorMsisdn))
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, operatorMsisdn);
			}
		}
		public List<Dto::Command> GetActualCommands(int[] vehicleIDs)
		{
			using (var entities = new Entities())
			{
				var commands = (from command in entities.Command
								where command.Sender_ID == OperatorId
								select command);


				if (vehicleIDs != null)
					commands = commands.Where(c => vehicleIDs.Contains(c.Target_ID));

				var commandIDs =
					(from command in commands
					 group new { command.ID, command.Date_Received } by new { command.Target_ID, command.Type_ID }
						 into g
					 select g.OrderByDescending(item => item.Date_Received).FirstOrDefault()).ToList().ConvertAll(item => item.ID);

				commands =
				(
					from command in entities.Command
						.Include(Orm.Command.Command_ParameterIncludePath)
					select command
				)
					.Where(c => commandIDs.Contains(c.ID));

				return commands
					.ToList()
					.ConvertAll(Orm::Command.ToDto);
			}
		}
		/// <summary>
		/// Получение списка персональных объектов операторов, которые могут видеть текущего оператора
		/// (вместо операторов возвращаются vehicle устройств представляющих этих операторов)
		/// </summary>
		/// <returns>Список операторов</returns>
		List<ShortVehicleInfo> IWebPersonalServer.WhoCanSeeMe()
		{
			const int vehicleAccessRightID = (int)SystemRight.VehicleAccess;

			using (var entities = new Entities())
			{
				//Ищем объект представляющий собой текущего оператора (asid.operator или (controller_type = 'SYGIC' и права админа на устройство))
				var operPersonalVehicle = entities.GetOperatorPersonalVehicle(OperatorId);

				if (operPersonalVehicle == null)
					return null;

				int operId = OperatorId;
				int operPersonalVehicleId = operPersonalVehicle.VEHICLE_ID;

				List<OPERATOR> opers;
				//Ищем все личные объекты операторов имеющих права на объект представляющий собой текущего оператора
				if (operPersonalVehicle.CONTROLLER.MLP_Controller != null && operPersonalVehicle.CONTROLLER.MLP_Controller.Asid != null)
				{
					//для MLP - это копия объекта с тем же asid-ом
					opers = (from ovr in entities.v_operator_vehicle_right
							 where
							  ovr.right_id == vehicleAccessRightID
							  && ovr.operator_id != operId
							  && ovr.VEHICLE.CONTROLLER.MLP_Controller.Asid.ID == operPersonalVehicle.CONTROLLER.MLP_Controller.Asid.ID
							 select ovr.OPERATOR).ToList();
				}
				else
				{
					opers = (from ovr in entities.v_operator_vehicle_right
							 where
							  ovr.VEHICLE.VEHICLE_ID == operPersonalVehicleId
							  && ovr.right_id == vehicleAccessRightID
							  && ovr.operator_id != operId
							  && ovr.VEHICLE.CONTROLLER.CONTROLLER_TYPE.TYPE_NAME == "Sygic"
							 select ovr.OPERATOR).ToList();
				}

				var objectList = new List<ShortVehicleInfo>();

				if (opers.Count > 0)
				{
					foreach (var oper in opers)
					{
						var operVehicle = entities.GetOperatorPersonalVehicle(oper.OPERATOR_ID);
						if (operVehicle != null)
						{
							objectList.Add(new ShortVehicleInfo { id = operVehicle.VEHICLE_ID, gNum = oper.NAME });
						}
					}
				}

				return objectList;
			}
		}
		/// <summary> Возвращает список объектов мониторинга по праву пользователя и по типу контроллера </summary>
		/// <param name="right"></param>
		/// <param name="controllerTypeName"></param>
		/// <returns></returns>
		/// <remarks>Заполняются только unique = vehicleID и ObjectName = vehicle.garageNumber</remarks>
		List<IWebMobileInfo> IWebPersonalServer.GetVehiclesByRightsAndControllerType(SystemRight right, string controllerTypeName)
		{
			using (var entities = new Entities())
			{
				var vehicles = entities.GetVehiclesByRight(OperatorId, right);
				var resVehicles = (from v in vehicles
								   where v.CONTROLLER.CONTROLLER_TYPE.TYPE_NAME == controllerTypeName
								   select v);

				if (resVehicles.Count() == 0)
					return null;

				var res = new List<IWebMobileInfo>();
				foreach (var vehicle in resVehicles)
				{
					var mInfo = new WebMobileInfo(vehicle.VEHICLE_ID, 0, vehicle.GARAGE_NUMBER, 0, 0, 0, 0, 0, 0, 0,
												  false, 0, 0, 0);
					res.Add(mInfo);
				}

				return res;

			}
		}
		/// <summary> Возвращает список системных прав пользователя </summary>
		public List<SystemRight> GetOperatorSystemRights(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities.v_operator_rights
					.Where(o => o.operator_id == operatorId)
					.Select(r => (SystemRight)r.right_id)
					.ToList();
			}
		}
		Rht::Operator IWebPersonalServer.GetOperatorRights(int operatorId)
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new SecurityException("Department is not assigned");

			// Проверяем права текущего оператора на изменение оператора
			var departments = ((IWebPersonalServer)this).GetAvailableDepartments();
			if (departments == null || departments.FindAll(department => department.id == departmentId.Value).Count == 0)
				throw new SecurityException("No admin rights for department");

			var rights = new[]
			{
				(int)SystemRight.SecurityAdministration,
				(int)SystemRight.VehicleAccess,
				(int)SystemRight.PathAccess,
				(int)SystemRight.ReportsAccess,
				(int)SystemRight.AddingTracker,
				//(int)SystemRight.CalcArrivalTime,
			};
			var result = GetOperatorDetails(operatorId);
			using (var entities = new Entities())
			{
				var baseOperatorRights =
					// ServiceManagement можно видеть только супер-админам, т.к. только он может назначать пользователя партнером
					from rr in entities.RIGHT
						//.Where(r => IsSuperAdministrator || r.RIGHT_ID != (int)SystemRight.ServiceManagement)
						.Where(r => IsSuperAdministrator || rights.Contains(r.RIGHT_ID))
					from ro in entities.RIGHT_OPERATOR
						.Where(x => x.OPERATOR_ID == operatorId && x.RIGHT_ID == rr.RIGHT_ID)
						.DefaultIfEmpty()
					select new Rht::Right
					{
						right_id = rr.RIGHT_ID,
						right = (SystemRight)rr.RIGHT_ID,
						allow = (bool?)ro.ALLOWED
					};
				result.rights = baseOperatorRights.ToArray();
			}
			return result;
		}
		Rht::Operator IWebPersonalServer.GetOperatorZoneRights(int operatorId)
		{
			var result = new Rht::Operator { id = operatorId };
			using (var entities = new Entities())
			{
				// Получение текущего администратора.
				var adminId = OperatorId;

				// Получение запроса на список всех геозон администратора.

				var zoneQuery = ComputedDepartmentId != null
					? entities.GEO_ZONE.Where(gz => gz.Department_ID == ComputedDepartmentId.Value)
					: entities.v_operator_zone_right.Where(
						ozr => ozr.operator_id == adminId &&
							   ozr.right_id == (int)SystemRight.SecurityAdministration)
						.Select(ozr => ozr.GEO_ZONE);

				var zonesList = zoneQuery
					.Select(z => new
					{
						ZoneId = z.ZONE_ID,
						Name = z.NAME,
						Description = z.DESCRIPTION,
						Color = z.COLOR
					})
					.Distinct()
					.ToList();

				// Получение запроса на список всех групп геозон администратора.
				var zoneGroupQuery = ComputedDepartmentId != null
					? entities.ZONEGROUP.Where(zg => zg.Department_ID == ComputedDepartmentId.Value)
					: entities.v_operator_zone_groups_right.Where(
						ozr => ozr.operator_id == adminId &&
							   ozr.right_id == (int)SystemRight.GroupAdministration)
						.Select(ozgr => ozgr.ZONEGROUP);

				var zoneGroupsList = zoneGroupQuery
					.Select(zg => new
					{
						ZoneGroupId = zg.ZONEGROUP_ID,
						Name = zg.NAME,
						Description = zg.DESCRIPTION
					})
					.Distinct()
					.ToList();

				// Получение списка прав на геозоны целевого оператора, исходя из эталонного списка администратора.
				var zoneRightsListTemp = entities.OPERATOR_ZONE
					.Where(x => x.OPERATOR_ID == operatorId)
					.ToList();
				var zoneRightsList =
				(
					from zon in zonesList
					from r in zoneRightsListTemp
						.Where(x => x.ZONE_ID == zon.ZoneId)
						.DefaultIfEmpty()
					select new
					{
						zon.ZoneId,
						RightId = r == null ? (int?)null : r.RIGHT_ID,
						Allowed = r == null ? (bool?)null : r.ALLOWED,
					}
				)
					.ToList();

				// Получение списка прав на группы геозон целевого оператора, исходя из
				// эталонного списка групп геозон администратора.
				var zoneGroupsRightsListTemp =
					entities.OPERATOR_ZONEGROUP.Where(x => x.OPERATOR_ID == operatorId).ToList();
				var zoneGroupsRightsList =
				(
					from zg in zoneGroupsList
					from r in zoneGroupsRightsListTemp
						.Where(x => x.ZONEGROUP_ID == zg.ZoneGroupId)
						.DefaultIfEmpty()
					select new
					{
						zg.ZoneGroupId,
						RightId = r == null ? (int?)null : r.RIGHT_ID,
						Allowed = r == null ? (bool?)null : r.ALLOWED,
					}
				)
					.ToList();

				// Добавляем JSON бизнес объекты для геозон.
				result.geozones = zonesList
					.Select(x => new Rht::Geozone
					{
						id          = x.ZoneId,
						name        = x.Name,
						description = x.Description,
						color       = x.Color
					})
					.ToArray();

				// Наделяем списком прав JSON объекты геозон.
				foreach (var z in result.geozones)
					z.rights = zoneRightsList
						.Where(x => x.ZoneId == z.id && x.RightId.HasValue)
						.Select(x => new Rht::Right
						{
							right_id = (int)x.RightId,
							allow = (bool)x.Allowed,
							right = (SystemRight)x.RightId
						})
						.ToArray();

				// Добавляем JSON бизнес объекты для групп геозон.
				result.geozonegroups = zoneGroupsList.Select(x => new Rht::GeozoneGroup
				{
					id          = x.ZoneGroupId,
					name        = x.Name,
					description = x.Description
				}).ToArray();

				var zoneGroupToZone = zoneGroupQuery
					.SelectMany(zg => zg.Members)
					.ToLookup(m => m.ZONEGROUP_ID, m => m.ZONE_ID);

				// Наделяем списком прав JSON объекты групп геозон и создаем списки заивисмых геозон.
				foreach (var zg in result.geozonegroups)
				{
					// Собственно, права.
					zg.rights =
						zoneGroupsRightsList.Where(x => x.ZoneGroupId == zg.id && x.RightId.HasValue).Select(
							x => new Rht::Right
							{
								right_id = (int)x.RightId,
								allow = (bool)x.Allowed,
								right = (SystemRight)x.RightId
							}).ToArray();

					// Зависимые геозоны.
					zg.geozoneIds = zoneGroupToZone[zg.id].ToArray();
				}

				//Группа "Прочие" (Не в группе)
				var notInGroupZoneIds = new HashSet<int>(zonesList.Select(z => z.ZoneId));
				foreach (var zg in result.geozonegroups)
				{
					foreach (var zoneId in zg.geozoneIds)
						notInGroupZoneIds.Remove(zoneId);
				}

				if (notInGroupZoneIds.Count != 0)
				{
					var strings = ResourceContainers.Get(CultureInfo);
					// Добавление группу ТС "Не в группе", для управления правами ТС без привязки к определенной группе.
					Array.Resize(ref result.geozonegroups, result.geozonegroups.Length + 1);
					result.geozonegroups[result.geozonegroups.Length - 1] = new Rht::GeozoneGroup
					{
						id          = -1,
						name        = strings["NotInGroup"],
						description = strings["NotInGroupDescription"],
						geozoneIds  = notInGroupZoneIds.ToArray()
					};
				}
			}

			return result;
		}
		public void ChangeOperatorZoneRights(Rht::Operator oper)
		{
			var departmentId = ComputedDepartmentId;
			if (departmentId == null)
				throw new SecurityException("Department is not assigned");

			// Проверяем права текущего оператора на изменение оператора
			var departments = ((IWebPersonalServer)this).GetAvailableDepartments();
			if (departments == null || departments.FindAll(department => department.id == departmentId.Value).Count == 0)
				throw new SecurityException("No admin rights for department");

			using (var entities = new Entities())
			{
				//Меняем права на геозоны.
				if (oper.geozones != null && oper.geozones.Length > 0)
				{
					foreach (var geozone in oper.geozones)
						foreach (var right in ZoneRights)
						{
							var current = entities.OPERATOR_ZONE.FirstOrDefault(
								x => x.OPERATOR_ID == oper.id &&
									 x.ZONE_ID == geozone.id &&
									 x.RIGHT_ID == (int)right);

							var existingRight = geozone.rights.FirstOrDefault(x => x.right_id == (int)right);
							if (existingRight != null)
							{
								if (current != null)
									current.ALLOWED = (bool)existingRight.allow;
								else
									entities.OPERATOR_ZONE.AddObject(new OPERATOR_ZONE
									{
										OPERATOR_ID = oper.id,
										ZONE_ID = geozone.id,
										RIGHT_ID = (int)right,
										ALLOWED = (bool)existingRight.allow,
									});
							}
							else
								if (current != null)
								entities.OPERATOR_ZONE.DeleteObject(current);
						}
				}

				// Меняем права на группы геозон.
				if (oper.geozonegroups != null && oper.geozonegroups.Length > 0)
				{
					foreach (var geozonegroup in oper.geozonegroups)
						foreach (var right in ZoneRights)
						{
							var current = entities.OPERATOR_ZONEGROUP.FirstOrDefault(
								x => x.OPERATOR_ID == oper.id &&
									 x.ZONEGROUP_ID == geozonegroup.id &&
									 x.RIGHT_ID == (int)right);

							var existingRight = geozonegroup.rights?.FirstOrDefault(x => x.right_id == (int)right);
							if (existingRight != null)
							{
								if (current != null)
									current.ALLOWED = (bool)existingRight.allow;
								else
									entities.OPERATOR_ZONEGROUP.AddObject(new OPERATOR_ZONEGROUP
									{
										OPERATOR_ID = oper.id,
										ZONEGROUP_ID = geozonegroup.id,
										RIGHT_ID = (int)right,
										ALLOWED = (bool)existingRight.allow,
									});
							}
							else
								if (current != null)
								entities.OPERATOR_ZONEGROUP.DeleteObject(current);

						}
				}
				entities.SaveChangesBySession(SessionId);
			}
		}
		/// <summary> Возвращает набор прав для текущего оператора </summary>
		/// <param name="operatorId"> Идентификатор оператора </param>
		/// <returns></returns>
		public Rht::Operator GetOperatorVehicleRights(int? operatorId)
		{
			// Если нет значения входного параметра, то возвращаем права текущего оператора
			if (operatorId == null)
				operatorId = OperatorId;

			var operatorSrcId = OperatorId;
			var operatorDstId = operatorId.Value;

			// Возвратим этот объект, и возможно что-то заполним в процессе метода
			var result = new Rht::Operator() { id = operatorDstId };
			using (var entities = new Entities())
			{
				// Получить права на объекты текущего пользователя
				var colRights = GetReplacedAccessOnVehicle();
				// Получить права текущего пользователя и пользователя из параметра
				var allVehRights = entities.v_operator_vehicle_right
					.Where(ovr =>
						(
							ovr.operator_id == operatorSrcId ||
							ovr.operator_id == operatorDstId
						)
						&&
						// Дополнительный фильтр
						(
							// Фильтровать по машинам только этого департамента
							!FilterByDepartment || ovr.VEHICLE.Department_ID == ComputedDepartmentId
						)
					)
					.Select(r => new { r.operator_id, r.vehicle_id, r.right_id, r.priority })
					.ToArray();
				// Получить собственные объекты текущего пользователя
				var srcVehIds = allVehRights
					.Where(r => r.operator_id == operatorSrcId && r.right_id == (int)SystemRight.SecurityAdministration)
					.Select(r => r.vehicle_id)
					.Distinct()
					.ToArray();
				// Получить права на собственные объекты текущего пользователя
				var srcVehRights = allVehRights
					.Where(r => r.operator_id == operatorSrcId && srcVehIds.Contains(r.vehicle_id))
					.Select(r => new { r.vehicle_id, r.right_id, r.priority })
					.ToArray();
				// Получить права на собственные объекты текущего пользователя для пользователя из параметра
				var dstVehRights = allVehRights
					.Where(r => r.operator_id == operatorDstId && srcVehIds.Contains(r.vehicle_id) && r.priority == 1)
					.Select(r => new { r.vehicle_id, r.right_id, r.priority })
					.ToArray();
				var veh = srcVehRights.GroupJoin(dstVehRights,
					l => new { l.vehicle_id, l.right_id },
					r => new { r.vehicle_id, r.right_id },
					(l, r) => new { l, r })
					.SelectMany(
						o => o.r.DefaultIfEmpty(),
			 			(l, r) => new
						{
							vehid = l.l.vehicle_id,
							right = new Rht::Right
							{
								right_id = l.l.right_id,
								right    = (SystemRight)l.l.right_id,
								allow    = r != null
							}
						})
					.Where(r => colRights.Contains(r.right.right.Value));
				// Получить права текущего пользователя и пользователя из параметра
				var allGrpRights = entities.v_operator_vehicle_groups_right
					.Where(ovr =>
						(
							ovr.operator_id == operatorSrcId ||
							ovr.operator_id == operatorDstId
						)
						&&
						// Дополнительный фильтр
						(
							// Фильтровать по машинам только этого департамента
							!FilterByDepartment || ovr.VEHICLEGROUP.Department_ID == ComputedDepartmentId
						)
					)
					.Select(r => new { r.operator_id, r.vehiclegroup_id, r.right_id, r.priority })
					.ToArray();
				// Получить идентификаторы собственных групп текущего пользователя
				var srcGrpIds = allGrpRights
					.Where(r => r.operator_id == operatorSrcId && r.right_id == (int)SystemRight.GroupAdministration)
					.Select(r => r.vehiclegroup_id)
					.Distinct()
					.ToArray();
				// Получить права на группы объектов текущего пользователя
				var srcGrpRights = allGrpRights
					.Where(r => r.operator_id == operatorSrcId && srcGrpIds.Contains(r.vehiclegroup_id))
					.Select(r => new { r.vehiclegroup_id, r.right_id, r.priority })
					.ToArray();
				// Получить права на собственные группы объектов текущего пользователя для пользователя из параметра
				var dstGrpRights = allGrpRights
					.Where(r => r.operator_id == operatorDstId && srcGrpIds.Contains(r.vehiclegroup_id) && r.priority == 1)
					.Select(r => new { r.vehiclegroup_id, r.right_id, r.priority })
					.ToArray();
				var grp = srcGrpRights.GroupJoin(dstGrpRights,
					l => new { l.vehiclegroup_id, l.right_id },
					r => new { r.vehiclegroup_id, r.right_id },
					(l, r) => new { l, r })
					.SelectMany(
						o => o.r.DefaultIfEmpty(),
			 			(l, r) => new
						{
							grpid = l.l.vehiclegroup_id,
							right = new Rht::Right
							{
								right_id = l.l.right_id,
								right    = (SystemRight)l.l.right_id,
								allow    = r != null
							}
						})
					.Where(r => colRights.Contains(r.right.right.Value));
				// Запрашиваем параметры объектов наблюдения собственных для текущего оператора
				var getVehiclesResult = Server.Instance()
					.GetVehiclesWithPositions(OperatorId, Department, new GetVehiclesArgs
					{
						VehicleIDs = srcVehIds,
						Culture = CultureInfo,
					});
				// Добавляем в результирующий объект набор объектов наблюдения и прав на них
				result.vehicles = getVehiclesResult
					.Select(v => new Rht::Vehicle
					{
						// Параметры объекта наблюдения
						id      = v.id,
						gNum    = v.Name,
						iconUrl = string.Empty,
						isMLP   = v.allowMlpRequest ?? false,
						// Права
						rights  = veh
							.Where(r => r.vehid == v.id)
							.Select(r => r.right)
							.ToArray(),
					})
					.ToArray();
				// Добавляем в результирующий объект набор групп объектов наблюдения и прав на них
				result.groups = entities.VEHICLEGROUP
					.Where(g => srcGrpIds.Contains(g.VEHICLEGROUP_ID))
					.OrderBy(g => g.NAME)
					.ToList()
					.Select(g => new Rht::VehicleGroup
					{
						// Параметры группы
						id          = g.VEHICLEGROUP_ID,
						name        = g.NAME,
						description = g.COMMENT,
						// Состав группы
						vehicleIds  = g.VEHICLE
							.Select(v => v.VEHICLE_ID)
							.ToArray(),
						// Права
						rights      = grp
							.Where(r => r.grpid == g.VEHICLEGROUP_ID)
							.Select(r => r.right)
							.ToArray()
					})
					.ToArray();
				// Определяем объекты не входящие в состав ни одной из групп
				var notInGroupsVehicles = srcVehIds.Except(result.groups.SelectMany(g => g.vehicleIds).Distinct());
				var stringsx = ResourceContainers.Get(CultureInfo);
				// Добавление группу ТС "Не в группе", для управления правами ТС без привязки к определенной группе.
				Array.Resize(ref result.groups, result.groups.Length + 1);
				result.groups[result.groups.Length - 1] = new Rht::VehicleGroup
				{
					id          = -1,
					name        = stringsx["NotInGroup"],
					description = stringsx["NotInGroupDescription"],
					vehicleIds  = notInGroupsVehicles.ToArray(),
					vehicles    = result.vehicles
						.Where(v => notInGroupsVehicles.Contains(v.id))
						.ToArray()
				};
			}
			return result;
		}
		List<Vehicle> IWebPersonalServer.GetVehicles(GetVehiclesArgs arguments)
		{
			if (arguments == null)
				arguments = new GetVehiclesArgs();

			if (arguments.FilterByDepartment == null)
				arguments.FilterByDepartment = FilterByDepartment;
			arguments.Culture = CultureInfo;
			var basicServer = (IBasicFunctionSet)Server.Instance();
			var vehicles = basicServer.GetVehiclesWithPositions(OperatorId, Department, arguments);

			foreach (var vehicle in vehicles)
			{
				vehicle.iconUrl = Server.Instance().GetVehicleIcon(vehicle.iconUrl, vehicle.vehicleKind);

				if (!IsPhysicalCustomer())
					continue;

				if (vehicle.commands == null)
					continue;
				var askPositionCommand = vehicle.commands
					.FirstOrDefault(c => c.Type == CmdType.AskPosition);
				if (askPositionCommand == null)
					continue;
				if (askPositionCommand.PayerVehicleID == null)
					continue;
				askPositionCommand.PayerVehicleID = null;
			}

			if (IsSuperAdministrator && arguments.IncludeLastData)
			{
				foreach (var vehicle in vehicles)
				{
					if (vehicle.logTime == null)
						continue;
					var mu = Server.Instance().GetLastPosition(vehicle.id);
					if (mu == null || mu.IP == null || mu.IP.Address == null)
						continue;
					vehicle.ipAddress = mu.IP.Address.ToString();
				}
			}
			return vehicles;
		}
		public List<Dto::VehicleControlDate> GetVehicleControlDates(int vehicleId)
		{
			List<Dto::VehicleControlDate> result;

			using (var context = new Entities())
			{
				result = context.VehicleControlDate
					.Where(d => d.Vehicle_ID == vehicleId)
					.OrderByDescending(d => d.Value)
					.Select(d => new Dto::VehicleControlDate
					{
						TypeId = (VehicleControlDateTypes)d.Type_ID,
						Value  = d.Value
					})
					.ToList();
			}
			return result;
		}
		/// <summary> Возвращает список групп геозон, доступных оператору </summary>
		/// <param name="getGroupsArguments"></param>
		Dto::Group[] IWebPersonalServer.GetGeoZoneGroups(GetGroupsArguments getGroupsArguments)
		{
			using (var entities = new Entities())
			{
				const int accessRightID = (int)SystemRight.ZoneAccess;
				var accessibleZoneGroupIDs =
				(
					from ozgr in entities.v_operator_zone_groups_right
					where
						ozgr.operator_id == OperatorId &&
						ozgr.right_id == accessRightID
					select ozgr.zonegroup_id
				)
					.ToArray();

				var zoneGroupsQuery =
				(
					from zg in entities.ZONEGROUP
						.Include(ZONEGROUP.MembersIncludePath)
						.Include(ZONEGROUP.MembersIncludePath + "." + ZONEGROUP_ZONE.ZoneIncludePath)
					select zg
				)
					.Where(zg => accessibleZoneGroupIDs.Contains(zg.ZONEGROUP_ID));

				if (FilterByDepartment)
					zoneGroupsQuery = zoneGroupsQuery
						.Where(zg => zg.Department_ID == ComputedDepartmentId.Value);

				if (getGroupsArguments == null || !getGroupsArguments.IncludeIgnored)
				{
					zoneGroupsQuery = zoneGroupsQuery
						.Where(vg =>
							!vg.OPERATOR_ZONEGROUP
								.Any(a =>
									a.OPERATOR_ID == OperatorId &&
									a.RIGHT_ID == (int)SystemRight.Ignore && a.ALLOWED));
				}

				var zoneGroups =
				(
					from zg in zoneGroupsQuery.ToList()
					select new Group
					{
						Id = zg.ZONEGROUP_ID,
						Name = zg.NAME,
						Description = zg.DESCRIPTION,
						ObjectIds = zg.Members
							.OrderBy(m => m.Zone.NAME)
							.ToList()
							.ConvertAll(zgz => zgz.Zone.ZONE_ID)
							.ToArray()
					}
				)
					.ToArray();

				const int editGroupRightID = (int)SystemRight.EditGroup;
				const int addGroupRightID = (int)SystemRight.AddZoneToGroup;

				var editableZoneGroups =
					(from ozgr in entities.v_operator_zone_groups_right
					 where ozgr.operator_id == OperatorId &&
						   ozgr.right_id == editGroupRightID
					 select ozgr.zonegroup_id).ToDictionary(i => i);

				var addableZoneGroups =
					(from ozgr in entities.v_operator_zone_groups_right
					 where ozgr.operator_id == OperatorId &&
						   ozgr.right_id == addGroupRightID
					 select ozgr.zonegroup_id).ToDictionary(i => i);

				foreach (var zoneGroup in zoneGroups)
				{
					if (editableZoneGroups.ContainsKey(zoneGroup.Id))
						zoneGroup.AllowEditContent = true;
					if (addableZoneGroups.ContainsKey(zoneGroup.Id))
						zoneGroup.AllowAddToGroup = true;
				}

				return zoneGroups;
			}
		}
		History IWebPersonalServer.GetHistory(int vehicleId, int logTimeFrom, int logTimeTo, FullLogOptions options)
		{
			if (options == null)
				options = new FullLogOptions();

			var server = Server.Instance();
			if (!server.IsAllowedVehicleAll(OperatorId, vehicleId, SystemRight.PathAccess))
				throw new SecurityException("No PathAccess right");

			var args = new GetVehiclesArgs
			{
				FilterByDepartment = false,
				VehicleIDs = new[] { vehicleId },
				Culture = CultureInfo
			};
			var vehicle = server
				.GetVehiclesWithPositions(OperatorId, Department, args)
				.FirstOrDefault();

			if (vehicle == null)
				return null; //no such vehicle or no rights

			// TODO: Проверка наличия ограничений глубины Пути ## Переделать, сейчас отключить
			/*
			if (Department == null || Department.Type != DepartmentType.Corporate)
			{
				var billingServices = GetBillingServices();
				var maxLogDepthHours = billingServices.Max(bs => bs.MaxLogDepthHours);
				if (maxLogDepthHours != null)
				{
					var currentLogTime = TimeHelper.GetSecondsFromBase();
					if (logTimeFrom < currentLogTime - maxLogDepthHours.Value*3600)
						logTimeFrom = currentLogTime - maxLogDepthHours.Value*3600;
					if (logTimeTo < currentLogTime - maxLogDepthHours.Value*3600)
						logTimeTo = currentLogTime - maxLogDepthHours.Value*3600;
				}
			}
			*/
			// Получаем лог
			var fullLog = server
				.GetFullLog(OperatorId, vehicleId, logTimeFrom, logTimeTo, options, CultureInfo)
				.Filter();
			// Получаем и обновляем группы истории
			var groups = fullLog
				.GetHistoryGroups()
				.UpdHistoryGroups(vehicle);
			// Получаем и обновляем статистику истории
			var statistics = fullLog.GetHistoryStatistics(groups);
			// Очищаем максимальную скорость, если не поддерживается для объекта наблюдения
			if (!vehicle.Supports(DeviceCapability.SpeedMonitoring))
				statistics.maxSpeed = default;

			////////////////////////////////////////////////////////////
			// Рассчитываем заправки и сливы
			using (var entities = new Entities())
			{
				var vehicleOrm = entities.GetVehicle(vehicle.id);
				var vehicleFuelTankVolume = !vehicleOrm.FUEL_TANK.HasValue || vehicleOrm.FUEL_TANK == 0 ? 1000 : vehicleOrm.FUEL_TANK.Value;
				var vehicleFuelSpendStandardKilometer = !vehicleOrm.FuelSpendStandardKilometer.HasValue || vehicleOrm.FuelSpendStandardKilometer.Value <= 0
					? Convert.ToInt32(entities.CONSTANTS.FirstOrDefault(c => c.NAME == "baseFuelSpendStandardKilometer").VALUE)
					: vehicleOrm.FuelSpendStandardKilometer.Value;
				var vehicleFuelSpendStandardLiter = !vehicleOrm.FuelSpendStandardLiter.HasValue || vehicleOrm.FuelSpendStandardLiter.Value <= 0
					? Convert.ToInt32(entities.CONSTANTS.FirstOrDefault(c => c.NAME == "baseFuelSpendStandardLiter").VALUE)
					: vehicleOrm.FuelSpendStandardLiter.Value;
				var vehicleFuelSpendStandardByMH = entities.GetRuleValue(vehicleOrm.VEHICLE_ID, RuleNumber.FuelSpendStandardPerMH);
				fullLog.FillRefuelsAndDrains(
					vehicleOrm.VEHICLE_ID,
					vehicleFuelTankVolume,
					vehicleFuelSpendStandardKilometer,
					vehicleFuelSpendStandardLiter,
					vehicleFuelSpendStandardByMH);
			}
			////////////////////////////////////////////////////////////
			// Добавляем сообщения оператору по данной машине
			var messages = GetMessagesForOperator(
				null,
				logTimeFrom.ToUtcDateTime(),
				logTimeTo.ToUtcDateTime(),
				new Pagination { Page = 1, RecordsPerPage = 10000 },
				new[] { vehicleId })
				.Select(m => new MessageFormat(m))
				.ToList();

			// Обрабатываем адреса
			var addresses = default(Dictionary<int, string>);
			if (options.IncludeAddresses)
			{
				var onlineGeocoding4All = Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All);

				var allLogTimes = Enumerable.Empty<int>()
					.Union(groups
						?.SelectMany(g => g.GetFromAndToLogTimes()) ?? Enumerable.Empty<int>())
					.Union(messages
						?.Select(m => m.Time.ToLogTime()) ?? Enumerable.Empty<int>())
					.Union(fullLog.Refuels
						?.Select(m => m.From.LogTime) ?? Enumerable.Empty<int>())
					.Union(fullLog.Drains
						?.Select(m => m.From.LogTime) ?? Enumerable.Empty<int>());

				var addressRequests = allLogTimes
					//.Distnct() Не нужен, т.к. Union убирает дубликаты в отличие от Concat
					// TODO: Вместо Single использовать HasFrom ?
					.Where(lt => fullLog.Geo.BinarySearch(x => lt - x.LogTime).Single)
					.Select(lt =>
					{
						var geo = fullLog.Geo[fullLog.Geo.BinarySearch(x => lt - x.LogTime).From];
						return new AddressRequest(
							id: lt,
							lat: (double)geo.Lat,
							lng: (double)geo.Lng,
							!((vehicle.IsOnlineGeocoding ?? false) || onlineGeocoding4All));
					})
					.ToArray();

				try
				{
					if (addressRequests.Length != 0)
					{
						var geoClient = GeoClient.Default;
						if (geoClient != null)
						{
							addresses = geoClient.GetAddresses(addressRequests, CultureInfo)
								.ToDictionary(a => a.Id, a => a.Address);
						}
					}
				}
				catch (Exception exception)
				{
					Trace.TraceInformation(exception.ToString());
					addresses = null;
				}
			}

			// Возвращаем историю
			return new History(vehicle)
			{
				statistics = statistics,
				log        = fullLog,
				groups     = groups,
				Addresses  = addresses,
				Messages   = messages
			};
		}
		/// <summary> Возвращает список доступных типов топлива </summary>
		List<FuelType> IWebPersonalServer.GetFuelTypes()
		{
			using (var entities = new Entities())
			{
				// Определяем текущий департамент (пока первый доступный оператору)

				var department = entities.DEPARTMENT
					.FirstOrDefault(d => d.DEPARTMENT_ID == ComputedDepartmentId);
				if (department != null)
				{
					if (!department.CountryReference.IsLoaded)
						department.CountryReference.Load();
					if (department.Country != null)
					{
						if (!department.Country.Fuel_Type.IsLoaded)
							department.Country.Fuel_Type.Load();
					}
				}

				//Берем типы топлива для страны, если для департамента указана страна, в противном случае берем типы топлива без привязки к стране
				var fuelTypes = department != null && department.Country != null
					? (IEnumerable<Fuel_Type>)department.Country.Fuel_Type
					: (from ft in entities.Fuel_Type where ft.Country == null select ft);

				return fuelTypes.Select(ft => new FuelType
				{
					ID = ft.ID,
					Name = ft.Name,
					Unit = (UnitOfMeasure)ft.Unit_Id,
					TankUnit = (UnitOfMeasure)ft.Tank_Unit_Id
				}).ToList();
			}
		}
		string IWebPersonalServer.GetCurrency()
		{
			using (var entities = new Entities())
				return entities.GetAvailableDepartmentsByOperatorIdQuery(OperatorId)
					.Select(d => d.Country.Currency)
					.FirstOrDefault() ?? "RUR";
		}
		Picture IWebPersonalServer.GetPicture(int vehicleID, int logTime)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedAll(entities.GetOperator(OperatorId), entities.GetVehicle(vehicleID), SystemRight.VehicleAccess))
					return null;
			}

			return Server.Instance().GetPicture(vehicleID, logTime);
		}
		byte[] IWebPersonalServer.GetThumbnailBytes(int vehicleID, int logTime, int? width, int? height)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedAll(entities.GetOperator(OperatorId), entities.GetVehicle(vehicleID), SystemRight.VehicleAccess))
					return null;
			}

			return Server.Instance().GetPictureThumbnail(vehicleID, logTime, width, height);
		}
		List<int> IWebPersonalServer.GetNearestPictures(int vehicleId, int logTime)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedAll(entities.GetOperator(OperatorId), entities.GetVehicle(vehicleId), SystemRight.VehicleAccess))
					return null;
			}

			return Server.Instance().DbManager.GetNearestPictures(vehicleId, logTime);
		}
		/// <summary> Возвращает перечень типов контроллеров, доступных для добавления в систему </summary>
		public ControllerType[] GetControllerTypeAllowedToAdd()
		{
			using (var entities = new Entities())
			{
				if (!CanOperatorAddTracker(entities))
					return new ControllerType[0];

				return GetAccessibleControllerTypes(entities);
			}
		}
		private ControllerType[] GetAccessibleControllerTypes(Entities entities)
		{
			IQueryable<CONTROLLER_TYPE> query = entities.CONTROLLER_TYPE;
			if (!IsSuperAdministrator)
				query = query.Where(ct => ct.AllowedToAddByCustomer == true);

			// Убрана дискриминация не корпоративных пользователей
			//if (!IsCorporateCustomer() && !IsSuperAdministrator)
			//{
			//	query =
			//		query.Where(
			//			ct =>
			//			ct.CommandTypes.Any(cmd => cmd.id == (int) CmdType.Setup) ||
			//			ct.TYPE_NAME == ControllerType.Names.Generic ||
			//			ct.TYPE_NAME == ControllerType.Names.Egts);
			//}

			var resource = ResourceContainers.Get(CultureInfo);
			return query
				.ToList()
				.Select(ct => ct.ToDto(resource))
				.OrderBy(ct => ct.SortOrder ?? int.MaxValue)
				.ThenBy(ct => ct.UserFriendlyName)
				.ToArray();
		}
		private bool CanOperatorAddTracker(Entities entities)
		{
			// Гостям нельзя добавлять трекеры
			if (IsGuest)
				return false;

			// Корпоративные клиенты (обязательно с департаментом нужного типа)
			if (ComputedDepartmentId != null && ComputedDepartmentType == DepartmentType.Corporate)
			{
				return
					// Супер-администратор
					IsSuperAdministrator ||
					// Партнер
					IsSalesManager ||
					// У текущего пользователя есть право AddTracker
					entities.v_operator_rights.Any(x =>
						x.operator_id == OperatorId &&
						x.right_id == (int)SystemRight.AddingTracker);
			}

			// Частные лица (преимущественно без департамента)
			// Проверяем количество неоплаченных трекеров
			var unpayedVehicleCount = entities.VEHICLE
				.Count(v =>
					v.Accesses.Any(ovr =>
						ovr.operator_id == OperatorId &&
						ovr.right_id == (int)SystemRight.SecurityAdministration) &&
						v.CONTROLLER.MLP_Controller.Asid.Contact == null);

			var unpayedVehicleLimit = entities.GetConstantAsInt("UnpayedVehicleCountPerUserLimit") ?? 5;

			return unpayedVehicleCount < unpayedVehicleLimit;
		}
		ControllerType[] IWebPersonalServer.GetControllerTypeAllowedToAssign(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var result = new List<ControllerType>();

				var resource = ResourceContainers.Get(CultureInfo);
				/*
				 * Временно убрано, до прояснения с точки зрения бизнеса
					if (entities.Billing_Service.Any(bs =>
						bs.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId &&
						bs.Type.Service_Type_Category == BillingService.NikaTracker))
					{
						result.AddRange(
							entities.CONTROLLER_TYPE
								.Where(ct =>
									ct.AllowedToAddByCustomer  == true &&
									ct.Default_Vehicle_Kind_ID == (int)VehicleKind.Tracker)
								.ToList()
								.Select(ct => ct.ToDTO(resource)));
					}
					else
				*/
				result.AddRange(GetAccessibleControllerTypes(entities));

				var currentControllerType =
					entities.CONTROLLER.Where(ct => ct.VEHICLE.VEHICLE_ID == vehicleId)
						.Select(c => c.CONTROLLER_TYPE)
						.FirstOrDefault();

				if (currentControllerType != null &&
					result.All(ct => ct.Id != currentControllerType.CONTROLLER_TYPE_ID))
					result.Insert(0, currentControllerType.ToDto(resource));

				return result
					.OrderBy(ct => ct.SortOrder ?? int.MaxValue)
					.ThenBy(ct => ct.UserFriendlyName)
					.ToArray();
			}
		}
		public void SetVehicleRuleValue(int vehicleId, RuleNumber ruleNumber, decimal value, Entities entities)
		{
			if (entities.VEHICLE_RULE.Any(r => r.VEHICLE.VEHICLE_ID == vehicleId && r.RULE.RULE_ID == (int)ruleNumber))
			{
				var rule = entities.VEHICLE_RULE.Where(r => r.VEHICLE.VEHICLE_ID == vehicleId && r.RULE.RULE_ID == (int)ruleNumber);
				foreach (var r in rule)
					r.VALUE = value;
			}
			else
			{
				var newRule = new VEHICLE_RULE
				{
					VEHICLE = entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId),
					RULE = entities.RULE.FirstOrDefault(r => r.RULE_ID == (int)ruleNumber),
					VALUE = value
				};
				entities.VEHICLE_RULE.AddObject(newRule);
			}
		}
		public void UpdateVehicleOwner(string ownerFirstName, string ownerSecondName, string ownerLastName,
			string ownerPhone1, string ownerPhone2, string ownerPhone3, int vehicleId, Entities entities)
		{
			var owner = entities.VEHICLE_OWNER.Where(
				vo => vo.VEHICLE.VEHICLE_ID == vehicleId &&
					  vo.OWNER_NUMBER == 1 //см. процедуру getVehicleProfile
				).Select(vo => vo.OWNER).FirstOrDefault();

			if (owner != null)
			{
				owner.FIRST_NAME = ownerFirstName;
				owner.SECOND_NAME = ownerSecondName;
				owner.LAST_NAME = ownerLastName;

				owner.PHONE_NUMBER1 = ownerPhone1;
				owner.PHONE_NUMBER2 = ownerPhone2;
				owner.PHONE_NUMBER3 = ownerPhone3;
			}
			else
			{
				owner = new OWNER
				{
					FIRST_NAME = ownerFirstName,
					LAST_NAME = ownerLastName,
					SECOND_NAME = ownerSecondName,
					PHONE_NUMBER1 = ownerPhone1,
					PHONE_NUMBER2 = ownerPhone2,
					PHONE_NUMBER3 = ownerPhone3
				};
				entities.OWNER.AddObject(owner);
				var vehicleOwner = new VEHICLE_OWNER
				{
					VEHICLE = entities.VEHICLE.First(v => v.VEHICLE_ID == vehicleId),
					OWNER = owner,
					OWNER_NUMBER = 1 //см. процедуру getVehicleProfile
				};
				entities.VEHICLE_OWNER.AddObject(vehicleOwner);
			}
		}
		public void UpdateVehicleControlDates(List<Dto::VehicleControlDate> controlDates, int vehicleId)
		{
			using (var context = new Entities())
			{
				var vehicle = context.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
				if (vehicle == null) return;
				var dobjects = vehicle.VehicleControlDate.ToList();
				foreach (var vehicleControlDate in dobjects)
					context.VehicleControlDate.DeleteObject(vehicleControlDate);
				foreach (var vehicleControlDate in controlDates)
				{
					var dbData = new EntityModel.VehicleControlDate()
					{
						Type_ID = (int)vehicleControlDate.TypeId,
						Value   = vehicleControlDate.Value,
						VEHICLE = vehicle
					};
					context.VehicleControlDate.AddObject(dbData);
				}
				context.SaveChangesBySession(SessionId);
			}
		}
		public void UpdateVehicleInfo(int vehicleId, decimal? fuelSpentStandart, decimal? fuelSpentStandardMH, string regNumber, string garageNumber, string brand, int? fuelTypeId, int? fuelTankVolume, int? vehicleKind, string ownerFirstName, string ownerSecondName, string ownerLastName, string ownerPhone1, string ownerPhone2, string ownerPhone3, byte[] vehicleImage)
		{
			using (var entities = new Entities())
			{
				//update vehicle
				var vehicle = entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
				if (vehicle == null)
					return;

				//update rules
				if (fuelSpentStandardMH != null)
					SetVehicleRuleValue(vehicleId, RuleNumber.FuelSpendStandardPerMH, (int)fuelSpentStandardMH.Value, entities);

				if (vehicleKind != null)
					vehicle.VEHICLE_KIND = entities.VEHICLE_KIND.FirstOrDefault(k => k.VEHICLE_KIND_ID == vehicleKind.Value);


				if (garageNumber != null)
					vehicle.GARAGE_NUMBER = garageNumber;

				if (vehicle.VEHICLE_KIND != null &&
					vehicle.VEHICLE_KIND.VEHICLE_KIND_ID != (int)VehicleKind.MobilePhone &&
					vehicle.VEHICLE_KIND.VEHICLE_KIND_ID != (int)VehicleKind.Tracker &&
					vehicle.VEHICLE_KIND.VEHICLE_KIND_ID != (int)VehicleKind.WebCamera)
				{
					vehicle.PUBLIC_NUMBER = regNumber;
					vehicle.VEHICLE_TYPE = brand;
					vehicle.Fuel_Type = fuelTypeId.HasValue ? entities.Fuel_Type.FirstOrDefault(ft => ft.ID == fuelTypeId) : null;
					vehicle.FUEL_TANK = fuelTankVolume.HasValue ? (short?)fuelTankVolume : null;
				}

				if (vehicle.DEPARTMENT != null && vehicle.DEPARTMENT.Country != null && vehicle.DEPARTMENT.Country.Name.ToLower() == "india")
				{
					// fuelSpentStandart км/л
					if (fuelSpentStandart > 0)
					{
						vehicle.FuelSpendStandardKilometer = (int)fuelSpentStandart.Value;
						vehicle.FuelSpendStandardLiter = 1;
					}
					else
					{
						vehicle.FuelSpendStandardKilometer = null;
						vehicle.FuelSpendStandardLiter = null;
					}
				}
				else
				{
					// fuelSpentStandart л/ 10 000 км
					if (fuelSpentStandart > 0)
					{
						vehicle.FuelSpendStandardKilometer = 10000;
						vehicle.FuelSpendStandardLiter = (int)(fuelSpentStandart.Value * 100);
					}
					else
					{
						vehicle.FuelSpendStandardKilometer = null;
						vehicle.FuelSpendStandardLiter = null;
					}
				}

				if (ownerFirstName != null ||
					ownerSecondName != null ||
					ownerLastName != null ||
					ownerPhone1 != null ||
					ownerPhone2 != null ||
					ownerPhone3 != null)
				{
					UpdateVehicleOwner(ownerFirstName, ownerSecondName, ownerLastName, ownerPhone1, ownerPhone2,
									   ownerPhone3, vehicleId, entities);
				}

				UpdateVehicleImage(vehicleImage, vehicleId, entities);

				entities.SaveChangesBySession(SessionId);
			}
		}
		public void UpdateVehicleImage(byte[] vehicleImage, int vehicleId, Entities entities)
		{
			if (null == entities)
				return;
			var vehiclePicture = entities
				?.VEHICLE
				?.FirstOrDefault(v => v.VEHICLE_ID == vehicleId)
				?.VEHICLE_PICTURE
				?.FirstOrDefault();
			if (0 < (vehicleImage?.Length ?? 0))
			{
				if (null != vehiclePicture)
					vehiclePicture.PICTURE = vehicleImage;
				else
					entities.AddToVEHICLE_PICTURE(new VEHICLE_PICTURE { VEHICLE_ID = vehicleId, PICTURE = vehicleImage });
			}
			else
			{
				if (null != vehiclePicture)
					entities.DeleteObject(vehiclePicture);
			}
		}
		DateTime IWebPersonalServer.GetServerDateTime()
		{
			return DateTime.UtcNow;
		}
		public LocationInfo GetLocationByIP(string ip)
		{
			var bytesIp = IPAddress.Parse(ip).GetAddressBytes();
			var intIp = (long)bytesIp[0] << 24 | (long)bytesIp[1] << 16 | (long)bytesIp[2] << 8 | (long)bytesIp[3];

			using (var entities = new Entities())
			{
				var data =
					(from location in entities.IPValues
					 join c in entities.IPRegions on location.CityId equals c.CityId
					 where location.RangeFromInt <= intIp && location.RangeToInt >= intIp
					 select new LocationInfo()
					 {
						 Longitude = c.Longitude.Value,
						 Latitude = c.Latitude.Value,
						 CityName = c.Name,
						 DistrictName = c.District,
						 IPCityId = c.CityId,
						 RegionName = c.Region
					 }).FirstOrDefault();
				return data;
			}
		}
		public string AppId { get; private set; }
		/// <summary> Используется для установки параметров мобильного приложения из веб-сессии, при потери текущей сессии </summary>
		/// <param name="appId"></param>
		public void SelectAppId(string appId)
		{
			using (var entities = new Entities())
			{
				var appClient = entities.AppClient
					.Include(AppClient.ContactIncludePath)
					.FirstOrDefault(a => a.AppId == appId && a.Operator_ID == OperatorId);
				if (appClient == null)
					throw new SecurityException("Current operator " + OperatorId + " has no AppId " + appId);

				AppId = appClient.AppId;
			}
		}
		public void UpdateClientRegistration(string appId, string gcmRegistrationId, string apnDeviceToken, bool debug = false)
		{
			if (IsGuest ||
				string.IsNullOrWhiteSpace(appId) &&
				string.IsNullOrWhiteSpace(gcmRegistrationId) &&
				string.IsNullOrWhiteSpace(apnDeviceToken))
				return;

			ContactType type;
			string contactValue;

			if (string.IsNullOrWhiteSpace(gcmRegistrationId))
			{
				type = debug ? ContactType.iOsDebug : ContactType.Apple;
				contactValue = apnDeviceToken;
			}
			else
			{
				type = ContactType.Android;
				contactValue = gcmRegistrationId;
			}

			if (!string.IsNullOrWhiteSpace(AppId))
			{
				using (var entities = new Entities())
				{
					var appClient = entities.AppClient
						.Include(AppClient.ContactIncludePath)
						.Include(AppClient.OPERATORIncludePath)
						.FirstOrDefault(x => x.AppId == appId);

					if (null != appClient && (appClient.Contact?.Type != (int?)type || appClient.Contact?.Value != contactValue))
						ClearAppIdIfNeeded();
				}
			}
			Server.Instance().SetAppClientContact(appId, type, contactValue);

			AppId = appId;
			using (var entities = new Entities())
			{
				var ownVehicleToUpdate = entities.VEHICLE
					.FirstOrDefault(v =>
						v.CONTROLLER.MLP_Controller.Asid.OPERATOR.OPERATOR_ID == OperatorId &&
						v.VEHICLE_KIND.VEHICLE_KIND_ID != (int)VehicleKind.Smartphone);

				if (ownVehicleToUpdate != null)
					ownVehicleToUpdate.VEHICLE_KIND = entities.GetVehicleKind(VehicleKind.Smartphone);

				Orm:: Contact appContact = null;

				if (!string.IsNullOrWhiteSpace(contactValue))
					appContact = entities.GetContact(type, contactValue);

				var appClient = entities
					.AppClient
					.Include(AppClient.ContactIncludePath)
					.Include(AppClient.OPERATORIncludePath)
					.First(ac => ac.AppId == appId);

				if (appClient.OPERATOR == null ||
					appClient.OPERATOR.OPERATOR_ID != OperatorId)
					appClient.OPERATOR = entities.GetOperator(OperatorId);

				if (entities.GetConstantAsBool(Constant.SinglePushToAppDestinationPerUser))
				{
					//Удаляем все остальные контакты
					var appClients = entities
						.AppClient
						.Where(ac =>
							ac.Operator_ID == OperatorId &&
							ac.AppId != appId)
						.ToList();

					foreach (var item in appClients)
					{
						entities.AppClient.DeleteObject(item);
					}
				}

				entities.SaveChangesBySession(SessionId);

				// Отправляем Push на существующее приложение обслуживания.
				if (appContact != null)
				{
					// Ищем актуальные запросы дружбы и пересылаем их на клиент
					entities.ResendFriendshipRequests(OperatorId, SaveContext.Session(SessionId), new List<Orm::Contact> { appContact });
				}
			}
		}
		public void LogUserAgent(string userAgent)
		{
			using (var entities = new Entities())
			{
				var userAgentRecord = entities.UserAgent.FirstOrDefault(x => x.Name == userAgent);
				if (userAgentRecord == null)
					entities.UserAgent.AddObject(userAgentRecord = new UserAgent { Name = userAgent });

				entities.Session_UserAgent.AddObject(
					new Session_UserAgent
					{
						Date = DateTime.UtcNow,
						Session_ID = SessionId,
						UserAgent = userAgentRecord
					});
				//В исторические таблицы ничего не пишется
				entities.SaveChanges();
			}
		}
		public void CleanupBeforeLogout()
		{
			Trace.TraceInformation("Trying to cleanup when logout {0} appId {1}", OperatorId, AppId);
			ClearAppIdIfNeeded();
		}
		public void SetSessionParameters(SessionParameters sessionParameters)
		{
			using (var entities = new Entities())
			{
				var parameters = entities.Session_Info.FirstOrDefault(s => s.SessionId == SessionId);
				if (parameters == null)
				{
					parameters = new Session_Info
					{
						SessionId = SessionId
					};
					entities.Session_Info.AddObject(parameters);
				}

				if (sessionParameters.ReasonModified)
					parameters.Reason = sessionParameters.Reason;
				if (sessionParameters.ParentSessionIdModified)
					parameters.ParentSessionId = sessionParameters.ParentSessionId;

				entities.SaveChangesByOperator(OperatorId);
			}
		}
		public bool IsLocked()
		{
			if (!ComputedDepartmentId.HasValue)
				return false;

			using (var entities = new Entities())
			{
				var lockDate = entities.DEPARTMENT
					.Where(d => d.DEPARTMENT_ID == ComputedDepartmentId)
					.Select(d => d.LockDate)
					.FirstOrDefault();
				return lockDate.HasValue;
			}
		}
		/// <summary> Удаляет записи по текущему идентификатору [мобильного] приложения </summary>
		private void ClearAppIdIfNeeded()
		{
			var appId = AppId;
			if (string.IsNullOrWhiteSpace(appId))
				return;
			AppId = null;
			using (var entities = new Entities())
			{
				var cloudContactId = entities
					.AppClient
					.Where(ac => ac.AppId == appId)
					.Select(ac => ac.Contact_ID)
					.FirstOrDefault();
				if (null == cloudContactId)
					return;

				var appClientsForCloudContactId = entities.AppClient
					.Where(ac => ac.Contact_ID == cloudContactId.Value)
					.ToList();
				foreach (var appClient in appClientsForCloudContactId)
				{
					if (appClient.EntityState == EntityState.Deleted)
						continue;
					entities.AppClient.DeleteObject(appClient);
				}
				entities.SaveChangesBySession(SessionId);
			}
		}
		public override string ToString()
		{
			return string.Format("{0} [OperatorID={1}]", GetType().Name, OperatorId);
		}
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null)
		{
			return Server.Instance().GetLatLngByAddress(address, out lat, out lng, mapGuid);
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null)
		{
			return Server.Instance().GetAddressesByText(searchText, language, mapGuid);
		}
		public string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false)
		{
			return Server.Instance().GetAddressByPoint(lat, lng, language, mapGuid, cacheOnly);
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null)
		{
			return Server.Instance().GetAddresses(requests, culture, mapGuid);
		}
		public DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null)
		{
			return Server.Instance().FillAddressColumn(dataSet, tableName, lngColumnName, latColumnName, addressColumnName, language, mapGuid);
		}
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			return Server.Instance().GetArrivalTimesToPoint(destination, origins, language);
		}
	}
}