﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.Resources;
using FORIS.TSS.Terminal;
using FORIS.TSS.WorkplaceShadow.Geo;
using Interfaces.Geo;
using MessageTemplate = FORIS.TSS.BusinessLogic.DTO.Messages.MessageTemplate;
using Vehicle = FORIS.TSS.BusinessLogic.DTO.Vehicle;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Базовый набор функций </summary>
	/// <remarks>
	/// MBR is only for RemotingServices.ExecuteMessage
	/// This object is not exposed through the remoting
	/// </remarks>
	public abstract class BasicFunctionSetImpl : Component, IBasicFunctionSet
	{
		#region Controls & Components

		protected IContainer components;
		protected CoreServerDatabaseManager databaseManager;

		#endregion Controls & Components

		#region Constructor & Dispose

		public BasicFunctionSetImpl()
		{
			InitializeComponent();

			AlarmReceived += (sender, args) =>
			{
				var vehicleId = args.MobilUnit.Unique;

				var logTime = args.MobilUnit.Time;

				while (true)
				{
					if (_lastAlarmEnabledLogTime.TryAdd(vehicleId, logTime))
						break;

					int prevLogTime;
					if (!_lastAlarmEnabledLogTime.TryGetValue(vehicleId, out prevLogTime))
						continue;

					if (prevLogTime > logTime)
						break;

					if (_lastAlarmEnabledLogTime.TryUpdate(vehicleId, logTime, prevLogTime))
						break;
				}
			};
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components      = new Container();
			databaseManager = new CoreServerDatabaseManager() { Database = Globals.TssDatabaseManager.DefaultDataBase };
		}

		#endregion Component Designer generated code

		/// <summary> Событие, которое срабатывает при появлении нового тревожного статуса у объекта наблюдения </summary>
		public event EventHandler<ReceiveAlarmArgs> AlarmReceived;

		protected void OnReceiveAlarm(ReceiveAlarmArgs args)
		{
			var alarmReceived = AlarmReceived;
			if (alarmReceived != null)
			{
				try
				{
					alarmReceived(this, args);
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();
				}
			}
		}

		public abstract string GetVehicleName(int? operatorId, int vehicleId);
		public abstract string GetAsidName(int asidId);
		public bool FilterByDepartment(int operatorId)
		{
			using (var entities = new Entities())
			{
				return 1 < entities.v_operator_department_right
					.Count(a =>
						a.operator_id == operatorId &&
						a.right_id    == (int)SystemRight.SecurityAdministration);
			}
		}
		public bool IsAllowedVehicleAll(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			using (var entities = new Entities())
				return entities.IsAllowedVehicleAll(operatorId, vehicleId, rights);
		}
		public bool IsAllowedVehicleAny(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			using (var entities = new Entities())
				return entities.IsAllowedVehicleAny(operatorId, vehicleId, rights);
		}
		public List<SystemRight> GetOperatorSystemRights(int operatorId)
		{
			using (var entities = new Entities())
			{
				return entities
					.v_operator_rights
					.Where(o => o.operator_id == operatorId)
					.Select(r => (SystemRight)r.right_id)
					.ToList();
			}
		}
		public abstract string GetConstant(Constant constant);
		private readonly TimingCache<bool, Dictionary<int, MessageTemplate>> _messageTemplates = new TimingCache<bool, Dictionary<int, MessageTemplate>>(new TimeSpan(1, 0, 0),
			input =>
			{
				using (var entities = new Entities())
				{
					return entities.MESSAGE_TEMPLATE
						.ToList()
						.ConvertAll(item => item.ToDTO())
						.ToDictionary(item => item.Id);
				}
			});
		public Dictionary<int, MessageTemplate> MessageTemplates
		{
			get { return _messageTemplates[true]; }
		}
		private readonly TimingCache<bool, Dictionary<int, BusinessLogic.DTO.Messages.MessageTemplateField>> _messageTemplateFields = new TimingCache<bool, Dictionary<int, BusinessLogic.DTO.Messages.MessageTemplateField>>(new TimeSpan(1, 0, 0),
			input =>
			{
				using (var entities = new Entities())
				{
					return entities.MESSAGE_TEMPLATE_FIELD
						.Include(MESSAGE_TEMPLATE_FIELD.MESSAGE_TEMPLATEIncludePath)
						.Include(MESSAGE_TEMPLATE_FIELD.DOTNET_TYPEIncludePath)
						.ToList()
						.ConvertAll(item => item.ToDTO())
						.ToDictionary(item => item.Id);
				}
			});
		public Dictionary<int, BusinessLogic.DTO.Messages.MessageTemplateField> MessageTemplateFields
		{
			get { return _messageTemplateFields[true]; }
		}
		private readonly TimingCache<int, int[]> _vehicleOperatorsCache =
			new TimingCache<int, int[]>(TimeSpan.FromMinutes(5), LoadVehicleOperators);
		/// <summary> Получение данных о запланированных рейсах и их заменах </summary>
		/// <param name="iReportDateTime">дата и время, на которые требуется получить данные о замененных рейсах</param>
		/// <param name="iMotorcadeID">ID группы ТС (номер автоколонны), для которой строится отчет</param>
		/// <returns>Заполненный датасет с данными из БД о заменах рейсов</returns>
		public DataSet SubstitutionsInfoGet(DateTime iReportDateTime, int iMotorcadeID, int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SubstitutionsInfoGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@MotorcadeID"].Value = iMotorcadeID;
				if (operatorId > 0)
				{
					clsStoredProcedure["@operator_id"].Value = operatorId;
				}
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.SubstitutionsInfoGet);
				return dsResultData;
			}
		}
		/// <summary> Получение данных о ТС на указанную дату, в путевых листах которых указано определенное состояние </summary>
		/// <param name="iReportDateTime">дата, на которую требуется получить данные о несостоявшихся рейсах.</param>
		/// <param name="iVehicleGroupID">ID автоколонны, для которой создается наряд</param>
		/// <param name="iWaybillState">номер состояния ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet VehiclesListByStateGet(DateTime iReportDateTime, int iVehicleGroupID, int iWaybillState)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.VehiclesListByStateGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@VehicleGroupID"].Value = iVehicleGroupID;
				clsStoredProcedure["@WaybillState"].Value = iWaybillState;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.VehiclesListByStateGet);
				return dsResultData;
			}
		}
		/// <summary> Получение данных о водителях на указанную дату, в записях WAYBILL которых указано определенное состояние </summary>
		/// <param name="iReportDateTime">дата, на которую требуется получить данные о несостоявшихся рейсах.</param>
		/// <param name="iDriverGroupID">ID группы водителей, для которой требуется получить отчет</param>
		/// <param name="iDriverState">номер состояния записи WAYBILL</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet DriversListByStateGet(DateTime iReportDateTime, int iDriverGroupID, int iDriverState)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DriversListByStateGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@DriverGroupID"].Value = iDriverGroupID;
				clsStoredProcedure["@DriverState"].Value = iDriverState;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.DriversListByStateGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка ТС с указанным состоянием для всех колонн на указанную дату.
		/// </summary>
		/// <param name="iReportDate">дата, на которую требуется получить данные ТС с указанным состоянием.</param>
		/// <param name="iWaybillState">номер состояния ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet VehiclesCommonListByStateGet(DateTime iReportDate, int iWaybillState)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.VehiclesCommonListByStateGet"))
			{
				clsStoredProcedure["@ReportDate"].Value = iReportDate;
				clsStoredProcedure["@WaybillState"].Value = iWaybillState;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.VehiclesCommonListByStateGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных по статистике доступности GPRS.
		/// </summary>
		/// <param name="iReportDate">дата, на которую требуется получить данные о доступности GPRS</param>
		/// <param name="iTimeType">определяет представление выводимого времени</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet GPRSAccessibilityStatisticsGet(DateTime iReportDate, char iTimeType, int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GPRSstat2"))
			{
				clsStoredProcedure["@date"].Value = iReportDate;
				clsStoredProcedure["@ttype"].Value = iTimeType;
				if (operatorId > 0)
				{
					clsStoredProcedure["@operator_id"].Value = operatorId;
				}
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.GPRSAccessibilityStatisticsGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка ТС указанной автоколонны с их состояниями на указанную дату.
		/// </summary>
		/// <param name="iReportDateTime">Дата и время, на которые требуется сформировать отчет</param>
		/// <param name="VehicleGroupID">номер автоколонны</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet VehiclesStatusesListGet(DateTime iReportDateTime, int VehicleGroupID)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.VehiclesStatusesListGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@VehicleGroupID"].Value = VehicleGroupID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.VehiclesStatusesListGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных для заполнения оборотной стороны путевого листа (расписания).
		/// </summary>
		/// <param name="iWaybillHeaderID"></param>
		/// <param name="iShift"></param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		/// <remarks>Добавлено: Алексей М. Блинов; 23.04.2005</remarks>
		public DataSet WaybillTimesinGet(int iWaybillHeaderID, long iShift)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.WaybillInTimesGet"))
			{
				clsStoredProcedure["@WaybillHeaderID"].Value = iWaybillHeaderID;
				clsStoredProcedure["@Shift"].Value = iShift;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.WaybillInTimesGet);
				DatasetHelperBase.Dump(dsResultData);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных для заполнения ПЛ.
		/// </summary>
		/// <param name="iWaybill_schedule_id">ID расписания ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet GetScheduleForWaybill(int iWaybill_schedule_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleForWaybill"))
			{
				sp["@schedule_id"].Value = iWaybill_schedule_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetScheduleForWaybill);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение данных для заполнения в расписании ПЛ особых рейсов (обедов, заправок и т.д.).
		/// </summary>
		/// <param name="iScheduleId">ID расписания</param>
		/// <param name="iTripKindID">тип поездки, данные для которой надо получить</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet SpecialTripsScheduleGet(int iScheduleId, int iTripKindID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SpecialTripsScheduleGet"))
			{
				sp["@schedule_id"].Value = iScheduleId;
				sp["@trip_kind_id"].Value = iTripKindID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.SpecialTripsScheduleGet);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение данных для заполнения ПЛ.
		/// </summary>
		/// <param name="waybill_header_id">ID заголовка ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		/// <remarks>Добавлено: Алексей М. Блинов; 23.04.2005</remarks>
		public DataSet GetDataForWaybillHistoryReport(int waybill_header_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_DATA_FOR_WAYBILL_HISTORY_REPORT"))
			{
				sp["@waybill_header_id"].Value = waybill_header_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_DATA_FOR_WAYBILL_HISTORY_REPORT);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение данных для заполнения ПЛ.
		/// </summary>
		/// <param name="waybill_header_id">ID заголовка ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		/// <remarks>Добавлено: Алексей М. Блинов; 23.04.2005</remarks>
		public DataSet GetTimeDeviations(int waybill_header_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_TIME_DEVIATIONS"))
			{
				sp["@WH_ID"].Value = waybill_header_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_TIME_DEVIATIONS);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение названий автоколонн по указанной дате. Выбираются только те автоколонны, у которых хотя бы на один автомобиль заведен ПЛ.
		/// </summary>
		/// <param name="iReportDate">дата, на которую требуется получить список автоколонн</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet ActiveMotorcadeNamesByDateGet(DateTime iReportDate, int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ActiveMotorcadeNamesByDateGet"))
			{
				clsStoredProcedure["@ReportDate"].Value = iReportDate;
				if (operatorId > 0)
					clsStoredProcedure["@operator_id"].Value = operatorId;

				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.ActiveMotorcadeNamesByDateGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение названий автоколонн по указанной дате. Выбираются только те автоколонны, у которых хотя бы на один автомобиль заведен ПЛ.
		/// </summary>
		/// <param name="iReportDate">дата, на которую требуется получить список автоколонн</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet ActiveMotorcadeNamesByDateGet(DateTime iReportDate)
		{
			return ActiveMotorcadeNamesByDateGet(iReportDate, 0);
		}
		/// <summary>
		/// Получение названий всех автоколонн.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet MotorcadeNamesGet(int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MotorcadeNamesGet"))
			{
				if (operatorId > 0)
				{
					clsStoredProcedure["@operator_id"].Value = operatorId;
				}
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.MotorcadeNamesGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка названий существующих в системе GSM-контроллеров.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet ActiveControllerTypeNamesGet()
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ActiveControllerTypeNamesGet"))
			{
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.ActiveControllerTypeNamesGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных для заполнения отчета "Путевой лист" по указанному ID ПЛ.
		/// </summary>
		/// <param name="iWaybillHeaderID">ID ПЛ, для которого получаются данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet WaybillDataByIDGet(int iWaybillHeaderID)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.WaybillDataByIDGet"))
			{
				clsStoredProcedure["@WaybillHeaderID"].Value = iWaybillHeaderID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.WaybillDataByIDGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных по сменам водителей у ПЛ с указанным ID: ФИО водителей, табельные номера, количество смен у данного ПЛ.
		/// </summary>
		/// <param name="iWaybillHeaderID">ID ПЛ, для которого получаются данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet DriversShiftsInfoGet(int iWaybillHeaderID)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DriversShiftsInfoGet"))
			{
				clsStoredProcedure["@WaybillHeaderID"].Value = iWaybillHeaderID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.DriversShiftsInfoGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение информации о расписании для построения отчета по набору параметров.
		/// </summary>
		/// <param name="iExitNumber">номер выхода</param>
		/// <param name="iDayKindID">ID типа дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet ScheduleByParametersGet(string iExitNumber, int iDayKindID)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ScheduleByParametersGet"))
			{
				clsStoredProcedure["@ExitNumber"].Value = iExitNumber;
				clsStoredProcedure["@DayKindID"].Value = iDayKindID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.ScheduleByParametersGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение недостающих параметров для отчета "Расписание" по уже известным.
		/// </summary>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iPeriodID">ID периода действия расписания</param>
		/// <param name="iDayKindID">ID типа дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet ScheduleReportParametersRefresh(int iRouteID, int iPeriodID, int iDayKindID, int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ScheduleReportParametersRefresh"))
			{
				clsStoredProcedure["@RouteID"].Value = iRouteID;
				clsStoredProcedure["@PeriodID"].Value = iPeriodID;
				clsStoredProcedure["@DayKindID"].Value = iDayKindID;
				if (operatorId > 0)
				{
					clsStoredProcedure["@operator_id"].Value = operatorId;
				}
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.ScheduleReportParametersRefresh);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных о специальных пометках для ПЛ.
		/// </summary>
		/// <param name="iWaybillHeaderID">ID путевого листа</param>
		/// <param name="iMarkDate">Дата пометки путевого листа</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet WaybillMarksGet(int iWaybillHeaderID, DateTime iMarkDate)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.WaybillMarksGet"))
			{
				clsStoredProcedure["@WaybillHeaderID"].Value = iWaybillHeaderID;
				clsStoredProcedure["@MarkDate"].Value = iMarkDate;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.WaybillMarksGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка конечных станций для всех маршрутов.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet EndStationsListGet()
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.EndStationsListGet"))
			{
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.EndStationsListGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка конечных всех станций для всех маршрутов.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet StationsListGet(int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.StationsListGet"))
			{
				if (operatorId > 0)
					clsStoredProcedure["@operator_id"].Value = operatorId;

				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.StationsListGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка конечных всех станций для всех маршрутов.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet StationsListGet()
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.StationsListGet"))
			{
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.StationsListGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение плановых и фактических интервалов отправлений автобусов данного маршрута от определенной конечной станции.
		/// </summary>
		/// <param name="iGeoSegmentID">ID остановки</param>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iReportDate">дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet MovementIntervalsGet(int iGeoSegmentID, int iRouteID, DateTime iReportDate)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MovementIntervalsGet"))
			{
				clsStoredProcedure["@GeoSegmentID"].Value = iGeoSegmentID;
				clsStoredProcedure["@RouteID"].Value = iRouteID;
				clsStoredProcedure["@ReportDate"].Value = iReportDate;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.MovementIntervalsGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных о запланированных по расписанию и фактических временах прохождения заданной точки автобусами заданного маршрута.
		/// </summary>
		/// <param name="iPointID">ID точки (остановки)</param>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iReportDateTime">дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet MovementIntervalsThroughPpointGet(int iPointID, int iRouteID, DateTime iReportDateTime)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MovementIntervalsThroughPpointGet"))
			{
				clsStoredProcedure["@PointID"].Value = iPointID;
				clsStoredProcedure["@RouteID"].Value = iRouteID;
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.MovementIntervalsThroughPpointGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение списка всех заведенных в БД маршрутов, получение списка рейсов и их статусов по каждому из маршрутов на заданный период.
		/// </summary>
		/// <param name="iReportDateTime">Дата и время, на которые строится отчет</param>
		/// <param name="iReportTimeInSeconds">Время, на которое строится отчет, в секундах от начала дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet TripsStatisticsByRoutesGet(DateTime iReportDateTime, int iReportTimeInSeconds)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.TripsStatisticsByRoutesGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@ReportTimeInSeconds"].Value = iReportTimeInSeconds;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.TripsStatisticsByRoutesGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение информации о запланированных и фактических рейсах на заданное время по каждому из маршрутов.
		/// </summary>
		/// <param name="iReportDateTime">Дата и время, на которые строится отчет</param>
		/// <param name="iReportTimeInSeconds">Дата и время в секундах от начала дня, на которые строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet TripsByRoutesAndTimeGet(DateTime iReportDateTime, int iReportTimeInSeconds)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.TripsByRoutesAndTimeGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@ReportTimeInSeconds"].Value = iReportTimeInSeconds;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.TripsByRoutesAndTimeGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение количества "полезных" (активных) рейсов для ПЛ с данным ID.
		/// </summary>
		/// <param name="iWaybillHeaderID">ID рассматриваемого ПЛ</param>
		/// <param name="iShiftNumber">Номер смены, для которой требуется получить количество рейсов</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet UsefullWaybilTripsCount(int iWaybillHeaderID, int iShiftNumber)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.UsefullWaybilTripsCount"))
			{
				clsStoredProcedure["@WaybillHeaderID"].Value = iWaybillHeaderID;
				clsStoredProcedure["@ShiftNumber"].Value = iShiftNumber;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.UsefullWaybilTripsCount);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение расширенной информации по определенной автоколонне на указанную дату.
		/// </summary>
		/// <param name="iReportDateTime">Дата, на которую требуется получить информацию из БД</param>
		/// <param name="iVehicleGroupID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet MotorcadeAdvancedDataGet(DateTime iReportDateTime, int iVehicleGroupID)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MotorcadeAdvancedDataGet"))
			{
				clsStoredProcedure["@ReportDateTime"].Value = iReportDateTime;
				clsStoredProcedure["@VehicleGroupID"].Value = iVehicleGroupID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.MotorcadeAdvancedDataGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение данных об интервалах недоступности GPRS на ТС.
		/// </summary>
		/// <param name="iReportDate">Дата, на которую требуется получить информацию из БД</param>
		/// <param name="iVehicleGroupID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <param name="iControllerTypeID">ID контроллера, для ТС с которым требуется построить отчет</param>
		/// <param name="iNeglibleInterval">Значение нерегистрируемого интервала отсутствия сигнала</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		/// <param name="iVehicleID"></param>
		public DataSet GPRSInaccessibilityDataGet(DateTime iReportDate, int iVehicleGroupID, int iControllerTypeID, int iNeglibleInterval, int iVehicleID, int operatorId)
		{
			using (StoredProcedure clsStoredProcedure = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GPRSInaccessibilityDataGet"))
			{
				clsStoredProcedure["@ReportDate"].Value = iReportDate;
				clsStoredProcedure["@VehicleGroupID"].Value = iVehicleGroupID;
				clsStoredProcedure["@ControllerTypeID"].Value = iControllerTypeID;
				clsStoredProcedure["@NeglibleInterval"].Value = iNeglibleInterval;
				clsStoredProcedure["@VehicleID"].Value = iVehicleID;
				if (operatorId > 0)
				{
					clsStoredProcedure["@operator_id"].Value = operatorId;
				}
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.GPRSInaccessibilityDataGet);
				return dsResultData;
			}
		}
		public DataSet SearchWaybills(int iWaybillHeaderId, DateTime dtDate, int iVehicleId, int iScheduleId, int iDriverId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SearchWaybills"))
			{
				sp["@waybill_header_id"].Value = (iWaybillHeaderId > 0) ? (object)iWaybillHeaderId : DBNull.Value;
				sp["@date"].Value = dtDate;
				sp["@vehicle_id"].Value = (iVehicleId > 0) ? (object)iVehicleId : DBNull.Value;
				sp["@schedule_id"].Value = (iScheduleId > 0) ? (object)iScheduleId : DBNull.Value;
				sp["@driver_id"].Value = (iDriverId > 0) ? (object)iDriverId : DBNull.Value;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.SearchWaybills);
				return dataSet;
			}
		}
		/// <summary> Получение истории движения </summary>
		/// <param name="count"></param>
		/// <param name="interval"></param>
		/// <param name="time_from"></param>
		/// <param name="time_to"></param>
		/// <param name="vehicleID"></param>
		/// <param name="workingIntervals"></param>
		/// <returns></returns>
		public DataSet GetMoveDetailHistory(int count, int interval, DateTime time_from, DateTime time_to, int vehicleID, IEnumerable<LogTimeSpanParam> workingIntervals)
		{
			return databaseManager.GetMoveDetailHistory(count, interval, time_from, time_to, vehicleID, workingIntervals);
		}
		public DataSet GetMoveGroupDay(int vehicleID, IEnumerable<LogTimeSpanParam> workingIntervals)
		{
			return databaseManager.GetMoveGroupDay(vehicleID, workingIntervals);
		}
		/// <summary> Получение истории движения </summary>
		/// <param name="time_from"></param>
		/// <param name="time_to"></param>
		/// <param name="vehicle"></param>
		/// <returns></returns>
		public DataSet GetMoveHistoryDistance(DateTime time_from, DateTime time_to, string vehicle)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetMoveHistoryDistance"))
			{
				sp["@time_from"].Value = time_from;
				sp["@time_to"].Value = time_to;
				sp["@vehicle"].Value = vehicle;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMoveHistoryDistance);
				return dataSet;
			}
		}
		/// <summary> Получение пробега для заданной машины за заданный период времени </summary>
		/// <param name="fromInt">дата начала периода (UTC)</param>
		/// <param name="toInt">дата конца периода (UTC)</param>
		/// <param name="vehicleID">ID машины</param>
		/// <returns>Пробег в метрах</returns>
		public float GetMoveHistoryDistanceValue(int fromInt, int toInt, int vehicleID)
		{
			float distance = 0;
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CalculateDistance"))
			{
				sp["@from"].Value = fromInt;
				sp["@to"].Value = toInt;
				sp["@vehicle_id"].Value = vehicleID;
				distance = (float)sp.ExecuteScalar();
			}

			return distance;
		}
		/// <summary> Возвращает список пользователей, которые имеют доступ к указанному объекту наблюдения </summary>
		public int[] GetVehicleOperators(int vehicleId)
		{
			return _vehicleOperatorsCache[vehicleId];
		}

		private static readonly int[] RightsForLoadVehicleOperators = new[]
		{
			SystemRight.VehicleAccess,
			SystemRight.Ignore,
		}.Cast<int>().ToArray();
		private static int[] LoadVehicleOperators(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var operators = entities.v_operator_vehicle_right
					.Where(ovr =>
						ovr.vehicle_id == vehicleId &&
						RightsForLoadVehicleOperators.Contains(ovr.right_id))
					.ToLookup(ovr => ovr.operator_id, ovr => (SystemRight)ovr.right_id);

				return operators
					.Where(g => !g.Contains(SystemRight.Ignore))
					.Select(g => g.Key)
					.ToArray();
			}
		}
		public CheckPasswordResult CheckPasswordForWeb(string login, string password)
		{
			if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
				return null;

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CheckPasswordForWeb"))
			{
				sp["@login"].Value = login;
				sp["@password"].Value = password;

				using (new ConnectionOpener(sp))
				{
					using (var reader = sp.ExecuteReader())
					{
						if (!reader.Read())
							return null;

						var operatorId = reader.GetInt32(reader.GetOrdinal("Operator_ID"));
						var oneTimePassword = reader.GetBoolean(reader.GetOrdinal("OneTimePassword"));
						var currentPassword = reader.GetString(reader.GetOrdinal("Password"));
						var operatorLocked = reader.GetBoolean(reader.GetOrdinal("IsLocked"));

						return currentPassword == password ? new CheckPasswordResult(operatorId, oneTimePassword, operatorLocked) : null;
					}
				}
			}
		}
		public ReportMenuVehiclesCapabilities GetReportMenuCapabilities(int operatorId, int? departmentId)
		{
			return Server.Instance().DbManager.GetReportMenuCapabilities(operatorId, departmentId);
		}
		public Department GetDepartment(int departmentId)
		{
			using (var entities = new Entities())
			{
				return entities.DEPARTMENT
					?.FirstOrDefault(item => item.DEPARTMENT_ID == departmentId)
					?.ToDto();
			}
		}
		public BusinessLogic.DTO.Messages.Message[] GetMessagesByAppId(int operatorId, string appId, int? lastMessageId, DateTime? from, DateTime? to, Pagination pagination)
		{
			BusinessLogic.DTO.Messages.Message[] messages;
			MessageField[] messagesFields;

			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetMessagesByAppId"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@appId"].Value = appId;
				sp["@operatorId"].Value = operatorId;
				if (from.HasValue)
					sp["@from"].Value = from;
				if (to.HasValue)
					sp["@to"].Value = to;
				if (lastMessageId.HasValue)
					sp["@lastMessageId"].Value = lastMessageId.Value;
				if (pagination != null)
				{
					sp["@skip"].Value = pagination.Skip;
					sp["@count"].Value = pagination.RecordsPerPage;
				}

				using (var reader = sp.ExecuteReader())
				{
					var totalMessagesCount = ReadCount(reader);
					var messagesCount = pagination?.RecordsPerPage ?? totalMessagesCount;
					reader.NextResult();
					messages = ReadMessages(reader, messagesCount);
					if (pagination != null)
					{
						pagination.TotalRecords     = totalMessagesCount;
						pagination.TotalPageRecords = messages.Length;
					}

					reader.NextResult();
					var messagesFieldsCount = ReadCount(reader);
					reader.NextResult();
					messagesFields = ReadMessageFields(reader, messagesFieldsCount);
				}
			}

			MergeFieldsToMessages(messages, messagesFields);
			return messages;
		}
		public BusinessLogic.DTO.Messages.Message[] GetMessagesByOperator(int operatorId, int? lastMessageId, DateTime? @from, DateTime? to, Pagination pagination, int[] vehicleIds)
		{
			BusinessLogic.DTO.Messages.Message[] messages;
			MessageField[] messagesFields;

			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetMessagesByOperatorId"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@operatorId"].Value = operatorId;
				if (from.HasValue)
					sp["@from"].Value = from;
				if (to.HasValue)
					sp["@to"].Value = to;
				if (lastMessageId.HasValue)
					sp["@lastMessageId"].Value = lastMessageId.Value;
				if (pagination != null)
				{
					sp["@skip"].Value  = pagination.Skip;
					sp["@count"].Value = pagination.RecordsPerPage;
				}
				if (vehicleIds != null && vehicleIds.Length != 0)
					sp.SetTableValuedParameter("@vehicleIds", vehicleIds);

				using (var reader = sp.ExecuteReader())
				{
					var totalMessagesCount = ReadCount(reader);

					var messagesCount      = totalMessagesCount;
					if (pagination != null && pagination.RecordsPerPage < messagesCount)
						messagesCount = pagination.RecordsPerPage;

					reader.NextResult();
					messages = ReadMessages(reader, messagesCount);
					if (pagination != null)
					{
						pagination.TotalRecords     = totalMessagesCount;
						pagination.TotalPageRecords = messages.Length;
					}

					reader.NextResult();
					var messagesFieldsCount = ReadCount(reader);
					reader.NextResult();
					messagesFields = ReadMessageFields(reader, messagesFieldsCount);
				}
			}

			MergeFieldsToMessages(messages, messagesFields);
			return messages;
		}
		public List<MessageWeb> GetMessagesByMsisdn(string msisdn, int? firstMessageId, Pagination pagination)
		{
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);
			using (var entities = new Entities())
			{
				var msisdnContact = entities.Contact.FirstOrDefault(c => c.Value == msisdn && c.Type == (int)ContactType.Phone);
				if (msisdnContact == null)
					return new List<MessageWeb>();

				firstMessageId = firstMessageId ?? entities.MESSAGE
					.OrderByDescending(m => m.MESSAGE_ID)
					.Select(m => m.MESSAGE_ID)
					.Take(1)
					.FirstOrDefault();

				var messagesQuery = entities.MESSAGE
					.Include(MESSAGE.MESSAGE_TEMPLATEIncludePath)
					.Include(MESSAGE.OwnerOperatorIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath)
					.Include(MESSAGE.MessageFieldIncludePath + "." + MESSAGE_FIELD.MESSAGE_TEMPLATE_FIELDIncludePath)
					.Include(MESSAGE.ContactsIncludePath)
					.Include(MESSAGE.ContactsIncludePath + "." + Message_Contact.ContactIncludePath)
					.Where(m => m.MESSAGE_ID <= firstMessageId.Value && m.BODY != null);
				messagesQuery = messagesQuery.Where(
						m => m.Contacts.Any(
								c => c.Contact_ID == msisdnContact.ID &&
									(c.Type == (int)MessageContactType.Destination || c.Type == (int)MessageContactType.Source)));
				if (pagination != null)
					pagination.TotalRecords = messagesQuery.Count();
				messagesQuery = messagesQuery.OrderByDescending(m => m.MESSAGE_ID);
				if (pagination != null)
					messagesQuery = messagesQuery.Skip(pagination.Skip).Take(pagination.RecordsPerPage);

				return messagesQuery.ToList().Select(m => m.ToWeb()).ToList();
			}
		}
		public List<MessageWeb> GetUpdatedMessages(DateTime from, DateTime to)
		{
			//TODO: использовать не даты, а timestamp
			return databaseManager.GetUpdatedMessages(from, to);
		}
		private BusinessLogic.DTO.Messages.Message[] ReadMessages(IDataReader reader, int messagesCount)
		{
			var messages = new List<BusinessLogic.DTO.Messages.Message>(messagesCount);

			var ordinalId              = reader.GetOrdinal("MESSAGE_ID");
			var ordinalTime            = reader.GetOrdinal("TIME");
			var ordinalBody            = reader.GetOrdinal("BODY");
			var ordinalSubj            = reader.GetOrdinal("SUBJECT");
			var ordinalTemplateId      = reader.GetOrdinal("TEMPLATE_ID");
			var ordinalOwnerOperatorId = reader.GetOrdinal("Owner_Operator_ID");
			var ordinalSortKey         = reader.GetOrdinal("SORT_KEY");
			while (reader.Read())
			{
				var message = new BusinessLogic.DTO.Messages.Message
				{
					Id              = (int)reader[ordinalId],
					Time            = (DateTime)reader[ordinalTime],
					Body            = reader[ordinalBody] != DBNull.Value ? (string)reader[ordinalBody] : string.Empty,
					Subject         = reader[ordinalSubj] != DBNull.Value ? (string)reader[ordinalSubj] : string.Empty,
					TemplateId      = reader[ordinalTemplateId] != DBNull.Value ? (int?)reader[ordinalTemplateId] : null,
					OwnerOperatorId = reader[ordinalOwnerOperatorId] != DBNull.Value ? (int?)reader[ordinalOwnerOperatorId] : null,
					Fields          = new List<MessageField>(),
					//////////////////////////////////////////////
					SortKey         = (long)reader[ordinalSortKey]
				};

				if (message.TemplateId.HasValue)
					message.Template = MessageTemplates[message.TemplateId.Value];
				messages.Add(message);
			}

			return messages.ToArray();
		}
		private MessageField[] ReadMessageFields(IDataReader reader, int messageFieldsCount)
		{
			var messagesFields = new List<MessageField>(messageFieldsCount);
			var ordinalId                     = reader.GetOrdinal("MESSAGE_FIELD_ID");
			var ordinalNessageId              = reader.GetOrdinal("MESSAGE_ID");
			var ordinalMessageTemplateFieldId = reader.GetOrdinal("MESSAGE_TEMPLATE_FIELD_ID");
			var ordinalContent                = reader.GetOrdinal("CONTENT");
			//////////////////////////////////////////////
			var ordinalSortKey                = reader.GetOrdinal("SORT_KEY");
			while (reader.Read())
			{
				var messageField = new MessageField
				{
					Id              = (int)reader[ordinalId],
					MessageId       = (int)reader[ordinalNessageId],
					TemplateFieldId = (int)reader[ordinalMessageTemplateFieldId],
					Value           = reader[ordinalContent] != DBNull.Value ? (string)reader[ordinalContent] : string.Empty,
					//////////////////////////////////////////////
					SortKey         = (long)reader[ordinalSortKey]
				};

				messageField.Template = MessageTemplateFields[messageField.TemplateFieldId];
				messagesFields.Add(messageField);
			}

			return messagesFields.ToArray();
		}
		private int ReadCount(IDataReader reader)
		{
			reader.Read();
			return reader.GetInt32(0);
		}
		private void MergeFieldsToMessages(BusinessLogic.DTO.Messages.Message[] messages, MessageField[] messagesFields)
		{
			messagesFields = messagesFields
				.OrderBy(f => f.SortKey)
				.ToArray();
			var idToMessage = messages
				.ToDictionary(m => m.Id);
			foreach (var f in messagesFields)
			{
				BusinessLogic.DTO.Messages.Message message;
				if (!idToMessage.TryGetValue(f.MessageId, out message))
					continue;
				message.Fields.Add(f);
			}
		}
		public DataSet GetAnalytic(int route, DateTime from, DateTime to, string rtnum, string schnum)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure(
				"dbo.Analytic"))
			{
				if (route < 1) sp["@rtnum"].Value = rtnum;
				else if (rtnum != "") sp["@route"].Value = route;
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				if (schnum != "") sp["@schnum"].Value = schnum;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.Analytic);
				return dataSet;
			}
		}
		public DataSet GetWayoutInfo(string route, DateTime date)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure(
				"dbo.GetWayoutInfo"))
			{
				sp["@route_number"].Value = route;
				sp["@date"].Value = date;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.WayoutInfo);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение ID путевого листа по номеру и дате.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet WaybillNewIDGet(string for_Num, DateTime for_Date, int operatorId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.WaybillNewIDGet"))
			{
				sp["@for_Num"].Value = for_Num;
				sp["@for_Date"].Value = for_Date;
				if (operatorId > 0)
				{
					sp["@operator_id"].Value = operatorId;
				}
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.WaybillNewIDGet);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение списка выходов по ID маршрута и дате.
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet AnaliticsOutputsGet(int for_RouteID, DateTime for_Date)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AnaliticsOutputsGet"))
			{
				sp["@for_RouteID"].Value = for_RouteID;
				sp["@for_Date"].Value = for_Date;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.AnaliticsOutputsGet);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение списка всех маршрутов, проходящих через определенную точку на указанную дату.
		/// </summary>
		/// <param name="iPointID">ID точки, для которой требуется получить данные</param>
		/// <param name="iReportDateTime">Дата и время, на которые строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet RoutesListThroughPointGet(int iPointID, DateTime iReportDateTime)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.RoutesListThroughPointGet"))
			{
				sp["@PointID"].Value = iPointID;
				sp["@ReportDateTime"].Value = iReportDateTime;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.RoutesListThroughPointGet);
				return dataSet;
			}
		}
		/// <summary>
		/// инкремент поля PRINTED (количество печати) и указание причины в поле PRINTREASON таблицы WAYBILL_HEADER после печати ПЛ
		/// Добавление новой записи о печати ПЛ в таблицу истории H_WAYBILL_HEADER
		/// </summary>
		/// <param name="for_ID">ID путевого листа, который печатался</param>
		/// <param name="set_PrintReason">Причина печати (в соотв. с новой табл. REPRINT_REASON)</param>
		/// <param name="set_OperatorID">ID оператора, который произвел печать</param>
		public void WaybillPrintSet(int for_ID, int set_PrintReason, int set_OperatorID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.WaybillPrintSet"))
			{
				sp["@for_ID"].Value = for_ID;
				sp["@set_PrintReason"].Value = set_PrintReason;
				sp["@set_OperatorID"].Value = set_OperatorID;
				sp.ExecuteScalar();
			}
		}
		/// <summary>
		/// Получение данных для построения отчета о пропущенных и забракованных рейсах, на укзанную дату
		/// </summary>
		/// <param name="for_Date">Дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet MissedTripsDetailGet(DateTime for_Date)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MissedTripsDetailGet"))
			{
				sp["@for_Date"].Value = for_Date;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.MissedTripsDetailGet);
				return dataSet;
			}
		}
		/// <summary>
		/// Получение данных об интервалах недоступности GPRS на ТС.
		/// </summary>
		/// <param name="iReportDate">Дата, на которую требуется получить информацию из БД</param>
		/// <param name="iVehicleGroupID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <param name="iControllerTypeID">ID контроллера, для ТС с которым требуется построить отчет</param>
		/// <param name="iNeglibleInterval">Значение нерегистрируемого интервала отсутствия сигнала</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet GPRSInaccessibilityDataGet(DateTime iReportDate, int iVehicleGroupID, int iControllerTypeID, int iNeglibleInterval)
		{
			using (StoredProcedure clsStoredProcedure = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GPRSInaccessibilityDataGet"))
			{
				clsStoredProcedure["@ReportDate"].Value = iReportDate;
				clsStoredProcedure["@VehicleGroupID"].Value = iVehicleGroupID;
				clsStoredProcedure["@ControllerTypeID"].Value = iControllerTypeID;
				clsStoredProcedure["@NeglibleInterval"].Value = iNeglibleInterval;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.GPRSInaccessibilityDataGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Получение количества сходов и браков для ТС указанной автоколонны за указанный период времени.
		/// </summary>
		/// <param name="iReportDateFrom">Начало периода, по которому строится отчет</param>
		/// <param name="iReportDateTo">Окончание периода, по которому строится отчет</param>
		/// <param name="iMotorcadeID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet OutsAndRejectsCountGet(DateTime iReportDateFrom, DateTime iReportDateTo, int iMotorcadeID)
		{
			using (StoredProcedure clsStoredProcedure = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.OutsAndRejectsCountGet"))
			{
				clsStoredProcedure["@ReportDateFrom"].Value = iReportDateFrom;
				clsStoredProcedure["@ReportDateTo"].Value = iReportDateTo;
				clsStoredProcedure["@MotorcadeID"].Value = iMotorcadeID;
				DataSet dsResultData = clsStoredProcedure.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dsResultData, Tables.OutsAndRejectsCountGet);
				return dsResultData;
			}
		}
		/// <summary>
		/// Процедура выборки всех или закрепленных за пользователем GUID-ов отчетов по заданному ID оператора
		/// </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet GetGuidsReportsForUser()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetGuidsReportsForUser"))
			{
				sp["@for_OperatorID"].Value = 0; // Для сервера выбераем все GUID-ы

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetGuidsReportsForUser);
				return dataSet;
			}
		}
		/// <summary>
		/// Возвращает набор данных, в котором содержится вся информация о зонах для выбранного ТС
		/// </summary>
		/// <param name="vehicle_id">идентификатор ТС в БД [VEHICLE.VEHICLE_ID]</param>
		/// <returns>набор данных, в котором содержится вся информация о зонах для выбранного ТС</returns>
		public virtual DataSet GetZones(int vehicle_id)
		{
			using (StoredProcedure sp = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetZones"))
			{
				sp["@vehicle_id"].Value = vehicle_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetZones);
				return dataSet;
			}
		}
		/// <summary>
		/// Возвращает все группы ТС доступные оператору
		/// </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public DataSet GetVehicleGroups(int operatorId)
		{
			using (StoredProcedure sp = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehicleGroups"))
			{
				sp["@operator_id"].Value = operatorId;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, new string[] { "VEHICLEGROUP" });
				return dataSet;
			}
		}
		/// <summary>
		/// выполняет процедуру dbo.GetRules
		/// </summary>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		public DataSet GetRules()
		{
			using (StoredProcedure sp = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetRules"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetRules);
				return ds;
			}
		}
		#region Универсальная функция для вызова хранимых процедур для отчетов
		/// <summary>
		/// Универсальная Функция вызова процедур для отчетов
		/// </summary>
		/// <param name="arParams">Список параметров для процедуры</param>
		/// <param name="strStoredProcedureName">Название вызываемой процедуры</param>
		/// <param name="strTablesNames">Список имен таблиц для переименования</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		public DataSet GetDataFromDB(ArrayList arParams, string strStoredProcedureName, string[] strTablesNames)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure(strStoredProcedureName))
			{
				foreach (ParamValue par in arParams)
				{
					var value = par.value;
					if (value != null && value.GetType() == typeof(DataTable))
					{
						sp.SetTableValuedParameter(par.strParam, (DataTable)value);
					}
					else
					{
						sp[par.strParam].Value = value;
					}
				}

				DataSet dataSet = sp.ExecuteDataSet();

				// Для изменяющих процедур, которые ничего не возвращают
				if (strTablesNames == null)
					return dataSet;
				DatasetHelperBase.RenameTables(dataSet, strTablesNames);

				return dataSet;
			}
		}
		#endregion
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null)
		{
			lat = default(double?);
			lng = default(double?);
			var errorResult = GetLatLngByAddressResult.UnknownError;
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return errorResult;
			try
			{
				return geoClient.GetLatLngByAddress(address, out lat, out lng, mapGuid);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return errorResult;

		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null)
		{
			var emptyResult = new AddressResponse[0];
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return emptyResult;
			try
			{
				return geoClient.GetAddressesByText(searchText, language, mapGuid);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return emptyResult;
		}
		public string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false)
		{
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return null;

			var address = default(string);
			try
			{
				address = geoClient.GetAddressByPoint(lat: lat, lng: lng, language, mapGuid);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return address;
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null)
		{
			var emptyResult = new AddressResponse[0];
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return emptyResult;

			try
			{
				return geoClient.GetAddresses(requests, culture, mapGuid);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return emptyResult;
		}
		public DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null)
		{
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return dataSet;

			try
			{
				return geoClient.FillAddressColumn(
					dataSet, tableName, lngColumnName, latColumnName, addressColumnName, language, mapGuid);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return dataSet;
		}
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return null;

			var result = new ArrivalTimesToPoint { PointLatLng = destination };
			try
			{
				result = geoClient.GetArrivalTimesToPoint(destination, origins, language);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return result;
		}
		public DataSet FillGeoZoneColumn(
			DataSet dataSet,
			int operatorId,
			int? vehicleId,
			string tableName,
			string lngColumnName,
			string latColumnName,
			string dataColumnName)
		{
			return GetPositionString(dataSet, tableName, lngColumnName, latColumnName, dataColumnName,
				(lat, lng) => GeoHelper.GetGeoZonesString(lat, lng, operatorId));
		}
		public DataSet FillGeoPointColumn(
			DataSet dataSet,
			int operatorId,
			int? vehicleId,
			int? radius,
			string tableName,
			string lngColumnName,
			string latColumnName,
			string dataColumnName)
		{
			return GetPositionString(dataSet, tableName, lngColumnName, latColumnName, dataColumnName,
				(lat, lng) => GeoHelper.GetNearestPointsString(lat, lng, radius, operatorId));
		}
		private static DataSet GetPositionString(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string dataColumnName, Func<double, double, string> func)
		{
			var table = dataSet.Tables[tableName];
			var lngColumn = table.Columns[lngColumnName];
			var latColumn = table.Columns[latColumnName];

			DataColumn dataColumn =
				table.Columns.Contains(dataColumnName)
					? table.Columns[dataColumnName]
					: table.Columns.Add(dataColumnName, typeof(string));

			foreach (DataRow dataRow in table.Rows)
			{
				if (dataRow[latColumn] == DBNull.Value ||
					dataRow[lngColumn] == DBNull.Value)
					continue;

				var lat = Convert.ToDouble(dataRow[latColumn]);
				var lng = Convert.ToDouble(dataRow[lngColumn]);

				dataRow[dataColumn] = func(lat, lng);
			}

			table.AcceptChanges();

			return dataSet;
		}

		public GeozoneGroup GetZoneGroupById(int geozonegroupId)
		{
			using (var entities = new Entities())
			{
				return entities.GetZoneGroup(geozonegroupId)
					?.Select(zg => new GeozoneGroup
					{
						id          = zg.ZONEGROUP_ID,
						name        = zg.NAME,
						description = zg.DESCRIPTION
					})
					?.FirstOrDefault();
			}
		}
		public GeoZone GetGeoZoneById(int geozoneId)
		{
			using var entities = new Entities();
			var ormZode = entities.GetZone(geozoneId).FirstOrDefault();
			return ormZode != default
				? new GeoZone
				{
					Id          = geozoneId,
					Name        = ormZode.NAME,
					Description = ormZode.DESCRIPTION }
				: default;
		}
		public List<GeoZone> GetGeoZonesByGroup(int operatorId, int geoZoneGroupId)
		{
			using var entities = new Entities();
			return entities.GetGeoZonesByGroup(operatorId, geoZoneGroupId)
				.Select(z => new GeoZone
				{
					Id          = z.ZONE_ID,
					Name        = z.NAME,
					Description = z.DESCRIPTION,
				})
				.ToList();
		}
		public List<GeoZone> GetGeoZones(int operatorId, int? departmentId, bool includeVertices, int? zoneId = null, bool includeIgnored = false, bool loadSingle = false)
		{
			var zonesDataSet = GetZonesWebForOperator(operatorId, departmentId, includeVertices, zoneId ?? 0, includeIgnored, loadSingle);
			if (zonesDataSet == null || zonesDataSet.Tables.Count < (includeVertices ? 2 : 1))
				return null;

			var zTable = zonesDataSet.Tables["Zone"];
			var result = new List<GeoZone>(zTable.Rows.Count);
			foreach (DataRow zr in zTable.Rows)
			{
				GeoZone zone = CreateGeoZoneDto(zonesDataSet, (int)zr["ZONE_ID"], includeVertices);
				if (zone != null)
					result.Add(zone);
			}
			return result;
		}
		public List<GeoZoneForPosition> GetGeoZonesForPositions(
			IEnumerable<VehicleZoneParam> vehicleZoneParams, IEnumerable<VehiclePositionParam> vehiclePositionParams)
		{
			return Server.Instance().DbManager.GetGeoZonesForPositions(vehicleZoneParams, vehiclePositionParams);
		}
		public List<GeoZoneForPosition> GetGeoZonesForPositions(
			IEnumerable<VehicleZoneParam> vehicleZoneParams, IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams)
		{
			return Server.Instance().DbManager.GetGeoZonesForVehicleLogTime(vehicleZoneParams, vehicleLogTimeParams);
		}
		public DataSet GetAllZoneDistanceStandarts(int operatorId)
		{
			return Server.Instance().DbManager.GetAllZoneDistanceStandarts(operatorId);
		}
		public DataSet GetZonesWebForOperator(int operatorId, int? departmentId, bool vertexGet, int zoneID, bool includeIgnored, bool loadSingle)
		{
			var arParams = new ArrayList();
			arParams.Add(new ParamValue("@operator_id", operatorId));
			arParams.Add(new ParamValue("@vertexGet", vertexGet));
			arParams.Add(new ParamValue("@zone_id", zoneID));
			arParams.Add(new ParamValue("@includeIgnored", includeIgnored));
			arParams.Add(new ParamValue("@loadSingle", loadSingle));
			if (departmentId.HasValue)
				arParams.Add(new ParamValue("@department_id", departmentId.Value));

			DataSet ds;
			if (vertexGet)
			{
				ds = GetDataFromDB(arParams, "GetZone", new[] { "Zone", "ZonePoint" });
				CentreCalc(ds);
			}
			else
			{
				ds = GetDataFromDB(arParams, "GetZone", new[] { "Zone" });
			}

			return ds;
		}
		private void CentreCalc(DataSet ds)
		{
			ds.Tables["Zone"].Columns.Add(new DataColumn("CentreX", typeof(double)));
			ds.Tables["Zone"].Columns.Add(new DataColumn("CentreY", typeof(double)));

			foreach (DataRow drZone in ds.Tables["Zone"].Rows)
			{
				DPointList pointList = new DPointList();
				DPoint pointCentr = new DPoint();

				DataRow[] drsZonePoints = ds.Tables["ZonePoint"].Select("ZONE_ID = " + drZone["ZONE_ID"]);
				if (drsZonePoints == null || drsZonePoints.Length < 1)
					continue;
				pointList.setSize(drsZonePoints.Length);
				for (int i = 0; i < drsZonePoints.Length; i++)
				{
					DPoint xy = new DPoint((float)(double)drsZonePoints[i]["X"], (float)(double)drsZonePoints[i]["Y"]);
					pointList.points[i] = xy;
				}
				double r;
				Math2D.findPointInPolygon(ref pointList, out pointCentr, out r);
				drZone["CentreX"] = pointCentr.x;
				drZone["CentreY"] = pointCentr.y;
			}
		}
		private GeoZone CreateGeoZoneDto(DataSet zone, int zoneId, bool includeVertices)
		{
			if (includeVertices && (zone.Tables.Count < 2 || zone.Tables[1].Rows.Count == 0) ||
				!includeVertices && zone.Tables.Count < 1)
			{
				return null;
			}
			DataRow zrow = zone.Tables[0].Select("ZONE_ID = " + zoneId.ToString(CultureInfo.InvariantCulture))[0];
			if (zrow == null)
			{
				return null;
			}

			var zoneDTO = new GeoZone();

			int zoneTypeId = 0;
			int color = 0;

			zoneDTO.Id = zoneId;
			zoneDTO.Name = zrow["NAME"].ToString();
			zoneDTO.Description = zrow["DESCRIPTION"].ToString();
			int.TryParse(zrow["ZONE_TYPE_ID"].ToString(), out zoneTypeId);
			zoneDTO.Type = (ZoneTypes)zoneTypeId;
			zoneDTO.Editable = (int)zrow["Editable"] == 1;

			int.TryParse(zrow["COLOR"].ToString(), out color);
			Color c = Color.FromArgb(color);

			if (includeVertices)
			{
				zoneDTO.Color = ColorTranslator.ToHtml(c);
				var pointsRows = zone.Tables[1].Select("ZONE_ID = " + zoneId);
				var points = new List<GeoZonePoint>();
				foreach (var row in pointsRows)
				{
					if (row.IsNull("X") || row.IsNull("Y"))
						continue;
					var point = new GeoZonePoint
					{
						Lng = (float)Convert.ToDouble(row["X"]),
						Lat = (float)Convert.ToDouble(row["Y"]),
						Radius = row["RADIUS"] != DBNull.Value ? (float)Convert.ToDouble(row["RADIUS"]) : (float?)null
					};

					if (point.Radius <= 0)
						point.Radius = null;
					points.Add(point);
				}

				zoneDTO.Points = points.ToArray();
			}

			return zoneDTO;
		}
		public void FillVehicleNames<T>(List<T> list,
			Expression<Func<T, int>> vehicleIdAccessor,
			Expression<Func<T, string>> vehicleNameAccessor,
			int operatorId,
			CultureInfo cultureInfo)
		{
			var memberVehicleIdSelectorExpression = (MemberExpression)vehicleIdAccessor.Body;
			var memberVehicleNameSelectorExpression = (MemberExpression)vehicleNameAccessor.Body;
			var vehicleIdProperty = memberVehicleIdSelectorExpression.Member as PropertyInfo;
			var vehicleIdField = memberVehicleIdSelectorExpression.Member as FieldInfo;
			var vehicleNameProperty = memberVehicleNameSelectorExpression.Member as PropertyInfo;
			var vehicleNameField = memberVehicleNameSelectorExpression.Member as FieldInfo;

			var args = new GetVehiclesArgs
			{
				VehicleIDs = list.Select(item => GetValueViaReflection<int>(vehicleIdProperty, vehicleIdField, item)).Where(id => id > 0).Distinct().ToArray(),
				Culture = cultureInfo
			};
			var vehicles = GetVehiclesWithPositions(operatorId, null, args).OrderBy(v => v.id).ToList();
			var orderedList = list.OrderBy(t => GetValueViaReflection<int>(vehicleIdProperty, vehicleIdField, t));
			var vehicleId = 0;
			var vehicleIndex = 0;
			foreach (var listItem in orderedList)
			{
				var currentVehicleId = GetValueViaReflection<int>(vehicleIdProperty, vehicleIdField, listItem);
				if (vehicleId < currentVehicleId)
				{
					while (vehicleIndex < vehicles.Count && vehicles[vehicleIndex].id < currentVehicleId)
						vehicleIndex++;
					vehicleId = vehicles[vehicleIndex].id;
				}

				var vehicleName = vehicleId == currentVehicleId ? vehicles[vehicleIndex].Name : string.Empty;
				SetValueViaReflection(vehicleNameProperty, vehicleNameField, listItem, vehicleName);
			}
		}
		private T GetValueViaReflection<T>(PropertyInfo property, FieldInfo field, object item)
		{
			var value = default(T);
			if (property != null)
			{
				value = (T)property.GetValue(item, null);
			}

			if (field != null)
			{
				value = (T)field.GetValue(item);
			}

			return value;
		}
		private void SetValueViaReflection(PropertyInfo property, FieldInfo field, object item, object value)
		{
			if (property != null)
			{
				property.SetValue(item, value, null);
			}

			if (field != null)
			{
				field.SetValue(item, value);
			}
		}
		public List<Group> GetVehicleGroupsWithVehicles(int operatorID, Department department, GetGroupsArguments arguments)
		{
			var vehicleGroupIds    = arguments != null ? arguments.Ids : null;
			var includeIgnored     = arguments != null && arguments.IncludeIgnored;
			var includeNotInGroups = arguments != null && arguments.IncludeNotInGroups;
			var includeAll         = arguments != null && arguments.IncludeAll;
			var includeStandard    = arguments != null && arguments.IncludeStandard;
			var cultureInfo        = arguments != null ? arguments.Culture : CultureInfo.CurrentCulture;
			var filterByDepartment =
				(arguments       == null || arguments.FilterByDepartment) &&
				(vehicleGroupIds == null || vehicleGroupIds.Length == 0);
			var departmentID = filterByDepartment && department != null ? department.id : (int?)null;

			using (var entities = new Entities())
			{
				// Право необходимое для отображение объектов наблюдения
				const int rightID = (int)SystemRight.VehicleAccess;

				// Запрос всех групп объектов наблюдения
				var vgs = (from vg in entities.VEHICLEGROUP select vg);

				// Если указаны конкретные группы объектов наблюдения, то отфильтровываем только их
				if (vehicleGroupIds != null && vehicleGroupIds.Length != 0)
					vgs = vgs.Where(vg => vehicleGroupIds.Contains(vg.VEHICLEGROUP_ID));

				// Если указан конкретный департамент, то отфильтровываем только группы этого департамента
				if (departmentID != null)
					vgs = vgs.Where(vg => vg.Department_ID == departmentID.Value);

				// Фильтруем группы для которых есть право VehicleAccess
				vgs = vgs.Where(vg =>
					entities.v_operator_vehicle_groups_right
						.Any(ovgr =>
							ovgr.operator_id     == operatorID &&
							ovgr.right_id        == rightID &&
							ovgr.vehiclegroup_id == vg.VEHICLEGROUP_ID));

				// Если не нужно показывать игнорируемые объекты, то убираем группы для которых установлено право SystemRight.Ignore
				if (!includeIgnored)
				{
					vgs = vgs
						.Where(vg =>
							!vg.OPERATOR_VEHICLEGROUP
								.Any(a =>
									a.OPERATOR_ID == operatorID &&
									a.RIGHT_ID    == (int)SystemRight.Ignore && a.ALLOWED));
				}

				// Получаем идентификаторы объектов в группах, с учетом всех фильтров выше
				var vgIDToVehicleIDs =
				(
					from vg in entities.VEHICLEGROUP
					from v in vg.VEHICLE
					group v.VEHICLE_ID by vg.VEHICLEGROUP_ID
						into vehicleIDsGroupedByVGID
					select vehicleIDsGroupedByVGID
				)
					.ToDictionary(item => item.Key);

				// Получаем перечень групп, которые можно редактировать из полученного списка групп
				const int editRightID = (int)SystemRight.EditGroup;
				var vehicleGroupsAllowedToEdit = new HashSet<int>(vgs
					.Where(vg =>
						entities.v_operator_vehicle_groups_right.Any(ovgr =>
							ovgr.operator_id == operatorID &&
							ovgr.right_id == editRightID &&
							ovgr.vehiclegroup_id == vg.VEHICLEGROUP_ID))
					.Select(vg => vg.VEHICLEGROUP_ID));

				// Результат по собственным группам
				var result = vgs
					.OrderBy(vg => vg.NAME)
					.ToList()
					.ConvertAll(vg =>
					{
						IGrouping<int, int> g;
						return new Group
						{
							Id          = vg.VEHICLEGROUP_ID,
							Name        = vg.NAME,
							Description = vg.COMMENT,
							ObjectIds   = vgIDToVehicleIDs.TryGetValue(vg.VEHICLEGROUP_ID, out g)
								? g.ToArray()
								: new int[] { },
							AllowEditContent = vehicleGroupsAllowedToEdit.Contains(vg.VEHICLEGROUP_ID),
							AllowAddToGroup  = vehicleGroupsAllowedToEdit.Contains(vg.VEHICLEGROUP_ID),
							UserDefined      = true
						};
					});

				// Если не нужны объекты вне групп, все объекты, и стандартных групп, возвращаем только собственные группы
				if (!includeNotInGroups && !includeAll && !includeStandard)
					return result;

				// Запрос всех объектов наблюдения, на которые есть права
				var allVehiclesQuery =
					entities.v_operator_vehicle_right.Where(
						ovr => ovr.operator_id == operatorID &&
							   ovr.right_id == (int)SystemRight.VehicleAccess)
						.Select(ovr => ovr.VEHICLE);

				// Если указан департамент, то фильтруем только объекты этого департамента
				if (null != departmentID)
					allVehiclesQuery = allVehiclesQuery.Where(v => v.Department_ID == departmentID);

				// Если не нужно показывать игнорируемые объекты, то убираем объекты для которых установлено право SystemRight.Ignore
				if (!includeIgnored)
				{
					allVehiclesQuery = allVehiclesQuery
						.Where(v =>
							!v.Accesses.Any(a =>
								a.operator_id == operatorID &&
								a.right_id == (int)SystemRight.Ignore));
				}

				var vids = new int[0];
				var strings = ResourceContainers.Get(cultureInfo);
				var ownVehicleIds = new HashSet<int>();
				// Если нужны стандартные группы, то формируем их
				if (includeStandard)
				{
					// Все объекты наблюдения
					var allVehicles = allVehiclesQuery
						.Select(v => new
						{
							id = v.VEHICLE_ID,
							vehicleKind   = v.VEHICLE_KIND.VEHICLE_KIND_ID,
							departmentId  = v.Department_ID,
							ownOperatorId = (int?)v.CONTROLLER.MLP_Controller.Asid.OPERATOR.OPERATOR_ID,
							isAdministrative = v.Accesses.Any(
								r => r.right_id == (int)SystemRight.SecurityAdministration &&
									 r.operator_id == operatorID)
						})
						.ToList()
						.ConvertAll(v => new Vehicle
						{
							id           = v.id,
							vehicleKind  = (VehicleKind)v.vehicleKind,
							departmentId = v.departmentId,
							IsOwnVehicle = v.ownOperatorId == operatorID,
							rights       = v.isAdministrative
								? new[] { SystemRight.SecurityAdministration }
								: new SystemRight[0]
						});
					vids = allVehicles.Select(v => v.id).ToArray();

					// Обнаруживаем категории объектов наблюдения
					foreach (var vehicle in allVehicles)
						vehicle.DetectCategory(department);

					// Формируем стандартные группы по категории исключая корпоративные машины
					var standardCategories =
						allVehicles
							.Where(v =>
								v.category != null                               &&
								v.category != VehicleCategory.Partner            &&
								v.category != VehicleCategory.Autopark           &&
								v.category != VehicleCategory.CorporativeTracker &&
								v.category != VehicleCategory.Own                &&
								// Исключаем корпорантов
								!v.departmentId.HasValue)
							.GroupBy(v => v.category.Value)
							.Select(g => new Group
							{
								Id          = -(int)(g.Key),
								Name        = strings[g.Key],
								ObjectIds   = g.Select(v => v.id).ToArray(),
								UserDefined = false
							})
							.OrderBy(g => g.Id);

					// Отделяем собственные объекты (Я)
					foreach (var vehicle in allVehicles.Where(v => v.category == VehicleCategory.Own))
						ownVehicleIds.Add(vehicle.id);

					// Добавляем в начало группу собственных объектов (Я) (похоже никогда не выполняется)
					var own = standardCategories.FirstOrDefault(x => x.Id == -(int)VehicleCategory.Own);
					if (own != null)
						result.Insert(0, own);

					// Добавляем стандартные группы по категории
					result.AddRange(standardCategories.Where(x => x.Id != -(int)VehicleCategory.Own));

					// Формируем стандартные группы по корпоративным машинам
					var standardCorporatives =
						allVehicles
							.Where(v => v.departmentId.HasValue && v.departmentId.Value != departmentID)
							.GroupBy(v => v.departmentId.Value)
							.Select(g => new Group
							{
								Id = -g.Key,
								Name = GetDepartment(g.Key)?.name ?? $@"{strings["NoName"]}({g.Key})",
								ObjectIds = g.Select(v => v.id).ToArray(),
								UserDefined = false
							})
							.OrderBy(g => g.Id);
					result.AddRange(standardCorporatives);
				}

				// Если нужна группа "Прочее", формируем её
				if (includeNotInGroups)
				{
					vids = allVehiclesQuery.Select(v => v.VEHICLE_ID).ToArray();

					var allInGroups = new HashSet<int>();
					foreach (var vid in result.SelectMany(g => g.ObjectIds))
						allInGroups.Add(vid);

					var notInGroupObjectIds = vids
						.Where(id => !allInGroups.Contains(id) &&
									 !ownVehicleIds.Contains(id))
						.ToArray();

					if (notInGroupObjectIds.Length != 0)
					{
						result.Add(new Group
						{
							Id          = Group.NotInGroupId,
							Name        = strings[VehicleCategory.Other],
							Description = strings["otherVehiclesDescription"],
							ObjectIds   = notInGroupObjectIds
						});
					}
				}
				// Если нужна группа "Все объекты", формируем её
				if (includeAll && result.All(g => g.ObjectIds.Length != vids.Length))
				{
					result.Add(new Group
					{
						Id        = Group.AllId,
						Name      = strings["All"],
						ObjectIds = vids
					});
				}

				return result;
			}
		}
		private readonly ConcurrentDictionary<int, int> _lastAlarmEnabledLogTime =
			new ConcurrentDictionary<int, int>();
		public List<Vehicle> GetVehiclesWithPositions(int operatorId, Department department, GetVehiclesArgs arguments)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"OperatorId:{operatorId},Department:{department?.name}".CallTraceMessage());
			// Проверяем возможность получения данных
			if (!Protection.ProtectionKey.CanGetData())
				throw new ApplicationException("Data getting is forbidden by license protection");

			// Подготавливаем параметры запроса данных из БД
			var departmentId       = department?.id;
			var vehicleIds         = arguments?.VehicleIDs;
			//var filterByDepartment = (arguments != null && (arguments.FilterByDepartment ?? false)) && (vehicleIds == null || vehicleIds.Length == 0);
			var filterByDepartment =
				(arguments?.FilterByDepartment ?? false)
					&&
				(0 == (vehicleIds?.Length ?? 0));
			var currentDate        = arguments?.CurrentDate;
			var includeAttributes  = arguments?.IncludeAttributes ?? true;
			var includeAddresses   = arguments?.IncludeAddresses  ?? false;
			var includeLastData    = arguments?.IncludeLastData   ?? false;
			var includeZones       = arguments?.IncludeZones      ?? false;
			var includeThumbnails  = arguments?.IncludeThumbnails ?? false;
			var cultureInfo        = arguments?.Culture;
			var objectsFilter      = arguments?.ObjectsFilter ?? VehicleObjectsFilter.Active;
			var onlineFilter       = arguments?.OnlineFilter  ?? VehicleOnlineFilter.Online | VehicleOnlineFilter.Offline;

			// Получаем данные из БД
			var vehicles = databaseManager.GetVehiclesWithPositions(
				operatorId,
				vehicleIds,
				filterByDepartment ? departmentId : null,
				includeLastData,
				includeAttributes,
				currentDate,
				includeZones);

			// Заполняем параметры дополнительные параметры
			foreach (var vehicle in vehicles)
			{
				// Сразу обновляем координаты и датчики, т.к. от этого зависят адреса и прочее
				UpdateVehicleWithTerminalData(vehicle);
				// Заполняем простые свойства зависящие от входных параметров
				vehicle.AttributesIncluded = includeAttributes;
				vehicle.LastDataIncluded   = includeLastData;
				// Если нет прав ни на просмотр датчиков ни на редактирование удаляем все значения
				if (!IsAllowedVehicleAll(operatorId, vehicle.id, UserRole.SensorViewRightsAll) &&
					!IsAllowedVehicleAll(operatorId, vehicle.id, UserRole.SensorEditRightsAll))
				{
					vehicle.Sensors = new List<Sensor>();
				}
			}

			// Готовим параметры языка и других дополнительных данных для заполнения
			var msisdnToName = default(Dictionary<string, string>);
			var phoneBook    = default(Dictionary<string, string>);
			// Выполняем действия для которых необходимы дополнительные данные БД
			using (var entities = new Entities())
			{
				// Языковые параметры переданы не были, получаем их у оператора
				if (cultureInfo == null)
					cultureInfo = entities.GetOperatorCulture(operatorId) ?? CultureInfo.CurrentCulture;

				// Если нужны эскизы объектов
				if (includeThumbnails)
				{
					var vehIds = vehicles
						.Select(v => v.id)
						.ToArray();
					var vehPcs = entities
						.VEHICLE_PICTURE
						.Where(p => vehIds.Contains(p.VEHICLE_ID))
						.ToList()
						.Where(p => null != p.PICTURE);
					foreach (var item in vehicles.Join(vehPcs.GroupBy(p => p.VEHICLE_ID), v => v.id, p => p.Key, (v, p) => new { Veh = v, Pic = p }))
					{
						try
						{
							using (var imageStream = new MemoryStream(item.Pic.FirstOrDefault().PICTURE))
							{
								using (var image = Image.FromStream(imageStream))
								{
									using (var thumbnailImage = image.GetThumbnailImage(100, 100, () => true, IntPtr.Zero))
									{
										using (var thumbnailStream = new MemoryStream())
										{
											thumbnailImage.Save(thumbnailStream, ImageFormat.Jpeg);
											item.Veh.Thumbnail = Convert.ToBase64String(thumbnailStream.ToArray());
										}
									}
								}
							}
						}
						catch { }
					}
				}

				// Если нужны атрибуты
				if (includeAttributes)
				{
					var operatorAsid = databaseManager.GetOperatorAsid(
						operatorId, filterByDepartment ? departmentId : null);

					// Заполняем свойства для объекта текущего пользователя
					var operatorVehicle = vehicles.FirstOrDefault(v => v.asidId != null && operatorAsid.Id == v.asidId.Value);
					if (operatorVehicle != null)
					{
						operatorVehicle.IsCurrentUser = true;
						operatorAsid.IsBlocked = operatorVehicle.BillingBlocking != null;
					}

					// Рассчитываем возможности
					vehicles.ForEach(vehicle => FillCapability(vehicle, operatorAsid));

					// Определяем категории
					foreach (var vehicle in vehicles)
					{
						vehicle.DetectCategory(department);
						if (vehicle.asidId == null)
							continue;
						vehicle.IsCurrentUser = operatorAsid.Id == vehicle.asidId.Value;
					}

					// Очищаем информацию о биллинге, в зависимости от прав
					foreach (var vehicle in vehicles)
					{
						if (vehicle.rights.Contains(SystemRight.DepartmentsAccess) ||
							vehicle.rights.Contains(SystemRight.SecurityAdministration))
							continue;
						vehicle.billingServices = null;
						vehicle.BillingBlocking = null;
					}

					// Заполняем словарь перекодировки номера телефона в имя (имя контакта введенного в настройках пользователя)
					using (new Stopwatcher(nameof(SearchPhoneBook).GetCallerMethodNameMessage()))
						msisdnToName = entities.GetOperatorMsisdnToNameDictionary(operatorId);
					var msisdns = vehicles
						.Where(v => !string.IsNullOrEmpty(v.phone))
						.Select(v => v.phone)
						.ToArray();

					// Заполняем словарь перекодировки номера телефона в имя (имя контакта из телефонной книги пользователя)
					using (new Stopwatcher(nameof(SearchPhoneBook).GetCallerMethodNameMessage()))
						phoneBook = SearchPhoneBook(operatorId, msisdns);
				}
			}

			// Создаем набор контейнеров для перевода
			ResourceContainers resourceContainers = null;
			if (cultureInfo != null)
				resourceContainers = ResourceContainers.Get(cultureInfo);

			// Если нужны адреса
			if (includeAddresses)
				AssignAddresses(vehicles, cultureInfo, arguments.MapGuid);

			// Если указан интервал на котором объект считается с актуальными данными
			if (arguments?.OnlineTime != null)
			{
				foreach (var vehicle in vehicles)
				{
					if (vehicle.logTime == null)
					{
						vehicle.isOld = vehicle.vehicleKind != VehicleKind.MobilePhone;
						continue;
					}
					var logTime = TimeHelper.GetDateTimeUTC(vehicle.logTime.Value);
					TimeSpan diff = DateTime.UtcNow - logTime;
					vehicle.isOld = diff > arguments.OnlineTime.Value;
				}
			}

			// Если нужны последние данные и дата считающаяся текущей не выбрана
			if (includeLastData && currentDate == null)
			{
				foreach (var vehicle in vehicles)
					UpdateAlarmSensor(vehicle);
			}

			// Если нужны последние данные очищаем пустые датчики
			if (includeLastData)
			{
				foreach (var vehicle in vehicles)
				{
					// Нет датчиков переходим к следующему объекту
					if (vehicle.Sensors == null)
						continue;
					// Есть датчики с отсутствующими исходными данными, удаляем такие
					vehicle.Sensors.RemoveAll(s => s.LogRecords == null);
				}
			}

			// Замена номеров телефонов на имена из списка контактов
			if (msisdnToName != null || phoneBook != null)
			{
				foreach (var vehicle in vehicles)
				{
					if (string.IsNullOrWhiteSpace(vehicle.phone))
						continue;

					string name;
					if (phoneBook != null && phoneBook.TryGetValue(vehicle.phone, out name) &&
						!string.IsNullOrEmpty(name))
					{
						vehicle.garageNum = name;
						continue;
					}
					if (msisdnToName != null && msisdnToName.TryGetValue(vehicle.phone, out name) && !string.IsNullOrEmpty(name))
						vehicle.garageNum = name;
				}
			}

			// Если есть возможность перевести тексты переводим
			if (resourceContainers != null)
			{
				// Переводим команды всех объектов
				var commands = vehicles
					.Where(v => v.commands != null)
					.SelectMany(v => v.commands);
				foreach (var command in commands)
				{
					command.ResultText = command.Status == CmdStatus.Completed
						? resourceContainers[command.Result.Value, command.Type]
						: resourceContainers[command.Status];
				}
				// Перебираем объекты 1
				foreach (var vehicle in vehicles)
				{
					if (vehicle.controllerType != null)
						vehicle.controllerType.UserFriendlyName =
							resourceContainers.GetLocalizedText<ControllerType>(vehicle.controllerType.Name);

					if (vehicle.Sensors == null)
						continue;
					// Производим перевод всех датчиков
					foreach (var sensor in vehicle.Sensors)
						sensor.LocalizeSensorFull(resourceContainers);
				}

				// Перебираем объекты 2
				foreach (var vehicle in vehicles)
				{
					string title = string.Empty;
					/// Если объект пользователя, то ставим заголовок 'Я', в остальных ничего не меняем
					if (vehicle.IsOwnVehicle ?? false)
						title = resourceContainers["I"];

					var sb = new StringBuilder(title);
					var delimeter = sb.Length != 0 ? " " : "";

					if (!string.IsNullOrWhiteSpace(vehicle.garageNum))
						sb.Append(delimeter).Append(vehicle.garageNum);
					else if (!string.IsNullOrWhiteSpace(vehicle.phone))
						sb.Append(delimeter).Append(vehicle.phone);
					else if (!string.IsNullOrWhiteSpace(vehicle.ctrlNum))
						sb.Append(delimeter).Append(vehicle.ctrlNum);

					vehicle.Name = sb.Length != 0 ? sb.ToString() : resourceContainers["Noname"];

					if (vehicle.RenderedServices == null)
						continue;
					foreach (var usedService in vehicle.RenderedServices)
					{
						usedService.Name = resourceContainers[usedService.Type];
					}
				}

				// Перебираем объекты 3
				foreach (var vehicle in vehicles)
				{
					if (vehicle.capabilities == null)
						continue;
					foreach (var capability in vehicle.capabilities)
					{
						if (capability.Value.Allowed || capability.Value.DisallowingReason == null)
							continue;
						var reasonText = resourceContainers[capability.Value.DisallowingReason, capability.Key];
						switch (capability.Value.DisallowingReason.Value)
						{
							case CapabilityDisallowingReason.WaitRequired:
								var nextTime = capability.Value.WaitTill;
								var minutes = nextTime != null
									? (int)(nextTime.Value - DateTime.UtcNow).TotalMinutes
									: 1;
								if (minutes < 1)
									minutes = 1;

								reasonText = reasonText.Replace("$minutes$", minutes.ToString());
								break;
						}
						capability.Value.DisallowingReasonText = reasonText;
					}
				}
			}

			// Замена имени у самого себя, объект Я???
			foreach (var vehicle in vehicles.Where(v =>
				(v.IsOwnVehicle ?? false) &&
				v.Owner != null &&
				string.IsNullOrWhiteSpace(v.Owner.name) &&
				!string.IsNullOrWhiteSpace(v.Name)))
			{
				vehicle.Owner.name = vehicle.Name;
			}

			// Фильтруем объекты по статусу
			if ((objectsFilter & VehicleObjectsFilter.Ignored) == 0)
				vehicles.RemoveAll(v => v.rights != null && v.rights.Contains(SystemRight.Ignore));

			// Фильтруем объекты по статусу
			if ((objectsFilter & VehicleObjectsFilter.Active) == 0)
				vehicles.RemoveAll(v => v.rights != null && !v.rights.Contains(SystemRight.Ignore));

			// Фильтруем объекты по статусу
			if (includeLastData)
			{
				if ((onlineFilter & VehicleOnlineFilter.Online) == 0)
					vehicles.RemoveAll(v => v.Online ?? true);
				if ((onlineFilter & VehicleOnlineFilter.Offline) == 0)
					vehicles.RemoveAll(v => (!v.Online ?? true));
			}

			// Выполняем всех подписчиков этого события, уведомляем, что объекты запрошены
			VehiclesRequested?.Invoke(operatorId, arguments, vehicles);

			// Возвращаем результат
			return vehicles;
		}
		public void UpdateAlarmSensor(Vehicle vehicle)
		{
			var alarm = vehicle.Sensors.FirstOrDefault(x => x.Number == SensorLegend.Alarm);
			if (alarm?.LogRecords?.Any(x => x.Value != 0) ?? true)
				return;
			int lastAlarmEnabledLogTime;
			if (!_lastAlarmEnabledLogTime.TryGetValue(vehicle.id, out lastAlarmEnabledLogTime))
				return;
			//$"UpdateAlarmSensor({vehicle}, {lastAlarmEnabledLogTime.ToUtcDateTime():yyMMdd-HHmmss}, {vehicle.logTime.Value.ToUtcDateTime():yyMMdd-HHmmss})".CallTraceInformation(2);
			if (5 * 60 < vehicle.logTime - lastAlarmEnabledLogTime)
				return;
			alarm.LogRecords = new List<SensorLogRecord>
			{
				new SensorLogRecord(lastAlarmEnabledLogTime, 1)
			};
			//$"UpdateAlarmSensor({vehicle}, {lastAlarmEnabledLogTime.ToUtcDateTime():yyMMdd-HHmmss}////////////////////)".CallTraceInformation(2);
		}
		private void UpdateVehicleWithTerminalData(Vehicle dbv)
		{
			var lastMobileUnitUpdate = Server.Instance().GetLastPosition(dbv.id);
			if (null != lastMobileUnitUpdate)
			{
				var tmv = lastMobileUnitUpdate.ToDTO();
				// Обновление данных связанных с координатами и перемещением
				if ((dbv.posLogTime ?? 0) < (tmv.posLogTime ?? 0))
				{
					dbv.posLogTime   = tmv.posLogTime.HasValue   ? tmv.posLogTime   : dbv.posLogTime;
					dbv.isValid      = tmv.isValid.HasValue      ? tmv.isValid      : dbv.isValid;
					dbv.lat          = tmv.lat.HasValue          ? tmv.lat          : dbv.lat;
					dbv.lng          = tmv.lng.HasValue          ? tmv.lng          : dbv.lng;
					dbv.course       = tmv.course.HasValue       ? tmv.course       : dbv.course;
					dbv.speed        = tmv.speed.HasValue        ? tmv.speed        : dbv.speed;
					dbv.radius       = tmv.radius.HasValue       ? tmv.radius       : dbv.radius;
					dbv.PositionType = tmv.PositionType.HasValue ? tmv.PositionType : dbv.PositionType;
				}
				if ((dbv.logTime ?? 0) < (tmv.logTime ?? 0))
				{
					dbv.logTime = tmv.logTime.HasValue ? tmv.logTime : dbv.logTime;
				}
				// Если из БД датчики не поступили, то данные датчиков не обновляем
				if (0 == (dbv.Sensors?.Count ?? 0))
					return;
				// Если из терминала датчики не поступили, то данные датчиков не обновляем
				if (0 == (tmv.Sensors?.Count ?? 0))
					return;
				// Обновляем датчики (только значения, локализация вне этого метода)
				dbv.Sensors = dbv.Sensors ?? new List<Sensor>();
				foreach (var tmvSensor in tmv.Sensors)
				{
					var dbvSensor = dbv.Sensors.FirstOrDefault(s => s.Number == tmvSensor.Number);
					if (null == dbvSensor)
						dbv.Sensors.Add(tmvSensor);
					else
					{
						var tmvSensorLogRecord = tmvSensor.LogRecords?.OrderByDescending(r => r.LogTime)?.FirstOrDefault();
						if (tmvSensorLogRecord.HasValue)
						{
							var dbvSensorLogRecord = dbvSensor.LogRecords?.OrderByDescending(r => r.LogTime)?.FirstOrDefault();
							if (dbvSensorLogRecord.HasValue)
							{
								if (dbvSensorLogRecord.Value.LogTime < tmvSensorLogRecord.Value.LogTime)
								{
									dbvSensor.LogRecords.Clear();
									dbvSensor.LogRecords.Add(tmvSensorLogRecord.Value);
								}
							}
						}
					}
				}
			}
		}
		public List<VehiclePositionParam> GetPositions(
			IEnumerable<VehicleLogTimeParam> vehicleLogTimeParams, bool includeNeighbour, CancellationToken? cancellationToken = null)
		{
			if (!Protection.ProtectionKey.CanGetData())
				throw new ApplicationException("Data getting is forbidden by license protection");

			return databaseManager
				.GetPositions(vehicleLogTimeParams, includeNeighbour, cancellationToken)
				.Select(p => new VehiclePositionParam(p.VehicleId, p.LogTime, p.Lat, p.Lng))
				.ToList();
		}
		public event Action<int, GetVehiclesArgs, List<Vehicle>> VehiclesRequested;
		private static void AddCapability(Vehicle vehicle, DeviceCapability capability)
		{
			var vehicleCapability = vehicle.CreateCapability(capability);
			if (vehicleCapability == null)
				return;
			vehicle.capabilities.Add(capability, vehicleCapability);
		}
		private void FillCapability(Vehicle vehicle, CoreServerDatabaseManager.OperatorAsid operatorAsid)
		{
			vehicle.capabilities = new Dictionary<DeviceCapability, DeviceCapabilityOption>();

			AddCapability(vehicle, DeviceCapability.ChangeDeviceSettings);
			AddCapability(vehicle, DeviceCapability.CapturePicture);
			AddCapability(vehicle, DeviceCapability.PhoneCallBack);

			if (vehicle.vehicleKind == VehicleKind.WebCamera && (vehicle.HasIP ?? false))
			{
				vehicle.capabilities.Add(
					DeviceCapability.LivePicture,
					new DeviceCapabilityOption { Allowed = true });

				vehicle.capabilities.Add(
					DeviceCapability.VideoArchive,
					new DeviceCapabilityOption { Allowed = true });
			}

			if (vehicle.vehicleKind != VehicleKind.MobilePhone &&
			vehicle.vehicleKind != VehicleKind.WebCamera &&
			vehicle.vehicleKind != VehicleKind.ResourceMeter)
				vehicle.capabilities.Add(
					DeviceCapability.SpeedMonitoring,
					new DeviceCapabilityOption { Allowed = true });
			//TODO: проверять, настроен ли на устройстве маппинг датчика зажигания
			//TODO: Если датчик не настроен, нужно указать Allowed = false
			//TODO: и причину - датчик зажигания не настроен
			if (vehicle.vehicleKind != VehicleKind.MobilePhone &&
				vehicle.vehicleKind != VehicleKind.Tracker &&
				vehicle.vehicleKind != VehicleKind.WebCamera &&
				vehicle.vehicleKind != VehicleKind.ResourceMeter)
				vehicle.capabilities.Add(
					DeviceCapability.IgnitionMonitoring,
					new DeviceCapabilityOption { Allowed = true });


			if (vehicle.asidId != null ||
				vehicle.controllerType != null &&
				(vehicle.controllerType.SupportsPositionAsking ?? false))
			{
				DateTime? waitTill;
				var positionAskingDisallowReason = GetPositionAskingDisallowReason(
					vehicle, operatorAsid, out waitTill);

				if (positionAskingDisallowReason == null && !(vehicle.allowMlpRequest ?? true))
					positionAskingDisallowReason = CapabilityDisallowingReason.PositionAskingIsPaused;

				vehicle.capabilities.Add(
					DeviceCapability.AllowAskPosition,
					new DeviceCapabilityOption
					{
						Allowed = positionAskingDisallowReason == null,
						DisallowingReason = positionAskingDisallowReason,
						WaitTill = waitTill
					});
			}

			if (vehicle.asidId != null)
			{
				DateTime? waitTill;
				var smsSendingDisallowReason = GetSmsSendingDisallowReason(vehicle, operatorAsid, out waitTill);
				vehicle.capabilities.Add(
					DeviceCapability.AllowSendSms,
					new DeviceCapabilityOption
					{
						Allowed = smsSendingDisallowReason == null,
						DisallowingReason = smsSendingDisallowReason,
						WaitTill = waitTill
					});
			}

			if (vehicle.controllerType != null && vehicle.controllerType.Name == ControllerType.Names.SoftTracker)
				vehicle.capabilities.Add(DeviceCapability.ScheduledTracking, new DeviceCapabilityOption { Allowed = true });
		}
		private void AssignAddresses(List<Vehicle> vehicles, CultureInfo culture, Guid? mapGuid)
		{
			var geoClient = GeoClient.Default;
			if (geoClient == null)
				return;
			try
			{
				var onlineGeocoding4All = Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All);
				var addresses = geoClient.GetAddresses(
					vehicles
						.Where(v =>
						v.posLogTime != null &&
						v.lat.HasValue       &&
						v.lng.HasValue       &&
						GeoHelper.ValidPosition(lat: v.lat.Value, lng: v.lng.Value))
						.Select(v => new AddressRequest(
							v.id,
							lat: v.lat.Value,
							lng: v.lng.Value,
							!((v.IsOnlineGeocoding ?? false) || onlineGeocoding4All)))
						.ToArray(),
					culture, mapGuid)
					.ToDictionary(ar => ar.Id, ar => ar.Address);

				foreach (var v in vehicles)
				{
					string address;
					if (addresses.TryGetValue(v.id, out address))
						v.address = address;
				}
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}
		private static CapabilityDisallowingReason? CheckServicesAndBlockings(
			Vehicle vehicle,
			CoreServerDatabaseManager.OperatorAsid operatorAsid,
			Predicate<BillingService> isApproriateOperatorService,
			Predicate<BillingService> isApproriateBillingService, DeviceCapability capability,
			out DateTime? waitTill)
		{
			waitTill = null;
			if (vehicle.BillingBlocking != null)
				return CapabilityDisallowingReason.VehicleIsBlocked;

			if (operatorAsid.IsBlocked)
				return CapabilityDisallowingReason.OperatorIsBlocked;

			//Проверить услугу на операторе

			foreach (var operatorService in operatorAsid.Services)
			{
				if (!isApproriateOperatorService(operatorService))
					continue;

				DateTime? currentWaitTill;
				if (!IsWaitRequired(capability, operatorService, out currentWaitTill))
					return null; //Имеется услуга и не надо ничего ждать

				if (currentWaitTill != null && (waitTill == null || currentWaitTill.Value < waitTill.Value))
					waitTill = currentWaitTill;
			}


			CapabilityDisallowingReason? result = CapabilityDisallowingReason.NoServiceOnOperator;

			if (!vehicle.rights.Contains(SystemRight.PayForCaller))
				return result;

			if (vehicle.billingServices == null)
				return CapabilityDisallowingReason.NoServiceOnVehicle;

			foreach (var billingService in vehicle.billingServices)
			{
				if (!isApproriateBillingService(billingService))
					continue;

				DateTime? currentWaitTill;
				if (!IsWaitRequired(capability, billingService, out currentWaitTill))
					return null; //Имеется услуга и не надо ничего ждать

				if (currentWaitTill != null && (waitTill == null || currentWaitTill.Value < waitTill.Value))
					waitTill = currentWaitTill;
			}

			if (waitTill == null)
				return CapabilityDisallowingReason.NoServiceOnVehicle;

			return CapabilityDisallowingReason.WaitRequired;
		}
		private static bool IsWaitRequired(DeviceCapability capability, BillingService billingService, out DateTime? waitTill)
		{
			waitTill = null;
			if (billingService.MinIntervalSeconds == null ||
				billingService.Status == null)
			{
				return false;
			}

			var serviceTypeCategory = GetRenderedServiceType(capability);
			if (serviceTypeCategory == null)
			{
				return false;
			}

			BillingServiceStatus serviceStatus;
			if (!billingService.Status.TryGetValue(serviceTypeCategory.Value, out serviceStatus))
				return false;

			waitTill = serviceStatus.NextTime;
			return DateTime.UtcNow < serviceStatus.NextTime;
		}
		private static CapabilityDisallowingReason? GetSmsSendingDisallowReason(Vehicle vehicle, CoreServerDatabaseManager.OperatorAsid operatorAsid, out DateTime? waitTill)
		{
			return CheckServicesAndBlockings(vehicle, operatorAsid, x => x.Sms ?? false, x => x.Sms ?? false, DeviceCapability.AllowSendSms, out waitTill);
		}
		public Dictionary<string, string> SearchPhoneBook(int operatorId, string[] msisdns = null)
		{
			Dictionary<string, string> phoneBook = null;
			var phoneBookContacts = databaseManager.SearchPhoneBookContacts(operatorId, msisdns);
			if (phoneBookContacts.Any())
			{
				phoneBook = new Dictionary<string, string>();
				foreach (var phoneBookContact in phoneBookContacts)
					if (!phoneBook.ContainsKey(phoneBookContact.Value))
						phoneBook.Add(phoneBookContact.Value, phoneBookContact.Name);
			}

			return phoneBook;
		}
		private readonly TimingCache<CmdType, TimeSpan?> _minIntervalCache =
			new TimingCache<CmdType, TimeSpan?>(TimeSpan.FromDays(1), cmdType =>
			{
				using (var entities = new Entities())
				{
					var minIntervalSeconds = entities.CommandTypes
						.Where(t => t.id == (int)cmdType)
						.Select(t => t.MinIntervalSeconds)
						.FirstOrDefault();
					return minIntervalSeconds != null
						? TimeSpan.FromSeconds(minIntervalSeconds.Value)
						: (TimeSpan?)null;
				}
			});
		private TimeSpan? GetMinInterval(CmdType cmdType)
		{
			return _minIntervalCache[cmdType];
		}
		private CapabilityDisallowingReason? GetPositionAskingDisallowReason(Vehicle vehicle, CoreServerDatabaseManager.OperatorAsid operatorAsid, out DateTime? waitTill)
		{
			var commandMinInterval = GetMinInterval(CmdType.AskPosition) ?? TimeSpan.FromMinutes(1);

			if (vehicle.commands != null)
			{
				var lastCommand = vehicle.commands.FirstOrDefault(c => c.Type == CmdType.AskPosition);
				if (lastCommand != null && lastCommand.ResultDate != null)
				{
					var timeSinceLastCommand = DateTime.UtcNow - lastCommand.ResultDate.Value;
					if (timeSinceLastCommand < commandMinInterval)
					{
						waitTill = lastCommand.ResultDate.Value.Add(commandMinInterval);
						return CapabilityDisallowingReason.WaitRequired;
					}
				}
			}

			var servicesReason = CheckServicesAndBlockings(vehicle, operatorAsid, x => x.Lbs ?? false, x => x.Lbs ?? false, DeviceCapability.AllowAskPosition, out waitTill);
			if (servicesReason != null)
				return servicesReason;

			if ((vehicle.asidId == null || !(vehicle.KnownMaskedMsisdn ?? false)) &&
				(string.IsNullOrWhiteSpace(vehicle.phone) && (vehicle.controllerType == null || !(vehicle.controllerType.SupportsPositionAsking ?? false))))
				return CapabilityDisallowingReason.UnknownPhoneNumber;

			if (vehicle.posLogTime != null)
			{
				var secondsSinceLastPosition = TimeHelper.GetSecondsFromBase() - vehicle.posLogTime.Value;
				if (secondsSinceLastPosition < commandMinInterval.TotalSeconds)
				{
					waitTill = DateTime.UtcNow.AddSeconds(commandMinInterval.TotalSeconds - secondsSinceLastPosition);
					return CapabilityDisallowingReason.WaitRequired;
				}
			}

			return null;
		}
		private static RenderedServiceType? GetRenderedServiceType(DeviceCapability capability)
		{
			switch (capability)
			{
				case DeviceCapability.AllowAskPosition:
					return RenderedServiceType.LBS;
				case DeviceCapability.AllowSendSms:
					return RenderedServiceType.SMS;
				default:
					return null;
			}
		}
		public FullLog GetFullLog(int operatorId, int vehicleId, int logTimeFrom, int logTimeTo, FullLogOptions options, CultureInfo cultureInfo = null)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"OperatorId:{operatorId},VehicleId:{vehicleId},UtcBeg:{logTimeFrom.ToUtcDateTime()},UtcEnd:{logTimeTo.ToUtcDateTime()}".CallTraceMessage());
			if (!Protection.ProtectionKey.CanGetData())
				throw new ApplicationException("Data getting is forbidden by license protection");

			if (options == null)
				throw new ArgumentNullException("options");

			if (!options.StopPeriod.HasValue)
				using (var entities = new Entities())
					options.StopPeriod = entities.GetStopPeriod();

			var fullLog = databaseManager.GetFullLog(vehicleId, logTimeFrom, logTimeTo, options);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/// Убираем датчики если нет прав
			if (!IsAllowedVehicleAll(operatorId, vehicleId, UserRole.SensorViewRightsAll) &&
				!IsAllowedVehicleAll(operatorId, vehicleId, UserRole.SensorEditRightsAll))
			{
				fullLog.SensorMappings = new Dictionary<SensorLegend, List<SensorMap>>();
				fullLog.Sensors        = new Dictionary<SensorLegend, Sensor>();
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (cultureInfo != null && fullLog.Sensors != null)
			{
				var resourceContainers = ResourceContainers.Get(cultureInfo);
				foreach (var sensor in fullLog.Sensors.Values)
					sensor.LocalizeSensorFull(resourceContainers);
			}
			return fullLog;
		}
		public List<RunLogRecord> GetRunLog(IEnumerable<int> vehicleIds, IEnumerable<LogTimeSpanParam> logTimeSpanParams, bool includeAdjacent)
		{
			return databaseManager.GetRunLog(vehicleIds, logTimeSpanParams, includeAdjacent);
		}
		public List<RunLogRecord> GetRunLogForGroup(int vehicleGroupId, int logTimeFrom, int logTimeTo)
		{
			return databaseManager.GetRunLogForGroup(vehicleGroupId, logTimeFrom, logTimeTo);
		}
		public List<RunLogRecord> GetRunLogForVehicle(int vehicleId, int logTimeFrom, int logTimeTo)
		{
			return databaseManager.GetRunLogForVehicle(vehicleId, logTimeFrom, logTimeTo);
		}
		public Vehicle GetVehicleById(int operatorId, int vehicleId, CultureInfo cultureInfo)
		{
			var args = new GetVehiclesArgs { VehicleIDs = new[] { vehicleId }, Culture = cultureInfo };
			var vehicle = GetVehiclesWithPositions(operatorId, null, args)
				.FirstOrDefault();
			if (vehicle == null)
				throw new VehicleNotFoundException("Vehicle not found by id " + vehicleId + " for operator " + operatorId);
			return vehicle;
		}
		public VehicleGroup GetVehicleGroupById(int vehicleGroupId)
		{
			using (var entities = new Entities())
			{
				return
					entities.VEHICLEGROUP
						.Where(x => x.VEHICLEGROUP_ID == vehicleGroupId)
						.Select(x => new VehicleGroup { id = x.VEHICLEGROUP_ID, name = x.NAME })
						.FirstOrDefault();
			}
		}
		public List<Vehicle> GetVehiclesByVehicleGroup(int vehicleGroupId, int operatorId, CultureInfo culture, params SystemRight[] systemRights)
		{
			int[] ids;
			var rights = systemRights.Cast<int>().ToArray();
			using (var entities = new Entities())
			{
				var idsQuery =
				(
					from vg in entities.VEHICLEGROUP
					from v in vg.VEHICLE
					where vg.VEHICLEGROUP_ID == vehicleGroupId
						 && v.Accesses.Any(r => r.right_id == (int)SystemRight.VehicleAccess && r.operator_id == operatorId)
					select v.VEHICLE_ID
				);

				if (rights.Any())
				{
					var rightCount = rights.Length;
					idsQuery = from id in idsQuery
							   where rightCount ==
						  (
								from r in entities.v_operator_vehicle_right
								where r.operator_id == operatorId && r.vehicle_id == id
									&& (rights.Any(right => r.right_id == right))
								select r.vehicle_id
						  ).Count()
							   select id;
				}

				ids = idsQuery.ToArray();
			}

			List<Vehicle> vehicles;
			if (ids.Length > 0)
			{
				var vehicleArgs = new GetVehiclesArgs
				{
					VehicleIDs = ids,
					Culture    = culture
				};
				vehicles = GetVehiclesWithPositions(operatorId, null, vehicleArgs);
			}
			else
			{
				vehicles = new List<Vehicle>();
			}

			return vehicles;
		}
		DataSet IBasicFunctionSet.GetFuelReportData(
			int operatorID,
			int? vehicleID, int? vehicleGroupID,
			int logTimeFrom, int logTimeTo,
			SensorLegend[] sensors,
			FuelSpendStandardType fuelSpendStandardType,
			CultureInfo culture)
		{
			using (var entities = new Entities())
			{
				if (vehicleID != null)
				{
					var args = new GetVehiclesArgs
					{
						VehicleIDs = new[] { vehicleID.Value },
						Culture = culture
					};
					var vehicleDto = GetVehiclesWithPositions(operatorID, null, args).FirstOrDefault();
					if (vehicleDto == null)
						throw new SystemRightAccessException("Operator has no access to vehicle with id = " + vehicleID.Value, vehicleID.Value, IdType.Vehicle, culture, SystemRight.VehicleAccess);
					var vehicle = entities.GetVehicle(vehicleID.Value);
					vehicle.GARAGE_NUMBER = vehicleDto.Name;
					return GetFuelReportV2DataForVehicle(
						operatorID, vehicle, logTimeFrom, logTimeTo, sensors, fuelSpendStandardType, culture);
				}

				if (vehicleGroupID != null)
				{
					return GetFuelReportV2DataForVehicleGroup(
						operatorID, vehicleGroupID.Value, logTimeFrom, logTimeTo, sensors, fuelSpendStandardType, culture);
				}

				throw new ArgumentException("vehicleID and vehicleGroupID cannot be both null");
			}
		}
		DataSet IBasicFunctionSet.GetFuelStationReportData(
			int operatorId, int vehicleId, int logTimeFrom, int logTimeTo)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedAll(entities.GetOperator(operatorId), entities.GetVehicle(vehicleId),
					SystemRight.VehicleAccess))
				{
					Trace.TraceInformation("Operator " + operatorId + " has no rights on vehicle " + vehicleId);
					return null;
				}

				var ds = new DataSet();

				var headerTable = new DataTable("Header");
				ds.Tables.Add(headerTable);

				headerTable.Columns.AddRange(new[]
				{
					new DataColumn("Vehicle_ID", typeof(int)),
					new DataColumn("DateFrom", typeof(DateTime)),
					new DataColumn("DateTo", typeof(DateTime)),
					new DataColumn("FuelAtStart", typeof(int)),
					new DataColumn("FuelAtEnd", typeof(int)),
					new DataColumn("Fillings", typeof(int)),
					new DataColumn("Refuels", typeof(int))
				});

				var dataTable = new DataTable("Data");
				ds.Tables.Add(dataTable);

				dataTable.Columns.AddRange(new[]
				{
					new DataColumn("DateFrom", typeof(DateTime)),
					new DataColumn("DateTo", typeof(DateTime)),
					new DataColumn("Volume", typeof(int)),
					new DataColumn("IsRefuel", typeof(bool))
				});

				var fullLog = GetFullLog(operatorId, vehicleId, logTimeFrom, logTimeTo, new FullLogOptions());

				var sensors = new[] { SensorLegend.Fuel, SensorLegend.CANFuel };
				var sensor = sensors.Any()
					? sensors.FirstOrDefault(s => fullLog.Sensors.ContainsKey(s) && fullLog.Sensors[s].LogRecords.Any())
					: fullLog.Sensors.Keys.FirstOrDefault(s => s == SensorLegend.Fuel || s == SensorLegend.CANFuel);
				var startFuelLevel = fullLog.SensorByLogTime(sensor, logTimeFrom);
				var endFuelLevel   = fullLog.SensorByLogTime(sensor, logTimeTo);

				var vehicle = entities.GetVehicle(vehicleId);

				decimal fuelTankVolume = !vehicle.FUEL_TANK.HasValue || vehicle.FUEL_TANK == 0 ? 25000 : vehicle.FUEL_TANK.Value;

				var absoluteFuelSensorError = fuelTankVolume * RelativeFuelSensorError;
				var intervals = fullLog.FindSensorChangeIntervals(sensor, 1 * 60, absoluteFuelSensorError).ToArray();

				if (startFuelLevel == null || endFuelLevel == null)
				{
					Trace.TraceError("Start or End fuel level data doesnot exists.");
					return null;
				}

				var refuels = intervals
					.GetSequences(i => 0 < i.To.Value - i.From.Value)
					.Select(g => new SensorChangeInterval { From = g.First().From, To = g.Last().To })
					.Where(sci => 1.5m * absoluteFuelSensorError < Math.Abs(sci.To.Value - sci.From.Value))
					.ToList();

				foreach (var sci in refuels)
				{
					var dataRow = dataTable.NewRow();

					dataRow["DateFrom"] = TimeHelper.GetLocalTime(sci.From.LogTime);
					dataRow["DateTo"] = TimeHelper.GetLocalTime(sci.To.LogTime);
					dataRow["Volume"] = sci.From.Value - sci.To.Value;
					dataRow["IsRefuel"] = true;

					dataTable.Rows.Add(dataRow);
				}

				return ds;
			}

		}
		const decimal RelativeFuelSensorError = 0.05m;
		private DataSet GetFuelReportV2DataForVehicle(int operatorId, VEHICLE vehicle, int logTimeFrom, int logTimeTo, SensorLegend[] sensors, FuelSpendStandardType fuelSpendStandardType, CultureInfo culture)
		{
			var vehicleId = vehicle.VEHICLE_ID;
			using (var entities = new Entities())
			{
				#region Проверяем права на объект наблюдения
				if (!entities.IsAllowedAll(entities.GetOperator(operatorId), vehicle, SystemRight.VehicleAccess))
				{
					throw new SystemRightAccessException(string.Format("{0} operatorId: {1}, vehicleId: {2}", this, operatorId, vehicleId),
						vehicleId,
						IdType.Vehicle,
						culture,
						SystemRight.VehicleAccess);
				}
				#endregion Проверяем права на объект наблюдения
				var ds = new DataSet();
				//"Data", "Total", "ReportHeader"
				#region Создание структуры таблицы "Data"
				var dataTable = new DataTable("Data");
				ds.Tables.Add(dataTable);
				var dataVehicleIDColumn    = dataTable.Columns.Add("Vehicle_ID", typeof(int));
				var dataDateFromColumn     = dataTable.Columns.Add("DateFrom", typeof(DateTime));
				var dataDateToColumn       = dataTable.Columns.Add("DateTo", typeof(DateTime));
				var dataF_prevColumn       = dataTable.Columns.Add("F_prev", typeof(float));
				var dataF_nextColumn       = dataTable.Columns.Add("F_next", typeof(float));
				var dataRefuelColumn       = dataTable.Columns.Add("Refuel", typeof(float));
				var dataLT_fromColumn      = dataTable.Columns.Add("LT_from", typeof(int));
				var dataLT_toColumn        = dataTable.Columns.Add("LT_to", typeof(int));
				var dataStopTimeColumn     = dataTable.Columns.Add("StopTime", typeof(int));
				var dataLatColumn          = dataTable.Columns.Add("Lat", typeof(decimal));
				var dataLngColumn          = dataTable.Columns.Add("Lng", typeof(decimal));
				var dataRefuelByBillColumn = dataTable.Columns.Add("RefuelByBill", typeof(decimal));
				#endregion Создание структуры таблицы "Data"
				#region Создание структуры таблицы "Total"
				var totalTable = new DataTable("Total");
				ds.Tables.Add(totalTable);
				var totalVehicle_IDColumn          = totalTable.Columns.Add("Vehicle_ID", typeof(int));
				var totalvehicleColumn             = totalTable.Columns.Add("vehicle", typeof(string));
				var totalVehicle_Kind_IDColumn     = totalTable.Columns.Add("Vehicle_Kind_ID", typeof(int));
				var totalSensorTypeColumn          = totalTable.Columns.Add("SensorType", typeof(int));
				var totalStart_levelColumn         = totalTable.Columns.Add("Start_level", typeof(float));
				var totalEnd_levelColumn           = totalTable.Columns.Add("End_level", typeof(float));
				var totalRefuel_countColumn        = totalTable.Columns.Add("Refuel_count", typeof(int));
				var totalRefuel_amountColumn       = totalTable.Columns.Add("Refuel_amount", typeof(double));
				var totalDump_countColumn          = totalTable.Columns.Add("Dump_count", typeof(int));
				var totalDump_amountColumn         = totalTable.Columns.Add("Dump_amount", typeof(double));
				var totalFuelSpendColumn           = totalTable.Columns.Add("FuelSpend", typeof(float));
				var totalRunColumn                 = totalTable.Columns.Add("Run", typeof(long));
				var totalFuelPerHundredColumn      = totalTable.Columns.Add("FuelPerHundred", typeof(float));
				var totalFuelByStandardColumn      = totalTable.Columns.Add("FuelByStandard", typeof(float));
				var totalDiffFromStandardColumn    = totalTable.Columns.Add("DiffFromStandard", typeof(float));
				var totalFuelSpendStandardColumn   = totalTable.Columns.Add("FuelSpendStandard", typeof(float));
				var totalMachineHoursColumn        = totalTable.Columns.Add("MachineHours", typeof(decimal));
				var totalFuelPerMachineHourColumn  = totalTable.Columns.Add("FuelPerMachineHour", typeof(decimal));
				var totalFuelByStandardMHColumn    = totalTable.Columns.Add("FuelByStandardMH", typeof(decimal));
				var totalDiffFromStandardMHColumn  = totalTable.Columns.Add("DiffFromStandardMH", typeof(decimal));
				var totalFuelSpendStandardMHColumn = totalTable.Columns.Add("FuelSpendStandardMH", typeof(decimal));
				var totalFuelSpendByCANColumn      = totalTable.Columns.Add("TotalFuelSpendByCAN", typeof(decimal));
				#endregion Создание структуры таблицы "Total"
				#region Создание структуры таблицы "ReportHeader"
				var reportHeaderTable = new DataTable("ReportHeader");
				ds.Tables.Add(reportHeaderTable);
				var reportHeaderVehicleColumn = reportHeaderTable.Columns.Add("Vehicle", typeof(string));
				#endregion Создание структуры таблицы "ReportHeader"
				#region Заполнение таблицы "ReportHeader"
				var reportHeaderRow = reportHeaderTable.NewRow();
				reportHeaderTable.Rows.Add(reportHeaderRow);
				reportHeaderRow[reportHeaderVehicleColumn] = vehicle.GARAGE_NUMBER;
				#endregion Заполнение таблицы "ReportHeader"
				#region Получаем лог(историю) по объекту, за указанный интервал
				var fullLog = GetFullLog(operatorId, vehicleId, logTimeFrom, logTimeTo, new FullLogOptions());
				#endregion Получаем лог(историю) по объекту, за указанный интервал
				#region Вычисляем заправки и сливы
				///////////////////////////////////////////////////////////////////////////////////////////////
				// Готовим параметры для заполнения
				var vehicleFuelTankVolume = !vehicle.FUEL_TANK.HasValue || vehicle.FUEL_TANK == 0 ? 1000 : vehicle.FUEL_TANK.Value;
				var vehicleFuelSpendStandardKilometer = !vehicle.FuelSpendStandardKilometer.HasValue || vehicle.FuelSpendStandardKilometer.Value <= 0
					? Convert.ToInt32(entities.CONSTANTS.FirstOrDefault(c => c.NAME == "baseFuelSpendStandardKilometer").VALUE)
					: vehicle.FuelSpendStandardKilometer.Value;
				var vehicleFuelSpendStandardLiter     = !vehicle.FuelSpendStandardLiter.HasValue || vehicle.FuelSpendStandardLiter.Value <= 0
					? Convert.ToInt32(entities.CONSTANTS.FirstOrDefault(c => c.NAME == "baseFuelSpendStandardLiter").VALUE)
					: vehicle.FuelSpendStandardLiter.Value;
				var vehicleFuelSpendStandardByMH = entities.GetRuleValue(vehicleId, RuleNumber.FuelSpendStandardPerMH);

				if (sensors == null)
					sensors = new[] { SensorLegend.Fuel, SensorLegend.CANFuel };

				var sensor = sensors != null && sensors.Any()
					? sensors.FirstOrDefault(s => fullLog.Sensors.ContainsKey(s) && fullLog.Sensors[s].LogRecords.Any())
					: fullLog.Sensors.Keys.FirstOrDefault(s => s == SensorLegend.Fuel || s == SensorLegend.CANFuel);

				fullLog.FillRefuelsAndDrains(
					vehicleId,
					vehicleFuelTankVolume,
					vehicleFuelSpendStandardKilometer,
					vehicleFuelSpendStandardLiter,
					vehicleFuelSpendStandardByMH,
					new[] { sensor });
				///////////////////////////////////////////////////////////////////////////////////////////////
				#endregion Вычисляем заправки и сливы

				var fuelSpendStandardKillomener = vehicleFuelSpendStandardKilometer;
				var fuelSpendStandardLiter      = vehicleFuelSpendStandardLiter;
				var fuelSpendStandardByMH       = vehicleFuelSpendStandardByMH;

				#region Заполнение таблицы "Data"
				var refuels = fullLog.Refuels;
				if (null == refuels)
					return ds;
				var drains  = fullLog.Drains;
				if (null == drains)
					return ds;

				// Функция перевода SensorChangeInterval(Интервал однородного изменения значения датчика) в строку таблицы
				Func<SensorChangeInterval, DataRow> createDataRowFromSci = sci =>
				{
					var @from = sci.From;
					var @to   = sci.To;
					var dataRow = dataTable.NewRow();
					// Заполняем поля из полученной записи интервала
					dataRow[dataVehicleIDColumn] = vehicleId;
					dataRow[dataDateFromColumn]  = TimeHelper.GetDateTimeUTC(@from.LogTime);
					dataRow[dataDateToColumn]    = TimeHelper.GetDateTimeUTC(@to.LogTime);
					dataRow[dataF_prevColumn]    = @from.Value;
					dataRow[dataF_nextColumn]    = @to.Value;
					dataRow[dataRefuelColumn]    = @to.Value - @from.Value;
					dataRow[dataLT_fromColumn]   = @from.LogTime;
					dataRow[dataLT_toColumn]     = @to.LogTime;
					dataRow[dataStopTimeColumn]  = @to.LogTime - @from.LogTime;
					// Ищем и заполняем данные в полученном логе, ближайшую geo точку
					var geoLogRecord = fullLog.GetCurrentOrPreviousGeoRecord(@to.LogTime);
					if (geoLogRecord != null)
					{
						dataRow[dataLatColumn] = geoLogRecord.Value.Lat;
						dataRow[dataLngColumn] = geoLogRecord.Value.Lng;
					}
					// Ищем и заполняем данные о чеках, на такую заправку
					var refuelByBill = entities.SensorValueChange
						.FirstOrDefault(svc =>
							svc.VEHICLE.VEHICLE_ID == vehicleId &&
							svc.CONTROLLER_SENSOR_LEGEND.Number == (int)sensor &&
							@from.LogTime <= svc.Log_Time &&
							svc.Log_Time <= @to.LogTime);
					if (refuelByBill != null)
						dataRow[dataRefuelByBillColumn] = refuelByBill.Value;

					return dataRow;
				};
				// Заполнение заправок
				foreach (var g in refuels)
					dataTable.Rows.Add(createDataRowFromSci(g));
				// Заполнение сливов
				foreach (var drain in drains)
					dataTable.Rows.Add(createDataRowFromSci(drain));
				#endregion Заполнение таблицы "Data"
				#region Заполнение таблицы "Total"

				var refuelCount  = refuels.Count;
				var refuelAmount = refuels.Sum(sci => sci.To.Value - sci.From.Value);
				var drainCount   = drains.Count;
				var drainAmount  = drains.Sum(d => d.From.Value - d.To.Value);

				var startFuelLevel = fullLog.SensorByLogTime(sensor, logTimeFrom);
				var endFuelLevel   = fullLog.SensorByLogTime(sensor, logTimeTo);

				// Расход топлива (включая сливы)
				var spendAmount = startFuelLevel - endFuelLevel + refuelAmount;

				var totalRow = totalTable.NewRow();
				totalTable.Rows.Add(totalRow);
				totalRow[totalVehicle_IDColumn]      = vehicleId;
				totalRow[totalvehicleColumn]         = vehicle.GARAGE_NUMBER;
				totalRow[totalVehicle_Kind_IDColumn] = vehicle.VEHICLE_KIND.VEHICLE_KIND_ID;
				totalRow[totalSensorTypeColumn]      = GetFuelSensorType(sensor);
				totalRow[totalStart_levelColumn]     = (object)startFuelLevel ?? DBNull.Value;
				totalRow[totalEnd_levelColumn]       = (object)endFuelLevel   ?? DBNull.Value;
				totalRow[totalRefuel_countColumn]    = refuelCount;
				totalRow[totalRefuel_amountColumn]   = refuelAmount;
				totalRow[totalDump_countColumn]      = drainCount;
				totalRow[totalDump_amountColumn]     = drainAmount;
				totalRow[totalFuelSpendColumn]       = (object)spendAmount ?? DBNull.Value;

				var totalRun = fullLog.GetRunDistance(logTimeFrom, logTimeTo) / 1000m; //Пробег в километрах
				totalRow[totalRunColumn] = totalRun;

				string departmentCountryName = vehicle.DEPARTMENT != null && vehicle.DEPARTMENT.Country != null ? vehicle.DEPARTMENT.Country.Name : "";
				//средний расход по датчику
				decimal? fuelPer100Km = null;
				if (departmentCountryName.Contains(Country.India))
				{
					if (0.001m < spendAmount)
						fuelPer100Km = totalRun * 1.0m / spendAmount;
				}
				else
				{
					if (0.001m < totalRun)
						fuelPer100Km = totalRun != 0 ? spendAmount / totalRun * 100m : null;
				}

				totalRow[totalFuelPerHundredColumn] = (object)fuelPer100Km ?? DBNull.Value;
				var fuelSpendStandardByRun = default(decimal?);
				if (departmentCountryName.Contains("India"))
					fuelSpendStandardByRun = fuelSpendStandardKillomener * 1.0m / fuelSpendStandardLiter;
				else
					fuelSpendStandardByRun = fuelSpendStandardLiter      * 1.0m / fuelSpendStandardKillomener * 100;

				// расход по нормативу
				var fuelSpendByStandardPerRun       = departmentCountryName.Contains("India")
					? totalRun / fuelSpendStandardByRun
					: totalRun * fuelSpendStandardByRun / 100;
				totalRow[totalFuelByStandardColumn] = fuelSpendByStandardPerRun;

				//отклонение за период
				totalRow[totalDiffFromStandardColumn]  = (object)(spendAmount - fuelSpendByStandardPerRun) ?? DBNull.Value;
				totalRow[totalFuelSpendStandardColumn] = (object)fuelSpendStandardByRun ?? DBNull.Value;

				Sensor ignition;
				if (fullLog.Sensors.TryGetValue(SensorLegend.Ignition, out ignition))
				{
					var fullStat =
						ignition.SensorStatistics.FirstOrDefault(x => x.LogTimeFrom == logTimeFrom && x.LogTimeTo == logTimeTo);
					if (fullStat.OnTime > 0)
					{
						var totalOnHours = (decimal)TimeSpan.FromSeconds(fullStat.OnTime).TotalHours;
						totalRow[totalMachineHoursColumn] = totalOnHours;
						totalRow[totalFuelPerMachineHourColumn] = 1 < totalOnHours ? spendAmount / totalOnHours : 0;
						var fuelSpendByStandardMH = totalOnHours * fuelSpendStandardByMH;
						totalRow[totalFuelByStandardMHColumn] = (object)fuelSpendByStandardMH ?? DBNull.Value;
						if (fuelSpendByStandardMH != null)
							totalRow[totalDiffFromStandardMHColumn] = spendAmount - fuelSpendByStandardMH.Value;
					}
					totalRow[totalFuelSpendStandardMHColumn] = (object)fuelSpendStandardByMH ?? DBNull.Value;
				}

				Sensor totalFuelSpendByCAN;
				if (fullLog.Sensors.TryGetValue(SensorLegend.CAN_TotalFuelSpend, out totalFuelSpendByCAN))
				{
					var startValue = fullLog.SensorByLogTime(SensorLegend.CAN_TotalFuelSpend, logTimeFrom);
					var endValue   = fullLog.SensorByLogTime(SensorLegend.CAN_TotalFuelSpend, logTimeTo);

					if (startValue != null && endValue != null)
					{
						totalRow[totalFuelSpendByCANColumn] = endValue.Value - startValue.Value;
					}
				}
				#endregion Заполнение таблицы "Total"
				return ds;
			}
		}
		private DataSet GetFuelReportV2DataForVehicleGroup(
			int operatorID, int vehicleGroupID,
			int logTimeFrom, int logTimeTo,
			SensorLegend[] sensors, FuelSpendStandardType fuelSpendStandardType,
			CultureInfo culture)
		{
			using (var entities = new Entities())
			{
				var vehicleGroup =
					(
						from ovgr in entities.v_operator_vehicle_groups_right
						where
							ovgr.operator_id     == operatorID                     &&
							ovgr.right_id        == (int)SystemRight.VehicleAccess &&
							ovgr.vehiclegroup_id == vehicleGroupID
						select ovgr.VEHICLEGROUP
					).FirstOrDefault();

				if (vehicleGroup == null)
					throw new VehicleGroupNotFoundException(string.Format("{0}, OperatorId: {1} vehicleGroupId: {2}", this, operatorID, vehicleGroupID));

				var vehicles = vehicleGroup
					.VEHICLE
					.Where(v =>
						2 == entities.v_operator_vehicle_right.Count(r => r.operator_id == operatorID && r.vehicle_id == v.VEHICLE_ID
							&&
						(r.right_id == (int)SystemRight.VehicleAccess || r.right_id == (int)SystemRight.PathAccess)))
					.Select(v => v)
					.ToArray();

				var args = new GetVehiclesArgs
				{
					VehicleIDs = vehicles.Select(item => item.VEHICLE_ID).ToArray(),
					Culture    = culture
				};
				var vehicleDTOs = GetVehiclesWithPositions(operatorID, null, args).ToDictionary(key => key.id);
				DataSet resultDataSet = null;
				foreach (var vehicle in vehicles)
				{
					var vehicleDTO = vehicleDTOs[vehicle.VEHICLE_ID];
					vehicle.GARAGE_NUMBER = vehicleDTO.Name;
					var ds = GetFuelReportV2DataForVehicle(operatorID, vehicle, logTimeFrom, logTimeTo, sensors, fuelSpendStandardType, culture);

					if (ds == null)
						continue;

					if (resultDataSet == null)
						resultDataSet = ds;
					else
						resultDataSet.Merge(ds);
				}

				if (resultDataSet != null)
				{
					resultDataSet.Tables["ReportHeader"].Rows.Clear();
					resultDataSet.Tables["ReportHeader"].Rows.Add(vehicleGroup.NAME);
				}

				return resultDataSet;
			}
		}
		private static int GetFuelSensorType(SensorLegend sensorLegend)
		{
			switch (sensorLegend)
			{
				case SensorLegend.Fuel:
					return 0;
				case SensorLegend.CANFuel:
					return 1;
				default:
					throw new ArgumentOutOfRangeException(
						"sensorLegend", sensorLegend,
						"Fuel report for sensor is not supported");
			}
		}
		List<DataAbsenceRecord> IBasicFunctionSet.GetDataAbsences(int operatorID, DateTime dateFrom, DateTime dateTo, CultureInfo cultureInfo)
		{
			var absences = GetAbsences(operatorID, dateFrom, dateTo);
			FillVehicleNames(absences, record => record.VehicleID, record => record.VehicleName, operatorID, cultureInfo);
			return absences;
		}
		private List<DataAbsenceRecord> GetAbsences(int operatorID, DateTime dateFrom, DateTime dateTo)
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ReportAboutDataAbsence"))
			{
				sp["@operatorID"].Value = operatorID;
				sp["@dateFrom"].Value = dateFrom;
				sp["@dateTo"].Value = dateTo;

				using (new ConnectionOpener(sp.Command.Connection))
				using (var r = sp.ExecuteReader())
				{
					var result = new List<DataAbsenceRecord>();

					var departmentIDOrdinal    = r.GetOrdinal("departmentID");
					var departmentNameOrdinal  = r.GetOrdinal("departmentName");
					var emailsOrdinal          = r.GetOrdinal("emails");
					var phonesOrdinal          = r.GetOrdinal("phones");
					var vehicleIDOrdinal       = r.GetOrdinal("vehicleID");
					var vehicleDeviceIDOrdinal = r.GetOrdinal("vehicleDeviceID");
					var lastInsertTimeOrdinal  = r.GetOrdinal("lastInsertTime");
					var lastLogTimeOrdinal     = r.GetOrdinal("lastLogTime");
					var lastGeoTimeOrdinal     = r.GetOrdinal("lastGeoTime");
					var lastCANTimeOrdinal     = r.GetOrdinal("lastCANTime");
					var lastFuelTimeOrdinal    = r.GetOrdinal("lastFuelTime");

					while (r.Read())
					{
						var item = new DataAbsenceRecord();

						item.DepartmentID    = r.IsDBNull(departmentIDOrdinal) ? (int?)null : r.GetInt32(departmentIDOrdinal);
						item.DepartmentName  = r.IsDBNull(departmentIDOrdinal) ? null : r.GetString(departmentNameOrdinal);
						item.Emails          = r.IsDBNull(emailsOrdinal) ? null : r.GetString(emailsOrdinal);
						item.Phones          = r.IsDBNull(phonesOrdinal) ? null : r.GetString(phonesOrdinal);
						item.VehicleID       = r.GetInt32(vehicleIDOrdinal);
						item.VehicleDeviceID = r.IsDBNull(vehicleDeviceIDOrdinal) ? null : r.GetString(vehicleDeviceIDOrdinal);
						item.LastInsertTime  = r.IsDBNull(lastInsertTimeOrdinal) ? (DateTime?)null : r.GetDateTime(lastInsertTimeOrdinal);
						item.LastLogTime     = r.IsDBNull(lastLogTimeOrdinal) ? (DateTime?)null : r.GetDateTime(lastLogTimeOrdinal);
						item.LastGeoTime     = r.IsDBNull(lastGeoTimeOrdinal) ? (DateTime?)null : r.GetDateTime(lastGeoTimeOrdinal);
						item.LastCANTime     = r.IsDBNull(lastCANTimeOrdinal) ? (DateTime?)null : r.GetDateTime(lastCANTimeOrdinal);
						item.LastFuelTime    = r.IsDBNull(lastFuelTimeOrdinal) ? (DateTime?)null : r.GetDateTime(lastFuelTimeOrdinal);

						result.Add(item);
					}

					return result;
				}
			}
		}
		List<CommercialServiceRecord> IBasicFunctionSet.ReportCommercialServices(int operatorId, DateTime dateFrom, DateTime dateTo)
		{
			var serviceRecords = new List<CommercialServiceRecord.ServiceRecord>();

			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ReportCommercialServices"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@operator_id"].Value = operatorId;
				sp["@from"].Value = dateFrom;
				sp["@to"].Value = dateTo;
				sp["@country"].Value = null;

				var reader = sp.ExecuteReader();

				var countryOrdinal = reader.GetOrdinal("Country");
				var macroRegionOrdinal = reader.GetOrdinal("MacroRegion");
				var customerNameOrdinal = reader.GetOrdinal("CustomerName");
				var contractNumberOrdinal = reader.GetOrdinal("ContractNumber");
				var terminalDeviceNumberOrdinal = reader.GetOrdinal("Terminal_Device_Number");
				var serviceTypeCategoryOrdinal = reader.GetOrdinal("ServiceTypeCategory");
				var serviceNameOrdinal = reader.GetOrdinal("ServiceName");
				var startDateOrdinal = reader.GetOrdinal("StartDate");
				var endDateOrdinal = reader.GetOrdinal("EndDate");
				var renderedCountOrdinal = reader.GetOrdinal("RenderedCount");
				var lbsPositionCountOrdinal = reader.GetOrdinal("LbsPositionCount");
				var lbsPositionRequestedOrdinal = reader.GetOrdinal("LbsPositionRequested");
				var renderedLastTimeOrdinal = reader.GetOrdinal("RenderedLastTime");

				while (reader.Read())
				{
					var record = new CommercialServiceRecord.ServiceRecord();
					serviceRecords.Add(record);
					record.Country = reader.GetStringOrNull(countryOrdinal);
					record.MacroRegion = reader.GetStringOrNull(macroRegionOrdinal);
					record.CustomerName = reader.GetStringOrNull(customerNameOrdinal);
					record.ContractNumber = reader.GetStringOrNull(contractNumberOrdinal);
					record.TerminalDeviceNumber = reader.GetStringOrNull(terminalDeviceNumberOrdinal);
					record.ServiceTypeCategory = reader.GetString(serviceTypeCategoryOrdinal);
					record.ServiceName = reader.GetString(serviceNameOrdinal);
					record.StartDate = reader.GetDateTimeOrNull(startDateOrdinal);
					record.EndDate = reader.GetDateTimeOrNull(endDateOrdinal);
					record.RenderedCount = reader.GetInt32(renderedCountOrdinal);
					record.RenderedCount = reader.GetInt32(renderedCountOrdinal);
					record.LbsPositionCount = reader.GetInt32(lbsPositionCountOrdinal);
					record.LbsPositionRequested = reader.GetInt32(lbsPositionRequestedOrdinal);
					record.RenderedLastTime = reader.GetDateTimeOrNull(renderedLastTimeOrdinal);
				}
			}

			var result = new List<CommercialServiceRecord>();
			foreach (var g in serviceRecords.GroupBy(
				s => new
				{
					s.Country,
					s.MacroRegion,
					s.CustomerName,
					s.ContractNumber,
					s.ServiceTypeCategory,
					s.ServiceName
				}))
			{
				var record = new CommercialServiceRecord();
				result.Add(record);
				record.Country = g.Key.Country;
				record.MacroRegion = g.Key.MacroRegion;
				record.CustomerName = g.Key.CustomerName;
				record.ContractNumber = g.Key.ContractNumber;
				record.ServiceTypeCategory = g.Key.ServiceTypeCategory;
				record.ServiceName = g.Key.ServiceName;
				record.ServiceCount = g.Count();
				record.RenderedCount = g.Sum(s => s.RenderedCount);
				record.RenderedLastTime = g.Max(s => s.RenderedLastTime);
				record.Services = g.ToList();
			}

			return result;
		}
		public AddRemoveServicesReportData ReportAddRemoveServices(DateTime dateFrom, DateTime dateTo, bool isDetailed, int operatorId)
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.ReportAddRemoveService"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@operator_id"].Value = operatorId;
				sp["@from"].Value = dateFrom;
				sp["@to"].Value = dateTo;
				sp["@detailed"].Value = isDetailed;

				var reader = sp.ExecuteReader();

				var result = new AddRemoveServicesReportData()
				{
					SummaryRecords = new List<AddRemoveServicesReportData.SummaryRecord>()
				};

				{
					var serviceIdOrdinal = reader.GetOrdinal("ID");
					var nameOrdinal = reader.GetOrdinal("Name");
					var minPriceOrdinal = reader.GetOrdinal("MinPrice");
					var maxPriceOrdinal = reader.GetOrdinal("MaxPrice");
					var countOnStartOrdinal = reader.GetOrdinal("CountOnStart");
					var countOfAddedOrdinal = reader.GetOrdinal("CountOfAdded");
					var countOfRemovedOrdinal = reader.GetOrdinal("CountOfRemoved");
					var countOnEndOrdinal = reader.GetOrdinal("CountOnEnd");

					while (reader.Read())
					{
						var record = new AddRemoveServicesReportData.SummaryRecord();
						result.SummaryRecords.Add(record);

						record.Id = reader.GetInt32(serviceIdOrdinal);
						record.Name = reader.GetString(nameOrdinal);
						record.MinPrice = reader.IsDBNull(minPriceOrdinal) ? (decimal?)null : reader.GetDecimal(minPriceOrdinal);
						record.MaxPrice = reader.IsDBNull(maxPriceOrdinal) ? (decimal?)null : reader.GetDecimal(maxPriceOrdinal);
						record.CountOfAdded = reader.GetInt32(countOfAddedOrdinal);
						record.CountOnStart = reader.GetInt32(countOnStartOrdinal);
						record.CountOfAdded = reader.GetInt32(countOfAddedOrdinal);
						record.CountOfRemoved = reader.GetInt32(countOfRemovedOrdinal);
						record.CountOnEnd = reader.GetInt32(countOnEndOrdinal);
					}
				}

				if (reader.NextResult())
				{
					result.DetailRecords = new List<AddRemoveServicesReportData.DetailRecord>();

					var serviceIdOrdinal = reader.GetOrdinal("Billing_Service_Type_ID");
					var terminalDeviceNumberOrdinal = reader.GetOrdinal("Terminal_Device_Number");
					var vehicleIdOrdinal = reader.GetOrdinal("VEHICLE_ID");
					var contractNumberOrdinal = reader.GetOrdinal("Contract_Number");
					var addedOrdinal = reader.GetOrdinal("Added");
					var removedOrdinal = reader.GetOrdinal("Removed");
					var startDateOrdinal = reader.GetOrdinal("StartDate");
					var endDateOrdinal = reader.GetOrdinal("EndDate");

					while (reader.Read())
					{
						var record = new AddRemoveServicesReportData.DetailRecord();
						result.DetailRecords.Add(record);
						record.ServiceId = reader.GetInt32(serviceIdOrdinal);
						record.TerminalDeviceNumber = reader.IsDBNull(terminalDeviceNumberOrdinal)
							? (long?)null
							: reader.GetInt64(terminalDeviceNumberOrdinal);
						record.VehicleId = reader.IsDBNull(vehicleIdOrdinal)
							? 0
							: reader.GetInt32(vehicleIdOrdinal);
						record.ContractNumber = reader.IsDBNull(contractNumberOrdinal)
							? null
							: reader.GetString(contractNumberOrdinal);
						record.Added = reader.GetBoolean(addedOrdinal);
						record.Removed = reader.GetBoolean(removedOrdinal);
						record.StartDate = reader.GetDateTime(startDateOrdinal);
						record.EndDate = reader.IsDBNull(endDateOrdinal)
							? (DateTime?)null
							: reader.GetDateTime(endDateOrdinal);
					}
				}

				return result;
			}
		}
		public ShareLink GetOrCreateShareLink(
			int operatorId, int logTimeBeg, int logTimeEnd, int vehicleId, string vehicleName, string vehicleDesc = null)
		{
			using (var entities = new Entities())
			{
				// Проверяем право на объект, если нет исключение, т.к. нельзя ни создать ни получить ссылку на объект без права
				if (!IsAllowedVehicleAll(operatorId, vehicleId, SystemRight.ShareLinkAccess))
					throw new SecurityException("Access denied");
				// Получаем полностью идентичную или создаем новую
				var linkBeg = logTimeBeg.ToUtcDateTime();
				var linkEnd = logTimeEnd.ToUtcDateTime();
				var ormShareLink = entities.GetQuery4ShareLinks()
					.Where(s => s.VEHICLE_SHARE_LINK_CREATOR_ID == operatorId)
					.Where(s => s.VEHICLE_SHARE_LINK_BEG        == linkBeg)
					.Where(s => s.VEHICLE_SHARE_LINK_END        == linkEnd)
					.Where(s => s.VEHICLE_SHARE_LINK_VEHICLE_ID == vehicleId)
					.Where(s => s.VEHICLE_SHARE_LINK_NAME       == vehicleName)
					.FirstOrDefault();
				if (null == ormShareLink)
				{
					var dtoShareLink = new ShareLink
					{
						LinkGuid    = ShareLink.EmptyGuid,
						CreatorId   = operatorId,
						LogTimeBeg  = logTimeBeg,
						LogTimeEnd  = logTimeEnd,
						VehicleId   = vehicleId,
						VehicleName = vehicleName,
						VehicleDesc = vehicleDesc,
					};
					ormShareLink = VEHICLE_SHARE_LINK.CreateFromDTO(dtoShareLink, entities);
					// Добавление объекта ORM в контекст
					entities.AddToVEHICLE_SHARE_LINK(ormShareLink);
					// Сохранение изменений с использованием оператора для сохранения истории
					entities.SaveChangesByOperator(operatorId);
				}
				return ormShareLink.ToDTO();
			}
		}
	}
}