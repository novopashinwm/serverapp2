﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.ServerApplication
{
	public partial class Server
	{
		private readonly ConcurrentDictionary<int, VehicleNotifyContextInfo> _notifyInfos = new ConcurrentDictionary<int, VehicleNotifyContextInfo>();

		private class VehicleNotifyContextInfo
		{
			/// <summary> Последнее значение датчика тревоги </summary>
			public int LastSensorValue;
			/// <summary> Последнее время датчика тревоги </summary>
			public int LastSensorTime;
		}
		private void CheckAlarmAndNotify(IMobilUnit[] mobilUnits)
		{
			foreach (var mu in mobilUnits)
				CheckAlarmAndNotify(mu);
		}
		private void CheckAlarmAndNotify(IMobilUnit mobilUnit)
		{
			//if (mobilUnit.Time < TimeHelper.GetSecondsFromBase() - 2 * 60)
			//	return;

			if (mobilUnit.SensorMap == null || !mobilUnit.SensorMap.ContainsKey(SensorLegend.Alarm))
				return;

			var sensorValue = mobilUnit.GetSensorValue(SensorLegend.Alarm);
			if (!sensorValue.HasValue)
				return;

			var vehicleId = mobilUnit.Unique;

			VehicleNotifyContextInfo previousVehicleAlarm;

			while (true)
			{
				if (_notifyInfos.TryGetValue(vehicleId, out previousVehicleAlarm))
					break;
				previousVehicleAlarm = new VehicleNotifyContextInfo();
				if (_notifyInfos.TryAdd(vehicleId, previousVehicleAlarm))
					break;
			}

			if (mobilUnit.Time < previousVehicleAlarm.LastSensorTime)
				return;

			var currentAlarmValue = (int)sensorValue.Value;

			if (previousVehicleAlarm.LastSensorValue == 0 && currentAlarmValue == 1)
			{
				var operatorIds = GetVehicleOperators(vehicleId);
				if (operatorIds.Length != 0)
				{
					OnReceiveAlarm(new ReceiveAlarmArgs(mobilUnit, operatorIds));
				}
			}

			previousVehicleAlarm.LastSensorValue = currentAlarmValue;
			previousVehicleAlarm.LastSensorTime  = mobilUnit.Time;
		}
		private void OnAlarmReceived(object sender, ReceiveAlarmArgs e)
		{
			using (var entities = new Entities())
			{
				// Нет объекта, выходим
				if (null == e?.MobilUnit)
					return;
				// Нет операторов, выходим
				if (0 == (e?.OperatorIds?.Length ?? 0))
					return;

				var appIdExists = !string.IsNullOrEmpty(e.MobilUnit.AppId);
				var vehicleId   = e.MobilUnit.Unique;
				// Нет объекта в БД, выходим
				var vehicle     = entities.VEHICLE
					.Where(v => v.VEHICLE_ID == vehicleId)
					.Select(v => new { })
					.FirstOrDefault();
				if (vehicle == null)
					return;

				// Шаблон сообщения
				var alrmTemplate = entities.MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.AlarmNotification);
				// Оператор этого объекта, ему не надо отправлять
				var alarmSelfOperatorId = entities.Asid
					.Where(a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId)
					.Select(a => a.OPERATOR.OPERATOR_ID)
					.FirstOrDefault();
				// Готовим запрос к БД (исполнения нет, только подготовка)
				var operatorsQuery = entities.OPERATOR
					.Where(o => e.OperatorIds.Contains(o.OPERATOR_ID) && alarmSelfOperatorId != o.OPERATOR_ID)
					.Distinct()
					.Select(o => new
					{
						CultureCode  = o.Culture.Code,
						TimeZoneCode = o.TimeZone.Code,
						OperatorId   = o.OPERATOR_ID
					});
				var messages = new List<MESSAGE>();
				// Создаем сообщения для всех операторов (ToList выполнит подготовленный ранее запрос)
				foreach (var operatorInfo in operatorsQuery.ToList())
				{
					// Отключаем отправку суперадминам, любых тревог
					if (entities.IsSuperAdministrator(operatorInfo.OperatorId))
						continue;
					// Получить имя объекта для указанного оператора
					var vehicleName = Instance().GetVehicleName(operatorInfo.OperatorId, vehicleId);
					// Язык оператора
					var opratLang = CultureInfo.GetCultureInfo(
						operatorInfo?.CultureCode ?? CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
					// Часовой пояс оператора
					var opratTzon = TimeZoneInfo.FindSystemTimeZoneById(operatorInfo.TimeZoneCode);
					// Сообщение тревоги
					var alarmMess = ResourceContainers.Get(opratLang)
						.GetLocalizedMessagePart("Alarm")
						.ToUpper(opratLang);
					// Дата тревоги
					var alarmTime = TimeHelper.GetLocalTime(e.MobilUnit.Time, opratTzon)
						.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
					var alarmText = $"{vehicleName} {alarmMess} {alarmTime}";
					// Создать список сообщений для всех действующих AppId этого оператора
					messages.AddRange(entities.CreateAppNotification(operatorInfo.OperatorId, vehicleName, alarmText));
					// Создать сообщение для Web
					messages.Add     (entities.CreateWebNotification(operatorInfo.OperatorId, vehicleName, alarmText));
				}
				// Заполняем поля для всех сообщений
				foreach (var message in messages)
				{
					// Шаблон сообщения
					message.MESSAGE_TEMPLATE  = alrmTemplate;
					// Идентификатор оператора от которого поступило событие, если есть
					if (default(int) != alarmSelfOperatorId)
						message.Owner_Operator_ID = alarmSelfOperatorId;
					// Идентификатор объекта
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.VehicleID, e.MobilUnit.Unique);
					// Время тревоги
					entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.Date,      TimeHelper.GetDateTimeUTC(e.MobilUnit.Time));
					// Номер телефона объекта если есть
					if (!string.IsNullOrWhiteSpace(e.MobilUnit.Telephone))
						entities.AddMessageField(message, MESSAGE_TEMPLATE_FIELD.FriendMsisdn, e.MobilUnit.Telephone);
				}
				// Сохраняем созданные сообщения
				try
				{
					entities.SaveChanges();
				}
				catch(Exception ex)
				{
					default(string)
						.WithException(ex, true)
						.CallTraceError();
					throw;
				}
			}
		}
	}
}