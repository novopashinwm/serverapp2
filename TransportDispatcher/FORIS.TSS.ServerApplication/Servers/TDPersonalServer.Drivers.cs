﻿using System.Collections.Generic;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel;


namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		List<Driver> GetDriversFromEntity(IEnumerable<DRIVER> drivers)
		{
			return drivers.Select(d => new Driver()
						{
							FirstName = d.NAME,
							LastName = d.SHORT_NAME,
							Phones = d.PHONE != null ? d.PHONE.Split(',') : null,
							SecondName = "",
							Id = d.DRIVER_ID
						}).ToList();
		}

		public List<Driver> GetFreeDrivers()
		{
			using (var entities = new Entities())
			{
				return GetDriversFromEntity(from d in entities.DRIVER
						where d.DRIVER_VEHICLE == null && d.DEPARTMENT == ComputedDepartmentId
						select  d);
							
			}
		}

		public List<Driver> GetVehicleDrivers(int vehicleId)
		{
			using (var entities = new Entities())
			{
				var rightIds = new[] {SystemRight.VehicleAccess, SystemRight.EditVehicles}.Select(r => (int)r);
				var rights = entities.v_operator_vehicle_right
					.Where(
						ovr => ovr.operator_id == OperatorId &&
							   ovr.vehicle_id == vehicleId &&
							   rightIds.Contains(ovr.right_id))
					.Select(ovr => (SystemRight) ovr.right_id)
					.ToArray();

				if (!rights.Contains(SystemRight.VehicleAccess))
					throw new SecurityException("No access to vehicle");

				var query =
					from d in entities.DRIVER
					where d.DRIVER_VEHICLE.VEHICLE == vehicleId
					select d;

				if (FilterByDepartment)
					query = query.Where(d => d.DEPARTMENT == ComputedDepartmentId);

				var result = GetDriversFromEntity(query);

				if (rights.Contains(SystemRight.EditVehicles))
				{
					foreach (var driver in result)
						driver.Editable = true;
				}

				return result;
			}
		}

		public void SetVehicleDriver(int vehicleId, int driverId)
		{
			using (var entities = new Entities())
			{
				if (!entities.IsAllowedVehicleAll(OperatorId, vehicleId, SystemRight.VehicleAccess, SystemRight.EditVehicles))
					throw new SecurityException("Not allowed");

				if (entities.DRIVER.Any(d => d.DRIVER_VEHICLE.DRIVER_ID == driverId))
					return;

				var driver = entities.DRIVER.FirstOrDefault(d => d.DRIVER_ID == driverId);
				var vehicle = entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
				var drivervehicle = new DRIVER_VEHICLE
				{
					VEHICLE       = vehicleId,
					VEHICLE1      = vehicle,
					DRIVER        = driver,
					DRIVER_ID     = driverId,
					DRIVER_NUMBER = 0
				};

				entities.DRIVER_VEHICLE.AddObject(drivervehicle);
				entities.SaveChangesBySession(SessionId);
			}
		}

		public void DeleteVehicleDriver(int vehicleId, int driverId)
		{
			using (var entities = new Entities())
			{
				if (!entities.DRIVER.Any(d => d.DRIVER_ID == driverId))
					return;

				var item = entities.DRIVER_VEHICLE.FirstOrDefault(d => d.VEHICLE == vehicleId && d.DRIVER_ID == driverId);
				if (item == null)
					return;
				entities.DeleteObject(item);
				entities.SaveChangesBySession(SessionId);
			}
		}

		public Driver UpdateDriver(int driverId, string firstName, string shortName, string[] phones)
		{
			using (var entities = new Entities())
			{
				if (!entities.DRIVER.Any(d => d.DEPARTMENT == ComputedDepartmentId && d.DRIVER_ID == driverId))
					return null;

				var driver = entities.DRIVER.FirstOrDefault(d => d.DRIVER_ID == driverId);
				if (driver != null)
				{
					driver.NAME = firstName;
					driver.SHORT_NAME = shortName;
					if (phones != null)
						driver.PHONE = string.Join(",", phones.Where(p => p.Length > 0));
				}
				else return null;
				entities.SaveChangesBySession(SessionId);
				return GetDriversFromEntity(new List<DRIVER>() { driver }).First();
			}
		}

		public Driver AddDriver(string firstName, string shortName, string[] phones, int? vehicleId = null)
		{
			using (var entities = new Entities())
			{
				var driver = new DRIVER {EXT_NUMBER = "0", NAME = firstName, SHORT_NAME = shortName};
				if (phones != null)
					driver.PHONE = string.Join(",", phones.Where(p=>p.Length>0));
				driver.DEPARTMENT = ComputedDepartmentId;
				driver.DRIVER_STATUS_ID = 1;
				entities.AddToDRIVER(driver);
				entities.SaveChangesBySession(SessionId);
				if (vehicleId.HasValue)
				{
					var vehicle = entities.VEHICLE.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
					var drivervehicle = new DRIVER_VEHICLE
					{
						VEHICLE = vehicleId.Value,
						VEHICLE1 = vehicle,
						DRIVER = driver,
						DRIVER_ID = driver.DRIVER_ID,
						DRIVER_NUMBER = 0
					};

					entities.AddToDRIVER_VEHICLE(drivervehicle);
					entities.SaveChangesBySession(SessionId);
				}
				return GetDriversFromEntity(new List<DRIVER>() { driver }).First();
			}
		}

		public void DeleteBaseVehicleDriver(int driverId)
		{
			using (var entities = new Entities())
			{
				if (!entities.DRIVER.Any(d => d.DEPARTMENT == ComputedDepartmentId && d.DRIVER_ID == driverId))
					return;

				var items = entities.DRIVER_VEHICLE.Where(d => d.DRIVER_ID == driverId);

				var driver = entities.DRIVER.FirstOrDefault(d => d.DRIVER_ID == driverId && ComputedDepartmentId == d.DEPARTMENT);
				if (driver!=null)
				{
					foreach(var c in items)
						entities.DeleteObject(c);

					entities.DeleteObject(driver);
					entities.SaveChangesBySession(SessionId);
				}
			}
		}
	}   
}
