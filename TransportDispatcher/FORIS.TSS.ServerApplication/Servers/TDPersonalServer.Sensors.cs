﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Resources;

namespace FORIS.TSS.ServerApplication
{
	public partial class TDPersonalServer
	{
		private void Localize(IEnumerable<Sensor> result)
		{
			var resourceContainers = ResourceContainers.Get(_cultureInfo);
			foreach (var sensor in result)
				sensor.LocalizeSensorMeta(resourceContainers);
		}
		/// <summary> Проверка прав текущего пользователя на просмотр значений и описания датчиков </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <returns></returns>
		public bool IsAllowedSensorsView(int vehicleId)
		{
			return IsAllowedSensorsEdit(vehicleId) ||
				IsAllowedVehicleAll(vehicleId, UserRole.SensorViewRightsAll);
		}
		/// <summary> Проверка прав текущего пользователя на просмотр значений и редактирование описания датчиков </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <returns></returns>
		public bool IsAllowedSensorsEdit(int vehicleId)
		{
			return IsAllowedVehicleAll(vehicleId, UserRole.SensorEditRightsAll);
		}
		public int? AddSensorMapping(int vehicleId, SensorMap sensorMap)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsEdit(vehicleId))
				return null;

			using (var entities = new Entities())
			{

				var ormController = entities.CONTROLLER
					.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == vehicleId);
				if (ormController == null)
					return null; //Нет такого терминала

				ormController.CONTROLLER_TYPEReference.Load();

				var controllerSensor =
				(
					from cs in entities.CONTROLLER_SENSOR
					where
						cs.NUMBER                             == sensorMap.InputNumber &&
						cs.CONTROLLER_TYPE.CONTROLLER_TYPE_ID == ormController.CONTROLLER_TYPE.CONTROLLER_TYPE_ID
					select cs
				)
					.FirstOrDefault();

				if (controllerSensor == null)
					return null;

				var sensorLegendNumber = (int)sensorMap.SensorLegend;
				var legend = (from l in entities.CONTROLLER_SENSOR_LEGEND
							  where l.Number == sensorLegendNumber
							  select l).FirstOrDefault();
				if (legend == null)
					return null;

				var map = entities.CONTROLLER_SENSOR_MAP
					.FirstOrDefault(m =>
						m.CONTROLLER.CONTROLLER_ID                             == ormController.CONTROLLER_ID           &&
						m.CONTROLLER_SENSOR.CONTROLLER_SENSOR_ID               == controllerSensor.CONTROLLER_SENSOR_ID &&
						m.CONTROLLER_SENSOR_LEGEND.CONTROLLER_SENSOR_LEGEND_ID == legend.CONTROLLER_SENSOR_LEGEND_ID);
				if (map != null)
				{
					map.Multiplier = sensorMap.Multiplier;
					map.Constant   = sensorMap.Constant;
					map.Min_Value  = sensorMap.MinValue;
					map.Max_Value  = sensorMap.MaxValue;
				}
				else
				{
					map = new CONTROLLER_SENSOR_MAP
					{
						CONTROLLER               = ormController,
						CONTROLLER_SENSOR        = controllerSensor,
						CONTROLLER_SENSOR_LEGEND = legend,
						Multiplier               = sensorMap.Multiplier,
						Constant                 = sensorMap.Constant,
						Min_Value                = sensorMap.MinValue,
						Max_Value                = sensorMap.MaxValue
					};
					entities.AddToCONTROLLER_SENSOR_MAP(map);
					entities.AddDefaultRules(ormController, legend);
				}

				entities.SaveChangesBySession(SessionId);
				return map.CONTROLLER_SENSOR_MAP_ID;
			}
		}
		[Obsolete]
		public void UpdateSensorMapping(SensorMap sensorMap)
		{
			using (var entities = new Entities())
			{
				var map =
				(
					from csm in entities.CONTROLLER_SENSOR_MAP
						.Include("CONTROLLER")
						.Include("CONTROLLER.CONTROLLER_TYPE")
						.Include("CONTROLLER_SENSOR")
						.Include("CONTROLLER_SENSOR_LEGEND")
						.Include("CONTROLLER.VEHICLE")
					where csm.CONTROLLER_SENSOR_MAP_ID == sensorMap.Id
					select csm
				).FirstOrDefault();

				if (map == null)
					return;

				var ormController = map?.CONTROLLER;
				if (ormController == null)
					return; //Нет такого терминала
				var ormVehicle = ormController.VEHICLE;
				if (ormVehicle == null)
					return;
				/// Проверка прав на операцию
				if (!IsAllowedSensorsEdit(ormVehicle.VEHICLE_ID))
					return;

				var controllerSensor =
				(
					from cs in entities.CONTROLLER_SENSOR
					where
						cs.NUMBER                             == sensorMap.InputNumber &&
						cs.CONTROLLER_TYPE.CONTROLLER_TYPE_ID == ormController.CONTROLLER_TYPE.CONTROLLER_TYPE_ID
					select cs
				)
					.FirstOrDefault();

				if (controllerSensor == null)
					return;

				var sensorLegendNumber = (int)sensorMap.SensorLegend;
				var legend =
				(
					from l in entities.CONTROLLER_SENSOR_LEGEND
					where l.Number == sensorLegendNumber
					select l
				)
					.FirstOrDefault();
				if (legend == null)
					return;

				map.CONTROLLER_SENSOR = controllerSensor;
				map.CONTROLLER_SENSOR_LEGEND = legend;
				map.Multiplier = sensorMap.Multiplier;
				map.Constant = sensorMap.Constant;
				map.Min_Value = sensorMap.MinValue;
				map.Max_Value = sensorMap.MaxValue;

				entities.SaveChangesBySession(SessionId);
			}
		}
		[Obsolete]
		public void DeleteSensorMapping(int mappingId)
		{
			using (var entities = new Entities())
			{
				var map =
				(
					from csm in entities.CONTROLLER_SENSOR_MAP.Include("CONTROLLER").Include("CONTROLLER.VEHICLE")
					where csm.CONTROLLER_SENSOR_MAP_ID == mappingId
					select csm
				)
					.FirstOrDefault();

				if (map == null)
					return;
				var ormController = map.CONTROLLER;
				if (ormController == null)
					return; //Нет такого терминала
				var ormVehicle = ormController.VEHICLE;
				if (ormVehicle == null)
					return;
				/// Проверка прав на операцию
				if (!IsAllowedSensorsEdit(ormVehicle.VEHICLE_ID))
					return;

				entities.DeleteMapping(map);

				entities.SaveChangesBySession(SessionId);
			}
		}
		[Obsolete]
		public SensorMap[] GetSensorMappings(int vehicleId)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsView(vehicleId))
				return new SensorMap[0];

			using (var entities = new Entities())
			{
				var result =
				(
					from v in entities.VEHICLE
					from csm in v.CONTROLLER.CONTROLLER_SENSOR_MAP
					where v.VEHICLE_ID == vehicleId
					select new SensorMap
					{
						Id           = csm.CONTROLLER_SENSOR_MAP_ID,
						SensorLegend = (SensorLegend)csm.CONTROLLER_SENSOR_LEGEND.Number,
						InputNumber  = csm.CONTROLLER_SENSOR.NUMBER,
						Multiplier   = csm.Multiplier ?? 0,
						Constant     = csm.Constant ?? 0,
						MinValue     = csm.Min_Value,
						MaxValue     = csm.Max_Value
					}
				)
					.ToArray();
				return result;
			}
		}
		public void SetSensorsMapping(int vehicleId, SensorLegendMapping[] sensorMap)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsEdit(vehicleId))
				return;

			using (var entities = new Entities())
			{
				var ormController = entities.CONTROLLER.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == vehicleId);
				if (ormController == null)
					return; //Нет такого терминала

				ormController.CONTROLLER_TYPEReference.Load();

				foreach (var legendMapping in sensorMap)
				{
					entities.SetSensorLegendMapping(ormController, legendMapping.SensorLegend, legendMapping.Mappings);
					entities.SaveChangesBySession(SessionId);
				}
			}
		}
		public void SetSensorLegendMapping(int vehicleId, SensorLegend sensorLegend, SensorMappingValue[] mappings)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsEdit(vehicleId))
				return;
			using (var entities = new Entities())
			{
				var ormController = entities.CONTROLLER.FirstOrDefault(c => c.VEHICLE.VEHICLE_ID == vehicleId);
				if (ormController == null)
					return; //Нет такого терминала

				ormController.CONTROLLER_TYPEReference.Load();

				entities.SetSensorLegendMapping(ormController, sensorLegend, mappings);
				entities.SaveChangesBySession(SessionId);
			}
		}
		public SensorLegendMapping[] GetSensorsMappings(int vehicleId)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsView(vehicleId))
				return new SensorLegendMapping[0];

			using (var entities = new Entities())
			{
				var result = (
					from v in entities.VEHICLE
					from csm in v.CONTROLLER.CONTROLLER_SENSOR_MAP
					where v.VEHICLE_ID == vehicleId
					select new
					{
						Id           = csm.CONTROLLER_SENSOR_MAP_ID,
						SensorLegend = csm.CONTROLLER_SENSOR_LEGEND.Number,
						InputNumber  = csm.CONTROLLER_SENSOR.NUMBER,
						Multiplier   = csm.Multiplier ?? 0,
						Constant     = csm.Constant ?? 0,
						MinValue     = csm.Min_Value,
						MaxValue     = csm.Max_Value
					})
					.ToArray()
					.GroupBy(s => s.SensorLegend)
					.Select(value => new SensorLegendMapping
					{
						SensorLegend = (SensorLegend)value.Key,
						Mappings = value.Select(s => new SensorMappingValue
						{
							Id = s.Id,
							InputNumber = s.InputNumber,
							Multiplier  = s.Multiplier,
							Constant    = s.Constant,
							MinValue    = s.MinValue,
							MaxValue    = s.MaxValue
						}).ToArray()
					}).ToArray();
				return result;
			}
		}
		public SensorMappingValue[] GetSensorLegendMappings(int vehicleId, SensorLegend sensorLegend)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsView(vehicleId))
				return new SensorMappingValue[0];

			using (var entities = new Entities())
			{
				var result =
				(
					from v in entities.VEHICLE
					from csm in v.CONTROLLER.CONTROLLER_SENSOR_MAP
					where v.VEHICLE_ID == vehicleId && csm.CONTROLLER_SENSOR_LEGEND.Number == (int)sensorLegend
					select new SensorMappingValue
					{
						Id          = csm.CONTROLLER_SENSOR_MAP_ID,
						InputNumber = csm.CONTROLLER_SENSOR.NUMBER,
						Multiplier  = csm.Multiplier ?? 0,
						Constant    = csm.Constant ?? 0,
						MinValue    = csm.Min_Value,
						MaxValue    = csm.Max_Value
					}
				)
					.ToArray();
				return result;
			}
		}
		public ControllerInput[] GetControllerInputs(int vehicleId)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsEdit(vehicleId))
				return new ControllerInput[0];

			ControllerInput[] inputs;
			using (var entities = new Entities())
			{
				inputs =
				(
					from c in entities.CONTROLLER
					from cs in c.CONTROLLER_TYPE.CONTROLLER_SENSOR
					where c.VEHICLE.VEHICLE_ID == vehicleId
					select new
					{
						Number            = cs.NUMBER,
						Name              = cs.Descript,
						DefaultLegend     = cs.CONTROLLER_SENSOR_LEGEND,
						DefaultMultuplier = cs.Default_Multiplier,
						DefaultConstant   = cs.Default_Constant
					})
						.ToList()
						.Select(x => new ControllerInput
						{
							Number            = x.Number,
							Name              = x.Name,
							DefaultLegend     = x.DefaultLegend != null ? (SensorLegend)x.DefaultLegend.Number : (SensorLegend?)null,
							DefaultMultiplier = x.DefaultLegend != null ? (x.DefaultMultuplier ?? 1)           : (decimal?)null,
							DefaultConstant   = x.DefaultLegend != null ? (x.DefaultConstant ?? 0)             : (decimal?)null
						}
				)
					.ToArray();
			}

			var rawValues = Server.Instance().GetRawSensorValues(vehicleId).ToDictionary(
				inputLogRecord => inputLogRecord.Number,
				inputLogRecord => inputLogRecord.Value);

			foreach (var input in inputs)
			{
				long rawValue;
				if (rawValues.TryGetValue(input.Number, out rawValue))
					input.Value = rawValue;
			}

			return inputs;
		}
		public List<Sensor> GetSensorLegends(int vehicleId)
		{
			/// Проверка прав на операцию
			if (!IsAllowedSensorsView(vehicleId))
				return new List<Sensor>();

			using (var entities = new Entities())
			{
				var hasFreeControllerSensors = entities
					.CONTROLLER
					.Where(c => c.VEHICLE.VEHICLE_ID == vehicleId)
					.SelectMany(c => c.CONTROLLER_TYPE.CONTROLLER_SENSOR)
					.Any(cs => !cs.Mandatory);

				IQueryable<CONTROLLER_SENSOR_LEGEND> query = entities.CONTROLLER_SENSOR_LEGEND.Include("CONTROLLER_SENSOR_TYPE");

				if (hasFreeControllerSensors || IsSuperAdministrator)
				{
					var vehicleKind = (VehicleKind)entities.VEHICLE.Where(v => v.VEHICLE_ID == vehicleId).Select(v => v.VEHICLE_KIND.VEHICLE_KIND_ID).First();

					SensorLegend[] sensorLegends;
					switch (vehicleKind)
					{
						case VehicleKind.WebCamera:
							sensorLegends = Server.VideoAnalyticsSensors;
							break;
						case VehicleKind.MobilePhone:
							sensorLegends = new SensorLegend[] {};
							break;
						case VehicleKind.Tracker:
							sensorLegends = new[]
							{
								SensorLegend.DigitalTemperatureSensor,
								SensorLegend.Alarm,
								SensorLegend.BatteryLevel,
								SensorLegend.IsPlugged,
								SensorLegend.ControllerMode,
								SensorLegend.PositionTracking,
								SensorLegend.HDOP
							};

							if (IsSuperAdministrator)
							{
								sensorLegends = sensorLegends
									.Union(new[]
									{
										SensorLegend.NetworkConnected,
										SensorLegend.GPSLocationProvider,
										SensorLegend.NetworkLocationProvider,
										SensorLegend.WiFi,
										SensorLegend.AirplaneMode
									})
									.ToArray();
							}

							break;
						case VehicleKind.ResourceMeter:
							sensorLegends = new[]
							{
								SensorLegend.ConsumedPower,
								SensorLegend.Power,
								SensorLegend.Current,
								SensorLegend.RelayStatus,
								SensorLegend.CoverOpeningTimes,
								SensorLegend.HeartBeatInterval
							};
							break;
						default:
							sensorLegends = null;
							break;
					}

					if (sensorLegends != null)
					{
						var sensorLegendNumbers = sensorLegends.Select(@enum => (int) @enum).ToArray();

						query =
							query.Where(
								legend =>
								//Из заданного списка доступных датчиков
								sensorLegendNumbers.Contains(legend.Number) ||
								//На этом автомобиле этот датчик уже настроен
								legend.CONTROLLER_SENSOR_MAP.Any(
									csm =>
									csm.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId));
					}
				}
				else
				{
					query = query.Where(legend => legend.CONTROLLER_SENSOR_MAP.Any(csm => csm.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId));
				}


				var result = query.ToList().ConvertAll(c => c.ToDTO());

				Localize(result);

				return result;
			}
		}
		public Sensor[] GetConfiguredSensorLegends(SensorType? sensorType, int? vehicleId)
		{
			using (var entities = new Entities())
			{
				IQueryable<CONTROLLER_SENSOR_LEGEND> query = entities.CONTROLLER_SENSOR_LEGEND.Include("CONTROLLER_SENSOR_TYPE");

				if (sensorType != null)
				{
					var sensorTypeId = (int)sensorType.Value;
					query = query.Where(sensor => sensor.CONTROLLER_SENSOR_TYPE.CONTROLLER_SENSOR_TYPE_ID == sensorTypeId);
				}

				if (vehicleId != null && !IsAllowedVehicleAll(vehicleId.Value, UserRole.DefaultOprRightsOnVehicle))
					throw new SecurityException($"Access denied for operator {OperatorId} to vehicle {vehicleId.Value}");

				if (vehicleId != null && !IsAllowedSensorsView(vehicleId.Value))
					return new Sensor[0];

				if (vehicleId != null)
				{
					var configuredSensorLegendNumbers = entities.VEHICLE
						.Where(v => v.VEHICLE_ID == vehicleId.Value)
						.SelectMany(v => v.CONTROLLER.CONTROLLER_SENSOR_MAP)
						.Select(csm => csm.CONTROLLER_SENSOR_LEGEND.Number)
						.Union(entities.VEHICLE
							.Where(v => v.VEHICLE_ID == vehicleId.Value)
							.SelectMany(v => v.CompoundRule)
							.SelectMany(cr => cr.CompoundRule_Sensor)
							.Select(crs => crs.CONTROLLER_SENSOR_LEGEND.Number))
						.Distinct()
						.ToArray();

					query = query.Where(sensor => configuredSensorLegendNumbers.Contains(sensor.Number));
				}
				else
				{
					query = query.Where(
						legend =>
						legend.CONTROLLER_SENSOR_MAP.Any(
							csm =>
							csm.CONTROLLER.VEHICLE.Accesses.Any(
								ovr => ovr.operator_id == OperatorId && ovr.right_id == (int)SystemRight.VehicleAccess)) ||
						legend.CompoundRule_Sensor.Any(
							crs =>
							crs.CompoundRule.CompoundRule_Message_Template.Any(
								crmt => crmt.OPERATOR.OPERATOR_ID == OperatorId)));
				}

				var result = query
					.ToArray()
					.Select(legend => legend.ToDTO())
					.ToArray();

				Localize(result);

				return result;
			}
		}
		public void SensorValueChange(int vehicleId, SensorLegend sensorLegend, int logTime, decimal? value)
		{
			if (!IsAllowedSensorsEdit(vehicleId))
				return;
			using (var entities = new Entities())
			{
				var existingSensorValueChanges = entities.SensorValueChange
					.Where(svc =>
						svc.VEHICLE.VEHICLE_ID              == vehicleId         &&
						svc.CONTROLLER_SENSOR_LEGEND.Number == (int)sensorLegend &&
						svc.Log_Time                        == logTime)
					.OrderBy(svc => svc.SensorValueChange_ID)
					.ToArray();
				if (existingSensorValueChanges.Length > 1)
				{
					foreach (var svc in existingSensorValueChanges.Take(existingSensorValueChanges.Length - 1))
						entities.DeleteSensorValueChange(svc);
				}

				var svcToUpdate = existingSensorValueChanges.LastOrDefault();
				if (value == null)
				{
					if (svcToUpdate != null)
						entities.DeleteSensorValueChange(svcToUpdate);
				}
				else
				{
					if (svcToUpdate == null)
					{
						svcToUpdate = new SensorValueChange
						{
							VEHICLE                  = entities.GetVehicle(vehicleId),
							CONTROLLER_SENSOR_LEGEND = entities.GetSensorLegend(sensorLegend),
							Log_Time                 = logTime
						};
						entities.SensorValueChange.AddObject(svcToUpdate);
					}
					svcToUpdate.Value = value.Value;
				}
				entities.SaveChangesBySession(SessionId);
			}
		}
	}
}