﻿namespace FORIS.TSS.ServerApplication
{
	partial class ProjectInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceInstallerBusinessService = new System.ServiceProcess.ServiceInstaller();
			this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			// 
			// serviceInstallerBusinessService
			// 
			this.serviceInstallerBusinessService.DisplayName = "FORIS.TSS.Server";
			this.serviceInstallerBusinessService.ServiceName = "FORIS.TSS.Server";
			this.serviceInstallerBusinessService.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// serviceProcessInstaller
			// 
			this.serviceProcessInstaller.Password = null;
			this.serviceProcessInstaller.Username = null;
			// 
			// ProjectInstaller1
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
			this.serviceInstallerBusinessService,
			this.serviceProcessInstaller});

		}

		#endregion

		private System.ServiceProcess.ServiceInstaller serviceInstallerBusinessService;
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
	}
}