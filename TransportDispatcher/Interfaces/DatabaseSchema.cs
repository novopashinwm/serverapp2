using System.Data;

namespace FORIS.TSS.BusinessLogic
{
    ///<summary>
    /// Class for creating table schema on client
    ///</summary>
    public class DatabaseSchema : DatabaseSchemaBase, IDatabaseSchema
    {
        #region IDatabaseSchema Members
        void IDatabaseSchema.MakeSchema(DataSet dataSet)
        {
            DatabaseSchema.MakeSchema(dataSet);
        }
        #endregion // IDatabaseSchema Members
        /// <summary>
        /// Initialises list of tables
        /// </summary>
        static DatabaseSchema()
        {
            tables.Add("B_data");
            tables.Add("BLADING_TYPE");
            tables.Add("BUSSTOP");
            tables.Add("CALENDAR_DAY");
            tables.Add("CONSTANTS");
            tables.Add("CONTRACTOR");
            tables.Add("CONTRACTOR_CALENDAR");
            tables.Add("CONTROLLER_SENSOR_TYPE");
            tables.Add("CONTROLLER_SENSOR_LEGEND");
            tables.Add("CONTROLLER_TYPE");
            tables.Add("CONTROLLER_SENSOR");
            tables.Add("COUNT_GPRS");
            tables.Add("COUNT_SMS");
            tables.Add("CUSTOMER");
            tables.Add("DAY");
            tables.Add("DAY_TYPE");
            tables.Add("DAY_TIME");
            tables.Add("DEPARTMENT");
            tables.Add("DECADE");
            tables.Add("DK_SET");
            tables.Add("DAY_KIND");
            tables.Add("CAL");
            tables.Add("DOTNET_TYPE");
            tables.Add("DRIVER_BONUS");
            tables.Add("DRIVER_MSG_TEMPLATE");
            tables.Add("DRIVER_STATUS");
            tables.Add("DRIVER");
            tables.Add("DRIVERGROUP");
            tables.Add("DRIVERGROUP_DRIVER");
            tables.Add("EMAIL");
            tables.Add("FACTORS");
            tables.Add("GEO_ZONE");
            tables.Add("GOODS_TYPE");
            tables.Add("GOODS");
            tables.Add("GRAPHIC");
            tables.Add("GRAPHIC_SHIFT");
            tables.Add("GUARD_EVENTS");
            tables.Add("Info_size_fileData");
            tables.Add("Job_DelBadXY");
            tables.Add("JOURNAL_DRIVER");
            tables.Add("kolomna_points");
            tables.Add("kolomna_points2");
            tables.Add("kolomna_points3");
            tables.Add("LOG_GUARD_EVENTS");
            tables.Add("LOGISTIC_ADDRESS");
            tables.Add("LOGISTIC_ORDER");
            tables.Add("GOODS_LOGISTIC_ORDER");
            tables.Add("MAPS");
            tables.Add("MAP_VERTEX");
            tables.Add("MEDIA_TYPE");
            tables.Add("MEDIA");
            tables.Add("MEDIA_ACCEPTORS");
            tables.Add("MESSAGE_BOARD");
            tables.Add("MESSAGE_TEMPLATE");
            tables.Add("MESSAGE");
            tables.Add("MESSAGE_TEMPLATE_FIELD");
            tables.Add("MESSAGE_FIELD");
            tables.Add("MONITOREE_LOG");
            tables.Add("OPERATOR");
            tables.Add("MESSAGE_OPERATOR");
            tables.Add("OPERATOR_PROFILE");
            tables.Add("OPERATORGROUP");
            tables.Add("OPERATORGROUP_OPERATOR");
            tables.Add("OPS_EVENT");
            tables.Add("LOG_OPS_EVENTS");
            tables.Add("ORDER_TYPE");
            tables.Add("ORDER");
            tables.Add("OWNER");
            tables.Add("PARAMETER");
            tables.Add("POINT_KIND");
            tables.Add("POINT");
            tables.Add("REPORT");
            tables.Add("REPRINT_REASON");
            tables.Add("RESERV");
            tables.Add("RIGHT");
            tables.Add("OPERATOR_DEPARTMENT");
            tables.Add("OPERATOR_DRIVER");
            tables.Add("OPERATOR_DRIVERGROUP");
            tables.Add("OPERATOR_REPORT");
            tables.Add("OPERATOR_ZONE");
            tables.Add("OPERATORGROUP_DEPARTMENT");
            tables.Add("OPERATORGROUP_DRIVER");
            tables.Add("OPERATORGROUP_DRIVERGROUP");
            tables.Add("OPERATORGROUP_REPORT");
            tables.Add("OPERATORGROUP_ZONE");
            tables.Add("RIGHT_OPERATOR");
            tables.Add("RIGHT_OPERATORGROUP");
            tables.Add("ROUTE");
            tables.Add("GEO_TRIP");
            tables.Add("GEO_SEGMENT");
            tables.Add("GEO_SEGMENT_RUNTIME");
            tables.Add("GEO_SEGMENT_VERTEX");
            tables.Add("OPERATOR_ROUTE");
            tables.Add("OPERATORGROUP_ROUTE");
            tables.Add("ROUTE_GEOZONE");
            tables.Add("ROUTE_POINT");
            tables.Add("ROUTE_TRIP");
            tables.Add("ROUTEGROUP");
            tables.Add("OPERATOR_ROUTEGROUP");
            tables.Add("OPERATORGROUP_ROUTEGROUP");
            tables.Add("ROUTEGROUP_ROUTE");
            tables.Add("RS");
            tables.Add("RS_OPERATIONSTYPE");
            tables.Add("RS_TRIPSTYPE");
            tables.Add("RS_TYPE");
            tables.Add("RS_VARIANT");
            tables.Add("RS_PERIOD");
            tables.Add("RS_ROUND_TRIP");
            tables.Add("RS_STEP");
            tables.Add("RS_NUMBER");
            tables.Add("RS_TRIP");
            tables.Add("RS_OPERATIONTYPE");
            tables.Add("RS_RUNTIME");
            tables.Add("RS_TRIPTYPE");
            tables.Add("RULE");
            tables.Add("SCHEDULEREVENT");
            tables.Add("EMAIL_SCHEDULEREVENT");
            tables.Add("SCHEDULERQUEUE");
            tables.Add("EMAIL_SCHEDULERQUEUE");
            tables.Add("SEASON");
            tables.Add("FACTOR_VALUES");
            tables.Add("PERIOD");
            tables.Add("SEATTYPE");
            tables.Add("Servers");
            tables.Add("SESSION");
            tables.Add("SMS_TYPE");
            tables.Add("sysdiagrams");
            tables.Add("TASK_PROCESSOR_LOG");
            tables.Add("Temp_TSS");
            tables.Add("TmpDriverName");
            tables.Add("TRAIL");
            tables.Add("VEHICLE_KIND");
            tables.Add("VEHICLE_STATUS");
            tables.Add("VEHICLE");
            tables.Add("BRIGADE");
            tables.Add("BRIGADE_DRIVER");
            tables.Add("CONTROLLER");
            tables.Add("CONTROLLER_INFO");
            tables.Add("CONTROLLER_SENSOR_MAP");
            tables.Add("CONTROLLER_STAT");
            tables.Add("CONTROLLER_TIME");
            tables.Add("COUNT_GSM");
            tables.Add("DRIVER_VEHICLE");
            tables.Add("GOODS_TYPE_VEHICLE");
            tables.Add("JOURNAL_VEHICLE");
            tables.Add("LOG_VEHICLE_STATUS");
            tables.Add("OPERATOR_VEHICLE");
            tables.Add("OPERATORGROUP_VEHICLE");
            tables.Add("SMS_LOG");
            tables.Add("VEHICLE_OWNER");
            tables.Add("VEHICLE_PICTURE");
            tables.Add("VEHICLE_RULE");
            tables.Add("VEHICLEGROUP");
            tables.Add("OPERATOR_VEHICLEGROUP");
            tables.Add("OPERATORGROUP_VEHICLEGROUP");
            tables.Add("VEHICLEGROUP_RULE");
            tables.Add("VEHICLEGROUP_VEHICLE");
            tables.Add("Version");
            tables.Add("WAYBILL");
            tables.Add("BLADING");
            tables.Add("WAYBILLMARK");
            tables.Add("WAYOUT");
            tables.Add("JOURNAL");
            tables.Add("JOURNAL_DAY");
            tables.Add("ORDER_TRIP");
            tables.Add("RS_WAYOUT");
            tables.Add("RS_SHIFT");
            tables.Add("RS_POINT");
            tables.Add("SCHEDULE");
            tables.Add("LOG_WAYBILL_HEADER_STATUS");
            tables.Add("SCHEDULE_DETAIL");
            tables.Add("SCHEDULE_DETAIL_INFO");
            tables.Add("WEB_LINE");
            tables.Add("WEB_LINE_VERTEX");
            tables.Add("WEB_POINT_TYPE");
            tables.Add("WEB_POINT");
            tables.Add("WORK_TIME");
            tables.Add("WORKPLACE_CONFIG");
            tables.Add("WORKSTATION");
            tables.Add("WORKTIME_STATUS_TYPE");
            tables.Add("TRIP_KIND");
            tables.Add("TRIP");
            tables.Add("SCHEDULE_GEOZONE");
            tables.Add("SCHEDULE_POINT");
            tables.Add("WAYBILL_HEADER");
            tables.Add("SCHEDULE_PASSAGE");
            tables.Add("WAYBILLHEADER_WAYBILLMARK");
            tables.Add("WORKTIME_REASON");
            tables.Add("WB_TRIP");
            tables.Add("ZONE_TYPE");
            tables.Add("ZONE_PRIMITIVE");
            tables.Add("GEO_ZONE_PRIMITIVE");
            tables.Add("ZONE_PRIMITIVE_VERTEX");
            tables.Add("ZONE_VEHICLE");
            tables.Add("ZONE_VEHICLEGROUP");
            tables.Add("ZONEGROUP");
            tables.Add("OPERATOR_ZONEGROUP");
            tables.Add("OPERATORGROUP_ZONEGROUP");
            tables.Add("ZONEGROUP_ZONE");
            h_tables.Add("H_B_data");
            h_tables.Add("H_BLADING_TYPE");
            h_tables.Add("H_BUSSTOP");
            h_tables.Add("H_CALENDAR_DAY");
            h_tables.Add("H_CONTRACTOR");
            h_tables.Add("H_CONTRACTOR_CALENDAR");
            h_tables.Add("H_CONTROLLER_SENSOR_TYPE");
            h_tables.Add("H_CONTROLLER_SENSOR_LEGEND");
            h_tables.Add("H_CONTROLLER_TYPE");
            h_tables.Add("H_CONTROLLER_SENSOR");
            h_tables.Add("H_COUNT_GPRS");
            h_tables.Add("H_CUSTOMER");
            h_tables.Add("H_DAY");
            h_tables.Add("H_DAY_TYPE");
            h_tables.Add("H_DAY_TIME");
            h_tables.Add("H_DEPARTMENT");
            h_tables.Add("H_DECADE");
            h_tables.Add("H_DK_SET");
            h_tables.Add("H_DAY_KIND");
            h_tables.Add("H_CAL");
            h_tables.Add("H_DOTNET_TYPE");
            h_tables.Add("H_DRIVER_BONUS");
            h_tables.Add("H_DRIVER_MSG_TEMPLATE");
            h_tables.Add("H_DRIVER_STATUS");
            h_tables.Add("H_DRIVER");
            h_tables.Add("H_DRIVERGROUP");
            h_tables.Add("H_DRIVERGROUP_DRIVER");
            h_tables.Add("H_EMAIL");
            h_tables.Add("H_FACTORS");
            h_tables.Add("H_GEO_ZONE");
            h_tables.Add("H_GOODS_TYPE");
            h_tables.Add("H_GOODS");
            h_tables.Add("H_GRAPHIC");
            h_tables.Add("H_GRAPHIC_SHIFT");
            h_tables.Add("H_GUARD_EVENTS");
            h_tables.Add("H_Info_size_fileData");
            h_tables.Add("H_Job_DelBadXY");
            h_tables.Add("H_JOURNAL_DRIVER");
            h_tables.Add("H_kolomna_points");
            h_tables.Add("H_kolomna_points2");
            h_tables.Add("H_kolomna_points3");
            h_tables.Add("H_LOGISTIC_ADDRESS");
            h_tables.Add("H_LOGISTIC_ORDER");
            h_tables.Add("H_GOODS_LOGISTIC_ORDER");
            h_tables.Add("H_MAPS");
            h_tables.Add("H_MAP_VERTEX");
            h_tables.Add("H_MEDIA_TYPE");
            h_tables.Add("H_MEDIA");
            h_tables.Add("H_MEDIA_ACCEPTORS");
            h_tables.Add("H_MESSAGE_BOARD");
            h_tables.Add("H_MESSAGE_TEMPLATE");
            h_tables.Add("H_MESSAGE");
            h_tables.Add("H_MESSAGE_TEMPLATE_FIELD");
            h_tables.Add("H_MESSAGE_FIELD");
            h_tables.Add("H_OPERATOR");
            h_tables.Add("H_MESSAGE_OPERATOR");
            h_tables.Add("H_OPERATOR_PROFILE");
            h_tables.Add("H_OPERATORGROUP");
            h_tables.Add("H_OPERATORGROUP_OPERATOR");
            h_tables.Add("H_ORDER_TYPE");
            h_tables.Add("H_ORDER");
            h_tables.Add("H_OWNER");
            h_tables.Add("H_PARAMETER");
            h_tables.Add("H_POINT_KIND");
            h_tables.Add("H_POINT");
            h_tables.Add("H_REPORT");
            h_tables.Add("H_REPRINT_REASON");
            h_tables.Add("H_RESERV");
            h_tables.Add("H_RIGHT");
            h_tables.Add("H_OPERATOR_DEPARTMENT");
            h_tables.Add("H_OPERATOR_DRIVER");
            h_tables.Add("H_OPERATOR_DRIVERGROUP");
            h_tables.Add("H_OPERATOR_REPORT");
            h_tables.Add("H_OPERATOR_ZONE");
            h_tables.Add("H_OPERATORGROUP_DEPARTMENT");
            h_tables.Add("H_OPERATORGROUP_DRIVER");
            h_tables.Add("H_OPERATORGROUP_DRIVERGROUP");
            h_tables.Add("H_OPERATORGROUP_REPORT");
            h_tables.Add("H_OPERATORGROUP_ZONE");
            h_tables.Add("H_RIGHT_OPERATOR");
            h_tables.Add("H_RIGHT_OPERATORGROUP");
            h_tables.Add("H_ROUTE");
            h_tables.Add("H_GEO_SEGMENT");
            h_tables.Add("H_GEO_SEGMENT_RUNTIME");
            h_tables.Add("H_GEO_SEGMENT_VERTEX");
            h_tables.Add("H_OPERATOR_ROUTE");
            h_tables.Add("H_OPERATORGROUP_ROUTE");
            h_tables.Add("H_ROUTE_GEOZONE");
            h_tables.Add("H_ROUTE_POINT");
            h_tables.Add("H_ROUTEGROUP");
            h_tables.Add("H_OPERATOR_ROUTEGROUP");
            h_tables.Add("H_OPERATORGROUP_ROUTEGROUP");
            h_tables.Add("H_ROUTEGROUP_ROUTE");
            h_tables.Add("H_RS");
            h_tables.Add("H_RS_OPERATIONSTYPE");
            h_tables.Add("H_RS_TYPE");
            h_tables.Add("H_RS_VARIANT");
            h_tables.Add("H_RS_PERIOD");
            h_tables.Add("H_RS_STEP");
            h_tables.Add("H_RS_NUMBER");
            h_tables.Add("H_RS_OPERATIONTYPE");
            h_tables.Add("H_RS_RUNTIME");
            h_tables.Add("H_RULE");
            h_tables.Add("H_SCHEDULEREVENT");
            h_tables.Add("H_EMAIL_SCHEDULEREVENT");
            h_tables.Add("H_SCHEDULERQUEUE");
            h_tables.Add("H_EMAIL_SCHEDULERQUEUE");
            h_tables.Add("H_SEASON");
            h_tables.Add("H_FACTOR_VALUES");
            h_tables.Add("H_PERIOD");
            h_tables.Add("H_SEATTYPE");
            h_tables.Add("H_Servers");
            h_tables.Add("H_SMS_TYPE");
            h_tables.Add("H_Temp_TSS");
            h_tables.Add("H_TmpDriverName");
            h_tables.Add("H_VEHICLE_KIND");
            h_tables.Add("H_VEHICLE_STATUS");
            h_tables.Add("H_VEHICLE");
            h_tables.Add("H_BRIGADE");
            h_tables.Add("H_BRIGADE_DRIVER");
            h_tables.Add("H_CONTROLLER");
            h_tables.Add("H_CONTROLLER_SENSOR_MAP");
            h_tables.Add("H_CONTROLLER_TIME");
            h_tables.Add("H_DRIVER_VEHICLE");
            h_tables.Add("H_GOODS_TYPE_VEHICLE");
            h_tables.Add("H_JOURNAL_VEHICLE");
            h_tables.Add("H_LOG_VEHICLE_STATUS");
            h_tables.Add("H_OPERATOR_VEHICLE");
            h_tables.Add("H_OPERATORGROUP_VEHICLE");
            h_tables.Add("H_VEHICLE_OWNER");
            h_tables.Add("H_VEHICLE_RULE");
            h_tables.Add("H_VEHICLEGROUP");
            h_tables.Add("H_OPERATOR_VEHICLEGROUP");
            h_tables.Add("H_OPERATORGROUP_VEHICLEGROUP");
            h_tables.Add("H_VEHICLEGROUP_RULE");
            h_tables.Add("H_VEHICLEGROUP_VEHICLE");
            h_tables.Add("H_Version");
            h_tables.Add("H_WAYBILL");
            h_tables.Add("H_BLADING");
            h_tables.Add("H_WAYBILLMARK");
            h_tables.Add("H_WAYOUT");
            h_tables.Add("H_JOURNAL");
            h_tables.Add("H_JOURNAL_DAY");
            h_tables.Add("H_RS_WAYOUT");
            h_tables.Add("H_RS_SHIFT");
            h_tables.Add("H_RS_POINT");
            h_tables.Add("H_SCHEDULE");
            h_tables.Add("H_LOG_WAYBILL_HEADER_STATUS");
            h_tables.Add("H_SCHEDULE_DETAIL");
            h_tables.Add("H_SCHEDULE_DETAIL_INFO");
            h_tables.Add("H_WEB_LINE");
            h_tables.Add("H_WEB_LINE_VERTEX");
            h_tables.Add("H_WEB_POINT_TYPE");
            h_tables.Add("H_WEB_POINT");
            h_tables.Add("H_WORK_TIME");
            h_tables.Add("H_WORKPLACE_CONFIG");
            h_tables.Add("H_WORKSTATION");
            h_tables.Add("H_WORKTIME_STATUS_TYPE");
            h_tables.Add("H_SCHEDULE_GEOZONE");
            h_tables.Add("H_WAYBILL_HEADER");
            h_tables.Add("H_WAYBILLHEADER_WAYBILLMARK");
            h_tables.Add("H_WORKTIME_REASON");
            h_tables.Add("H_ZONE_TYPE");
            h_tables.Add("H_ZONE_PRIMITIVE");
            h_tables.Add("H_GEO_ZONE_PRIMITIVE");
            h_tables.Add("H_ZONE_PRIMITIVE_VERTEX");
            h_tables.Add("H_ZONE_VEHICLE");
            h_tables.Add("H_ZONE_VEHICLEGROUP");
            h_tables.Add("H_ZONEGROUP");
            h_tables.Add("H_OPERATOR_ZONEGROUP");
            h_tables.Add("H_OPERATORGROUP_ZONEGROUP");
            h_tables.Add("H_ZONEGROUP_ZONE");
        }
        ///<summary>
        /// [B_data]
        ///</summary>
        public static void MakeTable_BData(DataSet dataset)
        {
            if (dataset.Tables.Contains("B_data") ) return;
            DataTable table = dataset.Tables.Add("B_data");
            table.Columns.Add("Type", typeof(System.String)); // char
            table.Columns.Add("date_b", typeof(System.DateTime)); // datetime
            table.Columns.Add("res", typeof(System.String)); // char
        }
        ///<summary>
        /// [BLADING]
        ///</summary>
        public static void MakeTable_Blading(DataSet dataset)
        {
            if (dataset.Tables.Contains("BLADING") ) return;
            DataTable table = dataset.Tables.Add("BLADING");
            table.Columns.Add("BLADING_ID", typeof(System.Int32)); // int
            table.Columns.Add("BLADING_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RECEIVE_SUM", typeof(System.Decimal)); // money
            table.Columns.Add("ACTUAL_SUM", typeof(System.Decimal)); // money
            table.Columns.Add("STUFF_NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("BILL_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("TIME_IN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_OUT", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("DISPATCHER_COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("SUM", typeof(System.Decimal)); // money
            table.Columns.Add("CONTRACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILL_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_BLADING
        ///</summary>
        public static void MakePK_Blading(DataSet dataset)
        {
            DataTable table = dataset.Tables["BLADING"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["BLADING_ID"]
            }
            ;
        }
        ///<summary>
        /// BLADING
        ///</summary>
        public static void MakeIdentities_Blading(DataSet dataset)
        {
            DataTable table = dataset.Tables["BLADING"];
            table.Columns["BLADING_ID"].AutoIncrement = true;
            table.Columns["BLADING_ID"].AutoIncrementSeed = -1;
            table.Columns["BLADING_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [BLADING_TYPE]
        ///</summary>
        public static void MakeTable_BladingType(DataSet dataset)
        {
            if (dataset.Tables.Contains("BLADING_TYPE") ) return;
            DataTable table = dataset.Tables.Add("BLADING_TYPE");
            table.Columns.Add("BLADING_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_BLADING_TYPE
        ///</summary>
        public static void MakePK_BladingType(DataSet dataset)
        {
            DataTable table = dataset.Tables["BLADING_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["BLADING_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// BLADING_TYPE
        ///</summary>
        public static void MakeIdentities_BladingType(DataSet dataset)
        {
            DataTable table = dataset.Tables["BLADING_TYPE"];
            table.Columns["BLADING_TYPE_ID"].AutoIncrement = true;
            table.Columns["BLADING_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["BLADING_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [BRIGADE]
        ///</summary>
        public static void MakeTable_Brigade(DataSet dataset)
        {
            if (dataset.Tables.Contains("BRIGADE") ) return;
            DataTable table = dataset.Tables.Add("BRIGADE");
            table.Columns.Add("BRIGADE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DECADE", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE", typeof(System.Int32)); // int
            table.Columns.Add("GRAPHIC", typeof(System.Int32)); // int
            table.Columns.Add("GRAPHIC_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("WAYOUT", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_BRIGADE
        ///</summary>
        public static void MakePK_Brigade(DataSet dataset)
        {
            DataTable table = dataset.Tables["BRIGADE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["BRIGADE_ID"]
            }
            ;
        }
        ///<summary>
        /// BRIGADE
        ///</summary>
        public static void MakeIdentities_Brigade(DataSet dataset)
        {
            DataTable table = dataset.Tables["BRIGADE"];
            table.Columns["BRIGADE_ID"].AutoIncrement = true;
            table.Columns["BRIGADE_ID"].AutoIncrementSeed = -1;
            table.Columns["BRIGADE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [BRIGADE_DRIVER]
        ///</summary>
        public static void MakeTable_BrigadeDriver(DataSet dataset)
        {
            if (dataset.Tables.Contains("BRIGADE_DRIVER") ) return;
            DataTable table = dataset.Tables.Add("BRIGADE_DRIVER");
            table.Columns.Add("BRIGADE_DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("BRIGADE", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_NUMBER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_BRIGADE_DRIVER
        ///</summary>
        public static void MakePK_BrigadeDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["BRIGADE_DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["BRIGADE_DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// BRIGADE_DRIVER
        ///</summary>
        public static void MakeIdentities_BrigadeDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["BRIGADE_DRIVER"];
            table.Columns["BRIGADE_DRIVER_ID"].AutoIncrement = true;
            table.Columns["BRIGADE_DRIVER_ID"].AutoIncrementSeed = -1;
            table.Columns["BRIGADE_DRIVER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [BUSSTOP]
        ///</summary>
        public static void MakeTable_Busstop(DataSet dataset)
        {
            if (dataset.Tables.Contains("BUSSTOP") ) return;
            DataTable table = dataset.Tables.Add("BUSSTOP");
            table.Columns.Add("BUSSTOP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SHORTNAME", typeof(System.String)); // nvarchar
            table.Columns.Add("ADDRESS", typeof(System.String)); // nvarchar
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_BUSSTOP
        ///</summary>
        public static void MakePK_Busstop(DataSet dataset)
        {
            DataTable table = dataset.Tables["BUSSTOP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["BUSSTOP_ID"]
            }
            ;
        }
        ///<summary>
        /// BUSSTOP
        ///</summary>
        public static void MakeIdentities_Busstop(DataSet dataset)
        {
            DataTable table = dataset.Tables["BUSSTOP"];
            table.Columns["BUSSTOP_ID"].AutoIncrement = true;
            table.Columns["BUSSTOP_ID"].AutoIncrementSeed = -1;
            table.Columns["BUSSTOP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CAL]
        ///</summary>
        public static void MakeTable_Cal(DataSet dataset)
        {
            if (dataset.Tables.Contains("CAL") ) return;
            DataTable table = dataset.Tables.Add("CAL");
            table.Columns.Add("DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("DAY_KIND", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CAL
        ///</summary>
        public static void MakePK_Cal(DataSet dataset)
        {
            DataTable table = dataset.Tables["CAL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DATE"]
            }
            ;
        }
        ///<summary>
        /// [CALENDAR_DAY]
        ///</summary>
        public static void MakeTable_CalendarDay(DataSet dataset)
        {
            if (dataset.Tables.Contains("CALENDAR_DAY") ) return;
            DataTable table = dataset.Tables.Add("CALENDAR_DAY");
            table.Columns.Add("CALENDAR_DAY_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY", typeof(System.DateTime)); // datetime
            table.Columns.Add("LOG_FROM", typeof(System.Int32)); // int
            table.Columns.Add("LOG_TO", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CALENDAR_DAY
        ///</summary>
        public static void MakePK_CalendarDay(DataSet dataset)
        {
            DataTable table = dataset.Tables["CALENDAR_DAY"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CALENDAR_DAY_ID"]
            }
            ;
        }
        ///<summary>
        /// CALENDAR_DAY
        ///</summary>
        public static void MakeIdentities_CalendarDay(DataSet dataset)
        {
            DataTable table = dataset.Tables["CALENDAR_DAY"];
            table.Columns["CALENDAR_DAY_ID"].AutoIncrement = true;
            table.Columns["CALENDAR_DAY_ID"].AutoIncrementSeed = -1;
            table.Columns["CALENDAR_DAY_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void MakeTable_Constants(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONSTANTS") ) return;
            DataTable table = dataset.Tables.Add("CONSTANTS");
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("VALUE", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONSTANTS_NAME
        ///</summary>
        public static void MakePK_Constants(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONSTANTS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["NAME"]
            }
            ;
        }
        ///<summary>
        /// [CONTRACTOR]
        ///</summary>
        public static void MakeTable_Contractor(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTRACTOR") ) return;
            DataTable table = dataset.Tables.Add("CONTRACTOR");
            table.Columns.Add("CONTRACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("CONTACT_INFO", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTRACTOR
        ///</summary>
        public static void MakePK_Contractor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTRACTOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTRACTOR_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTRACTOR
        ///</summary>
        public static void MakeIdentities_Contractor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTRACTOR"];
            table.Columns["CONTRACTOR_ID"].AutoIncrement = true;
            table.Columns["CONTRACTOR_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTRACTOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTRACTOR_CALENDAR]
        ///</summary>
        public static void MakeTable_ContractorCalendar(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTRACTOR_CALENDAR") ) return;
            DataTable table = dataset.Tables.Add("CONTRACTOR_CALENDAR");
            table.Columns.Add("CONTRACTOR_CALENDAR_ID", typeof(System.Int32)); // int
            table.Columns.Add("GOODS_ACCEPT_TIME_FROM", typeof(System.Int32)); // int
            table.Columns.Add("GOODS_ACCEPT_TIME_TO", typeof(System.Int32)); // int
            table.Columns.Add("PAYMENT_TIME_FROM", typeof(System.Int32)); // int
            table.Columns.Add("PAYMENT_TIME_TO", typeof(System.Int32)); // int
            table.Columns.Add("CONTRACTOR_CALENDAR_DAY_TYPE", typeof(System.Int32)); // int
            table.Columns.Add("DINNER_BREAK_FROM", typeof(System.Int32)); // int
            table.Columns.Add("DINNER_BREAK_TO", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CONTRACTOR_CALENDAR
        ///</summary>
        public static void MakePK_ContractorCalendar(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTRACTOR_CALENDAR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTRACTOR_CALENDAR_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTRACTOR_CALENDAR
        ///</summary>
        public static void MakeIdentities_ContractorCalendar(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTRACTOR_CALENDAR"];
            table.Columns["CONTRACTOR_CALENDAR_ID"].AutoIncrement = true;
            table.Columns["CONTRACTOR_CALENDAR_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTRACTOR_CALENDAR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void MakeTable_Controller(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER");
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PHONE", typeof(System.String)); // varchar
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("FIRMWARE", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_CONTROLLER
        ///</summary>
        public static void MakePK_Controller(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER
        ///</summary>
        public static void MakeIdentities_Controller(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER"];
            table.Columns["CONTROLLER_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_INFO]
        ///</summary>
        public static void MakeTable_ControllerInfo(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_INFO") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_INFO");
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME", typeof(System.DateTime)); // smalldatetime
            table.Columns.Add("DEVICE_ID", typeof(System.Byte[])); // varbinary
            table.Columns.Add("FIRMWARE", typeof(System.Byte[])); // varbinary
            table.Columns.Add("FUEL_LVAL", typeof(System.Int32)); // int
            table.Columns.Add("FUEL_UVAL", typeof(System.Int32)); // int
            table.Columns.Add("FUEL_FACTOR", typeof(System.Single)); // real
            table.Columns.Add("SERVER_IP", typeof(System.String)); // varchar
            table.Columns.Add("SERVER_PORT", typeof(System.Int32)); // int
            table.Columns.Add("TIME_ZONE", typeof(System.Byte)); // tinyint
            table.Columns.Add("VOICE_PHONE", typeof(System.String)); // varchar
            table.Columns.Add("VOICE_PHONE1", typeof(System.String)); // varchar
            table.Columns.Add("VOICE_PHONE2", typeof(System.String)); // varchar
            table.Columns.Add("DATA_PHONE", typeof(System.String)); // varchar
            table.Columns.Add("DATA_PHONE1", typeof(System.String)); // varchar
            table.Columns.Add("INTERVAL", typeof(System.Byte)); // tinyint
            table.Columns.Add("TCP", typeof(System.Boolean)); // bit
            table.Columns.Add("INET_ENTRY_POINT", typeof(System.String)); // varchar
            table.Columns.Add("LOGIN", typeof(System.String)); // varchar
            table.Columns.Add("PASSWORD", typeof(System.String)); // varchar
            table.Columns.Add("FUEL_SENSOR_DIRECTION", typeof(System.Boolean)); // bit
            table.Columns.Add("EXTRA", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_CONTROLLER_INFO
        ///</summary>
        public static void MakePK_ControllerInfo(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_INFO"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_ID"]
            }
            ;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void MakeTable_ControllerSensor(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR");
            table.Columns.Add("CONTROLLER_SENSOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("MAX_VALUE", typeof(System.Int32)); // int
            table.Columns.Add("MIN_VALUE", typeof(System.Int32)); // int
            table.Columns.Add("DEFAULT_VALUE", typeof(System.Int32)); // int
            table.Columns.Add("BITS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR
        ///</summary>
        public static void MakePK_ControllerSensor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR
        ///</summary>
        public static void MakeIdentities_ControllerSensor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR"];
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void MakeTable_ControllerSensorLegend(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_LEGEND");
            table.Columns.Add("CONTROLLER_SENSOR_LEGEND_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_LEGEND
        ///</summary>
        public static void MakePK_ControllerSensorLegend(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_LEGEND_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_LEGEND
        ///</summary>
        public static void MakeIdentities_ControllerSensorLegend(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void MakeTable_ControllerSensorMap(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_MAP");
            table.Columns.Add("CONTROLLER_SENSOR_MAP_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_LEGEND_ID", typeof(System.Int32)); // int
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_MAP
        ///</summary>
        public static void MakePK_ControllerSensorMap(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_MAP_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_MAP
        ///</summary>
        public static void MakeIdentities_ControllerSensorMap(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void MakeTable_ControllerSensorType(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_TYPE");
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakePK_ControllerSensorType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakeIdentities_ControllerSensorType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_STAT]
        ///</summary>
        public static void MakeTable_ControllerStat(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_STAT") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_STAT");
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("V", typeof(System.Int32)); // int
            table.Columns.Add("V1", typeof(System.Int32)); // int
            table.Columns.Add("V2", typeof(System.Int32)); // int
            table.Columns.Add("V3", typeof(System.Int32)); // int
            table.Columns.Add("V4", typeof(System.Int32)); // int
            table.Columns.Add("DS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// [CONTROLLER_TIME]
        ///</summary>
        public static void MakeTable_ControllerTime(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_TIME") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_TIME");
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("TIME_END", typeof(System.DateTime)); // datetime
            table.Columns.Add("CONTROLLER_TIME_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CONTROLLER_TIME
        ///</summary>
        public static void MakePK_ControllerTime(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TIME"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_TIME_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_TIME
        ///</summary>
        public static void MakeIdentities_ControllerTime(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TIME"];
            table.Columns["CONTROLLER_TIME_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_TIME_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_TIME_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void MakeTable_ControllerType(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_TYPE") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_TYPE");
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TYPE_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("PACKET_LENGTH", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CONTROLLER_TYPE
        ///</summary>
        public static void MakePK_ControllerType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_TYPE
        ///</summary>
        public static void MakeIdentities_ControllerType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TYPE"];
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [COUNT_GPRS]
        ///</summary>
        public static void MakeTable_CountGprs(DataSet dataset)
        {
            if (dataset.Tables.Contains("COUNT_GPRS") ) return;
            DataTable table = dataset.Tables.Add("COUNT_GPRS");
            table.Columns.Add("TIME_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("DURATION", typeof(System.Int32)); // int
            table.Columns.Add("BYTES", typeof(System.Int64)); // bigint
        }
        ///<summary>
        /// PK_COUNT_GPRS
        ///</summary>
        public static void MakePK_CountGprs(DataSet dataset)
        {
            DataTable table = dataset.Tables["COUNT_GPRS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TIME_BEGIN"]
                , table.Columns["DURATION"]
            }
            ;
        }
        ///<summary>
        /// [COUNT_GSM]
        ///</summary>
        public static void MakeTable_CountGsm(DataSet dataset)
        {
            if (dataset.Tables.Contains("COUNT_GSM") ) return;
            DataTable table = dataset.Tables.Add("COUNT_GSM");
            table.Columns.Add("TIME_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("DURATION", typeof(System.Int32)); // int
            table.Columns.Add("ID_OPERATOR", typeof(System.Int32)); // int
            table.Columns.Add("ID_VEHICLE", typeof(System.Int32)); // int
        }
        ///<summary>
        /// [COUNT_SMS]
        ///</summary>
        public static void MakeTable_CountSms(DataSet dataset)
        {
            if (dataset.Tables.Contains("COUNT_SMS") ) return;
            DataTable table = dataset.Tables.Add("COUNT_SMS");
            table.Columns.Add("TIME_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("DURATION", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGES", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_COUNT_SMS
        ///</summary>
        public static void MakePK_CountSms(DataSet dataset)
        {
            DataTable table = dataset.Tables["COUNT_SMS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TIME_BEGIN"]
                , table.Columns["DURATION"]
            }
            ;
        }
        ///<summary>
        /// [CUSTOMER]
        ///</summary>
        public static void MakeTable_Customer(DataSet dataset)
        {
            if (dataset.Tables.Contains("CUSTOMER") ) return;
            DataTable table = dataset.Tables.Add("CUSTOMER");
            table.Columns.Add("CUSTOMER_ID", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SHORT_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CUSTOMER
        ///</summary>
        public static void MakePK_Customer(DataSet dataset)
        {
            DataTable table = dataset.Tables["CUSTOMER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CUSTOMER_ID"]
            }
            ;
        }
        ///<summary>
        /// CUSTOMER
        ///</summary>
        public static void MakeIdentities_Customer(DataSet dataset)
        {
            DataTable table = dataset.Tables["CUSTOMER"];
            table.Columns["CUSTOMER_ID"].AutoIncrement = true;
            table.Columns["CUSTOMER_ID"].AutoIncrementSeed = -1;
            table.Columns["CUSTOMER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DAY]
        ///</summary>
        public static void MakeTable_Day(DataSet dataset)
        {
            if (dataset.Tables.Contains("DAY") ) return;
            DataTable table = dataset.Tables.Add("DAY");
            table.Columns.Add("DAY_ID", typeof(System.Int32)); // int
            table.Columns.Add("DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("TEMPERATURE", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DAY
        ///</summary>
        public static void MakePK_Day(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DAY_ID"]
            }
            ;
        }
        ///<summary>
        /// DAY
        ///</summary>
        public static void MakeIdentities_Day(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY"];
            table.Columns["DAY_ID"].AutoIncrement = true;
            table.Columns["DAY_ID"].AutoIncrementSeed = -1;
            table.Columns["DAY_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DAY_KIND]
        ///</summary>
        public static void MakeTable_DayKind(DataSet dataset)
        {
            if (dataset.Tables.Contains("DAY_KIND") ) return;
            DataTable table = dataset.Tables.Add("DAY_KIND");
            table.Columns.Add("DAY_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DK_SET", typeof(System.Int32)); // int
            table.Columns.Add("WEEKDAYS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DAY_KIND
        ///</summary>
        public static void MakePK_DayKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_KIND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DAY_KIND_ID"]
            }
            ;
        }
        ///<summary>
        /// DAY_KIND
        ///</summary>
        public static void MakeIdentities_DayKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_KIND"];
            table.Columns["DAY_KIND_ID"].AutoIncrement = true;
            table.Columns["DAY_KIND_ID"].AutoIncrementSeed = -1;
            table.Columns["DAY_KIND_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DAY_TIME]
        ///</summary>
        public static void MakeTable_DayTime(DataSet dataset)
        {
            if (dataset.Tables.Contains("DAY_TIME") ) return;
            DataTable table = dataset.Tables.Add("DAY_TIME");
            table.Columns.Add("DAY_TIME_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_END", typeof(System.Int32)); // int
            table.Columns.Add("DAY_TYPE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DAY_TIME
        ///</summary>
        public static void MakePK_DayTime(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_TIME"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DAY_TIME_ID"]
            }
            ;
        }
        ///<summary>
        /// DAY_TIME
        ///</summary>
        public static void MakeIdentities_DayTime(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_TIME"];
            table.Columns["DAY_TIME_ID"].AutoIncrement = true;
            table.Columns["DAY_TIME_ID"].AutoIncrementSeed = -1;
            table.Columns["DAY_TIME_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DAY_TYPE]
        ///</summary>
        public static void MakeTable_DayType(DataSet dataset)
        {
            if (dataset.Tables.Contains("DAY_TYPE") ) return;
            DataTable table = dataset.Tables.Add("DAY_TYPE");
            table.Columns.Add("DAY_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("WEEKDAYS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DAY_TYPE
        ///</summary>
        public static void MakePK_DayType(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DAY_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// DAY_TYPE
        ///</summary>
        public static void MakeIdentities_DayType(DataSet dataset)
        {
            DataTable table = dataset.Tables["DAY_TYPE"];
            table.Columns["DAY_TYPE_ID"].AutoIncrement = true;
            table.Columns["DAY_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["DAY_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DECADE]
        ///</summary>
        public static void MakeTable_Decade(DataSet dataset)
        {
            if (dataset.Tables.Contains("DECADE") ) return;
            DataTable table = dataset.Tables.Add("DECADE");
            table.Columns.Add("DECADE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DEPARTMENT", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("END", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_DECADE
        ///</summary>
        public static void MakePK_Decade(DataSet dataset)
        {
            DataTable table = dataset.Tables["DECADE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DECADE_ID"]
            }
            ;
        }
        ///<summary>
        /// DECADE
        ///</summary>
        public static void MakeIdentities_Decade(DataSet dataset)
        {
            DataTable table = dataset.Tables["DECADE"];
            table.Columns["DECADE_ID"].AutoIncrement = true;
            table.Columns["DECADE_ID"].AutoIncrementSeed = -1;
            table.Columns["DECADE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DEPARTMENT]
        ///</summary>
        public static void MakeTable_Department(DataSet dataset)
        {
            if (dataset.Tables.Contains("DEPARTMENT") ) return;
            DataTable table = dataset.Tables.Add("DEPARTMENT");
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_DEPARTMENT
        ///</summary>
        public static void MakePK_Department(DataSet dataset)
        {
            DataTable table = dataset.Tables["DEPARTMENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DEPARTMENT_ID"]
            }
            ;
        }
        ///<summary>
        /// DEPARTMENT
        ///</summary>
        public static void MakeIdentities_Department(DataSet dataset)
        {
            DataTable table = dataset.Tables["DEPARTMENT"];
            table.Columns["DEPARTMENT_ID"].AutoIncrement = true;
            table.Columns["DEPARTMENT_ID"].AutoIncrementSeed = -1;
            table.Columns["DEPARTMENT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DK_SET]
        ///</summary>
        public static void MakeTable_DkSet(DataSet dataset)
        {
            if (dataset.Tables.Contains("DK_SET") ) return;
            DataTable table = dataset.Tables.Add("DK_SET");
            table.Columns.Add("DK_SET_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_DK_SET
        ///</summary>
        public static void MakePK_DkSet(DataSet dataset)
        {
            DataTable table = dataset.Tables["DK_SET"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DK_SET_ID"]
            }
            ;
        }
        ///<summary>
        /// DK_SET
        ///</summary>
        public static void MakeIdentities_DkSet(DataSet dataset)
        {
            DataTable table = dataset.Tables["DK_SET"];
            table.Columns["DK_SET_ID"].AutoIncrement = true;
            table.Columns["DK_SET_ID"].AutoIncrementSeed = -1;
            table.Columns["DK_SET_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DOTNET_TYPE]
        ///</summary>
        public static void MakeTable_DotnetType(DataSet dataset)
        {
            if (dataset.Tables.Contains("DOTNET_TYPE") ) return;
            DataTable table = dataset.Tables.Add("DOTNET_TYPE");
            table.Columns.Add("DOTNET_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TYPE_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MESSAGE_FIELD_TYPE_ID
        ///</summary>
        public static void MakePK_DotnetType(DataSet dataset)
        {
            DataTable table = dataset.Tables["DOTNET_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DOTNET_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// DOTNET_TYPE
        ///</summary>
        public static void MakeIdentities_DotnetType(DataSet dataset)
        {
            DataTable table = dataset.Tables["DOTNET_TYPE"];
            table.Columns["DOTNET_TYPE_ID"].AutoIncrement = true;
            table.Columns["DOTNET_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["DOTNET_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DRIVER]
        ///</summary>
        public static void MakeTable_Driver(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVER") ) return;
            DataTable table = dataset.Tables.Add("DRIVER");
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SHORT_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DATE_OF_BIRTH", typeof(System.DateTime)); // datetime
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("CITY_ONLY", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("FIRED_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("DEPARTMENT", typeof(System.Int32)); // int
            table.Columns.Add("PHONE", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_DRIVER
        ///</summary>
        public static void MakePK_Driver(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// DRIVER
        ///</summary>
        public static void MakeIdentities_Driver(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER"];
            table.Columns["DRIVER_ID"].AutoIncrement = true;
            table.Columns["DRIVER_ID"].AutoIncrementSeed = -1;
            table.Columns["DRIVER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DRIVER_BONUS]
        ///</summary>
        public static void MakeTable_DriverBonus(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVER_BONUS") ) return;
            DataTable table = dataset.Tables.Add("DRIVER_BONUS");
            table.Columns.Add("driver_bonus_id", typeof(System.Int32)); // int
            table.Columns.Add("time_delta", typeof(System.Int32)); // int
            table.Columns.Add("bonus_percentage", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DRIVER_BONUS
        ///</summary>
        public static void MakePK_DriverBonus(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_BONUS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["driver_bonus_id"]
            }
            ;
        }
        ///<summary>
        /// DRIVER_BONUS
        ///</summary>
        public static void MakeIdentities_DriverBonus(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_BONUS"];
            table.Columns["driver_bonus_id"].AutoIncrement = true;
            table.Columns["driver_bonus_id"].AutoIncrementSeed = -1;
            table.Columns["driver_bonus_id"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DRIVER_MSG_TEMPLATE]
        ///</summary>
        public static void MakeTable_DriverMsgTemplate(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVER_MSG_TEMPLATE") ) return;
            DataTable table = dataset.Tables.Add("DRIVER_MSG_TEMPLATE");
            table.Columns.Add("DRIVER_MSG_TEMPLATE_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_DRIVER_MSG_TEMPLATE
        ///</summary>
        public static void MakePK_DriverMsgTemplate(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_MSG_TEMPLATE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVER_MSG_TEMPLATE_ID"]
            }
            ;
        }
        ///<summary>
        /// [DRIVER_STATUS]
        ///</summary>
        public static void MakeTable_DriverStatus(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVER_STATUS") ) return;
            DataTable table = dataset.Tables.Add("DRIVER_STATUS");
            table.Columns.Add("DRIVER_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_STATUS_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_DRIVER_STATUS
        ///</summary>
        public static void MakePK_DriverStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_STATUS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVER_STATUS_ID"]
            }
            ;
        }
        ///<summary>
        /// DRIVER_STATUS
        ///</summary>
        public static void MakeIdentities_DriverStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_STATUS"];
            table.Columns["DRIVER_STATUS_ID"].AutoIncrement = true;
            table.Columns["DRIVER_STATUS_ID"].AutoIncrementSeed = -1;
            table.Columns["DRIVER_STATUS_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DRIVER_VEHICLE]
        ///</summary>
        public static void MakeTable_DriverVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVER_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("DRIVER_VEHICLE");
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_NUMBER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_DRIVER_VEHICLE
        ///</summary>
        public static void MakePK_DriverVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVER_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// [DRIVERGROUP]
        ///</summary>
        public static void MakeTable_Drivergroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVERGROUP") ) return;
            DataTable table = dataset.Tables.Add("DRIVERGROUP");
            table.Columns.Add("DRIVERGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_T_DRIVERGROUP
        ///</summary>
        public static void MakePK_Drivergroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVERGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVERGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// DRIVERGROUP
        ///</summary>
        public static void MakeIdentities_Drivergroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVERGROUP"];
            table.Columns["DRIVERGROUP_ID"].AutoIncrement = true;
            table.Columns["DRIVERGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["DRIVERGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DRIVERGROUP_DRIVER]
        ///</summary>
        public static void MakeTable_DrivergroupDriver(DataSet dataset)
        {
            if (dataset.Tables.Contains("DRIVERGROUP_DRIVER") ) return;
            DataTable table = dataset.Tables.Add("DRIVERGROUP_DRIVER");
            table.Columns.Add("DRIVERGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_T_DRIVERGROUP_DRIVER
        ///</summary>
        public static void MakePK_DrivergroupDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["DRIVERGROUP_DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVERGROUP_ID"]
                , table.Columns["DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// [EMAIL]
        ///</summary>
        public static void MakeTable_Email(DataSet dataset)
        {
            if (dataset.Tables.Contains("EMAIL") ) return;
            DataTable table = dataset.Tables.Add("EMAIL");
            table.Columns.Add("EMAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("EMAIL", typeof(System.String)); // nvarchar
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_EMAIL
        ///</summary>
        public static void MakePK_Email(DataSet dataset)
        {
            DataTable table = dataset.Tables["EMAIL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["EMAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// EMAIL
        ///</summary>
        public static void MakeIdentities_Email(DataSet dataset)
        {
            DataTable table = dataset.Tables["EMAIL"];
            table.Columns["EMAIL_ID"].AutoIncrement = true;
            table.Columns["EMAIL_ID"].AutoIncrementSeed = -1;
            table.Columns["EMAIL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [EMAIL_SCHEDULEREVENT]
        ///</summary>
        public static void MakeTable_EmailSchedulerevent(DataSet dataset)
        {
            if (dataset.Tables.Contains("EMAIL_SCHEDULEREVENT") ) return;
            DataTable table = dataset.Tables.Add("EMAIL_SCHEDULEREVENT");
            table.Columns.Add("SCHEDULEREVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("EMAIL_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_EMAIL_SCHEDULEREVENT
        ///</summary>
        public static void MakePK_EmailSchedulerevent(DataSet dataset)
        {
            DataTable table = dataset.Tables["EMAIL_SCHEDULEREVENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULEREVENT_ID"]
                , table.Columns["EMAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// [EMAIL_SCHEDULERQUEUE]
        ///</summary>
        public static void MakeTable_EmailSchedulerqueue(DataSet dataset)
        {
            if (dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE") ) return;
            DataTable table = dataset.Tables.Add("EMAIL_SCHEDULERQUEUE");
            table.Columns.Add("SCHEDULERQUEUE_ID", typeof(System.Int32)); // int
            table.Columns.Add("EMAIL_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_EMAIL_SCHEDULERQUEUE
        ///</summary>
        public static void MakePK_EmailSchedulerqueue(DataSet dataset)
        {
            DataTable table = dataset.Tables["EMAIL_SCHEDULERQUEUE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULERQUEUE_ID"]
                , table.Columns["EMAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// [FACTOR_VALUES]
        ///</summary>
        public static void MakeTable_FactorValues(DataSet dataset)
        {
            if (dataset.Tables.Contains("FACTOR_VALUES") ) return;
            DataTable table = dataset.Tables.Add("FACTOR_VALUES");
            table.Columns.Add("FACTOR_VALUES_ID", typeof(System.Int32)); // int
            table.Columns.Add("FACTOR", typeof(System.Single)); // real
            table.Columns.Add("RUNTIME", typeof(System.Int32)); // int
            table.Columns.Add("SEASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY_TIME_ID", typeof(System.Int32)); // int
            table.Columns.Add("FACTOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_FACTOR_VALUES
        ///</summary>
        public static void MakePK_FactorValues(DataSet dataset)
        {
            DataTable table = dataset.Tables["FACTOR_VALUES"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["FACTOR_VALUES_ID"]
            }
            ;
        }
        ///<summary>
        /// FACTOR_VALUES
        ///</summary>
        public static void MakeIdentities_FactorValues(DataSet dataset)
        {
            DataTable table = dataset.Tables["FACTOR_VALUES"];
            table.Columns["FACTOR_VALUES_ID"].AutoIncrement = true;
            table.Columns["FACTOR_VALUES_ID"].AutoIncrementSeed = -1;
            table.Columns["FACTOR_VALUES_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [FACTORS]
        ///</summary>
        public static void MakeTable_Factors(DataSet dataset)
        {
            if (dataset.Tables.Contains("FACTORS") ) return;
            DataTable table = dataset.Tables.Add("FACTORS");
            table.Columns.Add("FACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("COMMENTS", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_FACTORS
        ///</summary>
        public static void MakePK_Factors(DataSet dataset)
        {
            DataTable table = dataset.Tables["FACTORS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["FACTOR_ID"]
            }
            ;
        }
        ///<summary>
        /// FACTORS
        ///</summary>
        public static void MakeIdentities_Factors(DataSet dataset)
        {
            DataTable table = dataset.Tables["FACTORS"];
            table.Columns["FACTOR_ID"].AutoIncrement = true;
            table.Columns["FACTOR_ID"].AutoIncrementSeed = -1;
            table.Columns["FACTOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_SEGMENT]
        ///</summary>
        public static void MakeTable_GeoSegment(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_SEGMENT") ) return;
            DataTable table = dataset.Tables.Add("GEO_SEGMENT");
            table.Columns.Add("GEO_SEGMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEO_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("LENGTH", typeof(System.Single)); // real
            table.Columns.Add("RUNTIME", typeof(System.Int32)); // int
            table.Columns.Add("STOP_TIME", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
            table.Columns.Add("POINT", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX", typeof(System.String)); // varchar
            table.Columns.Add("FACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("POINTTO", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GEO_SEGMENT
        ///</summary>
        public static void MakePK_GeoSegment(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GEO_SEGMENT_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_SEGMENT
        ///</summary>
        public static void MakeIdentities_GeoSegment(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT"];
            table.Columns["GEO_SEGMENT_ID"].AutoIncrement = true;
            table.Columns["GEO_SEGMENT_ID"].AutoIncrementSeed = -1;
            table.Columns["GEO_SEGMENT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_SEGMENT_RUNTIME]
        ///</summary>
        public static void MakeTable_GeoSegmentRuntime(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_SEGMENT_RUNTIME") ) return;
            DataTable table = dataset.Tables.Add("GEO_SEGMENT_RUNTIME");
            table.Columns.Add("GEO_SEGMENT_RUNTIME_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEO_SEGMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY_TIME_ID", typeof(System.Int32)); // int
            table.Columns.Add("RUNTIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GEO_SEGMENT_RUNTIME
        ///</summary>
        public static void MakePK_GeoSegmentRuntime(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT_RUNTIME"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GEO_SEGMENT_RUNTIME_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_SEGMENT_RUNTIME
        ///</summary>
        public static void MakeIdentities_GeoSegmentRuntime(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT_RUNTIME"];
            table.Columns["GEO_SEGMENT_RUNTIME_ID"].AutoIncrement = true;
            table.Columns["GEO_SEGMENT_RUNTIME_ID"].AutoIncrementSeed = -1;
            table.Columns["GEO_SEGMENT_RUNTIME_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_SEGMENT_VERTEX]
        ///</summary>
        public static void MakeTable_GeoSegmentVertex(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_SEGMENT_VERTEX") ) return;
            DataTable table = dataset.Tables.Add("GEO_SEGMENT_VERTEX");
            table.Columns.Add("GEO_SEGMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_GS", typeof(System.Int32)); // int
            table.Columns.Add("DIRECT", typeof(System.Int32)); // int
            table.Columns.Add("GEO_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_GT", typeof(System.Int32)); // int
            table.Columns.Add("GEO_SEGMENT_VERTEX_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GEO_SEGMENT_VERTEX
        ///</summary>
        public static void MakePK_GeoSegmentVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT_VERTEX"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GEO_SEGMENT_VERTEX_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_SEGMENT_VERTEX
        ///</summary>
        public static void MakeIdentities_GeoSegmentVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_SEGMENT_VERTEX"];
            table.Columns["GEO_SEGMENT_VERTEX_ID"].AutoIncrement = true;
            table.Columns["GEO_SEGMENT_VERTEX_ID"].AutoIncrementSeed = -1;
            table.Columns["GEO_SEGMENT_VERTEX_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_TRIP]
        ///</summary>
        public static void MakeTable_GeoTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_TRIP") ) return;
            DataTable table = dataset.Tables.Add("GEO_TRIP");
            table.Columns.Add("GEO_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("LENGTH", typeof(System.Single)); // real
            table.Columns.Add("POINTA", typeof(System.Int32)); // int
            table.Columns.Add("POINTB", typeof(System.Int32)); // int
            table.Columns.Add("COMMENTS", typeof(System.String)); // nvarchar
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RUNTIME", typeof(System.Int32)); // int
            table.Columns.Add("CREATOR_OPERATOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GEO_TRIP
        ///</summary>
        public static void MakePK_GeoTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GEO_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_TRIP
        ///</summary>
        public static void MakeIdentities_GeoTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_TRIP"];
            table.Columns["GEO_TRIP_ID"].AutoIncrement = true;
            table.Columns["GEO_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["GEO_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_ZONE]
        ///</summary>
        public static void MakeTable_GeoZone(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_ZONE") ) return;
            DataTable table = dataset.Tables.Add("GEO_ZONE");
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COLOR", typeof(System.Int32)); // int
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK__GEO_ZONE__6E8472CD
        ///</summary>
        public static void MakePK_GeoZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_ZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONE_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_ZONE
        ///</summary>
        public static void MakeIdentities_GeoZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_ZONE"];
            table.Columns["ZONE_ID"].AutoIncrement = true;
            table.Columns["ZONE_ID"].AutoIncrementSeed = -1;
            table.Columns["ZONE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GEO_ZONE_PRIMITIVE]
        ///</summary>
        public static void MakeTable_GeoZonePrimitive(DataSet dataset)
        {
            if (dataset.Tables.Contains("GEO_ZONE_PRIMITIVE") ) return;
            DataTable table = dataset.Tables.Add("GEO_ZONE_PRIMITIVE");
            table.Columns.Add("GEO_ZONE_PRIMITIVE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PRIMITIVE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__GEO_ZONE_PRIMITI__76259495
        ///</summary>
        public static void MakePK_GeoZonePrimitive(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_ZONE_PRIMITIVE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GEO_ZONE_PRIMITIVE_ID"]
            }
            ;
        }
        ///<summary>
        /// GEO_ZONE_PRIMITIVE
        ///</summary>
        public static void MakeIdentities_GeoZonePrimitive(DataSet dataset)
        {
            DataTable table = dataset.Tables["GEO_ZONE_PRIMITIVE"];
            table.Columns["GEO_ZONE_PRIMITIVE_ID"].AutoIncrement = true;
            table.Columns["GEO_ZONE_PRIMITIVE_ID"].AutoIncrementSeed = -1;
            table.Columns["GEO_ZONE_PRIMITIVE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GOODS]
        ///</summary>
        public static void MakeTable_Goods(DataSet dataset)
        {
            if (dataset.Tables.Contains("GOODS") ) return;
            DataTable table = dataset.Tables.Add("GOODS");
            table.Columns.Add("GOODS_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("PROPERTIES", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("GOODS_TYPE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GOODS
        ///</summary>
        public static void MakePK_Goods(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GOODS_ID"]
            }
            ;
        }
        ///<summary>
        /// GOODS
        ///</summary>
        public static void MakeIdentities_Goods(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS"];
            table.Columns["GOODS_ID"].AutoIncrement = true;
            table.Columns["GOODS_ID"].AutoIncrementSeed = -1;
            table.Columns["GOODS_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GOODS_LOGISTIC_ORDER]
        ///</summary>
        public static void MakeTable_GoodsLogisticOrder(DataSet dataset)
        {
            if (dataset.Tables.Contains("GOODS_LOGISTIC_ORDER") ) return;
            DataTable table = dataset.Tables.Add("GOODS_LOGISTIC_ORDER");
            table.Columns.Add("GOODS_LOGISTIC_ORDER_ID", typeof(System.Int32)); // int
            table.Columns.Add("LOGISTIC_ORDER_ID", typeof(System.Int32)); // int
            table.Columns.Add("GOODS_ID", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Double)); // float
        }
        ///<summary>
        /// PK_GOODS_LOGISTIC_ORDER
        ///</summary>
        public static void MakePK_GoodsLogisticOrder(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS_LOGISTIC_ORDER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GOODS_LOGISTIC_ORDER_ID"]
            }
            ;
        }
        ///<summary>
        /// GOODS_LOGISTIC_ORDER
        ///</summary>
        public static void MakeIdentities_GoodsLogisticOrder(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS_LOGISTIC_ORDER"];
            table.Columns["GOODS_LOGISTIC_ORDER_ID"].AutoIncrement = true;
            table.Columns["GOODS_LOGISTIC_ORDER_ID"].AutoIncrementSeed = -1;
            table.Columns["GOODS_LOGISTIC_ORDER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GOODS_TYPE]
        ///</summary>
        public static void MakeTable_GoodsType(DataSet dataset)
        {
            if (dataset.Tables.Contains("GOODS_TYPE") ) return;
            DataTable table = dataset.Tables.Add("GOODS_TYPE");
            table.Columns.Add("GOODS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TYPE_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("WEIGHT", typeof(System.Double)); // float
            table.Columns.Add("VOLUME", typeof(System.Double)); // float
            table.Columns.Add("MEASUREMENT_UNIT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_GOODS_TYPE
        ///</summary>
        public static void MakePK_GoodsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GOODS_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// GOODS_TYPE
        ///</summary>
        public static void MakeIdentities_GoodsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS_TYPE"];
            table.Columns["GOODS_TYPE_ID"].AutoIncrement = true;
            table.Columns["GOODS_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["GOODS_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GOODS_TYPE_VEHICLE]
        ///</summary>
        public static void MakeTable_GoodsTypeVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("GOODS_TYPE_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("GOODS_TYPE_VEHICLE");
            table.Columns.Add("GOODS_TYPE_VEHICLE_id", typeof(System.Int32)); // int
            table.Columns.Add("GOODS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CAPACITY", typeof(System.Double)); // float
        }
        ///<summary>
        /// PK_GOODS_TYPE_VEHICLE
        ///</summary>
        public static void MakePK_GoodsTypeVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["GOODS_TYPE_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GOODS_TYPE_VEHICLE_id"]
            }
            ;
        }
        ///<summary>
        /// [GRAPHIC]
        ///</summary>
        public static void MakeTable_Graphic(DataSet dataset)
        {
            if (dataset.Tables.Contains("GRAPHIC") ) return;
            DataTable table = dataset.Tables.Add("GRAPHIC");
            table.Columns.Add("GRAPHIC_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("DAY_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("SHIFT_COUNT", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GRAPHIC
        ///</summary>
        public static void MakePK_Graphic(DataSet dataset)
        {
            DataTable table = dataset.Tables["GRAPHIC"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GRAPHIC_ID"]
            }
            ;
        }
        ///<summary>
        /// GRAPHIC
        ///</summary>
        public static void MakeIdentities_Graphic(DataSet dataset)
        {
            DataTable table = dataset.Tables["GRAPHIC"];
            table.Columns["GRAPHIC_ID"].AutoIncrement = true;
            table.Columns["GRAPHIC_ID"].AutoIncrementSeed = -1;
            table.Columns["GRAPHIC_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GRAPHIC_SHIFT]
        ///</summary>
        public static void MakeTable_GraphicShift(DataSet dataset)
        {
            if (dataset.Tables.Contains("GRAPHIC_SHIFT") ) return;
            DataTable table = dataset.Tables.Add("GRAPHIC_SHIFT");
            table.Columns.Add("ID", typeof(System.Int32)); // int
            table.Columns.Add("GRAPHIC", typeof(System.Int32)); // int
            table.Columns.Add("DAY_NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("SHIFT", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_GRAPHIC_SHIFT
        ///</summary>
        public static void MakePK_GraphicShift(DataSet dataset)
        {
            DataTable table = dataset.Tables["GRAPHIC_SHIFT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ID"]
            }
            ;
        }
        ///<summary>
        /// GRAPHIC_SHIFT
        ///</summary>
        public static void MakeIdentities_GraphicShift(DataSet dataset)
        {
            DataTable table = dataset.Tables["GRAPHIC_SHIFT"];
            table.Columns["ID"].AutoIncrement = true;
            table.Columns["ID"].AutoIncrementSeed = -1;
            table.Columns["ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [GUARD_EVENTS]
        ///</summary>
        public static void MakeTable_GuardEvents(DataSet dataset)
        {
            if (dataset.Tables.Contains("GUARD_EVENTS") ) return;
            DataTable table = dataset.Tables.Add("GUARD_EVENTS");
            table.Columns.Add("GUARD_EVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_GUARD_EVENTS
        ///</summary>
        public static void MakePK_GuardEvents(DataSet dataset)
        {
            DataTable table = dataset.Tables["GUARD_EVENTS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["GUARD_EVENT_ID"]
            }
            ;
        }
        ///<summary>
        /// [Info_size_fileData]
        ///</summary>
        public static void MakeTable_InfoSizeFiledata(DataSet dataset)
        {
            if (dataset.Tables.Contains("Info_size_fileData") ) return;
            DataTable table = dataset.Tables.Add("Info_size_fileData");
            table.Columns.Add("Logic_FileName", typeof(System.String)); // nvarchar
            table.Columns.Add("FileName", typeof(System.String)); // nvarchar
            table.Columns.Add("Size(Kb)", typeof(System.String)); // nvarchar
            table.Columns.Add("data", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// [Job_DelBadXY]
        ///</summary>
        public static void MakeTable_JobDelbadxy(DataSet dataset)
        {
            if (dataset.Tables.Contains("Job_DelBadXY") ) return;
            DataTable table = dataset.Tables.Add("Job_DelBadXY");
            table.Columns.Add("Job_DelBadXY_ID", typeof(System.Int32)); // int
            table.Columns.Add("dtFrom", typeof(System.DateTime)); // datetime
            table.Columns.Add("dtTo", typeof(System.DateTime)); // datetime
            table.Columns.Add("NextInHour", typeof(System.Int32)); // int
            table.Columns.Add("time_allow", typeof(System.Int32)); // int
            table.Columns.Add("MaxSpeed", typeof(System.Int32)); // int
            table.Columns.Add("Del", typeof(System.Boolean)); // bit
            table.Columns.Add("lastDelCount", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__Job_DelBadXY__43300074
        ///</summary>
        public static void MakePK_JobDelbadxy(DataSet dataset)
        {
            DataTable table = dataset.Tables["Job_DelBadXY"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["Job_DelBadXY_ID"]
            }
            ;
        }
        ///<summary>
        /// Job_DelBadXY
        ///</summary>
        public static void MakeIdentities_JobDelbadxy(DataSet dataset)
        {
            DataTable table = dataset.Tables["Job_DelBadXY"];
            table.Columns["Job_DelBadXY_ID"].AutoIncrement = true;
            table.Columns["Job_DelBadXY_ID"].AutoIncrementSeed = -1;
            table.Columns["Job_DelBadXY_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [JOURNAL]
        ///</summary>
        public static void MakeTable_Journal(DataSet dataset)
        {
            if (dataset.Tables.Contains("JOURNAL") ) return;
            DataTable table = dataset.Tables.Add("JOURNAL");
            table.Columns.Add("JOURNAL_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("END", typeof(System.DateTime)); // datetime
            table.Columns.Add("DRIVER", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("WAYOUT", typeof(System.Int32)); // int
            table.Columns.Add("REPLACED_DRIVER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_JOURNAL
        ///</summary>
        public static void MakePK_Journal(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["JOURNAL_ID"]
            }
            ;
        }
        ///<summary>
        /// JOURNAL
        ///</summary>
        public static void MakeIdentities_Journal(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL"];
            table.Columns["JOURNAL_ID"].AutoIncrement = true;
            table.Columns["JOURNAL_ID"].AutoIncrementSeed = -1;
            table.Columns["JOURNAL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [JOURNAL_DAY]
        ///</summary>
        public static void MakeTable_JournalDay(DataSet dataset)
        {
            if (dataset.Tables.Contains("JOURNAL_DAY") ) return;
            DataTable table = dataset.Tables.Add("JOURNAL_DAY");
            table.Columns.Add("JOURNAL_DAY_ID", typeof(System.Int32)); // int
            table.Columns.Add("BRIGADE_DRIVER", typeof(System.Int32)); // int
            table.Columns.Add("DATE_DAY", typeof(System.DateTime)); // datetime
            table.Columns.Add("VEHICLE", typeof(System.Int32)); // int
            table.Columns.Add("WAYOUT", typeof(System.Int32)); // int
            table.Columns.Add("SHIFT", typeof(System.Int32)); // int
            table.Columns.Add("TIME_BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_END", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_JOURNAL_DAY
        ///</summary>
        public static void MakePK_JournalDay(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_DAY"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["JOURNAL_DAY_ID"]
            }
            ;
        }
        ///<summary>
        /// JOURNAL_DAY
        ///</summary>
        public static void MakeIdentities_JournalDay(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_DAY"];
            table.Columns["JOURNAL_DAY_ID"].AutoIncrement = true;
            table.Columns["JOURNAL_DAY_ID"].AutoIncrementSeed = -1;
            table.Columns["JOURNAL_DAY_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [JOURNAL_DRIVER]
        ///</summary>
        public static void MakeTable_JournalDriver(DataSet dataset)
        {
            if (dataset.Tables.Contains("JOURNAL_DRIVER") ) return;
            DataTable table = dataset.Tables.Add("JOURNAL_DRIVER");
            table.Columns.Add("JOURNAL_DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("END", typeof(System.DateTime)); // datetime
            table.Columns.Add("DRIVER", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_JOURNAL_DRIVER
        ///</summary>
        public static void MakePK_JournalDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["JOURNAL_DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// JOURNAL_DRIVER
        ///</summary>
        public static void MakeIdentities_JournalDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_DRIVER"];
            table.Columns["JOURNAL_DRIVER_ID"].AutoIncrement = true;
            table.Columns["JOURNAL_DRIVER_ID"].AutoIncrementSeed = -1;
            table.Columns["JOURNAL_DRIVER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [JOURNAL_VEHICLE]
        ///</summary>
        public static void MakeTable_JournalVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("JOURNAL_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("JOURNAL_VEHICLE");
            table.Columns.Add("JOURNAL_VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("END", typeof(System.DateTime)); // datetime
            table.Columns.Add("VEHICLE", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_JOURNAL_VEHICLE
        ///</summary>
        public static void MakePK_JournalVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["JOURNAL_VEHICLE_ID"]
            }
            ;
        }
        ///<summary>
        /// JOURNAL_VEHICLE
        ///</summary>
        public static void MakeIdentities_JournalVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["JOURNAL_VEHICLE"];
            table.Columns["JOURNAL_VEHICLE_ID"].AutoIncrement = true;
            table.Columns["JOURNAL_VEHICLE_ID"].AutoIncrementSeed = -1;
            table.Columns["JOURNAL_VEHICLE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [kolomna_points]
        ///</summary>
        public static void MakeTable_KolomnaPoints(DataSet dataset)
        {
            if (dataset.Tables.Contains("kolomna_points") ) return;
            DataTable table = dataset.Tables.Add("kolomna_points");
            table.Columns.Add("Col001", typeof(System.String)); // nvarchar
            table.Columns.Add("Col002", typeof(System.String)); // nvarchar
            table.Columns.Add("Col003", typeof(System.String)); // nvarchar
            table.Columns.Add("Col004", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [kolomna_points2]
        ///</summary>
        public static void MakeTable_KolomnaPoints2(DataSet dataset)
        {
            if (dataset.Tables.Contains("kolomna_points2") ) return;
            DataTable table = dataset.Tables.Add("kolomna_points2");
            table.Columns.Add("Col001", typeof(System.String)); // nvarchar
            table.Columns.Add("Col002", typeof(System.String)); // nvarchar
            table.Columns.Add("Col003", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [kolomna_points3]
        ///</summary>
        public static void MakeTable_KolomnaPoints3(DataSet dataset)
        {
            if (dataset.Tables.Contains("kolomna_points3") ) return;
            DataTable table = dataset.Tables.Add("kolomna_points3");
            table.Columns.Add("Col001", typeof(System.String)); // nvarchar
            table.Columns.Add("Col002", typeof(System.String)); // nvarchar
            table.Columns.Add("Col003", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [LOG_GUARD_EVENTS]
        ///</summary>
        public static void MakeTable_LogGuardEvents(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOG_GUARD_EVENTS") ) return;
            DataTable table = dataset.Tables.Add("LOG_GUARD_EVENTS");
            table.Columns.Add("MONITOREE_ID", typeof(System.Int32)); // int
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("GUARD_EVENT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// [LOG_OPS_EVENTS]
        ///</summary>
        public static void MakeTable_LogOpsEvents(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOG_OPS_EVENTS") ) return;
            DataTable table = dataset.Tables.Add("LOG_OPS_EVENTS");
            table.Columns.Add("RECORD_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPS_EVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("EVENT_TIME", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_LOG_OPS_EVENTS
        ///</summary>
        public static void MakePK_LogOpsEvents(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOG_OPS_EVENTS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RECORD_ID"]
            }
            ;
        }
        ///<summary>
        /// LOG_OPS_EVENTS
        ///</summary>
        public static void MakeIdentities_LogOpsEvents(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOG_OPS_EVENTS"];
            table.Columns["RECORD_ID"].AutoIncrement = true;
            table.Columns["RECORD_ID"].AutoIncrementSeed = -1;
            table.Columns["RECORD_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [LOG_VEHICLE_STATUS]
        ///</summary>
        public static void MakeTable_LogVehicleStatus(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOG_VEHICLE_STATUS") ) return;
            DataTable table = dataset.Tables.Add("LOG_VEHICLE_STATUS");
            table.Columns.Add("LOG_VEHICLE_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("EVENTTIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("ENDTIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_LOG_VEHICLE_STATUS
        ///</summary>
        public static void MakePK_LogVehicleStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOG_VEHICLE_STATUS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_VEHICLE_STATUS_ID"]
            }
            ;
        }
        ///<summary>
        /// [LOG_WAYBILL_HEADER_STATUS]
        ///</summary>
        public static void MakeTable_LogWaybillHeaderStatus(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS") ) return;
            DataTable table = dataset.Tables.Add("LOG_WAYBILL_HEADER_STATUS");
            table.Columns.Add("LOG_WAYBILL_HEADER_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILL_HEADER_ID", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILL_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("EVENT_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("SCHEDULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_LOG_WAYBILL_HEADER_STATUS
        ///</summary>
        public static void MakePK_LogWaybillHeaderStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_WAYBILL_HEADER_STATUS_ID"]
            }
            ;
        }
        ///<summary>
        /// LOG_WAYBILL_HEADER_STATUS
        ///</summary>
        public static void MakeIdentities_LogWaybillHeaderStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"];
            table.Columns["LOG_WAYBILL_HEADER_STATUS_ID"].AutoIncrement = true;
            table.Columns["LOG_WAYBILL_HEADER_STATUS_ID"].AutoIncrementSeed = -1;
            table.Columns["LOG_WAYBILL_HEADER_STATUS_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [LOGISTIC_ADDRESS]
        ///</summary>
        public static void MakeTable_LogisticAddress(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOGISTIC_ADDRESS") ) return;
            DataTable table = dataset.Tables.Add("LOGISTIC_ADDRESS");
            table.Columns.Add("LOGISTIC_ADDRESS_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTRACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ADDRESS", typeof(System.String)); // nvarchar
            table.Columns.Add("GEOZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTRACTOR_CALENDAR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_LOGISTIC_ADDRESS
        ///</summary>
        public static void MakePK_LogisticAddress(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOGISTIC_ADDRESS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOGISTIC_ADDRESS_ID"]
            }
            ;
        }
        ///<summary>
        /// LOGISTIC_ADDRESS
        ///</summary>
        public static void MakeIdentities_LogisticAddress(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOGISTIC_ADDRESS"];
            table.Columns["LOGISTIC_ADDRESS_ID"].AutoIncrement = true;
            table.Columns["LOGISTIC_ADDRESS_ID"].AutoIncrementSeed = -1;
            table.Columns["LOGISTIC_ADDRESS_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [LOGISTIC_ORDER]
        ///</summary>
        public static void MakeTable_LogisticOrder(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOGISTIC_ORDER") ) return;
            DataTable table = dataset.Tables.Add("LOGISTIC_ORDER");
            table.Columns.Add("LOGISTIC_ORDER_ID", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("CREATE_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("LOGISTIC_ADDRESS_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTRACTOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_TIME_FROM", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_TIME_TO", typeof(System.Int32)); // int
            table.Columns.Add("BLADING_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PLANNING_SHIPMENT_TIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_LOGISTIC_ORDER
        ///</summary>
        public static void MakePK_LogisticOrder(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOGISTIC_ORDER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOGISTIC_ORDER_ID"]
            }
            ;
        }
        ///<summary>
        /// LOGISTIC_ORDER
        ///</summary>
        public static void MakeIdentities_LogisticOrder(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOGISTIC_ORDER"];
            table.Columns["LOGISTIC_ORDER_ID"].AutoIncrement = true;
            table.Columns["LOGISTIC_ORDER_ID"].AutoIncrementSeed = -1;
            table.Columns["LOGISTIC_ORDER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MAP_VERTEX]
        ///</summary>
        public static void MakeTable_MapVertex(DataSet dataset)
        {
            if (dataset.Tables.Contains("MAP_VERTEX") ) return;
            DataTable table = dataset.Tables.Add("MAP_VERTEX");
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("MAP_ID", typeof(System.Int32)); // int
            table.Columns.Add("X", typeof(System.Double)); // float
            table.Columns.Add("Y", typeof(System.Double)); // float
            table.Columns.Add("VISIBLE", typeof(System.Boolean)); // bit
            table.Columns.Add("ENABLE", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK__MAP_VERTEX__57EB2638
        ///</summary>
        public static void MakePK_MapVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["MAP_VERTEX"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VERTEX_ID"]
            }
            ;
        }
        ///<summary>
        /// MAP_VERTEX
        ///</summary>
        public static void MakeIdentities_MapVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["MAP_VERTEX"];
            table.Columns["VERTEX_ID"].AutoIncrement = true;
            table.Columns["VERTEX_ID"].AutoIncrementSeed = -1;
            table.Columns["VERTEX_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MAPS]
        ///</summary>
        public static void MakeTable_Maps(DataSet dataset)
        {
            if (dataset.Tables.Contains("MAPS") ) return;
            DataTable table = dataset.Tables.Add("MAPS");
            table.Columns.Add("MAP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("FILE_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("VERSION", typeof(System.String)); // nvarchar
            table.Columns.Add("CHECK_SUM", typeof(System.Int32)); // int
            table.Columns.Add("GUID", typeof(System.Guid)); // uniqueidentifier
        }
        ///<summary>
        /// PK__MAPS__5602DDC6
        ///</summary>
        public static void MakePK_Maps(DataSet dataset)
        {
            DataTable table = dataset.Tables["MAPS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MAP_ID"]
            }
            ;
        }
        ///<summary>
        /// MAPS
        ///</summary>
        public static void MakeIdentities_Maps(DataSet dataset)
        {
            DataTable table = dataset.Tables["MAPS"];
            table.Columns["MAP_ID"].AutoIncrement = true;
            table.Columns["MAP_ID"].AutoIncrementSeed = -1;
            table.Columns["MAP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void MakeTable_Media(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA") ) return;
            DataTable table = dataset.Tables.Add("MEDIA");
            table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("TYPE", typeof(System.Byte)); // tinyint
        }
        ///<summary>
        /// PK_MEDIA
        ///</summary>
        public static void MakePK_Media(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MEDIA_ID"]
            }
            ;
        }
        ///<summary>
        /// MEDIA
        ///</summary>
        public static void MakeIdentities_Media(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA"];
            table.Columns["MEDIA_ID"].AutoIncrement = true;
            table.Columns["MEDIA_ID"].AutoIncrementSeed = -1;
            table.Columns["MEDIA_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void MakeTable_MediaAcceptors(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS") ) return;
            DataTable table = dataset.Tables.Add("MEDIA_ACCEPTORS");
            table.Columns.Add("MA_ID", typeof(System.Int32)); // int
            table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
            table.Columns.Add("INITIALIZATION", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MEDIA_ACCEPTORS
        ///</summary>
        public static void MakePK_MediaAcceptors(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MA_ID"]
            }
            ;
        }
        ///<summary>
        /// MEDIA_ACCEPTORS
        ///</summary>
        public static void MakeIdentities_MediaAcceptors(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
            table.Columns["MA_ID"].AutoIncrement = true;
            table.Columns["MA_ID"].AutoIncrementSeed = -1;
            table.Columns["MA_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void MakeTable_MediaType(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA_TYPE") ) return;
            DataTable table = dataset.Tables.Add("MEDIA_TYPE");
            table.Columns.Add("ID", typeof(System.Byte)); // tinyint
            table.Columns.Add("Name", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MediaType
        ///</summary>
        public static void MakePK_MediaType(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ID"]
            }
            ;
        }
        ///<summary>
        /// [MESSAGE]
        ///</summary>
        public static void MakeTable_Message(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE");
            table.Columns.Add("MESSAGE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TEMPLATE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SOURCE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SEVERITY_ID", typeof(System.Int32)); // int
            table.Columns.Add("POSITION_ID", typeof(System.Int32)); // int
            table.Columns.Add("SUBJECT", typeof(System.String)); // nvarchar
            table.Columns.Add("BODY", typeof(System.String)); // nvarchar
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("DESTINATION_ID", typeof(System.Int32)); // int
            table.Columns.Add("TYPE", typeof(System.Int32)); // int
            table.Columns.Add("GROUP", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_MESSAGE
        ///</summary>
        public static void MakePK_Message(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MESSAGE_ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE
        ///</summary>
        public static void MakeIdentities_Message(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE"];
            table.Columns["MESSAGE_ID"].AutoIncrement = true;
            table.Columns["MESSAGE_ID"].AutoIncrementSeed = -1;
            table.Columns["MESSAGE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MESSAGE_BOARD]
        ///</summary>
        public static void MakeTable_MessageBoard(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE_BOARD") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE_BOARD");
            table.Columns.Add("ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // varchar
            table.Columns.Add("DESC", typeof(System.String)); // varchar
            table.Columns.Add("NUM", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_MESSAGE_BOARD
        ///</summary>
        public static void MakePK_MessageBoard(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_BOARD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE_BOARD
        ///</summary>
        public static void MakeIdentities_MessageBoard(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_BOARD"];
            table.Columns["ID"].AutoIncrement = true;
            table.Columns["ID"].AutoIncrementSeed = -1;
            table.Columns["ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MESSAGE_FIELD]
        ///</summary>
        public static void MakeTable_MessageField(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE_FIELD") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE_FIELD");
            table.Columns.Add("MESSAGE_FIELD_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE_TEMPLATE_FIELD_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MESSAGE_FIELD
        ///</summary>
        public static void MakePK_MessageField(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_FIELD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MESSAGE_FIELD_ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE_FIELD
        ///</summary>
        public static void MakeIdentities_MessageField(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_FIELD"];
            table.Columns["MESSAGE_FIELD_ID"].AutoIncrement = true;
            table.Columns["MESSAGE_FIELD_ID"].AutoIncrementSeed = -1;
            table.Columns["MESSAGE_FIELD_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MESSAGE_OPERATOR]
        ///</summary>
        public static void MakeTable_MessageOperator(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE_OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE_OPERATOR");
            table.Columns.Add("MESSAGE_OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE_STATUS_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_MESSAGE_OPERATOR
        ///</summary>
        public static void MakePK_MessageOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MESSAGE_OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE_OPERATOR
        ///</summary>
        public static void MakeIdentities_MessageOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_OPERATOR"];
            table.Columns["MESSAGE_OPERATOR_ID"].AutoIncrement = true;
            table.Columns["MESSAGE_OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["MESSAGE_OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE]
        ///</summary>
        public static void MakeTable_MessageTemplate(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE_TEMPLATE");
            table.Columns.Add("MESSAGE_TEMPLATE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("HEADER", typeof(System.String)); // nvarchar
            table.Columns.Add("BODY", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MESSAGETEMPLATE
        ///</summary>
        public static void MakePK_MessageTemplate(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_TEMPLATE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MESSAGE_TEMPLATE_ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE_TEMPLATE
        ///</summary>
        public static void MakeIdentities_MessageTemplate(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_TEMPLATE"];
            table.Columns["MESSAGE_TEMPLATE_ID"].AutoIncrement = true;
            table.Columns["MESSAGE_TEMPLATE_ID"].AutoIncrementSeed = -1;
            table.Columns["MESSAGE_TEMPLATE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE_FIELD]
        ///</summary>
        public static void MakeTable_MessageTemplateField(DataSet dataset)
        {
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD") ) return;
            DataTable table = dataset.Tables.Add("MESSAGE_TEMPLATE_FIELD");
            table.Columns.Add("MESSAGE_TEMPLATE_FIELD_ID", typeof(System.Int32)); // int
            table.Columns.Add("MESSAGE_TEMPLATE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DOTNET_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MESSAGEFIELD
        ///</summary>
        public static void MakePK_MessageTemplateField(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_TEMPLATE_FIELD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MESSAGE_TEMPLATE_FIELD_ID"]
            }
            ;
        }
        ///<summary>
        /// MESSAGE_TEMPLATE_FIELD
        ///</summary>
        public static void MakeIdentities_MessageTemplateField(DataSet dataset)
        {
            DataTable table = dataset.Tables["MESSAGE_TEMPLATE_FIELD"];
            table.Columns["MESSAGE_TEMPLATE_FIELD_ID"].AutoIncrement = true;
            table.Columns["MESSAGE_TEMPLATE_FIELD_ID"].AutoIncrementSeed = -1;
            table.Columns["MESSAGE_TEMPLATE_FIELD_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MONITOREE_LOG]
        ///</summary>
        public static void MakeTable_MonitoreeLog(DataSet dataset)
        {
            if (dataset.Tables.Contains("MONITOREE_LOG") ) return;
            DataTable table = dataset.Tables.Add("MONITOREE_LOG");
            table.Columns.Add("MONITOREE_ID", typeof(System.Int32)); // int
            table.Columns.Add("LOG_TIME", typeof(System.Int32)); // int
            table.Columns.Add("X", typeof(System.Single)); // real
            table.Columns.Add("Y", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Byte)); // tinyint
            table.Columns.Add("MEDIA", typeof(System.Byte)); // tinyint
            table.Columns.Add("V", typeof(System.Byte)); // tinyint
            table.Columns.Add("V1", typeof(System.Byte)); // tinyint
            table.Columns.Add("V2", typeof(System.Byte)); // tinyint
            table.Columns.Add("V3", typeof(System.Byte)); // tinyint
            table.Columns.Add("V4", typeof(System.Byte)); // tinyint
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("FIRMWARE", typeof(System.Int16)); // smallint
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void MakeTable_Operator(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("LOGIN", typeof(System.String)); // nvarchar
            table.Columns.Add("PASSWORD", typeof(System.String)); // nvarchar
            //table.Columns.Add("PHONE", typeof(System.String)); // nvarchar
            table.Columns.Add("EMAIL", typeof(System.String)); // nvarchar
            table.Columns.Add("GUID", typeof(System.Guid)); // uniqueidentifier
        }
        ///<summary>
        /// PK_OPERATOR
        ///</summary>
        public static void MakePK_Operator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATOR
        ///</summary>
        public static void MakeIdentities_Operator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR"];
            table.Columns["OPERATOR_ID"].AutoIncrement = true;
            table.Columns["OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATOR_DEPARTMENT]
        ///</summary>
        public static void MakeTable_OperatorDepartment(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_DEPARTMENT") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_DEPARTMENT");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_DEPARTMENT
        ///</summary>
        public static void MakePK_OperatorDepartment(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_DEPARTMENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["DEPARTMENT_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_DRIVER]
        ///</summary>
        public static void MakeTable_OperatorDriver(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_DRIVER") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_DRIVER");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_DRIVER
        ///</summary>
        public static void MakePK_OperatorDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["DRIVER_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_DRIVERGROUP]
        ///</summary>
        public static void MakeTable_OperatorDrivergroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_DRIVERGROUP");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVERGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_DRIVERGROUP
        ///</summary>
        public static void MakePK_OperatorDrivergroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_DRIVERGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["DRIVERGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_PROFILE]
        ///</summary>
        public static void MakeTable_OperatorProfile(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_PROFILE") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_PROFILE");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("PROFILE", typeof(System.Byte[])); // image
            table.Columns.Add("OPERATOR_PROFILE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATOR_PROFILE
        ///</summary>
        public static void MakePK_OperatorProfile(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_PROFILE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_PROFILE_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATOR_PROFILE
        ///</summary>
        public static void MakeIdentities_OperatorProfile(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_PROFILE"];
            table.Columns["OPERATOR_PROFILE_ID"].AutoIncrement = true;
            table.Columns["OPERATOR_PROFILE_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATOR_PROFILE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATOR_REPORT]
        ///</summary>
        public static void MakeTable_OperatorReport(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_REPORT") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_REPORT");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("REPORT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_REPORT
        ///</summary>
        public static void MakePK_OperatorReport(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_REPORT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["REPORT_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_ROUTE]
        ///</summary>
        public static void MakeTable_OperatorRoute(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_ROUTE") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_ROUTE");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_ROUTE
        ///</summary>
        public static void MakePK_OperatorRoute(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_ROUTE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["ROUTE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_ROUTEGROUP]
        ///</summary>
        public static void MakeTable_OperatorRoutegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_ROUTEGROUP");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_ROUTEGROUP
        ///</summary>
        public static void MakePK_OperatorRoutegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_ROUTEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["ROUTEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_VEHICLE]
        ///</summary>
        public static void MakeTable_OperatorVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_VEHICLE");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATOR_VEHICLE
        ///</summary>
        public static void MakePK_OperatorVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["VEHICLE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_VEHICLEGROUP]
        ///</summary>
        public static void MakeTable_OperatorVehiclegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_VEHICLEGROUP");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATOR_VEHICLEGROUP
        ///</summary>
        public static void MakePK_OperatorVehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_VEHICLEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["VEHICLEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_ZONE]
        ///</summary>
        public static void MakeTable_OperatorZone(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_ZONE") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_ZONE");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_ZONE
        ///</summary>
        public static void MakePK_OperatorZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_ZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["ZONE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATOR_ZONEGROUP]
        ///</summary>
        public static void MakeTable_OperatorZonegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR_ZONEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR_ZONEGROUP");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATOR_ZONEGROUP
        ///</summary>
        public static void MakePK_OperatorZonegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR_ZONEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
                , table.Columns["ZONEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void MakeTable_Operatorgroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_OPERATORGROUP
        ///</summary>
        public static void MakePK_Operatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATORGROUP
        ///</summary>
        public static void MakeIdentities_Operatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP"];
            table.Columns["OPERATORGROUP_ID"].AutoIncrement = true;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATORGROUP_DEPARTMENT]
        ///</summary>
        public static void MakeTable_OperatorgroupDepartment(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_DEPARTMENT");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_DEPARTMENT
        ///</summary>
        public static void MakePK_OperatorgroupDepartment(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_DEPARTMENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["DEPARTMENT_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVER]
        ///</summary>
        public static void MakeTable_OperatorgroupDriver(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVER") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_DRIVER");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_DRIVER
        ///</summary>
        public static void MakePK_OperatorgroupDriver(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_DRIVER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["DRIVER_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVERGROUP]
        ///</summary>
        public static void MakeTable_OperatorgroupDrivergroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_DRIVERGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVERGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_DRIVERGROUP
        ///</summary>
        public static void MakePK_OperatorgroupDrivergroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_DRIVERGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["DRIVERGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void MakeTable_OperatorgroupOperator(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_OPERATOR");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATORGROUP_OPERATOR
        ///</summary>
        public static void MakePK_OperatorgroupOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_REPORT]
        ///</summary>
        public static void MakeTable_OperatorgroupReport(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_REPORT") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_REPORT");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("REPORT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_REPORT
        ///</summary>
        public static void MakePK_OperatorgroupReport(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_REPORT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["REPORT_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTE]
        ///</summary>
        public static void MakeTable_OperatorgroupRoute(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTE") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_ROUTE");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_ROUTE
        ///</summary>
        public static void MakePK_OperatorgroupRoute(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_ROUTE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["ROUTE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTEGROUP]
        ///</summary>
        public static void MakeTable_OperatorgroupRoutegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_ROUTEGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_ROUTEGROUP
        ///</summary>
        public static void MakePK_OperatorgroupRoutegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_ROUTEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["ROUTEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATORGROUP_ROUTEGROUP
        ///</summary>
        public static void MakeIdentities_OperatorgroupRoutegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_ROUTEGROUP"];
            table.Columns["OPERATORGROUP_ID"].AutoIncrement = true;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLE]
        ///</summary>
        public static void MakeTable_OperatorgroupVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_VEHICLE");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATORGROUP_VEHICLE
        ///</summary>
        public static void MakePK_OperatorgroupVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["VEHICLE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLEGROUP]
        ///</summary>
        public static void MakeTable_OperatorgroupVehiclegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_VEHICLEGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATORGROUP_VEHICLEGROUP
        ///</summary>
        public static void MakePK_OperatorgroupVehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["VEHICLEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_ZONE]
        ///</summary>
        public static void MakeTable_OperatorgroupZone(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_ZONE") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_ZONE");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_ZONE
        ///</summary>
        public static void MakePK_OperatorgroupZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_ZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["ZONE_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPERATORGROUP_ZONEGROUP]
        ///</summary>
        public static void MakeTable_OperatorgroupZonegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_ZONEGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_OPERATORGROUP_ZONEGROUP
        ///</summary>
        public static void MakePK_OperatorgroupZonegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_ZONEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
                , table.Columns["ZONEGROUP_ID"]
                , table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [OPS_EVENT]
        ///</summary>
        public static void MakeTable_OpsEvent(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPS_EVENT") ) return;
            DataTable table = dataset.Tables.Add("OPS_EVENT");
            table.Columns.Add("OPS_EVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPS_EVENT_NAME", typeof(System.String)); // char
        }
        ///<summary>
        /// PK_OPS_EVENT
        ///</summary>
        public static void MakePK_OpsEvent(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPS_EVENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPS_EVENT_ID"]
            }
            ;
        }
        ///<summary>
        /// OPS_EVENT
        ///</summary>
        public static void MakeIdentities_OpsEvent(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPS_EVENT"];
            table.Columns["OPS_EVENT_ID"].AutoIncrement = true;
            table.Columns["OPS_EVENT_ID"].AutoIncrementSeed = -1;
            table.Columns["OPS_EVENT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ORDER]
        ///</summary>
        public static void MakeTable_Order(DataSet dataset)
        {
            if (dataset.Tables.Contains("ORDER") ) return;
            DataTable table = dataset.Tables.Add("ORDER");
            table.Columns.Add("ORDER_ID", typeof(System.Int32)); // int
            table.Columns.Add("DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("DEPARTMENT", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_TYPE", typeof(System.Int32)); // int
            table.Columns.Add("CUSTOMER", typeof(System.Int32)); // int
            table.Columns.Add("RECIPIENT", typeof(System.String)); // nvarchar
            table.Columns.Add("RESPONDENT", typeof(System.String)); // nvarchar
            table.Columns.Add("TIME_BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_END", typeof(System.Int32)); // int
            table.Columns.Add("ORIGIN", typeof(System.String)); // nvarchar
            table.Columns.Add("DESTINATION", typeof(System.String)); // nvarchar
            table.Columns.Add("LENGTH", typeof(System.Double)); // float
        }
        ///<summary>
        /// PK_ORDER
        ///</summary>
        public static void MakePK_Order(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ORDER_ID"]
            }
            ;
        }
        ///<summary>
        /// ORDER
        ///</summary>
        public static void MakeIdentities_Order(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER"];
            table.Columns["ORDER_ID"].AutoIncrement = true;
            table.Columns["ORDER_ID"].AutoIncrementSeed = -1;
            table.Columns["ORDER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ORDER_TRIP]
        ///</summary>
        public static void MakeTable_OrderTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("ORDER_TRIP") ) return;
            DataTable table = dataset.Tables.Add("ORDER_TRIP");
            table.Columns.Add("ORDER_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
            table.Columns.Add("JOURNAL_DAY", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ORDER_TRIP
        ///</summary>
        public static void MakePK_OrderTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ORDER_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// ORDER_TRIP
        ///</summary>
        public static void MakeIdentities_OrderTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER_TRIP"];
            table.Columns["ORDER_TRIP_ID"].AutoIncrement = true;
            table.Columns["ORDER_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["ORDER_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ORDER_TYPE]
        ///</summary>
        public static void MakeTable_OrderType(DataSet dataset)
        {
            if (dataset.Tables.Contains("ORDER_TYPE") ) return;
            DataTable table = dataset.Tables.Add("ORDER_TYPE");
            table.Columns.Add("ORDER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_ORDER_TYPE
        ///</summary>
        public static void MakePK_OrderType(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ORDER_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// ORDER_TYPE
        ///</summary>
        public static void MakeIdentities_OrderType(DataSet dataset)
        {
            DataTable table = dataset.Tables["ORDER_TYPE"];
            table.Columns["ORDER_TYPE_ID"].AutoIncrement = true;
            table.Columns["ORDER_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["ORDER_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OWNER]
        ///</summary>
        public static void MakeTable_Owner(DataSet dataset)
        {
            if (dataset.Tables.Contains("OWNER") ) return;
            DataTable table = dataset.Tables.Add("OWNER");
            table.Columns.Add("OWNER_ID", typeof(System.Int32)); // int
            table.Columns.Add("FIRST_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("LAST_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SECOND_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("PHONE_NUMBER1", typeof(System.String)); // nvarchar
            table.Columns.Add("PHONE_NUMBER2", typeof(System.String)); // nvarchar
            table.Columns.Add("PHONE_NUMBER3", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_OWNER
        ///</summary>
        public static void MakePK_Owner(DataSet dataset)
        {
            DataTable table = dataset.Tables["OWNER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OWNER_ID"]
            }
            ;
        }
        ///<summary>
        /// OWNER
        ///</summary>
        public static void MakeIdentities_Owner(DataSet dataset)
        {
            DataTable table = dataset.Tables["OWNER"];
            table.Columns["OWNER_ID"].AutoIncrement = true;
            table.Columns["OWNER_ID"].AutoIncrementSeed = -1;
            table.Columns["OWNER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [PARAMETER]
        ///</summary>
        public static void MakeTable_Parameter(DataSet dataset)
        {
            if (dataset.Tables.Contains("PARAMETER") ) return;
            DataTable table = dataset.Tables.Add("PARAMETER");
            table.Columns.Add("ID", typeof(System.String)); // nchar
            table.Columns.Add("VALUE", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [PERIOD]
        ///</summary>
        public static void MakeTable_Period(DataSet dataset)
        {
            if (dataset.Tables.Contains("PERIOD") ) return;
            DataTable table = dataset.Tables.Add("PERIOD");
            table.Columns.Add("PERIOD_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("ROUTE", typeof(System.Int32)); // int
            table.Columns.Add("DAY_KIND", typeof(System.Int32)); // int
            table.Columns.Add("DK_SET", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("END", typeof(System.DateTime)); // datetime
            table.Columns.Add("COLOR", typeof(System.Int32)); // int
            table.Columns.Add("HOLIDAY", typeof(System.Boolean)); // bit
            table.Columns.Add("SEASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_PERIOD
        ///</summary>
        public static void MakePK_Period(DataSet dataset)
        {
            DataTable table = dataset.Tables["PERIOD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["PERIOD_ID"]
            }
            ;
        }
        ///<summary>
        /// PERIOD
        ///</summary>
        public static void MakeIdentities_Period(DataSet dataset)
        {
            DataTable table = dataset.Tables["PERIOD"];
            table.Columns["PERIOD_ID"].AutoIncrement = true;
            table.Columns["PERIOD_ID"].AutoIncrementSeed = -1;
            table.Columns["PERIOD_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [POINT]
        ///</summary>
        public static void MakeTable_Point(DataSet dataset)
        {
            if (dataset.Tables.Contains("POINT") ) return;
            DataTable table = dataset.Tables.Add("POINT");
            table.Columns.Add("POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("POINT_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("X", typeof(System.Double)); // float
            table.Columns.Add("Y", typeof(System.Double)); // float
            table.Columns.Add("RADIUS", typeof(System.Decimal)); // numeric
            table.Columns.Add("ADDRESS", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("BUSSTOP_ID", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_POINT
        ///</summary>
        public static void MakePK_Point(DataSet dataset)
        {
            DataTable table = dataset.Tables["POINT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["POINT_ID"]
            }
            ;
        }
        ///<summary>
        /// POINT
        ///</summary>
        public static void MakeIdentities_Point(DataSet dataset)
        {
            DataTable table = dataset.Tables["POINT"];
            table.Columns["POINT_ID"].AutoIncrement = true;
            table.Columns["POINT_ID"].AutoIncrementSeed = -1;
            table.Columns["POINT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [POINT_KIND]
        ///</summary>
        public static void MakeTable_PointKind(DataSet dataset)
        {
            if (dataset.Tables.Contains("POINT_KIND") ) return;
            DataTable table = dataset.Tables.Add("POINT_KIND");
            table.Columns.Add("POINT_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_POINT_KIND
        ///</summary>
        public static void MakePK_PointKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["POINT_KIND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["POINT_KIND_ID"]
            }
            ;
        }
        ///<summary>
        /// [REPORT]
        ///</summary>
        public static void MakeTable_Report(DataSet dataset)
        {
            if (dataset.Tables.Contains("REPORT") ) return;
            DataTable table = dataset.Tables.Add("REPORT");
            table.Columns.Add("REPORT_ID", typeof(System.Int32)); // int
            table.Columns.Add("REPORT_GUID", typeof(System.Guid)); // uniqueidentifier
            table.Columns.Add("REPORT_FILENAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK__REPORT__0062608F
        ///</summary>
        public static void MakePK_Report(DataSet dataset)
        {
            DataTable table = dataset.Tables["REPORT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["REPORT_ID"]
            }
            ;
        }
        ///<summary>
        /// REPORT
        ///</summary>
        public static void MakeIdentities_Report(DataSet dataset)
        {
            DataTable table = dataset.Tables["REPORT"];
            table.Columns["REPORT_ID"].AutoIncrement = true;
            table.Columns["REPORT_ID"].AutoIncrementSeed = -1;
            table.Columns["REPORT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [REPRINT_REASON]
        ///</summary>
        public static void MakeTable_ReprintReason(DataSet dataset)
        {
            if (dataset.Tables.Contains("REPRINT_REASON") ) return;
            DataTable table = dataset.Tables.Add("REPRINT_REASON");
            table.Columns.Add("REPRINT_REASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("REPRINT_REASON_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_REPRINT_REASON
        ///</summary>
        public static void MakePK_ReprintReason(DataSet dataset)
        {
            DataTable table = dataset.Tables["REPRINT_REASON"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["REPRINT_REASON_ID"]
            }
            ;
        }
        ///<summary>
        /// REPRINT_REASON
        ///</summary>
        public static void MakeIdentities_ReprintReason(DataSet dataset)
        {
            DataTable table = dataset.Tables["REPRINT_REASON"];
            table.Columns["REPRINT_REASON_ID"].AutoIncrement = true;
            table.Columns["REPRINT_REASON_ID"].AutoIncrementSeed = -1;
            table.Columns["REPRINT_REASON_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RESERV]
        ///</summary>
        public static void MakeTable_Reserv(DataSet dataset)
        {
            if (dataset.Tables.Contains("RESERV") ) return;
            DataTable table = dataset.Tables.Add("RESERV");
            table.Columns.Add("RESERV_ID", typeof(System.Int32)); // int
            table.Columns.Add("RESERV_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("DRIVER", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("END_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("RELEASED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void MakeTable_Right(DataSet dataset)
        {
            if (dataset.Tables.Contains("RIGHT") ) return;
            DataTable table = dataset.Tables.Add("RIGHT");
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("SYSTEM", typeof(System.Boolean)); // bit
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // ntext
            table.Columns.Add("HELP_URL", typeof(System.String)); // nvarchar
            table.Columns.Add("URL", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RIGHT
        ///</summary>
        public static void MakePK_Right(DataSet dataset)
        {
            DataTable table = dataset.Tables["RIGHT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// [RIGHT_OPERATOR]
        ///</summary>
        public static void MakeTable_RightOperator(DataSet dataset)
        {
            if (dataset.Tables.Contains("RIGHT_OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("RIGHT_OPERATOR");
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_RIGHT_OPERATOR
        ///</summary>
        public static void MakePK_RightOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["RIGHT_OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RIGHT_ID"]
                , table.Columns["OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// [RIGHT_OPERATORGROUP]
        ///</summary>
        public static void MakeTable_RightOperatorgroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("RIGHT_OPERATORGROUP") ) return;
            DataTable table = dataset.Tables.Add("RIGHT_OPERATORGROUP");
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOWED", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_RIGHT_OPERATORGROUP
        ///</summary>
        public static void MakePK_RightOperatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["RIGHT_OPERATORGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RIGHT_ID"]
                , table.Columns["OPERATORGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// [ROUTE]
        ///</summary>
        public static void MakeTable_Route(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTE") ) return;
            DataTable table = dataset.Tables.Add("ROUTE");
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("LENGTH", typeof(System.Single)); // real
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("COLOR", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX", typeof(System.String)); // varchar
            table.Columns.Add("PREFIX", typeof(System.String)); // nvarchar
            table.Columns.Add("DIGIT_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("TIME_BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_END", typeof(System.Int32)); // int
            table.Columns.Add("TIME_DEVIATION", typeof(System.Int32)); // int
            table.Columns.Add("DEVIATION", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ROUTE
        ///</summary>
        public static void MakePK_Route(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTE_ID"]
            }
            ;
        }
        ///<summary>
        /// ROUTE
        ///</summary>
        public static void MakeIdentities_Route(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE"];
            table.Columns["ROUTE_ID"].AutoIncrement = true;
            table.Columns["ROUTE_ID"].AutoIncrementSeed = -1;
            table.Columns["ROUTE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ROUTE_GEOZONE]
        ///</summary>
        public static void MakeTable_RouteGeozone(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTE_GEOZONE") ) return;
            DataTable table = dataset.Tables.Add("ROUTE_GEOZONE");
            table.Columns.Add("ROUTE_GEOZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEOZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ROUTE_GEOZONE
        ///</summary>
        public static void MakePK_RouteGeozone(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_GEOZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTE_GEOZONE_ID"]
            }
            ;
        }
        ///<summary>
        /// ROUTE_GEOZONE
        ///</summary>
        public static void MakeIdentities_RouteGeozone(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_GEOZONE"];
            table.Columns["ROUTE_GEOZONE_ID"].AutoIncrement = true;
            table.Columns["ROUTE_GEOZONE_ID"].AutoIncrementSeed = -1;
            table.Columns["ROUTE_GEOZONE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ROUTE_POINT]
        ///</summary>
        public static void MakeTable_RoutePoint(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTE_POINT") ) return;
            DataTable table = dataset.Tables.Add("ROUTE_POINT");
            table.Columns.Add("ROUTE_POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORD", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ROUTE_POINT
        ///</summary>
        public static void MakePK_RoutePoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_POINT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTE_POINT_ID"]
            }
            ;
        }
        ///<summary>
        /// ROUTE_POINT
        ///</summary>
        public static void MakeIdentities_RoutePoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_POINT"];
            table.Columns["ROUTE_POINT_ID"].AutoIncrement = true;
            table.Columns["ROUTE_POINT_ID"].AutoIncrementSeed = -1;
            table.Columns["ROUTE_POINT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ROUTE_TRIP]
        ///</summary>
        public static void MakeTable_RouteTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTE_TRIP") ) return;
            DataTable table = dataset.Tables.Add("ROUTE_TRIP");
            table.Columns.Add("ROUTE_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEO_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ROUTE_TRIP
        ///</summary>
        public static void MakePK_RouteTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTE_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// ROUTE_TRIP
        ///</summary>
        public static void MakeIdentities_RouteTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTE_TRIP"];
            table.Columns["ROUTE_TRIP_ID"].AutoIncrement = true;
            table.Columns["ROUTE_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["ROUTE_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ROUTEGROUP]
        ///</summary>
        public static void MakeTable_Routegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTEGROUP") ) return;
            DataTable table = dataset.Tables.Add("ROUTEGROUP");
            table.Columns.Add("ROUTEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_ROUTEGROUP
        ///</summary>
        public static void MakePK_Routegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTEGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// ROUTEGROUP
        ///</summary>
        public static void MakeIdentities_Routegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTEGROUP"];
            table.Columns["ROUTEGROUP_ID"].AutoIncrement = true;
            table.Columns["ROUTEGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["ROUTEGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ROUTEGROUP_ROUTE]
        ///</summary>
        public static void MakeTable_RoutegroupRoute(DataSet dataset)
        {
            if (dataset.Tables.Contains("ROUTEGROUP_ROUTE") ) return;
            DataTable table = dataset.Tables.Add("ROUTEGROUP_ROUTE");
            table.Columns.Add("ROUTEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ROUTEGROUP_ROUTE
        ///</summary>
        public static void MakePK_RoutegroupRoute(DataSet dataset)
        {
            DataTable table = dataset.Tables["ROUTEGROUP_ROUTE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ROUTEGROUP_ID"]
                , table.Columns["ROUTE_ID"]
            }
            ;
        }
        ///<summary>
        /// [RS]
        ///</summary>
        public static void MakeTable_Rs(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS") ) return;
            DataTable table = dataset.Tables.Add("RS");
            table.Columns.Add("RS_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS
        ///</summary>
        public static void MakePK_Rs(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_ID"]
            }
            ;
        }
        ///<summary>
        /// RS
        ///</summary>
        public static void MakeIdentities_Rs(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS"];
            table.Columns["RS_ID"].AutoIncrement = true;
            table.Columns["RS_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_NUMBER]
        ///</summary>
        public static void MakeTable_RsNumber(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_NUMBER") ) return;
            DataTable table = dataset.Tables.Add("RS_NUMBER");
            table.Columns.Add("RS_NUMBER_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_STEP", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("INTERVAL", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_NUMBER
        ///</summary>
        public static void MakePK_RsNumber(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_NUMBER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_NUMBER_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_NUMBER
        ///</summary>
        public static void MakeIdentities_RsNumber(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_NUMBER"];
            table.Columns["RS_NUMBER_ID"].AutoIncrement = true;
            table.Columns["RS_NUMBER_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_NUMBER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_OPERATIONSTYPE]
        ///</summary>
        public static void MakeTable_RsOperationstype(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_OPERATIONSTYPE") ) return;
            DataTable table = dataset.Tables.Add("RS_OPERATIONSTYPE");
            table.Columns.Add("RS_OPERATIONSTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALIAS", typeof(System.String)); // char
            table.Columns.Add("LETTER", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_OPERATIONSTYPE
        ///</summary>
        public static void MakePK_RsOperationstype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_OPERATIONSTYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_OPERATIONSTYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_OPERATIONSTYPE
        ///</summary>
        public static void MakeIdentities_RsOperationstype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_OPERATIONSTYPE"];
            table.Columns["RS_OPERATIONSTYPE_ID"].AutoIncrement = true;
            table.Columns["RS_OPERATIONSTYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_OPERATIONSTYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_OPERATIONTYPE]
        ///</summary>
        public static void MakeTable_RsOperationtype(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") ) return;
            DataTable table = dataset.Tables.Add("RS_OPERATIONTYPE");
            table.Columns.Add("RS_OPERATIONTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_OPERATIONSTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT_0", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN_0", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN", typeof(System.Int32)); // int
            table.Columns.Add("CODE", typeof(System.Int32)); // int
            table.Columns.Add("OPERATION_TIME", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_OPERATIONTYPE
        ///</summary>
        public static void MakePK_RsOperationtype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_OPERATIONTYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_OPERATIONTYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_OPERATIONTYPE
        ///</summary>
        public static void MakeIdentities_RsOperationtype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_OPERATIONTYPE"];
            table.Columns["RS_OPERATIONTYPE_ID"].AutoIncrement = true;
            table.Columns["RS_OPERATIONTYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_OPERATIONTYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_PERIOD]
        ///</summary>
        public static void MakeTable_RsPeriod(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_PERIOD") ) return;
            DataTable table = dataset.Tables.Add("RS_PERIOD");
            table.Columns.Add("PERIOD_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("END", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_PERIOD
        ///</summary>
        public static void MakePK_RsPeriod(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_PERIOD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["PERIOD_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_PERIOD
        ///</summary>
        public static void MakeIdentities_RsPeriod(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_PERIOD"];
            table.Columns["PERIOD_ID"].AutoIncrement = true;
            table.Columns["PERIOD_ID"].AutoIncrementSeed = -1;
            table.Columns["PERIOD_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_POINT]
        ///</summary>
        public static void MakeTable_RsPoint(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_POINT") ) return;
            DataTable table = dataset.Tables.Add("RS_POINT");
            table.Columns.Add("RS_POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_ROUND_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_WAYOUT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_TRIPTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_OPERATIONTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_SHIFT_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_IN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_OUT", typeof(System.Int32)); // int
            table.Columns.Add("STOP_TIME", typeof(System.Int32)); // int
            table.Columns.Add("OPERATION_TIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_POINT
        ///</summary>
        public static void MakePK_RsPoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_POINT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_POINT_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_POINT
        ///</summary>
        public static void MakeIdentities_RsPoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_POINT"];
            table.Columns["RS_POINT_ID"].AutoIncrement = true;
            table.Columns["RS_POINT_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_POINT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_ROUND_TRIP]
        ///</summary>
        public static void MakeTable_RsRoundTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_ROUND_TRIP") ) return;
            DataTable table = dataset.Tables.Add("RS_ROUND_TRIP");
            table.Columns.Add("RS_ROUND_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("INDEX", typeof(System.Int32)); // int
            table.Columns.Add("A_B", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_RS_TRIP
        ///</summary>
        public static void MakePK_RsRoundTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_ROUND_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_ROUND_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_ROUND_TRIP
        ///</summary>
        public static void MakeIdentities_RsRoundTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_ROUND_TRIP"];
            table.Columns["RS_ROUND_TRIP_ID"].AutoIncrement = true;
            table.Columns["RS_ROUND_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_ROUND_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_RUNTIME]
        ///</summary>
        public static void MakeTable_RsRuntime(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_RUNTIME") ) return;
            DataTable table = dataset.Tables.Add("RS_RUNTIME");
            table.Columns.Add("RS_RUNTIME_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("PERIOD_ID", typeof(System.Int32)); // int
            table.Columns.Add("RUNTIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_RUNTIME
        ///</summary>
        public static void MakePK_RsRuntime(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_RUNTIME"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_RUNTIME_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_RUNTIME
        ///</summary>
        public static void MakeIdentities_RsRuntime(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_RUNTIME"];
            table.Columns["RS_RUNTIME_ID"].AutoIncrement = true;
            table.Columns["RS_RUNTIME_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_RUNTIME_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_SHIFT]
        ///</summary>
        public static void MakeTable_RsShift(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_SHIFT") ) return;
            DataTable table = dataset.Tables.Add("RS_SHIFT");
            table.Columns.Add("RS_SHIFT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_WAYOUT_ID", typeof(System.Int32)); // int
            table.Columns.Add("SHIFT", typeof(System.Int32)); // int
            table.Columns.Add("PREPARE_TIME", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_SHIFT", typeof(System.Int32)); // int
            table.Columns.Add("CLOSE_TIME", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT_0", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN_0", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_SHIFT
        ///</summary>
        public static void MakePK_RsShift(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_SHIFT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_SHIFT_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_SHIFT
        ///</summary>
        public static void MakeIdentities_RsShift(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_SHIFT"];
            table.Columns["RS_SHIFT_ID"].AutoIncrement = true;
            table.Columns["RS_SHIFT_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_SHIFT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_STEP]
        ///</summary>
        public static void MakeTable_RsStep(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_STEP") ) return;
            DataTable table = dataset.Tables.Add("RS_STEP");
            table.Columns.Add("RS_STEP_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("END", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_STEP
        ///</summary>
        public static void MakePK_RsStep(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_STEP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_STEP_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_STEP
        ///</summary>
        public static void MakeIdentities_RsStep(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_STEP"];
            table.Columns["RS_STEP_ID"].AutoIncrement = true;
            table.Columns["RS_STEP_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_STEP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_TRIP]
        ///</summary>
        public static void MakeTable_RsTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_TRIP") ) return;
            DataTable table = dataset.Tables.Add("RS_TRIP");
            table.Columns.Add("RS_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Byte)); // tinyint
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_TRIP_1
        ///</summary>
        public static void MakePK_RsTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_TRIP
        ///</summary>
        public static void MakeIdentities_RsTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIP"];
            table.Columns["RS_TRIP_ID"].AutoIncrement = true;
            table.Columns["RS_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_TRIPSTYPE]
        ///</summary>
        public static void MakeTable_RsTripstype(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_TRIPSTYPE") ) return;
            DataTable table = dataset.Tables.Add("RS_TRIPSTYPE");
            table.Columns.Add("RS_TRIPSTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALIAS", typeof(System.String)); // char
            table.Columns.Add("LETTER", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_TRIPSTYPE
        ///</summary>
        public static void MakePK_RsTripstype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIPSTYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_TRIPSTYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_TRIPSTYPE
        ///</summary>
        public static void MakeIdentities_RsTripstype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIPSTYPE"];
            table.Columns["RS_TRIPSTYPE_ID"].AutoIncrement = true;
            table.Columns["RS_TRIPSTYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_TRIPSTYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_TRIPTYPE]
        ///</summary>
        public static void MakeTable_RsTriptype(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_TRIPTYPE") ) return;
            DataTable table = dataset.Tables.Add("RS_TRIPTYPE");
            table.Columns.Add("RS_TRIPTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_TRIPSTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_A", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_B", typeof(System.Int32)); // int
            table.Columns.Add("CODE", typeof(System.Int32)); // int
            table.Columns.Add("STOP_TIME", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_TRIPTYPE
        ///</summary>
        public static void MakePK_RsTriptype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIPTYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_TRIPTYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_TRIPTYPE
        ///</summary>
        public static void MakeIdentities_RsTriptype(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TRIPTYPE"];
            table.Columns["RS_TRIPTYPE_ID"].AutoIncrement = true;
            table.Columns["RS_TRIPTYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_TRIPTYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_TYPE]
        ///</summary>
        public static void MakeTable_RsType(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_TYPE") ) return;
            DataTable table = dataset.Tables.Add("RS_TYPE");
            table.Columns.Add("RS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RS_TYPE
        ///</summary>
        public static void MakePK_RsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_TYPE
        ///</summary>
        public static void MakeIdentities_RsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_TYPE"];
            table.Columns["RS_TYPE_ID"].AutoIncrement = true;
            table.Columns["RS_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_VARIANT]
        ///</summary>
        public static void MakeTable_RsVariant(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_VARIANT") ) return;
            DataTable table = dataset.Tables.Add("RS_VARIANT");
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("GUID", typeof(System.Guid)); // uniqueidentifier
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("START_POINT", typeof(System.Boolean)); // bit
            table.Columns.Add("STOP_TIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_RS_VARIANT
        ///</summary>
        public static void MakePK_RsVariant(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_VARIANT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_VARIANT_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_VARIANT
        ///</summary>
        public static void MakeIdentities_RsVariant(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_VARIANT"];
            table.Columns["RS_VARIANT_ID"].AutoIncrement = true;
            table.Columns["RS_VARIANT_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_VARIANT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RS_WAYOUT]
        ///</summary>
        public static void MakeTable_RsWayout(DataSet dataset)
        {
            if (dataset.Tables.Contains("RS_WAYOUT") ) return;
            DataTable table = dataset.Tables.Add("RS_WAYOUT");
            table.Columns.Add("RS_WAYOUT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RS_VARIANT_ID", typeof(System.Int32)); // int
            table.Columns.Add("GRAPHIC_ID", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
            table.Columns.Add("INDEX", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT_0", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_OUT", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_IN_0", typeof(System.Int32)); // int
            table.Columns.Add("WAYOUT_ID", typeof(System.Int32)); // int
            table.Columns.Add("START_POINT", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_RS_WAYOUT
        ///</summary>
        public static void MakePK_RsWayout(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_WAYOUT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RS_WAYOUT_ID"]
            }
            ;
        }
        ///<summary>
        /// RS_WAYOUT
        ///</summary>
        public static void MakeIdentities_RsWayout(DataSet dataset)
        {
            DataTable table = dataset.Tables["RS_WAYOUT"];
            table.Columns["RS_WAYOUT_ID"].AutoIncrement = true;
            table.Columns["RS_WAYOUT_ID"].AutoIncrementSeed = -1;
            table.Columns["RS_WAYOUT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RULE]
        ///</summary>
        public static void MakeTable_Rule(DataSet dataset)
        {
            if (dataset.Tables.Contains("RULE") ) return;
            DataTable table = dataset.Tables.Add("RULE");
            table.Columns.Add("RULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("CLIENT_SIDE", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_RULE
        ///</summary>
        public static void MakePK_Rule(DataSet dataset)
        {
            DataTable table = dataset.Tables["RULE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RULE_ID"]
            }
            ;
        }
        ///<summary>
        /// RULE
        ///</summary>
        public static void MakeIdentities_Rule(DataSet dataset)
        {
            DataTable table = dataset.Tables["RULE"];
            table.Columns["RULE_ID"].AutoIncrement = true;
            table.Columns["RULE_ID"].AutoIncrementSeed = -1;
            table.Columns["RULE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULE]
        ///</summary>
        public static void MakeTable_Schedule(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE");
            table.Columns.Add("SCHEDULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_ID", typeof(System.Int32)); // int
            table.Columns.Add("DAY_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
            table.Columns.Add("WAYOUT", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE
        ///</summary>
        public static void MakePK_Schedule(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULE_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULE
        ///</summary>
        public static void MakeIdentities_Schedule(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE"];
            table.Columns["SCHEDULE_ID"].AutoIncrement = true;
            table.Columns["SCHEDULE_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULE_DETAIL]
        ///</summary>
        public static void MakeTable_ScheduleDetail(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE_DETAIL") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE_DETAIL");
            table.Columns.Add("SCHEDULE_DETAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("SCHEDULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SHIFT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RUN_FULL", typeof(System.Single)); // real
            table.Columns.Add("RUN_EMPTY", typeof(System.Single)); // real
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE_DETAIL
        ///</summary>
        public static void MakePK_ScheduleDetail(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_DETAIL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULE_DETAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULE_DETAIL
        ///</summary>
        public static void MakeIdentities_ScheduleDetail(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_DETAIL"];
            table.Columns["SCHEDULE_DETAIL_ID"].AutoIncrement = true;
            table.Columns["SCHEDULE_DETAIL_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULE_DETAIL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULE_DETAIL_INFO]
        ///</summary>
        public static void MakeTable_ScheduleDetailInfo(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE_DETAIL_INFO") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE_DETAIL_INFO");
            table.Columns.Add("SCHEDULE_DETAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_DINNER", typeof(System.Int32)); // int
            table.Columns.Add("TIME_STOPWORK", typeof(System.Int32)); // int
            table.Columns.Add("TIME_BEGIN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_END", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE_DETAIL_INFO
        ///</summary>
        public static void MakePK_ScheduleDetailInfo(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_DETAIL_INFO"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULE_DETAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// [SCHEDULE_GEOZONE]
        ///</summary>
        public static void MakeTable_ScheduleGeozone(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE_GEOZONE") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE_GEOZONE");
            table.Columns.Add("SCHEDULE_GEOZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_GEOZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_IN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_OUT", typeof(System.Int32)); // int
            table.Columns.Add("TIME_DEVIATION", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE_GEOZONE
        ///</summary>
        public static void MakePK_ScheduleGeozone(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_GEOZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULE_GEOZONE_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULE_GEOZONE
        ///</summary>
        public static void MakeIdentities_ScheduleGeozone(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_GEOZONE"];
            table.Columns["SCHEDULE_GEOZONE_ID"].AutoIncrement = true;
            table.Columns["SCHEDULE_GEOZONE_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULE_GEOZONE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULE_PASSAGE]
        ///</summary>
        public static void MakeTable_SchedulePassage(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE_PASSAGE") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE_PASSAGE");
            table.Columns.Add("WH_ID", typeof(System.Int32)); // int
            table.Columns.Add("SP_ID", typeof(System.Int32)); // int
            table.Columns.Add("LOG_TIME_IN", typeof(System.DateTime)); // datetime
            table.Columns.Add("LOG_TIME_OUT", typeof(System.DateTime)); // datetime
            table.Columns.Add("TIME_IN", typeof(System.DateTime)); // datetime
            table.Columns.Add("TIME_OUT", typeof(System.DateTime)); // datetime
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE_PASSAGE
        ///</summary>
        public static void MakePK_SchedulePassage(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_PASSAGE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WH_ID"]
                , table.Columns["SP_ID"]
            }
            ;
        }
        ///<summary>
        /// [SCHEDULE_POINT]
        ///</summary>
        public static void MakeTable_SchedulePoint(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULE_POINT") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULE_POINT");
            table.Columns.Add("SCHEDULE_POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME_IN", typeof(System.Int32)); // int
            table.Columns.Add("TIME_OUT", typeof(System.Int32)); // int
            table.Columns.Add("TIME_DEVIATION", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEO_SEGMENT_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_SCHEDULE_POINT
        ///</summary>
        public static void MakePK_SchedulePoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_POINT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULE_POINT_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULE_POINT
        ///</summary>
        public static void MakeIdentities_SchedulePoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULE_POINT"];
            table.Columns["SCHEDULE_POINT_ID"].AutoIncrement = true;
            table.Columns["SCHEDULE_POINT_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULE_POINT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULEREVENT]
        ///</summary>
        public static void MakeTable_Schedulerevent(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULEREVENT") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULEREVENT");
            table.Columns.Add("SCHEDULEREVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("PLUGIN_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("CONFIG_XML", typeof(System.String)); // ntext
        }
        ///<summary>
        /// PK_SCHEDULEREVENT
        ///</summary>
        public static void MakePK_Schedulerevent(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULEREVENT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULEREVENT_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULEREVENT
        ///</summary>
        public static void MakeIdentities_Schedulerevent(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULEREVENT"];
            table.Columns["SCHEDULEREVENT_ID"].AutoIncrement = true;
            table.Columns["SCHEDULEREVENT_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULEREVENT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SCHEDULERQUEUE]
        ///</summary>
        public static void MakeTable_Schedulerqueue(DataSet dataset)
        {
            if (dataset.Tables.Contains("SCHEDULERQUEUE") ) return;
            DataTable table = dataset.Tables.Add("SCHEDULERQUEUE");
            table.Columns.Add("SCHEDULERQUEUE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SCHEDULEREVENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("NEAREST_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("CONFIG_XML", typeof(System.String)); // ntext
            table.Columns.Add("REPETITION_XML", typeof(System.String)); // ntext
        }
        ///<summary>
        /// PK_SCHEDULERQUEUE
        ///</summary>
        public static void MakePK_Schedulerqueue(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULERQUEUE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SCHEDULERQUEUE_ID"]
            }
            ;
        }
        ///<summary>
        /// SCHEDULERQUEUE
        ///</summary>
        public static void MakeIdentities_Schedulerqueue(DataSet dataset)
        {
            DataTable table = dataset.Tables["SCHEDULERQUEUE"];
            table.Columns["SCHEDULERQUEUE_ID"].AutoIncrement = true;
            table.Columns["SCHEDULERQUEUE_ID"].AutoIncrementSeed = -1;
            table.Columns["SCHEDULERQUEUE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SEASON]
        ///</summary>
        public static void MakeTable_Season(DataSet dataset)
        {
            if (dataset.Tables.Contains("SEASON") ) return;
            DataTable table = dataset.Tables.Add("SEASON");
            table.Columns.Add("SEASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("TIME_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("TIME_END", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_SEASON
        ///</summary>
        public static void MakePK_Season(DataSet dataset)
        {
            DataTable table = dataset.Tables["SEASON"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SEASON_ID"]
            }
            ;
        }
        ///<summary>
        /// SEASON
        ///</summary>
        public static void MakeIdentities_Season(DataSet dataset)
        {
            DataTable table = dataset.Tables["SEASON"];
            table.Columns["SEASON_ID"].AutoIncrement = true;
            table.Columns["SEASON_ID"].AutoIncrementSeed = -1;
            table.Columns["SEASON_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SEATTYPE]
        ///</summary>
        public static void MakeTable_Seattype(DataSet dataset)
        {
            if (dataset.Tables.Contains("SEATTYPE") ) return;
            DataTable table = dataset.Tables.Add("SEATTYPE");
            table.Columns.Add("SEATTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SEATTYPE_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_SEATTYPE
        ///</summary>
        public static void MakePK_Seattype(DataSet dataset)
        {
            DataTable table = dataset.Tables["SEATTYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SEATTYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// SEATTYPE
        ///</summary>
        public static void MakeIdentities_Seattype(DataSet dataset)
        {
            DataTable table = dataset.Tables["SEATTYPE"];
            table.Columns["SEATTYPE_ID"].AutoIncrement = true;
            table.Columns["SEATTYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["SEATTYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [Servers]
        ///</summary>
        public static void MakeTable_Servers(DataSet dataset)
        {
            if (dataset.Tables.Contains("Servers") ) return;
            DataTable table = dataset.Tables.Add("Servers");
            table.Columns.Add("Stend_ID", typeof(System.Int32)); // int
            table.Columns.Add("Stend_name", typeof(System.String)); // nvarchar
            table.Columns.Add("Place", typeof(System.String)); // nvarchar
            table.Columns.Add("OS", typeof(System.Int32)); // int
            table.Columns.Add("IP-address", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void MakeTable_Session(DataSet dataset)
        {
            if (dataset.Tables.Contains("SESSION") ) return;
            DataTable table = dataset.Tables.Add("SESSION");
            table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("HOST_IP", typeof(System.String)); // nvarchar
            table.Columns.Add("HOST_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SESSION_START", typeof(System.DateTime)); // datetime
            table.Columns.Add("SESSION_END", typeof(System.DateTime)); // datetime
            table.Columns.Add("CLIENT_VERSION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_SESSION
        ///</summary>
        public static void MakePK_Session(DataSet dataset)
        {
            DataTable table = dataset.Tables["SESSION"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SESSION_ID"]
            }
            ;
        }
        ///<summary>
        /// SESSION
        ///</summary>
        public static void MakeIdentities_Session(DataSet dataset)
        {
            DataTable table = dataset.Tables["SESSION"];
            table.Columns["SESSION_ID"].AutoIncrement = true;
            table.Columns["SESSION_ID"].AutoIncrementSeed = -1;
            table.Columns["SESSION_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SMS_LOG]
        ///</summary>
        public static void MakeTable_SmsLog(DataSet dataset)
        {
            if (dataset.Tables.Contains("SMS_LOG") ) return;
            DataTable table = dataset.Tables.Add("SMS_LOG");
            table.Columns.Add("SMS_LOG_ID", typeof(System.Int32)); // int
            table.Columns.Add("SMS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("OWNER_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_NUMBER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// [SMS_TYPE]
        ///</summary>
        public static void MakeTable_SmsType(DataSet dataset)
        {
            if (dataset.Tables.Contains("SMS_TYPE") ) return;
            DataTable table = dataset.Tables.Add("SMS_TYPE");
            table.Columns.Add("SMS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SMS_DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_SMS_TYPE
        ///</summary>
        public static void MakePK_SmsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["SMS_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SMS_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// SMS_TYPE
        ///</summary>
        public static void MakeIdentities_SmsType(DataSet dataset)
        {
            DataTable table = dataset.Tables["SMS_TYPE"];
            table.Columns["SMS_TYPE_ID"].AutoIncrement = true;
            table.Columns["SMS_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["SMS_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void MakeTable_Sysdiagrams(DataSet dataset)
        {
            if (dataset.Tables.Contains("sysdiagrams") ) return;
            DataTable table = dataset.Tables.Add("sysdiagrams");
            table.Columns.Add("name", typeof(System.String)); // nvarchar
            table.Columns.Add("principal_id", typeof(System.Int32)); // int
            table.Columns.Add("diagram_id", typeof(System.Int32)); // int
            table.Columns.Add("version", typeof(System.Int32)); // int
            table.Columns.Add("definition", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK__sysdiagrams__3E2B5435
        ///</summary>
        public static void MakePK_Sysdiagrams(DataSet dataset)
        {
            DataTable table = dataset.Tables["sysdiagrams"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["diagram_id"]
            }
            ;
        }
        ///<summary>
        /// sysdiagrams
        ///</summary>
        public static void MakeIdentities_Sysdiagrams(DataSet dataset)
        {
            DataTable table = dataset.Tables["sysdiagrams"];
            table.Columns["diagram_id"].AutoIncrement = true;
            table.Columns["diagram_id"].AutoIncrementSeed = -1;
            table.Columns["diagram_id"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [TASK_PROCESSOR_LOG]
        ///</summary>
        public static void MakeTable_TaskProcessorLog(DataSet dataset)
        {
            if (dataset.Tables.Contains("TASK_PROCESSOR_LOG") ) return;
            DataTable table = dataset.Tables.Add("TASK_PROCESSOR_LOG");
            table.Columns.Add("TASK_PROCESSOR_LOG_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("FROM", typeof(System.DateTime)); // datetime
            table.Columns.Add("TO", typeof(System.DateTime)); // datetime
            table.Columns.Add("LAST_POSITION_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("START_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("DIE_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("RESULT", typeof(System.Boolean)); // bit
            table.Columns.Add("RETRY_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("POSITIONS_RECEIVED", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_TASK_PROCESSOR_LOG
        ///</summary>
        public static void MakePK_TaskProcessorLog(DataSet dataset)
        {
            DataTable table = dataset.Tables["TASK_PROCESSOR_LOG"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TASK_PROCESSOR_LOG_ID"]
            }
            ;
        }
        ///<summary>
        /// TASK_PROCESSOR_LOG
        ///</summary>
        public static void MakeIdentities_TaskProcessorLog(DataSet dataset)
        {
            DataTable table = dataset.Tables["TASK_PROCESSOR_LOG"];
            table.Columns["TASK_PROCESSOR_LOG_ID"].AutoIncrement = true;
            table.Columns["TASK_PROCESSOR_LOG_ID"].AutoIncrementSeed = -1;
            table.Columns["TASK_PROCESSOR_LOG_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [Temp_TSS]
        ///</summary>
        public static void MakeTable_TempTss(DataSet dataset)
        {
            if (dataset.Tables.Contains("Temp_TSS") ) return;
            DataTable table = dataset.Tables.Add("Temp_TSS");
            table.Columns.Add("nomer", typeof(System.String)); // nvarchar
            table.Columns.Add("textdetail", typeof(System.String)); // varchar
            table.Columns.Add("FIO", typeof(System.String)); // text
            table.Columns.Add("csstatename", typeof(System.String)); // nvarchar
            table.Columns.Add("Complete", typeof(System.String)); // nvarchar
            table.Columns.Add("Executer", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [TmpDriverName]
        ///</summary>
        public static void MakeTable_Tmpdrivername(DataSet dataset)
        {
            if (dataset.Tables.Contains("TmpDriverName") ) return;
            DataTable table = dataset.Tables.Add("TmpDriverName");
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SHORT_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void MakeTable_Trail(DataSet dataset)
        {
            if (dataset.Tables.Contains("TRAIL") ) return;
            DataTable table = dataset.Tables.Add("TRAIL");
            table.Columns.Add("TRAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRAIL_TIME", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_TRAIL
        ///</summary>
        public static void MakePK_Trail(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRAIL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TRAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// TRAIL
        ///</summary>
        public static void MakeIdentities_Trail(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRAIL"];
            table.Columns["TRAIL_ID"].AutoIncrement = true;
            table.Columns["TRAIL_ID"].AutoIncrementSeed = -1;
            table.Columns["TRAIL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [TRIP]
        ///</summary>
        public static void MakeTable_Trip(DataSet dataset)
        {
            if (dataset.Tables.Contains("TRIP") ) return;
            DataTable table = dataset.Tables.Add("TRIP");
            table.Columns.Add("TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_TIME", typeof(System.Int32)); // int
            table.Columns.Add("END_TIME", typeof(System.Int32)); // int
            table.Columns.Add("SCHEDULE_DETAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP", typeof(System.Int32)); // int
            table.Columns.Add("ROUTE_TRIP_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_TRIP
        ///</summary>
        public static void MakePK_Trip(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// TRIP
        ///</summary>
        public static void MakeIdentities_Trip(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRIP"];
            table.Columns["TRIP_ID"].AutoIncrement = true;
            table.Columns["TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [TRIP_KIND]
        ///</summary>
        public static void MakeTable_TripKind(DataSet dataset)
        {
            if (dataset.Tables.Contains("TRIP_KIND") ) return;
            DataTable table = dataset.Tables.Add("TRIP_KIND");
            table.Columns.Add("TRIP_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("WORKTIME_STATUS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("WORK_TIME", typeof(System.Boolean)); // bit
            table.Columns.Add("LINE_TIME", typeof(System.Boolean)); // bit
            table.Columns.Add("IDLE_TIME", typeof(System.Boolean)); // bit
            table.Columns.Add("RESERVE_TIME", typeof(System.Boolean)); // bit
            table.Columns.Add("PRODUCTION", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_TRIP_KIND
        ///</summary>
        public static void MakePK_TripKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRIP_KIND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TRIP_KIND_ID"]
            }
            ;
        }
        ///<summary>
        /// [VEHICLE]
        ///</summary>
        public static void MakeTable_Vehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE");
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PUBLIC_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("GARAGE_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("VEHICLE_TYPE", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("VEHICLE_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("OWNER_ID", typeof(System.Int32)); // int
            table.Columns.Add("VALIDATOR_PRESENT", typeof(System.Boolean)); // bit
            table.Columns.Add("SEATTYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("SITTING_BERTHS_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("STANDING_BERTHS_COUNT", typeof(System.Int32)); // int
            table.Columns.Add("GUARD", typeof(System.Boolean)); // bit
            table.Columns.Add("FUEL_TANK", typeof(System.Int16)); // smallint
            table.Columns.Add("GRAPHIC", typeof(System.Int32)); // int
            table.Columns.Add("GRAPHIC_BEGIN", typeof(System.DateTime)); // datetime
            table.Columns.Add("TIME_PREPARATION", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_REMOVED_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("DEPARTMENT", typeof(System.Int32)); // int
            table.Columns.Add("VIN", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_VEHICLE
        ///</summary>
        public static void MakePK_Vehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLE
        ///</summary>
        public static void MakeIdentities_Vehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE"];
            table.Columns["VEHICLE_ID"].AutoIncrement = true;
            table.Columns["VEHICLE_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLE_KIND]
        ///</summary>
        public static void MakeTable_VehicleKind(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE_KIND") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE_KIND");
            table.Columns.Add("VEHICLE_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_VEHICLE_KIND
        ///</summary>
        public static void MakePK_VehicleKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_KIND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_KIND_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLE_KIND
        ///</summary>
        public static void MakeIdentities_VehicleKind(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_KIND"];
            table.Columns["VEHICLE_KIND_ID"].AutoIncrement = true;
            table.Columns["VEHICLE_KIND_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLE_KIND_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLE_OWNER]
        ///</summary>
        public static void MakeTable_VehicleOwner(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE_OWNER") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE_OWNER");
            table.Columns.Add("VEHICLE_OWNER_ID", typeof(System.Int32)); // int
            table.Columns.Add("OWNER_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("OWNER_NUMBER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_VEHICLE_OWNER
        ///</summary>
        public static void MakePK_VehicleOwner(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_OWNER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_OWNER_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLE_OWNER
        ///</summary>
        public static void MakeIdentities_VehicleOwner(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_OWNER"];
            table.Columns["VEHICLE_OWNER_ID"].AutoIncrement = true;
            table.Columns["VEHICLE_OWNER_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLE_OWNER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLE_PICTURE]
        ///</summary>
        public static void MakeTable_VehiclePicture(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE_PICTURE") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE_PICTURE");
            table.Columns.Add("VEHICLE_PICTURE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PICTURE", typeof(System.Byte[])); // image
        }
        ///<summary>
        /// PK_VEHICLE_PICTURE
        ///</summary>
        public static void MakePK_VehiclePicture(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_PICTURE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_PICTURE_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLE_PICTURE
        ///</summary>
        public static void MakeIdentities_VehiclePicture(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_PICTURE"];
            table.Columns["VEHICLE_PICTURE_ID"].AutoIncrement = true;
            table.Columns["VEHICLE_PICTURE_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLE_PICTURE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLE_RULE]
        ///</summary>
        public static void MakeTable_VehicleRule(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE_RULE") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE_RULE");
            table.Columns.Add("VEHICLE_RULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("RULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VALUE", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_VEHICLE_RULE
        ///</summary>
        public static void MakePK_VehicleRule(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_RULE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_RULE_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLE_RULE
        ///</summary>
        public static void MakeIdentities_VehicleRule(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_RULE"];
            table.Columns["VEHICLE_RULE_ID"].AutoIncrement = true;
            table.Columns["VEHICLE_RULE_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLE_RULE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLE_STATUS]
        ///</summary>
        public static void MakeTable_VehicleStatus(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLE_STATUS") ) return;
            DataTable table = dataset.Tables.Add("VEHICLE_STATUS");
            table.Columns.Add("VEHICLE_STATUS_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_STATUS_NAME", typeof(System.String)); // varchar
        }
        ///<summary>
        /// PK_VEHICLE_STATUS
        ///</summary>
        public static void MakePK_VehicleStatus(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLE_STATUS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLE_STATUS_ID"]
            }
            ;
        }
        ///<summary>
        /// [VEHICLEGROUP]
        ///</summary>
        public static void MakeTable_Vehiclegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLEGROUP") ) return;
            DataTable table = dataset.Tables.Add("VEHICLEGROUP");
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_VEHICLEGROUP
        ///</summary>
        public static void MakePK_Vehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLEGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLEGROUP
        ///</summary>
        public static void MakeIdentities_Vehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLEGROUP"];
            table.Columns["VEHICLEGROUP_ID"].AutoIncrement = true;
            table.Columns["VEHICLEGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLEGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLEGROUP_RULE]
        ///</summary>
        public static void MakeTable_VehiclegroupRule(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLEGROUP_RULE") ) return;
            DataTable table = dataset.Tables.Add("VEHICLEGROUP_RULE");
            table.Columns.Add("VEHICLEGROUP_RULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VALUE", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_VEHICLEGROUP_RULE
        ///</summary>
        public static void MakePK_VehiclegroupRule(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLEGROUP_RULE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLEGROUP_RULE_ID"]
            }
            ;
        }
        ///<summary>
        /// VEHICLEGROUP_RULE
        ///</summary>
        public static void MakeIdentities_VehiclegroupRule(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLEGROUP_RULE"];
            table.Columns["VEHICLEGROUP_RULE_ID"].AutoIncrement = true;
            table.Columns["VEHICLEGROUP_RULE_ID"].AutoIncrementSeed = -1;
            table.Columns["VEHICLEGROUP_RULE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [VEHICLEGROUP_VEHICLE]
        ///</summary>
        public static void MakeTable_VehiclegroupVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("VEHICLEGROUP_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("VEHICLEGROUP_VEHICLE");
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_VEHICLEGROUP_VEHICLE
        ///</summary>
        public static void MakePK_VehiclegroupVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["VEHICLEGROUP_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["VEHICLEGROUP_ID"]
                , table.Columns["VEHICLE_ID"]
            }
            ;
        }
        ///<summary>
        /// [Version]
        ///</summary>
        public static void MakeTable_Version(DataSet dataset)
        {
            if (dataset.Tables.Contains("Version") ) return;
            DataTable table = dataset.Tables.Add("Version");
            table.Columns.Add("ServisePack(Version)", typeof(System.String)); // char
            table.Columns.Add("date", typeof(System.DateTime)); // datetime
            table.Columns.Add("YO", typeof(System.String)); // char
        }
        ///<summary>
        /// [WAYBILL]
        ///</summary>
        public static void MakeTable_Waybill(DataSet dataset)
        {
            if (dataset.Tables.Contains("WAYBILL") ) return;
            DataTable table = dataset.Tables.Add("WAYBILL");
            table.Columns.Add("WAYBILL_ID", typeof(System.Int32)); // int
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("BEGIN_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("END_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("SHIFT_ID", typeof(System.Int32)); // int
            table.Columns.Add("RELEASED", typeof(System.Boolean)); // bit
            table.Columns.Add("ACTIVE", typeof(System.Boolean)); // bit
            table.Columns.Add("DESTINATION", typeof(System.Int32)); // int
            table.Columns.Add("RESERVE", typeof(System.Boolean)); // bit
            table.Columns.Add("WAYBILL_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("END_TIME_PLANNED", typeof(System.DateTime)); // datetime
            table.Columns.Add("TOTAL_PENALTY", typeof(System.Int32)); // int
            table.Columns.Add("TRAINEE", typeof(System.Boolean)); // bit
        }
        ///<summary>
        /// PK_WAYBILL
        ///</summary>
        public static void MakePK_Waybill(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WAYBILL_ID"]
            }
            ;
        }
        ///<summary>
        /// WAYBILL
        ///</summary>
        public static void MakeIdentities_Waybill(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILL"];
            table.Columns["WAYBILL_ID"].AutoIncrement = true;
            table.Columns["WAYBILL_ID"].AutoIncrementSeed = -1;
            table.Columns["WAYBILL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WAYBILL_HEADER]
        ///</summary>
        public static void MakeTable_WaybillHeader(DataSet dataset)
        {
            if (dataset.Tables.Contains("WAYBILL_HEADER") ) return;
            DataTable table = dataset.Tables.Add("WAYBILL_HEADER");
            table.Columns.Add("WAYBILL_HEADER_ID", typeof(System.Int32)); // int
            table.Columns.Add("SCHEDULE_ID", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILL_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CANCELLED", typeof(System.Boolean)); // bit
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("PRINTED", typeof(System.Int32)); // int
            table.Columns.Add("PRINTREASON", typeof(System.Int32)); // int
            table.Columns.Add("INITIAL_TRIP_KIND", typeof(System.Int32)); // int
            table.Columns.Add("TRAILOR1", typeof(System.Int32)); // int
            table.Columns.Add("TRAILOR2", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WAYBILL_HEADER
        ///</summary>
        public static void MakePK_WaybillHeader(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILL_HEADER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WAYBILL_HEADER_ID"]
            }
            ;
        }
        ///<summary>
        /// WAYBILL_HEADER
        ///</summary>
        public static void MakeIdentities_WaybillHeader(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILL_HEADER"];
            table.Columns["WAYBILL_HEADER_ID"].AutoIncrement = true;
            table.Columns["WAYBILL_HEADER_ID"].AutoIncrementSeed = -1;
            table.Columns["WAYBILL_HEADER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WAYBILLHEADER_WAYBILLMARK]
        ///</summary>
        public static void MakeTable_WaybillheaderWaybillmark(DataSet dataset)
        {
            if (dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK") ) return;
            DataTable table = dataset.Tables.Add("WAYBILLHEADER_WAYBILLMARK");
            table.Columns.Add("WAYBILLHEADER_ID", typeof(System.Int32)); // int
            table.Columns.Add("MARK_DATE", typeof(System.DateTime)); // datetime
            table.Columns.Add("WAYBILLMARK_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WAYBILLHEADER_WAYBILLMARK
        ///</summary>
        public static void MakePK_WaybillheaderWaybillmark(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WAYBILLHEADER_ID"]
                , table.Columns["WAYBILLMARK_ID"]
            }
            ;
        }
        ///<summary>
        /// [WAYBILLMARK]
        ///</summary>
        public static void MakeTable_Waybillmark(DataSet dataset)
        {
            if (dataset.Tables.Contains("WAYBILLMARK") ) return;
            DataTable table = dataset.Tables.Add("WAYBILLMARK");
            table.Columns.Add("WAYBILLMARK_ID", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILLMARK_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_WAYBILL_MARK_TYPE
        ///</summary>
        public static void MakePK_Waybillmark(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILLMARK"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WAYBILLMARK_ID"]
            }
            ;
        }
        ///<summary>
        /// WAYBILLMARK
        ///</summary>
        public static void MakeIdentities_Waybillmark(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYBILLMARK"];
            table.Columns["WAYBILLMARK_ID"].AutoIncrement = true;
            table.Columns["WAYBILLMARK_ID"].AutoIncrementSeed = -1;
            table.Columns["WAYBILLMARK_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WAYOUT]
        ///</summary>
        public static void MakeTable_Wayout(DataSet dataset)
        {
            if (dataset.Tables.Contains("WAYOUT") ) return;
            DataTable table = dataset.Tables.Add("WAYOUT");
            table.Columns.Add("ROUTE", typeof(System.Int32)); // int
            table.Columns.Add("EXT_NUMBER", typeof(System.String)); // nvarchar
            table.Columns.Add("WAYOUT_ID", typeof(System.Int32)); // int
            table.Columns.Add("graphic", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WAYOUT
        ///</summary>
        public static void MakePK_Wayout(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYOUT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WAYOUT_ID"]
            }
            ;
        }
        ///<summary>
        /// WAYOUT
        ///</summary>
        public static void MakeIdentities_Wayout(DataSet dataset)
        {
            DataTable table = dataset.Tables["WAYOUT"];
            table.Columns["WAYOUT_ID"].AutoIncrement = true;
            table.Columns["WAYOUT_ID"].AutoIncrementSeed = -1;
            table.Columns["WAYOUT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WB_TRIP]
        ///</summary>
        public static void MakeTable_WbTrip(DataSet dataset)
        {
            if (dataset.Tables.Contains("WB_TRIP") ) return;
            DataTable table = dataset.Tables.Add("WB_TRIP");
            table.Columns.Add("WB_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("BEGIN_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("END_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("COMMENT", typeof(System.String)); // nvarchar
            table.Columns.Add("WAYBILL_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRIP_KIND_ID", typeof(System.Int32)); // int
            table.Columns.Add("WAYBILL_HEADER_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ACTUAL_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("WORKTIME_REASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("GEO_TRIP_ID", typeof(System.Int32)); // int
            table.Columns.Add("PENALTY", typeof(System.Int32)); // int
            table.Columns.Add("ORDER_TRIP_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WB_TRIP
        ///</summary>
        public static void MakePK_WbTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["WB_TRIP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WB_TRIP_ID"]
            }
            ;
        }
        ///<summary>
        /// WB_TRIP
        ///</summary>
        public static void MakeIdentities_WbTrip(DataSet dataset)
        {
            DataTable table = dataset.Tables["WB_TRIP"];
            table.Columns["WB_TRIP_ID"].AutoIncrement = true;
            table.Columns["WB_TRIP_ID"].AutoIncrementSeed = -1;
            table.Columns["WB_TRIP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WEB_LINE]
        ///</summary>
        public static void MakeTable_WebLine(DataSet dataset)
        {
            if (dataset.Tables.Contains("WEB_LINE") ) return;
            DataTable table = dataset.Tables.Add("WEB_LINE");
            table.Columns.Add("LINE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__WEB_LINE__1475118B
        ///</summary>
        public static void MakePK_WebLine(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_LINE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LINE_ID"]
            }
            ;
        }
        ///<summary>
        /// WEB_LINE
        ///</summary>
        public static void MakeIdentities_WebLine(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_LINE"];
            table.Columns["LINE_ID"].AutoIncrement = true;
            table.Columns["LINE_ID"].AutoIncrementSeed = -1;
            table.Columns["LINE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WEB_LINE_VERTEX]
        ///</summary>
        public static void MakeTable_WebLineVertex(DataSet dataset)
        {
            if (dataset.Tables.Contains("WEB_LINE_VERTEX") ) return;
            DataTable table = dataset.Tables.Add("WEB_LINE_VERTEX");
            table.Columns.Add("LINE_VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("LINE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__WEB_LINE_VERTEX__20DAE870
        ///</summary>
        public static void MakePK_WebLineVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_LINE_VERTEX"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LINE_VERTEX_ID"]
            }
            ;
        }
        ///<summary>
        /// WEB_LINE_VERTEX
        ///</summary>
        public static void MakeIdentities_WebLineVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_LINE_VERTEX"];
            table.Columns["LINE_VERTEX_ID"].AutoIncrement = true;
            table.Columns["LINE_VERTEX_ID"].AutoIncrementSeed = -1;
            table.Columns["LINE_VERTEX_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WEB_POINT]
        ///</summary>
        public static void MakeTable_WebPoint(DataSet dataset)
        {
            if (dataset.Tables.Contains("WEB_POINT") ) return;
            DataTable table = dataset.Tables.Add("WEB_POINT");
            table.Columns.Add("POINT_ID", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__WEB_POINT__1DFE7BC5
        ///</summary>
        public static void MakePK_WebPoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_POINT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["POINT_ID"]
            }
            ;
        }
        ///<summary>
        /// WEB_POINT
        ///</summary>
        public static void MakeIdentities_WebPoint(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_POINT"];
            table.Columns["POINT_ID"].AutoIncrement = true;
            table.Columns["POINT_ID"].AutoIncrementSeed = -1;
            table.Columns["POINT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WEB_POINT_TYPE]
        ///</summary>
        public static void MakeTable_WebPointType(DataSet dataset)
        {
            if (dataset.Tables.Contains("WEB_POINT_TYPE") ) return;
            DataTable table = dataset.Tables.Add("WEB_POINT_TYPE");
            table.Columns.Add("TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK__WEB_POINT_TYPE__1B220F1A
        ///</summary>
        public static void MakePK_WebPointType(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_POINT_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// WEB_POINT_TYPE
        ///</summary>
        public static void MakeIdentities_WebPointType(DataSet dataset)
        {
            DataTable table = dataset.Tables["WEB_POINT_TYPE"];
            table.Columns["TYPE_ID"].AutoIncrement = true;
            table.Columns["TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WORK_TIME]
        ///</summary>
        public static void MakeTable_WorkTime(DataSet dataset)
        {
            if (dataset.Tables.Contains("WORK_TIME") ) return;
            DataTable table = dataset.Tables.Add("WORK_TIME");
            table.Columns.Add("DRIVER_ID", typeof(System.Int32)); // int
            table.Columns.Add("YEAR", typeof(System.Int32)); // int
            table.Columns.Add("MONTH", typeof(System.Int32)); // int
            table.Columns.Add("TIME_WORK", typeof(System.Int32)); // int
            table.Columns.Add("TIME_LINE", typeof(System.Int32)); // int
            table.Columns.Add("TIME_IDLE", typeof(System.Int32)); // int
            table.Columns.Add("TIME_RESERVE", typeof(System.Int32)); // int
            table.Columns.Add("TIME_NIGHT", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WORK_TIME
        ///</summary>
        public static void MakePK_WorkTime(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORK_TIME"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["DRIVER_ID"]
            }
            ;
        }
        ///<summary>
        /// [WORKPLACE_CONFIG]
        ///</summary>
        public static void MakeTable_WorkplaceConfig(DataSet dataset)
        {
            if (dataset.Tables.Contains("WORKPLACE_CONFIG") ) return;
            DataTable table = dataset.Tables.Add("WORKPLACE_CONFIG");
            table.Columns.Add("ID", typeof(System.Int32)); // int
            table.Columns.Add("PARAMETER_NAME", typeof(System.String)); // char
            table.Columns.Add("PARAMETER_VALUE", typeof(System.String)); // nvarchar
            table.Columns.Add("COMMENTS", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_WORKPLACE_CONFIG
        ///</summary>
        public static void MakePK_WorkplaceConfig(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKPLACE_CONFIG"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ID"]
            }
            ;
        }
        ///<summary>
        /// WORKPLACE_CONFIG
        ///</summary>
        public static void MakeIdentities_WorkplaceConfig(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKPLACE_CONFIG"];
            table.Columns["ID"].AutoIncrement = true;
            table.Columns["ID"].AutoIncrementSeed = -1;
            table.Columns["ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WORKSTATION]
        ///</summary>
        public static void MakeTable_Workstation(DataSet dataset)
        {
            if (dataset.Tables.Contains("WORKSTATION") ) return;
            DataTable table = dataset.Tables.Add("WORKSTATION");
            table.Columns.Add("WORKSTATION_ID", typeof(System.Int32)); // int
            table.Columns.Add("PLACE_NUM", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("CELLPHONE", typeof(System.String)); // nvarchar
            table.Columns.Add("FIO", typeof(System.String)); // nvarchar
            table.Columns.Add("STATUS", typeof(System.Int32)); // int
            table.Columns.Add("MACHINE_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_WORKSTATION
        ///</summary>
        public static void MakePK_Workstation(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKSTATION"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WORKSTATION_ID"]
            }
            ;
        }
        ///<summary>
        /// WORKSTATION
        ///</summary>
        public static void MakeIdentities_Workstation(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKSTATION"];
            table.Columns["WORKSTATION_ID"].AutoIncrement = true;
            table.Columns["WORKSTATION_ID"].AutoIncrementSeed = -1;
            table.Columns["WORKSTATION_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WORKTIME_REASON]
        ///</summary>
        public static void MakeTable_WorktimeReason(DataSet dataset)
        {
            if (dataset.Tables.Contains("WORKTIME_REASON") ) return;
            DataTable table = dataset.Tables.Add("WORKTIME_REASON");
            table.Columns.Add("WORKTIME_REASON_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("WORKTIME_STATUS_TYPE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_WORKTIME_REASON
        ///</summary>
        public static void MakePK_WorktimeReason(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKTIME_REASON"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WORKTIME_REASON_ID"]
            }
            ;
        }
        ///<summary>
        /// WORKTIME_REASON
        ///</summary>
        public static void MakeIdentities_WorktimeReason(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKTIME_REASON"];
            table.Columns["WORKTIME_REASON_ID"].AutoIncrement = true;
            table.Columns["WORKTIME_REASON_ID"].AutoIncrementSeed = -1;
            table.Columns["WORKTIME_REASON_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [WORKTIME_STATUS_TYPE]
        ///</summary>
        public static void MakeTable_WorktimeStatusType(DataSet dataset)
        {
            if (dataset.Tables.Contains("WORKTIME_STATUS_TYPE") ) return;
            DataTable table = dataset.Tables.Add("WORKTIME_STATUS_TYPE");
            table.Columns.Add("WORKTIME_STATUS_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("WORKTIME_STATUS_TYPE_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_WORKTIME_STATUS_TYPE
        ///</summary>
        public static void MakePK_WorktimeStatusType(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKTIME_STATUS_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["WORKTIME_STATUS_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// WORKTIME_STATUS_TYPE
        ///</summary>
        public static void MakeIdentities_WorktimeStatusType(DataSet dataset)
        {
            DataTable table = dataset.Tables["WORKTIME_STATUS_TYPE"];
            table.Columns["WORKTIME_STATUS_TYPE_ID"].AutoIncrement = true;
            table.Columns["WORKTIME_STATUS_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["WORKTIME_STATUS_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONE_PRIMITIVE]
        ///</summary>
        public static void MakeTable_ZonePrimitive(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONE_PRIMITIVE") ) return;
            DataTable table = dataset.Tables.Add("ZONE_PRIMITIVE");
            table.Columns.Add("PRIMITIVE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PRIMITIVE_TYPE", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("RADIUS", typeof(System.Single)); // real
        }
        ///<summary>
        /// PK__ZONE_PRIMITIVE__725503B1
        ///</summary>
        public static void MakePK_ZonePrimitive(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_PRIMITIVE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["PRIMITIVE_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONE_PRIMITIVE
        ///</summary>
        public static void MakeIdentities_ZonePrimitive(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_PRIMITIVE"];
            table.Columns["PRIMITIVE_ID"].AutoIncrement = true;
            table.Columns["PRIMITIVE_ID"].AutoIncrementSeed = -1;
            table.Columns["PRIMITIVE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONE_PRIMITIVE_VERTEX]
        ///</summary>
        public static void MakeTable_ZonePrimitiveVertex(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX") ) return;
            DataTable table = dataset.Tables.Add("ZONE_PRIMITIVE_VERTEX");
            table.Columns.Add("PRIMITIVE_VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("PRIMITIVE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VERTEX_ID", typeof(System.Int32)); // int
            table.Columns.Add("ORDER", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK__ZONE_PRIMITIVE_V__79F62579
        ///</summary>
        public static void MakePK_ZonePrimitiveVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_PRIMITIVE_VERTEX"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["PRIMITIVE_VERTEX_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONE_PRIMITIVE_VERTEX
        ///</summary>
        public static void MakeIdentities_ZonePrimitiveVertex(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_PRIMITIVE_VERTEX"];
            table.Columns["PRIMITIVE_VERTEX_ID"].AutoIncrement = true;
            table.Columns["PRIMITIVE_VERTEX_ID"].AutoIncrementSeed = -1;
            table.Columns["PRIMITIVE_VERTEX_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONE_TYPE]
        ///</summary>
        public static void MakeTable_ZoneType(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONE_TYPE") ) return;
            DataTable table = dataset.Tables.Add("ZONE_TYPE");
            table.Columns.Add("ZONE_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_ZONE_TYPE
        ///</summary>
        public static void MakePK_ZoneType(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONE_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// [ZONE_VEHICLE]
        ///</summary>
        public static void MakeTable_ZoneVehicle(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONE_VEHICLE") ) return;
            DataTable table = dataset.Tables.Add("ZONE_VEHICLE");
            table.Columns.Add("ZONE_VEHICLE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ZONE_VEHICLE
        ///</summary>
        public static void MakePK_ZoneVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_VEHICLE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONE_VEHICLE_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONE_VEHICLE
        ///</summary>
        public static void MakeIdentities_ZoneVehicle(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_VEHICLE"];
            table.Columns["ZONE_VEHICLE_ID"].AutoIncrement = true;
            table.Columns["ZONE_VEHICLE_ID"].AutoIncrementSeed = -1;
            table.Columns["ZONE_VEHICLE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONE_VEHICLEGROUP]
        ///</summary>
        public static void MakeTable_ZoneVehiclegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONE_VEHICLEGROUP") ) return;
            DataTable table = dataset.Tables.Add("ZONE_VEHICLEGROUP");
            table.Columns.Add("ZONE_VEHICLEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("VEHICLEGROUP_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ZONE_VEHICLEGROUP
        ///</summary>
        public static void MakePK_ZoneVehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_VEHICLEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONE_VEHICLEGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONE_VEHICLEGROUP
        ///</summary>
        public static void MakeIdentities_ZoneVehiclegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONE_VEHICLEGROUP"];
            table.Columns["ZONE_VEHICLEGROUP_ID"].AutoIncrement = true;
            table.Columns["ZONE_VEHICLEGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["ZONE_VEHICLEGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONEGROUP]
        ///</summary>
        public static void MakeTable_Zonegroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONEGROUP") ) return;
            DataTable table = dataset.Tables.Add("ZONEGROUP");
            table.Columns.Add("ZONEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_ZONEGROUP
        ///</summary>
        public static void MakePK_Zonegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONEGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONEGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONEGROUP
        ///</summary>
        public static void MakeIdentities_Zonegroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONEGROUP"];
            table.Columns["ZONEGROUP_ID"].AutoIncrement = true;
            table.Columns["ZONEGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["ZONEGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [ZONEGROUP_ZONE]
        ///</summary>
        public static void MakeTable_ZonegroupZone(DataSet dataset)
        {
            if (dataset.Tables.Contains("ZONEGROUP_ZONE") ) return;
            DataTable table = dataset.Tables.Add("ZONEGROUP_ZONE");
            table.Columns.Add("ZONEGROUP_ZONE_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONEGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ZONE_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_ZONEGROUP_ZONE
        ///</summary>
        public static void MakePK_ZonegroupZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONEGROUP_ZONE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ZONEGROUP_ZONE_ID"]
            }
            ;
        }
        ///<summary>
        /// ZONEGROUP_ZONE
        ///</summary>
        public static void MakeIdentities_ZonegroupZone(DataSet dataset)
        {
            DataTable table = dataset.Tables["ZONEGROUP_ZONE"];
            table.Columns["ZONEGROUP_ZONE_ID"].AutoIncrement = true;
            table.Columns["ZONEGROUP_ZONE_ID"].AutoIncrementSeed = -1;
            table.Columns["ZONEGROUP_ZONE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// Cal_DayKind
        ///</summary>
        public static void MakeRelation_Cal_DayKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Cal_DayKind")) return;
            DataColumn parent = dataset.Tables["DAY_KIND"].Columns["DAY_KIND_ID"];
            DataColumn child = dataset.Tables["CAL"].Columns["DAY_KIND"];
            dataset.Relations.Add("Cal_DayKind", parent, child);
        }
        ///<summary>
        /// Controller_Controller_Type
        ///</summary>
        public static void MakeRelation_Controller_Controller_Type(DataSet dataset)
        {
            if (dataset.Relations.Contains("Controller_Controller_Type")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_TYPE"].Columns["CONTROLLER_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_TYPE_ID"];
            dataset.Relations.Add("Controller_Controller_Type", parent, child);
        }
        ///<summary>
        /// Controller_Vehicle
        ///</summary>
        public static void MakeRelation_Controller_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("Controller_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("Controller_Vehicle", parent, child);
        }
        ///<summary>
        /// ControllerTime_Controller
        ///</summary>
        public static void MakeRelation_ControllerTime_Controller(DataSet dataset)
        {
            if (dataset.Relations.Contains("ControllerTime_Controller")) return;
            DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_TIME"].Columns["CONTROLLER_ID"];
            dataset.Relations.Add("ControllerTime_Controller", parent, child);
        }
        ///<summary>
        /// CountGSM_Operator
        ///</summary>
        public static void MakeRelation_CountGSM_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("CountGSM_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["COUNT_GSM"].Columns["ID_OPERATOR"];
            dataset.Relations.Add("CountGSM_Operator", parent, child);
        }
        ///<summary>
        /// CountGSM_Vehicle
        ///</summary>
        public static void MakeRelation_CountGSM_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("CountGSM_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["COUNT_GSM"].Columns["ID_VEHICLE"];
            dataset.Relations.Add("CountGSM_Vehicle", parent, child);
        }
        ///<summary>
        /// DayKind_DkSet
        ///</summary>
        public static void MakeRelation_DayKind_DkSet(DataSet dataset)
        {
            if (dataset.Relations.Contains("DayKind_DkSet")) return;
            DataColumn parent = dataset.Tables["DK_SET"].Columns["DK_SET_ID"];
            DataColumn child = dataset.Tables["DAY_KIND"].Columns["DK_SET"];
            dataset.Relations.Add("DayKind_DkSet", parent, child);
        }
        ///<summary>
        /// Driver_DriverStatus
        ///</summary>
        public static void MakeRelation_Driver_DriverStatus(DataSet dataset)
        {
            if (dataset.Relations.Contains("Driver_DriverStatus")) return;
            DataColumn parent = dataset.Tables["DRIVER_STATUS"].Columns["DRIVER_STATUS_ID"];
            DataColumn child = dataset.Tables["DRIVER"].Columns["DRIVER_STATUS_ID"];
            dataset.Relations.Add("Driver_DriverStatus", parent, child);
        }
        ///<summary>
        /// DriverGroupDriver_Driver
        ///</summary>
        public static void MakeRelation_DriverGroupDriver_Driver(DataSet dataset)
        {
            if (dataset.Relations.Contains("DriverGroupDriver_Driver")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["DRIVERGROUP_DRIVER"].Columns["DRIVER_ID"];
            dataset.Relations.Add("DriverGroupDriver_Driver", parent, child);
        }
        ///<summary>
        /// DriverGroupDriver_DriverGroup
        ///</summary>
        public static void MakeRelation_DriverGroupDriver_DriverGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("DriverGroupDriver_DriverGroup")) return;
            DataColumn parent = dataset.Tables["DRIVERGROUP"].Columns["DRIVERGROUP_ID"];
            DataColumn child = dataset.Tables["DRIVERGROUP_DRIVER"].Columns["DRIVERGROUP_ID"];
            dataset.Relations.Add("DriverGroupDriver_DriverGroup", parent, child);
        }
        ///<summary>
        /// DriverVehicle_Driver
        ///</summary>
        public static void MakeRelation_DriverVehicle_Driver(DataSet dataset)
        {
            if (dataset.Relations.Contains("DriverVehicle_Driver")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["DRIVER_VEHICLE"].Columns["DRIVER_ID"];
            dataset.Relations.Add("DriverVehicle_Driver", parent, child);
        }
        ///<summary>
        /// DriverVehicle_Vehicle
        ///</summary>
        public static void MakeRelation_DriverVehicle_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("DriverVehicle_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["DRIVER_VEHICLE"].Columns["VEHICLE"];
            dataset.Relations.Add("DriverVehicle_Vehicle", parent, child);
        }
        ///<summary>
        /// FactorValues_Factors
        ///</summary>
        public static void MakeRelation_FactorValues_Factors(DataSet dataset)
        {
            if (dataset.Relations.Contains("FactorValues_Factors")) return;
            DataColumn parent = dataset.Tables["FACTORS"].Columns["FACTOR_ID"];
            DataColumn child = dataset.Tables["FACTOR_VALUES"].Columns["FACTOR_ID"];
            dataset.Relations.Add("FactorValues_Factors", parent, child);
        }
        ///<summary>
        /// FactorValues_Season
        ///</summary>
        public static void MakeRelation_FactorValues_Season(DataSet dataset)
        {
            if (dataset.Relations.Contains("FactorValues_Season")) return;
            DataColumn parent = dataset.Tables["SEASON"].Columns["SEASON_ID"];
            DataColumn child = dataset.Tables["FACTOR_VALUES"].Columns["SEASON_ID"];
            dataset.Relations.Add("FactorValues_Season", parent, child);
        }
        ///<summary>
        /// FK_BLADING_BLADING_TYPE
        ///</summary>
        public static void MakeRelation_FK_BLADING_BLADING_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BLADING_BLADING_TYPE")) return;
            DataColumn parent = dataset.Tables["BLADING_TYPE"].Columns["BLADING_TYPE_ID"];
            DataColumn child = dataset.Tables["BLADING"].Columns["BLADING_TYPE_ID"];
            dataset.Relations.Add("FK_BLADING_BLADING_TYPE", parent, child);
        }
        ///<summary>
        /// FK_BLADING_CONTRACTOR
        ///</summary>
        public static void MakeRelation_FK_BLADING_CONTRACTOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BLADING_CONTRACTOR")) return;
            DataColumn parent = dataset.Tables["CONTRACTOR"].Columns["CONTRACTOR_ID"];
            DataColumn child = dataset.Tables["BLADING"].Columns["CONTRACTOR_ID"];
            dataset.Relations.Add("FK_BLADING_CONTRACTOR", parent, child);
        }
        ///<summary>
        /// FK_BLADING_WAYBILL
        ///</summary>
        public static void MakeRelation_FK_BLADING_WAYBILL(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BLADING_WAYBILL")) return;
            DataColumn parent = dataset.Tables["WAYBILL"].Columns["WAYBILL_ID"];
            DataColumn child = dataset.Tables["BLADING"].Columns["WAYBILL_ID"];
            dataset.Relations.Add("FK_BLADING_WAYBILL", parent, child);
        }
        ///<summary>
        /// FK_BRIGADE_DECADE
        ///</summary>
        public static void MakeRelation_FK_BRIGADE_DECADE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BRIGADE_DECADE")) return;
            DataColumn parent = dataset.Tables["DECADE"].Columns["DECADE_ID"];
            DataColumn child = dataset.Tables["BRIGADE"].Columns["DECADE"];
            dataset.Relations.Add("FK_BRIGADE_DECADE", parent, child);
        }
        ///<summary>
        /// FK_BRIGADE_DRIVER_BRIGADE
        ///</summary>
        public static void MakeRelation_FK_BRIGADE_DRIVER_BRIGADE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BRIGADE_DRIVER_BRIGADE")) return;
            DataColumn parent = dataset.Tables["BRIGADE"].Columns["BRIGADE_ID"];
            DataColumn child = dataset.Tables["BRIGADE_DRIVER"].Columns["BRIGADE"];
            dataset.Relations.Add("FK_BRIGADE_DRIVER_BRIGADE", parent, child);
        }
        ///<summary>
        /// FK_BRIGADE_DRIVER_DRIVER
        ///</summary>
        public static void MakeRelation_FK_BRIGADE_DRIVER_DRIVER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BRIGADE_DRIVER_DRIVER")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["BRIGADE_DRIVER"].Columns["DRIVER"];
            dataset.Relations.Add("FK_BRIGADE_DRIVER_DRIVER", parent, child);
        }
        ///<summary>
        /// FK_BRIGADE_GRAPHIC
        ///</summary>
        public static void MakeRelation_FK_BRIGADE_GRAPHIC(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BRIGADE_GRAPHIC")) return;
            DataColumn parent = dataset.Tables["GRAPHIC"].Columns["GRAPHIC_ID"];
            DataColumn child = dataset.Tables["BRIGADE"].Columns["GRAPHIC"];
            dataset.Relations.Add("FK_BRIGADE_GRAPHIC", parent, child);
        }
        ///<summary>
        /// FK_BRIGADE_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_BRIGADE_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_BRIGADE_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["BRIGADE"].Columns["VEHICLE"];
            dataset.Relations.Add("FK_BRIGADE_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_INFO_CONTROLLER
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_INFO_CONTROLLER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_INFO_CONTROLLER")) return;
            DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_INFO"].Columns["CONTROLLER_ID"];
            dataset.Relations.Add("FK_CONTROLLER_INFO_CONTROLLER", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_STAT_CONTROLLER
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_STAT_CONTROLLER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_STAT_CONTROLLER")) return;
            DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_STAT"].Columns["CONTROLLER_ID"];
            dataset.Relations.Add("FK_CONTROLLER_STAT_CONTROLLER", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensor_ControllerSensorType
        ///</summary>
        public static void MakeRelation_FK_ControllerSensor_ControllerSensorType(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensor_ControllerSensorType")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            dataset.Relations.Add("FK_ControllerSensor_ControllerSensorType", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensor_ControllerType
        ///</summary>
        public static void MakeRelation_FK_ControllerSensor_ControllerType(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensor_ControllerType")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_TYPE"].Columns["CONTROLLER_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_TYPE_ID"];
            dataset.Relations.Add("FK_ControllerSensor_ControllerType", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensorLegend_ControllerSensorType
        ///</summary>
        public static void MakeRelation_FK_ControllerSensorLegend_ControllerSensorType(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensorLegend_ControllerSensorType")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            dataset.Relations.Add("FK_ControllerSensorLegend_ControllerSensorType", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensorMap_Controller
        ///</summary>
        public static void MakeRelation_FK_ControllerSensorMap_Controller(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensorMap_Controller")) return;
            DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_ID"];
            dataset.Relations.Add("FK_ControllerSensorMap_Controller", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensorMap_ControllerSensor
        ///</summary>
        public static void MakeRelation_FK_ControllerSensorMap_ControllerSensor(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensorMap_ControllerSensor")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_SENSOR_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_SENSOR_ID"];
            dataset.Relations.Add("FK_ControllerSensorMap_ControllerSensor", parent, child);
        }
        ///<summary>
        /// FK_ControllerSensorMap_ControllerSensorLegend
        ///</summary>
        public static void MakeRelation_FK_ControllerSensorMap_ControllerSensorLegend(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ControllerSensorMap_ControllerSensorLegend")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Columns["CONTROLLER_SENSOR_LEGEND_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_SENSOR_LEGEND_ID"];
            dataset.Relations.Add("FK_ControllerSensorMap_ControllerSensorLegend", parent, child);
        }
        ///<summary>
        /// FK_DAY_TIME_DAY_TYPE
        ///</summary>
        public static void MakeRelation_FK_DAY_TIME_DAY_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_DAY_TIME_DAY_TYPE")) return;
            DataColumn parent = dataset.Tables["DAY_TYPE"].Columns["DAY_TYPE_ID"];
            DataColumn child = dataset.Tables["DAY_TIME"].Columns["DAY_TYPE_ID"];
            dataset.Relations.Add("FK_DAY_TIME_DAY_TYPE", parent, child);
        }
        ///<summary>
        /// FK_DECADE_DEPARTMENT
        ///</summary>
        public static void MakeRelation_FK_DECADE_DEPARTMENT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_DECADE_DEPARTMENT")) return;
            DataColumn parent = dataset.Tables["DEPARTMENT"].Columns["DEPARTMENT_ID"];
            DataColumn child = dataset.Tables["DECADE"].Columns["DEPARTMENT"];
            dataset.Relations.Add("FK_DECADE_DEPARTMENT", parent, child);
        }
        ///<summary>
        /// FK_DRIVER_DEPARTMENT
        ///</summary>
        public static void MakeRelation_FK_DRIVER_DEPARTMENT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_DRIVER_DEPARTMENT")) return;
            DataColumn parent = dataset.Tables["DEPARTMENT"].Columns["DEPARTMENT_ID"];
            DataColumn child = dataset.Tables["DRIVER"].Columns["DEPARTMENT"];
            dataset.Relations.Add("FK_DRIVER_DEPARTMENT", parent, child);
        }
        ///<summary>
        /// FK_EMAIL_SCHEDULEREVENT_EMAIL
        ///</summary>
        public static void MakeRelation_FK_EMAIL_SCHEDULEREVENT_EMAIL(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_EMAIL_SCHEDULEREVENT_EMAIL")) return;
            DataColumn parent = dataset.Tables["EMAIL"].Columns["EMAIL_ID"];
            DataColumn child = dataset.Tables["EMAIL_SCHEDULEREVENT"].Columns["EMAIL_ID"];
            dataset.Relations.Add("FK_EMAIL_SCHEDULEREVENT_EMAIL", parent, child);
        }
        ///<summary>
        /// FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT
        ///</summary>
        public static void MakeRelation_FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT")) return;
            DataColumn parent = dataset.Tables["SCHEDULEREVENT"].Columns["SCHEDULEREVENT_ID"];
            DataColumn child = dataset.Tables["EMAIL_SCHEDULEREVENT"].Columns["SCHEDULEREVENT_ID"];
            dataset.Relations.Add("FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT", parent, child);
        }
        ///<summary>
        /// FK_EMAIL_SCHEDULERQUEUE_EMAIL
        ///</summary>
        public static void MakeRelation_FK_EMAIL_SCHEDULERQUEUE_EMAIL(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_EMAIL_SCHEDULERQUEUE_EMAIL")) return;
            DataColumn parent = dataset.Tables["EMAIL"].Columns["EMAIL_ID"];
            DataColumn child = dataset.Tables["EMAIL_SCHEDULERQUEUE"].Columns["EMAIL_ID"];
            dataset.Relations.Add("FK_EMAIL_SCHEDULERQUEUE_EMAIL", parent, child);
        }
        ///<summary>
        /// FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE
        ///</summary>
        public static void MakeRelation_FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE")) return;
            DataColumn parent = dataset.Tables["SCHEDULERQUEUE"].Columns["SCHEDULERQUEUE_ID"];
            DataColumn child = dataset.Tables["EMAIL_SCHEDULERQUEUE"].Columns["SCHEDULERQUEUE_ID"];
            dataset.Relations.Add("FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE", parent, child);
        }
        ///<summary>
        /// FK_GEO_SEGMENT_RUNTIME_DAY_TIME
        ///</summary>
        public static void MakeRelation_FK_GEO_SEGMENT_RUNTIME_DAY_TIME(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GEO_SEGMENT_RUNTIME_DAY_TIME")) return;
            DataColumn parent = dataset.Tables["DAY_TIME"].Columns["DAY_TIME_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT_RUNTIME"].Columns["DAY_TIME_ID"];
            dataset.Relations.Add("FK_GEO_SEGMENT_RUNTIME_DAY_TIME", parent, child);
        }
        ///<summary>
        /// FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT
        ///</summary>
        public static void MakeRelation_FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT")) return;
            DataColumn parent = dataset.Tables["GEO_SEGMENT"].Columns["GEO_SEGMENT_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT_RUNTIME"].Columns["GEO_SEGMENT_ID"];
            dataset.Relations.Add("FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT", parent, child);
        }
        ///<summary>
        /// FK_GOODS_GOODS_TYPE
        ///</summary>
        public static void MakeRelation_FK_GOODS_GOODS_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GOODS_GOODS_TYPE")) return;
            DataColumn parent = dataset.Tables["GOODS_TYPE"].Columns["GOODS_TYPE_ID"];
            DataColumn child = dataset.Tables["GOODS"].Columns["GOODS_TYPE_ID"];
            dataset.Relations.Add("FK_GOODS_GOODS_TYPE", parent, child);
        }
        ///<summary>
        /// FK_GOODS_LOGISTIC_ORDER_GOODS
        ///</summary>
        public static void MakeRelation_FK_GOODS_LOGISTIC_ORDER_GOODS(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GOODS_LOGISTIC_ORDER_GOODS")) return;
            DataColumn parent = dataset.Tables["GOODS"].Columns["GOODS_ID"];
            DataColumn child = dataset.Tables["GOODS_LOGISTIC_ORDER"].Columns["GOODS_ID"];
            dataset.Relations.Add("FK_GOODS_LOGISTIC_ORDER_GOODS", parent, child);
        }
        ///<summary>
        /// FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER
        ///</summary>
        public static void MakeRelation_FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER")) return;
            DataColumn parent = dataset.Tables["LOGISTIC_ORDER"].Columns["LOGISTIC_ORDER_ID"];
            DataColumn child = dataset.Tables["GOODS_LOGISTIC_ORDER"].Columns["LOGISTIC_ORDER_ID"];
            dataset.Relations.Add("FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER", parent, child);
        }
        ///<summary>
        /// FK_GOODS_TYPE_VEHICLE_GOODS_TYPE
        ///</summary>
        public static void MakeRelation_FK_GOODS_TYPE_VEHICLE_GOODS_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GOODS_TYPE_VEHICLE_GOODS_TYPE")) return;
            DataColumn parent = dataset.Tables["GOODS_TYPE"].Columns["GOODS_TYPE_ID"];
            DataColumn child = dataset.Tables["GOODS_TYPE_VEHICLE"].Columns["GOODS_TYPE_ID"];
            dataset.Relations.Add("FK_GOODS_TYPE_VEHICLE_GOODS_TYPE", parent, child);
        }
        ///<summary>
        /// FK_GOODS_TYPE_VEHICLE_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_GOODS_TYPE_VEHICLE_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_GOODS_TYPE_VEHICLE_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["GOODS_TYPE_VEHICLE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("FK_GOODS_TYPE_VEHICLE_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_DAY_BRIGADE_DRIVER
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_DAY_BRIGADE_DRIVER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_DAY_BRIGADE_DRIVER")) return;
            DataColumn parent = dataset.Tables["BRIGADE_DRIVER"].Columns["BRIGADE_DRIVER_ID"];
            DataColumn child = dataset.Tables["JOURNAL_DAY"].Columns["BRIGADE_DRIVER"];
            dataset.Relations.Add("FK_JOURNAL_DAY_BRIGADE_DRIVER", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_DAY_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_DAY_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_DAY_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["JOURNAL_DAY"].Columns["VEHICLE"];
            dataset.Relations.Add("FK_JOURNAL_DAY_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_DAY_WAYOUT
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_DAY_WAYOUT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_DAY_WAYOUT")) return;
            DataColumn parent = dataset.Tables["WAYOUT"].Columns["WAYOUT_ID"];
            DataColumn child = dataset.Tables["JOURNAL_DAY"].Columns["WAYOUT"];
            dataset.Relations.Add("FK_JOURNAL_DAY_WAYOUT", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_DRIVER
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_DRIVER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_DRIVER")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["JOURNAL"].Columns["DRIVER"];
            dataset.Relations.Add("FK_JOURNAL_DRIVER", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_DRIVER_DRIVER
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_DRIVER_DRIVER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_DRIVER_DRIVER")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["JOURNAL_DRIVER"].Columns["DRIVER"];
            dataset.Relations.Add("FK_JOURNAL_DRIVER_DRIVER", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["JOURNAL"].Columns["VEHICLE"];
            dataset.Relations.Add("FK_JOURNAL_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_JOURNAL_VEHICLE_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_JOURNAL_VEHICLE_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_JOURNAL_VEHICLE_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["JOURNAL_VEHICLE"].Columns["VEHICLE"];
            dataset.Relations.Add("FK_JOURNAL_VEHICLE_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ADDRESS_CONTRACTOR
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ADDRESS_CONTRACTOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ADDRESS_CONTRACTOR")) return;
            DataColumn parent = dataset.Tables["CONTRACTOR"].Columns["CONTRACTOR_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ADDRESS"].Columns["CONTRACTOR_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ADDRESS_CONTRACTOR", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR")) return;
            DataColumn parent = dataset.Tables["CONTRACTOR_CALENDAR"].Columns["CONTRACTOR_CALENDAR_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ADDRESS"].Columns["CONTRACTOR_CALENDAR_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ADDRESS_GEO_ZONE
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ADDRESS_GEO_ZONE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ADDRESS_GEO_ZONE")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ADDRESS"].Columns["GEOZONE_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ADDRESS_GEO_ZONE", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ORDER_BLADING_TYPE
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ORDER_BLADING_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ORDER_BLADING_TYPE")) return;
            DataColumn parent = dataset.Tables["BLADING_TYPE"].Columns["BLADING_TYPE_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ORDER"].Columns["BLADING_TYPE_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ORDER_BLADING_TYPE", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ORDER_CONTRACTOR
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ORDER_CONTRACTOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ORDER_CONTRACTOR")) return;
            DataColumn parent = dataset.Tables["CONTRACTOR"].Columns["CONTRACTOR_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ORDER"].Columns["CONTRACTOR_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ORDER_CONTRACTOR", parent, child);
        }
        ///<summary>
        /// FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS
        ///</summary>
        public static void MakeRelation_FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS")) return;
            DataColumn parent = dataset.Tables["LOGISTIC_ADDRESS"].Columns["LOGISTIC_ADDRESS_ID"];
            DataColumn child = dataset.Tables["LOGISTIC_ORDER"].Columns["LOGISTIC_ADDRESS_ID"];
            dataset.Relations.Add("FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS", parent, child);
        }
        ///<summary>
        /// FK_OPERATOR_PROFILE_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_OPERATOR_PROFILE_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_OPERATOR_PROFILE_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_PROFILE"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_OPERATOR_PROFILE_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_ORDER_TRIP_JOURNAL_DAY
        ///</summary>
        public static void MakeRelation_FK_ORDER_TRIP_JOURNAL_DAY(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ORDER_TRIP_JOURNAL_DAY")) return;
            DataColumn parent = dataset.Tables["JOURNAL_DAY"].Columns["JOURNAL_DAY_ID"];
            DataColumn child = dataset.Tables["ORDER_TRIP"].Columns["JOURNAL_DAY"];
            dataset.Relations.Add("FK_ORDER_TRIP_JOURNAL_DAY", parent, child);
        }
        ///<summary>
        /// FK_ORDER_TRIP_ORDER
        ///</summary>
        public static void MakeRelation_FK_ORDER_TRIP_ORDER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ORDER_TRIP_ORDER")) return;
            DataColumn parent = dataset.Tables["ORDER"].Columns["ORDER_ID"];
            DataColumn child = dataset.Tables["ORDER_TRIP"].Columns["ORDER"];
            dataset.Relations.Add("FK_ORDER_TRIP_ORDER", parent, child);
        }
        ///<summary>
        /// FK_ROUTE_GEOZONE_GEO_ZONE
        ///</summary>
        public static void MakeRelation_FK_ROUTE_GEOZONE_GEO_ZONE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ROUTE_GEOZONE_GEO_ZONE")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["ROUTE_GEOZONE"].Columns["GEOZONE_ID"];
            dataset.Relations.Add("FK_ROUTE_GEOZONE_GEO_ZONE", parent, child);
        }
        ///<summary>
        /// FK_ROUTE_GEOZONE_ROUTE
        ///</summary>
        public static void MakeRelation_FK_ROUTE_GEOZONE_ROUTE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_ROUTE_GEOZONE_ROUTE")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["ROUTE_GEOZONE"].Columns["ROUTE_ID"];
            dataset.Relations.Add("FK_ROUTE_GEOZONE_ROUTE", parent, child);
        }
        ///<summary>
        /// FK_RS_DAY_TYPE
        ///</summary>
        public static void MakeRelation_FK_RS_DAY_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_DAY_TYPE")) return;
            DataColumn parent = dataset.Tables["DAY_TYPE"].Columns["DAY_TYPE_ID"];
            DataColumn child = dataset.Tables["RS"].Columns["DAY_TYPE_ID"];
            dataset.Relations.Add("FK_RS_DAY_TYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_NUMBER_RS_STEP
        ///</summary>
        public static void MakeRelation_FK_RS_NUMBER_RS_STEP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_NUMBER_RS_STEP")) return;
            DataColumn parent = dataset.Tables["RS_STEP"].Columns["RS_STEP_ID"];
            DataColumn child = dataset.Tables["RS_NUMBER"].Columns["RS_STEP"];
            dataset.Relations.Add("FK_RS_NUMBER_RS_STEP", parent, child);
        }
        ///<summary>
        /// FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE
        ///</summary>
        public static void MakeRelation_FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE")) return;
            DataColumn parent = dataset.Tables["RS_OPERATIONSTYPE"].Columns["RS_OPERATIONSTYPE_ID"];
            DataColumn child = dataset.Tables["RS_OPERATIONTYPE"].Columns["RS_OPERATIONSTYPE_ID"];
            dataset.Relations.Add("FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_PERIOD_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_PERIOD_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_PERIOD_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_PERIOD"].Columns["RS_VARIANT_ID"];
            dataset.Relations.Add("FK_RS_PERIOD_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_POINT_RS_OPERATIONTYPE
        ///</summary>
        public static void MakeRelation_FK_RS_POINT_RS_OPERATIONTYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_POINT_RS_OPERATIONTYPE")) return;
            DataColumn parent = dataset.Tables["RS_OPERATIONTYPE"].Columns["RS_OPERATIONTYPE_ID"];
            DataColumn child = dataset.Tables["RS_POINT"].Columns["RS_OPERATIONTYPE_ID"];
            dataset.Relations.Add("FK_RS_POINT_RS_OPERATIONTYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_POINT_RS_SHIFT
        ///</summary>
        public static void MakeRelation_FK_RS_POINT_RS_SHIFT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_POINT_RS_SHIFT")) return;
            DataColumn parent = dataset.Tables["RS_SHIFT"].Columns["RS_SHIFT_ID"];
            DataColumn child = dataset.Tables["RS_POINT"].Columns["RS_SHIFT_ID"];
            dataset.Relations.Add("FK_RS_POINT_RS_SHIFT", parent, child);
        }
        ///<summary>
        /// FK_RS_POINT_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RS_POINT_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_POINT_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_ROUND_TRIP"].Columns["RS_ROUND_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_POINT"].Columns["RS_ROUND_TRIP_ID"];
            dataset.Relations.Add("FK_RS_POINT_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RS_POINT_RS_TRIPTYPE
        ///</summary>
        public static void MakeRelation_FK_RS_POINT_RS_TRIPTYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_POINT_RS_TRIPTYPE")) return;
            DataColumn parent = dataset.Tables["RS_TRIPTYPE"].Columns["RS_TRIPTYPE_ID"];
            DataColumn child = dataset.Tables["RS_POINT"].Columns["RS_TRIPTYPE_ID"];
            dataset.Relations.Add("FK_RS_POINT_RS_TRIPTYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_POINT_RS_WAYOUT
        ///</summary>
        public static void MakeRelation_FK_RS_POINT_RS_WAYOUT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_POINT_RS_WAYOUT")) return;
            DataColumn parent = dataset.Tables["RS_WAYOUT"].Columns["RS_WAYOUT_ID"];
            DataColumn child = dataset.Tables["RS_POINT"].Columns["RS_WAYOUT_ID"];
            dataset.Relations.Add("FK_RS_POINT_RS_WAYOUT", parent, child);
        }
        ///<summary>
        /// FK_RS_ROUND_TRIP_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_ROUND_TRIP_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_ROUND_TRIP_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_ROUND_TRIP"].Columns["RS_VARIANT_ID"];
            dataset.Relations.Add("FK_RS_ROUND_TRIP_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_ROUTE
        ///</summary>
        public static void MakeRelation_FK_RS_ROUTE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_ROUTE")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["RS"].Columns["ROUTE_ID"];
            dataset.Relations.Add("FK_RS_ROUTE", parent, child);
        }
        ///<summary>
        /// FK_RS_RUNTIME_RS_PERIOD
        ///</summary>
        public static void MakeRelation_FK_RS_RUNTIME_RS_PERIOD(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_RUNTIME_RS_PERIOD")) return;
            DataColumn parent = dataset.Tables["RS_PERIOD"].Columns["PERIOD_ID"];
            DataColumn child = dataset.Tables["RS_RUNTIME"].Columns["PERIOD_ID"];
            dataset.Relations.Add("FK_RS_RUNTIME_RS_PERIOD", parent, child);
        }
        ///<summary>
        /// FK_RS_RUNTIME_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RS_RUNTIME_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_RUNTIME_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_RUNTIME"].Columns["RS_TRIP_ID"];
            dataset.Relations.Add("FK_RS_RUNTIME_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RS_SHIFT_RS_WAYOUT
        ///</summary>
        public static void MakeRelation_FK_RS_SHIFT_RS_WAYOUT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_SHIFT_RS_WAYOUT")) return;
            DataColumn parent = dataset.Tables["RS_WAYOUT"].Columns["RS_WAYOUT_ID"];
            DataColumn child = dataset.Tables["RS_SHIFT"].Columns["RS_WAYOUT_ID"];
            dataset.Relations.Add("FK_RS_SHIFT_RS_WAYOUT", parent, child);
        }
        ///<summary>
        /// FK_RS_STEP_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_STEP_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_STEP_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_STEP"].Columns["RS_VARIANT"];
            dataset.Relations.Add("FK_RS_STEP_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_TRIP_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_TRIP_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_TRIP_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_TRIP"].Columns["RS_VARIANT_ID"];
            dataset.Relations.Add("FK_RS_TRIP_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_TRIP0_ROUTE_TRIP
        ///</summary>
        public static void MakeRelation_FK_RS_TRIP0_ROUTE_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_TRIP0_ROUTE_TRIP")) return;
            DataColumn parent = dataset.Tables["ROUTE_TRIP"].Columns["ROUTE_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_TRIP"].Columns["ROUTE_TRIP_ID"];
            dataset.Relations.Add("FK_RS_TRIP0_ROUTE_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RS_TRIPTYPE_RS_TRIPSTYPE
        ///</summary>
        public static void MakeRelation_FK_RS_TRIPTYPE_RS_TRIPSTYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_TRIPTYPE_RS_TRIPSTYPE")) return;
            DataColumn parent = dataset.Tables["RS_TRIPSTYPE"].Columns["RS_TRIPSTYPE_ID"];
            DataColumn child = dataset.Tables["RS_TRIPTYPE"].Columns["RS_TRIPSTYPE_ID"];
            dataset.Relations.Add("FK_RS_TRIPTYPE_RS_TRIPSTYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_TRIPTYPE_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_TRIPTYPE_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_TRIPTYPE_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_TRIPTYPE"].Columns["RS_VARIANT_ID"];
            dataset.Relations.Add("FK_RS_TRIPTYPE_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_VARIANT_RS
        ///</summary>
        public static void MakeRelation_FK_RS_VARIANT_RS(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_VARIANT_RS")) return;
            DataColumn parent = dataset.Tables["RS"].Columns["RS_ID"];
            DataColumn child = dataset.Tables["RS_VARIANT"].Columns["RS_ID"];
            dataset.Relations.Add("FK_RS_VARIANT_RS", parent, child);
        }
        ///<summary>
        /// FK_RS_VARIANT_RS_TYPE
        ///</summary>
        public static void MakeRelation_FK_RS_VARIANT_RS_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_VARIANT_RS_TYPE")) return;
            DataColumn parent = dataset.Tables["RS_TYPE"].Columns["RS_TYPE_ID"];
            DataColumn child = dataset.Tables["RS_VARIANT"].Columns["RS_TYPE_ID"];
            dataset.Relations.Add("FK_RS_VARIANT_RS_TYPE", parent, child);
        }
        ///<summary>
        /// FK_RS_WAYOUT_GRAPHIC
        ///</summary>
        public static void MakeRelation_FK_RS_WAYOUT_GRAPHIC(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_WAYOUT_GRAPHIC")) return;
            DataColumn parent = dataset.Tables["GRAPHIC"].Columns["GRAPHIC_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["GRAPHIC_ID"];
            dataset.Relations.Add("FK_RS_WAYOUT_GRAPHIC", parent, child);
        }
        ///<summary>
        /// FK_RS_WAYOUT_RS_VARIANT
        ///</summary>
        public static void MakeRelation_FK_RS_WAYOUT_RS_VARIANT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_WAYOUT_RS_VARIANT")) return;
            DataColumn parent = dataset.Tables["RS_VARIANT"].Columns["RS_VARIANT_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["RS_VARIANT_ID"];
            dataset.Relations.Add("FK_RS_WAYOUT_RS_VARIANT", parent, child);
        }
        ///<summary>
        /// FK_RS_WAYOUT_WAYOUT
        ///</summary>
        public static void MakeRelation_FK_RS_WAYOUT_WAYOUT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RS_WAYOUT_WAYOUT")) return;
            DataColumn parent = dataset.Tables["WAYOUT"].Columns["WAYOUT_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["WAYOUT_ID"];
            dataset.Relations.Add("FK_RS_WAYOUT_WAYOUT", parent, child);
        }
        ///<summary>
        /// FK_RSOT_TRIPIN_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSOT_TRIPIN_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSOT_TRIPIN_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_OPERATIONTYPE"].Columns["TRIP_IN"];
            dataset.Relations.Add("FK_RSOT_TRIPIN_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSOT_TRIPIN0_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSOT_TRIPIN0_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSOT_TRIPIN0_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_OPERATIONTYPE"].Columns["TRIP_IN_0"];
            dataset.Relations.Add("FK_RSOT_TRIPIN0_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSOT_TRIPOUT_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSOT_TRIPOUT_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSOT_TRIPOUT_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_OPERATIONTYPE"].Columns["TRIP_OUT"];
            dataset.Relations.Add("FK_RSOT_TRIPOUT_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSOT_TRIPOUT0_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSOT_TRIPOUT0_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSOT_TRIPOUT0_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_OPERATIONTYPE"].Columns["TRIP_OUT_0"];
            dataset.Relations.Add("FK_RSOT_TRIPOUT0_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSSHIFT_TRIPIN_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSSHIFT_TRIPIN_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSSHIFT_TRIPIN_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_SHIFT"].Columns["TRIP_IN"];
            dataset.Relations.Add("FK_RSSHIFT_TRIPIN_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSSHIFT_TRIPIN0_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSSHIFT_TRIPIN0_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSSHIFT_TRIPIN0_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_SHIFT"].Columns["TRIP_IN_0"];
            dataset.Relations.Add("FK_RSSHIFT_TRIPIN0_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSSHIFT_TRIPOUT_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSSHIFT_TRIPOUT_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSSHIFT_TRIPOUT_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_SHIFT"].Columns["TRIP_OUT"];
            dataset.Relations.Add("FK_RSSHIFT_TRIPOUT_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSSHIFT_TRIPOUT0_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSSHIFT_TRIPOUT0_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSSHIFT_TRIPOUT0_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_SHIFT"].Columns["TRIP_OUT_0"];
            dataset.Relations.Add("FK_RSSHIFT_TRIPOUT0_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSTT_TRIPA_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSTT_TRIPA_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSTT_TRIPA_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_TRIPTYPE"].Columns["TRIP_A"];
            dataset.Relations.Add("FK_RSTT_TRIPA_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSTT_TRIPB_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSTT_TRIPB_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSTT_TRIPB_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_TRIPTYPE"].Columns["TRIP_B"];
            dataset.Relations.Add("FK_RSTT_TRIPB_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSW_TRIPIN_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSW_TRIPIN_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSW_TRIPIN_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["TRIP_IN"];
            dataset.Relations.Add("FK_RSW_TRIPIN_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSW_TRIPIN0_RS_TRIP0
        ///</summary>
        public static void MakeRelation_FK_RSW_TRIPIN0_RS_TRIP0(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSW_TRIPIN0_RS_TRIP0")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["TRIP_IN_0"];
            dataset.Relations.Add("FK_RSW_TRIPIN0_RS_TRIP0", parent, child);
        }
        ///<summary>
        /// FK_RSW_TRIPOUT_RS_TRIP
        ///</summary>
        public static void MakeRelation_FK_RSW_TRIPOUT_RS_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSW_TRIPOUT_RS_TRIP")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["TRIP_OUT"];
            dataset.Relations.Add("FK_RSW_TRIPOUT_RS_TRIP", parent, child);
        }
        ///<summary>
        /// FK_RSW_TRIPOUT0_RS_TRIP0
        ///</summary>
        public static void MakeRelation_FK_RSW_TRIPOUT0_RS_TRIP0(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_RSW_TRIPOUT0_RS_TRIP0")) return;
            DataColumn parent = dataset.Tables["RS_TRIP"].Columns["RS_TRIP_ID"];
            DataColumn child = dataset.Tables["RS_WAYOUT"].Columns["TRIP_OUT_0"];
            dataset.Relations.Add("FK_RSW_TRIPOUT0_RS_TRIP0", parent, child);
        }
        ///<summary>
        /// FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL
        ///</summary>
        public static void MakeRelation_FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL")) return;
            DataColumn parent = dataset.Tables["SCHEDULE_DETAIL"].Columns["SCHEDULE_DETAIL_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_DETAIL_INFO"].Columns["SCHEDULE_DETAIL_ID"];
            dataset.Relations.Add("FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL", parent, child);
        }
        ///<summary>
        /// FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE
        ///</summary>
        public static void MakeRelation_FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE")) return;
            DataColumn parent = dataset.Tables["ROUTE_GEOZONE"].Columns["ROUTE_GEOZONE_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_GEOZONE"].Columns["ROUTE_GEOZONE_ID"];
            dataset.Relations.Add("FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE", parent, child);
        }
        ///<summary>
        /// FK_SCHEDULE_GEOZONE_TRIP
        ///</summary>
        public static void MakeRelation_FK_SCHEDULE_GEOZONE_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SCHEDULE_GEOZONE_TRIP")) return;
            DataColumn parent = dataset.Tables["TRIP"].Columns["TRIP_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_GEOZONE"].Columns["TRIP_ID"];
            dataset.Relations.Add("FK_SCHEDULE_GEOZONE_TRIP", parent, child);
        }
        ///<summary>
        /// FK_SMS_LOG_OWNER
        ///</summary>
        public static void MakeRelation_FK_SMS_LOG_OWNER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SMS_LOG_OWNER")) return;
            DataColumn parent = dataset.Tables["OWNER"].Columns["OWNER_ID"];
            DataColumn child = dataset.Tables["SMS_LOG"].Columns["OWNER_ID"];
            dataset.Relations.Add("FK_SMS_LOG_OWNER", parent, child);
        }
        ///<summary>
        /// FK_SMS_LOG_SMS_TYPE
        ///</summary>
        public static void MakeRelation_FK_SMS_LOG_SMS_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SMS_LOG_SMS_TYPE")) return;
            DataColumn parent = dataset.Tables["SMS_TYPE"].Columns["SMS_TYPE_ID"];
            DataColumn child = dataset.Tables["SMS_LOG"].Columns["SMS_TYPE_ID"];
            dataset.Relations.Add("FK_SMS_LOG_SMS_TYPE", parent, child);
        }
        ///<summary>
        /// FK_SMS_LOG_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_SMS_LOG_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SMS_LOG_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["SMS_LOG"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("FK_SMS_LOG_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_VEHICLE_DEPARTMENT
        ///</summary>
        public static void MakeRelation_FK_VEHICLE_DEPARTMENT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_VEHICLE_DEPARTMENT")) return;
            DataColumn parent = dataset.Tables["DEPARTMENT"].Columns["DEPARTMENT_ID"];
            DataColumn child = dataset.Tables["VEHICLE"].Columns["DEPARTMENT"];
            dataset.Relations.Add("FK_VEHICLE_DEPARTMENT", parent, child);
        }
        ///<summary>
        /// FK_VEHICLE_OWNER_OWNER
        ///</summary>
        public static void MakeRelation_FK_VEHICLE_OWNER_OWNER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_VEHICLE_OWNER_OWNER")) return;
            DataColumn parent = dataset.Tables["OWNER"].Columns["OWNER_ID"];
            DataColumn child = dataset.Tables["VEHICLE_OWNER"].Columns["OWNER_ID"];
            dataset.Relations.Add("FK_VEHICLE_OWNER_OWNER", parent, child);
        }
        ///<summary>
        /// FK_VEHICLE_OWNER_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_VEHICLE_OWNER_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_VEHICLE_OWNER_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["VEHICLE_OWNER"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("FK_VEHICLE_OWNER_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_VEHICLE_PICTURE_VEHICLE
        ///</summary>
        public static void MakeRelation_FK_VEHICLE_PICTURE_VEHICLE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_VEHICLE_PICTURE_VEHICLE")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["VEHICLE_PICTURE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("FK_VEHICLE_PICTURE_VEHICLE", parent, child);
        }
        ///<summary>
        /// FK_Wb_Trip_Geo_Trip
        ///</summary>
        public static void MakeRelation_FK_Wb_Trip_Geo_Trip(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_Wb_Trip_Geo_Trip")) return;
            DataColumn parent = dataset.Tables["GEO_TRIP"].Columns["GEO_TRIP_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["GEO_TRIP_ID"];
            dataset.Relations.Add("FK_Wb_Trip_Geo_Trip", parent, child);
        }
        ///<summary>
        /// FK_WB_TRIP_ORDER_TRIP
        ///</summary>
        public static void MakeRelation_FK_WB_TRIP_ORDER_TRIP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_WB_TRIP_ORDER_TRIP")) return;
            DataColumn parent = dataset.Tables["ORDER_TRIP"].Columns["ORDER_TRIP_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["ORDER_TRIP_ID"];
            dataset.Relations.Add("FK_WB_TRIP_ORDER_TRIP", parent, child);
        }
        ///<summary>
        /// FK_WORK_TIME_DRIVER
        ///</summary>
        public static void MakeRelation_FK_WORK_TIME_DRIVER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_WORK_TIME_DRIVER")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["WORK_TIME"].Columns["DRIVER_ID"];
            dataset.Relations.Add("FK_WORK_TIME_DRIVER", parent, child);
        }
        ///<summary>
        /// GeoSegment_Factors
        ///</summary>
        public static void MakeRelation_GeoSegment_Factors(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegment_Factors")) return;
            DataColumn parent = dataset.Tables["FACTORS"].Columns["FACTOR_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT"].Columns["FACTOR_ID"];
            dataset.Relations.Add("GeoSegment_Factors", parent, child);
        }
        ///<summary>
        /// GeoSegment_GeoTrip
        ///</summary>
        public static void MakeRelation_GeoSegment_GeoTrip(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegment_GeoTrip")) return;
            DataColumn parent = dataset.Tables["GEO_TRIP"].Columns["GEO_TRIP_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT"].Columns["GEO_TRIP_ID"];
            dataset.Relations.Add("GeoSegment_GeoTrip", parent, child);
        }
        ///<summary>
        /// GeoSegment_Point
        ///</summary>
        public static void MakeRelation_GeoSegment_Point(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegment_Point")) return;
            DataColumn parent = dataset.Tables["POINT"].Columns["POINT_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT"].Columns["POINT"];
            dataset.Relations.Add("GeoSegment_Point", parent, child);
        }
        ///<summary>
        /// GeoSegment_PointTo
        ///</summary>
        public static void MakeRelation_GeoSegment_PointTo(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegment_PointTo")) return;
            DataColumn parent = dataset.Tables["POINT"].Columns["POINT_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT"].Columns["POINTTO"];
            dataset.Relations.Add("GeoSegment_PointTo", parent, child);
        }
        ///<summary>
        /// GeoSegmentVeretx_GeoSegment
        ///</summary>
        public static void MakeRelation_GeoSegmentVeretx_GeoSegment(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegmentVeretx_GeoSegment")) return;
            DataColumn parent = dataset.Tables["GEO_SEGMENT"].Columns["GEO_SEGMENT_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT_VERTEX"].Columns["GEO_SEGMENT_ID"];
            dataset.Relations.Add("GeoSegmentVeretx_GeoSegment", parent, child);
        }
        ///<summary>
        /// GeoSegmentVeretx_MapVeretx
        ///</summary>
        public static void MakeRelation_GeoSegmentVeretx_MapVeretx(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoSegmentVeretx_MapVeretx")) return;
            DataColumn parent = dataset.Tables["MAP_VERTEX"].Columns["VERTEX_ID"];
            DataColumn child = dataset.Tables["GEO_SEGMENT_VERTEX"].Columns["VERTEX_ID"];
            dataset.Relations.Add("GeoSegmentVeretx_MapVeretx", parent, child);
        }
        ///<summary>
        /// GeoTrip_PointA
        ///</summary>
        public static void MakeRelation_GeoTrip_PointA(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoTrip_PointA")) return;
            DataColumn parent = dataset.Tables["POINT"].Columns["POINT_ID"];
            DataColumn child = dataset.Tables["GEO_TRIP"].Columns["POINTA"];
            dataset.Relations.Add("GeoTrip_PointA", parent, child);
        }
        ///<summary>
        /// GeoTrip_PointB
        ///</summary>
        public static void MakeRelation_GeoTrip_PointB(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoTrip_PointB")) return;
            DataColumn parent = dataset.Tables["POINT"].Columns["POINT_ID"];
            DataColumn child = dataset.Tables["GEO_TRIP"].Columns["POINTB"];
            dataset.Relations.Add("GeoTrip_PointB", parent, child);
        }
        ///<summary>
        /// GeoTrip_Route
        ///</summary>
        public static void MakeRelation_GeoTrip_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoTrip_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["GEO_TRIP"].Columns["ROUTE_ID"];
            dataset.Relations.Add("GeoTrip_Route", parent, child);
        }
        ///<summary>
        /// GeoZonePrimitive_GeoZone
        ///</summary>
        public static void MakeRelation_GeoZonePrimitive_GeoZone(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoZonePrimitive_GeoZone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["GEO_ZONE_PRIMITIVE"].Columns["ZONE_ID"];
            dataset.Relations.Add("GeoZonePrimitive_GeoZone", parent, child);
        }
        ///<summary>
        /// GeoZonePrimitive_ZonePrimitive
        ///</summary>
        public static void MakeRelation_GeoZonePrimitive_ZonePrimitive(DataSet dataset)
        {
            if (dataset.Relations.Contains("GeoZonePrimitive_ZonePrimitive")) return;
            DataColumn parent = dataset.Tables["ZONE_PRIMITIVE"].Columns["PRIMITIVE_ID"];
            DataColumn child = dataset.Tables["GEO_ZONE_PRIMITIVE"].Columns["PRIMITIVE_ID"];
            dataset.Relations.Add("GeoZonePrimitive_ZonePrimitive", parent, child);
        }
        ///<summary>
        /// GraphicShift_Graphic
        ///</summary>
        public static void MakeRelation_GraphicShift_Graphic(DataSet dataset)
        {
            if (dataset.Relations.Contains("GraphicShift_Graphic")) return;
            DataColumn parent = dataset.Tables["GRAPHIC"].Columns["GRAPHIC_ID"];
            DataColumn child = dataset.Tables["GRAPHIC_SHIFT"].Columns["GRAPHIC"];
            dataset.Relations.Add("GraphicShift_Graphic", parent, child);
        }
        ///<summary>
        /// Journal_Driver1
        ///</summary>
        public static void MakeRelation_Journal_Driver1(DataSet dataset)
        {
            if (dataset.Relations.Contains("Journal_Driver1")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["JOURNAL"].Columns["REPLACED_DRIVER"];
            dataset.Relations.Add("Journal_Driver1", parent, child);
        }
        ///<summary>
        /// Journal_Wayout
        ///</summary>
        public static void MakeRelation_Journal_Wayout(DataSet dataset)
        {
            if (dataset.Relations.Contains("Journal_Wayout")) return;
            DataColumn parent = dataset.Tables["WAYOUT"].Columns["WAYOUT_ID"];
            DataColumn child = dataset.Tables["JOURNAL"].Columns["WAYOUT"];
            dataset.Relations.Add("Journal_Wayout", parent, child);
        }
        ///<summary>
        /// LogGuardEvents_GuardEvents
        ///</summary>
        public static void MakeRelation_LogGuardEvents_GuardEvents(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogGuardEvents_GuardEvents")) return;
            DataColumn parent = dataset.Tables["GUARD_EVENTS"].Columns["GUARD_EVENT_ID"];
            DataColumn child = dataset.Tables["LOG_GUARD_EVENTS"].Columns["GUARD_EVENT_ID"];
            dataset.Relations.Add("LogGuardEvents_GuardEvents", parent, child);
        }
        ///<summary>
        /// LogOpsEvents_Operator
        ///</summary>
        public static void MakeRelation_LogOpsEvents_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogOpsEvents_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["LOG_OPS_EVENTS"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("LogOpsEvents_Operator", parent, child);
        }
        ///<summary>
        /// LogOpsEvents_OpsEvent
        ///</summary>
        public static void MakeRelation_LogOpsEvents_OpsEvent(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogOpsEvents_OpsEvent")) return;
            DataColumn parent = dataset.Tables["OPS_EVENT"].Columns["OPS_EVENT_ID"];
            DataColumn child = dataset.Tables["LOG_OPS_EVENTS"].Columns["OPS_EVENT_ID"];
            dataset.Relations.Add("LogOpsEvents_OpsEvent", parent, child);
        }
        ///<summary>
        /// LogVehicleStatus_Operator
        ///</summary>
        public static void MakeRelation_LogVehicleStatus_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogVehicleStatus_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["LOG_VEHICLE_STATUS"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("LogVehicleStatus_Operator", parent, child);
        }
        ///<summary>
        /// LogVehicleStatus_Vehicle
        ///</summary>
        public static void MakeRelation_LogVehicleStatus_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogVehicleStatus_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["LOG_VEHICLE_STATUS"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("LogVehicleStatus_Vehicle", parent, child);
        }
        ///<summary>
        /// LogVehicleStatus_VehicleStatus
        ///</summary>
        public static void MakeRelation_LogVehicleStatus_VehicleStatus(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogVehicleStatus_VehicleStatus")) return;
            DataColumn parent = dataset.Tables["VEHICLE_STATUS"].Columns["VEHICLE_STATUS_ID"];
            DataColumn child = dataset.Tables["LOG_VEHICLE_STATUS"].Columns["VEHICLE_STATUS_ID"];
            dataset.Relations.Add("LogVehicleStatus_VehicleStatus", parent, child);
        }
        ///<summary>
        /// LogWaybillHeaderStatus_Operator
        ///</summary>
        public static void MakeRelation_LogWaybillHeaderStatus_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogWaybillHeaderStatus_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("LogWaybillHeaderStatus_Operator", parent, child);
        }
        ///<summary>
        /// LogWaybillHeaderStatus_Schedule
        ///</summary>
        public static void MakeRelation_LogWaybillHeaderStatus_Schedule(DataSet dataset)
        {
            if (dataset.Relations.Contains("LogWaybillHeaderStatus_Schedule")) return;
            DataColumn parent = dataset.Tables["SCHEDULE"].Columns["SCHEDULE_ID"];
            DataColumn child = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Columns["SCHEDULE_ID"];
            dataset.Relations.Add("LogWaybillHeaderStatus_Schedule", parent, child);
        }
        ///<summary>
        /// MapVeretx_Maps
        ///</summary>
        public static void MakeRelation_MapVeretx_Maps(DataSet dataset)
        {
            if (dataset.Relations.Contains("MapVeretx_Maps")) return;
            DataColumn parent = dataset.Tables["MAPS"].Columns["MAP_ID"];
            DataColumn child = dataset.Tables["MAP_VERTEX"].Columns["MAP_ID"];
            dataset.Relations.Add("MapVeretx_Maps", parent, child);
        }
        ///<summary>
        /// Media_MediaType
        ///</summary>
        public static void MakeRelation_Media_MediaType(DataSet dataset)
        {
            if (dataset.Relations.Contains("Media_MediaType")) return;
            DataColumn parent = dataset.Tables["MEDIA_TYPE"].Columns["ID"];
            DataColumn child = dataset.Tables["MEDIA"].Columns["TYPE"];
            dataset.Relations.Add("Media_MediaType", parent, child);
        }
        ///<summary>
        /// MediaAcceptors_Media
        ///</summary>
        public static void MakeRelation_MediaAcceptors_Media(DataSet dataset)
        {
            if (dataset.Relations.Contains("MediaAcceptors_Media")) return;
            DataColumn parent = dataset.Tables["MEDIA"].Columns["MEDIA_ID"];
            DataColumn child = dataset.Tables["MEDIA_ACCEPTORS"].Columns["MEDIA_ID"];
            dataset.Relations.Add("MediaAcceptors_Media", parent, child);
        }
        ///<summary>
        /// Message_MessageTemplate
        ///</summary>
        public static void MakeRelation_Message_MessageTemplate(DataSet dataset)
        {
            if (dataset.Relations.Contains("Message_MessageTemplate")) return;
            DataColumn parent = dataset.Tables["MESSAGE_TEMPLATE"].Columns["MESSAGE_TEMPLATE_ID"];
            DataColumn child = dataset.Tables["MESSAGE"].Columns["TEMPLATE_ID"];
            dataset.Relations.Add("Message_MessageTemplate", parent, child);
        }
        ///<summary>
        /// MessageField_Message
        ///</summary>
        public static void MakeRelation_MessageField_Message(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageField_Message")) return;
            DataColumn parent = dataset.Tables["MESSAGE"].Columns["MESSAGE_ID"];
            DataColumn child = dataset.Tables["MESSAGE_FIELD"].Columns["MESSAGE_ID"];
            dataset.Relations.Add("MessageField_Message", parent, child);
        }
        ///<summary>
        /// MessageField_MessageTemplateField
        ///</summary>
        public static void MakeRelation_MessageField_MessageTemplateField(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageField_MessageTemplateField")) return;
            DataColumn parent = dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Columns["MESSAGE_TEMPLATE_FIELD_ID"];
            DataColumn child = dataset.Tables["MESSAGE_FIELD"].Columns["MESSAGE_TEMPLATE_FIELD_ID"];
            dataset.Relations.Add("MessageField_MessageTemplateField", parent, child);
        }
        ///<summary>
        /// MessageOperator_Message
        ///</summary>
        public static void MakeRelation_MessageOperator_Message(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageOperator_Message")) return;
            DataColumn parent = dataset.Tables["MESSAGE"].Columns["MESSAGE_ID"];
            DataColumn child = dataset.Tables["MESSAGE_OPERATOR"].Columns["MESSAGE_ID"];
            dataset.Relations.Add("MessageOperator_Message", parent, child);
        }
        ///<summary>
        /// MessageOperator_Operator
        ///</summary>
        public static void MakeRelation_MessageOperator_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageOperator_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["MESSAGE_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("MessageOperator_Operator", parent, child);
        }
        ///<summary>
        /// MessageTemplateField_DotnetType
        ///</summary>
        public static void MakeRelation_MessageTemplateField_DotnetType(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageTemplateField_DotnetType")) return;
            DataColumn parent = dataset.Tables["DOTNET_TYPE"].Columns["DOTNET_TYPE_ID"];
            DataColumn child = dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Columns["DOTNET_TYPE_ID"];
            dataset.Relations.Add("MessageTemplateField_DotnetType", parent, child);
        }
        ///<summary>
        /// MessageTemplateField_MessageTemplate
        ///</summary>
        public static void MakeRelation_MessageTemplateField_MessageTemplate(DataSet dataset)
        {
            if (dataset.Relations.Contains("MessageTemplateField_MessageTemplate")) return;
            DataColumn parent = dataset.Tables["MESSAGE_TEMPLATE"].Columns["MESSAGE_TEMPLATE_ID"];
            DataColumn child = dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Columns["MESSAGE_TEMPLATE_ID"];
            dataset.Relations.Add("MessageTemplateField_MessageTemplate", parent, child);
        }
        ///<summary>
        /// MonitoreeLog_MediaType
        ///</summary>
        public static void MakeRelation_MonitoreeLog_MediaType(DataSet dataset)
        {
            if (dataset.Relations.Contains("MonitoreeLog_MediaType")) return;
            DataColumn parent = dataset.Tables["MEDIA_TYPE"].Columns["ID"];
            DataColumn child = dataset.Tables["MONITOREE_LOG"].Columns["MEDIA"];
            dataset.Relations.Add("MonitoreeLog_MediaType", parent, child);
        }
        ///<summary>
        /// OoperatorDriverGroup_DriverGroup
        ///</summary>
        public static void MakeRelation_OoperatorDriverGroup_DriverGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OoperatorDriverGroup_DriverGroup")) return;
            DataColumn parent = dataset.Tables["DRIVERGROUP"].Columns["DRIVERGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVERGROUP"].Columns["DRIVERGROUP_ID"];
            dataset.Relations.Add("OoperatorDriverGroup_DriverGroup", parent, child);
        }
        ///<summary>
        /// OperatorDepartment_Operator
        ///</summary>
        public static void MakeRelation_OperatorDepartment_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDepartment_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DEPARTMENT"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorDepartment_Operator", parent, child);
        }
        ///<summary>
        /// OperatorDepartment_Right
        ///</summary>
        public static void MakeRelation_OperatorDepartment_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDepartment_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DEPARTMENT"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorDepartment_Right", parent, child);
        }
        ///<summary>
        /// OperatorDriver_Driver
        ///</summary>
        public static void MakeRelation_OperatorDriver_Driver(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDriver_Driver")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVER"].Columns["DRIVER_ID"];
            dataset.Relations.Add("OperatorDriver_Driver", parent, child);
        }
        ///<summary>
        /// OperatorDriver_Operator
        ///</summary>
        public static void MakeRelation_OperatorDriver_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDriver_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVER"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorDriver_Operator", parent, child);
        }
        ///<summary>
        /// OperatorDriver_Right
        ///</summary>
        public static void MakeRelation_OperatorDriver_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDriver_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVER"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorDriver_Right", parent, child);
        }
        ///<summary>
        /// OperatorDriverGroup_Operator
        ///</summary>
        public static void MakeRelation_OperatorDriverGroup_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDriverGroup_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVERGROUP"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorDriverGroup_Operator", parent, child);
        }
        ///<summary>
        /// OperatorDriverGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorDriverGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorDriverGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_DRIVERGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorDriverGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupDepartment_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupDepartment_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDepartment_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupDepartment_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupDepartment_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupDepartment_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDepartment_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupDepartment_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriver_Driver
        ///</summary>
        public static void MakeRelation_OperatorGroupDriver_Driver(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriver_Driver")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVER"].Columns["DRIVER_ID"];
            dataset.Relations.Add("OperatorGroupDriver_Driver", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriver_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupDriver_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriver_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVER"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupDriver_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriver_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupDriver_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriver_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVER"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupDriver_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriverGroup_DriverGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupDriverGroup_DriverGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriverGroup_DriverGroup")) return;
            DataColumn parent = dataset.Tables["DRIVERGROUP"].Columns["DRIVERGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Columns["DRIVERGROUP_ID"];
            dataset.Relations.Add("OperatorGroupDriverGroup_DriverGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriverGroup_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupDriverGroup_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriverGroup_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupDriverGroup_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupDriverGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupDriverGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupDriverGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupDriverGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupOperator_Operator
        ///</summary>
        public static void MakeRelation_OperatorGroupOperator_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupOperator_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorGroupOperator_Operator", parent, child);
        }
        ///<summary>
        /// OperatorGroupOperator_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupOperator_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupOperator_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_OPERATOR"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupOperator_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorgroupReport_Operator
        ///</summary>
        public static void MakeRelation_OperatorgroupReport_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorgroupReport_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_REPORT"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorgroupReport_Operator", parent, child);
        }
        ///<summary>
        /// OperatorgroupReport_Report
        ///</summary>
        public static void MakeRelation_OperatorgroupReport_Report(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorgroupReport_Report")) return;
            DataColumn parent = dataset.Tables["REPORT"].Columns["REPORT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_REPORT"].Columns["REPORT_ID"];
            dataset.Relations.Add("OperatorgroupReport_Report", parent, child);
        }
        ///<summary>
        /// OperatorgroupReport_Right
        ///</summary>
        public static void MakeRelation_OperatorgroupReport_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorgroupReport_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_REPORT"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorgroupReport_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupRoute_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupRoute_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRoute_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTE"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupRoute_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupRoute_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupRoute_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRoute_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupRoute_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupRoute_Route
        ///</summary>
        public static void MakeRelation_OperatorGroupRoute_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRoute_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTE"].Columns["ROUTE_ID"];
            dataset.Relations.Add("OperatorGroupRoute_Route", parent, child);
        }
        ///<summary>
        /// OperatorGroupRouteGroup_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupRouteGroup_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRouteGroup_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupRouteGroup_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupRouteGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupRouteGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRouteGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupRouteGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupRouteGroup_RouteGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupRouteGroup_RouteGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupRouteGroup_RouteGroup")) return;
            DataColumn parent = dataset.Tables["ROUTEGROUP"].Columns["ROUTEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Columns["ROUTEGROUP_ID"];
            dataset.Relations.Add("OperatorGroupRouteGroup_RouteGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicle_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicle_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicle_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLE"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupVehicle_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicle_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicle_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicle_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupVehicle_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicle_Vehicle
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicle_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicle_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("OperatorGroupVehicle_Vehicle", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicleGroup_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicleGroup_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicleGroup_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupVehicleGroup_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicleGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicleGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicleGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupVehicleGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupVehicleGroup_VehicleGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupVehicleGroup_VehicleGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupVehicleGroup_VehicleGroup")) return;
            DataColumn parent = dataset.Tables["VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            dataset.Relations.Add("OperatorGroupVehicleGroup_VehicleGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupZone_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupZone_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZone_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONE"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupZone_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupZone_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupZone_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZone_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupZone_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupZone_Zone
        ///</summary>
        public static void MakeRelation_OperatorGroupZone_Zone(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZone_Zone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONE"].Columns["ZONE_ID"];
            dataset.Relations.Add("OperatorGroupZone_Zone", parent, child);
        }
        ///<summary>
        /// OperatorGroupZoneGroup_OperatorGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupZoneGroup_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZoneGroup_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("OperatorGroupZoneGroup_OperatorGroup", parent, child);
        }
        ///<summary>
        /// OperatorGroupZoneGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorGroupZoneGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZoneGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorGroupZoneGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorGroupZoneGroup_ZoneGroup
        ///</summary>
        public static void MakeRelation_OperatorGroupZoneGroup_ZoneGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorGroupZoneGroup_ZoneGroup")) return;
            DataColumn parent = dataset.Tables["ZONEGROUP"].Columns["ZONEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Columns["ZONEGROUP_ID"];
            dataset.Relations.Add("OperatorGroupZoneGroup_ZoneGroup", parent, child);
        }
        ///<summary>
        /// OperatorReport_Operator
        ///</summary>
        public static void MakeRelation_OperatorReport_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorReport_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_REPORT"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorReport_Operator", parent, child);
        }
        ///<summary>
        /// OperatorReport_Report
        ///</summary>
        public static void MakeRelation_OperatorReport_Report(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorReport_Report")) return;
            DataColumn parent = dataset.Tables["REPORT"].Columns["REPORT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_REPORT"].Columns["REPORT_ID"];
            dataset.Relations.Add("OperatorReport_Report", parent, child);
        }
        ///<summary>
        /// OperatorReport_Right
        ///</summary>
        public static void MakeRelation_OperatorReport_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorReport_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_REPORT"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorReport_Right", parent, child);
        }
        ///<summary>
        /// OperatorRoute_Operator
        ///</summary>
        public static void MakeRelation_OperatorRoute_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRoute_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTE"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorRoute_Operator", parent, child);
        }
        ///<summary>
        /// OperatorRoute_Right
        ///</summary>
        public static void MakeRelation_OperatorRoute_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRoute_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorRoute_Right", parent, child);
        }
        ///<summary>
        /// OperatorRoute_Route
        ///</summary>
        public static void MakeRelation_OperatorRoute_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRoute_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTE"].Columns["ROUTE_ID"];
            dataset.Relations.Add("OperatorRoute_Route", parent, child);
        }
        ///<summary>
        /// OperatorRouteGroup_Operator
        ///</summary>
        public static void MakeRelation_OperatorRouteGroup_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRouteGroup_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTEGROUP"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorRouteGroup_Operator", parent, child);
        }
        ///<summary>
        /// OperatorRouteGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorRouteGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRouteGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorRouteGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorRouteGroup_RouteGroup
        ///</summary>
        public static void MakeRelation_OperatorRouteGroup_RouteGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorRouteGroup_RouteGroup")) return;
            DataColumn parent = dataset.Tables["ROUTEGROUP"].Columns["ROUTEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ROUTEGROUP"].Columns["ROUTEGROUP_ID"];
            dataset.Relations.Add("OperatorRouteGroup_RouteGroup", parent, child);
        }
        ///<summary>
        /// OperatorVehicle_Operator
        ///</summary>
        public static void MakeRelation_OperatorVehicle_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicle_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLE"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorVehicle_Operator", parent, child);
        }
        ///<summary>
        /// OperatorVehicle_Right
        ///</summary>
        public static void MakeRelation_OperatorVehicle_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicle_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorVehicle_Right", parent, child);
        }
        ///<summary>
        /// OperatorVehicle_Vehicle
        ///</summary>
        public static void MakeRelation_OperatorVehicle_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicle_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("OperatorVehicle_Vehicle", parent, child);
        }
        ///<summary>
        /// OperatorVehicleGroup_Operator
        ///</summary>
        public static void MakeRelation_OperatorVehicleGroup_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicleGroup_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLEGROUP"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorVehicleGroup_Operator", parent, child);
        }
        ///<summary>
        /// OperatorVehicleGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorVehicleGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicleGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorVehicleGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorVehicleGroup_VehicleGroup
        ///</summary>
        public static void MakeRelation_OperatorVehicleGroup_VehicleGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorVehicleGroup_VehicleGroup")) return;
            DataColumn parent = dataset.Tables["VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATOR_VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            dataset.Relations.Add("OperatorVehicleGroup_VehicleGroup", parent, child);
        }
        ///<summary>
        /// OperatorZone_Operator
        ///</summary>
        public static void MakeRelation_OperatorZone_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZone_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONE"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorZone_Operator", parent, child);
        }
        ///<summary>
        /// OperatorZone_Right
        ///</summary>
        public static void MakeRelation_OperatorZone_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZone_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONE"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorZone_Right", parent, child);
        }
        ///<summary>
        /// OperatorZone_Zone
        ///</summary>
        public static void MakeRelation_OperatorZone_Zone(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZone_Zone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONE"].Columns["ZONE_ID"];
            dataset.Relations.Add("OperatorZone_Zone", parent, child);
        }
        ///<summary>
        /// OperatorZoneGroup_Operator
        ///</summary>
        public static void MakeRelation_OperatorZoneGroup_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZoneGroup_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONEGROUP"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("OperatorZoneGroup_Operator", parent, child);
        }
        ///<summary>
        /// OperatorZoneGroup_Right
        ///</summary>
        public static void MakeRelation_OperatorZoneGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZoneGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONEGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("OperatorZoneGroup_Right", parent, child);
        }
        ///<summary>
        /// OperatorZoneGroup_ZoneGroup
        ///</summary>
        public static void MakeRelation_OperatorZoneGroup_ZoneGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("OperatorZoneGroup_ZoneGroup")) return;
            DataColumn parent = dataset.Tables["ZONEGROUP"].Columns["ZONEGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATOR_ZONEGROUP"].Columns["ZONEGROUP_ID"];
            dataset.Relations.Add("OperatorZoneGroup_ZoneGroup", parent, child);
        }
        ///<summary>
        /// Order_Customer
        ///</summary>
        public static void MakeRelation_Order_Customer(DataSet dataset)
        {
            if (dataset.Relations.Contains("Order_Customer")) return;
            DataColumn parent = dataset.Tables["CUSTOMER"].Columns["CUSTOMER_ID"];
            DataColumn child = dataset.Tables["ORDER"].Columns["CUSTOMER"];
            dataset.Relations.Add("Order_Customer", parent, child);
        }
        ///<summary>
        /// Order_OrderType
        ///</summary>
        public static void MakeRelation_Order_OrderType(DataSet dataset)
        {
            if (dataset.Relations.Contains("Order_OrderType")) return;
            DataColumn parent = dataset.Tables["ORDER_TYPE"].Columns["ORDER_TYPE_ID"];
            DataColumn child = dataset.Tables["ORDER"].Columns["ORDER_TYPE"];
            dataset.Relations.Add("Order_OrderType", parent, child);
        }
        ///<summary>
        /// Period_DayKind
        ///</summary>
        public static void MakeRelation_Period_DayKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Period_DayKind")) return;
            DataColumn parent = dataset.Tables["DAY_KIND"].Columns["DAY_KIND_ID"];
            DataColumn child = dataset.Tables["PERIOD"].Columns["DAY_KIND"];
            dataset.Relations.Add("Period_DayKind", parent, child);
        }
        ///<summary>
        /// Period_DkSet
        ///</summary>
        public static void MakeRelation_Period_DkSet(DataSet dataset)
        {
            if (dataset.Relations.Contains("Period_DkSet")) return;
            DataColumn parent = dataset.Tables["DK_SET"].Columns["DK_SET_ID"];
            DataColumn child = dataset.Tables["PERIOD"].Columns["DK_SET"];
            dataset.Relations.Add("Period_DkSet", parent, child);
        }
        ///<summary>
        /// Period_Route
        ///</summary>
        public static void MakeRelation_Period_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("Period_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["PERIOD"].Columns["ROUTE"];
            dataset.Relations.Add("Period_Route", parent, child);
        }
        ///<summary>
        /// Period_Season
        ///</summary>
        public static void MakeRelation_Period_Season(DataSet dataset)
        {
            if (dataset.Relations.Contains("Period_Season")) return;
            DataColumn parent = dataset.Tables["SEASON"].Columns["SEASON_ID"];
            DataColumn child = dataset.Tables["PERIOD"].Columns["SEASON_ID"];
            dataset.Relations.Add("Period_Season", parent, child);
        }
        ///<summary>
        /// Point_Busstop
        ///</summary>
        public static void MakeRelation_Point_Busstop(DataSet dataset)
        {
            if (dataset.Relations.Contains("Point_Busstop")) return;
            DataColumn parent = dataset.Tables["BUSSTOP"].Columns["BUSSTOP_ID"];
            DataColumn child = dataset.Tables["POINT"].Columns["BUSSTOP_ID"];
            dataset.Relations.Add("Point_Busstop", parent, child);
        }
        ///<summary>
        /// Point_MapVeretx
        ///</summary>
        public static void MakeRelation_Point_MapVeretx(DataSet dataset)
        {
            if (dataset.Relations.Contains("Point_MapVeretx")) return;
            DataColumn parent = dataset.Tables["MAP_VERTEX"].Columns["VERTEX_ID"];
            DataColumn child = dataset.Tables["POINT"].Columns["VERTEX_ID"];
            dataset.Relations.Add("Point_MapVeretx", parent, child);
        }
        ///<summary>
        /// Point_PointKind
        ///</summary>
        public static void MakeRelation_Point_PointKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Point_PointKind")) return;
            DataColumn parent = dataset.Tables["POINT_KIND"].Columns["POINT_KIND_ID"];
            DataColumn child = dataset.Tables["POINT"].Columns["POINT_KIND_ID"];
            dataset.Relations.Add("Point_PointKind", parent, child);
        }
        ///<summary>
        /// RightOperator_Operator
        ///</summary>
        public static void MakeRelation_RightOperator_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("RightOperator_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["RIGHT_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("RightOperator_Operator", parent, child);
        }
        ///<summary>
        /// RightOperator_Right
        ///</summary>
        public static void MakeRelation_RightOperator_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("RightOperator_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["RIGHT_OPERATOR"].Columns["RIGHT_ID"];
            dataset.Relations.Add("RightOperator_Right", parent, child);
        }
        ///<summary>
        /// RightOperatorGroup_OperatorGroup
        ///</summary>
        public static void MakeRelation_RightOperatorGroup_OperatorGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("RightOperatorGroup_OperatorGroup")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["RIGHT_OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("RightOperatorGroup_OperatorGroup", parent, child);
        }
        ///<summary>
        /// RightOperatorGroup_Right
        ///</summary>
        public static void MakeRelation_RightOperatorGroup_Right(DataSet dataset)
        {
            if (dataset.Relations.Contains("RightOperatorGroup_Right")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["RIGHT_OPERATORGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("RightOperatorGroup_Right", parent, child);
        }
        ///<summary>
        /// RouteGroupRoute_Route
        ///</summary>
        public static void MakeRelation_RouteGroupRoute_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("RouteGroupRoute_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["ROUTEGROUP_ROUTE"].Columns["ROUTE_ID"];
            dataset.Relations.Add("RouteGroupRoute_Route", parent, child);
        }
        ///<summary>
        /// RouteGroupRoute_RouteGroup
        ///</summary>
        public static void MakeRelation_RouteGroupRoute_RouteGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("RouteGroupRoute_RouteGroup")) return;
            DataColumn parent = dataset.Tables["ROUTEGROUP"].Columns["ROUTEGROUP_ID"];
            DataColumn child = dataset.Tables["ROUTEGROUP_ROUTE"].Columns["ROUTEGROUP_ID"];
            dataset.Relations.Add("RouteGroupRoute_RouteGroup", parent, child);
        }
        ///<summary>
        /// RoutePoint_Point
        ///</summary>
        public static void MakeRelation_RoutePoint_Point(DataSet dataset)
        {
            if (dataset.Relations.Contains("RoutePoint_Point")) return;
            DataColumn parent = dataset.Tables["POINT"].Columns["POINT_ID"];
            DataColumn child = dataset.Tables["ROUTE_POINT"].Columns["POINT_ID"];
            dataset.Relations.Add("RoutePoint_Point", parent, child);
        }
        ///<summary>
        /// RoutePoint_Route
        ///</summary>
        public static void MakeRelation_RoutePoint_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("RoutePoint_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["ROUTE_POINT"].Columns["ROUTE_ID"];
            dataset.Relations.Add("RoutePoint_Route", parent, child);
        }
        ///<summary>
        /// RouteTrip_GeoTrip
        ///</summary>
        public static void MakeRelation_RouteTrip_GeoTrip(DataSet dataset)
        {
            if (dataset.Relations.Contains("RouteTrip_GeoTrip")) return;
            DataColumn parent = dataset.Tables["GEO_TRIP"].Columns["GEO_TRIP_ID"];
            DataColumn child = dataset.Tables["ROUTE_TRIP"].Columns["GEO_TRIP_ID"];
            dataset.Relations.Add("RouteTrip_GeoTrip", parent, child);
        }
        ///<summary>
        /// RouteTrip_Route
        ///</summary>
        public static void MakeRelation_RouteTrip_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("RouteTrip_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["ROUTE_TRIP"].Columns["ROUTE_ID"];
            dataset.Relations.Add("RouteTrip_Route", parent, child);
        }
        ///<summary>
        /// Schedule_DayKind
        ///</summary>
        public static void MakeRelation_Schedule_DayKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Schedule_DayKind")) return;
            DataColumn parent = dataset.Tables["DAY_KIND"].Columns["DAY_KIND_ID"];
            DataColumn child = dataset.Tables["SCHEDULE"].Columns["DAY_KIND_ID"];
            dataset.Relations.Add("Schedule_DayKind", parent, child);
        }
        ///<summary>
        /// Schedule_Route
        ///</summary>
        public static void MakeRelation_Schedule_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("Schedule_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["SCHEDULE"].Columns["ROUTE_ID"];
            dataset.Relations.Add("Schedule_Route", parent, child);
        }
        ///<summary>
        /// Schedule_Wayout
        ///</summary>
        public static void MakeRelation_Schedule_Wayout(DataSet dataset)
        {
            if (dataset.Relations.Contains("Schedule_Wayout")) return;
            DataColumn parent = dataset.Tables["WAYOUT"].Columns["WAYOUT_ID"];
            DataColumn child = dataset.Tables["SCHEDULE"].Columns["WAYOUT"];
            dataset.Relations.Add("Schedule_Wayout", parent, child);
        }
        ///<summary>
        /// ScheduleDetail_Schedule
        ///</summary>
        public static void MakeRelation_ScheduleDetail_Schedule(DataSet dataset)
        {
            if (dataset.Relations.Contains("ScheduleDetail_Schedule")) return;
            DataColumn parent = dataset.Tables["SCHEDULE"].Columns["SCHEDULE_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_DETAIL"].Columns["SCHEDULE_ID"];
            dataset.Relations.Add("ScheduleDetail_Schedule", parent, child);
        }
        ///<summary>
        /// SchedulePassage_SchedulePoint
        ///</summary>
        public static void MakeRelation_SchedulePassage_SchedulePoint(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulePassage_SchedulePoint")) return;
            DataColumn parent = dataset.Tables["SCHEDULE_POINT"].Columns["SCHEDULE_POINT_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_PASSAGE"].Columns["SP_ID"];
            dataset.Relations.Add("SchedulePassage_SchedulePoint", parent, child);
        }
        ///<summary>
        /// SchedulePassage_WaybillHeader
        ///</summary>
        public static void MakeRelation_SchedulePassage_WaybillHeader(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulePassage_WaybillHeader")) return;
            DataColumn parent = dataset.Tables["WAYBILL_HEADER"].Columns["WAYBILL_HEADER_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_PASSAGE"].Columns["WH_ID"];
            dataset.Relations.Add("SchedulePassage_WaybillHeader", parent, child);
        }
        ///<summary>
        /// SchedulePoint_GeoSegment
        ///</summary>
        public static void MakeRelation_SchedulePoint_GeoSegment(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulePoint_GeoSegment")) return;
            DataColumn parent = dataset.Tables["GEO_SEGMENT"].Columns["GEO_SEGMENT_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_POINT"].Columns["GEO_SEGMENT_ID"];
            dataset.Relations.Add("SchedulePoint_GeoSegment", parent, child);
        }
        ///<summary>
        /// SchedulePoint_RoutePoint
        ///</summary>
        public static void MakeRelation_SchedulePoint_RoutePoint(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulePoint_RoutePoint")) return;
            DataColumn parent = dataset.Tables["ROUTE_POINT"].Columns["ROUTE_POINT_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_POINT"].Columns["ROUTE_POINT_ID"];
            dataset.Relations.Add("SchedulePoint_RoutePoint", parent, child);
        }
        ///<summary>
        /// SchedulePoint_Trip
        ///</summary>
        public static void MakeRelation_SchedulePoint_Trip(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulePoint_Trip")) return;
            DataColumn parent = dataset.Tables["TRIP"].Columns["TRIP_ID"];
            DataColumn child = dataset.Tables["SCHEDULE_POINT"].Columns["TRIP_ID"];
            dataset.Relations.Add("SchedulePoint_Trip", parent, child);
        }
        ///<summary>
        /// SchedulerQueue_SchedulerEvent
        ///</summary>
        public static void MakeRelation_SchedulerQueue_SchedulerEvent(DataSet dataset)
        {
            if (dataset.Relations.Contains("SchedulerQueue_SchedulerEvent")) return;
            DataColumn parent = dataset.Tables["SCHEDULEREVENT"].Columns["SCHEDULEREVENT_ID"];
            DataColumn child = dataset.Tables["SCHEDULERQUEUE"].Columns["SCHEDULEREVENT_ID"];
            dataset.Relations.Add("SchedulerQueue_SchedulerEvent", parent, child);
        }
        ///<summary>
        /// Trail_Session
        ///</summary>
        public static void MakeRelation_Trail_Session(DataSet dataset)
        {
            if (dataset.Relations.Contains("Trail_Session")) return;
            DataColumn parent = dataset.Tables["SESSION"].Columns["SESSION_ID"];
            DataColumn child = dataset.Tables["TRAIL"].Columns["SESSION_ID"];
            dataset.Relations.Add("Trail_Session", parent, child);
        }
        ///<summary>
        /// Trip_RouteTrip
        ///</summary>
        public static void MakeRelation_Trip_RouteTrip(DataSet dataset)
        {
            if (dataset.Relations.Contains("Trip_RouteTrip")) return;
            DataColumn parent = dataset.Tables["ROUTE_TRIP"].Columns["ROUTE_TRIP_ID"];
            DataColumn child = dataset.Tables["TRIP"].Columns["ROUTE_TRIP_ID"];
            dataset.Relations.Add("Trip_RouteTrip", parent, child);
        }
        ///<summary>
        /// Trip_ScheduleDetail
        ///</summary>
        public static void MakeRelation_Trip_ScheduleDetail(DataSet dataset)
        {
            if (dataset.Relations.Contains("Trip_ScheduleDetail")) return;
            DataColumn parent = dataset.Tables["SCHEDULE_DETAIL"].Columns["SCHEDULE_DETAIL_ID"];
            DataColumn child = dataset.Tables["TRIP"].Columns["SCHEDULE_DETAIL_ID"];
            dataset.Relations.Add("Trip_ScheduleDetail", parent, child);
        }
        ///<summary>
        /// Trip_TripKind
        ///</summary>
        public static void MakeRelation_Trip_TripKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Trip_TripKind")) return;
            DataColumn parent = dataset.Tables["TRIP_KIND"].Columns["TRIP_KIND_ID"];
            DataColumn child = dataset.Tables["TRIP"].Columns["TRIP_KIND_ID"];
            dataset.Relations.Add("Trip_TripKind", parent, child);
        }
        ///<summary>
        /// TripKind_WorktimeStatusType
        ///</summary>
        public static void MakeRelation_TripKind_WorktimeStatusType(DataSet dataset)
        {
            if (dataset.Relations.Contains("TripKind_WorktimeStatusType")) return;
            DataColumn parent = dataset.Tables["WORKTIME_STATUS_TYPE"].Columns["WORKTIME_STATUS_TYPE_ID"];
            DataColumn child = dataset.Tables["TRIP_KIND"].Columns["WORKTIME_STATUS_TYPE_ID"];
            dataset.Relations.Add("TripKind_WorktimeStatusType", parent, child);
        }
        ///<summary>
        /// Vehicle_Graphic
        ///</summary>
        public static void MakeRelation_Vehicle_Graphic(DataSet dataset)
        {
            if (dataset.Relations.Contains("Vehicle_Graphic")) return;
            DataColumn parent = dataset.Tables["GRAPHIC"].Columns["GRAPHIC_ID"];
            DataColumn child = dataset.Tables["VEHICLE"].Columns["GRAPHIC"];
            dataset.Relations.Add("Vehicle_Graphic", parent, child);
        }
        ///<summary>
        /// Vehicle_VehicleKind
        ///</summary>
        public static void MakeRelation_Vehicle_VehicleKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("Vehicle_VehicleKind")) return;
            DataColumn parent = dataset.Tables["VEHICLE_KIND"].Columns["VEHICLE_KIND_ID"];
            DataColumn child = dataset.Tables["VEHICLE"].Columns["VEHICLE_KIND_ID"];
            dataset.Relations.Add("Vehicle_VehicleKind", parent, child);
        }
        ///<summary>
        /// Vehicle_VehicleStatus
        ///</summary>
        public static void MakeRelation_Vehicle_VehicleStatus(DataSet dataset)
        {
            if (dataset.Relations.Contains("Vehicle_VehicleStatus")) return;
            DataColumn parent = dataset.Tables["VEHICLE_STATUS"].Columns["VEHICLE_STATUS_ID"];
            DataColumn child = dataset.Tables["VEHICLE"].Columns["VEHICLE_STATUS_ID"];
            dataset.Relations.Add("Vehicle_VehicleStatus", parent, child);
        }
        ///<summary>
        /// VehiclegroupRule_Rule
        ///</summary>
        public static void MakeRelation_VehiclegroupRule_Rule(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehiclegroupRule_Rule")) return;
            DataColumn parent = dataset.Tables["RULE"].Columns["RULE_ID"];
            DataColumn child = dataset.Tables["VEHICLEGROUP_RULE"].Columns["RULE_ID"];
            dataset.Relations.Add("VehiclegroupRule_Rule", parent, child);
        }
        ///<summary>
        /// VehiclegroupRule_Vehicle
        ///</summary>
        public static void MakeRelation_VehiclegroupRule_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehiclegroupRule_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            DataColumn child = dataset.Tables["VEHICLEGROUP_RULE"].Columns["VEHICLEGROUP_ID"];
            dataset.Relations.Add("VehiclegroupRule_Vehicle", parent, child);
        }
        ///<summary>
        /// VehicleGroupVehicle_Vehicle
        ///</summary>
        public static void MakeRelation_VehicleGroupVehicle_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehicleGroupVehicle_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["VEHICLEGROUP_VEHICLE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("VehicleGroupVehicle_Vehicle", parent, child);
        }
        ///<summary>
        /// VehicleGroupVehicle_VehicleGroup
        ///</summary>
        public static void MakeRelation_VehicleGroupVehicle_VehicleGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehicleGroupVehicle_VehicleGroup")) return;
            DataColumn parent = dataset.Tables["VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            DataColumn child = dataset.Tables["VEHICLEGROUP_VEHICLE"].Columns["VEHICLEGROUP_ID"];
            dataset.Relations.Add("VehicleGroupVehicle_VehicleGroup", parent, child);
        }
        ///<summary>
        /// VehicleRule_Rule
        ///</summary>
        public static void MakeRelation_VehicleRule_Rule(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehicleRule_Rule")) return;
            DataColumn parent = dataset.Tables["RULE"].Columns["RULE_ID"];
            DataColumn child = dataset.Tables["VEHICLE_RULE"].Columns["RULE_ID"];
            dataset.Relations.Add("VehicleRule_Rule", parent, child);
        }
        ///<summary>
        /// VehicleRule_Vehicle
        ///</summary>
        public static void MakeRelation_VehicleRule_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("VehicleRule_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["VEHICLE_RULE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("VehicleRule_Vehicle", parent, child);
        }
        ///<summary>
        /// Waybill_Driver
        ///</summary>
        public static void MakeRelation_Waybill_Driver(DataSet dataset)
        {
            if (dataset.Relations.Contains("Waybill_Driver")) return;
            DataColumn parent = dataset.Tables["DRIVER"].Columns["DRIVER_ID"];
            DataColumn child = dataset.Tables["WAYBILL"].Columns["DRIVER_ID"];
            dataset.Relations.Add("Waybill_Driver", parent, child);
        }
        ///<summary>
        /// WaybillHeader_Schedule
        ///</summary>
        public static void MakeRelation_WaybillHeader_Schedule(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillHeader_Schedule")) return;
            DataColumn parent = dataset.Tables["SCHEDULE"].Columns["SCHEDULE_ID"];
            DataColumn child = dataset.Tables["WAYBILL_HEADER"].Columns["SCHEDULE_ID"];
            dataset.Relations.Add("WaybillHeader_Schedule", parent, child);
        }
        ///<summary>
        /// WaybillHeader_Trailor1
        ///</summary>
        public static void MakeRelation_WaybillHeader_Trailor1(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillHeader_Trailor1")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["WAYBILL_HEADER"].Columns["TRAILOR1"];
            dataset.Relations.Add("WaybillHeader_Trailor1", parent, child);
        }
        ///<summary>
        /// WaybillHeader_Trailor2
        ///</summary>
        public static void MakeRelation_WaybillHeader_Trailor2(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillHeader_Trailor2")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["WAYBILL_HEADER"].Columns["TRAILOR2"];
            dataset.Relations.Add("WaybillHeader_Trailor2", parent, child);
        }
        ///<summary>
        /// WaybillHeader_TripKind
        ///</summary>
        public static void MakeRelation_WaybillHeader_TripKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillHeader_TripKind")) return;
            DataColumn parent = dataset.Tables["TRIP_KIND"].Columns["TRIP_KIND_ID"];
            DataColumn child = dataset.Tables["WAYBILL_HEADER"].Columns["INITIAL_TRIP_KIND"];
            dataset.Relations.Add("WaybillHeader_TripKind", parent, child);
        }
        ///<summary>
        /// WaybillHeader_Vehicle
        ///</summary>
        public static void MakeRelation_WaybillHeader_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillHeader_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["WAYBILL_HEADER"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("WaybillHeader_Vehicle", parent, child);
        }
        ///<summary>
        /// Waybillheader_Waybillmark_Waybillmark
        ///</summary>
        public static void MakeRelation_Waybillheader_Waybillmark_Waybillmark(DataSet dataset)
        {
            if (dataset.Relations.Contains("Waybillheader_Waybillmark_Waybillmark")) return;
            DataColumn parent = dataset.Tables["WAYBILLMARK"].Columns["WAYBILLMARK_ID"];
            DataColumn child = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Columns["WAYBILLMARK_ID"];
            dataset.Relations.Add("Waybillheader_Waybillmark_Waybillmark", parent, child);
        }
        ///<summary>
        /// WaybillheaderWaybillmark_WaybillHeader
        ///</summary>
        public static void MakeRelation_WaybillheaderWaybillmark_WaybillHeader(DataSet dataset)
        {
            if (dataset.Relations.Contains("WaybillheaderWaybillmark_WaybillHeader")) return;
            DataColumn parent = dataset.Tables["WAYBILL_HEADER"].Columns["WAYBILL_HEADER_ID"];
            DataColumn child = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Columns["WAYBILLHEADER_ID"];
            dataset.Relations.Add("WaybillheaderWaybillmark_WaybillHeader", parent, child);
        }
        ///<summary>
        /// Wayout_Graphic
        ///</summary>
        public static void MakeRelation_Wayout_Graphic(DataSet dataset)
        {
            if (dataset.Relations.Contains("Wayout_Graphic")) return;
            DataColumn parent = dataset.Tables["GRAPHIC"].Columns["GRAPHIC_ID"];
            DataColumn child = dataset.Tables["WAYOUT"].Columns["graphic"];
            dataset.Relations.Add("Wayout_Graphic", parent, child);
        }
        ///<summary>
        /// Wayout_Route
        ///</summary>
        public static void MakeRelation_Wayout_Route(DataSet dataset)
        {
            if (dataset.Relations.Contains("Wayout_Route")) return;
            DataColumn parent = dataset.Tables["ROUTE"].Columns["ROUTE_ID"];
            DataColumn child = dataset.Tables["WAYOUT"].Columns["ROUTE"];
            dataset.Relations.Add("Wayout_Route", parent, child);
        }
        ///<summary>
        /// WbTrip_Trip
        ///</summary>
        public static void MakeRelation_WbTrip_Trip(DataSet dataset)
        {
            if (dataset.Relations.Contains("WbTrip_Trip")) return;
            DataColumn parent = dataset.Tables["TRIP"].Columns["TRIP_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["TRIP_ID"];
            dataset.Relations.Add("WbTrip_Trip", parent, child);
        }
        ///<summary>
        /// WbTrip_TripKind
        ///</summary>
        public static void MakeRelation_WbTrip_TripKind(DataSet dataset)
        {
            if (dataset.Relations.Contains("WbTrip_TripKind")) return;
            DataColumn parent = dataset.Tables["TRIP_KIND"].Columns["TRIP_KIND_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["TRIP_KIND_ID"];
            dataset.Relations.Add("WbTrip_TripKind", parent, child);
        }
        ///<summary>
        /// WbTrip_Waybill
        ///</summary>
        public static void MakeRelation_WbTrip_Waybill(DataSet dataset)
        {
            if (dataset.Relations.Contains("WbTrip_Waybill")) return;
            DataColumn parent = dataset.Tables["WAYBILL"].Columns["WAYBILL_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["WAYBILL_ID"];
            dataset.Relations.Add("WbTrip_Waybill", parent, child);
        }
        ///<summary>
        /// WbTrip_WaybillHeader
        ///</summary>
        public static void MakeRelation_WbTrip_WaybillHeader(DataSet dataset)
        {
            if (dataset.Relations.Contains("WbTrip_WaybillHeader")) return;
            DataColumn parent = dataset.Tables["WAYBILL_HEADER"].Columns["WAYBILL_HEADER_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["WAYBILL_HEADER_ID"];
            dataset.Relations.Add("WbTrip_WaybillHeader", parent, child);
        }
        ///<summary>
        /// WbTrip_WorktimeReason
        ///</summary>
        public static void MakeRelation_WbTrip_WorktimeReason(DataSet dataset)
        {
            if (dataset.Relations.Contains("WbTrip_WorktimeReason")) return;
            DataColumn parent = dataset.Tables["WORKTIME_REASON"].Columns["WORKTIME_REASON_ID"];
            DataColumn child = dataset.Tables["WB_TRIP"].Columns["WORKTIME_REASON_ID"];
            dataset.Relations.Add("WbTrip_WorktimeReason", parent, child);
        }
        ///<summary>
        /// WebLine_Operator
        ///</summary>
        public static void MakeRelation_WebLine_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebLine_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["WEB_LINE"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("WebLine_Operator", parent, child);
        }
        ///<summary>
        /// WebLineVertex_Vertex
        ///</summary>
        public static void MakeRelation_WebLineVertex_Vertex(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebLineVertex_Vertex")) return;
            DataColumn parent = dataset.Tables["MAP_VERTEX"].Columns["VERTEX_ID"];
            DataColumn child = dataset.Tables["WEB_LINE_VERTEX"].Columns["VERTEX_ID"];
            dataset.Relations.Add("WebLineVertex_Vertex", parent, child);
        }
        ///<summary>
        /// WebLineVertex_WebLine
        ///</summary>
        public static void MakeRelation_WebLineVertex_WebLine(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebLineVertex_WebLine")) return;
            DataColumn parent = dataset.Tables["WEB_LINE"].Columns["LINE_ID"];
            DataColumn child = dataset.Tables["WEB_LINE_VERTEX"].Columns["LINE_ID"];
            dataset.Relations.Add("WebLineVertex_WebLine", parent, child);
        }
        ///<summary>
        /// WebPoint_Operator
        ///</summary>
        public static void MakeRelation_WebPoint_Operator(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebPoint_Operator")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["WEB_POINT"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("WebPoint_Operator", parent, child);
        }
        ///<summary>
        /// WebPoint_Type
        ///</summary>
        public static void MakeRelation_WebPoint_Type(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebPoint_Type")) return;
            DataColumn parent = dataset.Tables["WEB_POINT_TYPE"].Columns["TYPE_ID"];
            DataColumn child = dataset.Tables["WEB_POINT"].Columns["TYPE_ID"];
            dataset.Relations.Add("WebPoint_Type", parent, child);
        }
        ///<summary>
        /// WebPoint_Vertex
        ///</summary>
        public static void MakeRelation_WebPoint_Vertex(DataSet dataset)
        {
            if (dataset.Relations.Contains("WebPoint_Vertex")) return;
            DataColumn parent = dataset.Tables["MAP_VERTEX"].Columns["VERTEX_ID"];
            DataColumn child = dataset.Tables["WEB_POINT"].Columns["VERTEX_ID"];
            dataset.Relations.Add("WebPoint_Vertex", parent, child);
        }
        ///<summary>
        /// WorktimeReason_WorktimeStatusType
        ///</summary>
        public static void MakeRelation_WorktimeReason_WorktimeStatusType(DataSet dataset)
        {
            if (dataset.Relations.Contains("WorktimeReason_WorktimeStatusType")) return;
            DataColumn parent = dataset.Tables["WORKTIME_STATUS_TYPE"].Columns["WORKTIME_STATUS_TYPE_ID"];
            DataColumn child = dataset.Tables["WORKTIME_REASON"].Columns["WORKTIME_STATUS_TYPE_ID"];
            dataset.Relations.Add("WorktimeReason_WorktimeStatusType", parent, child);
        }
        ///<summary>
        /// ZoneGroupZone_Zone
        ///</summary>
        public static void MakeRelation_ZoneGroupZone_Zone(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneGroupZone_Zone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["ZONEGROUP_ZONE"].Columns["ZONE_ID"];
            dataset.Relations.Add("ZoneGroupZone_Zone", parent, child);
        }
        ///<summary>
        /// ZoneGroupZone_ZoneGroup
        ///</summary>
        public static void MakeRelation_ZoneGroupZone_ZoneGroup(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneGroupZone_ZoneGroup")) return;
            DataColumn parent = dataset.Tables["ZONEGROUP"].Columns["ZONEGROUP_ID"];
            DataColumn child = dataset.Tables["ZONEGROUP_ZONE"].Columns["ZONEGROUP_ID"];
            dataset.Relations.Add("ZoneGroupZone_ZoneGroup", parent, child);
        }
        ///<summary>
        /// ZonePrimitive_ZoneType
        ///</summary>
        public static void MakeRelation_ZonePrimitive_ZoneType(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZonePrimitive_ZoneType")) return;
            DataColumn parent = dataset.Tables["ZONE_TYPE"].Columns["ZONE_TYPE_ID"];
            DataColumn child = dataset.Tables["ZONE_PRIMITIVE"].Columns["PRIMITIVE_TYPE"];
            dataset.Relations.Add("ZonePrimitive_ZoneType", parent, child);
        }
        ///<summary>
        /// ZonePrimitiveVertex_MapVeretx
        ///</summary>
        public static void MakeRelation_ZonePrimitiveVertex_MapVeretx(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZonePrimitiveVertex_MapVeretx")) return;
            DataColumn parent = dataset.Tables["MAP_VERTEX"].Columns["VERTEX_ID"];
            DataColumn child = dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Columns["VERTEX_ID"];
            dataset.Relations.Add("ZonePrimitiveVertex_MapVeretx", parent, child);
        }
        ///<summary>
        /// ZonePrimitiveVertex_ZonePrimitive
        ///</summary>
        public static void MakeRelation_ZonePrimitiveVertex_ZonePrimitive(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZonePrimitiveVertex_ZonePrimitive")) return;
            DataColumn parent = dataset.Tables["ZONE_PRIMITIVE"].Columns["PRIMITIVE_ID"];
            DataColumn child = dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Columns["PRIMITIVE_ID"];
            dataset.Relations.Add("ZonePrimitiveVertex_ZonePrimitive", parent, child);
        }
        ///<summary>
        /// ZoneVehicle_Vehicle
        ///</summary>
        public static void MakeRelation_ZoneVehicle_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneVehicle_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
            DataColumn child = dataset.Tables["ZONE_VEHICLE"].Columns["VEHICLE_ID"];
            dataset.Relations.Add("ZoneVehicle_Vehicle", parent, child);
        }
        ///<summary>
        /// ZoneVehicle_Zone
        ///</summary>
        public static void MakeRelation_ZoneVehicle_Zone(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneVehicle_Zone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["ZONE_VEHICLE"].Columns["ZONE_ID"];
            dataset.Relations.Add("ZoneVehicle_Zone", parent, child);
        }
        ///<summary>
        /// ZoneVehicleGroup_Vehicle
        ///</summary>
        public static void MakeRelation_ZoneVehicleGroup_Vehicle(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneVehicleGroup_Vehicle")) return;
            DataColumn parent = dataset.Tables["VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            DataColumn child = dataset.Tables["ZONE_VEHICLEGROUP"].Columns["VEHICLEGROUP_ID"];
            dataset.Relations.Add("ZoneVehicleGroup_Vehicle", parent, child);
        }
        ///<summary>
        /// ZoneVehicleGroup_Zone
        ///</summary>
        public static void MakeRelation_ZoneVehicleGroup_Zone(DataSet dataset)
        {
            if (dataset.Relations.Contains("ZoneVehicleGroup_Zone")) return;
            DataColumn parent = dataset.Tables["GEO_ZONE"].Columns["ZONE_ID"];
            DataColumn child = dataset.Tables["ZONE_VEHICLEGROUP"].Columns["ZONE_ID"];
            dataset.Relations.Add("ZoneVehicleGroup_Zone", parent, child);
        }
        
        public static void MakeSchema(DataSet dataset)
        {
            if (dataset.Tables.Contains("BLADING"))
            {
                MakePK_Blading(dataset);
                MakeIdentities_Blading(dataset);
            }
            if (dataset.Tables.Contains("BLADING_TYPE"))
            {
                MakePK_BladingType(dataset);
                MakeIdentities_BladingType(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE"))
            {
                MakePK_Brigade(dataset);
                MakeIdentities_Brigade(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE_DRIVER"))
            {
                MakePK_BrigadeDriver(dataset);
                MakeIdentities_BrigadeDriver(dataset);
            }
            if (dataset.Tables.Contains("BUSSTOP"))
            {
                MakePK_Busstop(dataset);
                MakeIdentities_Busstop(dataset);
            }
            if (dataset.Tables.Contains("CAL"))
            {
                MakePK_Cal(dataset);
            }
            if (dataset.Tables.Contains("CALENDAR_DAY"))
            {
                MakePK_CalendarDay(dataset);
                MakeIdentities_CalendarDay(dataset);
            }
            if (dataset.Tables.Contains("CONSTANTS"))
            {
                MakePK_Constants(dataset);
            }
            if (dataset.Tables.Contains("CONTRACTOR"))
            {
                MakePK_Contractor(dataset);
                MakeIdentities_Contractor(dataset);
            }
            if (dataset.Tables.Contains("CONTRACTOR_CALENDAR"))
            {
                MakePK_ContractorCalendar(dataset);
                MakeIdentities_ContractorCalendar(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER"))
            {
                MakePK_Controller(dataset);
                MakeIdentities_Controller(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_INFO"))
            {
                MakePK_ControllerInfo(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR"))
            {
                MakePK_ControllerSensor(dataset);
                MakeIdentities_ControllerSensor(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"))
            {
                MakePK_ControllerSensorLegend(dataset);
                MakeIdentities_ControllerSensorLegend(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"))
            {
                MakePK_ControllerSensorMap(dataset);
                MakeIdentities_ControllerSensorMap(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakePK_ControllerSensorType(dataset);
                MakeIdentities_ControllerSensorType(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_TIME"))
            {
                MakePK_ControllerTime(dataset);
                MakeIdentities_ControllerTime(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakePK_ControllerType(dataset);
                MakeIdentities_ControllerType(dataset);
            }
            if (dataset.Tables.Contains("COUNT_GPRS"))
            {
                MakePK_CountGprs(dataset);
            }
            if (dataset.Tables.Contains("COUNT_SMS"))
            {
                MakePK_CountSms(dataset);
            }
            if (dataset.Tables.Contains("CUSTOMER"))
            {
                MakePK_Customer(dataset);
                MakeIdentities_Customer(dataset);
            }
            if (dataset.Tables.Contains("DAY"))
            {
                MakePK_Day(dataset);
                MakeIdentities_Day(dataset);
            }
            if (dataset.Tables.Contains("DAY_KIND"))
            {
                MakePK_DayKind(dataset);
                MakeIdentities_DayKind(dataset);
            }
            if (dataset.Tables.Contains("DAY_TIME"))
            {
                MakePK_DayTime(dataset);
                MakeIdentities_DayTime(dataset);
            }
            if (dataset.Tables.Contains("DAY_TYPE"))
            {
                MakePK_DayType(dataset);
                MakeIdentities_DayType(dataset);
            }
            if (dataset.Tables.Contains("DECADE"))
            {
                MakePK_Decade(dataset);
                MakeIdentities_Decade(dataset);
            }
            if (dataset.Tables.Contains("DEPARTMENT"))
            {
                MakePK_Department(dataset);
                MakeIdentities_Department(dataset);
            }
            if (dataset.Tables.Contains("DK_SET"))
            {
                MakePK_DkSet(dataset);
                MakeIdentities_DkSet(dataset);
            }
            if (dataset.Tables.Contains("DOTNET_TYPE"))
            {
                MakePK_DotnetType(dataset);
                MakeIdentities_DotnetType(dataset);
            }
            if (dataset.Tables.Contains("DRIVER"))
            {
                MakePK_Driver(dataset);
                MakeIdentities_Driver(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_BONUS"))
            {
                MakePK_DriverBonus(dataset);
                MakeIdentities_DriverBonus(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_MSG_TEMPLATE"))
            {
                MakePK_DriverMsgTemplate(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_STATUS"))
            {
                MakePK_DriverStatus(dataset);
                MakeIdentities_DriverStatus(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_VEHICLE"))
            {
                MakePK_DriverVehicle(dataset);
            }
            if (dataset.Tables.Contains("DRIVERGROUP"))
            {
                MakePK_Drivergroup(dataset);
                MakeIdentities_Drivergroup(dataset);
            }
            if (dataset.Tables.Contains("DRIVERGROUP_DRIVER"))
            {
                MakePK_DrivergroupDriver(dataset);
            }
            if (dataset.Tables.Contains("EMAIL"))
            {
                MakePK_Email(dataset);
                MakeIdentities_Email(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULEREVENT"))
            {
                MakePK_EmailSchedulerevent(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE"))
            {
                MakePK_EmailSchedulerqueue(dataset);
            }
            if (dataset.Tables.Contains("FACTOR_VALUES"))
            {
                MakePK_FactorValues(dataset);
                MakeIdentities_FactorValues(dataset);
            }
            if (dataset.Tables.Contains("FACTORS"))
            {
                MakePK_Factors(dataset);
                MakeIdentities_Factors(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT"))
            {
                MakePK_GeoSegment(dataset);
                MakeIdentities_GeoSegment(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_RUNTIME"))
            {
                MakePK_GeoSegmentRuntime(dataset);
                MakeIdentities_GeoSegmentRuntime(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_VERTEX"))
            {
                MakePK_GeoSegmentVertex(dataset);
                MakeIdentities_GeoSegmentVertex(dataset);
            }
            if (dataset.Tables.Contains("GEO_TRIP"))
            {
                MakePK_GeoTrip(dataset);
                MakeIdentities_GeoTrip(dataset);
            }
            if (dataset.Tables.Contains("GEO_ZONE"))
            {
                MakePK_GeoZone(dataset);
                MakeIdentities_GeoZone(dataset);
            }
            if (dataset.Tables.Contains("GEO_ZONE_PRIMITIVE"))
            {
                MakePK_GeoZonePrimitive(dataset);
                MakeIdentities_GeoZonePrimitive(dataset);
            }
            if (dataset.Tables.Contains("GOODS"))
            {
                MakePK_Goods(dataset);
                MakeIdentities_Goods(dataset);
            }
            if (dataset.Tables.Contains("GOODS_LOGISTIC_ORDER"))
            {
                MakePK_GoodsLogisticOrder(dataset);
                MakeIdentities_GoodsLogisticOrder(dataset);
            }
            if (dataset.Tables.Contains("GOODS_TYPE"))
            {
                MakePK_GoodsType(dataset);
                MakeIdentities_GoodsType(dataset);
            }
            if (dataset.Tables.Contains("GOODS_TYPE_VEHICLE"))
            {
                MakePK_GoodsTypeVehicle(dataset);
            }
            if (dataset.Tables.Contains("GRAPHIC"))
            {
                MakePK_Graphic(dataset);
                MakeIdentities_Graphic(dataset);
            }
            if (dataset.Tables.Contains("GRAPHIC_SHIFT"))
            {
                MakePK_GraphicShift(dataset);
                MakeIdentities_GraphicShift(dataset);
            }
            if (dataset.Tables.Contains("GUARD_EVENTS"))
            {
                MakePK_GuardEvents(dataset);
            }
            if (dataset.Tables.Contains("Job_DelBadXY"))
            {
                MakePK_JobDelbadxy(dataset);
                MakeIdentities_JobDelbadxy(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL"))
            {
                MakePK_Journal(dataset);
                MakeIdentities_Journal(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DAY"))
            {
                MakePK_JournalDay(dataset);
                MakeIdentities_JournalDay(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DRIVER"))
            {
                MakePK_JournalDriver(dataset);
                MakeIdentities_JournalDriver(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_VEHICLE"))
            {
                MakePK_JournalVehicle(dataset);
                MakeIdentities_JournalVehicle(dataset);
            }
            if (dataset.Tables.Contains("LOG_OPS_EVENTS"))
            {
                MakePK_LogOpsEvents(dataset);
                MakeIdentities_LogOpsEvents(dataset);
            }
            if (dataset.Tables.Contains("LOG_VEHICLE_STATUS"))
            {
                MakePK_LogVehicleStatus(dataset);
            }
            if (dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS"))
            {
                MakePK_LogWaybillHeaderStatus(dataset);
                MakeIdentities_LogWaybillHeaderStatus(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ADDRESS"))
            {
                MakePK_LogisticAddress(dataset);
                MakeIdentities_LogisticAddress(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ORDER"))
            {
                MakePK_LogisticOrder(dataset);
                MakeIdentities_LogisticOrder(dataset);
            }
            if (dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakePK_MapVertex(dataset);
                MakeIdentities_MapVertex(dataset);
            }
            if (dataset.Tables.Contains("MAPS"))
            {
                MakePK_Maps(dataset);
                MakeIdentities_Maps(dataset);
            }
            if (dataset.Tables.Contains("MEDIA"))
            {
                MakePK_Media(dataset);
                MakeIdentities_Media(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS"))
            {
                MakePK_MediaAcceptors(dataset);
                MakeIdentities_MediaAcceptors(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_TYPE"))
            {
                MakePK_MediaType(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE"))
            {
                MakePK_Message(dataset);
                MakeIdentities_Message(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_BOARD"))
            {
                MakePK_MessageBoard(dataset);
                MakeIdentities_MessageBoard(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_FIELD"))
            {
                MakePK_MessageField(dataset);
                MakeIdentities_MessageField(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_OPERATOR"))
            {
                MakePK_MessageOperator(dataset);
                MakeIdentities_MessageOperator(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE"))
            {
                MakePK_MessageTemplate(dataset);
                MakeIdentities_MessageTemplate(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"))
            {
                MakePK_MessageTemplateField(dataset);
                MakeIdentities_MessageTemplateField(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR"))
            {
                MakePK_Operator(dataset);
                MakeIdentities_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DEPARTMENT"))
            {
                MakePK_OperatorDepartment(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVER"))
            {
                MakePK_OperatorDriver(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP"))
            {
                MakePK_OperatorDrivergroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_PROFILE"))
            {
                MakePK_OperatorProfile(dataset);
                MakeIdentities_OperatorProfile(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_REPORT"))
            {
                MakePK_OperatorReport(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTE"))
            {
                MakePK_OperatorRoute(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP"))
            {
                MakePK_OperatorRoutegroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLE"))
            {
                MakePK_OperatorVehicle(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP"))
            {
                MakePK_OperatorVehiclegroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONE"))
            {
                MakePK_OperatorZone(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONEGROUP"))
            {
                MakePK_OperatorZonegroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakePK_Operatorgroup(dataset);
                MakeIdentities_Operatorgroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT"))
            {
                MakePK_OperatorgroupDepartment(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVER"))
            {
                MakePK_OperatorgroupDriver(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP"))
            {
                MakePK_OperatorgroupDrivergroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR"))
            {
                MakePK_OperatorgroupOperator(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_REPORT"))
            {
                MakePK_OperatorgroupReport(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTE"))
            {
                MakePK_OperatorgroupRoute(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP"))
            {
                MakePK_OperatorgroupRoutegroup(dataset);
                MakeIdentities_OperatorgroupRoutegroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE"))
            {
                MakePK_OperatorgroupVehicle(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP"))
            {
                MakePK_OperatorgroupVehiclegroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONE"))
            {
                MakePK_OperatorgroupZone(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP"))
            {
                MakePK_OperatorgroupZonegroup(dataset);
            }
            if (dataset.Tables.Contains("OPS_EVENT"))
            {
                MakePK_OpsEvent(dataset);
                MakeIdentities_OpsEvent(dataset);
            }
            if (dataset.Tables.Contains("ORDER"))
            {
                MakePK_Order(dataset);
                MakeIdentities_Order(dataset);
            }
            if (dataset.Tables.Contains("ORDER_TRIP"))
            {
                MakePK_OrderTrip(dataset);
                MakeIdentities_OrderTrip(dataset);
            }
            if (dataset.Tables.Contains("ORDER_TYPE"))
            {
                MakePK_OrderType(dataset);
                MakeIdentities_OrderType(dataset);
            }
            if (dataset.Tables.Contains("OWNER"))
            {
                MakePK_Owner(dataset);
                MakeIdentities_Owner(dataset);
            }
            if (dataset.Tables.Contains("PERIOD"))
            {
                MakePK_Period(dataset);
                MakeIdentities_Period(dataset);
            }
            if (dataset.Tables.Contains("POINT"))
            {
                MakePK_Point(dataset);
                MakeIdentities_Point(dataset);
            }
            if (dataset.Tables.Contains("POINT_KIND"))
            {
                MakePK_PointKind(dataset);
            }
            if (dataset.Tables.Contains("REPORT"))
            {
                MakePK_Report(dataset);
                MakeIdentities_Report(dataset);
            }
            if (dataset.Tables.Contains("REPRINT_REASON"))
            {
                MakePK_ReprintReason(dataset);
                MakeIdentities_ReprintReason(dataset);
            }
            if (dataset.Tables.Contains("RIGHT"))
            {
                MakePK_Right(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATOR"))
            {
                MakePK_RightOperator(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATORGROUP"))
            {
                MakePK_RightOperatorgroup(dataset);
            }
            if (dataset.Tables.Contains("ROUTE"))
            {
                MakePK_Route(dataset);
                MakeIdentities_Route(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_GEOZONE"))
            {
                MakePK_RouteGeozone(dataset);
                MakeIdentities_RouteGeozone(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_POINT"))
            {
                MakePK_RoutePoint(dataset);
                MakeIdentities_RoutePoint(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_TRIP"))
            {
                MakePK_RouteTrip(dataset);
                MakeIdentities_RouteTrip(dataset);
            }
            if (dataset.Tables.Contains("ROUTEGROUP"))
            {
                MakePK_Routegroup(dataset);
                MakeIdentities_Routegroup(dataset);
            }
            if (dataset.Tables.Contains("ROUTEGROUP_ROUTE"))
            {
                MakePK_RoutegroupRoute(dataset);
            }
            if (dataset.Tables.Contains("RS"))
            {
                MakePK_Rs(dataset);
                MakeIdentities_Rs(dataset);
            }
            if (dataset.Tables.Contains("RS_NUMBER"))
            {
                MakePK_RsNumber(dataset);
                MakeIdentities_RsNumber(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONSTYPE"))
            {
                MakePK_RsOperationstype(dataset);
                MakeIdentities_RsOperationstype(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE"))
            {
                MakePK_RsOperationtype(dataset);
                MakeIdentities_RsOperationtype(dataset);
            }
            if (dataset.Tables.Contains("RS_PERIOD"))
            {
                MakePK_RsPeriod(dataset);
                MakeIdentities_RsPeriod(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT"))
            {
                MakePK_RsPoint(dataset);
                MakeIdentities_RsPoint(dataset);
            }
            if (dataset.Tables.Contains("RS_ROUND_TRIP"))
            {
                MakePK_RsRoundTrip(dataset);
                MakeIdentities_RsRoundTrip(dataset);
            }
            if (dataset.Tables.Contains("RS_RUNTIME"))
            {
                MakePK_RsRuntime(dataset);
                MakeIdentities_RsRuntime(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT"))
            {
                MakePK_RsShift(dataset);
                MakeIdentities_RsShift(dataset);
            }
            if (dataset.Tables.Contains("RS_STEP"))
            {
                MakePK_RsStep(dataset);
                MakeIdentities_RsStep(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIP"))
            {
                MakePK_RsTrip(dataset);
                MakeIdentities_RsTrip(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPSTYPE"))
            {
                MakePK_RsTripstype(dataset);
                MakeIdentities_RsTripstype(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPTYPE"))
            {
                MakePK_RsTriptype(dataset);
                MakeIdentities_RsTriptype(dataset);
            }
            if (dataset.Tables.Contains("RS_TYPE"))
            {
                MakePK_RsType(dataset);
                MakeIdentities_RsType(dataset);
            }
            if (dataset.Tables.Contains("RS_VARIANT"))
            {
                MakePK_RsVariant(dataset);
                MakeIdentities_RsVariant(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT"))
            {
                MakePK_RsWayout(dataset);
                MakeIdentities_RsWayout(dataset);
            }
            if (dataset.Tables.Contains("RULE"))
            {
                MakePK_Rule(dataset);
                MakeIdentities_Rule(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE"))
            {
                MakePK_Schedule(dataset);
                MakeIdentities_Schedule(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_DETAIL"))
            {
                MakePK_ScheduleDetail(dataset);
                MakeIdentities_ScheduleDetail(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_DETAIL_INFO"))
            {
                MakePK_ScheduleDetailInfo(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_GEOZONE"))
            {
                MakePK_ScheduleGeozone(dataset);
                MakeIdentities_ScheduleGeozone(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_PASSAGE"))
            {
                MakePK_SchedulePassage(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_POINT"))
            {
                MakePK_SchedulePoint(dataset);
                MakeIdentities_SchedulePoint(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULEREVENT"))
            {
                MakePK_Schedulerevent(dataset);
                MakeIdentities_Schedulerevent(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULERQUEUE"))
            {
                MakePK_Schedulerqueue(dataset);
                MakeIdentities_Schedulerqueue(dataset);
            }
            if (dataset.Tables.Contains("SEASON"))
            {
                MakePK_Season(dataset);
                MakeIdentities_Season(dataset);
            }
            if (dataset.Tables.Contains("SEATTYPE"))
            {
                MakePK_Seattype(dataset);
                MakeIdentities_Seattype(dataset);
            }
            if (dataset.Tables.Contains("SESSION"))
            {
                MakePK_Session(dataset);
                MakeIdentities_Session(dataset);
            }
            if (dataset.Tables.Contains("SMS_TYPE"))
            {
                MakePK_SmsType(dataset);
                MakeIdentities_SmsType(dataset);
            }
            if (dataset.Tables.Contains("sysdiagrams"))
            {
                MakePK_Sysdiagrams(dataset);
                MakeIdentities_Sysdiagrams(dataset);
            }
            if (dataset.Tables.Contains("TASK_PROCESSOR_LOG"))
            {
                MakePK_TaskProcessorLog(dataset);
                MakeIdentities_TaskProcessorLog(dataset);
            }
            if (dataset.Tables.Contains("TRAIL"))
            {
                MakePK_Trail(dataset);
                MakeIdentities_Trail(dataset);
            }
            if (dataset.Tables.Contains("TRIP"))
            {
                MakePK_Trip(dataset);
                MakeIdentities_Trip(dataset);
            }
            if (dataset.Tables.Contains("TRIP_KIND"))
            {
                MakePK_TripKind(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE"))
            {
                MakePK_Vehicle(dataset);
                MakeIdentities_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_KIND"))
            {
                MakePK_VehicleKind(dataset);
                MakeIdentities_VehicleKind(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_OWNER"))
            {
                MakePK_VehicleOwner(dataset);
                MakeIdentities_VehicleOwner(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_PICTURE"))
            {
                MakePK_VehiclePicture(dataset);
                MakeIdentities_VehiclePicture(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_RULE"))
            {
                MakePK_VehicleRule(dataset);
                MakeIdentities_VehicleRule(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_STATUS"))
            {
                MakePK_VehicleStatus(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakePK_Vehiclegroup(dataset);
                MakeIdentities_Vehiclegroup(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_RULE"))
            {
                MakePK_VehiclegroupRule(dataset);
                MakeIdentities_VehiclegroupRule(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_VEHICLE"))
            {
                MakePK_VehiclegroupVehicle(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL"))
            {
                MakePK_Waybill(dataset);
                MakeIdentities_Waybill(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER"))
            {
                MakePK_WaybillHeader(dataset);
                MakeIdentities_WaybillHeader(dataset);
            }
            if (dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK"))
            {
                MakePK_WaybillheaderWaybillmark(dataset);
            }
            if (dataset.Tables.Contains("WAYBILLMARK"))
            {
                MakePK_Waybillmark(dataset);
                MakeIdentities_Waybillmark(dataset);
            }
            if (dataset.Tables.Contains("WAYOUT"))
            {
                MakePK_Wayout(dataset);
                MakeIdentities_Wayout(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP"))
            {
                MakePK_WbTrip(dataset);
                MakeIdentities_WbTrip(dataset);
            }
            if (dataset.Tables.Contains("WEB_LINE"))
            {
                MakePK_WebLine(dataset);
                MakeIdentities_WebLine(dataset);
            }
            if (dataset.Tables.Contains("WEB_LINE_VERTEX"))
            {
                MakePK_WebLineVertex(dataset);
                MakeIdentities_WebLineVertex(dataset);
            }
            if (dataset.Tables.Contains("WEB_POINT"))
            {
                MakePK_WebPoint(dataset);
                MakeIdentities_WebPoint(dataset);
            }
            if (dataset.Tables.Contains("WEB_POINT_TYPE"))
            {
                MakePK_WebPointType(dataset);
                MakeIdentities_WebPointType(dataset);
            }
            if (dataset.Tables.Contains("WORK_TIME"))
            {
                MakePK_WorkTime(dataset);
            }
            if (dataset.Tables.Contains("WORKPLACE_CONFIG"))
            {
                MakePK_WorkplaceConfig(dataset);
                MakeIdentities_WorkplaceConfig(dataset);
            }
            if (dataset.Tables.Contains("WORKSTATION"))
            {
                MakePK_Workstation(dataset);
                MakeIdentities_Workstation(dataset);
            }
            if (dataset.Tables.Contains("WORKTIME_REASON"))
            {
                MakePK_WorktimeReason(dataset);
                MakeIdentities_WorktimeReason(dataset);
            }
            if (dataset.Tables.Contains("WORKTIME_STATUS_TYPE"))
            {
                MakePK_WorktimeStatusType(dataset);
                MakeIdentities_WorktimeStatusType(dataset);
            }
            if (dataset.Tables.Contains("ZONE_PRIMITIVE"))
            {
                MakePK_ZonePrimitive(dataset);
                MakeIdentities_ZonePrimitive(dataset);
            }
            if (dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX"))
            {
                MakePK_ZonePrimitiveVertex(dataset);
                MakeIdentities_ZonePrimitiveVertex(dataset);
            }
            if (dataset.Tables.Contains("ZONE_TYPE"))
            {
                MakePK_ZoneType(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLE"))
            {
                MakePK_ZoneVehicle(dataset);
                MakeIdentities_ZoneVehicle(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLEGROUP"))
            {
                MakePK_ZoneVehiclegroup(dataset);
                MakeIdentities_ZoneVehiclegroup(dataset);
            }
            if (dataset.Tables.Contains("ZONEGROUP"))
            {
                MakePK_Zonegroup(dataset);
                MakeIdentities_Zonegroup(dataset);
            }
            if (dataset.Tables.Contains("ZONEGROUP_ZONE"))
            {
                MakePK_ZonegroupZone(dataset);
                MakeIdentities_ZonegroupZone(dataset);
            }
            if (dataset.Tables.Contains("CAL") && dataset.Tables.Contains("DAY_KIND"))
            {
                MakeRelation_Cal_DayKind(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER") && dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakeRelation_Controller_Controller_Type(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_Controller_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_TIME") && dataset.Tables.Contains("CONTROLLER"))
            {
                MakeRelation_ControllerTime_Controller(dataset);
            }
            if (dataset.Tables.Contains("COUNT_GSM") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_CountGSM_Operator(dataset);
            }
            if (dataset.Tables.Contains("COUNT_GSM") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_CountGSM_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("DAY_KIND") && dataset.Tables.Contains("DK_SET"))
            {
                MakeRelation_DayKind_DkSet(dataset);
            }
            if (dataset.Tables.Contains("DRIVER") && dataset.Tables.Contains("DRIVER_STATUS"))
            {
                MakeRelation_Driver_DriverStatus(dataset);
            }
            if (dataset.Tables.Contains("DRIVERGROUP_DRIVER") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_DriverGroupDriver_Driver(dataset);
            }
            if (dataset.Tables.Contains("DRIVERGROUP_DRIVER") && dataset.Tables.Contains("DRIVERGROUP"))
            {
                MakeRelation_DriverGroupDriver_DriverGroup(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_VEHICLE") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_DriverVehicle_Driver(dataset);
            }
            if (dataset.Tables.Contains("DRIVER_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_DriverVehicle_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("FACTOR_VALUES") && dataset.Tables.Contains("FACTORS"))
            {
                MakeRelation_FactorValues_Factors(dataset);
            }
            if (dataset.Tables.Contains("FACTOR_VALUES") && dataset.Tables.Contains("SEASON"))
            {
                MakeRelation_FactorValues_Season(dataset);
            }
            if (dataset.Tables.Contains("BLADING") && dataset.Tables.Contains("BLADING_TYPE"))
            {
                MakeRelation_FK_BLADING_BLADING_TYPE(dataset);
            }
            if (dataset.Tables.Contains("BLADING") && dataset.Tables.Contains("CONTRACTOR"))
            {
                MakeRelation_FK_BLADING_CONTRACTOR(dataset);
            }
            if (dataset.Tables.Contains("BLADING") && dataset.Tables.Contains("WAYBILL"))
            {
                MakeRelation_FK_BLADING_WAYBILL(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE") && dataset.Tables.Contains("DECADE"))
            {
                MakeRelation_FK_BRIGADE_DECADE(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE_DRIVER") && dataset.Tables.Contains("BRIGADE"))
            {
                MakeRelation_FK_BRIGADE_DRIVER_BRIGADE(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE_DRIVER") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_FK_BRIGADE_DRIVER_DRIVER(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE") && dataset.Tables.Contains("GRAPHIC"))
            {
                MakeRelation_FK_BRIGADE_GRAPHIC(dataset);
            }
            if (dataset.Tables.Contains("BRIGADE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_BRIGADE_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_INFO") && dataset.Tables.Contains("CONTROLLER"))
            {
                MakeRelation_FK_CONTROLLER_INFO_CONTROLLER(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_STAT") && dataset.Tables.Contains("CONTROLLER"))
            {
                MakeRelation_FK_CONTROLLER_STAT_CONTROLLER(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") && dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakeRelation_FK_ControllerSensor_ControllerSensorType(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") && dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakeRelation_FK_ControllerSensor_ControllerType(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND") && dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakeRelation_FK_ControllerSensorLegend_ControllerSensorType(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER"))
            {
                MakeRelation_FK_ControllerSensorMap_Controller(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER_SENSOR"))
            {
                MakeRelation_FK_ControllerSensorMap_ControllerSensor(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"))
            {
                MakeRelation_FK_ControllerSensorMap_ControllerSensorLegend(dataset);
            }
            if (dataset.Tables.Contains("DAY_TIME") && dataset.Tables.Contains("DAY_TYPE"))
            {
                MakeRelation_FK_DAY_TIME_DAY_TYPE(dataset);
            }
            if (dataset.Tables.Contains("DECADE") && dataset.Tables.Contains("DEPARTMENT"))
            {
                MakeRelation_FK_DECADE_DEPARTMENT(dataset);
            }
            if (dataset.Tables.Contains("DRIVER") && dataset.Tables.Contains("DEPARTMENT"))
            {
                MakeRelation_FK_DRIVER_DEPARTMENT(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULEREVENT") && dataset.Tables.Contains("EMAIL"))
            {
                MakeRelation_FK_EMAIL_SCHEDULEREVENT_EMAIL(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULEREVENT") && dataset.Tables.Contains("SCHEDULEREVENT"))
            {
                MakeRelation_FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE") && dataset.Tables.Contains("EMAIL"))
            {
                MakeRelation_FK_EMAIL_SCHEDULERQUEUE_EMAIL(dataset);
            }
            if (dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE") && dataset.Tables.Contains("SCHEDULERQUEUE"))
            {
                MakeRelation_FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_RUNTIME") && dataset.Tables.Contains("DAY_TIME"))
            {
                MakeRelation_FK_GEO_SEGMENT_RUNTIME_DAY_TIME(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_RUNTIME") && dataset.Tables.Contains("GEO_SEGMENT"))
            {
                MakeRelation_FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT(dataset);
            }
            if (dataset.Tables.Contains("GOODS") && dataset.Tables.Contains("GOODS_TYPE"))
            {
                MakeRelation_FK_GOODS_GOODS_TYPE(dataset);
            }
            if (dataset.Tables.Contains("GOODS_LOGISTIC_ORDER") && dataset.Tables.Contains("GOODS"))
            {
                MakeRelation_FK_GOODS_LOGISTIC_ORDER_GOODS(dataset);
            }
            if (dataset.Tables.Contains("GOODS_LOGISTIC_ORDER") && dataset.Tables.Contains("LOGISTIC_ORDER"))
            {
                MakeRelation_FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER(dataset);
            }
            if (dataset.Tables.Contains("GOODS_TYPE_VEHICLE") && dataset.Tables.Contains("GOODS_TYPE"))
            {
                MakeRelation_FK_GOODS_TYPE_VEHICLE_GOODS_TYPE(dataset);
            }
            if (dataset.Tables.Contains("GOODS_TYPE_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_GOODS_TYPE_VEHICLE_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DAY") && dataset.Tables.Contains("BRIGADE_DRIVER"))
            {
                MakeRelation_FK_JOURNAL_DAY_BRIGADE_DRIVER(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DAY") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_JOURNAL_DAY_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DAY") && dataset.Tables.Contains("WAYOUT"))
            {
                MakeRelation_FK_JOURNAL_DAY_WAYOUT(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_FK_JOURNAL_DRIVER(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_DRIVER") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_FK_JOURNAL_DRIVER_DRIVER(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_JOURNAL_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_JOURNAL_VEHICLE_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ADDRESS") && dataset.Tables.Contains("CONTRACTOR"))
            {
                MakeRelation_FK_LOGISTIC_ADDRESS_CONTRACTOR(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ADDRESS") && dataset.Tables.Contains("CONTRACTOR_CALENDAR"))
            {
                MakeRelation_FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ADDRESS") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_FK_LOGISTIC_ADDRESS_GEO_ZONE(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ORDER") && dataset.Tables.Contains("BLADING_TYPE"))
            {
                MakeRelation_FK_LOGISTIC_ORDER_BLADING_TYPE(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ORDER") && dataset.Tables.Contains("CONTRACTOR"))
            {
                MakeRelation_FK_LOGISTIC_ORDER_CONTRACTOR(dataset);
            }
            if (dataset.Tables.Contains("LOGISTIC_ORDER") && dataset.Tables.Contains("LOGISTIC_ADDRESS"))
            {
                MakeRelation_FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_PROFILE") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_OPERATOR_PROFILE_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("ORDER_TRIP") && dataset.Tables.Contains("JOURNAL_DAY"))
            {
                MakeRelation_FK_ORDER_TRIP_JOURNAL_DAY(dataset);
            }
            if (dataset.Tables.Contains("ORDER_TRIP") && dataset.Tables.Contains("ORDER"))
            {
                MakeRelation_FK_ORDER_TRIP_ORDER(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_GEOZONE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_FK_ROUTE_GEOZONE_GEO_ZONE(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_GEOZONE") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_FK_ROUTE_GEOZONE_ROUTE(dataset);
            }
            if (dataset.Tables.Contains("RS") && dataset.Tables.Contains("DAY_TYPE"))
            {
                MakeRelation_FK_RS_DAY_TYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_NUMBER") && dataset.Tables.Contains("RS_STEP"))
            {
                MakeRelation_FK_RS_NUMBER_RS_STEP(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") && dataset.Tables.Contains("RS_OPERATIONSTYPE"))
            {
                MakeRelation_FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_PERIOD") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_PERIOD_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT") && dataset.Tables.Contains("RS_OPERATIONTYPE"))
            {
                MakeRelation_FK_RS_POINT_RS_OPERATIONTYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT") && dataset.Tables.Contains("RS_SHIFT"))
            {
                MakeRelation_FK_RS_POINT_RS_SHIFT(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT") && dataset.Tables.Contains("RS_ROUND_TRIP"))
            {
                MakeRelation_FK_RS_POINT_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT") && dataset.Tables.Contains("RS_TRIPTYPE"))
            {
                MakeRelation_FK_RS_POINT_RS_TRIPTYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_POINT") && dataset.Tables.Contains("RS_WAYOUT"))
            {
                MakeRelation_FK_RS_POINT_RS_WAYOUT(dataset);
            }
            if (dataset.Tables.Contains("RS_ROUND_TRIP") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_ROUND_TRIP_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_FK_RS_ROUTE(dataset);
            }
            if (dataset.Tables.Contains("RS_RUNTIME") && dataset.Tables.Contains("RS_PERIOD"))
            {
                MakeRelation_FK_RS_RUNTIME_RS_PERIOD(dataset);
            }
            if (dataset.Tables.Contains("RS_RUNTIME") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RS_RUNTIME_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT") && dataset.Tables.Contains("RS_WAYOUT"))
            {
                MakeRelation_FK_RS_SHIFT_RS_WAYOUT(dataset);
            }
            if (dataset.Tables.Contains("RS_STEP") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_STEP_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIP") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_TRIP_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIP") && dataset.Tables.Contains("ROUTE_TRIP"))
            {
                MakeRelation_FK_RS_TRIP0_ROUTE_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPTYPE") && dataset.Tables.Contains("RS_TRIPSTYPE"))
            {
                MakeRelation_FK_RS_TRIPTYPE_RS_TRIPSTYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPTYPE") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_TRIPTYPE_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS_VARIANT") && dataset.Tables.Contains("RS"))
            {
                MakeRelation_FK_RS_VARIANT_RS(dataset);
            }
            if (dataset.Tables.Contains("RS_VARIANT") && dataset.Tables.Contains("RS_TYPE"))
            {
                MakeRelation_FK_RS_VARIANT_RS_TYPE(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("GRAPHIC"))
            {
                MakeRelation_FK_RS_WAYOUT_GRAPHIC(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("RS_VARIANT"))
            {
                MakeRelation_FK_RS_WAYOUT_RS_VARIANT(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("WAYOUT"))
            {
                MakeRelation_FK_RS_WAYOUT_WAYOUT(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSOT_TRIPIN_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSOT_TRIPIN0_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSOT_TRIPOUT_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_OPERATIONTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSOT_TRIPOUT0_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSSHIFT_TRIPIN_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSSHIFT_TRIPIN0_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSSHIFT_TRIPOUT_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_SHIFT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSSHIFT_TRIPOUT0_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSTT_TRIPA_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_TRIPTYPE") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSTT_TRIPB_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSW_TRIPIN_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSW_TRIPIN0_RS_TRIP0(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSW_TRIPOUT_RS_TRIP(dataset);
            }
            if (dataset.Tables.Contains("RS_WAYOUT") && dataset.Tables.Contains("RS_TRIP"))
            {
                MakeRelation_FK_RSW_TRIPOUT0_RS_TRIP0(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_DETAIL_INFO") && dataset.Tables.Contains("SCHEDULE_DETAIL"))
            {
                MakeRelation_FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_GEOZONE") && dataset.Tables.Contains("ROUTE_GEOZONE"))
            {
                MakeRelation_FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_GEOZONE") && dataset.Tables.Contains("TRIP"))
            {
                MakeRelation_FK_SCHEDULE_GEOZONE_TRIP(dataset);
            }
            if (dataset.Tables.Contains("SMS_LOG") && dataset.Tables.Contains("OWNER"))
            {
                MakeRelation_FK_SMS_LOG_OWNER(dataset);
            }
            if (dataset.Tables.Contains("SMS_LOG") && dataset.Tables.Contains("SMS_TYPE"))
            {
                MakeRelation_FK_SMS_LOG_SMS_TYPE(dataset);
            }
            if (dataset.Tables.Contains("SMS_LOG") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_SMS_LOG_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE") && dataset.Tables.Contains("DEPARTMENT"))
            {
                MakeRelation_FK_VEHICLE_DEPARTMENT(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_OWNER") && dataset.Tables.Contains("OWNER"))
            {
                MakeRelation_FK_VEHICLE_OWNER_OWNER(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_OWNER") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_VEHICLE_OWNER_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_PICTURE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_FK_VEHICLE_PICTURE_VEHICLE(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("GEO_TRIP"))
            {
                MakeRelation_FK_Wb_Trip_Geo_Trip(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("ORDER_TRIP"))
            {
                MakeRelation_FK_WB_TRIP_ORDER_TRIP(dataset);
            }
            if (dataset.Tables.Contains("WORK_TIME") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_FK_WORK_TIME_DRIVER(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT") && dataset.Tables.Contains("FACTORS"))
            {
                MakeRelation_GeoSegment_Factors(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT") && dataset.Tables.Contains("GEO_TRIP"))
            {
                MakeRelation_GeoSegment_GeoTrip(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT") && dataset.Tables.Contains("POINT"))
            {
                MakeRelation_GeoSegment_Point(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT") && dataset.Tables.Contains("POINT"))
            {
                MakeRelation_GeoSegment_PointTo(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_VERTEX") && dataset.Tables.Contains("GEO_SEGMENT"))
            {
                MakeRelation_GeoSegmentVeretx_GeoSegment(dataset);
            }
            if (dataset.Tables.Contains("GEO_SEGMENT_VERTEX") && dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakeRelation_GeoSegmentVeretx_MapVeretx(dataset);
            }
            if (dataset.Tables.Contains("GEO_TRIP") && dataset.Tables.Contains("POINT"))
            {
                MakeRelation_GeoTrip_PointA(dataset);
            }
            if (dataset.Tables.Contains("GEO_TRIP") && dataset.Tables.Contains("POINT"))
            {
                MakeRelation_GeoTrip_PointB(dataset);
            }
            if (dataset.Tables.Contains("GEO_TRIP") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_GeoTrip_Route(dataset);
            }
            if (dataset.Tables.Contains("GEO_ZONE_PRIMITIVE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_GeoZonePrimitive_GeoZone(dataset);
            }
            if (dataset.Tables.Contains("GEO_ZONE_PRIMITIVE") && dataset.Tables.Contains("ZONE_PRIMITIVE"))
            {
                MakeRelation_GeoZonePrimitive_ZonePrimitive(dataset);
            }
            if (dataset.Tables.Contains("GRAPHIC_SHIFT") && dataset.Tables.Contains("GRAPHIC"))
            {
                MakeRelation_GraphicShift_Graphic(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_Journal_Driver1(dataset);
            }
            if (dataset.Tables.Contains("JOURNAL") && dataset.Tables.Contains("WAYOUT"))
            {
                MakeRelation_Journal_Wayout(dataset);
            }
            if (dataset.Tables.Contains("LOG_GUARD_EVENTS") && dataset.Tables.Contains("GUARD_EVENTS"))
            {
                MakeRelation_LogGuardEvents_GuardEvents(dataset);
            }
            if (dataset.Tables.Contains("LOG_OPS_EVENTS") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_LogOpsEvents_Operator(dataset);
            }
            if (dataset.Tables.Contains("LOG_OPS_EVENTS") && dataset.Tables.Contains("OPS_EVENT"))
            {
                MakeRelation_LogOpsEvents_OpsEvent(dataset);
            }
            if (dataset.Tables.Contains("LOG_VEHICLE_STATUS") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_LogVehicleStatus_Operator(dataset);
            }
            if (dataset.Tables.Contains("LOG_VEHICLE_STATUS") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_LogVehicleStatus_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("LOG_VEHICLE_STATUS") && dataset.Tables.Contains("VEHICLE_STATUS"))
            {
                MakeRelation_LogVehicleStatus_VehicleStatus(dataset);
            }
            if (dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_LogWaybillHeaderStatus_Operator(dataset);
            }
            if (dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS") && dataset.Tables.Contains("SCHEDULE"))
            {
                MakeRelation_LogWaybillHeaderStatus_Schedule(dataset);
            }
            if (dataset.Tables.Contains("MAP_VERTEX") && dataset.Tables.Contains("MAPS"))
            {
                MakeRelation_MapVeretx_Maps(dataset);
            }
            if (dataset.Tables.Contains("MEDIA") && dataset.Tables.Contains("MEDIA_TYPE"))
            {
                MakeRelation_Media_MediaType(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS") && dataset.Tables.Contains("MEDIA"))
            {
                MakeRelation_MediaAcceptors_Media(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE") && dataset.Tables.Contains("MESSAGE_TEMPLATE"))
            {
                MakeRelation_Message_MessageTemplate(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_FIELD") && dataset.Tables.Contains("MESSAGE"))
            {
                MakeRelation_MessageField_Message(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_FIELD") && dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"))
            {
                MakeRelation_MessageField_MessageTemplateField(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_OPERATOR") && dataset.Tables.Contains("MESSAGE"))
            {
                MakeRelation_MessageOperator_Message(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_MessageOperator_Operator(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD") && dataset.Tables.Contains("DOTNET_TYPE"))
            {
                MakeRelation_MessageTemplateField_DotnetType(dataset);
            }
            if (dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD") && dataset.Tables.Contains("MESSAGE_TEMPLATE"))
            {
                MakeRelation_MessageTemplateField_MessageTemplate(dataset);
            }
            if (dataset.Tables.Contains("MONITOREE_LOG") && dataset.Tables.Contains("MEDIA_TYPE"))
            {
                MakeRelation_MonitoreeLog_MediaType(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP") && dataset.Tables.Contains("DRIVERGROUP"))
            {
                MakeRelation_OoperatorDriverGroup_DriverGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DEPARTMENT") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorDepartment_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DEPARTMENT") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorDepartment_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVER") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_OperatorDriver_Driver(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVER") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorDriver_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVER") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorDriver_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorDriverGroup_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_DRIVERGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorDriverGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupDepartment_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupDepartment_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVER") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_OperatorGroupDriver_Driver(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVER") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupDriver_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVER") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupDriver_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP") && dataset.Tables.Contains("DRIVERGROUP"))
            {
                MakeRelation_OperatorGroupDriverGroup_DriverGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupDriverGroup_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupDriverGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorGroupOperator_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupOperator_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_REPORT") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorgroupReport_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_REPORT") && dataset.Tables.Contains("REPORT"))
            {
                MakeRelation_OperatorgroupReport_Report(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_REPORT") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorgroupReport_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTE") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupRoute_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupRoute_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTE") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_OperatorGroupRoute_Route(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupRouteGroup_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupRouteGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP") && dataset.Tables.Contains("ROUTEGROUP"))
            {
                MakeRelation_OperatorGroupRouteGroup_RouteGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupVehicle_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupVehicle_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_OperatorGroupVehicle_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupVehicleGroup_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupVehicleGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP") && dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakeRelation_OperatorGroupVehicleGroup_VehicleGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONE") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupZone_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupZone_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_OperatorGroupZone_Zone(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_OperatorGroupZoneGroup_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorGroupZoneGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP") && dataset.Tables.Contains("ZONEGROUP"))
            {
                MakeRelation_OperatorGroupZoneGroup_ZoneGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_REPORT") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorReport_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_REPORT") && dataset.Tables.Contains("REPORT"))
            {
                MakeRelation_OperatorReport_Report(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_REPORT") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorReport_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTE") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorRoute_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorRoute_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTE") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_OperatorRoute_Route(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorRouteGroup_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorRouteGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ROUTEGROUP") && dataset.Tables.Contains("ROUTEGROUP"))
            {
                MakeRelation_OperatorRouteGroup_RouteGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLE") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorVehicle_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorVehicle_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_OperatorVehicle_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorVehicleGroup_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorVehicleGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_VEHICLEGROUP") && dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakeRelation_OperatorVehicleGroup_VehicleGroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONE") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorZone_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONE") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorZone_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_OperatorZone_Zone(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONEGROUP") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_OperatorZoneGroup_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONEGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_OperatorZoneGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR_ZONEGROUP") && dataset.Tables.Contains("ZONEGROUP"))
            {
                MakeRelation_OperatorZoneGroup_ZoneGroup(dataset);
            }
            if (dataset.Tables.Contains("ORDER") && dataset.Tables.Contains("CUSTOMER"))
            {
                MakeRelation_Order_Customer(dataset);
            }
            if (dataset.Tables.Contains("ORDER") && dataset.Tables.Contains("ORDER_TYPE"))
            {
                MakeRelation_Order_OrderType(dataset);
            }
            if (dataset.Tables.Contains("PERIOD") && dataset.Tables.Contains("DAY_KIND"))
            {
                MakeRelation_Period_DayKind(dataset);
            }
            if (dataset.Tables.Contains("PERIOD") && dataset.Tables.Contains("DK_SET"))
            {
                MakeRelation_Period_DkSet(dataset);
            }
            if (dataset.Tables.Contains("PERIOD") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_Period_Route(dataset);
            }
            if (dataset.Tables.Contains("PERIOD") && dataset.Tables.Contains("SEASON"))
            {
                MakeRelation_Period_Season(dataset);
            }
            if (dataset.Tables.Contains("POINT") && dataset.Tables.Contains("BUSSTOP"))
            {
                MakeRelation_Point_Busstop(dataset);
            }
            if (dataset.Tables.Contains("POINT") && dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakeRelation_Point_MapVeretx(dataset);
            }
            if (dataset.Tables.Contains("POINT") && dataset.Tables.Contains("POINT_KIND"))
            {
                MakeRelation_Point_PointKind(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_RightOperator_Operator(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATOR") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_RightOperator_Right(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATORGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_RightOperatorGroup_OperatorGroup(dataset);
            }
            if (dataset.Tables.Contains("RIGHT_OPERATORGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_RightOperatorGroup_Right(dataset);
            }
            if (dataset.Tables.Contains("ROUTEGROUP_ROUTE") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_RouteGroupRoute_Route(dataset);
            }
            if (dataset.Tables.Contains("ROUTEGROUP_ROUTE") && dataset.Tables.Contains("ROUTEGROUP"))
            {
                MakeRelation_RouteGroupRoute_RouteGroup(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_POINT") && dataset.Tables.Contains("POINT"))
            {
                MakeRelation_RoutePoint_Point(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_POINT") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_RoutePoint_Route(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_TRIP") && dataset.Tables.Contains("GEO_TRIP"))
            {
                MakeRelation_RouteTrip_GeoTrip(dataset);
            }
            if (dataset.Tables.Contains("ROUTE_TRIP") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_RouteTrip_Route(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE") && dataset.Tables.Contains("DAY_KIND"))
            {
                MakeRelation_Schedule_DayKind(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_Schedule_Route(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE") && dataset.Tables.Contains("WAYOUT"))
            {
                MakeRelation_Schedule_Wayout(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_DETAIL") && dataset.Tables.Contains("SCHEDULE"))
            {
                MakeRelation_ScheduleDetail_Schedule(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_PASSAGE") && dataset.Tables.Contains("SCHEDULE_POINT"))
            {
                MakeRelation_SchedulePassage_SchedulePoint(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_PASSAGE") && dataset.Tables.Contains("WAYBILL_HEADER"))
            {
                MakeRelation_SchedulePassage_WaybillHeader(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_POINT") && dataset.Tables.Contains("GEO_SEGMENT"))
            {
                MakeRelation_SchedulePoint_GeoSegment(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_POINT") && dataset.Tables.Contains("ROUTE_POINT"))
            {
                MakeRelation_SchedulePoint_RoutePoint(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULE_POINT") && dataset.Tables.Contains("TRIP"))
            {
                MakeRelation_SchedulePoint_Trip(dataset);
            }
            if (dataset.Tables.Contains("SCHEDULERQUEUE") && dataset.Tables.Contains("SCHEDULEREVENT"))
            {
                MakeRelation_SchedulerQueue_SchedulerEvent(dataset);
            }
            if (dataset.Tables.Contains("TRAIL") && dataset.Tables.Contains("SESSION"))
            {
                MakeRelation_Trail_Session(dataset);
            }
            if (dataset.Tables.Contains("TRIP") && dataset.Tables.Contains("ROUTE_TRIP"))
            {
                MakeRelation_Trip_RouteTrip(dataset);
            }
            if (dataset.Tables.Contains("TRIP") && dataset.Tables.Contains("SCHEDULE_DETAIL"))
            {
                MakeRelation_Trip_ScheduleDetail(dataset);
            }
            if (dataset.Tables.Contains("TRIP") && dataset.Tables.Contains("TRIP_KIND"))
            {
                MakeRelation_Trip_TripKind(dataset);
            }
            if (dataset.Tables.Contains("TRIP_KIND") && dataset.Tables.Contains("WORKTIME_STATUS_TYPE"))
            {
                MakeRelation_TripKind_WorktimeStatusType(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE") && dataset.Tables.Contains("GRAPHIC"))
            {
                MakeRelation_Vehicle_Graphic(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE") && dataset.Tables.Contains("VEHICLE_KIND"))
            {
                MakeRelation_Vehicle_VehicleKind(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE") && dataset.Tables.Contains("VEHICLE_STATUS"))
            {
                MakeRelation_Vehicle_VehicleStatus(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_RULE") && dataset.Tables.Contains("RULE"))
            {
                MakeRelation_VehiclegroupRule_Rule(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_RULE") && dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakeRelation_VehiclegroupRule_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_VehicleGroupVehicle_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("VEHICLEGROUP_VEHICLE") && dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakeRelation_VehicleGroupVehicle_VehicleGroup(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_RULE") && dataset.Tables.Contains("RULE"))
            {
                MakeRelation_VehicleRule_Rule(dataset);
            }
            if (dataset.Tables.Contains("VEHICLE_RULE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_VehicleRule_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL") && dataset.Tables.Contains("DRIVER"))
            {
                MakeRelation_Waybill_Driver(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER") && dataset.Tables.Contains("SCHEDULE"))
            {
                MakeRelation_WaybillHeader_Schedule(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_WaybillHeader_Trailor1(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_WaybillHeader_Trailor2(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER") && dataset.Tables.Contains("TRIP_KIND"))
            {
                MakeRelation_WaybillHeader_TripKind(dataset);
            }
            if (dataset.Tables.Contains("WAYBILL_HEADER") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_WaybillHeader_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK") && dataset.Tables.Contains("WAYBILLMARK"))
            {
                MakeRelation_Waybillheader_Waybillmark_Waybillmark(dataset);
            }
            if (dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK") && dataset.Tables.Contains("WAYBILL_HEADER"))
            {
                MakeRelation_WaybillheaderWaybillmark_WaybillHeader(dataset);
            }
            if (dataset.Tables.Contains("WAYOUT") && dataset.Tables.Contains("GRAPHIC"))
            {
                MakeRelation_Wayout_Graphic(dataset);
            }
            if (dataset.Tables.Contains("WAYOUT") && dataset.Tables.Contains("ROUTE"))
            {
                MakeRelation_Wayout_Route(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("TRIP"))
            {
                MakeRelation_WbTrip_Trip(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("TRIP_KIND"))
            {
                MakeRelation_WbTrip_TripKind(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("WAYBILL"))
            {
                MakeRelation_WbTrip_Waybill(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("WAYBILL_HEADER"))
            {
                MakeRelation_WbTrip_WaybillHeader(dataset);
            }
            if (dataset.Tables.Contains("WB_TRIP") && dataset.Tables.Contains("WORKTIME_REASON"))
            {
                MakeRelation_WbTrip_WorktimeReason(dataset);
            }
            if (dataset.Tables.Contains("WEB_LINE") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_WebLine_Operator(dataset);
            }
            if (dataset.Tables.Contains("WEB_LINE_VERTEX") && dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakeRelation_WebLineVertex_Vertex(dataset);
            }
            if (dataset.Tables.Contains("WEB_LINE_VERTEX") && dataset.Tables.Contains("WEB_LINE"))
            {
                MakeRelation_WebLineVertex_WebLine(dataset);
            }
            if (dataset.Tables.Contains("WEB_POINT") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_WebPoint_Operator(dataset);
            }
            if (dataset.Tables.Contains("WEB_POINT") && dataset.Tables.Contains("WEB_POINT_TYPE"))
            {
                MakeRelation_WebPoint_Type(dataset);
            }
            if (dataset.Tables.Contains("WEB_POINT") && dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakeRelation_WebPoint_Vertex(dataset);
            }
            if (dataset.Tables.Contains("WORKTIME_REASON") && dataset.Tables.Contains("WORKTIME_STATUS_TYPE"))
            {
                MakeRelation_WorktimeReason_WorktimeStatusType(dataset);
            }
            if (dataset.Tables.Contains("ZONEGROUP_ZONE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_ZoneGroupZone_Zone(dataset);
            }
            if (dataset.Tables.Contains("ZONEGROUP_ZONE") && dataset.Tables.Contains("ZONEGROUP"))
            {
                MakeRelation_ZoneGroupZone_ZoneGroup(dataset);
            }
            if (dataset.Tables.Contains("ZONE_PRIMITIVE") && dataset.Tables.Contains("ZONE_TYPE"))
            {
                MakeRelation_ZonePrimitive_ZoneType(dataset);
            }
            if (dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX") && dataset.Tables.Contains("MAP_VERTEX"))
            {
                MakeRelation_ZonePrimitiveVertex_MapVeretx(dataset);
            }
            if (dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX") && dataset.Tables.Contains("ZONE_PRIMITIVE"))
            {
                MakeRelation_ZonePrimitiveVertex_ZonePrimitive(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLE") && dataset.Tables.Contains("VEHICLE"))
            {
                MakeRelation_ZoneVehicle_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLE") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_ZoneVehicle_Zone(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLEGROUP") && dataset.Tables.Contains("VEHICLEGROUP"))
            {
                MakeRelation_ZoneVehicleGroup_Vehicle(dataset);
            }
            if (dataset.Tables.Contains("ZONE_VEHICLEGROUP") && dataset.Tables.Contains("GEO_ZONE"))
            {
                MakeRelation_ZoneVehicleGroup_Zone(dataset);
            }
        }
}
}
