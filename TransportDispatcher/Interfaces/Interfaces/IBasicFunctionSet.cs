﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using Dto = FORIS.TSS.BusinessLogic.DTO;
using Enm = FORIS.TSS.BusinessLogic.Enums;
using Geo = Interfaces.Geo;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Базовый функционал приложения </summary>
	public interface IBasicFunctionSet : Geo::IGeoAddressProvider, Geo::IGeoArrivalProvider
	{
		/// <summary> Получение данных о запланированных рейсах и их заменах </summary>
		/// <param name="iReportDateTime">дата, на которую требуется получить данные о замененных рейсах</param>
		/// <param name="iMotorcadeID">ID группы ТС (номер автоколонны), для которой строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet SubstitutionsInfoGet(DateTime iReportDateTime, int iMotorcadeID, int operatorId);
		/// <summary> Получение данных о ТС на указанную дату, в путевых листах которых указано определенное состояние </summary>
		/// <param name="iReportDate">дата, на которую требуется получить данные о несостоявшихся рейсах.</param>
		/// <param name="iVehicleGroupID">ID автоколонны, для которой создается наряд</param>
		/// <param name="iWaybillState">номер состояния ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet VehiclesListByStateGet(DateTime iReportDate, int iVehicleGroupID, int iWaybillState);
		/// <summary> Получение данных о водителях на указанную дату, в записях WAYBILL которых указано определенное состояние </summary>
		/// <param name="iReportDate">дата, на которую требуется получить данные о несостоявшихся рейсах.</param>
		/// <param name="iDriverGroupID">ID группы водителей, для которой требуется получить отчет</param>
		/// <param name="iDriverState">номер состояния записи WAYBILL</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet DriversListByStateGet(DateTime iReportDate, int iDriverGroupID, int iDriverState);
		/// <summary> Получение списка ТС с указанным состоянием для всех колонн на указанную дату </summary>
		/// <param name="iReportDate">Дата, на которую требуется получить данные ТС с указанным состоянием.</param>
		/// <param name="iWaybillState">номер состояния ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet VehiclesCommonListByStateGet(DateTime iReportDate, int iWaybillState);
		/// <summary> Получение данных по статистике доступности GPRS </summary>
		/// <param name="iReportDate">дата, на которую требуется получить данные о доступности GPRS</param>
		/// <param name="iTimeType">определяет представление выводимого времени</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GPRSAccessibilityStatisticsGet(DateTime iReportDate, char iTimeType, int operatorId);
		/// <summary> Получение списка ТС указанной автоколонны с их состояниями на указанную дату </summary>
		/// <param name="iReportDateTime">дата и время, на которые требуется сформировать отчет</param>
		/// <param name="VehicleGroupID">номер автоколонны</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet VehiclesStatusesListGet(DateTime iReportDateTime, int VehicleGroupID);
		/// <summary> Получение данных для заполнения оборотной стороны путевого листа (расписания) </summary>
		/// <param name="iWaybillHeaderID">ID ПЛ, для которого будут получены данные</param>
		/// <param name="iShift"></param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet WaybillTimesinGet(int iWaybillHeaderID, long iShift);
		/// <summary> Получение данных для заполнения путевого листа </summary>
		/// <param name="waybill_header_id"></param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GetScheduleForWaybill(int waybill_header_id);
		/// <summary> Получение данных для заполнения в расписании ПЛ особых рейсов (обедов, заправок и т.д.) </summary>
		/// <param name="waybill_header_id">ID заголовка ПЛ</param>
		/// <param name="trip_kind_id">тип поездки, данные для которой надо получить</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet SpecialTripsScheduleGet(int waybill_header_id, int trip_kind_id);
		/// <summary> Получение названий автоколонн по указанной дате. Выбираются только те автоколонны, у которых хотя бы на один автомобиль заведен ПЛ </summary>
		/// <param name="iReportDate">дата, на которую требуется получить список автоколонн</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ActiveMotorcadeNamesByDateGet(DateTime iReportDate);
		DataSet ActiveMotorcadeNamesByDateGet(DateTime iReportDate, int operatorId);
		DataSet MotorcadeNamesGet(int operatorId);
		/// <summary> Получение списка названий существующих в системе GSM-контроллеров </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ActiveControllerTypeNamesGet();
		/// <summary> Получение данных для заполнения отчета "Путевой лист" по указанному ID ПЛ </summary>
		/// <param name="iWaybillHeaderID">ID ПЛ, для которого получаются данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet WaybillDataByIDGet(int iWaybillHeaderID);
		/// <summary> Получение данных по сменам водителей у ПЛ с указанным ID: ФИО водителей, табельные номера, количество смен у данного ПЛ </summary>
		/// <param name="iWaybillHeaderID">ID ПЛ, для которого получаются данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet DriversShiftsInfoGet(int iWaybillHeaderID);
		/// <summary> Получение информации о расписании для построения отчета по набору параметров </summary>
		/// <param name="iExitNumber">номер выхода</param>
		/// <param name="iDayKindID">ID типа дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ScheduleByParametersGet(string iExitNumber, int iDayKindID);
		/// <summary> Получение недостающих параметров для отчета "Расписание" по уже известным </summary>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iPeriodID">ID периода действия расписания</param>
		/// <param name="iDayKindID">ID типа дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ScheduleReportParametersRefresh(int iRouteID, int iPeriodID, int iDayKindID, int operatorId);
		/// <summary> Получение данных о специальных пометках для ПЛ </summary>
		/// <param name="iWaybillHeaderID">ID путевого листа</param>
		/// <param name="iMarkDate">Дата пометки путевого листа</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet WaybillMarksGet(int iWaybillHeaderID, DateTime iMarkDate);
		/// <summary> Получение списка конечных станций для всех маршрутов </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet EndStationsListGet();
		/// <summary> Получение списка всех конечных для всех маршрутов </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet StationsListGet();
		/// <summary> Получение списка всех конечных для всех маршрутов </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet StationsListGet(int operatorId);
		/// <summary> Получение плановых и фактических интервалов отправлений автобусов данного маршрута от определенной конечной станции </summary>
		/// <param name="iGeoSegmentID">ID остановки</param>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iReportDate">дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet MovementIntervalsGet(int iGeoSegmentID, int iRouteID, DateTime iReportDate);
		/// <summary> Получение данных о запланированных по расписанию и фактических временах прохождения заданной точки автобусами заданного маршрута </summary>
		/// <param name="iPointID">ID точки (остановки)</param>
		/// <param name="iRouteID">ID маршрута</param>
		/// <param name="iReportDateTime">дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet MovementIntervalsThroughPpointGet(int iPointID, int iRouteID, DateTime iReportDateTime);
		/// <summary> Получение списка всех заведенных в БД маршрутов, получение списка рейсов и их статусов по каждому из маршрутов на заданный период </summary>
		/// <param name="iReportDate">Дата, на которую строится отчет</param>
		/// <param name="iReportTimeInSeconds">Время, на которое строится отчет, в секундах от начала дня</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet TripsStatisticsByRoutesGet(DateTime iReportDate, int iReportTimeInSeconds);
		/// <summary> Получение информации о запланированных и фактических рейсах на заданное время по каждому из маршрутов </summary>
		/// <param name="iReportDateTime">Дата и время, на которые строится отчет</param>
		/// <param name="iReportTimeInSeconds">Дата и время в секундах от начала дня, на которые строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet TripsByRoutesAndTimeGet(DateTime iReportDateTime, int iReportTimeInSeconds);
		/// <summary> Получение количества "полезных" (активных) рейсов для ПЛ с данным ID </summary>
		/// <param name="iWaybillHeaderID">ID рассматриваемого ПЛ</param>
		/// <param name="iShiftNumber">Номер смены, для которой требуется получить количество рейсов</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet UsefullWaybilTripsCount(int iWaybillHeaderID, int iShiftNumber);
		/// <summary> Получение расширенной информации по определенной автоколонне на указанную дату </summary>
		/// <param name="iReportDateTime">Дата, на которую требуется получить информацию из БД</param>
		/// <param name="iVehicleGroupID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet MotorcadeAdvancedDataGet(DateTime iReportDateTime, int iVehicleGroupID);
		/// <summary> Получение данных об интервалах недоступности GPRS на ТС </summary>
		/// <param name="iReportDate">Дата, на которую требуется получить информацию из БД</param>
		/// <param name="iVehicleGroupID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <param name="iControllerTypeID">ID контроллера, для ТС с которым требуется построить отчет</param>
		/// <param name="iNeglibleInterval">Значение не регистрируемого интервала отсутствия сигнала</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GPRSInaccessibilityDataGet(DateTime iReportDate, int iVehicleGroupID, int iControllerTypeID, int iNeglibleInterval, int iVehicleID, int operatorId);
		DataSet GetDataForWaybillHistoryReport(int waybill_id);
		/// <summary> Получение данных для заполнения ПЛ </summary>
		/// <param name="waybill_header_id">ID заголовка ПЛ</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GetTimeDeviations(int waybill_header_id);
		DataSet SearchWaybills(int iWaybillHeaderId, DateTime dtDate, int iVehicleId, int iScheduleId, int iDriverId);
		/// <summary> Информация для печати отчета о пробеге: за период для отдельного ТС </summary>
		DataSet GetMoveHistoryDistance(DateTime time_from, DateTime time_to, string vehicle);
		/// <summary> Информация для печати истории: за период для отдельного ТС </summary>
		DataSet GetMoveDetailHistory(int count, int interval, DateTime time_from, DateTime time_to, int vehicleID, IEnumerable<Dto::Params.LogTimeSpanParam> workingIntervals);
		/// <summary> Возвращает суточный пробег указанного транспортного средства </summary>
		/// <param name="vehicleID">Идентификатор транспортного средства.</param>
		/// <param name="workingIntervals">Перечислитель интервалов рабочих часов, выпадающий на общий интервал отчета.</param>
		/// <returns></returns>
		DataSet GetMoveGroupDay(int vehicleID, IEnumerable<Dto::Params.LogTimeSpanParam> workingIntervals);
		/// <summary> Получение пробега для заданной машины за заданный период времени </summary>
		/// <param name="fromInt">дата начала периода (UTC)</param>
		/// <param name="toInt">дата конца периода (UTC)</param>
		/// <param name="vehicleID">ID машины</param>
		/// <returns>Пробег в метрах</returns>
		float GetMoveHistoryDistanceValue(int fromInt, int toInt, int vehicleID);
		/// <summary> get data for analytic report </summary>
		/// <param name="route">ROUTE_ID</param>
		/// <param name="from">begin time</param>
		/// <param name="to">end time</param>
		/// <param name="rtnum">ROUTE.EXT_NUMBER</param>
		/// <param name="schnum">SCHEDULE.EXT_NUMBER</param>
		/// <returns></returns>
		DataSet GetAnalytic(int route, DateTime from, DateTime to, string rtnum, string schnum);
		/// <summary> Получение ID путевого листа по номеру и дате </summary>
		/// <param name="for_Num">Номер путевого листа, для которого строится отчет</param>
		/// <param name="for_Date">Дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet WaybillNewIDGet(string for_Num, DateTime for_Date, int operatorId);
		/// <summary> Получение списка выходов по ID маршрута и дате </summary>
		/// <param name="for_RouteID">ID маршрута, для которого строится отчет</param>
		/// <param name="for_Date">Дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet AnaliticsOutputsGet(int for_RouteID, DateTime for_Date);
		/// <summary> Получение списка всех маршрутов, проходящих через определенную точку на указанную дату </summary>
		/// <param name="iPointID">ID точки, для которой требуется получить данные</param>
		/// <param name="iReportDateTime">Дата и время, на которые строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet RoutesListThroughPointGet(int iPointID, DateTime iReportDateTime);
		/// <summary>
		/// инкремент поля PRINTED (количество печати) и указание причины в поле PRINTREASON таблицы WAYBILL_HEADER после печати ПЛ
		/// Запись о печати ПЛ в таблицу истории H_WAYBILL_HEADER
		/// </summary>
		/// <param name="for_ID">ID путевого листа, который печатался</param>
		/// <param name="set_PrintReason">Причина печати (в соотв. с новой табл. REPRINT_REASON)</param>
		/// <param name="set_OperatorID">ID оператора, который произвел печать</param>
		void WaybillPrintSet(int for_ID, int set_PrintReason, int set_OperatorID);
		/// <summary> Получение данных для построения отчета о пропущенных и забракованных рейсах, на указанную дату </summary>
		/// <param name="for_Date">Дата, на которую строится отчет</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet MissedTripsDetailGet(DateTime for_Date);
		/// <summary> Получение количества сходов и браков для ТС указанной автоколонны за указанный период времени </summary>
		/// <param name="iReportDateFrom">Начало периода, по которому строится отчет</param>
		/// <param name="iReportDateTo">Окончание периода, по которому строится отчет</param>
		/// <param name="iMotorcadeID">ID группы ТС (автоколонны), для которой требуется получить данные</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet OutsAndRejectsCountGet(DateTime iReportDateFrom, DateTime iReportDateTo, int iMotorcadeID);
		/// <summary> Процедура выборки всех или закрепленных за пользователем GUID-ов отчетов по заданному ID оператора </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GetGuidsReportsForUser();
		/// <summary> Универсальная Функция вызова процедур для отчетов </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GetDataFromDB(ArrayList arParams, string strStoredProcedureName, string[] strTablesNames);
		DataSet FillGeoZoneColumn(
			DataSet dataSet,
			int operatorID,
			int? vehicleID,
			string tableName,
			string lngColumnName,
			string latColumnName,
			string dataColumnName);
		DataSet FillGeoPointColumn(
			DataSet dataSet,
			int operatorID,
			int? vehicleID,
			int? radius,
			string tableName,
			string lngColumnName,
			string latColumnName,
			string dataColumnName);
		/// <summary> Отчет о работе по выходам </summary>
		/// <param name="route"></param>
		/// <param name="date"></param>
		/// <returns></returns>
		DataSet GetWayoutInfo(string route, DateTime date);
		DataSet GetZones(int vehicle_id);
		/// <summary> Возвращает все группы ТС доступные оператору </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		DataSet GetVehicleGroups(int operatorId);
		/// <summary> Возвращает информацию по группам оператора и их составе </summary>
		List<Dto::Group> GetVehicleGroupsWithVehicles(int operatorID, Dto::Department departmentID, Dto::GetGroupsArguments arguments = null);
		/// <summary> Возвращает исчерпывающую информацию об объектах наблюдения, доступных оператору </summary>
		List<Dto::Vehicle> GetVehiclesWithPositions(int operatorId, Dto::Department department, Dto::GetVehiclesArgs arguments);
		/// <summary> Возвращает позиции пар идентификатора объекта и момента времени (возможно включить ближайшие точки) </summary>
		List<Dto::VehiclePositionParam> GetPositions(IEnumerable<Dto::VehicleLogTimeParam> vehicleLogTimeParams, bool includeNeighbour, CancellationToken? cancellationToken = null);
		/// <summary> Возвращает все сообщения текущего пользователя по идентификатору пользователя и набору объектов наблюдения </summary>
		Dto::Messages.Message[] GetMessagesByOperator(int operatorId, int? lastMessageId, DateTime? @from, DateTime? to, Dto::Pagination pagination, int[] vehicleIds);
		/// <summary> Возвращает историю наблюдения за объектом </summary>
		/// <param name="operatorId">Идентификатор оператора, выполняющего запрос</param>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="logTimeFrom">Время начала периода наблюдения</param>
		/// <param name="logTimeTo">Время конца периода наблюдения</param>
		/// <param name="options"></param>
		/// <param name="cultureInfo">Культура, с которой нужно локализовать результат</param>
		Dto::Historical.FullLog GetFullLog(int operatorId, int vehicleId, int logTimeFrom, int logTimeTo, Dto::Historical.FullLogOptions options, CultureInfo cultureInfo = null);
		/// <summary> Возвращает список соответствий объекта наблюдения, времени и пробега </summary>
		/// <param name="vehicleGroupId">Идентификатор группы объектов наблюдения</param>
		/// <param name="logTimeFrom">Время начала периода наблюдения</param>
		/// <param name="logTimeTo">Время конца периода наблюдения</param>
		List<Dto::Historical.RunLogRecord> GetRunLogForGroup(int vehicleGroupId, int logTimeFrom, int logTimeTo);
		/// <summary> Возвращает список соответствий объекта наблюдения, времени и пробега </summary>
		/// <param name="vehicleId"></param>
		/// <param name="logTimeFrom">Время начала периода наблюдения</param>
		/// <param name="logTimeTo">Время конца периода наблюдения</param>
		List<Dto::Historical.RunLogRecord> GetRunLogForVehicle(int vehicleId, int logTimeFrom, int logTimeTo);
		/// <summary> Возвращает список соответствий объекта наблюдения, времени и пробега </summary>
		/// <param name="vehicleIds">Список идентификаторов группы объектов наблюдения</param>
		/// <param name="logTimeSpanParams">Список интервалов, за который необходимо получить данные</param>
		/// <param name="includeAdjacent">Включать ли в вывод точки, смежные с границами интервалов</param>
		List<Dto::Historical.RunLogRecord> GetRunLog(IEnumerable<int> vehicleIds, IEnumerable<Dto::Params.LogTimeSpanParam> logTimeSpanParams, bool includeAdjacent);
		/// <summary> Возвращает список ТС группы ТС, для которых пользователь имеет указанные права </summary>
		/// <param name="vehicleGroupId"></param>
		/// <param name="operatorId"></param>
		/// <param name="culture"></param>
		/// <param name="systemRights"></param>
		/// <returns></returns>
		List<Dto::Vehicle> GetVehiclesByVehicleGroup(int vehicleGroupId, int operatorId, CultureInfo culture, params SystemRight[] systemRights);
		/// <summary>Возвращает <see cref="DataSet"/> для отчета по топливу: поиск заправок и сливов </summary>
		/// <param name="operatorID">Идентификатор оператора, от имени которого выполняется отчет</param>
		/// <param name="vehicleID">Идентификатор объекта наблюдения</param>
		/// <param name="vehicleGroupID">Идентификатор группы объектов наблюдения</param>
		/// <param name="logTimeFrom">UNIX time начала периода, за который строится отчет</param>
		/// <param name="logTimeTo">UNIX time конца периода, за который строится отчет</param>
		/// <param name="sensors">Датчики, используемые для построения отчета</param>
		/// <param name="fuelSpendStandardType">Тип норматива расхода топлива</param>
		/// <param name="cultureInfo"></param>
		DataSet GetFuelReportData(int operatorID, int? vehicleID, int? vehicleGroupID, int logTimeFrom, int logTimeTo, Enm::SensorLegend[] sensors, Enm::FuelSpendStandardType fuelSpendStandardType, CultureInfo cultureInfo);
		/// <summary> Возвращает бизнес объект наблюдения по id </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="vehicleId">ID объекта наблюдения</param>
		/// <param name="cultureInfo">Культура для локализации</param>
		/// <returns>Vehicle(id, garageNum)</returns>
		Dto::Vehicle GetVehicleById(int operatorId, int vehicleId, CultureInfo cultureInfo = null);
		/// <summary> Возвращает <see cref="DataSet"/> для отчета по топливу АЗС </summary>
		/// <param name="operatorId">Идентификатор оператора, от имени которого выполняется отчет</param>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="logTimeFrom">UNIX time начала периода, за который строится отчет</param>
		/// <param name="logTimeTo">UNIX time конца периода, за который строится отчет</param>
		/// <returns></returns>
		DataSet GetFuelStationReportData(int operatorId, int vehicleId, int logTimeFrom, int logTimeTo);
		/// <summary> Возвращает бизнес объект группы транспортных средств по идентификатору </summary>
		/// <param name="vehicleGroupId">Идентификатор группы</param>
		/// <returns></returns>
		Dto::OperatorRights.VehicleGroup GetVehicleGroupById(int vehicleGroupId);
		Dto::OperatorRights.GeozoneGroup GetZoneGroupById(int geozonegroupId);
		Dto::GeoZone GetGeoZoneById(int geozoneId);
		List<Dto::GeoZone> GetGeoZones(int operatorId, int? departmentId, bool includeVertices, int? zoneId = null, bool includeIgnored = false, bool loadSingle = false);
		List<Dto::GeoZone> GetGeoZonesByGroup(int operatorId, int geoZoneGroupId);
		List<Dto::GeoZoneForPosition> GetGeoZonesForPositions(
			IEnumerable<Dto::VehicleZoneParam> vehicleZoneParams,
			IEnumerable<Dto::VehicleLogTimeParam> vehicleLogTimeParams);
		DataSet GetAllZoneDistanceStandarts(int operatorId);
		List<Dto::DataAbsenceRecord> GetDataAbsences(int operatorID, DateTime dateFrom, DateTime dateTo, CultureInfo cultureInfo);
		List<Dto::CommercialServiceRecord> ReportCommercialServices(int operatorId, DateTime dateFrom, DateTime dateTo);
		Dto::AddRemoveServicesReportData ReportAddRemoveServices(DateTime dateFrom, DateTime dateTo, bool isDetailed, int operatorId);
		/// <summary> Проверяет логин и пароль пользователя </summary>
		/// <param name="login">Логин</param>
		/// <param name="password">Пароль</param>
		CheckPasswordResult CheckPasswordForWeb(string login, string password);
		/// <summary> Возвращает параметры необходимые для определения доступности отчетов </summary>
		/// <param name="operatorId"></param>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		Dto::ReportMenuVehiclesCapabilities GetReportMenuCapabilities(int operatorId, int? departmentId);
		/// <summary> Возвращает клиента по идентификатору </summary>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		Dto::Department GetDepartment(int departmentId);
		/// <summary> Заполнение имен тс на основе набора данных </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="vehicleIdAccessor"></param>
		/// <param name="vehicleNameAccessor"></param>
		/// <param name="operatorId"></param>
		/// <param name="cultureInfo"></param>
		void FillVehicleNames<T>(List<T> list, Expression<Func<T, int>> vehicleIdAccessor, Expression<Func<T, string>> vehicleNameAccessor, int operatorId, CultureInfo cultureInfo);
		/// <summary> Получение имени объекта </summary>
		/// <param name="operatorId"></param>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		string GetVehicleName(int? operatorId, int vehicleId);
		/// <summary> Получение имени объекта </summary>
		/// <param name="asidId"></param>
		/// <returns></returns>
		string GetAsidName(int asidId);
		/// <summary> Нужно ли включать фильтр по клиенту при получении данных </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		bool FilterByDepartment(int operatorId);
		/// <summary> Проверяет есть ли у оператора все указанные права на vehicle </summary>
		bool IsAllowedVehicleAll(int operatorId, int vehicleId, params SystemRight[] rights);
		/// <summary> Проверяет есть ли у оператора любое из указанных прав на vehicle </summary>
		bool IsAllowedVehicleAny(int operatorId, int vehicleId, params SystemRight[] rights);
		/// <summary> Возвращает список системных прав пользователя </summary>
		List<SystemRight> GetOperatorSystemRights(int operatorId);
		/// <summary> Возвращает строковое значение константы </summary>
		/// <param name="constant"> Константа </param>
		/// <returns> Строковое значение константы</returns>
		string GetConstant(Enm::Constant constant);
		/// <summary> Получить или создать общую ссылку </summary>
		/// <param name="operatorId"></param>
		/// <param name="logTimeBeg"></param>
		/// <param name="logTimeEnd"></param>
		/// <param name="vehicleId"></param>
		/// <param name="vehicleName"></param>
		/// <param name="vehicleDesc"></param>
		/// <returns></returns>
		Dto::ShareLink GetOrCreateShareLink(
			int operatorId, int logTimeBeg, int logTimeEnd, int vehicleId, string vehicleName, string vehicleDesc = null);
	}
}