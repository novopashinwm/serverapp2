﻿using System;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data
{
	[Serializable]
	public class CheckPasswordResult
	{
		public readonly bool OneTimePassword;
		public readonly int  OperatorId;
		public readonly bool IsLocked;

		public CheckPasswordResult(int operatorId, bool oneTimePassword, bool isLocked)
		{
			OperatorId      = operatorId;
			OneTimePassword = oneTimePassword;
			IsLocked        = isLocked;
		}
	}
}