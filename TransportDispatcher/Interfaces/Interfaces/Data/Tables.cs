﻿using System;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data
{
	/// <summary>
	/// Summary description for TableName.
	/// </summary>
	public class Tables
	{
		/// <summary>
		/// </summary>
		protected readonly static string[] arr_GetZones = new string[]
		{
			"ZONEGROUP",
			"GEO_ZONE", 
			"ZONEGROUP_ZONE",
			"DEPARTMENT",
			"VEHICLE",
			"VEHICLEGROUP",
			"VEHICLEGROUP_VEHICLE",
			"ZONE_VEHICLE",
			"ZONE_VEHICLEGROUP"
		};

		/// <summary>
		/// </summary>
		public static string[] GetZones
		{
			get
			{
				return arr_GetZones;
			}
		}

		/// <summary>
		/// </summary>
		protected readonly static string[] arr_GetZonesForMap = new string[]
		{
			"MAPS",
			"MAP_VERTEX",
			"ZONE_PRIMITIVE_VERTEX",
			"ZONE_PRIMITIVE",
			"GEO_ZONE_PRIMITIVE",
			"GEO_ZONE",
			"ZONES" // non deletable zones
		};

		/// <summary>
		/// Название таблиц для процедуры GetOwners, которая возвращает
		/// информацию о владельце и совладельцах
		/// </summary>
		public static string[] GetZonesForMap
		{
			get
			{
				return arr_GetZonesForMap;
			}
		}

		/// <summary>
		/// название таблиц для процедуры GetOwners, которая возвращает
		/// информацию о владельце и совладельцах
		/// </summary>
		protected readonly static string[] arr_GetOwners = new string[]
		{
			"OWNER", 
			"VEHICLE_OWNER"
		};

		/// <summary>
		/// Название таблиц для процедуры GetOwners, которая возвращает
		/// информацию о владельце и совладельцах
		/// </summary>
		public static string[] GetOwners
		{
			get
			{
				return arr_GetOwners;
			}
		}

		protected readonly static string[] arr_GetSchedulerQueue = new string[]{"SCHEDULERQUEUE", "SCHEDULEREVENT"};
		/// <summary>
		/// SCHEDULERQUEUE, SCHEDULEREVENT
		/// </summary>
		public static string[] GetSchedulerQueue
		{
			get
			{
				return arr_GetSchedulerQueue;
			}
		}

		protected readonly static string[] arr_SelectTasksFromQueue = new string[] { "SCHEDULEREVENT", "SCHEDULERQUEUE" }; //, "EMAIL_SCHEDULERQUEUE", "EMAIL" };
		public static string[] SelectTasksFromQueue
		{
			get
			{
				return arr_SelectTasksFromQueue;
			}
		}

		protected readonly static string[] arr_GetUnitNumbers = new string[]{"CONTROLLER"};
		public static string[] GetUnitNumbers
		{
			get
			{
				return arr_GetUnitNumbers;
			}
		}

		protected readonly static string[] arr_GetVehiclesForMonitoring = new string[]{
				"DEPARTMENT",	
				//"ROUTE",
				"VEHICLE", 
				/*"CONTROLLER_TYPE",*/ 
				"CONTROLLER", 
				"CONTROLLER_INFO", 
				"MEDIA", 
				"MEDIA_TYPE", 
				/*"OPS_EVENT",*/
				"VEHICLEGROUP",
				"VEHICLEGROUP_VEHICLE",
				"CONTROLLER_SENSOR_MAP"};

		public static string[] GetVehiclesForMonitoring
		{
			get
			{
				return arr_GetVehiclesForMonitoring;
			}
		}

		protected readonly static string[] arr_Analytic = new string[]{"Analytic"};
		public static string[] Analytic
		{
			get
			{
				return arr_Analytic;
			}
		}

		protected readonly static string[] arr_WayoutInfo = new string[] { "WayoutInfo" };
		public static string[] WayoutInfo
		{
			get
			{
				return arr_WayoutInfo;
			}
		}

		protected readonly static string[] arr_GetRules = new string[]
		{
			"RULE",
			"VEHICLE_RULE", "VEHICLE",
			"VEHICLEGROUP_RULE", "VEHICLEGROUP",
			"VEHICLEGROUP_VEHICLE",
			"CONTROLLER", "CONTROLLER_INFO"
		};

		public static string[] GetRules
		{
			get
			{
				return arr_GetRules;
			}
		}

		protected readonly static string[] arr_GetMessages = new string[]
		{
			"MESSAGE", "MESSAGE_OPERATOR", "VEHICLE"
		};

		public static string[] GetMessages
		{
			get
			{
				return arr_GetMessages;
			}
		}

		protected readonly static string[] arr_GetWaybillsPartPermanent = new string[]
		{
			"WAYBILLMARK", 
			"DK_SET", "DAY_KIND", "TRIP_KIND"
		};

		public static string[] GetWaybillsPartPermanent
		{
			get
			{
				return arr_GetWaybillsPartPermanent;
			}
		}

		public static string[] GetDataForController
		{
			get{return arr_GetDataForController;}
		}
		protected readonly static string[] arr_GetDataForController = new string[]
			{"CONTROLLER", /*"CONTROLLER_INFO", "CONTROLLER_STAT", */"CONTROLLER_TYPE", "VEHICLE"};

		public static string[] GetDataForControllerWithId
		{
			get{return arr_GetDataForControllerWithId;}
		}
		protected readonly static string[] arr_GetDataForControllerWithId = new string[]
			{"CONTROLLER", "CONTROLLER_INFO", "CONTROLLER_TIME", /*"CONTROLLER_STAT",*/ "CONTROLLER_TYPE", "VEHICLE"};

		public static string[] GetDataForControllers
		{
			get{return arr_GetDataForControllers;}
		}
		protected readonly static string[] arr_GetDataForControllers = new string[] { "CONTROLLER", "CONTROLLER_INFO", "CONTROLLER_TIME", "CONTROLLER_SENSOR_MAP" };
		
		public static string[] GetListsForControllers
		{
			get{return arr_GetListsForControllers;}
		}
		protected readonly static string[] arr_GetListsForControllers = new string[] { "CONTROLLER_TYPE", "VEHICLE", "CONTROLLER_SENSOR", "CONTROLLER_SENSOR_TYPE", "CONTROLLER_SENSOR_LEGEND" };
		
		protected readonly static string[] arr_GetWaybillsPartWaybillList = new string[]
		{
			"H_WAYBILL_HEADER", "H_WAYBILL",
			"WAYBILL_HEADER", "WAYBILL", "WB_TRIP", "GEO_TRIP",
			"WAYBILLHEADER_WAYBILLMARK", "DAY"
		};

		protected readonly static string[] arr_GetControllerOldFW = new string[] { "CONTROLLER_INFO" };

		public static string[] GetControllerOldFW
		{
			get { return arr_GetControllerOldFW; }
		}

		public static string[] GetWaybillsPartWaybillList
		{
			get
			{
				return arr_GetWaybillsPartWaybillList;
			}
		}

		protected readonly static string[] arr_GetWaybillsPartClosed = new string[]
		{
			"PERIOD", "TRIP", "SCHEDULE_DETAIL", "SCHEDULE", "ROUTE",
			"VEHICLE", "VEHICLEGROUP_VEHICLE", "VEHICLEGROUP", "DRIVER"
		};

		public static string[] GetWaybillsPartClosed
		{
			get
			{
				return arr_GetWaybillsPartClosed;
			}
		}

		protected readonly static string[] arr_GetWaybillsPartUnclosed = new string[]
		{
			"TRIP", "SCHEDULE_DETAIL", "SCHEDULE", "ROUTE",
			"VEHICLE"
		};

		public static string[] GetWaybillsPartUnclosed
		{
			get
			{
				return arr_GetWaybillsPartUnclosed;
			}
		}

		protected readonly static string[] arr_GET_DATA_FOR_WAYBILL_HISTORY_REPORT = new string[]{"WAYBILL_HEADER", "SCHEDULE", "ROUTE", "WAYBILL", "SCHEDULE_POINT", "ROUTE_POINT", "POINT", "DRIVER", "VEHICLE", "DRIVER_BONUS"};
		public static string[] GET_DATA_FOR_WAYBILL_HISTORY_REPORT
		{
			get
			{
				return arr_GET_DATA_FOR_WAYBILL_HISTORY_REPORT;
			}
		}
		protected readonly static string[] arr_GET_TIME_DEVIATIONS = new string[]{"WAYBILL_CALCULATED"};
		public static string[] GET_TIME_DEVIATIONS
		{
			get
			{
				return arr_GET_TIME_DEVIATIONS;
			}
		}

		protected readonly static string[] arr_GET_SECURITY = new string[]
		{
			"OPERATOR",
			"OPERATORGROUP",
			"OPERATORGROUP_OPERATOR", 
			"RIGHT",
			"RIGHT_OPERATOR",
			"RIGHT_OPERATORGROUP", 
			"DRIVER",
			"DRIVERGROUP_DRIVER",
			"DRIVERGROUP", 
			"OPERATOR_DRIVER",
			"OPERATOR_DRIVERGROUP",
			"OPERATORGROUP_DRIVER",
			"OPERATORGROUP_DRIVERGROUP", 
			"VEHICLE",
			"VEHICLEGROUP_VEHICLE",
			"VEHICLEGROUP", 
			"OPERATOR_VEHICLE",
			"OPERATOR_VEHICLEGROUP",
			"OPERATORGROUP_VEHICLE",
			"OPERATORGROUP_VEHICLEGROUP", 
			"ROUTE",
			"ROUTEGROUP_ROUTE",
			"ROUTEGROUP",
			"SCHEDULE",
			"WAYOUT",
			"OPERATOR_ROUTE",
			"OPERATOR_ROUTEGROUP",
			"OPERATORGROUP_ROUTE",
			"OPERATORGROUP_ROUTEGROUP",
			"ZONE",
			"ZONEGROUP",
			"ZONEGROUP_ZONE", 
			"OPERATOR_ZONE",
			"OPERATOR_ZONEGROUP",
			"OPERATORGROUP_ZONE",
			"OPERATORGROUP_ZONEGROUP",
			"REPORT",
			"OPERATOR_REPORT",
			"OPERATORGROUP_REPORT",
			"DEPARTMENT",
			"OPERATOR_DEPARTMENT",
			"OPERATORGROUP_DEPARTMENT"	 		
		};

		public static string[] GET_SECURITY
		{
			get
			{
				return arr_GET_SECURITY;
			}
		}

		protected readonly static string[] arr_GET_SYSTEM_RIGHTS = new string[]{"OPERATOR", "OPERATORGROUP_OPERATOR", "RIGHT_OPERATOR", "OPERATORGROUP", "RIGHT_OPERATORGROUP", "RIGHT"};
		public static string[] GET_SYSTEM_RIGHTS
		{
			get
			{
				return arr_GET_SYSTEM_RIGHTS;
			}
		}

		protected readonly static string[] arr_GET_WAYBILL_HEADER_STATUS_HISTORY = new string[]
		{
			"H_WAYBILL_HEADER",
			"TRAIL",
			"SESSION",
			"OPERATOR"
		};

		public static string[] GET_WAYBILL_HEADER_STATUS_HISTORY
		{
			get
			{
				return arr_GET_WAYBILL_HEADER_STATUS_HISTORY;
			}
		}


		#region справочник ТС

		public static string[] GetListsForVehicles
		{
			get
			{
				string[] res = {
								//"GRAPHIC",
								//"GRAPHIC_SHIFT",
								//"DRIVERGROUP",
								//"DRIVER",
								//"DRIVERGROUP_DRIVER",
								"CONTROLLER_TYPE",
								"SEATTYPE",
								"VEHICLE_KIND",
								"CONTROLLER_SENSOR",
								"CONTROLLER_SENSOR_TYPE",
								"CONTROLLER_SENSOR_LEGEND",
								"ROUTE",
								"RULE",
								"GEO_ZONE"    
							   };

				return res;
			}
		}

		[Obsolete]
		public static string[] GetDataForVehicles
		{
			get
			{
				string[] res = {
								"DEPARTMENT",
								"VEHICLEGROUP",
								"VEHICLE",
								"VEHICLEGROUP_VEHICLE",
								"DRIVER_VEHICLE",
								"CONTROLLER",
								"CONTROLLER_INFO",
								"VEHICLE_PICTURE",
								"CONTROLLER_SENSOR_MAP"
							   };

				return res;
			}
		}

		private static readonly string[] getDataForDepartmentNew =
			new string[]
				{
					"DEPARTMENT"
				};

		public static string[] GetDataForDepartmentNew
		{
			get { return Tables.getDataForDepartmentNew; }
		}

		private static readonly string[] getDataForVehicleNew =
			new string[]
				{
					"VEHICLEGROUP",
					"VEHICLE",
					"VEHICLEGROUP_VEHICLE",
					"VEHICLE_PICTURE"
				};

		public static string[] GetDataForVehicleNew
		{
			get { return Tables.getDataForVehicleNew; }
		}

		private static readonly string[] getDataForRuleNew =
			new string[]
				{
					"RULE"
				};
		 
		public static string[] GetDataForRuleNew
		{
			get { return Tables.getDataForRuleNew; }
		}

		#endregion // справочник ТС

		#region справочник ТС

		public static string[] GetListsForDrivers
		{
			get
			{
				string[] res = {
								   "DEPARTMENT"								
							   };

				return res;
			}
		}


		public static string[] GetDataForDrivers
		{
			get
			{
				string [] res = {
									"DRIVERGROUP",
									"DRIVER",
									"DRIVERGROUP_DRIVER"
								};

				return res;
			}
		}

		#endregion // справочник ТС
		
		#region Журнал

		public static string[] GetDepartment
		{
			get
			{
				string [] res = 
					{
						"ROUTE",
						"WAYOUT",
						"SCHEDULE",
						"SCHEDULE_DETAIL",
						"SCHEDULE_DETAIL_INFO",

						"CUSTOMER",
						"ORDER_TYPE",
						"ORDER",
						"ORDER_TRIP",

						"DK_SET",
						"DAY_KIND",
						"PERIOD",

						"GRAPHIC",
						"GRAPHIC_SHIFT",

						"DEPARTMENT",
						
//						"DRIVERGROUP",
						"DRIVER",
//						"DRIVERGROUP_DRIVER",
						"JOURNAL_DRIVER",

//						"VEHICLEGROUP",
						"VEHICLE",
//						"VEHICLEGROUP_VEHICLE",
						"JOURNAL_VEHICLE",
						
						"DECADE",
						"BRIGADE",
						"BRIGADE_DRIVER",
						
						"JOURNAL_DAY"
					};
				return res;
			}
			
		}
		
		public static string[] GetListsForOldJournal
		{
			get
			{
				string[] res =
					{
						"GRAPHIC",
						"GRAPHIC_SHIFT",
						"DAY_KIND",
						"DK_SET",
						"PERIOD",
						"ROUTE",
						"WAYOUT",
						"SCHEDULE",
						"SCHEDULE_DETAIL",
						"SCHEDULE_DETAIL_INFO",
						"VEHICLEGROUP",
						"VEHICLE",
						"VEHICLEGROUP_VEHICLE",
						"DRIVER",
						"DRIVER_VEHICLE",
					};

				return res;
			}
		}

		public static string[] GetDataForOldJournal
		{
			get
			{
				string[] res =
					{
						"DRIVER_WORKTIME",
						"JOURNAL_DRIVER",
						"JOURNAL_VEHICLE",
						"JOURNAL"
					};

				return res;
			}
		}
		

		#endregion Журнал

		#region Новый Ж

		/// <summary>
		/// 
		/// </summary>
		public static string[] GetDataForDepartments
		{
			get
			{
				string[] res =
					{
						"DEPARTMENT"
					};

				return res;
			}
		}

		public static string[] GetDataForSchedulesInfo
		{
			get
			{
				string[] res =
					{
						"WAYOUT",
						"SCHEDULE",
						"SCHEDULE_DETAIL",
						"SCHEDULE_DETAIL_INFO",
					};

				return res;
			}
		}

		/// <summary>
		/// Таблицы для журнала автоколонны
		/// </summary>
		public static string[] GetDataForJournals
		{
			get
			{
				string[] res =	{
									"DEPARTMENT",
									"CUSTOMER",
									"ORDER_TYPE"
								};
				
				return res;			
			}
		}
		public static string[] GetDataForDepartment
		{
			get
			{
				string[] res =
					{
						"DRIVER",
						"JOURNAL_DRIVER",

						"VEHICLE",
						"JOURNAL_VEHICLE",

						"DECADE",
					};

				return res;
			}
		}
		public static string[] GetWorkTimeForMonth
		{
			get
			{
				string[] res =
					{
						"WORK_TIME"
					};

				return res;
			}
		}

		public static string[] GetDataForDecade
		{
			get
			{
				string[] res =	{

									"BRIGADE",
									"BRIGADE_DRIVER",
									"JOURNAL_DAY",

									"ORDER",
									"ORDER_TRIP"
								};

				return res;
			}
		}

		
		#endregion // Новый Ж

		#region Заказы

		public static string[] GetListsForOrders
		{
			get
			{
				string[] Result = 
					{
						"DEPARTMENT"
					};

				return Result;
			}
		}


		public static string[] GetDataForOrders
		{
			get
			{
				string[] Result =
					{
						"ORDER_TYPE",
						"CUSTOMER",
						"ORDER",
						"ORDER_TRIP"
					};

				return Result;
			}
		}
		

		#endregion // Заказы

		#region Линейная ведомость

		public static string[] GetDataForLineRoll
		{
			get
			{
				string[] res = 
					{
						"WAYBILL_HEADER",
						"WAYBILLHEADER_WAYBILLMARK",
						"WAYBILL",
						"WB_TRIP",
						"POINT",
						"BUSSTOP",
						"DAY"
					};
				return res;
			}
		}
		public static string[] GetListsForLineRoll
		{
			get
			{
				string[] res = 
					{
						"WAYOUT",
						"DRIVER",
						"VEHICLE",
						"CONTROLLER",
						"CONTROLLER_TYPE",
						"VEHICLEGROUP",
						"VEHICLEGROUP_VEHICLE",
						"DRIVERGROUP",
						"DRIVERGROUP_DRIVER",
						"ROUTE",
						"ROUTEGROUP",
						"ROUTEGROUP_ROUTE",
						"TRIP_KIND",
						"WAYBILLMARK",
						"ROUTE_TRIP",
						"GEO_TRIP"
					};
				return res;
			}
		}

		#endregion //Линейная ведомость

		#region Наряд

		public static string[] GetDataForRoster
		{
			get
			{
				string[] res = 
					{
						"WAYBILL_HEADER",
						"WAYBILLHEADER_WAYBILLMARK",
						"WAYBILL",
						"WB_TRIP",
						"POINT",
						"BUSSTOP",
						"DAY"
					};
				return res;
			}
		}
		public static string[] GetListsForRoster
		{
			get
			{
				string[] res = 
					{
						"WAYOUT",
						"DRIVER",
						"VEHICLE",
						"CONTROLLER",
						"CONTROLLER_TYPE",
						"VEHICLEGROUP",
						"VEHICLEGROUP_VEHICLE",
						"DRIVERGROUP",
						"DRIVERGROUP_DRIVER",
						"TRIP_KIND",
						"WAYBILLMARK",
						"ROUTE_TRIP",
						"GEO_TRIP"
					};
				return res;
			}
		}

		protected readonly static string[] arr_GetListsForScheduleGen = new string[]
					{
						"ROUTE",
						"ROUTE_TRIP",
						"WAYOUT",						
						"RS",
						"RS_VARIANT",
						"RS_TYPE",
						"RS_TRIPSTYPE",
						"RS_OPERATIONSTYPE",
						"PERIOD",
						"DK_SET",
						"DAY_KIND",	
						"DAY_TYPE",	
						"DAY_TIME",
						"POINT_KIND",
						"TRIP_KIND",
						"GRAPHIC",
						"GEO_TRIP",
						"GEO_SEGMENT",						
						"GEO_SEGMENT_RUNTIME",
						"POINT",
						"BUSSTOP",
						"SCHEDULE"
					};
		
		public static string[] GetListsForScheduleGen
		{
			get { return arr_GetListsForScheduleGen; }
		}
		
		protected readonly static string[] arr_GetDataForScheduleGen = new string[]
					{
						"RS_WAYOUT",
						"RS_SHIFT",
						"RS_TRIP",
						"RS_ROUND_TRIP",
						"RS_POINT",
						"RS_TRIPTYPE",
						"RS_OPERATIONTYPE",
						"RS_PERIOD",
						"RS_RUNTIME",
						"RS_STEP",
						"RS_NUMBER"
						
		};

		public static string[] GetDataForScheduleGen
		{
			get { return arr_GetDataForScheduleGen; }
		}
		
		public static string[] GetScheduleByDate
		{
			get
			{
				string[] res = 
					{
						"SCHEDULE",
						"SCHEDULE_DETAIL",
						"TRIP"
					};
				
				return res;
			}
		}

		#endregion // Наряд

		#region Календарь

		public static string[] GetDataForCalendar
		{
			get
			{
				string[] res =
					{
						"ROUTEGROUP",
						"ROUTE",
						"ROUTEGROUP_ROUTE",

						"DAY_KIND",
						"DK_SET",
						"PERIOD",
						"DEPARTMENT"
					};
				return res;
			}
		}

		#endregion Календарь

		#region Графики

		public static string[] GetDataForGraphic
		{
			get
			{
				string[] res =
					{
						"GRAPHIC",
						"GRAPHIC_SHIFT",
						"DEPARTMENT"
					};
				return res;
			}			

		}

		#endregion Графики

		public static string[] Schedule
		{
			get
			{
				string[] res = 
					{
						"SCHEDULE"
					};
				return res;
			}
		}

		protected readonly static string[] arr_GetCounts = new string[]
		{
			"COUNT_GPRS", "COUNT_GSM", "COUNT_SMS", 
			"OPERATOR", 
			"VEHICLE", "CONTROLLER"
		};

		public static string[] GetCounts
		{
			get
			{
				return arr_GetCounts;
			}
		}

		protected readonly static string[] arr_GetDataForOperatorPropertiesForm = new string[]{
			"RIGHT", "OPERATOR", "OPERATORGROUP", "OPERATORGROUP_OPERATOR", 
			"RIGHT_OPERATOR", "RIGHT_OPERATORGROUP", "VEHICLE", "VEHICLEGROUP", 
			"VEHICLEGROUP_VEHICLE", "OPERATOR_VEHICLE", "OPERATOR_VEHICLEGROUP", 
			"OPERATORGROUP_VEHICLE", "OPERATORGROUP_VEHICLEGROUP", "ROUTE", "ROUTEGROUP", 
			"ROUTEGROUP_ROUTE", "OPERATOR_ROUTE", "OPERATOR_ROUTEGROUP", "OPERATORGROUP_ROUTE", 
			"OPERATORGROUP_ROUTEGROUP", "OPERATOR_DRIVER", "OPERATOR_DRIVERGROUP", 
			"OPERATORGROUP_DRIVER", "OPERATORGROUP_DRIVERGROUP", "DRIVER", "DRIVERGROUP", 
			"DRIVERGROUP_DRIVER", "REPORT", "OPERATOR_REPORT", "OPERATORGROUP_REPORT",
			"DEPARTMENT", "OPERATOR_DEPARTMENT", "OPERATORGROUP_DEPARTMENT",
			"GEO_ZONE", "ZONEGROUP", "ZONEGROUP_ZONE", 
			"OPERATOR_ZONE", "OPERATOR_ZONEGROUP", "OPERATORGROUP_ZONE", "OPERATORGROUP_ZONEGROUP"
		}; 
		public static string[] GetDataForOperatorPropertiesForm
		{
			get
			{
				return arr_GetDataForOperatorPropertiesForm;
			}
		}
		
		protected readonly static string[] arr_GetDataForRoutePropertiesCreate = new string[]{"POINT", "DAY_KIND", "POINT_KIND", "DK_SET", "TRIP_KIND", "GRAPHIC" };
		public static string[] GetDataForRoutePropertiesCreate
		{
			get
			{
				return arr_GetDataForRoutePropertiesCreate;
			}
		}
		protected readonly static string[] arr_GetDataForRoutePropertiesEdit = new string[]{"ROUTE",
			/* "ROUTE_POINT",*/ "POINT",
		   "SCHEDULE",
		   "DAY_KIND",
		   "POINT_KIND",
		   "DK_SET",
		   "TRIP_KIND",
		   "WAYOUT",
		   "GRAPHIC",
		   "ROUTE_TRIP",
		   "GEO_TRIP",
		   "GEO_SEGMENT",
		   "SEASON",
		   "DAY_TYPE",
		   "DAY_TIME",
		   "GEO_SEGMENT_RUNTIME",
		   "FACTOR_VALUES",
		   "FACTORS",
			"BUSSTOP", 
			"PERIOD",
			"SCHEDULE_COUNT",
			"SCHEDULE_DETAIL",
			"TRIP",
			"SCHEDULE_POINT"
		};
		public static string[] GetDataForRoutePropertiesEdit
		{
			get
			{
				return arr_GetDataForRoutePropertiesEdit;
			}
		}

		protected readonly static string[] arr_GetDataForControllerFuel = new string[]{"CONTROLLER_STAT"};
		public static string[] GetDataForControllerFuel
		{
			get
			{
				return arr_GetDataForControllerFuel;
			}
		}

		protected readonly static string[] arr_GetDataForFactors = new string[]{"POINT", "GEO_TRIP", "GEO_SEGMENT", "FACTORS", "SEASON" , "DAY_TYPE", "DAY_TIME", "FACTOR_VALUES"};
		public static string[] GetDataForFactors
		{
			get{return arr_GetDataForFactors;}
		}

		protected readonly static string[] arr_GetDataForRoutes = new string[] { "ROUTE", "GEO_TRIP", "POINT", "BUSSTOP", "DEPARTMENT" };
		public static string[] GetDataForRoutes
		{
			get
			{
				return arr_GetDataForRoutes;
			}
		}
		protected readonly static string[] arr_GetDataForRoutesAndPoints = new string[]{"ROUTE", "ROUTE_POINT", "POINT", "SCHEDULE", "ROUTE_TRIP_GEO_TRIP"};
		public static string[] GetDataForRoutesAndPoints
		{
			get
			{
				return arr_GetDataForRoutesAndPoints;
			}
		}
//		protected readonly static string[] arr_GetDataForSchedulePropertiesDialog = new string[]{"DAY_KIND", "TRIP_KIND", "SCHEDULE", "ROUTE", "SCHEDULE_DETAIL", "SCHEDULE_POINT", "ROUTE_POINT", "POINT", "TRIP"};
		protected readonly static string[] arr_GetDataForSchedulePropertiesDialog =
			new string[] { "TRIP_KIND", "SCHEDULE_DETAIL", "TRIP", "SCHEDULE_POINT", "ROUTE_TRIP", "GEO_TRIP", "GEO_SEGMENT"/*, "ROUTE_POINT"*/};
		public static string[] GetDataForSchedulePropertiesDialog
		{
			get
			{
				return arr_GetDataForSchedulePropertiesDialog;
			}
		}
		protected readonly static string[] arr_GetDataForStatusBar = new string[]{"OPERATOR"};
		public static string[] GetDataForStatusBar
		{
			get
			{
				return arr_GetDataForStatusBar;
			}
		}

		protected readonly static string[] arr_GetOperatorProfile = new string[]{"OPERATOR_PROFILE"};
		public static string[] GetOperatorProfile
		{
			get
			{
				return arr_GetOperatorProfile;
			}
		}

		protected readonly static string[] arr_GetDataForVehiclePropertiesEdit = new string[]
			{
				"VEHICLE", 
				"DRIVER_VEHICLE",
				"OWNER"
			};
		public static string[] GetDataForVehiclePropertiesEdit
		{
			get
			{
				string[] arr1 = GetDataForVehiclePropertiesNew;
				string[] arr2 = arr_GetDataForVehiclePropertiesEdit;
				string[] res = new string[arr1.Length + arr2.Length];
				arr1.CopyTo(res, 0);
				arr2.CopyTo(res, arr1.Length);
				return res;
			}
		}
		protected readonly static string[] arr_GetDataForVehiclePropertiesNew = new string[]
		{
			"DRIVER", 
			"CONTROLLER_TYPE",
			"GRAPHIC", 
			"GRAPHIC_SHIFT",
			"SEATTYPE"
		};
		public static string[] GetDataForVehiclePropertiesNew
		{
			get
			{
				return arr_GetDataForVehiclePropertiesNew;
			}
		}

		protected readonly static string[] arr_GetDayKind = new string[]{"DAY_KIND"};
		public static string[] GetDayKind
		{
			get
			{
				return arr_GetDayKind;
			}
		}
		protected readonly static string[] arr_GetDayKind1 = new string[]{"DAY_KIND"};
		public static string[] GetDayKind1
		{
			get
			{
				return arr_GetDayKind1;
			}
		}
		protected readonly static string[] arr_GetDayKinds = new string[]{"DAY_KIND"};
		public static string[] GetDayKinds
		{
			get
			{
				return arr_GetDayKinds;
			}
		}
		protected readonly static string[] arr_GetDriverGroupsAndDrivers = new string[]{"DRIVERGROUP", "DRIVER", "DRIVERGROUP_DRIVER", "DEPARTMENT"};
		public static string[] GetDriverGroupsAndDrivers
		{
			get
			{
				return arr_GetDriverGroupsAndDrivers;
			}
		}
		protected readonly static string[] arr_GetFreeSchedules = new string[]{"FREE_SCHEDULE"};
		public static string[] GetFreeSchedules
		{
			get
			{
				return arr_GetFreeSchedules;
			}
		}
		protected readonly static string[] arr_GetHistoryForWaybill = new string[]{"WAYBILL_HEADER", "WAYBILL", "WB_TRIP", "SCHEDULE", "SCHEDULE_DETAIL", "TRIP", "SCHEDULE_POINT", "ROUTE", "ROUTE_POINT", "POINT", "DRIVER", "VEHICLE", "TRIP_KIND"};
		public static string[] GetHistoryForWaybill
		{
			get
			{
				return arr_GetHistoryForWaybill;
			}
		}
		protected readonly static string[] arr_GetLastPosition = new string[]{"MONITOREE_LOG"};
		public static string[] GetLastPosition
		{
			get
			{
				return arr_GetLastPosition;
			}
		}

		protected readonly static string[] arr_GetDataForWeb = new string[] { "ALLOW_VEHICLE", "VEHICLE_PROFILE", "VEHICLE_CAPABILITY" };
		public static string[] GetDataForWeb 
		{
			get
			{
				return arr_GetDataForWeb ;
			}
		}
		protected readonly static string[] arr_GetLastPositions = new string[]{"MONITOREE_LOG"};
		public static string[] GetLastPositions
		{
			get
			{
				return arr_GetLastPositions;
			}
		}
		protected readonly static string[] arr_GetLog = new string[]{ "VEHICLE", "MONITOREE_LOG" };
		public static string[] GetLog
		{
			get
			{
				return arr_GetLog;
			}
		}
		protected readonly static string[] arr_GetLogForEmulator = new string[]{"MONITOREE_LOG"};
		public static string[] GetLogForEmulator
		{
			get
			{
				return arr_GetLogForEmulator;
			}
		}
		protected readonly static string[] arr_GetOperator = new string[]{"OPERATOR"};
		public static string[] GetOperator
		{
			get
			{
				return arr_GetOperator;
			}
		}
		protected readonly static string[] arr_GetOperatorGroupsAndOperators = new string[]{"OPERATORGROUP", "OPERATOR", "OPERATORGROUP_OPERATOR"};
		public static string[] GetOperatorGroupsAndOperators
		{
			get
			{
				return arr_GetOperatorGroupsAndOperators;
			}
		}
		protected readonly static string[] arr_GetPeriods = new string[]{"PERIOD"};
		public static string[] GetPeriods
		{
			get
			{
				return arr_GetPeriods;
			}
		}
		protected readonly static string[] arr_GetRoster = new string[]{"ROSTER"};
		public static string[] GetRoster
		{
			get
			{
				return arr_GetRoster;
			}
		}
		protected readonly static string[] arr_GetRouteForVehicle = new string[]{"SCHEDULE"};
		public static string[] GetRouteForVehicle
		{
			get
			{
				return arr_GetRouteForVehicle;
			}
		}
		protected readonly static string[] arr_GetRouteGroupsAndRoutes = new string[]{"ROUTE", "ROUTEGROUP", "ROUTEGROUP_ROUTE"};
		public static string[] GetRouteGroupsAndRoutes
		{
			get
			{
				return arr_GetRouteGroupsAndRoutes;
			}
		}
		protected readonly static string[] arr_GetSchedule = new string[]{"ROUTE-ROUTE_POINT", "ROUTE-SCHEDULE"};
		public static string[] GetSchedule
		{
			get
			{
				return arr_GetSchedule;
			}
		}
		protected readonly static string[] arr_GetScheduleForWaybill = new string[]{"SCHEDULE", "SCHEDULE_DETAIL", "ROUTE_POINT", "TRIP", "SCHEDULE_POINT"};
		public static string[] GetScheduleForWaybill
		{
			get
			{
				return arr_GetScheduleForWaybill;
			}
		}
		protected readonly static string[] arr_SpecialTripsScheduleGet = new string[]{"TRIP", "SCHEDULE_POINT"};
		public static string[] SpecialTripsScheduleGet
		{
			get
			{
				return arr_SpecialTripsScheduleGet;
			}
		}
		protected readonly static string[] arr_GetScheduleInfo = 
			new string[]{"SCHEDULE-WH-VEHICLE", "TRIP-WAYBILL-WBTRIP"};
		public static string[] GetScheduleInfo
		{
			get
			{
				return arr_GetScheduleInfo;
			}
		}
		protected readonly static string[] arr_GetSchedulePassage = new string[]{"SCHEDULE_PASSAGE"};
		public static string[] GetSchedulePassage
		{
			get
			{
				return arr_GetSchedulePassage;
			}
		}
		protected readonly static string[] arr_GetSpecialDays = new string[]{"SPECIAL_DAY"};
		public static string[] GetSpecialDays
		{
			get
			{
				return arr_GetSpecialDays;
			}
		}

		protected readonly static string[] arr_GetUnclosed = new string[]
		{
			"ROUTE", 
			"SCHEDULE", 
			"SCHEDULE_DETAIL", 
			"TRIP", 
			"TRIP_KIND", 
			"VEHICLE"
		};

		public static string[] GetUnclosed
		{
			get
			{
				return arr_GetUnclosed;
			}
		}
		protected readonly static string[] arr_GetVehicleGroupsAndVehicles = new string[]
			{
				"VEHICLEGROUP", "VEHICLE", "VEHICLEGROUP_VEHICLE", "CONTROLLER_TYPE", "CONTROLLER", "SEATTYPE", "VEHICLE_KIND", "DEPARTMENT" 
			};
		public static string[] GetVehicleGroupsAndVehicles
		{
			get
			{
				return arr_GetVehicleGroupsAndVehicles;
			}
		}
		protected readonly static string[] arr_GetVehicle = new string[]{"VEHICLE", "CONTROLLER"};
		public static string[] GetVehicle
		{
			get
			{
				return arr_GetVehicle;
			}
		}
		protected readonly static string[] arr_GetVehiclesForRoute = new string[]{"WAYBILL_HEADER"};
		public static string[] GetVehiclesForRoute
		{
			get
			{
				return arr_GetVehiclesForRoute;
			}
		}
		protected readonly static string[] arr_GetVehiclesTimeInForAllRoutes = new string[]{"VEHICLES_TIME_IN"};
		public static string[] GetVehiclesTimeInForAllRoutes
		{
			get
			{
				return arr_GetVehiclesTimeInForAllRoutes;
			}
		}
		protected readonly static string[] arr_WaybillInTimesGet = new string[]{"WAYBILL_SCHEDULE"};
		public static string[] WaybillInTimesGet
		{
			get
			{
				return arr_WaybillInTimesGet;
			}
		}
		protected readonly static string[] arr_SubstitutionsInfoGet = new string[]{"SubstitutionsInfo"};
		public static string[] SubstitutionsInfoGet
		{
			get
			{
				return arr_SubstitutionsInfoGet;
			}
		}
		protected readonly static string[] arr_VehiclesListByStateGet = new string[]{"VehiclesListByState"};
		public static string[] VehiclesListByStateGet
		{
			get
			{
				return arr_VehiclesListByStateGet;
			}
		}
		protected readonly static string[] arr_DriversListByStateGet = new string[]{"DriversListByState"};
		public static string[] DriversListByStateGet
		{
			get
			{
				return arr_DriversListByStateGet;
			}
		}
		protected readonly static string[] arr_VehiclesCommonListByStateGet = new string[]{"VehiclesCommonListByState"};
		public static string[] VehiclesCommonListByStateGet
		{
			get
			{
				return arr_VehiclesCommonListByStateGet;
			}
		}
		protected readonly static string[] arr_GPRSAccessibilityStatisticsGet = new string[]{"GPRSAccessibilityStatistics"};
		public static string[] GPRSAccessibilityStatisticsGet
		{
			get
			{
				return arr_GPRSAccessibilityStatisticsGet;
			}
		}
		protected readonly static string[] arr_GetDataForPointPropertiesDialog = new string[] { "POINT", "POINT_KIND", "MAP_VERTEX" };
		public static string[] GetDataForPointPropertiesDialog
		{
			get
			{
				return arr_GetDataForPointPropertiesDialog;
			}
		}
		protected readonly static string[] arr_GetReservedWaybills = new string[]{"RESERVED_WH", "WAYBILL_DRIVER"};
		public static string[] GetReservedWaybills
		{
			get
			{
				return arr_GetReservedWaybills;
			}
		}
		protected readonly static string[] arr_GetScheduleTrips = new string[]{"SCHEDULE_TRIP"};
		public static string[] GetScheduleTrips
		{
			get
			{
				return arr_GetScheduleTrips;
			}
		}
		protected readonly static string[] arr_GetWaybillStatus = new string[]{"TRIP_KIND", "WORKTIME_STATUS_TYPE", "WORKTIME_REASON"};
		public static string[] GetWaybillStatus
		{
			get
			{
				return arr_GetWaybillStatus;
			}
		}													 
		
		protected readonly static string[] arr_GetMoveDetailHistory = new string[]{"CountAll", "CountInterval", "Interval", "Drivers", "MoveHistory"};
		public static string[] GetMoveDetailHistory 
		{
			get
			{
				return arr_GetMoveDetailHistory;
			}
		}
		protected readonly static string[] arr_GetMoveGroupDay = new [] { "monitoringObject", "position" };
		public static string[] GetMoveGroupDay
		{
			get
			{
				return arr_GetMoveGroupDay;
			}
		}
		protected readonly static string[] arr_GetMoveHistoryDistance = new string[]{"Interval", "Drivers", "MoveHistoryDistance"};
		public static string[] GetMoveHistoryDistance 
		{
			get
			{
				return arr_GetMoveHistoryDistance;
			}
		}
		protected readonly static string[] arr_GetMoveHistoryDrivers = new string[]{"Interval", "Drivers"};
		public static string[] GetMoveHistoryDrivers 
		{
			get
			{
				return arr_GetMoveHistoryDrivers;
			}
		}
		protected readonly static string[] arr_ConstantsGet = new string[] {"ConstantsList"};
		public static string[] ConstantsGet 
		{
			get
			{
				return arr_ConstantsGet;
			}
		}
		protected readonly static string[] arr_UsefullWaybilTripsCount = new string[]{"UsefullWaybilTrips"};
		public static string[] UsefullWaybilTripsCount 
		{
			get
			{
				return arr_UsefullWaybilTripsCount;
			}
		}
		protected readonly static string[] arr_MotorcadeAdvancedDataGet = new string[]{"VehiclesCommonCount", "VehiclesWithWaybillsCount", "OrderedVehiclesCount", "OnlineVehiclesCount", "SchedulesPlannedCount", "SchedulesFactualCount", "PDTechnicalOutsCount", "PDPlannedTripsCount", "PDLostTripsCount"};
		public static string[] MotorcadeAdvancedDataGet 
		{
			get
			{
				return arr_MotorcadeAdvancedDataGet;
			}
		}
		protected readonly static string[] arr_GPRSInaccessibilityDataGet = new string[]{"GPRSInaccessibilityData"};
		public static string[] GPRSInaccessibilityDataGet 
		{
			get
			{
				return arr_GPRSInaccessibilityDataGet;
			}
		}
		protected readonly static string[] arr_OutsAndRejectsCountGet = new string[]{"OutsCount", "RejectsCount"};
		public static string[] OutsAndRejectsCountGet 
		{
			get
			{
				return arr_OutsAndRejectsCountGet;
			}
		}

		protected readonly static string[] arr_GetAllStation = new string[]{"POINT"};
		public static string[] GetAllStation  
		{
			get
			{
				return arr_GetAllStation ;
			}
		}

		protected readonly static string[] arr_GetDataForGeoTrip = new string[]{"ROUTE", "ROUTE_TRIP", "GEO_TRIP", "GEO_SEGMENT", "UNSIGNPOINT"};
		public static string[] GetDataForGeoTrip   
		{
			get
			{
				return arr_GetDataForGeoTrip ;
			}
		}
		
		protected readonly static string[] arr_GetTripInRoute= new string[]{"Geo_TRIP"};
		public static string[] GetTripInRoute   
		{
			get
			{
				return arr_GetTripInRoute;
			}
		}

		protected readonly static string[] arr_VehiclesStatusesListGet = new string[] {"VehiclesStatusesList"};
		public static string[] VehiclesStatusesListGet 
		{
			get
			{
				return arr_VehiclesStatusesListGet;
			}
		}
		protected readonly static string[] arr_ActiveMotorcadeNamesByDateGet = new string[] { "VEHICLEGROUP" }; //{"ActiveMotorcadeNames"};
		public static string[] ActiveMotorcadeNamesByDateGet 
		{ 
			get
			{
				return arr_ActiveMotorcadeNamesByDateGet;
			}
		}

		protected readonly static string[] arr_MotorcadeNamesGet = new string[] { "VEHICLEGROUP" }; //{"MotorcadeNames"}; 
		public static string[] MotorcadeNamesGet 
		{
			get
			{
				return arr_MotorcadeNamesGet;
			}
		}

		protected readonly static string[] arr_ActiveControllerTypeNamesGet = new string[] {"ActiveControllerTypeNames"};
		public static string[] ActiveControllerTypeNamesGet
		{
			get
			{
				return arr_ActiveControllerTypeNamesGet;
			}
		}
		protected readonly static string[] arr_WaybillDataByIDGet = new string[] {"WaybillDataByID"};
		public static string[] WaybillDataByIDGet 
		{
			get
			{
				return arr_WaybillDataByIDGet;
			}
		}
		protected readonly static string[] arr_DriversShiftsInfoGet = new string[] {"DriversShiftsInfo"};
		public static string[] DriversShiftsInfoGet 
		{
			get
			{
				return arr_DriversShiftsInfoGet;
			}
		}
		protected readonly static string[] arr_ScheduleByParametersGet = new string[] {"ScheduleByParameters"};
		public static string[] ScheduleByParametersGet 
		{
			get
			{
				return arr_ScheduleByParametersGet;
			}
		}
		protected readonly static string[] arr_ScheduleReportParametersRefresh = new string[] {/*"RouteNumbersList"*/"Route", "PeriodNamesList", "DayTypesList", "ExitNumbersList"};
		public static string[] ScheduleReportParametersRefresh 
		{
			get
			{
				return arr_ScheduleReportParametersRefresh;
			}
		}
		protected readonly static string[] arr_WaybillMarksGet = new string[] {"WaybillMarksList"};
		public static string[] WaybillMarksGet 
		{
			get
			{
				return arr_WaybillMarksGet;
			}
		}
		protected readonly static string[] arr_EndStationsListGet = new string[] {"EndStationsList"};
		public static string[] EndStationsListGet 
		{
			get
			{
				return arr_EndStationsListGet;
			}
		}
		protected readonly static string[] arr_StationsListGet = new string[] {"StationsList"};
		public static string[] StationsListGet 
		{
			get
			{
				return arr_StationsListGet;
			}
		}
		protected readonly static string[] arr_MovementIntervalsGet = new string[] {"TheoreticalMovementIntervals", "FactualMovementIntervals"};
		public static string[] MovementIntervalsGet 
		{
			get
			{
				return arr_MovementIntervalsGet;
			}
		}
		protected readonly static string[] arr_MovementIntervalsThroughPpointGet = new string[] {"MovementIntervalsThroughPpoint"};
		public static string[] MovementIntervalsThroughPpointGet 
		{
			get
			{
				return arr_MovementIntervalsThroughPpointGet;
			}
		}
		protected readonly static string[] arr_TripsStatisticsByRoutesGet = new string[] {"RoutesFullList", "TripsList"};
		public static string[] TripsStatisticsByRoutesGet 
		{
			get
			{
				return arr_TripsStatisticsByRoutesGet;
			}
		}
		protected readonly static string[] arr_TripsByRoutesAndTimeGet = new string[] {"FactualTripsData", "RoutesFullList", "PlannedTripsData"};
		public static string[] TripsByRoutesAndTimeGet 
		{
			get
			{
				return arr_TripsByRoutesAndTimeGet;
			}
		}

		protected readonly static string[] arr_GetMedia = new string[]{"GetMedia"};
		public static string[] GetMedia
		{
			get
			{
				return arr_GetMedia;
			}
		}

		protected readonly static string[] arr_GetVehicles = new string[]{"VEHICLE", "CONTROLLER"};
		public static string[] GetVehicles
		{
			get
			{
				return arr_GetVehicles;
			}
		}
		protected readonly static string[] arr_NewSelectCommand = new string[]{"NewSelectCommand"};
		public static string[] NewSelectCommand
		{
			get
			{
				return arr_NewSelectCommand;
			}
		}
		protected readonly static string[] arr_GetRouteInfo = new string[]{"RouteInfo", "RouteType"};
		public static string[] GetRouteInfo
		{
			get
			{
				return arr_GetRouteInfo;
			}
		}
		protected readonly static string[] arr_GetRoute = new string[]{"Route"};
		public static string[] GetRoute
		{
			get
			{
				return arr_GetRoute;
			}
		}

		protected readonly static string[] arr_GetWaybillsByDate = new string[]
		{
			"DK_SET", "DAY_KIND", "PERIOD", 
			"SCHEDULE", "SCHEDULE_DETAIL", "TRIP",
			"WAYBILL_HEADER", "WAYBILL", "WB_TRIP",
			"WAYBILLHEADER_WAYBILLMARK", "WAYBILLMARK"
		};

		public static string[] GetWaybillsByDate
		{
			get
			{
				return arr_GetWaybillsByDate;
			}
		}

		protected readonly static string[] arr_SearchWaybills = new string[]
		{
			"WAYBILL_HEADER",
			"WAYBILL", 
			"WB_TRIP"
		};

		public static string[] SearchWaybills
		{
			get
			{
				return arr_SearchWaybills;
			}
		}

		/// <summary>
		/// название таблиц для процедуры WaybillNewIDGet, которая возвращает ID путевого листа по номеру и дате
		/// </summary>
		protected readonly static string[] arr_WaybillNewIDGet = new string[] {"WAYBILL_HEADER"};
		public static string[] WaybillNewIDGet
		{
			get
			{
				return arr_WaybillNewIDGet;
			}
		}

		/// <summary>
		/// название таблиц для процедуры AnaliticsOutputsGet, которая возвращает список выходов по ID маршрута и дате
		/// </summary>
		protected readonly static string[] arr_AnaliticsOutputsGet = new string[] {"SCHEDULE"};
		public static string[] AnaliticsOutputsGet
		{
			get
			{
				return arr_AnaliticsOutputsGet;
			}
		}

		/// <summary>
		/// название таблиц для процедуры RoutesListThroughPointGet, которая возвращает список маршрутов по ID точки.
		/// </summary>
		protected readonly static string[] arr_RoutesListThroughPointGet = new string[] { "Route" };//{"RoutesListThroughPoint"};
		public static string[] RoutesListThroughPointGet
		{
			get
			{
				return arr_RoutesListThroughPointGet;
			}
		}

		protected readonly static string[] arr_GetFuelStat = new string[] {"FUEL_STAT"};
		public static string[] GetFuelStat
		{
			get
			{
				return arr_GetFuelStat;
			}
		}

		/// <summary>
		/// название таблиц для процедуры MissedTripsDetailGet, которая возвращает данные для построения отчета о пропущенных и забракованных рейсах, на укзанную дату
		/// </summary>
		protected readonly static string[] arr_MissedTripsDetailGet = new string[] 
		{   
			"WAYBILL_HEADER" //, ???
		}; 
		public static string[] MissedTripsDetailGet
		{
			get
			{
				return arr_MissedTripsDetailGet;
			}
		}
		/// <summary>
		/// название таблиц для процедуры GetOperatorsSessionByDate, которая возвращает
		/// список сессий оператора за указанный промежуток времени
		/// </summary>
		protected readonly static string[] arr_GetOperatorsSessionByDate = new string[] 
		{   
			"SESSION_STAT" //, ???
		}; 
		public static string[] GetOperatorsSessionByDate
		{
			get
			{
				return arr_GetOperatorsSessionByDate;
			}
		}
		/// <summary>
		/// название таблиц для процедуры GetHTables, которая возвращает
		/// список H-таблиц
		/// </summary>
		protected readonly static string[] arr_GetHTables = new string[] 
		{   
			"H_TABLES"
		}; 
		public static string[] GetHTables
		{
			get
			{
				return arr_GetHTables;
			}
		}
		/// <summary>
		/// название таблиц для процедуры GetOpearatorActions, которая возвращает
		/// список действий оператора
		/// </summary>
		protected readonly static string[] arr_GetOpearatorActions = new string[] 
		{   
			"ACTIONS"
		}; 
		public static string[] GetOpearatorActions
		{
			get
			{
				return arr_GetOpearatorActions;
			}
		} 

		/// <summary>
		/// название таблиц для процедуры GetGuidsReportsForUser, которая возвращает
		/// все или закрепленные за пользователем GUID-ы отчетов по заданному ID оператора
		/// </summary> 
		protected readonly static string[] arr_GetGuidsReportsForUser = new string[]{"REPORT"};
		public static string[] GetGuidsReportsForUser 
		{
			get
			{
				return arr_GetGuidsReportsForUser;
			}
		}
		


		protected readonly static string[] arr_GetWorkstationsList = new string[]{"WORKSTATION", "PHONE_GARAGE"};
		/// <summary>
		/// Список рабочих станций и телефонов для оператора
		/// </summary>
		public static string[] GetWorkstationsList 
		{
			get
			{
				return arr_GetWorkstationsList;
			}
		}

		protected readonly static string[] arr_GetMessageList = new string[] { "MESSAGE" };

		/// <summary>
		/// Список сообщений принятых/отправленных оператором
		/// </summary>
		public static string[] GetMessageList
		{
			get
			{
				return arr_GetMessageList;
			}
		}

		protected readonly static string[] arr_GetDriverMsgTemplate = new string[] { "DRIVER_MSG_TEMPLATE" };

		/// <summary>
		/// Список шаблонов сообщений водителям
		/// </summary>
		public static string[] GetDriverMsgTemplate
		{
			get
			{
				return arr_GetDriverMsgTemplate;
			}
		}

		protected readonly static string[] arr_GetTaskLog = new string[] {"TASK_PROCESSOR_LOG"};

		/// <summary>
		/// Название таблиц журнала автозапроса
		/// </summary>
		public static string[] GetTaskLog
		{
			get { return arr_GetTaskLog; }
		}

		/// <summary>
		/// Названия таблиц для сообщений
		/// </summary>
		private static readonly string[] arr_GetTableNameForMessage = new string[] {
			"MESSAGE",
			"MESSAGE_FIELD",
			"MESSAGE_TEMPLATE",
			"MESSAGE_TEMPLATE_FIELD",
			"MESSAGE_OPERATOR",
			"MESSAGE_BOARD",
			"DOTNET_TYPE"
		};
		public static string[] GetTableNameForMessage
		{
			get
			{
				return arr_GetTableNameForMessage;
			}
		}

		public static readonly string[] MlpRequestsReport = {"RESULT"};
	}
}