using System;

namespace FORIS.TSS.BusinessLogic
{
    [Serializable]
    public struct BladingPrimaryKey
    {
        public System.Int32 BladingId;
        public BladingPrimaryKey(System.Int32 BladingId)
        {
            this.BladingId = BladingId;
        }
        public object[] GetObjects()
        {
            return new object[]{BladingId};
        }
    }
    [Serializable]
    public struct BladingTypePrimaryKey
    {
        public System.Int32 BladingTypeId;
        public BladingTypePrimaryKey(System.Int32 BladingTypeId)
        {
            this.BladingTypeId = BladingTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{BladingTypeId};
        }
    }
    [Serializable]
    public struct BrigadePrimaryKey
    {
        public System.Int32 BrigadeId;
        public BrigadePrimaryKey(System.Int32 BrigadeId)
        {
            this.BrigadeId = BrigadeId;
        }
        public object[] GetObjects()
        {
            return new object[]{BrigadeId};
        }
    }
    [Serializable]
    public struct BrigadeDriverPrimaryKey
    {
        public System.Int32 BrigadeDriverId;
        public BrigadeDriverPrimaryKey(System.Int32 BrigadeDriverId)
        {
            this.BrigadeDriverId = BrigadeDriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{BrigadeDriverId};
        }
    }
    [Serializable]
    public struct BusstopPrimaryKey
    {
        public System.Int32 BusstopId;
        public BusstopPrimaryKey(System.Int32 BusstopId)
        {
            this.BusstopId = BusstopId;
        }
        public object[] GetObjects()
        {
            return new object[]{BusstopId};
        }
    }
    [Serializable]
    public struct CalPrimaryKey
    {
        public System.DateTime Date;
        public CalPrimaryKey(System.DateTime Date)
        {
            this.Date = Date;
        }
        public object[] GetObjects()
        {
            return new object[]{Date};
        }
    }
    [Serializable]
    public struct CalendarDayPrimaryKey
    {
        public System.Int32 CalendarDayId;
        public CalendarDayPrimaryKey(System.Int32 CalendarDayId)
        {
            this.CalendarDayId = CalendarDayId;
        }
        public object[] GetObjects()
        {
            return new object[]{CalendarDayId};
        }
    }
    [Serializable]
    public struct ConstantsPrimaryKey
    {
        public System.String Name;
        public ConstantsPrimaryKey(System.String Name)
        {
            this.Name = Name;
        }
        public object[] GetObjects()
        {
            return new object[]{Name};
        }
    }
    [Serializable]
    public struct ContractorPrimaryKey
    {
        public System.Int32 ContractorId;
        public ContractorPrimaryKey(System.Int32 ContractorId)
        {
            this.ContractorId = ContractorId;
        }
        public object[] GetObjects()
        {
            return new object[]{ContractorId};
        }
    }
    [Serializable]
    public struct ContractorCalendarPrimaryKey
    {
        public System.Int32 ContractorCalendarId;
        public ContractorCalendarPrimaryKey(System.Int32 ContractorCalendarId)
        {
            this.ContractorCalendarId = ContractorCalendarId;
        }
        public object[] GetObjects()
        {
            return new object[]{ContractorCalendarId};
        }
    }
    [Serializable]
    public struct ControllerPrimaryKey
    {
        public System.Int32 ControllerId;
        public ControllerPrimaryKey(System.Int32 ControllerId)
        {
            this.ControllerId = ControllerId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerId};
        }
    }
    [Serializable]
    public struct ControllerInfoPrimaryKey
    {
        public System.Int32 ControllerId;
        public ControllerInfoPrimaryKey(System.Int32 ControllerId)
        {
            this.ControllerId = ControllerId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerId};
        }
    }
    [Serializable]
    public struct ControllerSensorPrimaryKey
    {
        public System.Int32 ControllerSensorId;
        public ControllerSensorPrimaryKey(System.Int32 ControllerSensorId)
        {
            this.ControllerSensorId = ControllerSensorId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorId};
        }
    }
    [Serializable]
    public struct ControllerSensorLegendPrimaryKey
    {
        public System.Int32 ControllerSensorLegendId;
        public ControllerSensorLegendPrimaryKey(System.Int32 ControllerSensorLegendId)
        {
            this.ControllerSensorLegendId = ControllerSensorLegendId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorLegendId};
        }
    }
    [Serializable]
    public struct ControllerSensorMapPrimaryKey
    {
        public System.Int32 ControllerSensorMapId;
        public ControllerSensorMapPrimaryKey(System.Int32 ControllerSensorMapId)
        {
            this.ControllerSensorMapId = ControllerSensorMapId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorMapId};
        }
    }
    [Serializable]
    public struct ControllerSensorTypePrimaryKey
    {
        public System.Int32 ControllerSensorTypeId;
        public ControllerSensorTypePrimaryKey(System.Int32 ControllerSensorTypeId)
        {
            this.ControllerSensorTypeId = ControllerSensorTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorTypeId};
        }
    }
    [Serializable]
    public struct ControllerTimePrimaryKey
    {
        public System.Int32 ControllerTimeId;
        public ControllerTimePrimaryKey(System.Int32 ControllerTimeId)
        {
            this.ControllerTimeId = ControllerTimeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerTimeId};
        }
    }
    [Serializable]
    public struct ControllerTypePrimaryKey
    {
        public System.Int32 ControllerTypeId;
        public ControllerTypePrimaryKey(System.Int32 ControllerTypeId)
        {
            this.ControllerTypeId = ControllerTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerTypeId};
        }
    }
    [Serializable]
    public struct CountGprsPrimaryKey
    {
        public System.DateTime TimeBegin;
        public System.Int32 Duration;
        public CountGprsPrimaryKey(System.DateTime TimeBegin, System.Int32 Duration)
        {
            this.TimeBegin = TimeBegin;
            this.Duration = Duration;
        }
        public object[] GetObjects()
        {
            return new object[]{TimeBegin, Duration};
        }
    }
    [Serializable]
    public struct CountSmsPrimaryKey
    {
        public System.DateTime TimeBegin;
        public System.Int32 Duration;
        public CountSmsPrimaryKey(System.DateTime TimeBegin, System.Int32 Duration)
        {
            this.TimeBegin = TimeBegin;
            this.Duration = Duration;
        }
        public object[] GetObjects()
        {
            return new object[]{TimeBegin, Duration};
        }
    }
    [Serializable]
    public struct CustomerPrimaryKey
    {
        public System.Int32 CustomerId;
        public CustomerPrimaryKey(System.Int32 CustomerId)
        {
            this.CustomerId = CustomerId;
        }
        public object[] GetObjects()
        {
            return new object[]{CustomerId};
        }
    }
    [Serializable]
    public struct DayPrimaryKey
    {
        public System.Int32 DayId;
        public DayPrimaryKey(System.Int32 DayId)
        {
            this.DayId = DayId;
        }
        public object[] GetObjects()
        {
            return new object[]{DayId};
        }
    }
    [Serializable]
    public struct DayKindPrimaryKey
    {
        public System.Int32 DayKindId;
        public DayKindPrimaryKey(System.Int32 DayKindId)
        {
            this.DayKindId = DayKindId;
        }
        public object[] GetObjects()
        {
            return new object[]{DayKindId};
        }
    }
    [Serializable]
    public struct DayTimePrimaryKey
    {
        public System.Int32 DayTimeId;
        public DayTimePrimaryKey(System.Int32 DayTimeId)
        {
            this.DayTimeId = DayTimeId;
        }
        public object[] GetObjects()
        {
            return new object[]{DayTimeId};
        }
    }
    [Serializable]
    public struct DayTypePrimaryKey
    {
        public System.Int32 DayTypeId;
        public DayTypePrimaryKey(System.Int32 DayTypeId)
        {
            this.DayTypeId = DayTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{DayTypeId};
        }
    }
    [Serializable]
    public struct DecadePrimaryKey
    {
        public System.Int32 DecadeId;
        public DecadePrimaryKey(System.Int32 DecadeId)
        {
            this.DecadeId = DecadeId;
        }
        public object[] GetObjects()
        {
            return new object[]{DecadeId};
        }
    }
    [Serializable]
    public struct DepartmentPrimaryKey
    {
        public System.Int32 DepartmentId;
        public DepartmentPrimaryKey(System.Int32 DepartmentId)
        {
            this.DepartmentId = DepartmentId;
        }
        public object[] GetObjects()
        {
            return new object[]{DepartmentId};
        }
    }
    [Serializable]
    public struct DkSetPrimaryKey
    {
        public System.Int32 DkSetId;
        public DkSetPrimaryKey(System.Int32 DkSetId)
        {
            this.DkSetId = DkSetId;
        }
        public object[] GetObjects()
        {
            return new object[]{DkSetId};
        }
    }
    [Serializable]
    public struct DotnetTypePrimaryKey
    {
        public System.Int32 DotnetTypeId;
        public DotnetTypePrimaryKey(System.Int32 DotnetTypeId)
        {
            this.DotnetTypeId = DotnetTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{DotnetTypeId};
        }
    }
    [Serializable]
    public struct DriverPrimaryKey
    {
        public System.Int32 DriverId;
        public DriverPrimaryKey(System.Int32 DriverId)
        {
            this.DriverId = DriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverId};
        }
    }
    [Serializable]
    public struct DriverBonusPrimaryKey
    {
        public System.Int32 DriverBonusId;
        public DriverBonusPrimaryKey(System.Int32 DriverBonusId)
        {
            this.DriverBonusId = DriverBonusId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverBonusId};
        }
    }
    [Serializable]
    public struct DriverMsgTemplatePrimaryKey
    {
        public System.Int32 DriverMsgTemplateId;
        public DriverMsgTemplatePrimaryKey(System.Int32 DriverMsgTemplateId)
        {
            this.DriverMsgTemplateId = DriverMsgTemplateId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverMsgTemplateId};
        }
    }
    [Serializable]
    public struct DriverStatusPrimaryKey
    {
        public System.Int32 DriverStatusId;
        public DriverStatusPrimaryKey(System.Int32 DriverStatusId)
        {
            this.DriverStatusId = DriverStatusId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverStatusId};
        }
    }
    [Serializable]
    public struct DriverVehiclePrimaryKey
    {
        public System.Int32 DriverId;
        public DriverVehiclePrimaryKey(System.Int32 DriverId)
        {
            this.DriverId = DriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverId};
        }
    }
    [Serializable]
    public struct DrivergroupPrimaryKey
    {
        public System.Int32 DrivergroupId;
        public DrivergroupPrimaryKey(System.Int32 DrivergroupId)
        {
            this.DrivergroupId = DrivergroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{DrivergroupId};
        }
    }
    [Serializable]
    public struct DrivergroupDriverPrimaryKey
    {
        public System.Int32 DrivergroupId;
        public System.Int32 DriverId;
        public DrivergroupDriverPrimaryKey(System.Int32 DrivergroupId, System.Int32 DriverId)
        {
            this.DrivergroupId = DrivergroupId;
            this.DriverId = DriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{DrivergroupId, DriverId};
        }
    }
    [Serializable]
    public struct EmailPrimaryKey
    {
        public System.Int32 EmailId;
        public EmailPrimaryKey(System.Int32 EmailId)
        {
            this.EmailId = EmailId;
        }
        public object[] GetObjects()
        {
            return new object[]{EmailId};
        }
    }
    [Serializable]
    public struct EmailSchedulereventPrimaryKey
    {
        public System.Int32 SchedulereventId;
        public System.Int32 EmailId;
        public EmailSchedulereventPrimaryKey(System.Int32 SchedulereventId, System.Int32 EmailId)
        {
            this.SchedulereventId = SchedulereventId;
            this.EmailId = EmailId;
        }
        public object[] GetObjects()
        {
            return new object[]{SchedulereventId, EmailId};
        }
    }
    [Serializable]
    public struct EmailSchedulerqueuePrimaryKey
    {
        public System.Int32 SchedulerqueueId;
        public System.Int32 EmailId;
        public EmailSchedulerqueuePrimaryKey(System.Int32 SchedulerqueueId, System.Int32 EmailId)
        {
            this.SchedulerqueueId = SchedulerqueueId;
            this.EmailId = EmailId;
        }
        public object[] GetObjects()
        {
            return new object[]{SchedulerqueueId, EmailId};
        }
    }
    [Serializable]
    public struct FactorValuesPrimaryKey
    {
        public System.Int32 FactorValuesId;
        public FactorValuesPrimaryKey(System.Int32 FactorValuesId)
        {
            this.FactorValuesId = FactorValuesId;
        }
        public object[] GetObjects()
        {
            return new object[]{FactorValuesId};
        }
    }
    [Serializable]
    public struct FactorsPrimaryKey
    {
        public System.Int32 FactorId;
        public FactorsPrimaryKey(System.Int32 FactorId)
        {
            this.FactorId = FactorId;
        }
        public object[] GetObjects()
        {
            return new object[]{FactorId};
        }
    }
    [Serializable]
    public struct GeoSegmentPrimaryKey
    {
        public System.Int32 GeoSegmentId;
        public GeoSegmentPrimaryKey(System.Int32 GeoSegmentId)
        {
            this.GeoSegmentId = GeoSegmentId;
        }
        public object[] GetObjects()
        {
            return new object[]{GeoSegmentId};
        }
    }
    [Serializable]
    public struct GeoSegmentRuntimePrimaryKey
    {
        public System.Int32 GeoSegmentRuntimeId;
        public GeoSegmentRuntimePrimaryKey(System.Int32 GeoSegmentRuntimeId)
        {
            this.GeoSegmentRuntimeId = GeoSegmentRuntimeId;
        }
        public object[] GetObjects()
        {
            return new object[]{GeoSegmentRuntimeId};
        }
    }
    [Serializable]
    public struct GeoSegmentVertexPrimaryKey
    {
        public System.Int32 GeoSegmentVertexId;
        public GeoSegmentVertexPrimaryKey(System.Int32 GeoSegmentVertexId)
        {
            this.GeoSegmentVertexId = GeoSegmentVertexId;
        }
        public object[] GetObjects()
        {
            return new object[]{GeoSegmentVertexId};
        }
    }
    [Serializable]
    public struct GeoTripPrimaryKey
    {
        public System.Int32 GeoTripId;
        public GeoTripPrimaryKey(System.Int32 GeoTripId)
        {
            this.GeoTripId = GeoTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{GeoTripId};
        }
    }
    [Serializable]
    public struct GeoZonePrimaryKey
    {
        public System.Int32 ZoneId;
        public GeoZonePrimaryKey(System.Int32 ZoneId)
        {
            this.ZoneId = ZoneId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZoneId};
        }
    }
    [Serializable]
    public struct GeoZonePrimitivePrimaryKey
    {
        public System.Int32 GeoZonePrimitiveId;
        public GeoZonePrimitivePrimaryKey(System.Int32 GeoZonePrimitiveId)
        {
            this.GeoZonePrimitiveId = GeoZonePrimitiveId;
        }
        public object[] GetObjects()
        {
            return new object[]{GeoZonePrimitiveId};
        }
    }
    [Serializable]
    public struct GoodsPrimaryKey
    {
        public System.Int32 GoodsId;
        public GoodsPrimaryKey(System.Int32 GoodsId)
        {
            this.GoodsId = GoodsId;
        }
        public object[] GetObjects()
        {
            return new object[]{GoodsId};
        }
    }
    [Serializable]
    public struct GoodsLogisticOrderPrimaryKey
    {
        public System.Int32 GoodsLogisticOrderId;
        public GoodsLogisticOrderPrimaryKey(System.Int32 GoodsLogisticOrderId)
        {
            this.GoodsLogisticOrderId = GoodsLogisticOrderId;
        }
        public object[] GetObjects()
        {
            return new object[]{GoodsLogisticOrderId};
        }
    }
    [Serializable]
    public struct GoodsTypePrimaryKey
    {
        public System.Int32 GoodsTypeId;
        public GoodsTypePrimaryKey(System.Int32 GoodsTypeId)
        {
            this.GoodsTypeId = GoodsTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{GoodsTypeId};
        }
    }
    [Serializable]
    public struct GoodsTypeVehiclePrimaryKey
    {
        public System.Int32 GoodsTypeVehicleId;
        public GoodsTypeVehiclePrimaryKey(System.Int32 GoodsTypeVehicleId)
        {
            this.GoodsTypeVehicleId = GoodsTypeVehicleId;
        }
        public object[] GetObjects()
        {
            return new object[]{GoodsTypeVehicleId};
        }
    }
    [Serializable]
    public struct GraphicPrimaryKey
    {
        public System.Int32 GraphicId;
        public GraphicPrimaryKey(System.Int32 GraphicId)
        {
            this.GraphicId = GraphicId;
        }
        public object[] GetObjects()
        {
            return new object[]{GraphicId};
        }
    }
    [Serializable]
    public struct GraphicShiftPrimaryKey
    {
        public System.Int32 Id;
        public GraphicShiftPrimaryKey(System.Int32 Id)
        {
            this.Id = Id;
        }
        public object[] GetObjects()
        {
            return new object[]{Id};
        }
    }
    [Serializable]
    public struct GuardEventsPrimaryKey
    {
        public System.Int32 GuardEventId;
        public GuardEventsPrimaryKey(System.Int32 GuardEventId)
        {
            this.GuardEventId = GuardEventId;
        }
        public object[] GetObjects()
        {
            return new object[]{GuardEventId};
        }
    }
    [Serializable]
    public struct JobDelbadxyPrimaryKey
    {
        public System.Int32 JobDelbadxyId;
        public JobDelbadxyPrimaryKey(System.Int32 JobDelbadxyId)
        {
            this.JobDelbadxyId = JobDelbadxyId;
        }
        public object[] GetObjects()
        {
            return new object[]{JobDelbadxyId};
        }
    }
    [Serializable]
    public struct JournalPrimaryKey
    {
        public System.Int32 JournalId;
        public JournalPrimaryKey(System.Int32 JournalId)
        {
            this.JournalId = JournalId;
        }
        public object[] GetObjects()
        {
            return new object[]{JournalId};
        }
    }
    [Serializable]
    public struct JournalDayPrimaryKey
    {
        public System.Int32 JournalDayId;
        public JournalDayPrimaryKey(System.Int32 JournalDayId)
        {
            this.JournalDayId = JournalDayId;
        }
        public object[] GetObjects()
        {
            return new object[]{JournalDayId};
        }
    }
    [Serializable]
    public struct JournalDriverPrimaryKey
    {
        public System.Int32 JournalDriverId;
        public JournalDriverPrimaryKey(System.Int32 JournalDriverId)
        {
            this.JournalDriverId = JournalDriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{JournalDriverId};
        }
    }
    [Serializable]
    public struct JournalVehiclePrimaryKey
    {
        public System.Int32 JournalVehicleId;
        public JournalVehiclePrimaryKey(System.Int32 JournalVehicleId)
        {
            this.JournalVehicleId = JournalVehicleId;
        }
        public object[] GetObjects()
        {
            return new object[]{JournalVehicleId};
        }
    }
    [Serializable]
    public struct LogOpsEventsPrimaryKey
    {
        public System.Int32 RecordId;
        public LogOpsEventsPrimaryKey(System.Int32 RecordId)
        {
            this.RecordId = RecordId;
        }
        public object[] GetObjects()
        {
            return new object[]{RecordId};
        }
    }
    [Serializable]
    public struct LogVehicleStatusPrimaryKey
    {
        public System.Int32 LogVehicleStatusId;
        public LogVehicleStatusPrimaryKey(System.Int32 LogVehicleStatusId)
        {
            this.LogVehicleStatusId = LogVehicleStatusId;
        }
        public object[] GetObjects()
        {
            return new object[]{LogVehicleStatusId};
        }
    }
    [Serializable]
    public struct LogWaybillHeaderStatusPrimaryKey
    {
        public System.Int32 LogWaybillHeaderStatusId;
        public LogWaybillHeaderStatusPrimaryKey(System.Int32 LogWaybillHeaderStatusId)
        {
            this.LogWaybillHeaderStatusId = LogWaybillHeaderStatusId;
        }
        public object[] GetObjects()
        {
            return new object[]{LogWaybillHeaderStatusId};
        }
    }
    [Serializable]
    public struct LogisticAddressPrimaryKey
    {
        public System.Int32 LogisticAddressId;
        public LogisticAddressPrimaryKey(System.Int32 LogisticAddressId)
        {
            this.LogisticAddressId = LogisticAddressId;
        }
        public object[] GetObjects()
        {
            return new object[]{LogisticAddressId};
        }
    }
    [Serializable]
    public struct LogisticOrderPrimaryKey
    {
        public System.Int32 LogisticOrderId;
        public LogisticOrderPrimaryKey(System.Int32 LogisticOrderId)
        {
            this.LogisticOrderId = LogisticOrderId;
        }
        public object[] GetObjects()
        {
            return new object[]{LogisticOrderId};
        }
    }
    [Serializable]
    public struct MapVertexPrimaryKey
    {
        public System.Int32 VertexId;
        public MapVertexPrimaryKey(System.Int32 VertexId)
        {
            this.VertexId = VertexId;
        }
        public object[] GetObjects()
        {
            return new object[]{VertexId};
        }
    }
    [Serializable]
    public struct MapsPrimaryKey
    {
        public System.Int32 MapId;
        public MapsPrimaryKey(System.Int32 MapId)
        {
            this.MapId = MapId;
        }
        public object[] GetObjects()
        {
            return new object[]{MapId};
        }
    }
    [Serializable]
    public struct MediaPrimaryKey
    {
        public System.Int32 MediaId;
        public MediaPrimaryKey(System.Int32 MediaId)
        {
            this.MediaId = MediaId;
        }
        public object[] GetObjects()
        {
            return new object[]{MediaId};
        }
    }
    [Serializable]
    public struct MediaAcceptorsPrimaryKey
    {
        public System.Int32 MaId;
        public MediaAcceptorsPrimaryKey(System.Int32 MaId)
        {
            this.MaId = MaId;
        }
        public object[] GetObjects()
        {
            return new object[]{MaId};
        }
    }
    [Serializable]
    public struct MediaTypePrimaryKey
    {
        public System.Byte Id;
        public MediaTypePrimaryKey(System.Byte Id)
        {
            this.Id = Id;
        }
        public object[] GetObjects()
        {
            return new object[]{Id};
        }
    }
    [Serializable]
    public struct MessagePrimaryKey
    {
        public System.Int32 MessageId;
        public MessagePrimaryKey(System.Int32 MessageId)
        {
            this.MessageId = MessageId;
        }
        public object[] GetObjects()
        {
            return new object[]{MessageId};
        }
    }
    [Serializable]
    public struct MessageBoardPrimaryKey
    {
        public System.Int32 Id;
        public MessageBoardPrimaryKey(System.Int32 Id)
        {
            this.Id = Id;
        }
        public object[] GetObjects()
        {
            return new object[]{Id};
        }
    }
    [Serializable]
    public struct MessageFieldPrimaryKey
    {
        public System.Int32 MessageFieldId;
        public MessageFieldPrimaryKey(System.Int32 MessageFieldId)
        {
            this.MessageFieldId = MessageFieldId;
        }
        public object[] GetObjects()
        {
            return new object[]{MessageFieldId};
        }
    }
    [Serializable]
    public struct MessageOperatorPrimaryKey
    {
        public System.Int32 MessageOperatorId;
        public MessageOperatorPrimaryKey(System.Int32 MessageOperatorId)
        {
            this.MessageOperatorId = MessageOperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{MessageOperatorId};
        }
    }
    [Serializable]
    public struct MessageTemplatePrimaryKey
    {
        public System.Int32 MessageTemplateId;
        public MessageTemplatePrimaryKey(System.Int32 MessageTemplateId)
        {
            this.MessageTemplateId = MessageTemplateId;
        }
        public object[] GetObjects()
        {
            return new object[]{MessageTemplateId};
        }
    }
    [Serializable]
    public struct MessageTemplateFieldPrimaryKey
    {
        public System.Int32 MessageTemplateFieldId;
        public MessageTemplateFieldPrimaryKey(System.Int32 MessageTemplateFieldId)
        {
            this.MessageTemplateFieldId = MessageTemplateFieldId;
        }
        public object[] GetObjects()
        {
            return new object[]{MessageTemplateFieldId};
        }
    }
    [Serializable]
    public struct OperatorPrimaryKey
    {
        public System.Int32 OperatorId;
        public OperatorPrimaryKey(System.Int32 OperatorId)
        {
            this.OperatorId = OperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId};
        }
    }
    [Serializable]
    public struct OperatorDepartmentPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 DepartmentId;
        public System.Int32 RightId;
        public OperatorDepartmentPrimaryKey(System.Int32 OperatorId, System.Int32 DepartmentId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.DepartmentId = DepartmentId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, DepartmentId, RightId};
        }
    }
    [Serializable]
    public struct OperatorDriverPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 DriverId;
        public System.Int32 RightId;
        public OperatorDriverPrimaryKey(System.Int32 OperatorId, System.Int32 DriverId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.DriverId = DriverId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, DriverId, RightId};
        }
    }
    [Serializable]
    public struct OperatorDrivergroupPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 DrivergroupId;
        public System.Int32 RightId;
        public OperatorDrivergroupPrimaryKey(System.Int32 OperatorId, System.Int32 DrivergroupId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.DrivergroupId = DrivergroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, DrivergroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorProfilePrimaryKey
    {
        public System.Int32 OperatorProfileId;
        public OperatorProfilePrimaryKey(System.Int32 OperatorProfileId)
        {
            this.OperatorProfileId = OperatorProfileId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorProfileId};
        }
    }
    [Serializable]
    public struct OperatorReportPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 ReportId;
        public System.Int32 RightId;
        public OperatorReportPrimaryKey(System.Int32 OperatorId, System.Int32 ReportId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.ReportId = ReportId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, ReportId, RightId};
        }
    }
    [Serializable]
    public struct OperatorRoutePrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 RouteId;
        public System.Int32 RightId;
        public OperatorRoutePrimaryKey(System.Int32 OperatorId, System.Int32 RouteId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.RouteId = RouteId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, RouteId, RightId};
        }
    }
    [Serializable]
    public struct OperatorRoutegroupPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 RoutegroupId;
        public System.Int32 RightId;
        public OperatorRoutegroupPrimaryKey(System.Int32 OperatorId, System.Int32 RoutegroupId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.RoutegroupId = RoutegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, RoutegroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorVehiclePrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 VehicleId;
        public System.Int32 RightId;
        public OperatorVehiclePrimaryKey(System.Int32 OperatorId, System.Int32 VehicleId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.VehicleId = VehicleId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, VehicleId, RightId};
        }
    }
    [Serializable]
    public struct OperatorVehiclegroupPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 VehiclegroupId;
        public System.Int32 RightId;
        public OperatorVehiclegroupPrimaryKey(System.Int32 OperatorId, System.Int32 VehiclegroupId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.VehiclegroupId = VehiclegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, VehiclegroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorZonePrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 ZoneId;
        public System.Int32 RightId;
        public OperatorZonePrimaryKey(System.Int32 OperatorId, System.Int32 ZoneId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.ZoneId = ZoneId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, ZoneId, RightId};
        }
    }
    [Serializable]
    public struct OperatorZonegroupPrimaryKey
    {
        public System.Int32 OperatorId;
        public System.Int32 ZonegroupId;
        public System.Int32 RightId;
        public OperatorZonegroupPrimaryKey(System.Int32 OperatorId, System.Int32 ZonegroupId, System.Int32 RightId)
        {
            this.OperatorId = OperatorId;
            this.ZonegroupId = ZonegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId, ZonegroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public OperatorgroupPrimaryKey(System.Int32 OperatorgroupId)
        {
            this.OperatorgroupId = OperatorgroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId};
        }
    }
    [Serializable]
    public struct OperatorgroupDepartmentPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 DepartmentId;
        public System.Int32 RightId;
        public OperatorgroupDepartmentPrimaryKey(System.Int32 OperatorgroupId, System.Int32 DepartmentId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.DepartmentId = DepartmentId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, DepartmentId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupDriverPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 DriverId;
        public System.Int32 RightId;
        public OperatorgroupDriverPrimaryKey(System.Int32 OperatorgroupId, System.Int32 DriverId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.DriverId = DriverId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, DriverId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupDrivergroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 DrivergroupId;
        public System.Int32 RightId;
        public OperatorgroupDrivergroupPrimaryKey(System.Int32 OperatorgroupId, System.Int32 DrivergroupId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.DrivergroupId = DrivergroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, DrivergroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupOperatorPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 OperatorId;
        public OperatorgroupOperatorPrimaryKey(System.Int32 OperatorgroupId, System.Int32 OperatorId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.OperatorId = OperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, OperatorId};
        }
    }
    [Serializable]
    public struct OperatorgroupReportPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 ReportId;
        public System.Int32 RightId;
        public OperatorgroupReportPrimaryKey(System.Int32 OperatorgroupId, System.Int32 ReportId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.ReportId = ReportId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, ReportId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupRoutePrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 RouteId;
        public System.Int32 RightId;
        public OperatorgroupRoutePrimaryKey(System.Int32 OperatorgroupId, System.Int32 RouteId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.RouteId = RouteId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, RouteId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupRoutegroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 RoutegroupId;
        public System.Int32 RightId;
        public OperatorgroupRoutegroupPrimaryKey(System.Int32 OperatorgroupId, System.Int32 RoutegroupId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.RoutegroupId = RoutegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, RoutegroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupVehiclePrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 VehicleId;
        public System.Int32 RightId;
        public OperatorgroupVehiclePrimaryKey(System.Int32 OperatorgroupId, System.Int32 VehicleId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.VehicleId = VehicleId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, VehicleId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupVehiclegroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 VehiclegroupId;
        public System.Int32 RightId;
        public OperatorgroupVehiclegroupPrimaryKey(System.Int32 OperatorgroupId, System.Int32 VehiclegroupId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.VehiclegroupId = VehiclegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, VehiclegroupId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupZonePrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 ZoneId;
        public System.Int32 RightId;
        public OperatorgroupZonePrimaryKey(System.Int32 OperatorgroupId, System.Int32 ZoneId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.ZoneId = ZoneId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, ZoneId, RightId};
        }
    }
    [Serializable]
    public struct OperatorgroupZonegroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public System.Int32 ZonegroupId;
        public System.Int32 RightId;
        public OperatorgroupZonegroupPrimaryKey(System.Int32 OperatorgroupId, System.Int32 ZonegroupId, System.Int32 RightId)
        {
            this.OperatorgroupId = OperatorgroupId;
            this.ZonegroupId = ZonegroupId;
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId, ZonegroupId, RightId};
        }
    }
    [Serializable]
    public struct OpsEventPrimaryKey
    {
        public System.Int32 OpsEventId;
        public OpsEventPrimaryKey(System.Int32 OpsEventId)
        {
            this.OpsEventId = OpsEventId;
        }
        public object[] GetObjects()
        {
            return new object[]{OpsEventId};
        }
    }
    [Serializable]
    public struct OrderPrimaryKey
    {
        public System.Int32 OrderId;
        public OrderPrimaryKey(System.Int32 OrderId)
        {
            this.OrderId = OrderId;
        }
        public object[] GetObjects()
        {
            return new object[]{OrderId};
        }
    }
    [Serializable]
    public struct OrderTripPrimaryKey
    {
        public System.Int32 OrderTripId;
        public OrderTripPrimaryKey(System.Int32 OrderTripId)
        {
            this.OrderTripId = OrderTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{OrderTripId};
        }
    }
    [Serializable]
    public struct OrderTypePrimaryKey
    {
        public System.Int32 OrderTypeId;
        public OrderTypePrimaryKey(System.Int32 OrderTypeId)
        {
            this.OrderTypeId = OrderTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{OrderTypeId};
        }
    }
    [Serializable]
    public struct OwnerPrimaryKey
    {
        public System.Int32 OwnerId;
        public OwnerPrimaryKey(System.Int32 OwnerId)
        {
            this.OwnerId = OwnerId;
        }
        public object[] GetObjects()
        {
            return new object[]{OwnerId};
        }
    }
    [Serializable]
    public struct PeriodPrimaryKey
    {
        public System.Int32 PeriodId;
        public PeriodPrimaryKey(System.Int32 PeriodId)
        {
            this.PeriodId = PeriodId;
        }
        public object[] GetObjects()
        {
            return new object[]{PeriodId};
        }
    }
    [Serializable]
    public struct PointPrimaryKey
    {
        public System.Int32 PointId;
        public PointPrimaryKey(System.Int32 PointId)
        {
            this.PointId = PointId;
        }
        public object[] GetObjects()
        {
            return new object[]{PointId};
        }
    }
    [Serializable]
    public struct PointKindPrimaryKey
    {
        public System.Int32 PointKindId;
        public PointKindPrimaryKey(System.Int32 PointKindId)
        {
            this.PointKindId = PointKindId;
        }
        public object[] GetObjects()
        {
            return new object[]{PointKindId};
        }
    }
    [Serializable]
    public struct ReportPrimaryKey
    {
        public System.Int32 ReportId;
        public ReportPrimaryKey(System.Int32 ReportId)
        {
            this.ReportId = ReportId;
        }
        public object[] GetObjects()
        {
            return new object[]{ReportId};
        }
    }
    [Serializable]
    public struct ReprintReasonPrimaryKey
    {
        public System.Int32 ReprintReasonId;
        public ReprintReasonPrimaryKey(System.Int32 ReprintReasonId)
        {
            this.ReprintReasonId = ReprintReasonId;
        }
        public object[] GetObjects()
        {
            return new object[]{ReprintReasonId};
        }
    }
    [Serializable]
    public struct RightPrimaryKey
    {
        public System.Int32 RightId;
        public RightPrimaryKey(System.Int32 RightId)
        {
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{RightId};
        }
    }
    [Serializable]
    public struct RightOperatorPrimaryKey
    {
        public System.Int32 RightId;
        public System.Int32 OperatorId;
        public RightOperatorPrimaryKey(System.Int32 RightId, System.Int32 OperatorId)
        {
            this.RightId = RightId;
            this.OperatorId = OperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{RightId, OperatorId};
        }
    }
    [Serializable]
    public struct RightOperatorgroupPrimaryKey
    {
        public System.Int32 RightId;
        public System.Int32 OperatorgroupId;
        public RightOperatorgroupPrimaryKey(System.Int32 RightId, System.Int32 OperatorgroupId)
        {
            this.RightId = RightId;
            this.OperatorgroupId = OperatorgroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{RightId, OperatorgroupId};
        }
    }
    [Serializable]
    public struct RoutePrimaryKey
    {
        public System.Int32 RouteId;
        public RoutePrimaryKey(System.Int32 RouteId)
        {
            this.RouteId = RouteId;
        }
        public object[] GetObjects()
        {
            return new object[]{RouteId};
        }
    }
    [Serializable]
    public struct RouteGeozonePrimaryKey
    {
        public System.Int32 RouteGeozoneId;
        public RouteGeozonePrimaryKey(System.Int32 RouteGeozoneId)
        {
            this.RouteGeozoneId = RouteGeozoneId;
        }
        public object[] GetObjects()
        {
            return new object[]{RouteGeozoneId};
        }
    }
    [Serializable]
    public struct RoutePointPrimaryKey
    {
        public System.Int32 RoutePointId;
        public RoutePointPrimaryKey(System.Int32 RoutePointId)
        {
            this.RoutePointId = RoutePointId;
        }
        public object[] GetObjects()
        {
            return new object[]{RoutePointId};
        }
    }
    [Serializable]
    public struct RouteTripPrimaryKey
    {
        public System.Int32 RouteTripId;
        public RouteTripPrimaryKey(System.Int32 RouteTripId)
        {
            this.RouteTripId = RouteTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{RouteTripId};
        }
    }
    [Serializable]
    public struct RoutegroupPrimaryKey
    {
        public System.Int32 RoutegroupId;
        public RoutegroupPrimaryKey(System.Int32 RoutegroupId)
        {
            this.RoutegroupId = RoutegroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{RoutegroupId};
        }
    }
    [Serializable]
    public struct RoutegroupRoutePrimaryKey
    {
        public System.Int32 RoutegroupId;
        public System.Int32 RouteId;
        public RoutegroupRoutePrimaryKey(System.Int32 RoutegroupId, System.Int32 RouteId)
        {
            this.RoutegroupId = RoutegroupId;
            this.RouteId = RouteId;
        }
        public object[] GetObjects()
        {
            return new object[]{RoutegroupId, RouteId};
        }
    }
    [Serializable]
    public struct RsPrimaryKey
    {
        public System.Int32 RsId;
        public RsPrimaryKey(System.Int32 RsId)
        {
            this.RsId = RsId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsId};
        }
    }
    [Serializable]
    public struct RsNumberPrimaryKey
    {
        public System.Int32 RsNumberId;
        public RsNumberPrimaryKey(System.Int32 RsNumberId)
        {
            this.RsNumberId = RsNumberId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsNumberId};
        }
    }
    [Serializable]
    public struct RsOperationstypePrimaryKey
    {
        public System.Int32 RsOperationstypeId;
        public RsOperationstypePrimaryKey(System.Int32 RsOperationstypeId)
        {
            this.RsOperationstypeId = RsOperationstypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsOperationstypeId};
        }
    }
    [Serializable]
    public struct RsOperationtypePrimaryKey
    {
        public System.Int32 RsOperationtypeId;
        public RsOperationtypePrimaryKey(System.Int32 RsOperationtypeId)
        {
            this.RsOperationtypeId = RsOperationtypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsOperationtypeId};
        }
    }
    [Serializable]
    public struct RsPeriodPrimaryKey
    {
        public System.Int32 PeriodId;
        public RsPeriodPrimaryKey(System.Int32 PeriodId)
        {
            this.PeriodId = PeriodId;
        }
        public object[] GetObjects()
        {
            return new object[]{PeriodId};
        }
    }
    [Serializable]
    public struct RsPointPrimaryKey
    {
        public System.Int32 RsPointId;
        public RsPointPrimaryKey(System.Int32 RsPointId)
        {
            this.RsPointId = RsPointId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsPointId};
        }
    }
    [Serializable]
    public struct RsRoundTripPrimaryKey
    {
        public System.Int32 RsRoundTripId;
        public RsRoundTripPrimaryKey(System.Int32 RsRoundTripId)
        {
            this.RsRoundTripId = RsRoundTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsRoundTripId};
        }
    }
    [Serializable]
    public struct RsRuntimePrimaryKey
    {
        public System.Int32 RsRuntimeId;
        public RsRuntimePrimaryKey(System.Int32 RsRuntimeId)
        {
            this.RsRuntimeId = RsRuntimeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsRuntimeId};
        }
    }
    [Serializable]
    public struct RsShiftPrimaryKey
    {
        public System.Int32 RsShiftId;
        public RsShiftPrimaryKey(System.Int32 RsShiftId)
        {
            this.RsShiftId = RsShiftId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsShiftId};
        }
    }
    [Serializable]
    public struct RsStepPrimaryKey
    {
        public System.Int32 RsStepId;
        public RsStepPrimaryKey(System.Int32 RsStepId)
        {
            this.RsStepId = RsStepId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsStepId};
        }
    }
    [Serializable]
    public struct RsTripPrimaryKey
    {
        public System.Int32 RsTripId;
        public RsTripPrimaryKey(System.Int32 RsTripId)
        {
            this.RsTripId = RsTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsTripId};
        }
    }
    [Serializable]
    public struct RsTripstypePrimaryKey
    {
        public System.Int32 RsTripstypeId;
        public RsTripstypePrimaryKey(System.Int32 RsTripstypeId)
        {
            this.RsTripstypeId = RsTripstypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsTripstypeId};
        }
    }
    [Serializable]
    public struct RsTriptypePrimaryKey
    {
        public System.Int32 RsTriptypeId;
        public RsTriptypePrimaryKey(System.Int32 RsTriptypeId)
        {
            this.RsTriptypeId = RsTriptypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsTriptypeId};
        }
    }
    [Serializable]
    public struct RsTypePrimaryKey
    {
        public System.Int32 RsTypeId;
        public RsTypePrimaryKey(System.Int32 RsTypeId)
        {
            this.RsTypeId = RsTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsTypeId};
        }
    }
    [Serializable]
    public struct RsVariantPrimaryKey
    {
        public System.Int32 RsVariantId;
        public RsVariantPrimaryKey(System.Int32 RsVariantId)
        {
            this.RsVariantId = RsVariantId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsVariantId};
        }
    }
    [Serializable]
    public struct RsWayoutPrimaryKey
    {
        public System.Int32 RsWayoutId;
        public RsWayoutPrimaryKey(System.Int32 RsWayoutId)
        {
            this.RsWayoutId = RsWayoutId;
        }
        public object[] GetObjects()
        {
            return new object[]{RsWayoutId};
        }
    }
    [Serializable]
    public struct RulePrimaryKey
    {
        public System.Int32 RuleId;
        public RulePrimaryKey(System.Int32 RuleId)
        {
            this.RuleId = RuleId;
        }
        public object[] GetObjects()
        {
            return new object[]{RuleId};
        }
    }
    [Serializable]
    public struct SchedulePrimaryKey
    {
        public System.Int32 ScheduleId;
        public SchedulePrimaryKey(System.Int32 ScheduleId)
        {
            this.ScheduleId = ScheduleId;
        }
        public object[] GetObjects()
        {
            return new object[]{ScheduleId};
        }
    }
    [Serializable]
    public struct ScheduleDetailPrimaryKey
    {
        public System.Int32 ScheduleDetailId;
        public ScheduleDetailPrimaryKey(System.Int32 ScheduleDetailId)
        {
            this.ScheduleDetailId = ScheduleDetailId;
        }
        public object[] GetObjects()
        {
            return new object[]{ScheduleDetailId};
        }
    }
    [Serializable]
    public struct ScheduleDetailInfoPrimaryKey
    {
        public System.Int32 ScheduleDetailId;
        public ScheduleDetailInfoPrimaryKey(System.Int32 ScheduleDetailId)
        {
            this.ScheduleDetailId = ScheduleDetailId;
        }
        public object[] GetObjects()
        {
            return new object[]{ScheduleDetailId};
        }
    }
    [Serializable]
    public struct ScheduleGeozonePrimaryKey
    {
        public System.Int32 ScheduleGeozoneId;
        public ScheduleGeozonePrimaryKey(System.Int32 ScheduleGeozoneId)
        {
            this.ScheduleGeozoneId = ScheduleGeozoneId;
        }
        public object[] GetObjects()
        {
            return new object[]{ScheduleGeozoneId};
        }
    }
    [Serializable]
    public struct SchedulePassagePrimaryKey
    {
        public System.Int32 WhId;
        public System.Int32 SpId;
        public SchedulePassagePrimaryKey(System.Int32 WhId, System.Int32 SpId)
        {
            this.WhId = WhId;
            this.SpId = SpId;
        }
        public object[] GetObjects()
        {
            return new object[]{WhId, SpId};
        }
    }
    [Serializable]
    public struct SchedulePointPrimaryKey
    {
        public System.Int32 SchedulePointId;
        public SchedulePointPrimaryKey(System.Int32 SchedulePointId)
        {
            this.SchedulePointId = SchedulePointId;
        }
        public object[] GetObjects()
        {
            return new object[]{SchedulePointId};
        }
    }
    [Serializable]
    public struct SchedulereventPrimaryKey
    {
        public System.Int32 SchedulereventId;
        public SchedulereventPrimaryKey(System.Int32 SchedulereventId)
        {
            this.SchedulereventId = SchedulereventId;
        }
        public object[] GetObjects()
        {
            return new object[]{SchedulereventId};
        }
    }
    [Serializable]
    public struct SchedulerqueuePrimaryKey
    {
        public System.Int32 SchedulerqueueId;
        public SchedulerqueuePrimaryKey(System.Int32 SchedulerqueueId)
        {
            this.SchedulerqueueId = SchedulerqueueId;
        }
        public object[] GetObjects()
        {
            return new object[]{SchedulerqueueId};
        }
    }
    [Serializable]
    public struct SeasonPrimaryKey
    {
        public System.Int32 SeasonId;
        public SeasonPrimaryKey(System.Int32 SeasonId)
        {
            this.SeasonId = SeasonId;
        }
        public object[] GetObjects()
        {
            return new object[]{SeasonId};
        }
    }
    [Serializable]
    public struct SeattypePrimaryKey
    {
        public System.Int32 SeattypeId;
        public SeattypePrimaryKey(System.Int32 SeattypeId)
        {
            this.SeattypeId = SeattypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{SeattypeId};
        }
    }
    [Serializable]
    public struct SessionPrimaryKey
    {
        public System.Int32 SessionId;
        public SessionPrimaryKey(System.Int32 SessionId)
        {
            this.SessionId = SessionId;
        }
        public object[] GetObjects()
        {
            return new object[]{SessionId};
        }
    }
    [Serializable]
    public struct SmsTypePrimaryKey
    {
        public System.Int32 SmsTypeId;
        public SmsTypePrimaryKey(System.Int32 SmsTypeId)
        {
            this.SmsTypeId = SmsTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{SmsTypeId};
        }
    }
    [Serializable]
    public struct SysdiagramsPrimaryKey
    {
        public System.Int32 DiagramId;
        public SysdiagramsPrimaryKey(System.Int32 DiagramId)
        {
            this.DiagramId = DiagramId;
        }
        public object[] GetObjects()
        {
            return new object[]{DiagramId};
        }
    }
    [Serializable]
    public struct TaskProcessorLogPrimaryKey
    {
        public System.Int32 TaskProcessorLogId;
        public TaskProcessorLogPrimaryKey(System.Int32 TaskProcessorLogId)
        {
            this.TaskProcessorLogId = TaskProcessorLogId;
        }
        public object[] GetObjects()
        {
            return new object[]{TaskProcessorLogId};
        }
    }
    [Serializable]
    public struct TrailPrimaryKey
    {
        public System.Int32 TrailId;
        public TrailPrimaryKey(System.Int32 TrailId)
        {
            this.TrailId = TrailId;
        }
        public object[] GetObjects()
        {
            return new object[]{TrailId};
        }
    }
    [Serializable]
    public struct TripPrimaryKey
    {
        public System.Int32 TripId;
        public TripPrimaryKey(System.Int32 TripId)
        {
            this.TripId = TripId;
        }
        public object[] GetObjects()
        {
            return new object[]{TripId};
        }
    }
    [Serializable]
    public struct TripKindPrimaryKey
    {
        public System.Int32 TripKindId;
        public TripKindPrimaryKey(System.Int32 TripKindId)
        {
            this.TripKindId = TripKindId;
        }
        public object[] GetObjects()
        {
            return new object[]{TripKindId};
        }
    }
    [Serializable]
    public struct VehiclePrimaryKey
    {
        public System.Int32 VehicleId;
        public VehiclePrimaryKey(System.Int32 VehicleId)
        {
            this.VehicleId = VehicleId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehicleId};
        }
    }
    [Serializable]
    public struct VehicleKindPrimaryKey
    {
        public System.Int32 VehicleKindId;
        public VehicleKindPrimaryKey(System.Int32 VehicleKindId)
        {
            this.VehicleKindId = VehicleKindId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehicleKindId};
        }
    }
    [Serializable]
    public struct VehicleOwnerPrimaryKey
    {
        public System.Int32 VehicleOwnerId;
        public VehicleOwnerPrimaryKey(System.Int32 VehicleOwnerId)
        {
            this.VehicleOwnerId = VehicleOwnerId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehicleOwnerId};
        }
    }
    [Serializable]
    public struct VehiclePicturePrimaryKey
    {
        public System.Int32 VehiclePictureId;
        public VehiclePicturePrimaryKey(System.Int32 VehiclePictureId)
        {
            this.VehiclePictureId = VehiclePictureId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehiclePictureId};
        }
    }
    [Serializable]
    public struct VehicleRulePrimaryKey
    {
        public System.Int32 VehicleRuleId;
        public VehicleRulePrimaryKey(System.Int32 VehicleRuleId)
        {
            this.VehicleRuleId = VehicleRuleId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehicleRuleId};
        }
    }
    [Serializable]
    public struct VehicleStatusPrimaryKey
    {
        public System.Int32 VehicleStatusId;
        public VehicleStatusPrimaryKey(System.Int32 VehicleStatusId)
        {
            this.VehicleStatusId = VehicleStatusId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehicleStatusId};
        }
    }
    [Serializable]
    public struct VehiclegroupPrimaryKey
    {
        public System.Int32 VehiclegroupId;
        public VehiclegroupPrimaryKey(System.Int32 VehiclegroupId)
        {
            this.VehiclegroupId = VehiclegroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehiclegroupId};
        }
    }
    [Serializable]
    public struct VehiclegroupRulePrimaryKey
    {
        public System.Int32 VehiclegroupRuleId;
        public VehiclegroupRulePrimaryKey(System.Int32 VehiclegroupRuleId)
        {
            this.VehiclegroupRuleId = VehiclegroupRuleId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehiclegroupRuleId};
        }
    }
    [Serializable]
    public struct VehiclegroupVehiclePrimaryKey
    {
        public System.Int32 VehiclegroupId;
        public System.Int32 VehicleId;
        public VehiclegroupVehiclePrimaryKey(System.Int32 VehiclegroupId, System.Int32 VehicleId)
        {
            this.VehiclegroupId = VehiclegroupId;
            this.VehicleId = VehicleId;
        }
        public object[] GetObjects()
        {
            return new object[]{VehiclegroupId, VehicleId};
        }
    }
    [Serializable]
    public struct WaybillPrimaryKey
    {
        public System.Int32 WaybillId;
        public WaybillPrimaryKey(System.Int32 WaybillId)
        {
            this.WaybillId = WaybillId;
        }
        public object[] GetObjects()
        {
            return new object[]{WaybillId};
        }
    }
    [Serializable]
    public struct WaybillHeaderPrimaryKey
    {
        public System.Int32 WaybillHeaderId;
        public WaybillHeaderPrimaryKey(System.Int32 WaybillHeaderId)
        {
            this.WaybillHeaderId = WaybillHeaderId;
        }
        public object[] GetObjects()
        {
            return new object[]{WaybillHeaderId};
        }
    }
    [Serializable]
    public struct WaybillheaderWaybillmarkPrimaryKey
    {
        public System.Int32 WaybillheaderId;
        public System.Int32 WaybillmarkId;
        public WaybillheaderWaybillmarkPrimaryKey(System.Int32 WaybillheaderId, System.Int32 WaybillmarkId)
        {
            this.WaybillheaderId = WaybillheaderId;
            this.WaybillmarkId = WaybillmarkId;
        }
        public object[] GetObjects()
        {
            return new object[]{WaybillheaderId, WaybillmarkId};
        }
    }
    [Serializable]
    public struct WaybillmarkPrimaryKey
    {
        public System.Int32 WaybillmarkId;
        public WaybillmarkPrimaryKey(System.Int32 WaybillmarkId)
        {
            this.WaybillmarkId = WaybillmarkId;
        }
        public object[] GetObjects()
        {
            return new object[]{WaybillmarkId};
        }
    }
    [Serializable]
    public struct WayoutPrimaryKey
    {
        public System.Int32 WayoutId;
        public WayoutPrimaryKey(System.Int32 WayoutId)
        {
            this.WayoutId = WayoutId;
        }
        public object[] GetObjects()
        {
            return new object[]{WayoutId};
        }
    }
    [Serializable]
    public struct WbTripPrimaryKey
    {
        public System.Int32 WbTripId;
        public WbTripPrimaryKey(System.Int32 WbTripId)
        {
            this.WbTripId = WbTripId;
        }
        public object[] GetObjects()
        {
            return new object[]{WbTripId};
        }
    }
    [Serializable]
    public struct WebLinePrimaryKey
    {
        public System.Int32 LineId;
        public WebLinePrimaryKey(System.Int32 LineId)
        {
            this.LineId = LineId;
        }
        public object[] GetObjects()
        {
            return new object[]{LineId};
        }
    }
    [Serializable]
    public struct WebLineVertexPrimaryKey
    {
        public System.Int32 LineVertexId;
        public WebLineVertexPrimaryKey(System.Int32 LineVertexId)
        {
            this.LineVertexId = LineVertexId;
        }
        public object[] GetObjects()
        {
            return new object[]{LineVertexId};
        }
    }
    [Serializable]
    public struct WebPointPrimaryKey
    {
        public System.Int32 PointId;
        public WebPointPrimaryKey(System.Int32 PointId)
        {
            this.PointId = PointId;
        }
        public object[] GetObjects()
        {
            return new object[]{PointId};
        }
    }
    [Serializable]
    public struct WebPointTypePrimaryKey
    {
        public System.Int32 TypeId;
        public WebPointTypePrimaryKey(System.Int32 TypeId)
        {
            this.TypeId = TypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{TypeId};
        }
    }
    [Serializable]
    public struct WorkTimePrimaryKey
    {
        public System.Int32 DriverId;
        public WorkTimePrimaryKey(System.Int32 DriverId)
        {
            this.DriverId = DriverId;
        }
        public object[] GetObjects()
        {
            return new object[]{DriverId};
        }
    }
    [Serializable]
    public struct WorkplaceConfigPrimaryKey
    {
        public System.Int32 Id;
        public WorkplaceConfigPrimaryKey(System.Int32 Id)
        {
            this.Id = Id;
        }
        public object[] GetObjects()
        {
            return new object[]{Id};
        }
    }
    [Serializable]
    public struct WorkstationPrimaryKey
    {
        public System.Int32 WorkstationId;
        public WorkstationPrimaryKey(System.Int32 WorkstationId)
        {
            this.WorkstationId = WorkstationId;
        }
        public object[] GetObjects()
        {
            return new object[]{WorkstationId};
        }
    }
    [Serializable]
    public struct WorktimeReasonPrimaryKey
    {
        public System.Int32 WorktimeReasonId;
        public WorktimeReasonPrimaryKey(System.Int32 WorktimeReasonId)
        {
            this.WorktimeReasonId = WorktimeReasonId;
        }
        public object[] GetObjects()
        {
            return new object[]{WorktimeReasonId};
        }
    }
    [Serializable]
    public struct WorktimeStatusTypePrimaryKey
    {
        public System.Int32 WorktimeStatusTypeId;
        public WorktimeStatusTypePrimaryKey(System.Int32 WorktimeStatusTypeId)
        {
            this.WorktimeStatusTypeId = WorktimeStatusTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{WorktimeStatusTypeId};
        }
    }
    [Serializable]
    public struct ZonePrimitivePrimaryKey
    {
        public System.Int32 PrimitiveId;
        public ZonePrimitivePrimaryKey(System.Int32 PrimitiveId)
        {
            this.PrimitiveId = PrimitiveId;
        }
        public object[] GetObjects()
        {
            return new object[]{PrimitiveId};
        }
    }
    [Serializable]
    public struct ZonePrimitiveVertexPrimaryKey
    {
        public System.Int32 PrimitiveVertexId;
        public ZonePrimitiveVertexPrimaryKey(System.Int32 PrimitiveVertexId)
        {
            this.PrimitiveVertexId = PrimitiveVertexId;
        }
        public object[] GetObjects()
        {
            return new object[]{PrimitiveVertexId};
        }
    }
    [Serializable]
    public struct ZoneTypePrimaryKey
    {
        public System.Int32 ZoneTypeId;
        public ZoneTypePrimaryKey(System.Int32 ZoneTypeId)
        {
            this.ZoneTypeId = ZoneTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZoneTypeId};
        }
    }
    [Serializable]
    public struct ZoneVehiclePrimaryKey
    {
        public System.Int32 ZoneVehicleId;
        public ZoneVehiclePrimaryKey(System.Int32 ZoneVehicleId)
        {
            this.ZoneVehicleId = ZoneVehicleId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZoneVehicleId};
        }
    }
    [Serializable]
    public struct ZoneVehiclegroupPrimaryKey
    {
        public System.Int32 ZoneVehiclegroupId;
        public ZoneVehiclegroupPrimaryKey(System.Int32 ZoneVehiclegroupId)
        {
            this.ZoneVehiclegroupId = ZoneVehiclegroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZoneVehiclegroupId};
        }
    }
    [Serializable]
    public struct ZonegroupPrimaryKey
    {
        public System.Int32 ZonegroupId;
        public ZonegroupPrimaryKey(System.Int32 ZonegroupId)
        {
            this.ZonegroupId = ZonegroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZonegroupId};
        }
    }
    [Serializable]
    public struct ZonegroupZonePrimaryKey
    {
        public System.Int32 ZonegroupZoneId;
        public ZonegroupZonePrimaryKey(System.Int32 ZonegroupZoneId)
        {
            this.ZonegroupZoneId = ZonegroupZoneId;
        }
        public object[] GetObjects()
        {
            return new object[]{ZonegroupZoneId};
        }
    }
}
