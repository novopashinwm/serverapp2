﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.BusinessLogic.Mail
{
	/// <summary> DTO для информации о подписке на отчёт </summary>
	[Serializable]
	public class ReportSubscription
	{
		/// <summary> Включена ли рассылка </summary>
		public bool             Enabled;
		/// <summary> Идентификатор подписки </summary>
		public int              Id;
		/// <summary> Описание расписания </summary>
		public string           ScheduleDescription;
		/// <summary> Объект расписания </summary>
		public IRepetitionClass Schedule;
		/// <summary> Идентификатор подписки </summary>
		public int              ScheduleEventId;
		/// <summary> Название отчёта </summary>
		public string           Name;
		/// <summary> Ближайшее время построения отчёта </summary>
		public int?             NearestTime;
		/// <summary> Время "с", за которое будет построен отчёт </summary>
		public int?             ReportFrom;
		/// <summary> Время "по", за которое будет построен отчёт </summary>
		public int?             ReportTo;
		//TODO: объединить EmailIds и Emails
		/// <summary> Список идентификаторов адресов электронной почты </summary>
		public List<int>        EmailIds;
		/// <summary> Список электронных адресов </summary>
		public List<string>     Emails;
		/// <summary> Идентификатор объекта </summary>
		public int?             ObjectId;
		/// <summary> Тип идентификатора объекта </summary>
		public IdType?          ObjectIdType;
	}
}