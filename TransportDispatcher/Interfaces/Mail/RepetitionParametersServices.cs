﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;

namespace FORIS.TSS.BusinessLogic
{
	public class RepetitionParametersServices
	{
		protected static TraceSwitch tsScheduler =
			new TraceSwitch("SCHEDULER", "Repetition info");
		/// <summary> Converts current parameters set into string </summary>
		public static string ToXML(IRepetitionClass parameters)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				SoapFormatter formatter = new SoapFormatter { AssemblyFormat = FormatterAssemblyStyle.Simple };
				try
				{
					formatter.Serialize(ms, parameters);
				}
				catch (Exception ex)
				{
					Trace.WriteLineIf(tsScheduler.TraceError, ex.ToString(), "SCHEDULER");
				}
				ms.Close();
				byte[] bytes = ms.ToArray();
				string result = Encoding.Default.GetString(bytes);
				return result;
			}
		}
		/// <summary> Converts string into parameters set </summary>
		/// <param name="xml"></param>
		/// <returns></returns>
		public static IRepetitionClass FromXML(string xml)
		{
			if (xml == null)
				return null;

			byte[] bytes = Encoding.Default.GetBytes(xml);
			using (MemoryStream ms = new MemoryStream(bytes))
			{
				var formatter = new SoapFormatter();
				formatter.AssemblyFormat = FormatterAssemblyStyle.Full;
				object obj = null;
				try
				{
					obj = formatter.Deserialize(ms);
				}
				catch (Exception ex)
				{
					Trace.WriteLineIf(tsScheduler.TraceError,
						"Exception during serialization: " + ex.ToString(), "SCHEDULER");
				}
				if (obj == null)
				{
					return null;
				}
				return (IRepetitionClass)obj;
			}
		}
	}
}