﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Параметры для данного отчета </summary>
	[Serializable]
	public class VehicleGroupMoveParameters : ReportParameters, IMonitoreeObjectContainer
	{
		internal const int MaxDays = 31;

		private int vehicleGroupID = -1;

		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupPicker")]
		[Order(1)]
		[Browsable(true)]
		public int VehicleGroupId
		{
			get { return vehicleGroupID; }
			set { vehicleGroupID = value; }
		}
		/// <summary>
		/// Свойство - Дата начала периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
		[Browsable(true)]
		public override DateTime DateFrom
		{
			get
			{
				return dtDateFrom.Date;
			}
			set
			{
				dtDateFrom = value.Date;
			}
		}


		/// <summary>
		/// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
		[Browsable(true)]
		public override DateTime DateTo
		{
			get
			{
				return dtDateTo.Date;
			}
			set
			{
				dtDateTo = value.Date;
			}
		}

		private int operatorID = 0;

		[Browsable(false)]
		public new int OperatorId
		{
			get { return operatorID; }
			set { operatorID = value; }
		}

		private int[] _vehicleIds;

		/// <summary> Список объектов наблюдения, чтобы строить отчёт групповой отчёт не создавая группу </summary>
		[DisplayName("VehicleIds"), Type(typeof(List<int>)), ControlType(ReportParametersUtils.VehiclesPicker, "VehiclesPicker")] 
		[Order(2)]
		[XmlIgnore]
		public List<int> VehicleIds { get { return _vehicleIds?.ToList(); } set { _vehicleIds = value?.ToArray(); } }

		[NonSerialized]
		private List<DateTime> _dates;

		/// <summary> Список дат, за которые нужно построить отчёт </summary>
		/// <remarks>Требование от Pashupati/Chanson: нужно исключать некоторые дни, например, праздники </remarks>
		[DisplayName("Dates"), Type(typeof(List<DateTime>)), ControlType(ReportParametersUtils.DatesPicker, "DatesPicker")]
		[Order(3)]
		[XmlIgnore]
		public List<DateTime> Dates { get { return _dates; } set { _dates = value; } }

		[NonSerialized]
		private WorkingHoursInterval _workingHoursInterval;

		[DisplayName("WorkingHoursInterval"), Type(typeof(WorkingHoursInterval))]
		[ControlType(ReportParametersUtils.WorkingHoursInterval, "WorkingHoursInterval"), Order(256)]
		public WorkingHoursInterval WorkingHoursInterval
		{
			get { return _workingHoursInterval; }
			set { _workingHoursInterval = value; }
		}

		#region IMonitoreeObjectContainer Members

		public int MonitoreeObjectId
		{
			get { return vehicleGroupID; }
		}

		public IdType MonitoreeObjectIdType
		{
			get { return IdType.VehicleGroup; }
		}

		#endregion
	}
}