﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Групповой отчёт по пробегу </summary>
	[Guid("7CF57572-0F9A-4f82-9136-E191DF144C9A")]
	public class VehicleGroupMove : TssReportBase
	{
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ReportDescription"];
		}

		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ReportFullDescription"];
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			if (!base.ReportParametersInstanceCheck(iReportParameters))
				return false;

			// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
			var clsReportParameters = iReportParameters as VehicleGroupMoveParameters;
			if (clsReportParameters == null)
				return false;

			if (clsReportParameters.VehicleGroupId <= 0)
			{
				if (clsReportParameters.VehicleIds == null ||
					clsReportParameters.VehicleIds.Count == 0)
					return false;
			}

			if (clsReportParameters.Dates == null ||
				clsReportParameters.Dates.Count == 0)
			{
				if (clsReportParameters.DateFrom > clsReportParameters.DateTo)
					return false;
			}

			return true;
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new VehicleGroupMoveParameters { OperatorId = settings.OperatorId };
		}
		public override int Order
		{
			get { return 1; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new VehicleGroupMoveParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (VehicleGroupMoveParameters)iReportParameters;

				// Если есть набор идентификаторов, то очищаем идентификатор группы и считаем, что есть произвольный набор машин
				if (0 < (reportParameters.VehicleIds?.Count ?? 0))
					reportParameters.VehicleGroupId = -1;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}
		}
		private struct Interval
		{
			public readonly DateTime From;
			public readonly DateTime To;
			public Interval(DateTime @from, DateTime @to)
			{
				From = @from;
				To   = @to;
			}
		}
		private List<Interval> GetIntervals(VehicleGroupMoveParameters parameters)
		{
			var days = GetDays(parameters);
			var result = new List<Interval>(days.Count);
			foreach (var day in days)
			{
				DateTime localDateTimeFrom;
				DateTime localDateTimeTo;

				if (parameters.WorkingHoursInterval != null &&
					parameters.WorkingHoursInterval.WorkFrom != parameters.WorkingHoursInterval.WorkTo)
				{
					localDateTimeFrom = day.Add(parameters.WorkingHoursInterval.WorkFrom);

					localDateTimeTo = parameters.WorkingHoursInterval.WorkFrom < parameters.WorkingHoursInterval.WorkTo
						? day.Add(parameters.WorkingHoursInterval.WorkTo)
						: day.AddDays(1).Add(parameters.WorkingHoursInterval.WorkTo);
				}
				else
				{
					localDateTimeFrom = day;
					localDateTimeTo = day.AddDays(1);
				}

				result.Add(new Interval(localDateTimeFrom, localDateTimeTo));
			}
			return result;
		}
		/// <summary> Подготовка списка параметров для передачи в функцию вызова хранимой процедуры </summary>
		/// <param name="parameters"> Параметры отчета </param>
		/// <returns></returns>
		private ArrayList ParamsPreparingForStorProc(VehicleGroupMoveParameters parameters)
		{
			var arParams = new ArrayList();

			var dayIntervalTable = new DataTable("DateTimeRange");
			dayIntervalTable.Columns.Add("dateFrom", typeof(int));
			dayIntervalTable.Columns.Add("dateTo", typeof(int));
			var intervals = GetIntervals(parameters);
			foreach (var interval in intervals)
			{
				var logTimeFrom = TimeHelper.GetSecondsFromBase(
					TimeZoneInfo.ConvertTimeToUtc(
						DateTime.SpecifyKind(interval.From, DateTimeKind.Unspecified),
						parameters.TimeZoneInfo));
				var logTimeTo = TimeHelper.GetSecondsFromBase(
					TimeZoneInfo.ConvertTimeToUtc(
						DateTime.SpecifyKind(interval.To, DateTimeKind.Unspecified),
						parameters.TimeZoneInfo));
				dayIntervalTable.Rows.Add(logTimeFrom, logTimeTo);
			}
			dayIntervalTable.AcceptChanges();

			arParams.Add(new ParamValue("@logTimeList", dayIntervalTable));
			arParams.Add(new ParamValue("@operator_Id", parameters.OperatorId));

			if (parameters.VehicleIds != null && parameters.VehicleIds.Count != 0)
			{
				arParams.Add(new ParamValue("@vehicleIds", GetDataTable(parameters.VehicleIds)));
			}
			else if (parameters.VehicleGroupId > 0)
			{
				//TODO: получить содержимое группы объектов и вызвать с параметром @vehicleIds, чтобы унифицировать вызов хранимой процедуры
				arParams.Add(new ParamValue("@vehicleGroupId", parameters.VehicleGroupId));
			}

			return arParams;
		}
		private static List<DateTime> GetDays(VehicleGroupMoveParameters parameters)
		{
			DateTime dtFrom = parameters.DateFrom;
			DateTime dtTo   = parameters.DateTo;

			List<DateTime> days;
			if (parameters.Dates != null && parameters.Dates.Count != 0)
			{
				if (VehicleGroupMoveParameters.MaxDays < parameters.Dates.Count)
					days = parameters.Dates.OrderBy(x => x).Take(VehicleGroupMoveParameters.MaxDays).ToList();
				else
					days = parameters.Dates.OrderBy(x => x).ToList();
			}
			else
			{
				if (VehicleGroupMoveParameters.MaxDays < (dtTo - dtFrom).TotalDays + 1)
					dtTo = dtFrom.AddDays(VehicleGroupMoveParameters.MaxDays - 1);
				days = TimeHelper.GetDayList(dtFrom, dtTo);
			}

			return days;
		}
		private DataTable GetDataTable(List<int> vehicleIds)
		{
			var result = new DataTable();
			result.Columns.Add("Id", typeof(int));
			foreach (var vehicleId in vehicleIds)
				result.Rows.Add(vehicleId);
			result.AcceptChanges();
			return result;
		}
		protected ReportVehicleGroupMoveDataSet GetReportTypedDataSet(ReportParameters iReportParameters)
		{
			ReportVehicleGroupMoveDataSet dsReport = null;
			try
			{
				dsReport = new ReportVehicleGroupMoveDataSet();

				if (iReportParameters != null && iReportParameters.ShowBlankReportProp == false)
				{
					var reportParameters = (VehicleGroupMoveParameters)iReportParameters;

					DateTime dtFrom = reportParameters.DateFrom;
					DateTime dtTo = reportParameters.DateTo;

					// Получение данных для отчета из БД.

					var ds = GetDataFromDatabase(reportParameters);

					var vehicleIds = ds.Tables["Data"].Rows.Cast<DataRow>().Select(row => row["Vehicle_ID"]).Cast<int>().Distinct().ToArray();
					var args = new GetVehiclesArgs
					{
						VehicleIDs = vehicleIds,
						Culture = reportParameters.Culture
					};
					var vehicles = mclsInstance.GetVehiclesWithPositions(
						reportParameters.OperatorId, null, args).ToDictionary(v => v.id);
					foreach (DataRow row in ds.Tables["Data"].Rows)
					{
						var vehicleId = (int)row["Vehicle_ID"];
						Vehicle vehicle;
						string vehicleName = null;
						if (vehicles.TryGetValue(vehicleId, out vehicle))
							vehicleName = vehicle.Name;
						if (string.IsNullOrWhiteSpace(vehicleName))
							vehicleName = ResourceContainers.Get(reportParameters.Culture)["Noname"];
						row["vehicle_name"] = vehicleName;
					}

					var dataView = ds.Tables["Data"].DefaultView;
					dataView.Sort = "vehicle_name";

					ds.Tables.Remove("Data");
					ds.Tables.Add(dataView.ToTable());

					var daysTable = new DataTable("Days");
					ds.Tables.Add(daysTable);
					for (var i = 1; i <= VehicleGroupMoveParameters.MaxDays; ++i)
						daysTable.Columns.Add(i.ToString(CultureInfo.InvariantCulture));
					var daysRow = daysTable.NewRow();
					daysTable.Rows.Add(daysRow);

					var headerTable = ds.Tables["Header"];

					if (headerTable.Rows.Count != 0)
					{
						var fromColumn = headerTable.Columns.Add("From", typeof(string));
						var toColumn = headerTable.Columns.Add("To", typeof(string));
						var headerRow = headerTable.Rows[0];
						headerRow[fromColumn] = dtFrom.ToString("dd.MM.yyyy");
						headerRow[toColumn] = dtTo.ToString("dd.MM.yyyy");
					}

					var dayList = GetDays(reportParameters);
					for (var i = 0; i < dayList.Count; ++i)
						daysRow[(i + 1).ToString(CultureInfo.InvariantCulture)] = dayList[i].ToString("dd.MM");

					if (ds.Tables["Data"].Rows.Count > 0 && ds.Tables["Header"].Rows.Count > 0)
					{
						dsReport.Merge(ds);
					}
					else
					{
						// Заполнение заголовка отчета (Формирование строки в таблице ReportHeader)
						ReportVehicleGroupMoveDataSet.HeaderRow rowHeader = dsReport.Header.NewHeaderRow();
						rowHeader.From = dtFrom.ToShortDateString() + " " + dtFrom.ToShortTimeString();
						rowHeader.To = dtTo.ToShortDateString() + " " + dtTo.ToShortTimeString();
						rowHeader.TotalFuel = 0;
						rowHeader.TotalRun = 0;
						dsReport.Header.AddHeaderRow(rowHeader);
					}
				}

				//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
				ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
				//Добавление в датасет данных атрибутов предприятия.
				dsReport.Merge(dsEstablishmentAttributes);

				return dsReport;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}
			finally
			{
				if (dsReport != null)
				{
					dsReport.Dispose();
				}
			}
		}
		private DataSet GetDataFromDatabase(VehicleGroupMoveParameters reportParameters)
		{
			var ds = mclsInstance.GetDataFromDB(
				ParamsPreparingForStorProc(reportParameters),
				"ReportVehicleGroupMoveV2",
				new[] { "Data", "Header" }
				);
			return ds;
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var typedData = GetReportTypedDataSet(reportParameters);
			return GetReportHtml(typedData, (VehicleGroupMoveParameters)reportParameters);
		}
		private string GetReportHtml(ReportVehicleGroupMoveDataSet dataSet, VehicleGroupMoveParameters reportParameters)
		{
			var stringBuilder = new StringBuilder();
			var @string       = GetResourceStringContainer(reportParameters.Culture);

			var intervals = GetIntervals(reportParameters);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteStartElement("thead");

				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "header44");
				xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

				xw.WriteElementString("td", @string["NumberCaption"]);
				xw.WriteElementString("td", @string["NameCaption"]);

				var dayRow = dataSet.Tables["Days"].Rows[0];
				var daysDict = new SortedDictionary<int, string>();
				var iter = 0;
				foreach (var o in dayRow.ItemArray.Where(x => x != DBNull.Value))
				{
					++iter;
					daysDict.Add(iter, o.ToString());
					xw.WriteElementString("td", o.ToString());
				}

				var dataTable = dataSet.Tables["Data"];
				var odometerPresent = dataTable.Rows.Cast<DataRow>().Any(dr => !dr.IsNull("OdometerStart") || !dr.IsNull("OdometerEnd"));

				if (odometerPresent)
				{
					xw.WriteElementString("td", @string["OdometerStartCaption"]);
					xw.WriteElementString("td", @string["OdometerEndCaption"]);
				}

				xw.WriteElementString("td", @string["TotalRunCaption"]);
				xw.WriteElementString("td", @string["FuelConsumptionCaption"]);

				//tr
				xw.WriteEndElement();
				//thead
				xw.WriteEndElement();

				xw.WriteStartElement("tbody");
				var irow = 0;

				foreach (DataRow dr in dataTable.Rows)
				{
					++irow;
					var vehicleId = (int)dr["Vehicle_ID"];

					xw.WriteStartElement("tr");

					xw.WriteElementString("td", irow.ToString(CultureInfo.InvariantCulture));
					xw.WriteElementString("td", dr["Vehicle_Name"].ToString());

					foreach (var day in daysDict.Keys)
					{
						xw.WriteStartElement("td");
						var text = dr[day.ToString(CultureInfo.InvariantCulture)].ToString();
						var interval = intervals[day - 1];
						xw.WritePathLink(reportParameters, vehicleId, interval.From, interval.To, text);
						xw.WriteEndElement();
					}

					if (odometerPresent)
					{
						xw.WriteElementString("td", dr["OdometerStart"].ToString());
						xw.WriteElementString("td", dr["OdometerEnd"].ToString());
					}

					xw.WriteElementString("td", dr["SumRun"].ToString());

					var fuelSpentByStandard = dr["FuelSpentByStandard"] as decimal?;

					xw.WriteStartElement("td");

					if (fuelSpentByStandard == null)
					{
						xw.WriteStartElement("a");
						xw.WriteAttributeString("href",
												string.Format(
													"///{0}/objects.aspx?id={1}#1",
													reportParameters.ApplicationPath, vehicleId));
						xw.WriteAttributeString("title", @string["FuelSpentStandardIsNotSetDescription"]);
						xw.WriteString(@string["FuelSpentStandardIsNotSet"]);
						xw.WriteEndElement();
					}
					else
					{
						xw.WriteAttributeString("class", "number");
						xw.WriteString(fuelSpentByStandard.Value.ToString(reportParameters.Culture));
					}

					xw.WriteEndElement(); //td.fuelSpendByStandard

					//tr
					xw.WriteEndElement();
				}

				//tbody
				xw.WriteEndElement();

				xw.WriteStartElement("tfoot");
				xw.WriteStartElement("tr");

				xw.WriteElementString("td", string.Empty);
				xw.WriteElementString("td", @string["Total"]);
				xw.WriteStartElement("td");
				// + два поля для показаний одометра
				xw.WriteAttributeString("colspan", (daysDict.Count + (odometerPresent ? 2 : 0)).ToString(CultureInfo.InvariantCulture));
				//td
				xw.WriteEndElement();

				var totalRun = Convert.ToInt32(dataSet.Tables["Header"].Rows[0]["TotalRun"]);
				var totalFuel = Convert.ToInt32(dataSet.Tables["Header"].Rows[0]["TotalFuel"]);
				xw.WriteElementString("td", totalRun.ToString(CultureInfo.InvariantCulture));
				xw.WriteElementString("td", totalFuel > 0 ? totalFuel.ToString(CultureInfo.InvariantCulture) : string.Empty);

				//tr
				xw.WriteEndElement();
				//tfoot
				xw.WriteEndElement();
			}
			return stringBuilder.ToString();
		}

		/// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			ReportClassWrapper rptResultReport = null;
			ReportVehicleGroupMoveDataSet dataSet = null;
			var processed = false;
			try
			{
				dataSet = GetReportTypedDataSet(iReportParameters);

				//Создание экземпляра отчета.
				rptResultReport = new VehicleGroupMoveReport();

				//Локализация надписей отчета
				if (iReportParameters != null)
					TranslateReport<VehicleGroupMove>(rptResultReport, iReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(dataSet);

				if (rptResultReport.ExportOptions.ExportFormatType == ExportFormatType.Excel ||
					rptResultReport.ExportOptions.ExportFormatType == ExportFormatType.ExcelRecord)
				{
					rptResultReport.ExportOptions.FormatOptions = new ExcelFormatOptions
					{
						ExcelUseConstantColumnWidth = false,
						ExcelTabHasColumnHeadings = true
					};
				}

				//Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();

				if (dataSet != null)
					dataSet.Dispose();
			}
		}
	}
}