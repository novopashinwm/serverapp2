﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Reports.DrivingSafety;
using Compass.Ufin.Reports.GeocodingViaExcel;
using Compass.Ufin.Reports.PassengerFlow;
using Compass.Ufin.Reports.ShareLinks;
using FORIS.TSS.BusinessLogic.Reports;
using FORIS.TSS.TransportDispatcher.FuelCostCalculation;
using RU.NVG.NIKA.Reports.AddRemoveServicesReport;
using RU.NVG.NIKA.Reports.CommercialServices;
using RU.NVG.NIKA.Reports.DataAbsence;

namespace FORIS.TSS.TransportDispatcher.Reports.ReportCollection
{
	public class Repository : IReportRepository
	{
		private readonly TssReportBase[] _reports =
		{
			new MoveGroupDayTssReport(),
			new WebGisMoveHistory(),
			new WebGisMoveHistoryTwoColumns(),
			new VehicleGroupMove(),
			new FuelCostCalculationReport(),
			new ZoneLeaveEnterReport(),
			new RoutingSheet(),
			new RoutingSheetSummary(),
			new ReportSensorsSingle(),
			new AccessControlReport(),
			new GeoZoneOverTssReport(),
			new SpeedViolationTSSReport(),
			new SpeedViolationVehicleTSSReport(),
			new FuelCostTSSReport(),
			new FuelCostSingleTSSReport(),
			new CurrentStateObjectsReport(),
			new DataAbsenceReport(),
			new CommercialServicesReport(),
			new AddRemoveServicesReport(),
			new PassengerFlowReport(),
			new RoutePointsReport(),
			new TripsAndMileageReport(),
			new ShareLinksReport(),
			new SensorValueHistoryReport(),
			new DriverStatusReport(),
		};

		private readonly Dictionary<Guid, TssReportBase> _guidToReports;
		private static Repository                        _instance;
		private static readonly object InstanceLock = new object();
		private Repository()
		{
			_guidToReports = _reports.ToDictionary(key => key.GetGuid());
		}
		public TssReportBase this[Guid guid]
		{
			get
			{
				return _guidToReports.TryGetValue(guid, out TssReportBase result) ? result : null;
			}
		}
		public IEnumerable<ITssReport> Reports
		{
			get { return _reports; }
		}
		public static IReportRepository Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (InstanceLock)
					{
						if (_instance == null)
						{
							_instance = new Repository();
						}
					}
				}
				return _instance;
			}
		}
	}
}