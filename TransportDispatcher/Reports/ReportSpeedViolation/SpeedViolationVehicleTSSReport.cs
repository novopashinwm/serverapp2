﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;


namespace FORIS.TSS.TransportDispatcher.Reports
{
	#region SpeedViolationTSSReport
	/// <summary> Отчет о превышении скорости </summary> 
	[Guid("E3C8AAD3-9682-48FB-A2B4-F5C45927797C")]
	public class SpeedViolationVehicleTSSReport : SpeedViolationTSSReport
	{
		//Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета.
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new SpeedVehicleViolationReportParameters();

			//Датасет для хранения значений доопределенных параметров, полученных из БД через хранимую процедуру.
		}

		public override int Order
		{
			get { return 2; }
		}

		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}

		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
		/// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters">Набор параметров для создания отчета</param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters == null)
				{
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new SpeedVehicleViolationReportParameters();
				}

				// Возвращаются скорректированные параметры.
				return iReportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}

		}

		/// <summary>
		/// Функция проверяет переданный набор параметров на корректность заполнения.
		/// </summary>
		/// <param name="iReportParameters">Заполненный набор параметров отчета</param>
		/// <returns>В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false</returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			//Проверка был ли передан набор параметров для отчета.
			var clsReportParameters = iReportParameters as SpeedVehicleViolationReportParameters;
			if (clsReportParameters == null)
				return false;

			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.

			if (clsReportParameters.SpeedInput <= 0
				|| clsReportParameters.DateFrom.ToShortDateString() == ""
				|| clsReportParameters.DateTo.ToShortDateString() == "")
			{
				return false;
			}

			if (clsReportParameters.SpeedInput <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}

		/// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{

			//Датасет с требуемой структурой данных для формы отчета.
			SpeedViolationDataset dsSpeedViolationDataset = null;
			ReportClassWrapper rptResultReport = null;
			var processed = false;
			try
			{
				//Создание пустого датасета с требуемой структурой данных для формы отчета.
				dsSpeedViolationDataset = new SpeedViolationDataset();

				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters != null)
				{
					//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
					var clsReportParameters = iReportParameters as SpeedVehicleViolationReportParameters;
					//Требуется заполнение отчета данными?
					if (clsReportParameters != null && !clsReportParameters.ShowBlankReportProp)
					{
						#region Заполнение таблицы

						// Заполнение заголовка отчета
						// Создание строки, имеющей структуру данных как у таблицы ReportHeader.
						SpeedViolationDataset.ReportHeaderRow rowReportHeader = dsSpeedViolationDataset.ReportHeader.NewReportHeaderRow();
						// Период, за который строится отчет
						rowReportHeader.Period =
							clsReportParameters.DateTimeInterval.DateFrom.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
						rowReportHeader.Period += " - ";
						rowReportHeader.Period +=
							clsReportParameters.DateTimeInterval.DateTo.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);

						// Группа ТС
						rowReportHeader.VehicleGroup = (clsReportParameters.VehicleGroupProp != null && clsReportParameters.VehicleGroupProp.Text != null)
														   ? clsReportParameters.VehicleGroupProp.Text.Trim()
														   : string.Empty;
						// Допустимая скорость
						rowReportHeader.Speed = clsReportParameters.SpeedInput.ToString(clsReportParameters.Culture);
						//Отправка данных в отчет.
						dsSpeedViolationDataset.ReportHeader.AddReportHeaderRow(rowReportHeader);

						var dsDataFromDB = GetReportDataSet(clsReportParameters);

						string strVehicle = "";

						var totals = dsDataFromDB.Tables["SpeedViolation"].Rows.Cast<DataRow>().ToList()
						.Select(dr => new
						{
							VehicleID   = (int)dr["vehicle_id"],
							VehicleName = dr["vehicle_name"] as string,
							Duration    = (int)dr["to_log_time"] - (int)dr["from_log_time"],
							Run         = Convert.ToDecimal(dr["run"])
						})
						.GroupBy(x => x.VehicleID)
						.Select(x => new
						{
							Id       = x.Key,
							Name     = x.First().VehicleName,
							Count    = x.Count(),
							Duration = x.Sum(y => y.Duration),
							OvrRun   = x.Sum(y => y.Run)
						});
						foreach (var total in totals)
						{
							var ovrRun = total.OvrRun;
							var allRun = (decimal)mclsInstance.GetMoveHistoryDistanceValue(
								clsReportParameters.DateFromInt, clsReportParameters.DateToInt, total.Id) * 0.001M;
							var prcRun = default(decimal?);
							if (0 != allRun)
								prcRun = ovrRun / allRun;

							SpeedViolationDataset.summaryRow summaryRow = dsSpeedViolationDataset.summary.NewsummaryRow();

							summaryRow.VEHICLE  = total.Name;
							summaryRow.COUNT    = total.Count.ToString(clsReportParameters.Culture);
							summaryRow.DURATION = TimeHelper.GetTimeSpanReadableShort(TimeSpan.FromSeconds(total.Duration), clsReportParameters.Culture);
							summaryRow.OVRRUN   = ovrRun.ToString("#0.000", clsReportParameters.Culture);
							summaryRow.ALLRUN   = allRun.ToString("#0.000", clsReportParameters.Culture);
							if (prcRun.HasValue)
								summaryRow.PERCENT  = prcRun.Value.ToString("#0.00%", clsReportParameters.Culture);

							dsSpeedViolationDataset.summary.AddsummaryRow(summaryRow);
						}

						foreach (DataRow dr in dsDataFromDB.Tables["SpeedViolation"].Rows)
						{
							//Создание строки, имеющей структуру данных как у таблицы подотчета 
							SpeedViolationDataset.dataRow rowData = dsSpeedViolationDataset.data.NewdataRow();

							// ТС
							if (dr["vehicle_name"].ToString().Trim() != strVehicle)
							{
								rowData.VEHICLE = dr["vehicle_name"].ToString();
								strVehicle = dr["vehicle_name"].ToString().Trim();
							}

							// Водитель
							rowData.DRIVER = dr["DRIVER"].ToString().Trim();

							// Интервал времени, когда наблюдалось превышение
							rowData.DATETIME =
								iReportParameters.FormatUtcAsLocal(
									TimeHelper.GetDateTimeUTC((int)dr["FROM_LOG_TIME"]))
								+ " - " +
								iReportParameters.FormatUtcAsLocal(
									TimeHelper.GetDateTimeUTC((int)dr["TO_LOG_TIME"]));

							// Скорость
							rowData.SPEED = dr["SPEED"].ToString();

							//Отправка данных в отчет.
							dsSpeedViolationDataset.data.AdddataRow(rowData);
						}

						#endregion Заполнение таблицы
					}
				}

				//Датасет для хранения констант с атрибутами предприятия.
				//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
				ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
				//Добавление в датасет данных атрибутов предприятия.
				dsSpeedViolationDataset.Merge(dsEstablishmentAttributes);

				//Создание экземпляра отчета.
				rptResultReport = new SpeedViolationReport();

				//Локализация надписей отчета
				if (iReportParameters != null)
					TranslateReport<SpeedViolationVehicleTSSReport>(rptResultReport, iReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(dsSpeedViolationDataset);
				// Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();

				if (dsSpeedViolationDataset != null)
					dsSpeedViolationDataset.Dispose();
			}
		}
	}
	#endregion SpeedViolationTSSReport
}