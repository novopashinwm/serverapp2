﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary>
	/// Параметры для данного отчета.
	/// </summary>
	[Serializable]
	public class SpeedVehicleViolationReportParameters : ReportParameters, IMonitoreeObjectContainer
	{
		/// <summary>
		/// Время начала периода, на который формируется отчет.
		/// </summary>
		protected string mstrReportTimeFrom = ""; //DateTime.Now.ToShortTimeString();
		/// <summary>
		/// Время окончания периода, на который формируется отчет.
		/// </summary>
		protected string mstrReportTimeTo = ""; //DateTime.Now.ToShortTimeString();

		/// <summary>
		/// Скорость 
		/// </summary>
		protected int iSpeed = 60;

		/// <summary>
		/// Группа ТС
		/// </summary>
		protected TagListBoxItem tlbiVehicleGroup = new TagListBoxItem("", null);

		private int _vehicleGroupId;


		/// <summary>
		/// Скорость
		/// </summary>
		//[DisplayName("SpeedDN", 1, true), Category("ReportParsCat"), Description("SpeedDesc")]
		[DisplayName("SpeedInput"), Type(typeof(int)), ControlType(ReportParametersUtils.SpeedInput, "SpeedInput"), Order(1)]
		public int SpeedInput
		{
			get
			{
				return iSpeed;
			}
			set
			{
				iSpeed = value;
			}
		}

		/// <summary>
		/// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
		/// </summary>
		[Browsable(true)]
		[DisplayName("PeriodBeginDateDN", 2, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
		public override DateTime DateFrom
		{
			get
			{
				return dtDateFrom;
			}
			set
			{
				dtDateFrom = value;
			}
		}

		/// <summary>
		/// Время начала периода, на который формируется отчет.
		/// </summary>
		[DisplayName("PeriodBeginTimeDN", 3, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
		[XmlIgnore, Obsolete("Оставлен для совместимости с текущими расписаниями отчетов (для SoapSerializer). При переходе на другой сериализатор, можно будет удалить.")]
		public string ReportTimeFromProp { get; set; }

		/// <summary>
		/// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[Browsable(true)]
		[DisplayName("PeriodEndDateDN", 4, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
		public override DateTime DateTo
		{
			get
			{
				return dtDateTo;
			}
			set
			{
				dtDateTo = value;
			}
		}

		/// <summary>
		/// Время окончания периода, на который формируется доклад.
		/// </summary>
		[DisplayName("PeriodEndTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
		[XmlIgnore, Obsolete("Оставлен для совместимости с текущими расписаниями отчетов (для SoapSerializer). При переходе на другой сериализатор, можно будет удалить.")]
		public string ReportTimeToProp { get; set; }

		/// <summary>
		/// Группа ТС, для которой формируется отчет.
		/// </summary>
		[DisplayName("TSGroupDN", 6, true), Category("ReportParsCat"), Description("TSGroupDesc")]
		public TagListBoxItem VehicleGroupProp
		{
			get
			{
				return tlbiVehicleGroup;
			}
			set
			{
				tlbiVehicleGroup = value;
			}
		}

		[DisplayName("VehicleId"), Type(typeof(int))]
		[ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker")]
		[Options("AllowNulls:1", SystemRight.PathAccess), Order(5)]
		public int VehicleId
		{
			get { return _vehicleGroupId; }
			set
			{
				if (tlbiVehicleGroup == null)
				{
					tlbiVehicleGroup = new TagListBoxItem
					{
						Tag = value
					};
				}
				_vehicleGroupId = value;
			}
		}

		#region IMonitoreeObjectContainer Members

		IdType IMonitoreeObjectContainer.MonitoreeObjectIdType
		{
			get { return IdType.Vehicle; }
		}

		int IMonitoreeObjectContainer.MonitoreeObjectId
		{
			get { return VehicleId; }
		}

		#endregion
	}
}
