﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	#region SpeedViolationTSSReport
	/// <summary> Отчет о превышении скорости </summary> 
	[Guid("B7CB4C01-50DB-4bf4-82AB-F71E5D595BCC")]
	public class SpeedViolationTSSReport : TssReportBase
	{
		protected const string RunFormatKilometers = "0.000";
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[GetType().Name];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[GetType().Name + "Description"];
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new SpeedViolationReportParameters();
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			return GetReportHtml(reportParameters);
		}
		private string GetReportHtml(ReportParameters clsReportParameters)
		{
			var stringBuilder = new StringBuilder();
			var report = GetReportDataSet(clsReportParameters);
			var @string = GetResourceStringContainer(clsReportParameters.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xw.WriteAttributeString("class", "reportBody");
				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["CaptionReport"]);
				xw.WriteElementString("td", @string[GetType().Name]);
				xw.WriteEndElement(); //tr


				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["CaptionDateFrom"]);
				xw.WriteElementString("td", clsReportParameters.DateFrom.ToShortDateString());
				xw.WriteEndElement(); //tr

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["CaptionDateTo"]);
				xw.WriteElementString("td", clsReportParameters.DateTo.ToShortDateString());
				xw.WriteEndElement(); //tr

				xw.WriteEndElement(); //table

				//Сводный отчёт: общее количество фактов езды с превышением скорости, суммарное время и пробег
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xw.WriteAttributeString("class", "reportTable withBorders");
				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("th", @string["SummaryColObjHdr"]);

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["SummaryColCntHdrDescription"]);
				xw.WriteString(@string["SummaryColCntHdr"]);
				xw.WriteEndElement();

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["SummaryColDurHdrDescription"]);
				xw.WriteString(@string["SummaryColDurHdr"]);
				xw.WriteEndElement();

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["SummaryColOrvHdrDescription"]);
				xw.WriteString(@string["SummaryColOrvHdr"]);
				xw.WriteEndElement();

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["SummaryColAllHdrDescription"]);
				xw.WriteString(@string["SummaryColAllHdr"]);
				xw.WriteEndElement();

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["SummaryColPerHdrDescription"]);
				xw.WriteString(@string["SummaryColPerHdr"]);
				xw.WriteEndElement();

				xw.WriteEndElement(); //tr
				xw.WriteEndElement(); //thead

				var rows = report.Tables[0].Rows.Cast<DataRow>().ToList();
				var totals = rows.Select(dr => new
				{
					VehicleID   = (int)dr["vehicle_id"],
					VehicleName = dr["vehicle_name"] as string,
					Duration    = (int)dr["to_log_time"] - (int)dr["from_log_time"],
					Run         = Convert.ToDecimal(dr["run"])
				})
					.GroupBy(x => x.VehicleID)
					.Select(x => new
					{
						Id       = x.Key,
						Name     = x.First().VehicleName,
						Count    = x.Count(),
						Duration = x.Sum(y => y.Duration),
						OvrRun   = x.Sum(y => y.Run)
					});

				foreach (var total in totals.OrderBy(x => x.Name))
				{
					var ovrRun = total.OvrRun;
					var allRun = (decimal)mclsInstance.GetMoveHistoryDistanceValue(
						clsReportParameters.DateFromInt, clsReportParameters.DateToInt, total.Id) * 0.001M;
					var prcRun = default(decimal?);
					if (0 != allRun)
						prcRun = ovrRun / allRun;

					xw.WriteStartElement("tr");

					xw.WriteElementString("td", total.Name);
					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(total.Count.ToString(clsReportParameters.Culture));
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(TimeHelper.GetTimeSpanReadableShort(TimeSpan.FromSeconds(total.Duration), clsReportParameters.Culture));
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(ovrRun.ToString("#0.000", clsReportParameters.Culture));
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(allRun.ToString("#0.000", clsReportParameters.Culture));
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					if (prcRun.HasValue)
					{
						xw.WriteAttributeString("class", "number");
						xw.WriteString(prcRun.Value.ToString("#0.00%", clsReportParameters.Culture));
					}
					xw.WriteEndElement();

					xw.WriteEndElement(); //tr
				}
				xw.WriteEndElement(); //table

				//Детализация
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xw.WriteAttributeString("class", "reportTable withBorders");
				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("th", @string["SummaryColObjHdr"]);
				xw.WriteElementString("th", @string["timeFrom"]);
				xw.WriteElementString("th", @string["timeTo"]);
				xw.WriteElementString("th", @string["maxSpeed"]);

				xw.WriteStartElement("th");
				xw.WriteAttributeString("title", @string["runDescription"]);
				xw.WriteString(@string["run"]);
				xw.WriteEndElement();

				xw.WriteEndElement(); //tr
				xw.WriteEndElement(); //thead
				foreach (var dr in rows)
				{
					xw.WriteStartElement("tr");
					var dateFrom = (DateTime)dr["datetimefrom"];
					var dateTo = (DateTime)dr["datetimeto"];

					xw.WriteElementString("td", dr["vehicle_name"].ToString());
					xw.WriteElementString("td", dateFrom.ToString(TimeHelper.DefaultTimeFormatUpToMinutes));
					xw.WriteElementString("td", dateTo.ToString(TimeHelper.DefaultTimeFormatUpToMinutes));

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(dr["speed"].ToString());
					xw.WriteEndElement();


					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
							, clsReportParameters.ApplicationPath
							, dr["vehicle_id"]
							, dateFrom.ToString("dd.MM.yyyy+HH:mm:ss")
							, dateTo.ToString("dd.MM.yyyy+HH:mm:ss")
							);

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteStartElement("a");
					xw.WriteAttributeString("href", path.Replace(' ', '+'));
					var run = Convert.ToDecimal(dr["run"]);
					xw.WriteString(run.ToString(RunFormatKilometers, clsReportParameters.Culture));
					xw.WriteEndElement();
					xw.WriteEndElement();

					xw.WriteEndElement();
				}

				xw.WriteEndElement();
			}
			return stringBuilder.ToString();
		}

		protected DataSet GetReportDataSet(ReportParameters clsReportParameters)
		{
			var report = mclsInstance.GetDataFromDB(
				ParamsPreparingForStorProc(clsReportParameters),
				"dbo.GetOverSpeeding", new[] { "SpeedViolation" }
				);

			report.DataSetName  = "report";
			var table           = report.Tables["SpeedViolation"];
			var colVehicleName  = table.Columns.Add("vehicle_name", typeof(string));
			var colDateTimeFrom = table.Columns.Add("DateTimeFrom", typeof(DateTime));
			var colDateTimeTo   = table.Columns.Add("DateTimeTo",   typeof(DateTime));
			var colLogTimeFrom  = table.Columns["from_log_time"];
			var colLogTimeTo    = table.Columns["to_log_time"];

			var ids = table.Rows.Cast<DataRow>().Select(row => row["vehicle_id"]).Cast<int>().Distinct().ToArray();
			var args = new GetVehiclesArgs
			{
				VehicleIDs = ids,
				Culture    = clsReportParameters.Culture
			};
			var vehicles = mclsInstance.GetVehiclesWithPositions(clsReportParameters.OperatorId, null, args).ToDictionary(key => key.id);
			foreach (DataRow row in table.Rows)
			{
				var vehicleId = (int)row["vehicle_id"];
				row[colVehicleName] = vehicles[vehicleId].Name;
				row[colDateTimeFrom] =
					TimeZoneInfo.ConvertTimeFromUtc(
						TimeHelper.GetDateTimeUTC((int)row[colLogTimeFrom]), clsReportParameters.TimeZoneInfo);
				row[colDateTimeTo] =
					TimeZoneInfo.ConvertTimeFromUtc(
						TimeHelper.GetDateTimeUTC((int)row[colLogTimeTo]), clsReportParameters.TimeZoneInfo);
			}

			var view = table.DefaultView;
			view.Sort = "vehicle_name, from_log_time";
			report.Tables.RemoveAt(0);
			report.Tables.Add(view.ToTable());

			return report;
		}

		public override int Order
		{
			get { return 3; }
		}

		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
		/// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters">Набор параметров для создания отчета</param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters == null)
				{
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new SpeedViolationReportParameters();
				}

				// Возвращаются скорректированные параметры.
				return iReportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}

		}

		/// <summary>
		/// Функция проверяет переданный набор параметров на корректность заполнения.
		/// </summary>
		/// <param name="iReportParameters">Заполненный набор параметров отчета</param>
		/// <returns>В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false</returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			//Проверка был ли передан набор параметров для отчета.
			var clsReportParameters = iReportParameters as SpeedViolationReportParameters;
			if (clsReportParameters == null)
			{
				if (iReportParameters is SpeedVehicleViolationReportParameters)
					return base.ReportParametersInstanceCheck(iReportParameters);
				return false;
			}

			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.

			if (clsReportParameters.SpeedInput <= 0
				|| clsReportParameters.DateFrom.ToShortDateString() == ""
				|| clsReportParameters.DateTo.ToShortDateString() == "")
			{
				return false;
			}

			if (clsReportParameters.SpeedInput <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}

		/// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{

			//Датасет с требуемой структурой данных для формы отчета.
			SpeedViolationDataset dsSpeedViolationDataset = null;
			ReportClassWrapper rptResultReport = null;
			var processed = false;
			try
			{
				//Создание пустого датасета с требуемой структурой данных для формы отчета.
				dsSpeedViolationDataset = new SpeedViolationDataset();

				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters != null)
				{
					//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
					var clsReportParameters = iReportParameters as SpeedViolationReportParameters;
					//Требуется заполнение отчета данными?
					if (clsReportParameters.ShowBlankReportProp == false)
					{
						#region Заполнение таблицы

						// Заполнение заголовка отчета
						// Создание строки, имеющей структуру данных как у таблицы ReportHeader.
						SpeedViolationDataset.ReportHeaderRow rowReportHeader = dsSpeedViolationDataset.ReportHeader.NewReportHeaderRow();
						// Период, за который строится отчет
						rowReportHeader.Period =
							clsReportParameters.DateTimeInterval.DateFrom.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
						rowReportHeader.Period += " - ";
						rowReportHeader.Period +=
							clsReportParameters.DateTimeInterval.DateTo.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);

						// Группа ТС
						rowReportHeader.VehicleGroup = (clsReportParameters.VehicleGroupProp != null && clsReportParameters.VehicleGroupProp.Text != null)
							? clsReportParameters.VehicleGroupProp.Text.Trim()
							: string.Empty;
						// Допустимая скорость
						rowReportHeader.Speed = clsReportParameters.SpeedInput.ToString(clsReportParameters.Culture);
						//Отправка данных в отчет.
						dsSpeedViolationDataset.ReportHeader.AddReportHeaderRow(rowReportHeader);

						var dsDataFromDB = GetReportDataSet(clsReportParameters);

						string strVehicle = "";

						var totals = dsDataFromDB.Tables["SpeedViolation"].Rows.Cast<DataRow>().ToList()
							.Select(dr => new
							{
								VehicleID   = (int)dr["vehicle_id"],
								VehicleName = dr["vehicle_name"] as string,
								Duration    = (int)dr["to_log_time"] - (int)dr["from_log_time"],
								Run         = Convert.ToDecimal(dr["run"])
							})
							.GroupBy(x => x.VehicleID)
							.Select(x => new
							{
								Id       = x.Key,
								Name     = x.First().VehicleName,
								Count    = x.Count(),
								Duration = x.Sum(y => y.Duration),
								OvrRun   = x.Sum(y => y.Run)
							});
						foreach (var total in totals)
						{
							var ovrRun = total.OvrRun;
							var allRun = (decimal)mclsInstance.GetMoveHistoryDistanceValue(
								clsReportParameters.DateFromInt, clsReportParameters.DateToInt, total.Id) * 0.001M;
							var prcRun = default(decimal?);
							if (0 != allRun)
								prcRun = ovrRun / allRun;

							SpeedViolationDataset.summaryRow summaryRow = dsSpeedViolationDataset.summary.NewsummaryRow();

							summaryRow.VEHICLE  = total.Name;
							summaryRow.COUNT    = total.Count.ToString(clsReportParameters.Culture);
							summaryRow.DURATION = TimeHelper.GetTimeSpanReadableShort(TimeSpan.FromSeconds(total.Duration), clsReportParameters.Culture);
							summaryRow.OVRRUN   = ovrRun.ToString("#0.000", clsReportParameters.Culture);
							summaryRow.ALLRUN   = allRun.ToString("#0.000", clsReportParameters.Culture);
							if (prcRun.HasValue)
								summaryRow.PERCENT  = prcRun.Value.ToString("#0.00%", clsReportParameters.Culture);
							dsSpeedViolationDataset.summary.AddsummaryRow(summaryRow);
						}

						foreach (DataRow dr in dsDataFromDB.Tables["SpeedViolation"].Rows)
						{
							//Создание строки, имеющей структуру данных как у таблицы подотчета 
							SpeedViolationDataset.dataRow rowData = dsSpeedViolationDataset.data.NewdataRow();

							// ТС
							if (dr["vehicle_name"].ToString().Trim() != strVehicle)
							{
								rowData.VEHICLE = dr["vehicle_name"].ToString();
								strVehicle = dr["vehicle_name"].ToString().Trim();
							}

							// Водитель
							rowData.DRIVER = dr["DRIVER"].ToString().Trim();

							// Интервал времени, когда наблюдалось превышение
							rowData.DATETIME =
								iReportParameters.FormatUtcAsLocal(
									TimeHelper.GetDateTimeUTC((int)dr["FROM_LOG_TIME"]))
								+ " - " +
								iReportParameters.FormatUtcAsLocal(
									TimeHelper.GetDateTimeUTC((int)dr["TO_LOG_TIME"]));

							// Скорость
							rowData.SPEED = dr["SPEED"].ToString();

							//Отправка данных в отчет.
							dsSpeedViolationDataset.data.AdddataRow(rowData);
						}

						#endregion Заполнение таблицы
					}
				}

				//Датасет для хранения констант с атрибутами предприятия.
				//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
				ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
				//Добавление в датасет данных атрибутов предприятия.
				dsSpeedViolationDataset.Merge(dsEstablishmentAttributes);

				//Создание экземпляра отчета.
				rptResultReport = new SpeedViolationReport();

				//Локализация надписей отчета
				if (iReportParameters != null)
					TranslateReport<SpeedViolationTSSReport>(rptResultReport, iReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(dsSpeedViolationDataset);
				// Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();

				if (dsSpeedViolationDataset != null)
					dsSpeedViolationDataset.Dispose();
			}
		}

		#region Подготовка списка параметров для передачи в функцию вызова хранимой процедуры
		private ArrayList ParamsPreparingForStorProc(ReportParameters reportParams)
		{
			// Заменяем дату окончания периода на конец суток до секунд
			if (reportParams.DateTimeInterval.DateTo.TimeOfDay == TimeSpan.Zero)
				reportParams.DateTimeInterval.DateTo = reportParams.DateTimeInterval.DateTo.AddDays(1).AddSeconds(-1);
			// Готовим парметры для процедуры
			var iDateTimeFrom = TimeHelper.GetSecondsFromBase(
				TimeHelper.GetUtcTime(reportParams.DateTimeInterval.DateFrom, reportParams.TimeZoneInfo));
			var iDateTimeTo        = TimeHelper.GetSecondsFromBase(
				TimeHelper.GetUtcTime(reportParams.DateTimeInterval.DateTo, reportParams.TimeZoneInfo));
			var vehicleParams      = reportParams as SpeedVehicleViolationReportParameters;
			var vehicleGroupParams = reportParams as SpeedViolationReportParameters;
			var iVehicleGroupID    = vehicleGroupParams != null ? vehicleGroupParams.VehicleGroupId : 0;
			var iVehicleID         = vehicleParams != null ? vehicleParams.VehicleId : 0;
			var iSpeed             = vehicleParams != null
					? vehicleParams.SpeedInput
					: vehicleGroupParams != null
						? vehicleGroupParams.SpeedInput
						: 0;

			var arParams = new ArrayList
			{
				new ParamValue("@time_from",       iDateTimeFrom),
				new ParamValue("@time_to",         iDateTimeTo),
				new ParamValue("@vehiclegroup_id", iVehicleGroupID),
				new ParamValue("@vehicle_id",      iVehicleID),
				new ParamValue("@speed_top",       iSpeed),
				new ParamValue("@operatorID",      reportParams.OperatorId)
			};

			return arParams;
		}
		#endregion Подготовка списка параметров для передачи в функцию вызова хранимой процедуры


	}
	#endregion SpeedViolationTSSReport
}