<xsl:stylesheet version="1.0"
 xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">
      <xsl:apply-templates/>
    </Workbook>

  </xsl:template>

  <xsl:template match="/*">
    <Styles>
      <Style ss:ID="Default" ss:Name="Normal">
        <Alignment ss:Vertical="Bottom"/>
        <Borders/>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000"/>
        <Interior/>
        <NumberFormat/>
        <Protection/>
      </Style>
      <Style ss:ID="s63">
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
      </Style>
      <Style ss:ID="s67">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
      </Style>
      <Style ss:ID="s68">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat/>
      </Style>
      <Style ss:ID="s74">
        <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
        <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
      </Style>
      <Style ss:ID="s75">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat ss:Format="0"/>
      </Style>
    </Styles>
    <Worksheet>
      <xsl:attribute name="ss:Name">
        <xsl:value-of select="/AccessControlReport/Legend/reportLegendValue[1]"/>
      </xsl:attribute>
      <Table ss:ExpandedColumnCount="12" x:FullColumns="1" x:FullRows="1" ss:DefaultRowHeight="15">
        <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="75.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="120"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="68.25"/>
        <Column ss:AutoFitWidth="0" ss:Width="66.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="76.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="70.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="81"/>
        <Column ss:AutoFitWidth="0" ss:Width="87"/>
        <xsl:apply-templates select="//Legend" />
        <Row />
        <xsl:apply-templates select="//bodyHeader" />
        <xsl:apply-templates select="//bodyData" />
      </Table>
    </Worksheet>
  </xsl:template>

  <xsl:template match="Legend">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="reportLegendKey">
    <Cell ss:MergeAcross="1" ss:StyleID="s63">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="reportLegendValue">
    <Cell ss:MergeAcross="4">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyHeader">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="bodyHeader/VehicleName">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyHeader/JustDate">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyHeader/TimeIncoming">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyHeader/TimeOutgoing">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyData">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="bodyData/VehicleName">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyData/JustDate">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyData/TimeIncoming">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="bodyData/TimeOutgoing">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

</xsl:stylesheet>