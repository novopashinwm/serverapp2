﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт "Маршрутный лист." </summary>
	[Guid("C0F0D3BE-33CD-4AAA-9043-C7578884982D")]
	public class RoutingSheet : TssReportBase
	{
		public override int Order
		{
			get { return 1; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeoPointReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			DataSet dataSet = null;
			RoutingExcelPdfReport report = null;
			var processed = false;
			try
			{
				dataSet = GetReportDataSet((RoutingSheetParameters)iReportParameters);
				report = new RoutingExcelPdfReport();
				report.SetDataSource(dataSet);
				processed = true;
				return report;
			}
			finally
			{
				if(dataSet != null) dataSet.Dispose();
				if(!processed && report != null) report.Dispose();
			}
		}
		private DataSet GetReportDataSet(RoutingSheetParameters clsReportParameters)
		{
			var isGroupReport = clsReportParameters.VehicleGroupId != 0;

			var dateTimeFrom = clsReportParameters.ToUtc(clsReportParameters.DateFrom);
			var dateTimeTo = clsReportParameters.ToUtc(clsReportParameters.DateTo.AddDays(1).AddSeconds(-1));

			List<ReportDataElement> finalList;
			List<GeoZone> nonVisitedZones;
			if (isGroupReport)
			{
				finalList = GetFinalListByVehicleGroup(
					clsReportParameters.VehicleGroupId
					, dateTimeFrom
					, dateTimeTo
					, clsReportParameters.OperatorId
					, clsReportParameters.GeoZoneGroupId
					, clsReportParameters.Culture
					, clsReportParameters.TimeZoneInfo, out nonVisitedZones);
			}
			else if (clsReportParameters.VehicleProp != null)
			{
				var vehicleId = clsReportParameters.VehicleProp.Tag as int?;
				if (vehicleId == null)
					throw new VehicleNotFoundException("Unable to determine vehicle or vehicle group to build the report");

				finalList = GetFinalListByVehicle(
					vehicleId.Value
					, dateTimeFrom
					, dateTimeTo
					, clsReportParameters.OperatorId
					, clsReportParameters.GeoZoneGroupId
					, clsReportParameters.TimeZoneInfo
					, out nonVisitedZones
					, clsReportParameters.Culture);
			}
			else
			{
				throw new VehicleNotFoundException("Unable to determine vehicle or vehicle group to build the report");
			}

			var ds = new DataSet("RoutingSheet");

			var @string = GetResourceStringContainer(clsReportParameters.Culture);

			// Заголовок.

			var hData = new DataTable("Legend");
			hData.Columns.Add(new DataColumn("reportLegendKey"));
			hData.Columns.Add(new DataColumn("reportLegendValue"));
			hData.Columns.Add(new DataColumn("id"));
			ds.Tables.Add(hData);

			var hRow = hData.NewRow();

			hRow["reportLegendKey"] = @string["CaptionReport"];
			hRow["reportLegendValue"] = @string["RoutingSheet"];
			hRow["id"] = "CaptionReport";

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			if (isGroupReport)
			{
				hRow["reportLegendKey"] = @string["CaptionObjectGroupName"];

				hRow["reportLegendValue"] = GetVehicleGroupName(clsReportParameters);
			}
			else
			{
				hRow["reportLegendKey"] = @string["CaptionObjectName"];
				hRow["reportLegendValue"] = GetVehicleName(clsReportParameters);
			}

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = @string["CaptionDateFrom"];
			hRow["reportLegendValue"] = clsReportParameters.DateFrom.ToShortDateString();

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = @string["CaptionDateTo"];
			hRow["reportLegendValue"] = clsReportParameters.DateTo.ToShortDateString();

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = @string["CaptionZoneGroupName"];


			hRow["reportLegendValue"] = GetZoneGroupName(clsReportParameters);


			hData.Rows.Add(hRow);

			// Данные.
			var tData = new DataTable("Report");
			tData.Columns.Add(new DataColumn("PointId"));
			tData.Columns.Add(new DataColumn("TotalRun"));
			tData.Columns.Add(new DataColumn("PointName"));
			tData.Columns.Add(new DataColumn("PointAddress"));
			tData.Columns.Add(new DataColumn("TimeIncoming"));
			tData.Columns.Add(new DataColumn("TimeOutgoing"));
			tData.Columns.Add(new DataColumn("TimeElapsed"));
			ds.Tables.Add(tData);

			// Заголовок таблицы.
			var cRow = tData.NewRow();
			cRow["PointId"] = @string["CaptionObjectName"];
			cRow["TotalRun"] = @string["Run"];
			cRow["PointName"] = @string["PointName"];
			cRow["PointAddress"] = @string["PointAddress"];
			cRow["TimeIncoming"] = @string["TimeIncoming"];
			cRow["TimeOutgoing"] = @string["TimeOutgoing"];
			cRow["TimeElapsed"] = @string["TimeElapsed"];
			tData.Rows.Add(cRow);

			if (isGroupReport)
			{
				foreach (var list in finalList.GroupBy(x => x.VehicleId).OrderBy(g => g.First().VehicleName))
				{
					var elements = list.OrderBy(x => x.Id).ToList();
					foreach (var el in elements)
					{
						var dRow = tData.NewRow();
						dRow["PointId"] = el.VehicleName;
						dRow["totalRun"] = el.Odometer;
						dRow["PointName"] = el.ZoneName;
						dRow["PointAddress"] = el.Address;
						dRow["TimeIncoming"] = el.TimeIncoming;
						dRow["TimeOutgoing"] = el.TimeOutgoing;
						dRow["TimeElapsed"] = el.TimeElapsed;
						tData.Rows.Add(dRow);
					}

					var finalRow = tData.NewRow();
					finalRow["PointId"] = string.Format(@string["TotalBy"], elements.First().VehicleName);
					finalRow["totalRun"] = string.Format("{0} {1}", elements.Sum(x => x.Odometer), @string["km"]);
					finalRow["TimeIncoming"] = string.Empty;
					finalRow["TimeOutgoing"] = string.Empty;
					finalRow["TimeElapsed"] = TimeSpan.FromSeconds(elements.Sum(x => x.TimeElapsed.TotalSeconds)).ToString();

					tData.Rows.Add(finalRow);
				}
			}
			else
			{
				foreach (var el in finalList)
				{
					var dRow = tData.NewRow();
					dRow["PointId"] = el.VehicleName;
					dRow["TotalRun"] = el.Odometer;
					dRow["PointName"] = el.ZoneName;
					dRow["PointAddress"] = el.Address;
					dRow["TimeIncoming"] = TimeZoneInfo.ConvertTimeFromUtc(el.TimeIncoming, clsReportParameters.TimeZoneInfo);
					dRow["TimeOutgoing"] = TimeZoneInfo.ConvertTimeFromUtc(el.TimeOutgoing, clsReportParameters.TimeZoneInfo);
					dRow["TimeElapsed"] = el.TimeElapsed;
					tData.Rows.Add(dRow);
				}
				var finalRow = tData.NewRow();
				finalRow["PointId"] = @string["Total"];
				finalRow["TotalRun"] = string.Format("{0} {1}", finalList.Sum(x => x.Odometer), @string["km"]);
				finalRow["TimeIncoming"] = string.Empty;
				finalRow["TimeOutgoing"] = string.Empty;
				finalRow["TimeElapsed"] =
					TimeSpan.FromSeconds(finalList.Sum(x => x.TimeElapsed.TotalSeconds)).ToString();
				tData.Rows.Add(finalRow);
			}

			return ds;
		}
		private string GetZoneGroupName(RoutingSheetParameters reportParameters)
		{
			var geozoneGroup = mclsInstance.GetZoneGroupById(reportParameters.GeoZoneGroupId);
			return geozoneGroup != null ? geozoneGroup.name : string.Empty;
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var routingSheetParameters = new RoutingSheetParameters { OperatorID = settings.OperatorId };
			return routingSheetParameters;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as RoutingSheetParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleGroupId <= 0)
			{
				if (parameters.VehicleProp == null)
					return false;
				var vehicleId = parameters.VehicleProp.Tag as int?;
				if (vehicleId == null || vehicleId.Value <= 0)
					return false;
			}

			if (parameters.GeoZoneGroupId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["RoutingSheet"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["RoutingSheetDescription"];
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var clsReportParameters = (RoutingSheetParameters)reportParameters;

			return GetReportHtml(clsReportParameters);
		}
		private string GetVehicleGroupName(RoutingSheetParameters reportParameters)
		{
			var vehicleGroup = mclsInstance.GetVehicleGroupById(reportParameters.VehicleGroupId);
			return vehicleGroup != null ? vehicleGroup.name : string.Empty;
		}
		private string GetReportHtml(RoutingSheetParameters clsReportParameters)
		{
			var stringBuilder = new StringBuilder();

			var @string = GetResourceStringContainer(clsReportParameters.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xmlWriter = new XmlTextWriter(stringWriter))
			{
				var isGroupReport = clsReportParameters.VehicleGroupId != 0;
				var dateTimeFrom = clsReportParameters.ToUtc(clsReportParameters.DateFrom);
				var dateTimeTo = clsReportParameters.ToUtc(clsReportParameters.DateTo.AddDays(1).AddSeconds(-1));

				List<GeoZone> nonVisitedZones;
				var finalList = isGroupReport
					? GetFinalListByVehicleGroup(
						clsReportParameters.VehicleGroupId
						,dateTimeFrom
						,dateTimeTo
						,clsReportParameters.OperatorId
						,clsReportParameters.GeoZoneGroupId
						,clsReportParameters.Culture
						,clsReportParameters.TimeZoneInfo
						,out nonVisitedZones)
					: GetFinalListByVehicle(
						(int)clsReportParameters.VehicleProp.Tag
						,dateTimeFrom
						,dateTimeTo
						,clsReportParameters.OperatorId
						,clsReportParameters.GeoZoneGroupId
						,clsReportParameters.TimeZoneInfo
						,out nonVisitedZones
						,clsReportParameters.Culture);

				bool zoneDescriptionExists = finalList.Any(x => !string.IsNullOrWhiteSpace(x.Address));

				xmlWriter.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xmlWriter.WriteAttributeString("class", "reportHeader");

				xmlWriter.WriteStartElement("tr");
				xmlWriter.WriteElementString("td", @string["CaptionReport"]);
				xmlWriter.WriteElementString("td", @string["RoutingSheet"]);
				xmlWriter.WriteEndElement();

				var vehicleName = GetVehicleName(clsReportParameters);
				if (!string.IsNullOrWhiteSpace(vehicleName))
				{
					xmlWriter.WriteStartElement("tr");
					xmlWriter.WriteElementString("td", @string["CaptionObjectName"]);
					xmlWriter.WriteElementString("td", vehicleName);
					xmlWriter.WriteEndElement();
				}

				var vehicleGroupName = GetVehicleGroupName(clsReportParameters);
				if (!string.IsNullOrWhiteSpace(vehicleGroupName))
				{
					xmlWriter.WriteStartElement("tr");
					xmlWriter.WriteElementString("td", @string["CaptionObjectGroupName"]);
					xmlWriter.WriteElementString("td", vehicleGroupName);
					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteStartElement("tr");
				xmlWriter.WriteElementString("td", @string["CaptionZoneGroupName"]);
				xmlWriter.WriteElementString("td", GetZoneGroupName(clsReportParameters));
				xmlWriter.WriteEndElement();

				xmlWriter.WriteStartElement("tr");
				xmlWriter.WriteElementString("td", @string["CaptionDateFrom"]);
				xmlWriter.WriteElementString("td", clsReportParameters.DateFrom.ToShortDateString());
				xmlWriter.WriteEndElement();

				xmlWriter.WriteStartElement("tr");
				xmlWriter.WriteElementString("td", @string["CaptionDateTo"]);
				xmlWriter.WriteElementString("td", clsReportParameters.DateTo.ToShortDateString());
				xmlWriter.WriteEndElement();

				xmlWriter.WriteEndElement(); //table

				xmlWriter.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xmlWriter.WriteAttributeString("class", "reportBody reportTable");
				xmlWriter.WriteStartElement("thead");
				xmlWriter.WriteStartElement("tr");

				if (isGroupReport)
					xmlWriter.WriteElementString("th", @string["monitoringObject"]);

				xmlWriter.WriteElementString("th", @string["Run"]);
				xmlWriter.WriteElementString("th", @string["PointName"]);
				if (zoneDescriptionExists)
					xmlWriter.WriteElementString("th", @string["PointAddress"]);
				xmlWriter.WriteElementString("th", @string["TimeIncoming"]);
				xmlWriter.WriteElementString("th", @string["TimeOutgoing"]);
				xmlWriter.WriteElementString("th", @string["TimeElapsed"]);
				xmlWriter.WriteElementString("td", string.Empty);

				xmlWriter.WriteEndElement();
				xmlWriter.WriteEndElement();
				foreach (var byVehicle in finalList.GroupBy(x => x.VehicleId).OrderBy(x => x.First().VehicleName))
				{
					var vehicleElements = byVehicle.OrderBy(x => x.Id).ToList();

					for (var i = 0; i != vehicleElements.Count; ++i)
					{
						var element = vehicleElements[i];

						var localTimeIncoming = element.TimeIncoming != DateTime.MinValue
							? TimeZoneInfo.ConvertTimeFromUtc(element.TimeIncoming, clsReportParameters.TimeZoneInfo)
							: (DateTime?)null;
						var localTimeOutgoing = element.TimeOutgoing != DateTime.MinValue
							? TimeZoneInfo.ConvertTimeFromUtc(element.TimeOutgoing, clsReportParameters.TimeZoneInfo)
							: (DateTime?)null;

						xmlWriter.WriteStartElement("tr");

						if (isGroupReport)
						{
							xmlWriter.WriteElementString("td", i == 0 ? element.VehicleName : string.Empty);
						}
						xmlWriter.WriteStartElement("td");
						xmlWriter.WriteAttributeString("class", "number");
						xmlWriter.WriteValue(element.Odometer.ToString(CultureInfo.InvariantCulture));
						xmlWriter.WriteEndElement();

						xmlWriter.WriteElementString("td", element.ZoneName);
						if (zoneDescriptionExists)
							xmlWriter.WriteElementString("td", element.Address);
						xmlWriter.WriteElementString("td", localTimeIncoming != null ? localTimeIncoming.Value.ToString(TimeHelper.DefaultTimeFormatUpToMinutes, CultureInfo.InvariantCulture) : string.Empty);
						xmlWriter.WriteElementString("td", localTimeOutgoing != null ? localTimeOutgoing.Value.ToString(TimeHelper.DefaultTimeFormatUpToMinutes, CultureInfo.InvariantCulture) : string.Empty);
						xmlWriter.WriteElementString("td", element.TimeElapsed.ToString());
						xmlWriter.WriteElementString("td", string.Empty);

						if (localTimeIncoming != null && localTimeOutgoing != null)
						{
							var path =
								string.Format(
									"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}"
									, clsReportParameters.ApplicationPath
									, element.VehicleId
									, localTimeIncoming.Value.ToString("dd.MM.yyyy+HH:mm:ss")
									, localTimeOutgoing.Value.ToString("dd.MM.yyyy+HH:mm:ss"));

							xmlWriter.WriteStartElement("td");
							xmlWriter.WriteStartElement("a");
							xmlWriter.WriteAttributeString("href", path.Replace(' ', '+'));
							xmlWriter.WriteString(@string["ShowOnMap"]);
							xmlWriter.WriteEndElement();
							xmlWriter.WriteEndElement();

						}
						else
						{
							xmlWriter.WriteElementString("td", string.Empty);
						}

						xmlWriter.WriteEndElement();
					}
					xmlWriter.WriteStartElement("tr");
					xmlWriter.WriteAttributeString("class", "rawbold");

					xmlWriter.WriteElementString("td", string.Format(@string["TotalBy"], vehicleElements.First().VehicleName));
					xmlWriter.WriteElementString("td", string.Format("{0} {1}", vehicleElements.Sum(x => x.Odometer), @string["km"]));
					xmlWriter.WriteElementString("td", string.Empty);
					if (zoneDescriptionExists)
						xmlWriter.WriteElementString("td", string.Empty);
					xmlWriter.WriteElementString("td", string.Empty);
					xmlWriter.WriteElementString("td", string.Empty);
					xmlWriter.WriteElementString("td", TimeSpan.FromSeconds(vehicleElements.Sum(x => x.TimeElapsed.TotalSeconds)).ToString());

					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement(); //table

				//Список непосещенных геозон (бета-версия)
				if (nonVisitedZones != null && nonVisitedZones.Count != 0)
				{
					xmlWriter.WriteStartElement("p");
					xmlWriter.WriteElementString("h2", @string["NonVisitedZones"]);
					xmlWriter.WriteStartElement("ol");
					foreach (var zone in nonVisitedZones.OrderBy(z => z.Name, System.StringComparer.OrdinalIgnoreCase))
					{
						xmlWriter.WriteElementString("li", zone.Name);
					}
					xmlWriter.WriteEndElement();
					xmlWriter.WriteEndElement();
				}
			}
			return stringBuilder.ToString();
		}
		private class Stop
		{
			public int  LogTimeFrom;
			public int  LogTimeTo;
			public long Odometer;
			public int  ZoneId;
		}
		private List<ReportDataElement> GetFinalListByVehicleGroup(int vehicleGroupId, DateTime dtmReportDateTimeFrom, DateTime dtmReportDateTimeTo, int operatorId, int geoZoneGroupId, CultureInfo cultureInfo, TimeZoneInfo timeZoneInfo, out List<GeoZone> nonVisitedZones)
		{
			var logTimeFrom = TimeHelper.GetSecondsFromBase(dtmReportDateTimeFrom);
			var logTimeTo = TimeHelper.GetSecondsFromBase(dtmReportDateTimeTo);

			var runsLog = mclsInstance.GetRunLogForGroup(vehicleGroupId, logTimeFrom, logTimeTo);
			var vehicles = mclsInstance.GetVehiclesByVehicleGroup(vehicleGroupId, operatorId, cultureInfo, SystemRight.PathAccess).ToDictionary(v => v.id, v => v.Name);

			return GetFinalList(operatorId, geoZoneGroupId, timeZoneInfo, runsLog, vehicles, out nonVisitedZones);
		}
		private List<ReportDataElement> GetFinalList(int operatorId, int geoZoneGroupId, TimeZoneInfo timeZoneInfo, List<RunLogRecord> runsLog, Dictionary<int, string> vehicles, out List<GeoZone> nonVisitedZones)
		{
			var vehicleStops = new Dictionary<int, List<Stop>>();

			foreach (var g in runsLog.ToLookup(l => l.VehicleId))
			{
				var runs = g
					.OrderBy(l => l.LogTime)
					.GetSequences((x, y) => x.Odometer == y.Odometer)
					.Where(s => 1 < s.Count)
					.Select(s => new Stop
					{
						LogTimeFrom = s.First().LogTime,
						LogTimeTo   = s.Last().LogTime,
						Odometer    = s.First().Odometer
					})
					.ToList();

				vehicleStops.Add(g.Key, runs);
			}

			var geozones = mclsInstance
				.GetGeoZonesByGroup(operatorId, geoZoneGroupId)
				.ToDictionary(z => z.Id);

			var vehicleZoneParams = vehicles.Keys.SelectMany(vid => geozones.Keys.Select(zid => new VehicleZoneParam(vid, zid)));

			//Начало каждой остановки
			var vehicleLogTimeParams = vehicleStops
				.SelectMany(pair => pair.Value
					.Select(stop => new VehicleLogTimeParam(pair.Key, stop.LogTimeFrom)))
				.ToArray();

			var geoZonesForPositions = mclsInstance
				.GetGeoZonesForPositions(vehicleZoneParams, vehicleLogTimeParams)
				.ToLookup(x => new VehicleLogTimeParam(x.VehicleId, x.LogTime), x => x.ZoneId);

			var result = new List<ReportDataElement>();
			foreach (var vid in vehicles.Keys)
			{
				List<Stop> stops;
				if (!vehicleStops.TryGetValue(vid, out stops))
					continue;

				foreach (var stop in stops)
				{
					stop.ZoneId = geoZonesForPositions[new VehicleLogTimeParam(vid, stop.LogTimeFrom)].FirstOrDefault();
				}

				stops = stops
					.GetSequences((x, y) => x.ZoneId == y.ZoneId)
					.Where(x => x.First().ZoneId != default(int))
					.Select(s => new Stop
					{
						LogTimeFrom = s.First().LogTimeFrom,
						LogTimeTo   = s.Last().LogTimeTo,
						Odometer    = s.First().Odometer,
						ZoneId      = s.First().ZoneId
					})
					.ToList();

				result.AddRange(GetReportElementDataIntervals(stops, vid, timeZoneInfo, geozones));
			}

			foreach (var item in result)
			{
				item.VehicleName = vehicles[item.VehicleId];
			}

			var visitedZones = new HashSet<int>(
				vehicleStops.Values
					.SelectMany(s => s)
					.Select(s => s.ZoneId)
					.Where(i => i != default(int)));

			nonVisitedZones = geozones.Values.Where(z => !visitedZones.Contains(z.Id)).ToList();

			return result;
		}
		private IEnumerable<ReportDataElement> GetReportElementDataIntervals(IList<Stop> stops, int vehicleId, TimeZoneInfo timeZoneInfo, Dictionary<int, GeoZone> geozones)
		{
			var result = new List<ReportDataElement>();

			var counter = 0;

			// Есть хоть одно пересечение с геозоной.
			if (!stops.Any())
				return result;

			var currentOdometer = stops.First().Odometer;

			foreach (var stop in stops)
			{
				++counter;
				var run = (stop.Odometer - currentOdometer)/100000;
				currentOdometer = stop.Odometer;

				var reportDataElement = new ReportDataElement();
				reportDataElement.Id = counter;
				reportDataElement.VehicleId = vehicleId;
				reportDataElement.Description = string.Format("{0}", counter);
				reportDataElement.Odometer = Math.Round((double) run, 0);
				GeoZone stopZone;
				if (geozones.TryGetValue(stop.ZoneId, out stopZone))
				{
					reportDataElement.ZoneName = stopZone.Name;
					reportDataElement.Address = stopZone.Description;
				}
				reportDataElement.TimeIncoming = TimeHelper.GetDateTimeUTC(stop.LogTimeFrom);
				reportDataElement.TimeOutgoing = TimeHelper.GetDateTimeUTC(stop.LogTimeTo);
				reportDataElement.TimeElapsed = TimeSpan.FromSeconds(stop.LogTimeTo - stop.LogTimeFrom);
				result.Add(reportDataElement);
			}

			if (result.Count > 0)
			{
				//Для каждой точки, первой на начало дня, сбросить пробег до неё от предыдущей зоны
				var cDate = TimeZoneInfo.ConvertTimeFromUtc(result.First().TimeIncoming, timeZoneInfo).Date;
				var dDate = TimeZoneInfo.ConvertTimeFromUtc(result.Last().TimeOutgoing, timeZoneInfo).Date;

				while (cDate <= dDate)
				{
					var resetRunToFirstPoint = result.FirstOrDefault(
						x => TimeZoneInfo.ConvertTimeFromUtc(x.TimeIncoming, timeZoneInfo).Date == cDate);
					if (resetRunToFirstPoint != null)
						resetRunToFirstPoint.Odometer = 0;
					cDate = cDate.AddDays(1);
				}
			}

			return result;
		}
		private List<ReportDataElement> GetFinalListByVehicle(
			int vehicleId,
			DateTime dtmReportDateTimeFrom,
			DateTime dtmReportDateTimeTo,
			int operatorId,
			int geoZoneGroupId,
			TimeZoneInfo timeZoneInfo, out List<GeoZone> nonVisitedZones, CultureInfo cultureInfo)
		{
			var logTimeFrom = TimeHelper.GetSecondsFromBase(dtmReportDateTimeFrom);
			var logTimeTo = TimeHelper.GetSecondsFromBase(dtmReportDateTimeTo);

			var runsLog = mclsInstance.GetRunLogForVehicle(vehicleId, logTimeFrom, logTimeTo);
			var vehicles = new Dictionary<int, string> {{vehicleId, mclsInstance.GetVehicleById(operatorId, vehicleId, cultureInfo).Name}};
			return GetFinalList(operatorId, geoZoneGroupId, timeZoneInfo, runsLog, vehicles, out nonVisitedZones);
		}
		private string GetVehicleName(RoutingSheetParameters parameters)
		{
			if (parameters.VehicleProp == null)
				return null;
			var id = parameters.VehicleProp.Tag as int?;
			if (id == null)
				return null;
			return mclsInstance.GetVehicleById(parameters.OperatorID, id.Value, parameters.Culture).Name;
		}
	}
}