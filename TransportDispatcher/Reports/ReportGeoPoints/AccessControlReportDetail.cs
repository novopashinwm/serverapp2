﻿using System;

namespace FORIS.TSS.TransportDispatcher.Reports
{
    [Serializable]
    public class AccessControlReportDetail
    {
        /// <summary>Время входа в зону</summary>
        public int LogTimeEnter;

        /// <summary>Время выхода из зону</summary>
        public int LogTimeExit;

        /// <summary>Время начала дня</summary>
        public int DateTimeInt;

        /// <summary>Тип посещения зоны</summary>
        public AccessControlReportDetailType Type;
    }
}