﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт контроля доступа в определенную геозону группы транспортных средств. </summary>
	[Guid("F4ED91AC-115F-4E46-9931-1206DBAB05D6")]
	public class AccessControlReport : TssReportBase
	{
		public override int Order
		{
			get { return 4; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeoPointReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var dataset = GetReportDataSet((AccessControlReportParameters)iReportParameters);
			var customReport = new AccessControlExcelPdfReport();
			customReport.SetDataSource(dataset);
			return customReport;
		}
		public DataSet GetReportDataSet(AccessControlReportParameters pars)
		{
			var finalResult = GetFinalList(pars);
			var strings = GetResourceStringContainer(pars.Culture);

			var result = new DataSet("AccessControlReport");

			var hData = new DataTable("Legend");
			hData.Columns.Add(new DataColumn("reportLegendKey"));
			hData.Columns.Add(new DataColumn("reportLegendValue"));
			result.Tables.Add(hData);

			var hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["CaptionReport"];
			hRow["reportLegendValue"] = strings["AccessControlReport"];

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["CaptionDateFrom"];
			hRow["reportLegendValue"] = pars.DateFrom.ToShortDateString();

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["CaptionDateTo"];
			hRow["reportLegendValue"] = pars.DateTo.ToShortDateString();

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["CaptionZoneName"];
			hRow["reportLegendValue"] = mclsInstance.GetGeoZoneById(pars.StartZoneId).Name;

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["TimeIn"];
			hRow["reportLegendValue"] = pars.DateFrom.ToString("HH:mm");

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"] = strings["TimeOut"];
			hRow["reportLegendValue"] = pars.DateTo.ToString("HH:mm");

			hData.Rows.Add(hRow);

			var bodyHeaderTable = new DataTable("bodyHeader");
			bodyHeaderTable.Columns.AddRange(new[]
			{
				new DataColumn("VehicleName"),
				new DataColumn("JustDate"),
				new DataColumn("TimeIncoming"),
				new DataColumn("TimeOutgoing")
			});
			result.Tables.Add(bodyHeaderTable);

			var headerRow = bodyHeaderTable.NewRow();
			headerRow["VehicleName"] = strings["VehicleName"];
			headerRow["JustDate"] = strings["JustDate"];
			headerRow["TimeIncoming"] = strings["TimeIncoming"];
			headerRow["TimeOutgoing"] = strings["TimeOutgoing"];
			bodyHeaderTable.Rows.Add(headerRow);

			var bodyDataTable = new DataTable("bodyData");
			bodyDataTable.Columns.AddRange(new[]
			{
				new DataColumn("VehicleName"),
				new DataColumn("JustDate"),
				new DataColumn("TimeIncoming"),
				new DataColumn("TimeOutgoing")
			});
			result.Tables.Add(bodyDataTable);

			foreach (var item in finalResult)
			{
				var dayStarts =
					finalResult.SelectMany(x => x.Details).Select(d => d.DateTimeInt).Distinct().OrderBy(x => x);

				foreach (var dayStartLogTime in dayStarts)
				{
					foreach (var det in item.Details.Where(x => x.DateTimeInt == dayStartLogTime))
					{
						var dataRow = bodyDataTable.NewRow();

						dataRow["VehicleName"] = item.Name;
						dataRow["JustDate"] =
							TimeHelper.GetLocalTime(dayStartLogTime, pars.TimeZoneInfo).ToShortDateString();
						dataRow["TimeIncoming"] =
							TimeHelper.GetLocalTime(det.LogTimeEnter, pars.TimeZoneInfo).ToShortTimeString();
						dataRow["TimeOutgoing"] =
							TimeHelper.GetLocalTime(det.LogTimeExit, pars.TimeZoneInfo).ToShortTimeString();
						bodyDataTable.Rows.Add(dataRow);
					}
				}
			}
			return result;
		}
		private List<AccessControlReportResult> GetFinalList(AccessControlReportParameters pars)
		{
			var dailyIntervals = TimeHelperEx.GetLocalDailyIntervals(
				pars.DateTimeInterval.DateFrom.Date, pars.DateTimeInterval.DateTo.Date,
				pars.WorkingHoursInterval.WorkFrom.Hours, pars.WorkingHoursInterval.WorkFrom.Minutes,
				pars.WorkingHoursInterval.WorkTo.Hours, pars.WorkingHoursInterval.WorkTo.Minutes)
				.Select(i => new
				{
					//Начало дня
					DayStart = TimeHelper.GetSecondsFromBase(pars.ToUtc(i.From.Date)),
					//Начало ежедневного интервала
					WorkFrom = TimeHelper.GetSecondsFromBase(pars.ToUtc(i.From)),
					//Конец ежедневного интервала
					WorkTo = TimeHelper.GetSecondsFromBase(pars.ToUtc(i.To)),
				})
				.ToArray();

			//Как должны рассчитывать:
			//1. Проверить последнюю точку на вхождение в зону до начала рабочего времени независимо от статуса движение / стоянка
			//2. Проверить первую точку на вхождение в зону после конца рабочего времени независимо от статуса движение / стоянка
			//3. Проверить наличие остановок

			var resultSet = new List<AccessControlReportResult>();

			var vehicles = mclsInstance.GetVehiclesByVehicleGroup(pars.VehicleGroupId, pars.OperatorId, pars.Culture, SystemRight.PathAccess);

			//TODO: брать из Geo_Log
			var runsLog = mclsInstance.GetRunLog(
				vehicles.Select(v => v.id),
				dailyIntervals.Select(i => new LogTimeSpanParam(i.WorkFrom, i.WorkTo)),
				true);

			var vidToRunsLog = runsLog.ToLookup(x => x.VehicleId, x => x.LogTime);

			var geoZonesForPositions = mclsInstance
				.GetGeoZonesForPositions(
					vehicles.Select(v => new VehicleZoneParam(v.id, pars.StartZoneId)),
					runsLog.Select(rl => new VehicleLogTimeParam(rl.VehicleId, rl.LogTime)).Distinct())
				.ToLookup(x => x.VehicleId, x => x.LogTime);

			foreach (var v in vehicles)
			{
				var vehicleSet = new AccessControlReportResult {VehicleId = v.id, Name = v.Name};
				resultSet.Add(vehicleSet);

				var inZoneLogTimes = new HashSet<int>(geoZonesForPositions[v.id]);

				var inZoneSequences = vidToRunsLog[v.id].OrderBy(x => x).GetSequences(inZoneLogTimes.Contains);

				foreach (var inZoneSequence in inZoneSequences)
				{
					var logTimeEnter = inZoneSequence.First();
					var logTimeExit = inZoneSequence.Last();

					var dailyInterval = dailyIntervals.FirstOrDefault(
						di => di.WorkFrom <= logTimeEnter && logTimeExit <= di.WorkTo);

					if (dailyInterval == null)
						continue;

					var localDayStart = TimeHelper.GetLocalTime(logTimeEnter, pars.TimeZoneInfo);

					vehicleSet.Details.Add(new AccessControlReportDetail
					{
						LogTimeEnter = logTimeEnter,
						LogTimeExit = logTimeExit,
						Type = AccessControlReportDetailType.Normal,
						DateTimeInt =
							TimeHelper.GetSecondsFromBase(TimeZoneInfo.ConvertTimeToUtc(localDayStart.Date,
																						pars.TimeZoneInfo))
					});
				}
			}
			return resultSet;
		}
		public override object GetReportAsObj (ReportParameters reportParameters)
		{
			var clsReportParameters = (AccessControlReportParameters) reportParameters;
			return GetReportHtml(clsReportParameters);
		}
		private string GetReportHtml (AccessControlReportParameters pars)
		{
			var sb = new StringBuilder();
			var @string = GetResourceStringContainer(pars.Culture);

			using (var stringWriter = new StringWriter(sb))
			using (var xmlWriter = new XmlTextWriter(stringWriter))
			{
				// Подитоги по неделям.
				xmlWriter.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xmlWriter.WriteAttributeString("class", "reportBody short");

				xmlWriter.WriteStartElement("thead");
				xmlWriter.WriteStartElement("tr");

				xmlWriter.WriteElementString("th", @string["monitoringObject"]);
				xmlWriter.WriteElementString("th", @string["TimeIncoming"]);
				xmlWriter.WriteElementString("th", @string["TimeOutgoing"]);
				xmlWriter.WriteElementString("th", @string["Path"]);

				xmlWriter.WriteEndElement();
				xmlWriter.WriteEndElement();

				var finalResult = GetFinalList(pars);
				if (finalResult == null)
					throw new ArgumentException(
						string.Format(
							"List result is empty. Parameters are. from:{0}, to:{1}, zfid:{2}, vgid:{3}",
							pars.DateFrom, pars.DateTo, pars.StartZoneId, pars.VehicleGroupId));

				foreach (var item in finalResult)
				{
					xmlWriter.WriteStartElement("tr");
					xmlWriter.WriteAttributeString("class", "rawbold");
					xmlWriter.WriteElementString("td", item.Name);
					xmlWriter.WriteEndElement();

					var dayStarts =
						finalResult.SelectMany(x => x.Details).Select(d => d.DateTimeInt).Distinct().OrderBy(x => x);

					foreach (var dayStart in dayStarts)
					{
						xmlWriter.WriteStartElement("tr");
						xmlWriter.WriteAttributeString("class", "rawbold");
						xmlWriter.WriteElementString("td", string.Empty);
						xmlWriter.WriteStartElement("td");
						xmlWriter.WriteAttributeString("colspan", "2");
						xmlWriter.WriteAttributeString("style", "text-align: center; font-weight: bold;");
						xmlWriter.WriteString(
							TimeHelper.GetLocalTime(dayStart, pars.TimeZoneInfo).ToShortDateString());
						xmlWriter.WriteEndElement();
						xmlWriter.WriteEndElement();

						foreach (var det in item.Details.Where(x => x.DateTimeInt == dayStart))
						{
							xmlWriter.WriteStartElement("tr");
							xmlWriter.WriteElementString("td", string.Empty);
							xmlWriter.WriteElementString("td",
														 TimeHelper.GetLocalTime(det.LogTimeEnter, pars.TimeZoneInfo)
															 .ToShortTimeString());
							xmlWriter.WriteElementString("td",
														 TimeHelper.GetLocalTime(det.LogTimeExit, pars.TimeZoneInfo)
															 .ToShortTimeString());

							var path =
								string.Format(
									"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
									, pars.ApplicationPath
									, item.VehicleId
									,
									TimeHelper.GetLocalTime(det.LogTimeEnter, pars.TimeZoneInfo).ToString(
										"dd.MM.yyyy+HH:mm:ss")
									,
									TimeHelper.GetLocalTime(det.LogTimeExit, pars.TimeZoneInfo).ToString(
										"dd.MM.yyyy+HH:mm:ss"));

							xmlWriter.WriteStartElement("td");
							xmlWriter.WriteStartElement("a");
							xmlWriter.WriteAttributeString("href", path.Replace(' ', '+'));
							xmlWriter.WriteString(@string["ShowOnMap"]);
							xmlWriter.WriteEndElement();
							xmlWriter.WriteEndElement();

							xmlWriter.WriteEndElement();
						}
					}
				}
			}

			return sb.ToString();
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new AccessControlReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as AccessControlReportParameters;

			if (parameters == null)
				return false;

			if (parameters.VehicleGroupId <= 0)
				return false;

			if (parameters.StartZoneId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["AccessControlReport"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["AccessControlReportDescription"];
		}
	}
}