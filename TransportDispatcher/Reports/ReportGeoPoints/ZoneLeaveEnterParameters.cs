﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary>
	/// Параметры для данного отчета.
	/// </summary>
	[Serializable]
	public class ZoneLeaveEnterParameters : ReportParameters, IMonitoreeObjectContainer
	{
		protected TagListBoxItem VehicleProp = new TagListBoxItem("", null);

		public IdType MonitoreeObjectIdType
		{
			get { return IdType.Vehicle; }
		}

		public int MonitoreeObjectId
		{
			get
			{
				return (int)VehicleProp.Tag;
			}
		}

		[DisplayName("StartZoneId"), Type(typeof(int)), ControlType(ReportParametersUtils.ZonePicker, "OutgoingGeozoneName")]
		public int StartZoneId { get; set; }

		[DisplayName("EndZoneId"), Type(typeof(int)), ControlType(ReportParametersUtils.ZonePicker, "EnteringGeozoneName")]
		public int EndZoneId { get; set; }

		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupName")]
		public int VehicleGroupId { get; set; }
		/// <summary> Интервал построения отчета </summary>
		[ControlType(ReportParametersUtils.DateTimeFromToPicker)]
		[Order(255)]
		[Options("Accuracy:3")] // Точность до минут
		public override DateTimeInterval DateTimeInterval
		{
			get
			{
				return base.DateTimeInterval;
			}
			set
			{
				base.DateTimeInterval = value;
			}
		}
	}
}