﻿using System;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	internal class ActionInterval
	{
		public int           VehicleId;
		public string        PositionFrom;
		public string        ZoneFrom;
		public int           LogTime;
		public int           LogTimeExit;
		public long          Odometer;
		public GeoLogRecord? Coords;
	}
	internal class ReportDataElement
	{
		public int      Id;
		public int      VehicleId;
		public string   VehicleName;
		public string   Description;
		public double   Odometer;
		public string   ZoneName;
		public string   Address;
		public DateTime TimeIncoming;
		public DateTime TimeOutgoing;
		public TimeSpan TimeElapsed;
	}
	internal enum ActionIntervalType
	{
		Motion  = 0,
		Parking = 1,
		Skipped = 2,
	}
	internal class ReportDataSummaryElement
	{
		public DateTime ReportDate;
		public double   Run;
		public DateTime StartDateTime;
		public DateTime EndDateTime;
		public TimeSpan Elapsed;
		public int      PointsVisited;
		public int      Id;
		public int      WeekNumber;
	}
}