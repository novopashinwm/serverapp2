﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary>
	/// Параметры отчета контроля доступа.
	/// </summary>
	[Serializable]
	public class AccessControlReportParameters : ReportParameters
	{
		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupPicker")]
		public int VehicleGroupId { get; set; }

		[DisplayName("StartZoneId"), Type(typeof(int)), ControlType(ReportParametersUtils.ZonePicker, "PointName")]
		public int StartZoneId { get; set; }

		private WorkingHoursInterval _workingHoursInterval;

		[DisplayName("WorkingHoursInterval"), Type(typeof(WorkingHoursInterval))]
		[ControlType(ReportParametersUtils.WorkingHoursInterval, "WorkingHoursInterval"), Order(256)]
		public WorkingHoursInterval WorkingHoursInterval
		{
			get { return _workingHoursInterval; }
			set { _workingHoursInterval = value; }
		}
		/// <summary> Интервал построения отчета </summary>
		[ControlType(ReportParametersUtils.DateTimeFromToPicker)]
		[Order(255)]
		[Options("Accuracy:3")] // Точность до минут
		public override DateTimeInterval DateTimeInterval
		{
			get
			{
				return base.DateTimeInterval;
			}
			set
			{
				base.DateTimeInterval = value;
			}
		}
	}
}