<xsl:stylesheet version="1.0"
 xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">
      <xsl:apply-templates/>
    </Workbook>

  </xsl:template>

  <xsl:template match="/*">
    <Styles>
      <Style ss:ID="Default" ss:Name="Normal">
        <Alignment ss:Vertical="Bottom"/>
        <Borders/>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000"/>
        <Interior/>
        <NumberFormat/>
        <Protection/>
      </Style>
      <Style ss:ID="s63">
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
      </Style>
      <Style ss:ID="s67">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
      </Style>
      <Style ss:ID="s68">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat/>
      </Style>
      <Style ss:ID="s74">
        <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
        <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
      </Style>
      <Style ss:ID="s75">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat ss:Format="0"/>
      </Style>
    </Styles>
    <Worksheet>
      <xsl:attribute name="ss:Name">
        <xsl:value-of select="/RoutingSheetSummary/Legend/reportLegendValue[1]"/>
      </xsl:attribute>
      <Table ss:ExpandedColumnCount="12" x:FullColumns="1" x:FullRows="1" ss:DefaultRowHeight="15">
        <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="75.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="120"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="68.25"/>
        <Column ss:AutoFitWidth="0" ss:Width="66.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="76.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="70.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="81"/>
        <Column ss:AutoFitWidth="0" ss:Width="87"/>
        <xsl:apply-templates select="//Legend" />
        <Row />
        <xsl:apply-templates select="//summaryHeader" />
        <xsl:apply-templates select="//summaryBody" />
      </Table>
    </Worksheet>
    <xsl:if test="/RoutingSheetSummary/dailyHeader">
      <Worksheet>
        <xsl:attribute name="ss:Name">
          <xsl:text>Details</xsl:text>
        </xsl:attribute>
        <Table>
          <xsl:apply-templates select="//dailyHeader" />
          <xsl:apply-templates select="//dailyBody" />
        </Table>
      </Worksheet>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Legend">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="reportLegendKey">
    <Cell ss:MergeAcross="1" ss:StyleID="s63">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="reportLegendValue">
    <Cell ss:MergeAcross="4">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="summaryHeader">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="summaryHeader/DateFrom">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryHeader/DateTo">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryHeader/DataPointsVisited">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryHeader/DataTotalTimeSpend">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryHeader/DataTotalRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="summaryBody">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="summaryBody/VehicleName">
    <Cell ss:Index="2" ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryBody/DateFrom">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryBody/DateTo">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryBody/DataPointsVisited">
    <Cell ss:StyleID="s67">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryBody/DataTotalTimeSpend">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="summaryBody/DataTotalRun">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="dailyHeader">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="dailyHeader/ReportDate">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyHeader/TotalRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyHeader/TimeIncoming">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyHeader/TimeOutgoing">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyHeader/PointsCount">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyHeader/TimeElapsed">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>


  <xsl:template match="dailyBody">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="dailyBody/ReportDate">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyBody/TotalRun">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyBody/TimeIncoming">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyBody/TimeOutgoing">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyBody/PointsCount">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="dailyBody/TimeElapsed">
    <Cell ss:StyleID="s68">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

</xsl:stylesheet>