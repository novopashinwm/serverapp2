﻿namespace FORIS.TSS.TransportDispatcher.Reports
{
    public enum AccessControlReportDetailType
    {
        /// <summary>
        /// Объект наблюдения находился в указанной зоне на начало дня.
        /// </summary>
        AlreadyIn,

        /// <summary>
        /// В течении для въехал и выехал.
        /// </summary>
        Normal,

        /// <summary>
        /// На конец дня остался в геозоне.
        /// </summary>
        Stay,

        /// <summary>
        /// Посещение геозоны одной точкой (моментальный вход и выход).
        /// </summary>
        OnePoint
    }
}