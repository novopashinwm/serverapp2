﻿using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.TransportDispatcher.Reports
{
    /// <summary>
    /// Параметры для данного отчета.
    /// </summary>
    [Serializable]
    public class RoutingSheetParameters : ReportParameters, IMonitoreeObjectContainer
    {
        [Browsable(false)]
        public int OperatorID { get; set; }

        /// <summary>
        /// Показывать/не показывать адрес в отчёте
        /// </summary>
        protected bool showAddress;

        /// <summary>
        /// GUID карты
        /// </summary>
        [Obsolete("Do not use, dedicated for compatibility for old versions deserialization")]
        protected string mapGuid = Guid.Empty.ToString();

        /// <summary>
        /// Для хранения значения флага, определяющего что следует подставлять текущую дату. Если он равен true, то вводить дату не требуется, будет всегда подставляться текущая.
        /// </summary>
        protected bool mblnAlwaysCurrentDate;
        /// <summary>
        /// Время начала периода, на который формируется отчет.
        /// </summary>
        protected string mstrReportTimeFrom = DateTime.Now.ToShortTimeString();
        /// <summary>
        /// Время окончания периода, на который формируется отчет.
        /// </summary>
        protected string mstrReportTimeTo = DateTime.Now.ToShortTimeString();
        /// <summary>
        /// Гаражный номер ТС.
        /// </summary>
        protected TagListBoxItem vehicleProp = new TagListBoxItem("", null);
        //protected FORIS.TSS.TransportDispatcher.TagComboBoxItem tcbiVehicleNumber = new TagComboBoxItem("", null);

        /// <summary>
        /// Идентификатор группы геозон, групп участников отчета.
        /// </summary>
        protected int geoZoneGroupId;

        /// <summary>
        /// Идентификатор группы объектов наблюдения.
        /// </summary>
        protected int vehicleGroupId;

        /// <summary>
        /// Название группы объектов наблюдения.
        /// </summary>
        protected string vehicleGroupName;

        /// <summary>
        /// Название группы геозон.
        /// </summary>
        protected string geoZoneGroupName;

        protected int count = 1000;
        protected int interval = 60;
        protected int parkingInterval = 60 * 5;

        // Набор данных для Editor-ов
        public DataTable dsParamsForEditor;


        /// <summary>
        /// Показывать/не показывать адрес в отчёте
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("ShowAddress", 9, true), Category("ReportParsCat"), Description("ShowAddressDesc")]
        public bool ShowAddress
        {
            get
            {
                return showAddress;
            }
            set
            {
                showAddress = value;
            }
        }

        [Browsable(false)]
        [BusinessLogic.Attributes.DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupPicker"), Order(1)]
        public int VehicleGroupId
        {
            get { return vehicleGroupId; }
            set { vehicleGroupId = value; }
        }

        [Obsolete("Этот параметр не заполняется системой")]
        [Browsable(false)]
        public string VehicleGroupName
        {
            get { return vehicleGroupName; }
            set { vehicleGroupName = value; }
        }

        [Browsable(false)]
        [BusinessLogic.Attributes.DisplayName("GeoZoneGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.ZoneGroupPicker, "ZoneGroupPicker"), Order(2)]
        public int GeoZoneGroupId
        {
            get { return geoZoneGroupId; }
            set { geoZoneGroupId = value; }
        }

        [Obsolete("Этот параметр не заполняется системой")]
        [Browsable(false)]
        public string GeoZoneGroupName
        {
            get { return geoZoneGroupName; }
            set { geoZoneGroupName = value; }
        }

        /// <summary>
        /// Идентификатор ТС.
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("TSNumberDN", 9, true), Category("ReportParsCat"), Description("TSNumberDesc")]
        [Browsable(true)]
        public TagListBoxItem VehicleProp
        {
            get
            {
                return vehicleProp;
            }
            set
            {
                vehicleProp = value;
            }
        }

        /// <summary>
        /// Флаг, определяющий, что следует подставлять текущую дату. Если он равен true, то вводить дату не требуется, будет всегда подставляться текущая.
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("InsertCurrDateDN"), Category("ReportParsCat"), Description("InsertCurrDateDesc")]
        [Browsable(false)]
        public bool AlwaysForCurrentDateProp
        {
            get
            {
                return mblnAlwaysCurrentDate;
            }
            set
            {
                mblnAlwaysCurrentDate = value;
            }
        }

        /// <summary>
        /// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
        [Browsable(true)]
        public override DateTime DateFrom
        {
            get
            {
                return dtDateFrom.Date;
            }
            set
            {
                dtDateFrom = value.Date;
            }
        }

        /// <summary>
        /// Время начала периода, на который формируется доклад.
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
        public string ReportTimeFromProp
        {
            get
            {
                return mstrReportTimeFrom;
            }
            set
            {
                //Вызов функции проверки введенного пользователем значения времени.
                mstrReportTimeFrom = TimeInputCheck(value);
            }
        }

        /// <summary>
        /// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
        [Browsable(true)]
        public override DateTime DateTo
        {
            get
            {
                return dtDateTo.Date;
            }
            set
            {
                dtDateTo = value.Date;
            }
        }

        /// <summary>
        /// Время окончания периода, на который формируется доклад.
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
        public string ReportTimeToProp
        {
            get
            {
                return mstrReportTimeTo;
            }
            set
            {
                //Вызов функции проверки введенного пользователем значения времени.
                mstrReportTimeTo = TimeInputCheck(value);
            }
        }

        /// <summary>
        /// Количество позиций
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
        public int PositionsСount
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
            }
        }
        /// <summary>
        /// Интервал
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("IntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
        public int Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
            }
        }

        /// <summary>
        /// Интервал
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("ParkingIntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
        public int ParkingInterval
        {
            get
            {
                return parkingInterval;
            }
            set
            {
                parkingInterval = value;
            }
        }

        public IdType MonitoreeObjectIdType
        {
            get { return IdType.VehicleGroup; }
        }

        public int MonitoreeObjectId
        {
            get
            {
                if (vehicleProp == null)
                    vehicleProp = new TagListBoxItem();
                vehicleProp.Tag = vehicleGroupId;
                return (int)vehicleProp.Tag;
            }
        }
    }
}