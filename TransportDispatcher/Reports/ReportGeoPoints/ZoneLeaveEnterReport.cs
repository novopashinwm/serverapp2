﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт "Вход выход из геозоны." </summary>
	[Guid("C1387D78-34EF-4F91-9709-2FC8515A3E11")]
	public class ZoneLeaveEnterReport : TssReportBase
	{
		public override int Order
		{
			get { return 3; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeoPointReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var dsMoveDetailHistory = GetReportDataSet((ZoneLeaveEnterParameters)iReportParameters);

			var customReport = new ZoneLeaveEnterExcelPdfReport();

			customReport.SetDataSource(dsMoveDetailHistory);

			return customReport;
		}

		private static void AddLegendRow(DataTable hData, string key, string value)
		{
			var hRow = hData.NewRow();
			hRow["reportLegendKey"] = key;
			hRow["reportLegendValue"] = value;
			hData.Rows.Add(hRow);
		}

		private DataSet GetReportDataSet(ZoneLeaveEnterParameters pars)
		{
			var finalList = GetFinalList(pars);
			if (finalList == null)
				throw new ArgumentException(string.Format("List result is empty. Parameters are. from:{0}, to:{1}, zfid:{2}, ztid:{3}, vgid:{4}", pars.DateFrom, pars.DateTo, pars.StartZoneId, pars.EndZoneId, pars.VehicleGroupId));

			var ds = new DataSet("ZoneLeaveEnterReport");

			var @string = GetResourceStringContainer(pars.Culture);

			// Заголовок.

			var hData = new DataTable("Legend");
			hData.Columns.Add(new DataColumn("reportLegendKey"));
			hData.Columns.Add(new DataColumn("reportLegendValue"));
			ds.Tables.Add(hData);

			AddLegendRow(hData, @string["CaptionReport"], GetReportName(pars.Culture));

			var description = GetReportDesc(pars.Culture);
			if (!string.IsNullOrWhiteSpace(description))
				AddLegendRow(hData, @string["Description"], description);

			AddLegendRow(hData, @string["VehicleGroupName"], mclsInstance.GetVehicleGroupById(pars.VehicleGroupId).name);
			AddLegendRow(hData, @string["CaptionDateFrom"], pars.DateFrom.ToShortDateString());
			AddLegendRow(hData, @string["CaptionDateTo"], pars.DateTo.ToShortDateString());
			AddLegendRow(hData, @string["OutgoingGeozoneName"], mclsInstance.GetGeoZoneById(pars.StartZoneId).Name);
			AddLegendRow(hData, @string["EnteringGeozoneName"], mclsInstance.GetGeoZoneById(pars.EndZoneId).Name);

			// Данные.
			var tData = new DataTable("summaryHeader");
			AddReportColumns(tData);
			ds.Tables.Add(tData);

			// Заголовок таблицы.
			var cRow = tData.NewRow();
			cRow["PointId"] = @string["CaptionObjectName"];
			cRow["TotalRun"] = @string["Run"];
			cRow["PointNameFrom"] = @string["PointNameFrom"];
			cRow["PointNameTo"] = @string["PointNameTo"];
			cRow["TimeOutgoing"] = @string["TimeOutgoing"];
			cRow["TimeIncoming"] = @string["TimeIncoming"];
			cRow["TimeSpent"] = @string["TimeSpent"];
			tData.Rows.Add(cRow);

			tData = new DataTable("summaryBody");
			AddReportColumns(tData);
			ds.Tables.Add(tData);

			foreach (var el in finalList)
			{
				var dRow = tData.NewRow();
				dRow["PointId"] = el.VehicleName;
				dRow["TotalRun"] = (el.Run / 100000).ToString("0.000", CultureInfo.InvariantCulture);
				dRow["PointNameFrom"] = el.ZoneLeftName;
				dRow["PointNameTo"] = el.ZoneEnterName;
				dRow["TimeOutgoing"] = TimeHelper.GetLocalTime(el.LogTimeLeft, pars.TimeZoneInfo).ToString("dd.MM.yyyy HH:mm");
				dRow["TimeIncoming"] = TimeHelper.GetLocalTime(el.LogTimeEnter, pars.TimeZoneInfo).ToString("dd.MM.yyyy HH:mm");
				dRow["TimeSpent"] = TimeHelper.GetTimeSpanReadableShort(TimeHelperEx.TimeSpanFromSeconds(Math.Abs(el.LogTimeLeft - el.LogTimeEnter)) ?? TimeSpan.Zero);
				tData.Rows.Add(dRow);
			}
			return ds;
		}

		private static void AddReportColumns(DataTable tData)
		{
			tData.Columns.Add(new DataColumn("PointId"));
			tData.Columns.Add(new DataColumn("TimeOutgoing"));
			tData.Columns.Add(new DataColumn("PointNameFrom"));
			tData.Columns.Add(new DataColumn("TimeIncoming"));
			tData.Columns.Add(new DataColumn("PointNameTo"));
			tData.Columns.Add(new DataColumn("TotalRun"));
			tData.Columns.Add(new DataColumn("TimeSpent"));
		}

		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new ZoneLeaveEnterParameters { OperatorId = settings.OperatorId };
		}

		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as ZoneLeaveEnterParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleGroupId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}

		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ZoneLeaveEnterReport"];
		}

		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ZoneLeaveEnterReportDescription"];
		}

		private IEnumerable<ZoneLeaveEnterResult> GetFinalList(ZoneLeaveEnterParameters parameters)
		{
			var vehicles = GetVehiclesByParameterGroup(parameters.VehicleGroupId, parameters.OperatorId, parameters.Culture);
			var result = new List<ZoneLeaveEnterResult>();

			var startendzones = new List<GeoZone>
			{
				mclsInstance.GetGeoZoneById(parameters.StartZoneId),
			};
			if (parameters.StartZoneId != parameters.EndZoneId)
			{
				var zone = mclsInstance.GetGeoZoneById(parameters.EndZoneId);
				startendzones.Add(zone);
			}

			var runsLog = mclsInstance.GetRunLogForGroup(
				parameters.VehicleGroupId,
				parameters.DateFromInt,
				parameters.DateToInt);

			foreach (var vid in vehicles)
			{
				var actionIntervals = runsLog.Where(x => x.VehicleId == vid.id).Select(record => new ActionInterval
				{
					VehicleId = record.VehicleId,
					LogTime = record.LogTime,
					Odometer = record.Odometer,
				}).OrderBy(item => item.LogTime).ToList();

				var vehicleLogTimeParams = actionIntervals.Select(ai => new VehicleLogTimeParam(vid.id, ai.LogTime)).ToArray();
				var vehicleZoneParams = new List<VehicleZoneParam>
				{
					new VehicleZoneParam(vid.id, parameters.StartZoneId),
				};
				if(parameters.StartZoneId != parameters.EndZoneId)
				{
					var @param = new VehicleZoneParam(vid.id, parameters.EndZoneId);
					vehicleZoneParams.Add(@param);
				}
				var geoZonesForPositions =
					mclsInstance.GetGeoZonesForPositions(vehicleZoneParams, vehicleLogTimeParams);

				var orderedList = geoZonesForPositions.OrderBy(x => x.LogTime);
				ZoneLeaveEnterResult re = null;
				var outZoneIndex = -1;
				var curentZoneIndex = -1;
				foreach (var p in orderedList)
				{
					var searchResult = actionIntervals.BinarySearch<ActionInterval>(x => p.LogTime.CompareTo(x.LogTime));
					curentZoneIndex = searchResult.From;
					if (re == null)
					{
						re = new ZoneLeaveEnterResult
						{
							VehicleId    = p.VehicleId,
							VehicleName  = vid.Name,
							LogTimeLeft  = p.LogTime,
							ZoneLeftId   = p.ZoneId,
							ZoneLeftName = startendzones.First(x => x.Id == p.ZoneId).Name
						};

						outZoneIndex = curentZoneIndex;
						continue;
					}

					if (re.ZoneLeftId != p.ZoneId || curentZoneIndex != outZoneIndex + 1)
					{
						re.LogTimeLeft   = actionIntervals[outZoneIndex + 1].LogTime;
						re.ZoneEnterId   = p.ZoneId;
						re.ZoneEnterName = startendzones.First(x => x.Id == p.ZoneId).Name;
						re.LogTimeEnter  = p.LogTime;
						re.Run           = actionIntervals.First(
							x => x.VehicleId == re.VehicleId && x.LogTime == re.LogTimeEnter).Odometer
							-
							actionIntervals.First(
								x => x.VehicleId == re.VehicleId && (x.LogTime == re.LogTimeLeft || re.LogTimeLeft == 0)).Odometer;
						result.Add(re);

						re = new ZoneLeaveEnterResult
						{
							VehicleId    = p.VehicleId,
							VehicleName  = vid.Name,
							ZoneLeftId   = p.ZoneId,
							ZoneLeftName = startendzones.First(x => x.Id == p.ZoneId).Name
						};

						outZoneIndex = curentZoneIndex;
					}
					else
					{
						outZoneIndex++;
					}
				}
			}

			if (result.Count > 0)
				UpdateWithStandardDistance(result, parameters.StartZoneId, parameters.EndZoneId, parameters.OperatorId);

			return result;
		}

		private void UpdateWithStandardDistance(List<ZoneLeaveEnterResult> list, int startZoneId, int endZoneId, int operatorId)
		{
			var sd = mclsInstance.GetAllZoneDistanceStandarts(operatorId).Tables[0]
				.AsEnumerable()
				.Where(x =>
					Convert.ToInt32(x["zone_id_from"]) == startZoneId &&
					Convert.ToInt32(x["zone_id_to"])   == endZoneId ||
					Convert.ToInt32(x["zone_id_from"]) == endZoneId &&
					Convert.ToInt32(x["zone_id_to"])   == startZoneId)
				.Select(x => new
				{
					ZoneFrom = Convert.ToInt32(x["zone_id_from"]),
					ZoneTo   = Convert.ToInt32(x["zone_id_to"]),
					Distance = Convert.ToDouble(x["distance"])
				})
				.ToList();
			foreach (var sdx in sd)
			{
				var sdxClosure = sdx;
				foreach (var elem in
					list.Where(elem => elem.ZoneLeftId == sdxClosure.ZoneFrom && elem.ZoneEnterId == sdxClosure.ZoneTo))
				{
					elem.StandardDistance = sdx.Distance;
					elem.DistanceDifference = (elem.Run / 100) - sdx.Distance; //Пробег - в сантиметрах -> метры
				}
			}
		}

		private IEnumerable<Vehicle> GetVehiclesByParameterGroup(int vehicleGroupId, int operatorId, CultureInfo culture)
		{
			var result = mclsInstance.GetVehiclesByVehicleGroup(vehicleGroupId, operatorId, culture, SystemRight.PathAccess);
			return result;
		}

		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var clsReportParameters = (ZoneLeaveEnterParameters)reportParameters;

			return GetReportHtml(clsReportParameters);
		}

		private string GetReportHtml(ZoneLeaveEnterParameters pars)
		{
			var stringBuilder = new StringBuilder();
			var @string = GetResourceStringContainer(pars.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xmlWriter = new XmlTextWriter(stringWriter))
			{
				// Подитоги по неделям.
				xmlWriter.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xmlWriter.WriteAttributeString("class", "subReportBody");

				xmlWriter.WriteStartElement("thead");
				xmlWriter.WriteStartElement("tr");

				xmlWriter.WriteElementString("th", @string["monitoringObject"]);
				xmlWriter.WriteElementString("th", @string["StartGeoZone"]);
				xmlWriter.WriteElementString("th", @string["TimeOutgoing"]);
				xmlWriter.WriteElementString("th", @string["Run"]);
				xmlWriter.WriteElementString("th", @string["StandardDistance"]);
				xmlWriter.WriteElementString("th", @string["DifferentDistance"]);
				xmlWriter.WriteElementString("th", @string["EndGeoZone"]);
				xmlWriter.WriteElementString("th", @string["TimeIncoming"]);
				xmlWriter.WriteElementString("th", @string["Path"]);

				xmlWriter.WriteEndElement();
				xmlWriter.WriteEndElement();

				var finalResult = GetFinalList(pars);
				if (finalResult == null)
					throw new ArgumentException(string.Format("List result is empty. Parameters are. from:{0}, to:{1}, zfid:{2}, ztid:{3}, vgid:{4}", pars.DateFrom, pars.DateTo, pars.StartZoneId, pars.EndZoneId, pars.VehicleGroupId));

				var cnt = 0;
				foreach (var r in finalResult)
				{
					cnt++;
					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
							, pars.ApplicationPath
							, r.VehicleId
							, TimeHelper.GetLocalTime(r.LogTimeLeft, pars.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss")
							, TimeHelper.GetLocalTime(r.LogTimeEnter, pars.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss"));

					xmlWriter.WriteStartElement("tr");
					xmlWriter.WriteElementString("td", r.VehicleName);
					xmlWriter.WriteElementString("td", r.ZoneLeftName);
					xmlWriter.WriteElementString("td", TimeHelper.GetLocalTime(r.LogTimeLeft, pars.TimeZoneInfo).ToString(pars.Culture));
					xmlWriter.WriteElementString("td", string.Format("{0:0.000}", r.Run / 100000));

					xmlWriter.WriteStartElement("td");
					var innerText = (int)Math.Round(r.StandardDistance) != 0
						? string.Format("{0:0.000}", r.StandardDistance / 1000) //м -> км
						: @string["StandardNotSet"];
					xmlWriter.WriteStartElement("a");
					xmlWriter.WriteAttributeString("id", string.Format("standard{0}", cnt));
					xmlWriter.WriteAttributeString("href", "#");
					xmlWriter.WriteAttributeString("onclick", string.Format(
						"javascript:myReports.changeStandartDistance('{0}', '{1}', '{2}', '{3}', {5}, 'standard{4}');"
						, r.ZoneLeftId, r.ZoneEnterId, r.ZoneLeftName, r.ZoneEnterName, cnt, Math.Round(r.StandardDistance)));
					xmlWriter.WriteString(innerText);
					xmlWriter.WriteEndElement();
					xmlWriter.WriteEndElement();

					xmlWriter.WriteStartElement("td");
					if ((int)Math.Round(r.StandardDistance) != 0)
					{
						if (r.DistanceDifference > 0)
							xmlWriter.WriteAttributeString("class", "negativeValue");
						xmlWriter.WriteString(string.Format("{0:0.000}", r.DistanceDifference / 1000));
					}
					xmlWriter.WriteEndElement();

					xmlWriter.WriteElementString("td", r.ZoneEnterName);
					xmlWriter.WriteElementString("td", TimeHelper.GetLocalTime(r.LogTimeEnter, pars.TimeZoneInfo).ToString(pars.Culture));

					xmlWriter.WriteStartElement("td");
					xmlWriter.WriteStartElement("a");
					xmlWriter.WriteAttributeString("href", path.Replace(' ', '+'));
					xmlWriter.WriteString(@string["ShowOnMap"]);
					xmlWriter.WriteEndElement();
					xmlWriter.WriteEndElement();


					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement();
			}

			return stringBuilder.ToString();
		}
	}
}