﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.TransportDispatcher.Reports
{
    [Serializable]
    public class AccessControlReportResult
    {
        public int VehicleId;
        public string Name;
        public List<AccessControlReportDetail> Details;

        public AccessControlReportResult()
        {
            Details = new List<AccessControlReportDetail>();
        }
    }
}