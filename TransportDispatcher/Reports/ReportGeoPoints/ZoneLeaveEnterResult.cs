﻿namespace FORIS.TSS.TransportDispatcher.Reports
{
    internal class ZoneLeaveEnterResult
    {
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public int LogTimeLeft { get; set; }
        public int LogTimeEnter { get; set; }
        public double Run { get; set; }
        public double StandardDistance { get; set; }
        public double DistanceDifference { get; set; }
        public int ZoneLeftId { get; set; }
        public int ZoneEnterId { get; set; }
        public string ZoneLeftName { get; set; }
        public string ZoneEnterName { get; set; }
    }
}