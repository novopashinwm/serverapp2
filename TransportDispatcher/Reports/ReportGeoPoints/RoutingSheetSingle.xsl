<xsl:stylesheet version="1.0"
 xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">

      <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
          <Alignment ss:Vertical="Bottom"/>
          <Borders/>
          <Font/>
          <Interior/>
          <NumberFormat/>
          <Protection/>
        </Style>
        <Style ss:ID="s20">
          <Font ss:Bold="0"/>
          <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
        </Style>
        <Style ss:ID="s21">
          <Font ss:Bold="1"/>
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
        </Style>
        <Style ss:ID="s31">
          <Font ss:Bold="1"/>
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
        </Style>
        <Style ss:ID="s22">
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
          <Font ss:Bold="1"/>
          <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s23" ss:Name="Currency">
          <NumberFormat
           ss:Format="_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"/>
        </Style>
        <Style ss:ID="s24">
          <NumberFormat ss:Format="_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"/>
        </Style>
        <Style ss:ID="s25">
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
        </Style>
        <Style ss:ID="s28">
          <NumberFormat ss:Format="#,##0.000"/>
        </Style>
      </Styles>

      <xsl:apply-templates/>
    </Workbook>
  </xsl:template>

  <xsl:template match="/*">
    <Worksheet>
      <xsl:attribute name="ss:Name">
        <xsl:value-of select="/RoutingSheet/Legend[id='CaptionReport']/reportLegendValue"/>
      </xsl:attribute>
      <Table x:FullColumns="1" x:FullRows="1">
        <Column ss:AutoFitWidth="0" ss:Width="70" />
        <Column ss:AutoFitWidth="0" ss:Width="70" />
        <Column ss:AutoFitWidth="0" ss:Width="100" />
        <Column ss:AutoFitWidth="0" ss:Width="200" />
        <Column ss:AutoFitWidth="0" ss:Width="100" />
        <Column ss:AutoFitWidth="0" ss:Width="100" />
        <Column ss:AutoFitWidth="0" ss:Width="100" />
        <xsl:apply-templates/>
      </Table>
    </Worksheet>
  </xsl:template>

  <xsl:template match="/*/Report">
    <xsl:choose>
      <xsl:when test="position() = 1">
        <Row>
          <xsl:for-each select="*">
            <Cell ss:StyleID="s21">
              <Data ss:Type="String">
                <xsl:value-of select="text()"/>
              </Data>
            </Cell>
          </xsl:for-each>
        </Row>
      </xsl:when>
      <xsl:otherwise>
        <Row>
          <xsl:for-each select="*">
            <Cell ss:StyleID="s20">
              <Data ss:Type="String">
                <xsl:value-of select="text()"/>
              </Data>
            </Cell>
          </xsl:for-each>
        </Row>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/*/Legend">
    <Row>
      <Cell>
        <Data ss:Type="String">
          <xsl:value-of select="./reportLegendKey"/>
        </Data>
      </Cell>
      <Cell>
        <Data ss:Type="String">
          <xsl:value-of select="./reportLegendValue"/>
        </Data>
      </Cell>
    </Row>
  </xsl:template>

</xsl:stylesheet>
