﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт "Маршрутный лист сводный." </summary>
	/// <remarks> Также известен как "By days" </remarks>
	[Guid("A2E44AB0-8678-4B5E-B80D-91B866F3867A")]
	public class RoutingSheetSummary : TssReportBase
	{
		public override int Order
		{
			get { return 2; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeoPointReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			RoutingSummaryExcelPdfReport customReport = null;
			DataSet dataSet = null;
			var processed = false;
			try
			{
				dataSet = GetReportDataSet((RoutingSheetSummaryParameters)iReportParameters);
				customReport = new RoutingSummaryExcelPdfReport();
				customReport.SetDataSource(dataSet);
				processed = true;
				return customReport;
			}
			finally
			{
				if (dataSet != null)
					dataSet.Dispose();
				if(!processed && customReport != null)
					customReport.Dispose();
			}
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var routingSheetParameters = new RoutingSheetSummaryParameters();
			return routingSheetParameters;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as RoutingSheetSummaryParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleId <= 0)
				return false;

			if (parameters.GeoZoneGroupId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["RoutingSheetSummary"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["RoutingSheetSummaryDescription"];
		}
		private DataSet GetReportDataSet(RoutingSheetSummaryParameters pars)
		{
			if (pars.VehicleId == 0)
				return null;

			var vehicleObj = mclsInstance.GetVehicleById(pars.OperatorId, pars.VehicleId, pars.Culture);

			var finalList = GetFinalList(pars);
			if (finalList == null)
				throw new ArgumentException(string.Format("List result is empty. Parameters are. from:{0}, to:{1}, gzid:{2}, vid:{3}", pars.DateFrom, pars.DateTo, pars.GeoZoneGroupId, pars.VehicleId));

			var @string = GetResourceStringContainer(pars.Culture);

			var reportSummaryList = new List<ReportDataSummaryElement>();

			var dfi = DateTimeFormatInfo.CurrentInfo ?? DateTimeFormatInfo.InvariantInfo;
			var calendar = dfi.Calendar;

			var currentDate = pars.DateFrom;
			var cntr = 0;
			while (currentDate <= pars.DateTo)
			{
				cntr++;
				DateTime currentDateClosure = currentDate;
				var sublist =
					finalList.Where(
						x =>
						x.TimeIncoming > currentDateClosure && x.TimeIncoming < currentDateClosure.AddDays(1)).ToArray();

				ReportDataSummaryElement reportDataSummaryElement;
				var weekOfYear = calendar.GetWeekOfYear(currentDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

				if (sublist.Any())
					reportDataSummaryElement = new ReportDataSummaryElement
					{
						Id = cntr,
						ReportDate = currentDate,
						Run = sublist.Sum(x => x.Odometer),
						StartDateTime = sublist.Min(x => x.TimeIncoming),
						EndDateTime = sublist.Max(x => x.TimeOutgoing),
						Elapsed = TimeSpan.FromSeconds(sublist.Sum(x => x.TimeElapsed.TotalSeconds)),
						PointsVisited = sublist.Count(),
						WeekNumber = weekOfYear
					};
				else
				{
					reportDataSummaryElement = new ReportDataSummaryElement
					{
						Id = cntr,
						ReportDate = currentDate,
						Run = 0,
						StartDateTime = DateTime.MinValue,
						EndDateTime = DateTime.MinValue,
						Elapsed = TimeSpan.Zero,
						PointsVisited = 0,
						WeekNumber = weekOfYear
					};
				}
				reportSummaryList.Add(reportDataSummaryElement);
				currentDate = currentDate.AddDays(1);
			}

			var result = new DataSet("RoutingSheetSummary");
			var legend = new DataTable("Legend");
			result.Tables.Add(legend);

			legend.Columns.AddRange(new[]
			{
				new DataColumn("reportLegendKey"),
				new DataColumn("reportLegendValue")
			});

			var legendRow = legend.NewRow();
			legendRow["reportLegendKey"] = @string["CaptionReport"];
			legendRow["reportLegendValue"] = @string["RoutingSheetSummary"];
			legend.Rows.Add(legendRow);

			legendRow = legend.NewRow();
			legendRow["reportLegendKey"] = @string["CaptionObjectName"];
			legendRow["reportLegendValue"] = vehicleObj.Name;
			legend.Rows.Add(legendRow);

			legendRow = legend.NewRow();
			legendRow["reportLegendKey"] = @string["CaptionDateFrom"];
			legendRow["reportLegendValue"] = pars.DateFrom.ToShortDateString();
			legend.Rows.Add(legendRow);

			legendRow = legend.NewRow();
			legendRow["reportLegendKey"] = @string["CaptionDateTo"];
			legendRow["reportLegendValue"] = pars.DateTo.ToShortDateString();
			legend.Rows.Add(legendRow);

			legendRow = legend.NewRow();
			legendRow["reportLegendKey"] = @string["CaptionZoneGroupName"];
			var geozoneGroup = mclsInstance.GetZoneGroupById(pars.GeoZoneGroupId);
			legendRow["reportLegendValue"] = geozoneGroup != null ? geozoneGroup.name : string.Empty;
			legend.Rows.Add(legendRow);

			// Сводная таблица.
			// Заголовок.
			var summaryHeader = new DataTable("summaryHeader");
			summaryHeader.Columns.AddRange(new[]
			{
				new DataColumn("WeekNumber"),
				new DataColumn("DateFrom"),
				new DataColumn("DateTo"),
				new DataColumn("DataPointsVisited"),
				new DataColumn("DataTotalTimeSpend"),
				new DataColumn("DataTotalRun")
			});
			result.Tables.Add(summaryHeader);

			var summaryHeaderRow = summaryHeader.NewRow();

			summaryHeaderRow["WeekNumber"] = @string["WeekNumber"];
			summaryHeaderRow["DateFrom"] = @string["DateFrom"];
			summaryHeaderRow["DateTo"] = @string["DateTo"];
			summaryHeaderRow["DataPointsVisited"] = @string["DataPointsVisited"];
			summaryHeaderRow["DataTotalTimeSpend"] = @string["DataTotalTimeSpend"];
			summaryHeaderRow["DataTotalRun"] = @string["DataTotalRun"];

			summaryHeader.Rows.Add(summaryHeaderRow);

			// Данные.
			var summaryBody = new DataTable("summaryBody");
			summaryBody.Columns.AddRange(new[]
			{
				new DataColumn("WeekNumber"),
				new DataColumn("DateFrom"),
				new DataColumn("DateTo"),
				new DataColumn("DataPointsVisited"),
				new DataColumn("DataTotalTimeSpend"),
				new DataColumn("DataTotalRun")
			});
			result.Tables.Add(summaryBody);

			var groupedResults =
					reportSummaryList.GroupBy(x => x.WeekNumber).Select(group => new
					{
						group.Key,
						Cnt = group.Count(),
						MinDate = group.Min(x => x.ReportDate),
						MaxDate = group.Max(x => x.ReportDate),
						PointsVisited = group.Sum(x => x.PointsVisited),
						TotalRun = group.Sum(x => x.Run),
						TotalTimeSpend = group.Sum(x => x.Elapsed.TotalSeconds)
					});
			foreach (var item in groupedResults)
			{
				var bodyRow = summaryBody.NewRow();
				bodyRow["WeekNumber"] = item.Key;
				bodyRow["DateFrom"] = string.Format(item.MinDate.ToShortDateString());
				bodyRow["DateTo"] = string.Format(item.MaxDate.ToShortDateString());
				bodyRow["DataPointsVisited"] = item.PointsVisited;
				bodyRow["DataTotalTimeSpend"] = TimeHelperEx.GetTimeSpanReadable(
					TimeSpan.FromSeconds(item.TotalTimeSpend), pars.Culture);
				bodyRow["DataTotalRun"] = item.TotalRun;
				summaryBody.Rows.Add(bodyRow);
			}

			var dailyHeader = new DataTable("dailyHeader");
			dailyHeader.Columns.AddRange(new[]
			{
				new DataColumn("ReportDate"),
				new DataColumn("TotalRun"),
				new DataColumn("TimeIncoming"),
				new DataColumn("TimeOutgoing"),
				new DataColumn("PointsCount"),
				new DataColumn("TimeElapsed")
			});
			result.Tables.Add(dailyHeader);

			var dailyHeaderRow = dailyHeader.NewRow();
			dailyHeaderRow["ReportDate"] = @string["ReportDate"];
			dailyHeaderRow["TotalRun"] = @string["TotalRun"];
			dailyHeaderRow["TimeIncoming"] = @string["TimeIncomingToFirstZone"];
			dailyHeaderRow["TimeOutgoing"] = @string["TimeOutgoingFromLastZone"];
			dailyHeaderRow["PointsCount"] = @string["PointsVisited"];
			dailyHeaderRow["TimeElapsed"] = @string["TimeElapsed"];
			dailyHeader.Rows.Add(dailyHeaderRow);

			var dailyBody = new DataTable("dailyBody");
			dailyBody.Columns.AddRange(new[]
			{
				new DataColumn("ReportDate"),
				new DataColumn("TotalRun"),
				new DataColumn("TimeIncoming"),
				new DataColumn("TimeOutgoing"),
				new DataColumn("PointsCount"),
				new DataColumn("TimeElapsed")
			});
			result.Tables.Add(dailyBody);

			foreach (var item in reportSummaryList)
			{
				var bodyRow = dailyBody.NewRow();
				bodyRow["ReportDate"] = string.Format("{0} ({1})", item.ReportDate.ToString("dd.MM.yyyy"), item.WeekNumber);
				bodyRow["TotalRun"] = string.Format("{0} {1}", Math.Round(item.Run, 0), @string["km"]);
				bodyRow["TimeIncoming"] = item.StartDateTime != DateTime.MinValue
					? item.StartDateTime.ToString("HH:mm:ss")
					: string.Empty;
				bodyRow["TimeOutgoing"] = item.EndDateTime != DateTime.MinValue
					? item.EndDateTime.ToString("HH:mm:ss")
					: string.Empty;
				bodyRow["PointsCount"] = item.PointsVisited.ToString(CultureInfo.InvariantCulture);
				bodyRow["TimeElapsed"] = item.Elapsed.ToString();
				dailyBody.Rows.Add(bodyRow);
			}

			return result;
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var clsReportParameters = (RoutingSheetSummaryParameters)reportParameters;

			return GetReportHtml(clsReportParameters);
		}
		private string GetReportHtml(RoutingSheetSummaryParameters pars)
		{
			if (pars.VehicleId == 0)
				return string.Empty;

			var vehicleObj = mclsInstance.GetVehicleById(pars.OperatorId, pars.VehicleId, pars.Culture);

			var stringBuilder = new StringBuilder();
			var @string = GetResourceStringContainer(pars.Culture);

			stringBuilder.Append(string.Format("{0}: {1}<br /><br />", @string["monitoringObject"], vehicleObj.Name));

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				var finalList = GetFinalList(pars);
				if (finalList == null)
					throw new ArgumentException(string.Format("List result is empty. Parameters are. from:{0}, to:{1}, gzid:{2}, vid:{3}", pars.DateFrom, pars.DateTo, pars.GeoZoneGroupId, pars.VehicleId));

				var reportSummaryList = new List<ReportDataSummaryElement>();

				var dfi = DateTimeFormatInfo.CurrentInfo ?? DateTimeFormatInfo.InvariantInfo;
				var calendar = dfi.Calendar;

				var currentDate = pars.DateFrom;
				var cntr = 0;
				while (currentDate <= pars.DateTo)
				{
					cntr++;
					DateTime currentDateClosure = currentDate;
					var sublist =
						finalList.Where(
							x =>
							x.TimeIncoming > currentDateClosure && x.TimeIncoming < currentDateClosure.AddDays(1)).ToArray();

					ReportDataSummaryElement reportDataSummaryElement;
					var weekOfYear = calendar.GetWeekOfYear(currentDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

					if (sublist.Any())
						reportDataSummaryElement = new ReportDataSummaryElement
						{
							Id = cntr,
							ReportDate = currentDate,
							Run = sublist.Sum(x => x.Odometer),
							StartDateTime = sublist.Min(x => x.TimeIncoming),
							EndDateTime = sublist.Max(x => x.TimeOutgoing),
							Elapsed = TimeSpan.FromSeconds(sublist.Sum(x => x.TimeElapsed.TotalSeconds)),
							PointsVisited = sublist.Count(),
							WeekNumber = weekOfYear
						};
					else
					{
						reportDataSummaryElement = new ReportDataSummaryElement
						{
							Id            = cntr,
							ReportDate    = currentDate,
							Run           = 0,
							StartDateTime = DateTime.MinValue,
							EndDateTime   = DateTime.MinValue,
							Elapsed       = TimeSpan.Zero,
							PointsVisited = 0,
							WeekNumber    = weekOfYear
						};
					}
					reportSummaryList.Add(reportDataSummaryElement);
					currentDate = currentDate.AddDays(1);
				}

				// Под итоги по неделям.
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xw.WriteAttributeString("class", "subReportBody");

				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("th", @string["WeekNumber"]);
				xw.WriteElementString("th", @string["DateInterval"]);
				xw.WriteElementString("th", @string["PointsVisited"]);
				xw.WriteElementString("th", @string["TimeSpent"]);
				xw.WriteElementString("th", @string["TotalRun"]);

				xw.WriteEndElement();
				xw.WriteEndElement();

				var groupedResults =
					reportSummaryList.GroupBy(x => x.WeekNumber).Select(group => new
					{
						group.Key,
						Cnt            = group.Count(),
						MinDate        = group.Min(x => x.ReportDate),
						MaxDate        = group.Max(x => x.ReportDate),
						PointsVisited  = group.Sum(x => x.PointsVisited),
						TotalRun       = group.Sum(x => x.Run),
						TotalTimeSpend = group.Sum(x => x.Elapsed.TotalSeconds)
					});

				foreach (var element in groupedResults)
				{
					xw.WriteStartElement("tr");

					xw.WriteElementString("td", element.Key.ToString(CultureInfo.InvariantCulture));
					xw.WriteElementString("td", string.Format("{0} — {1}",
						element.MinDate.ToString("dd.MM.yyyy"),
						element.MaxDate.ToString("dd.MM.yyyy")));
					xw.WriteElementString("td", element.PointsVisited.ToString(CultureInfo.InvariantCulture));
					xw.WriteElementString("td",
										  TimeHelperEx.GetTimeSpanReadable(
											  TimeSpan.FromSeconds(element.TotalTimeSpend), pars.Culture));
					xw.WriteElementString("td", string.Format("{0} {1}", element.TotalRun, @string["km"]));

					xw.WriteEndElement();
				}

				xw.WriteEndElement();

				// Подитоги по дням.
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");

				xw.WriteAttributeString("class", "reportBody");

				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("th", @string["ReportDate"]);
				xw.WriteElementString("th", @string["Run"]);
				xw.WriteElementString("th", @string["TimeIncomingToFirstZone"]);
				xw.WriteElementString("th", @string["TimeOutgoingFromLastZone"]);
				xw.WriteElementString("th", @string["PointsVisited"]);
				xw.WriteElementString("th", @string["TimeElapsed"]);

				xw.WriteEndElement();
				xw.WriteEndElement();
				foreach (var el in reportSummaryList)
				{
					xw.WriteStartElement("tr");

					xw.WriteElementString("td",
										  string.Format("{0} ({1})", el.ReportDate.ToString("dd.MM.yyyy"),
														el.WeekNumber));
					xw.WriteElementString("td",
										  string.Format("{0} {1}", Math.Round(el.Run, 0), @string["km"]));
					xw.WriteElementString("td",
										  el.StartDateTime != DateTime.MinValue
											  ? el.StartDateTime.ToString("HH:mm:ss")
											  : string.Empty);
					xw.WriteElementString("td",
										  el.EndDateTime != DateTime.MinValue
											  ? el.EndDateTime.ToString("HH:mm:ss")
											  : string.Empty);
					xw.WriteElementString("td", el.PointsVisited.ToString(CultureInfo.InvariantCulture));
					xw.WriteElementString("td", el.Elapsed.ToString());

					xw.WriteEndElement();
				}

				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "rawbold");

				xw.WriteElementString("td", @string["Total"]);
				xw.WriteElementString("td",
									  string.Format("{0} {1}", reportSummaryList.Sum(x => x.Run),
													@string["km"]));
				xw.WriteElementString("td", string.Empty);
				xw.WriteElementString("td", string.Empty);
				xw.WriteElementString("td", reportSummaryList.Sum(x => x.PointsVisited).ToString(CultureInfo.InvariantCulture));
				xw.WriteElementString("td",
									  TimeSpan.FromSeconds(reportSummaryList.Sum(x => x.Elapsed.TotalSeconds))
										  .ToString());

				xw.WriteEndElement();

				xw.WriteEndElement();
			}
			return stringBuilder.ToString();
		}
		private List<ReportDataElement> GetFinalList(RoutingSheetSummaryParameters pars)
		{
			// Получение списка геозон по выбранной группе геозон.
			var geozones = mclsInstance.GetGeoZonesByGroup(
				pars.OperatorId,
				pars.GeoZoneGroupId
				);

			// TODO: Переписать без учета того, что в результате может быть пустой объект.
			if (geozones == null || geozones.Count == 0)
				return new List<ReportDataElement>();

			return GetReportDataByVehicle(mclsInstance.GetVehicleById(pars.OperatorId, pars.VehicleId, pars.Culture), pars, geozones);
		}
		private List<ReportDataElement> GetReportDataByVehicle(
			Vehicle vehicle, RoutingSheetSummaryParameters pars, List<GeoZone> geozones)
		{
			var result = new List<ReportDataElement>();
			var vehicleId = vehicle.id;

			var log = mclsInstance.GetFullLog(
				pars.OperatorId,
				vehicleId,
				TimeHelper.GetSecondsFromBase(pars.ToUtc(pars.DateFrom)),
				TimeHelper.GetSecondsFromBase(pars.ToUtc(pars.DateTo.AddDays(1).AddSeconds(-1))),
				new FullLogOptions { CutOutstandingData = false, IsGeoPointsFilterOn = false }, pars.Culture);

			// Получение интервалов по текущему объекту наблюдения.
			var actionIntervals = log.Runs.Select(record => new ActionInterval
			{
				LogTime  = record.LogTime,
				Odometer = record.Odometer,
				Coords   = log.GetCurrentOrPreviousGeoRecord(record.LogTime)
			}).ToList();

			// Исключить предыдущую точку. Зачем?
			//actionIntervals.RemoveAt(0);

			var vehicleZoneParams =
				geozones.Select(gz => new VehicleZoneParam(vehicleId, gz.Id));

			var vehicleLogTimeParams = new List<VehicleLogTimeParam>();

			vehicleLogTimeParams.AddRange(log.GetActionIntervalsStep1().Where(x => !x.IsMotion)
				.SelectMany(ai => new[]
				{
					new VehicleLogTimeParam(vehicleId, TimeHelper.GetSecondsFromBase(ai.Start)),
					new VehicleLogTimeParam(vehicleId, TimeHelper.GetSecondsFromBase(ai.End))
				}).Distinct());

			var geoZonesForPositions =
				mclsInstance.GetGeoZonesForPositions(vehicleZoneParams, vehicleLogTimeParams);

			foreach (var ai in actionIntervals)
			{
				var aiLocal = ai;
				if (!geoZonesForPositions.Any(x => x.LogTime == aiLocal.LogTime && x.VehicleId == vehicleId))
					continue;

				var zoneId = geoZonesForPositions
					.First(x => x.LogTime == aiLocal.LogTime && x.VehicleId == vehicleId)
					.ZoneId;
				var zone = geozones.First(x => x.Id == zoneId);

				ai.ZoneFrom     = zone.Name;
				ai.PositionFrom = zone.Description;
			}

			ActionInterval    ca = null;
			ReportDataElement ce = null;

			if (actionIntervals.Any(x => x.ZoneFrom != null))
			{
				var currentodometer = actionIntervals.First(x => x.ZoneFrom != null).Odometer;

				foreach (var i in actionIntervals)
				{
					if (!string.IsNullOrWhiteSpace(i.ZoneFrom))
					{
						if (ca == null)
						{
							ca = i;
							ce = new ReportDataElement
							{
								VehicleId    = vehicle.id,
								VehicleName  = vehicle.Name,
								Odometer     = Math.Round((i.Odometer - currentodometer) / 100000d, 0),
								ZoneName     = i.ZoneFrom,
								Address      = i.PositionFrom,
								TimeIncoming = TimeHelper.GetDateTimeUTC(i.LogTime)
							};
							currentodometer = i.Odometer;
						}
						else
						{
							if (ca.ZoneFrom == i.ZoneFrom)
								continue;
							ca = i;

							ce.TimeOutgoing = TimeHelper.GetDateTimeUTC(i.LogTime);
							ce.TimeElapsed  = ce.TimeOutgoing - ce.TimeIncoming;
							result.Add(ce);

							ce = new ReportDataElement
							{
								VehicleId    = vehicle.id,
								VehicleName  = vehicle.Name,
								Odometer     = Math.Round((i.Odometer - currentodometer) / 100000d, 0),
								ZoneName     = i.ZoneFrom,
								Address      = i.PositionFrom,
								TimeIncoming = TimeHelper.GetDateTimeUTC(i.LogTime)
							};

							currentodometer = i.Odometer;
						}
					}
					else
					{
						if (ca != null)
						{
							ce.TimeOutgoing = TimeHelper.GetDateTimeUTC(i.LogTime);
							ce.TimeElapsed  = ce.TimeOutgoing - ce.TimeIncoming;
							result.Add(ce);
							ce = null;
							ca = null;
						}
					}
				}

				if (ce != null)
				{
					ce.TimeOutgoing = TimeHelper.GetDateTimeUTC(ca.LogTime);
					ce.TimeElapsed  = ce.TimeOutgoing - ce.TimeIncoming;
					result.Add(ce);
				}
			}

			if (result.Count == 0)
				return null;

			// Обнуление километража при посещении первой точки в сутках.
			//Для каждой точки, первой на начало дня, сбросить пробег до неё от предыдущей зоны
			var cDate = TimeZoneInfo.ConvertTimeFromUtc(result.First().TimeIncoming, pars.TimeZoneInfo).Date;
			var dDate = TimeZoneInfo.ConvertTimeFromUtc(result.Last().TimeOutgoing, pars.TimeZoneInfo).Date;

			while (cDate <= dDate)
			{
				var resetRunToFirstPoint = result.FirstOrDefault(
					x => TimeZoneInfo.ConvertTimeFromUtc(x.TimeIncoming, pars.TimeZoneInfo).Date == cDate);
				if (resetRunToFirstPoint != null)
					resetRunToFirstPoint.Odometer = 0;
				cDate = cDate.AddDays(1);
			}
			return result;
		}
	}
}