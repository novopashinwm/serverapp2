﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class RoutingSummaryExcelPdfReport : IReportClass
	{
		private DataSet _ds;
		private Font _fontNorm;
		private Font _fontBold;

		public RoutingSummaryExcelPdfReport()
		{
			_ds       = new DataSet();
			_fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			_fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);
		}
		public void SetDataSource(DataSet dataSet)
		{
			_ds = dataSet;
		}

		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					var stringReport = WorkbookEngine.CreateWorkbook(_ds, "RoutingSheetSummary.xsl");
					var encoding = new UTF8Encoding(false);

					using (TextWriter writer = new StreamWriter(fileName, false, encoding))
					{
						writer.Write(stringReport);
					}
					break;
				case ReportTypeEnum.Acrobat:
					var document = new Document(PageSize.A4);

					try
					{
						var pdfWriter = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
						pdfWriter.PageEvent = new PdfReportPageEvents();

						var reportName = _ds.Tables["Legend"].Rows[0]["reportLegendValue"].ToString();
						document.AddTitle(reportName); // необходимо до открытия документа
						document.Open();
						document.NewPage();
						var commonTable = new PdfPTable(2) { WidthPercentage = 50f, HorizontalAlignment = Element.ALIGN_LEFT };

						var ht = _ds.Tables["Legend"];
						foreach (DataRow row in ht.Rows)
						{
							commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendKey"].ToString(), _fontBold)));
							commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendValue"].ToString(), _fontNorm)));
						}

						document.Add(commonTable);
						document.Add(new Paragraph(new Phrase(" ")));

						var dt = _ds.Tables["summaryHeader"];
						if (dt != null)
						{
							var pTable = new PdfPTable(dt.Columns.Count);
							pTable.DefaultCell.Border = Rectangle.RECTANGLE;

							// Данные.
							foreach (DataRow row in dt.Rows)
							{
								var pCells = new PdfPCell[dt.Columns.Count];
								var pRow = new PdfPRow(pCells);
								for (var i = 0; i < dt.Columns.Count; i++)
								{
									pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold));
								}

								pTable.Rows.Add(pRow);
							}

							// Ширина таблицы.
							pTable.WidthPercentage = 100f;
							// Релятивные значения ширины столбцов.
							//pTable.SetTotalWidth(new[] { 5f, 5f, 5f, 5f, 5f });

							document.Add(pTable);
							//document.Add(new Paragraph(new Phrase(" ")));

							dt = _ds.Tables["summaryBody"];
							pTable = new PdfPTable(dt.Columns.Count);
							pTable.DefaultCell.Border = Rectangle.RECTANGLE;

							// Данные.
							foreach (DataRow row in dt.Rows)
							{
								var pCells = new PdfPCell[dt.Columns.Count];
								var pRow = new PdfPRow(pCells);
								for (var i = 0; i < dt.Columns.Count; i++)
								{
									pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));
								}

								pTable.Rows.Add(pRow);
							}

							// Ширина таблицы.
							pTable.WidthPercentage = 100f;
							// Релятивные значения ширины столбцов.
							//pTable.SetTotalWidth(new[] { 5f, 5f, 5f, 5f, 5f });

							document.Add(pTable);
							document.Add(new Paragraph(new Phrase(" ")));
						}


						dt = _ds.Tables["dailyHeader"];
						if (dt != null)
						{
							var pTable = new PdfPTable(dt.Columns.Count);
							pTable.DefaultCell.Border = Rectangle.RECTANGLE;

							// Данные.
							foreach (DataRow row in dt.Rows)
							{
								var pCells = new PdfPCell[dt.Columns.Count];
								var pRow = new PdfPRow(pCells);
								for (var i = 0; i < dt.Columns.Count; i++)
								{
									pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold));
								}

								pTable.Rows.Add(pRow);
							}

							// Ширина таблицы.
							pTable.WidthPercentage = 100f;
							// Релятивные значения ширины столбцов.
							//pTable.SetTotalWidth(new[] { 5f, 5f, 5f, 5f, 5f });

							document.Add(pTable);
							//document.Add(new Paragraph(new Phrase(" ")));

							dt = _ds.Tables["dailyBody"];
							pTable = new PdfPTable(dt.Columns.Count);
							pTable.DefaultCell.Border = Rectangle.RECTANGLE;

							// Данные.
							foreach (DataRow row in dt.Rows)
							{
								var pCells = new PdfPCell[dt.Columns.Count];
								var pRow = new PdfPRow(pCells);
								for (var i = 0; i < dt.Columns.Count; i++)
								{
									pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));
								}

								pTable.Rows.Add(pRow);
							}

							// Ширина таблицы.
							pTable.WidthPercentage = 100f;
							// Релятивные значения ширины столбцов.
							//pTable.SetTotalWidth(new[] { 5f, 5f, 5f, 5f, 5f });

							document.Add(pTable);
							document.Add(new Paragraph(new Phrase(" ")));
						}
					}
					catch (Exception ex)
					{
						Trace.Write(ex.Message);
						throw;
					}

					document.Close();
					break;
				default:
					throw new ArgumentOutOfRangeException("formatType");
			}
			// PDF Export
		}
		public void Dispose()
		{
		}
	}
}