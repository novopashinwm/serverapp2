﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary>
	/// Параметры для данного отчета.
	/// </summary>
	[Serializable]
	public class RoutingSheetSummaryParameters : ReportParameters
	{
		[DisplayName("VehicleId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker"), Options(SystemRight.PathAccess)]
		public int VehicleId { get; set; }

		[DisplayName("GeoZoneGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.ZoneGroupPicker, "ZoneGroupPicker")]
		public int GeoZoneGroupId { get; set; }

		//TODO: Implement the IMonitoreeObjectContainer, leave backward compatibility with existant subscribtions.
	}
}