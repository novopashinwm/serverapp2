﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.DataAbsence
{
	class DataAbsenceReportExcelFormatter : BaseReportExcelGenerator
	{
		private readonly DataAbsenceReportParameters _parameters;
		private readonly List<DataAbsenceRecord> _records;
		private readonly List<CellInfo> _cells;
		private readonly ResourceStringContainer _strings;

		public DataAbsenceReportExcelFormatter(
			ITssReport report,
			DataAbsenceReportParameters parameters,
			List<DataAbsenceRecord> records)
			: base(report, parameters)
		{
			_parameters = parameters;
			_records    = records;
			_strings    = report.GetResourceStringContainer(parameters.Culture);
			_cells      = new List<CellInfo>();

			if (_records.Any(c => !string.IsNullOrWhiteSpace(c.DepartmentName)))
				_cells.Add(new CellInfo
				{
					Key = "departmentName",
					GetValue = x => ((DataAbsenceRecord)x).DepartmentName
				});

			if (_records.Any(c => !string.IsNullOrWhiteSpace(c.Emails)))
				_cells.Add(new CellInfo
				{
					Key = "emails",
					GetValue = x => ((DataAbsenceRecord)x).Emails
				});

			if (_records.Any(c => !string.IsNullOrWhiteSpace(c.Phones)))
				_cells.Add(new CellInfo
				{
					Key = "phones",
					GetValue = x => ((DataAbsenceRecord)x).Phones
				});

			_cells.Add(new CellInfo
			{
				Key = "vehicleName",
				GetValue = x => ((DataAbsenceRecord)x).VehicleName
			});

			if (_records.Any(c => !string.IsNullOrWhiteSpace(c.VehicleDeviceID)))
				_cells.Add(new CellInfo
				{
					Key = "vehicleDeviceID",
					GetValue = x => ((DataAbsenceRecord)x).VehicleDeviceID
				});

			if (_records.Any(c => c.LastInsertTime != null))
			{
				_cells.Add(new CellInfo
				{
					Key = "lastInsertTime",
					ExcelType = "DateTime",
					GetValue = x => ToString(((DataAbsenceRecord)x).LastInsertTime)
				});
				_cells.Add(new CellInfo
				{
					Key = "lastInsertDays",
					ExcelType = "Number",
					GetValue = x => AgeDaysToString(((DataAbsenceRecord)x).LastInsertTime)
				});
			}
			if (_records.Any(c => c.LastLogTime != null))
			{
				_cells.Add(new CellInfo
				{
					Key = "lastLogTime",
					ExcelType = "DateTime",
					GetValue = x => ToString(((DataAbsenceRecord)x).LastLogTime)
				});
				_cells.Add(new CellInfo
				{
					Key = "lastLogDays",
					ExcelType = "Number",
					GetValue = x => AgeDaysToString(((DataAbsenceRecord)x).LastLogTime)
				});
			}
			if (_records.Any(c => c.LastGeoTime != null))
			{
				_cells.Add(new CellInfo
				{
					Key = "lastGeoTime",
					ExcelType = "DateTime",
					GetValue = x => ToString(((DataAbsenceRecord)x).LastGeoTime)
				});
				_cells.Add(new CellInfo
				{
					Key = "lastGeoDays",
					ExcelType = "Number",
					GetValue = x => AgeDaysToString(((DataAbsenceRecord)x).LastGeoTime)
				});
			}

			if (_records.Any(c => c.LastCANTime != null))
			{
				_cells.Add(new CellInfo
				{
					Key = "lastCANTime",
					ExcelType = "DateTime",
					GetValue = x => ToString(((DataAbsenceRecord)x).LastCANTime)
				});
				_cells.Add(new CellInfo
				{
					Key = "lastCANDays",
					ExcelType = "Number",
					GetValue = x => AgeDaysToString(((DataAbsenceRecord)x).LastCANTime)
				});
			}

			if (_records.Any(c => c.LastFuelTime != null))
			{
				_cells.Add(new CellInfo
				{
					Key = "lastFuelTime",
					ExcelType = "DateTime",
					GetValue = x => ToString(((DataAbsenceRecord)x).LastFuelTime)
				});
				_cells.Add(new CellInfo
				{
					Key = "lastFuelDays",
					ExcelType = "Number",
					GetValue = x => AgeDaysToString(((DataAbsenceRecord)x).LastFuelTime)
				});
			}

			foreach (var cell in _cells)
			{
				cell.Name = _strings[cell.Key];
				cell.Width = 1;
			}
		}

		private string AgeDaysToString(DateTime? dateTime)
		{
			if (dateTime == null)
				return string.Empty;

			var value = (int)(DateTime.UtcNow - dateTime.Value).TotalDays;

			return value.ToString(_parameters.Culture);
		}

		protected override string WorksheetName
		{
			get { return "Data"; }
		}

		protected override void FillHeader()
		{
			base.FillHeader();

			FillDateIntervalHeader();
		}

		protected override void FillTable()
		{
			foreach (var record in _records)
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
				foreach (var cellInfo in Cells)
				{
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
				}
				AddRow(row);
			}
		}

		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}
	}
}