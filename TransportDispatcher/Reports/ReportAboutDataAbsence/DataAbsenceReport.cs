﻿using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.DataAbsence
{
	/// <summary> Отчет о корп. клиентах, переставших пользоваться системой </summary>
	[Guid("6000AB2F-FCC8-4671-AC18-FC138F23A26F")]
	public class DataAbsenceReport : TssReportBase
	{
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var @params = (DataAbsenceReportParameters)iReportParameters;

			var dateFromUtc = iReportParameters.ToUtc(iReportParameters.DateFrom);
			var dateToUtc = iReportParameters.ToUtc(iReportParameters.DateTo.Date.AddDays(1).AddSeconds(-1));

			var records = mclsInstance.GetDataAbsences(
				iReportParameters.OperatorId, dateFromUtc, dateToUtc, @params.Culture);

			return new DataAbsenceReportFormatter(this, @params, records);
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new DataAbsenceReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as DataAbsenceReportParameters;

			if (parameters == null)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["DataAbsence"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["DataAbsenceDescription"];
		}
		public override int Order
		{
			get { return 10; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			return new[] { ReportTypeEnum.Excel };
		}
	}
}