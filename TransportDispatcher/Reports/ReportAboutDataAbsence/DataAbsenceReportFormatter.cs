﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.DataAbsence
{
	class DataAbsenceReportFormatter : BaseReportExcelFormatter
	{
		private readonly DataAbsenceReport           _report;
		private readonly DataAbsenceReportParameters _parameters;
		private readonly List<DataAbsenceRecord>     _records;

		public DataAbsenceReportFormatter(
			DataAbsenceReport           report,
			DataAbsenceReportParameters parameters,
			List<DataAbsenceRecord>     records)
		{
			_report     = report;
			_parameters = parameters;
			_records    = records;
		}
		protected override string GetExcelXmlString()
		{
			var generator = new DataAbsenceReportExcelFormatter(_report, _parameters, _records);

			return generator.Create();
		}
	}
}