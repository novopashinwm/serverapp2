﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.AddRemoveServicesReport
{
	internal class AddRemoveServicesReportExcelFormatter : BaseReportExcelGenerator
	{
		private readonly ResourceStringContainer     _strings;
		private readonly List<CellInfo>              _cells;
		private readonly List<CellInfo>              _detailCells;
		private readonly AddRemoveServicesReportData _data;

		public AddRemoveServicesReportExcelFormatter(
			AddRemoveServicesReport report,
			AddRemoveServicesReportParameters parameters,
			AddRemoveServicesReportData data) : base(report, parameters)
		{
			_data    = data;
			_strings = report.GetResourceStringContainer(parameters.Culture);
			_cells   = new List<CellInfo>();

			_cells.Add(new CellInfo
			{
				Key      = "ServiceName",
				GetValue = x => ((AddRemoveServicesReportData.SummaryRecord)x).Name
			});
			_cells.Add(new CellInfo
			{
				Key       = "MinPrice",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => FormatPrice(((AddRemoveServicesReportData.SummaryRecord)x).MinPrice)
			});
			_cells.Add(new CellInfo
			{
				Key       = "MaxPrice",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => FormatPrice(((AddRemoveServicesReportData.SummaryRecord)x).MaxPrice)
			});
			_cells.Add(new CellInfo
			{
				Key       = "CountOnStart",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((AddRemoveServicesReportData.SummaryRecord)x).CountOnStart.ToString(parameters.Culture)
			});
			_cells.Add(new CellInfo
			{
				Key       = "CountOfAdded",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((AddRemoveServicesReportData.SummaryRecord)x).CountOfAdded.ToString(parameters.Culture)
			});
			_cells.Add(new CellInfo
			{
				Key       = "CountOfRemoved",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((AddRemoveServicesReportData.SummaryRecord)x).CountOfRemoved.ToString(parameters.Culture)
			});
			_cells.Add(new CellInfo
			{
				Key       = "CountOnEnd",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((AddRemoveServicesReportData.SummaryRecord)x).CountOnEnd.ToString(parameters.Culture)
			});
			_cells.Add(new CellInfo
			{
				Key       = "CountDelta",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((AddRemoveServicesReportData.SummaryRecord) x).CountDelta.ToString(parameters.Culture)
			});

			var serviceToName = _data.SummaryRecords.ToDictionary(x => x.Id, x => x.Name);

			_detailCells = new List<CellInfo>();

			_detailCells.Add(new CellInfo
			{
				Key = "ServiceName",
				GetValue = x => serviceToName[((AddRemoveServicesReportData.DetailRecord)x).ServiceId]
			});

			_detailCells.Add(new CellInfo
			{
				Key = "TerminalDeviceNumber",
				GetValue = x =>
					{
						var terminalDeviceNumber = ((AddRemoveServicesReportData.DetailRecord) x).TerminalDeviceNumber;
						if (terminalDeviceNumber == null)
							return string.Empty;
						return terminalDeviceNumber.Value.ToString(CultureInfo.InvariantCulture);
					}
			});

			_detailCells.Add(new CellInfo
			{
				Key = "VehicleName",
				GetValue = x => ((AddRemoveServicesReportData.DetailRecord)x).VehicleName ?? string.Empty
			});

			_detailCells.Add(new CellInfo
			{
				Key = "ContractNumber",
				GetValue = x => ((AddRemoveServicesReportData.DetailRecord)x).ContractNumber ?? string.Empty
			});

			_detailCells.Add(new CellInfo
			{
				Key = "Added",
				GetValue = x => FormatBooleanAsYesNo(((AddRemoveServicesReportData.DetailRecord)x).Added)
			});

			_detailCells.Add(new CellInfo
			{
				Key = "Removed",
				GetValue = x => FormatBooleanAsYesNo(((AddRemoveServicesReportData.DetailRecord)x).Removed)
			});

			_detailCells.Add(new CellInfo
			{
				Key = "StartDate",
				GetValue = x => FormatDateTime(((AddRemoveServicesReportData.DetailRecord)x).StartDate)
			});

			_detailCells.Add(new CellInfo
			{
				Key = "EndDate",
				GetValue = x => FormatDateTime(((AddRemoveServicesReportData.DetailRecord)x).EndDate)
			});

			foreach (var cell in _cells.Union(_detailCells))
			{
				cell.Name = _strings[cell.Key];
			}
		}
		private string FormatDateTime(DateTime? dateTime)
		{
			if (dateTime == null)
				return string.Empty;

			return dateTime.Value.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
		}
		private string FormatBooleanAsYesNo(bool value)
		{
			return _strings[value ? "Yes" : "No"];
		}
		private string FormatPrice(decimal? price)
		{
			if (price == null)
				return string.Empty;

			return price.Value.ToString("#.00", NumberHelper.FormatInfo);
		}
		protected override void FillTable()
		{
			foreach (var record in _data.SummaryRecords)
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
				foreach (var cellInfo in Cells)
				{
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
				}
				AddRow(row);
			}

			if (_data.DetailRecords != null)
			{
				var table = CreateTableInNewWorksheet(_detailCells, _strings["Details"]);

				CreateTableHeader(table, _detailCells, true);
				foreach (var record in _data.DetailRecords)
				{
					var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
					foreach (var cellInfo in _detailCells)
					{
						row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
					}
					AddRow(row, table);
				}
			}
		}
		protected override void FillHeader()
		{
			base.FillHeader();

			FillDateIntervalHeader();
		}
		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}
	}
}