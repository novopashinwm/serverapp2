﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.AddRemoveServicesReport
{
	class AddRemoveServicesReportFormatter : BaseReportExcelFormatter
	{
		private readonly AddRemoveServicesReport           _report;
		private readonly AddRemoveServicesReportParameters _parameters;
		private readonly AddRemoveServicesReportData       _data;

		public AddRemoveServicesReportFormatter(
			AddRemoveServicesReport report,
			AddRemoveServicesReportParameters parameters,
			AddRemoveServicesReportData data)
		{
			_report = report;
			_parameters = parameters;
			_data = data;
		}

		protected override string GetExcelXmlString()
		{
			var generator = new AddRemoveServicesReportExcelFormatter(_report, _parameters, _data);

			return generator.Create();
		}
	}
}