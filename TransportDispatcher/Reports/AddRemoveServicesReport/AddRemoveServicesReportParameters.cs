﻿using System;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.AddRemoveServicesReport
{
	[Serializable]
	public class AddRemoveServicesReportParameters : ReportParameters
	{
		private bool _isDetailed;

		[DisplayName("IsDetailed"), Type(typeof(bool)), ControlType(ReportParametersUtils.Boolean, "IsDetailed")]
		public Boolean IsDetailed
		{
			get { return _isDetailed; }
			set { _isDetailed = value; }
		}
	}
}