﻿using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;
using GuidAttribute = System.Runtime.InteropServices.GuidAttribute;

namespace RU.NVG.NIKA.Reports.AddRemoveServicesReport
{
	/// <summary> Отчёт "Подключение услуг" </summary>
	/// <summary> Показывает, сколько и каких услуг было подключено и удалено абонентам за период </summary>
	[Guid("2E0725EB-978D-439A-8112-3ECBDBCCD4D3")]
	public class AddRemoveServicesReport : TssReportBase
	{
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var parameters = (AddRemoveServicesReportParameters)iReportParameters;


			var dateFrom = parameters.ToUtc(parameters.DateFrom.Date);
			var dateTo = parameters.ToUtc(parameters.DateTo.Date.AddDays(1).AddSeconds(-1));

			var data = mclsInstance.ReportAddRemoveServices(
				dateFrom, dateTo, parameters.IsDetailed, parameters.OperatorId);

			mclsInstance.FillVehicleNames(data.DetailRecords, record => record.VehicleId, record => record.VehicleName, iReportParameters.OperatorId, iReportParameters.Culture);

			if (data.DetailRecords != null)
			{
				foreach (var record in data.DetailRecords)
				{
					record.StartDate = parameters.FromUtc(record.StartDate);
					if (record.EndDate != null)
						record.EndDate = parameters.FromUtc(record.EndDate.Value);
				}
			}
			return new AddRemoveServicesReportFormatter(this, parameters, data);
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new AddRemoveServicesReportParameters();
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as AddRemoveServicesReportParameters;
			if (parameters == null)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["AddRemoveServicesReport"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["AddRemoveServicesReportDescription"];
		}
		public override int Order
		{
			get { return 12; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Excel;
		}
	}
}