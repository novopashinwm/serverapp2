﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class FuelCostSingleReportParameters : FuelCostReportParameters
	{
		[DisplayName("VehicleId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker"), Order(1), Options(SystemRight.PathAccess)]
		public int VehicleId
		{
			get { return VehicleID; }
			set { VehicleID = value; }
		}

		public new int VehicleGroupId { get; set; }
	}
}