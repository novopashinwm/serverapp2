﻿using System;
using System.Data;
using System.Globalization;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	internal static class DataRowExtension
	{
		public static string IntegerToString(this DataRow dr, string columnName)
		{
			var o = dr[columnName];
			if (o == DBNull.Value)
				return string.Empty;

			var i = Convert.ToInt32(dr[columnName]);

			return i.ToString(CultureInfo.InvariantCulture);
		}
	}
}