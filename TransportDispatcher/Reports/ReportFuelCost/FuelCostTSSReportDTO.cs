﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class FuelCostTssReportDTO
	{
		/// <summary> Строка данных отчета </summary>
		[Serializable]
		public class FuelCostReportRowData
		{
			/// <summary> Адрес, вычисленный по координате (Lat, Lng) </summary>
			public string Address;
			/// <summary> Интервал однородного изменения значения датчика (начало интервала) </summary>
			public DateTime? DateFrom;
			/// <summary> Интервал однородного изменения значения датчика (конец интервала) </summary>
			public DateTime? DateTo;
			/// <summary> Значение датчика на конец интервала </summary>
			public float? F_next;
			/// <summary> Значение датчика на начало интервала </summary>
			public float? F_prev;
			/// <summary> Целочисленное значение даты начала интервала </summary>
			public int? LT_From;
			/// <summary>  Целочисленное значение даты конца интервала </summary>
			public int? LT_To;
			/// <summary> Широта </summary>
			public decimal? Lat;
			/// <summary> Долгота </summary>
			public decimal? Lng;
			/// <summary> Изменение в баке (F_next - F_prev) </summary>
			public float? Refuel;
			/// <summary> Количество литров для заправки по оплате </summary>
			public decimal? RefuelByBill;
			/// <summary> Длительность интервала LT_To - LT_From </summary>
			public int? StopTime;
			/// <summary> Идентификатор ТС </summary>
			public int Vehicle_ID;
		}
		/// <summary> Строка итогов отчета </summary>
		[Serializable]
		public class FuelCostReportTotalRow
		{
			/// <summary> Заправлено объем, л. </summary>
			public double? Refuel_amount;
			/// <summary> Количество заправок </summary>
			public int? Refuel_count;
			/// <summary> Слито объем, л. </summary>
			public double? Dump_amount;
			/// <summary> Количество сливов </summary>
			public int? Dump_count;
			/// <summary> Расход по нормативу, л (если выбран расчет по пробегу) </summary>
			public float? FuelByStandard;
			/// <summary> Расход по нормативу, л (если выбран расчет в моточасах) </summary>
			public decimal? FuelByStandardMH;
			/// <summary> Средний расход по датчику, л/100км (если выбран расчет по пробегу) </summary>
			public float? FuelPerHundred;
			/// <summary> Средний расход по датчику, л/моточас (если выбран расчет в моточасах) </summary>
			public decimal? FuelPerMachineHour;
			/// <summary> Расход топлива за период, л. </summary>
			public float? FuelSpend;
			/// <summary> Норматив расхода топлива, л/100км (если выбран расчет по пробегу) </summary>
			public float? FuelSpendStandard;
			/// <summary> Норматив расхода топлива, л/моточас (если выбран расчет в моточасах) </summary>
			public decimal? FuelSpendStandardMH;
			/// <summary> Отклонение за период, л (если выбран расчет по пробегу) </summary>
			public float? DiffFromStandard;
			/// <summary> Отклонение за период, л (если выбран расчет в моточасах) </summary>
			public decimal? DiffFromStandardMH;
			/// <summary> Всего моточасов (если выбран расчет в моточасах) </summary>
			public decimal? MachineHours;
			/// <summary> Пробег в километрах </summary>
			public long? Run;
			/// <summary> Идентификатор типа сенсора </summary>
			public int? SensorType;
			/// <summary> Название типа сенсора </summary>
			public string SensorTypeName;
			/// <summary> Уровень топлива в начале периода, л. </summary>
			public float? Start_level;
			/// <summary> Уровень топлива в конце периода, л. </summary>
			public float? End_level;
			/// <summary> Название ТС </summary>
			public string Vehicle;
			/// <summary> Тип ТС </summary>
			/// <see cref="VehicleKind"/>
			public int? Vehicle_Kind_ID;
			/// <summary> Идентификатор ТС </summary>
			public int Vehicle_ID;
		}
		[Serializable]
		public class FuelCostReportHeaderData
		{
			/// <summary> Тип норматива расхода топлива
			/// Run         - Норматив по пробегу:   литров на километр.
			/// EngineHours - Норматив по моточасам: литров на моточас.
			/// </summary>
			public string FuelSpendStandardType;
			/// <summary> Название ТС </summary>
			public string Vehicle;
			/// <summary> Дата начала отчета в выбранной таймзоне </summary>
			public DateTime? DtFrom;
			/// <summary> Дата конца отчета в выбранной таймзоне </summary>
			public DateTime? DtTo;
			/// <summary> Локализованный заголовок "Информация по возможным сливам" </summary>
			public string TxtDumpsInfo;
			/// <summary> Локализованный заголовок "Информация по заправкам" </summary>
			public string TxtRefuelsInfo;
		}
		public List<FuelCostReportRowData>    Rows;
		public List<FuelCostReportTotalRow>   Totals;
		public List<FuelCostReportHeaderData> Headers;
	}
}