﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	#region FuelCostTSSReport
	/// <summary> Отчет по топливу: по одной машине и по группе, по пробегу и моточасам </summary> 
	[Guid("7FA68680-9660-4b9a-9DF0-986F6F9A4F56")]
	public class FuelCostTSSReport : TssReportBase
	{
		public const string DataTableName    = "Data";
		public const string TotalTableName   = "Total";
		public const string HeadersTableName = "ReportHeader";

		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostGroupReport"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostGroupReportDescription"];
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var fuelCostReportParameters = new FuelCostReportParameters { OperatorID = settings.OperatorId };

			return fuelCostReportParameters;
		}
		public override int Order
		{
			get { return 4; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
		/// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters">Набор параметров для создания отчета</param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters == null)
				{
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new FuelCostReportParameters();
				}

				//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
				var clsReportParameters = (FuelCostReportParameters)iReportParameters;

				// Возвращаются скорректированные параметры.
				return clsReportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}

		}
		/// <summary>
		/// Функция проверяет переданный набор параметров на корректность заполнения.
		/// </summary>
		/// <param name="iReportParameters">Заполненный набор параметров отчета</param>
		/// <returns>В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false</returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var clsReportParameters = iReportParameters as FuelCostReportParameters;
			//Проверка был ли передан набор параметров для отчета.
			if (clsReportParameters == null)
				return false;

			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.


			if (clsReportParameters.ShowBlankReportProp)
				return true;

			if (clsReportParameters.VehicleID <= 0)
			{
				if (clsReportParameters.VehicleGroupProp == null)
					return false;
				var vehicleGroupId = clsReportParameters.VehicleGroupProp.Tag as int?;
				if (vehicleGroupId == null || vehicleGroupId.Value <= 0)
					return false;
			}

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		protected FuelCostDataSet GetReportTypedDataSet(ReportParameters reportParameters)
		{
			// Создание пустого датасета с требуемой структурой данных для формы отчета.
			var dsFuelCostDataSet = new FuelCostDataSet();

			// Проверка был ли передан набор параметров для отчета.
			if (reportParameters != null)
			{
				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var clsReportParameters = (FuelCostReportParameters)reportParameters;

				// Требуется заполнение отчета данными?
				if (clsReportParameters.ShowBlankReportProp == false)
				{
					// Получение данных
					var dsDataFromDb = GetReportData(clsReportParameters);

					if (!dsDataFromDb.Tables["Data"].Columns.Contains("Address"))
						dsDataFromDb.Tables["Data"].Columns.Add("Address", typeof(string));

					dsDataFromDb = mclsInstance.FillAddressColumn(
						dsDataFromDb,
						"Data",
						"Lng", 
						"Lat", 
						"Address",
						clsReportParameters.Culture.TwoLetterISOLanguageName,
						clsReportParameters.MapGuid);
					dsFuelCostDataSet.Merge(dsDataFromDb);
					foreach (FuelCostDataSet.DataRow row in dsFuelCostDataSet.Data.Rows)
					{
						row.DateFrom = clsReportParameters.FromUtc(row.DateFrom);
						row.DateTo   = clsReportParameters.FromUtc(row.DateTo);
					}

					var resourceManager = new ResourceManager("FORIS.TSS.TransportDispatcher.Reports.Strings", typeof(FuelCostTSSReport).Assembly);

					// Заполнение заголовка отчета (Создание строки в таблице ReportHeader)
					// Переводим даты начала и конца в локальное время клиента
					var drHeader = (FuelCostDataSet.ReportHeaderRow)dsFuelCostDataSet.ReportHeader.Rows[0];

					drHeader.dtFrom = clsReportParameters.FromUtc(clsReportParameters.DateFrom);
					drHeader.dtTo   = clsReportParameters.FromUtc(clsReportParameters.DateTo);

					drHeader.txtDumpsInfo   = resourceManager.GetString("txtDumpsInfo", clsReportParameters.Culture);
					drHeader.txtRefuelsInfo = resourceManager.GetString("txtRefuelsInfo", clsReportParameters.Culture);

					drHeader.FuelSpendStandardType = clsReportParameters.FuelSpendStandardType.ToString();

					foreach (FuelCostDataSet.TotalRow totalDataRow in dsFuelCostDataSet.Total.Rows)
					{
						if (totalDataRow.IsSensorTypeNull())
							continue;

						switch (totalDataRow.SensorType)
						{
							case 0:
								totalDataRow.SensorTypeName = resourceManager.GetString("FLS", clsReportParameters.Culture);
								break;
							case 1:
								totalDataRow.SensorTypeName = resourceManager.GetString("CAN", clsReportParameters.Culture);
								break;
						}
					}
				}
			}

			// Датасет для хранения констант с атрибутами предприятия.
			// Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
			ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
			// Добавление в датасет данных атрибутов предприятия.
			dsFuelCostDataSet.Merge(dsEstablishmentAttributes);

			return dsFuelCostDataSet;
		}
		private string GetReportHtml(DataSet dataSet, FuelCostReportParameters clsReportParameters)
		{
			var totalDataTable = dataSet.Tables["Total"];

			var totalFuelSpendByCanColumn = totalDataTable.Columns["TotalFuelSpendByCAN"];

			var totalFuelSpendByCanPresent =
				totalFuelSpendByCanColumn != null &&
				totalDataTable
					.Rows.Cast<DataRow>()
					.Any(r => !r.IsNull(totalFuelSpendByCanColumn));

			var stringBuilder = new StringBuilder();
			var @string = GetResourceStringContainer(clsReportParameters.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				// **************************
				// Сводный отчет. Заголовок.
				// **************************

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteStartElement("thead");

				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "header44");
				xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

				// Название
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["NameCaption"]);
				xw.WriteEndElement();

				// Тип датчика
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["TypeCaption"]);
				xw.WriteEndElement();

				// Уровень топлива в начале периода, л
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["FuelLevelStartCaption"]);
				xw.WriteEndElement();

				// Заправки
				xw.WriteStartElement("td");
				xw.WriteAttributeString("colspan", "2");
				xw.WriteString(@string["RefillsCaption"]);
				xw.WriteEndElement();

				// Сливы
				xw.WriteStartElement("td");
				xw.WriteAttributeString("colspan", "2");
				xw.WriteString(@string["DrainsCaption"]);
				xw.WriteEndElement();

				// Уровень топлива в конце периода, л
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["FuelLevelEndCaption"]);
				xw.WriteEndElement();

				// Общий пробег
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string[clsReportParameters.FuelSpendStandardType == FuelSpendStandardType.Run
					? "TotalRunCaption"
					: "TotalMachineHours"]
					);
				xw.WriteEndElement();

				// Средний расход по датчику, л/100км
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(
					@string[clsReportParameters.FuelSpendStandardType == FuelSpendStandardType.Run
								? "AvgSpentCaption"
								: "lblAvgSpendSensorMH"]);
				xw.WriteEndElement();

				// Расход топлива за период, л
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["SpentForPeriodCaption"]);
				xw.WriteEndElement();

				// Норматив расхода топлива, л/100км
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string[
					clsReportParameters.FuelSpendStandardType == FuelSpendStandardType.Run
					? "FuelSpentNormCaption"
					: "lblFuelSpendStandardMH"]);
				xw.WriteEndElement();

				// Расход по нормативу, л
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["FuelSpentFactCaption"]);
				xw.WriteEndElement();

				// Отклонение за период, л
				xw.WriteStartElement("td");
				xw.WriteAttributeString("rowspan", "2");
				xw.WriteString(@string["FuelSpentDeviationCaption"]);
				xw.WriteEndElement();

				//Расход по ДАРТ
				if (totalFuelSpendByCanPresent)
				{
					xw.WriteStartElement("td");
					xw.WriteAttributeString("title", @string["FuelSpentByCANDescription"]);
					xw.WriteAttributeString("rowspan", "2");
					xw.WriteString(@string["FuelSpentByCAN"]);
					xw.WriteEndElement();
				}

				// tr
				xw.WriteEndElement();

				// Вторая строка заголовка.
				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "header44");
				xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

				xw.WriteElementString("td", @string["CountCaption"]);
				xw.WriteElementString("td", @string["VolumeCaption"]);
				xw.WriteElementString("td", @string["CountCaption"]);
				xw.WriteElementString("td", @string["VolumeCaption"]);

				// tr
				xw.WriteEndElement();

				// thead
				xw.WriteEndElement();

				// **************************
				// Сводный отчет. Тело отчета.
				// **************************

				var vehicleDict = new Dictionary<int, string>();
				var sensorsDict = new Dictionary<int, int>();

				xw.WriteStartElement("tbody");

				foreach (DataRow dr in totalDataTable.Rows)
				{
					if (!vehicleDict.ContainsKey(Convert.ToInt32(dr["Vehicle_ID"])))
						vehicleDict.Add(Convert.ToInt32(dr["Vehicle_ID"]), dr["Vehicle"].ToString());

					if (!sensorsDict.ContainsKey(Convert.ToInt32(dr["Vehicle_ID"])))
						sensorsDict.Add(Convert.ToInt32(dr["Vehicle_ID"]), Convert.ToInt32(dr["SensorType"]));

					xw.WriteStartElement("tr");
					xw.WriteAttributeString("class", "header44");
					xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

					xw.WriteElementString("td", dr["Vehicle"].ToString());
					xw.WriteElementString("td", dr["SensorTypeName"].ToString());
					xw.WriteElementString("td", dr.IntegerToString("Start_level"));
					xw.WriteElementString("td", dr.IntegerToString("Refuel_count"));
					xw.WriteElementString("td", dr.IntegerToString("Refuel_amount"));
					xw.WriteElementString("td", dr.IntegerToString("Dump_count"));
					xw.WriteElementString("td", dr.IntegerToString("Dump_amount"));
					xw.WriteElementString("td", dr.IntegerToString("End_level"));

					if ((string)dataSet.Tables["ReportHeader"].Rows[0]["FuelSpendStandardType"] == "EngineHours")
					{
						xw.WriteElementString("td", dr.IntegerToString("MachineHours"));
						xw.WriteElementString("td", dr.IntegerToString("FuelPerMachineHour"));
						xw.WriteElementString("td", dr.IntegerToString("FuelSpend"));
						xw.WriteElementString("td", dr.IntegerToString("FuelSpendStandardMH"));
						xw.WriteElementString("td", dr.IntegerToString("FuelByStandardMH"));
						xw.WriteElementString("td", dr.IntegerToString("DiffFromStandardMH"));
					}
					else
					{
						xw.WriteElementString("td", dr.IntegerToString("Run"));
						xw.WriteElementString("td", dr.IntegerToString("FuelPerHundred"));
						xw.WriteElementString("td", dr.IntegerToString("FuelSpend"));
						xw.WriteElementString("td", dr.IntegerToString("FuelSpendStandard"));
						xw.WriteElementString("td", dr.IntegerToString("FuelByStandard"));
						xw.WriteElementString("td", dr.IntegerToString("DiffFromStandard"));
					}

					if (totalFuelSpendByCanPresent)
					{
						xw.WriteElementString("td", dr.IntegerToString(totalFuelSpendByCanColumn.ColumnName));
					}

					// tr
					xw.WriteEndElement();
				}

				// tbody
				xw.WriteEndElement();
				// table
				xw.WriteEndElement();

				var dataTable = dataSet.Tables["Data"];
				var refuels = dataTable.Rows.Cast<DataRow>().Count(dr => Convert.ToInt32(dr["Refuel"]) > 0);

				if (refuels > 0)
				{
					// Подпись таблицы по заправкам.
					xw.WriteElementString("br", string.Empty);
					xw.WriteString(@string["RefuelsInfo"]);

					// Заголовок таблицы по заправкам.
					xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
					xw.WriteAttributeString("class", "reportTable withBorders");

					xw.WriteStartElement("thead");

					xw.WriteStartElement("tr");
					xw.WriteAttributeString("class", "header44");
					xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

					xw.WriteElementString("td", @string["NumberCaption"]);
					xw.WriteElementString("td", @string["NameCaption"]);

					AddDateTimeHeaderCell(xw, @string);

					xw.WriteElementString("td", @string["VolumeSensorCaption"]);
					xw.WriteElementString("td", @string["VolumeBillCaption"]);
					xw.WriteElementString("td", @string["AddressCaption"]);
					xw.WriteElementString("td", @string["ViewOnMapCaption"]);

					//tr
					xw.WriteEndElement();
					//thead
					xw.WriteEndElement();

					// Тело таблицы по заправкам.
					xw.WriteStartElement("tbody");
					var counter = 0;
					foreach (var dr in dataTable.Rows.Cast<DataRow>().Where(dr => Convert.ToInt32(dr["Refuel"]) > 0))
					{
						counter++;
						xw.WriteStartElement("tr");
						xw.WriteAttributeString("class", "header44");
						xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

						xw.WriteElementString("td", counter.ToString(CultureInfo.InvariantCulture));
						xw.WriteElementString("td", vehicleDict[Convert.ToInt32(dr["Vehicle_ID"])]);

						AddDateTimeValueCells(dr, xw);

						xw.WriteElementString("td", dr.IntegerToString("Refuel"));

						// Ссылка на ввод объема топлива по чеку.
						var refuelByBill = dr["RefuelByBill"] == DBNull.Value
											   ? 0
											   : Convert.ToInt32(dr["RefuelByBill"]);

						xw.WriteStartElement("td");
						xw.WriteStartElement("a");
						xw.WriteAttributeString("class", "refuelByBill");
						xw.WriteAttributeString("href", "javascript:void()");
						xw.WriteAttributeString("vehicleId", dr["Vehicle_ID"].ToString());
						xw.WriteAttributeString("logTime", dr["LT_to"].ToString());
						xw.WriteAttributeString("sensor", sensorsDict[Convert.ToInt32(dr["Vehicle_ID"])].ToString(CultureInfo.InvariantCulture));
						xw.WriteAttributeString("value", refuelByBill != 0 ? refuelByBill.ToString(CultureInfo.InvariantCulture) : @string["notSet"]);

						xw.WriteString(refuelByBill != 0 ? refuelByBill.ToString(CultureInfo.InvariantCulture) : @string["notSet"]);
						// a
						xw.WriteEndElement();
						// td
						xw.WriteEndElement();

						xw.WriteElementString("td", dr["Address"].ToString());

						// Ссылка на путь
						var path =
							string.Format(
								"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=5"
								, clsReportParameters.ApplicationPath
								, Convert.ToInt32(dr["Vehicle_ID"])
								, TimeHelper.GetLocalTime((Convert.ToInt32(dr["LT_from"])), clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss")
								, TimeHelper.GetLocalTime((Convert.ToInt32(dr["LT_to"])), clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss"));

						xw.WriteStartElement("td");
						xw.WriteStartElement("a");
						xw.WriteAttributeString("href", path.Replace(' ', '+'));
						xw.WriteString(@string["ShowOnMap"]);
						xw.WriteEndElement();
						xw.WriteEndElement();

						//tr
						xw.WriteEndElement();
					}
					// tbody
					xw.WriteEndElement();
					// table
					xw.WriteEndElement();
				}

				var drains = dataTable.Rows.Cast<DataRow>().Count(dr => Convert.ToInt32(dr["Refuel"]) < 0);

				if (drains > 0)
				{
					// Подпись таблицы по сликам.
					xw.WriteElementString("br", string.Empty);
					xw.WriteString(@string["DumpsInfo"]);

					// Заголовок таблицы по сликам.
					xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
					xw.WriteAttributeString("class", "reportTable withBorders");

					xw.WriteStartElement("thead");

					xw.WriteStartElement("tr");
					xw.WriteAttributeString("class", "header44");
					xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

					xw.WriteElementString("td", @string["NumberCaption"]);
					xw.WriteElementString("td", @string["NameCaption"]);

					AddDateTimeHeaderCell(xw, @string);

					xw.WriteElementString("td", @string["VolumeSensorCaption"]);
					xw.WriteElementString("td", @string["AddressCaption"]);
					xw.WriteElementString("td", @string["ViewOnMapCaption"]);

					//tr
					xw.WriteEndElement();
					//thead
					xw.WriteEndElement();

					// Тело таблицы по сливам.
					xw.WriteStartElement("tbody");
					var counter = 0;
					foreach (var dr in dataTable.Rows.Cast<DataRow>().Where(dr => Convert.ToInt32(dr["Refuel"]) < 0))
					{
						counter++;
						xw.WriteStartElement("tr");
						xw.WriteAttributeString("class", "header44");
						xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

						xw.WriteElementString("td", counter.ToString(CultureInfo.InvariantCulture));
						xw.WriteElementString("td", vehicleDict[Convert.ToInt32(dr["Vehicle_ID"])]);
						AddDateTimeValueCells(dr, xw);
						xw.WriteElementString("td", (Convert.ToInt32(dr["Refuel"]) * -1).ToString(CultureInfo.InvariantCulture));

						xw.WriteElementString("td", dr["Address"].ToString());

						// Ссылка на путь
						var path =
							string.Format(
								"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=5"
								, clsReportParameters.ApplicationPath
								, Convert.ToInt32(dr["Vehicle_ID"])
								, TimeHelper.GetLocalTime((Convert.ToInt32(dr["LT_from"])), clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss")
								, TimeHelper.GetLocalTime((Convert.ToInt32(dr["LT_to"])), clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss"));

						xw.WriteStartElement("td");
						xw.WriteStartElement("a");
						xw.WriteAttributeString("href", path.Replace(' ', '+'));
						xw.WriteString(@string["ShowOnMap"]);
						xw.WriteEndElement();
						xw.WriteEndElement();

						//tr
						xw.WriteEndElement();
					}
					// tbody
					xw.WriteEndElement();
					// table
					xw.WriteEndElement();
				}
			}

			return stringBuilder.ToString();
		}
		private static void AddDateTimeValueCells(DataRow dr, XmlTextWriter xw)
		{
			var dateTime = (DateTime)dr["DateTo"];
			xw.WriteStartElement("td");
			xw.WriteAttributeString("style", "text-align:right;padding-right:0.5em;border-right:none");
			xw.WriteString(dateTime.Date.ToString(TimeHelper.DefaultTimeFormatUpToDays));
			xw.WriteEndElement();
			xw.WriteStartElement("td");
			xw.WriteAttributeString("style", "text-align:left;border-left:none");
			xw.WriteString(dateTime.TimeOfDay.ToString());
			xw.WriteEndElement();
		}
		private static void AddDateTimeHeaderCell(XmlTextWriter xw, ResourceStringContainer @string)
		{
			xw.WriteStartElement("td");
			xw.WriteAttributeString("colspan", "2");
			xw.WriteString(@string["DateTimeCaption"]);
			xw.WriteEndElement();
		}
		private DataSet GetReportData(FuelCostReportParameters reportParams)
		{
			var vehicleGroupID = reportParams.VehicleGroupProp != null
				? reportParams.VehicleGroupProp.Tag as int?
				: null;

			return mclsInstance.GetFuelReportData(
				reportParams.OperatorID,
				vehicleGroupID == null ? reportParams.VehicleID : (int?)null,
				vehicleGroupID,
				reportParams.DateFromInt,
				reportParams.DateToInt,
				reportParams.SensorType != null && reportParams.SensorType.list != null
					? reportParams.SensorType
						.list
						.ToArray()
						.Select(o =>
						{
							var i = (int)o;
							switch (i)
							{
								case 0:
									return SensorLegend.Fuel;
								case 1:
									return SensorLegend.CANFuel;
								default:
									throw new NotSupportedException($@"Sensor {i} is not supported");
							}
						})
						.ToArray()
					: null,
				reportParams.FuelSpendStandardType,
				reportParams.Culture);
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var typedData = GetReportTypedDataSet(reportParameters);
			return GetReportHtml(typedData, (FuelCostReportParameters)reportParameters);
		}
		public override object GetReportAsDto(ReportParameters reportParameters)
		{
			var dataSet = GetReportTypedDataSet(reportParameters);
			return ConvertToDTO(dataSet);
		}
		private FuelCostTssReportDTO ConvertToDTO(FuelCostDataSet dsFuelCostDataSet)
		{
			var dataTable = dsFuelCostDataSet.Tables[DataTableName];
			var dataRows = new List<FuelCostTssReportDTO.FuelCostReportRowData>();
			foreach (FuelCostDataSet.DataRow row in dataTable.Rows)
			{
				var dataRow = new FuelCostTssReportDTO.FuelCostReportRowData
				{
					Address      = row.IsAddressNull() ? string.Empty : row.Address,
					DateFrom     = row.IsDateFromNull() ? (DateTime?)null : row.DateFrom,
					DateTo       = row.IsDateToNull() ? (DateTime?)null : row.DateTo,
					F_next       = row.IsF_nextNull() ? (float?)null : row.F_next,
					F_prev       = row.IsF_prevNull() ? (float?)null : row.F_prev,
					LT_From      = row.IsLT_fromNull() ? (int?)null : row.LT_from,
					LT_To        = row.IsLT_toNull() ? (int?)null : row.LT_to,
					Lat          = row.IsLatNull() ? (decimal?)null : row.Lat,
					Lng          = row.IsLngNull() ? (decimal?)null : row.Lng,
					Refuel       = row.IsRefuelNull() ? (float?)null : row.Refuel,
					RefuelByBill = row.IsRefuelByBillNull() ? (decimal?)null : row.RefuelByBill,
					StopTime     = row.IsStopTimeNull() ? (int?)null : row.StopTime,
					Vehicle_ID   = row.Vehicle_ID,
				};
				dataRows.Add(dataRow);
			}

			var totalTable = dsFuelCostDataSet.Tables[TotalTableName];
			var totalRows = new List<FuelCostTssReportDTO.FuelCostReportTotalRow>();
			foreach (FuelCostDataSet.TotalRow row in totalTable.Rows)
			{
				var totalRow = new FuelCostTssReportDTO.FuelCostReportTotalRow
				{
					DiffFromStandard = row.IsDiffFromStandardNull() ? (float?)null : row.DiffFromStandard,
					DiffFromStandardMH = row.IsDiffFromStandardMHNull() ? (decimal?)null : row.DiffFromStandardMH,
					Dump_amount = row.IsDump_amountNull() ? (double?)null : row.Dump_amount,
					Dump_count = row.IsDump_countNull() ? (int?)null : row.Dump_count,
					End_level = row.IsEnd_levelNull() ? (float?)null : row.End_level,
					FuelByStandard = row.IsFuelByStandardNull() ? (float?)null : row.FuelByStandard,
					FuelByStandardMH = row.IsFuelByStandardMHNull() ? (decimal?)null : row.FuelByStandardMH,
					FuelPerHundred = row.IsFuelPerHundredNull() ? (float?)null : row.FuelPerHundred,
					FuelPerMachineHour = row.IsFuelPerMachineHourNull() ? (decimal?)null : row.FuelPerMachineHour,
					FuelSpend = row.IsFuelSpendNull() ? (float?)null : row.FuelSpend,
					FuelSpendStandard = row.IsFuelSpendStandardNull() ? (float?)null : row.FuelSpendStandard,
					FuelSpendStandardMH = row.IsFuelByStandardMHNull() ? (decimal?)null : row.FuelSpendStandardMH,
					MachineHours = row.IsMachineHoursNull() ? (decimal?)null : row.MachineHours,
					Refuel_amount = row.IsRefuel_amountNull() ? (double?)null : row.Refuel_amount,
					Refuel_count = row.IsRefuel_countNull() ? (int?)null : row.Refuel_count,
					Run = row.IsRunNull() ? (long?)null : row.Run,
					SensorType = row.IsSensorTypeNull() ? (int?)null : row.SensorType,
					SensorTypeName = row.IsSensorTypeNameNull() ? string.Empty : row.SensorTypeName,
					Start_level = row.IsStart_levelNull() ? (float?)null : row.Start_level,
					Vehicle = row.IsVehicleNull() ? string.Empty : row.Vehicle,
					Vehicle_Kind_ID = row.IsVehicle_Kind_IDNull() ? (int?)null : row.Vehicle_Kind_ID,
					Vehicle_ID = row.Vehicle_ID,
				};
				totalRows.Add(totalRow);
			}

			var headersTable = dsFuelCostDataSet.Tables[HeadersTableName];
			var headerRows = new List<FuelCostTssReportDTO.FuelCostReportHeaderData>();
			foreach (FuelCostDataSet.ReportHeaderRow row in headersTable.Rows)
			{
				var headerRow = new FuelCostTssReportDTO.FuelCostReportHeaderData
				{
					FuelSpendStandardType = row.IsFuelSpendStandardTypeNull() ? string.Empty : row.FuelSpendStandardType,
					Vehicle = row.IsVehicleNull() ? string.Empty : row.Vehicle,
					DtFrom = row.IsdtFromNull() ? (DateTime?)null : row.dtFrom,
					DtTo = row.IsdtToNull() ? (DateTime?)null : row.dtTo,
					TxtDumpsInfo = row.IstxtDumpsInfoNull() ? string.Empty : row.txtDumpsInfo,
					TxtRefuelsInfo = row.IstxtRefuelsInfoNull() ? string.Empty : row.txtRefuelsInfo,
				};
				headerRows.Add(headerRow);
			}

			return new FuelCostTssReportDTO
			{
				Rows = dataRows,
				Totals = totalRows,
				Headers = headerRows
			};
		}

		/// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			FuelCostDataSet dataSet = null;
			try
			{
				var reportParameters = (FuelCostReportParameters)iReportParameters;
				var xmlCrystalReport = new XmlCrystalReport(reportParameters);
				dataSet = GetReportTypedDataSet(iReportParameters);
				var dto = ConvertToDTO(dataSet);
				xmlCrystalReport.SetDataSet(dataSet, dto);
				return xmlCrystalReport;
			}
			finally
			{
				if (dataSet != null)
					dataSet.Dispose();
			}
		}
	}

	#endregion FuelCostTSSReport
}