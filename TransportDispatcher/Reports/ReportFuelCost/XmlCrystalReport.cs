﻿using System;
using System.IO;
using System.Runtime.Serialization;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class XmlCrystalReport : IReportClass
	{
		private FuelCostDataSet _dsFuelCostDataSet;

		private FuelCostTssReportDTO _dto;

		private readonly FuelCostReportParameters _clsReportParameters;

		public XmlCrystalReport(FuelCostReportParameters clsReportParameters)
		{
			_clsReportParameters = clsReportParameters;
		}

		public void Dispose()
		{
		}

		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Xml:
					ExportToXml(fileName);
					break;
				default:
					ExportToCrystal(formatType, fileName);
					break;
			}
		}

		public void SetDataSet(FuelCostDataSet dataSet, FuelCostTssReportDTO dto)
		{
			_dsFuelCostDataSet = dataSet;
			_dto = dto;
		}

		private void ExportToXml(string fileName)
		{
			var serializer = new DataContractSerializer(typeof(FuelCostTssReportDTO));
			using (var filestream = File.Open(fileName, FileMode.Create))
			{
				serializer.WriteObject(filestream, _dto);
			}
		}

		private void ExportToCrystal(ReportTypeEnum formatType, string fileName)
		{
			var crystallReport = CreateCrystalReport();
			crystallReport.ExportToDisk(formatType, fileName);
		}

		private IReportClass CreateCrystalReport()
		{
			ReportClassWrapper rptResultReport = null;
			var processed = false;
			try
			{
				//Проставляем ReportHeader_ID, чтобы Crystal Report корректно выбирал корень данных, из которого начинать вести join
				foreach (FuelCostDataSet.ReportHeaderRow row in _dsFuelCostDataSet.ReportHeader.Rows)
					row.ReportHeader_ID = 1;

				foreach (FuelCostDataSet.ConstantsListRow row in _dsFuelCostDataSet.ConstantsList.Rows)
					row.ReportHeader_ID = 1;

				foreach (FuelCostDataSet.TotalRow row in _dsFuelCostDataSet.Total.Rows)
					row.ReportHeader_ID = 1;

				//Создание экземпляра отчета.
				rptResultReport = CreateApproriateReportClass(
					_clsReportParameters.IsReportOnGroup,
					_clsReportParameters.FuelSpendStandardType);

				//Локализация надписей отчета
				if (_clsReportParameters != null)
					TssReportBase.TranslateReport<FuelCostTSSReport>(rptResultReport, _clsReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(_dsFuelCostDataSet);
				// Горизонтальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation =
					PaperOrientation.Landscape;
				processed = true;
				//Функция возвращает сформированный экземпляр отчета.
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();
			}
		}

		private static ReportClassWrapper CreateApproriateReportClass(bool isGroupReport, FuelSpendStandardType fuelSpendStandardType)
		{
			if (isGroupReport)
			{
				switch (fuelSpendStandardType)
				{
					case FuelSpendStandardType.EngineHours:
						return new GroupFuelReportMH();
					case FuelSpendStandardType.Run:
						return new GroupFuelReport();
					default:
						throw new NotSupportedException("Fuel spend standard type " + fuelSpendStandardType + " is not supported for group report");
				}
			}

			switch (fuelSpendStandardType)
			{
				case FuelSpendStandardType.EngineHours:
					return new FuelCostReportMH();
				case FuelSpendStandardType.Run:
					return new FuelCostReport();
				default:
					throw new NotSupportedException("Fuel spend standard type " + fuelSpendStandardType + " is not supported");
			}
		}
	}
}