using System;
using System.Collections;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Editors;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> ��������� ��� ������� ������ </summary>
	[Serializable]
	public class FuelCostReportParameters : ReportParameters, IMonitoreeObjectContainer
	{
		/// <summary>
		/// ��. ������� �����������
		/// </summary>
		protected int vehicleID = -1;

		/// <summary>
		/// ������������� ��.
		/// </summary>
		[DisplayName("TSNumberDN", 8, true), Category("ReportParsCat"), Description("TSNumberDesc")]
		public int VehicleID
		{
			get
			{
				return vehicleID;
			}
			set
			{
				vehicleID = value;
			}
		}

		[Browsable(false)]
		public int OperatorID
		{
			get;
			set;
		}

		/// <summary>
		/// ������ ��, ��� ������� ����������� �����.
		/// </summary>
		[DisplayName("TSGroupDN", 6, true),
		Category("ReportParsCat"),
		Description("TSGroupDesc")]
		public TagListBoxItem VehicleGroupProp
		{
			get;
			set;
		}

		[Browsable(false)]
		public bool IsReportOnGroup
		{
			get
			{
				return VehicleGroupProp != null &&
					   VehicleGroupProp.Tag is int;
			}
		}

		public PropValue SensorType
		{
			get;
			set;
		}

		[DisplayName("SensorTypeValue"), Type(typeof(string)), ControlType(ReportParametersUtils.FuelSensorTypePicker, "FuelSensorTypePicker"), Order(2)]
		public string SensorTypeValue
		{
			get { return SensorType.Text; }
			set
			{
				var arrayList = new ArrayList();
				var stvalues = value.Split(';');
				foreach (var stvalue in stvalues)
					arrayList.Add(Convert.ToInt32(stvalue));
				SensorType = new PropValue(arrayList, string.Empty);
			}
		}

		[DisplayName("FuelSpendStandardType"), Type(typeof(FuelSpendStandardType)), ControlType(ReportParametersUtils.FuelSpendStandardTypePicker, "FuelSpendStandardTypePicker"), Order(3)]
		public FuelSpendStandardType FuelSpendStandardType { get; set; }

		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupPicker"), Order(1)]
		public int VehicleGroupId
		{
			get
			{
				if (VehicleGroupProp == null)
					return 0;

				return (VehicleGroupProp.Tag as int?) ?? 0;
			}
			set
			{
				if (VehicleGroupProp == null)
					VehicleGroupProp = new TagListBoxItem();
				VehicleGroupProp.Tag = value;
			}
		}

		[Browsable(false)]
		public string departmentCountryName { get; set; }

		#region IMonitoreeObjectContainer Members

		BusinessLogic.Server.IdType IMonitoreeObjectContainer.MonitoreeObjectIdType
		{
			get { return IsReportOnGroup ? IdType.VehicleGroup : IdType.Vehicle; }
		}

		int IMonitoreeObjectContainer.MonitoreeObjectId
		{
			get { return IsReportOnGroup ? (int)VehicleGroupProp.Tag : VehicleID; }
		}

		#endregion

		[DisplayName("DateTimeInterval"), Type(typeof(DateTimeInterval))]
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod"), Options("Accuracy:0"), Order(255)]
		public override DateTimeInterval DateTimeInterval
		{
			get
			{
				return base.DateTimeInterval;
			}
			set
			{
				base.DateTimeInterval = value;
			}
		}
	}
}