﻿using System.Globalization;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Guid("D16C0E54-8878-4B7D-812F-C0485ED40EC5")]
	public class FuelCostSingleTSSReport : FuelCostTSSReport
	{
		public override int Order
		{
			get
			{
				return 3;
			}
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostReport"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostReportDescription"];
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new FuelCostSingleReportParameters { OperatorID = settings.OperatorId };
		}
	}
}