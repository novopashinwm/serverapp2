﻿using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Параметры для данного отчета </summary>
	[Serializable]
	public class MoveReportParameters : ReportParameters, IMonitoreeObjectContainer
	{
		/// <summary> Показывать/не показывать адрес в отчёте </summary>
		protected bool showAddress = false;

		/// <summary> GUID карты </summary>
		[Obsolete("Do not use, dedicated for compatibility for old versions of report parameters")]
		protected string mapGuid = Guid.Empty.ToString();

		/// <summary> Для хранения значения флага, определяющего что следует подставлять текущую дату. Если он равен true, то вводить дату не требуется, будет всегда подставляться текущая </summary>
		protected bool mblnAlwaysCurrentDate = false;

		/// <summary> Время начала периода, на который формируется отчет </summary>
		protected string mstrReportTimeFrom = DateTime.Now.ToShortTimeString();

		/// <summary> Время окончания периода, на который формируется отчет </summary>
		protected string mstrReportTimeTo = DateTime.Now.ToShortTimeString();

		/// <summary> Гаражный номер ТС </summary>
		protected TagListBoxItem vehicleProp = new TagListBoxItem("", null);

		//protected FORIS.TSS.TransportDispatcher.TagComboBoxItem tcbiVehicleNumber = new TagComboBoxItem("", null);

		protected int count = 1000;
		protected int interval = 60;
		protected int parkingInterval = 60 * 5;

		// Набор данных для Editor-ов
		[NonSerialized]
		private DataTable _dsParamsForEditor;
		public DataTable DsParamsForEditor
		{
			get { return _dsParamsForEditor; }
			set { _dsParamsForEditor = value; }
		}

		/// <summary> Показывать/не показывать адрес в отчёте </summary>
		[DisplayName("ShowAddress", 9, true), Category("ReportParsCat"), Description("ShowAddressDesc")]
		public bool ShowAddress
		{
			get { return showAddress; }
			set { showAddress = value; }
		}

		/// <summary> Идентификатор ТС </summary>
		[DisplayName("TSNumberDN", 9, true), Category("ReportParsCat"), Description("TSNumberDesc")]
		[Browsable(true)]
		public TagListBoxItem VehicleProp
		{
			get { return vehicleProp; }
			set { vehicleProp = value; }
		}

		/// <summary> Флаг, определяющий, что следует подставлять текущую дату. Если он равен true, то вводить дату не требуется, будет всегда подставляться текущая </summary>
		[DisplayName("InsertCurrDateDN"), Category("ReportParsCat"), Description("InsertCurrDateDesc")]
		[Browsable(false)]
		public bool AlwaysForCurrentDateProp
		{
			get { return mblnAlwaysCurrentDate; }
			set { mblnAlwaysCurrentDate = value; }
		}

		/// <summary> Дата начала периода, на который формируется отчет (если отчет формируется за период) </summary>
		[DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
		[Browsable(true)]
		public override DateTime DateFrom
		{
			get { return dtDateFrom.Date; }
			set { dtDateFrom = value.Date; }
		}

		/// <summary> Время начала периода, на который формируется доклад </summary>
		[DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
		public string ReportTimeFromProp
		{
			get { return mstrReportTimeFrom; }
			set
			{
				//Вызов функции проверки введенного пользователем значения времени.
				mstrReportTimeFrom = TimeInputCheck(value);
			}
		}

		/// <summary> Дата окончания периода, на который формируется отчет (если отчет формируется за период) </summary>
		[DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
		[Browsable(true)]
		public override DateTime DateTo
		{
			get { return dtDateTo.Date; }
			set { dtDateTo = value.Date; }
		}

		/// <summary> Время окончания периода, на который формируется доклад </summary>
		[DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
		public string ReportTimeToProp
		{
			get { return mstrReportTimeTo; }
			set
			{
				//Вызов функции проверки введенного пользователем значения времени.
				mstrReportTimeTo = TimeInputCheck(value);
			}
		}

		/// <summary> Количество позиций </summary>
		[DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
		public int PositionsСount
		{
			get { return count; }
			set { count = value; }
		}

		/// <summary> Интервал </summary>
		[DisplayName("Interval")]
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.IntervalPicker, "IntervalPicker")]
		[Order(3)]
		public int Interval
		{
			get { return interval; }
			set { interval = value; }
		}

		/// <summary> Интервал </summary>
		[DisplayName("ParkingIntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc"), Order(2)]
		public int ParkingInterval
		{
			get { return parkingInterval; }
			set { parkingInterval = value; }
		}

		[DisplayName("VehicleId")]
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker")]
		[Order(1)]
		[Options(SystemRight.PathAccess)]
		public int VehicleId
		{
			get { return (int)vehicleProp.Tag; }
			set { vehicleProp.Tag = value; }
		}

		[DisplayName("DateTimeInterval")]
		[Type(typeof(DateTimeInterval))]
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod")]
		[Order(255)]
		[Options("Accuracy:0")]
		public override DateTimeInterval DateTimeInterval
		{
			get { return base.DateTimeInterval; }
			set { base.DateTimeInterval = value; }
		}

		private WorkingHoursInterval _workingHoursInterval;

		[DisplayName("WorkingHoursInterval")]
		[Type(typeof(WorkingHoursInterval))]
		[ControlType(ReportParametersUtils.WorkingHoursInterval, "WorkingHoursInterval")]
		[Order(256)]
		public WorkingHoursInterval WorkingHoursInterval
		{
			get { return _workingHoursInterval; }
			set { _workingHoursInterval = value; }
		}

		public IdType MonitoreeObjectIdType { get { return IdType.Vehicle; } }
		public int MonitoreeObjectId { get { return VehicleId; } }
	}
}