﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic.Enums;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class WebGisMoveHistoryExcelPdfReport : IReportClass
	{
		private DataSet _ds;
		private Font _fontNorm;
		private Font _fontBold;

		public WebGisMoveHistoryExcelPdfReport()
		{
			_ds       = new DataSet();
			_fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			_fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);
		}
		public void SetDataSource(DataSet dataSet)
		{
			_ds = dataSet;
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					ExportReportAsExcel(fileName);
					break;
				case ReportTypeEnum.Acrobat:
					ExportReportAsAcrobat(fileName);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(formatType));
			}
			// PDF Export
		}
		private void ExportReportAsExcel(string fileName)
		{
			var stringReport = WorkbookEngine.CreateWorkbook(_ds, "WebGisMoveHistory.xsl");
			var encoding = new UTF8Encoding(false);
			using (TextWriter writer = new StreamWriter(fileName, false, encoding))
			{
				writer.Write(stringReport);
			}
		}
		private void ExportReportAsAcrobat(string fileName)
		{
			var document = new Document(PageSize.A4);
			var pdfWriter = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
			pdfWriter.PageEvent = new PdfReportPageEvents();

			var ht = _ds.Tables["Legend"];
			var reportName = ht.Rows.Cast<DataRow>().FirstOrDefault()["reportLegendValue"].ToString();

			document.AddTitle(reportName); // необходимо до открытия документа
			document.Open();
			document.NewPage();

			// Заголовок таблицы.
			var commonTable = new PdfPTable(2) { WidthPercentage = 50f, HorizontalAlignment = Element.ALIGN_LEFT };

			foreach (DataRow row in ht.Rows)
			{
				commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendKey"].ToString(), _fontBold)));
				commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendValue"].ToString(), _fontNorm)));
			}

			document.Add(commonTable);
			document.Add(new Paragraph(new Phrase(" ")));

			// Таблица с данными.
			var dt = _ds.Tables["MoveHistory"];

			var pTable = new PdfPTable(5);
			pTable.DefaultCell.Border = Element.RECTANGLE;
			pTable.HeaderRows = 1;

			// Заголовок для таблицы с данными.
			var cCells = new PdfPCell[5];
			var cRow = new PdfPRow(cCells);

			// Ряд для заголовков колонок с данными.
			var captionsRow = _ds.Tables["MoveHistoryCaptions"].Rows[0];

			cCells[0] = new PdfPCell(new Phrase(captionsRow["Log_Time"].ToString(),      _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };
			cCells[1] = new PdfPCell(new Phrase(captionsRow["Speed"].ToString(),         _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };
			cCells[2] = new PdfPCell(new Phrase(captionsRow["Dist"].ToString(),          _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };
			cCells[3] = new PdfPCell(new Phrase(captionsRow["TotalDistance"].ToString(), _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };
			cCells[4] = new PdfPCell(new Phrase(captionsRow["Address"].ToString(),       _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };

			pTable.Rows.Add(cRow);

			// Данные.
			foreach (DataRow row in dt.Rows)
			{
				var pCells = new PdfPCell[5];
				var pRow = new PdfPRow(pCells);

				pCells[0] = new PdfPCell(new Phrase(row["Log_Time"].ToString(),    _fontNorm));
				pCells[1] = new PdfPCell(new Phrase(row["Speed"].ToString(),       _fontNorm)) { HorizontalAlignment = Element.ALIGN_RIGHT };
				pCells[2] = new PdfPCell(new Phrase(row["Dist"].ToString(),        _fontNorm)) { HorizontalAlignment = Element.ALIGN_RIGHT };
				pCells[3] = new PdfPCell(new Phrase(row["Temperature"].ToString(), _fontNorm)) { HorizontalAlignment = Element.ALIGN_RIGHT };
				pCells[4] = new PdfPCell(new Phrase(row["Address"].ToString(),     _fontNorm));

				pTable.Rows.Add(pRow);
			}

			// Ширина таблицы.
			pTable.WidthPercentage = 100f;
			pTable.SetTotalWidth(new[] {8f, 5f, 5f, 5f, 20f});

			document.Add(pTable);

			document.Add(new Paragraph(new Phrase(" ")));

			var totalRunCaption = _ds.Tables["Total"].Rows[0]["Caption"];
			var totalRunValue = _ds.Tables["Total"].Rows[0]["Run"];

			document.Add(new Paragraph(new Phrase(string.Format("{0}: {1}", totalRunCaption, totalRunValue), _fontNorm)));

			document.Close();
			return;
		}
		public ExportOptions ExportOptions
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}
		public ReportDefinition ReportDefinition
		{
			get { throw new NotImplementedException(); }
		}
		public void Dispose()
		{
		}
	}
}