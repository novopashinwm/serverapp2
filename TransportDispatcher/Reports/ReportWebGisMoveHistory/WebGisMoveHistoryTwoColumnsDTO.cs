﻿using System;
using System.Collections.Generic;
using System.Data;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class WebGisMoveHistoryTwoColumnsDTO
	{
		[Serializable]
		public class ReportHeader
		{
			/// <summary> Название ТС </summary>
			public string ObjectName;
			/// <summary> Дата начала интервала отчета в выбранной тайм зоне </summary>
			public DateTime DateFrom;
			/// <summary> Дата конца интервала отчета в выбранной тайм зоне </summary>
			public DateTime DateTo;
			/// <summary> Выбранное время стоянки </summary>
			public TimeSpan ParkingInterval;
			/// <summary> Всего </summary>
			public decimal TotalRun;
			/// <summary> Всего времени, проведенного в движении </summary>
			public TimeSpan TotalTime;
			/// <summary> Всего времени, проведенного в стоянке </summary>
			public TimeSpan TotalParkingTime;
			/// <summary> Максимальная скорость </summary>
			public byte? MaxSpeed;
		}
		[Serializable]
		public class ReportRowData
		{
			/// <summary> Дата начала интервала записи в выбранной тайм зоне </summary>
			public DateTime DateFrom;
			/// <summary> Дата конца интервала записи в выбранной тайм зоне </summary>
			public DateTime DateTo;
			/// <summary> Адрес начальной точки </summary>
			public string AddressFrom;
			/// <summary> Адрес конечной точки </summary>
			public string AddressTo;
			/// <summary> Пройденное расстояние </summary>
			public decimal Distance;
			/// <summary> Средняя скорость </summary>
			public int AverageSpeed;
			/// <summary> Времени в пути </summary>
			public TimeSpan TotalTime;
			/// <summary> Времени в стоянке </summary>
			public TimeSpan TotalParking;
		}
		public ReportHeader Header;
		public List<ReportRowData> Rows;
		public DataSet ToDataset(ResourceStringContainer resourceStringContainer)
		{
			var dataSet = new DataSet("Report");
			var reportHeader = new DataTable(WebGisMoveHistoryTwoColumns.ReportHeaderTableName);
			dataSet.Tables.Add(reportHeader);
			var reportHeaderColumns = new[]
			{
				new DataColumn(WebGisMoveHistoryTwoColumns.ReportLegendKey),
				new DataColumn(WebGisMoveHistoryTwoColumns.ReportLegendValue)
			};
			reportHeader.Columns.AddRange(reportHeaderColumns);

			var @string = resourceStringContainer;
			var reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2NameCaption];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = string.Format("{0}", @string["MotionIntervals"]);

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Capobjectname];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = Header.ObjectName;

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Capdatefrom];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = Header.DateFrom.ToString("dd.MM.yyyy HH:mm:ss");

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Capdateto];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = Header.DateTo.ToString("dd.MM.yyyy HH:mm:ss");

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Capparkinginterval];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = TimeHelperEx.GetTimeSpanReadable(Header.ParkingInterval, @string.Culture);

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Captotalrun];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = string.Format("{0:0.###} {1}", Header.TotalRun, @string["report2km"]);

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Captotaltime];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = TimeHelperEx.GetTimeSpanReadable(Header.TotalTime, @string.Culture);

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Captotalparkingtime];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = TimeHelperEx.GetTimeSpanReadable(Header.TotalParkingTime, @string.Culture);

			reportHeader.Rows.Add(reportHeaderCaptionRow);
			reportHeaderCaptionRow = reportHeader.NewRow();

			reportHeaderCaptionRow[reportHeaderColumns[0]] = @string[WebGisMoveHistoryTwoColumns.Report2Capmaxspeed];
			reportHeaderCaptionRow[reportHeaderColumns[1]] = string.Format("{0} {1}", Header.MaxSpeed, @string["report2kmh"]);

			reportHeader.Rows.Add(reportHeaderCaptionRow);

			var reportBodyHeader = new DataTable(WebGisMoveHistoryTwoColumns.BodyHeaderTableName);
			dataSet.Tables.Add(reportBodyHeader);

			var reportBodyColumnsHeaders = new[]
			{
				new DataColumn(WebGisMoveHistoryTwoColumns.SDateFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.STimeFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAddressFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.SDateTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.STimeTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAddressTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.SDistance),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAverageSpeed),
				new DataColumn(WebGisMoveHistoryTwoColumns.STotalTime),
				new DataColumn(WebGisMoveHistoryTwoColumns.STotalParking),
				new DataColumn(WebGisMoveHistoryTwoColumns.SStart),
				new DataColumn(WebGisMoveHistoryTwoColumns.SEnd)
			};

			reportBodyHeader.Columns.AddRange(reportBodyColumnsHeaders);

			var reportBodyHeaderRow = reportBodyHeader.NewRow();
			reportBodyHeaderRow[reportBodyColumnsHeaders[0]] = @string[WebGisMoveHistoryTwoColumns.SDateFrom];
			reportBodyHeaderRow[reportBodyColumnsHeaders[1]] = @string[WebGisMoveHistoryTwoColumns.STimeFrom];
			reportBodyHeaderRow[reportBodyColumnsHeaders[2]] = @string[WebGisMoveHistoryTwoColumns.SAddressFrom];
			reportBodyHeaderRow[reportBodyColumnsHeaders[3]] = @string[WebGisMoveHistoryTwoColumns.SDateTo];
			reportBodyHeaderRow[reportBodyColumnsHeaders[4]] = @string[WebGisMoveHistoryTwoColumns.STimeTo];
			reportBodyHeaderRow[reportBodyColumnsHeaders[5]] = @string[WebGisMoveHistoryTwoColumns.SAddressTo];
			reportBodyHeaderRow[reportBodyColumnsHeaders[6]] = @string[WebGisMoveHistoryTwoColumns.SDistance];
			reportBodyHeaderRow[reportBodyColumnsHeaders[7]] = @string[WebGisMoveHistoryTwoColumns.SAverageSpeed];
			reportBodyHeaderRow[reportBodyColumnsHeaders[8]] = @string[WebGisMoveHistoryTwoColumns.STotalTime];
			reportBodyHeaderRow[reportBodyColumnsHeaders[9]] = @string[WebGisMoveHistoryTwoColumns.STotalParking];
			reportBodyHeaderRow[reportBodyColumnsHeaders[10]] = @string[WebGisMoveHistoryTwoColumns.SStart];
			reportBodyHeaderRow[reportBodyColumnsHeaders[11]] = @string[WebGisMoveHistoryTwoColumns.SEnd];

			reportBodyHeader.Rows.Add(reportBodyHeaderRow);
			var reportBody = new DataTable(WebGisMoveHistoryTwoColumns.BodyTableName);
			dataSet.Tables.Add(reportBody);
			var reportBodyColumns = new[]
			{
				new DataColumn(WebGisMoveHistoryTwoColumns.SDateFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.STimeFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAddressFrom),
				new DataColumn(WebGisMoveHistoryTwoColumns.SDateTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.STimeTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAddressTo),
				new DataColumn(WebGisMoveHistoryTwoColumns.SDistance),
				new DataColumn(WebGisMoveHistoryTwoColumns.SAverageSpeed),
				new DataColumn(WebGisMoveHistoryTwoColumns.STotalTime),
				new DataColumn(WebGisMoveHistoryTwoColumns.STotalParking)
			};

			reportBody.Columns.AddRange(reportBodyColumns);

			foreach (var row in Rows)
			{
				var reportBodyRow = reportBody.NewRow();

				reportBodyRow[WebGisMoveHistoryTwoColumns.SDateFrom]     = row.DateFrom.ToString("dd.MM.yyyy");
				reportBodyRow[WebGisMoveHistoryTwoColumns.STimeFrom]     = row.DateFrom.ToString("HH:mm:ss");
				reportBodyRow[WebGisMoveHistoryTwoColumns.SDateTo]       = row.DateTo.ToString("dd.MM.yyyy");
				reportBodyRow[WebGisMoveHistoryTwoColumns.STimeTo]       = row.DateTo.ToString("HH:mm:ss");
				reportBodyRow[WebGisMoveHistoryTwoColumns.SAddressFrom]  = row.AddressFrom;
				reportBodyRow[WebGisMoveHistoryTwoColumns.SAddressTo]    = row.AddressTo;
				reportBodyRow[WebGisMoveHistoryTwoColumns.SDistance]     = row.Distance.ToString("0.###");
				reportBodyRow[WebGisMoveHistoryTwoColumns.SAverageSpeed] = row.AverageSpeed.ToString(@string.Culture);
				reportBodyRow[WebGisMoveHistoryTwoColumns.STotalTime]    = row.TotalTime.ToString();
				reportBodyRow[WebGisMoveHistoryTwoColumns.STotalParking] = row.TotalParking.ToString();

				reportBody.Rows.Add(reportBodyRow);
			}

			return dataSet;
		}
	}
}