﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт: "История движения" </summary>
	[Guid("F824B9A6-F435-4940-9788-1C46495BB73F")]
	public class WebGisMoveHistory : TssReportBase
	{
		/// <summary> Текстовое описание данного отчета </summary>
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["WebGisMoveHistory"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["WebGisMoveHistoryDescription"];
		}
		/// <summary> Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета </summary>
		/// <param name="settings"></param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var moveReportParameters = new MoveReportParameters { OperatorId = settings.OperatorId };

			//Возвращение параметров отчета.
			return moveReportParameters;
		}
		public override int Order
		{
			get { return 0; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
		/// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters"> Набор параметров для создания отчета </param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			//Переменная для хранения полученных параметров отчета.
			var clsReportParameters = new MoveReportParameters();

			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters == null)
				{
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new MoveReportParameters();
				}

				//[В функцию был передан набор параметров]
				//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				clsReportParameters = (MoveReportParameters)iReportParameters;

				//Требуется построение отчета на дату, заданную в параметрах, или на текущую дату?
				if (clsReportParameters.AlwaysForCurrentDateProp)
				{
					//[Требуется построение отчета на текущую дату]
					//Установка значения свойству From параметров значения даты два дня назад. Отчет строится за последние два дня.
					clsReportParameters.DateFrom = DateTime.Now.AddDays(-2).Date;
					//Установка значения свойству To параметров значения текущей даты.
					clsReportParameters.DateTo   = DateTime.Now.Date;
				}

				//Возвращаются скорректированные параметры.
				return clsReportParameters;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return clsReportParameters;
			}
		}
		/// <summary> Функция проверяет переданный набор параметров на корректность заполнения </summary>
		/// <param name="iReportParameters"> Заполненный набор параметров отчета </param>
		/// <returns> В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false </returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
			var clsReportParameters = (MoveReportParameters)iReportParameters;
			//Получение из переданных параметров флага, определяющего отображается ли данный отчет заполненным, или он будет отображаться пустым.
			bool blnShowBlankReport = clsReportParameters.ShowBlankReportProp;

			//Требуется заполнение отчета данными?
			if (blnShowBlankReport)
				return true;

			var result = base.ReportParametersInstanceCheck(iReportParameters);

			if (clsReportParameters.VehicleProp == null ||
				((clsReportParameters.VehicleProp.Tag as int?) ?? 0) == 0)
				result.AddWrongArgument("VehicleId", "Vehicle is not selected");

			if (clsReportParameters.Interval < 0)
				result.AddWrongArgument("Interval", "Interval cannot be less than zero");

			if (clsReportParameters.ParkingInterval < 0)
				result.AddWrongArgument("ParkingInterval", "ParkingInterval cannot be less than zero");

			if (clsReportParameters.PositionsСount < 0)
				result.AddWrongArgument("PositionsСount", "PositionsСount cannot be less than zero");

			return result;
		}
		protected DataSet GetReportTypedDataSet(ReportParameters pars)
		{
			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
			var clsReportParameters = (MoveReportParameters)pars;

			//Создание датасета для хранения информации отчета.

			var strings = GetResourceStringContainer(pars.Culture);

			var dtmReportDateFrom = TimeHelper.GetUtcTime(
				clsReportParameters.DateTimeInterval.DateFrom,
				clsReportParameters.TimeZoneInfo);

			var dtmReportDateTo = TimeHelper.GetUtcTime(
				clsReportParameters.DateTimeInterval.DateTo,
				clsReportParameters.TimeZoneInfo);

			var vehicleId = clsReportParameters.VehicleId;
			var vehicle = mclsInstance.GetVehicleById(pars.OperatorId, vehicleId, pars.Culture);
			if (vehicle == null)
				throw new SystemRightAccessException("Vehicle not found by id " + vehicleId,
					vehicleId, IdType.Vehicle, pars.Culture, SystemRight.VehicleAccess);
			if (!vehicle.rights.Contains(SystemRight.PathAccess))
				throw new SystemRightAccessException("Operator has no PathAccessRight on vehicle " + vehicle.id,
					vehicle.id, IdType.Vehicle, pars.Culture, SystemRight.PathAccess);

			var workingIntervals = TimeHelperEx.GetLocalDailyIntervals(
				pars.DateTimeInterval.DateFrom.Date, pars.DateTimeInterval.DateTo.Date,
				clsReportParameters.WorkingHoursInterval.WorkFrom.Hours,
				clsReportParameters.WorkingHoursInterval.WorkFrom.Minutes,
				clsReportParameters.WorkingHoursInterval.WorkTo.Hours,
				clsReportParameters.WorkingHoursInterval.WorkTo.Minutes)
					.Select(i => new LogTimeSpanParam
					(
						TimeHelper.GetSecondsFromBase(pars.ToUtc(i.From)),
						TimeHelper.GetSecondsFromBase(pars.ToUtc(i.To))
					));
			// Получаем данные отчета
			DataSet dsMoveDetailHistory = mclsInstance.GetMoveDetailHistory(
				clsReportParameters.PositionsСount,
				clsReportParameters.Interval,
				dtmReportDateFrom,
				dtmReportDateTo,
				vehicleId,
				workingIntervals);

			// Добавляем легенду для отчета.
			var hData = new DataTable("Legend");
			hData.Columns.Add(new DataColumn("reportLegendKey"));
			hData.Columns.Add(new DataColumn("reportLegendValue"));
			dsMoveDetailHistory.Tables.Add(hData);

			var hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["report2NameCaption"];
			hRow["reportLegendValue"] = strings["WebGisMoveHistory"];

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["report2capObjectName"];
			hRow["reportLegendValue"] = vehicle.Name;

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["report2capDateFrom"];
			hRow["reportLegendValue"] = clsReportParameters
				.DateTimeInterval
				.DateFrom
				.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["report2capDateTo"];
			hRow["reportLegendValue"] = clsReportParameters
				.DateTimeInterval
				.DateTo
				.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["IntervalPicker"];
			hRow["reportLegendValue"] = TimeHelperEx
				.GetIntervalPreset(clsReportParameters.Interval, clsReportParameters.Culture);

			hData.Rows.Add(hRow);

			if
			(
				clsReportParameters.WorkingHoursInterval.WorkFrom != clsReportParameters.WorkingHoursInterval.WorkTo
				||
				clsReportParameters.WorkingHoursInterval.WorkFrom != TimeSpan.Zero
			)
			{
				hRow = hData.NewRow();

				hRow["reportLegendKey"]   = strings["report2capWorkingHoursInterval"];
				hRow["reportLegendValue"] = string.Format(
					"{0} — {1}",
					clsReportParameters
						.WorkingHoursInterval
						.WorkFrom
						.ToString(@"hh\:mm"),
					clsReportParameters
						.WorkingHoursInterval
						.WorkTo
						.ToString(@"hh\:mm"));

				hData.Rows.Add(hRow);
			}

			// Добавляем таблицу заголовков для основного блока данных.

			var dataCaptions = new DataTable("MoveHistoryCaptions");

			const string columnNameLogTime       = "Log_Time";
			const string columnNameSpeed         = "Speed";
			const string columnNameDist          = "Dist";
			const string columnNameTotalDistance = "TotalDistance";
			const string columnNameAddress       = "Address";

			dataCaptions.Columns.Add(columnNameLogTime);
			dataCaptions.Columns.Add(columnNameSpeed);
			dataCaptions.Columns.Add(columnNameDist);
			dataCaptions.Columns.Add(columnNameTotalDistance);
			dataCaptions.Columns.Add(columnNameAddress);
			dsMoveDetailHistory.Tables.Add(dataCaptions);

			var dcRow = dataCaptions.NewRow();

			dcRow[columnNameLogTime]       = strings["report2inlFrom"];
			dcRow[columnNameSpeed]         = strings["report2inlAvgSpeed"];
			dcRow[columnNameDist]          = strings["report2inlDistance"];
			dcRow[columnNameTotalDistance] = strings["report2capTotalRun"];
			dcRow[columnNameAddress]       = strings["report2capAddress"];
			dataCaptions.Rows.Add(dcRow);

			// Накапливает суммарный пробег, сгруппированный по водителям
			decimal groupedDriverSum = 0;
			// Накапливает общий пробег.
			decimal totalRun = 0;
			int group = 0;
			string data = string.Empty;
			var moveHistoryTable = dsMoveDetailHistory.Tables["MoveHistory"];

			var groupColumn = new DataColumn("Group", typeof(int));
			moveHistoryTable.Columns.Add(groupColumn);
			var totalDistanceColumn = new DataColumn("Temperature", typeof(string));
			moveHistoryTable.Columns.Add(totalDistanceColumn);

			for (int i = 0; i < moveHistoryTable.Rows.Count; i++)
			{
				var dr = moveHistoryTable.Rows[i];
				/////////////////////////////////////////////////
				dr.BeginEdit();
				/////////////////////////////////////////////////
				dr["Log_Time"] = pars.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(Convert.ToInt32(dr["Log_Time"])));
				// установка номеров групп (если табельный номер сменяется увеличиваем группу)
				if (dr["ext_number"].ToString() != data)
				{
					data = dr["ext_number"].ToString();
					group++;
					groupedDriverSum = 0;
				}
				dr["Group"] = group;

				// Будем выводить просуммированный пробег от первой точки до текущей
				if (dr["dist"] != DBNull.Value)
				{
					groupedDriverSum += Convert.ToDecimal(dr["dist"]);
					totalRun         += Convert.ToDecimal(dr["dist"]);
				}
				dr[totalDistanceColumn] = groupedDriverSum.ToString("0.###", NumberHelper.FormatInfo);
				/////////////////////////////////////////////////
				dr.EndEdit();
				/////////////////////////////////////////////////
			}
			moveHistoryTable.Columns.Remove("Temperature_");

			var stopRowIntervals = moveHistoryTable
				.Select()
				.GetSequences((prev, curr) =>
				{
					var distanceFromPrev = Convert.ToDouble(curr["dist"]);
					//TODO: сравнивать адреса, но адреса в таком случае нужно получать в виде структуры ("адрес", расстояние)
					return distanceFromPrev < 0.1;
				})
				.Where(seq => 1 < seq.Count);

			foreach (var stopRowInterval in stopRowIntervals)
			{
				var intervalStartTime = TimeHelper.ConvertToDateTime(stopRowInterval.First()["Log_Time"]);
				var intervalEndTime   = TimeHelper.ConvertToDateTime(stopRowInterval.Last()["Log_Time"]);

				if (clsReportParameters.ParkingInterval <= (intervalEndTime - intervalStartTime).TotalSeconds)
				{
					// Парковка длительностью более ParkingInterval - нужно оставить только начало и конец
					if (stopRowInterval.Count < 3)
						continue;

					for (var i = 1; i != stopRowInterval.Count - 1; ++i)
						moveHistoryTable.Rows.Remove(stopRowInterval[i]);
				}
				else
				{
					// Парковка длиной менее ParkingInterval - удалить полностью
					foreach (var stopRow in stopRowInterval)
						moveHistoryTable.Rows.Remove(stopRow);
				}
			}

			var intervalTable  = dsMoveDetailHistory.Tables["Interval"];
			var workFromString = clsReportParameters.WorkingHoursInterval.WorkFrom.ToString(@"hh\:mm");
			var workToString   = clsReportParameters.WorkingHoursInterval.WorkTo.ToString(@"hh\:mm");
			foreach (DataRow dr in intervalTable.Rows)
			{
				if (dr["From"].ToString() != string.Empty && dr["To"].ToString() != string.Empty)
				{
					/////////////////////////////////////////////////
					dr.BeginEdit();
					/////////////////////////////////////////////////
					dr["From"] = pars.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(Convert.ToInt32(dr["From"])));
					dr["To"]   = pars.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(Convert.ToInt32(dr["To"])));
					/////////////////////////////////////////////////
					dr.EndEdit();
					/////////////////////////////////////////////////
				}
				if (clsReportParameters.WorkingHoursInterval.WorkFrom != clsReportParameters.WorkingHoursInterval.WorkTo)
				{
					/////////////////////////////////////////////////
					dr.BeginEdit();
					/////////////////////////////////////////////////
					dr["WorkFrom"] = workFromString;
					dr["WorkTo"]   = workToString;
					/////////////////////////////////////////////////
					dr.EndEdit();
					/////////////////////////////////////////////////
				}
			}

			#region Фиктивные строки для вывода шапки, на случай если какие-нибудь таблицы в наборе пусты

			if (intervalTable.Rows.Count < 1)
			{
				DataRow row     = intervalTable.NewRow();
				row["From"]     = pars.FormatUtcAsLocal(dtmReportDateFrom);
				row["To"]       = pars.FormatUtcAsLocal(dtmReportDateTo);
				row["WorkFrom"] = workFromString;
				row["WorkTo"]   = workToString;
				intervalTable.Rows.Add(row);
			}
			else
			if (intervalTable.Rows[0]["From"].ToString().Trim() == "" || intervalTable.Rows[0]["To"].ToString().Trim() == "")
			{
				DataRow row = intervalTable.Rows[0];
				row["From"]     = pars.FormatUtcAsLocal(dtmReportDateFrom);
				row["To"]       = pars.FormatUtcAsLocal(dtmReportDateTo);
				row["WorkFrom"] = workFromString;
				row["WorkTo"]   = workToString;
			}
			if (moveHistoryTable.Rows.Count < 1)
			{
				DataRow row = moveHistoryTable.NewRow();

				var vehicleRow = mclsInstance.GetVehiclesWithPositions(pars.OperatorId, null,
					new GetVehiclesArgs() { VehicleIDs = new[] { vehicleId }, IncludeAttributes = true, IncludeLastData = false }).SingleOrDefault();

				row["PUBLIC_NUMBER"] = vehicleRow?.regNum;
				row["GARAGE_NUMBER"] = vehicleRow?.Name;
				moveHistoryTable.Rows.Add(row);
			}

			#endregion Фиктивные строки для вывода шапки, на случай если какие-нибудь таблицы в наборе пусты

			dsMoveDetailHistory.AcceptChanges();

			// Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
			ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
			// Добавление в датасет данных атрибутов предприятия.
			dsMoveDetailHistory.Merge(dsEstablishmentAttributes);

			DataTable dtTotal = dsMoveDetailHistory.Tables.Add("Total");
			dtTotal.Columns.Add("Caption", typeof(string));
			dtTotal.Columns.Add("Run",     typeof(decimal));
			dtTotal.Rows.Add(new object[]
			{
				strings["report2capTotalRun"],
				string.Format("{0:0.###}", totalRun)
			});
			// Заполняем колонку адресов
			dsMoveDetailHistory = mclsInstance.FillAddressColumn(
				dsMoveDetailHistory,
				"MoveHistory",
				"X",
				"Y",
				"Address",
				clsReportParameters.Culture.TwoLetterISOLanguageName,
				clsReportParameters.MapGuid);
			// Заполняем колонку геозон
			dsMoveDetailHistory = mclsInstance.FillGeoZoneColumn(
				dsMoveDetailHistory,
				clsReportParameters.OperatorId,
				vehicleId,
				"MoveHistory",
				"X",
				"Y",
				"GeoZone");
			// Заполняем колонку точек интереса
			dsMoveDetailHistory = mclsInstance.FillGeoPointColumn(
				dsMoveDetailHistory,
				clsReportParameters.OperatorId,
				vehicleId,
				null,
				"MoveHistory",
				"X",
				"Y",
				"GeoPoint");
			return dsMoveDetailHistory;
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var clsReportParameters = (MoveReportParameters)reportParameters;
			return GetReportHtml(clsReportParameters);
		}
		private string GetReportHtml(MoveReportParameters clsReportParameters)
		{
			var stringBuilder = new StringBuilder();

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				var finalDataSet = GetReportTypedDataSet(clsReportParameters);

				var @string = GetResourceStringContainer(clsReportParameters.Culture);

				var caption = string.Format("{0}", @string["WebGisMoveHistory"]);

				xw.WriteElementString("h1", "http://www.w3.org/1999/xhtml", caption);

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportHeader");
				xw.WriteStartElement("tbody");

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["report2capObjectName"]);
				xw.WriteElementString("td", mclsInstance.GetVehicleById(clsReportParameters.OperatorId, clsReportParameters.VehicleId, clsReportParameters.Culture).Name);
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["report2capDateFrom"]);
				xw.WriteElementString("td", clsReportParameters.DateTimeInterval.DateFrom.ToString(TimeHelper.DefaultTimeFormat));
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["report2capDateTo"]);
				xw.WriteElementString("td", clsReportParameters.DateTimeInterval.DateTo.ToString(TimeHelper.DefaultTimeFormat));
				xw.WriteEndElement();

				// Указаны рабочие часы.
				if (clsReportParameters.WorkingHoursInterval.WorkFrom != clsReportParameters.WorkingHoursInterval.WorkTo)
				{
					xw.WriteStartElement("tr");
					xw.WriteElementString("td", @string["report2capWorkingHoursInterval"]);
					xw.WriteElementString("td", string.Format("{0} — {1}",
						clsReportParameters.WorkingHoursInterval.WorkFrom.ToString(@"hh\:mm"),
						clsReportParameters.WorkingHoursInterval.WorkTo.ToString(@"hh\:mm")
						));
					xw.WriteEndElement();
				}

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["IntervalPicker"]);
				xw.WriteElementString("td",
									  TimeHelperEx.GetTimeSpanReadable(
										  TimeSpan.FromSeconds(clsReportParameters.Interval),
										  clsReportParameters.Culture));
				xw.WriteEndElement();

				xw.WriteEndElement();
				xw.WriteEndElement();

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("td", @string["report2inlFrom"]);
				xw.WriteElementString("td", @string["report2inlAvgSpeed"]);
				xw.WriteElementString("td", @string["report2inlDistance"]);
				xw.WriteElementString("td", @string["report2capTotalRun"]);
				xw.WriteElementString("td", @string["report2capAddress"]);

				xw.WriteEndElement();
				xw.WriteEndElement();

				xw.WriteStartElement("tbody");
				decimal totalRun = 0;

				foreach (DataRow row in finalDataSet.Tables["MoveHistory"].Rows)
				{
					decimal dist;
					Decimal.TryParse(row["Dist"].ToString(), out dist);
					totalRun += dist;

					var intervalString = row["Log_Time"].ToString();
					DateTime intervalDateStart;

					if (!DateTime.TryParse(intervalString, out intervalDateStart))
						continue;
					DateTime intervalDateEnd = intervalDateStart.AddSeconds(clsReportParameters.Interval);

					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
							, clsReportParameters.ApplicationPath
							, clsReportParameters.VehicleId
							, intervalDateStart.ToString(TimeHelper.DefaultTimeFormatUpToMinutes)
							, intervalDateEnd.ToString(TimeHelper.DefaultTimeFormatUpToMinutes));

					xw.WriteStartElement("tr");

					xw.WriteElementString("td", intervalDateStart.ToString(TimeHelper.DefaultTimeFormat));

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(row["Speed"].ToString());
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteStartElement("a");
					xw.WriteAttributeString("href", path.Replace(' ', '+'));
					xw.WriteString(dist.ToString(CultureInfo.InvariantCulture));
					xw.WriteEndElement();
					xw.WriteEndElement();

					xw.WriteStartElement("td");
					xw.WriteAttributeString("class", "number");
					xw.WriteString(totalRun.ToString(CultureInfo.InvariantCulture));
					xw.WriteEndElement();

					xw.WriteElementString("td", string.Format("{0}{1}{2}",
						row["Address"],
						string.IsNullOrWhiteSpace(row["GeoZone"].ToString())
							? string.Empty
							: string.Format(", [{0}]", row["GeoZone"]),
						string.IsNullOrWhiteSpace(row["GeoPoint"].ToString())
							? string.Empty
							: string.Format(", '{0}'", row["GeoPoint"])
						));

					xw.WriteEndElement();
				}

				// tbody
				xw.WriteEndElement();
				// table
				xw.WriteEndElement();

				var total = string.Format("{0}: {1}",
										  finalDataSet.Tables["Total"].Rows[0]["Caption"],
										  finalDataSet.Tables["Total"].Rows[0]["Run"]
					);

				xw.WriteElementString("h4", "http://www.w3.org/1999/xhtml", total);
			}

			return stringBuilder.ToString();
		}
		/// <summary> Создает отчет, используя переданные параметры </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var dsMoveDetailHistory = GetReportTypedDataSet(iReportParameters);

			var customReport = new WebGisMoveHistoryExcelPdfReport();

			customReport.SetDataSource(dsMoveDetailHistory);

			return customReport;
		}
	}
}