﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт: "Интервалы движения" </summary>
	[Guid("E7E8996D-80C0-4F31-8E1E-E8859019983B")]
	public class WebGisMoveHistoryTwoColumns : TssReportBase
	{
		public const string ReportLegendKey = "reportLegendKey";
		public const string ReportLegendValue = "reportLegendValue";

		public const string ReportHeaderTableName = "reportHeader";
		public const string BodyHeaderTableName = "reportBodyHeader";
		public const string BodyTableName = "reportBody";

		public const string Report2NameCaption = "report2NameCaption";
		public const string Report2Capobjectname = "report2capObjectName";
		public const string Report2Capdatefrom = "report2capDateFrom";
		public const string Report2Capdateto = "report2capDateTo";
		public const string Report2Capparkinginterval = "report2capParkingInterval";
		public const string Report2Captotalrun = "report2capTotalRun";
		public const string Report2Captotaltime = "report2capTotalTime";
		public const string Report2Captotalparkingtime = "report2capTotalParkingTime";
		public const string Report2Capmaxspeed = "report2capMaxSpeed";

		public const string SDateFrom = "report2inlFrom";
		public const string STimeFrom = "report2inlFromTime";
		public const string SAddressFrom = "report2capAddressFrom";
		public const string SAddressTo = "report2capAddressTo";
		public const string SDateTo = "report2inlTo";
		public const string STimeTo = "report2inlToTime";
		public const string SDistance = "report2inlDistance";
		public const string SAverageSpeed = "report2inlAvgSpeed";
		public const string STotalTime = "report2inlTimeSpan";
		public const string STotalParking = "report2capParking";
		public const string SStart = "report2capStart";
		public const string SEnd = "report2capEnd";

		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["MotionIntervals"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["MotionIntervalsDescription"];
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new MoveIntervalReportParameters();
		}
		public override int Order
		{
			get { return 1; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			//Переменная для хранения полученных параметров отчета.
			var clsReportParameters = new MoveIntervalReportParameters();

			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters != null)
				{
					//[В функцию был передан набор параметров].
					//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
					clsReportParameters = (MoveIntervalReportParameters)iReportParameters;

					//Требуется построение отчета на дату, заданную в параметрах, или на текущую дату?
					if (clsReportParameters.AlwaysForCurrentDateProp)
					{
						//[Требуется построение отчета на текущую дату].
						//Установка значения свойству From параметров значения даты два дня назад. Отчет строится за последние два дня.
						clsReportParameters.DateFrom = DateTime.Now.AddDays(-2).Date;
						//Установка значения свойству To параметров значения текущей даты.
						clsReportParameters.DateTo   = DateTime.Now.Date;
					}

					//Возвращаются скорректированные параметры.
					return clsReportParameters;
				}
				
				//Если набор параметров не был передан в данный отчет, то создать его заново.
				return new MoveIntervalReportParameters();
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				//Функция возвращает пустой набор параметров.
				return clsReportParameters;
			}
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			if (!base.ReportParametersInstanceCheck(iReportParameters))
				return false;

			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
			var clsReportParameters = (MoveIntervalReportParameters) iReportParameters;

			//Получение из переданных параметров флага, определяющего отображается ли данный отчет заполненным, или он будет отображаться пустым.
			bool blnShowBlankReport = clsReportParameters.ShowBlankReportProp;

			//Требуется заполнение отчета данными?
			if (blnShowBlankReport)
				return true;

			// [Заполнение отчета данными не требуется].
			// Функция возвращает положительный результат проверки параметров, потому что для пустого отчета корректность параметров смысла не имеет, а кнопку запуска построения отчета надо делать доступной.
			// [Требуется заполнение отчета данными].
			// Анализ корректности заполнения параметров.
			bool blnParametersCheckResult =
				(clsReportParameters.ParkingInterval >= 0) &&
				(clsReportParameters.PositionsСount  >= 0) &&
				(clsReportParameters.DateTimeInterval.DateFrom < clsReportParameters.DateTimeInterval.DateTo);
			// Функция возвращает результат проверки параметров.
			return blnParametersCheckResult;
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			return GetReportHtml((MoveIntervalReportParameters)reportParameters);
		}
		class ActionInterval
		{
			public int      From;
			public string   PositionFrom;
			public int      To;
			public string   PositionTo;
			public decimal  Run;
			public int      AverageSpeed;

			public decimal? LatFrom;
			public decimal? LngFrom;

			public decimal? LatTo;
			public decimal? LngTo;
			public string   ZoneFrom;
			public string   ZoneTo;

			public string   PointFrom;
			public string   PointTo;

			public ActionIntervalType ActionIntervalType;
		}
		enum ActionIntervalType
		{
			Motion  = 0,
			Parking = 1,
			Skipped
		}
		private string GetReportHtml(MoveIntervalReportParameters clsReportParameters)
		{
			var stringBuilder = new StringBuilder();

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				var vehicleId  = clsReportParameters.VehicleId;
				var operatorId = clsReportParameters.OperatorId;

				var logTimeFrom = TimeHelper.GetSecondsFromBase(TimeHelper.GetUtcTime(
				 clsReportParameters.DateTimeInterval.DateFrom,
				 clsReportParameters.TimeZoneInfo));
				var logTimeTo = TimeHelper.GetSecondsFromBase(TimeHelper.GetUtcTime(
					clsReportParameters.DateTimeInterval.DateTo,
					clsReportParameters.TimeZoneInfo));

				var parkingInterval = clsReportParameters.ParkingInterval;

				var log = mclsInstance.GetFullLog(operatorId, vehicleId, logTimeFrom, logTimeTo,
					new FullLogOptions
					{
						CutOutstandingData = false
					},
					clsReportParameters.Culture);

				var actionIntervals = GetActionIntervals(log, parkingInterval).ToArray();
				FillAddresses(clsReportParameters, actionIntervals.Where(x => x.ActionIntervalType == ActionIntervalType.Motion).ToList());

				var maxSpeed = log.Gps.Any()
					? (from max in log.Gps select max.Speed).Max()
					: 0;

				var totalDistance = log.Gps.Any()
					? (from dist in actionIntervals select dist.Run).Sum()
					: 0;

				var runPeriodSeconds   = 0;
				var parkingTimeSeconds = 0;

				foreach (var i in actionIntervals)
				{
					switch (i.ActionIntervalType)
					{
						case ActionIntervalType.Motion:
							runPeriodSeconds += Math.Abs(i.From - i.To);
							break;
						case ActionIntervalType.Parking:
						case ActionIntervalType.Skipped:
							parkingTimeSeconds += Math.Abs(i.From - i.To);
							break;
						default:
							continue;
					}
				}

				var runPeriod   = new TimeSpan(0, 0, runPeriodSeconds);
				var parkingTime = new TimeSpan(0, 0, parkingTimeSeconds);

				var @string = GetResourceStringContainer(clsReportParameters.Culture);

				var caption = string.Format("{0}", @string["MotionIntervals"]);

				xw.Formatting = Formatting.None;

				xw.WriteElementString("h1", "http://www.w3.org/1999/xhtml", caption);

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportHeader");
				xw.WriteStartElement("tbody");

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Capobjectname]);
				xw.WriteElementString("td", mclsInstance.GetVehicleById(operatorId, vehicleId, clsReportParameters.Culture).Name);
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Capdatefrom]);
				xw.WriteElementString("td", clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(logTimeFrom)));
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Capdateto]);
				xw.WriteElementString("td", clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(logTimeTo)));
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Capparkinginterval]);
				xw.WriteElementString("td", TimeHelperEx.GetTimeSpanReadable(TimeSpan.FromSeconds(parkingInterval), clsReportParameters.Culture));
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Captotalrun]);
				xw.WriteElementString("td", string.Format("{0:0.###} {1}", totalDistance, @string["report2km"]));
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Captotaltime]);
				xw.WriteElementString("td", TimeHelperEx.GetTimeSpanReadable(runPeriod, clsReportParameters.Culture));
				xw.WriteEndElement();


				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Captotalparkingtime]);
				xw.WriteElementString("td", TimeHelperEx.GetTimeSpanReadable(parkingTime, clsReportParameters.Culture));
				xw.WriteEndElement();

				//TODO: корректно вычислить dataAbsenceTotalTime, учитывая это значение в totalTime и parkingTime, и раскомментировать
				//if (TimeSpan.Zero < dataAbsenceTotalTime)
				//{
				//    xmlWriter.WriteStartElement("tr");
				//    xmlWriter.WriteElementString("td", 
				//        @string["report2capTotalDataAbsenceTime"] + " " +
				//        TimeHelper.GetTimeSpanReadable(log.Options.NoDataPeriod, clsReportParameters.Culture));
				//    xmlWriter.WriteElementString("td", 
				//        TimeHelper.GetTimeSpanReadable(
				//        dataAbsenceTotalTime, 
				//        clsReportParameters.Culture));
				//    xmlWriter.WriteEndElement();
				//}

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string[Report2Capmaxspeed]);
				xw.WriteElementString("td", string.Format("{0} {1}", maxSpeed, @string["report2kmh"]));
				xw.WriteEndElement();

				xw.WriteEndElement();
				xw.WriteEndElement();

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportBody reportTable withBorders");
				
				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");
				
				xw.WriteStartElement("th");
				xw.WriteAttributeString("style", "min-width: 260px;");
				xw.WriteString(@string[SDateFrom]);
				xw.WriteEndElement();

				xw.WriteStartElement("th");
				xw.WriteAttributeString("style", "min-width: 260px;");
				xw.WriteString(@string[SDateTo]);
				xw.WriteEndElement();

				xw.WriteElementString("th", @string[SDistance]);
				xw.WriteElementString("th", @string[SAverageSpeed]);
				xw.WriteElementString("th", @string[STotalTime]);
				xw.WriteElementString("th", @string[STotalParking]);
				xw.WriteElementString("th", "");
				xw.WriteEndElement();
				xw.WriteEndElement();

				xw.WriteStartElement("tbody");
				foreach (var interval in actionIntervals)
				{

					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
							, clsReportParameters.ApplicationPath
							, vehicleId
							, TimeHelper.GetLocalTime(interval.From, clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss")
							, TimeHelper.GetLocalTime(interval.To, clsReportParameters.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss"));

					switch (interval.ActionIntervalType)
					{
						case ActionIntervalType.Motion:
							xw.WriteStartElement("tr");

							var positionsFrom = new[] { interval.PositionFrom, interval.ZoneFrom, interval.PointFrom }.Where(s => !string.IsNullOrWhiteSpace(s));
							var positionsTo = new[] { interval.PositionTo, interval.ZoneTo, interval.PointTo }.Where(s => !string.IsNullOrWhiteSpace(s));

							xw.WriteElementString("td",
								clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(interval.From))
								+ " " + string.Join(", ", positionsFrom));
							xw.WriteElementString("td",
								clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(interval.To))
								+ " " + string.Join(", ", positionsTo));
							WriteNumberCell(xw, interval.Run.ToString("0.###"));
							WriteNumberCell(xw, interval.AverageSpeed.ToString(clsReportParameters.Culture));
							xw.WriteElementString("td", TimeHelperEx.GetTimeSpanReadable(TimeSpan.FromSeconds(interval.To - interval.From), clsReportParameters.Culture));
							xw.WriteElementString("td", string.Empty);

							xw.WriteStartElement("td");
							xw.WriteStartElement("a");
							xw.WriteAttributeString("href", path.Replace(' ', '+'));
							xw.WriteString(@string["ShowOnMap"]);
							xw.WriteEndElement();
							xw.WriteEndElement();

							xw.WriteEndElement();
							break;
						case ActionIntervalType.Parking:

							xw.WriteStartElement("tr");

							xw.WriteElementString("td", clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(interval.From)));
							xw.WriteStartElement("td");

							xw.WriteString(clsReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(interval.To)));
							xw.WriteEndElement();

							xw.WriteElementString("td", string.Empty);
							xw.WriteElementString("td", string.Empty);
							xw.WriteElementString("td", string.Empty);
							xw.WriteElementString("td", TimeHelperEx.GetTimeSpanReadable(TimeSpan.FromSeconds(interval.To - interval.From), clsReportParameters.Culture));


							xw.WriteStartElement("td");
							xw.WriteStartElement("a");
							xw.WriteAttributeString("href", path.Replace(' ', '+'));
							xw.WriteString(@string["ShowOnMap"]);
							xw.WriteEndElement();
							xw.WriteEndElement();

							xw.WriteEndElement();
							break;
						default:
							continue;
					}

				}
				xw.WriteEndElement();
				xw.WriteEndElement();
			}

			return stringBuilder.ToString();
		}
		private static void WriteNumberCell(XmlTextWriter xw, string value)
		{
			xw.WriteStartElement("td");
			xw.WriteAttributeString("class", "number");
			xw.WriteString(value);
			xw.WriteEndElement();
		}
		public override object GetReportAsDto(ReportParameters reportParameters)
		{
			var clsReportParameters = (MoveIntervalReportParameters)reportParameters;
			var vehicleId           = clsReportParameters.VehicleId;
			var operatorId          = clsReportParameters.OperatorId;
			var logTimeFrom         = TimeHelper.GetSecondsFromBase(TimeHelper.GetUtcTime(clsReportParameters.DateTimeInterval.DateFrom, clsReportParameters.TimeZoneInfo));
			var logTimeTo           = TimeHelper.GetSecondsFromBase(TimeHelper.GetUtcTime(clsReportParameters.DateTimeInterval.DateTo, clsReportParameters.TimeZoneInfo));
			var parkingInterval     = clsReportParameters.ParkingInterval;
			var log                 = mclsInstance.GetFullLog(operatorId, vehicleId, logTimeFrom, logTimeTo, new FullLogOptions { CutOutstandingData = false }, clsReportParameters.Culture);
			var actionIntervals     = GetActionIntervals(log, parkingInterval).ToArray();
			var maxSpeed            = log.Gps.Any() ? (from max in log.Gps select max.Speed).Max() : 0;
			var totalDistance       = log.Gps.Any() ? (from dist in actionIntervals select dist.Run).Sum() : 0;
			var runPeriodSeconds    = 0;
			var parkingTimeSeconds  = 0;

			FillAddresses(clsReportParameters, actionIntervals.Where(x => x.ActionIntervalType == ActionIntervalType.Motion).ToList());
			foreach (var i in actionIntervals)
			{
				switch (i.ActionIntervalType)
				{
					case ActionIntervalType.Motion:
						runPeriodSeconds += Math.Abs(i.From - i.To);
						break;
					case ActionIntervalType.Parking:
					case ActionIntervalType.Skipped:
						parkingTimeSeconds += Math.Abs(i.From - i.To);
						break;
					default:
						continue;
				}
			}

			var runPeriod = new TimeSpan(0, 0, runPeriodSeconds);
			var parkingTime = new TimeSpan(0, 0, parkingTimeSeconds);

			var header = new WebGisMoveHistoryTwoColumnsDTO.ReportHeader
			{
				ObjectName       = mclsInstance.GetVehicleById(clsReportParameters.OperatorId, vehicleId, clsReportParameters.Culture).Name,
				DateFrom         = TimeHelper.GetLocalTime(logTimeFrom, clsReportParameters.TimeZoneInfo),
				DateTo           = TimeHelper.GetLocalTime(logTimeTo, clsReportParameters.TimeZoneInfo),
				ParkingInterval  = TimeSpan.FromSeconds(parkingInterval),
				TotalRun         = totalDistance,
				TotalTime        = runPeriod,
				TotalParkingTime = parkingTime,
				MaxSpeed         = maxSpeed
			};

			var rows = new List<WebGisMoveHistoryTwoColumnsDTO.ReportRowData>();
			foreach (var interval in actionIntervals)
			{
				var row = new WebGisMoveHistoryTwoColumnsDTO.ReportRowData
					{
						DateFrom = TimeHelper.GetLocalTime(interval.From, clsReportParameters.TimeZoneInfo),
						DateTo = TimeHelper.GetLocalTime(interval.To, clsReportParameters.TimeZoneInfo)
					};

				switch (interval.ActionIntervalType)
				{

					case ActionIntervalType.Motion:
						var positionsFrom = new[] { interval.PositionFrom, interval.ZoneFrom, interval.PointFrom }.Where(s => !string.IsNullOrWhiteSpace(s));
						var positionsTo = new[] { interval.PositionTo, interval.ZoneTo, interval.PointTo }.Where(s => !string.IsNullOrWhiteSpace(s));

						row.AddressFrom = string.Join(" ", positionsFrom);
						row.AddressTo = string.Join(" ", positionsTo);

						row.Distance = interval.Run;
						row.AverageSpeed = interval.AverageSpeed;
						row.TotalTime = TimeSpan.FromSeconds(interval.To - interval.From);
						break;
					case ActionIntervalType.Parking:
						row.TotalParking = TimeSpan.FromSeconds(interval.To - interval.From);
						break;
					default:
						continue;
				}

				rows.Add(row);
			}

			return new WebGisMoveHistoryTwoColumnsDTO()
			{
				Header = header,
				Rows = rows
			};
		}
		private void FillAddresses(MoveIntervalReportParameters clsReportParameters, List<ActionInterval> motionIntervals)
		{
			var vehicleId  = clsReportParameters.VehicleId;
			var operatorId = clsReportParameters.OperatorId;
			var ds = new DataSet();
			const string tableName = "report";

			var dt = new DataTable { TableName = tableName };
			ds.Tables.Add(dt);

			const string latColumnName = "lat";
			dt.Columns.Add(new DataColumn { ColumnName = latColumnName, DataType = typeof(decimal) });
			const string lngColumnName = "lng";
			dt.Columns.Add(new DataColumn { ColumnName = lngColumnName, DataType = typeof(decimal) });
			const string addressColumnName = "address";
			dt.Columns.Add(new DataColumn { ColumnName = addressColumnName, DataType = typeof(string) });
			const string geoPointColumnName = "geoPoint";
			dt.Columns.Add(new DataColumn { ColumnName = geoPointColumnName, DataType = typeof(string) });
			const string geoZoneColumnName = "geoZone";
			dt.Columns.Add(new DataColumn { ColumnName = geoZoneColumnName, DataType = typeof(string) });

			// TODO: убрать стоянку первую.
			foreach (var i in motionIntervals)
			{
				var rowFrom = dt.NewRow();
				if (i.LatFrom != null && i.LngFrom != null)
				{
					rowFrom[latColumnName] = i.LatFrom.Value;
					rowFrom[lngColumnName] = i.LngFrom.Value;
				}

				dt.Rows.Add(rowFrom);

				var rowTo = dt.NewRow();

				if (i.LatTo != null && i.LngTo != null)
				{
					rowTo[latColumnName] = i.LatTo.Value;
					rowTo[lngColumnName] = i.LngTo.Value;
				}
				dt.Rows.Add(rowTo);
			}

			ds = mclsInstance.FillAddressColumn(ds, tableName, lngColumnName, latColumnName, addressColumnName, clsReportParameters.Culture.TwoLetterISOLanguageName, clsReportParameters.MapGuid);
			ds = mclsInstance.FillGeoZoneColumn(ds, operatorId, vehicleId, tableName, lngColumnName, latColumnName, geoZoneColumnName);
			ds = mclsInstance.FillGeoPointColumn(ds, operatorId, vehicleId, null, tableName, lngColumnName, latColumnName, geoPointColumnName);

			var rowIndex = 0;
			var rows = ds.Tables[tableName].Rows;
			foreach (var mi in motionIntervals)
			{
				mi.PositionFrom = rows[rowIndex][addressColumnName]  as string;
				mi.ZoneFrom     = rows[rowIndex][geoZoneColumnName]  as string;
				mi.PointFrom    = rows[rowIndex][geoPointColumnName] as string;
				++rowIndex;
				mi.PositionTo   = rows[rowIndex][addressColumnName]  as string;
				mi.ZoneTo       = rows[rowIndex][geoZoneColumnName]  as string;
				mi.PointTo      = rows[rowIndex][geoPointColumnName] as string;
				++rowIndex;
			}
		}
		private static IEnumerable<ActionInterval> GetActionIntervals(FullLog log, int parkingInterval)
		{
			var actionIntervals = new List<ActionInterval>();
			ActionInterval currentMotionInterval = null;
			ActionInterval currentParkingInterval = null;

			foreach (var interval in log.GetActionIntervalsStep1())
			{
				var position1 = log.GetCurrentOrPreviousGeoRecord(TimeHelper.GetSecondsFromBase(interval.Start));
				var position2 = log.GetCurrentOrPreviousGeoRecord(TimeHelper.GetSecondsFromBase(interval.End));

				if (!position1.HasValue || !position2.HasValue)
					continue;

				if (interval.IsMotion)
				{
					currentParkingInterval = null;

					if (currentMotionInterval == null)
					{
						currentMotionInterval = new ActionInterval
						{
							ActionIntervalType = ActionIntervalType.Motion,
							From               = TimeHelper.GetSecondsFromBase(interval.Start),
							To                 = TimeHelper.GetSecondsFromBase(interval.Start) + interval.TimeSpanSeconds,
							LatFrom            = position1.Value.Lat,
							LngFrom            = position1.Value.Lng,
							LatTo              = position2.Value.Lat,
							LngTo              = position2.Value.Lng,
							Run                = interval.Run//,
							//AverageSpeed = (int) interval.AvgSpeed
						};
						actionIntervals.Add(currentMotionInterval);
					}
					else
					{
						currentMotionInterval.LatTo = position2.Value.Lat;
						currentMotionInterval.LngTo = position2.Value.Lng;
						currentMotionInterval.To   += interval.TimeSpanSeconds;
						currentMotionInterval.Run  += interval.Run;
					}
				}
				if (!interval.IsMotion)
				{
					currentMotionInterval = null;

					if (currentParkingInterval == null)
					{
						currentParkingInterval = new ActionInterval
						{
							ActionIntervalType = ActionIntervalType.Parking,
							From               = TimeHelper.GetSecondsFromBase(interval.Start),
							To                 = TimeHelper.GetSecondsFromBase(interval.Start) + interval.TimeSpanSeconds,
							LatFrom            = position1.Value.Lat,
							LngFrom            = position1.Value.Lng,
							LatTo              = position2.Value.Lat,
							LngTo              = position2.Value.Lng
						};
						actionIntervals.Add(currentParkingInterval);
					}
					else
					{
						currentParkingInterval.LatTo = position2.Value.Lat;
						currentParkingInterval.LngTo = position2.Value.Lng;
						currentParkingInterval.To   += interval.TimeSpanSeconds;
					}
				}
			}

			foreach (var interval in actionIntervals)
			{
				if (interval.ActionIntervalType == ActionIntervalType.Parking && Math.Abs(interval.From - interval.To) <= parkingInterval)
					interval.ActionIntervalType = ActionIntervalType.Skipped;

				if (interval.ActionIntervalType == ActionIntervalType.Motion)
					interval.AverageSpeed = (int)(interval.Run / Math.Abs(interval.From - interval.To) * 3.6m);

				interval.Run /= 1000;
			}

			return actionIntervals; //.Where(x => x.LatFrom != null && x.Run > 0.050m || x.ActionIntervalType == ActionIntervalType.Parking);
		}
		/// <summary> Создает отчет, используя переданные параметры </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			try
			{
				var reportParams = (MoveIntervalReportParameters)iReportParameters;
				var reportData   = (WebGisMoveHistoryTwoColumnsDTO)GetReportAsDto(reportParams);
				return new ExcelPdfReport(this, reportParams, reportData);
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);

				return null;
			}
		}
	}
}