﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ExportOptions = CrystalDecisions.Shared.ExportOptions;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class ExcelPdfReport : IReportClass
	{
		private readonly MoveIntervalReportParameters   _params;
		private readonly WebGisMoveHistoryTwoColumnsDTO _data;
		private readonly ResourceStringContainer        _strings;
		private readonly Font _fontNorm;
		private readonly Font _fontBold;

		public ExcelPdfReport(WebGisMoveHistoryTwoColumns report, MoveIntervalReportParameters parameters, WebGisMoveHistoryTwoColumnsDTO data)
		{
			_data     = data;
			_params   = parameters;
			_strings  = report.GetResourceStringContainer(parameters.Culture);
			_fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			_fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					ExportToExcel(fileName, _strings);
					break;
				case ReportTypeEnum.Acrobat:
					ExportToAcrobat(fileName, _strings);
					break;
				case ReportTypeEnum.Xml:
					ExportToXml(fileName);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(formatType));
			}

			// PDF Export
		}
		private void ExportToAcrobat(string fileName, ResourceStringContainer resourceStringContainer)
		{
			var document = new Document(PageSize.A4);
			var ds = _data.ToDataset(resourceStringContainer);
			try
			{
				var pdfWriter = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
				pdfWriter.PageEvent = new PdfReportPageEvents();

				var reportName = ds.Tables["reportHeader"].Rows[0]["reportLegendValue"].ToString();

				document.AddTitle(reportName); // необходимо до открытия документа
				document.Open();
				document.NewPage();
				var commonTable = new PdfPTable(2) { WidthPercentage = 50f, HorizontalAlignment = Element.ALIGN_LEFT };

				var ht = ds.Tables["reportHeader"];
				foreach (DataRow row in ht.Rows)
				{
					commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendKey"].ToString(),   _fontBold)));
					commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendValue"].ToString(), _fontNorm)));
				}

				document.Add(commonTable);
				document.Add(new Paragraph(new Phrase(" ")));

				var dt = ds.Tables["reportBody"];
				var pTable = new PdfPTable(dt.Columns.Count);
				pTable.DefaultCell.Border = Rectangle.NO_BORDER;

				var dth = ds.Tables["reportBodyHeader"];
				//Лишние смердженные колонки для двухэтажного заголовка. sStart и sEnd.
				var actualColumns = dth.Columns.Count - 2;

				foreach (DataRow row in dth.Rows)
				{
					var phCells = new PdfPCell[actualColumns];
					var phRow = new PdfPRow(phCells);
					for (var i = 0; i < actualColumns; i++)
					{
						phCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold)) { BackgroundColor = BaseColor.LIGHT_GRAY };
					}
					pTable.Rows.Add(phRow);
				}

				foreach (DataRow row in dt.Rows)
				{
					var pCells = new PdfPCell[dt.Columns.Count];
					var pRow = new PdfPRow(pCells);
					for (var i = 0; i < dt.Columns.Count; i++)
					{
						var pdfPCell = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));

						var column = dth.Columns[i];
						switch (column.ColumnName)
						{
							case "report2inlAvgSpeed":
							case "report2inlDistance":
								pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
								break;
						}
						pCells[i] = pdfPCell;
					}
					pTable.Rows.Add(pRow);
				}
				// Повторяющийся заголовок.
				pTable.HeaderRows = 1;
				// Ширина таблицы.
				pTable.WidthPercentage = 100f;
				// Релятивные значения ширины столбцов.
				pTable.SetTotalWidth(new[] { 1.2f, 1f, 3.5f, 1.2f, 1f, 3f, 1f, 1f, 1f, 1f });

				document.Add(pTable);
			}
			catch (Exception ex)
			{
				Trace.Write(ex.Message);
				throw;
			}

			document.Close();
		}
		private void ExportToExcel(string fileName, ResourceStringContainer resourceStringContainer)
		{
			var ds = _data.ToDataset(resourceStringContainer);
			var stringReport = WorkbookEngine.CreateWorkbook(ds, "MoveHistory.xsl");
			var encoding = new UTF8Encoding(false);

			using (TextWriter writer = new StreamWriter(fileName, false, encoding))
			{
				writer.Write(stringReport);
			}
		}
		private void ExportToXml(string fileName)
		{
			var serializer = new DataContractSerializer(typeof(WebGisMoveHistoryTwoColumnsDTO));
			using (var filestream = File.Open(fileName, FileMode.Create))
			{
				serializer.WriteObject(filestream, _data);
			}
		}
		public ExportOptions ExportOptions
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}
		public ReportDefinition ReportDefinition
		{
			get { throw new NotImplementedException(); }
		}
		public void Dispose()
		{
		}
	}
}