<xsl:stylesheet version="1.0"
 xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">

      <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
          <Alignment ss:Vertical="Bottom"/>
          <Borders/>
          <Font ss:FontName="Arial"/>
          <Interior/>
          <NumberFormat/>
          <Protection/>
        </Style>
        <Style ss:ID="s63">
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s95">
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
        </Style>
        <Style ss:ID="s96">
          <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
        </Style>
        <Style ss:ID="s97">
          <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss"/>
        </Style>
        <Style ss:ID="s106">
          <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss"/>
          <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s109">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
          <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s110">
          <Alignment ss:Horizontal="Right" ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
          <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s111">
          <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Arial" x:CharSet="204" x:Family="Swiss"/>
          <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s112">
          <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <NumberFormat ss:Format="0"/>
        </Style>
        <Style ss:ID="s113">
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <NumberFormat ss:Format="0"/>
        </Style>
        <Style ss:ID="s114">
          <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <NumberFormat ss:Format="0.000"/>
        </Style>
      </Styles>

      <xsl:apply-templates/>
    </Workbook>
  </xsl:template>

  <xsl:template match="/*">
    <Worksheet>
      <xsl:attribute name="ss:Name">
        <xsl:value-of select="/NewDataSet/Legend/reportLegendValue[1]" />
      </xsl:attribute>
      <Table ss:ExpandedColumnCount="7" x:FullColumns="1"
   x:FullRows="1">
        <Column ss:AutoFitWidth="0" ss:Width="108.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="121.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="99"/>
        <Column ss:AutoFitWidth="0" ss:Width="116.25"/>
        <Column ss:AutoFitWidth="0" ss:Width="435"/>
        <Column ss:AutoFitWidth="0" ss:Width="405"/>

        <xsl:apply-templates select="//Legend" />
        <Row />
        <xsl:apply-templates select="//MoveHistoryCaptions" />
        <xsl:apply-templates select="//MoveHistory" />
      </Table>
    </Worksheet>
  </xsl:template>

  <xsl:template match="Legend">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="MoveHistoryCaptions">
    <Row>
      <Cell ss:StyleID="s63">
        <Data ss:Type="String">
          <xsl:value-of select="Log_Time"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s63">
        <Data ss:Type="String">
          <xsl:value-of select="Speed"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s63">
        <Data ss:Type="String">
          <xsl:value-of select="Dist"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s63">
        <Data ss:Type="String">
          <xsl:value-of select="TotalDistance"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s63">
        <Data ss:Type="String">
          <xsl:value-of select="Address"/>
        </Data>
      </Cell>
    </Row>
  </xsl:template>

  <xsl:template match="reportLegendKey">
    <Cell ss:MergeAcross="1" ss:StyleID="s63">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="reportLegendValue">
    <Cell ss:MergeAcross="4" >
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="MoveHistory">
    <Row>
      <Cell ss:StyleID="s95">
        <Data ss:Type="String">
          <xsl:value-of select="Log_Time"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s96">
        <Data ss:Type="Number">
          <xsl:value-of select="Speed"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s96">
        <Data ss:Type="Number">
          <xsl:value-of select="Dist"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s96">
        <Data ss:Type="Number">
          <xsl:value-of select="Temperature"/>
        </Data>
      </Cell>
      <Cell ss:StyleID="s95">
        <Data ss:Type="String">
          <xsl:value-of select="Address"/>
        </Data>
      </Cell>
    </Row>
  </xsl:template>
</xsl:stylesheet>