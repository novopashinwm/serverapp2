﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.TransportDispatcher.Reports;

namespace FORIS.TSS.TransportDispatcher.FuelCostCalculation
{
	[Guid("5159616A-0D75-4174-BAFD-4F6467A96400")]
	public class FuelCostCalculationReport : TssReportBase
	{
		public override object GetReportAsObj(ReportParameters iReportParameters)
		{
			var reportDataSet = GetReportDataSet(iReportParameters);
			return GetReportHtml(reportDataSet, (FuelCostCalculationParameters)iReportParameters);
		}
		private string GetReportHtml(FuelCostCalculationDataSet dataSet, FuelCostCalculationParameters reportParameters)
		{
			var stringBuilder = new StringBuilder();
			var @string       = GetResourceStringContainer(reportParameters.Culture);
			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders fuel-cost-calculation");

				xw.WriteStartElement("thead");

				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "header44");
				xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

				xw.WriteStartElement("td");
				xw.WriteAttributeString("style", "text-align:left; padding-left:1em;");
				xw.WriteString(@string["NameCaption"]);
				xw.WriteEndElement();

				//TODO: добавить возможность редактирования типа топлива
				xw.WriteElementString("td", @string["FuelTypeCaption"]);
				xw.WriteElementString("td", @string["Run"]);
				//TODO: добавить возможность редактирования норматива
				xw.WriteElementString("td", @string["FuelSpendStandardCaption"]);
				//TODO: добавить возможность редактирования норматива
				xw.WriteElementString("td", @string["EngineHoursStandard"]);
				xw.WriteElementString("td", @string["EngineHours"]);
				xw.WriteElementString("td", @string["FuelSpendCaption"]);
				//todo: Получить валюту из объекта наблюдения.
				xw.WriteElementString("td", @string["FuelCostCaption"] + @string["PerLitre"]);
				xw.WriteElementString("td", @string["FuelTotalCostCaption"] + @string["PerLitre"]);

				//tr
				xw.WriteEndElement();
				//thead
				xw.WriteEndElement();

				var dataTable = dataSet.Tables["Data"];
				xw.WriteStartElement("tbody");
				foreach (DataRow dr in dataTable.Rows)
				{
					xw.WriteStartElement("tr");

					xw.WriteStartElement("td");
					xw.WriteAttributeString("style", "text-align:left; padding-left:1em;");
					xw.WriteString(dr["Garage_Number"].ToString());
					xw.WriteEndElement();

					xw.WriteElementString("td", dr["Fuel_Type_Name"].ToString());
					WriteNumber(xw, dr["Distance"], "0.###");
					WriteNumber(xw, dr["FuelSpentStandart"], "0.##");
					WriteNumber(xw, dr["EngineHoursStandard"], "0.##");
					WriteNumber(xw, dr["EngineHours"], "0.##");
					WriteNumber(xw, dr["FuelSpent"], "0.##");
					WriteNumber(xw, dr["FuelPricePerLitre"], "0.##");
					WriteNumber(xw, dr["FuelCost"], "0.##");

					//tr
					xw.WriteEndElement();
				}
				//tbody
				xw.WriteEndElement();

				xw.WriteStartElement("tfoot");

				xw.WriteStartElement("td");
				xw.WriteAttributeString("colspan", "8");
				xw.WriteString(@string["Total"]);
				//td
				xw.WriteEndElement();

				WriteNumber(xw, dataSet.Tables["ReportHeader"].Rows[0]["TotalFuelCost"], "0.##");

				//tfoot
				xw.WriteEndElement();
			}
			return stringBuilder.ToString();
		}
		private static void WriteNumber(XmlTextWriter xw, object value, string format)
		{
			xw.WriteStartElement("td");
			xw.WriteAttributeString("class", "number");
			xw.WriteString(value == DBNull.Value ? string.Empty : Convert.ToDecimal(value).ToString(format));
			xw.WriteEndElement();
		}
		private static readonly int[] AllWeekDays = { 0, 1, 2, 3, 4, 5, 6 };
		private FuelCostCalculationDataSet GetReportDataSet(ReportParameters iReportParameters)
		{
			var reportParameters = (FuelCostCalculationParameters)iReportParameters;
			if (reportParameters.WorkingHoursInterval.WorkFrom == reportParameters.WorkingHoursInterval.WorkTo)
			{
				reportParameters.WorkingHoursInterval.WorkFrom = new TimeSpan(0, 0, 0);
				reportParameters.WorkingHoursInterval.WorkTo   = new TimeSpan(0, 0, 0);
			}

			var selectDays = (reportParameters.Weekdays ?? AllWeekDays).Select(v => (DayOfWeek)v);
			var rangeDays = new List<DateTime>((int)((reportParameters.DateTo - reportParameters.DateFrom).TotalDays));

			for (var currendDate = reportParameters.DateFrom;
				 currendDate <= reportParameters.DateTo;
				 currendDate = currendDate.AddDays(1))
			{
				rangeDays.Add(currendDate);
			}

			rangeDays =
			(
				from p in rangeDays
				join rp in selectDays on p.DayOfWeek equals rp
				select p
			)
				.ToList();

			var rangeDateTable = new DataTable("DateTimeRange");
			rangeDateTable.Columns.Add("dateFrom", typeof(int));
			rangeDateTable.Columns.Add("dateTo",   typeof(int));

			rangeDays.ForEach(date =>
			{
				DataRow row = rangeDateTable.NewRow();
				row["dateFrom"] = TimeHelper.GetSecondsFromBase(reportParameters.ToUtc(
					date.Add(reportParameters.WorkingHoursInterval.WorkFrom)));

				if (reportParameters.WorkingHoursInterval.WorkFrom >= reportParameters.WorkingHoursInterval.WorkTo)
				{
					row["dateTo"] = TimeHelper.GetSecondsFromBase(reportParameters.ToUtc(
					date.AddDays(1).Add(reportParameters.WorkingHoursInterval.WorkTo)
					));
				}
				else
				{
					row["dateTo"] = TimeHelper.GetSecondsFromBase(reportParameters.ToUtc(
						date.Add(reportParameters.WorkingHoursInterval.WorkTo)));
				}
				rangeDateTable.Rows.Add(row);
			});
			var data = mclsInstance.GetDataFromDB(
				new ArrayList
					{
						new ParamValue("@operatorID", reportParameters.OperatorId),
						new ParamValue("@vehicleGroupId", reportParameters.VehicleGroupProp.Tag as int? ?? (object)DBNull.Value),
						new ParamValue("@dateRange", rangeDateTable),
					},
				"dbo.ReportFuelCostCalculation",
				new[] { "Data" }
				);
			var table = data.Tables["Data"];
			var ids = table.Rows.Cast<DataRow>().Select(row => row["Vehicle_ID"]).Cast<int>().Distinct().ToArray();
			var args = new GetVehiclesArgs
			{
				VehicleIDs = ids,
				Culture    = reportParameters.Culture
			};
			using (var entities = new Entities())
			{
				var vehs = entities.VEHICLE
					.Include(VEHICLE.Fuel_TypeIncludePath)
					.Where(v => ids.Contains(v.VEHICLE_ID))
					.ToDictionary(v => v.VEHICLE_ID, v => new { v.Fuel_Type, v.FuelSpendStandardKilometer, v.FuelSpendStandardLiter });

				var vehicles = mclsInstance.GetVehiclesWithPositions(reportParameters.OperatorId, null, args).ToDictionary(key => key.id);
				foreach (DataRow row in table.Rows)
				{
					var vehicleId = (int)row["Vehicle_ID"];
					var vehicle = vehicles[vehicleId];
					var entity = vehs[vehicleId];
					row["Garage_Number"] = vehicle.Name;
					var unit = entity.Fuel_Type != null
						? (UnitOfMeasure)entity.Fuel_Type.Unit_Id
						: UnitOfMeasure.LitrePer100Km;
					decimal fuelSpent;
					switch (unit)
					{
						case UnitOfMeasure.KmPerM3:
						case UnitOfMeasure.KmPerLitre:
							fuelSpent = entity.FuelSpendStandardKilometer ?? ((decimal?)row["FuelSpendStandardKilometer"] ?? 0);
							break;
						case UnitOfMeasure.LitrePer100Km:
							fuelSpent = entity.FuelSpendStandardLiter ?? ((decimal?)row["FuelSpendStandardLiter"] ?? 0);
							break;
						default:
							throw new NotImplementedException("unit is not known " + unit);
					}

					row["FuelSpentStandart"] = MeasureUnitHelper.ConvertTo(fuelSpent, unit, UnitOfMeasure.LitrePer100Km);
				}
			}

			var view = table.DefaultView;
			view.Sort = "Garage_Number";
			data.Tables.Remove(table);
			data.Tables.Add(view.ToTable());

			var reportDataSet = new FuelCostCalculationDataSet();
			reportDataSet.Merge(data);

			decimal totalFuelCost = 0;
			foreach (FuelCostCalculationDataSet.DataRow row in reportDataSet.Data.Rows)
			{
				if (row.IsDistanceNull() || row.IsFuelSpentStandartNull())
					continue;

				var litresPerKm = MeasureUnitHelper.ConvertTo(row.FuelSpentStandart, UnitOfMeasure.LitrePer100Km, UnitOfMeasure.LitrePerKm);
				row.FuelSpent = row.Distance * litresPerKm;
				if (!row.IsEngineHoursStandardNull() && !row.IsEngineHoursNull())
					row.FuelSpent += row.EngineHoursStandard * row.EngineHours;

				if (row.IsFuel_Type_IDNull())
					continue;

				decimal fuelCostPerLitre;
				if (reportParameters.FuelCosts.TryGetValue(row.Fuel_Type_ID, out fuelCostPerLitre))
				{
					row.FuelPricePerLitre = fuelCostPerLitre;
					row.FuelCost          = row.FuelSpent * fuelCostPerLitre;
					totalFuelCost        += row.FuelCost;
				}
			}

			var drHeader = reportDataSet.ReportHeader.NewReportHeaderRow();
			drHeader.dtFrom       = reportParameters.DateFrom;
			drHeader.dtTo         = reportParameters.DateTo;
			drHeader.TimeFrom     = reportParameters.WorkingHoursInterval.WorkFrom.ToString(@"hh\.mm");
			drHeader.TimeTo       = reportParameters.WorkingHoursInterval.WorkTo.ToString(@"hh\.mm");
			drHeader.VehicleGroup = mclsInstance.GetVehicleGroupById((int)reportParameters.VehicleGroupProp.Tag).name;
			drHeader.TotalFuelCost = totalFuelCost;
			// TODO: Вытащить валюту из департамента.
			drHeader.Currency = string.Empty;// reportParameters.Currency;

			reportDataSet.ReportHeader.AddReportHeaderRow(drHeader);

			//Датасет для хранения констант с атрибутами предприятия.
			//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
			ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
			//Добавление в датасет данных атрибутов предприятия.
			reportDataSet.Merge(dsEstablishmentAttributes);
			reportDataSet.AcceptChanges();
			return reportDataSet;
		}
		public override int Order
		{
			get { return 5; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			ReportClassWrapper rptResultReport = null;
			FuelCostCalculationDataSet dataSet = null;
			var processed = false;
			try
			{
				//Создание экземпляра отчета.
				rptResultReport = new FuelCostCalculationCrystalReport();
				if (iReportParameters != null)
					TranslateReport<FuelCostCalculationReport>(rptResultReport, iReportParameters.Culture);
				//Подключение отчета к данным, собранным в DataSet'е.
				dataSet = GetReportDataSet(iReportParameters);
				rptResultReport.SetDataSource(dataSet);
				// Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation =
					CrystalDecisions.Shared.PaperOrientation.Portrait;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();

				if (dataSet != null)
					dataSet.Dispose();
			}
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new FuelCostCalculationParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as FuelCostCalculationParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleGroupId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostCalculation"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["FuelCostCalculationDescription"];
		}
	}
}