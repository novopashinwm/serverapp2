﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.TransportDispatcher.Reports;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.FuelCostCalculation
{
	[Serializable]
	public class FuelCostCalculationParameters : ReportParameters
	{
		[Browsable(false)]
		[DisplayName("Weekdays"), Type(typeof(int[])), ControlType(ReportParametersUtils.WeekDaysPicker, "WeekDaysPicker"), Order(256)]
		public int[] Weekdays { get; set; }

		[Serializable]
		private class FuelCost
		{
			public int     FuelTypeID;
			public decimal Value;
		}

		// Хранение параметров отчетов в БД подразумевает сериализацию в SOAP, для которой не поддерживается сериализация generics
		private FuelCost[] _fuelCosts;

		[Browsable(false)]
		[DisplayName("FuelCosts"), Type(typeof(Dictionary<int, decimal>)), ControlType(ReportParametersUtils.FuelCostsPicker, "FuelCostsPicker"), Order(3)]
		public Dictionary<int, decimal> FuelCosts
		{
			get
			{
				if (_fuelCosts == null)
					return new Dictionary<int, decimal>();

				return _fuelCosts
					.Where(fuelCost => fuelCost != null)
					.ToDictionary(fuelCost => fuelCost.FuelTypeID, fuelCost => fuelCost.Value);
			}
			set
			{
				_fuelCosts = value
					.ToList()
					.ConvertAll(pair => new FuelCost
					{
						FuelTypeID = pair.Key,
						Value      = pair.Value
					})
					.ToArray();
			}
		}

		/// <summary> Группа ТС, для которой формируется отчет </summary>
		[DisplayName("TSGroupDN", 6, true)]
		[Category("ReportParsCat")]
		[Description("TSGroupDesc")]
		public TagListBoxItem VehicleGroupProp
		{
			get;
			set;
		}

		/// <summary> Валюта, для которой строится отчет </summary>
		[Browsable(false)]
		public string Currency { get; set; }

		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupPicker"), Order(1)]
		public int VehicleGroupId
		{
			get
			{
				return (int)VehicleGroupProp.Tag;
			}
			set
			{
				VehicleGroupProp = new TagListBoxItem
				{
					Tag = value
				};
			}
		}

		string _departmentCountryName = "";
		/// <summary> Страна для которой строится отчет </summary>
		[Browsable(false)]
		public string departmentCountryName
		{
			get { return string.IsNullOrEmpty(_departmentCountryName) ? "" : _departmentCountryName; }
			set { _departmentCountryName = departmentCountryName; }
		}

		private WorkingHoursInterval _workingHoursInterval;

		[DisplayName("WorkingHoursInterval"), Type(typeof(WorkingHoursInterval))]
		[ControlType(ReportParametersUtils.WorkingHoursInterval, "WorkingHoursInterval"), Order(257)]
		public WorkingHoursInterval WorkingHoursInterval
		{
			get { return _workingHoursInterval; }
			set { _workingHoursInterval = value; }
		}
	}
}