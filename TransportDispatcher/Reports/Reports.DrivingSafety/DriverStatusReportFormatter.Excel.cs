﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.DrivingSafety
{
	public partial class DriverStatusReportFormatter
	{
		private void ExportReportAsExcelFile(string fileName)
		{
			ReportExcelExtensions.CreateWorkbook()
				.WithStyles()
				.CreateWorksheet(_worksheetName)
				.CreateTable(_cells)
				.WithReportHeaderRow(_cells, _reportStrings[$"Report"],   _reportStrings[$"{nameof(DriverStatusReport)}_Name"])
				.WithReportHeaderRow(_cells, _reportStrings[$"dateFrom"], $"{new DateTimeOffset(_data.LogTimeBeg.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")
				.WithReportHeaderRow(_cells, _reportStrings[$"dateTo"],   $"{new DateTimeOffset(_data.LogTimeEnd.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")
				.WithTableHeadRow(_cells)
				.With((xElement) =>
				{
					foreach (var reportRow in _data.Rows.OrderBy(r => r.Timestamp))
						xElement.WithTableBodyRow(_cells, reportRow);
				})
				.Root()
				.Build()
				.Save(fileName);
		}
	}
}