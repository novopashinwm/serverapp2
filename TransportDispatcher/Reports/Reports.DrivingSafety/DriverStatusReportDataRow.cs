﻿using System;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.Enums;

namespace Compass.Ufin.Reports.DrivingSafety
{
	/// <summary> Запись отчета, событие о нарушении  </summary>
	[Serializable]
	[DataContract]
	public class DriverStatusReportDataRow
	{
		/// <summary> Метка времени </summary>
		[DataMember] public DateTimeOffset          Timestamp         { get; set; }
		/// <summary> Объект наблюдения </summary>
		[DataMember] public string                  ObjectName        { get; set; }
		/// <summary> Водитель </summary>
		[DataMember] public string                  DriverName        { get; set; }
		/// <summary> Тип нарушения режима вождения </summary>
		[DataMember] public ViolationOfDrivingType? ViolationType     { get; set; }
		/// <summary> Ссылка на видео нарушения </summary>
		[DataMember] public string                  ViolationVideoUrl { get; set; }
	}
}