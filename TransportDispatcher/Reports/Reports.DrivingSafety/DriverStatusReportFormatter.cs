﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.DrivingSafety
{
	public sealed partial class DriverStatusReportFormatter : IReportClass
	{
		private readonly DriverStatusReport           _report;
		private readonly DriverStatusReportParameters _params;
		private readonly ResourceStringContainer      _reportStrings;
		private readonly ResourceContainers           _commonStrings;
		private readonly DriverStatusReportData       _data;
		private readonly string                       _worksheetName;
		private readonly List<CellInfo>               _cells;

		public DriverStatusReportFormatter(
			DriverStatusReport           report,
			DriverStatusReportParameters reportParameters,
			DriverStatusReportData       data)
		{
			// Входные параметры
			_report    = report;
			_params    = reportParameters;
			_data      = data;
			// Производные параметры
			_reportStrings = report.GetResourceStringContainer(_params.Culture);
			_commonStrings = ResourceContainers.Get(_params.Culture);
			// Заполняем имя листа отчета из ресурса
			_worksheetName = _reportStrings[typeof(DriverStatusReport).Name + "_SheetName"];
			// Параметры форматирования отчета
			_cells   = new List<CellInfo>()
			{
				new CellInfo
				{
					Key       = $@"{nameof(DriverStatusReportDataRow)}_{nameof(DriverStatusReportDataRow.Timestamp)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => ((DriverStatusReportDataRow)x).Timestamp.ToString(),
				},
				new CellInfo
				{
					Key       = $@"{nameof(DriverStatusReportDataRow)}_{nameof(DriverStatusReportDataRow.DriverName)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => ((DriverStatusReportDataRow)x).DriverName,
				},
				new CellInfo
				{
					Key       = $@"{nameof(DriverStatusReportDataRow)}_{nameof(DriverStatusReportDataRow.ObjectName)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => ((DriverStatusReportDataRow)x).ObjectName,
				},
				new CellInfo
				{
					Key       = $@"{nameof(DriverStatusReportDataRow)}_{nameof(DriverStatusReportDataRow.ViolationType)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => _reportStrings[$@"{nameof(ViolationOfDrivingType)}_{((DriverStatusReportDataRow)x).ViolationType}"],
				},
				new CellInfo
				{
					Key       = $@"{nameof(DriverStatusReportDataRow)}_{nameof(DriverStatusReportDataRow.ViolationVideoUrl)}",
					ExcelType = CellInfo.ExcelTypes.HyperLink,
					GetValue  = x => ((DriverStatusReportDataRow)x).ViolationVideoUrl,
					Width     = 3,
				},
			};
			foreach (var cell in _cells)
				if (string.IsNullOrWhiteSpace(cell.Name))
					cell.Name = _reportStrings[cell.Key];
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					ExportReportAsExcelFile(fileName);
					break;
				case ReportTypeEnum.Acrobat:
					ExportReportAsAcrobatFile(fileName);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(formatType));
			}
		}
		public void Dispose() { }
	}
}