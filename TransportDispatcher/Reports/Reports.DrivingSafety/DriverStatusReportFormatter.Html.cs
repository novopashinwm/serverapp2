﻿using System;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.DrivingSafety
{
	public partial class DriverStatusReportFormatter
	{
		public string GetReportAsHtmlString()
		{
			var html = (XNamespace)"http://www.w3.org/1999/xhtml";
			// Если нет данных, пишем об отсутствии и выходим
			if (_data.Rows.Length == 0)
				return (new XElement(html + "span", _reportStrings["NoData"])).ToString();

			// Формирование отчета (с заголовком с именем отчета)
			var xdiv = new XElement(html + "div");
			xdiv.Add(new XElement(html + "h1", _reportStrings[$"{nameof(DriverStatusReport)}_Name"]));
			var rbody = new XElement(html + "thead",
				new XElement("tr",
					new XElement("td", _reportStrings[$"dateFrom"]),
					new XElement("td", $"{new DateTimeOffset(_data.LogTimeBeg.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")),
				new XElement("tr",
					new XElement("td", _reportStrings[$"dateTo"]),
					new XElement("td", $"{new DateTimeOffset(_data.LogTimeEnd.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}"))
			);
			xdiv.Add(new XElement(html + "table",
				new XAttribute("class", "reportHeader"),
				new XAttribute("style", "table-layout: auto !important;"),
					rbody));

			// Заголовок таблицы
			var thead = new XElement(html + "thead",
				// Строка названий колонок в заголовке таблицы
				new XElement("tr",
					_cells.Select(c =>
						new XElement("td",
							new XAttribute("style", $@"width:auto;text-align:center;font-weight:bold;"),
							c.Name
						)
					)
				)
			);
			// Данные таблицы
			var tbody = new XElement(html + "tbody");
			foreach (var reportRow in _data.Rows.OrderBy(r => r.Timestamp))
			{
				tbody.Add(
					new XElement("tr",
						_cells.Select(c =>
							new XElement("td",
								new XAttribute("style", $@"width:auto;text-align:{GetTextAlign(c)};font-weight:normal;"),
								c.ExcelType != CellInfo.ExcelTypes.HyperLink
									? c.GetValue(reportRow)
									: new XElement("a",
										new XAttribute("href", c.GetValue(reportRow)),
										new XAttribute("target", "_blank"),
										c.GetValue(reportRow))
							)
						)
					)
				);
			}
			xdiv.Add(new XElement(html + "table",
				new XAttribute("class", "reportTable withBorders"),
					thead, tbody));
			return xdiv.ToString();
		}
		private string GetTextAlign(CellInfo cellInfo)
		{
			switch (cellInfo.ExcelType)
			{
				case CellInfo.ExcelTypes.DateTime:
				case CellInfo.ExcelTypes.TimeSpan:
				case CellInfo.ExcelTypes.HyperLink:
					return "center";
				case CellInfo.ExcelTypes.Number:
					return "right";
				case CellInfo.ExcelTypes.String:
				default:
					return "left";
			}
		}
	}
}