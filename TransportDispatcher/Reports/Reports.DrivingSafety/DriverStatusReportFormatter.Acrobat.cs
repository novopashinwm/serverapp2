﻿using System;
using System.IO;
using FORIS.TSS.TransportDispatcher.Reports;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Compass.Ufin.Reports.DrivingSafety
{
	public partial class DriverStatusReportFormatter
	{
		private void ExportReportAsAcrobatFile(string fileName)
		{
			throw new NotImplementedException();
			var fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			var fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);
			using (var stream = new FileStream(fileName, FileMode.Create))
			{
				using (var document = new Document(PageSize.A4))
				{
					var pdfWriter = PdfWriter.GetInstance(document, stream);
					pdfWriter.PageEvent = new PdfReportPageEvents();
					document.AddTitle(_reportStrings[$"{nameof(DriverStatusReport)}_Name"]); // необходимо до открытия документа
					document.Open();
					document.NewPage();
					//document.Add(CreateHeaderTable());
				}
			}
		}
	}
}