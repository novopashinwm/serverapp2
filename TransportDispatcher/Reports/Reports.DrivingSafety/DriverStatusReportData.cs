﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.DTO;

namespace Compass.Ufin.Reports.DrivingSafety
{
	[Serializable]
	[DataContract]
	public class DriverStatusReportData
	{
		[DataMember] public List<Vehicle>               Vehicles   { get; set; }
		[DataMember] public int                         LogTimeBeg { get; set; }
		[DataMember] public int                         LogTimeEnd { get; set; }
		[DataMember] public DriverStatusReportDataRow[] Rows       { get; set; } = Array.Empty<DriverStatusReportDataRow>();
	}
}