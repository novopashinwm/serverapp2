﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;
using Dto = FORIS.TSS.BusinessLogic.DTO;

namespace Compass.Ufin.Reports.DrivingSafety
{
	/// <summary> Отчет "Контроль водителя" </summary>
	[Guid("3DA8B483-F065-46F2-87D1-2828A50DF010")]
	public sealed partial class DriverStatusReport : TssReportBase
	{
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(DriverStatusReport) + "_Name"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(DriverStatusReport) + "_Desc"];
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.DrivingSafety.ToString();
		}
		public override int Order => 1;
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Html;
			yield return ReportTypeEnum.Excel;
			//yield return ReportTypeEnum.Acrobat;
		}
		public override ResourceStringContainer GetResourceStringContainer(CultureInfo culture)
		{
			return new ResourceStringContainer(culture, $"{GetType().Namespace}.{nameof(DriverStatusReport)}.Strings", GetType().Assembly);
		}
		public override IReportClass Create(ReportParameters reportParameters)
		{
			// Получение данных и создание объекта форматирования отчёта для экспорта в файл
			return GetFormatter(reportParameters);
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			return GetFormatter(reportParameters)?.GetReportAsHtmlString();
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new DriverStatusReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new DriverStatusReportParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (DriverStatusReportParameters)iReportParameters;

				// Если есть набор идентификаторов, то очищаем идентификатор группы и считаем, что есть произвольный набор машин
				if (0 < (reportParameters.VehicleIds?.Count ?? 0))
					reportParameters.VehicleGroupId = -1;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return null;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var reportParameters = iReportParameters as DriverStatusReportParameters;
			if (reportParameters == null)
				return false;

			// Проверяем базовые правила
			var checkResult = base.ReportParametersInstanceCheck(iReportParameters);

			// Проверяем параметры получения списка объектов наблюдения
			if (0 >= reportParameters.VehicleGroupId && 0 == (reportParameters.VehicleIds?.Count ?? 0))
				checkResult.AddWrongArgument(nameof(reportParameters.VehicleIds), "Vehicles is not selected");

			return checkResult;
		}
		private DriverStatusReportFormatter GetFormatter(ReportParameters reportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = reportParameters as DriverStatusReportParameters;
			if (parameters == null)
				return default;

			// Получение данных и создание объекта форматирования отчёта
			return new DriverStatusReportFormatter(this, parameters, GetReportData(parameters));
		}
		private DriverStatusReportData GetReportData(DriverStatusReportParameters reportParameters)
		{
			// Проверка параметров
			if (null == reportParameters)
				return default;
			// Формируем параметры интервала отчета
			var logTimeBeg = reportParameters.ToUtc(reportParameters.DateTimeInterval.DateFrom).ToLogTime();
			var logTimeEnd = reportParameters.ToUtc(reportParameters.DateTimeInterval.DateTo  ).ToLogTime();
			var logTimeNow = DateTime.UtcNow.ToLogTime();
			// Корректируем будущее
			if (logTimeEnd > logTimeNow)
				logTimeEnd = logTimeNow;
			//////////////////////////////
			var requiredVehicleRights = new[] { DriverStatusReportParameters.RequiredVehicleRight };
			//////////////////////////////
			// Заполняем данные из БД
			var data = new DriverStatusReportData
			{
				// Интервал отчёта
				LogTimeBeg = logTimeBeg,
				LogTimeEnd = logTimeEnd,
				// Объекты наблюдения
				Vehicles = reportParameters.VehicleIds != null && reportParameters.VehicleIds.Count != 0
					? mclsInstance.GetVehiclesWithPositions(
						reportParameters.OperatorId, null, new GetVehiclesArgs
						{
							VehicleIDs = reportParameters.VehicleIds.ToArray(),
							Culture    = reportParameters.Culture
						})
						.Where(v => requiredVehicleRights.All(r => v?.rights?.Contains(r) ?? false))
						.ToList()
					: mclsInstance.GetVehiclesByVehicleGroup(
						reportParameters.VehicleGroupId,
						reportParameters.OperatorId,
						reportParameters.Culture,
						requiredVehicleRights),
			};
			// Записи отчёта
			data.Rows = mclsInstance.GetMessagesByOperator(
				reportParameters.OperatorId,
				default,
				data.LogTimeBeg.ToUtcDateTime(),
				data.LogTimeEnd.ToUtcDateTime(),
				new Pagination { Page = 1, RecordsPerPage = int.MaxValue },
				data.Vehicles
					.Select(v => v.id)
					.ToArray())
				.Where(m => m.Template?.Name == Dto::Messages.MessageTemplate.EventViolationOfDriving)
				.Select(m => new DriverStatusReportDataRow
				{
					Timestamp         = DateTimeOffset.TryParse(m.Fields?.FirstOrDefault(f => f.Template?.Name == nameof(EventViolationOfDrivingData.EventTimeUtc))?.Value, out var eventTime)
						? eventTime.ToOffset(reportParameters.TimeZoneInfo.BaseUtcOffset)
						: new DateTimeOffset(m.Time).ToOffset(reportParameters.TimeZoneInfo.BaseUtcOffset),
					ObjectName        = m.Fields?.FirstOrDefault(f => f.Template?.Name == "VehicleName")?.Value,
					DriverName        = m.Fields?.FirstOrDefault(f => f.Template?.Name == nameof(EventViolationOfDrivingData.DriverName))?.Value,
					ViolationType     = Enum.TryParse<ViolationOfDrivingType>(m.Fields?.FirstOrDefault(f => f.Template?.Name == nameof(EventViolationOfDrivingData.EventType))?.Value, out var eventType)
						? eventType
						: default(ViolationOfDrivingType?),
					ViolationVideoUrl = m.Fields?.FirstOrDefault(f => f.Template?.Name == nameof(EventViolationOfDrivingData.EventVideoUrl))?.Value,
				})
				.OrderByDescending(m => m.Timestamp).ThenBy(m => m.DriverName)
				.ToArray();

			return data;
		}
	}
}