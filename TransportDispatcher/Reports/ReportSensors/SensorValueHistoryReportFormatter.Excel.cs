﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public partial class SensorValueHistoryReportFormatter
	{
		private void ExportReportAsExcelFile(string fileName)
		{
			ReportExcelExtensions.CreateWorkbook()
				.WithStyles()
				.CreateWorksheet(_worksheetName)
				.CreateTable(_cells)
				.WithReportHeaderRow(_cells, _reportStrings[$"Report"],                                         _reportStrings[$"{nameof(SensorValueHistoryReport)}_Name"])
				.WithReportHeaderRow(_cells, _reportStrings[$"{nameof(SensorValueHistoryReport)}_VehicleName"], $"{_data.Vehicle.Name}")
				.WithReportHeaderRow(_cells, _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorName"],  $"{_data.Sensor.Name}")
				.WithReportHeaderRow(_cells, _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorType"],  _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorType_{_data.Sensor.Type}"])
				.WithReportHeaderRow(_cells, _reportStrings[$"{nameof(SensorValueHistoryReport)}_Interval"],    $"{_params.Interval}")
				.WithReportHeaderRow(_cells, _reportStrings[$"dateFrom"],                                       $"{new DateTimeOffset(_data.LogTimeBeg.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")
				.WithReportHeaderRow(_cells, _reportStrings[$"dateTo"],                                         $"{new DateTimeOffset(_data.LogTimeEnd.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")
				.WithTableHeadRow(_cells)
				.With((xElement) =>
				{
					foreach (var reportRow in _data.Rows.OrderBy(r => r.Timestamp))
						xElement.WithTableBodyRow(_cells, reportRow);
				})
				.Root()
				.Build()
				.Save(fileName);
		}
	}
}