﻿using System;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	[DataContract]
	public class SensorValueHistoryReportData
	{
		[DataMember] public Vehicle                           Vehicle    { get; set; }
		[DataMember] public FullLog                           FullLog    { get; set; }
		[DataMember] public int                               LogTimeBeg { get; set; }
		[DataMember] public int                               LogTimeEnd { get; set; }
		[DataMember] public Sensor                            Sensor     { get; set; }
		[DataMember] public SensorValueHistoryReportDataRow[] Rows       { get; set; } = Array.Empty<SensorValueHistoryReportDataRow>();
	}
}