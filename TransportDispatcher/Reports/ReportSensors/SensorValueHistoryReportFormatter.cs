﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public sealed partial class SensorValueHistoryReportFormatter : IReportClass
	{
		private readonly SensorValueHistoryReport           _report;
		private readonly SensorValueHistoryReportParameters _params;
		private readonly ResourceStringContainer            _reportStrings;
		private readonly ResourceContainers                 _commonStrings;
		private readonly SensorValueHistoryReportData       _data;
		private readonly string                             _worksheetName;
		private readonly List<CellInfo>                     _cells;

		public SensorValueHistoryReportFormatter(
			SensorValueHistoryReport           report,
			SensorValueHistoryReportParameters reportParameters,
			SensorValueHistoryReportData       data)
		{
			// Входные параметры
			_report    = report;
			_params    = reportParameters;
			_data      = data;
			// Производные параметры
			_reportStrings = report.GetResourceStringContainer(_params.Culture);
			_commonStrings = ResourceContainers.Get(_params.Culture);
			// Заполняем имя листа отчета из ресурса
			_worksheetName = _reportStrings[typeof(SensorValueHistoryReport).Name + "_SheetName"];
			// Локализуем датчик
			_data.Sensor = data.Sensor?.LocalizeSensorFull(_commonStrings);
			// Параметры форматирования отчета
			_cells   = new List<CellInfo>()
			{
				new CellInfo
				{
					Key       = $@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.Timestamp)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => ((SensorValueHistoryReportDataRow)x).Timestamp.ToString(),
				},
				new CellInfo
				{
					Key       = $@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.Value)}",
					ExcelType = (_data.Sensor?.Type ?? SensorType.Analog) == SensorType.Digital
						? CellInfo.ExcelTypes.String
						: CellInfo.ExcelTypes.Number,
					GetValue  = x => ((SensorValueHistoryReportDataRow)x).Value.HasValue
						? (_data.Sensor?.Type ?? SensorType.Analog) == SensorType.Digital
							? _data.Sensor?.GetDisplayedValue(((SensorValueHistoryReportDataRow)x).Value.Value, _commonStrings)
							: ((SensorValueHistoryReportDataRow)x).Value?.ToString("0.00", NumberHelper.FormatInfo)
						: string.Empty,
					Name      = _reportStrings[$@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.Value)}"]
						+ $"{(!string.IsNullOrWhiteSpace(_data.Sensor.Unit) ? $", {_data.Sensor.Unit}" : "")}"
				},
				new CellInfo
				{
					Key       = $@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.Distance)}",
					ExcelType = CellInfo.ExcelTypes.Number,
					GetValue  = x => ((SensorValueHistoryReportDataRow)x).Distance?.ToString("0.00", NumberHelper.FormatInfo),
				},
				new CellInfo
				{
					Key       = $@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.TotalRun)}",
					ExcelType = CellInfo.ExcelTypes.Number,
					GetValue  = x => ((SensorValueHistoryReportDataRow)x).TotalRun?.ToString("0.00", NumberHelper.FormatInfo),
				},
				new CellInfo
				{
					Key       = $@"{nameof(SensorValueHistoryReportDataRow)}_{nameof(SensorValueHistoryReportDataRow.Address)}",
					ExcelType = CellInfo.ExcelTypes.String,
					GetValue  = x => ((SensorValueHistoryReportDataRow)x).Address,
					Width     = 3,
				},
			};
			foreach (var cell in _cells)
				if (string.IsNullOrWhiteSpace(cell.Name))
					cell.Name = _reportStrings[cell.Key];
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					ExportReportAsExcelFile(fileName);
					break;
				case ReportTypeEnum.Acrobat:
					ExportReportAsAcrobatFile(fileName);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(formatType));
			}
		}
		public void Dispose() { }
	}
}