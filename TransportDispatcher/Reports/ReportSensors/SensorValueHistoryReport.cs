﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Resources;
using Interfaces.Geo;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> ИСТОРИЯ ЗНАЧЕНИЙ ДАТЧИКА </summary>
	[Guid("E1A5C8C8-195F-4F28-BD24-9D9083E949BE")]
	public sealed partial class SensorValueHistoryReport : TssReportBase
	{
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(SensorValueHistoryReport) + "_Name"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(SensorValueHistoryReport) + "_Desc"];
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.SensorsReports.ToString();
		}
		/// <summary> Порядок отчета </summary>
		public override int Order => 1;
		/// <summary> Поддерживаемые форматы отчета </summary>
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Html;
			yield return ReportTypeEnum.Excel;
		}
		public override ResourceStringContainer GetResourceStringContainer(CultureInfo culture)
		{
			return new ResourceStringContainer(culture, $"{GetType().Namespace}.{nameof(SensorValueHistoryReport)}.Strings", GetType().Assembly);
		}
		public override IReportClass Create(ReportParameters reportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = reportParameters as SensorValueHistoryReportParameters;
			if (parameters == null)
				return default;

			// Получение данных и создание объекта форматирования отчёта для экспорта в файл
			return new SensorValueHistoryReportFormatter(this, parameters, GetReportData(parameters));
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = reportParameters as SensorValueHistoryReportParameters;
			if (parameters == null)
				return default;

			// Получение данных и создание объекта форматирования отчёта
			using var formatter = new SensorValueHistoryReportFormatter(this, parameters, GetReportData(parameters));
			// Формирование отчета
			return formatter.GetReportAsHtmlString();
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new SensorValueHistoryReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new SensorValueHistoryReportParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (SensorValueHistoryReportParameters)iReportParameters;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return null;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var reportParameters = iReportParameters as SensorValueHistoryReportParameters;
			if (reportParameters == null)
				return false;

			// Проверяем базовые правила
			var checkResult = base.ReportParametersInstanceCheck(iReportParameters);

			var dateFrom = iReportParameters.DateTimeInterval.DateFrom;
			var dateTo   = iReportParameters.DateTimeInterval.DateTo;

			// Общая длительность интервала
			//if ((dateTo - dateFrom).TotalDays > 3)
			//	checkResult.AddWrongArgument("DateTimeInterval", "DateTimeInterval cannot be greater 3 days");

			// Объект наблюдения
			if (0 >= reportParameters.VehicleId)
				checkResult.AddWrongArgument("VehicleId", "Vehicle is not selected");

			// Производный датчик
			if (!Enum.TryParse<SensorLegend>(reportParameters.SensorId.ToString(), false, out _))
				checkResult.AddWrongArgument("Sensor", "Sensor is not selected");

			return checkResult;
		}
		private SensorValueHistoryReportData GetReportData(SensorValueHistoryReportParameters reportParameters)
		{
			// Проверка параметров
			if (null == reportParameters)
				return default;
			if (!Enum.TryParse<SensorLegend>(reportParameters.SensorId.ToString(), false, out var sensorLegend))
				return default;
			// Формируем параметры интервала отчета
			var logTimeBeg = reportParameters.ToUtc(reportParameters.DateTimeInterval.DateFrom).ToLogTime();
			var logTimeEnd = reportParameters.ToUtc(reportParameters.DateTimeInterval.DateTo  ).ToLogTime();
			var logTimeNow = DateTime.UtcNow.ToLogTime();
			// Корректируем будущее
			if (logTimeEnd > logTimeNow)
				logTimeEnd = logTimeNow;
			// Заполняем данные из БД
			var data = new SensorValueHistoryReportData
			{
				// Интервал отчета
				LogTimeBeg = logTimeBeg,
				LogTimeEnd = logTimeEnd,
				// Объект наблюдения
				Vehicle = mclsInstance.GetVehicleById(
					reportParameters.OperatorId,
					reportParameters.VehicleId,
					reportParameters.Culture),
				// История
				FullLog = mclsInstance.GetFullLog(
					reportParameters.OperatorId,
					reportParameters.VehicleId,
					logTimeBeg,
					logTimeEnd,
					new FullLogOptions
					{
						CutOutstandingData  = false,
						IsGeoPointsFilterOn = false,
						MaxTotalPointCount  = logTimeEnd - logTimeBeg,
						MaxGeoPointsCount   = logTimeEnd - logTimeBeg,
						MaxPointsCount      = logTimeEnd - logTimeBeg,
					}),
			};
			// Обрабатываем полученные данные
			data.Sensor = data.FullLog?.Sensors?.GetValueOrDefault(sensorLegend)
				?? data.Vehicle?.Sensors?.FirstOrDefault(s => s.Number == sensorLegend)
				?? sensorLegend.GetDefaultSensor(ResourceContainers.Get(reportParameters.Culture));
			if (0 == (data.Sensor?.LogRecords?.Count ?? 0))
				return data;
			var logTimes =
			(
				0 != reportParameters.Interval
				? Enumerable.Range(0, (logTimeEnd - logTimeBeg) / reportParameters.Interval + 1)
					?.Select(i => logTimeBeg + i * reportParameters.Interval)
				: data.Sensor
					?.LogRecords
					?.Select(l => l.LogTime)
			)
				?.Union(new[] { logTimeBeg, logTimeEnd })
				?.Where(t => logTimeBeg <= t && t <= logTimeEnd)
				?.OrderBy(v => v)
				?.ToArray();
			data.Rows = logTimes.Skip(1).Zip(logTimes, (curr, prev) => new { Beg = prev, End = curr })
				.Select(interval =>
				{
					//////////////////////////////////////////////////////////////////////
					/// Обработка полученных данных
					//////////////////////////////////////////////////////////////////////
					var geoPoint = data.FullLog?.GetCurrentOrPreviousGeoRecord(interval.Beg);
					var valPoint = (data.Sensor?.Type ?? SensorType.Analog) == SensorType.Digital
						// Для дискретных датчиков всегда берем последнее значение (т.к. обычно приходит только при изменении)
						? data.Sensor
							?.LogRecords
							?.Where(r => logTimeBeg <= r.LogTime && r.LogTime < interval.End)
							?.OrderBy(r => r.LogTime)
							?.Select(v => (decimal?)v.Value)
							?.LastOrDefault()
						// Для аналоговых берем среднее на интервале
						: data.Sensor
							?.LogRecords
							?.Where(r => interval.Beg <= r.LogTime && r.LogTime < interval.End)
							?.Select(v => (decimal?)v.Value)
							?.Average(v => v);
					return new SensorValueHistoryReportDataRow
					{
						Timestamp = new DateTimeOffset(interval.Beg.ToUtcDateTime()).ToOffset(reportParameters.TimeZoneInfo.BaseUtcOffset),
						Duration  = TimeSpan.FromSeconds(interval.End - interval.Beg),
						Distance  = data.FullLog.GetRunDistance(interval.Beg, interval.End) / 1000m,
						TotalRun  = data.FullLog.GetRunDistance(logTimeBeg, interval.End) / 1000m,
						Lat       = geoPoint?.Lat,
						Lng       = geoPoint?.Lng,
						Value     = valPoint,
					};
				})
				.ToArray();
			data.Rows = FillAddresses(reportParameters, data.Rows)?.ToArray();
			return data;
		}
		private IEnumerable<SensorValueHistoryReportDataRow> FillAddresses(SensorValueHistoryReportParameters parameters, IEnumerable<SensorValueHistoryReportDataRow> reportRows)
		{
			var requests = reportRows
				?.Where(r => r.Lat.HasValue && r.Lng.HasValue)
				?.GroupBy(r => new { r.Lat, r.Lng })
				?.Select((itm, idx) => new { idx, itm });
			if (0 == (requests?.Count() ?? 0))
				return reportRows;

			var addresses = mclsInstance.GetAddresses(
				requests
					.Select(r => new AddressRequest(
						r.idx,
						lat: (double)r.itm.Key.Lat.Value,
						lng: (double)r.itm.Key.Lng.Value))
					.ToArray(),
				parameters.Culture,
				parameters.MapGuid)
				?.ToDictionary(k => k.Id);
			if (0 == (addresses?.Count() ?? 0))
				return reportRows;

			foreach (var request in requests)
			{
				if (addresses.TryGetValue(request.idx, out AddressResponse addressResponse))
				{
					foreach (var row in request.itm)
						row.Address = addressResponse.Address;
				}
			}
			return reportRows;
		}
	}
}