﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class ReportSensorsParameters : ReportParameters, IMonitoreeObjectContainer
	{
		protected TagListBoxItem VehicleProp = new TagListBoxItem("", null);

		public IdType MonitoreeObjectIdType
		{
			get { return IdType.Vehicle; }
		}

		public int MonitoreeObjectId
		{
			get
			{
				if (VehicleProp.Tag == null)
					VehicleProp.Tag = VehicleGroupId;
				return (int)VehicleProp.Tag;
			}
		}

		[DisplayName("SensorId")]
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.SensorPicker, "ChooseSensorLabel")]
		[Options("SensorType:" + nameof(SensorType.Digital))]
		public int SensorId { get; set; }

		[DisplayName("VehicleGroupId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehicleGroupPicker, "VehicleGroupName")]
		public int VehicleGroupId { get; set; }

		[DisplayName("IsDetailed"), Type(typeof(bool)), ControlType(ReportParametersUtils.Boolean, "IsDetailed")]
		public Boolean IsDetailed { get; set; }

		[DisplayName("IsGroupedByDays"), Type(typeof(bool)), ControlType(ReportParametersUtils.Boolean, "IsGroupedByDays")]
		public Boolean IsGroupedByDays { get; set; }

		private int[] _vehicleIds;

		/// <summary> Список объектов наблюдения, чтобы строить отчёт групповой отчёт не создавая группу </summary>
		[DisplayName("VehicleIds"), Type(typeof(List<int>)), ControlType(ReportParametersUtils.VehiclesPicker, "VehiclesPicker")]
		[Order(2)]
		[XmlIgnore]
		public List<int> VehicleIds { get { return _vehicleIds?.ToList(); } set { _vehicleIds = value?.ToArray(); } }
	}
}