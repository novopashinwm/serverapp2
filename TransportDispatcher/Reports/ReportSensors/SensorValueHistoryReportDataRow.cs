﻿using System;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Запись отчета, представляющая собой интервал, на котором вычислено значение датчика в зависимости от типа </summary>
	/// <remarks> Если тип датчика <see cref="SensorType.Digital"/>, то значение вычисляется по мажоритарному принципу </remarks>
	/// <remarks> Если тип датчика <see cref="SensorType.Analog"/>, то значение вычисляется как среднее на интервале </remarks>
	[Serializable]
	[DataContract]
	public class SensorValueHistoryReportDataRow
	{
		/// <summary> Метка времени начала интервала </summary>
		[DataMember] public DateTimeOffset  Timestamp { get; set; }
		/// <summary> Длина интервала </summary>
		[DataMember] public TimeSpan        Duration  { get; set; }
		/// <summary> Значение постоянное для интервала (вычисляется в зависимости от типа) </summary>
		[DataMember] public decimal?        Value     { get; set; }
		/// <summary> Пробег на этом интервале </summary>
		[DataMember] public decimal?        Distance  { get; set; }
		/// <summary> Нарастающий итог пробега от начала отчёта </summary>
		[DataMember] public decimal?        TotalRun  { get; set; }
		[DataMember] public decimal?        Lat       { get; set; }
		[DataMember] public decimal?        Lng       { get; set; }
		[DataMember] public string          Address   { get; set; }
	}
}