﻿using System;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public partial class SensorValueHistoryReportFormatter
	{
		public string GetReportAsHtmlString()
		{
			var html = (XNamespace)"http://www.w3.org/1999/xhtml";
			// Если нет данных, пишем об отсутствии и выходим
			if (!(_data.Rows?.Any() ?? false))
				return (new XElement(html + "span", _reportStrings["NoData"])).ToString();

			// Формирование отчета (с заголовком с именем отчета)
			var xdiv = new XElement(html + "div");
			xdiv.Add(new XElement(html + "h1", _reportStrings[$"{nameof(SensorValueHistoryReport)}_Name"]));
			var rbody = new XElement(html + "thead",
				new XElement("tr",
					new XElement("td", _reportStrings[$"{nameof(SensorValueHistoryReport)}_VehicleName"]),
					new XElement("td", $"{_data.Vehicle.Name}")),
				new XElement("tr",
					new XElement("td", _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorName"]),
					new XElement("td", $"{_data.Sensor.Name}")),
				new XElement("tr",
					new XElement("td", _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorType"]),
					new XElement("td", _reportStrings[$"{nameof(SensorValueHistoryReport)}_SensorType_{_data.Sensor.Type}"])),
				new XElement("tr",
					new XElement("td", _reportStrings[$"{nameof(SensorValueHistoryReport)}_Interval"]),
					new XElement("td", $"{_params.Interval}")),
				new XElement("tr",
					new XElement("td", _reportStrings[$"dateFrom"]),
					new XElement("td", $"{new DateTimeOffset(_data.LogTimeBeg.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}")),
				new XElement("tr",
					new XElement("td", _reportStrings[$"dateTo"]),
					new XElement("td", $"{new DateTimeOffset(_data.LogTimeEnd.ToUtcDateTime()).ToOffset(_params.TimeZoneInfo.BaseUtcOffset)}"))
			);
			xdiv.Add(new XElement(html + "table",
				new XAttribute("class", "reportHeader"),
				new XAttribute("style", "table-layout: auto !important;"),
					rbody));

			// Заголовок таблицы
			var thead = new XElement(html + "thead",
				// Строка названий колонок в заголовке таблицы
				new XElement("tr",
					_cells.Select(c =>
						new XElement("td",
							new XAttribute("style", $@"width:auto;text-align:center;font-weight:bold;"),
							c.Name
						)
					)
				)
			);
			// Данные таблицы
			var tbody = new XElement(html + "tbody");
			foreach (var reportRow in _data.Rows.OrderBy(r => r.Timestamp))
			{
				tbody.Add(
					new XElement("tr",
						_cells.Select(c =>
							new XElement("td",
								new XAttribute("style", $@"width:auto;text-align:{GetTextAlign(c)};font-weight:normal;"),
								c.GetValue(reportRow)
							)
						)
					)
				);
			}
			xdiv.Add(new XElement(html + "table",
				new XAttribute("class", "reportTable withBorders"),
					thead, tbody));
			return xdiv.ToString();
		}
		private string GetTextAlign(CellInfo cellInfo)
		{
			switch (cellInfo.ExcelType)
			{
				case CellInfo.ExcelTypes.DateTime:
					return "center";
				case CellInfo.ExcelTypes.Number:
					return "right";
				case CellInfo.ExcelTypes.String:
				default:
					return "left";
			}
		}
	}
}