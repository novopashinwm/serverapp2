﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Групповой отчет по одному дискретному датчику </summary>
	[Guid("78288D6E-6AD2-40B5-BE4C-ABDCDC6579A9")]
	public sealed class ReportSensorsSingle : TssReportBase
	{
		private const string ReportName        = "ReportSensorsSingle";
		private const string ReportDescription = "ReportSensorsSingleDescription";
		public override int Order
		{
			get { return 0; }
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.SensorsReports.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			try
			{
				var ds = GetReportDataSet((ReportSensorsParameters)iReportParameters);

				var customReport = new ReportSensorsExcelPdfReport();

				customReport.SetDataSource(ds);

				return customReport;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return null;
			}
		}
		/// <summary> Получить данные в виде DataSet для передачи в CrystalReports </summary>
		/// <param name="clsReportParameters"></param>
		/// <returns></returns>
		private DataSet GetReportDataSet(ReportSensorsParameters clsReportParameters)
		{
			var finalList = GetFinalList(clsReportParameters);
			var strings   = GetResourceStringContainer(clsReportParameters.Culture);

			var result = new DataSet();

			var hData = new DataTable("Legend");
			hData.Columns.Add(new DataColumn("reportLegendKey"));
			hData.Columns.Add(new DataColumn("reportLegendValue"));
			result.Tables.Add(hData);

			var hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["CaptionReport"];
			hRow["reportLegendValue"] = strings["ReportName"];

			if (clsReportParameters.VehicleIds == null || clsReportParameters.VehicleIds.Count == 0)
			{
				hData.Rows.Add(hRow);
				hRow = hData.NewRow();

				hRow["reportLegendKey"]   = strings["VehicleGroupName"];
				hRow["reportLegendValue"] = mclsInstance.GetVehicleGroupById(clsReportParameters.VehicleGroupId).name;
			}

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["ReportDateFrom"];
			hRow["reportLegendValue"] = clsReportParameters.DateFrom.ToString(TimeHelper.DefaultTimeFormatUpToDays);

			hData.Rows.Add(hRow);
			hRow = hData.NewRow();

			hRow["reportLegendKey"]   = strings["ReportDateTo"];
			hRow["reportLegendValue"] = clsReportParameters.DateTo.ToString(TimeHelper.DefaultTimeFormatUpToDays);

			hData.Rows.Add(hRow);

			var reportSensorsResult = finalList.FirstOrDefault();

			if (reportSensorsResult != null && (reportSensorsResult.CountOff + reportSensorsResult.CountOn) != 0)
			{
				hRow = hData.NewRow();

				hRow["reportLegendKey"]   = strings["SensorTypeTitle"];
				hRow["reportLegendValue"] = reportSensorsResult.SensorType == SensorType.Analog
					? strings["SensorTypeAnalog"]
					: strings["SensorTypeDigital"];

				hData.Rows.Add(hRow);


				hRow = hData.NewRow();

				hRow["reportLegendKey"]   = strings["SensorNameTitle"];
				hRow["reportLegendValue"] = reportSensorsResult.SensorName;

				hData.Rows.Add(hRow);
			}

			var dt = new DataTable("SensorCommonLegend");

			result.Tables.Add(dt);

			dt.Columns.AddRange(new[]
			{
				new DataColumn("VehicleName"),
				new DataColumn("TimeWhenRun"),
				new DataColumn("TimeWhenOnButNotRun"),
				new DataColumn("TimeWhenOnAndRun"),
				new DataColumn("TotalOn"),
				new DataColumn("TotalOff"),
				new DataColumn("TotalTimeOn"),
				new DataColumn("TotalTimeOff"),
				new DataColumn("TotalTimeOut"),
				new DataColumn("RunWhenOn"),
				new DataColumn("RunWhenOff")
			});

			var headerLegendRow = dt.NewRow();

			var sensorOn  = reportSensorsResult != null ? reportSensorsResult.SensorValueNames[1] : strings["SensorOn"];
			var sensorOff = reportSensorsResult != null ? reportSensorsResult.SensorValueNames[0] : strings["SensorOff"];

			headerLegendRow["VehicleName"]         = strings["VehicleNameTitle"];
			headerLegendRow["TimeWhenRun"]         = strings["TimeWhenRun"];
			headerLegendRow["TimeWhenOnButNotRun"] = string.Format(strings["TimeWhenOnButNotRun"], sensorOn).CapitalizeFirstChar();
			headerLegendRow["TimeWhenOnAndRun"]    = string.Format(strings["TimeWhenOnAndRun"], sensorOn).CapitalizeFirstChar();
			headerLegendRow["TotalOn"]             = string.Format(strings["TotalOn"], sensorOn).CapitalizeFirstChar();
			headerLegendRow["TotalOff"]            = string.Format(strings["TotalOff"], sensorOff).CapitalizeFirstChar();
			headerLegendRow["TotalTimeOn"]         = string.Format(strings["TotalTimeOn"], sensorOn).CapitalizeFirstChar();
			headerLegendRow["TotalTimeOff"]        = string.Format(strings["TotalTimeOff"], sensorOff).CapitalizeFirstChar();
			headerLegendRow["TotalTimeOut"]        = strings["TotalTimeOut"];
			headerLegendRow["RunWhenOn"]           = string.Format(strings["RunWhenOn"], sensorOn).CapitalizeFirstChar();
			headerLegendRow["RunWhenOff"]          = string.Format(strings["RunWhenOff"], sensorOff).CapitalizeFirstChar();

			dt.Rows.Add(headerLegendRow);

			dt = new DataTable("SensorCommonData");

			dt.Columns.AddRange(new[]
			{
				new DataColumn("VehicleName"),
				new DataColumn("TimeWhenRun"),
				new DataColumn("TimeWhenOnButNotRun"),
				new DataColumn("TimeWhenOnAndRun"),
				new DataColumn("TotalOn"),
				new DataColumn("TotalOff"),
				new DataColumn("TotalTimeOn"),
				new DataColumn("TotalTimeOff"),
				new DataColumn("TotalTimeOut"),
				new DataColumn("RunWhenOn"),
				new DataColumn("RunWhenOff")
			});

			result.Tables.Add(dt);

			foreach (var rsr in finalList)
			{
				var dataLegendRow = dt.NewRow();

				dataLegendRow["VehicleName"]         = rsr.VehicleName;
				dataLegendRow["TimeWhenRun"]         = TimeHelper.GetTimeSpanReadableShort(rsr.TimeWhenRun, clsReportParameters.Culture);
				dataLegendRow["TimeWhenOnButNotRun"] = TimeHelper.GetTimeSpanReadableShort(rsr.TimeWhenOnButNotRun, clsReportParameters.Culture);
				dataLegendRow["TimeWhenOnAndRun"]    = TimeHelper.GetTimeSpanReadableShort(rsr.TimeWhenOnAndRun, clsReportParameters.Culture);
				dataLegendRow["TotalOn"]             = rsr.CountOn.ToString(clsReportParameters.Culture);
				dataLegendRow["TotalOff"]            = rsr.CountOff.ToString(clsReportParameters.Culture);
				dataLegendRow["TotalTimeOn"]         = TimeHelper.GetTimeSpanReadableShort(rsr.TotalOn, clsReportParameters.Culture);
				dataLegendRow["TotalTimeOff"]        = TimeHelper.GetTimeSpanReadableShort(rsr.TotalOff, clsReportParameters.Culture);
				dataLegendRow["TotalTimeOut"]        = TimeHelper.GetTimeSpanReadableShort(rsr.TotalOut, clsReportParameters.Culture);
				dataLegendRow["RunWhenOn"]           = rsr.RunWhenOn.ToString(CultureInfo.InvariantCulture);
				dataLegendRow["RunWhenOff"]          = rsr.RunWhenOff.ToString(CultureInfo.InvariantCulture);

				dt.Rows.Add(dataLegendRow);
			}

			if (clsReportParameters.IsGroupedByDays)
			{
				var dtg = new DataTable("SensorGroupedLegend");
				result.Tables.Add(dtg);

				dtg.Columns.AddRange(new[]
				{
					new DataColumn("LogTime"),
					new DataColumn("VehicleName"),
					new DataColumn("TimeWhenRun"),
					new DataColumn("TimeWhenOnButNotRun"),
					new DataColumn("TimeWhenOnAndRun"),
					new DataColumn("TotalOn"),
					new DataColumn("TotalOff"),
					new DataColumn("TotalTimeOn"),
					new DataColumn("TotalTimeOff"),
					new DataColumn("TotalTimeOut"),
					new DataColumn("RunWhenOn"),
					new DataColumn("RunWhenOff")
				});

				var groupedLegendRow = dtg.NewRow();

				groupedLegendRow["LogTime"]             = strings["LogTime"];
				groupedLegendRow["VehicleName"]         = strings["VehicleNameTitle"];
				groupedLegendRow["TimeWhenRun"]         = strings["TimeWhenRun"];
				groupedLegendRow["TimeWhenOnButNotRun"] =
					string.Format(strings["TimeWhenOnButNotRun"], sensorOn).CapitalizeFirstChar();
				groupedLegendRow["TimeWhenOnAndRun"]    =
					string.Format(strings["TimeWhenOnAndRun"], sensorOn).CapitalizeFirstChar();
				groupedLegendRow["TotalOn"]             = string.Format(strings["TotalOn"], sensorOn).CapitalizeFirstChar();
				groupedLegendRow["TotalOff"]            = string.Format(strings["TotalOff"], sensorOff).CapitalizeFirstChar();
				groupedLegendRow["TotalTimeOn"]         = string.Format(strings["TotalTimeOn"], sensorOn).CapitalizeFirstChar();
				groupedLegendRow["TotalTimeOff"]        =
					string.Format(strings["TotalTimeOff"], sensorOff).CapitalizeFirstChar();
				groupedLegendRow["TotalTimeOut"]        = strings["TotalTimeOut"];
				groupedLegendRow["RunWhenOn"]           = string.Format(strings["RunWhenOn"], sensorOn).CapitalizeFirstChar();
				groupedLegendRow["RunWhenOff"]          = string.Format(strings["RunWhenOff"], sensorOff).CapitalizeFirstChar();

				dtg.Rows.Add(groupedLegendRow);

				dtg = new DataTable("SensorGroupedData");
				result.Tables.Add(dtg);

				dtg.Columns.AddRange(new[]
				{
					new DataColumn("LogTime"),
					new DataColumn("VehicleName"),
					new DataColumn("TimeWhenRun"),
					new DataColumn("TimeWhenOnButNotRun"),
					new DataColumn("TimeWhenOnAndRun"),
					new DataColumn("TotalOn"),
					new DataColumn("TotalOff"),
					new DataColumn("TotalTimeOn"),
					new DataColumn("TotalTimeOff"),
					new DataColumn("TotalTimeOut"),
					new DataColumn("RunWhenOn"),
					new DataColumn("RunWhenOff")
				});

				foreach (var fi in finalList)
				{
					foreach (var row in fi.GroupByDayValues)
					{
						var groupedDataRow = dtg.NewRow();

						groupedDataRow["LogTime"]             = row.Date.ToShortDateString();
						groupedDataRow["VehicleName"]         = fi.VehicleName;
						groupedDataRow["TimeWhenRun"]         = TimeHelper.GetTimeSpanReadableShort(row.TimeWhenRun, clsReportParameters.Culture);
						groupedDataRow["TimeWhenOnButNotRun"] = TimeHelper.GetTimeSpanReadableShort(row.TimeWhenOnButNotRun, clsReportParameters.Culture);
						groupedDataRow["TimeWhenOnAndRun"]    = TimeHelper.GetTimeSpanReadableShort(row.TimeWhenOnAndRun, clsReportParameters.Culture);
						groupedDataRow["TotalOn"]             = row.CountOn.ToString(clsReportParameters.Culture);
						groupedDataRow["TotalOff"]            = row.CountOff.ToString(clsReportParameters.Culture);
						groupedDataRow["TotalTimeOn"]         = TimeHelper.GetTimeSpanReadableShort(row.TotalOn, clsReportParameters.Culture);
						groupedDataRow["TotalTimeOff"]        = TimeHelper.GetTimeSpanReadableShort(row.TotalOff, clsReportParameters.Culture);
						groupedDataRow["TotalTimeOut"]        = TimeHelper.GetTimeSpanReadableShort(row.TotalOut, clsReportParameters.Culture);
						// Представление дробных чисел. Для xls.
						groupedDataRow["RunWhenOn"]           = row.RunWhenOn.ToString(CultureInfo.InvariantCulture);
						groupedDataRow["RunWhenOff"]          = row.RunWhenOff.ToString(CultureInfo.InvariantCulture);

						dtg.Rows.Add(groupedDataRow);
					}
				}
			}

			if (clsReportParameters.IsDetailed)
			{
				var dtl = new DataTable("SensorMapLegend");

				result.Tables.Add(dtl);

				dtl.Columns.AddRange(new[]
				{
					new DataColumn("VehicleName"),
					new DataColumn("LogTime"),
					new DataColumn("SensorValue"),
					new DataColumn("Lat"),
					new DataColumn("Lng"),
					new DataColumn("Address")
				});
				var sensorMapLegendRow = dtl.NewRow();

				sensorMapLegendRow["VehicleName"] = strings["VehicleNameTitle"];
				sensorMapLegendRow["LogTime"]     = strings["LogTime"];
				sensorMapLegendRow["SensorValue"] = strings["SensorValueTitle"];
				sensorMapLegendRow["Lat"]         = strings["Lat"];
				sensorMapLegendRow["Lng"]         = strings["Lng"];
				sensorMapLegendRow["Address"]     = strings["AddressTitle"];

				dtl.Rows.Add(sensorMapLegendRow);

				var dtd = new DataTable("SensorMap");

				result.Tables.Add(dtd);

				dtd.Columns.AddRange(new[]
				{
					new DataColumn("VehicleName"),
					new DataColumn("LogTime"),
					new DataColumn("SensorValue"),
					new DataColumn("Lat"),
					new DataColumn("Lng"),
					new DataColumn("Address")
				});

				foreach (var item in finalList)
				{
					var values = item.FlatValues;
					foreach (var sls in values)
					{
						var row = dtd.NewRow();
						row["VehicleName"] = item.VehicleName;
						row["LogTime"]     = sls.Time.ToString(TimeHelper.DefaultTimeFormat);
						row["SensorValue"] = sls.Value;
						var geoRecord = sls.GeoLogRecord;
						if (geoRecord.HasValue)
						{
							row["Lat"] = geoRecord.Value.Lat;
							row["Lng"] = geoRecord.Value.Lng;
						}
						dtd.Rows.Add(row);
					}
				}

				result = mclsInstance.FillAddressColumn(
					result,
					"SensorMap",
					"Lng",
					"Lat",
					"Address",
					clsReportParameters.Culture.TwoLetterISOLanguageName,
					clsReportParameters.MapGuid);
			}

			// Добавим в результирующий набор данных подписи таблиц.
			var tablesCaptions = new DataTable("TableCaptions");
			tablesCaptions.Columns.Add(new DataColumn("key"));
			tablesCaptions.Columns.Add(new DataColumn("value"));
			result.Tables.Add(tablesCaptions);

			const string summaryDataTableCaption = "SummaryDataTableCaption";
			const string groupByDaysTableCaption = "GroupByDaysTableCaption";
			const string detailedTableCaption    = "DetailedTableCaption";

			var tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = summaryDataTableCaption;
			tcRow["value"] = @strings[summaryDataTableCaption];
			tablesCaptions.Rows.Add(tcRow);

			tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = groupByDaysTableCaption;
			tcRow["value"] = @strings[groupByDaysTableCaption];
			tablesCaptions.Rows.Add(tcRow);

			tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = detailedTableCaption;
			tcRow["value"] = @strings[detailedTableCaption];
			tablesCaptions.Rows.Add(tcRow);

			const string summaryDataTableCaptionShort = "SummaryDataTableCaptionShort";
			const string groupByDaysTableCaptionShort = "GroupByDaysTableCaptionShort";
			const string detailedTableCaptionShort    = "DetailedTableCaptionShort";

			tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = summaryDataTableCaptionShort;
			tcRow["value"] = @strings[summaryDataTableCaptionShort];
			tablesCaptions.Rows.Add(tcRow);

			tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = groupByDaysTableCaptionShort;
			tcRow["value"] = @strings[groupByDaysTableCaptionShort];
			tablesCaptions.Rows.Add(tcRow);

			tcRow = tablesCaptions.NewRow();
			tcRow["key"]   = detailedTableCaptionShort;
			tcRow["value"] = @strings[detailedTableCaptionShort];
			tablesCaptions.Rows.Add(tcRow);

			return result;
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new ReportSensorsParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new ReportSensorsParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (ReportSensorsParameters)iReportParameters;

				// Если есть набор идентификаторов, то очищаем идентификатор группы и считаем, что есть произвольный набор машин
				if (0 < (reportParameters.VehicleIds?.Count ?? 0))
					reportParameters.VehicleGroupId = -1;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as ReportSensorsParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleGroupId <= 0 &&
				(parameters.VehicleIds == null || parameters.VehicleIds.Count == 0))
				return false;

			if (parameters.SensorId <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[ReportName];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[ReportDescription];
		}
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var clsReportParameters = (ReportSensorsParameters)reportParameters;

			return GetReportHtml(clsReportParameters);
		}
		private string GetReportHtml(ReportSensorsParameters clsReportParameters)
		{
			// Получение результирующего списка.
			var finalList = GetFinalList(clsReportParameters);

			var stringBuilder = new StringBuilder();

			var @string = GetResourceStringContainer(clsReportParameters.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				var anyResults = finalList.Any();
				if (!anyResults)
				{
					xw.WriteElementString("span", @string["NoData"]);
					return stringBuilder.ToString();
				}

				var sensorOn  = finalList.First().SensorValueNames[1];
				var sensorOff = finalList.First().SensorValueNames[0];

				var caption = @string["ReportName"];

				var resultSensorName = finalList.First().SensorName;

				var resultSensorType = finalList.First().SensorType == SensorType.Analog
					? @string["SensorTypeAnalog"]
					: @string["SensorTypeDigital"];

				xw.WriteElementString("h1", "http://www.w3.org/1999/xhtml", caption);

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "short");


				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["SensorTypeTitle"]);
				xw.WriteElementString("td", resultSensorType);
				xw.WriteEndElement();

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["SensorNameTitle"]);
				xw.WriteElementString("td", resultSensorName);
				xw.WriteEndElement();

				if (clsReportParameters.VehicleIds == null || clsReportParameters.VehicleIds.Count == 0)
				{
					xw.WriteStartElement("tr");
					xw.WriteElementString("td", @string["VehicleGroupName"]);
					xw.WriteElementString("td",
						mclsInstance.GetVehicleGroupById(clsReportParameters.VehicleGroupId).name);
					xw.WriteEndElement();
				}

				xw.WriteEndElement();

				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteElementString("caption", @string["SummaryDataTableCaption"]);

				xw.WriteStartElement("thead");

				xw.WriteStartElement("tr");
				xw.WriteElementString("td", @string["VehicleNameTitle"]);

				xw.WriteElementWithClass("td", "number", @string["TimeWhenRun"]);
				xw.WriteElementWithClass("td", "number", string.Format(@string["TimeWhenOnButNotRun"], sensorOn).CapitalizeFirstChar());
				xw.WriteElementWithClass("td", "number", string.Format(@string["TimeWhenOnAndRun"], sensorOn).CapitalizeFirstChar());

				xw.WriteElementWithClass("td", "number", string.Format(@string["TotalOn"], sensorOn).CapitalizeFirstChar());
				xw.WriteElementWithClass("td", "number", string.Format(@string["TotalOff"], sensorOff).CapitalizeFirstChar());
				xw.WriteElementString("td", string.Format(@string["TotalTimeOn"], sensorOn).CapitalizeFirstChar());
				xw.WriteElementString("td", string.Format(@string["TotalTimeOff"], sensorOff).CapitalizeFirstChar());
				xw.WriteElementString("td", @string["TotalTimeOut"]);
				xw.WriteElementWithClass("td", "number", string.Format(@string["RunWhenOn"], sensorOn).CapitalizeFirstChar());
				xw.WriteElementWithClass("td", "number", string.Format(@string["RunWhenOff"], sensorOff).CapitalizeFirstChar());
				xw.WriteEndElement();

				xw.WriteEndElement();

				xw.WriteStartElement("tbody");
				foreach (var item in finalList)
				{
					xw.WriteStartElement("tr");
					xw.WriteElementString("td", item.VehicleName);
					xw.WriteElementWithClass("td", "number",
											 TimeHelper.GetTimeSpanReadableShort(item.TimeWhenRun,
																			clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 TimeHelper.GetTimeSpanReadableShort(item.TimeWhenOnButNotRun,
																			clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 TimeHelper.GetTimeSpanReadableShort(item.TimeWhenOnAndRun,
																			clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 item.CountOn.ToString(clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 item.CountOff.ToString(clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "string",
											 TimeHelper.GetTimeSpanReadableShort(item.TotalOn, clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "string",
											 TimeHelper.GetTimeSpanReadableShort(item.TotalOff, clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "string",
											 TimeHelper.GetTimeSpanReadableShort(item.TotalOut, clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 Math.Round(item.RunWhenOn, 3).ToString(clsReportParameters.Culture));
					xw.WriteElementWithClass("td", "number",
											 Math.Round(item.RunWhenOff, 3).ToString(clsReportParameters.Culture));

					xw.WriteStartElement("td");
					xw.WritePathLink(clsReportParameters, item.VehicleId, clsReportParameters.DateFrom, clsReportParameters.DateTo.ToEndOfDay(), @string["Path"]);
					xw.WriteEndElement();

					xw.WriteEndElement();
				}

				//tbody
				xw.WriteEndElement();
				//table
				xw.WriteEndElement();

				if (clsReportParameters.IsGroupedByDays)
				{
					xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
					xw.WriteAttributeString("class", "reportTable withBorders topspace");
					xw.WriteElementString("caption", @string["GroupByDaysTableCaption"]);

					xw.WriteStartElement("thead");
					xw.WriteStartElement("tr");
					xw.WriteElementString("td", @string["LogTime"]);
					xw.WriteElementString("td", @string["VehicleNameTitle"]);
					xw.WriteElementString("td", @string["TimeWhenRun"]);
					xw.WriteElementString("td",
						string.Format(@string["TimeWhenOnButNotRun"], sensorOn).CapitalizeFirstChar());
					xw.WriteElementString("td",
						string.Format(@string["TimeWhenOnAndRun"], sensorOn).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Format(@string["TotalOn"], sensorOn).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Format(@string["TotalOff"], sensorOff).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Format(@string["TotalTimeOn"], sensorOn).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Format(@string["TotalTimeOff"], sensorOff).CapitalizeFirstChar());
					xw.WriteElementString("td", @string["TotalTimeOut"]);
					xw.WriteElementString("td", string.Format(@string["RunWhenOn"], sensorOn).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Format(@string["RunWhenOff"], sensorOff).CapitalizeFirstChar());
					xw.WriteElementString("td", string.Empty);
					//tr
					xw.WriteEndElement();
					//thead
					xw.WriteEndElement();

					xw.WriteStartElement("body");

					foreach (var fi in finalList)
					{
						foreach (var row in fi.GroupByDayValues)
						{
							xw.WriteStartElement("tr");

							xw.WriteElementString("td", row.Date.ToShortDateString());
							xw.WriteElementString("td", fi.VehicleName);
							xw.WriteElementWithClass("td", "number",
								TimeHelper.GetTimeSpanReadableShort(row.TimeWhenRun,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number",
								TimeHelper.GetTimeSpanReadableShort(row.TimeWhenOnButNotRun,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number",
								TimeHelper.GetTimeSpanReadableShort(row.TimeWhenOnAndRun,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number", row.CountOn.ToString(clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number", row.CountOff.ToString(clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "string",
								TimeHelper.GetTimeSpanReadableShort(row.TotalOn,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "string",
								TimeHelper.GetTimeSpanReadableShort(row.TotalOff,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "string",
								TimeHelper.GetTimeSpanReadableShort(row.TotalOut,
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number",
								Math.Round(row.RunWhenOn, 3).ToString(
									clsReportParameters.Culture));
							xw.WriteElementWithClass("td", "number",
								Math.Round(row.RunWhenOff, 3).ToString(
									clsReportParameters.Culture));

							xw.WriteStartElement("td");
							xw.WritePathLink(clsReportParameters, fi.VehicleId, row.Date, row.Date.ToEndOfDay(),
								@string["Path"]);
							xw.WriteEndElement();

							//tr
							xw.WriteEndElement();
						}
					}

					// tbody
					xw.WriteEndElement();
					// table
					xw.WriteEndElement();
				}

				// Детализация.
				if (clsReportParameters.IsDetailed)
				{
					var ds = new DataSet();
					var dt = new DataTable("SensorMap");

					ds.Tables.Add(dt);

					dt.Columns.AddRange(new[]
					{
						new DataColumn("VehicleName"),
						new DataColumn("LogTime"),
						new DataColumn("SensorValue"),
						new DataColumn("Lat"),
						new DataColumn("Lng"),
						new DataColumn("Address")
					});

					foreach (var item in finalList)
					{
						var values = item.FlatValues;
						foreach (var sls in values)
						{
							var row = dt.NewRow();
							row["VehicleName"] = item.VehicleName;
							row["LogTime"] = sls.Time.ToString(TimeHelper.DefaultTimeFormat);
							row["SensorValue"] = sls.Value;
							var geoRecord = sls.GeoLogRecord;
							if (geoRecord.HasValue)
							{
								row["Lat"] = geoRecord.Value.Lat;
								row["Lng"] = geoRecord.Value.Lng;
							}
							dt.Rows.Add(row);
						}
					}

					ds = mclsInstance.FillAddressColumn(
						ds,
						"SensorMap",
						"Lng",
						"Lat",
						"Address",
						clsReportParameters.Culture.TwoLetterISOLanguageName,
						clsReportParameters.MapGuid);

					xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
					xw.WriteAttributeString("class", "reportTable withBorders topspace");
					xw.WriteElementString("caption", @string["DetailedTableCaption"]);

					xw.WriteStartElement("thead");

					xw.WriteStartElement("tr");
					xw.WriteElementString("td", @string["VehicleNameTitle"]);
					xw.WriteElementString("td", @string["SensorTimeTitle"]);
					xw.WriteElementString("td", @string["SensorValueTitle"]);
					xw.WriteElementString("td", @string["AddressTitle"]);
					xw.WriteEndElement();

					//thead
					xw.WriteEndElement();

					xw.WriteStartElement("tbody");
					foreach (DataRow r in ds.Tables["SensorMap"].Rows)
					{
						xw.WriteStartElement("tr");
						xw.WriteElementWithClass("td", "string", r["VehicleName"].ToString());
						xw.WriteElementWithClass("td", "string", r["LogTime"].ToString());
						xw.WriteElementWithClass("td", "string", r["SensorValue"].ToString());
						xw.WriteElementWithClass("td", "string", r["Address"].ToString());
						xw.WriteEndElement();
					}
					//tbody
					xw.WriteEndElement();
					//table
					xw.WriteEndElement();
				}
			}

			return stringBuilder.ToString();
		}
		private List<ReportSensorsResult> GetFinalList(ReportSensorsParameters clsReportParameters)
		{
			return GetFinalList2(clsReportParameters);
		}
		/// <summary> Получить данные отчет, алгоритм 1 </summary>
		/// <param name="clsReportParameters"> Параметры отчета </param>
		/// <returns></returns>
		/// <remarks> Получаем данные по всей истории, а таблицу дневных данных нарезаем из общей истории </remarks>
		private List<ReportSensorsResult> GetFinalList1(ReportSensorsParameters clsReportParameters)
		{
			var vehicleGroupId   = clsReportParameters.VehicleGroupId;
			var operatorId       = clsReportParameters.OperatorId;
			var sensorValuesList = new List<ReportSensorsResult>();

			List<Vehicle> vehicles;

			if (clsReportParameters.VehicleIds       != null &&
				clsReportParameters.VehicleIds.Count != 0)
			{
				vehicles = mclsInstance.GetVehiclesWithPositions(
					operatorId, null, new GetVehiclesArgs
					{
						VehicleIDs = clsReportParameters.VehicleIds.ToArray(),
						Culture    = clsReportParameters.Culture
					});
				vehicles = vehicles.Where(v => v.rights != null && v.rights.Contains(SystemRight.PathAccess)).ToList();
			}
			else
			{
				vehicles = mclsInstance.GetVehiclesByVehicleGroup(vehicleGroupId, operatorId, clsReportParameters.Culture, SystemRight.PathAccess);
			}

			var localDateFrom = clsReportParameters.DateTimeInterval.DateFrom;
			var logTimeFrom   = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(localDateFrom));
			var localDateTo   = clsReportParameters.DateTimeInterval.DateTo.ToEndOfDay(); //TODO: извлекать информацию о точности периода из DateTimeInterval
			var logTimeTo     = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(localDateTo));

			foreach (var vehicle in vehicles)
			{
				var log = mclsInstance.GetFullLog(
					operatorId,
					vehicle.id,
					logTimeFrom,
					logTimeTo,
					new FullLogOptions
					{
						CutOutstandingData = true
					},
					clsReportParameters.Culture)
					.Filter();

				if (log.Sensors == null)
					continue;
				var sensorLegend = (SensorLegend)clsReportParameters.SensorId;

				if (!log.Sensors.TryGetValue(sensorLegend, out var sensor))
					continue;

				var sensorName = sensor.Name;

				var sensorStatistic = log.CalculateSensorStatistics(sensorLegend, logTimeFrom, logTimeTo);

				var rcd = new ReportSensorsResult
				{
					SensorType          = sensor.Type,
					SensorName          = sensorName,
					SensorValueNames    = sensor.ValueNames,
					VehicleId           = vehicle.id,
					VehicleName         = vehicle.Name,
					TotalOn             = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.OnTime)  ?? TimeSpan.Zero,
					TotalOff            = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.OffTime) ?? TimeSpan.Zero,
					TotalOut            = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.OutTime) ?? TimeSpan.Zero,
					CountOn             = sensorStatistic.OnCount,
					CountOff            = sensorStatistic.OffCount,
					RunWhenOn           = sensorStatistic.RunWhenOn,
					RunWhenOff          = sensorStatistic.RunWhenOff,
					TimeWhenRun         = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.TimeWhenRun)       ?? TimeSpan.Zero,
					TimeWhenOnButNotRun = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.TimeWhenOnAndStop) ?? TimeSpan.Zero,
					TimeWhenOnAndRun    = TimeHelperEx.TimeSpanFromSeconds(sensorStatistic.TimeWhenOnAndRun)  ?? TimeSpan.Zero,
					FlatValues          = new List<SensorLogStruct>(),
					GroupByDayValues    = new List<GroupByDayResult>()
				};

				sensorValuesList.Add(rcd);

				switch (sensor.Type)
				{
					case SensorType.Digital:
						rcd.FlatValues.AddRange(
							GetFlatValues(clsReportParameters, log, sensor));

						rcd.GroupByDayValues.AddRange(
							GetGroupedByDayValues(clsReportParameters, log, sensor));
						break;
				}
				// switch sensorType
			}
			return sensorValuesList;
		}
		/// <summary> Получить данные отчет, алгоритм 2 </summary>
		/// <param name="clsReportParameters"> Параметры отчета </param>
		/// <returns></returns>
		/// <remarks> Получаем данные дневных историй и суммируем в общую таблицу отчета </remarks>
		private List<ReportSensorsResult> GetFinalList2(ReportSensorsParameters clsReportParameters)
		{
			var vehicleGroupId   = clsReportParameters.VehicleGroupId;
			var operatorId       = clsReportParameters.OperatorId;
			var sensorValuesList = new List<ReportSensorsResult>();

			List<Vehicle> vehicles;

			if (clsReportParameters.VehicleIds       != null &&
				clsReportParameters.VehicleIds.Count != 0)
			{
				vehicles = mclsInstance.GetVehiclesWithPositions(
					operatorId, null, new GetVehiclesArgs
					{
						VehicleIDs = clsReportParameters.VehicleIds.ToArray(),
						Culture    = clsReportParameters.Culture
					});
				vehicles = vehicles.Where(v => v.rights != null && v.rights.Contains(SystemRight.PathAccess)).ToList();
			}
			else
			{
				vehicles = mclsInstance.GetVehiclesByVehicleGroup(vehicleGroupId, operatorId, clsReportParameters.Culture, SystemRight.PathAccess);
			}

			var localDateFrom = clsReportParameters.DateTimeInterval.DateFrom;
			var logTimeFrom   = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(localDateFrom));
			var localDateTo   = clsReportParameters.DateTimeInterval.DateTo.ToEndOfDay(); //TODO: извлекать информацию о точности периода из DateTimeInterval
			var logTimeTo     = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(localDateTo));

			foreach (var vehicle in vehicles)
			{
				var sensorLegend = (SensorLegend)clsReportParameters.SensorId;

				var sensor = vehicle.Sensors.FirstOrDefault(s => s.Number == sensorLegend);
				if (sensor == default)
					continue;

				// Получаем дневные истории
				var dayLogs = GetGroupedByDayFullLogs(
					clsReportParameters,
					vehicle.id,
					new FullLogOptions
					{
						CutOutstandingData = true
					})
					.ToList();
				var dayStats  = dayLogs
					.Select(l =>
					{
						var daySensStat = l.Value.CalculateSensorStatistics(sensorLegend, l.Value.LogTimeFrom, l.Value.LogTimeTo);
						return new GroupByDayResult
						{
							Date                = l.Key,
							TotalOn             = TimeHelperEx.TimeSpanFromSeconds(daySensStat.OnTime)  ?? TimeSpan.Zero,
							TotalOff            = TimeHelperEx.TimeSpanFromSeconds(daySensStat.OffTime) ?? TimeSpan.Zero,
							TotalOut            = TimeHelperEx.TimeSpanFromSeconds(daySensStat.OutTime) ?? TimeSpan.Zero,
							CountOn             = daySensStat.OnCount,
							CountOff            = daySensStat.OffCount,
							RunWhenOn           = daySensStat.RunWhenOn,
							RunWhenOff          = daySensStat.RunWhenOff,
							TimeWhenRun         = TimeHelperEx.TimeSpanFromSeconds(daySensStat.TimeWhenRun)       ?? TimeSpan.Zero,
							TimeWhenOnButNotRun = TimeHelperEx.TimeSpanFromSeconds(daySensStat.TimeWhenOnAndStop) ?? TimeSpan.Zero,
							TimeWhenOnAndRun    = TimeHelperEx.TimeSpanFromSeconds(daySensStat.TimeWhenOnAndRun)  ?? TimeSpan.Zero
						};
					})
					.ToList();
				var dayValues = dayLogs
					.SelectMany(l =>
					{
						if (!l.Value.Sensors.TryGetValue(sensorLegend, out var logSensor))
							return Enumerable.Empty<SensorLogStruct>();
						return GetFlatValues(clsReportParameters, l.Value, logSensor);
					})
					.Distinct()
					.ToList();
				var rcd = new ReportSensorsResult
				{
					SensorType          = sensor.Type,
					SensorName          = sensor.Name,
					SensorValueNames    = sensor.ValueNames,
					VehicleId           = vehicle.id,
					VehicleName         = vehicle.Name,
					TotalOn             = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TotalOn)),
					TotalOff            = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TotalOff)),
					TotalOut            = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TotalOut)),
					CountOn             = dayStats.Sum(s => s.CountOn),
					CountOff            = dayStats.Sum(s => s.CountOff),
					RunWhenOn           = dayStats.Sum(s => s.RunWhenOn),
					RunWhenOff          = dayStats.Sum(s => s.RunWhenOff),
					TimeWhenRun         = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TimeWhenRun)),
					TimeWhenOnButNotRun = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TimeWhenOnButNotRun)),
					TimeWhenOnAndRun    = dayStats.Aggregate(TimeSpan.Zero, (total, next) => total.Add(next.TimeWhenOnAndRun)),
					GroupByDayValues    = dayStats,
					FlatValues          = dayValues,
				};

				sensorValuesList.Add(rcd);
			}
			return sensorValuesList;
		}
		/// <summary> Возвращает набор историй по дням </summary>
		private IEnumerable<KeyValuePair<DateTime, FullLog>> GetGroupedByDayFullLogs(
			ReportSensorsParameters clsReportParameters, int vehicleId, FullLogOptions options)
		{
			var repLogTimeFrom = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(clsReportParameters.DateFrom));
			var repLogTimeTo   = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(clsReportParameters.DateTo.ToEndOfDay()));
			var repDayList     = TimeHelper.GetDayList(clsReportParameters.DateFrom, clsReportParameters.DateTo);
			foreach (var day in repDayList.OrderBy(d => d))
			{
				var dayLogTimeFrom = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(day));
				var dayLogTimeTo   = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(day.AddDays(1)));
				// Полные сутки берем только по внутренним датам
				if (dayLogTimeTo > repLogTimeTo)
					dayLogTimeTo = repLogTimeTo;
				// Получаем историю за сутки
				var log = mclsInstance.GetFullLog(
					clsReportParameters.OperatorId,
					vehicleId,
					dayLogTimeFrom,
					dayLogTimeTo,
					options,
					clsReportParameters.Culture)
					.Filter();

				yield return new KeyValuePair<DateTime, FullLog>(day, log);
			}
		}

		/// <summary>Возвращает сгруппированные по датам значения. </summary>
		private IEnumerable<GroupByDayResult> GetGroupedByDayValues(
			ReportSensorsParameters clsReportParameters, FullLog log, Sensor sensor)
		{
			var dr = sensor.LogRecords;
			if (dr.Count == 0)
				yield break;

			var dayList = TimeHelper.GetDayList(clsReportParameters.DateFrom, clsReportParameters.DateTo);

			foreach (var day in dayList)
			{
				var logTimeFrom = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(day));
				var logTimeTo   = TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(day.AddDays(1)));
				// Полные сутки берем только по внутренним датам
				if (logTimeTo > log.LogTimeTo)
					logTimeTo = log.LogTimeTo;
				// Рассчитываем статистику по дню
				var detailedStat = log.CalculateSensorStatistics(sensor.Number, logTimeFrom, logTimeTo);

				yield return new GroupByDayResult
				{
					Date                = day,
					TotalOn             = TimeHelperEx.TimeSpanFromSeconds(detailedStat.OnTime)  ?? TimeSpan.Zero,
					TotalOff            = TimeHelperEx.TimeSpanFromSeconds(detailedStat.OffTime) ?? TimeSpan.Zero,
					TotalOut            = TimeHelperEx.TimeSpanFromSeconds(detailedStat.OutTime) ?? TimeSpan.Zero,
					CountOn             = detailedStat.OnCount,
					CountOff            = detailedStat.OffCount,
					RunWhenOn           = detailedStat.RunWhenOn,
					RunWhenOff          = detailedStat.RunWhenOff,
					TimeWhenRun         = TimeHelperEx.TimeSpanFromSeconds(detailedStat.TimeWhenRun)       ?? TimeSpan.Zero,
					TimeWhenOnButNotRun = TimeHelperEx.TimeSpanFromSeconds(detailedStat.TimeWhenOnAndStop) ?? TimeSpan.Zero,
					TimeWhenOnAndRun    = TimeHelperEx.TimeSpanFromSeconds(detailedStat.TimeWhenOnAndRun)  ?? TimeSpan.Zero
				};
			}
		}
		private IEnumerable<SensorLogStruct> GetFlatValues(ReportSensorsParameters clsReportParameters, FullLog log, Sensor sensor)
		{
			var dr = sensor?.LogRecords?.OrderBy(x => x.LogTime)?.ToArray() ?? new SensorLogRecord[0];
			if (dr.Length == 0)
				yield break;

			for (var i = 1; i < dr.Length; i++)
			{
				var sensorLogRecord = dr[i];

				if (sensorLogRecord.Value != dr[i - 1].Value)
					yield return new SensorLogStruct
					{
						Time         = TimeHelper.GetLocalTime(sensorLogRecord.LogTime, clsReportParameters.TimeZoneInfo),
						Value        = sensor.ValueNames[sensorLogRecord.Value != 0 ? 1 : 0],
						GeoLogRecord = log.GetCurrentOrPreviousGeoRecord(sensorLogRecord.LogTime)
					};
			}
		}
	}
	public class ReportSensorsResult
	{
		public string[]               SensorValueNames;
		public SensorType             SensorType          { get; set; }
		public int                    VehicleId           { get; set; }
		public string                 VehicleName         { get; set; }
		public string                 SensorName          { get; set; }
		public List<SensorLogStruct>  FlatValues          { get; set; }
		public List<GroupByDayResult> GroupByDayValues    { get; set; }
		public int                    CountOn             { get; set; }
		public int                    CountOff            { get; set; }
		public TimeSpan               TotalOn             { get; set; }
		public TimeSpan               TotalOff            { get; set; }
		public TimeSpan               TotalOut            { get; set; }
		public decimal                RunWhenOn           { get; set; }
		public decimal                RunWhenOff          { get; set; }
		public TimeSpan               TimeWhenRun         { get; set; }
		public TimeSpan               TimeWhenOnButNotRun { get; set; }
		public TimeSpan               TimeWhenOnAndRun    { get; set; }
	}
	public class GroupByDayResult
	{
		public int      CountOn             { get; set; }
		public int      CountOff            { get; set; }
		public DateTime Date                { get; set; }
		public TimeSpan TotalOn             { get; set; }
		public TimeSpan TotalOff            { get; set; }
		public TimeSpan TotalOut            { get; set; }
		public decimal  RunWhenOn           { get; set; }
		public decimal  RunWhenOff          { get; set; }
		public TimeSpan TimeWhenRun         { get; set; }
		public TimeSpan TimeWhenOnButNotRun { get; set; }
		public TimeSpan TimeWhenOnAndRun    { get; set; }
	}
	public struct SensorLogStruct
	{
		public DateTime      Time;
		public string        Value;
		public GeoLogRecord? GeoLogRecord;
	}
}