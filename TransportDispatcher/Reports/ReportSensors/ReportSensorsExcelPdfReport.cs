﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class ReportSensorsExcelPdfReport : IReportClass
	{
		private DataSet       _ds;
		private readonly Font _fontNorm;
		private readonly Font _fontBold;

		public ReportSensorsExcelPdfReport()
		{
			_ds       = new DataSet();
			_fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			_fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);
		}
		public void SetDataSource(DataSet dataSet)
		{
			_ds = dataSet;
		}

		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			switch (formatType)
			{
				case ReportTypeEnum.Excel:
					var stringReport = WorkbookEngine.CreateWorkbook(_ds, "ReportSensorsSingle.xsl");
					var encoding = new UTF8Encoding(false);

					using (TextWriter writer = new StreamWriter(fileName, false, encoding))
					{
						writer.Write(stringReport);
					}
					break;
				case ReportTypeEnum.Acrobat:
					var document = new Document(PageSize.A4);
					var pdfWriter = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
					pdfWriter.PageEvent = new PdfReportPageEvents();

					var reportName = _ds.Tables["Legend"].Rows[0]["reportLegendValue"].ToString();
					document.AddTitle(reportName); // необходимо до открытия документа
					document.Open();
					document.NewPage();
					var commonTable = new PdfPTable(2) { WidthPercentage = 50f, HorizontalAlignment = Element.ALIGN_LEFT };

					var ht = _ds.Tables["Legend"];
					foreach (DataRow row in ht.Rows)
					{
						commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendKey"].ToString(), _fontBold)));
						commonTable.AddCell(new PdfPCell(new Phrase(row["reportLegendValue"].ToString(), _fontNorm)));
					}

					document.Add(commonTable);

					var dt = _ds.Tables["SensorCommonLegend"];
					if (dt != null)
					{
						document.Add(new Paragraph(new Phrase(" ")));
						document.Add(new Phrase(_ds.Tables["TableCaptions"].Rows[0]["value"].ToString(), _fontNorm));

						var pTable = new PdfPTable(dt.Columns.Count);
						pTable.DefaultCell.Border = Rectangle.RECTANGLE;

						// Заголовок.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
							{
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold));
							}

							pTable.Rows.Add(pRow);
						}

						dt = _ds.Tables["SensorCommonData"];

						// Данные.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
							{
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));
							}

							pTable.Rows.Add(pRow);
						}

						pTable.WidthPercentage = 100f;
						pTable.HeaderRows = 1;

						document.Add(pTable);
					}

					dt = _ds.Tables["SensorGroupedLegend"];
					if (dt != null)
					{
						document.Add(new Paragraph(new Phrase(" ")));
						document.Add(new Phrase(_ds.Tables["TableCaptions"].Rows[1]["value"].ToString(), _fontNorm));

						var pTable = new PdfPTable(dt.Columns.Count);
						pTable.DefaultCell.Border = Rectangle.RECTANGLE;

						// Заголовок.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
							{
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold));
							}

							pTable.Rows.Add(pRow);
						}

						dt = _ds.Tables["SensorGroupedData"];

						// Данные.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
							{
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));
							}

							pTable.Rows.Add(pRow);
						}

						pTable.WidthPercentage = 100f;
						pTable.HeaderRows = 1;

						document.Add(pTable);
					}

					dt = _ds.Tables["SensorMapLegend"];
					if (dt != null)
					{
						document.Add(new Paragraph(new Phrase(" ")));
						document.Add(new Phrase(_ds.Tables["TableCaptions"].Rows[2]["value"].ToString(), _fontNorm));

						var pTable = new PdfPTable(dt.Columns.Count);
						pTable.DefaultCell.Border = Rectangle.RECTANGLE;

						// Заголовок.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow   = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontBold));
							pTable.Rows.Add(pRow);
						}

						dt = _ds.Tables["SensorMap"];
						// Данные.
						foreach (DataRow row in dt.Rows)
						{
							var pCells = new PdfPCell[dt.Columns.Count];
							var pRow   = new PdfPRow(pCells);
							for (var i = 0; i < dt.Columns.Count; i++)
								pCells[i] = new PdfPCell(new Phrase(row.ItemArray[i].ToString(), _fontNorm));
							pTable.Rows.Add(pRow);
						}

						pTable.WidthPercentage = 100f;
						document.Add(pTable);
					}

					document.Close();
					break;
				default:
					throw new ArgumentOutOfRangeException("formatType");
			}
			// PDF Export
		}
		public void Dispose()
		{
		}
	}
}