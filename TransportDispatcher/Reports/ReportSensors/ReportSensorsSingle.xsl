<xsl:stylesheet version="1.0"
 xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">
      <xsl:apply-templates/>
    </Workbook>

  </xsl:template>

  <xsl:template match="/*">
    <Styles>
      <Style ss:ID="Default" ss:Name="Normal">
        <Alignment ss:Vertical="Bottom"/>
        <Borders/>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000"/>
        <Interior/>
        <NumberFormat/>
        <Protection/>
      </Style>
      <Style ss:ID="s63">
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
      </Style>
      <Style ss:ID="s67">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
      </Style>
      <Style ss:ID="s68">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat/>
      </Style>
      <Style ss:ID="s74">
        <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <Font ss:FontName="Calibri" x:CharSet="204" x:Family="Swiss" ss:Size="11"
         ss:Color="#000000" ss:Bold="1"/>
        <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
      </Style>
      <Style ss:ID="s75">
        <Borders>
          <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
          <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
        </Borders>
        <NumberFormat ss:Format="0"/>
      </Style>
    </Styles>

    <Worksheet>
      <xsl:attribute name="ss:Name">
        <xsl:value-of select="//TableCaptions/key[.='SummaryDataTableCaptionShort']/../value"/>
      </xsl:attribute>
      <Table ss:ExpandedColumnCount="12" x:FullColumns="1" x:FullRows="1" ss:DefaultRowHeight="15">
        <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="75.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="84"/>
        <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="68.25"/>
        <Column ss:AutoFitWidth="0" ss:Width="66.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
        <Column ss:AutoFitWidth="0" ss:Width="76.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="70.5"/>
        <Column ss:AutoFitWidth="0" ss:Width="81"/>
        <Column ss:AutoFitWidth="0" ss:Width="87"/>
        <xsl:apply-templates select="//Legend" />

        <xsl:apply-templates select="//SensorCommonLegend" />
        <xsl:apply-templates select="//SensorCommonData" />
      </Table>
    </Worksheet>
    <xsl:if test="/NewDataSet/SensorGroupedData">
      <Worksheet>
        <xsl:attribute name="ss:Name">
          <xsl:value-of select="//TableCaptions/key[.='GroupByDaysTableCaptionShort']/../value"/>
        </xsl:attribute>
        <Table ss:ExpandedColumnCount="12" x:FullColumns="1" x:FullRows="1" ss:DefaultRowHeight="15">
          <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
          <Column ss:AutoFitWidth="0" ss:Width="75.75"/>
          <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
          <Column ss:AutoFitWidth="0" ss:Width="84"/>
          <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
          <Column ss:AutoFitWidth="0" ss:Width="68.25"/>
          <Column ss:AutoFitWidth="0" ss:Width="66.75"/>
          <Column ss:AutoFitWidth="0" ss:Width="72.75"/>
          <Column ss:AutoFitWidth="0" ss:Width="76.5"/>
          <Column ss:AutoFitWidth="0" ss:Width="70.5"/>
          <Column ss:AutoFitWidth="0" ss:Width="81"/>
          <Column ss:AutoFitWidth="0" ss:Width="87"/>
          <xsl:apply-templates select="//SensorGroupedLegend" />
          <xsl:apply-templates select="//SensorGroupedData" />
        </Table>
      </Worksheet>
    </xsl:if>

    <xsl:if test="/NewDataSet/SensorMap">
      <Worksheet>
        <xsl:attribute name="ss:Name">
          <xsl:value-of select="//TableCaptions/key[.='DetailedTableCaptionShort']/../value"/>
        </xsl:attribute>
        <Table ss:ExpandedColumnCount="6" x:FullColumns="1" x:FullRows="1" ss:DefaultRowHeight="15">
          <Column ss:AutoFitWidth="0" ss:Width="75.75" />
          <Column ss:AutoFitWidth="0" ss:Width="94.5"  />
          <Column ss:AutoFitWidth="0" ss:Width="75.75" />
          <Column ss:AutoFitWidth="0" ss:Width="47.25" />
          <Column ss:AutoFitWidth="0" ss:Width="47.25" />
          <Column ss:AutoFitWidth="0" ss:Width="427.5" />
          <xsl:apply-templates select="//SensorMapLegend" />
          <xsl:apply-templates select="//SensorMap" />
        </Table>
      </Worksheet>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Legend">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="reportLegendKey">
    <Cell ss:MergeAcross="1" ss:StyleID="s63">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="reportLegendValue">
    <Cell ss:MergeAcross="4">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="SensorCommonLegend">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="SensorCommonData">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="SensorCommonLegend/VehicleName">
    <Cell ss:Index="1" ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TimeWhenRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TimeWhenOnButNotRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TimeWhenOnAndRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TotalOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TotalOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TotalTimeOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TotalTimeOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/TotalTimeOut">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/RunWhenOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonLegend/RunWhenOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="SensorCommonData/VehicleName">
    <Cell ss:Index="1" ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TimeWhenRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TimeWhenOnButNotRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TimeWhenOnAndRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TotalOn">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TotalOff">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TotalTimeOn">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TotalTimeOff">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/TotalTimeOut">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/RunWhenOn">
    <Cell ss:StyleID="s75">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorCommonData/RunWhenOff">
    <Cell ss:StyleID="s75">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="SensorGroupedLegend">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="SensorGroupedData">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>

  <xsl:template match="SensorGroupedLegend/LogTime">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/VehicleName">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TimeWhenRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TimeWhenOnButNotRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TimeWhenOnAndRun">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TotalOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TotalOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TotalTimeOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TotalTimeOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/TotalTimeOut">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/RunWhenOn">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedLegend/RunWhenOff">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="SensorGroupedData/LogTime">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/VehicleName">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TimeWhenRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TimeWhenOnButNotRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TimeWhenOnAndRun">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TotalOn">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TotalOff">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TotalTimeOn">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TotalTimeOff">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/TotalTimeOut">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/RunWhenOn">
    <Cell ss:StyleID="s75">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorGroupedData/RunWhenOff">
    <Cell ss:StyleID="s75">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

  <xsl:template match="SensorMapLegend">
    <Row ss:Height="30">
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="SensorMapLegend/VehicleName">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMapLegend/LogTime">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMapLegend/SensorValue">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMapLegend/Lat">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMapLegend/Lng">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMapLegend/Address">
    <Cell ss:StyleID="s74">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap">
    <Row>
      <xsl:apply-templates select="*" />
    </Row>
  </xsl:template>
  <xsl:template match="SensorMap/VehicleName">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap/LogTime">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap/SensorValue">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap/Lat">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap/Lng">
    <Cell ss:StyleID="s68">
      <Data ss:Type="Number">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="SensorMap/Address">
    <Cell ss:StyleID="s67">
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>
  <xsl:template match="*">
    <Cell>
      <Data ss:Type="String">
        <xsl:value-of select="."/>
      </Data>
    </Cell>
  </xsl:template>

</xsl:stylesheet>