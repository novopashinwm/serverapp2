﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class SensorValueHistoryReportParameters : ReportParameters
	{
		/// <summary> Объект наблюдения по которому строим отчёт </summary>
		/// <remarks> В список должны попасть только объекты на которые есть право просмотра истории <see cref="SystemRight.PathAccess"/></remarks>
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.VehiclePicker)]
		[Order(1)]
		[Options(SystemRight.PathAccess)]
		public int VehicleId { get; set; }
		/// <summary> Производный датчик </summary>
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.SensorPicker)]
		[Order(2)]
		public int SensorId { get; set; }
		/// <summary> Интервал агрегации (интервал между строками отчёта), сек </summary>
		[Type(typeof(int))]
		[ControlType(ReportParametersUtils.IntervalPicker)]
		[Order(3)]
		public int Interval { get; set; } = 60;
		/// <summary> Интервал построения отчета </summary>
		[ControlType(ReportParametersUtils.DateTimeFromToPicker)]
		[Order(255)]
		[Options("Accuracy:3")] // Точность до минут
		public override DateTimeInterval DateTimeInterval { get; set; }
	}
}