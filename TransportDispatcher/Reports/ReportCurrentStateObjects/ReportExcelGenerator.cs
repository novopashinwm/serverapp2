﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	internal class ReportExcelGenerator : BaseReportExcelGenerator
	{
		private readonly ResourceStringContainer             _strings;
		private readonly ReportCurrentStateObjectsParameters _parameters;
		private readonly List<Group>                         _groups;
		private readonly List<Vehicle>                       _vehicles;
		private readonly IEnumerable<CellInfo>               _cells;
		
		public ReportExcelGenerator(
			ITssReport report,
			ReportCurrentStateObjectsParameters parameters,
			IEnumerable<CellInfo> cells,
			List<Vehicle> vehicles,
			List<Group> groups,
			ResourceStringContainer strings) :
			base(report, parameters)
		{
			_strings    = strings;
			_parameters = parameters;
			_groups     = groups;
			_vehicles   = vehicles;
			_cells      = cells;
		}
		protected override string WorksheetName
		{
			get { return _strings["Report"]; }
		}
		protected override IEnumerable<CellInfo> Cells
		{
			get
			{
				return _cells;
			}
		}
		protected override void CreateTableHeader(XElement table = null, IEnumerable<CellInfo> cells = null, bool autoFilter = true)
		{
			// Блокируем стандартный вызов, т.к. в нем предполагалась одна таблица
			// Нужно это для удаления лишнего заголовка таблицы
			// Далее везде вызываем base.CreateTableHeader
		}
		protected override void FillTable()
		{
			if (_groups == null)
			{
				base.CreateTableHeader(autoFilter: false);
				CreateTableBody(_vehicles.OrderBy(v => v.Name).ToList());
			}
			else
			{
				AddRow(new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0")));
				foreach (var @group in _groups)
				{
					var headRow = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
					headRow.Add(new XElement("Cell",
						new XAttribute(SpreadsheetNS + "StyleID", "s69"),
						new XAttribute(SpreadsheetNS + "MergeAcross", 5),
						new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"),
							string.IsNullOrEmpty(@group.Name) ? _strings["NoName"] : @group.Name)
					));
					AddRow(headRow);

					base.CreateTableHeader(autoFilter: false);
					CreateTableBody(_vehicles.Where(v => @group.ObjectIds.Contains(v.id)).OrderBy(v => v.Name).ToList());

					AddRow(new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0")));
					AddRow(new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0")));
				}

				var vehiclesInGroups = _groups
					.Where(g => g.ObjectIds != null)
					.SelectMany(g => g.ObjectIds, (g, id) => id)
					.ToList();
				if (_vehicles.Any(v => !vehiclesInGroups.Contains(v.id)))
				{
					var headRow = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
					headRow.Add(
						new XElement("Cell",
							new XAttribute(SpreadsheetNS + "StyleID", "s69"),
							new XAttribute(SpreadsheetNS + "MergeAcross", 5),
							new XElement("Data", new XAttribute(SpreadsheetNS + "Type", "String"), _strings["NotInGroup"])
						));
					AddRow(headRow);
					base.CreateTableHeader(autoFilter: false);
					CreateTableBody(_vehicles.Where(v => !vehiclesInGroups.Contains(v.id)).OrderBy(v => v.Name).ToList());
				}
			}
		}
		protected override void FillHeader()
		{
			base.FillHeader();

			if (!string.IsNullOrEmpty(_parameters.GroupName))
			{
				AddRow(new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
					CreateCellTitleHead(_strings["ObjectsGroup"]),
					CreateCellValueHead(_parameters.GroupName)
				));
			}

			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
				CreateCellTitleHead(_strings["posDate"]),
				CreateCellValueHead(_parameters.DateReport.ToString(_parameters.Culture))
			));

			var filterLocalizedValue = string.Empty;
			if (_parameters.FilterVehicleByState == ShowVehiclesMode.None || _parameters.FilterVehicleByState == ShowVehiclesMode.All)
				filterLocalizedValue = _strings[_parameters.FilterVehicleByState.ToString()];
			else
				filterLocalizedValue = string.Join(", ", Enum
					.GetValues(typeof(ShowVehiclesMode))
					.OfType<ShowVehiclesMode>()
					.Where(v => !v.Equals(ShowVehiclesMode.None) && _parameters.FilterVehicleByState.HasFlag(v))
					.Select(v => _strings[v.ToString()]));

			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
				CreateCellTitleHead(_strings["Filter"]),
				CreateCellValueHead(filterLocalizedValue)
			));
		}
		void CreateTableBody(List<Vehicle> vehicles)
		{
			for (int i = 0; i < vehicles.Count; i++)
			{
				var veh = vehicles.ElementAt(i);
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
				foreach (var cellInfo in Cells)
				{
					var cellElement = ObjectFieldCellInfo.LineNumberKey == cellInfo.Key
						? CreateDataCell(cellInfo, (i + 1).ToString())
						: CreateDataCell(cellInfo, veh);
					row.Add(cellElement);
				}
				AddRow(row);
			}
		}
		private XElement CreateDataCell(CellInfo cellInfo, Vehicle vehicle)
		{
			return CreateDataCell(cellInfo, cellInfo.GetValue(vehicle));
		}
	}
}