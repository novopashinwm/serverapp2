﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	class CurrentStateObjectsPrintedReport : IReportClass
	{
		private List<Vehicle> _vehicles;
		private List<Group>   _groups;

		readonly Font _fontNorm, _fontBold;
		readonly ReportCurrentStateObjectsParameters _parameters;
		private readonly CurrentStateObjectsReport   _report;
		readonly ResourceStringContainer             _strings;
		readonly Dictionary<string, CellInfo>        _cellInfos;
		readonly ResourceStringContainer             _sensorsResources;

		private readonly ResourceContainers          _resource;

		const float DefaultTableWidth = 100f;

		public CurrentStateObjectsPrintedReport(
			CurrentStateObjectsReport report,
			ResourceStringContainer strings,
			ResourceStringContainer sensorsResources,
			ReportCurrentStateObjectsParameters parameters)
		{
			_report           = report;
			_strings          = strings;
			_sensorsResources = sensorsResources;
			_parameters       = parameters;
			_resource         = ResourceContainers.Get(parameters.Culture);

			_fontNorm = new Font(ReportHelper.ArialBaseFont, 7, Font.NORMAL);
			_fontBold = new Font(ReportHelper.ArialBaseFont, 7, Font.BOLD);

			_cellInfos = new ObjectFieldCellInfo(_parameters.FieldsToView).GetCells();

			const string sensorFieldPrefix = "sensor.";

			foreach (var cell in _cellInfos.Values)
			{
				cell.Name = cell.Key.StartsWith(sensorFieldPrefix)
					? _sensorsResources[cell.Key.Substring(sensorFieldPrefix.Length)]
					: _strings[cell.Key];
				if (ObjectFieldCellInfo.LineNumberKey != cell.Key)
					cell.GetValue = x => GetValueForVehicle(cell.Key, (Vehicle)x);
			}
		}
		public void FillData(List<Vehicle> vehicles, List<Group> groups)
		{
			_vehicles = vehicles;
			_groups   = groups;
		}
		public void Dispose()
		{
			//TODO: Add smth to dispose. ITextSharp destruction?
		}
		public void ExportToDisk(ReportTypeEnum formatType, string fileName)
		{
			if (formatType != ReportTypeEnum.Acrobat && formatType != ReportTypeEnum.Excel)
				throw new NotSupportedException("Report format not supported " + formatType.ToString());

			if (formatType == ReportTypeEnum.Excel)
			{
				var creator      = new ReportExcelGenerator(_report, _parameters, _cellInfos.Values, _vehicles, _groups, _strings);
				var stringReport = creator.Create(false);
				var encoding     = new UTF8Encoding(false);

				using (TextWriter writer = new StreamWriter(fileName, false, encoding))
				{
					writer.Write(stringReport);
				}
				return;
			}

			using (var stream = new FileStream(fileName, FileMode.Create))
			{
				using (var document = new Document(PageSize.A4.Rotate()))
				{
					var pdfWriter = PdfWriter.GetInstance(document, stream);
					pdfWriter.PageEvent = new PdfReportPageEvents();
					document.AddTitle(_strings["CurrentStateObjectsReport"]); // необходимо до открытия документа
					document.Open();
					document.NewPage();
					document.Add(CreateHeaderTable());
					if (_groups != null)
					{
						foreach (var group in _groups)
						{
							document.Add(new Paragraph(new Phrase(" ")));
							document.Add(CreateVehiclesTable(group, _vehicles.Where(v => group.ObjectIds != null && group.ObjectIds.Contains(v.id)).OrderBy(v => v.Name).ToList()));
						}

						var vehiclesInGroups = _groups
							.Where(g => g.ObjectIds != null)
							.SelectMany(g => g.ObjectIds, (g, id) => id)
							.ToList();
						if (_vehicles.Any(v => !vehiclesInGroups.Contains(v.id)))
						{
							document.Add(new Paragraph(new Phrase(" ")));
							document.Add(CreateVehiclesTable(null, _vehicles.Where(v => !vehiclesInGroups.Contains(v.id)).OrderBy(v => v.Name).ToList()));
						}
					}
					else
					{
						document.Add(new Paragraph(new Phrase(" ")));
						document.Add(CreateVehiclesTable(null, _vehicles.OrderBy(v => v.Name).ToList()));
					}
				}
			}
		}
		private string GetValueForVehicle(string key, Vehicle vehicle)
		{
			switch (key)
			{
				case "ObjectId":
					return vehicle.id.ToString();
				case "name":
					return vehicle.Name;
				case "vehicleKind":
					return _resource[vehicle.vehicleKind];
				case "billingServices":
					break;
				case "billingBlocking":
					break;
				case "regNum":
					return vehicle.regNum;
				case "brand":
					return vehicle.brand;
				case "posDate":
					return vehicle.posDate.HasValue ? TimeZoneInfo.ConvertTimeFromUtc(vehicle.posDate.Value, _parameters.TimeZoneInfo).ToString() : "";
				case "address":
					return vehicle.address;
				case "speed":
					return vehicle.speed.HasValue ? vehicle.speed.Value.ToString(_parameters.Culture) : "";
				case "phone":
					return vehicle.phone;
				case "ctrlNum":
					return vehicle.ctrlNum;
				case "ctrl":
					return vehicle.controllerType != null ? _resource.GetLocalizedText<ControllerType>(vehicle.controllerType.Name) : "";
				case "Notes":
					return vehicle.Notes;
			}
			if (key.Contains("sensor."))
			{
				if (vehicle.Sensors == null)
					return null;

				SensorLegend sensorLegend;
				if (Enum.TryParse(key.Replace("sensor.", ""), true, out sensorLegend))
				{
					var sensor = vehicle.Sensors.FirstOrDefault(s => s.Number == sensorLegend);
					if (sensor == null)
						return string.Empty;
					var sensorValue = sensor.LogRecords.FirstOrDefault();
					if (sensorValue.Equals(default(SensorLogRecord)))
						return string.Empty;
					if (sensor.Type == SensorType.Analog)
						return sensorValue.Value.ToString(_parameters.Culture);
					if (sensorValue.Value == 0)
						return _strings["Off"];
					return _strings["On"];
				}
				return string.Empty;
			}
			return string.Empty;
		}
		private void CreateGroupHeader(Group group, PdfPTable table)
		{
			var name = default(string);
			if (group == null)
				name = _strings["NotInGroup"];
			else if (string.IsNullOrEmpty(group.Name))
				name = _strings["NoName"];
			else name = group.Name;
			table.AddCell(new PdfPCell(new Phrase(name, _fontBold)) { Colspan = table.NumberOfColumns });
			table.HeaderRows += 1;
		}
		private PdfPTable CreateVehiclesTableHeader(Group group)
		{
			var table = new PdfPTable(_cellInfos.Count);
			table.DefaultCell.Border = Element.RECTANGLE;
			table.WidthPercentage    = DefaultTableWidth;
			table.HeaderRows         = 1;

			var widthUnit = DefaultTableWidth / _cellInfos.Sum(v => v.Value.Width ?? 1);

			table.SetTotalWidth(_cellInfos.Select(v => (v.Value.Width ?? 1) / widthUnit).ToArray());
			var cells = new List<PdfPCell>();
			// Заголовок группы если нужен (строка 1)
			if (_groups != null)
				CreateGroupHeader(group, table);
			// Заголовок таблицы (наименования колонок, строка 2)
			foreach (var cellInfo in _cellInfos.Values)
				table.AddCell(new PdfPCell(new Phrase(cellInfo.Name, _fontBold)) { HorizontalAlignment = Element.ALIGN_CENTER });
			return table;
		}
		private PdfPTable CreateVehiclesTable(Group group, List<Vehicle> vehicles)
		{
			var vehiclesTable = default(PdfPTable);
			if (vehicles.Any())
			{
				vehiclesTable = CreateVehiclesTableHeader(group);
				foreach (var vehicle in vehicles)
					FillVehicleRow(vehicle, vehiclesTable);
			}
			return vehiclesTable;
		}
		private void FillVehicleRow(Vehicle vehicle, PdfPTable table)
		{
			foreach (var cellInfo in _cellInfos.Values)
			{
				var pdfCell = default(PdfPCell);
				switch (cellInfo.Key)
				{
					case ObjectFieldCellInfo.LineNumberKey:
						pdfCell = new PdfPCell(new Phrase((table.Rows.Count - table.HeaderRows + 1).ToString(), _fontNorm)) { HorizontalAlignment = Element.ALIGN_RIGHT };
						break;
					case ObjectFieldCellInfo.ObjectIdKey:
						pdfCell = new PdfPCell(new Phrase(cellInfo?.GetValue(vehicle) ?? string.Empty,          _fontNorm)) { HorizontalAlignment = Element.ALIGN_RIGHT };
						break;
					default:
						pdfCell = new PdfPCell(new Phrase(cellInfo?.GetValue(vehicle) ?? string.Empty,          _fontNorm));
						break;
				}
				table.AddCell(pdfCell);
			}
		}
		private PdfPTable CreateHeaderTable()
		{
			var headerTable = new PdfPTable(2) { WidthPercentage = 50f, HorizontalAlignment = Element.ALIGN_LEFT };
			headerTable.Rows.Add(new PdfPRow(
				new[]
					{
						new PdfPCell(new Phrase(_strings["Report"], _fontBold)),
						new PdfPCell(new Phrase(_strings["CurrentStateObjectsReport"], _fontNorm))
					}
				));

			headerTable.Rows.Add(new PdfPRow(
				new[]
					{
						new PdfPCell(new Phrase(_strings["posDate"], _fontBold)),
						new PdfPCell(new Phrase(_parameters.DateReport.ToString(_parameters.Culture), _fontNorm))
					}
				));

			if (!string.IsNullOrEmpty(_parameters.GroupName))
			{
				headerTable.Rows.Add(new PdfPRow(
					new[]
						{
							new PdfPCell(new Phrase(_strings["ObjectsGroup"], _fontBold)),
							new PdfPCell(new Phrase(_parameters.GroupName, _fontNorm))
						}
					));
			}

			var filterLocalizedValue = string.Empty;
			if (_parameters.FilterVehicleByState == ShowVehiclesMode.None || _parameters.FilterVehicleByState == ShowVehiclesMode.All)
				filterLocalizedValue = _strings[_parameters.FilterVehicleByState.ToString()];
			else
				filterLocalizedValue = string.Join(", ", Enum
					.GetValues(typeof(ShowVehiclesMode))
					.OfType<ShowVehiclesMode>()
					.Where(v => !v.Equals(ShowVehiclesMode.None) && _parameters.FilterVehicleByState.HasFlag(v))
					.Select(v => _strings[v.ToString()]));
			headerTable.Rows.Add(new PdfPRow(
				new[]
					{
						new PdfPCell(new Phrase(_strings["Filter"], _fontBold)),
						new PdfPCell(new Phrase(filterLocalizedValue, _fontNorm))
					}
				));
			return headerTable;
		}
	}
}