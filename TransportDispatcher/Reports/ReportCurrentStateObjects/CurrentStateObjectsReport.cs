﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчет о текущем состоянии объектов </summary>
	[Guid("1C22ACF2-A71A-4E46-A189-237F6B8DF07A")]
	public class CurrentStateObjectsReport : TssReportBase
	{
		public override int Order
		{
			get { return 10; }
		}

		private readonly TimeSpan onl1Threshold = TimeSpan.FromMinutes(1 * 15);
		private readonly TimeSpan old1Threshold = TimeSpan.FromMinutes(1 * 60);
		private readonly TimeSpan old2Threshold = TimeSpan.FromMinutes(3 * 03);

		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var parameters = (ReportCurrentStateObjectsParameters)iReportParameters;
			var filterByDepartment = mclsInstance.FilterByDepartment(iReportParameters.OperatorId);
			var args = new GetVehiclesArgs
			{
				IncludeAttributes  = true,
				IncludeLastData    = true,
				IncludeAddresses   = true,
				MapGuid            = iReportParameters.MapGuid,
				Culture            = iReportParameters.Culture,
				FilterByDepartment = filterByDepartment
			};
			var department = parameters.DepartmentId != null ? mclsInstance.GetDepartment(parameters.DepartmentId.Value) : null;
			var vehicles   = mclsInstance.GetVehiclesWithPositions(iReportParameters.OperatorId, department, args);
			var groups     = mclsInstance.GetVehicleGroupsWithVehicles(iReportParameters.OperatorId, department, new GetGroupsArguments
			{
				FilterByDepartment = filterByDepartment,
				IncludeNotInGroups = true,
				IncludeStandard    = false,
				IncludeAll         = false,
			});

			#region Фильтруем объекты по времени !!!ПОЗИЦИИ!!!
			// Удаляем объекты без данных из списка
			vehicles.RemoveAll(v => 0 == (parameters.FilterVehicleByState & ShowVehiclesMode.Unknown) &&
				(v.posLogTime ?? 0) == 0);
			// Удаляем Old3 объекты из списка
			vehicles.RemoveAll(v => 0 == (parameters.FilterVehicleByState & ShowVehiclesMode.Old3)    &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) <= TimeSpan.MaxValue &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) >  old2Threshold);
			// Удаляем Old2 объекты из списка
			vehicles.RemoveAll(v => 0 == (parameters.FilterVehicleByState & ShowVehiclesMode.Old2)    &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) <= old2Threshold     &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) >  old1Threshold);
			// Удаляем Old1 объекты из списка
			vehicles.RemoveAll(v => 0 == (parameters.FilterVehicleByState & ShowVehiclesMode.Old1)    &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) <= old1Threshold     &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) >  onl1Threshold);
			// Удаляем Online объекты из списка
			vehicles.RemoveAll(v => 0 == (parameters.FilterVehicleByState & ShowVehiclesMode.Online)  &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) <= onl1Threshold     &&
				(DateTime.UtcNow - TimeHelper.GetDateTimeUTC(v.posLogTime ?? 0)) >  TimeSpan.Zero);
			#endregion Фильтруем объекты по времени !!!ПОЗИЦИИ!!!

			#region Фильтруем группы и объекты по группам
			// Исключаемые группы
			var excludedGroups = groups
				.Where(g => parameters.ExcludeVehicleGroups.Contains(g.Id));
				// Будет нужно если группы будут иерархическими
				//.SelectMany(g =>
				//	g.GetDescendants(i =>
				//		groups.Where(s => i.ChildrenIds.Contains(s.Id)), true))
				//.Distinct();
			// Группы без исключаемых
			var includedGroups = groups.Except(excludedGroups);
			// Объекты содержащиеся только в исключаемых группах
			var vehiclesInOnlyExcludedGroups = excludedGroups
				.SelectMany(g => g.ObjectIds)
				.Except(includedGroups
					.SelectMany(g => g.ObjectIds));
			// Удаляем объекты содержащиеся только в исключаемых группах
			vehicles.RemoveAll(v => vehiclesInOnlyExcludedGroups.Contains(v.id));
			groups = includedGroups
				// Берем только не пустые после фильтрации группы, т.к. нет иерархии делаем просто
				.Where(g => vehicles.Any(v => g?.ObjectIds?.Contains(v.id) ?? false))
				.ToList();

			#endregion Фильтруем группы и объекты по группам

			if (parameters.GroupId.HasValue)
			{
				var group = groups.FirstOrDefault(g=>g.Id == parameters.GroupId.Value);
				if (group != null)
					vehicles = vehicles.Where(v => group.ObjectIds != null && group.ObjectIds.Contains(v.id)).ToList();
				else if (parameters.GroupId == -3)
				{
					var objectsInGroups = groups.Where(g=>g.ObjectIds!=null).SelectMany(g => g.ObjectIds, (g, id) => id);
					vehicles = vehicles.Where(v => !objectsInGroups.Contains(v.id)).ToList();
				}
				groups = null;
			}

			var strings = GetResourceStringContainer(parameters.Culture);
			var sensors = ResourceContainers.Get(parameters.Culture).GetContainer<SensorLegend>();

			var result = new CurrentStateObjectsPrintedReport(this, strings, sensors, parameters);
			foreach (var vehicle in vehicles)
			{
				if (vehicle.posLogTime.HasValue)
					vehicle.posDate = TimeHelper.GetDateTimeUTC(vehicle.posLogTime.Value);
			}
			result.FillData(vehicles, groups);
			return result;
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var result = new ReportCurrentStateObjectsParameters { OperatorId = settings.OperatorId };
			return result;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as ReportCurrentStateObjectsParameters;

			if (parameters == null)
				return false;

			return true;
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["CurrentStateObjectsReport"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["CurrentStateObjectsReportDescription"];
		}
		public override string GetReportFileName(ReportParameters reportParameters)
		{
			var parameters = (ReportCurrentStateObjectsParameters)reportParameters;

			var reportName = GetReportName(reportParameters.Culture);
			reportName = PrepareStringToFileName(reportName);

			var sb = new StringBuilder();
			sb.Append(reportName);
			sb.Append(".");
			sb.Append(FormatDateTimeForFileName(parameters.DateReport));

			return sb.ToString().Replace(' ', '_');
		}
	}
}