﻿using FORIS.TSS.BusinessLogic.Enums;


namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class ReportCurrentStateObjectsParameters : ReportParameters
	{
		public string[]         FieldsToView         { get; set; }
		public ShowVehiclesMode FilterVehicleByState { get; set; }
		public int[]            ExcludeVehicleGroups { get; set; }
		public bool             ShowGroups           { get; set; }
		public int?             GroupId              { get; set; }
		public string           GroupName            { get; set; }
	}
}