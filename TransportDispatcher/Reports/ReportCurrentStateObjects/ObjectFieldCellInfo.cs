﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> данные ячейки для вывода информации по объекту </summary>
	public class ObjectFieldCellInfo
	{
		public  const    string                       LineNumberKey = "LineNumber";
		public  const    string                       ObjectIdKey   = "ObjectId";
		private readonly Dictionary<string, CellInfo> _objectsCellInfo;
		private readonly string[]                     _fieldsNames;

		public ObjectFieldCellInfo(string[] fieldsNames)
		{
			_fieldsNames = fieldsNames;
			var cellInfos = new List<CellInfo>
			{
				new CellInfo { Key = LineNumberKey, Width = 0.5f, ExcelType = "Number"},
				new CellInfo { Key = ObjectIdKey,   Width = 0.7f, ExcelType = "Number"},
				new CellInfo { Key = "name",        Width = 1.0f },
				new CellInfo { Key = "vehicleKind", Width = 0.7f },
				new CellInfo { Key = "regNum",      Width = 1.0f },
				new CellInfo { Key = "brand",       Width = 0.7f },
				new CellInfo { Key = "posDate",     Width = 1.0f },
				new CellInfo { Key = "address",     Width = 2.0f },
				new CellInfo { Key = "speed",       Width = 0.7f, ExcelType = "Number"},
				new CellInfo { Key = "phone",       Width = 1.0f },
				new CellInfo { Key = "ctrlNum",     Width = 1.0f },
				new CellInfo { Key = "ctrl",        Width = 1.0f },
				new CellInfo { Key = "Notes",       Width = 2.0f },
			};
			cellInfos.AddRange(
				Enum.GetNames(typeof(SensorLegend))
					.Select(name => new CellInfo { Key = "sensor." + name, Width = 1 }));
			// Тут должна быть уникальность ключа, иначе сломается
			_objectsCellInfo = cellInfos.ToDictionary(c => c.Key);
		}
		public Dictionary<string, CellInfo> GetCells()
		{
			var result = _objectsCellInfo
				.Where(c => _fieldsNames.Contains(c.Key) || LineNumberKey == c.Key || ObjectIdKey == c.Key)
				.ToDictionary(k => k.Key, v => v.Value);
			foreach (var key in result.Keys)
				result[key].Width = result[key].Width;
			return result;
		}
	}
}