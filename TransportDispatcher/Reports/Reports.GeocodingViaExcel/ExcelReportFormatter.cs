﻿using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	internal class ExcelReportFormatter : BaseReportExcelFormatter
	{
		private readonly BaseExcelGenerator _excelGenerator;

		public ExcelReportFormatter(BaseExcelGenerator excelGenerator)
		{
			_excelGenerator = excelGenerator;
		}

		protected override string GetExcelXmlString()
		{
			return _excelGenerator.Create();
		}
	}
}