﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using ExcelDataReader;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.TransportDispatcher.Reports;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	/// <summary> Параметры отчета с элементом управления загрузки Excel файла </summary>
	[Serializable]
	public class ExcelFilePickerReportParameters : ReportParameters
	{
		[ControlType(ReportParametersUtils.MileageFilePicker, "MileageFilePicker")]
		[DisplayName("InputContent")][Type(typeof(byte[]))][Order(1)]
		[Browsable(true)]
		public byte[] InputContent { get; set; }
		/// <summary> Переопределяем дату отчета, т.к. нужно и время и все в Utc </summary>
		public override DateTime DateReport => DateTime.UtcNow;
		/// <summary> Интервал построения отчета </summary>
		/// <remarks> Переопределение нужно для блокировки отображения </remarks>
		public override DateTimeInterval DateTimeInterval { get; set; }
		/// <summary> Получить исходную таблицу запроса данных </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private DataSet GetExcelDataSet(bool useColumnDataType = true, bool useHeaderRow = true)
		{
			using (var stream = new MemoryStream(InputContent))
			{
				using (var reader4DataSet = ExcelReaderFactory.CreateReader(stream))
				{
					// Возвращает таблицу для каждого листа
					return reader4DataSet.AsDataSet(
						new ExcelDataSetConfiguration()
						{
							// Использовать тип данных из колонки, иначе все поля object
							UseColumnDataType  = useColumnDataType,
							ConfigureDataTable = (reader4Table) => new ExcelDataTableConfiguration()
							{
								// Использовать первую строку, как наименование колонок
								UseHeaderRow = useHeaderRow,
							}
						});
				}
			}
		}
		protected DataTable GetExcelDataTable()
		{
			return GetExcelDataSet(false, true)
				?.Tables
				?.OfType<DataTable>()
				?.FirstOrDefault();
		}
	}
}