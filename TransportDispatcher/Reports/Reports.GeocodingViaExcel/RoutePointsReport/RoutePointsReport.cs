﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;
using Interfaces.Geo;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	/// <summary> Отчёт "Точки пути" </summary>
	/// <summary> Показывает информацию об адресах каждой точки пути </summary>
	[Guid("AA2E72A5-BBB4-473D-8533-AE11F57075E7")]
	public class RoutePointsReport : TssReportBase
	{
		/// <summary> Создает отчет, используя переданные параметры </summary>
		/// <param name="iReportParameters"> Параметры отчета </param>
		/// <returns></returns>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = (RoutePointsReportParameters)iReportParameters;
			// Формирование отчета
			return new ExcelReportFormatter(new RoutePointsReportExcelGenerator(
				this, parameters, GetRows(parameters)));
		}
		/// <summary> Получить данные для отчета </summary>
		/// <param name="srcTable"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private List<RoutePointsReportRow> GetRows(RoutePointsReportParameters parameters)
		{
			var strings = GetResourceStringContainer(parameters.Culture);
			var repRows = Enumerable.Empty<RoutePointsReportRow>().ToList();
			try
			{
				repRows = parameters.GetExcelData();
			}
			catch(Exception)
			{
				var defaultRow = RoutePointsReportRow.Empty;
				defaultRow.Row.RowError = strings[$@"{nameof(RoutePointsReport)}_InvalidFile_Error"];
				return (new[] { defaultRow }).ToList();
			}
			if (0 < repRows.Count)
			{
				// Выбираем информацию по департаменту
				var dept = parameters.DepartmentId.HasValue ? mclsInstance.GetDepartment(parameters.DepartmentId.Value) : null;
				// Поиск доступных объектов наблюдения
				var vehs = mclsInstance.GetVehiclesWithPositions(
					parameters.OperatorId,
					dept,
					new GetVehiclesArgs()
					{
						IncludeAddresses  = false,
						IncludeAttributes = true,
						IncludeZones      = false,
						IncludeLastData   = false,
						Culture           = parameters.Culture
					})
					.Select(v => new { v.id, v.Name })
					.ToList();

				var getRows = repRows
					// Только хорошие данные (с заполненными и понятными полями)
					.Where(r => r.IsValid)
					// Только доступные объекты наблюдения
					.GroupJoin(vehs,
						r => r.ObjectName,
						v => v.Name,
						(r, v) => new { r, v = v.DefaultIfEmpty() })
					.Select(x => new { x.r, v = x.v.FirstOrDefault() });
				var hasAccessVehicles = getRows
					// Только записи с доступными объектами наблюдения
					.Where(x => null != x.v)
					// Для оптимизации количества вызовов БД, группируем по запросу
					.GroupBy(r => new
					{
						ObjectId = r.v.id,
						LogTime  = TimeHelper.GetSecondsFromBase(
							TimeZoneInfo.ConvertTimeToUtc(
								DateTime.SpecifyKind(r.r.Date.Value + r.r.Time.Value, DateTimeKind.Unspecified),
								parameters.TimeZoneInfo))
					})
					// Только первые 10000 штук, удовлетворяющие проверкам
					.Take(10000);
				// Получаем все позиции по всем доступным объектам
				var positions = mclsInstance.GetPositions(hasAccessVehicles.Select(v => new VehicleLogTimeParam
				(
					v.Key.ObjectId,
					v.Key.LogTime
				)), true);
				foreach (var hasAccessVehicle in hasAccessVehicles)
				{
					var position = positions
						?.Where(p => p.VehicleId == hasAccessVehicle.Key.ObjectId && p.LogTime <= hasAccessVehicle.Key.LogTime)
						?.OrderByDescending(p => p.LogTime)
						?.FirstOrDefault();
					if (position.HasValue)
					{
						foreach (var grpRow in hasAccessVehicle)
						{
							grpRow.r.Lat = position.Value.Lat;
							grpRow.r.Lng = position.Value.Lng;
						}
					}
				}
				FillAddresses(parameters, hasAccessVehicles.SelectMany(v => v.Select(r => r.r)));
				var notAccessVehicles = getRows
					// Только строки с недоступными объектами наблюдения
					.Where(x => null == x.v)
					.Select(x => x.r);
				foreach (var notAccessVehicle in notAccessVehicles)
					if (null != notAccessVehicle.Row)
					{
						notAccessVehicle.Row.RowError = string.Concat(
							strings[$@"{nameof(RoutePointsReport)}Row_NoPermission_Error"],
							"\n",
							notAccessVehicle.Row.RowError)
							.Trim();
					}
			}
			return repRows;
		}
		private void FillAddresses(RoutePointsReportParameters parameters, IEnumerable<RoutePointsReportRow> reportRows)
		{
			var requests = reportRows
				?.Where(r => r.Lat.HasValue && r.Lng.HasValue)
				?.GroupBy(r => new { r.Lat, r.Lng })
				?.Select((itm, idx) => new { idx, itm });

			var addresses = mclsInstance.GetAddresses(
				requests
					.Select(r => new AddressRequest(
						r.idx,
						lat: (double)r.itm.Key.Lat.Value,
						lng: (double)r.itm.Key.Lng.Value))
					.ToArray(),
				parameters.Culture,
				parameters.MapGuid)
				?.ToDictionary(k => k.Id);
			foreach (var request in requests)
			{
				var addressResponse = default(AddressResponse);
				if (addresses.TryGetValue(request.idx, out addressResponse))
				{
					foreach (var row in request.itm)
						row.AddressLocation = addressResponse.Address;
				}
			}
		}
		/// <summary> Возвращает экземпляр класса, содержащий набор параметров для данного отчета </summary>
		/// <param name="settings"></param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new RoutePointsReportParameters();
		}
		/// <summary> Функция проверяет переданный набор параметров на корректность заполнения </summary>
		/// <param name="iReportParameters"></param>
		/// <returns></returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as RoutePointsReportParameters;
			if (parameters == null)
				return false;
			// Никакие проверки делать не нужно, чтобы вывести все ошибки в сам отчет
			return true;
		}
		/// <summary> Текстовое описание отчета/ </summary>
		/// <param name="culture"> Культура, для которой следует вернуть описание </param>
		/// <returns></returns>
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[$@"{nameof(RoutePointsReport)}"];
		}
		/// <summary> Возвращает описание, что именно должен вернуть отчёт, в том числе важные замечания </summary>
		/// <param name="culture"> Культура, для которой следует вернуть описание </param>
		/// <returns></returns>
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[$@"{nameof(RoutePointsReport)}_Description"];
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeocodingViaExcelReports.ToString();
		}
		/// <summary> Порядковый номер отчета </summary>
		public override int Order => 1;
		public override bool IsSubscriptionVisible => false;
		/// <summary> Возвращает список поддерживаемых отчётом форматов </summary>
		/// <returns></returns>
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Excel;
		}
		public override string GetReportFileName(ReportParameters reportParameters)
		{
			if (reportParameters == null)
				throw new ArgumentNullException(nameof(reportParameters));

			var reportName = PrepareStringToFileName(
				GetReportName(reportParameters.Culture));
			// Готовим дату отчета в часовом поясе пользователя
			var reportDate = FormatDateTimeForFileName(
				TimeZoneInfo.ConvertTimeFromUtc(
					reportParameters.DateReport,
					reportParameters.TimeZoneInfo));

			return $@"{reportName}.{reportDate}".Replace(' ', '_');
		}
	}
}