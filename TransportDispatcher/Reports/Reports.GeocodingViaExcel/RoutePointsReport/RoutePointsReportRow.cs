﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	internal class RoutePointsReportRow
	{
		public const string DateFormat = "dd.MM.yyyy";
		public const string TimeFormat = "HH:mm";
		public const string NumbFormat = "0.00";
		private static Dictionary<string, int> FieldMapping = new Dictionary<string, int>
		{
			{ nameof(LineNumber),      0 },
			{ nameof(ObjectName),      1 },
			{ nameof(Date),            2 },
			{ nameof(Time),            3 },
			{ nameof(AddressLocation), 4 },
		};
		public RoutePointsReportRow(DataRow row)
		{
			Row = row;
		}
		private int? GetInt(string text)
		{
			var parsedVal = default(int);
			return int.TryParse(text?.Trim(), out parsedVal)
				? parsedVal
				: (int?)null;
		}
		private int? GetInt(object value)
		{
			return (value as int?)
				?? GetInt(value.ToString());
		}
		private DateTime? GetDateTime(string text)
		{
			return TimeHelper.GetDateTime(text?.Trim());
		}
		private DateTime? GetDateTime(object value)
		{
			return (value as DateTime?)
				?? GetDateTime(value.ToString().Trim());
		}
		public string GetColumnNameByProperty(string propertyName)
		{
			var columnName = default(string);
			if (null != Row?.Table?.Columns)
			{
				var columnIdex = default(int);
				if (FieldMapping.TryGetValue(propertyName, out columnIdex))
				{
					if (columnIdex < Row.Table.Columns.Count)
						columnName = Row.Table.Columns[columnIdex].ColumnName;
					else
						columnName = $@"Column{columnIdex}";
				}
				else
				{
					columnName = $@"Column{columnIdex}";
				}
			}
			return columnName;
		}
		public object GetRawValueByProperty(string propertyName)
		{
			var rawValue = default(object);
			if (null != Row?.ItemArray?.Length)
			{
				var columnIdex = default(int);
				if (FieldMapping.TryGetValue(propertyName, out columnIdex))
				{
					if (columnIdex < Row.ItemArray.Length)
						rawValue = Row.ItemArray[columnIdex];
				}
			}
			return rawValue;
		}
		public string GetLocalizedFieldErrors(ResourceStringContainer resourceStringContainer)
		{
			var strings = resourceStringContainer;
			return
			(
				(!string.IsNullOrWhiteSpace(ObjectName) ? string.Empty : string.Format(strings[$@"{nameof(RoutePointsReportRow)}_{nameof(ObjectName)}_Error"], strings[GetColumnNameByProperty(nameof(ObjectName))]))                                    + "\n" +
				(Date.HasValue                          ? string.Empty : string.Format(strings[$@"{nameof(RoutePointsReportRow)}_{nameof(Date)}_Error"],       strings[GetColumnNameByProperty(nameof(Date)      )], DateTime.Now.ToString(DateFormat))) + "\n" +
				(Time.HasValue                          ? string.Empty : string.Format(strings[$@"{nameof(RoutePointsReportRow)}_{nameof(Time)}_Error"],       strings[GetColumnNameByProperty(nameof(Time)      )], DateTime.Now.ToString(TimeFormat)))
			).Trim();
		}

		public int?      LineNumber      => GetInt(     GetRawValueByProperty(nameof(LineNumber)));
		public string    ObjectName      =>       (     GetRawValueByProperty(nameof(ObjectName)))?.ToString();
		public DateTime? Date            => GetDateTime(GetRawValueByProperty(nameof(Date)      ))?.Date;
		public TimeSpan? Time            => GetDateTime(GetRawValueByProperty(nameof(Time)      ))?.TimeOfDay;
		public decimal?  Lat             { get; set; }
		public decimal?  Lng             { get; set; }
		public string    AddressLocation { get; set; }
		public bool      IsValid         => string.IsNullOrWhiteSpace(Row?.RowError) && !string.IsNullOrWhiteSpace(ObjectName) && Date.HasValue && Time.HasValue;
		public DataRow   Row             { get; private set; }
		public string    RowError        => Row.RowError;
		public static RoutePointsReportRow Empty
		{
			get
			{
				var tempTable = new DataTable("RoutePoints");
				tempTable.Columns.AddRange(FieldMapping
					.OrderBy(c => c.Value)
					.Select(c => new DataColumn($@"{nameof(RoutePointsReportRow)}_{c.Key}", typeof(object)))
					.ToArray());
				return new RoutePointsReportRow(tempTable.NewRow());
			}
		}
	}
}