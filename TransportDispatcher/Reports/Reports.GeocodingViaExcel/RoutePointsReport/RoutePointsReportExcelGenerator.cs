﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	internal class RoutePointsReportExcelGenerator : BaseReportExcelGenerator
	{
		private readonly ResourceStringContainer    _strings;
		private readonly List<CellInfo>             _cells;
		private readonly List<RoutePointsReportRow> _data;
		private readonly string                     _worksheetName;

		/// <summary> Переопределение листа по умолчанию для отчета </summary>
		protected override string WorksheetName => _worksheetName ?? base.WorksheetName;
		/// <summary> Переопределение метода формирования заголовка отчета (над таблицей) </summary>
		protected override void FillHeader()
		{
			// Необходимо переопределить для блокировки действий базового класса
		}
		/// <summary> Переопределение метода заполнения отчета </summary>
		protected override void FillTable()
		{
			if (0 == (_data?.Count ?? 0))
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "1"));
				foreach (var cellInfo in Cells)
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(RoutePointsReportRow.Empty)));
				AddRow(row);
			}
			foreach (var record in _data)
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "1"));
				foreach (var cellInfo in Cells)
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
				AddRow(row);
			}
		}
		/// <summary> Переопределение свойства описания ячеек отчета </summary>
		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}

		public RoutePointsReportExcelGenerator(
			RoutePointsReport               report,
			ExcelFilePickerReportParameters parameters,
			List<RoutePointsReportRow>      data) : base(report, parameters)
		{
			_data    = data;
			_strings = report.GetResourceStringContainer(parameters.Culture);

			var defaultRow = _data?.FirstOrDefault();
			if (null == defaultRow)
			{
				defaultRow = RoutePointsReportRow.Empty;
				_data = (new[] { defaultRow }).ToList();
			}
			_worksheetName = defaultRow.Row?.Table?.TableName;

			_cells = new List<CellInfo>();
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.LineNumber)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(RoutePointsReportRow.LineNumber)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x =>
					((RoutePointsReportRow)x).LineNumber
						?.ToString(parameters.Culture)
					??
					((RoutePointsReportRow)x).GetRawValueByProperty(nameof(RoutePointsReportRow.LineNumber))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.ObjectName)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(RoutePointsReportRow.ObjectName)),
				//Width     = null, // Автоподбор ширины
				Width     = 25F/25F, // 25 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x =>
					((RoutePointsReportRow)x).ObjectName
						?.ToString(parameters.Culture)
					??
					((RoutePointsReportRow)x).GetRawValueByProperty(nameof(RoutePointsReportRow.ObjectName))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.Date)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(RoutePointsReportRow.Date)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.DateTime,
				GetValue  = x =>
					((RoutePointsReportRow)x).Date
						?.ToString(RoutePointsReportRow.DateFormat)
					??
					((RoutePointsReportRow)x).GetRawValueByProperty(nameof(RoutePointsReportRow.Date))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.Time)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(RoutePointsReportRow.Time)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.TimeSpan,
				GetValue  = x =>
					(((RoutePointsReportRow)x).Date + ((RoutePointsReportRow)x).Time)
						?.ToString(RoutePointsReportRow.TimeFormat)
					??
					((RoutePointsReportRow)x).GetRawValueByProperty(nameof(RoutePointsReportRow.Time))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.AddressLocation)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(RoutePointsReportRow.AddressLocation)),
				//Width     = null, // Автоподбор ширины
				Width     = 40F/25F, // 40 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((RoutePointsReportRow)x).AddressLocation
					?.ToString(parameters.Culture),
			});
			if (_data.Any(r => !r.IsValid))
			{
				_cells.Add(new CellInfo
				{
					Key      = $@"{nameof(RoutePointsReportRow)}_{nameof(RoutePointsReportRow.RowError)}",
					//Width    = null, // Автоподбор ширины
					Width     = 40F/25F, // 40 в Excel
					//ExcelType = CellInfo.ExcelTypes.Number,
					GetValue = x => string.Concat(
						string.Empty,
						((RoutePointsReportRow)x).RowError,
						"\n",
						((RoutePointsReportRow)x).GetLocalizedFieldErrors(_strings))
						.Trim(),
				});
			}
			foreach (var cell in _cells)
				if (null == cell.Name || cell.Key == cell.Name)
					cell.Name = _strings[cell.Key];
		}
	}
}