﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	[Serializable]
	public class RoutePointsReportParameters : ExcelFilePickerReportParameters
	{
		internal List<RoutePointsReportRow> GetExcelData()
		{
			return GetExcelDataTable()
				?.Rows
				?.OfType<DataRow>()
				?.Select(r => new RoutePointsReportRow(r))
				?.ToList() ?? Enumerable.Empty<RoutePointsReportRow>().ToList();
		}
	}
}