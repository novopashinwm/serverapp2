﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	[Serializable]
	public class TripsAndMileageReportParameters : ExcelFilePickerReportParameters
	{
		internal List<TripsAndMileageReportRow> GetExcelData()
		{
			return GetExcelDataTable()
				?.Rows
				?.OfType<DataRow>()
				?.Select(r => new TripsAndMileageReportRow(r))
				?.ToList() ?? Enumerable.Empty<TripsAndMileageReportRow>().ToList();
		}
	}
}