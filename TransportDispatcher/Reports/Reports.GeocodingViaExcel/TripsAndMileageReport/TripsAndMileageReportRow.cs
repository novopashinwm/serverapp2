﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	internal class TripsAndMileageReportRow
	{
		public const string DateFormat = "dd.MM.yyyy";
		public const string TimeFormat = "HH:mm";
		public const string NumbFormat = "#,0.00";
		private static Dictionary<string, int> FieldMapping = new Dictionary<string, int>
		{
			{ nameof(LineNumber),         0 },
			{ nameof(ObjectName),         1 },
			{ nameof(DateBeg),            2 },
			{ nameof(DateEnd),            3 },
			{ nameof(TimeBeg),            4 },
			{ nameof(TimeEnd),            5 },
			{ nameof(MileageSrc),         6 },
			{ nameof(MileageClc),         7 },
			{ nameof(AddressLocationBeg), 8 },
			{ nameof(AddressLocationEnd), 9 },
		};
		public TripsAndMileageReportRow(DataRow row)
		{
			Row = row;
		}
		private int? GetInt(string text)
		{
			var parsedVal = default(int);
			return int.TryParse(text?.Trim(), out parsedVal)
				? parsedVal
				: (int?)null;
		}
		private int? GetInt(object value)
		{
			return (value as int?)
				?? GetInt(value.ToString());
		}
		private DateTime? GetDateTime(string text)
		{
			return TimeHelper.GetDateTime(text?.Trim());
		}
		private DateTime? GetDateTime(object value)
		{
			return (value as DateTime?)
				?? GetDateTime(value.ToString().Trim());
		}
		public string GetColumnNameByProperty(string propertyName)
		{
			var columnName = default(string);
			if (null != Row?.Table?.Columns)
			{
				var columnIdex = default(int);
				if (FieldMapping.TryGetValue(propertyName, out columnIdex))
				{
					if (columnIdex < Row.Table.Columns.Count)
						columnName = Row.Table.Columns[columnIdex].ColumnName;
					else
						columnName = $@"Column{columnIdex}";
				}
				else
				{
					columnName = $@"Column{columnIdex}";
				}
			}
			return columnName;
		}
		public object GetRawValueByProperty(string propertyName)
		{
			var rawValue = default(object);
			if (null != Row?.ItemArray?.Length)
			{
				var columnIdex = default(int);
				if (FieldMapping.TryGetValue(propertyName, out columnIdex))
				{
					if (columnIdex < Row.ItemArray.Length)
						rawValue = Row.ItemArray[columnIdex];
				}
			}
			return rawValue;
		}
		public string GetLocalizedFieldErrors(ResourceStringContainer resourceStringContainer)
		{
			var strings = resourceStringContainer;
			return
			(
				(!string.IsNullOrWhiteSpace(ObjectName) ? string.Empty : string.Format(strings[$@"{nameof(TripsAndMileageReportRow)}_{nameof(ObjectName)}_Error"], strings[GetColumnNameByProperty(nameof(ObjectName))]))                                  + "\n" +
				(DateBeg.HasValue                       ? string.Empty : string.Format(strings[$@"{nameof(TripsAndMileageReportRow)}_{nameof(DateBeg)}_Error"],    strings[GetColumnNameByProperty(nameof(DateBeg) )], DateTime.Now.ToString(DateFormat))) + "\n" +
				(DateEnd.HasValue                       ? string.Empty : string.Format(strings[$@"{nameof(TripsAndMileageReportRow)}_{nameof(DateEnd)}_Error"],    strings[GetColumnNameByProperty(nameof(DateEnd) )], DateTime.Now.ToString(DateFormat))) + "\n" +
				(TimeBeg.HasValue                       ? string.Empty : string.Format(strings[$@"{nameof(TripsAndMileageReportRow)}_{nameof(TimeBeg)}_Error"],    strings[GetColumnNameByProperty(nameof(TimeBeg) )], DateTime.Now.ToString(TimeFormat))) + "\n" +
				(TimeEnd.HasValue                       ? string.Empty : string.Format(strings[$@"{nameof(TripsAndMileageReportRow)}_{nameof(TimeEnd)}_Error"],    strings[GetColumnNameByProperty(nameof(TimeEnd) )], DateTime.Now.ToString(TimeFormat)))
			).Trim();
		}
		public int?      LineNumber         => GetInt     (GetRawValueByProperty(nameof(LineNumber)));
		public string    ObjectName         =>            (GetRawValueByProperty(nameof(ObjectName)))?.ToString();
		public DateTime? DateBeg            => GetDateTime(GetRawValueByProperty(nameof(DateBeg)   ))?.Date;
		public DateTime? DateEnd            => GetDateTime(GetRawValueByProperty(nameof(DateEnd)   ))?.Date;
		public TimeSpan? TimeBeg            => GetDateTime(GetRawValueByProperty(nameof(TimeBeg)   ))?.TimeOfDay;
		public TimeSpan? TimeEnd            => GetDateTime(GetRawValueByProperty(nameof(TimeEnd)   ))?.TimeOfDay;
		public string    MileageSrc         =>            (GetRawValueByProperty(nameof(MileageSrc)))?.ToString();
		public float?    MileageClc         { get; set; }
		public decimal?  LatBeg             { get; set; }
		public decimal?  LngBeg             { get; set; }
		public decimal?  LatEnd             { get; set; }
		public decimal?  LngEnd             { get; set; }
		public string    AddressLocationBeg { get; set; }
		public string    AddressLocationEnd { get; set; }
		public bool      IsValid            => string.IsNullOrWhiteSpace(Row?.RowError) && !string.IsNullOrWhiteSpace(ObjectName) && DateBeg.HasValue && DateEnd.HasValue && TimeBeg.HasValue && TimeEnd.HasValue;
		public DataRow   Row                { get; private set; }
		public string    RowError           => Row.RowError;
		public static TripsAndMileageReportRow Empty
		{
			get
			{
				var tempTable = new DataTable("TripsAndMileage");
				tempTable.Columns.AddRange(FieldMapping
					.OrderBy(c => c.Value)
					.Select(c => new DataColumn($@"{nameof(TripsAndMileageReportRow)}_{c.Key}", typeof(object)))
					.ToArray());
				return new TripsAndMileageReportRow(tempTable.NewRow());
			}
		}
	}
}