﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	internal class TripsAndMileageReportExcelGenerator : BaseReportExcelGenerator
	{
		private readonly ResourceStringContainer        _strings;
		private readonly List<CellInfo>                 _cells;
		private readonly List<TripsAndMileageReportRow> _data;
		private readonly string                         _worksheetName;

		/// <summary> Переопределение листа по умолчанию для отчета </summary>
		protected override string WorksheetName => _worksheetName ?? base.WorksheetName;
		/// <summary> Переопределение метода формирования заголовка отчета (над таблицей) </summary>
		protected override void FillHeader()
		{
			// Необходимо переопределить для блокировки действий базового класса
		}
		/// <summary> Переопределение метода заполнения отчета </summary>
		protected override void FillTable()
		{
			if (0 == (_data?.Count ?? 0))
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "1"));
				foreach (var cellInfo in Cells)
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(TripsAndMileageReportRow.Empty)));
				AddRow(row);
			}
			foreach (var record in _data)
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "1"));
				foreach (var cellInfo in Cells)
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
				AddRow(row);
			}
		}
		/// <summary> Переопределение свойства описания ячеек отчета </summary>
		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}

		public TripsAndMileageReportExcelGenerator(
			TripsAndMileageReport           report,
			TripsAndMileageReportParameters parameters,
			List<TripsAndMileageReportRow>  data) : base(report, parameters)
		{
			_data    = data;
			_strings = report.GetResourceStringContainer(parameters.Culture);

			var defaultRow = _data?.FirstOrDefault();
			if (null == defaultRow)
			{
				defaultRow = TripsAndMileageReportRow.Empty;
				_data = (new[] { defaultRow }).ToList();
			}
			_worksheetName = defaultRow.Row?.Table?.TableName;

			_cells = new List<CellInfo>();
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.LineNumber)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.LineNumber)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x =>
					((TripsAndMileageReportRow)x).LineNumber
						?.ToString(parameters.Culture)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.LineNumber))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.ObjectName)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.ObjectName)),
				//Width     = null, // Автоподбор ширины
				Width     = 25F/25F, // 25 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x =>
					((TripsAndMileageReportRow)x).ObjectName
						?.ToString(parameters.Culture)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.ObjectName))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.DateBeg)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.DateBeg)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.DateTime,
				GetValue  = x =>
					((TripsAndMileageReportRow)x).DateBeg
						?.ToString(TripsAndMileageReportRow.DateFormat)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.DateBeg))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.DateEnd)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.DateEnd)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.DateTime,
				GetValue  = x =>
					((TripsAndMileageReportRow)x).DateEnd
						?.ToString(TripsAndMileageReportRow.DateFormat)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.DateEnd))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.TimeBeg)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.TimeBeg)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.TimeSpan,
				GetValue = x =>
					(((TripsAndMileageReportRow)x).DateBeg + ((TripsAndMileageReportRow)x).TimeBeg)
						?.ToString(TripsAndMileageReportRow.TimeFormat)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.TimeBeg))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.TimeEnd)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.TimeEnd)),
				//Width     = null, // Автоподбор ширины
				Width     = 10F/25F, // 10 в Excel
				//ExcelType = CellInfo.ExcelTypes.TimeSpan,
				GetValue  = x =>
					(((TripsAndMileageReportRow)x).DateEnd + ((TripsAndMileageReportRow)x).TimeEnd)
						?.ToString(TripsAndMileageReportRow.TimeFormat)
					??
					((TripsAndMileageReportRow)x).GetRawValueByProperty(nameof(TripsAndMileageReportRow.TimeEnd))
						?.ToString(),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.MileageSrc)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.MileageSrc)),
				//Width     = null, // Автоподбор ширины
				Width     = 15F/25F, // 15 в Excel
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((TripsAndMileageReportRow)x).MileageSrc
					?.ToString(parameters.Culture),
			});

			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.MileageClc)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.MileageClc)),
				//Width     = null, // Автоподбор ширины
				Width     = 15F/25F, // 15 в Excel
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((TripsAndMileageReportRow)x).MileageClc
					?.ToString(TripsAndMileageReportRow.NumbFormat, NumberHelper.FormatInfo),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.AddressLocationBeg)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.AddressLocationBeg)),
				//Width     = null, // Автоподбор ширины
				Width     = 40F/25F, // 40 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((TripsAndMileageReportRow)x).AddressLocationBeg
					?.ToString(parameters.Culture),
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.AddressLocationEnd)}",
				Name      = defaultRow.GetColumnNameByProperty(nameof(TripsAndMileageReportRow.AddressLocationEnd)),
				//Width     = null, // Автоподбор ширины
				Width     = 40F/25F, // 40 в Excel
				//ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((TripsAndMileageReportRow)x).AddressLocationEnd
					?.ToString(parameters.Culture),
			});
			if (_data.Any(r => !r.IsValid))
			{
				_cells.Add(new CellInfo
				{
					Key      = $@"{nameof(TripsAndMileageReportRow)}_{nameof(TripsAndMileageReportRow.RowError)}",
					//Width    = null, // Автоподбор ширины
					Width     = 40F/25F, // 40 в Excel
					//ExcelType = CellInfo.ExcelTypes.Number,
					GetValue = x => string.Concat(
						string.Empty,
						((TripsAndMileageReportRow)x).RowError,
						"\n",
						((TripsAndMileageReportRow)x).GetLocalizedFieldErrors(_strings))
						.Trim(),
				});
			}
			foreach (var cell in _cells)
				if (null == cell.Name || cell.Key == cell.Name)
					cell.Name = _strings[cell.Key];
		}
	}
}