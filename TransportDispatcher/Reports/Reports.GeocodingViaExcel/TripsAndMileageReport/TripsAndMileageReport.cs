﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;
using Interfaces.Geo;

namespace Compass.Ufin.Reports.GeocodingViaExcel
{
	/// <summary> Отчёт "Поездки и пробег" </summary>
	/// <summary> Показывает информацию пробегах для каждого интервала </summary>
	[Guid("C2F431F3-C2DC-4069-9B3A-2812C592D71D")]
	public class TripsAndMileageReport : TssReportBase
	{
		/// <summary> Создает отчет, используя переданные параметры </summary>
		/// <param name="iReportParameters"> Параметры отчета </param>
		/// <returns></returns>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = (TripsAndMileageReportParameters)iReportParameters;
			// Формирование отчета
			return new ExcelReportFormatter(new TripsAndMileageReportExcelGenerator(
				this, parameters, GetRows(parameters)));
		}
		/// <summary> Получить данные для отчета </summary>
		/// <param name="srcTable"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private List<TripsAndMileageReportRow> GetRows(TripsAndMileageReportParameters parameters)
		{
			var strings = GetResourceStringContainer(parameters.Culture);
			var repRows = Enumerable.Empty<TripsAndMileageReportRow>().ToList();
			try
			{
				repRows = parameters.GetExcelData();
			}
			catch(Exception)
			{
				var defaultRow = TripsAndMileageReportRow.Empty;
				defaultRow.Row.RowError = strings[$@"{nameof(TripsAndMileageReport)}_InvalidFile_Error"];
				return (new[] { defaultRow }).ToList();
			}
			if (0 < repRows.Count)
			{
				// Выбираем информацию по департаменту
				var dept = parameters.DepartmentId.HasValue ? mclsInstance.GetDepartment(parameters.DepartmentId.Value) : null;
				// Поиск доступных объектов наблюдения
				var vehs = mclsInstance.GetVehiclesWithPositions(
					parameters.OperatorId,
					dept,
					new GetVehiclesArgs()
					{
						IncludeAddresses  = false,
						IncludeAttributes = true,
						IncludeZones      = false,
						IncludeLastData   = false,
						Culture           = parameters.Culture
					})
					.Select(v => new { v.id, v.Name })
					.ToList();

				var getRows = repRows
					// Только хорошие данные (с заполненными и понятными полями)
					.Where(r => r.IsValid)
					// Только доступные объекты наблюдения
					.GroupJoin(vehs,
						r => r.ObjectName,
						v => v.Name,
						(r, v) => new { r, v = v.DefaultIfEmpty() })
					.Select(x => new { x.r, v = x.v.FirstOrDefault() });
				var hasAccessVehicles = getRows
					// Только записи с доступными объектами наблюдения
					.Where(x => null != x.v)
					// Для оптимизации количества вызовов БД, группируем по запросу
					.GroupBy(r => new
					{
						ObjectId   = r.v.id,
						// Вычисляем начало периода
						LogTimeBeg = TimeHelper.GetSecondsFromBase(
							TimeZoneInfo.ConvertTimeToUtc(
								DateTime.SpecifyKind(r.r.DateBeg.Value + r.r.TimeBeg.Value, DateTimeKind.Unspecified),
								parameters.TimeZoneInfo)),
						// Вычисляем окончание периода
						LogTimeEnd = TimeHelper.GetSecondsFromBase(
							TimeZoneInfo.ConvertTimeToUtc(
								DateTime.SpecifyKind(r.r.DateEnd.Value + r.r.TimeEnd.Value, DateTimeKind.Unspecified),
								parameters.TimeZoneInfo)),
					})
					// Только первые 10000 штук, удовлетворяющие проверкам
					.Take(10000);
				// Получаем все позиции по всем доступным объектам
				var positions = mclsInstance.GetPositions(
					hasAccessVehicles.SelectMany(v => new[]
					{
						new VehicleLogTimeParam(v.Key.ObjectId, v.Key.LogTimeBeg),
						new VehicleLogTimeParam(v.Key.ObjectId, v.Key.LogTimeEnd),
					})
					.Distinct(),
					true);
				foreach (var hasAccessVehicle in hasAccessVehicles)
				{
					// Запрос пробега объекта наблюдения за период
					var grpMileage = mclsInstance.GetMoveHistoryDistanceValue(
						hasAccessVehicle.Key.LogTimeBeg, hasAccessVehicle.Key.LogTimeEnd, hasAccessVehicle.Key.ObjectId);
					// Заполняем только пробеги большие 0
					//if (0 < grpMileage) Заполняем все по просьбе заказчика
					{
						// Обновляем все записи в группе (если вдруг есть дубликаты)
						foreach (var grpRow in hasAccessVehicle)
							grpRow.r.MileageClc = grpMileage * 0.001F; // Значение получаем в метрах, требуются километры
					}
					var positionBeg = positions
						?.Where(p => p.VehicleId == hasAccessVehicle.Key.ObjectId && p.LogTime <= hasAccessVehicle.Key.LogTimeBeg)
						?.OrderByDescending(p => p.LogTime)
						?.FirstOrDefault();
					if (positionBeg.HasValue)
					{
						foreach (var grpRow in hasAccessVehicle)
						{
							grpRow.r.LatBeg = positionBeg.Value.Lat;
							grpRow.r.LngBeg = positionBeg.Value.Lng;
						}
					}
					var positionEnd = positions
						?.Where(p => p.VehicleId == hasAccessVehicle.Key.ObjectId && p.LogTime <= hasAccessVehicle.Key.LogTimeEnd)
						?.OrderByDescending(p => p.LogTime)
						?.FirstOrDefault();
					if (positionBeg.HasValue)
					{
						foreach (var grpRow in hasAccessVehicle)
						{
							grpRow.r.LatEnd = positionEnd.Value.Lat;
							grpRow.r.LngEnd = positionEnd.Value.Lng;
						}
					}
				}
				FillAddresses(parameters, hasAccessVehicles.SelectMany(v => v.Select(r => r.r)));
				var notAccessVehicles = getRows
					// Только строки с недоступными объектами наблюдения
					.Where(x => null == x.v)
					.Select(x => x.r);
				foreach (var notAccessVehicle in notAccessVehicles)
					if (null != notAccessVehicle.Row)
					{
						notAccessVehicle.Row.RowError = string.Concat(
							strings[$@"{nameof(TripsAndMileageReport)}Row_NoPermission_Error"],
							"\n",
							notAccessVehicle.Row.RowError)
							.Trim();
					}
			}
			return repRows;
		}
		private void FillAddresses(TripsAndMileageReportParameters parameters, IEnumerable<TripsAndMileageReportRow> reportRows)
		{
			var requests = reportRows
				?.Where(r => r.LatBeg.HasValue && r.LngBeg.HasValue)
				?.Select(r => new { Lat = r.LatBeg, Lng = r.LngBeg, Row = r })
				?.Union(reportRows
				?.Where(r => r.LatEnd.HasValue && r.LngEnd.HasValue)
				?.Select(r => new { Lat = r.LatEnd, Lng = r.LngEnd, Row = r }))
				?.GroupBy(g => new { g.Lat, g.Lng });

			var addressReqs = requests
				.Select((itm, idx) => new AddressRequest(
					idx,
					lat: (double)itm.Key.Lat.Value,
					lng: (double)itm.Key.Lng.Value))
				.ToArray();

			var addressRess = mclsInstance.GetAddresses(
				addressReqs,
				parameters.Culture,
				parameters.MapGuid);
			var responses = addressReqs
				.Join(
					addressRess,
					lft => lft.Id,
					rht => rht.Id,
					(lft, rht) => new { lft.Lat, lft.Lng, rht.Address, rht.Distance });

			foreach (var request in requests)
			{
				var response = responses.FirstOrDefault(r => r.Lat == (double?)request.Key.Lat && r.Lng == (double?)request.Key.Lng);
				if (null != response)
				{
					foreach (var row in request)
					{
						if (row.Row.LatBeg == request.Key.Lat && row.Row.LngBeg == request.Key.Lng)
							row.Row.AddressLocationBeg = response.Address;
						// без else, т.к. координаты могут быть одинаковые и в начале пути и в конце
						if (row.Row.LatEnd == request.Key.Lat && row.Row.LngEnd == request.Key.Lng)
							row.Row.AddressLocationEnd = response.Address;
					}
				}
			}
		}
		/// <summary> Возвращает экземпляр класса, содержащий набор параметров для данного отчета </summary>
		/// <param name="settings"></param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new TripsAndMileageReportParameters();
		}
		/// <summary> Функция проверяет переданный набор параметров на корректность заполнения </summary>
		/// <param name="iReportParameters"></param>
		/// <returns></returns>
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as TripsAndMileageReportParameters;
			if (parameters == null)
				return false;
			// Никакие проверки делать не нужно, чтобы вывести все ошибки в сам отчет
			return true;
		}
		/// <summary> Текстовое описание отчета/ </summary>
		/// <param name="culture"> Культура, для которой следует вернуть описание </param>
		/// <returns></returns>
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[$@"{nameof(TripsAndMileageReport)}"];
		}
		/// <summary> Возвращает описание, что именно должен вернуть отчёт, в том числе важные замечания </summary>
		/// <param name="culture"> Культура, для которой следует вернуть описание </param>
		/// <returns></returns>
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[$@"{nameof(TripsAndMileageReport)}_Description"];
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeocodingViaExcelReports.ToString();
		}
		/// <summary> Порядковый номер отчета </summary>
		public override int Order => 2;
		public override bool IsSubscriptionVisible => false;
		/// <summary> Возвращает список поддерживаемых отчётом форматов </summary>
		/// <returns></returns>
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Excel;
		}
		public override string GetReportFileName(ReportParameters reportParameters)
		{
			if (reportParameters == null)
				throw new ArgumentNullException(nameof(reportParameters));

			var reportName = PrepareStringToFileName(
				GetReportName(reportParameters.Culture));
			// Готовим дату отчета в часовом поясе пользователя
			var reportDate = FormatDateTimeForFileName(
				TimeZoneInfo.ConvertTimeFromUtc(
					reportParameters.DateReport,
					reportParameters.TimeZoneInfo));

			return $@"{reportName}.{reportDate}".Replace(' ', '_'); ;
		}
	}
}