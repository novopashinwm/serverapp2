﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Resources;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт о прохождении через зоны </summary>
	[Guid("6E2DE7AD-2656-4812-BC1C-0EEA32613662")]
	public class GeoZoneOverTssReport : TssReportBase
	{
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ReportZoneOver"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["ReportZoneOverDescription"];
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var reportParameters = new GeoZoneOverTssReportParameters();

			return reportParameters;
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as GeoZoneOverTssReportParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleProp == null)
				return false;

			var vehicleId = parameters.VehicleProp.Tag as int?;
			if (vehicleId == null || vehicleId.Value <= 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override int Order
		{
			get { return 0; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GeoPointReports.ToString();
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				//Проверка был ли передан набор параметров для отчета.
				if (iReportParameters == null)
					return new GeoZoneOverTssReportParameters();

				//Если набор параметров не был передан в данный отчет, то создать его заново.
				//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
				var reportParameters = (GeoZoneOverTssReportParameters)iReportParameters;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return null;
			}
		}
		protected GeoZoneOverDataSet GetReportTypedDataSet(ReportParameters iReportParameters)
		{
			GeoZoneOverDataSet dsReport = null;

			try
			{
				dsReport = new GeoZoneOverDataSet();

				if (iReportParameters != null && iReportParameters.ShowBlankReportProp == false)
				{
					var reportParameters = (GeoZoneOverTssReportParameters)iReportParameters;

					DateTime dtFrom = reportParameters.DateTimeInterval.DateFrom;
					DateTime dtTo = reportParameters.DateTimeInterval.DateTo;

					// Заполнение заголовка отчета (Формирование строки в таблице ReportHeader)
					GeoZoneOverDataSet.HeaderRow rowHeader = dsReport.Header.NewHeaderRow();
					rowHeader.mo = mclsInstance.GetVehicleById(iReportParameters.OperatorId, reportParameters.VehicleId, iReportParameters.Culture).Name;
					rowHeader.From = dtFrom.ToShortDateString() + " " + dtFrom.ToShortTimeString();
					rowHeader.To = dtTo.ToShortDateString() + " " + dtTo.ToShortTimeString();
					dsReport.Header.AddHeaderRow(rowHeader);

					var zoneIds = reportParameters.ZoneIds;
					if (zoneIds == null ||
						zoneIds.Count == 0)
					{
						zoneIds = mclsInstance.GetGeoZones(
							iReportParameters.OperatorId,
							iReportParameters.DepartmentId != null &&
							mclsInstance.FilterByDepartment(iReportParameters.OperatorId)
								? iReportParameters.DepartmentId
								: null, false).Select(z => z.Id).ToList();
					}

					// Получение данных для отчета из БД.
					DataSet ds = mclsInstance.GetDataFromDB(
						ParamsPreparingForStorProcOperator(
							(time_t)iReportParameters.ToUtc(dtFrom),
							(time_t)iReportParameters.ToUtc(dtTo),
							reportParameters.VehicleId,
							reportParameters.OperatorId,
							zoneIds.ToArray()
							),
						"GetLogAndZonesForVehicleForOperator",
						new[] { "data", "total" }
						);

					if (ds.Tables["data"].Rows.Count > 0 && ds.Tables["total"].Rows.Count > 0)
					{
						foreach (DataRow dataRw in ds.Tables["data"].Rows)
						{
							//id, dtIn, dtOut, zone, zone_id, Distance, AvgDistance, StopsTime, StandartDistance, StandartDistanceDiff
							GeoZoneOverDataSet.DataRow rowData = dsReport.Data.NewDataRow();

							rowData.dtIn = (int)dataRw["dtIn"];
							rowData.dtOut = (int)dataRw["dtOut"];
							rowData.DateTime =
								iReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(rowData.dtIn)) +
								" - " +
								iReportParameters.FormatUtcAsLocal(TimeHelper.GetDateTimeUTC(rowData.dtOut));
							rowData.Zone = (string)dataRw["zone"];
							rowData.Distance = (double)dataRw["Distance"];

							//Подсчет продолжительности участка
							rowData.IntervalStr = GetIntervalString(rowData.dtOut - rowData.dtIn, iReportParameters.Culture);

							if (!dataRw.IsNull("AvgDistance"))
								rowData.AvgDistance = (double)dataRw["AvgDistance"];

							if (!dataRw.IsNull("StopsTime"))
							{
								rowData.StopsTime = GetIntervalString((int)dataRw["StopsTime"], iReportParameters.Culture);
							}

							if (!dataRw.IsNull("StandartDistance"))
								rowData.StandartDistance = (double)dataRw["StandartDistance"];

							if (!dataRw.IsNull("StandartDistanceDiff"))
								rowData.StandartDistanceDiff = (double)dataRw["StandartDistanceDiff"];

							if (!dataRw.IsNull("zone_id_from"))
								rowData.zone_id_from = (int)dataRw["zone_id_from"];

							if (!dataRw.IsNull("zone_id_to"))
								rowData.zone_id_to = (int)dataRw["zone_id_to"];

							if (!dataRw.IsNull("zone_name_from"))
								rowData.zone_name_from = (string)dataRw["zone_name_from"];

							if (!dataRw.IsNull("zone_name_to"))
								rowData.zone_name_to = (string)dataRw["zone_name_to"];

							if (!dataRw.IsNull("Odometer"))
								rowData.Odometer = Convert.ToDecimal(dataRw["Odometer"]);

							dsReport.Data.AddDataRow(rowData);
						}

						var odometerPresent = Convert.ToInt32(dsReport.Data.Compute("count(Odometer)", "Odometer is not null")) != 0;

						if (odometerPresent)
						{
							//Расчет пробега по одометру и отклонения от одометра
							for (var i = 1; i < dsReport.Data.Rows.Count - 1; ++i)
							{
								var row = (GeoZoneOverDataSet.DataRow)dsReport.Data.Rows[i];
								if (row.IsAvgDistanceNull())
									continue;


								var prevRow = ((GeoZoneOverDataSet.DataRow)dsReport.Data.Rows[i - 1]);
								if (prevRow.IsOdometerNull())
									continue;
								var nextRow = ((GeoZoneOverDataSet.DataRow)dsReport.Data.Rows[i + 1]);
								if (nextRow.IsOdometerNull())
									continue;
								row.OdometerDistance = nextRow.Odometer - prevRow.Odometer;

								if (0.001 <= row.AvgDistance)
								{
									row.OdometerDistanceDeviation =
										(int)(100 * (row.OdometerDistance - (decimal)row.AvgDistance) / (decimal)row.AvgDistance);
								}
								else if (0 < row.OdometerDistance)
								{
									row.OdometerDistanceDeviation = 100;
								}
							}
						}

						//Заполнение хэдера
						//vehicle_id, total_distance, total_in_zone_time, total_out_zone_time, FuelStandartPer100,
						//total_stops_time, total_standart_distance, total_standart_distance_diff
						DataRow totalRow = ds.Tables["total"].Rows[0];
						rowHeader.Vehicle_id = (int)totalRow["vehicle_id"];
						rowHeader.TotalDistance = (double)totalRow["total_distance"];
						rowHeader.TotalInZoneTimeStr = GetIntervalString((int)totalRow["total_in_zone_time"], iReportParameters.Culture);
						rowHeader.TotalOutZoneTimeStr = GetIntervalString((int)totalRow["total_out_zone_time"], iReportParameters.Culture);
						rowHeader.TotalStandartDistance = (double)totalRow["total_standart_distance"];
						rowHeader.TotalStandartDistanceDiff = (double)totalRow["total_standart_distance_diff"];
						rowHeader.TotalStopsTimeStr = GetIntervalString((int)totalRow["total_stops_time"], iReportParameters.Culture);
						rowHeader.FuelStandartPer100 = (double)totalRow["FuelStandartPer100"];
						rowHeader.OdometerPresent = odometerPresent;
					}
					else
					{
						rowHeader.Vehicle_id = -1;
						rowHeader.TotalDistance = 0;
						rowHeader.TotalInZoneTimeStr = "";
						rowHeader.TotalOutZoneTimeStr = "";
						rowHeader.TotalStandartDistance = 0;
						rowHeader.TotalStandartDistanceDiff = 0;
						rowHeader.TotalStopsTimeStr = "";
						rowHeader.FuelStandartPer100 = -1;
					}

				}

				//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
				ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
				//Добавление в датасет данных атрибутов предприятия.
				dsReport.Merge(dsEstablishmentAttributes);

				return dsReport;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);
				return null;
			}
			finally
			{
				if (dsReport != null)
					dsReport.Dispose();
			}
		}

		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var typedData = GetReportTypedDataSet(reportParameters);
			return GetReportHtml(typedData, (GeoZoneOverTssReportParameters)reportParameters);
		}

		string GetReportHtml(GeoZoneOverDataSet data, GeoZoneOverTssReportParameters pars)
		{
			var stringBuilder = new StringBuilder();
			var @string = GetResourceStringContainer(pars.Culture);

			//stringBuilder.Append(string.Format("{0}: {1}<br /><br />", @string["monitoringObject"], vehicleObj.garageNum));

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				// Подитоги по дням.
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteStartElement("thead");
				xw.WriteStartElement("tr");

				xw.WriteElementString("th", @string["DateTime"]);
				xw.WriteElementString("th", @string["IntervalStr"]);
				xw.WriteElementString("th", @string["Zone"]);
				xw.WriteElementString("th", @string["Distance"]);
				xw.WriteElementString("th", @string["lblRunAndZone"]);
				xw.WriteElementString("th", @string["lblStandard"]);
				xw.WriteElementString("th", @string["lblDeviation"]);
				xw.WriteElementString("th", @string["StopsTime"]);
				xw.WriteElementString("th", @string["WatchOnMap"]);

				xw.WriteEndElement();
				xw.WriteEndElement();

				xw.WriteStartElement("tbody");

				var counter = 0;

				foreach (GeoZoneOverDataSet.DataRow dr in data.Tables["Data"].Rows)
				{
					counter++;
					int intervalDateStartInt;
					int intervalDateEndInt;

					int.TryParse(dr["dtIn"].ToString(), out intervalDateStartInt);
					int.TryParse(dr["dtOut"].ToString(), out intervalDateEndInt);

					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
							, pars.ApplicationPath
							, pars.VehicleId
							, TimeHelper.GetLocalTime(intervalDateStartInt, pars.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss")
							, TimeHelper.GetLocalTime(intervalDateEndInt, pars.TimeZoneInfo).ToString("dd.MM.yyyy+HH:mm:ss"));

					xw.WriteStartElement("tr");

					xw.WriteElementString("td", string.Format("{0}", dr["DateTime"]));
					xw.WriteElementString("td", string.Format("{0}", dr["IntervalStr"]));
					xw.WriteElementString("td", string.Format("{0}", dr["Zone"]));
					xw.WriteElementString("td", string.Format("{0}", dr["Distance"]));
					xw.WriteElementString("td", string.Format("{0}", dr["AvgDistance"]));

					if (!string.IsNullOrWhiteSpace(dr["zone_id_from"].ToString()) && !string.IsNullOrWhiteSpace(dr["zone_id_to"].ToString()))
					{
						var rowid = string.Format("row{0}", counter);

						var link = string.Format("myReports.changeStandartDistance({0}, {1}, '{2}', '{3}', {4}, '{5}')",
							dr["zone_id_from"],
							dr["zone_id_to"],
							dr["zone_name_from"],
							dr["zone_name_to"],
							string.IsNullOrWhiteSpace(dr["StandartDistance"].ToString()) ? 0 : dr["StandartDistance"],
							rowid);

						xw.WriteStartElement("td");
						xw.WriteStartElement("a");
						xw.WriteAttributeString("href", string.Format("javascript:{0}", link));
						xw.WriteAttributeString("id", rowid);
						xw.WriteString(string.IsNullOrWhiteSpace(dr["StandartDistance"].ToString())
							? @string["SetDistanceStandard"]
							: dr["StandartDistance"].ToString());
						xw.WriteEndElement();
						xw.WriteEndElement();
					}
					else
						xw.WriteElementString("td", string.Format("{0}", dr["StandartDistance"]));

					xw.WriteElementString("td", string.Format("{0}", dr["StandartDistanceDiff"]));

					xw.WriteElementString("td", string.Format("{0}", dr["StopsTime"]));

					xw.WriteStartElement("td");
					xw.WriteStartElement("a");
					xw.WriteAttributeString("href", path.Replace(' ', '+'));
					xw.WriteString(@string["WatchOnMap"]);
					xw.WriteEndElement();
					xw.WriteEndElement();

					xw.WriteEndElement();
				}

				var totalDataRow = data.Tables["Header"].Rows[0];

				xw.WriteStartElement("tr");

				xw.WriteElementString("td", @string["Total"]);
				xw.WriteElementString("td", string.Empty);


				xw.WriteElementString("td", string.Format("{2}: {0} / {3}: {1}",
					totalDataRow["TotalInZoneTimeStr"],
					totalDataRow["TotalOutZoneTimeStr"],
					@string["TotalInZone"],
					@string["TotalOutZone"]
					));
				xw.WriteElementString("td", totalDataRow["TotalDistance"].ToString());
				xw.WriteElementString("td", string.Empty);
				xw.WriteElementString("td", totalDataRow["TotalStandartDistance"].ToString());
				xw.WriteElementString("td", totalDataRow["TotalStandartDistanceDiff"].ToString());
				xw.WriteElementString("td", totalDataRow["TotalStopsTimeStr"].ToString());
				xw.WriteElementString("td", string.Empty);

				//</tr>
				xw.WriteEndElement();

				//</tbody>
				xw.WriteEndElement();
				//</table>
				xw.WriteEndElement();

				decimal fuelStandartPer100;
				decimal totalDistance;
				decimal.TryParse(totalDataRow["TotalStopsTimeStr"].ToString(), out fuelStandartPer100);
				decimal.TryParse(totalDataRow["TotalDistance"].ToString(), out totalDistance);


				decimal totalFuelSpent = totalDistance / 100 * fuelStandartPer100;

				xw.WriteStartElement("div");
				xw.WriteElementString("p", string.Format(@string["TotalDeviation"],
					totalFuelSpent.ToString("0"),
					fuelStandartPer100.ToString("0")
					));

				//</div>
				xw.WriteEndElement();
			}
			return stringBuilder.ToString();
		}
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			ReportClassWrapper rptResultReport = null;
			GeoZoneOverDataSet dataset         = null;
			var processed = false;
			try
			{
				dataset = GetReportTypedDataSet(iReportParameters);

				//Создание экземпляра отчета.
				var isMLP = iReportParameters != null &&
							((iReportParameters.DeviceCapabilities & DeviceCapability.AllowAskPosition) != 0);

				rptResultReport = isMLP ? new GeoZoneOverReportMLP() : new GeoZoneOverReport();

				//Локализация надписей отчета
				if (iReportParameters != null)
					TranslateReport<GeoZoneOverTssReport>(rptResultReport, iReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(dataset);
				//Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();

				if (dataset != null)
					dataset.Dispose();
			}
		}

		private string GetIntervalString(int intervalInSeconds, CultureInfo culture)
		{
			var @string = GetResourceStringContainer(culture);
			var interval = new TimeSpan(0, 0, intervalInSeconds);
			var sb = new StringBuilder();
			if (interval.Days > 0)
				sb.Append(string.Format("{0:D1}{1} ", interval.Days, @string["Days"]));
			sb.Append(string.Format("{0:D1}{1} ", interval.Hours, @string["Hours"]));
			sb.Append(string.Format("{0:D1}{1} ", interval.Minutes, @string["Minutes"]));

			return sb.ToString();
		}

		#region Подготовка списка параметров для передачи в функцию вызова хранимой процедуры

		private static ArrayList ParamsPreparingForStorProcOperator(int dtFrom, int dtTo, int vehicleID, int operatorId, int[] zoneIds)
		{
			var arParams = new ArrayList
				{
					new ParamValue("@dtFrom", dtFrom),
					new ParamValue("@dtTo", dtTo),
					new ParamValue("@vehicle_id", vehicleID)
				};

			if (operatorId > 0)
				arParams.Add(new ParamValue("@operatorId", operatorId));

			//Собираем строку со списком id зон с разделителями ';'
			if (zoneIds.Any())
				arParams.Add(new ParamValue("@zoneIDList", string.Join(";", zoneIds.ToArray())));

			return arParams;
		}
		#endregion Подготовка списка параметров для передачи в функцию вызова хранимой процедуры
	}
}