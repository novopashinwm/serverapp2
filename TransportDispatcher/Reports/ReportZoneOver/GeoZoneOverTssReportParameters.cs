﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	[Serializable]
	public class GeoZoneOverTssReportParameters : ReportParameters, IMonitoreeObjectContainer
	{
		// Набор данных для Editor-ов
		[NonSerialized]
		private DataSet _dsParamsForEditor;
		public DataSet DsParamsForEditor
		{
			get { return _dsParamsForEditor; }
			set { _dsParamsForEditor = value; }
		}

		/// <summary>
		/// Время начала периода, на который формируется отчет.
		/// </summary>
		protected string timeFrom = DateTime.Now.ToShortTimeString();
		/// <summary>
		/// Время окончания периода, на который формируется отчет.
		/// </summary>
		protected string timeTo = DateTime.Now.ToShortTimeString();
		/// <summary>
		/// Объект слежения
		/// </summary>
		protected TagListBoxItem vehicleProp = new TagListBoxItem("", null);

		protected int interval = 1;
		protected int count = 10000;

		/// <summary>
		/// Показывать/не показывать адрес в отчёте
		/// </summary>
		protected bool showAddress;

		/// <summary>
		/// GUID карты
		/// </summary>
		[Obsolete("Do not use, dedicated for compatibility for old versions deserialization")]
		protected string mapGuid = Guid.Empty.ToString();

		[Obsolete("Do not use, dedicated for compatibility for old versions deserialization")]
		private int operatorID = 0;

		/// <summary>
		/// Гаражный номер ТС.
		/// </summary>
		[DisplayName("TSNumberDN", 9, true), Category("ReportParsCat"), Description("TSNumberDesc")]
		public TagListBoxItem VehicleProp
		{
			get
			{
				return vehicleProp;
			}
			set
			{
				vehicleProp = value;
			}
		}

		/// <summary>
		/// Свойство - Дата начала периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
		[Browsable(true)]
		public override DateTime DateFrom
		{
			get
			{
				return dtDateFrom.Date;
			}
			set
			{
				dtDateFrom = value.Date;
			}
		}

		/// <summary>
		/// Время начала периода, на который формируется доклад.
		/// </summary>
		[DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
		public string TimeFrom
		{
			get
			{
				return timeFrom;
			}
			set
			{
				//Вызов функции проверки введенного пользователем значения времени.
				timeFrom = TimeInputCheck(value);
			}
		}

		/// <summary>
		/// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
		[Browsable(true)]
		public override DateTime DateTo
		{
			get
			{
				return dtDateTo.Date;
			}
			set
			{
				dtDateTo = value.Date;
			}
		}

		/// <summary>
		/// Время окончания периода, на который формируется доклад.
		/// </summary>
		[DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
		public string TimeTo
		{
			get
			{
				return timeTo;
			}
			set
			{
				//Вызов функции проверки введенного пользователем значения времени.
				timeTo = TimeInputCheck(value);
			}
		}

		/// <summary>
		/// Интервал
		/// </summary>
		[DisplayName("IntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
		[Browsable(false)]
		public int Interval
		{
			get
			{
				return interval;
			}
			set
			{
				interval = value;
			}
		}

		/// <summary>
		/// Количество позиций
		/// </summary>
		[DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
		[Browsable(false)]
		public int PositionsСount
		{
			get
			{
				return count;
			}
			set
			{
				count = value;
			}
		}

		/// <summary>
		/// Показывать/не показывать адрес в отчёте
		/// </summary>
		[Browsable(false)]
		public bool ShowAddress
		{
			get
			{
				return showAddress;
			}
			set
			{
				showAddress = value;
			}
		}

		private int[] _zoneIds = new int[0];
		[DisplayName("ZoneIds"), Type(typeof(List<int>)), ControlType(ReportParametersUtils.ZonesPicker, "ZonesPicker"), Order(2)]
		public List<int> ZoneIds
		{
			get
			{
				if (_zoneIds == null)
					_zoneIds = new int[0];
				return _zoneIds.ToList();
			}
			set { _zoneIds = value.ToArray(); }
		}

		public IdType MonitoreeObjectIdType
		{
			get { return IdType.Vehicle; }
		}

		public int MonitoreeObjectId
		{
			get { return VehicleId; }
		}

		[DisplayName("VehicleId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker"), Order(1), Options(SystemRight.PathAccess)]
		public int VehicleId
		{
			get { return (int)vehicleProp.Tag; }
			set
			{
				vehicleProp.Tag = value;
			}
		}

		[DisplayName("DateTimeInterval"), Type(typeof(DateTimeInterval))]
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod"), Options("Accuracy:0"), Order(255)]
		public override DateTimeInterval DateTimeInterval { get; set; }
	}
}