﻿using System;

namespace Compass.Ufin.Reports.ShareLinks
{
	internal class ShareLinksReportRow
	{
		internal int    LogTimeBeg { get; set; }
		internal int    LogTimeEnd { get; set; }
		internal int    ObjectId   { get; set; }
		internal string ObjectName { get; set; }
		internal string ObjectLink { get; set; }
	}
}