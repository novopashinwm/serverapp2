﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.ShareLinks
{
	/// <summary> Отчет массового создания ссылок доступа к объектам </summary>
	[Guid("111DDBBE-E95F-4988-AB63-8FFF0F2B272B")]
	public sealed partial class ShareLinksReport : TssReportBase
	{
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = (ShareLinksReportParameters)iReportParameters;
			// Получение данных
			var data = GetRows(parameters).ToList();
			// Формирование отчета
			return new ShareLinksReportFormatter(new ShareLinksReportExcelGenerator(this, parameters, data));
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new ShareLinksReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new ShareLinksReportParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (ShareLinksReportParameters)iReportParameters;

				// Если есть набор идентификаторов, то очищаем идентификатор группы и считаем, что есть произвольный набор машин
				if (0 < (reportParameters.VehicleIds?.Count ?? 0))
					reportParameters.VehicleGroupId = -1;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError(GetType() + ": {0}", ex);
				return null;
			}
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var reportParameters = iReportParameters as ShareLinksReportParameters;
			if (reportParameters == null)
				return false;

			if (0 >= reportParameters.VehicleGroupId && 0 == (reportParameters.VehicleIds?.Count ?? 0))
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		/// <summary> Текстовое описание отчета </summary>
		/// <param name="culture">Культура, для которой следует вернуть описание</param>
		/// <returns></returns>
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(ShareLinksReport) + "_Name"];
		}
		/// <summary> Возвращает описание, что именно должен вернуть отчёт, в том числе важные замечания </summary>
		/// <param name="culture">Культура, для которой следует вернуть описание.</param>
		/// <returns></returns>
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(ShareLinksReport) + "_Desc"];
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		/// <summary> Порядок отчета </summary>
		public override int Order => 1;
		/// <summary> Поддерживаемые форматы отчета </summary>
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Html;
			yield return ReportTypeEnum.Excel;
			//yield return ReportTypeEnum.Acrobat;
		}
		private IEnumerable<ShareLinksReportRow> GetRows(ShareLinksReportParameters parameters)
		{
			if (null == parameters)
				return Enumerable.Empty< ShareLinksReportRow>();
			//////////////////////////////
			var neededVehicleRights = new[] { SystemRight.ShareLinkAccess };
			//////////////////////////////
			var reportVehicles = default(List<Vehicle>);
			if (0 != (parameters.VehicleIds?.Count ?? 0))
			{
				reportVehicles = mclsInstance.GetVehiclesWithPositions(
					parameters.OperatorId, null, new GetVehiclesArgs
					{
						VehicleIDs = parameters.VehicleIds.ToArray(),
						Culture    = parameters.Culture
					})
					.Where(v => neededVehicleRights.All(r => v?.rights?.Contains(r) ?? false))
					.ToList();
			}
			else
			{
				reportVehicles = mclsInstance.GetVehiclesByVehicleGroup(
					parameters.VehicleGroupId,
					parameters.OperatorId,
					parameters.Culture,
					neededVehicleRights);
			}
			// Формируем параметры интервала отчета (полученные из параметров, указанных пользователем)
			var logTimeBeg = parameters.DateFromInt;
			var logTimeEnd = parameters.DateToInt;
			// Формируем набор записей отчета
			return reportVehicles
				.Select(v =>
				{
					var reportRow = new ShareLinksReportRow
					{
						LogTimeBeg = logTimeBeg,
						LogTimeEnd = logTimeEnd,
						ObjectId   = v.id,
						ObjectName = v.Name,
					};
					var dtoShareLink = default(ShareLink);
					try
					{
						dtoShareLink = mclsInstance.GetOrCreateShareLink(
							operatorId  : parameters.OperatorId,
							logTimeBeg  : logTimeBeg,
							logTimeEnd  : logTimeEnd,
							vehicleId   : v.id,
							vehicleName : v.Name
						);
					}
					catch
					{
					}
					if (default(ShareLink) != dtoShareLink)
					{
						reportRow.ObjectLink = new UriBuilder($@"https://{parameters.ApplicationPath}/services/share.aspx")
						{
							Query = $@"link={dtoShareLink.LinkGuid}"
						}.ToString();
					}
					return reportRow;
				});
		}
	}
}