﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.ShareLinks
{
	[Serializable]
	public class ShareLinksReportParameters : ReportParameters
	{
		/// <summary> Группа объектов наблюдения по которой строить отчет </summary>
		[ControlType(ReportParametersUtils.VehicleGroupPicker)]
		[Order(001)]
		public int VehicleGroupId { get; set; }
		/// <summary> Список объектов наблюдения, чтобы строить отчёт групповой отчёт не создавая группу </summary>
		[ControlType(ReportParametersUtils.VehiclesPicker)]
		[Order(002)]
		// Фильтровать по наличию права управления
		[Options(SystemRight.SecurityAdministration)]
		public List<int> VehicleIds { get; set; } // Тип менять нельзя, т.к. VehiclesPicker считает, что там List<int>
		/// <summary> Интервал построения отчета </summary>
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod")]
		[Order(255)]
		[Options("Accuracy:3;TimeDirection:1")]
		public override DateTimeInterval DateTimeInterval { get; set; }
	}
}