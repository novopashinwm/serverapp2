﻿using System;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.ShareLinks
{
	public sealed partial class ShareLinksReport
	{
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			return GetReportHtml(reportParameters as ShareLinksReportParameters);
		}
		private string GetReportHtml(ShareLinksReportParameters reportParameters)
		{
			// Проверка параметров
			if (null == reportParameters)
				return default(string);
			// Получение ресурсов
			var strings = GetResourceStringContainer(reportParameters.Culture);
			// Получение данных
			var reportRows = GetRows(reportParameters)
				.OrderBy(r => r.ObjectName);
			var html = (XNamespace)"http://www.w3.org/1999/xhtml";
			// Если нет данных, пишем об отсутствии и выходим
			if (!reportRows.Any())
				return (new XElement(html + "span", strings["NoData"])).ToString();
			// Формирование отчета (с заголовком с именем отчета)
			var xdiv = new XElement(html + "div");
			var cols = new []
			{
				new
				{
					Key      = $@"{nameof(ShareLinksReportRow)}_{nameof(ShareLinksReportRow.ObjectName)}",
					GetValue = new Func<ShareLinksReportRow, XNode>(x => new XText(x.ObjectName)),
					Width    = 20,
				},
				new
				{
					Key      = $@"{nameof(ShareLinksReportRow)}_{nameof(ShareLinksReportRow.ObjectLink)}",
					GetValue = new Func<ShareLinksReportRow, XNode>(x => new XElement("a", new XAttribute("href", x.ObjectLink), x.ObjectLink)),
					Width    = 80,
				},
			};
			// Заголовок таблицы группы
			var thead = new XElement(html + "thead",
				// Строка названий колонок в заголовке таблицы
				new XElement("tr",
					cols.Select(c =>
						new XElement("td",
							new XAttribute("style", $@"width:{c.Width}%;text-align:center;font-weight:bold;"),
							strings[c.Key]
						)
					)
				)
			);
			// Данные таблицы
			var tbody = new XElement(html + "tbody");
			foreach (var reportRow in reportRows.OrderBy(r => r.ObjectName))
			{
				tbody.Add(
					new XElement("tr",
						cols.Select(c =>
							new XElement("td",
								new XAttribute("style", $@"width:{c.Width}%;text-align:left;font-weight:normal;"),
								c.GetValue(reportRow)
							)
						)
					)
				);
			}
			var table = new XElement(html + "table",
				new XAttribute("class", "reportTable withBorders"),
					thead, tbody);
			return table.ToString();
		}
	}
}