﻿using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.ShareLinks
{
	class ShareLinksReportFormatter : BaseReportExcelFormatter
	{
		private readonly BaseExcelGenerator _excelGenerator;

		public ShareLinksReportFormatter(BaseExcelGenerator excelGenerator)
		{
			_excelGenerator = excelGenerator;
		}

		protected override string GetExcelXmlString()
		{
			return _excelGenerator.Create();
		}
	}
}