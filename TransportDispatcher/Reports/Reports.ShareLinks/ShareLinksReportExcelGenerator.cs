﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.ShareLinks
{
	internal class ShareLinksReportExcelGenerator : BaseReportExcelGenerator
	{
		private readonly ShareLinksReport           _report;
		private readonly ShareLinksReportParameters _params;
		private readonly List<ShareLinksReportRow>  _data;
		private readonly ResourceStringContainer    _strings;
		private readonly string                     _worksheetName;
		private readonly List<CellInfo>             _cells;
		/// <summary> Переопределение листа по умолчанию для отчета </summary>
		protected override string WorksheetName => _worksheetName ?? base.WorksheetName;
		/// <summary> Переопределение метода формирования заголовка отчета (над таблицей) </summary>
		protected override void FillHeader()
		{
			//////////////////////////////////////////////////////////////////////////
			var reportBegLoc    = TimeHelper.GetLocalTime(_params.DateFromInt, _params.TimeZoneInfo);
			var reportBegOffset = new DateTimeOffset(reportBegLoc, _params.TimeZoneInfo.GetUtcOffset(reportBegLoc));
			var reportBegString = string.Empty
				+ $@"{reportBegOffset.ToString(TimeHelper.DefaultTimeFormatUpToMinutes, _params.Culture)}"
				+ $@" ({(reportBegOffset.Offset >= TimeSpan.Zero ? '+' : '-')}{reportBegOffset.Offset:hh\:mm})";
			//////////////////////////////////////////////////////////////////////////
			var reportEndLoc    = TimeHelper.GetLocalTime(_params.DateToInt, _params.TimeZoneInfo);
			var reportEndOffset = new DateTimeOffset(reportEndLoc, _params.TimeZoneInfo.GetUtcOffset(reportEndLoc));
			var reportEndString = string.Empty
				+ $@"{reportEndOffset.ToString(TimeHelper.DefaultTimeFormatUpToMinutes, _params.Culture)}"
				+ $@" ({(reportEndOffset.Offset >= TimeSpan.Zero ? '+' : '-')}{reportEndOffset.Offset:hh\:mm})";
			//////////////////////////////////////////////////////////////////////////
			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
					CreateCellTitleHead(_strings["Report"]),
					CreateCellValueHead(_report.GetReportName(_params.Culture), Cells.Count() - 2)));
			AddRow(
				new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
						CreateCellTitleHead(_strings["dateFrom"]),
						CreateCellValueHead(reportBegString, Cells.Count() - 2)));
			AddRow(
				new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
						CreateCellTitleHead(_strings["dateTo"]),
						CreateCellValueHead(reportEndString, Cells.Count() - 2)));
		}
		protected override void CreateTableHeader(XElement table = null, IEnumerable<CellInfo> cells = null, bool autoFilter = true)
		{
			// Блокируем стандартный вызов, т.к. в нем предполагалась одна таблица
			// Нужно это для удаления лишнего заголовка таблицы
			// Далее везде вызываем base.CreateTableHeader
			base.CreateTableHeader(table, cells, autoFilter);
		}
		/// <summary> Переопределение метода заполнения отчета </summary>
		protected override void FillTable()
		{
			if (null == _data)
				return;
			// Данные таблицы (Сортировка: IntervalBeg, SensorName)
			foreach (var reportRow in _data.OrderBy(x => x.ObjectName))
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
				foreach (var cellInfo in Cells)
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(reportRow)));
				AddRow(row);
			}
		}
		/// <summary> Переопределение свойства описания ячеек отчета </summary>
		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}
		public ShareLinksReportExcelGenerator(ShareLinksReport report, ShareLinksReportParameters parameters, List<ShareLinksReportRow> data)
			: base(report, parameters)
		{
			_report  = report;
			_params  = parameters;
			_data    = data;
			_strings = _report.GetResourceStringContainer(_params.Culture);
			// Заполняем имя листа отчета из ресурса
			_worksheetName = _strings[nameof(ShareLinksReport) + "_SheetName"];
			// Заполняем описание таблицы отчета
			_cells = new List<CellInfo>();
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(ShareLinksReportRow)}_{nameof(ShareLinksReportRow.ObjectName)}",
				GetValue  = x => ((ShareLinksReportRow)x).ObjectName,
				Width     = 030F / 25F, // 030 в Excel
			});
			_cells.Add(new CellInfo
			{
				Key      = $@"{nameof(ShareLinksReportRow)}_{nameof(ShareLinksReportRow.ObjectLink)}",
				GetValue = x => ((ShareLinksReportRow)x).ObjectLink,
				Width    = 100F / 25F, // 100 в Excel
			});
			foreach (var cell in _cells)
				cell.Name = _strings[cell.Key];
		}
	}
}