﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Params;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Отчёт: суточный пробег" </summary>
	[Guid("B4C4E567-3CA3-4252-BC9E-E9949FA9C7A7")]
	public class MoveGroupDayTssReport : TssReportBase
	{
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["MoveGroupDayTssReport"];
		}

		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["MoveGroupDayTssReportDescription"];
		}

		//Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета.
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			var moveReportParameters = new MoveGroupDayReportParameters();
			//Возвращение параметров отчета.
			return moveReportParameters;
		}

		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as MoveGroupDayReportParameters;
			if (parameters == null)
				return false;

			if (parameters.VehicleProp == null ||
				!(parameters.VehicleProp.Tag is int) ||
				(int)parameters.VehicleProp.Tag == 0)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}

		public override int Order
		{
			get { return 0; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.GpsReports.ToString();
		}
		/// <summary>
		/// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
		/// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
		/// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
		/// </summary>
		/// <param name="iReportParameters">Набор параметров для создания отчета</param>
		/// <returns></returns>
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				if (iReportParameters == null)
					return new MoveGroupDayReportParameters();

				//Если набор параметров не был передан в данный отчет, то создать его заново.
				//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
				var reportParameters = (MoveGroupDayReportParameters)iReportParameters;

				//Датасет для хранения полученных из БД данных.
				//Получение из БД данных для заполнения листбокса с номерами ТС.

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);

				return null;
			}
		}

		protected MoveDataset GetReportTypedDataSet(ReportParameters reportParameters)
		{
			//Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
			var clsReportParameters = (MoveGroupDayReportParameters)reportParameters;
			//Создание датасета для хранения информации отчета.
			var dsMoveGroupDay = new MoveDataset();

			try
			{
				//Получение даты начала периода, на который следует получить отчет, из параметров.
				DateTime dtFrom = clsReportParameters.DateFrom;
				//Получение даты окончания периода, на который следует получить отчет, из параметров.
				DateTime dtTo = clsReportParameters.DateTo;

				var vehicleId = (int)clsReportParameters.VehicleProp.Tag;

				MoveDataset.VehicleRow vr = dsMoveGroupDay.Vehicle.NewVehicleRow();
				vr.Garage_Number = mclsInstance.GetVehicleById(reportParameters.OperatorId, vehicleId, reportParameters.Culture).Name;
				vr.Vehicle_ID = vehicleId;
				dsMoveGroupDay.Vehicle.AddVehicleRow(vr);

				var workingIntervals = TimeHelperEx.GetLocalDailyIntervals(
					clsReportParameters.DateTimeInterval.DateFrom.Date, clsReportParameters.DateTimeInterval.DateTo.Date,
					clsReportParameters.WorkingHoursInterval.WorkFrom.Hours, clsReportParameters.WorkingHoursInterval.WorkFrom.Minutes,
					clsReportParameters.WorkingHoursInterval.WorkTo.Hours, clsReportParameters.WorkingHoursInterval.WorkTo.Minutes)
					.Select(i => new LogTimeSpanParam(
									 TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(i.From)),
									 TimeHelper.GetSecondsFromBase(clsReportParameters.ToUtc(i.To))
									 ));

				var positions = mclsInstance.GetMoveGroupDay(vehicleId, workingIntervals);

				positions.DataSetName = "positions";
				var fromColumn = positions.Tables["position"].Columns["from"];
				var userTimeZone = reportParameters.TimeZoneInfo;

				foreach (DataRow row in positions.Tables["position"].Rows)
				{
					var localDateTime = TimeHelper.GetLocalTime((int)row[fromColumn], userTimeZone).Date;

					MoveDataset.positionsRow pr = dsMoveGroupDay.positions.NewpositionsRow();
					pr.Log_Time = localDateTime;
					pr.Log_DateFrom = localDateTime.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
					pr.Dist = (float)((double)row["dist"]);
					pr.Max_Speed = row.IsNull("max_speed") ? 0 : (int)row["max_speed"];
					pr.Avg_Speed = row.IsNull("avg_speed") ? 0 : (int)row["avg_speed"];
					dsMoveGroupDay.positions.AddpositionsRow(pr);
				}


				MoveDataset.IntervalRow ir = dsMoveGroupDay.Interval.NewIntervalRow();
				ir.From = dtFrom.ToShortDateString();
				ir.To = dtTo.ToShortDateString();

				dsMoveGroupDay.Interval.AddIntervalRow(ir);


				//Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
				ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
				//Добавление в датасет данных атрибутов предприятия.
				dsMoveGroupDay.Merge(dsEstablishmentAttributes);

				/*
				if(clsReportParameters.CreateExcelReport)
				{
					CreateExcel(dsMoveGroupDay, clsReportParameters);
					return null;
				}
				*/

				return dsMoveGroupDay;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError("{0}", ex);

				return null;
			}
		}

		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			var dataSet = GetReportTypedDataSet(reportParameters);
			return GetReportHtml(dataSet, (MoveGroupDayReportParameters)reportParameters);
		}

		private string GetReportHtml(DataSet dataSet, MoveGroupDayReportParameters reportParameters)
		{
			var stringBuilder = new StringBuilder();
			var @string = GetResourceStringContainer(reportParameters.Culture);

			using (var stringWriter = new StringWriter(stringBuilder))
			using (var xw = new XmlTextWriter(stringWriter))
			{
				xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
				xw.WriteAttributeString("class", "reportTable withBorders");

				xw.WriteStartElement("thead");

				xw.WriteStartElement("tr");
				xw.WriteAttributeString("class", "header44");
				xw.WriteAttributeString("style", "width:100%;margin-top:10px;");

				xw.WriteElementString("td", @string["DateCaption"]);
				xw.WriteElementString("td", @string["RunCaption"]);
				xw.WriteElementString("td", @string["MaxSpeedCaption"]);
				xw.WriteElementString("td", @string["AvgSpeedCaption"]);
				xw.WriteElementString("td", @string["ShowOnMapCaption"]);

				//tr
				xw.WriteEndElement();
				//thead
				xw.WriteEndElement();

				var positions = dataSet.Tables["positions"];

				xw.WriteStartElement("tbody");
				var totalRun = 0;
				foreach (DataRow dr in positions.Rows)
				{
					totalRun += Convert.ToInt32(dr["Dist"]);

					xw.WriteStartElement("tr");

					var increaseActualDate = reportParameters.WorkingHoursInterval.WorkFrom >=
											 reportParameters.WorkingHoursInterval.WorkTo
												 ? 1 : 0;

					var actualFromDate = ((DateTime)dr["Log_Time"]);

					xw.WriteElementString("td", actualFromDate.ToString("dd.MM.yyyy"));
					xw.WriteElementString("td", dr["Dist"].ToString());
					xw.WriteElementString("td", dr["Max_Speed"].ToString());
					xw.WriteElementString("td", dr["Avg_Speed"].ToString());

					// Ссылка на путь
					var path =
						string.Format(
							"https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=5"
							, reportParameters.ApplicationPath
							, reportParameters.VehicleId
							, actualFromDate.Add(reportParameters.WorkingHoursInterval.WorkFrom)
								.ToString("dd.MM.yyyy+HH:mm:ss")
							, ((DateTime)dr["Log_Time"]).Date.AddDays(increaseActualDate)
								.Add(reportParameters.WorkingHoursInterval.WorkTo)
								.ToString("dd.MM.yyyy+HH:mm:ss"));

					xw.WriteStartElement("td");
					xw.WriteStartElement("a");
					xw.WriteAttributeString("href", path.Replace(' ', '+'));
					xw.WriteString(@string["ShowOnMap"]);
					xw.WriteEndElement();
					xw.WriteEndElement();

					//tr
					xw.WriteEndElement();
				}
				//tbody
				xw.WriteEndElement();
				//table
				xw.WriteEndElement();

				xw.WriteElementString("br", string.Empty);
				xw.WriteString(@string["TotalRun"] + " " + totalRun.ToString(CultureInfo.InvariantCulture));
			}
			return stringBuilder.ToString();
		}

		/// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			ReportClassWrapper rptResultReport = null;
			MoveDataset dataSet = null;
			var processed = false;
			try
			{
				dataSet = GetReportTypedDataSet(iReportParameters);

				//Создание экземпляра отчета.
				rptResultReport = new MoveGroupDayReport();

				//Локализация надписей отчета
				if (iReportParameters != null)
					TranslateReport<MoveGroupDayTssReport>(rptResultReport, iReportParameters.Culture);

				//Подключение отчета к данным, собранным в DataSet'е.
				rptResultReport.SetDataSource(dataSet);
				//Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
				//Функция возвращает сформированный экземпляр отчета.
				processed = true;
				return rptResultReport;
			}
			finally
			{
				if (!processed && rptResultReport != null)
					rptResultReport.Dispose();
				if (dataSet != null)
					dataSet.Dispose();
			}
		}
	}
}