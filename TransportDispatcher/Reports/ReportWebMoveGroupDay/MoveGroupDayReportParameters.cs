﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Config;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary>
	/// Параметры для сгруппированного отчета по пробегу (MoveGroupDayReport).
	/// </summary>
	[Serializable]
	public sealed class MoveGroupDayReportParameters : ReportParameters, IMonitoreeObjectContainer
	{
		private DateTime dateFrom = DateTime.Now.Date.AddDays(-7);
		private DateTime dateTo = DateTime.Now.Date;

		private TagListBoxItem vehicleProp = new TagListBoxItem("", null);

		// Набор данных для Editor-ов
		[NonSerialized]
		private DataTable _dsParamsForEditor;
		public DataTable DsParamsForEditor
		{
			get { return _dsParamsForEditor; }
			set { _dsParamsForEditor = value; }
		}

		public MoveGroupDayReportParameters()
		{
			this.ExcelReportDir = Path.GetPathRoot(Environment.CurrentDirectory);
			string folder = GlobalsConfig.AppSettings["ExcelReportFolder"];

			if (!string.IsNullOrEmpty(folder))
			{
				this.ExcelReportDir = folder;
			}
		}

		/// <summary>
		/// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
		/// </summary>
		[DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
		[Browsable(true)]
		public override DateTime DateFrom
		{
			get { return dateFrom.Date; }
			set { dateFrom = value.Date; }
		}

		/// <summary>
		/// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
		/// </summary>
		[DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
		[Browsable(true)]
		public override DateTime DateTo
		{
			get { return dateTo.Date; }
			set { dateTo = value.Date; }
		}

		[Browsable(false)]
		public TagListBoxItem VehicleProp
		{
			get
			{
				return vehicleProp;
			}
			set
			{
				vehicleProp = value;
			}
		}

		private bool createExcelReport = false;
		[DisplayName("CreateExcelReport", 3, true), Category("ReportParams"), Description("CreateExcelReportDesc")]
		public bool CreateExcelReport
		{
			get { return createExcelReport; }
			set { createExcelReport = value; }
		}

		private string excelReportDir = "";
		[Browsable(false)]
		public string ExcelReportDir
		{
			get { return excelReportDir; }
			set { excelReportDir = value; }
		}

		public IdType MonitoreeObjectIdType
		{
			get { return IdType.Vehicle; }
		}

		public int MonitoreeObjectId
		{
			get { return (int)vehicleProp.Tag; }
		}

		[DisplayName("VehicleId"), Type(typeof(int)), ControlType(ReportParametersUtils.VehiclePicker, "VehiclePicker"), Order(1), Options(SystemRight.PathAccess)]
		public int VehicleId
		{
			get { return MonitoreeObjectId; }
			set
			{
				vehicleProp = new TagListBoxItem
				{
					Tag = value
				};
			}
		}

		private WorkingHoursInterval _workingHoursInterval;

		[DisplayName("WorkingHoursInterval"), Type(typeof(WorkingHoursInterval))]
		[ControlType(ReportParametersUtils.WorkingHoursInterval, "WorkingHoursInterval"), Order(256)]
		public WorkingHoursInterval WorkingHoursInterval
		{
			get { return _workingHoursInterval; }
			set { _workingHoursInterval = value; }
		}
	}
}
