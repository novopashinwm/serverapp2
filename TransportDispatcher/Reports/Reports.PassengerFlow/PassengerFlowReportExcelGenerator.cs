﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.PassengerFlow
{
	internal class PassengerFlowReportExcelGenerator : BaseReportExcelGenerator
	{
		private readonly PassengerFlowReport           _report;
		private readonly PassengerFlowReportParameters _params;
		private readonly List<PassengerFlowReportRow>  _data;
		private readonly ResourceStringContainer       _strings;
		private readonly string                        _worksheetName;
		private readonly List<CellInfo>                _cells;

		/// <summary> Переопределение листа по умолчанию для отчета </summary>
		protected override string WorksheetName => _worksheetName ?? base.WorksheetName;

		/// <summary> Переопределение метода формирования заголовка отчета (над таблицей) </summary>
		protected override void FillHeader()
		{
			//////////////////////////////////////////////////////////////////////////
			var reportBegLoc    = TimeHelper.GetLocalTime(_params.DateFromInt, _params.TimeZoneInfo);
			var reportBegOffset = new DateTimeOffset(reportBegLoc, _params.TimeZoneInfo.GetUtcOffset(reportBegLoc));
			var reportBegString = string.Empty
				+ $@"{reportBegOffset.ToString(TimeHelper.DefaultTimeFormatUpToDays, _params.Culture)}"
				+ $@" ({(reportBegOffset.Offset >= TimeSpan.Zero ? '+' : '-')}{reportBegOffset.Offset:hh\:mm})";
			//////////////////////////////////////////////////////////////////////////
			var reportEndLoc    = TimeHelper.GetLocalTime(_params.DateToInt, _params.TimeZoneInfo);
			var reportEndOffset = new DateTimeOffset(reportEndLoc, _params.TimeZoneInfo.GetUtcOffset(reportEndLoc));
			var reportEndString = string.Empty
				+ $@"{reportEndOffset.ToString(TimeHelper.DefaultTimeFormatUpToDays, _params.Culture)}"
				+ $@" ({(reportEndOffset.Offset >= TimeSpan.Zero ? '+' : '-')}{reportEndOffset.Offset:hh\:mm})";
			//////////////////////////////////////////////////////////////////////////
			AddRow(new XElement("Row",
				new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
					CreateCellTitleHead(_strings["Report"]),
					CreateCellValueHead(_report.GetReportName(_params.Culture), Cells.Count() - 2)));
			AddRow(
				new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
						CreateCellTitleHead(_strings["dateFrom"]),
						CreateCellValueHead(reportBegString, Cells.Count() - 2)));
			AddRow(
				new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
						CreateCellTitleHead(_strings["dateTo"]),
						CreateCellValueHead(reportEndString, Cells.Count() - 2)));
			AddRow(
				new XElement("Row",
					new XAttribute(SpreadsheetNS + "AutoFitHeight", 0),
						CreateCellTitleHead(_strings["AggregationInterval"]),
						CreateCellValueHead(TimeHelperEx.GetIntervalPreset(_params.Interval, _params.Culture), Cells.Count() - 2)));
		}
		protected override void CreateTableHeader(XElement table = null, IEnumerable<CellInfo> cells = null, bool autoFilter = true)
		{
			// Блокируем стандартный вызов, т.к. в нем предполагалась одна таблица
			// Нужно это для удаления лишнего заголовка таблицы
			// Далее везде вызываем base.CreateTableHeader
		}
		/// <summary> Переопределение метода заполнения отчета </summary>
		protected override void FillTable()
		{
			if (null == _data)
				return;
			var reportGroups = _data
				?.GroupBy(g => new { g.ObjectName, g.Timestamp })
				?.OrderBy(g => g.Key.ObjectName)
				?.ThenBy(g => g.Key.Timestamp);
			foreach (var reportGroup in reportGroups)
			{
				// Расчет итогов в группе (Сортировка: SensorName, IntervalBeg)
				var reportGroupItemPrev = default(PassengerFlowReportRow);
				foreach (var reportGroupItem in reportGroup.OrderBy(x => x.SensorName).ThenBy(x => x.IntervalBeg))
				{
					reportGroupItem.TotalData = reportGroupItem.ValueData;
					if (default(PassengerFlowReportRow) != reportGroupItemPrev && reportGroupItemPrev.SensorName == reportGroupItem.SensorName)
						reportGroupItem.TotalData += reportGroupItemPrev.TotalData;
					reportGroupItemPrev = reportGroupItem;
				}
				var reportGroupName = string.Empty
					+ _strings[$@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.ObjectName)}"]
					+ $@": {reportGroup.Key.ObjectName}"
					+ $@", "
					+ _strings[$@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.Timestamp)}"]
					+ $@": {reportGroup.Key.Timestamp.ToString(TimeHelper.DefaultTimeFormatUpToDays, _params.Culture)}"
					+ $@" ({(reportGroup.Key.Timestamp.Offset >= TimeSpan.Zero ? '+' : '-')}{reportGroup.Key.Timestamp.Offset:hh\:mm})";
				// Пустая строка
				AddRow(
					new XElement("Row",
						new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"),
						CreateCellTitleHead(string.Empty, Cells.Count() - 1)));
				// Заголовок группы
				AddRow(
					new XElement("Row",
						new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"),
						CreateCellTitleHead(reportGroupName, Cells.Count() - 1)));
				// Заголовок таблицы
				base.CreateTableHeader(autoFilter: false);
				// Данные таблицы (Сортировка: IntervalBeg, SensorName)
				foreach (var reportGroupItem in reportGroup.OrderBy(x => x.IntervalBeg).ThenBy(x => x.SensorName))
				{
					var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
					foreach (var cellInfo in Cells)
						row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(reportGroupItem)));
					AddRow(row);
				}
				// Итоги таблицы
				foreach (var sensorNameGroup in reportGroup.GroupBy(g => g.SensorName).OrderBy(g => g.Key))
				{
					AddRow(
						new XElement("Row",
							new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"),
							CreateCellTitleHead(string.Empty),
							CreateCellTitleHead(sensorNameGroup.Key),
							CreateCellTitleHead(sensorNameGroup.Sum(x => (int)x.ValueData).ToString(_params.Culture))));
				}
			}
		}
		/// <summary> Переопределение свойства описания ячеек отчета </summary>
		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}

		public PassengerFlowReportExcelGenerator(PassengerFlowReport report, PassengerFlowReportParameters parameters,List<PassengerFlowReportRow> data)
			: base(report, parameters)
		{
			_report  = report;
			_params  = parameters;
			_data    = data;
			_strings = _report.GetResourceStringContainer(_params.Culture);
			// Заполняем имя листа отчета из ресурса
			_worksheetName = _strings[typeof(PassengerFlowReport).Name + "_SheetName"];
			// Заполняем описание таблицы отчета
			_cells = new List<CellInfo>();
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(PassengerFlowReportRow)}_Interval",
				//ExcelType = CellInfo.ExcelTypes.TimeSpan,
				GetValue  = x => string.Empty
				   + $@"{((PassengerFlowReportRow)x).IntervalBeg:hh\:mm}"
				   + $@" - "
				   + $@"{((PassengerFlowReportRow)x).IntervalEnd:hh\:mm}",
				Width     = 0.8f,
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.SensorName)}",
				GetValue  = x => ((PassengerFlowReportRow)x).SensorName,
				Width     = 0.8f,
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.ValueData)}",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((int)((PassengerFlowReportRow)x).ValueData).ToString(parameters.Culture),
				Width     = 0.4f,
			});
			_cells.Add(new CellInfo
			{
				Key       = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.TotalData)}",
				ExcelType = CellInfo.ExcelTypes.Number,
				GetValue  = x => ((int)((PassengerFlowReportRow)x).TotalData).ToString(parameters.Culture),
				Width     = 0.4f,
			});
			_cells.Add(new CellInfo
			{
				Key      = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.Address)}",
				GetValue = x => ((PassengerFlowReportRow)x).Address,
				Width    = 4.0f,
			});
			foreach (var cell in _cells)
				cell.Name = _strings[cell.Key];
		}
	}
}