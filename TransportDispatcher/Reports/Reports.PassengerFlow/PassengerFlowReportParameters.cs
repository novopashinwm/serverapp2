﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.PassengerFlow
{
	[Serializable]
	public class PassengerFlowReportParameters : ReportParameters
	{
		/// <summary> Группа объектов наблюдения по которой строить отчет </summary>
		//[DisplayName("VehicleGroupId")] // берем из самого свойства, тут нужно только для замены
		//[Type(typeof(int))] // берем из самого свойства, тут нужно только для замены
		[ControlType(ReportParametersUtils.VehicleGroupPicker)]
		[Order(1)]
		public int VehicleGroupId { get; set; }
		/// <summary> Список объектов наблюдения, чтобы строить отчёт групповой отчёт не создавая группу </summary>
		//[DisplayName("VehicleIds")] // берем из самого свойства, тут нужно только для замены
		//[Type(typeof(List<int>))] // берем из самого свойства, тут нужно только для замены
		[ControlType(ReportParametersUtils.VehiclesPicker)]
		[Order(2)]
		// Фильтровать по виду объекта, типа автобус и наличию права просмотра истории
		[Options("VehicleKind:" + nameof(VehicleKind.Bus), SystemRight.PathAccess)]
		public List<int> VehicleIds { get; set; } // Тип менять нельзя, т.к. VehiclesPicker считает, что там List<int>
		/// <summary> Интервал </summary>
		//[DisplayName("Interval")] // берем из самого свойства, тут нужно только для замены
		//[Type(typeof(int))] // берем из самого свойства, тут нужно только для замены
		[ControlType(ReportParametersUtils.IntervalPicker)]
		[Order(3)]
		public int Interval { get; set; } = 60;
		/// <summary> Интервал построения отчета </summary>
		//[DisplayName("DateTimeInterval")] // берем из самого свойства, тут нужно только для замены
		//[Type(typeof(DateTimeInterval))] // берем из самого свойства, тут нужно только для замены
		[ControlType(ReportParametersUtils.DateTimeFromToPicker, "ReportPeriod")]
		[Order(255)]
		[Options("Accuracy:1")]
		public override DateTimeInterval DateTimeInterval { get; set; }
	}
}