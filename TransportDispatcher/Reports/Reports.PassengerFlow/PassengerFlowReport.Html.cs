﻿using System;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.PassengerFlow
{
	public sealed partial class PassengerFlowReport
	{
		public override object GetReportAsObj(ReportParameters reportParameters)
		{
			return GetReportHtml(reportParameters as PassengerFlowReportParameters);
		}
		private string GetReportHtml(PassengerFlowReportParameters reportParameters)
		{
			// Проверка параметров
			if (null == reportParameters)
				return default(string);
			// Получение ресурсов
			var strings = GetResourceStringContainer(reportParameters.Culture);
			// Получение данных
			var reportGroups = GetRows(reportParameters)
				?.GroupBy(g => new { g.ObjectName, g.Timestamp })
				?.OrderBy(g => g.Key.ObjectName)
				?.ThenBy(g => g.Key.Timestamp);
			var html = (XNamespace)"http://www.w3.org/1999/xhtml";
			// Если нет данных, пишем об отсутствии и выходим
			if (!reportGroups.Any())
				return (new XElement(html + "span", strings["NoData"])).ToString();
			// Формирование отчета (с заголовком с именем отчета)
			var xdiv = new XElement(html + "div");
			var cols = new []
			{
				new
				{
					Key      = $@"{nameof(PassengerFlowReportRow)}_Interval",
					GetValue = new Func<PassengerFlowReportRow, string>(x =>
						$@"{x.IntervalBeg.ToString(@"hh\:mm")} - {x.IntervalEnd.ToString(@"hh\:mm")}"),
				},
				new
				{
					Key      = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.SensorName)}",
					GetValue = new Func<PassengerFlowReportRow, string>(x => x.SensorName),
				},
				new
				{
					Key      = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.ValueData)}",
					GetValue = new Func<PassengerFlowReportRow, string>(x =>
						((int)x.ValueData).ToString(reportParameters.Culture)),
				},
				new
				{
					Key      = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.TotalData)}",
					GetValue = new Func<PassengerFlowReportRow, string>(x =>
						((int)x.TotalData).ToString(reportParameters.Culture)),
				},
				new
				{
					Key      = $@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.Address)}",
					GetValue = new Func<PassengerFlowReportRow, string>(x => x.Address),
				},
			};
			foreach (var reportGroup in reportGroups)
			{
				// Расчет итогов в группе (Сортировка: SensorName, IntervalBeg)
				var reportGroupItemPrev = default(PassengerFlowReportRow);
				foreach (var reportGroupItem in reportGroup.OrderBy(x => x.SensorName).ThenBy(x => x.IntervalBeg))
				{
					reportGroupItem.TotalData = reportGroupItem.ValueData;
					if (default(PassengerFlowReportRow) != reportGroupItemPrev && reportGroupItemPrev.SensorName == reportGroupItem.SensorName)
						reportGroupItem.TotalData += reportGroupItemPrev.TotalData;
					reportGroupItemPrev = reportGroupItem;
				}
				// Пустая строка
				xdiv.Add(new XElement(html + "br"));
				// Имя группы
				var reportGroupName = string.Empty
					+ strings[$@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.ObjectName)}"]
					+ $@": {reportGroup.Key.ObjectName}"
					+ $@", "
					+ strings[$@"{nameof(PassengerFlowReportRow)}_{nameof(PassengerFlowReportRow.Timestamp)}"]
					+ $@": {reportGroup.Key.Timestamp.ToString(TimeHelper.DefaultTimeFormatUpToDays, reportParameters.Culture)}"
					+ $@" ({(reportGroup.Key.Timestamp.Offset >= TimeSpan.Zero ? '+' : '-')}{reportGroup.Key.Timestamp.Offset:hh\:mm})";
				// Заголовок таблицы группы
				var thead = new XElement(html + "thead",
					// Строка с именем группы в заголовке таблицы
					new XElement("tr",
						new XElement("td",
							new XAttribute("colspan", cols.Length),
							reportGroupName
						)
					),
					// Строка названий колонок в заголовке таблицы
					new XElement("tr",
						cols.Select(c => new XElement("td", strings[c.Key]))
					)
				);
				// Данные таблицы (Сортировка: IntervalBeg, SensorName)
				var tbody = new XElement(html + "tbody");
				foreach (var reportGroupItem in reportGroup.OrderBy(x => x.IntervalBeg).ThenBy(x => x.SensorName))
				{
					tbody.Add(
						new XElement("tr",
							cols.Select(c => new XElement("td", c.GetValue(reportGroupItem)))
						)
					);
				}
				// Итоги таблицы
				var tfoot = new XElement(html + "tfoot");
				foreach (var sensorNameGroup in reportGroup.GroupBy(g => g.SensorName).OrderBy(g => g.Key))
				{
					tfoot.Add(
						new XElement("tr",
							new XElement("td", new XAttribute("border", "0")),
							new XElement("td", sensorNameGroup.Key),
							new XElement("td", new XAttribute("border", "0")),
							new XElement("td", sensorNameGroup.Sum(x => (int)x.ValueData).ToString(reportParameters.Culture)),
							new XElement("td", new XAttribute("border", "0"))
						)
					);
				}
				xdiv.Add(new XElement(html + "table",
					new XAttribute("class", "reportTable withBorders"),
					thead, tfoot, tbody));
			}
			return xdiv.ToString();
		}
	}
}