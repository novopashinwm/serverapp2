﻿using FORIS.TSS.TransportDispatcher.Reports;

namespace Compass.Ufin.Reports.PassengerFlow
{
	class PassengerFlowReportFormatter : BaseReportExcelFormatter
	{
		private readonly BaseExcelGenerator _excelGenerator;

		public PassengerFlowReportFormatter(BaseExcelGenerator excelGenerator)
		{
			_excelGenerator = excelGenerator;
		}

		protected override string GetExcelXmlString()
		{
			return _excelGenerator.Create();
		}
	}
}