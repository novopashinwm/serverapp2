﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.TransportDispatcher.Reports;
using Interfaces.Geo;

namespace Compass.Ufin.Reports.PassengerFlow
{
	/// <summary> Отчет о пассажиропотоке </summary>
	[Guid("D2FF4940-2AC0-4030-9CF1-B54EF2A59A7D")]
	public sealed partial class PassengerFlowReport : TssReportBase
	{
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			// Преобразование типа параметров к параметрам отчета
			var parameters = (PassengerFlowReportParameters)iReportParameters;
			// Получение данных
			var data = GetRows(parameters).ToList();
			// Формирование отчета
			return new PassengerFlowReportFormatter(new PassengerFlowReportExcelGenerator(this, parameters, data));
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new PassengerFlowReportParameters { OperatorId = settings.OperatorId };
		}
		public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
		{
			try
			{
				// Проверка был ли передан набор параметров для отчета.
				if (null == iReportParameters)
					//Если набор параметров не был передан в данный отчет, то создать его заново.
					return new PassengerFlowReportParameters();

				// Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчёта.
				var reportParameters = (PassengerFlowReportParameters)iReportParameters;

				// Если есть набор идентификаторов, то очищаем идентификатор группы и считаем, что есть произвольный набор машин
				if (0 < (reportParameters.VehicleIds?.Count ?? 0))
					reportParameters.VehicleGroupId = -1;

				// Возвращаются скорректированные параметры.
				return reportParameters;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
				Trace.TraceError(GetType() + ": {0}", ex);
				return null;
			}
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var reportParameters = iReportParameters as PassengerFlowReportParameters;
			if (reportParameters == null)
				return false;

			if (0 >= reportParameters.VehicleGroupId && 0 == (reportParameters.VehicleIds?.Count ?? 0))
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		/// <summary> Текстовое описание отчета </summary>
		/// <param name="culture">Культура, для которой следует вернуть описание</param>
		/// <returns></returns>
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(PassengerFlowReport) + "_Name"];
		}
		/// <summary> Возвращает описание, что именно должен вернуть отчёт, в том числе важные замечания </summary>
		/// <param name="culture">Культура, для которой следует вернуть описание.</param>
		/// <returns></returns>
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)[nameof(PassengerFlowReport) + "_Desc"];
		}
		/// <summary> Возвращает группу, в которую входит данный отчёт </summary>
		public override string GetGroupName()
		{
			return ReportMenuGroupType.SensorsReports.ToString();
		}
		/// <summary> Порядок отчета </summary>
		public override int Order => 1;
		/// <summary> Поддерживаемые форматы отчета </summary>
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Html;
			yield return ReportTypeEnum.Excel;
			//yield return ReportTypeEnum.Acrobat;
		}
		private IEnumerable<PassengerFlowReportRow> GetRows(PassengerFlowReportParameters parameters)
		{
			var reportRows = new List<PassengerFlowReportRow>();
			if (null == parameters)
				return reportRows;
			//////////////////////////////
			var neededVehicleRights = new[] { SystemRight.PathAccess };
			//////////////////////////////
			var reportVehicles = default(List<Vehicle>);
			if (0 != (parameters.VehicleIds?.Count ?? 0))
			{
				reportVehicles = mclsInstance.GetVehiclesWithPositions(
					parameters.OperatorId, null, new GetVehiclesArgs
					{
						VehicleIDs = parameters.VehicleIds.ToArray(),
						Culture    = parameters.Culture
					})
					.Where(v => neededVehicleRights.All(r => v?.rights?.Contains(r) ?? false))
					.ToList();
			}
			else
			{
				reportVehicles = mclsInstance.GetVehiclesByVehicleGroup(
					parameters.VehicleGroupId,
					parameters.OperatorId,
					parameters.Culture,
					neededVehicleRights);
			}
			// Формируем параметры интервала отчета (полученные из параметров, указанных пользователем)
			var logTimeBeg = parameters.DateFromInt;
			var logTimeEnd = parameters.DateToInt;
			// Указываем необходимые производные датчики
			var neededVehicleSensors = new[]
			{
				SensorLegend.Door1Entered, SensorLegend.Door1Leaved, SensorLegend.Door1Offline, SensorLegend.Door1Sabotage,
				SensorLegend.Door2Entered, SensorLegend.Door2Leaved, SensorLegend.Door2Offline, SensorLegend.Door2Sabotage,
				SensorLegend.Door3Entered, SensorLegend.Door3Leaved, SensorLegend.Door3Offline, SensorLegend.Door3Sabotage,
				SensorLegend.Door4Entered, SensorLegend.Door4Leaved, SensorLegend.Door4Offline, SensorLegend.Door4Sabotage,
				SensorLegend.Door5Entered, SensorLegend.Door5Leaved, SensorLegend.Door5Offline, SensorLegend.Door5Sabotage,
				SensorLegend.Door6Entered, SensorLegend.Door6Leaved, SensorLegend.Door6Offline, SensorLegend.Door6Sabotage,
				SensorLegend.Door7Entered, SensorLegend.Door7Leaved, SensorLegend.Door7Offline, SensorLegend.Door7Sabotage,
			};
			// Фильтруем список объектов, чтобы в списке был хотя бы один из необходимых датчиков
			reportVehicles = reportVehicles
				.Where(r => r.Sensors.Any(s => neededVehicleSensors.Contains(s.Number)))
				.ToList();
			// Осуществляем перебор оставшихся объектов наблюдения и запрашиваем историю
			foreach (var reportVehicle in reportVehicles)
			{
				var vehLogTimeBeg = logTimeBeg;
				var vehLogTimeEnd = logTimeEnd;
				// TODO: Получить текущий тариф, в mclsInstance.GetVehiclesWithPositions и  mclsInstance.GetVehiclesByVehicleGroup тарифа нет
				//var vehCurItemPrd = reportVehicle.CurrentTariff as CommonActionResponse<ProductItem>;
				//if (null != vehCurItemPrd?.Object)
				//{
				//	// Если удалось определить тариф, то корректируем возможный интервал времени
				//	var utcNow = DateTime.UtcNow;
				//	var maxUtc = utcNow;
				//	//var minUtc = maxUtc - TimeSpan.FromDays(vehCurItemPrd.Object.Addons.Fi);
				//}
				var fullLog = mclsInstance.GetFullLog(
					parameters.OperatorId,
					reportVehicle.id,
					vehLogTimeBeg,
					vehLogTimeEnd,
					new FullLogOptions { CutOutstandingData = true, },
					parameters.Culture);
				var reportVehicleData = fullLog
					?.Sensors
					?.Where(s => neededVehicleSensors.Contains(s.Key))
					?.SelectMany(s => s.Value.LogRecords
						// Получаем записи только со значениями
						?.Where(r => r.Value > 0)
						// Сортируем по времени
						?.OrderBy(r => r.LogTime)
						// Выбираем необходимое
						?.Select(r =>
						{
							var dateLoc = TimeHelper.GetLocalTime(r.LogTime, parameters.TimeZoneInfo);
							var dateVal = new DateTimeOffset(dateLoc, parameters.TimeZoneInfo.GetUtcOffset(dateLoc));
							var spanBeg = TimeSpan.FromSeconds((((int)(dateVal - dateVal.Date).TotalSeconds) / parameters.Interval + 0) * parameters.Interval);
							var spanEnd = TimeSpan.FromSeconds((((int)(dateVal - dateVal.Date).TotalSeconds) / parameters.Interval + 1) * parameters.Interval);
							// Коррекция на конец суток
							spanEnd = TimeSpan.FromDays(1) > spanEnd ? spanEnd : TimeSpan.Zero;
							return new
							{
								Name    = s.Value.Name,
								Date    = dateVal.Date,
								SpanBeg = spanBeg,
								SpanEnd = spanEnd,
								Value   = r.Value
							};
						}))
					?.GroupBy(g => new { g.Date, g.Name, g.SpanBeg, g.SpanEnd })
					?.Select(g =>
					{
						var geoPoint = fullLog.GetCurrentOrPreviousGeoRecord((g.Key.Date.ToUniversalTime() + g.Key.SpanEnd).ToLogTime());
						return new PassengerFlowReportRow
						{
							ObjectId    = reportVehicle.id,
							ObjectName  = reportVehicle.Name,
							SensorName  = g.Key.Name,
							Timestamp   = new DateTimeOffset(g.Key.Date, parameters.TimeZoneInfo.GetUtcOffset(g.Key.Date)),
							IntervalBeg = g.Key.SpanBeg,
							IntervalEnd = g.Key.SpanEnd,
							ValueData   = g.Sum(v => v.Value),
							Lat         = geoPoint?.Lat,
							Lng         = geoPoint?.Lng,
						};
					});
				reportRows.AddRange(reportVehicleData);
			}
			// Заполняем адреса для оставшихся записей
			FillAddresses(parameters, reportRows);
			// Возвращаем результат
			return reportRows;
		}
		private void FillAddresses(PassengerFlowReportParameters parameters, IEnumerable<PassengerFlowReportRow> reportRows)
		{
			var requests = reportRows
				?.Where(r => r.Lat.HasValue && r.Lng.HasValue)
				?.GroupBy(r => new { r.Lat, r.Lng })
				?.Select((itm, idx) => new { idx, itm });
			if (0 == (requests?.Count() ?? 0))
				return;

			var addresses = mclsInstance.GetAddresses(
				requests
					.Select(r => new AddressRequest(
						r.idx,
						lat: (double)r.itm.Key.Lat.Value,
						lng: (double)r.itm.Key.Lng.Value))
					.ToArray(),
				parameters.Culture,
				parameters.MapGuid)
				?.ToDictionary(k => k.Id);
			if (0 == (addresses?.Count() ?? 0))
				return;

			foreach (var request in requests)
			{
				var addressResponse = default(AddressResponse);
				if (addresses.TryGetValue(request.idx, out addressResponse))
				{
					foreach (var row in request.itm)
						row.Address = addressResponse.Address;
				}
			}
		}
	}
}