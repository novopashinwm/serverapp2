﻿using System;

namespace Compass.Ufin.Reports.PassengerFlow
{
	internal class PassengerFlowReportRow
	{
		public int            ObjectId;
		public string         ObjectName;
		public string         SensorName;
		public DateTimeOffset Timestamp;
		public TimeSpan       IntervalBeg;
		public TimeSpan       IntervalEnd;
		public decimal        ValueData;
		public decimal        TotalData = 0.0m;
		public decimal?       Lat;
		public decimal?       Lng;
		public string         Address;
	}
}