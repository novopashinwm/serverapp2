﻿using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TransportDispatcher.Reports;
using GuidAttribute = System.Runtime.InteropServices.GuidAttribute;

namespace RU.NVG.NIKA.Reports.CommercialServices
{
	/// <summary> Отчёт о коммерческих услугах </summary>
	[Guid("6690093C-C323-450C-B23B-91EFCF404409")]
	public class CommercialServicesReport : TssReportBase
	{
		internal enum ServiceUnit
		{
			SMS,
			LBS,
			VehicleDays
		}

		internal static readonly Dictionary<string, ServiceUnit> ServiceUnits = 
			new Dictionary<string, ServiceUnit>
			{
				{"SMS",            ServiceUnit.SMS},
				{"LBS",            ServiceUnit.LBS},
				{"MlpCharging",    ServiceUnit.LBS},
				{"LBSTracker",     ServiceUnit.VehicleDays},
				{"GNSS",           ServiceUnit.VehicleDays},
				{"NikaTracker",    ServiceUnit.VehicleDays},
				{"NikaAdmin",      ServiceUnit.SMS},
				{"FRNIKAMPXWhere", ServiceUnit.LBS},
				{"FRNIKASMS",      ServiceUnit.SMS}
			};
		public override IReportClass Create(ReportParameters iReportParameters)
		{
			var parameters = (ReportCommercialServicesParameters)iReportParameters;

			var dateFrom = parameters.ToUtc(parameters.DateFrom.Date);
			var dateTo = parameters.ToUtc(parameters.DateTo.Date.AddDays(1).AddSeconds(-1));

			var records = mclsInstance.ReportCommercialServices(parameters.OperatorId, dateFrom, dateTo);

			foreach (var record in records)
			{
				if (record.RenderedLastTime != null)
					record.RenderedLastTime = parameters.FromUtc(record.RenderedLastTime.Value);
			}

			return new CommercialServicesFormatter(this, parameters, records);
		}
		public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
		{
			return new ReportCommercialServicesParameters();
		}
		public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
		{
			var parameters = iReportParameters as ReportCommercialServicesParameters;
			if (parameters == null)
				return false;

			return base.ReportParametersInstanceCheck(iReportParameters);
		}
		public override string GetReportName(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["CommercialServices"];
		}
		public override string GetReportDesc(CultureInfo culture)
		{
			return GetResourceStringContainer(culture)["CommercialServicesDescription"];
		}
		public override int Order
		{
			get { return 11; }
		}
		public override string GetGroupName()
		{
			return ReportMenuGroupType.CommonReports.ToString();
		}
		public override IEnumerable<ReportTypeEnum> GetSupportedFormats()
		{
			yield return ReportTypeEnum.Excel;
		}
	}
}