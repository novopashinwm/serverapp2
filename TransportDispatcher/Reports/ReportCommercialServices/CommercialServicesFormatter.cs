﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.CommercialServices
{
	internal class CommercialServicesFormatter : BaseReportExcelFormatter
	{
		private readonly CommercialServicesReport           _report;
		private readonly ReportCommercialServicesParameters _parameters;
		private readonly List<CommercialServiceRecord>      _records;

		public CommercialServicesFormatter(
			CommercialServicesReport report, 
			ReportCommercialServicesParameters parameters, 
			List<CommercialServiceRecord> records)
		{
			_report = report;
			_parameters = parameters;
			_records = records;
		}
		protected override string GetExcelXmlString()
		{
			var generator = new CommercialServicesReportExcelFormatter(_report, _parameters, _records);

			return generator.Create();
		}
	}
}