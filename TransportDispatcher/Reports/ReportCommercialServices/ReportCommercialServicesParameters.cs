﻿using System;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.CommercialServices
{
	[Serializable]
	public class ReportCommercialServicesParameters : ReportParameters
	{
		private bool _isDetailedToCustomer;

		[DisplayName("IsDetailedToCustomer"), Type(typeof(bool)), ControlType(ReportParametersUtils.Boolean, "IsDetailedToCustomer")]
		public Boolean IsDetailedToCustomer
		{
			get { return _isDetailedToCustomer; }
			set { _isDetailedToCustomer = value; }
		}
	}
}