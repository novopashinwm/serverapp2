﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;

namespace RU.NVG.NIKA.Reports.CommercialServices
{
	internal class CommercialServicesReportExcelFormatter : BaseReportExcelGenerator
	{
		private readonly List<CommercialServiceRecord> _records;
		private readonly List<CellInfo>                _cells;
		private readonly List<CellInfo>                _detailCells;
		private readonly ResourceStringContainer       _strings;

		private readonly ReportCommercialServicesParameters _parameters;

		public CommercialServicesReportExcelFormatter(
			CommercialServicesReport report,
			ReportCommercialServicesParameters parameters,
			List<CommercialServiceRecord> records) : base(report, parameters)
		{
			_parameters = parameters;
			_records    = records;
			_strings    = report.GetResourceStringContainer(parameters.Culture);

			_cells = new List<CellInfo>
				{
					new CellInfo
						{
							Key = "Country",
							GetValue = x => ((CommercialServiceRecord) x).Country
						},
					new CellInfo
						{
							Key = "MacroRegion",
							GetValue = x => ((CommercialServiceRecord) x).MacroRegion
						},
					new CellInfo
						{
							Key = "CustomerName",
							GetValue = x => ((CommercialServiceRecord) x).CustomerName
						},
					new CellInfo
						{
							Key = "ServiceName",
							GetValue = x => ((CommercialServiceRecord) x).ServiceName
						},
					new CellInfo
						{
							Key = "ServiceCount",
							ExcelType = CellInfo.ExcelTypes.Number,
							GetValue = x => ((CommercialServiceRecord) x).ServiceCount.ToString(parameters.Culture)
						},
					new CellInfo
						{
							Key = "RenderedCount",
							ExcelType = CellInfo.ExcelTypes.Number,
							GetValue = x => ((CommercialServiceRecord) x).RenderedCount.ToString(parameters.Culture)
						},
					new CellInfo
						{
							Key = "RenderedLastTime",
							ExcelType = CellInfo.ExcelTypes.DateTime,
							GetValue = x =>
								{
									var renderedLastTime = ((CommercialServiceRecord) x).RenderedLastTime;
									if (renderedLastTime == null)
										return string.Empty;
									return XmlConvert.ToString(renderedLastTime.Value,
															   XmlDateTimeSerializationMode.Unspecified);
								}
						},
					new CellInfo
						{
							Key = "ServiceUnit",
							GetValue =
								delegate(object x)
									{
										var record = ((CommercialServiceRecord) x);
										var serviceTypeCategory = record.ServiceTypeCategory;
										CommercialServicesReport.ServiceUnit serviceUnit;
										if (!CommercialServicesReport.ServiceUnits.TryGetValue(serviceTypeCategory,
																							   out serviceUnit))
											return string.Empty;

										return _strings[serviceUnit.ToString()];
									}
						}
				};

			_detailCells = new List<CellInfo>
				{
					new CellInfo
						{
							Key = "Country",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).Country
						},
					new CellInfo
						{
							Key = "MacroRegion",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).MacroRegion
						},
					new CellInfo
						{
							Key = "CustomerName",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).CustomerName
						},
					new CellInfo
						{
							Key = "ContractNumber",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).ContractNumber
						},
					new CellInfo
						{
							Key = "TerminalDeviceNumber",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).TerminalDeviceNumber
						},
					new CellInfo
						{
							Key = "ServiceName",
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).ServiceName
						},
					new CellInfo
						{
							Key = "StartDate",
							GetValue =
								delegate(object x)
									{
										var date = ((CommercialServiceRecord.ServiceRecord) x).StartDate;
										return date != null ? date.Value.ToShortDateString() : string.Empty;
									}
						},
					new CellInfo
						{
							Key = "EndDate",
							GetValue =
								delegate(object x)
									{
										var date = ((CommercialServiceRecord.ServiceRecord)x).EndDate;
										return date != null ? date.Value.ToShortDateString() : string.Empty;
									}
						},
					new CellInfo
						{
							Key = "RenderedCount",
							ExcelType = CellInfo.ExcelTypes.Number,
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).RenderedCount.ToString(parameters.Culture)
						},
					new CellInfo
						{
							Key = "ServiceUnit",
							GetValue =
								delegate(object x)
									{
										var record = ((CommercialServiceRecord.ServiceRecord) x);
										var serviceTypeCategory = record.ServiceTypeCategory;
										CommercialServicesReport.ServiceUnit serviceUnit;
										if (!CommercialServicesReport.ServiceUnits.TryGetValue(serviceTypeCategory,
																							   out serviceUnit))
											return string.Empty;

										return _strings[serviceUnit.ToString()];
									}
						},
					new CellInfo
						{
							Key = "RenderedLastTime",
							ExcelType = CellInfo.ExcelTypes.DateTime,
							GetValue = x =>
								{
									var renderedLastTime = ((CommercialServiceRecord.ServiceRecord) x).RenderedLastTime;
									if (renderedLastTime == null)
										return string.Empty;
									return XmlConvert.ToString(renderedLastTime.Value,
															   XmlDateTimeSerializationMode.Unspecified);
								}
						},
					new CellInfo
						{
							Key = "LbsPositionCount",
							ExcelType = CellInfo.ExcelTypes.Number,
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).LbsPositionCount.ToString(parameters.Culture)
						},
					new CellInfo
						{
							Key = "LbsPositionRequested",
							ExcelType = CellInfo.ExcelTypes.Number,
							GetValue = x => ((CommercialServiceRecord.ServiceRecord) x).LbsPositionRequested.ToString(parameters.Culture)
						}
				};

			foreach (var cell in _cells.Union(_detailCells))
			{
				cell.Name = _strings[cell.Key];
			}
		}

		protected override void FillHeader()
		{
			base.FillHeader();
			FillDateIntervalHeader();
		}

		protected override void FillTable()
		{
			foreach (var record in _records)
			{
				var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
				foreach (var cellInfo in Cells)
				{
					row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
				}
				AddRow(row);
			}

			if (_parameters.IsDetailedToCustomer)
			{
				var table = CreateTableInNewWorksheet(_detailCells, _strings["IsDetailedToCustomer"]);

				CreateTableHeader(table, _detailCells, true);
				foreach (var record in _records.SelectMany(r => r.Services))
				{
					var row = new XElement("Row", new XAttribute(SpreadsheetNS + "AutoFitHeight", "0"));
					foreach (var cellInfo in _detailCells)
					{
						row.Add(CreateDataCell(cellInfo, cellInfo.GetValue(record)));
					}
					AddRow(row, table);
				}
			}
		}

		protected override IEnumerable<CellInfo> Cells
		{
			get { return _cells; }
		}
	}
}