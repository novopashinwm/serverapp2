﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG.abonent
{
	public partial class Abonent : BasePage
	{
		#region AbonentPage
		[PageAction("default")]
		public void ShowPage()
		{
			RenderAbonentPage("abonent", "abonentPage");
		}
		private void RenderAbonentPage(string templateFileName, string templateName)
		{
			try
			{
				TemplateFile = templateFileName;
				AddProperty("template", templateName);
				if (OperatorId != null)
					AddProperty("AdminId", OperatorId.Value.ToString(CultureInfo.InvariantCulture));

				if (IWebPS.DepartmentExtIDIsAllowed)
				{
					AddProperty("DepartmentExtIDIsRequired", "true");
				}

				string menuItem = GetParamValue("menuItem");
				AddProperty("menuItem", menuItem);

				ResponseSerializationBeg();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
			}
			ResponseSerializationEnd();
		}
		[PageAction("createNewCustomer", RequestType = DataType.Json, ResponseType = DataType.Json)]
		public object CreateNewCustomer(string customerName, string customerAdminLogin, string departmentExtID = null)
		{
			int departmentId;
			var createDepartmentResult = IWebPS.CreateDepartment(customerName, customerAdminLogin, departmentExtID, out departmentId);

			if (createDepartmentResult == CreateDepartmentResult.Success)
				return departmentId;

			return createDepartmentResult;
		}
		[JsonPageAction("ResetAdministratorPassword", HttpMethod.Unspecified)]
		public LoginInfo ResetAdministratorPassword(string login)
		{
			LoginInfo result = default(LoginInfo);
			try
			{
				result = IWebPS.ResetAdministratorPassword(login);
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}
		#endregion AbonentPage
	}
}