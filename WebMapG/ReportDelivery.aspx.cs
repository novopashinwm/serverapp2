﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using Interfaces.Web.Enums;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class ReportsDeliveryDefault : BasePage
	{
		[PageAction("default")]
		public void ShowPage()
		{
			TemplateFile = "reportDelivery";
			AddProperty("template", "deliveryList");

			dontCacheMe();
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}

		[PageAction("dataObjects", Method = HttpMethod.Get, RequestType = DataType.Empty, ResponseType = DataType.Json)]
		public Dictionary<string, object> GetDataObjects()
		{
			var subscriptions = IWebPS.GetSubscriptions();

			return new Dictionary<string, object>
			{
				{ "subscriptions", subscriptions         },
				{ "vehicles",      GetVehiclesDTO(false) },
				{ "vehicleGroups", GetVehicleGroupsDTO() },
			};
		}
		[JsonPageAction("setEnabled", HttpMethod.Unspecified)]
		public OkFailedResult SetEnabled(int id, bool value)
		{
			var result = OkFailedResult.Failed;
			try
			{
				IWebPS.SetScheduleEnabled(id, value);
				result = OkFailedResult.OK;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}
	}
}