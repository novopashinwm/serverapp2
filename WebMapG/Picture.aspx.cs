﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Resources;
using System.Web;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using Interfaces.Web.Enums;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Picture : BasePage
	{
		[PageAction("default")]
		public void Get()
		{
			TemplateFile = "picture";
			AddProperty("template", "picturePage");

			var vehicleID = int.Parse(GetParamValue("objectId"));
			var logTime = int.Parse(GetParamValue("logTime"));

			AddProperty("vehicleId", vehicleID.ToString(CultureInfo.InvariantCulture));
			AddProperty("logtime", logTime.ToString(CultureInfo.InvariantCulture));

			var vehicles = IWebPS.GetVehicles(new GetVehiclesArgs{VehicleIDs =  new[] {vehicleID}});
			if (vehicles != null && vehicles.Count > 0)
			{
				var vehicle     = vehicles[0];
				var vehicleInfo = $"{vehicle.vehicleKind} {vehicle.regNum} {vehicle.Name}";

				AddProperty("vehicleInfo", vehicleInfo);
			}
			
			var pictureids = IWebPS.GetNearestPictures(vehicleID, logTime);

			AddProperty("pictureids",
				pictureids != null && pictureids.Count != 0
					? JsonHelper.SerializeObjectToJson(pictureids)
					: "null");

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}

		[JsonPageAction("getthumbnailsids", HttpMethod.Unspecified)]
		public List<int> GetThumbnailsIds(int objectId, int logTime)
		{
			var result = default(List<int>);
			try
			{
				result = IWebPS.GetNearestPictures(objectId, logTime);
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}

		[PageAction("angleofview")]
		public void AngleOfViewPictire()
		{
			var angle = float.Parse(GetParamValue("angle"), NumberHelper.FormatInfo);

			var resourceManager = new ResourceManager("Resources.Images", System.Reflection.Assembly.Load("App_GlobalResources"));
			object obj = resourceManager.GetObject("fieldofview", CurrentCulture);
			
			var b = ((Bitmap)(obj));

			if (b == null)
				throw new ApplicationException("fieldofview resource not loaded");

			using (var returnBitmap = new Bitmap(b.Width, b.Height))
			using (var g = Graphics.FromImage(returnBitmap))
			{
				g.TranslateTransform((float) b.Width/2, (float) b.Height/2);
				g.RotateTransform(angle);
				g.TranslateTransform(-(float) b.Width/2, -(float) b.Height/2);

				g.DrawImage(b, new Point(0, 0));

				Response.Clear();
				SetJpegContentType();
				CacheResponse();
				returnBitmap.Save(response.OutputStream, ImageFormat.Png);
				Response.End();
			}
		}

		private void CacheResponse()
		{
			Response.Cache.SetExpires(DateTime.Now.AddYears(1));
			Response.Cache.SetCacheability(HttpCacheability.Public);
			Response.Cache.SetMaxAge(DateTime.Now.AddYears(1) - DateTime.Now);
			Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
		}

		[PageAction("getpicture")]
		public void GetPicture()
		{
			var vehicleID = int.Parse(GetParamValue("objectId"));
			int logTime;
			if (!int.TryParse(GetParamValue("logTime"), out logTime))
				BadRequest("Unable to parse logTime");

			var bytes = GetPictureContent(vehicleID, logTime);

			Response.Clear();
			SetJpegContentType();
			if (bytes != null)
			{
				CacheResponse();
				Response.OutputStream.Write(bytes, 0, bytes.Length);
			}
			else
			{
				Response.OutputStream.Write(new byte[0], 0, 0);
			}
			Response.End();
		}

		[PageAction("getthumbnail")]
		public void GetThumbnail()
		{
			var vehicleID = int.Parse(GetParamValue("objectId"));
			var logTime = int.Parse(GetParamValue("logTime"));
			int parsedWidth, parsedHeight;
			var width = int.TryParse(GetParamValue("width"), out parsedWidth) ? parsedWidth : (int?) null;
			var height = int.TryParse(GetParamValue("height"), out parsedHeight) ? parsedHeight : (int?) null;
			var thumbContent = IWebPS.GetThumbnailBytes(vehicleID, logTime, width, height);

			Response.Clear();
			SetJpegContentType();
			if (thumbContent != null)
			{
				CacheResponse();
				Response.OutputStream.Write(thumbContent, 0, thumbContent.Length);
			}

			Response.End();
		}

		[PageAction("image-base64", Method = HttpMethod.Post, ResponseType = DataType.Json)]
		public Dictionary<string, object> ImageToBase64()
		{
			var base64Image = string.Empty;
			var result = ImageToBase64Result.Success;
			if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
			{
				if (isImageOk(Request.Files[0].InputStream))
				{
					Request.Files[0].InputStream.Position = 0;
					var imgBuffer = new byte[Request.Files[0].ContentLength];
					Request.Files[0].InputStream.Read(imgBuffer, 0, Request.Files[0].ContentLength);
					base64Image = Convert.ToBase64String(imgBuffer);
				}
				else
				{
					result = ImageToBase64Result.ImageIsTooBig;
				}
			}
			else
			{
				result = ImageToBase64Result.ServerError;
			}

			return new Dictionary<string, object>
			{
				{ "result",  result                         },
				{ "image",   base64Image                    },
				{ "message", RM.GetString(result.ToString())}
			};
		}

		private byte[] GetPictureContent(int vehicleId, int logtime)
		{
			var picture = IWebPS.GetPicture(vehicleId, logtime);

			if (picture == null)
				return null;

			return picture.Bytes;
		}
	}
}