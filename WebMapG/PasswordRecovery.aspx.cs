﻿using System;
using System.Security;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class PasswordRecovery : BaseLoginPage
	{
		private const string RecoverPasswordActionKey = "recover-password";

		[PageAction("default")]
		public void ShowPasswordRecoveryForm()
		{
			TemplateFile = "register";
			AddProperty("template", "password-recovery-form");
			AddProperty("title", GetMessageData("PasswordRecovery"));
			AddProperty("email", GetParamValue("email"));

			RenderPromo();
		}
		[PageAction("send-confirmation-email")]
		public void SendConfirmationEmail()
		{
			var email = GetParamValue("email");

			if (string.IsNullOrEmpty(email))
			{
				AddMessage(
					new Message
					{
						Severity = Severity.Error,
						MessageBody = GetMessageData("emailNotSet")
					});
				ShowPasswordRecoveryForm();
				return;
			}

			TemplateFile = "register";
			AddProperty("title", GetMessageData("PasswordRecovery"));
			AddProperty("email", GetParamValue("email"));
			
			try
			{
				WebLoginProvider.SendConfirmationEmail(
					email,
					GetMessageData("PasswordRecoveryConfirmationEmailSubject"),
					string.Format(GetMessageData("PasswordRecoveryConfirmationEmailBody"),
						UrlPrefix + 
						(Request.ApplicationPath != null && Request.ApplicationPath.EndsWith("/")
							? Request.ApplicationPath.Substring(0, Request.ApplicationPath.Length - 1)
							: Request.ApplicationPath) + 
						"/PasswordRecovery.aspx?a=" + 
						RecoverPasswordActionKey + 
						"&confirmation-key={0}"),
					true
					);
				AddProperty("template", "email-with-password-recovery-has-been-sent");
			}
			catch(SecurityException ex)
			{
				AddProperty("message", ex.Message);
				AddProperty("template", "email-has-not-been-sent");
			}

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[PageAction(RecoverPasswordActionKey)]
		public void RecoverPassword()
		{
			var confirmationKey = GetParamValue("confirmation-key");

			LoginByConfirmationKey(confirmationKey);

			//LoginByConfirmationKey never returns in case with no error
			ShowPasswordRecoveryForm();
		}
		[JsonPageAction("ajax-send-confirmation-email", HttpMethod.Unspecified)]
		public bool AjaxSendConfirmationEmail()
		{
			var email = GetParamValue("email");
			try
			{
				if (string.IsNullOrEmpty(email))
					return false;
				WebLoginProvider.SendConfirmationEmail(
					email,
					GetMessageData("PasswordRecoveryConfirmationEmailSubject"),
					string.Format(GetMessageData("PasswordRecoveryConfirmationEmailBody"),
						UrlPrefix +
						(Request.ApplicationPath != null && Request.ApplicationPath.EndsWith("/")
							? Request.ApplicationPath.Substring(0, Request.ApplicationPath.Length - 1)
							: Request.ApplicationPath) +
						"/PasswordRecovery.aspx?a=" + RecoverPasswordActionKey + "&confirmation-key={0}"),
					true);
				return true;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return false;
		}
	}
}