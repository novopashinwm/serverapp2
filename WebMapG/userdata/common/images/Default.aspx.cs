﻿using System;

namespace FORIS.TSS.UI.WebMapG.userdata.common.images
{
	public class UserdataCommonImagesDefault : DirectoryListing
	{
		[PageAction("default")]
		public void Default()
		{
			SetXmlContentType();
			TemplateFile = "gallery";
			AddProperty("template", "imageList");
			ResponseSerializationBeg();
			Listing();
			ResponseSerializationEnd();
		}

		protected override string[] GetFiles(String path)
		{
			String[] files = System.IO.Directory.GetFiles(path, "*.png");
			return files;
		}
	}
}