﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Config;
using RU.NVG.UI.WebMapG.Model.Common;
using RU.NVG.UI.WebMapG.Model.Vehicles;

namespace FORIS.TSS.UI.WebMapG.vehicles
{
	public class Attributes : BaseRestfulPage
	{
		[PageAction(Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<VehicleAttribute> Get(int vehicleId, int version = 0, VehicleAttributeName[] names = null)
		{
			if (vehicleId == default(int))
			{
				BadRequest("vehicleId is not specified");
				return null;
			}

			if (version == 0)
				return IWebPS.GetVehicleAttributes(
					vehicleId,
					VehicleAttributeName.Name,
					VehicleAttributeName.DeviceId,
					VehicleAttributeName.TrackerType,
					VehicleAttributeName.Phone,
					VehicleAttributeName.Password);

			return IWebPS.GetVehicleAttributes(vehicleId, names);
		}

		[Obsolete]
		[PageAction(Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Enum Set(int vehicleId, VehicleAttributeName name, string value)
		{
			var result = IWebPS.SetVehicleAttribute(vehicleId, name, value);
			if (result == null)
				BadRequest("Attribute \"" + name + "\" is not supported");
			return result;
		}

		[PageAction("set-attribute", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SetVehicleAttributeResult SetV2(int vehicleId, VehicleAttributeName name, string value)
		{
			var result = IWebPS.SetVehicleAttribute(vehicleId, name, value);
			if (result == null)
				BadRequest("Attribute \"" + name + "\" is not supported");
			return new SetVehicleAttributeResult
			{
				Result = result,
				ResultText = string.Format(CurrentCulture, GetErrorMessageFormat(result), value)
			};
		}

		[PageAction("get-values", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<VehicleAttributeValue> Get(int vehicleId, VehicleAttributeName attributeName)
		{
			
			var attributeValues = IWebPS.GetVehicleAttributeValues(vehicleId, attributeName, null);
			switch (attributeName)
			{
				case VehicleAttributeName.Icon:
					if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["vehicleIconPath"]))
					{
						foreach (var attributeValue in attributeValues)
						{
							var relativePhysicalPath = attributeValue.Icon.Replace(HostingEnvironment.ApplicationPhysicalPath, "~/");
							attributeValue.Icon = VirtualPathUtility.ToAbsolute(relativePhysicalPath);
						}
					}
					break;
				case VehicleAttributeName.TrackerType:
					foreach (var attributeValue in attributeValues)
					{
						attributeValue.IconBase64 = GetBase64StringForUrl(attributeValue.Icon);
					}
					break;
			}

			return attributeValues;
		}

		[PageAction("vehicle-profile-history", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public GetListResponse<VehicleProfileHistory> GetProfileHistory(int vehicleId, string name)
		{
			var result = new GetListResponse<VehicleProfileHistory>
			{
				Items = IWebPS.GetVehicleProfileHistory(vehicleId, name)
			};
			return result;
		}

		[Obsolete]
		[PageAction("vehicle-icons", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public string[] GetIcons()
		{
			string webSiteUrl = GlobalsConfig.AppSettings["webSiteUrl"];
			if (string.IsNullOrEmpty(webSiteUrl)) 
				webSiteUrl = "http://tss.sitels.ru";
			var relativePath = Request.ApplicationPath + "/img/userdata/";
			var path = Server.MapPath(relativePath);
			var result = Directory
				.GetFiles(path, "*.png")
				.Select(f => webSiteUrl + relativePath + Path.GetFileName(f)).ToArray();
			return result;
		}

		[PageAction("set-values", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Dictionary<VehicleAttributeName, Enum> SetVehicleAttributes(
			int vehicleId,
			Dictionary<VehicleAttributeName, string> values)
		{
			return IWebPS.SetVehicleAttributes(vehicleId, values);
		}
	}
}