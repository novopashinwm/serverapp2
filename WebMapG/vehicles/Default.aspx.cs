﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Billing.Core;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.UI.WebMapG.Old_App_Code;
using Interfaces.Web.Enums;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG.vehicles
{
	public partial class Default : BaseRestfulPage
	{
		[PageAction("default", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<Vehicle> Get(
			int[] vehicleIds,
			string[] objectsFilters,
			string[] onlineFilters,
			bool includeIgnored = false,
			bool includeAddresses = false,
			bool includeThumbnails = false)
		{
			var arguments = new GetVehiclesArgs
			{
				IncludeAddresses  = includeAddresses,
				IncludeThumbnails = includeThumbnails,
				VehicleIDs        = vehicleIds,
				IncludeLastData   = true,
				IncludeAttributes = true,
				MapGuid           = MapGuid,
				OnlineFilter      = onlineFilters  != null ? ParseVehicleOnlineFilter(onlineFilters)   : (VehicleOnlineFilter?)null,
				ObjectsFilter     = objectsFilters != null ? ParseVehicleObjectsFilter(objectsFilters) : VehicleObjectsFilter.Active,
				OnlineTime        = GetOnlineTime()
			};

			if (includeIgnored)
			{
				arguments.ObjectsFilter = (arguments.ObjectsFilter ?? VehicleObjectsFilter.None) | VehicleObjectsFilter.Ignored;
			}
			var vehicles = new List<Vehicle>();
			using (Stopwatcher.GetElapsedLogger($"GetVehicles".CallTraceMessage(), null, ContextHelper.StopwatchLoggingThreshold))
				vehicles = IWebPS.GetVehicles(arguments);
			if ((vehicles?.Count ?? 0) > 0)
			{
				// Перебираем полученные объекты и обновляем свойства
				foreach (var vehicle in vehicles)
				{
					try
					{
					}
					catch (Exception ex)
					{
						default(string)
							.WithException(ex)
							.CallTraceError();
					}
				}
				////////////////////////////////////////////////
				// Замена имени для общей ссылки
				if (null != SessionShareLink?.VehicleName)
				{
					var shareLinkVehicle = vehicles.FirstOrDefault(v => v.id == SessionShareLink.VehicleId);
					if (null != shareLinkVehicle)
						shareLinkVehicle.Name = SessionShareLink.VehicleName;
				}
				////////////////////////////////////////////////
				// Заполняем биллинговую информацию
				using (Stopwatcher.GetElapsedLogger($"GetBillings".CallTraceMessage(), null, ContextHelper.StopwatchLoggingThreshold))
				{
					// Выбираем тип клиента
					var accountType = IWebPS?.Department?.Type == DepartmentType.Corporate
						? AccountType.Corporate
						: AccountType.Private;
					////////////////////////////////////////////////
					// Получить список продуктов
					var productList = InvokeBilling<IPaymentProvider, ProductList>((factory) =>
					{
						using var logger = Stopwatcher.GetElapsedLogger($"GetObjectCurrentProducts".CallTraceMessage(), null, ContextHelper.StopwatchLoggingThreshold);
						var confirmedEmail = default(string); // Пока тут не нужен
						return factory
							.CreateChannel()
							.GetObjectCurrentProducts(
								vehicles?.Select(v => v.id)?.ToArray(),
								confirmedEmail,
								IWebPS.CultureInfo.TwoLetterISOLanguageName);
					});
					////////////////////////////////////////////////
					// Получить продукт по умолчанию для корпоративных клиентов
					var productCorpDflt = InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
					{
						using var logger = Stopwatcher.GetElapsedLogger($"GetObjectDefaultProductByAccountType".CallTraceMessage(), null, ContextHelper.StopwatchLoggingThreshold);
						return factory
							.CreateChannel()
							.GetObjectDefaultProductByAccountType(AccountType.Corporate, IWebPS.CultureInfo.TwoLetterISOLanguageName);
					});
					////////////////////////////////////////////////
					// Получить продукт по умолчанию для частных клиентов
					var productPrivDflt = InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
					{
						using var logger = Stopwatcher.GetElapsedLogger($"GetObjectDefaultProductByAccountType".CallTraceMessage(), null, ContextHelper.StopwatchLoggingThreshold);
						return factory
							.CreateChannel()
							.GetObjectDefaultProductByAccountType(AccountType.Private, IWebPS.CultureInfo.TwoLetterISOLanguageName);
					});
					////////////////////////////////////////////////
					// Перебираем полученные объекты и заполняем тарифы
					foreach (var vehicle in vehicles)
					{
						// Выбираем тариф по умолчанию
						var productDflt = vehicle.departmentId.HasValue
							? productCorpDflt
							: productPrivDflt;
						// Тариф для конкретного объекта (сначала по умолчанию)
						var vehicleProduct = new CommonActionResponse<ProductItem>
						{
							Result = productList.Result,
							ResultText = productList.ResultText,
							Object = productDflt.Object
								?.WitoutIcons()
								?.WitoutPrices(),
						};
						// Если список тарифов есть, то пытаемся его найти
						var productListObject = productList.Object as ProductList;
						if (null != productListObject)
						{
							vehicleProduct.Object = productListObject
								?.FirstOrDefault(o => o
									?.Params
									?.FirstOrDefault(p => p.Code == ProductParamItem.MonitoringObjectIdenKey)
									?.Value == vehicle.id.ToString())
								?.WitoutIcons()
								?.WitoutPrices() ?? vehicleProduct.Object;
							// Если услуга остановлена или не определена, то ставим по умолчанию
							if ((vehicleProduct.Object?.Status?.Id ?? (int)ProductStatus.Unspecified) != (int)ProductStatus.Active)
								vehicleProduct.Object.Addons = productDflt.Object.Addons;
						}
						// Если это не владелец, то убираем идентификацию тарифа и оставляем только дополнения
						if (vehicle?.Owner?.id != IWebPS.OperatorId)
						{
							vehicleProduct.Object = vehicleProduct
								?.Object
								?.WitoutNames();
						}
						vehicle.CurrentTariff = vehicleProduct;
					}
				}
			}
			////////////////////////////////////////////////
			// Сохраняем в кэш полученные и преобразованные результаты для конкретного соединения
			Global.GlobalCache.Set($"Vehicles_{SessionId}", vehicles, DateTimeOffset.MaxValue);
			////////////////////////////////////////////////
			return vehicles;
		}
		private static VehicleOnlineFilter ParseVehicleOnlineFilter(string[] input)
		{
			var result = VehicleOnlineFilter.None;
			foreach (var s in input)
			{
				VehicleOnlineFilter e;
				if (!Enum.TryParse(s, true, out e))
					throw new ArgumentOutOfRangeException("input", s, @"Unable to parse enum value");
				result |= e;
			}
			return result;
		}
		private static VehicleObjectsFilter ParseVehicleObjectsFilter(string[] input)
		{
			var result = VehicleObjectsFilter.None;
			foreach (var s in input)
			{
				VehicleObjectsFilter e;
				if (!Enum.TryParse(s, true, out e))
					throw new ArgumentOutOfRangeException("input", s, @"Unable to parse enum value");
				result |= e;
			}
			return result;
		}
		[PageAction("default", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void Post(Vehicle vehicle)
		{
			if (vehicle.lat != null && vehicle.lng != null)
			{
				IWebPS.SetVehiclePosition(vehicle.id, vehicle.lat.Value, vehicle.lng.Value);
			}
		}
		[PageAction("delete", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool Delete(int id)
		{
			return IWebPS.DeleteVehicle(id) == DeleteVehicleResult.Success;
		}
		[PageAction("remove", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public RU.NVG.UI.WebMapG.Model.Vehicles.DeleteVehicleResult DeleteVehicle(int id)
		{
			var result = IWebPS.DeleteVehicle(id);
			return new RU.NVG.UI.WebMapG.Model.Vehicles.DeleteVehicleResult
			{
				Result = result,
				ResultText = GetMessageData(result.ToString(), CurrentCulture)
			};
		}
		[PageAction("move", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void MoveVehicle(int id, int targetDepartmentId)
		{
			IWebPS.MoveVehicle(id, targetDepartmentId);
		}
	}
}