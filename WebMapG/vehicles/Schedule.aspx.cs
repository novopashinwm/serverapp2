﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG.vehicles
{
	public class Schedule : BasePage
	{
		[PageAction(Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<TrackingSchedule> GetList(int vehicleId)
		{
			return IWebPS.GetVehicleSchedules(vehicleId);
		}
		[PageAction(Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public TrackingSchedule Set(int vehicleId, TrackingSchedule value)
		{
			return IWebPS.SetVehicleSchedule(vehicleId, value);
		}
		[PageAction("delete", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void Delete(int id)
		{
			IWebPS.DeleteVehicleSchedule(id);
		}
	}
}