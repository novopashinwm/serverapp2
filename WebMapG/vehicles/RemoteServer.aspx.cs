﻿using FORIS.TSS.BusinessLogic.Enums;
using RemoteTerminalServerDTO = FORIS.TSS.BusinessLogic.Terminal.Model.RemoteTerminalServer;

namespace FORIS.TSS.UI.WebMapG.vehicles
{
	public class RemoteServer : BasePage
	{
		[JsonPageAction]
		public RemoteTerminalServerDTO[] Get(int vehicleId)
		{
			return IWebPS.GetRemoteTerminalServerManager(vehicleId).GetRemoteTerminalServers();
		}
		[JsonPageAction(Method = HttpMethod.Post)]
		public void Post(int vehicleId, RemoteTerminalServerDTO value)
		{
			IWebPS.GetRemoteTerminalServerManager(vehicleId).SetRemoteTerminalServers(value);
		}
	}
}