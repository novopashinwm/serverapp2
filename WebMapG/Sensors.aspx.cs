﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.UI.WebMapG.Old_App_Code;
using Interfaces.Web.Enums;
using Jayrock.Json;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Sensors : BasePage
	{
		[PageAction("default")]
		public void ListSensors()
		{
			TemplateFile = "sensors";
			AddProperty("template", "sensorsPage");

			var vehicleId = int.Parse(GetParamValue("id"));
			var vehicle = GetVehicleById(vehicleId);

			if(!IWebPS.IsAllowedSensorsEdit(vehicleId))
				throw new SecurityException("Sensor management is not allowed");

			AddProperty("objectName", vehicle.Name);

			AddProperty("vehicleJSON",          JsonHelper.SerializeObjectToJson(GetVehicleById(vehicleId)));
			AddProperty("sensorsJSON",          JsonHelper.SerializeObjectToJson(IWebPS.GetSensorMappings(vehicleId)));
			AddProperty("controllerInputsJSON", JsonHelper.SerializeObjectToJson(IWebPS.GetControllerInputs(vehicleId)));
			AddProperty("legendsJSON",          JsonHelper.SerializeObjectToJson(IWebPS.GetSensorLegends(vehicleId)));

			ResponseSerializationBeg();

			ResponseSerializationEnd();
		}
		[PageAction("SensorMappings", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SensorLegendMapping[] GetSensorMappings(int vehicleID)
		{
			return IWebPS.GetSensorsMappings(vehicleID);
		}
		[PageAction("SensorMappings", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SensorLegendMapping SaveSensorMappings(int vehicleId, SensorLegendMapping mapping)
		{
			IWebPS.SetSensorLegendMapping(vehicleId, mapping.SensorLegend, mapping.Mappings);
			var mappings = IWebPS.GetSensorLegendMappings(vehicleId, mapping.SensorLegend);
			return new SensorLegendMapping
			{
				SensorLegend = mapping.SensorLegend,
				Mappings = mappings
			};
		}
		[PageAction("ControllerInputs", RequestType = DataType.Json, ResponseType = DataType.Json, Method = HttpMethod.Get)]
		public ControllerInput[] GetControllerInputs(int vehicleID)
		{
			return IWebPS.GetControllerInputs(vehicleID);
		}
		[PageAction("SensorLegends", RequestType = DataType.Json, ResponseType = DataType.Json, Method = HttpMethod.Get)]
		public List<Sensor> GetSensorLegends(int vehicleID)
		{
			return IWebPS.GetSensorLegends(vehicleID);
		}
		[JsonPageAction("delete", HttpMethod.Unspecified)]
		[Obsolete]
		public OkFailedResult Delete()
		{
			var result = OkFailedResult.Failed;
			try
			{
				var idsString = GetParamValue("ids");
				var ids       = Array.ConvertAll(idsString.Split(','), int.Parse);
				foreach (var id in ids)
					IWebPS.DeleteSensorMapping(id);
				result = OkFailedResult.OK;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}
		[PageAction("update")]
		public void Update()
		{
			WrapJsonAction(delegate
			{
				var mapsJson = (JsonArray)JsonHelper.DeserializeObjectFromJson(GetParamValue("maps"));
				var vehicleId = int.Parse(GetParamValue("id"));
				var ids = new List<int?>(mapsJson.Length);
				foreach (JsonObject mapJson in mapsJson)
				{
					var sensorMap = new SensorMap
					{
						SensorLegend = (SensorLegend)Enum.Parse(typeof(SensorLegend), mapJson["sensorLegend"].ToString()),
						InputNumber = ((JsonNumber)mapJson["inputNumber"]).ToInt32(),
						Multiplier = ((JsonNumber)mapJson["multiplier"]).ToDecimal(),
						Constant = ((JsonNumber)mapJson["constant"]).ToDecimal(),
						MinValue = mapJson.Contains("minValue") || mapJson["minValue"] is JsonNull ? ((JsonNumber)mapJson["minValue"]).ToInt64() : (long?)null,
						MaxValue = mapJson.Contains("maxValue") || mapJson["maxValue"] is JsonNull ? ((JsonNumber)mapJson["maxValue"]).ToInt64() : (long?)null,
					};

					if (mapJson.Contains("id") && mapJson["id"] is JsonNumber)
					{
						sensorMap.Id = ((JsonNumber)mapJson["id"]).ToInt32();
						IWebPS.UpdateSensorMapping(sensorMap);
						ids.Add(sensorMap.Id);
					}
					else
					{
						ids.Add(IWebPS.AddSensorMapping(vehicleId, sensorMap));
					}
				}
				return ids.Where(id => id != null).ToArray();
			});
		}
		[JsonPageAction("getSensorMappings", HttpMethod.Unspecified)]
		[Obsolete]
		public SensorMap[] GetSensorMappings(string vehicleId)
		{
			SensorMap[] result = default(SensorMap[]);
			if (!Request.IsAjaxRequest())
				return result;
			try
			{
				result = IWebPS.GetSensorMappings(Convert.ToInt32(vehicleId));
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}
		[JsonPageAction("sensorValueChange", HttpMethod.Unspecified)]
		public OkFailedResult SensorValueChange()
		{
			var result = OkFailedResult.Failed;
			try
			{
				var vehicleId    = int.Parse(GetParamValue("vehicleId"));
				var sensorLegend = (SensorLegend)Enum.Parse(typeof(SensorLegend), GetParamValue("sensor"));
				var logTime      = int.Parse(GetParamValue("logTime"));
				var valueString  = GetParamValue("value");

				decimal? valueNumber;

				if (string.IsNullOrWhiteSpace(valueString))
					valueNumber = null;
				else
					valueNumber = decimal.Parse(valueString);

				IWebPS.SensorValueChange(vehicleId, sensorLegend, logTime, valueNumber);

				result = OkFailedResult.OK;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return result;
		}
	}
}