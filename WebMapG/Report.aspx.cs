﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.TransportDispatcher.Reports;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class ReportsIndex : BasePage
	{
		protected override void Process()
		{
			var rights = OperatorSystemRights;
			if (rights == null || !rights.Contains(SystemRight.ReportsAccess))
				throw new SecurityException("No ReportAccess right");

			base.Process();
		}

		private List<ReportMenuGroup> _reportMenu;
		private ReportMenuItem        _selectedMenuItem;

		private void GenerateMenu()
		{
			_reportMenu = IWebPS.GetReportMenu();
			if (_reportMenu == null)
				return;

			var selectedMenuItemId = FindoutSelectedReportId();

			_selectedMenuItem = _reportMenu
				.Where(m => m.IsAvailable)
				.SelectMany(menuGroup => menuGroup.ReportMenuItems, (x, y) => y)
				.FirstOrDefault(reportMenuItem => reportMenuItem.Id == selectedMenuItemId && reportMenuItem.IsAvailable);

			Response.Cookies.Add(new HttpCookie("currentReportId", selectedMenuItemId.ToString(CultureInfo.InvariantCulture)));

			if (_selectedMenuItem != null)
				_selectedMenuItem.Selected = true;
			else
				_reportMenu
					.Where(m => m.IsAvailable)
					.SelectMany(x => x.ReportMenuItems, (x, y) => y)
					.First(r => r.IsAvailable)
					.Selected = true;

			foreach (var menuGroup in _reportMenu)
			{
				if (menuGroup.ReportMenuItems == null)
					continue;
				var selectedReportIndex = menuGroup.ReportMenuItems.FindIndex(x => x.Selected);
				if (selectedReportIndex < 0)
					continue;
				var selectedReportGuid = Guid.Parse(menuGroup.ReportMenuItems[selectedReportIndex].Guid);
				var selectedReport     = IWebPS.GetReportMenuItem(selectedReportGuid);
				selectedReport.Selected = true;
				menuGroup.ReportMenuItems[selectedReportIndex] = selectedReport;
				_selectedMenuItem = selectedReport;
				break;
			}
		}
		private int FindoutSelectedReportId()
		{
			int selectedMenuItemId;
			if (int.TryParse(Request.QueryString["currentreportid"], out selectedMenuItemId))
				return selectedMenuItemId;

			if (int.TryParse(GetParamValue("reportid"), out selectedMenuItemId))
				return selectedMenuItemId;

			var currentReportIdCookie = Request.Cookies["currentReportId"];
			if (currentReportIdCookie != null && int.TryParse(currentReportIdCookie.Value, out selectedMenuItemId) && selectedMenuItemId != 0)
				return selectedMenuItemId;

			return _reportMenu.SelectMany(x => x.ReportMenuItems, (x, y) => y).First().Id;
		}
		[PageAction("default")]
		public void DefaultProcess()
		{
			TemplateFile = "report";
			AddProperty("template", "reports");
			GenerateMenu();

			dontCacheMe();
			SetHtmlContentType();

			ResponseSerializationBeg();

			SerializeReportMenu();
			SerializeCommonData();

			ResponseSerializationEnd();
		}
		[JsonPageAction("CreateObjectsCurrentStateReport", Method = HttpMethod.Unspecified)]
		public string CreateObjectsCurrentStateReport(
			ReportTypeEnum reportTypeEnum,
			string[]       fieldsToView,
			string[]       filterVehicleByState,
			int[]          excludeVehicleGroups,
			bool?          showGroups        = null,
			int?           selectedGroupId   = null,
			string         selectedGroupName = null)
		{
			var reportId = Guid.Parse("1C22ACF2-A71A-4E46-A189-237F6B8DF07A");
			var @params = new PARAMS(true);

			// Преобразование и коррекция параметров
			// 1. Формат отчета
			var supportedFormats = new[] { ReportTypeEnum.Acrobat, ReportTypeEnum.Excel };
			if (!supportedFormats.Contains(reportTypeEnum))
				reportTypeEnum = ReportTypeEnum.Acrobat;
			// 2. Список полей
			fieldsToView = fieldsToView ?? new string[0];
			// 3. Фильтр по статусу объекта
			if (0 == (filterVehicleByState ?? new string[0]).Length)
				filterVehicleByState = new[] { string.Empty };
			var filterVehicleByStateValue = filterVehicleByState
				.Select(v => { var value = default(ShowVehiclesMode); Enum.TryParse(v, true, out value); return value; })
				.Aggregate((a, b) => a | b);
			// 4. Список идентификаторов исключенных групп
			excludeVehicleGroups = excludeVehicleGroups ?? new int[0];

			// Подготовка для передачи в отчет
			@params.Add("FieldsToView",         fieldsToView);
			@params.Add("FilterVehicleByState", filterVehicleByStateValue);
			@params.Add("ExcludeVehicleGroups", excludeVehicleGroups);
			@params.Add("ShowGroups",           showGroups.HasValue ? showGroups.Value : true);
			@params.Add("GroupId",              selectedGroupId);
			@params.Add("GroupName",            selectedGroupName);
			@params.Add("Culture",              CurrentCulture);
			@params.Add("DepartmentId",         IWebPS.Department!=null? (int?)IWebPS.Department.id : null);
			@params.Add("DateReport",           DateTime.Now);
			// Добавить стандартные для всех отчетов параметры
			AppendBaseParams(@params);
			// Сформировать отчет
			return IWebPS.CreateReportAsFile(reportId, @params, reportTypeEnum);
		}
		[PageAction("get-report-menu", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<ReportMenuGroup> GetReportMenu()
		{
			var reportMenuGroups = IWebPS.GetReportMenu();
			foreach (var reportMenuGroup in reportMenuGroups)
			{
				if (reportMenuGroup.ReportMenuItems == null)
					continue;
				reportMenuGroup.ReportMenuItems = reportMenuGroup.ReportMenuItems
					?.Select(r =>
					{
						var reportMenuItem = IWebPS.GetReportMenuItem(Guid.Parse(r.Guid));
						reportMenuItem.ReportMenuGroupId = reportMenuGroup.Id;
						return reportMenuItem;
					})
					?.ToList();
			}
			return reportMenuGroups;
		}
		[PageAction("createCustomReport")]
		public void CreateCustomReport()
		{
			TemplateFile = "report";
			AddProperty("template", "reports");
			GenerateMenu();
			SetHtmlContentType();

			var reportId     = new Guid(_selectedMenuItem.Guid);
			var reportParams = GetCustomReportParameters();

			ResponseSerializationBeg();

			SerializeReportMenu();
			SerializeCommonData();

			var file = GetParamValue("reportFile");

			switch (file)
			{
				case "pdf":
					CreateReportAsFile(reportId, reportParams, ReportTypeEnum.Acrobat);
					break;
				case "xls":
					CreateReportAsFile(reportId, reportParams, ReportTypeEnum.Excel);
					break;
				case "xml":
					CreateReportAsFile(reportId, reportParams, ReportTypeEnum.Xml);
					break;
				default:
					var report = IWebPS.CreateReportAsObj(reportId, reportParams);
					var reportAsString = report as string;
					if (reportAsString != null)
					{
						Writer.WriteStartElement("InlineResult");
						Writer.WriteRaw(reportAsString);
						Writer.WriteEndElement();
					}
					var reportAsDataSet = report as DataSet;
					if (reportAsDataSet != null)
					{
						reportAsDataSet.DataSetName = "report";
						reportAsDataSet.WriteXml(Writer);
					}
					break;
			}
			ResponseSerializationEnd();
		}
		[PageAction("generate-report", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<object> GenerateReport(Guid reportGuid, ReportTypeEnum reportType, PARAMS reportParams)
		{
			var response = new CommonActionResponse<object>();
			try
			{
				var reportItem = IWebPS.GetReportMenu()
					.SelectMany(g => g.ReportMenuItems)
					.FirstOrDefault(r => reportGuid.Equals(r.Guid));
				if (null == reportItem)
				{
					response.Object = null;
					response.Result = CommonActionResult.ObjectNotFound;
					return response;
				}
				switch (reportType)
				{
					case ReportTypeEnum.Acrobat:
					case ReportTypeEnum.Excel:
					case ReportTypeEnum.RichTextFormat:
					case ReportTypeEnum.Word:
					case ReportTypeEnum.Crystal:
					case ReportTypeEnum.Xml:
						response.Object = new { ReportFile = IWebPS.CreateReportAsFile(reportGuid, reportParams, reportType) };
						break;
					case ReportTypeEnum.Html:
					default:
						response.Object = new { ReportBody = IWebPS.CreateReportAsObj(reportGuid, reportParams) };
						break;
				}
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		/// <summary> Подписка на отчет </summary>
		[PageAction("subscribe-report", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<bool?> SubscribeReport(
			Guid reportGuid, ReportTypeEnum reportType, PARAMS reportParams, DateTime reportCreateTime, IRepetitionClass repetition, List<int> listEmailIds)
		{
			var response = new CommonActionResponse<bool?> { Object = null };
			try
			{
				response.Object = true;
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		/// <summary> Возвращает список доступных типов топлива </summary>
		[PageAction("get-fuel-types", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<List<FuelType>> GetFuelTypes()
		{
			var response = new CommonActionResponse<List<FuelType>> { Object = null };
			try
			{
				response.Object = IWebPS.GetFuelTypes();
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		/// <summary> Возвращает список доступных типов топлива </summary>
		[PageAction("get-currency", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<string> GetCurrency()
		{
			var response = new CommonActionResponse<string> { Object = null };
			try
			{
				response.Object = IWebPS.GetCurrency();
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		private void CreateReportAsFile(Guid reportId, PARAMS reportParams, ReportTypeEnum fileType)
		{
			string reportFileName = IWebPS.CreateReportAsFile(reportId, reportParams, fileType);

			Writer.WriteStartElement("ReportFileName");
			Writer.WriteString(reportFileName);
			Writer.WriteEndElement();
		}
		[PageAction("getReportFile")]
		public void GetReportFile()
		{
			if (OperatorId == null)
			{
				Unauthorized();
				return;
			}

			var file = Path.GetFileName(GetParamValue("file"));

			if (file == null)
				throw new ApplicationException("file can not be null");

			var fileName = Path.Combine(
				Path.Combine(
					ConfigurationManager.AppSettings["reportPath"],
					OperatorId.Value.ToString(CultureInfo.InvariantCulture)),
				file);

			try
			{
				Response.Clear();

				using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
				{
					var fileBody = new byte[fs.Length];
					fs.Read(fileBody, 0, (int) fs.Length);
					var path = Path.GetExtension(fileName);
					switch (path)
					{
						case ".xls":
							response.ContentType = "application/vnd.ms-excel";
							contentTypeSet = true;
							break;
						case ".xlsx":
							response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
							contentTypeSet = true;
							break;
						case ".pdf":
							response.ContentType = "application/pdf";
							contentTypeSet = true;
							break;
						default:
							SetApplicationMultipartContentType();
							break;
					}

					string encodedFileName;
					switch (Request.Browser.Browser)
					{
						case "Firefox":
							encodedFileName = file;
							break;
						default:
							encodedFileName = HttpUtility.UrlEncode(file);
							break;
					}

					var dispositionValue = "attachment; filename=" + encodedFileName;

					var encoding = new UTF8Encoding(false);
					Response.ContentEncoding = encoding;

					Response.AddHeader("Content-Disposition", dispositionValue);

					Response.BinaryWrite(fileBody);

					Response.Flush();
					Response.End();
				}
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("error"), ex.Message));
			}
		}
		/// <summary> Подписка на отчет </summary>
		[PageAction("repsubscribe", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool Subscribe(
			string repsubsribetime,
			string reportDateFrom, string reportTimeFrom,
			string reportDateTo, string reportTimeTo,
			string subscribers, string subscriberEmails)
		{
			GenerateMenu();

			DateTime repDateFrom;
			if (!TryParseDateTime(reportDateFrom + " " + reportTimeFrom, out repDateFrom))
				throw new ArgumentException(@"Unable to parse date and time", "reportDateFrom");

			DateTime repDateTo;
			if (!TryParseDateTime(reportDateTo + " " + reportTimeTo, out repDateTo))
				throw new ArgumentException(@"Unable to parse date and time", "reportDateTo");

			DateTime reportCreateTime;
			if (!TryParseDateTime(GetParamValue("repsubsribetime"), out reportCreateTime))
				throw new ArgumentException(@"Unable to parse date and time", "repsubsribetime");

			// Список подписываемых Email'ов
			List<int> emailIds = new List<int>();

			if (!string.IsNullOrWhiteSpace(subscribers))
			{
				emailIds.AddRange(subscribers
					.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
					.Select(int.Parse));
			}
			if (!string.IsNullOrWhiteSpace(subscriberEmails))
			{
				var emails = subscriberEmails
					.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
				foreach (var email in emails)
				{
					var emailId = IWebPS.GetContactId(ContactType.Email, email);
					if (!emailIds.Contains(emailId))
						emailIds.Add(emailId);
				}
			}
			if (emailIds.Count == 0)
				throw new ArgumentException("No subscribers");

			var repetition = GetRepetitionFromParams(ref reportCreateTime);

			var reportId     = new Guid(_selectedMenuItem.Guid);
			var reportParams = GetCustomReportParameters();

			IWebPS.ReportToSchedulerQueue(reportId, reportParams, repetition, reportCreateTime, emailIds);

			return true;
		}
		private IRepetitionClass GetRepetitionFromParams(ref DateTime reportCreateTime)
		{
			IRepetitionClass repetition = null;
			var daily = (GetParamValue("intervalDeliveryDay").Trim().ToLower() == "on");
			var weekly = (GetParamValue("intervalDeliveryWeek").Trim().ToLower() == "on");
			if (daily)
			{
				repetition = new RepeatOnNthDay();
				((RepeatOnNthDay) repetition).N = 1;
				//Если время больше текущего, то проведем рассылку сегодня, а если нет, то завтра
				if (reportCreateTime > DateTime.UtcNow)
				{
					reportCreateTime = reportCreateTime.AddDays(-1);
				}
			}
			else if (weekly)
			{
				repetition = new RepeatEveryWeek();
				((RepeatEveryWeek) repetition).N = 1;
				var days = GetParamValues("deliveryWeekDays_", "on");
				//TimeSpan daysDiff;
				foreach (
					string dayName in
						days.Keys.Select(key => key.Substring(key.IndexOf('_') + 1).ToLower()))
				{
					switch (dayName)
					{
						case "monday":
						case "mondayshort":
							((RepeatEveryWeek) repetition).Monday = true;
							//daysDiff = DateTime.Now
							break;
						case "tuesday":
						case "tuesdayshort":
							((RepeatEveryWeek) repetition).Tuesday = true;
							break;
						case "wednesday":
						case "wednesdayshort":
							((RepeatEveryWeek) repetition).Wednesday = true;
							break;
						case "thursday":
						case "thursdayshort":
							((RepeatEveryWeek) repetition).Thursday = true;
							break;
						case "friday":
						case "fridayshort":
							((RepeatEveryWeek) repetition).Friday = true;
							break;
						case "saturday":
						case "saturdayshort":
							((RepeatEveryWeek) repetition).Saturday = true;
							break;
						case "sunday":
						case "sundayshort":
							((RepeatEveryWeek) repetition).Sunday = true;
							break;
					}

					//Если первая рассылка попадает на текущий день недели и время больше текущего, то проведем рассылку сегодня
					var timeInUTC = repetition.GetNextTimeInUTC(reportCreateTime);

					if (timeInUTC.HasValue && (timeInUTC.Value - DateTime.UtcNow).TotalDays >= 7)
						reportCreateTime = reportCreateTime.AddDays(-1);
				}
			}
			return repetition;
		}
		/// <summary> Добавление параметров на основании типа отчета </summary>
		private PARAMS GetCustomReportParameters()
		{
			var result = new PARAMS(true);
			var selectedMenu = _reportMenu.SelectMany(x => x.ReportMenuItems).FirstOrDefault(x => x.Selected);
			if (null == selectedMenu)
				throw new InvalidOperationException("No menu item selected");

			foreach (var pMenu in selectedMenu.Properties)
			{
				var httpRequestParamName = pMenu.Name;
				if (pMenu.Control == ReportParametersUtils.MileageFilePicker)
				{
					var mileageFile = GetRequestFiles().FirstOrDefault();
					if (null != mileageFile)
					{
						using (var binaryReader = new BinaryReader(mileageFile.InputStream))
						{
							result.Add(pMenu.Name, binaryReader.ReadBytes((int)mileageFile.ContentLength));
						}
					}
				}

				if (pMenu.Control == ReportParametersUtils.WeekDaysPicker)
				{
					var days = new[] {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};

					var dayIndices = new List<int>(days.Length);
					for (var i = 0; i != days.Length; ++i)
					{
						if (string.IsNullOrWhiteSpace(GetParamValue(days[i])))
							continue;
						dayIndices.Add(i);
					}
					result.Add(pMenu.Name, dayIndices.ToArray());

					continue;
				}

				if (pMenu.Type == typeof(int[]))
				{
					var paramsString = GetParamValue(httpRequestParamName);
					var intArray = paramsString
						.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
						.Select(
							delegate(string @string)
							{
								int intValue;
								if (!int.TryParse(@string, out intValue))
								{
									System.Diagnostics.Trace.TraceWarning(
										"{0}.GetCustomReportParameters unable to parse parameter {1} = {2} as int",
										this, httpRequestParamName, @string);
									return (int?) null;
								}
								return intValue;
							})
						.Where(x => x != null)
						.Select(x => x.Value)
						.ToArray();

					// TODO: избавиться в будущем от hardcode проверок для любых типов параметров отчета, возвращающих массив чисел.
					if (httpRequestParamName == "Weekdays")
					{
						result.Add(httpRequestParamName, intArray);
					}
				}

				if (pMenu.Type == typeof(Dictionary<int, decimal>))
				{
					// Тип контрола словарь стоимости топлива.
					if (httpRequestParamName.StartsWith("FuelCosts"))
					{
						var dictTypes = new Dictionary<int, decimal>();

						foreach (var pair in GetParamValues(httpRequestParamName, string.Empty))
						{
							int fuelCostId;
							int.TryParse(pair.Key.Substring(httpRequestParamName.Length), out fuelCostId);

							decimal dec;
							var fueValue = GetParamValue(pair.Key).Replace(',', '.');
							if (decimal.TryParse(
								fueValue,
								NumberStyles.Float,
								CultureInfo.InvariantCulture,
								out dec)
								&& fuelCostId != 0
								)
								dictTypes.Add(fuelCostId, dec);
						}
						result.Add(httpRequestParamName, dictTypes);
					}
				}

				if (pMenu.Type == typeof(int))
				{
					var intParamValue = GetParamValue(httpRequestParamName);
					int intParam;
					int.TryParse(intParamValue, out intParam);
					result.Add(httpRequestParamName, intParam);
				}

				if (pMenu.Type == typeof(bool))
				{
					var boolResult = GetParamValue(httpRequestParamName);
					switch (boolResult)
					{
						case "on":
							result.Add(httpRequestParamName, true);
							break;
					}
				}

				if (pMenu.Type == typeof(string))
					result.Add(httpRequestParamName, GetParamValue(httpRequestParamName));

				if (pMenu.Type == typeof(List<int>))
					result.Add(httpRequestParamName, GetParamListValues(httpRequestParamName));

				if (pMenu.Type == typeof (List<DateTime>))
				{
					var arrayJson = GetParamValue(httpRequestParamName);
					if (!string.IsNullOrWhiteSpace(arrayJson))
					{
						var strings = JsonHelper.DeserializeObjectFromJson<string[]>(arrayJson);
						var dates = strings.Select(s => DateTime.ParseExact(s, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal)).ToList();
						result.Add(httpRequestParamName, dates);
					}
				}

				if (pMenu.Type == typeof(DateTimeInterval))
				{
					DateTime dateFrom;
					DateTime dateTo;

					DateTime timeFrom;
					DateTime timeTo;

					if (!TryParseDateTime(GetParamValue("reportdatefrom"), out dateFrom))
						dateFrom = DateTime.Now.AddDays(-2);
					if (!TryParseDateTime(GetParamValue("reportdateto"), out dateTo))
						dateTo = DateTime.Now;

					if (!TryParseDateTime(GetParamValue("reporttimefrom"), out timeFrom))
						timeFrom = DateTime.MinValue;
					dateFrom = dateFrom.Add(timeFrom.TimeOfDay);

					if (!TryParseDateTime(GetParamValue("reporttimeto"), out timeTo))
						timeTo = DateTime.MinValue;
					dateTo = dateTo.Add(timeTo.TimeOfDay);

					var dateTimeInterval = new DateTimeInterval(dateFrom, dateTo, IntervalAccuracy.ToMinute);

					result.Add(httpRequestParamName, dateTimeInterval);

					System.Diagnostics.Trace.TraceInformation(
						"History request (report) from user {0}: {1} to {2}",
						IWebPS.OperatorId, 
						XmlConvert.ToString(dateFrom, XmlDateTimeSerializationMode.Local),
						XmlConvert.ToString(dateTo, XmlDateTimeSerializationMode.Local));
				}

				if (pMenu.Type == typeof(WorkingHoursInterval))
				{
					TimeSpan workFrom;
					TimeSpan workTo;

					DateTime workTimeFrom;
					DateTime workTimeTo;

					if (!TryParseDateTime(GetParamValue("worktimefrom"), out workTimeFrom))
						workTimeFrom = DateTime.MinValue;
					workFrom = workTimeFrom.TimeOfDay;

					if (!TryParseDateTime(GetParamValue("worktimeto"), out workTimeTo))
						workTimeTo = DateTime.MinValue;
					workTo = workTimeTo.TimeOfDay;

					var workingHoursInterval = new WorkingHoursInterval(workFrom, workTo);
					result.Add(httpRequestParamName, workingHoursInterval);
				}

				if (pMenu.Type == typeof(FuelSpendStandardType))
				{
					var fuelSpendStandardType = (FuelSpendStandardType)Enum.Parse(typeof(FuelSpendStandardType), GetParamValue(httpRequestParamName));
					result.Add(httpRequestParamName, fuelSpendStandardType);
				}
			}
			return AppendBaseParams(result);
		}
		private void SerializeReportMenu()
		{
			if (_reportMenu == null || _reportMenu.Count <= 0) return;

			Writer.WriteStartElement("ReportMenuGroups");

			foreach (var rmg in _reportMenu.Where(x => x.IsAvailable))
			{
				Writer.WriteStartElement("ReportMenuGroup");
				Writer.WriteAttributeString("Name", rmg.Name);
				Writer.WriteAttributeString("IsAvailable", rmg.IsAvailable ? "1" : "0");
				var orderedMenuList = rmg.ReportMenuItems.Where(x => x.IsAvailable).OrderBy(x => x.Order).ToList();
				foreach (var rmi in orderedMenuList)
				{
					Writer.WriteStartElement("ReportMenuItem");
					Writer.WriteAttributeString("Id", rmi.Id.ToString(CultureInfo.InvariantCulture));
					Writer.WriteAttributeString("Name", rmi.Name);
					Writer.WriteAttributeString("Description", rmi.Description);
					Writer.WriteAttributeString("Selected", rmi.Selected ? "1" : "0");
					Writer.WriteAttributeString("IsAvailable", rmi.IsAvailable ? "1" : "0");
					Writer.WriteAttributeString("IsSubscriptionVisible", rmi.IsSubscriptionVisible ? "1" : "0");

					if (rmi.Formats != null)
					{
						Writer.WriteStartElement("Formats");
						foreach (var format in rmi.Formats)
							Writer.WriteElementString("Format", format.ToString());
						Writer.WriteEndElement();
					}

					if (rmi.Properties.Count > 0)
					{
						Writer.WriteStartElement("ReportParameters");
						foreach (var param in rmi.Properties.Where(param => param.Type != null).OrderBy(x => x.Order))
						{
							Writer.WriteStartElement("ReportParameter");
							Writer.WriteAttributeString("name", param.Name);
							Writer.WriteAttributeString("type", param.Type.ToString());
							Writer.WriteAttributeString("control", param.Control);
							Writer.WriteAttributeString("description", param.Description);
							if (!string.IsNullOrWhiteSpace(param.Options))
								Writer.WriteAttributeString("options", param.Options);
							Writer.WriteEndElement();
						}
						Writer.WriteEndElement();
					}
					Writer.WriteEndElement();
				}
				Writer.WriteEndElement();
			}
			Writer.WriteEndElement();
		}
		protected override void ResponseSerializationBeg()
		{
			GetDataProps();
			base.ResponseSerializationBeg();
		}
		private void GetDataProps()
		{
			var vehicleProperty = _selectedMenuItem.Properties.FirstOrDefault(item => item.Control == ReportParametersUtils.VehiclePicker);
			if (vehicleProperty != null)
			{
				var vehicles = GetVehiclesDTO(false, vehicleProperty.Rights);
				AddProperty("vehiclesJSON", JsonHelper.SerializeObjectToJson(vehicles));
			}

			if (_selectedMenuItem.Properties.Any(item => item.Control == ReportParametersUtils.VehicleGroupPicker))
				AddProperty("vehicleGroupsJSON", JsonHelper.SerializeObjectToJson(GetVehicleGroupsDTO()));

			if(_selectedMenuItem.Properties.Any(item => item.Control == ReportParametersUtils.ZonePicker))
				AddProperty("geozonesJSON", JsonHelper.SerializeObjectToJson(GetGeoZonesDTO(false)));

			if(_selectedMenuItem.Properties.Any(item => item.Control == ReportParametersUtils.ZoneGroupPicker))
				AddProperty("geozoneGroupsJSON", JsonHelper.SerializeObjectToJson(GetGeoZoneGroupsDTO()));

			var sensorPickerProperty = _selectedMenuItem.Properties.FirstOrDefault(item => item.Control == ReportParametersUtils.SensorPicker);
			if (sensorPickerProperty != default)
			{
				if (!string.IsNullOrWhiteSpace(sensorPickerProperty.Options))
				{
					var sensorTypes = GetEnumOptions<SensorType>(sensorPickerProperty.Options);
					if (0 < (sensorTypes?.Count() ?? 0))
					{
						var sensors = new List<Sensor>();
						foreach (var sensorType in sensorTypes)
							sensors.AddRange(IWebPS.GetConfiguredSensorLegends(sensorType));
						AddProperty("legendsJSON", JsonHelper.SerializeObjectToJson(sensors.OrderBy(s => s.Name).ToArray()));
					}
					else
						AddProperty("legendsJSON", JsonHelper.SerializeObjectToJson(IWebPS.GetConfiguredSensorLegends().OrderBy(s => s.Name).ToArray()));
				}
				else
					AddProperty("legendsJSON", JsonHelper.SerializeObjectToJson(IWebPS.GetConfiguredSensorLegends().OrderBy(s => s.Name).ToArray()));
			}
		}
		private void SerializeCommonData()
		{
			try
			{
				if (_selectedMenuItem.Properties.Any(item => item.Control == ReportParametersUtils.ZonesPicker))
					Geozone.GeozonesToXml(IWebPS, Writer);

				var vehiclesProperty = _selectedMenuItem.Properties.FirstOrDefault(item => item.Control == ReportParametersUtils.VehiclesPicker);
				if (vehiclesProperty != null)
				{
					WriteVehiclesXml(Writer, vehiclesProperty);
				}

				if (_selectedMenuItem.Properties.Any(item => item.Control == ReportParametersUtils.FuelCostsPicker))
				{
					WriteFuelTypes(Writer);
					WriteCurrency(Writer);
				}
			}
			catch (Exception ex)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				WriteLog(ex);
			}
		}
		private IEnumerable<T> GetEnumOptions<T>(string options) where T : struct
		{
			return Regex.Matches(options, $@"^?;?{typeof(T).Name}\:(?<Value>[^,;$]+);?$?", RegexOptions.IgnoreCase | RegexOptions.Compiled)
				?.OfType<Match>()
				?.Select(m =>
				{
					return Enum.TryParse<T>(m.Groups["Value"].Value, true, out var result)
						? (T?)result
						: default;
				})
				?.Where(t => t.HasValue)
				?.Select(t => t.Value);
		}
		private void WriteVehiclesXml(XmlTextWriter writer, ReportMenuProperty reportMenuProperty = null)
		{
			var vehicles = GetVehiclesDTO(false, new[] { SystemRight.PathAccess })
				.Values
				.OrderBy(v => v.Name, StringComparer.OrdinalIgnoreCase)
				.ToList();
			if (null != reportMenuProperty && !string.IsNullOrWhiteSpace(reportMenuProperty.Options))
			{
				var opts = GetEnumOptions<VehicleKind>(reportMenuProperty.Options);
				vehicles = vehicles
					.Where(v => opts.Contains(v.vehicleKind))
					.ToList();
			}

			writer.WriteStartElement("vehicles");
			foreach (var vehicle in vehicles)
			{
				writer.WriteStartElement("vehicle");
				writer.WriteAttributeString("id", vehicle.id.ToString());
				writer.WriteAttributeString("name", vehicle.Name);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}
		private PARAMS AppendBaseParams(PARAMS @params)
		{
			if (@params == null)
				@params = new PARAMS(true);
			if (!@params.ContainsKey("MapGuid"))
				@params.Add("MapGuid", MapGuid);
			if (!@params.ContainsKey("ApplicationPath"))
				@params.Add("ApplicationPath", string.Format("{0}{1}", Request.ServerVariables["HTTP_HOST"], Request.ApplicationPath));
			return @params;
		}
	}
}