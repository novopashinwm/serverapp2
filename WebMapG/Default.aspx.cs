﻿using System;
using System.Configuration;
using System.Web;
using FORIS.TSS.UI.WebMapG;
using RU.NVG.UI.WebMapG.Model.Common;

public partial class _Default : BaseLoginPage
{
	protected override void Page_Load(object sender, EventArgs e)
	{
		MandatoryPageLoad();
		
		IsMobile = false;

		var action = GetParamValue("a");
		var ssidCookie = Request.Cookies[ContextHelper.SessionCookieName];
		if (ssidCookie != null)
		{
			if (action != "logout")
			{
				if (ssidCookie != null)
				{
					SessionId = ssidCookie.Value;
				}

				if (OperatorId != null)
				{
					if (!IsAuthorized)
					{
						var l = TryLogIn(OperatorId.Value);
						if (l != LoginResult.Success)
						{
							DeleteSsidCookie();
							return;
						}
					}

					if (IsMobile)
					{
						RedirectResponse(@"Default.aspx?a=getvehicles&includeAddresses=true");
					}
					else
					{
						var defaultPageAfterLogin = ConfigurationManager.AppSettings["DefaultPageAfterLogin"];
						if (!string.IsNullOrWhiteSpace(defaultPageAfterLogin))
							RedirectResponse(defaultPageAfterLogin);
						else
						{
							string showMode = "";
							if (Request.Cookies["showmode"] != null)
								showMode = Request.Cookies["showmode"].Value;
							RedirectResponse(@"map.aspx?show=" + showMode);
						}
					}
				}
				else
				{
					DeleteSsidCookie();
					RedirectToAboutPageIfNeeded();
				}
			}
		}
		else
		{
			RedirectToAboutPageIfNeeded();
		}

		if (!Page.IsPostBack)
		{
			Process();
			UpdateSessionDateTime();
		}
	}

	private void DeleteSsidCookie()
	{
		Response.Cookies.Add(new HttpCookie(ContextHelper.SessionCookieName) { Expires = DateTime.Now.AddDays(-1) });
	}

	/// <summary>
	/// AS: Обработка по умолчанию
	/// </summary>
	[PageAction("default")]
	public void DefaultProcess()
	{
		IsMobile = false;
		RenderLoginPage();
	}
}