﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Xml;
using System.Xml.Serialization;
using DTO;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using Interfaces.Web;
using Jayrock.Json;
using RU.NVG.UI.WebMapG.Model.Common;
using RU.NVG.UI.WebMapG.Model.MlpSchedule;
using AddTrackerResultCode = FORIS.TSS.BusinessLogic.ResultCodes.AddTrackerResult;

namespace FORIS.TSS.UI.WebMapG
{
	public class Objects : BasePage
	{
		[Serializable]
		public class HistoryParams
		{
			public int monitoringObjectId;
			public DateTime? from;
			public DateTime? to;
			public int interval;
			public FullLogOptions options;
		}
		/// <summary> Страница со списком объектов мониторинга </summary>
		[PageAction]
		public void ShowPage()
		{
			RenderObjectPage("objects_G", "objectInfo");
		}
		[PageAction("getobjectsforlist")]
		public void GetObjectsForList()
		{
			TemplateFile = "objects_G";
			AddProperty("template", "shortList");

			ResponseSerializationBeg();
			try
			{
				GetAllObjects();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
			}
			ResponseSerializationEnd();
		}
		[PageAction("journal")]
		public void GetJournalMap()
		{
			if (!OperatorSystemRights.Contains(SystemRight.PathAccess))
				throw new SecurityException("No PathAccess right");

			TemplateFile = "objects_G";
			AddProperty("template", "journal");


			HistoryParams histParams = ParseHistoryParams();

			SetObjectToSession("journalObjectId", histParams.monitoringObjectId);

			dontCacheMe();

			InsertDOCTypeDeclaration(true);

			ResponseSerializationBeg();
			if (histParams.monitoringObjectId > 0)
			{
				try
				{
					// AS: если не выбран какой-то один объект, нарисуем все
					//Vehicles.VehiclesInfo obj = GetObjectById(histParams.monitoringObjectId);
					var obj = new Vehicles.VehiclesInfo(histParams.monitoringObjectId, IWebPS);
					if (!obj.Rights.Contains(SystemRight.PathAccess))
						throw new SecurityException("No PathAccess right");

					obj.WriteXml(Writer);

					//Дата должна быть в локали браузера, а в histParams по-умолчанию - в UTC => нужно преобразовать
					//Также следует учесть, что DateTime.MinValue используется в шаблоне для обозначение "нет данных"
					histParams.from = histParams.from != null
						? TimeZoneInfo.ConvertTimeFromUtc(histParams.from.Value, TimeZoneInfo)
						: DateTime.MinValue;
					histParams.to = histParams.to != null
						? TimeZoneInfo.ConvertTimeFromUtc(histParams.to.Value, TimeZoneInfo)
						: DateTime.MinValue;

					SerializeHistoryParameters(histParams);
				}
				catch (SecurityException)
				{
					throw;
				}
				catch (Exception myEx)
				{
					WriteLog(myEx);
				}
			}

			SerializeMapDescriptions();
			ResponseSerializationEnd();
		}
		private void RenderObjectPage(string templateFile, string templateName)
		{
			TemplateFile = templateFile;
			AddProperty("template", templateName);

			AddProperty("isDepartmentAdmin", IWebPS.GetAvailableDepartmentsCount() > 0 ? "1" : "0");

			AddProperty("tab", GetParamValue("tab"));
			AddProperty("focus", GetParamValue("focus"));

			int objectId;
			if (!int.TryParse(GetParamValue("id"), out objectId) || objectId <= 0)
			{
				AddProperty("initPage", "true");
				AddProperty("allowAddTracker", IWebPS.AllowAddTracker);
				AddProperty("legendsJSON", JsonHelper.SerializeObjectToJson(IWebPS.GetConfiguredSensorLegends()));
				ResponseSerializationBeg();
			}
			else
			{
				ResponseSerializationBeg();
				Writer.WriteStartElement("vehicle");
				Writer.WriteAttributeString("id", objectId.ToString());
				Writer.WriteAttributeString("name", IWebPS.GetVehicleName(objectId));
				Writer.WriteEndElement();
			}

			ResponseSerializationEnd();
		}
		[JsonPageAction("getTrackerTypes", HttpMethod.Get)]
		public ControllerType[] GetTrackerTypes(bool appendImageBase64 = true)
		{
			var response = IWebPS.GetControllerTypeAllowedToAdd();
			if (appendImageBase64)
				return AppendImageBase64(response);
			return response;
		}
		private ControllerType[] AppendImageBase64(ControllerType[] controllerTypes)
		{
			return Array.ConvertAll(controllerTypes, AppendImageBase64);
		}
		private ControllerType AppendImageBase64(ControllerType input)
		{
			input.ImageBase64 = GetBase64StringForUrl(input.ImagePath);
			return input;
		}
		[JsonPageAction("getTrackerTypesAllowedToAssign", HttpMethod.Get)]
		public ControllerType[] GetTrackerTypes(int vehicleId)
		{
			return AppendImageBase64(IWebPS.GetControllerTypeAllowedToAssign(vehicleId));
		}
		[JsonPageAction("setTrackerType", HttpMethod.Unspecified)]
		public bool SetTrackerType(int vehicleId, string trackerType)
		{
			try
			{
				IWebPS.SetVehicleDeviceType(vehicleId, trackerType);
				return true;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
			return false;
		}
		[JsonPageAction("fullLog")]
		public FullLog GetFullLog()
		{
			var history = GetHistory();
			return history != null ? history.log : null;
		}
		[JsonPageAction("history")]
		public History GetHistory()
		{
			var historyParams = ParseHistoryParams();

			var utcNow = DateTime.UtcNow;
			var @from  = historyParams.@from ?? utcNow;
			var to     = historyParams.to    ?? utcNow;

			return IWebPS.GetHistory(
				historyParams.monitoringObjectId,
				TimeHelper.GetSecondsFromBase(from),
				TimeHelper.GetSecondsFromBase(to),
				historyParams.options);
		}
		[PageAction("gethistoryJSON", Method = HttpMethod.Post, ResponseType = DataType.Json)]
		public CommonDTOResponse GetHistoryJson()
		{
			var historyJson = new CommonDTOResponse();
			try
			{
				var mobileHistory = GetHistory();
				historyJson.value = mobileHistory;

				// Если нет данных - сообщение, что нет данных
				if (mobileHistory == null || mobileHistory.log == null ||
					(
						mobileHistory.log.Geo.Count == 0 &&
						mobileHistory.log.Gps.Count == 0 &&
						mobileHistory.log.Pictures.Count == 0 &&
						mobileHistory.log.Sensors.All(p => p.Value.LogRecords.Count == 0)))
				{
					var msg = GetMessageData("NoDataForPeriod");
					AddMessage(new Message(Severity.Warning, msg, msg));
				}

				historyJson.resCode  = ResponseCode.OK;
				historyJson.messages = Messages;
			}
			catch (SecurityException)
			{
				throw;
			}
			catch (Exception myEx)
			{
				WriteLog(myEx);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), GetMessageData("ServerError")));
				historyJson.resCode = ResponseCode.ERROR;
			}
			return historyJson;
		}
		/// <summary> Парсинг переданных браузером значений для выборки исторических позиций </summary>
		/// <returns></returns>
		private HistoryParams ParseHistoryParams()
		{
			int objectId;
			if (!int.TryParse(GetParamValue("id"), out objectId))
				throw new ArgumentException("Parameter expected: 'id' - identifier of vehicle");

			int intervalSeconds;
			if (!int.TryParse(GetParamValue("interval"), out intervalSeconds))
				intervalSeconds = 30;

			var hp = new HistoryParams();
			hp.from = ParseLocalDateTime(GetParamValue("dateFrom"));
			hp.to   = ParseLocalDateTime(GetParamValue("dateTo"));

			if (hp.from != null && hp.to != null)
			{
				//TODO: заменить на счётчик производительности
				System.Diagnostics.Trace.TraceInformation(
					"History request (path) from user {0}: {1} to {2}",
					IWebPS.OperatorId,
					XmlConvert.ToString(hp.from.Value, XmlDateTimeSerializationMode.Local),
					XmlConvert.ToString(hp.to.Value, XmlDateTimeSerializationMode.Local));
			}

			if (hp.to.HasValue && hp.to.Value.Second > 0)
			{
				hp.to = hp.to.Value.AddMinutes(1);
				hp.to = hp.to.Value.AddSeconds(hp.to.Value.Second * -1);
			}

			hp.interval = intervalSeconds;
			hp.monitoringObjectId = objectId;

			var optionsJSONString = GetParamValue("options");
			if (!string.IsNullOrEmpty(optionsJSONString))
			{
				var optionsJSON = JsonHelper.DeserializeObjectFromJson(GetParamValue("options")) as JsonObject;

				hp.options = new FullLogOptions();
				if (optionsJSON != null)
				{
					var noDataPeriodString = (string)optionsJSON["noDataPeriod"];
					if (!string.IsNullOrWhiteSpace(noDataPeriodString))
						hp.options.NoDataPeriod = XmlConvert.ToTimeSpan(noDataPeriodString);
					hp.options.PositionAccuracy = optionsJSON["positionAccuracy"] != null
						? Convert.ToInt32(optionsJSON["positionAccuracy"])
						: (int?)null;
					if (optionsJSON["maxPointsCount"] != null)
						hp.options.MaxPointsCount = Convert.ToInt32(optionsJSON["maxPointsCount"]);
					if (optionsJSON["maxGeoPointsCount"] != null)
						hp.options.MaxGeoPointsCount = Convert.ToInt32(optionsJSON["maxGeoPointsCount"]);
					var includeAddresses = optionsJSON["includeAddresses"];
					hp.options.IncludeAddresses = includeAddresses != null && Convert.ToBoolean(includeAddresses);
					hp.options.StopPeriod = optionsJSON["stopPeriod"] != null
						? TimeSpan.FromSeconds(Convert.ToInt32(optionsJSON["stopPeriod"]))
						: (TimeSpan?) null;
				}
			}
			// Заглушка для просмотра истории, мобильные приложения передают 1000 и на некоторых объектах возникает ошибка
			// logTimeTo could not be less than logTimeFrom
			// Parameter name: logTimeTo
			// FORIS.TSS.Common.Utils
			// 	at FORIS.TSS.Common.Helpers.FullLogHelper.GetRunDistance(FullLog this, Int32 logTimeFrom, Int32 logTimeTo) in Base\FORIS.TSS.Common.Utils\Helpers\FullLogHelper.cs:line 80
			if (null != hp.options)
				hp.options.MaxGeoPointsCount = 10000 < hp.options.MaxGeoPointsCount ? hp.options.MaxGeoPointsCount : 10000;

			return hp;
		}
		private DateTime? ParseLocalDateTime(string s)
		{
			if (s == null)
				return null;

			DateTime from;

			return TryParseDateTime(s.Trim(), out from)
				? TimeZoneInfo.ConvertTimeToUtc(from, TimeZoneInfo)
				: (DateTime?)null;
		}
		/// <summary> Картинка объекта (если есть) </summary>
		[PageAction("getimage")]
		public void GetImage()
		{
			int id;
			int.TryParse(GetParamValue("id"), out id);
			Vehicles.VehiclesInfo vi = (Vehicles.VehiclesInfo)GetObjectFromSession("vehicleInfo_" + id);
			SetImageJpgContentType();

			if (vi != null)
			{
				SetObjectToSession("vehicleInfo_" + id, null);
				Response.OutputStream.Write(vi.VehicleImage, 0, vi.VehicleImage.Length);
			}
		}
		/// <summary> Сохранение информации об объекте </summary>
		[PageAction("save")]
		public void Save(string ownerFirstName, string ownerSecondName, string ownerLastName,
			string ownerPhone1, string ownerPhone2, string ownerPhone3,
			string garageNumber, string regNumber, string brand,
			string typeControlDate, string controlDate,
			string trackerPhone = null, string trackerPassword = null, string trackerType = null, string ip = null, string route = null,
			string maxAllowedSpeed = null, string notes = null)
		{
			int vehicleId;
			int.TryParse(GetParamValue("id"), out vehicleId);

			var vehicle = new Vehicles.VehiclesInfo(vehicleId, IWebPS);
			if (vehicle.ID == 0)
			{
				Forbidden();
				return;
			}

			var vehicelControlDates = new List<VehicleControlDate>();

			//TODO: принимать JSON
			var controlDateTypes = typeControlDate != null ? typeControlDate.Split(new[] { ',' }) : new string[] { };
			var controlDates = controlDate != null ? controlDate.Split(new[] { ',' }) : new string[] { };

			if (controlDates.Length == controlDateTypes.Length)
			{
				for (var ind = 0; ind < controlDateTypes.Length; ind++)
				{
					int typeControlDateId;
					DateTime date;
					if (string.IsNullOrEmpty(controlDateTypes[ind])
						|| string.IsNullOrEmpty(controlDates[ind])
						|| !int.TryParse(controlDateTypes[ind], NumberStyles.None, null, out typeControlDateId)
						|| !DateTime.TryParse(controlDates[ind] + " 00:00", null, DateTimeStyles.None, out date))
						continue;
					vehicelControlDates.Add(new VehicleControlDate
					{
						Value = date,
						TypeId = (VehicleControlDateTypes)typeControlDateId
					});
				}
			}

			try
			{
				byte[] imgBuffer = null;
				if (Request.Files != null && Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
				{
					if (isImageOk(Request.Files[0].InputStream))
					{
						Request.Files[0].InputStream.Position = 0;
						imgBuffer = new byte[Request.Files[0].ContentLength];
						Request.Files[0].InputStream.Read(imgBuffer, 0, Request.Files[0].ContentLength);
					}
					else
					{
						AddMessage(new Message(Severity.Warning, GetMessageData("messageSizeTooBig"),
							GetMessageData("imageSizeMax")));
					}
				}
				var fields = new List<dynamic>
				{
					new { Key = "deviceId",            FieldName = VehicleAttributeName.DeviceId,             ErrorMessage = "UnsuccessfulDeviceIDChange" },
					new { Key = "trackerPhone",        FieldName = VehicleAttributeName.Phone,                ErrorMessage = "UnsuccessfulDevicePhoneChange" },
					new { Key = "trackerPassword",     FieldName = VehicleAttributeName.Password,             ErrorMessage = "UnsuccessfulDevicePasswordChange" },
					new { Key = "ip",                  FieldName = VehicleAttributeName.IpAddress,            ErrorMessage = "UnsuccessfulDeviceIPChange" },
					new { Key = "trackerType",         FieldName = VehicleAttributeName.TrackerType,          ErrorMessage = "ServerError" },
					new { Key = "fuelTypeId",          FieldName = VehicleAttributeName.FuelType,             ErrorMessage = "ServerError" },
					new { Key = "fuelSpentStandart",   FieldName = VehicleAttributeName.FuelSpentStandart,    ErrorMessage = "ServerError" },
					new { Key = "fuelSpentStandart",   FieldName = VehicleAttributeName.FuelSpentStandartGas, ErrorMessage = "ServerError" },
					new { Key = "fuelSpentStandardMH", FieldName = VehicleAttributeName.FuelSpentStandartMH,  ErrorMessage = "ServerError" },
					new { Key = "fuelTankVolume",      FieldName = VehicleAttributeName.FuelTankVolume,       ErrorMessage = "ServerError" },
					new { Key = "fuelTankVolume",      FieldName = VehicleAttributeName.FuelTankGasVolume,    ErrorMessage = "ServerError" },
					new { Key = "regNumber",           FieldName = VehicleAttributeName.PublicNumber,         ErrorMessage = "ServerError" },
					new { Key = "garageNumber",        FieldName = VehicleAttributeName.Name,                 ErrorMessage = "ServerError" },
					new { Key = "brand",               FieldName = VehicleAttributeName.VehicleType,          ErrorMessage = "ServerError" },
					new { Key = "vehicleKind",         FieldName = VehicleAttributeName.VehicleKind,          ErrorMessage = "ServerError" },
					new { Key = "maxAllowedSpeed",     FieldName = VehicleAttributeName.MaxAllowedSpeed,      ErrorMessage = "ServerError" },
					new { Key = "ownerFirstName",      FieldName = VehicleAttributeName.OwnerFirstName,       ErrorMessage = "ServerError" },
					new { Key = "ownerSecondName",     FieldName = VehicleAttributeName.OwnerSecondName,      ErrorMessage = "ServerError" },
					new { Key = "ownerLastName",       FieldName = VehicleAttributeName.OwnerLastName,        ErrorMessage = "ServerError" },
					new { Key = "ownerPhone1",         FieldName = VehicleAttributeName.OwnerPhoneNumber1,    ErrorMessage = "ServerError" },
					new { Key = "ownerPhone2",         FieldName = VehicleAttributeName.OwnerPhoneNumber2,    ErrorMessage = "ServerError" },
					new { Key = "ownerPhone3",         FieldName = VehicleAttributeName.OwnerPhoneNumber3,    ErrorMessage = "ServerError" },
					new { Key = "route",               FieldName = VehicleAttributeName.Route,                ErrorMessage = "ServerError" },
					new { Key = "icon",                FieldName = VehicleAttributeName.Icon,                 ErrorMessage = "ServerError" },
					new { Key = "notes",               FieldName = VehicleAttributeName.Notes,                ErrorMessage = "ServerError" },
				};
				var attributes = IWebPS.GetVehicleAttributes(vehicleId);
				var attributesDic = attributes.ToDictionary(a => a.Name);
				foreach (var field in fields)
				{
					if (!attributesDic.ContainsKey(field.FieldName))
						continue;

					var value = GetParamValue(field.Key, null);
					if (value == null)
						continue;

					var attribute = (VehicleAttribute)attributesDic[field.FieldName];
					if(value == attribute.Value.Replace(",", "."))
						continue;

					var result = IWebPS.SetVehicleAttribute(vehicleId, field.FieldName, value);
					if (result.ToString() == "Success")
						continue;

					AddMessage(new Message(
						Severity.Error,
						GetMessageData("ServerError"),
						string.Format(CurrentCulture, "{1}: " + GetErrorMessageFormat(result), value, attribute.UserFriendlyName)
						));
				}

				if (imgBuffer != null)
				{
					var result = IWebPS.SetVehicleAttribute(vehicleId, VehicleAttributeName.Image, Convert.ToBase64String(imgBuffer));
					if (result.ToString() != "Success")
					{
						var attribute = attributesDic[VehicleAttributeName.Image];
						AddMessage(new Message(
							Severity.Error,
							GetMessageData("ServerError"),
							string.Format(CurrentCulture, "{0}: " + GetErrorMessageFormat(result), attribute.UserFriendlyName)
							));
					}
				}
				IWebPS.UpdateVehicleControlDates(vehicelControlDates, vehicleId);

				string iconUrl = GetParamValue(nameof(VehicleProfile.Icon).ToLowerInvariant());

				string iconVal = iconUrl;
				if (iconUrl == "default")
					iconVal = "";

				var iconUrlParts = iconVal.Split('/');
				iconVal = iconUrlParts[iconUrlParts.Length - 1];
				//iconVal.Replace("http://" + Request.Url.Host + Request.ApplicationPath, "");

				vehicle.GarageNumber = garageNumber;

				var vehicles = (Vehicles)GetObjectFromSession("Allow_Vehicles") ?? LoadVehicles();
				foreach (Vehicles.VehiclesInfo vi in vehicles)
				{
					if (vi.ID != vehicleId)
						continue;
					vi.AddPropertyToCollection(nameof(VehicleProfile.Icon).ToLowerInvariant(), iconVal);
					break;
				}

				vehicles[vehicleId] = vehicle;
				SetObjectToSession("Allow_Vehicles", vehicles);
			}
			catch (Exception ex)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				WriteLog(ex);
			}
			ShowPage();
		}
		private static readonly Guid AskPositionGuid = new Guid("1DDE8967-2796-4cd4-827A-FAEE55BA6AC7");
		[PageAction("mlp_tracking_schedule")]
		public void MLPTrackingSchedule()
		{
			TemplateFile = "objects_G";
			AddProperty("template", "mlp_tracking_schedule");

			var idString = GetParamValue("id");
			AddProperty("id", idString);
			var idTypeString = GetParamValue("idType");
			AddProperty("idType", idTypeString);

			var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo);
			AddProperty("DefaultStartDateTime", XmlConvert.ToString(now, XmlDateTimeSerializationMode.Local));

			int id;
			if (!int.TryParse(idString, out id))
				return;
			var idType = (IdType)Enum.Parse(typeof(IdType), idTypeString);

			if (idType != IdType.Vehicle && idType != IdType.VehicleGroup)
				return;

			dontCacheMe();
			ResponseSerializationBeg();

			var repetition = IWebPS.GetMlpSchedule(idType, id);

			Writer.WriteStartElement("repetition");
			if (repetition != null)
			{
				var repetitionXml = SerializationHelper.ToXML(repetition);
				Writer.WriteRaw(repetitionXml);
			}
			Writer.WriteEndElement();

			SerializeObject(IWebPS.GetEntity(idType, id));

			ResponseSerializationEnd();
		}
		[JsonPageAction("get-mlp-tracking-schedule", HttpMethod.Get)]
		public object GetMlpTrackingScheduler(int id, IdType idType)
		{
			var repetition = IWebPS.GetMlpSchedule(idType, id);
			if (repetition == null)
				return new Dictionary<string, string> { {"type", "none"} };

			return repetition;
		}
		[JsonPageAction("save-mlp-tracking-schedule", HttpMethod.Post)]
		public IRepetitionClass SaveMlpTrackingScheduler(IdType idType, int id, RepetitionType type, string schedule)
		{
			IRepetitionClass repetition;
			switch (type)
			{
				case RepetitionType.RepeatCustomWeekDayCustomTime:
					repetition = JsonHelper.DeserializeObjectFromJson<RepeatCustomWeekDayCustomTime>(schedule);
					break;
				case RepetitionType.RepeatEveryTime:
					repetition = JsonHelper.DeserializeObjectFromJson<RepeatEveryTime>(schedule);
					break;
				case RepetitionType.RepeatEveryWeekGivenTimes:
					repetition = JsonHelper.DeserializeObjectFromJson<RepeatEveryWeekGivenTimes>(schedule);
					break;
				case RepetitionType.None:
					repetition = null;
					break;
				default:
					throw new NotImplementedException("repetition: " + type);
			}

			IWebPS.SetMlpSchedule(idType, id, repetition);
			return IWebPS.GetMlpSchedule(idType, id);
		}
		/// <summary> Удаление из базы изображения объекта </summary>
		[PageAction("removeImage")]
		public void RemoveImage()
		{
			SetXmlContentType();

			//AddMessage(new Message(Severity.Warning, GetMessageData("noPermissionToEdit"), ""));
			ResponseSerializationBeg();
			try
			{
				int vehicleId;
				int.TryParse(GetParamValue("vehicleid"), out vehicleId);
				ArrayList arParams = new ArrayList();
				arParams.Add(new ParamValue("@vehicle_id", vehicleId));

				IWebPS.GetDataFromDB(
					arParams,
					"dbo.RemoveVehiclePicture",
					new string[] { }
					);
			}
			catch (Exception ex)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				WriteLog(ex);
			}
			ResponseSerializationEnd();
		}
		/// <summary> Выдадим страницу для выбора иконки объекта </summary>
		[PageAction("icon")]
		public void IconSelection()
		{
			TemplateFile = "objects_G";
			AddProperty("template", "iconSelection");

			int objectId;
			int.TryParse(GetParamValue("id"), out objectId);

			ResponseSerializationBeg();

			try
			{
				var vehicle = GetVehicleById(objectId);
				vehicle.WriteXml(Writer);
			}
			catch (Exception ex)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				WriteLog(ex);
			}

			ResponseSerializationEnd();
		}
		/// <summary> Вытащим и сериализуем все объекты </summary>
		protected virtual void GetAllObjects()
		{
			//List<Vehicles> objects = new List<Vehicles>();

			Vehicles vehicles = ((Vehicles) GetObjectFromSession("Allow_Vehicles"));

			// <objects>
			Writer.WriteStartElement("objects");

			/*foreach (Vehicles.VehiclesInfo vi in vehicles.GetValues())
			{
				vi.UpdateVehicle();
				vi.WriteXml((XmlTextWriter)Writer);
			}*/
			int[] vehs = vehicles.GetCheckedVehicles();
			IDictionary<int, IWebMobileInfoCP> positions = IWebPS.GetLastPositions(vehs);
			foreach (IWebMobileInfoCP mu in positions.Values)
			{
				//MobilUnit mo = (MobilUnit)mu;
				//Создать ТС
				Vehicles.VehiclesInfo vi = vehicles.GetVehicleByID(mu.Unique);
				if (vi != null)
				{
					vi.PositionInfo = mu;
					vi.UpdateVehicle(TimeZoneInfo);
					vi.WriteXml((XmlTextWriter) Writer);
				}
			}
			Writer.WriteEndElement();
			// </objects>
		}
		protected new Vehicles.VehiclesInfo GetObjectById(int id)
		{
			Vehicles.VehiclesInfo obj = null;
			var vehicles = LoadVehicles(id);
			var positions = IWebPS.GetLastPositions(new[] { id });

			Vehicles.VehiclesInfo vi = vehicles.GetVehicleByID(id);
			if (vi != null)
			{
				IWebMobileInfoCP mu;
				if (positions.TryGetValue(id, out mu))
					vi.PositionInfo = mu;
				vi.UpdateVehicle(TimeZoneInfo);
				obj = vi;
			}

			return obj;
		}
		private ImageCodecInfo GetJpgEncoderInfo()
		{
			string jpgMime = "image/jpeg";
			ImageCodecInfo res = null;
			ImageCodecInfo[] Encoders = ImageCodecInfo.GetImageEncoders();
			for (int i = 0; res == null && i < Encoders.Length; i++)
			{
				if (Encoders[i].MimeType == jpgMime)
				{
					res = Encoders[i];
				}
			}
			return res;
		}
		/// <summary> Сериализация параметров для истории </summary>
		/// <param name="hp">Структура параметров для получения истории</param>
		/// <returns></returns>
		private void SerializeHistoryParameters(HistoryParams hp)
		{
			// <historyParameters>
			//Writer.WriteStartElement("historyParameters");

			XmlSerializer serializer = new XmlSerializer(hp.GetType());
			serializer.Serialize(Writer, hp);

			//Writer.WriteEndElement();
			// </historyParameters>
		}
		/// <summary> Сериализует объект с помощью стандартного сериализатора <see cref="System.Xml.Serialization.XmlSerializer"/> </summary>
		/// <param name="object"></param>
		private void SerializeObject(object @object)
		{
			if (@object == null)
				throw new ArgumentNullException("object");

			//TODO: использовать кэш сериализаторов (Type => Serializer) для улучшения производительности
			var serializer = new XmlSerializer(@object.GetType());
			serializer.Serialize(Writer, @object);
		}
		[JsonPageAction("addTrackerByPhoneNumber", HttpMethod.Post)]
		public AddTrackerResult AddTrackerByPhoneNumber(string phoneNumber)
		{
			if (!IWebPS.AllowAddTracker)
				throw new SecurityException("No add tracker right");
			var result = IWebPS.AddTrackerByPhoneNumber(phoneNumber);
			return result;
		}
		[JsonPageAction("addEmptyTracker", HttpMethod.Post)]
		public AddTrackerResult AddTrackerAsEmpty()
		{
			if (!IWebPS.AllowAddTracker)
				throw new SecurityException("No add tracker right");
			var result = IWebPS.AddTrackerAsEmpty();
			return result;
		}
		[JsonPageAction("addTracker", HttpMethod.Post)]
		public object AddTracker(string trackerType, string alias = "", string trackerNumber = null, string trackerIMEIs = "", bool returnVehicleIdOnSuccess = false)
		{
			if (!IWebPS.AllowAddTracker)
				throw new SecurityException("No add tracker right");

			var vehicleIDs = new List<int>();

			object result;
			switch (trackerType)
			{
				case "mtsTracker":
					throw new NotSupportedException();
				case "byTemplate":
				{
					var trackerNumbers = trackerIMEIs
						.Split(new[] {"\n", "\r"}, StringSplitOptions.RemoveEmptyEntries)
						.Select(s => s.Trim())
						.Distinct()
						.Where(s => !string.IsNullOrWhiteSpace(s))
						.ToList();

					var templateVehicleId = int.Parse(GetParamValue("trackerTemplate"));

					var trackerNumberToResult = new Dictionary<string, object>();

					foreach (var trackerNumberFromList in trackerNumbers)
					{
						trackerNumberToResult.Add(
							trackerNumberFromList,
							IWebPS.CheckTrackerToAddByTemplate(templateVehicleId, trackerNumberFromList));
					}

					if (trackerNumberToResult.Values.Any(r => (AddTrackerResultCode)r != AddTrackerResultCode.Success))
						return trackerNumberToResult;

					trackerNumberToResult.Clear();

					foreach (var trackerNumberFromList in trackerNumbers)
					{
						int vehicleId;
						var addTrackerResult = IWebPS.AddTrackerByTemplate(templateVehicleId, trackerNumberFromList, out vehicleId);
						trackerNumberToResult.Add(trackerNumberFromList,
							returnVehicleIdOnSuccess &&
							addTrackerResult == AddTrackerResultCode.Success
								? (object) vehicleId
								: addTrackerResult);

						if (addTrackerResult == AddTrackerResultCode.Success)
							vehicleIDs.Add(vehicleId);
					}

					if (trackerNumberToResult.ContainsValue(AddTrackerResultCode.Success))
						SetObjectToSession("Allow_Vehicles", LoadVehicles());

					result = trackerNumberToResult;
				}
					break;
				default:
				{
					var trackerNumbers = GetParamValue("trackerIMEIs")
						.Split(new[] {"\n", "\r"}, StringSplitOptions.RemoveEmptyEntries)
						.Select(s => s.Trim())
						.Distinct()
						.Where(s => !string.IsNullOrWhiteSpace(s))
						.ToList();

					var trackerNumberToResult = new Dictionary<string, object>();

					foreach (var trackerNumberFromList in trackerNumbers)
						trackerNumberToResult.Add(trackerNumberFromList,
							IWebPS.CheckTrackerToAddByType(trackerType, trackerNumberFromList));

					if (trackerNumberToResult.Values.Any(r => (AddTrackerResultCode) r != AddTrackerResultCode.Success))
						return trackerNumberToResult;

					trackerNumberToResult.Clear();

					foreach (var trackerNumberFromList in trackerNumbers)
					{
						int vehicleId;
						var addTrackerResult = IWebPS.AddTrackerByType(trackerType, trackerNumberFromList, out vehicleId);
						trackerNumberToResult.Add(trackerNumberFromList,
							returnVehicleIdOnSuccess &&
							addTrackerResult == AddTrackerResultCode.Success
								? (object) vehicleId
								: addTrackerResult);
						if (addTrackerResult == AddTrackerResultCode.Success)
							vehicleIDs.Add(vehicleId);
					}

					if (trackerNumberToResult.ContainsValue(AddTrackerResultCode.Success))
						SetObjectToSession("Allow_Vehicles", LoadVehicles());

					result = trackerNumberToResult;
				}
					break;
			}

			int vehicleGroupId;
			if (int.TryParse(GetParamValue("groupsForNewTracker"), out vehicleGroupId))
			{
				foreach (var vid in vehicleIDs)
				{
					IWebPS.AddVehicleToGroup(vid, vehicleGroupId);
				}
			}

			return result;
		}
		[JsonPageAction("addTrackers", HttpMethod.Post)]
		public CommonActionResponse<AddTrackerItem[]> AddTrackers(string trackerType, AddTrackerItem[] trackerItems, int? vehicleGroupId)
		{
			var response = new CommonActionResponse<AddTrackerItem[]> { Object = trackerItems };
			try
			{
				if (!IWebPS.AllowAddTracker)
					throw new SecurityException("No add tracker right");
				foreach (var trackerItem in response.Object)
				{
					var vehicleId = default(int);
					try
					{
						trackerItem.Result           = IWebPS.AddTrackerByType(trackerType, trackerItem.TrackerId, out vehicleId);
						trackerItem.Result.VehicleId = vehicleId;
					}
					catch (Exception ex)
					{
						WriteLog(ex);
						trackerItem.Result = AddTrackerResultCode.InvalidNumberOrControlCode;
					}
					if (trackerItem.Result.VehicleId.HasValue && 0 < trackerItem.Result.VehicleId.Value)
					{
						IWebPS.SetVehicleAttributes(trackerItem.Result.VehicleId.Value,
							new[]
							{
								new { Name = VehicleAttributeName.Name,         Value = trackerItem.Name               },
								new { Name = VehicleAttributeName.VehicleKind,  Value = VehicleKind.Vehicle.ToString() },
								// Доступность зависит от VehicleKind
								new { Name = VehicleAttributeName.PublicNumber, Value = trackerItem.Name               },
								new { Name = VehicleAttributeName.Phone,        Value = trackerItem.TrackerPhone       },
							}
							.Where(i => !string.IsNullOrWhiteSpace(i.Value))
							.ToDictionary(i => i.Name, i => i.Value));
						if (vehicleGroupId.HasValue)
							IWebPS.AddVehicleToGroup(trackerItem.Result.VehicleId.Value, vehicleGroupId.Value);
					}
				}
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
	}
}