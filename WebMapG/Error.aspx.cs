﻿using System;
using System.Security.Cryptography;
using System.Threading;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Error : BasePage
	{
		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();

			if (Request.QueryString["isMobile"] != null || Request.Path.Contains(Request.ApplicationPath + @"/mobile"))
			{
				IsMobile = true;
			}

			RenderExceptionPage();
			//Process();
		}


		[PageAction("Exception")]
		public virtual void RenderExceptionPage()
		{
			var delay = new byte[1];
			RandomNumberGenerator prng = new RNGCryptoServiceProvider();

			prng.GetBytes(delay);
			Thread.Sleep((int)delay[0]);

			if (prng is IDisposable)
				(prng as IDisposable).Dispose();

			if (!string.IsNullOrEmpty(GetParamValue("SetStatus")))
			{
				Response.StatusCode = int.Parse(GetParamValue("SetStatus").Split('?')[0]);
			}

			SetHtmlContentType();

			//Если нет какого-то ресурсного файла, не рисуем страницу, а просто возвращаем статус
			if (Response.StatusCode == 404)
			{
				var setStatusStr = GetParamValue("SetStatus").ToLower();
				if ((setStatusStr != "404" && IsPageInnerResource(setStatusStr))
					|| (setStatusStr == "404" && IsPageInnerResource(Request.Url.AbsoluteUri)))
				{
						Response.Write("File not found");
						return;
				}
			}

			TemplateFile = "error";
			AddProperty("template", "exception");
			
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}

		private bool IsPageInnerResource(string resourceUrl)
		{
			if (string.IsNullOrEmpty(resourceUrl))
				return false;

			var url = resourceUrl.ToLower();
			return (url.Contains(".png")
				|| url.Contains(".css")
				|| url.Contains(".jpg")
				|| url.Contains(".js")
				|| url.Contains(".xml")
				|| url.Contains(".xsl")
				|| url.Contains(".gif")
				|| url.Contains(".aspx"));
		}
	}
}