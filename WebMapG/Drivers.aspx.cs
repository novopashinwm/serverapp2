﻿using System;
using System.Collections.Generic;
using DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Drivers : BasePage
	{
		[JsonPageAction("getfreedrivers")]
		public List<BusinessLogic.DTO.Driver> GetFreeDrivers()
		{
			return IWebPS.GetFreeDrivers();
		}

		[JsonPageAction("getvehicledrivers", Method = HttpMethod.Get)]
		public List<BusinessLogic.DTO.Driver> GetVehicleDrivers(int vehicleId)
		{
			return IWebPS.GetVehicleDrivers(vehicleId);
		}

		[JsonPageAction("setvehicledriver", HttpMethod.Post)]
		public void SetVehicleDriver(int vehicleId, int driverId)
		{
			IWebPS.SetVehicleDriver(vehicleId, driverId);
		}
		[JsonPageAction("deletevehicledriver", HttpMethod.Unspecified)]
		public Dictionary<string, string> DeleteVehicleDriver(int vehicleId, int driverId)
		{
			IWebPS.DeleteVehicleDriver(vehicleId, driverId);
			return new Dictionary<string, string>
			{
				{ "driverId",   driverId.ToString() },
				{ "vehicleId", vehicleId.ToString() },
			};
		}
		[JsonPageAction("editdriver", HttpMethod.Unspecified)]
		public BusinessLogic.DTO.Driver EditDriver(string firstName, string shortName, 
			string phones, string vehicleId = "", string driverId = "")
		{
			var result = default(BusinessLogic.DTO.Driver);
			if (driverId.Length > 0)
				result = IWebPS.UpdateDriver(Convert.ToInt32(driverId),
					firstName, shortName, phones.Split(new[] { ',' }));
			else
				result = IWebPS.AddDriver(firstName, shortName, phones.Split(new[] { ',' }),
					vehicleId.Length > 0 ? (int?)Convert.ToInt32(vehicleId) : null);
			return result;
		}

		[PageAction("deletebasevehicledriver")]
		public CommonDTOResponse DeleteBaseVehicleDriver(int driverId)
		{
			IWebPS.DeleteBaseVehicleDriver(driverId);
			return new CommonDTOResponse();
		}
	}
}