<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
    <xsl:import href="common.xsl" />
    <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />


    <!-- AS: Диалог сохранения зоны -->
    <xsl:template name="saveZoneDlg">
        <div id="saveZoneDlg" class="ontop">
            <div style="width:100%;margin-top:5px;">
                <div style="width:70%;float:left;">
                    <span style="padding-left:5px;">
                        <b>
                            <xsl:value-of select="$locale/data[@name='zoneEdition']/value" />
                        </b>
                    </span>

                    <span class="question">
                        <span class="question-sec bottom">
                            <xsl:value-of select="$locale/data[@name='zoneHelp']/value" />
                        </span>
                    </span>
                </div>
            </div>
            <div style="clear:both;background-color:#fff;margin:2px;width:99%;color:#000;">
                <div style="padding:5px;">
                    <xsl:call-template name="textEdit">
                        <xsl:with-param name="id" select="'zoneName'" />
                        <xsl:with-param name="value" select="'#zoneName#'" />
                        <xsl:with-param name="satteliteText" select="$locale/data[@name='name']/value" />
                        <xsl:with-param name="multiline" select="'true'" />
                        <xsl:with-param name="rows" select="'1'" />
                        <xsl:with-param name="required" select="'true'" />
                        <xsl:with-param name="style" select="'height:25px;'" />
                    </xsl:call-template>
                </div>
                <div style="padding:5px;">
                    <xsl:call-template name="textEdit">
                        <xsl:with-param name="id" select="'zoneDescription'" />
                        <xsl:with-param name="value" select="'#zoneDescription#'" />
                        <xsl:with-param name="multiline" select="'true'" />
                        <xsl:with-param name="satteliteText" select="$locale/data[@name='description']/value" />
                        <!--xsl:with-param name="rows" select="'3'" /-->
                        <xsl:with-param name="style" select="'height:60px;'" />
                    </xsl:call-template>
                </div>
                <div style="width:100%;padding:5px;">
                    <xsl:call-template name="colorPick">
                        <xsl:with-param name="colorPickerContainerId" select="'colorPicker'" />
                    </xsl:call-template>
                </div>
                <div id="errorGeozone"></div>
            </div>
            <!--<xsl:if test="/response/systemRights[right = 'SecurityAdministration']">
              <div style="padding:5px;">
                <input type="checkbox" name="allOperators" id="allOperators" style="width:15px;margin-right:5px;" />
                <xsl:value-of select="$locale/data[@name='forAllOperators']/value" />
              </div>
            </xsl:if>-->
            <div style="width:100%;margin-bottom:5px;">
                <div style="padding:5px;">
                    <button type="button" class="btn" data-geozone-save="">
                        <span></span>
                        <xsl:value-of select="$locale/data[@name='save']/value" />
                    </button>
                    <button type="button" class="btn" data-geozone-cancel="" >
                        <xsl:value-of select="$locale/data[@name='cancel']/value" />
                    </button>
                </div>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>


<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="page.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
