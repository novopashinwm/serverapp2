<?xml version='1.0'?>
<!DOCTYPE xml [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="common.xsl" />
	<xsl:import href="uielements.xsl" />
	<xsl:import href="dateTime.xsl" />
  <!--xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="no" /-->
  <xsl:output method="xml" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="no" />


<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//@template = 'getVehicles'">
			<xsl:apply-templates select="." mode="getVehicles" />
		</xsl:when>
	</xsl:choose>
</xsl:template>

<!--
	AS: шаблон обработки данных об объектах слежения
	Рендерим список в HTML разметке, координаты - как обычно
-->
<xsl:template match="response" mode="getVehicles">
	<!--xsl:variable name="vehicles">
		<xsl:choose>
			<xsl:when test="./@showMode = 'online'">
				<xsl:copy-of select="./vehicles/vehicle[@old != 'true']" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="./vehicles/vehicle" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable-->
  <response selected="{./@selected}">
		<div id="objectList">
			<xsl:choose>
				<xsl:when test="./@showMode = 'online'">
					<xsl:apply-templates select="./vehicles/vehicle[@old != 'true']" mode="list">
						<xsl:sort select="number(./@ctrlNumber)" order="ascending" />
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="./vehicles/vehicle" mode="list">
						<xsl:sort select="number(./@ctrlNumber)" order="ascending" />
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<splitter />
    <div>
			<!--xsl:apply-templates select="./vehicles/vehicle[@old != 'true']" mode="map" /-->
      <!--xsl:choose>
				<xsl:when test="./@showMode = 'online'">
					<xsl:apply-templates select="./vehicles/vehicle[@old != 'true']" mode="map" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="./vehicles/vehicle" mode="map" />
				</xsl:otherwise>
			</xsl:choose-->
		</div>
		<!--xsl:if test="(./vehicles[1]/@selected &gt; 0) and (./vehicles[1]/vehicle[@id = /response/vehicles[1]/@selected]/@validPosition = 'true')"-->
		<xsl:if test="(./vehicles[1]/@selected &gt; 0)">
			<splitter />
			<xsl:element name="selectedPoint">
				<xsl:attribute name="longitude"><xsl:value-of select="./vehicles[1]/vehicle[@id = /response/vehicles[1]/@selected]/@longitude" /></xsl:attribute>
				<xsl:attribute name="latitude"><xsl:value-of select="./vehicles[1]/vehicle[@id = /response/vehicles[1]/@selected]/@latitude" /></xsl:attribute>
			</xsl:element>
		</xsl:if>
    
    <splitter />
    <omap>
      <xsl:choose>
        <xsl:when test="./@showMode = 'online'">
          <xsl:apply-templates select="./vehicles/vehicle[@old != 'true']" mode="mapG" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="./vehicles/vehicle" mode="mapG" />
        </xsl:otherwise>
      </xsl:choose>
    </omap>
  </response>
</xsl:template>

<xsl:template match="vehicle" mode="mapG">
  <monitoringObject>
    <raw>
      <xsl:apply-templates select="." mode="raw" />
    </raw>
  </monitoringObject>
</xsl:template>

<xsl:template match="vehicle" mode="raw">
  <xsl:copy-of select="." />
</xsl:template>


  <xsl:template match="vehicle" mode="list">
    <!--div id="objectList_{./@id}" class="objectCardList" style="width:expression((navigator.userAgent.indexOf('MSIE') >= 0) ? '100%' : '');padding-top:5px;padding-bottom:5px;border-bottom:1px solid #ccc;"-->
    <div id="objectList_{./@id}" vName="{./@GarageNumber}" class="objectCardList" style="width:98%;border-bottom:1px solid #ccc;">
      <xsl:if test="/response/vehicles[1]/@selected = ./@id">
        <!--
			AS: если это выделенный объект, установим ему CSS класс selected2 (цвет фона светло-зеленый или типа того)
			-->
        <xsl:attribute name="class">selected2</xsl:attribute>
      </xsl:if>

      <!--xsl:variable name="imageUrl">
			<xsl:apply-templates select="." mode="imageUrlList" />
		</xsl:variable-->

      <div style="float:left; width:10%;padding-left:5px;">
        <span class="toggle" onclick="Monitoring.centerObject({./@id}, event, {./@longitude}, {./@latitude},'{./@GarageNumber}')">
          <xsl:choose>
            <xsl:when test="./@validPosition = 'true'">
              <!--xsl:call-template name="formatImage">
							<xsl:with-param name="browser" select="/response/@browser" />
							<xsl:with-param name="width" select="'32'" />
							<xsl:with-param name="height" select="'32'" />
							<xsl:with-param name="src" select="$imageUrl" />
						</xsl:call-template-->
              <span>
                <xsl:apply-templates select="." mode="imageUrlList" />
              </span>
              <!--span><xsl:apply-templates select="." mode="imageUrlMap2" /></span-->
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="formatImage">
                <xsl:with-param name="browser" select="/response/@browser" />
                <xsl:with-param name="width" select="'18'" />
                <xsl:with-param name="height" select="'18'" />
                <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/off.png')" />
                <xsl:with-param name="title" select="$locale/data[@name='noSignal']/value" />
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </span>
      </div>
      <div style="float:left;width:88%">
        <div style="width:100%;">
          <div style="float:left;width:45%;">
            <span class="toggle underlined" onclick="Monitoring.centerObject({./@id}, event, {./@longitude}, {./@latitude}, '{./@GarageNumber}')">
              <xsl:if test="./@validPosition = 'false'">
                <xsl:attribute name="class"></xsl:attribute>
                <xsl:attribute name="onclick"></xsl:attribute>
              </xsl:if>
              <xsl:value-of select="./@GarageNumber" />
              <!--xsl:value-of select="./@ctrlNumber" /-->
            </span>
            &#160;<xsl:text />
            (<span title="{$locale/data[@name='controllerNumber']/value}">
              <xsl:value-of select="./@ctrlNumber" />
            </span>)
            <xsl:text>&#160;</xsl:text>
          </div>
          <div style="float:left;width:49%;">
            <!--xsl:value-of select="./@Date" /-->
            <!--xsl:call-template name="time-width-day">
						<xsl:with-param name="current-date" select="/response/@serverDate" />
						<xsl:with-param name="format-date" select="./@Date2" />
						<xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
						<xsl:with-param name="lang" select="/response/@language" />
					</xsl:call-template-->
            <span class="utcDate">
              <xsl:value-of select="./@Date2" />
            </span>
          </div>
          <xsl:call-template name="clear" />
        </div>

        <div style="width:100%;clear:both;padding-left:10px;">
          <div style="float:left;width:51%;padding-top:2px;">
            <xsl:variable name="trackSrc">
              <xsl:value-of select="/response/@ApplicationPath" />/img/track2.png
            </xsl:variable>
            <a href="{/response/@ApplicationPath}/objects.aspx?a=journal&amp;id={./@id}">
              <span class="toggle">
                <xsl:call-template name="formatImage">
                  <xsl:with-param name="browser" select="/response/@browser" />
                  <xsl:with-param name="width" select="'16'" />
                  <xsl:with-param name="height" select="'16'" />
                  <xsl:with-param name="src" select="$trackSrc" />
                  <xsl:with-param name="title" select="$locale/data[@name='path']/value" />
                </xsl:call-template>
              </span>
              <xsl:value-of select="$locale/data[@name='path']/value" />
            </a>
            &#160;<xsl:text />
          </div>
          <div style="float:left;width:49%">
            <xsl:if test="./@validPosition = 'true'">
              <xsl:variable name="ignitionValue">
                <xsl:choose>
                  <xsl:when test="./@ignition = 0">off</xsl:when>
                  <xsl:otherwise>on</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="ignitionSrc">
                <xsl:value-of select="/response/@ApplicationPath" />/img/infocard/ign-<xsl:value-of select="$ignitionValue" />.png
              </xsl:variable>
              <xsl:call-template name="formatImage">
                <xsl:with-param name="browser" select="/response/@browser" />
                <xsl:with-param name="width" select="'16'" />
                <xsl:with-param name="height" select="'16'" />
                <xsl:with-param name="src" select="$ignitionSrc" />
              </xsl:call-template>
              <xsl:text> </xsl:text>
              <xsl:value-of select="$locale/data[@name='ignition']/value" />
              <xsl:text> </xsl:text>
              <xsl:choose>
                <xsl:when test="$ignitionValue = 'on'">
                  <xsl:value-of select="$locale/data[@name='on1']/value" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$locale/data[@name='off1']/value" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:if>
            &#160;<xsl:text />
          </div>
          <div style="float:left;width:51%;padding-top:2px;">
            <xsl:variable name="propSrc">
              <xsl:value-of select="/response/@ApplicationPath" />/img/properties1.png
            </xsl:variable>
            <a href="{/response/@ApplicationPath}/objects.aspx?id={./@id}">
              <span class="toggle">
                <xsl:call-template name="formatImage">
                  <xsl:with-param name="browser" select="/response/@browser" />
                  <xsl:with-param name="width" select="'16'" />
                  <xsl:with-param name="height" select="'15'" />
                  <xsl:with-param name="src" select="$propSrc" />
                  <xsl:with-param name="title" select="$locale/data[@name='properties']/value" />
                </xsl:call-template>
              </span>
              &#160;<xsl:text />
              <xsl:value-of select="$locale/data[@name='properties']/value" />
            </a>
          </div>
          <div style="float:left;width:49%">
            <xsl:if test="./@validPosition = 'true'">
              <xsl:value-of select="./@currSpeed" />
              &#160;<xsl:text />
              <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
            </xsl:if>
            &#160;<xsl:text />
          </div>
          <xsl:if test="./@ctrlNumber = '55'">
            <div style="float:left;width:51%">
              <span class="toggle underlined" onclick="jQuery.sms.showSendDialog(event, '{./@GarageNumber}', {./@id})">
                <img src="{/response/@ApplicationPath}/img/msg.gif" title="{$locale/data[@name='sendMessage']/value}" />
                &#160;<xsl:text />
                <xsl:value-of select="$locale/data[@name='sendMessage']/value" />
              </span>
            </div>
            <div style="float:left;width:49%">
              <!--xsl:if test="./@validPosition = 'true'">
							<xsl:value-of select="./@address" />
						</xsl:if-->
              &#160;<xsl:text />
            </div>
          </xsl:if>
        </div>
        <div style="clear:both;width:100%;padding-left:10px;">
          <xsl:if test="./@validPosition = 'true'">
            <xsl:value-of select="./@address" />
          </xsl:if>
        </div>

        <!-- AS: статус объекта -->
        <div style="clear:both;width:100%;padding-left:10px;">
          <xsl:apply-templates select="." mode="status" />
        </div>

      </div>
      <div class="clear">
        &#160;<xsl:text/>
      </div>
    </div>
    <!--div class="divider" style="clear:both"><xsl:text> </xsl:text></div-->
  </xsl:template>  
  
<xsl:template match="vehicle" mode="listG">
	<!--div id="objectList_{./@id}" class="objectCardList" style="width:expression((navigator.userAgent.indexOf('MSIE') >= 0) ? '100%' : '');padding-top:5px;padding-bottom:5px;border-bottom:1px solid #ccc;"-->
	<div id="objectList_{./@id}" class="objectCardList" style="width:98%;border-bottom:1px solid #ccc;">
		<xsl:if test="/response/vehicles[1]/@selected = ./@id">
			<!--
			AS: если это выделенный объект, установим ему CSS класс selected2 (цвет фона светло-зеленый или типа того)
			-->
			<xsl:attribute name="class">selected2</xsl:attribute>
		</xsl:if>

		<!--xsl:variable name="imageUrl">
			<xsl:apply-templates select="." mode="imageUrlList" />
		</xsl:variable-->

		<div style="float:left; width:10%;padding-left:5px;">
			<span class="toggle" onclick="Monitoring.centerObject({./@id}, event, {./@longitude}, {./@latitude},'{./@GarageNumber}')">
				<xsl:choose>
					<xsl:when test="./@validPosition = 'true'">
						<!--xsl:call-template name="formatImage">
							<xsl:with-param name="browser" select="/response/@browser" />
							<xsl:with-param name="width" select="'32'" />
							<xsl:with-param name="height" select="'32'" />
							<xsl:with-param name="src" select="$imageUrl" />
						</xsl:call-template-->
						<span><xsl:apply-templates select="." mode="imageUrlList" /></span>
						<!--span><xsl:apply-templates select="." mode="imageUrlMap2" /></span-->
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="formatImage">
							<xsl:with-param name="browser" select="/response/@browser" />
							<xsl:with-param name="width" select="'18'" />
							<xsl:with-param name="height" select="'18'" />
							<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/off.png')" />
							<xsl:with-param name="title" select="$locale/data[@name='noSignal']/value" />
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</div>
		<div style="float:left;width:88%">
			<div style="width:100%;">
				<div style="float:left;width:55%;">
					<span class="toggle underlined" onclick="Monitoring.centerObject({./@id}, event, {./@longitude}, {./@latitude}, '{./@GarageNumber}')">
						<xsl:if test="./@validPosition = 'false'">
							<xsl:attribute name="class"></xsl:attribute>
							<xsl:attribute name="onclick"></xsl:attribute>
						</xsl:if>
						<xsl:value-of select="./@GarageNumber" />
						<!--xsl:value-of select="./@ctrlNumber" /-->
					</span>
					&#160;<xsl:text />
					(<span title="{$locale/data[@name='controllerNumber']/value}"><xsl:value-of select="./@ctrlNumber" /></span>)
					<xsl:text>&#160;</xsl:text>
				</div>
				<div style="float:left;width:49%;">
					<!--xsl:value-of select="./@Date" /-->
					<!--xsl:call-template name="time-width-day">
						<xsl:with-param name="current-date" select="/response/@serverDate" />
						<xsl:with-param name="format-date" select="./@Date2" />
						<xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
						<xsl:with-param name="lang" select="/response/@language" />
					</xsl:call-template-->
					<span class="utcDate"><xsl:value-of select="./@Date2" /></span>
				</div>
				<xsl:call-template name="clear" />
			</div>
			
			<div style="width:100%;clear:both;padding-left:10px;">
				<div style="float:left;width:51%;padding-top:2px;">
					<xsl:variable name="trackSrc">
						<xsl:value-of select="/response/@ApplicationPath" />/img/track2.png
					</xsl:variable>
					<a href="{/response/@ApplicationPath}/objects.aspx?a=journal&amp;id={./@id}">
						<span class="toggle">
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'16'" />
								<xsl:with-param name="height" select="'16'" />
								<xsl:with-param name="src" select="$trackSrc" />
								<xsl:with-param name="title" select="$locale/data[@name='path']/value" />
							</xsl:call-template>
						</span>
						<xsl:value-of select="$locale/data[@name='path']/value" />
					</a>
					&#160;<xsl:text />
				</div>
				<div style="float:left;width:49%">
					<xsl:if test="./@validPosition = 'true'">
						<xsl:variable name="ignitionValue">
							<xsl:choose>
								<xsl:when test="./@ignition = 0">off</xsl:when>
								<xsl:otherwise>on</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="ignitionSrc">
							<xsl:value-of select="/response/@ApplicationPath" />/img/infocard/ign-<xsl:value-of select="$ignitionValue" />.png
						</xsl:variable>
						<xsl:call-template name="formatImage">
							<xsl:with-param name="browser" select="/response/@browser" />
							<xsl:with-param name="width" select="'16'" />
							<xsl:with-param name="height" select="'16'" />
							<xsl:with-param name="src" select="$ignitionSrc" />
						</xsl:call-template>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$locale/data[@name='ignition']/value" /><xsl:text> </xsl:text>
						<xsl:choose>
							<xsl:when test="$ignitionValue = 'on'">
								<xsl:value-of select="$locale/data[@name='on1']/value" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$locale/data[@name='off1']/value" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					&#160;<xsl:text />
				</div>
				<div style="float:left;width:51%;padding-top:2px;">
					<xsl:variable name="propSrc">
						<xsl:value-of select="/response/@ApplicationPath" />/img/properties1.png
					</xsl:variable>
					<a href="{/response/@ApplicationPath}/objects.aspx?id={./@id}">
						<span class="toggle">
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'16'" />
								<xsl:with-param name="height" select="'15'" />
								<xsl:with-param name="src" select="$propSrc" />
								<xsl:with-param name="title" select="$locale/data[@name='properties']/value" />
							</xsl:call-template>
						</span>
						&#160;<xsl:text />
						<xsl:value-of select="$locale/data[@name='properties']/value" />
					</a>
				</div>
				<div style="float:left;width:49%">
					<xsl:if test="./@validPosition = 'true'">
						<xsl:value-of select="./@currSpeed" />
						&#160;<xsl:text />
						<xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
					</xsl:if>
					&#160;<xsl:text />
				</div>
				<xsl:if test="./@ctrlNumber = '55'">
					<div style="float:left;width:51%">
						<span class="toggle underlined" onclick="jQuery.sms.showSendDialog(event, '{./@GarageNumber}', {./@id})">
							<img src="{/response/@ApplicationPath}/img/msg.gif" title="{$locale/data[@name='sendMessage']/value}" />
							&#160;<xsl:text />
							<xsl:value-of select="$locale/data[@name='sendMessage']/value" />
						</span>
					</div>
					<div style="float:left;width:49%">
						<!--xsl:if test="./@validPosition = 'true'">
							<xsl:value-of select="./@address" />
						</xsl:if-->
						&#160;<xsl:text />
					</div>
				</xsl:if>
			</div>
			<div style="clear:both;width:100%;padding-left:10px;">
				<xsl:if test="./@validPosition = 'true'">
					<xsl:value-of select="./@address" />
				</xsl:if>
			</div>

			<!-- AS: статус объекта -->
			<div style="clear:both;width:100%;padding-left:10px;">
				<xsl:apply-templates select="." mode="status" />
			</div>
			
		</div>
		<div class="clear">&#160;<xsl:text/></div>
	</div>
	<!--div class="divider" style="clear:both"><xsl:text> </xsl:text></div-->
</xsl:template>


<!-- AS: шаблон для отображения объекта слежения в списке ближайших найденных объектов -->
<xsl:template match="vehicle" mode="listLite">
	<div xmlns="http://www.w3.org/1999/xhtml">
		<span class="toggle" onclick="Monitoring.centerObject({./@id}, event, {./@longitude}, {./@latitude},'{./@GarageNumber}')">
			<xsl:choose>
				<xsl:when test="./@validPosition = 'true'">
					<span>
						<xsl:apply-templates select="." mode="imageUrlList">
							<xsl:with-param name="showAdditionalGraphic" select="false()" />
						</xsl:apply-templates>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="/response/@browser" />
						<xsl:with-param name="width" select="'18'" />
						<xsl:with-param name="height" select="'18'" />
						<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/off.png')" />
						<xsl:with-param name="title" select="$locale/data[@name='noSignal']/value" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</span>
		<span class="toggle underlined" onclick="Monitoring.centerObjectAndShowRoute({./@id}, event, {./@longitude}, {./@latitude}, '{./@GarageNumber}')">
			<xsl:if test="./@validPosition = 'false'">
				<xsl:attribute name="class"></xsl:attribute>
				<xsl:attribute name="onclick"></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="./@GarageNumber" />
		</span>
		&#160;<xsl:text />
		(<span title="{$locale/data[@name='controllerNumber']/value}"><xsl:value-of select="./@ctrlNumber" /></span>)
	</div>
</xsl:template>


<xsl:template match="vehicle" mode="map">
	<!--xsl:if test="./@validPosition = 'true'"-->
	<xsl:variable name="xcorr">
		<xsl:choose>
			<xsl:when test="./@validPosition = 'true'"><xsl:value-of select="./@x" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="./@xcp" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="ycorr">
		<xsl:choose>
			<xsl:when test="./@validPosition = 'true'"><xsl:value-of select="./@y" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="./@ycp" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

		<xsl:variable name="ignitionImageUrl">
			<xsl:choose>
				<xsl:when test="./@ignition = 1">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/infocard/ign-on.png')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/infocard/ign-off.png')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="numberImageUrl">
			<xsl:value-of select="concat(/response/@ApplicationPath, '/img/clouds/object-number2.png')" />
		</xsl:variable>
		<xsl:variable name="detailsCloudImageUrl">
			<xsl:value-of select="concat(/response/@ApplicationPath, '/img/clouds/object-details2.png')" />
		</xsl:variable>

		<!--div>
			<span><xsl:value-of select="$xcorr" /></span> <xsl:text /><span><xsl:value-of select="$ycorr" /></span>
		</div-->
		<xsl:variable name="objectStyle">
			<xsl:variable name="commonStyle">
				position:absolute; left:<xsl:value-of select="$xcorr - 9" />px; top:<xsl:value-of select="$ycorr - 9" />px; width:32px; height:32px;
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="../@selected = ./@id">
					<xsl:value-of select="concat($commonStyle, 'z-index:10000;')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($commonStyle, 'z-index:2000;')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div class="mapObject" style="{$objectStyle}" id="objectmap_{./@id}">
			<span class="toggle" onclick="jQuery.showObjectCard({./@id}, event, {./@longitude}, {./@latitude}, '{./@GarageNumber}')"><xsl:apply-templates select="." mode="imageUrlMap2" /></span>
			<div class="objectNumber toggle" id="monitoringObjectNumber_{./@id}" onclick="jQuery.showObjectCard({./@id}, event, {./@longitude}, {./@latitude}, '{./@GarageNumber}')">
				<xsl:choose>
					<xsl:when test="contains(/response/@browser, 'IE5') or contains(/response/@browser, 'IE6') or contains(/response/@browser, 'IE7')">
						<xsl:attribute name="style">
							filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$numberImageUrl" />', sizingMethod='scale')";width:85px; height:27px;
							<xsl:if test="../@selected = ./@id">display:none;</xsl:if>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="style">
							background-image:url(<xsl:value-of select="$numberImageUrl" />);background-repeat:no-repeat;background-position:center center;;width:85px; height:27px;
							<xsl:if test="../@selected = ./@id">display:none;</xsl:if>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<!--span style="padding:5px"><xsl:value-of select="./@ctrlNumber" /></span-->
				<span style="padding:5px; color: #000;"><xsl:value-of select="./@GarageNumber" /></span>
			</div>

			<xsl:variable name="cardStyle">
				<xsl:variable name="tmpS">
					<xsl:if test="not(contains(/response/@browser, 'IE5')) and not(contains(/response/@browser, 'IE6')) and not(contains(/response/@browser, 'IE7'))">
						background-image:url(<xsl:value-of select="$detailsCloudImageUrl" />);background-repeat:no-repeat;background-position:center center;width:348px;height:164px;
					</xsl:if>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="../@selected = ./@id">
						<xsl:value-of select="concat($tmpS, 'display:block;')" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat($tmpS, 'display:none;')" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<div class="objectCard" style="{$cardStyle}" id="trackingObjectCard_{./@id}">
				<xsl:choose>
					<xsl:when test="contains(/response/@browser, 'IE5') or contains(/response/@browser, 'IE6') or contains(/response/@browser, 'IE7')">
						<div>
							<xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$detailsCloudImageUrl" />', sizingMethod='scale')";width:348px; height:164px;</xsl:attribute>
						</div>
					</xsl:when>
				</xsl:choose>
				<div>
					<xsl:if test="contains(/response/@browser, 'IE5') or contains(/response/@browser, 'IE6') or contains(/response/@browser, 'IE7')">
						<xsl:attribute name="style">position:absolute;left:0px;top:10px;width:348px; height:164px;</xsl:attribute>
					</xsl:if>
					<div style="width:100%;height:30px;">
						<div style="float:left;width:165px;padding-left:5px;padding-top:5px">
							<span class="white">
								<b><xsl:value-of select="./@GarageNumber" /></b>
								&#160;<xsl:text />(<xsl:value-of select="./@ctrlNumber" />)
							</span>
						</div>
						<div style="float:left;width:150px" class="right-side white">
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'16'" />
								<xsl:with-param name="height" select="'18'" />
								<xsl:with-param name="src" select="$ignitionImageUrl" />
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:choose>
								<xsl:when test="./@ignition = 1">
									<xsl:value-of select="$locale/data[@name='on2']/value" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$locale/data[@name='off2']/value" />
								</xsl:otherwise>
							</xsl:choose>
							<span class="divider"><xsl:text> </xsl:text></span>
							<span class="closeWin" onclick="jQuery.closeObjectCard('{./@id}')"><span><xsl:text>&#160;x</xsl:text></span></span>
							<!--span class="closeWin" onclick="jQuery.closeObjectCard('trackingObjectCard_{./@id}')"><span><xsl:text>&#160;x</xsl:text></span></span-->
						</div>
					</div>
					<div style="clear:both;padding-left:5px;padding-top:2px;padding-bottom:0;margin:0;width:100%;height:70px;overflow:hidden;">
						<div style="float:left;width:80px">
							<!--xsl:call-template name="getTimeOnly">
								<xsl:with-param name="dateAndTime" select="./@Date" />
							</xsl:call-template-->
							<!--xsl:call-template name="time-width-day">
								<xsl:with-param name="current-date" select="/response/@serverDate" />
								<xsl:with-param name="format-date" select="./@Date2" />
								<xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
								<xsl:with-param name="lang" select="/response/@language" />
							</xsl:call-template-->
							<span class="utcDate"><xsl:value-of select="./@Date2" /></span>
						</div>
						<div style="float:left;width:235px">
							<div class="address" id="address_{./@id}">
								<xsl:value-of select="./@address" />
							</div>
							<!--div class="divider"><xsl:text> </xsl:text></div-->
							<div>
								<xsl:value-of select="$locale/data[@name='speed']/value" />: <xsl:value-of select="./@currSpeed" />
								<xsl:text> </xsl:text>
								<xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
							</div>
							<div>
								<xsl:apply-templates select="." mode="status" />
							</div>
						</div>
						<div class="divider33 clear"><xsl:text> </xsl:text></div>
					</div>
					<xsl:apply-templates select="." mode="cardBottomOperations" />
				</div>
			</div>
		</div>
	<!--/xsl:if-->
</xsl:template>

<xsl:template match="vehicle" mode="cardBottomOperations">
	<div style="clear:both;padding:5px;width:100%">
		<div style="float:left;width:50px">
			<xsl:text>&#160;</xsl:text>
		</div>
		<div style="float:left;width:265px">
			<a href="{/response/@ApplicationPath}/objects.aspx?a=journal&amp;id={./@id}" class="white"><xsl:value-of select="$locale/data[@name='path']/value" /></a>
			<xsl:text> </xsl:text>
			<xsl:if test="./@ctrlNumber = '55'">
				<span class="toggle underlined white" onclick="jQuery.sms.showSendDialog(event, '{./@GarageNumber}', {./@id})"><xsl:value-of select="$locale/data[@name='sendMessage']/value" /></span>
				<xsl:text> </xsl:text>
			</xsl:if>
			<a href="{/response/@ApplicationPath}/objects.aspx?id={./@id}" class="white"><xsl:value-of select="$locale/data[@name='properties']/value" /></a>
			<xsl:text> </xsl:text>
			<xsl:choose>
				<xsl:when test="//@useDialer = 'true' and //@loggedUser != 'guest'">
					<span class="toggle white" style="margin-top:5px;" onclick="PhoneDialer.call('{./ControllerPhoneNumber}')">
						<xsl:call-template name="formatImage">
							<xsl:with-param name="browser" select="//@browser" />
							<xsl:with-param name="height" select="18" />
							<xsl:with-param name="width" select="18" />
							<xsl:with-param name="src" select="concat(//@ApplicationPath, '/img/infocard/call.png')" />
							<xsl:with-param name="title" select="$locale/data[@name='call']/value" />
						</xsl:call-template>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="//@browser" />
						<xsl:with-param name="height" select="18" />
						<xsl:with-param name="width" select="18" />
						<xsl:with-param name="src" select="concat(//@ApplicationPath, '/img/infocard/call-off.png')" />
						<xsl:with-param name="title" select="$locale/data[@name='functionDisabled']/value" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</div>
	<div class="clear"><xsl:text> </xsl:text></div>
</xsl:template>

<!-- Шаблон генерит URL картинки -->
<xsl:template match="vehicle" mode="imageUrlMap">
	<!--xsl:value-of select="./@angle" /-->
	<xsl:variable name="correctAngle">
		<xsl:variable name="tmpAngle" select="number(./@angle)" />
		<xsl:choose>
			<xsl:when test="$tmpAngle &lt; 18">
				<xsl:value-of select="18 + $tmpAngle" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="-(18 - $tmpAngle)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="./@status = 0">
			<xsl:choose>
				
				<!-- AS: скорость от 0 до 5 кмч -->
				<xsl:when test="./@currSpeed &gt; 0 and ./@currSpeed &lt; 5">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/pause.png')" />
				</xsl:when>

				<!-- AS: скорость 0 -->
				<xsl:when test="./@currSpeed = 0">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/stopped.png')" />
				</xsl:when>

				<!-- AS: скорость выше 5 кмч -->
				<xsl:otherwise>
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/online/rest', $correctAngle, '.png')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="vehicle" mode="imageUrlMap2">
	<xsl:param name="noArrows" select="false()" />

	<xsl:variable name="browser" select="//@browser" />
	<xsl:choose>
		<xsl:when test="./settings and ./settings/icon">
			<xsl:variable name="correctAngle">
				<xsl:variable name="tmpAngle" select="number(./@angle)" />
				<xsl:choose>
					<xsl:when test="$tmpAngle &lt; 180">
						<xsl:value-of select="180 + $tmpAngle" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="-(180 - $tmpAngle)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="iconUrl">
				<!--xsl:if test="not(contains(./settings/icon, /response/@ApplicationPath))">
					<xsl:value-of select="/response/@ApplicationPath" />
				</xsl:if-->
				<xsl:value-of select="./settings/icon" />
			</xsl:variable>
			<span>
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="'32'" />
					<xsl:with-param name="height" select="'32'" />
					<xsl:with-param name="src" select="$iconUrl" />
				</xsl:call-template>
				
				<!-- AS: курс объекта -->
				<xsl:choose>
					<xsl:when test="./@currSpeed &gt; 0 and not($noArrows)">
						<span style="position:absolute;left:-20px;top:-20px;border:0px solid #000;z-index:-1" class="directionArrow">
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'70'" />
								<xsl:with-param name="height" select="'70'" />
								<!--xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/userdata/arrows/dir', $correctAngle, '.png')" /-->
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/userdata/bluearrows/arrow', $correctAngle, '.png')" />
							</xsl:call-template>
						</span>
					</xsl:when>
				</xsl:choose>

				<xsl:choose>
					<xsl:when test="not(contains($browser, 'IE5') or contains($browser, 'IE6') or contains($browser, 'IE7'))">
						<xsl:choose>
							<xsl:when test="./@validPosition = true()">
								<xsl:choose>
									<xsl:when test="./@status = 0">
										<xsl:choose>
											<xsl:when test="./@currSpeed = 0">
												<span style="position:absolute;left:-5px;top:-5px;border:0px solid #000;z-index:-1">
													<xsl:call-template name="formatImage">
														<xsl:with-param name="browser" select="/response/@browser" />
														<xsl:with-param name="width" select="'20'" />
														<xsl:with-param name="height" select="'20'" />
														<!--xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/userdata/arrows/dir', $correctAngle, '.png')" /-->
														<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/small_parking.png')" />
													</xsl:call-template>
												</span>
											</xsl:when>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:apply-templates select="." mode="status">
											<xsl:with-param name="onmap" select="true()" />
										</xsl:apply-templates>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<span style="position:absolute;left:-5px;top:-5px;border:0px solid #000;z-index:-1">
									<xsl:call-template name="formatImage">
										<xsl:with-param name="browser" select="/response/@browser" />
										<xsl:with-param name="width" select="'20'" />
										<xsl:with-param name="height" select="'20'" />
										<!--xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/userdata/arrows/dir', $correctAngle, '.png')" /-->
										<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/off.png')" />
									</xsl:call-template>
								</span>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
				<!--xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="'32'" />
					<xsl:with-param name="height" select="'32'" />
					<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/rest.png')" />
				</xsl:call-template-->
			</span>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="correctAngle">
				<xsl:variable name="tmpAngle" select="number(./@angle2)" />
				<xsl:choose>
					<xsl:when test="$tmpAngle &lt; 18">
						<xsl:value-of select="18 + $tmpAngle" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="-(18 - $tmpAngle)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="./@validPosition = 'false'">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="/response/@browser" />
						<xsl:with-param name="width" select="'18'" />
						<xsl:with-param name="height" select="'18'" />
						<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/off.png')" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="./@status = 0">
					<xsl:choose>
						<!-- AS: скорость от 0 до 5 кмч -->
						<xsl:when test="./@currSpeed &gt; 0 and ./@currSpeed &lt; 5">
							<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/pause.png')" /-->
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'32'" />
								<xsl:with-param name="height" select="'32'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/rest.png')" />
							</xsl:call-template>
						</xsl:when>

						<!-- AS: скорость 0 -->
						<xsl:when test="./@currSpeed = 0">
							<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/stopped.png')" /-->
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'32'" />
								<xsl:with-param name="height" select="'32'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/parking3.png')" />
								<!--xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/onguard.png')" /-->
								
							</xsl:call-template>
						</xsl:when>

						<!-- AS: скорость выше 5 кмч -->
						<xsl:otherwise>
							<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/online/rest', $correctAngle, '.png')" /-->
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'32'" />
								<xsl:with-param name="height" select="'32'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/online/rest', $correctAngle, '.png')" />
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="./@status = 1">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="/response/@browser" />
						<xsl:with-param name="width" select="'32'" />
						<xsl:with-param name="height" select="'32'" />
						<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/online/rest', $correctAngle, '.png')" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="./@status = 2">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="/response/@browser" />
						<xsl:with-param name="width" select="'32'" />
						<xsl:with-param name="height" select="'32'" />
						<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/secureW.png')" />
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="./@status = 4">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="/response/@browser" />
						<xsl:with-param name="width" select="'32'" />
						<xsl:with-param name="height" select="'32'" />
						<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/onguard.png')" />
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: картинка в списке -->
<xsl:template match="vehicle" mode="imageUrlList">
	<xsl:param name="showAdditionalGraphic" select="true()" />
	<xsl:variable name="correctAngle">
		<xsl:variable name="tmpAngle" select="number(./@angle2)" />
		<xsl:choose>
			<xsl:when test="$tmpAngle &lt; 18">
				<xsl:value-of select="18 + $tmpAngle" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="-(18 - $tmpAngle)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="browser" select="//@browser" />
	<xsl:choose>
		<xsl:when test="./settings and ./settings/icon">
			<!--xsl:value-of select="./settings/icon" /-->
			<xsl:variable name="iconUrl">
				<!--xsl:if test="not(contains(./settings/icon, /response/@ApplicationPath))">
					<xsl:value-of select="/response/@ApplicationPath" />
				</xsl:if-->
				<xsl:value-of select="./settings/icon" />
			</xsl:variable>
			<span>
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="'32'" />
					<xsl:with-param name="height" select="'32'" />
					<xsl:with-param name="src" select="$iconUrl" />
				</xsl:call-template>
				<xsl:choose>
					<xsl:when test="not(contains($browser, 'IE5') or contains($browser, 'IE6') or contains($browser, 'IE7')) and $showAdditionalGraphic = true()">
						<xsl:variable name="statusFieldName" select="concat('status', ./@status)" />
						<xsl:variable name="statusValue" select="$locale/data[@name=$statusFieldName]/value" />
						<xsl:choose>
							<xsl:when test="./@status = 0">
								<xsl:choose>
									<xsl:when test="./@currSpeed = 0">
										<span style="position:relative;left:-5px;top:-40px;border:0px solid #000;z-index:1">
											<xsl:call-template name="formatImage">
												<xsl:with-param name="browser" select="/response/@browser" />
												<xsl:with-param name="width" select="'20'" />
												<xsl:with-param name="height" select="'20'" />
												<!--xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/userdata/arrows/dir', $correctAngle, '.png')" /-->
												<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/small_parking.png')" />
											</xsl:call-template>
										</span>
									</xsl:when>
								</xsl:choose>
							</xsl:when>
							<!--xsl:when test="./@status = 16">
								<span style="position:relative;left:-5px;top:-40px;border:0px solid #000;z-index:1">
									<xsl:call-template name="formatImage">
										<xsl:with-param name="browser" select="/response/@browser" />
										<xsl:with-param name="width" select="'20'" />
										<xsl:with-param name="height" select="'20'" />
										<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/call-small.png')" />
										<xsl:with-param name="title" select="$statusValue" />
									</xsl:call-template>
								</span>
							</xsl:when>
							<xsl:when test="./@status = 32">
								<span style="position:relative;left:-5px;top:-40px;border:0px solid #000;z-index:1">
									<xsl:call-template name="formatImage">
										<xsl:with-param name="browser" select="/response/@browser" />
										<xsl:with-param name="width" select="'20'" />
										<xsl:with-param name="height" select="'20'" />
										<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/icons/objects2/emergency.png')" />
										<xsl:with-param name="title" select="$statusValue" />
									</xsl:call-template>
								</span>
							</xsl:when>
							<xsl:otherwise>
								<span style="position:relative;left:-5px;top:-40px;border:0px solid #000;z-index:1">
									<xsl:call-template name="formatImage">
										<xsl:with-param name="browser" select="/response/@browser" />
										<xsl:with-param name="width" select="'20'" />
										<xsl:with-param name="height" select="'20'" />
										<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/attention.png')" />
									</xsl:call-template>
								</span>
							</xsl:otherwise-->
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</span>
			<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/rest.png')" /-->
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="imgUrl">
				<xsl:choose>
					<xsl:when test="./@status = 0">
						<xsl:choose>
							<!-- AS: скорость от 0 до 5 кмч -->
							<xsl:when test="./@currSpeed &gt; 0 and ./@currSpeed &lt; 5">
								<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/pause.png')" /-->
								<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/rest.png')" />
							</xsl:when>

							<!-- AS: скорость 0 -->
							<xsl:when test="./@currSpeed = 0">
								<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/stopped.png')" /-->
								<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/parking3.png')" />
							</xsl:when>

							<!-- AS: скорость выше 5 кмч -->
							<xsl:otherwise>
								<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/moving.png')" /-->
								<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/online/rest0.png')" /-->
								<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/online/rest', $correctAngle, '.png')" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/attention.png')" /-->
						<xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/attention.png')" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="formatImage">
				<xsl:with-param name="browser" select="/response/@browser" />
				<xsl:with-param name="width" select="'32'" />
				<xsl:with-param name="height" select="'32'" />
				<xsl:with-param name="src" select="$imgUrl" />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: генерит иконки для объекта в журнале движения -->
<xsl:template match="unit" mode="imageJournalUrl">
	<xsl:variable name="correctAngle">
		<xsl:variable name="tmpAngle" select="number(./@angle)" />
		<xsl:choose>
			<xsl:when test="$tmpAngle &lt; 18">
				<xsl:value-of select="18 + $tmpAngle" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="-(18 - $tmpAngle)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="./Status = 0">
			<xsl:choose>
				<xsl:when test="./Speed &gt; 0 and ./Speed &lt; 5">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_rest.png')" />
				</xsl:when>

				<xsl:when test="./Speed = 0">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_stop.png')" />
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/history/h_rest', $correctAngle, '.png')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="./Status = 1">
			<xsl:choose>
				<xsl:when test="./Speed &gt; 0 and ./Speed &lt; 5">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_rest.png')" />
				</xsl:when>

				<xsl:when test="./Speed = 0">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_stop.png')" />
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/history/h_rest', $correctAngle, '.png')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="2">
			<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_parkingW.png')" />
		</xsl:when>
		<xsl:when test="4">
			<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_parking.png')" />
		</xsl:when>
	</xsl:choose>
</xsl:template>


<!-- Шаблон генерит URL для выбранной точки в журнале движения объекта -->
<xsl:template match="unit" mode="imageJournalUrlSelected">
	<!--xsl:value-of select="./@angle" /-->
	<xsl:variable name="correctAngle">
		<xsl:variable name="tmpAngle" select="number(./@angle)" />
		<xsl:choose>
			<xsl:when test="$tmpAngle &lt; 18">
				<xsl:value-of select="18 + $tmpAngle" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="-(18 - $tmpAngle)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="./Status = 0">
			<xsl:choose>
				
				<!-- AS: скорость от 0 до 5 кмч -->
				<xsl:when test="./Speed &gt; 0 and ./@currSpeed &lt; 5">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_rest_s.png')" />
				</xsl:when>

				<!-- AS: скорость 0 -->
				<xsl:when test="./Speed = 0">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/objects2/h_stop_s.png')" />
				</xsl:when>

				<!-- AS: скорость выше 5 кмч -->
				<xsl:otherwise>
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/history/h_rest_s', $correctAngle, '.png')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<!-- AS: статус объекта -->
<xsl:template match="vehicle" mode="status">
	<xsl:param name="onmap" select="false()" />

	<xsl:if test="./@ctrlNumber = '55'">
		<xsl:variable name="statusStyle">
			<xsl:if test="$onmap = true()">position:absolute;left:-10px;top:0;</xsl:if>
		</xsl:variable>

		<xsl:variable name="statusFieldName" select="concat('status', ./@status)" />
		<xsl:variable name="statusValue" select="$locale/data[@name=$statusFieldName]/value" />
		<!--xsl:value-of select="$statusFieldName" /-->
		<xsl:choose>
			<xsl:when test="$statusValue">
				<xsl:choose>
					<xsl:when test="./@status = 0 or ./@status = 8">
						<!-- AS: свободен -->
						<span>
							<xsl:if test="string-length($statusStyle) &gt; 0">
								<xsl:attribute name="style"><xsl:value-of select="$statusStyle" /></xsl:attribute>
							</xsl:if>
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'20'" />
								<xsl:with-param name="height" select="'20'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/flagGreen.png')" />
								<xsl:with-param name="title" select="$statusValue" />
							</xsl:call-template>
						</span>
					</xsl:when>
					<xsl:when test="./@status = 16">
						<!-- AS: едет на вызов -->
						<span>
							<xsl:if test="string-length($statusStyle) &gt; 0">
								<xsl:attribute name="style"><xsl:value-of select="$statusStyle" /></xsl:attribute>
							</xsl:if>
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'20'" />
								<xsl:with-param name="height" select="'20'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/flagOrange.png')" />
								<xsl:with-param name="title" select="$statusValue" />
							</xsl:call-template>
						</span>
					</xsl:when>
					<xsl:when test="./@status = 32">
						<!-- На вызове -->
						<span>
							<xsl:if test="string-length($statusStyle) &gt; 0">
								<xsl:attribute name="style"><xsl:value-of select="$statusStyle" /></xsl:attribute>
							</xsl:if>
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="'20'" />
								<xsl:with-param name="height" select="'20'" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/object-car/flagRed.png')" />
								<xsl:with-param name="title" select="$statusValue" />
							</xsl:call-template>
						</span>
					</xsl:when>
				</xsl:choose>
				<xsl:text>&#160;</xsl:text>
				<span title="{$locale/data[@name='statusOfAnObject']/value}"><xsl:value-of select="$statusValue" /></span>
			</xsl:when>
			<xsl:otherwise><xsl:text>&#160;</xsl:text></xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>

<!-- Шаблон генерит URL для первой точки в журнале движения объекта -->
<xsl:template match="unit" mode="imageJournalUrlFirst">
	<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icong1.png')" />
</xsl:template>

<!-- Шаблон генерит URL для последней точки в журнале движения объекта -->
<xsl:template match="unit" mode="imageJournalUrlLast">
	<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icong2.png')" />
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\page\page.xmltrackingObject.xsl_1.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->