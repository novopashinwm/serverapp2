﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
  <!ENTITY amp2 "&amp;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <xsl:import href="common.xsl" />
    <xsl:import href="uielements.xsl" />
    <xsl:import href="dateTime.xsl" />
    <xsl:output method="xml" indent="yes"/>
    <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//@template = 'picturePage'">
                <xsl:apply-templates select="." mode="picturePage" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="response" mode="picturePage">
        <html class="page_pictures" xmlns="http://www.w3.org/1999/xhtml">
            <xsl:variable name="title" select="concat(./@objectName, ' - ', $locale/data[@name='picture']/value)" />
            <head>

                <xsl:apply-templates select="." mode="headContent"></xsl:apply-templates>
            </head>
            <body style="overflow:auto;">
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:call-template name="commonBodyIncludes" />
                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'objects'" />
                </xsl:apply-templates>
                <xsl:call-template name="commonPopupMessage" />
                <div id="additionalPageContent">
                    <div class="main-content-header no-margin">
                        <div class="deviceName">
                            <xsl:value-of select="./@vehicleInfo" />
                        </div>

                        <div class="datepicker">
                            <div class="input-group"  data-datepicker="Date">
                                <input id="dateFrom" class="data dateFrom inp" type="text"/>
                                <span class="input-group-btn">
                                    <button class="btn">
                                        <span class="typcn typcn-calendar-outline"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="main-content no-margin">
                        <div class="picturePage">
                            <div class="ImageGallery">
                                <div id="actionsDiv">
                                    <div class="action-filter"></div>
                                </div>
                                <div class="no-image"></div>
                                <div id="mainImage"></div>
                            </div>

                            <div class="container">
                                <h4>
                                    <xsl:value-of select="$locale/data[@name='eventList']/value"/>
                                </h4>
                                <a href="#" class="autoUpload button">
                                    <xsl:value-of select="$locale/data[@name='switchonAutoupdate']/value"/> 
                                </a>
                                <div class="clear"></div>
                                <div class="scrolledDiv">
                                    <div id="thumbnails"></div>  
                                </div>  
                            </div>
                  
                        </div>
            
              
                    </div>
                    <div class="clear" />
                </div>


                <script type="text/javascript">
                    UFIN.pageType = 'picture';
                    UFIN.gallery = {};
                    UFIN.gallery.data = {
                    vehicleId:  <xsl:value-of select="./@vehicleId" />,
                    logtime: <xsl:value-of select="./@logtime" />,
                    pictureids: <xsl:value-of select="./@pictureids" />
                    };
                </script>

                <xsl:apply-templates select="." mode="pageFooter" />
                <xsl:call-template name="requireJS" />
                <xsl:call-template name="websso"/>
            </body>
        </html>
      
    </xsl:template>

    <xsl:template name="localTitle">
        <xsl:if test="/response/@vehicleInfo != ''">
            <xsl:value-of select="/response/@vehicleInfo"/>
            <xsl:text> / </xsl:text>
        </xsl:if>

    </xsl:template>
    
</xsl:stylesheet>
