<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:doc="http://ufin.online/2016/documents">
    <xsl:import href="common.xsl" />
    <xsl:import href="dialer.xsl" />
    <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <!--xsl:template match="/">
            <xsl:choose>
                    <xsl:when test="//@template = 'userSettings'">
                            <xsl:apply-templates select="." mode="userSettings" />
                    </xsl:when>
            </xsl:choose>
    </xsl:template-->

    <!--xsl:template match="response" mode="userSettings"-->
    <xsl:template match="/">
        <html>
            <xsl:apply-templates select="./response" mode="header" />
            <!--onUnload="PhoneDialer.unload()" -->
            <body style="overflow:auto;">
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>

                <script type="text/javascript">
                    UFIN.pageType = 'settings';
                    UFIN.loggedUserId = '<xsl:value-of select="./response/Person/Id" />';

                </script>
                <xsl:call-template name="websso"/>
                <xsl:apply-templates select="./response" mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'settings'" />
                </xsl:apply-templates>
                <div class="b-content">
                    <div class="aside-block">
                        <div class="legend">
                            <xsl:value-of select="$locale/data[@name='userSettings']/value" />
                        </div>
                        <xsl:apply-templates  select="./response" mode="leftMenu" />
                    </div>
                    <div class="main-content">
                        <xsl:apply-templates select="./response" />
                    </div>
                    <xsl:call-template name="waitDialog" />
                </div>

                <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />

            </body>
        </html>
    </xsl:template>
    <!-- AS: верхние переключалки -->
    <xsl:template match="response" mode="topSelector">
        <div style="padding:5px;border:solid 1px #ccc">
            <xsl:choose>
                <xsl:when test="./@template = 'userSettings'">
                    <h3>
                        <xsl:value-of select="$locale/data[@name='mySettings']/value" />
                    </h3>
                    <xsl:if test="/response/@AllowUseDialer = 'true'">
                        <a href="{./@ApplicationPath}/services/connection" style="float:right">
                            <xsl:value-of select="$locale/data[@name='connectionSettings']/value" />
                        </a>
                    </xsl:if>

                    <xsl:if test="/response/Departments/Department/TypeIsChangeable = 'true'">
                        <a href="{/response/@ApplicationPath}/department.aspx?a=requestCorporativeForm"
                           title="{$locale/data[@name='goToCorporativeDescription']/value}"
                           style="float:right">
                            <xsl:value-of select="$locale/data[@name='changeServiceType']/value" />
                        </a>
                    </xsl:if>


                </xsl:when>
                <xsl:when test="./@template = 'connectionSettings'">
                    <a href="{./@ApplicationPath}/settings.aspx">
                        <xsl:value-of select="$locale/data[@name='mySettings']/value" />
                    </a>
                    |<xsl:text>&#160;</xsl:text>
                    <strong>
                        <xsl:value-of select="$locale/data[@name='connectionSettings']/value" />
                    </strong>
                </xsl:when>
            </xsl:choose>
            <div class="clear"></div>
        </div>


    </xsl:template>



    <!--Смена логина-->
    <xsl:template match="response" mode="loginChange">
        <h2>
            <xsl:value-of select="$locale/data[@name='loginChange']/value" />:
        </h2>
        <div class="clear"></div>
        <div style="float:left;width:30%;">
            <span>
                <xsl:value-of select="$locale/data[@name='newLogin']/value" />
            </span>
        </div>
        <div style="float:left;width:69%;">
            <input type="text" id="newlogin" name="newlogin" style="width:150px" class="data" />
        </div>
    </xsl:template>

    <!-- AS: интервал обновления -->
    <xsl:template match="response" mode="refreshInterval">
        <div class="settings">
            <xsl:variable name="refreshValue">
                <xsl:choose>
                    <xsl:when test="string-length(./@refresh) = 0 or ./@refresh &lt; 5">
                        <xsl:text>30</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="./@refresh" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <input type="number" id="refresh" name="refresh" class="data inp" value="{$refreshValue}" min="5" max="99999" />
            <label>
                <xsl:value-of select="$locale/data[@name='refreshPeriodNote']/value" />
            </label>
            <span class="question">
                <span class="question-sec">
                    <xsl:value-of select="$locale/data[@name='refreshPeriodDependencyNote']/value" />
                </span>
            </span>
            <div class="clear"></div>
        </div>
        <!--div style="float:left;width:30%;">
          <strong>
           : <xsl:value-of select="$locale/data[@name='refreshInterval']/value" />
          </strong>
          <div>
            <xsl:value-of select="$locale/data[@name='refreshPeriodNote']/value" />
          </div>
        </div>
        <div style="float:left;width:10%;padding-left:0;padding-right:0;margin-left:0;margin-right:10px">
              </div>
        <div style="float:left;width:58%;">
          <span>
            <xsl:value-of select="$locale/data[@name='refreshPeriodDependencyNote']/value" />
          </span>
        </div-->

    </xsl:template>

    <!-- AS: актуальность объектов слежения -->
    <xsl:template match="response" mode="objectsOnlineDetermination">
        <div class="settings">
            <xsl:variable name="actualityValue">
                <xsl:choose>
                    <xsl:when test="string-length(./@onlinehrs) = 0">
                        <xsl:text>24</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="./@onlinehrs" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <input type="number" id="objectsOnlineValue" name="objectsOnlineValue" class="data inp" value="{$actualityValue}"  min="1" max="99999" />
            <label>
                <xsl:value-of select="$locale/data[@name='onlineObjectsInHours']/value" />

            </label>
            <span class="question">
                <span class="question-sec">
                    <xsl:value-of select="$locale/data[@name='onlineObjectsDescription']/value" />
                </span>
            </span>
            <div class="clear"></div>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="leftMenu">
        <div class="menu-right" >
            <ul class="tabs" id="settingsTabs">
                <li>
                    <a href="#" tab-index="5">
                        <xsl:value-of select="$locale/data[@name='baseTab']/value" />
                    </a>
                </li>
                <xsl:if test="not(/response/@WebSSO)">
                    <li>
                        <a href="#" tab-index="3" style="width:181px;" id="settingsPassword">
                            <xsl:if test="/response/@ForcedToChangePassword = 'true'">
                                <xsl:attribute name="class">warning</xsl:attribute>
                                <xsl:attribute name="title">
                                    <xsl:value-of select="/response/message/MessageBody"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="$locale/data[@name='Password']/value" />
                        </a>
                    </li>
                </xsl:if>

                <li>
                    <a href="#" tab-index="1" id="settingsEmail">
                        <xsl:value-of select="$locale/UserSettings/data[@name='EmailAddresses']/value" />
                    </a>
                </li>
                <li>
                    <a href="#" tab-index="2" id="settingsPhone">
                        <xsl:value-of select="$locale/UserSettings/data[@name='Phones']/value" />
                    </a>
                </li>

                <li>
                    <a href="#" tab-index="4" id="settingsMonitoring">
                        <xsl:value-of select="$locale/data[@name='monitoring']/value" />
                    </a>
                </li>
            </ul>
        </div>
    </xsl:template>



    <!-- AS: инфа об операторе -->
    <xsl:template match="Person">
        <div class="tabbable" id="settings-tabs" data-tab-tabs="#settingsTabs" >
            <div class="tab-content" tab-index="5" >
                <div class="baseData vertical">
                    <div class="settings controlBlock">
                        <div class="controlDescription">
                            <xsl:value-of select="$locale/data[@name='Login']/value" />
                        </div>

                        <div class="editable underline canedit control" id="userName">
                            <span></span>
                        </div>
                    </div>
                    <div class="settings controlBlock">
                        <div class="controlDescription">
                            <xsl:value-of select="$locale/data[@name='OperatorLogin']/value" />
                        </div>
                        <div class="editable underline control" id="userLogin">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-content" tab-index="1" >
                <div class="topspace2" id="emailListContainer">

                </div>
            </div>

            <div class="tab-content" tab-index="2" >
                <div class="topspace2" id="phoneListContainer">

                </div>
            </div>

            <xsl:if test="not(/response/@WebSSO)">
                <div class="tab-content" tab-index="3">
                    <div class="vertical">
                    <form action="{/response/@ApplicationPath}/settings.aspx" method="post" id="form">

                        <xsl:if test="/response/@ForcedToChangePassword != 'true'">
                            <div class="controlBlock">
                                <div class="controlDescription">
                                    <xsl:value-of select="$locale/data[@name='currentPwd']/value" />
                                </div>
                                <div class="control">
                                    <input type="password" id="currentpwd" name="currentpwd" class="data inp" />
                                </div>
                            </div>
                        </xsl:if>
                        <div class="controlBlock">
                            <div class="controlDescription">
                                <xsl:value-of select="$locale/data[@name='newPwd']/value" />
                            </div>
                            <div class="control">
                                <input type="password" id="newpwd1" name="newpwd1" class="data inp" />
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="controlDescription">
                                <xsl:value-of select="$locale/data[@name='retypeNewPwd']/value" />
                            </div>
                            <div class="control">
                                <input type="password" id="newpwd2" name="newpwd2" class="data inp" />
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="control">
                                <button class="save-password-button btn">
                                    <xsl:value-of select="$locale/data[@name='save']/value" />
                                </button>
                            </div>
                        </div>


                        <input type="hidden" name="a" value="changePassword"/>
                    </form>
                </div>
                </div>
            </xsl:if>

            <div class="tab-content" tab-index="4">
                <xsl:apply-templates select="/response" mode="refreshInterval" />
                <xsl:apply-templates select="/response" mode="objectsOnlineDetermination" />

            </div>
        </div>
        <div>


        </div>
        <div style="width:100%;display:none;margin-top: 2em" id="email-not-confirmed-yet-div">
            <div style="float:left;width:30%;">
                <xsl:value-of select="$locale/data[@name='confirmation-state']/value" />
            </div>
            <div style="float:left;width:70%;">
                <span id="confirmation-state">

                </span>
                <xsl:text> </xsl:text>
                <a href="javascript:void(0);" onclick="UserSettings.confirmEmail()" id="confirm-email-button">
                    <xsl:value-of select="$locale/data[@name='confirm']/value" />
                </a>
            </div>
        </div>
        <div class="divider"></div>
        <div style="display:none;margin-top: 2em" id="email-confirmation-div">
            <div style="float:left;width:30%;">
                <xsl:value-of select="$locale/data[@name='paste-confirmation-key']/value" />
            </div>
            <div style="float:left;width:70%;">
                <xsl:call-template name="textEdit">
                    <xsl:with-param name="id" select="'email-confirmation-key'" />
                    <xsl:with-param name="style" select="'width:300px'" />
                </xsl:call-template>
                <p>
                    <xsl:value-of select="$locale/data[@name='email-confirmation-key-part-1']/value" />
                    <span id="email-waiting-for-confirmation"></span>
                    <xsl:value-of select="$locale/data[@name='email-confirmation-key-part-2']/value" />
                </p>
            </div>
        </div>
        <xsl:call-template name="clear" />
    </xsl:template>

    <!--  Шаблоны, обрабатывающие страницы-->
    <!-- AS: настройки юзера -->
    <xsl:template match="response[@template='userSettings']">
        <!--xsl:apply-templates select="." mode="topSelector" /-->

        <fieldset class="noborder">
            <xsl:apply-templates select="./Person" />

        </fieldset>

    </xsl:template>

    <!-- AS: настройки соединения
    Здесь просто вытаскивается контент из внешнего XML-файла.
    Контент содержит разметку, ссылки на другие ресурсы и т.п.
    -->
    <xsl:template match="response[@template='connectionSettings']">
        <xsl:apply-templates select="." mode="topSelector" />

        <xsl:variable name="language" select="./@language" />

        <xsl:variable name="documentFile">
            <xsl:text>../../documents/connection/connection.xml</xsl:text>
        </xsl:variable>
        <xsl:variable name="docFile" select="document($documentFile)/document" />

        <div style="height:85%;border-top:0px solid #ccc;">
            <div>
                <xsl:copy-of select="$docFile/content[@language = $language][@mode='commonDescription']/*" />
            </div>
            <div class="divider">
                <xsl:text> </xsl:text>
            </div>
            <strong>
                <xsl:value-of select="$locale/data[@name='voiceSetup']/value" />
            </strong>
            <div style="width:100%;margin-top:10px;">
                <div style="float:left;width:48%;">
                    <div style="margin-right:10px;">
                        <div style="width:100%;">
                            <div style="width:5%;float:left;">
                                <span class="big">
                                    <strong>1</strong>
                                </span>
                            </div>
                            <div style="width:95%;float:left;">
                                <div>
                                    <xsl:copy-of select="$docFile/content[@language = $language][@mode='step1']/*" />
                                </div>
                            </div>
                        </div>
                        <xsl:call-template name="clear" />
                    </div>
                </div>
                <div style="float:left;width:48%;border-left:1px solid #ccc;">
                    <div style="margin-left:10px;">
                        <div class="width:100%;">
                            <div style="width:5%;float:left;">
                                <span class="big">
                                    <strong>2</strong>
                                </span>
                            </div>
                            <div style="width:90%;float:left;">
                                <label>
                                    <input type="checkbox" id="usedialer" style="width:20px;" onchange="PhoneDialer.setRefreshInterval(event)" />
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:value-of select="$locale/data[@name='useDialer']/value" />
                                </label>
                                <div class="grey">
                                    <xsl:copy-of select="$locale/data[@name='youNeedModem']/value" />
                                </div>
                                <div id="useDialerMessage" style="margin-top:10px;" class="grey"></div>
                            </div>
                        </div>
                    </div>
                    <xsl:call-template name="clear" />
                </div>
                <xsl:call-template name="clear" />
            </div>
            <script type="text/javascript">
                UFIN.PhoneDialer = true;

            </script>
        </div>
    </xsl:template>

    <xsl:template name="contactsContentPhones">
        <table class="short lined" id="contentPhoneTable">
            <thead>
                <xsl:if test="count(/response/Person/Phones/Phone) = 0">
                    <xsl:attribute name="style">display:none;</xsl:attribute>
                </xsl:if>
                <tr>
                    <th style="width: 21em;">
                        <xsl:copy-of select="$locale/data[@name='phoneValue']/value" />
                    </th>
                    <th>
                        <xsl:copy-of select="$locale/data[@name='confirmStatus']/value" />
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
            <tbody>
                <xsl:for-each select="/response/Person/Phones/Phone">
                    <tr>
                        <td>
                            <xsl:value-of select="Value"/>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="IsConfirmed = 'true'">
                                    <img src="{//@ApplicationPath}/img/ok.gif" width="15" height="15" title="{$locale/data[@name='confirmed']/value}" alt="{$locale/data[@name='ok']/value}" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <a href="javascript:UserSettings.VerifyPhone('{Value}')">
                                        <img src="{//@ApplicationPath}/img/error.gif" width="16" height="16" title="{$locale/data[@name='phoneNotConfirmed']/value}" alt="{$locale/data[@name='cancel']/value}" />
                                    </a>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td>
                            <a href="javascript:void(0)" id="{Id}" onclick='UserSettings.removePhone(this)'>
                                <xsl:copy-of select="$locale/data[@name='phoneRemoveCaption']/value" />
                            </a>
                        </td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>

        <button type="button" id="addNewPhone" onclick="UserSettings.addNewPhone()">
            <xsl:value-of select="$locale/data[@name='addPhone']/value"/>
        </button>

    </xsl:template>



</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\page\page.xmlsettings.xsl_1.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
