<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
AS: Работа со строками
-->

<!-- AS: В нижний регистр -->
<xsl:template name="toLowerCase">
	<xsl:param name="string" />
	<xsl:param name="lang" select="en-US" />
	<xsl:if test="$string">
		<xsl:choose>
			<xsl:when test="$lang = 'ru-RU'">
				<xsl:value-of select='translate($string,"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ","абвгдеёжзийклмнопрстуфхцчшщъыьэюя")'/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select='translate($string,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")'/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>


</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->