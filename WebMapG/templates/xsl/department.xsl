﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
  <xsl:import href="common.xsl" />
  <xsl:import href="xslt-scripts.xsl" />
  <xsl:output method="html" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="/response/@template = 'requestCorporativeForm'">
        <xsl:apply-templates select="." mode="requestCorporativeForm" />
      </xsl:when>
      <xsl:when test="/response/@template = 'approveCorporativeForm'">
        <xsl:apply-templates select="." mode="approveCorporativeForm" />
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="response" mode="requestCorporativeForm">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:variable name="title" select="$locale/data[@name='requestCorporativeFormTitle']/value" />
      <head>
        <xsl:apply-templates select="." mode="headContent">
          <xsl:with-param name="title" select="concat($title, ' - ', $locale/data[@name='ApplicationMainTitle']/value)" />
        </xsl:apply-templates>
        <script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/department.js"> </script>
      </head>
      <body style="overflow:auto;">
        <xsl:call-template name="commonBodyIncludes" />
        <xsl:apply-templates select="." mode="pageHeader">
          <xsl:with-param name="selectedTab" select="'settings'" />
        </xsl:apply-templates>
        <xsl:call-template name="commonPopupMessage" />
        <div id="additionalPageContent">
          <div class="main-content">
            <div class="content-header">
              <h2>
                <xsl:value-of select="$title" />
              </h2>
            </div>

            <xsl:choose>
              <xsl:when test="not(./@sentDate)">
                <div>
                  <p>
                    <xsl:copy-of select="$locale/data[@name='requestSwitchingToCorporativeDescription']/value/* "/>
                  </p>
                  <p>
                    <form method="POST" action="{./@ApplicationPath}/department.aspx">
                      <input type="hidden" name="a" value="requestCorporativeSubmit" />
                      <p>
                        <input type="checkbox" id="confirm-being-corporative-customer" />
                        <label for="confirm-being-corporative-customer">
                          <xsl:value-of select="$locale/data[@name='confirm-being-corporative-customer']/value" />
                        </label>
                      </p>
                      <button type="submit" disabled="disabled" id="send-request">
                        <xsl:value-of select="$locale/data[@name='sendRequest']/value"/>
                      </button>
                    </form>
                  </p>
                </div>
              </xsl:when>
              <xsl:otherwise>
                <div>
                  <p>
                    <xsl:value-of select="$locale/data[@name='switchingToCorporativeRequestHasBeenSent']/value"/>
                    <xsl:value-of select="./@sentDate"/>
                  </p>
                </div>
              </xsl:otherwise>
            </xsl:choose>
          </div>
          <xsl:call-template name="requireJS" />

            <xsl:apply-templates select="." mode="pageFooter" />
        </div>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="response" mode="approveCorporativeForm">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:variable name="title" select="$locale/data[@name='approveCorporativeFormTitle']/value" />
      <head>
        <xsl:apply-templates select="." mode="headContent">
          <xsl:with-param name="title" select="concat($title, ' - ', $locale/data[@name='ApplicationMainTitle']/value)" />
        </xsl:apply-templates>
        <script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/department.js"> </script>
      </head>
      <body style="overflow:auto;">
        <xsl:call-template name="commonBodyIncludes" />
        <xsl:apply-templates select="." mode="pageHeader">
          <xsl:with-param name="selectedTab" select="'settings'" />
        </xsl:apply-templates>
        <xsl:call-template name="commonPopupMessage" />
        <div id="additionalPageContent">
          <div class="main-content">
            <div class="content-header">
              <h2>
                <xsl:value-of select="$title" />
              </h2>
            </div>

            <div>
              <xsl:choose>
              <xsl:when test="/response/Departments/Department/Type = 'Physical'">
                <form method="post" action="{./@ApplicationPath}/department.aspx">
                  <input type="hidden" name="a" value="approveCorporativeSubmit"></input>
                  <p>
                    <xsl:value-of select="$locale/data[@name='pressButtonToMakeCorporative']/value"/>
                    <xsl:value-of select="/response/Departments/Department/Name"/>
                  </p>
                  <button type="submit">
                    <xsl:value-of select="$locale/data[@name='turnOn']/value"/>
                  </button>
                </form>
              </xsl:when>
                <xsl:when test="/response/Departments/Department/Type = 'Corporate'">
                  <p>
                    <xsl:value-of select="$locale/data[@name='isCorporative']/value"/>
                    <xsl:value-of select="/response/Departments/Department/Name"/>
                  </p>
                </xsl:when>
            </xsl:choose>
            </div>
          </div>
          <xsl:call-template name="requireJS" />

            <xsl:apply-templates select="." mode="pageFooter" />
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
