<?xml version="1.0" encoding="utf-8" ?>
<!-- xmlns:ms="urn:schemas-microsoft-com:xslt" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:import href="../common.xsl" />

  <!--  media-type="text/xml; charset=UTF-8"  -->
  <xsl:output method="xml" version="1.0" doctype-public="-//WAPFORUM//DTD XHTML Mobile 1.0//EN" doctype-system="http://www.wapforum.org/DTD/xhtml-mobile10.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="no" media-type="text/xml" />
  <!--xsl:output method="xml" version="1.0" doctype-public="-//WAPFORUM//DTD XHTML Mobile 1.0//EN" doctype-system="http://www.wapforum.org/DTD/xhtml-mobile10.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="no" media-type="text/xml; charset=UTF-8" /-->

  <xsl:template match="response" mode="header">

    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="description" content="Онлайн мониторинг и позиционирование транспортых средств" />
      <meta name="keywords" content="мониторинг, транспортных, средств, позиционирование, карта, местности, контроль, местонахождения, объектов, маршрут, движения" />
      
      <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
      <!--<meta http-equiv="CACHE-CONTROL" content="max-age=2" />-->
      <meta http-equiv="PRAGMA" content="NO-CACHE" />

      <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width"/>
      <meta name="format-detection" content="telephone=no"/>


      <title>
        <xsl:value-of select="./@title" />
      </title>
      <link rel="stylesheet" href="{./@baseWebAppPath}/css/mobile/mobi.css" />
      <script src="{./@baseWebAppPath}/includes/scripts/lib/jquery/jquery.js" type="text/javascript"></script>
      <link rel="stylesheet" href="{$localLogoCSS}" />
      <link rel="shortcut icon" href="{./@baseWebAppPath}/{./@favicon}"/>
      <xsl:element name="script">
        <xsl:attribute name="src"><![CDATA[//maps.google.com/maps/api/js?sensor=false]]></xsl:attribute>
        <xsl:attribute name="type">text/javascript</xsl:attribute>
      </xsl:element>
    </head>

  </xsl:template>

  <xsl:template match="message">
    <xsl:if test="string-length(./MessageTitle) &gt; 0 or string-length(./MessageBody) &gt; 0">
      <div>
        <p>
          <xsl:if test="string-length(./MessageBody) &gt; 0">
            <xsl:call-template name="replace">
              <xsl:with-param name="str" select="./MessageBody" />
            </xsl:call-template>
          </xsl:if>
        </p>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="response" mode="mobilePageHeader">
    
    <div id="head">
      <div>
        <xsl:attribute name="class">
<!--          <xsl:choose>-->
<!--            <xsl:when test="contains($localLogoCSS, 'whiteLogo')">-->
<!--              whiteLogo-logo-->
<!--            </xsl:when>-->
<!--            <xsl:otherwise>-->
<!--              <xsl:choose>-->
<!--                <xsl:when test="not(contains(./@language,'ru'))">-->
<!--                  logo_en-->
<!--                </xsl:when>-->
<!--                <xsl:otherwise>-->
<!--                  logo-->
<!--                </xsl:otherwise>-->
<!--              </xsl:choose>-->
<!--            </xsl:otherwise>-->
<!--          </xsl:choose>-->
        </xsl:attribute>
        <a href="http://www.pda.ufin.online/" class="b"></a>
        <xsl:choose>
          <xsl:when test="not(contains(./@template, 'login'))">
            <ul>
              <li>
                <a class="objects" href="{./@ApplicationPath}/mobile/default.aspx?a=getvehicles&amp;includeAddresses=true&amp;ssid={./@ssid}&amp;mapGuid={./mapdescriptions/MapDescription[current = 'true']/guid}" title="{$locale/data[@name='TrackingObjectsShort']/value}">
                  <b></b>
                </a>
              </li>
              <li>
                <a class="spicer" href="">
                  <b></b>
                </a>
              </li>
              <li>
                <a class="settings" href="{./@ApplicationPath}/settings.aspx?ismobile=true" title="{$locale/data[@name='userSettings']/value}">
                  <b></b>
                </a>
              </li>
              <li>
                <a class="spicer" href="">
                  <b></b>
                </a>
              </li>
              <li>
                <a class="reload" href="" title="{$locale/data[@name='reload']/value}">
                  <b></b>
                </a>
              </li>
            </ul>
            <script type="text/javascript">
              $(function(){
              $("a.exit").click(function(){
              if (typeof localStorage != "undefined"){
              try{
              var authData = JSON.parse(localStorage.getItem("userAuthData"));
              authData.islogin = false;
              localStorage.setItem("userAuthData", JSON.stringify(authData));
              }
              catch(e){
                if (window.console)
                  console.log(e);
              }
              }
              location.href = $(this).attr("href");
              return false;
              });
              });
            </script>
            <a class="exit" href="{./@ApplicationPath}/mobile/default.aspx?a=logout" title="{$locale/data[@name='Logout']/value}">
              <b></b>
            </a>
          </xsl:when>
        </xsl:choose>
          <div class="langs">
              <xsl:apply-templates mode="languageSelector" select="."/>
          </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="response" mode="pageFooter">
    <xsl:call-template name="clear" />
    <xsl:choose>
      <xsl:when test="not(contains(./@template, 'login'))">
        <div id="footer">
          <a href="http://www.it.sitronics.com/" class="grey">
            <xsl:value-of select="$locale/data[@name='stsCopyright']/value" />
          </a>
          <div>
            <a href="{./@ApplicationPath}/mobile/default.aspx?a=getvehicles&amp;includeAddresses=true&amp;ssid={./@ssid}&amp;mapGuid={./mapdescriptions/MapDescription[current = 'true']/guid}">
              <xsl:value-of select="$locale/data[@name='TrackingObjectsShort']/value" />
            </a>
            <a href="{./@ApplicationPath}/settings.aspx?ssid={./@ssid}">
              <xsl:value-of select="$locale/data[@name='userSettings']/value" />
            </a>
            <a href="{./@ApplicationPath}/mobile/default.aspx?ssid={./@ssid}">
              <xsl:value-of select="$locale/data[@name='Logout']/value" />
            </a>
          </div>
        </div>
      </xsl:when>
    </xsl:choose>
    <p id="offsetWidthParagraph" style="visibility: hidden">
      This text is intendend for determining actual width of screen of mobile device.
      The idea is that mobile browser usually fits text to screen size.
      In normal condition it should be not visible.
      Visibility was set up by CSS.
      If you see the text your browser does not work properly.
    </p>
  </xsl:template>



    <xsl:template match="response" mode="languageSelector">
        <xsl:choose>
            <xsl:when test="not(contains(./@location,'gu-'))">
                <ul class="languge_selector">
                    <xsl:choose>
                        <xsl:when test="contains(./@language, 'ru-') = 'true'">
                            <li title="Текущий язык - русский">
                                <span>ru</span>
                            </li>
                            <li>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=en" title="Switch to English language">en</a>
                            </li>

                        </xsl:when>
                        <xsl:otherwise>
                            <li>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=ru" title="Переключиться на русский язык">ru</a>
                            </li>
                            <li title="Current language is English">
                                <span >en</span>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </ul>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
