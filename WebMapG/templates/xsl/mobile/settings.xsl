<?xml version="1.0" encoding="utf-8" ?>

<!-- xmlns:ms="urn:schemas-microsoft-com:xslt" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:user="urn:my-scripts">
  <xsl:import href="common.xsl" />
  <xsl:import href="../xslt-scripts.xsl"/>
  <!--  media-type="text/xml; charset=UTF-8"  -->
  <xsl:output method="xml" version="1.0" doctype-public="-//WAPFORUM//DTD XHTML Mobile 1.0//EN" doctype-system="http://www.wapforum.org/DTD/xhtml-mobile10.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="no" media-type="text/xml" />
  <xsl:param name="date" />

  <!-- AS: Отобразить все объекты слежения -->
  <xsl:template match="/">
    <html>
      <xsl:apply-templates select="/response" mode="header" />
      <body>
        <div id="cnt">
          <xsl:apply-templates select="/response" mode="mobilePageHeader" />
          <div class="content">
            <h1><xsl:value-of select="/response/@title" /></h1>
            <xsl:apply-templates select="/response/message" />
            <xsl:apply-templates select="/response" mode="page-content" />
          </div>
        </div>
        <xsl:apply-templates select="." mode="pageFooter" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="response[@template = 'userSettings']" mode="page-content">
    <form action="{//@ApplicationPath}/settings.aspx" method="post">
      <input type="hidden" name="a" value="changePassword" />
      <input type="hidden" name="ssid" value="{./@ssid}" />
      <input type="hidden" name="ismobile" value="true" />
      <xsl:choose>
        <xsl:when test="./@ForcedToChangePassword = 'true'">
        </xsl:when>
        <xsl:otherwise>
          <table>
            <tr>
              <td>
                <xsl:value-of select="$locale/data[@name='currentPwd']/value" />
              </td>
            </tr>
            <tr>
              <td>
                <input type="password" id="currentpwd" name="currentpwd" />
              </td>
            </tr>
          </table>
        </xsl:otherwise>
      </xsl:choose>
      <table>
        <tr>
          <td>
            <xsl:value-of select="$locale/data[@name='newPwd']/value" />
          </td>
        </tr>
        <tr>
          <td>
            <input type="password" id="newpwd1" name="newpwd1" />
          </td>
        </tr>
        <tr>
          <td>
            <xsl:value-of select="$locale/data[@name='retypeNewPwd']/value" />
          </td>
        </tr>
        <tr>
          <td>
            <input type="password" id="newpwd2" name="newpwd2" class="data" />
          </td>
        </tr>
      </table>
      <input type="submit" class="login-button" value="{$locale/data[@name='save']/value}" />
    </form>
  </xsl:template>
</xsl:stylesheet>