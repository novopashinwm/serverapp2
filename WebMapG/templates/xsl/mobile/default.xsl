﻿<?xml version="1.0" encoding="utf-8" ?>

<!-- xmlns:ms="urn:schemas-microsoft-com:xslt" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:user="urn:my-scripts">
	<xsl:import href="common.xsl" />
  <xsl:import href="../xslt-scripts.xsl"/>
	<!--  media-type="text/xml; charset=UTF-8"  -->
	<xsl:output method="xml" version="1.0" doctype-public="-//WAPFORUM//DTD XHTML Mobile 1.0//EN" doctype-system="http://www.wapforum.org/DTD/xhtml-mobile10.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="no" media-type="text/xml" />
	<xsl:param name="date" />


<xsl:template match="/">
	<!--html xmlns="http://www.w3.org/1999/xhtml">
		<xsl:apply-templates select="/response" mode="header" />
		<body>
			<div>
				<img src="{/response/@baseWebAppPath}/img/f4_80_80.jpg" alt="hello" />
			</div>
		</body>
	</html-->
	<xsl:choose>
		<xsl:when test="/response/@template = 'mobileObjectList'">
			<xsl:apply-templates select="/response" mode="mobileObjectList" />
		</xsl:when>
		<xsl:when test="/response/@template = 'mobileObjectPos'">
			<xsl:apply-templates select="/response" mode="mobileObjectPos" />
		</xsl:when>
		<xsl:when test="/response/@template = 'login'">
			<xsl:apply-templates select="/response" mode="default" />
			<!--xsl:apply-templates select="/response" mode="test" /-->
		</xsl:when>
    <xsl:when test="/response/@template = 'messagePage'">
      <xsl:apply-templates select="/response" mode="messagePage" />
    </xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="/response" mode="default" />
			<!--xsl:apply-templates select="/response" mode="test" /-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: главная страница. С формой авторизации -->
<xsl:template match="response" mode="default">
<html>
	<xsl:apply-templates select="." mode="header" />
	<body>
    <script src="{//@baseWebAppPath}/includes/scripts/mobile/login.js"></script>
    <script src="{./@baseWebAppPath}/includes/scripts/mobile/global.js" type="text/javascript"></script>
    <div id="cnt">
		<xsl:apply-templates select="." mode="mobilePageHeader" />
      <div class="content">
        <h1><xsl:value-of select="/response/@applicationShortName" /></h1>
        <p>
          <form action="{//@ApplicationPath}/login.aspx" method="get">
            <xsl:if test="string-length(/response/message/MessageTitle) > 0">
            <div style="color:red;" class="error-login-message">
              <xsl:value-of select="/response/message/MessageTitle" />
            </div>
            </xsl:if>
            <table>
              <tr>
                <td>
                  <xsl:value-of select="$locale/data[@name='OperatorLogin']/value" />:
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="login" id="txtLogin"  class="formField" />
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:value-of select="$locale/data[@name='Password']/value" />:
                </td>
              </tr>
              <tr>
                <td>
                  <input type="password" name="password" id="password"  class="formField" />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" class="login-button"  value="{$locale/data[@name='GetIn']/value}" />
                  <img src="{/response/@baseWebAppPath}/img/wait.gif" style="position: relative;top: 5px;left: 3px; display:none;"/>
                  <input type="hidden" name="a" id="a" value="login" />
                  <input type="hidden" name="mobile" id="mobile" value="1" />
                  <input type="hidden" name="tz" id="tz" value="" />
                </td>
              </tr>
            </table>
          </form>
        </p>
      </div>
		</div>
		<xsl:apply-templates select="." mode="pageFooter" />
	</body>
</html>
</xsl:template>

<!-- AS: Отобразить все объекты слежения -->
<xsl:template match="response" mode="mobileObjectList">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
		<xsl:apply-templates select="." mode="header" />
    <script src="{./@baseWebAppPath}/includes/scripts/mobile/global.js" type="text/javascript"></script>
		<body>
      <div id="cnt">
        <xsl:apply-templates select="." mode="mobilePageHeader" />
        <xsl:apply-templates select="./message" />
        <div class="content">
          <h1><xsl:value-of select="$locale/data[@name='TrackingObjectsTitle']/value" />
          </h1>
          <xsl:apply-templates select="./vehicles/vehicle" />
        </div>
      </div>
			<xsl:apply-templates select="." mode="pageFooter" />
		</body>
	</html>
</xsl:template>

<xsl:template match="vehicle" mode="map-reference">
    <xsl:variable name="reference-text">
      <xsl:choose>
        <xsl:when test="./@GarageNumber and ./@GarageNumber != ''">
          <xsl:value-of select="./@GarageNumber" />
        </xsl:when>
        <xsl:otherwise>
          &lt;<xsl:value-of select="$locale/data[@name='noname']/value" />&gt;
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="./@posLogTime and ./@posLogTime != ''">
        <a href="{/response/@ApplicationPath}/mobile/trackingObject.aspx?a=getPosition&amp;mapguid={/response/mapdescriptions/MapDescription[current = 'true']/guid}&amp;id={./@id}&amp;ssid={/response/@ssid}">
          <xsl:copy-of select="$reference-text"/>
        </a>        
      </xsl:when>
      <xsl:otherwise>
        <a href="#" onclick="return false;">
          <xsl:copy-of select="$reference-text"/>
        </a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
<!-- AS: отрисовка объекта слежения -->
<xsl:template match="vehicle">
  <div>
    <xsl:choose>
      <xsl:when test="./@posLogTime and ./@posLogTime != ''">
        <xsl:attribute name="class">
          obj
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="class">
          obj deactive
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="." mode="map-reference" />
		
          <xsl:if test="./@posLogTime and ./@posLogTime != ''">
            <small>
                <xsl:call-template name="logtime-tostring">
                  <xsl:with-param name="logtime" select="./@posLogTime" />
                </xsl:call-template>
            </small>
          </xsl:if>
          <xsl:if test="./Capabilities/Capability[. = 'SpeedMonitoring'] and ./@currSpeed != ''">
            <small>
                <xsl:value-of select="./@currSpeed" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
            </small>
          </xsl:if>
          <xsl:if test="./Capabilities/Capability[. = 'address']">
            <small>
                <xsl:value-of select="./@address" />
            </small>
          </xsl:if>
          <xsl:if test="./Capabilities/Capability[. = 'AllowAskPosition']">
            <!--<small>
                <a href="{/response/@ApplicationPath}/mobile/Default.aspx?a=askPosition&amp;mobileid={./@id}&amp;ssid={/response/@ssid}">
                  <xsl:value-of select="$locale/data[@name='sendAskPositionCommand']/value" />
                </a>
            </small>-->
          </xsl:if>
      <xsl:if test="./@address and ./@address != ''">
        <small>
          <xsl:value-of select="./@address"/>
        </small>       
      </xsl:if>
      <xsl:if test="./@radius != '0'">
        <!--<small>
          <xsl:value-of select="$locale/data[@name='positionAccuracy']/value" />:
          <xsl:value-of select="./@radius"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="$locale/data[@name='meterShort']/value"/>
        </small>-->
      </xsl:if>
      <!--TODO: реализовать вывод результата последней команды-->
	</div>
</xsl:template>

<xsl:template match="Sensors">
    <xsl:if test="count(./Sensor) > 1">
      <div>
        <table cellpadding="0" cellspacing="0">
          <xsl:for-each select="./Sensor">
            <xsl:variable name="sensorLegend" select="./@Legend" />
            <tr>
              <td>
                <xsl:choose>
                  <xsl:when test="$locale/sensorNames/data[@name = $sensorLegend]/@value">
                    <xsl:value-of select="$locale/sensorNames/data[@name = $sensorLegend]/@value" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$sensorLegend" />
                  </xsl:otherwise>
                </xsl:choose>:
              </td>
              <xsl:if test="./@Type = 'Analog'">
                <td>
                  <xsl:value-of select="round(./@Value)"/>
                  <xsl:value-of select="$locale/sensorUnits/data[@name = $sensorLegend]/@value" />
                </td>
              </xsl:if>
              <xsl:if test="./@Type = 'Digital'">
                <td>
                  <xsl:choose>
                    <xsl:when test="./@Value = 0">
                      <xsl:value-of select="$locale/data[@name='off2']/value" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="$locale/data[@name='on2']/value" />
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
      </div>
    </xsl:if>
  </xsl:template>
  
<!-- AS: отрисовка объекта на карте -->
<xsl:template match="response" mode="mobileObjectPos">
	<xsl:variable name="zoomInValue">
		<xsl:value-of select="./@scale + 1" />
	</xsl:variable>

	<xsl:variable name="zoomOutValue">
		<xsl:value-of select="./@scale - 1" />
	</xsl:variable>

  <xsl:variable name="scaleString">
    <xsl:choose>
    <xsl:when test="./@scale">
      <xsl:text>&amp;zoom=</xsl:text>
      <xsl:value-of select="./@scale + 9"/>
    </xsl:when>
      <xsl:otherwise>
        <xsl:text>&amp;visible=</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
	<html>
		<xsl:apply-templates select="." mode="header" />
    <xsl:variable name="icon">
      <xsl:choose>
        <xsl:when  test="starts-with(/response/@icon, 'http://')">
          <xsl:value-of select="/response/@icon"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/response/@ApplicationPath"/>
          <xsl:value-of select="/response/@icon"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:element name="script">
      <xsl:attribute name="src"><![CDATA[//maps.google.com/maps/api/js?sensor=false]]></xsl:attribute>
      <xsl:attribute name="type">text/javascript</xsl:attribute>
    </xsl:element>

    
		<body onload="initialize({/response/@jsonObject}, '{/response/@scale}')">
      <script src="{./@baseWebAppPath}/includes/scripts/mobile/global.js" type="text/javascript"></script>
      <script src="{./@baseWebAppPath}/includes/scripts/mobile/default.js" type="text/javascript"></script>
      
      <div id="cnt">
        <xsl:apply-templates select="." mode="mobilePageHeader" />
        <div class="content o">
          <div style="background-color:whiteSmoke; padding:5px;
            border-width:1px;
            border-style:solid;
            border-bottom-color:#aaa;
            border-right-color:#aaa;
            border-top-color:#ddd;
            border-left-color:#ddd;
            border-radius:3px;
            -moz-border-radius:3px;
            -webkit-border-radius:3px;" id="btn">

            <table cellpadding="0" class="o-descr">
              <tr>
                <td>
                  <h1 style="font-size: 16px; color:#0670C1;margin-bottom: 0;cursor:pointer;">
                    <nobr>
                      <xsl:value-of select="./vehicle/@GarageNumber" />
                    </nobr>
                  </h1>
                </td>
                <td>
                  <nobr>
                    <xsl:value-of select="./vehicle/@currSpeed" />
                    <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
                  </nobr>
                </td>
                <td style="padding-top: 2px;">
                  <nobr>
                    <xsl:call-template name="logtime-tostring">
                      <xsl:with-param name="logtime" select="./vehicle/@posLogTime" />
                    </xsl:call-template>
                  </nobr>
                </td>
              </tr>
            </table>
            <xsl:value-of select="./address" />
            &#160;
            <xsl:value-of  select="./address/@coordinates" />
            
          </div>
            
            
              <div id="map">
                <a href="{/response/@ApplicationPath}/mobile/trackingObject.aspx?a=getPosition&amp;mapguid={/response/mapdescriptions/MapDescription[current = 'true']/guid}&amp;id={./vehicle/@id}&amp;ssid={./@ssid}&amp;mapType={/response/@mapType}" class="control reload"></a>
                <a href="" class="control zoomin" ></a>
                <a href="" class="control zoomout" ></a>
                <a href="" class="control map"></a>
                <div id="map_canvas" style="width: 100%;height:400px;"></div>
              </div>
            
          <xsl:apply-templates select="./vehicle/Sensors" />
        </div>
      </div>
      <xsl:apply-templates select="." mode="pageFooter" />
		</body>
	</html>
</xsl:template>

<xsl:template match="response" mode="scales">
	<xsl:call-template name="bildScales">
		<xsl:with-param name="currentScale" select="'0'" />
		<xsl:with-param name="maxScale" select="./@maxScale" />
		<xsl:with-param name="selectedScale" select="./@scale" />
	</xsl:call-template>
</xsl:template>

<xsl:template name="bildScales">
	<xsl:param name="currentScale" />
	<xsl:param name="maxScale" />
	<xsl:param name="selectedScale" />
	<xsl:variable name="imageHeight">
		<xsl:value-of select="$currentScale * 3 + 3" />
	</xsl:variable>

	<span>
		<xsl:choose>
			<xsl:when test="$currentScale = $selectedScale">
				<img border="0" src="{//response/@baseWebAppPath}/img/scale{$currentScale}_s.gif" width="12" height="{$imageHeight}" />
			</xsl:when>
			<xsl:otherwise>
				<a href="{//response/@ApplicationPath}/mobile/trackingObject.aspx?a=getPosition&amp;mapguid={/response/mapdescriptions/MapDescription[current = 'true']/guid}&amp;id={./vehicle/@id}&amp;ssid={./@ssid}&amp;scale={$currentScale}&amp;mapType={/response/@mapType}">
					<img border="0" src="{//response/@baseWebAppPath}/img/scale{$currentScale}.gif" width="12" height="{$imageHeight}" />
				</a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text>
	</span>
	<xsl:if test="$currentScale &lt; $maxScale">
		<xsl:call-template name="bildScales">
			<xsl:with-param name="currentScale" select="$currentScale + 1" />
			<xsl:with-param name="maxScale" select="$maxScale" />
			<xsl:with-param name="selectedScale" select="$selectedScale" />
		</xsl:call-template>
	</xsl:if>
</xsl:template>

  <xsl:template match="response" mode="messagePage">
    <xsl:apply-templates select="." mode="header"/>
    <body>
      <script src="{./@baseWebAppPath}/includes/scripts/mobile/global.js" type="text/javascript"></script>
      <div id="cnt">
      <xsl:apply-templates select="." mode="mobilePageHeader" />
      <div class="content">
        <xsl:apply-templates select="/response/message" />
        <div>
          <span>
            <a href="{./@ApplicationPath}/mobile/Default.aspx?a=getvehicles&amp;ssid={./@ssid}">
              <xsl:value-of select="$locale/data[@name='TrackingObjectsTitle']/value" />
            </a>
          </span>
          <xsl:text> | </xsl:text>
          <span>
            <a href="{/response/@ApplicationPath}/mobile/trackingObject.aspx?a=getPosition&amp;mapguid={/response/mapdescriptions/MapDescription[current = 'true']/guid}&amp;id={./vehicle/@id}&amp;ssid={./@ssid}">
              <xsl:value-of select="$locale/data[@name='map']/value" />
            </a>
          </span>
        </div>
      </div>
      </div>
      <xsl:apply-templates select="." mode="pageFooter" />
    </body>
  </xsl:template>

  <xsl:template name="logtime-tostring">
    <xsl:param name="logtime" />
    <xsl:variable name="timezoneinfo" select="/response/Person/TimeZoneInfoString" />
    
    <xsl:value-of select="user:LogTimeToDateTimeString($logtime, $timezoneinfo)"/>
  </xsl:template>

</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->