<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:user="urn:my-scripts">
  <xsl:import href="common.xsl" />
  <xsl:import href="../xslt-scripts.xsl"/>
  <!--  media-type="text/xml; charset=UTF-8"  -->
  <xsl:output method="xml" version="1.0" doctype-public="-//WAPFORUM//DTD XHTML Mobile 1.0//EN" doctype-system="http://www.wapforum.org/DTD/xhtml-mobile10.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="no" media-type="text/xml" />
  <xsl:template match="/">
    <xsl:apply-templates select="/response" mode="session-closed" />
  </xsl:template>

  <xsl:template match="response" mode="session-closed">
    <html>
      <xsl:apply-templates select="." mode="header" />
      <body>
        <div id="cnt">
          <xsl:apply-templates select="." mode="mobilePageHeader" />
          <div class="content">
            <h1>
              <xsl:value-of select="$locale/data[@name='sessionNotCreatedMessage']/value" />
            </h1>
            
            <p>
              <a>
                <xsl:attribute name="href">
                  <xsl:choose>
                    <xsl:when test="/response/@ApplicationPath = ''">/mobile/</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="/response/@ApplicationPath"/>/mobile/
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:value-of select="$locale/data[@name='gotoStartPage']/value" />
              </a>
            </p>
          </div>
        </div>
        <xsl:apply-templates select="." mode="pageFooter" />
      </body>
    </html>
  </xsl:template>
  
</xsl:stylesheet>