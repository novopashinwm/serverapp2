<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:doc="http://ufin.online/2016/documents">
  <xsl:import href="common.xsl" />
  <xsl:import href="dialer.xsl" />
  <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <html>
      <xsl:apply-templates select="./response" mode="header" />
      <body style="overflow:auto;">
        <link type="text/css" rel="stylesheet" href="{./@ApplicationPath}/css/options.css"/>
        
        <xsl:apply-templates select="./response" mode="pageHeader">
          <xsl:with-param name="selectedTab" select="'settings'" />
        </xsl:apply-templates>
        <xsl:apply-templates select="./response" mode="subTabs" />
        <xsl:apply-templates select="." mode="pageFooter" />
          <xsl:call-template name="websso"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="response" mode="subTabs">
    <div id="pageContent">
      <div class="sub-tabs">
        <ul id="objectTabs">
          <li id="contactsSelector" class="selected">
            <a href="javascript: void(0);">
              <!--<xsl:value-of select="$locale/data[@name='TrackingObjectsTitle']/value" />-->
              <xsl:text>Контакты</xsl:text>
            </a>
          </li>
          <li id="securitySelector">
            <a href="javascript: void(0);">
              <xsl:text>Безопасность</xsl:text>
            </a>
          </li>
          <li id="personalDataSelector">
            <a href="javascript: void(0);">
              <xsl:text>Личные данные</xsl:text>
            </a>
          </li>
          <li id="otherSelector">
            <a href="javascript: void(0);">
              <xsl:text>Прочие</xsl:text>
            </a>
          </li>
        </ul>
      </div>
      <div class="sub-content">
        <div id="contactsContent" class="ontop content sub-content-page">
          <fieldset>
            <legend>
              <xsl:text>contactsContent</xsl:text>
            </legend>
            <xsl:call-template name="contactsContentPhones" />
            <xsl:call-template name="contactsContentEmails" />
          </fieldset>
        </div>
        <div id="securityContent" class="ontop content sub-content-page" style="display:none">
          <xsl:text>securityContent</xsl:text>
        </div>
        <div id="personalDataContent" class="sub-content-page" style="display:none">
          <xsl:text>personalDataContent</xsl:text>
        </div>
        <div id="otherContent" class="sub-content-page" style="display:none">
          <xsl:text>personotherContentalDataContent</xsl:text>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="contactsContentEmails">
    <input type="button" value="Добавить email" id="addNewEmail" onclick="UserSettings.addNewEmail()"/>
    <table class="short" id="contentEmailTable">
      <thead>
        <tr>
          <th>Id</th>
          <th>Value</th>
          <th>IsConfirmed</th>
          <th>Control</th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="/response/Person/Emails/Email">
          <tr>
            <td>
              <xsl:value-of select="Id"/>
            </td>
            <td>
              <xsl:value-of select="Value"/>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="IsConfirmed = 'true'">
                  <xsl:text>OK</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>Cancel</xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td>
              <input type="button" value="remove" id="emailRemover{Id}" onclick="UserSettings.removeEmail({id})" />
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template name="contactsContentPhones">
    <input type="button" value="Добавить номер" id="addNewPhone" onclick="UserSettings.addNewPhone()"/>
    <table class="short" id="contentPhoneTable">
      <thead>
        <tr>
          <th>Id</th>
          <th>Value</th>
          <th>IsConfirmed</th>
          <th>Control</th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="/response/Person/Phones/Phone">
          <tr>
            <td>
              <xsl:value-of select="Id"/>
            </td>
            <td>
              <xsl:value-of select="Value"/>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="IsConfirmed = 'true'">
                  <xsl:text>OK</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>Cancel</xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td>
              <input type="button" value="remove" id="phoneRemover{Id}" onclick="UserSettings.removePhone({id})" />
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

</xsl:stylesheet>