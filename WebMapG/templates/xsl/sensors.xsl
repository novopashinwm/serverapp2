﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
  <xsl:import href="common.xsl" />
  <xsl:import href="trackingObject_G.xsl" />
  <xsl:import href="uielements.xsl" />
  <xsl:import href="dateTime.xsl" />
  <xsl:import href="charts.xsl" />
  <xsl:import href="geozone.xsl" />
  <xsl:output method="xml" indent="yes"/>
  <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="//@template = 'sensorsPage'">
        <xsl:apply-templates select="." mode="sensorsPage" />
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="response" mode="sensorsPage">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:variable name="title" select="concat(./@objectName, ' - ', $locale/data[@name='sensors']/value)" />
      <head>
        <xsl:apply-templates select="." mode="headContent">
          <xsl:with-param name="title" select="concat($title, ' - ', $locale/data[@name='ApplicationMainTitle']/value)" />
        </xsl:apply-templates>

        <script type="text/javascript">
            UFIN.pageType = 'sensors';
          <xsl:text>var dataObjects = {</xsl:text>
          <xsl:text>vehicle:</xsl:text>
          <xsl:value-of select="./@vehicleJSON"/>
          <xsl:text>,</xsl:text>

          <xsl:text>sensorMaps:</xsl:text>
          <xsl:value-of select="./@sensorsJSON"/>
          <xsl:text>,</xsl:text>

          <xsl:text>controllerInputs:</xsl:text>
          <xsl:value-of select="./@controllerInputsJSON"/>
          <xsl:text>,</xsl:text>

          <xsl:text>legends:</xsl:text>
          <xsl:value-of select="./@legendsJSON"/>

          <xsl:text>}</xsl:text>


        </script>
      </head>
      <body style="overflow:auto;">
          <xsl:if test="/response/@color-schema = 'blue' ">
              <xsl:attribute name="class">
                  blue-schema
              </xsl:attribute>
          </xsl:if>
          <xsl:if test="/response/@WebSSO">
              <xsl:attribute name="class">websso</xsl:attribute>
          </xsl:if>
          <xsl:call-template name="websso"/>
        <xsl:call-template name="commonBodyIncludes" />
        <xsl:apply-templates select="." mode="pageHeader">
          <xsl:with-param name="selectedTab" select="'objects'" />
        </xsl:apply-templates>
        <xsl:call-template name="commonPopupMessage" />
        <div id="additionalPageContent">
          <div class="main-content no-margin">
            <div class="content-header">
              <xsl:value-of select="$title" />              
            </div>
              <div class="clear"></div>
          </div>
        </div>
        
          <xsl:call-template name="requireJS" />

          <xsl:apply-templates select="." mode="pageFooter" />
          
      </body>
    </html>
  </xsl:template>
  
</xsl:stylesheet>
