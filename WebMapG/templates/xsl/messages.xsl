<?xml version='1.0'?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:user="urn:my-scripts">
	<xsl:import href="common.xsl" />
	<xsl:import href="uielements.xsl" />
  <xsl:import href="xslt-scripts.xsl"/>
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="no" />


<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//@template = 'messageList'">
			<xsl:apply-templates select="." mode="messageList" />
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="response" mode="messageList">
	<div style="padding:5px;">
    <xsl:apply-templates select="./messages/message" mode="chat">
      <xsl:sort order="descending" select="Time" />
    </xsl:apply-templates>
	</div>
</xsl:template>

<xsl:template match="message" mode="chat">
	<div style="width:100%;clear:both; margin-bottom:20px">
    <div style="float:left;width:100%;">
      <xsl:choose>
        <xsl:when test="./Type = 'Incoming'">
          <xsl:value-of select="$locale/data[@name='messageFrom']/value" />
          <xsl:text>: </xsl:text>
          <xsl:choose>
            <xsl:when test="./Source and ./Source != ''">
              <xsl:value-of select="./Source" />
            </xsl:when>
            <xsl:otherwise>
              &lt;<xsl:value-of select="$locale/data[@name='unknown']/value" />&gt;
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="./Type = 'Outgoing'">
          <xsl:value-of select="$locale/data[@name='messageTo']/value" />
          <xsl:text>: </xsl:text>
          <xsl:choose>
            <xsl:when test="./Destination and ./Destination != ''">
              <xsl:value-of select="./Destination" />
            </xsl:when>
            <xsl:otherwise>
              &lt;<xsl:value-of select="$locale/data[@name='unknown']/value" />&gt;
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </div>
    <div style="float:left;width:20%;">
			<xsl:variable name="ImageUrl">
				<xsl:choose>
					<xsl:when test="./Type = 'incoming'">
						<xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/message-incoming.png')" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat(/response/@ApplicationPath, '/img/object-car/message-outgoing.png')" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="messageTypeText">
				<xsl:choose>
					<xsl:when test="./Type = 'incoming'">
						<xsl:value-of select="$locale/data[@name='incomingMessage']/value" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$locale/data[@name='outgoingMessage']/value" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="formatImage">
				<xsl:with-param name="browser" select="/response/@browser" />
				<xsl:with-param name="width" select="'18'" />
				<xsl:with-param name="height" select="'18'" />
				<xsl:with-param name="src" select="$ImageUrl" />
				<xsl:with-param name="title" select="$messageTypeText" />
			</xsl:call-template>
		</div>
		<div style="float:left;width:80%;">
			<xsl:value-of select="./MessageBody" />
		</div>
		<div style="clear:both;width:100%">
			<!--span class="grey"><xsl:value-of select="$locale/data[@name='dispatched']/value" />: <xsl:value-of select="./Time" /></span-->
			<span class="grey">
        <xsl:value-of select="$locale/data[@name='dispatched']/value" />
        <xsl:text>: </xsl:text>
        <xsl:value-of select="user:DateTimeToString(./Time)" />
      </span>
		</div>
	</div>
</xsl:template>


</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->