<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:chart="http://web.ufin.online" xmlns:v="urn:schemas-microsoft-com:vml">
<xsl:import href="dateTime.xsl" />

<!--
AS: здесь полнейший пиздец. Все переделать нахуй
-->

<!-- AS: рисование оси X -->
<xsl:template name="chart:xaxis">
	<xsl:param name="min" select="0" />
	<xsl:param name="max" select="100" />
	<xsl:param name="offsetX" select="0" />
	<xsl:param name="offsetY" select="0" />
	<xsl:param name="width" select="500" />
	<xsl:param name="height" select="500" />
	<xsl:param name="mjTicks" select="10" />
	<xsl:param name="minTicks" select="4" />
	<xsl:param name="mjExtent" select="4" />
	<xsl:param name="label" select="true()" />
	<xsl:param name="context" />
	<xsl:param name="values" />
	<xsl:param name="valueMultiplier" select="1" />
	<xsl:param name="axisName" />
	<xsl:param name="browser" />

	<xsl:variable name="range" select="$max - $min" />
	<xsl:variable name="scale" select="$width div $range" />

	<xsl:choose>
		<xsl:when test="contains($browser, 'IE')">
			<v:line from="{$min}, 0" to="{$max}, 0" />
			<xsl:call-template name="chart:ticks">
				<xsl:with-param name="xMajor1" select="$min" />
				<xsl:with-param name="yMajor1" select="$height" />
				<xsl:with-param name="xMajor2" select="$min" />
				<xsl:with-param name="yMajor2" select="-$mjExtent" />
				<xsl:with-param name="labelMajor" select="$label" />
				<xsl:with-param name="freq" select="$minTicks" />
				<xsl:with-param name="nTicks" select="$mjTicks * $minTicks + 1" />
				<xsl:with-param name="xIncr" select="($max - $min) div ($mjTicks * $minTicks)" />
				<xsl:with-param name="scale" select="1 div $scale" />
				<xsl:with-param name="values" select="$values" />
				<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
				<xsl:with-param name="browser" select="$browser" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<svg:g transform="translate({$offsetX}, {$offsetY + $height}) scale({$scale}, -1) translate({$min}, 0)" stroke="black">
				<svg:line x1="{$min}" y1="0" x2="{$max}" y2="0" stroke-width="2" />
			</svg:g>
			<svg:g transform="translate({$offsetX}, {$offsetY + $height}) scale({$scale}, -1) translate({$min}, 0)">
				<xsl:call-template name="chart:ticks">
					<xsl:with-param name="xMajor1" select="$min" />
					<xsl:with-param name="yMajor1" select="$height" />
					<xsl:with-param name="xMajor2" select="$min" />
					<xsl:with-param name="yMajor2" select="-$mjExtent" />
					<xsl:with-param name="labelMajor" select="$label" />
					<xsl:with-param name="freq" select="$minTicks" />
					<xsl:with-param name="nTicks" select="$mjTicks * $minTicks + 1" />
					<xsl:with-param name="xIncr" select="($max - $min) div ($mjTicks * $minTicks)" />
					<xsl:with-param name="scale" select="1 div $scale" />
					<xsl:with-param name="values" select="$values" />
					<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
					<xsl:with-param name="browser" select="$browser" />
				</xsl:call-template>
			</svg:g>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: рисование оси Y -->
<xsl:template name="chart:yaxis">
	<xsl:param name="min" select="0" />
	<xsl:param name="max" select="100" />
	<xsl:param name="offsetX" select="0" />
	<xsl:param name="offsetY" select="0" />
	<xsl:param name="width" select="500" />
	<xsl:param name="height" select="500" />
	<xsl:param name="mjTicks" select="10" />
	<xsl:param name="minTicks" select="4" />
	<xsl:param name="mjExtent" select="4" />
	<xsl:param name="label" select="true()" />
	<xsl:param name="context" />
	<xsl:param name="values" />
	<xsl:param name="valueMultiplier" select="1" />
	<xsl:param name="axisName" />
	<xsl:param name="browser" />

	<xsl:variable name="range" select="$max - $min" />
	<xsl:variable name="scale" select="$height div $range" />

	<xsl:choose>
		<xsl:when test="contains($browser, 'IE')">
			<v:line from="0, {$min}" to="0, {$max}" />
			<xsl:call-template name="chart:ticks">
				<xsl:with-param name="xMajor1" select="-$mjExtent" />
				<xsl:with-param name="yMajor1" select="$min" />
				<xsl:with-param name="xMajor2" select="$width" />
				<xsl:with-param name="yMajor2" select="$min" />
				<xsl:with-param name="labelMajor" select="$label" />
				<xsl:with-param name="freq" select="$minTicks" />
				<xsl:with-param name="nTicks" select="$mjTicks * $minTicks + 1" />
				<xsl:with-param name="yIncr" select="($max - $min) div ($mjTicks * $minTicks)" />
				<xsl:with-param name="scale" select="1 div $scale" />
				<xsl:with-param name="values" select="$values" />
				<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
				<xsl:with-param name="browser" select="$browser" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<svg:g transform="translate({$offsetX}, {$offsetY + $height}) scale(1, {-$scale}) translate(0, {-$min})" stroke="black">
				<svg:line x1="0" y1="{$min}" x2="0" y2="{$max}" stroke-width="2" />
			</svg:g>
			<svg:g transform="translate({$offsetX}, {$offsetY + $height}) scale(1, {-$scale}) translate(0, {-$min})">
				<xsl:call-template name="chart:ticks">
					<xsl:with-param name="xMajor1" select="-$mjExtent" />
					<xsl:with-param name="yMajor1" select="$min" />
					<xsl:with-param name="xMajor2" select="$width" />
					<xsl:with-param name="yMajor2" select="$min" />
					<xsl:with-param name="labelMajor" select="$label" />
					<xsl:with-param name="freq" select="$minTicks" />
					<xsl:with-param name="nTicks" select="$mjTicks * $minTicks + 1" />
					<xsl:with-param name="yIncr" select="($max - $min) div ($mjTicks * $minTicks)" />
					<xsl:with-param name="scale" select="1 div $scale" />
					<xsl:with-param name="values" select="$values" />
					<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
					<xsl:with-param name="browser" select="$browser" />
				</xsl:call-template>
			</svg:g>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<!-- AS: подписи и деления осей -->
<xsl:template name="chart:ticks">
	<xsl:param name="xMajor1" />
	<xsl:param name="yMajor1" />
	<xsl:param name="xMajor2" />
	<xsl:param name="yMajor2" />
	<xsl:param name="labelMajor" />
	<xsl:param name="freq" />
	<xsl:param name="nTicks" select="0" />
	<xsl:param name="xIncr" select="0" />
	<xsl:param name="yIncr" select="0" />
	<xsl:param name="i" select="0" />
	<xsl:param name="scale" />
	<xsl:param name="values" />
	<xsl:param name="valueMultiplier" select="1" />
	<xsl:param name="browser" />

	<xsl:if test="$i &lt; $nTicks">
		<xsl:choose>
			<xsl:when test="$i mod $freq = 0">
				<xsl:choose>
					<xsl:when test="contains($browser, 'IE')">
						<v:line from="{$xMajor1}, {$yMajor1}" to="{$xMajor2}, {$yMajor2}" />
					</xsl:when>
					<xsl:otherwise>
						<svg:g stroke="#c0c0c0">
							<svg:line x1="{$xMajor1}" y1="{$yMajor1}" x2="{$xMajor2}" y2="{$yMajor2}" stroke-width="1">
							</svg:line>
						</svg:g>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$labelMajor">
					<xsl:choose>
						
						<!-- AS: ось X -->
						<xsl:when test="$xIncr &gt; 0">
							<xsl:choose>
								<xsl:when test="contains($browser, 'IE')">
								</xsl:when>
								<xsl:otherwise>
									<svg:text x="{$xMajor1}" y="{$yMajor2 + 15}" transform="translate({$xMajor1}, {$yMajor2}) scale({$scale}, -1) translate({-$xMajor1}, {-$yMajor2})">
										<xsl:choose>
											<xsl:when test="$values">
												<xsl:apply-templates select="$values[1]" mode="chart" />
											</xsl:when>
										</xsl:choose>
									</svg:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						
						<!-- AS: Ось Y -->
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="contains($browser, 'IE')">
								</xsl:when>
								<xsl:otherwise>
									<svg:text x="{$xMajor1 - 40}" y="{$yMajor1}" transform="translate({$xMajor1}, {$yMajor1}) scale({$scale}, -1) translate({-$xMajor1}, {-$yMajor1})">
										<!--xsl:choose>
											<xsl:when test="$values">
												<xsl:value-of select="format-number(($yMajor1 * $valueMultiplier), '#0.0')" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="format-number(($yMajor1 * $valueMultiplier), '#0.0')" />
											</xsl:otherwise>
										</xsl:choose-->
										<xsl:value-of select="format-number(($yMajor1 * $valueMultiplier), '#0.0')" />
									</svg:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:when>
		</xsl:choose>

		<xsl:choose>
			<xsl:when test="$values">
				<xsl:call-template name="chart:ticks">
					<xsl:with-param name="xMajor1" select="$xMajor1 + $xIncr" />
					<xsl:with-param name="yMajor1" select="$yMajor1 + $yIncr" />
					<xsl:with-param name="xMajor2" select="$xMajor2 + $xIncr" />
					<xsl:with-param name="yMajor2" select="$yMajor2 + $yIncr" />
					<xsl:with-param name="labelMajor" select="$labelMajor" />
					<xsl:with-param name="freq" select="$freq" />
					<xsl:with-param name="nTicks" select="$nTicks" />
					<xsl:with-param name="xIncr" select="$xIncr" />
					<xsl:with-param name="yIncr" select="$yIncr" />
					<xsl:with-param name="i" select="$i + 1" />
					<xsl:with-param name="scale" select="$scale" />
					<xsl:with-param name="values" select="$values[position() != 1]" />
					<!--xsl:with-param name="values" select="$values" /-->
					<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
					<xsl:with-param name="browser" select="$browser" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="chart:ticks">
					<xsl:with-param name="xMajor1" select="$xMajor1 + $xIncr" />
					<xsl:with-param name="yMajor1" select="$yMajor1 + $yIncr" />
					<xsl:with-param name="xMajor2" select="$xMajor2 + $xIncr" />
					<xsl:with-param name="yMajor2" select="$yMajor2 + $yIncr" />
					<xsl:with-param name="labelMajor" select="$labelMajor" />
					<xsl:with-param name="freq" select="$freq" />
					<xsl:with-param name="nTicks" select="$nTicks" />
					<xsl:with-param name="xIncr" select="$xIncr" />
					<xsl:with-param name="yIncr" select="$yIncr" />
					<xsl:with-param name="i" select="$i + 1" />
					<xsl:with-param name="scale" select="$scale" />
					<xsl:with-param name="valueMultiplier" select="$valueMultiplier" />
					<xsl:with-param name="browser" select="$browser" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>

<xsl:template match="date" mode="chart">
	<xsl:call-template name="getTimeOnly">
		<xsl:with-param name="dateAndTime" select="." />
		<xsl:with-param name="delimiter" select="'T'" />
	</xsl:call-template>
</xsl:template>

<xsl:template match="dateTime" mode="chart">
	<xsl:call-template name="getTimeOnly">
		<xsl:with-param name="dateAndTime" select="." />
		<xsl:with-param name="delimiter" select="'T'" />
	</xsl:call-template>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no" ><SourceSchema srcSchemaPath="..\..\..\..\..\temp\page\page.xmlobjects.xsl.xml" srcSchemaRoot="response" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/></MapperInfo><MapperBlockPosition><template name="chart:xaxis"></template><template name="chart:ticks"></template></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->