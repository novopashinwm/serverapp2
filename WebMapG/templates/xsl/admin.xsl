﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <xsl:import href="common.xsl" />
    <xsl:import href="trackingObject_G.xsl" />
    <xsl:import href="uielements.xsl" />
    <xsl:import href="dateTime.xsl" />
    <xsl:import href="charts.xsl" />
    <xsl:import href="geozone.xsl" />
    <xsl:output method="xml" indent="yes"/>
    <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//@template = 'adminPage'">
                <xsl:apply-templates select="." mode="adminPage" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:variable name="path" select="/response/@ApplicationPath" />
    
    <xsl:template match="response" mode="adminPage">
        <html class="page_admin" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <script>
                    //В новом интерфейса разрешаем отображать старую страницу только внутри фрейма.
                    //Если это не так - используем правильную ссылку.
                    if (window.top === window) location.replace('map.aspx#page/admin');
                </script>
                <xsl:apply-templates select="." mode="headContent" />

                <script type="text/javascript">
                    UFIN.pageType = 'admin';
                    UFIN.customerType = '<xsl:value-of select="/response/Departments/Department/Type" />'.toLowerCase();

                </script>         
        
            </head>

            <body style="overflow:auto;">
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
          
                <xsl:call-template name="commonBodyIncludes" />
  
                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'admin'" />
                </xsl:apply-templates>

                <xsl:variable name="menuItem">
                    <xsl:choose>
                        <xsl:when test="./@menuItem and ./@menuItem != ''">
                            <xsl:value-of select="./@menuItem"/>
                        </xsl:when>
                        <xsl:when test="./systemRights[right = 'SecurityAdministration']">
                            <xsl:value-of select="'operatorRights'"/>
                        </xsl:when>
                        <xsl:when test="./systemRights[right = 'ServiceManagement']">
                            <xsl:value-of select="'newCustomer'"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:variable>

                <div class="b-content">
                    <div class="aside-block sub" id="tabs" >
                        <ul>
                            <li id="menuItem-operatorRights">
                                <a href="#user">
                                    <xsl:value-of select="$locale/data[@name='adminOperators']/value" />
                                </a>
                            </li>
                            <li>
                                <a href="#object">
                                    <xsl:value-of select="$locale/data[@name='adminObjects']/value" />
                                </a>
                            </li>
                        </ul>
                        <div class="" id="user">
                            <xsl:call-template name="tab">
                                <xsl:with-param name="menuItem">
                                    <xsl:value-of select="$menuItem"/>
                                </xsl:with-param>
                                <xsl:with-param name="tabType">user</xsl:with-param>
                            </xsl:call-template>
                        </div>
                        <div class="" id="object">
                            <xsl:call-template name="tab">
                                <xsl:with-param name="menuItem">
                                    <xsl:value-of select="$menuItem"/>
                                </xsl:with-param>
                                <xsl:with-param name="tabType">object</xsl:with-param>
                            </xsl:call-template>
                        </div>
                    </div>
                    <div class="main-content tabs">
                        <div id="content-operatorRightsEdit" class="rightColumnN tabs__tab">
                            <div class="content-rights-label">
                                <xsl:value-of select="$locale/data[@name='noOperatorSelected']/value" />
                            </div>
                            <h1 class="name"></h1>
                            <div id="commandPanel" style="display:none;">
                                <div class="sub-tabs">
                                    <ul class="pulled-left">
                                        <li id="aVehiclesTab" class="settingsTab">
                                            <a href="#">
                                                <xsl:value-of select="$locale/data[@name='vehiclesTab']/value" />
                                            </a>
                                        </li>
                                        <li id="aZonesTab" class="settingsTab">
                                            <a href="#">
                                                <xsl:value-of select="$locale/data[@name='zonesTab']/value" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="operMessages"></div>
                            </div>
                            <div id="no-operator-selected-pane">
                                <p>
                                    <xsl:value-of select="$locale/data[@name='noOperatorSelected']/value" />
                                </p>
                            </div>
                            <div id="common-pane"></div>
                            <div id="main-pane"></div>
                        </div>
                        <div id="content-objectsRightsEdit" class="rightColumnN tabs__tab">
                            <div class="content-rights-label">
                                <xsl:value-of select="$locale/data[@name='noObjectSelected']/value" />
                            </div>
                            <h1 class="name"></h1>
                            <div id="no-object-selected-pane">
                                <p>
                                    <xsl:value-of select="$locale/data[@name='noObjectSelected']/value" />
                                </p>
                            </div>
                            <div id="common-pane2"></div>
                            <div id="main-pane2"></div>
                        </div>
                    </div>


                </div>
                <xsl:call-template name="addOperator"/>
                <xsl:call-template name="addFriend"/>
                <xsl:call-template name="commonPopupMessage" />
                <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />
                <!--xsl:call-template name="waitDialog" /-->   
            </body>
        </html>
    </xsl:template>

    <xsl:template name="tab">
        <xsl:param name="menuItem"/>
        <xsl:param name="tabType"/>
        <div class="sub-content-flex bigPad">
            <div id="content-operatorRights" class="sub-content-page">
                <div class="leftColumnN">
                    <div class="vert-menu-head">
                        <xsl:choose>
                            <xsl:when test="$tabType = 'user'">
                                <xsl:attribute name="id">admin-menu</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="id">object-menu</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                        <div class="loadInProgress"></div>
                    </div>
                </div>                   
                <div class="clear"></div>
            </div>

        </div>

    </xsl:template>
    
    <xsl:template name="addOperator">
        <div class="addOperator adding tpl" style="display: none;" title="{$locale/data[@name='newUser']/value}">
            <div class="popupAdmin">
                <form class="new-user" autocomplete="off">
                    <div class="inner">
                        <div class="controlBlock">
                            <div class="controlDescription" for="name">
                                <xsl:value-of select="$locale/data[@name='firstName']/value" />
                            </div>
                            <div class="control">
                                <input type="text" id="_operatorName" name="operatorName" autocomplete="off" class="inp"/>
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="controlDescription" for="login">
                                <xsl:value-of select="$locale/data[@name='OperatorLogin']/value" />
                            </div>
                            <div class="control">
                                <input type="text" id="_operatorLogin" name="operatorLogin" autocomplete="off" class="inp"/>
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="controlDescription" for="name">
                                <xsl:value-of select="$locale/data[@name='Password']/value" />
                            </div>
                            <div class="control">
                                <input type="password" id="_operatorPassword" name="operatorPassword" class="pass inp" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="controlDescription" for="login">
                                <xsl:value-of select="$locale/data[@name='ConfirmPassword']/value" />
                            </div>
                            <div class="control">
                                <input type="password" id="_confirmOperatorPassword" name="confirmOperatorPassword" class="rpass inp" autocomplete="off"/>
                            </div>
                        </div>
                        <p class="err_message"></p>
                        <div class="controlBlock">
                            <div class="control">
                                <button disabled="disabled" type="submit" class="button button_save btn">
                                    <xsl:value-of select="$locale/data[@name='create']/value" />
                                </button>
                            </div>
                        </div>
                    </div>    
                </form>
                
            </div>
        </div> 
    </xsl:template>
    
    <xsl:template name="addFriend">
        <div class="addFriend adding tpl" style="display: none;">
            <div class="popupAdmin">
                <div class="inner">
                    <div>
                        <input type="text" id="search" name="search" placeholder="{$locale/data[@name='searchPlaceholder']/value}" class="inp"/>
                        <button  class="btn icon search_button" title="{$locale/data[@name='search']/value}">
                            <span class="typcn typcn-zoom-outline"></span>
                        </button>
                    </div>
                    <p class="searchHint">
                        * <xsl:value-of select="$locale/data[@name='searchHint']/value" />
                    </p>
                    
                    <p class="results">
                        
                    </p>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>
