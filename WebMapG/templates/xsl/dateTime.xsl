<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="common.xsl" />
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="no" />

<!--
AS: в этом файле будут лежать шаблоны, работающие с датами.
В основном, форматирование дат для вывода на устройство
-->

<!-- AS: форматирование промежутков времени - TimeSpan.
TimeSpan ожидается в виде строки hh:mm:ss. Все, что нужно сделать - разбить строку на лексеммы с разделителем ':'
-->
<xsl:template name="formatTimeSpan">
	<xsl:param name="timeSpan" select="'00:00:00'" />
	<xsl:param name="position" select="0" />
	<xsl:param name="withSeconds" select="false" />
	
	<xsl:choose>
		<xsl:when test="not($timeSpan)" />
		<xsl:when test="$position = 0">
			<!-- AS: часы -->
			<xsl:variable name="hours">
				<xsl:value-of select="translate(substring-before($timeSpan, ':'), '0', '')" />
			</xsl:variable>
			<xsl:if test="string-length($hours) &gt; 0">
				<xsl:value-of select="$hours" /><xsl:text> </xsl:text><xsl:value-of select="$locale/data[@name='hourShort']/value" /><xsl:text> </xsl:text>
			</xsl:if>
		</xsl:when>
		<xsl:when test="$position = 1">
			<!-- AS: минуты -->
			<xsl:variable name="minutes">
				<xsl:value-of select="translate(substring-before($timeSpan, ':'), '0', '')" />
			</xsl:variable>
			<xsl:if test="string-length($minutes) &gt; 0">
				<xsl:value-of select="$minutes" /><xsl:text> </xsl:text><xsl:value-of select="$locale/data[@name='minuteShort']/value" /><xsl:text> </xsl:text>
			</xsl:if>
		</xsl:when>
		<xsl:when test="($position = 2) and ($withSeconds = false)">
			<!-- AS: секунды -->
			<xsl:variable name="seconds">
				<xsl:value-of select="translate($timeSpan, '0', '')" />
			</xsl:variable>
			<xsl:if test="string-length($seconds) &gt; 0">
				<xsl:value-of select="$seconds" /><xsl:text> </xsl:text><xsl:value-of select="$locale/data[@name='secondShort']/value" />
			</xsl:if>
		</xsl:when>
	</xsl:choose>
	<xsl:if test="$position &lt; 3">
		<xsl:call-template name="formatTimeSpan">
			<xsl:with-param name="timeSpan" select="substring-after($timeSpan, ':')" />
			<xsl:with-param name="position" select="$position + 1" />
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!-- AS:приведение даты в человеческий вид.
Смысл такой - если дата - сегодня, то выводится слово "сегодня" и время.
Иначе - дата и время. Время - без секунд.
Дата, которую нужно отформатировать, сравнивается с текущей, передаваемой параметром.
-->
<xsl:template name="time-width-day">
	<xsl:param name="current-date" />
	<xsl:param name="format-date" />
	<xsl:param name="dateEqualMessage" />
	<xsl:param name="lang" select="'ru-RU'" />
	<xsl:param name="format-date-string" select="'%d-%m-%y %h:%m'" />

	<xsl:variable name="cdate">
		<xsl:value-of select="substring-before($current-date, 'T')" />
	</xsl:variable>

	<xsl:variable name="fdate">
		<xsl:value-of select="substring-before($format-date, 'T')" />
	</xsl:variable>

	<xsl:variable name="ftime">
		<xsl:value-of select="substring-after($format-date, 'T')" />
	</xsl:variable>

	<xsl:variable name="d">
		<xsl:choose>
			<xsl:when test="$cdate = $fdate">
				<xsl:value-of select="$dateEqualMessage" />, <xsl:text />
			</xsl:when>
			<xsl:otherwise>
				<!--xsl:value-of select="$fdate" /-->
				<xsl:call-template name="dateToLocal2">
					<xsl:with-param name="date" select="$fdate" />
					<xsl:with-param name="lang" select="$lang" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="formattedTime">
		<xsl:call-template name="getTimeOnlyFromTime">
			<xsl:with-param name="time" select="$ftime" />
		</xsl:call-template>
	</xsl:variable>

	<xsl:value-of select="concat($d, ' ', $formattedTime)" />
</xsl:template>

<!-- AS: разбиение строки на лексеммы.
Спиздил шаблон у Села Монтано, который в свою очередь пизданул его у Дженни Теннисон.
Я заплатил бабло за книжку мистера Монтано, так что считаю, что могу использовать этот ебучий шаблон как мне будет угодно.
Тем более, этот шаблон висит в публичном доступе. Короче, ниипет.
Мне нахуй не надо иметь полные понты, поэтому остается лишь основной шаблон, который режет строку на лексемы.
P.S. Специально для Тютюкина: вышли, блять, фотку!
-->
<xsl:template name="tokenize-delimiters">
	<xsl:param name="string" />
	<xsl:param name="delimiters" />
	<xsl:param name="last-delimit" />

	<xsl:variable name="delimiter" select="substring($delimiters, 1, 1)" />
	<xsl:choose>
		<xsl:when test="not($delimiter)">
			<!-- AS:, все, строку разобрали -->
			<token><xsl:value-of select="$string" /></token>
		</xsl:when>
		<xsl:when test="contains($string, $delimiter)">
			<xsl:if test="not(starts-with($string, $delimiter))">
				<xsl:call-template name="tokenize-delimiters">
					<xsl:with-param name="string" select="substring-before($string, $delimiter)" />
					<xsl:with-param name="delimiters" select="substring($delimiter, 2)" />
				</xsl:call-template>
			</xsl:if>
			<xsl:call-template name="tokenize-delimiters">
				<xsl:with-param name="string" select="substring-after($string, $delimiter)" />
				<xsl:with-param name="delimiters" select="$delimiter" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="tokenize-delimiters">
				<xsl:with-param name="string" select="$string" />
				<xsl:with-param name="delimiters" select="substring($delimiter, 2)" />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: Выдерем время из полной даты. Только часы и минуты -->
<xsl:template name="getTimeOnly">
	<xsl:param name="dateAndTime" />
	<xsl:param name="delimiter" select="' '" />
	<xsl:variable name="formattedOutput">
		<xsl:variable name="time">
			<xsl:value-of select="substring-after($dateAndTime, $delimiter)" />
		</xsl:variable>
		<xsl:value-of select="substring($time, 1, 5)" />
	</xsl:variable>
	<xsl:value-of select="$formattedOutput" />
</xsl:template>

<!-- AS: Выдерем время из полного времени. Только часы и минуты -->
<xsl:template name="getTimeOnlyFromTime">
	<xsl:param name="time" />
	<xsl:param name="delimiter" select="':'" />
	<xsl:variable name="formattedOutput">
		<xsl:value-of select="substring($time, 1, string-length($time) - 3)" />
	</xsl:variable>
	<xsl:value-of select="$formattedOutput" />
</xsl:template>


<!-- AS: Выдерем время из полной даты. Только часы и минуты -->
<xsl:template name="getDateOnly">
	<xsl:param name="dateAndTime" />
	<xsl:param name="delimiter" select="'T'" />
	<!--xsl:variable name="formattedOutput">
		<xsl:variable name="time">
			<xsl:value-of select="substring-after($dateAndTime, $delimiter)" />
		</xsl:variable>
		<xsl:value-of select="substring($time, 1, string-length($time) - 3)" />
	</xsl:variable>
	<xsl:value-of select="$formattedOutput" /-->
	<xsl:value-of select="substring-before($dateAndTime, $delimiter)" />
</xsl:template>

<!--
AS: возвращает строку в формате yyy-mm-ddThh:mm:ss из строки, например, такой: yyy-mm-ddThh:mm:ss+hh:00
Короче, отрезает все, что после '+'
-->
<xsl:template name="prepare-date-time-in-utc">
	<xsl:param name="utcString" />
	<xsl:if test="$utcString">
		<xsl:value-of select="substring-before($utcString, '+')" />
	</xsl:if>
</xsl:template>


<!--
	AS: переводим дату в локальный формат на основе текущего выбранного языка.
	Дата в формате yyyy-mm-dd
-->
<xsl:template name="dateToLocal2">
	<xsl:param name="lang" select="'ru-RU'" />
	<xsl:param name="date" />
	<xsl:param name="delimiter" select="'-'" />

	<xsl:choose>
		<xsl:when test="$lang = 'ru-RU'">
			<xsl:call-template name="dateToRu">
				<xsl:with-param name="date" select="$date" />
				<xsl:with-param name="delimiter" select="$delimiter" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="$lang = 'en-US'">
			<xsl:call-template name="dateToEn">
				<xsl:with-param name="date" select="$date" />
				<xsl:with-param name="delimiter" select="$delimiter" />
			</xsl:call-template>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<!--
	AS: дату в русский формат, дата в формате yyyy-mm-dd
-->
<xsl:template name="dateToRu">
	<xsl:param name="date" />
	<xsl:param name="delimiter" select="'-'" />
	<xsl:variable name="year" select="substring-before($date, $delimiter)" />
	<xsl:variable name="rest" select="substring-after($date, $delimiter)" />
	<xsl:variable name="month" select="substring-before($rest, $delimiter)" />
	<xsl:variable name="day" select="substring-after($rest, $delimiter)" />

	<xsl:value-of select="concat($day, '.', $month, '.', $year)" />
</xsl:template>

<!--
	AS: дату в ангийский формат, дата в формате yyyy-mm-dd
-->
<xsl:template name="dateToEn">
	<xsl:param name="date" />
	<xsl:param name="delimiter" select="'-'" />

	<xsl:value-of select="$date" />
</xsl:template>

<!-- AS: конвертим дату в локаль с помощью JavaScript -->
<xsl:template name="dateToLocal">
	<xsl:param name="dateAndTime" />
	<xsl:param name="delimiter" select="'T'" />
	<xsl:variable name="dateOnly">
		<xsl:call-template name="getDateOnly">
			<xsl:with-param name="dateAndTime" select="$dateAndTime" />
			<xsl:with-param name="delimiter" select="$delimiter" />
		</xsl:call-template>
	</xsl:variable>
	<script type="text/javascript">
		myDateExtension.dateToLocalString(myDateExtension.utcToLocal('<xsl:value-of select="$dateOnly" />'));
	</script>
</xsl:template>


<!-- AS: минимальная дата, устанавливаемая при инициализации переменной типа DateTime в Microsoft.NET Framework
	Если нужна проверка, указана ли дата, юзаем эту переменную
-->
<xsl:variable name="minDateTime" select="'0001-01-01T00:00:00'" />


<!-- AS: дни недели возвращаем -->
<xsl:template match="Monday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='monday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='monday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Tuesday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='tuesday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='tuesday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Wednesday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='wednesday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='wednesday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Thursday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='thursday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='thursday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Friday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='friday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='friday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Saturday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='saturday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='saturday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="Sunday">
	<xsl:param name="useShortNames" select="false()" />
	<xsl:choose>
		<xsl:when test="$useShortNames = true()">
			<xsl:value-of select="$locale/weekdays/short/data[@name='sunday']/value" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$locale/weekdays/full/data[@name='sunday']/value" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->