<?xml version='1.0'?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

<!-- AS: шаблон с апплетом-звонилкой -->
<xsl:template name="dialerApplet">
	<xsl:param name="phone" select="'+79055255076'" />
	<xsl:param name="archive" select="'./applets/scallmonitor.jar,./applets/scomm.jar'" />
	<xsl:param name="isHidden" select="true()" />

	<xsl:variable name="height">
		<xsl:choose>
			<xsl:when test="$isHidden = true()">0</xsl:when>
			<xsl:otherwise>270</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<applet code="NewApplet.class" archive="{$archive}" width="250" height="{$height}" name="dialer">
		<param name="phonenumber" value="{$phone}" />
	</applet>
</xsl:template>

<xsl:template name="dialer">
	<xsl:param name="isHidden" select="true()" />
	<xsl:param name="phone" select="'+79055255076'" />
	<xsl:param name="archive" select="'./applets/scallmonitor.jar,./applets/scomm.jar'" />
	<xsl:param name="position" select="'absolute'" />

	<xsl:if test="//@useDialer = 'true'">
		<xsl:variable name="divStyle">
			<xsl:choose>
				<xsl:when test="$isHidden = true()">height:25px;</xsl:when>
				<xsl:otherwise>height:290px;</xsl:otherwise>
			</xsl:choose>
			margin:0;padding:0;
			position:absolute;
			background-color:#c0c0c0;
			border:1px solid #999;
			width:260px;
			float:hidden;
			<xsl:choose>
				<xsl:when test="$position = 'absolute'">position:absolute;</xsl:when>
				<xsl:otherwise>position:relative;</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div style="{$divStyle}" id="dialerContainer" class="ontop">
			<div style="width:100%;">
				<div style="width:80%;float:left;"><span style="padding:2px;"><strong><xsl:value-of select="$locale/data[@name='dialer']/value" /></strong></span></div>
				<div style="width:20%;float:left;" class="ride-side">
					<xsl:variable name="toggleClass">
						<xsl:choose>
							<xsl:when test="$isHidden = true()">vertical-show</xsl:when>
							<xsl:otherwise>vertical-hide</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<span class="toggle {$toggleClass}" onclick="PhoneDialer.showHideDialer(event)">&#160;</span>
				</div>
			</div>
			<div style="padding:2px;">
				<xsl:call-template name="dialerApplet">
					<xsl:with-param name="phone" select="$phone" />
					<xsl:with-param name="archive" select="$archive" />
					<xsl:with-param name="isHidden" select="$isHidden" />
				</xsl:call-template>
			</div>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template match="response" mode="dialer">
	<xsl:param name="isHidden" select="true()" />

	<xsl:if test="./@loggedUser != 'guest'">
		<xsl:call-template name="dialer">
			<xsl:with-param name="isHidden" select="$isHidden" />
		</xsl:call-template>
	</xsl:if>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\page\page.xmlsettings.xsl_1.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition><template name="dialerApplet"></template></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->