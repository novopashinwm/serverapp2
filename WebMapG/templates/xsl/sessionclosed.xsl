<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="common.xsl" />
	<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="/response" mode="session-closed" />
  </xsl:template>

  <xsl:template match="response" mode="session-closed">
    <html>
      <head>
        <title>
          <xsl:value-of select="$locale/data[@name='sessionNotCreatedMessage']/value" />
        </title>
        <xsl:apply-templates select="." mode="headContent" />
      </head>
      <body class="SessionIsClosed">
        <xsl:call-template name="common-page-header"></xsl:call-template>
        <div id="additionalPageContent">
          <div class="main-content">
            <h1>
              <xsl:value-of select="$locale/data[@name='sessionNotCreatedMessage']/value" />
            </h1>
            <p>
              <a>
                <xsl:attribute name="href">
                  <xsl:choose>
                    <xsl:when test="/response/@ApplicationPath = ''">/</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="/response/@ApplicationPath"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:value-of select="$locale/data[@name='gotoStartPage']/value" />
              </a>
            </p>
          </div>
        </div>
        <!--xsl:call-template name="fstPageFooter" /-->
        <!--xsl:call-template name="pageFooter" /-->
        <xsl:apply-templates select="." mode="pageFooter" />
          <xsl:call-template name="websso"/>
      </body>
    </html>
    </xsl:template>
    
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->