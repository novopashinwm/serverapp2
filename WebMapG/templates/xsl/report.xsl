<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
        <!ENTITY nbsp "&#160;">
        <!ENTITY hellip "&#8230;">
        ]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="urn:my-scripts"
                xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Interfaces.Data/Interfaces">


    <xsl:import href="xslt-scripts.xsl"/>
    <xsl:import href="common.xsl"/>
    <xsl:import href="dateTime.xsl"/>
    <xsl:import href="uielements.xsl"/>
    <xsl:import href="geozone.xsl"/>
    <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes"
                omit-xml-declaration="yes"/>

    <xsl:decimal-format name="ru" decimal-separator="," grouping-separator=" " NaN="-" percent="%" zero-digit="0"
                        digit="#" pattern-separator=";"/>

    <!-- Базовый вызов шаблона. -->
    <xsl:template match="/">
        <xsl:apply-templates select="./response" mode="reports">
            <xsl:with-param name="inlineResult" select="//InlineResult"/>
        </xsl:apply-templates>
    </xsl:template>

    <!-- Страница отчетов -->
    <xsl:template match="response" mode="reports">
        <xsl:param name="inlineResult"/>
        <html class="page_reports">
            <head>
                <script>
                    //В новом интерфейса разрешаем отображать старую страницу только внутри фрейма.
                    //Если это не так - используем правильную ссылку.
                    if (window.top === window) location.replace('map.aspx#page/reports');
                </script>
                <xsl:apply-templates select="." mode="headContent"/>


                <!--
                Файлы включены в единую таблицу стилей ufin.css и ufin_light.css

                Стилизация отчетов
                <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/reports.css" />
                <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/pickmeup.css" />
                -->


                <!-- Задает маску для контролов выбора дат параметров отчетов. -->
                <!-- Сериализует JSON бизнес-объекты на клиент. -->
                <script type="text/javascript">
                    UFIN.uiElem = 'true';
                    var dataObjects = {
                    <xsl:text>vehicles:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@vehiclesJSON">
                            <xsl:value-of select="@vehiclesJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            []
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>,</xsl:text>

                    <xsl:text>vehicleGroups:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@vehicleGroupsJSON">
                            <xsl:value-of select="@vehicleGroupsJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            []
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>,</xsl:text>

                    <xsl:text>geozones:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@geozonesJSON">
                            <xsl:value-of select="@geozonesJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            []
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>,</xsl:text>

                    <xsl:text>geozoneGroups:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@geozoneGroupsJSON">
                            <xsl:value-of select="@geozoneGroupsJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            []
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>,</xsl:text>

                    <xsl:text>legends:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@legendsJSON">
                            <xsl:value-of select="@legendsJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            []
                        </xsl:otherwise>
                    </xsl:choose>

                    }
                </script>

            </head>
            <body>
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>

                <xsl:call-template name="commonBodyIncludes"/>

                <xsl:call-template name="websso"/>
                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab">report</xsl:with-param>
                </xsl:apply-templates>
                <xsl:if test="./message">
                    <p style="margin:10px" class="message Warning">
                        <xsl:call-template name="replace">
                            <xsl:with-param name="str" select="./message/MessageBody"/>
                        </xsl:call-template>
                    </p>
                </xsl:if>

                <div class="b-content">
                    <div class="b-block pull-left">
                        <div class="b-block__header">
                            <xsl:value-of select="$locale/data[@name='reports']/value"/>
                        </div>
                        <div class="b-block__container">
                            <xsl:call-template name="reportMenuBuilder"/>
                            <xsl:call-template name="deliveryList"/>
                        </div>
                    </div>

                    <div class="main-content">
                        <div style="padding-left: 16px; padding-right: 16px;">
                            <!-- Параметры отчета. -->
                            <xsl:call-template name="reportParams"/>

                            <!-- Результат выполнения отчета. -->
                            <div id="reportResults">
                                <xsl:if test="$inlineResult">
                                    <xsl:copy-of select="$inlineResult"/>
                                </xsl:if>
                                <xsl:if test="//ReportFileName">
                                    <div id="reportResultsFile">
                                        <p>
                                            <xsl:value-of select="$locale/data[@name='reportWasBuilt']/value"/>
                                        </p>
                                        <p>
                                            <xsl:value-of
                                                    select="$locale/data[@name='fileDownloadingWillStartAutomaticly']/value"/>
                                        </p>
                                        <p>
                                            <xsl:value-of select="$locale/data[@name='OtherwiseUse']/value"/>
                                            <xsl:text> </xsl:text>
                                            <a href="{/response/@ApplicationPath}/Report.aspx?a=getReportFile&amp;file={//ReportFileName}"
                                               title="{$locale/data[@name='ReportFile']/value}"
                                               target="_blank">
                                                <xsl:value-of select="$locale/data[@name='LinkToGetReportFile']/value"/>
                                            </a>
                                        </p>
                                        <input type="hidden" name="ReportFileName" id="ReportFileName"
                                               value="{//ReportFileName}"/>
                                    </div>
                                </xsl:if>
                            </div>
                        </div>
                    </div>
                </div>

                <xsl:call-template name="commonPopupMessage"/>
                <xsl:call-template name="requireJS"/>

                <xsl:apply-templates select="." mode="pageFooter"/>
                <!--xsl:call-template name="addEmailInput"/-->
            </body>
        </html>
    </xsl:template>

    <!-- Построитель параметров отчетов. -->
    <xsl:template name="reportParams">
        <div id="reportsForm">
            <xsl:call-template name="customReportParameters"/>

            <xsl:call-template name="reportProcess">
                <xsl:with-param name="reportid" select="'reportProcess'"/>
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template name="buildUiControl">
        <xsl:param name="id"/>
        <xsl:param name="description"/>
        <xsl:param name="control"/>
        <xsl:param name="type"/>
        <xsl:param name="options"/>
        <xsl:choose>
            <xsl:when test="$type = 'System.Boolean'">
                <!--Блок частного параметра.-->
                <div class="controlBlock">

                    <!--Контрол параметра.-->
                    <div class="control">
                        <input
                                id="{$id}"
                                type="checkbox" class="inp data"
                                name="{$id}"
                        />
                        <!--Название параметра.-->
                        <label for="{$id}">
                            <span>
                                <xsl:value-of select="$description"/>
                            </span>
                        </label>
                    </div>
                </div>
            </xsl:when>
            <xsl:when test="$control='DatesPicker'">
                <div class="controlBlock">
                    <xsl:variable name="dateTimeFromToPicker"
                                  select="//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control='DateTimeFromToPicker']"/>
                    <!-- Название параметра. -->
                    <div class="controlDescription">
                        <xsl:call-template name="choosePicker">
                            <xsl:with-param name="group" select="'x-date-select'"/>
                            <xsl:with-param name="control" select="$dateTimeFromToPicker/@control"/>
                            <xsl:with-param name="description" select="$dateTimeFromToPicker/@description"/>
                        </xsl:call-template>
                        <xsl:call-template name="choosePicker">
                            <xsl:with-param name="group" select="'x-date-select'"/>
                            <xsl:with-param name="control" select="$control"/>
                            <xsl:with-param name="description" select="$description"/>
                        </xsl:call-template>
                    </div>
                    <!-- Контрол параметра. -->
                    <div class="control" id="{$control}">
                        <xsl:call-template name="multipleDateSelector">
                            <xsl:with-param name="id">
                                <xsl:value-of select="$id"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </div>
                    <xsl:call-template name="DateTimeFromToPicker">
                        <xsl:with-param name="control" select="$dateTimeFromToPicker/@control"/>
                        <xsl:with-param name="options" select="$dateTimeFromToPicker/@options"/>
                    </xsl:call-template>
                </div>
            </xsl:when>
            <xsl:when test="$control = 'DateTimeFromToPicker'">
                <xsl:if test="not(//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control='DatesPicker'])">
                    <!-- Блок частного параметра. -->
                    <div class="controlBlock">
                        <!-- Название параметра. -->
                        <div class="controlDescription">
                            <xsl:value-of select="$description"/>
                        </div>
                        <xsl:call-template name="DateTimeFromToPicker">
                            <xsl:with-param name="control" select="$control"/>
                            <xsl:with-param name="options" select="$options"/>
                        </xsl:call-template>
                    </div>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$control = 'VehicleGroupPicker'">
                <xsl:if test="not(//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control='VehiclesPicker'])">
                    <!-- Блок частного параметра. -->
                    <div class="controlBlock">
                        <!-- Название параметра. -->
                        <div class="controlDescription">
                            <xsl:value-of select="$description"/>
                        </div>
                        <div class="control">
                            <xsl:call-template name="VehicleGroupPicker">
                                <xsl:with-param name="id" select="$id"/>
                                <xsl:with-param name="options" select="$options"/>
                            </xsl:call-template>
                        </div>
                    </div>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$control='VehiclesPicker'">
                <xsl:variable name="vehicleGroupPicker"
                              select="//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control='VehicleGroupPicker']"/>
                <!-- Блок частного параметра. -->
                <div class="controlBlock">
                    <!-- Название параметра. -->
                    <div class="controlDescription">
                        <xsl:call-template name="choosePicker">
                            <xsl:with-param name="group" select="'x-vehicle-select'"/>
                            <xsl:with-param name="control" select="$vehicleGroupPicker/@control"/>
                            <xsl:with-param name="description" select="$vehicleGroupPicker/@description"/>
                        </xsl:call-template>
                        <xsl:call-template name="choosePicker">
                            <xsl:with-param name="group" select="'x-vehicle-select'"/>
                            <xsl:with-param name="control" select="$control"/>
                            <xsl:with-param name="description" select="$description"/>
                        </xsl:call-template>
                    </div>
                    <div class="control" id="{$control}">
                        <xsl:apply-templates select="/response/vehicles" mode="checklist"/>
                    </div>
                    <div class="control" id="{$vehicleGroupPicker/@control}">
                        <xsl:call-template name="VehicleGroupPicker">
                            <xsl:with-param name="id" select="$vehicleGroupPicker/@name"/>
                            <xsl:with-param name="options" select="$vehicleGroupPicker/@options"/>
                        </xsl:call-template>
                    </div>
                </div>
            </xsl:when>
            <xsl:when test="$control = 'FuelCostsPicker'">
                <!-- Блок частного параметра. -->
                <div class="controlBlock">
                    <!-- Название параметра. -->
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='FuelPrice']/value"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="/response/Currency"/>
                        <xsl:value-of select="$locale/data[@name='perLitre']/value"/>
                    </div>
                    <div class="control">
                        <xsl:call-template name="FuelCostSelector">
                            <xsl:with-param name="id" select="$id"/>
                        </xsl:call-template>
                    </div>
                    <input type="hidden" value="FuelCosts" id="FuelCosts" name="FuelCosts"/>
                </div>
            </xsl:when>
            <xsl:when test="$control = 'MileageFilePicker'">
                <!-- Контрол выбора файла с пробегами -->
                <div class="controlBlock">
                    <!-- Название параметра. -->
                    <div class="controlDescription">
                        <xsl:value-of select="$description"/>
                    </div>
                    <div class="filePicker control control_horz typcn typcn-clip">
                        <input class="filePicker__edit inp" type="text" disabled="disabled" placeholder="{$locale/data[@name='FileNotSelected']/value}" />
                        <button class="filePicker__selector btn" type="button" disabled="disabled">
                            <xsl:value-of select="$locale/data[@name='SelectFile']/value" />
                        </button>
                        <input class="filePicker__elem hidden" type="file" name="MileageFile" accept=".xls,.xlsx" />
                    </div>
                </div>
                <!--Предпросмотр XLS-файла-->
                <div class="controlBlock hidden" id="xlsPreview">
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='FileSourceData']/value"/>
                    </div>
                    <div class="filePreview control">
                        <!--(*) Место для предварительного просмотра-->
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <!-- Блок частного параметра. -->
                <div class="controlBlock">
                    <!-- Название параметра. -->
                    <div class="controlDescription">
                        <xsl:value-of select="$description"/>
                    </div>
                    <xsl:choose>

                        <xsl:when test="$control = 'ZonePicker'">
                            <div class="control">
                                <select
                                        id="{$id}"
                                        name="{$id}"
                                        class="data inp">
                                </select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'VehiclePicker'">
                            <div class="control">
                                <select id="{$id}" name="{$id}" class="inp data"></select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'ZonesPicker'">
                            <div class="control">
                                <xsl:apply-templates select="/response/zones" mode="checklist"/>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'ZoneGroupPicker'">
                            <div class="control">
                                <select
                                        id="{$id}"
                                        name="{$id}"
                                        class="data">
                                </select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'IntervalPicker'">
                            <div class="control">
                                <xsl:call-template name="refreshInterval">
                                    <xsl:with-param name="controlId" select="'Interval'"/>
                                    <xsl:with-param name="forParking" select="'false'"/>
                                    <xsl:with-param name="forHistory" select="'true'"/>
                                </xsl:call-template>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'ParkingIntervalPicker'">
                            <div class="control">
                                <xsl:call-template name="refreshInterval">
                                    <xsl:with-param name="controlId" select="'ParkingInterval'"/>
                                    <xsl:with-param name="forParking" select="'true'"/>
                                    <xsl:with-param name="forHistory" select="'false'"/>
                                </xsl:call-template>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'SpeedInput'">
                            <div class="control">
                                <input type="text" id="{$id}" name="{$id}" class="data inp"/>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'SensorPicker'">
                            <div class="control">
                                <select
                                        id="{$id}"
                                        name="{$id}"
                                        class="inp data">
                                </select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'FuelSensorTypePicker'">
                            <div class="control">
                                <select class="inp data" name="{$id}" id="{$id}">
                                    <option value="0;1">
                                        *
                                    </option>
                                    <option value="0">
                                        <xsl:value-of select="$locale/data[@name='fuelLevelSensor']/value"/>
                                    </option>
                                    <option value="1">
                                        CAN
                                    </option>
                                </select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'FuelSpendStandardTypePicker'">
                            <div class="control">
                                <select class="inp data" name="{$id}" id="{$id}">
                                    <option value="Run">
                                        <xsl:value-of select="$locale/data[@name='fuelStandardByRun']/value"/>
                                    </option>
                                    <option value="EngineHours">
                                        <xsl:value-of select="$locale/data[@name='fuelStandardByEngineHours']/value"/>
                                    </option>
                                </select>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'WorkingHoursInterval'">
                            <div class="control">
                                <xsl:call-template name="workingHoursCtlCustom">
                                    <xsl:with-param name="worktimefrom" select="'worktimefrom'"/>
                                    <xsl:with-param name="worktimeto" select="'worktimeto'"/>
                                </xsl:call-template>
                            </div>
                        </xsl:when>
                        <xsl:when test="$control = 'WeekDaysPicker'">
                            <div class="control">
                                <xsl:call-template name="WeekDaySelector">
                                    <xsl:with-param name="id" select="$id"/>
                                </xsl:call-template>
                            </div>
                        </xsl:when>
                    </xsl:choose>
                </div>

            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="choosePicker">
        <xsl:param name="group"/>
        <xsl:param name="control"/>
        <xsl:param name="description"/>
        <input type="radio" name="{$group}" id="{$control}Radio" managed-id="{$control}"
               onclick="$.Reports.xRadioClick('{$group}', '{$control}Radio')" value="{$control}"/>
        <label for="{$control}Radio">
            <xsl:value-of select="$description"/>
        </label>
    </xsl:template>

    <xsl:template name="VehicleGroupPicker">
        <xsl:param name="id"/>
        <xsl:param name="options"/>
        <select id="{$id}" name="{$id}" class="inp data">

            <xsl:variable name="vAccuracyKey" select="substring-before($options, ':')"/>
            <xsl:variable name="vAccuracyValue" select="substring-after($options, ':')"/>

            <xsl:if test="$vAccuracyKey = 'AllowNulls' and $vAccuracyValue = '1'">
                <option>*</option>
            </xsl:if>
        </select>
    </xsl:template>


    <xsl:template name="DateTimeFromToPicker">
        <xsl:param name="control"/>
        <xsl:param name="options"/>
        <div id="{$control}">
            <!-- Контрол параметра. -->
            <xsl:variable name="vAccuracyValueWithTail" select="substring-after($options, 'Accuracy:')"/>
            <xsl:variable name="vAccuracyValue" select="substring($vAccuracyValueWithTail, 1, 1)"/>

            <xsl:variable name="vTimeDirectionValueWithTail" select="substring-after($options, 'TimeDirection:')"/>
            <xsl:variable name="vTimeDirectionValue" select="substring($vTimeDirectionValueWithTail, 1, 1)"/>

            <xsl:choose>
                <xsl:when test="$vAccuracyValue != ''">
                    <xsl:choose>
                        <xsl:when test="$vAccuracyValue = '1'">
                            <div class="control">
                                <xsl:call-template name="dateTimeFromToCtlCustom">
                                    <xsl:with-param name="dateFrom" select="'reportdatefrom'"/>
                                    <xsl:with-param name="dateTo" select="'reportdateto'"/>
                                    <xsl:with-param name="timeDirection" select="$vTimeDirectionValue"/>
                                </xsl:call-template>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="control">
                                <xsl:call-template name="dateTimeFromToCtlCustom">
                                    <xsl:with-param name="dateFrom" select="'reportdatefrom'"/>
                                    <xsl:with-param name="timeFrom" select="'reporttimefrom'"/>
                                    <xsl:with-param name="dateTo" select="'reportdateto'"/>
                                    <xsl:with-param name="timeTo" select="'reporttimeto'"/>
                                    <xsl:with-param name="timeDirection" select="$vTimeDirectionValue"/>
                                </xsl:call-template>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:when>
                <xsl:otherwise>
                    <div class="control">
                        <xsl:call-template name="dateTimeFromToCtlCustom">
                            <xsl:with-param name="dateFrom" select="'reportdatefrom'"/>
                            <xsl:with-param name="timeFrom" select="'reporttimefrom'"/>
                            <xsl:with-param name="dateTo" select="'reportdateto'"/>
                            <xsl:with-param name="timeTo" select="'reporttimeto'"/>
                            <xsl:with-param name="timeDirection" select="$vTimeDirectionValue"/>
                        </xsl:call-template>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <!-- Параметры отчета. -->
    <xsl:template name="customReportParameters">
        <div class="reportSettings b-report" id="reportSettings">

            <!-- Если присутсвует контрол для передачи файла на сервер -->
            <xsl:variable name="filePicker" select="//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control = 'MileageFilePicker']" />

            <!-- Параметры некоторых отчётов не умещают свои значения в URL GET-запроса, поэтому нужно строить такие отчёты как POST. -->
            <xsl:variable name="formMethod">
                <xsl:choose>
                    <xsl:when test="//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter[@control = 'ZonesPicker' or @control = 'VehiclesPicker' or @control = 'MileageFilePicker' ]">post</xsl:when>
                    <xsl:otherwise>get</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!--Если требуется передать файл - меняем способ кодирования-->
            <xsl:variable name="formEncoding">
                <xsl:choose>
                    <xsl:when test="$filePicker">multipart/form-data</xsl:when>
                    <xsl:otherwise>application/x-www-form-urlencoded</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <form action="{/response/@ApplicationPath}/Report.aspx" style="margin:0;" method="{$formMethod}" enctype="{$formEncoding}"
                  onsubmit="jQuery.Reports.createCustomReport()" id="reportForm">
                <xsl:attribute name="class">
                    <xsl:text>vertical</xsl:text>
                    <!--Если отчет содержит выбор файла, то разрешаем формирование отчета только из JS-кода-->
                    <xsl:if test="$filePicker">
                        <xsl:text> enableReportByJS</xsl:text>
                    </xsl:if>
                </xsl:attribute>

                <!-- Управление построением параметрами отчета -->
                <xsl:for-each select="//ReportMenuItem[@Selected = 1]/ReportParameters/ReportParameter">
                    <xsl:call-template name="buildUiControl">
                        <xsl:with-param name="id" select="./@name"/>
                        <xsl:with-param name="description" select="./@description"/>
                        <xsl:with-param name="control" select="./@control"/>
                        <xsl:with-param name="type" select="./@type"/>
                        <xsl:with-param name="options" select="./@options"/>
                    </xsl:call-template>
                </xsl:for-each>

                <!-- Управление построением отчета и подписка -->
                <div class="divider">
                    <xsl:text> </xsl:text>
                </div>
                <xsl:call-template name="reportTypesButtons">
                    <xsl:with-param name="btn1OnClickHandler">
                        <xsl:if test="//ReportMenuItem[@Selected = 1]/Formats[Format = 'Acrobat']">
                            <xsl:text>jQuery.Reports.createCustomReportPdf()</xsl:text>
                        </xsl:if>
                    </xsl:with-param>
                    <xsl:with-param name="btn2OnClickHandler">
                        <xsl:if test="//ReportMenuItem[@Selected = 1]/Formats[Format = 'Excel']">
                            <xsl:text>jQuery.Reports.createCustomReportXls()</xsl:text>
                        </xsl:if>
                    </xsl:with-param>
                    <xsl:with-param name="btn3OnClickHandler">
                        <xsl:if test="//ReportMenuItem[@Selected = 1]/Formats[Format = 'Html']">
                            <xsl:text>jQuery.Reports.createCustomReport()</xsl:text>
                        </xsl:if>
                    </xsl:with-param>
                    <xsl:with-param name="reportProcessid" select="'reportProcess'"/>
                </xsl:call-template>

                <div class="divider">
                    <xsl:text> </xsl:text>
                </div>

                <input type="hidden" name="mapGuid" id="mapGuid"/>
                <input type="hidden" name="a" value="createCustomReport"/>
                <input type="hidden" id="reportFile" name="reportFile" value=""/>
                <input type="hidden" id="currentreportid" name="currentreportid"
                       value="{//ReportMenuItem[@Selected = 1]/@Id}"/>
            </form>

            <!-- Если разрешена подписка -->
            <xsl:if test="//ReportMenuItem[@Selected = 1 and @IsSubscriptionVisible = 1]">
            <xsl:call-template name="reportDeliveryFull">
                <xsl:with-param name="operator" select="./Person"/>
            </xsl:call-template>
            <div class="divider">
                <xsl:text> </xsl:text>
            </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template name="reportDeliveryFull">
        <xsl:param name="reportId" select="//ReportMenuItem[@Selected = 1]/@Id"/>
        <xsl:param name="operator"/>
        <xsl:if test="//@loggedUser != 'guest'">
            <xsl:choose>
                <xsl:when test="$operator">
                    <xsl:call-template name="reportDelivery">
                        <xsl:with-param name="rdId" select="$reportId"/>
                        <xsl:with-param name="operatorEmail" select="$operator/Email"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$locale/data[@name='operatorEmailNotSetMessage']/value"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <!-- Подписка на создаваемый отчет. -->
    <xsl:template name="reportDelivery">
        <xsl:param name="rdId" select="1"/>
        <xsl:param name="operatorEmail" select="''"/>

        <xsl:variable name="reportId" select="$rdId"/>

        <div class="note delivery b-subscription" id="deliveryBlock{$rdId}">
            <div style="">
                <label class="togglerBtn" style="vertical-align: top;" for="deliveryBlockCheckbox{$rdId}">
                    <input type="checkbox" style="width:20px;" onclick=" myReports.toggleDelivery(event);"
                           id="deliveryBlockCheckbox{$rdId}"/>
                    <span class="chckbx"></span>
                    <span>
                        <xsl:text>&#160;</xsl:text>
                        <xsl:value-of select="$locale/data[@name='subscribeForReport']/value"/>
                    </span>
                    <span class="question">
                        <span class="question-sec">
                            <xsl:value-of select="$locale/data[@name='reportSubscriptionMessage']/value"/>
                        </span>
                    </span>
                </label>
            </div>
            <div id="deliveryControlBox{$rdId}" class="b-subscription__block">
                <!--<p class="grey" style="padding:0!important;margin:0;">
                          <xsl:copy-of select="$locale/data[@name='reportSubscriptionMessage']/value" />
                          <a href="mailto:{$operatorEmail}">
                            <xsl:value-of select="$operatorEmail" />
                          </a>
                        </p>-->

                <p>
                    <ul>
                        <xsl:for-each select="/response/Person/Emails/*">
                            <li>
                                <input type="checkbox" style="width:20px;" class="subscriptionEmailCheckbox inp"
                                       id="subsEmail{./Id}"/>
                                <label for="subsEmail{./Id}">
                                    <span>
                                        <xsl:if test="./Comment">
                                            <xsl:attribute name="title">
                                                <xsl:value-of select="./Comment"/>
                                            </xsl:attribute>
                                        </xsl:if>
                                        <xsl:value-of select="./Value"/>
                                    </span>
                                </label>
                            </li>
                        </xsl:for-each>
                    </ul>
                    <div class="forEmails">

                    </div>
                </p>
                <div>
                    <fieldset>
                        <h2 class="mar15">
                            <xsl:value-of select="$locale/data[@name='deliveryInterval']/value"/>
                        </h2>
                        <div>
                            <input type="radio" id="intervalDeliveryDay" class="deliverydata inp"
                                   name="intervalDelivery" style="width:20px;" disabled="disabled" checked="checked"
                                   onchange="myReports.disableWeekDays(event)"/>
                            <label class="lableAfterInput" for="intervalDeliveryDay">
                                <span class="label-pretty">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:copy-of select="$locale/data[@name='daily']/value/node()"/>
                                </span>
                            </label>
                            <xsl:call-template name="clear"/>
                        </div>

                        <div style="border-top:1px solid #c0c0c0; padding-top:10px;">
                            <input type="radio" id="intervalDeliveryWeek" class="deliverydata inp"
                                   name="intervalDelivery" style="width:20px;" disabled="disabled"
                                   onchange="myReports.enableWeekDays(event)"/>
                            <label class="lableAfterInput" for="intervalDeliveryWeek">
                                <span class="label-pretty">
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:copy-of select="$locale/data[@name='weekly']/value/node()"/>
                                </span>
                            </label>
                            <div style="margin-top:5px; margin-left: 25px" class="weekdays">
                                <xsl:call-template name="weekDaySelectionPretty">
                                    <xsl:with-param name="groupName" select="'deliveryWeekDays'"/>
                                </xsl:call-template>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div style="margin-top:10px;">
                    <label class="mandatory grey">
                        <xsl:value-of select="$locale/data[@name='reportDeliveryCreateDate']/value"/>:
                        <div>
                            <input
                                    id="repsubsribetime"
                                    type="text"
                                    disabled="disabled"
                                    value="18:00"
                                    style="width:150px; margin: 0 0 10px;"
                                    class="deliverydata timeTo data time inp small"
                                    onkeyup="myReports.checkReadiness(event)"
                                    maxlength="5"/>
                            <xsl:text>&#160;</xsl:text>
                            <div id="subscribeToCustomReportResult" class="alert b-subscription__state"></div>
                            <div class="b-subscription__navigation">
                                <button type="button" id="subscribeToCustomReportButton" disabled="disabled" class="btn"
                                        onclick="myReports.subscribeToCustomReport({$reportId}, event)">
                                    <xsl:value-of select="$locale/data[@name='subscribeForReportShort']/value"/>
                                </button>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <div id="subscriptionResults{$reportId}">
        </div>
    </xsl:template>

    <xsl:template name="workingHoursCtlCustom">
        <xsl:param name="worktimefrom"/>
        <xsl:param name="worktimeto"/>
        <div>
            <div class="inputs">
                <div>
                    <span class="worktimeLabel">
                        <xsl:value-of select="$locale/data[@name='workFromLabel']/value"/>
                        <xsl:text>&nbsp;</xsl:text>
                    </span>
                    <input type="text" class="timeFrom data time worktime inp" id="{$worktimefrom}"
                           name="{$worktimefrom}"/>
                    <xsl:text>&nbsp;&nbsp;&nbsp;&nbsp;</xsl:text>
                    <span class="worktimeLabel">
                        <xsl:value-of select="$locale/data[@name='workToLabel']/value"/>
                        <xsl:text>&nbsp;</xsl:text>
                    </span>
                    <input type="text" class="timeTo data time worktime inp" id="{$worktimeto}" name="{$worktimeto}"/>
                </div>
            </div>
            <input type="hidden" value="WorkingHoursInterval" id="WorkingHoursInterval" name="WorkingHoursInterval"/>
        </div>
    </xsl:template>

    <xsl:template name="dateTimeFromToCtlCustom">
        <xsl:param name="dateFrom"/>
        <xsl:param name="dateTo"/>
        <xsl:param name="timeFrom"/>
        <xsl:param name="timeTo"/>
        <xsl:param name="timeDirection"/>
        <div class ="dateTimeFromToCtlCustom">

            <xsl:choose>
                <xsl:when test="$timeDirection = ''">
                    <div class="row intervalselector">
                        <label class="block">
                            <xsl:value-of select="$locale/data[@name='SelectFor']/value"/>:
                        </label>
                        <xsl:call-template name="dateIntervalForUserCommonPeriods">
                            <xsl:with-param name="dateFrom" select="$dateFrom"/>
                            <xsl:with-param name="dateTo" select="$dateTo"/>
                            <xsl:with-param name="timeFrom" select="$timeFrom"/>
                            <xsl:with-param name="timeTo" select="$timeTo"/>
                            <xsl:with-param name="timeDirection" select="$timeDirection"/>
                        </xsl:call-template>
                    </div>
                </xsl:when>
            </xsl:choose>


            <div class="row intervalselector">
                <xsl:choose>
                    <xsl:when test="$timeDirection = ''">

                        <label class="block">
                            <xsl:value-of select="$locale/data[@name='SelectLast']/value"/>:
                        </label>
                    </xsl:when>
                    <xsl:otherwise>
                        <label class="block">
                            <xsl:value-of select="$locale/data[@name='SelectNext']/value"/>:
                        </label>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:call-template name="dateInterval">
                    <xsl:with-param name="dateFrom" select="$dateFrom"/>
                    <xsl:with-param name="dateTo" select="$dateTo"/>
                    <xsl:with-param name="timeFrom" select="$timeFrom"/>
                    <xsl:with-param name="timeTo" select="$timeTo"/>
                    <xsl:with-param name="timeDirection" select="$timeDirection"/>
                </xsl:call-template>
            </div>
            <div class="inputs">
                <div class="row">
                    <!-- dateFrom -->
                    <div class="label">
                        <xsl:value-of select="$locale/data[@name='dateFrom']/value"/>
                        <xsl:text>: </xsl:text>
                    </div>
                    <div class="dateFromWrapper input input-group input-group-sm" id="{$dateFrom}_out"
                         data-datepicker="From">
                        <input type="text" class="dateFrom data date inp form-control" id="{$dateFrom}"
                               name="{$dateFrom}"/>
                        <span class="input-group-addon">
                            <span class="typcn typcn-calendar-outline"></span>
                        </span>
                    </div>
                    <xsl:if test="$timeFrom">
                        <div class="timeFromWrapper input input-group  input-group-sm" id="{$timeFrom}_out"
                             data-timepicker="From">
                            <input type="text" class="timeFrom data time inp form-control" id="{$timeFrom}"
                                   name="{$timeFrom}"/>
                            <span class="input-group-addon">
                                <span class="typcn typcn-time"></span>
                            </span>
                        </div>
                    </xsl:if>
                </div>

                <div class="row">
                    <!-- dateTo -->
                    <div class="label">
                        <xsl:value-of select="$locale/data[@name='dateTo']/value"/>
                        <xsl:text>: </xsl:text>
                    </div>
                    <div class="dateToWrapper input input-group  input-group-sm" id="{$dateTo}_out"
                         data-datepicker="To">
                        <input type="text" class="dateTo data date inp form-control" id="{$dateTo}" name="{$dateTo}"/>
                        <span class="input-group-addon">
                            <span class="typcn typcn-calendar-outline"></span>
                        </span>
                    </div>
                    <xsl:if test="$timeTo">
                        <div class="timeToWrapper input input-group input-group-sm" id="{$timeTo}_out"
                             data-timepicker="To">
                            <input type="text" class="timeTo data time inp form-control" id="{$timeTo}"
                                   name="{$timeTo}"/>
                            <span class="input-group-addon">
                                <span class="typcn typcn-time"></span>
                            </span>
                        </div>
                    </xsl:if>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- AS: Типы отображения отчета -->
    <xsl:template name="reportTypesButtons">
        <xsl:param name="btn1OnClickHandler"/>
        <xsl:param name="btn2OnClickHandler"/>
        <xsl:param name="btn3OnClickHandler"/>
        <xsl:param name="reportProcessid"/>
        <div class="horizontalGroup">
            <xsl:if test="$btn3OnClickHandler != ''">
                <button class="createReportBtn btn" type="button" disabled="disabled">
                    <xsl:attribute name="onclick">
                        <xsl:value-of select="$btn3OnClickHandler"/>
                    </xsl:attribute>
                    <xsl:value-of select="$locale/data[@name='createReport']/value"/>
                </button>
            </xsl:if>
            <xsl:if test="$btn1OnClickHandler != ''">
                <button class="createReportBtn btn" type="button" disabled="disabled">
                    <xsl:attribute name="onclick">
                        <xsl:value-of select="$btn1OnClickHandler"/>
                    </xsl:attribute>
                    <xsl:value-of select="$locale/data[@name='reportBuildInPdf']/value"/>
                </button>
            </xsl:if>
            <xsl:if test="$btn2OnClickHandler != ''">
                <button class="createReportBtn btn" type="button" disabled="disabled">
                    <xsl:attribute name="onclick">
                        <xsl:value-of select="$btn2OnClickHandler"/>
                    </xsl:attribute>
                    <xsl:value-of select="$locale/data[@name='reportBuildInExcel']/value"/>
                </button>
            </xsl:if>
        </div>
    </xsl:template>

    <!-- Построитель меню отчетов. -->
    <xsl:template name="reportMenuBuilder">
        <ul class="b-verticalMenu">
            <xsl:for-each select="ReportMenuGroups/ReportMenuGroup">
                <xsl:if test="./ReportMenuItem">
                    <!-- Формирование группы отчетов. -->
                    <li class="b-verticalMenu__item">
                        <xsl:variable name="reportMenuGroupName" select="./@Name"/>
                        <span>
                            <xsl:value-of select="$locale/data[@name=$reportMenuGroupName]/value"/>
                        </span>
                        <ul class="b-verticalMenu__subitems">
                            <xsl:for-each select="./ReportMenuItem">
                                <xsl:variable name="reportMenuItemName" select="./@Name"/>
                                <!-- Формирование элемента отчета. -->
                                <xsl:if test="@IsAvailable = '1'">
                                    <li id="reportMenuItem{./@Id}" class="b-verticalMenu__subitem"
                                        title="{./@Description}">
                                        <xsl:if test="./@Selected = 1">
                                            <xsl:attribute name="class">b-verticalMenu__subitem b-selected
                                            </xsl:attribute>
                                        </xsl:if>
                                        <a href="{/response/@ApplicationPath}/Report.aspx?currentreportId={./@Id}">
                                            <xsl:value-of select="$reportMenuItemName"/>
                                        </a>
                                    </li>
                                </xsl:if>
                            </xsl:for-each>
                        </ul>
                    </li>
                </xsl:if>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <!-- Прогресс-бар построения отчета. -->
    <xsl:template name="reportProcess">
        <xsl:param name="reportid" select="'reportProcess'"/>
        <p id="{$reportid}" style="display:none;" class="message ok">
            <img src="{/response/@baseWebAppPath}/img/wait.gif"/>&#160;
            <xsl:text/>
            <xsl:value-of select="$locale/data[@name='reportProcess']/value"/>
        </p>
    </xsl:template>

    <!-- Ссылка на список существующих подписок на отчеты. -->
    <xsl:template name="deliveryList">
        <ul class="b-verticalMenu">
            <li class="b-verticalMenu__item">
                <xsl:choose>
                    <xsl:when test="//@loggedUser != 'guest'">
                        <a href="./ReportDelivery.aspx">
                            <xsl:value-of select="$locale/data[@name='deliveryList']/value"/>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <span class="grey">
                            <xsl:value-of select="$locale/data[@name='deliveryList']/value"/>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
            </li>
        </ul>
    </xsl:template>

    <!-- Сообщение об ошибках, примечания к странице. -->
    <xsl:template match="response" mode="reportFiles">
        <xsl:if test="./message">
            <p style="margin:10px" class="message Warning">
                <xsl:call-template name="replace">
                    <xsl:with-param name="str" select="./message/MessageBody"/>
                </xsl:call-template>
            </p>
        </xsl:if>
        <xsl:apply-templates select="./HistoryReports" mode="file"/>
    </xsl:template>

    <!-- Блок сообщения о результате подписки. -->
    <xsl:template match="response" mode="subscribeResults">
        <div>
            <xsl:choose>
                <xsl:when test="./message">
                    <p style="margin:10px" class="message Warning">
                        <xsl:call-template name="replace">
                            <xsl:with-param name="str" select="./message/MessageBody"/>
                        </xsl:call-template>
                    </p>
                </xsl:when>
                <xsl:otherwise>
                    <p style="margin:10px" class="message ok">
                        <xsl:value-of select="$locale/data[@name='reportSubscriptionAdded']/value"/>
                    </p>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>


    <xsl:template name="localTitle">

        <xsl:value-of select="./ReportMenuGroups/ReportMenuGroup/ReportMenuItem[@Selected = 1]/@Name"/>
        <xsl:text> / </xsl:text>
    </xsl:template>


    <xsl:template name="addEmailInput">
        <xsl:param name="path"></xsl:param>

        <script id="addEmailInputIE" type="text/html">

            <form>
                <input type="text"/>
                <button id="addEmail">
                    <xsl:value-of select="$locale/data[@name='Add']/value"/>
                </button>
                <button id="cancel">
                    <xsl:value-of select="$locale/data[@name='Cancel']/value"/>
                </button>
            </form>


        </script>

        <script id="addEmailInput" type="text/html">

            <![CDATA[
                    <%
                ]]>

            var add = '<xsl:value-of select="$locale/data[@name='Add']/value"/>';
            var cancel = '<xsl:value-of select="$locale/data[@name='Cancel']/value"/>';

            <![CDATA[
                    %>
                ]]>

            <![CDATA[


                <form>
                    <input type="text"/>
                    <button id="addEmail"><%- add %></button>
                    <button id="cancel"><%- cancel %></button>
                </form>


                ]]>


        </script>

    </xsl:template>

    <xsl:template match="zones" mode="checklist">
        <div>
            <button type="button" class="btn small" onclick="$.Reports.selectAll('zonesBox')">
                <xsl:value-of select="$locale/data[@name='SelectAll']/value"/>
            </button>
            <xsl:text>&#160;</xsl:text>
            <button type="button" class="btn small" onclick="$.Reports.deselectAll('zonesBox')">
                <xsl:value-of select="$locale/data[@name='DeselectAll']/value"/>
            </button>
            <input type="hidden" id="ZoneIds" name="ZoneIds" value="" class="data"/>
            <div id="zonesBox">
                <xsl:apply-templates select="./Zone" mode="checklist"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="Zone" mode="checklist">
        <div class="checkbox-pretty">
            <input type="checkbox" id="ZoneIds_{./ZONE_ID}" name="ZoneIds_{./ZONE_ID}" class="data inp"/>
            <label for="ZoneIds_{./ZONE_ID}" data-zone="{./ZONE_ID}">
                <span>
                    <xsl:choose>
                        <xsl:when test="./NAME and ./NAME != ''">
                            <xsl:value-of select="./NAME"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>[</xsl:text>
                            <xsl:value-of select="$locale/data[@name='noname']/value"/>
                            <xsl:text>]</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </span>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="vehicles" mode="checklist">
        <div>
            <button type="button" class="btn small" onclick="$.Reports.selectAll('vehiclesBox')">
                <xsl:value-of select="$locale/data[@name='SelectAll']/value"/>
            </button>
            <xsl:text>&#160;</xsl:text>
            <button type="button" class="btn small" onclick="$.Reports.deselectAll('vehiclesBox')">
                <xsl:value-of select="$locale/data[@name='DeselectAll']/value"/>
            </button>
            <input type="hidden" id="VehicleIds" name="VehicleIds" value="" class="data"/>
            <div id="vehiclesBox" style="max-height:200px;overflow:auto;">
                <xsl:apply-templates select="./vehicle" mode="checklist"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="vehicle" mode="checklist">
        <div class="checkbox-pretty">
            <input type="checkbox" id="VehicleIds_{./@id}" name="VehicleIds_{./@id}" class="data inp" value="on"/>
            <label for="VehicleIds_{./@id}">
                <span>
                    <xsl:choose>
                        <xsl:when test="./@name and ./@name != ''">
                            <xsl:value-of select="./@name"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>[</xsl:text>
                            <xsl:value-of select="$locale/data[@name='noname']/value"/>
                            <xsl:text>]</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </span>
            </label>
        </div>
    </xsl:template>

    <xsl:template name="multipleDateSelector">
        <xsl:param name="id" select="multipleDateSelector"/>

        <input type="hidden" id="{$id}Input" name="{$id}"/>

        <div id="{$id}" class="multiple">

        </div>

    </xsl:template>

</xsl:stylesheet>
