<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="common.xsl" />
	<xsl:import href="uielements.xsl" />
	<xsl:import href="dateTime.xsl" />
	<xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//@template = 'imageList'">
			<xsl:apply-templates select="." mode="imageList" />
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="response" mode="imageList">
	<response>
		<filelist>
			<div>
				<xsl:choose>
					<xsl:when test="count(./files/file) &gt; 0">
            <ul style="list-style-type:none;margin:0;padding:0;">
              <xsl:apply-templates select="./files/file" mode="image" />              
            </ul>
            <!--
            <div style="width:300px;">
							<div style="width:50%;float:left">
								<xsl:apply-templates select="./files/file[position() mod 2 != 0]" mode="image" />
							</div>
							<div style="width:50%;float:left">
								<xsl:apply-templates select="./files/file[position() mod 2 = 0]" mode="image" />
							</div>
						</div>
						<div class="clear">&#160;<xsl:text /></div>
            -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$locale/data[@name='empty']/value" />...
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</filelist>
	</response>
</xsl:template>

<xsl:template match="file" mode="image">
  <li style="float:left; width:55px;">
    <span class="toggle galleryItem" style="padding:5px;">
      <xsl:call-template name="formatImage">
        <xsl:with-param name="browser" select="/response/@browser" />
        <xsl:with-param name="src" select="." />
        <xsl:with-param name="style" select="'vertical-align:middle;'" />
      </xsl:call-template>
    </span>
  </li>
</xsl:template>

</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->