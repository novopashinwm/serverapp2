<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml">

	<!--
        ******************************************************************************************************
        Парсер XML-ответа сервера с параметрами для отображения страницы.

        Создает скрипт с сырыми данными ответа для передачи их в web-приложение.
        Дополнительно заполняет структуру полезной ипнформацией.
        На случай возникновения синтаксической ошибки в первом сприпте, во втором сохраняем ошибку.
        ******************************************************************************************************
    -->

	<!-- Переменные для замены одинарных на парные кавычки -->
	<xsl:variable name="s-quote">'</xsl:variable>
	<xsl:variable name="d-quote">"</xsl:variable>

	<xsl:template name="ScriptResponse">
		<script>
			<xsl:text>var UFIN={timing:{pageStart:new Date()}}</xsl:text>
		</script>
		<script>
			<xsl:text>UFIN.response={</xsl:text>
			<xsl:apply-templates select="/response" mode="ScriptResponse_AddRawData" />
			<xsl:text>}</xsl:text>
		</script>
		<script>
			<xsl:text>if(!UFIN.response){UFIN.response={error:'Error parsing server response.'}}</xsl:text>
		</script>
	</xsl:template>

	<!-- Преобразует XML-ответ сервера в структуру с сырыми данными -->
	<xsl:template match="/response" mode="ScriptResponse_AddRawData">
		<!-- Добавляем все атрибуты XML-файла -->
		<xsl:text>xmlAttribs:{</xsl:text>
		<xsl:apply-templates select="/response/@*" mode="ScriptResponse_AddAttrib" />
		<xsl:text>},</xsl:text>

		<!-- Добавляем элементы XML-файла -->
		<xsl:text>xmlElems:{</xsl:text>
		<!--Игнорируем структуру старого сайта, которая передается в response/pageContent-->
		<xsl:apply-templates select="/response/*[not(name()='pageContent')]" mode="ScriptResponse_AddObjectElem" />
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- Добавляет в структуру скрипта информацию об очередном XML-атрибуте корневого элемента. -->
	<xsl:template match="@*" mode="ScriptResponse_AddAttrib">
		<!-- Чтобы не было синтаксической ошибки заменяем одинарные кавычки на двойные в именах и значениях (сервер возвращает ответ как хочет...)  -->
		<xsl:text>'</xsl:text>
		<xsl:value-of select="translate(name(), $s-quote, $d-quote)" />
		<xsl:text>':'</xsl:text>
            <!-- Дополнительно заменяем в значении атрибута возвраты каретки на символы \n -->
            <xsl:call-template name="ReplaceCR">
                <xsl:with-param name="text" select="translate(., $s-quote, $d-quote)" />
            </xsl:call-template>
		<xsl:text>'</xsl:text>
		<!-- Если это не последний элемент в наборе - добавляем запятую -->
		<xsl:if test="position()!=count(../@*)">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- Добавляет в структуру скрипта информацию об элементе типа Объект. -->
	<xsl:template match="*" mode="ScriptResponse_AddSingleObjectElem">
		<!-- Выводим текущее имя свойства -->
		<xsl:text>'</xsl:text>
		<xsl:value-of select="translate(name(), $s-quote, $d-quote)" />
		<xsl:text>':</xsl:text>

		<!-- Определяем количество дочерних элементов текущего свойства чтобы определить его тип -->
		<xsl:variable name="count" select="count(./*)" />

		<!-- Проверяем тип текущего элемента -->
		<xsl:choose>

			<!-- Обычное свойство (нет вложенных элементов) -->
			<xsl:when test="$count=0">
				<!-- Добавляем значение этого свойства (заменяя служебные символы) -->
				<xsl:text>'</xsl:text>
                    <xsl:call-template name="ReplaceCR">
                        <xsl:with-param name="text" select="translate(., $s-quote, $d-quote)" />
                    </xsl:call-template>
				<xsl:text>'</xsl:text>
			</xsl:when>

			<!-- Элемент имеет подэлементы - это либо объект, либо массив -->
			<xsl:when test="$count!=0">
				<!-- Определяем имена первого и последнего подэлемента -->
				<xsl:variable name="first" select="name(./*[position()=1])" />
				<xsl:variable name="last" select="name(./*[position()=$count])" />

				<xsl:choose>
					<!-- Если имена первого и последнего элемента совпадают - то оформляем как массив -->
					<xsl:when test="$first=$last">
						<xsl:text>[</xsl:text>
						<xsl:apply-templates select="./*" mode="ScriptResponse_AddArrayElem" />
						<xsl:text>]</xsl:text>
					</xsl:when>

					<!-- Если содержимое имеет разные имена - это объект -->
					<xsl:when test="$first!=$last">
						<xsl:text>{</xsl:text>
						<xsl:apply-templates select="./*" mode="ScriptResponse_AddObjectElem" />
						<xsl:text>}</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:when>

		</xsl:choose>
	</xsl:template>

	<!--
		Добавляет в структуру скрипта информацию об элементе типа Объект.
		(!) В отличие от ScriptResponse_AddSingleObjectElem - добавляет запятые между соседними элементами.
	-->
	<xsl:template match="*" mode="ScriptResponse_AddObjectElem">
		<!-- Обрабатываем текущий объект -->
		<xsl:apply-templates select="." mode="ScriptResponse_AddSingleObjectElem" />

		<!-- Если это не последний элемент в наборе - добавляем запятую -->
		<xsl:if test="position()!=count(../*)">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- Добавляет в структуру скрипта информацию об элементе типа Массив. -->
	<xsl:template match="*" mode="ScriptResponse_AddArrayElem">
		<xsl:text>{</xsl:text>
		<xsl:apply-templates select="." mode="ScriptResponse_AddSingleObjectElem" />
		<xsl:text>}</xsl:text>
		<!-- Если это не последний элемент в массиве - добавляем запятую -->
		<xsl:if test="position()!=count(../*)">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>


</xsl:stylesheet>