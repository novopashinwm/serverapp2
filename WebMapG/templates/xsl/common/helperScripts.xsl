<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
				exclude-result-prefixes="msxsl"
                xmlns:js="urn:my-scripts">

    <!--
        ******************************************************************************************************
        Набор полезных функций на языке JavaScript.
        Вызов функций: через js:xxx, где xxx - имя функции, а js - пространство имен.
        (!) Для работы необходимо описать пространство имен как: xmlns:js="urn:my-scripts"
        ******************************************************************************************************
    -->

    <!--
	    Возвращает текущее время в миллисекундах
	-->
    <msxsl:script language="JScript" implements-prefix="js">
        <![CDATA[
        function getDateTime() {
            return +new Date();
        }
        ]]>
    </msxsl:script>

    <!--
	    Возвращает количество секунд .
	    Если параметр отсутствует, то использует текущее время.
	-->
    <msxsl:script language="JScript" implements-prefix="js">
        <![CDATA[
        function getSeconds(time) {
            time = time || +new Date();
            return new Date(time).getSeconds();
        }
        ]]>
    </msxsl:script>

    <!--
	    Возвращает количество минут.
	    Если параметр отсутствует, то использует текущее время.
	-->
    <msxsl:script language="JScript" implements-prefix="js">
        <![CDATA[
        function getMinutes(time) {
            time = time || +new Date();
            return new Date(time).getMinutes();
        }
        ]]>
    </msxsl:script>

    <msxsl:script language="JScript" implements-prefix="js">
        <![CDATA[
        function chooseImageIndex(dayCount, nightCount, intervalMinutes) {
            dayCount = dayCount || 2;
            nightCount = nightCount || 2;
            intervalMinutes = intervalMinutes || 30;
            if (intervalMinutes < 10) {
                intervalMinutes = 10;
            }
            var date = new Date();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            minutes = minutes % intervalMinutes;
            if (hours >= 6 && hours < 19) {
                return Math.floor(minutes / (intervalMinutes / dayCount)) + 1;
            }
            else {
                return Math.floor(minutes / (intervalMinutes / nightCount)) + 1 + dayCount;
            }
        }
        ]]>
    </msxsl:script>



</xsl:stylesheet>