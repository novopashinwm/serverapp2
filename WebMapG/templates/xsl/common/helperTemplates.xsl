<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml">

	<!--
        ******************************************************************************************************
		Набор полезных шаблонов для выполнения вспомогательных функций
        ******************************************************************************************************
    -->

	<!--
	    Добавляет завершающаую косую черту к строке с путем.
        Строка с результатом возвращается в выходной поток.
	-->
    <xsl:template name="AddTrailSlash">
		<xsl:param name="path" />

        <xsl:choose>
            <!--Если пустая строка - возвращаем косую черту-->
            <xsl:when test="$path=''">
                <xsl:value-of select="'/'" />
            </xsl:when>
            <!--Иначе - проверяем конец строки-->
            <xsl:when test="not($path = '')">
                <xsl:choose>
                    <xsl:when test="substring($path, string-length($path)) = '/'">
                        <xsl:value-of select="$path" />
                    </xsl:when>
                    <xsl:when test="not(substring($path, string-length($path)) = '/')">
                        <xsl:value-of select="concat($path, '/')" />
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
	</xsl:template>

    <!--
	    Удаляет завершающий слеш в строке с путем
        Строка с результатом возвращается в выходной поток.
	-->
    <xsl:template name="RemoveTrailSlash">
        <xsl:param name="path" />

        <xsl:choose>
            <!--Если пустая строка ничего не возвращаем-->
            <xsl:when test="$path=''">
                <xsl:value-of select="''" />
            </xsl:when>
            <!--Иначе - проверяем конец строки-->
            <xsl:when test="not($path = '')">
                <xsl:choose>
                    <xsl:when test="substring($path, string-length($path)) = '/'">
                        <xsl:value-of select="substring($path, 1, string-length($path) - 1)" />
                    </xsl:when>
                    <xsl:when test="not(substring($path, string-length($path)) = '/')">
                        <xsl:value-of select="$path" />
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--
	    Добавляет лидирующую косую черту к строке с путем.
        Строка с результатом возвращается в выходной поток.
	-->
    <xsl:template name="AddLeadingSlash">
        <xsl:param name="path" />

        <xsl:choose>
            <!--Если пустая строка - возвращаем косую черту-->
            <xsl:when test="$path=''">
                <xsl:value-of select="'/'" />
            </xsl:when>
            <!--Иначе - проверяем начало строки-->
            <xsl:when test="not($path = '')">
                <xsl:choose>
                    <xsl:when test="substring($path, 1, 1) = '/'">
                        <xsl:value-of select="$path" />
                    </xsl:when>
                    <xsl:when test="not(substring($path, 1, 1) = '/')">
                        <xsl:value-of select="concat('/', $path)" />
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--
       Удаляет лидирующий слеш в строке с путем
       Строка с результатом возвращается в выходной поток.
   -->
    <xsl:template name="RemoveLeadingSlash">
        <xsl:param name="path" />

        <xsl:choose>
            <!--Если пустая строка ничего не возвращаем-->
            <xsl:when test="$path=''">
                <xsl:value-of select="''" />
            </xsl:when>
            <!--Иначе - проверяем начало строки-->
            <xsl:when test="not($path = '')">
                <xsl:choose>
                    <xsl:when test="substring($path, 1, 1) = '/'">
                        <xsl:value-of select="substring($path, 2, string-length($path) - 1)" />
                    </xsl:when>
                    <xsl:when test="not(substring($path, 1, 1) = '/')">
                        <xsl:value-of select="$path" />
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--
	    Делает замену одной подстроки на другую
	    Строка с результатом возвращается в выходной поток.
	-->
    <xsl:template name="ReplaceStr">
        <xsl:param name="text"/>
        <xsl:param name="from"/>
        <xsl:param name="to"/>

        <xsl:choose>
            <xsl:when test="contains($text, $from)">
                <!-- Вывод подстроки предшествующей образцу + вывод строки замены -->
                <xsl:value-of select="substring-before($text, $from)"/>
                <xsl:value-of select="$to"/>


                <!-- Вход в итерацию -->
                <xsl:call-template name="ReplaceStr">
                    <!-- В качестве входного параметра задается подстрока после образца замены -->
                    <xsl:with-param name="text" select="substring-after($text, $from)"/>
                    <xsl:with-param name="from" select="$from"/>
                    <xsl:with-param name="to" select="$to"/>
                </xsl:call-template>

            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
	    Делает замену символа возврата каретки на служебный символы для JSON
	    Строка с результатом возвращается в выходной поток.
	-->
    <xsl:template name="ReplaceCR">
        <xsl:param name="text"/>

        <xsl:call-template name="ReplaceStr">
            <xsl:with-param name="text" select="$text" />
            <xsl:with-param name="from" select="'&#10;'" />
            <xsl:with-param name="to" select="'\n'" />
        </xsl:call-template>

    </xsl:template>

</xsl:stylesheet>