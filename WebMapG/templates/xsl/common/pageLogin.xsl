<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:js="urn:my-scripts">
    <xsl:import href="helperScripts.xsl" />

    <!--
        ****************************
        Генератор фоновых картинок
        ****************************
    -->

    <!--Индекс фоновой картинки (с ведущим нулем) -->
    <xsl:variable name="BG_IMAGE_INDEX" select="format-number(js:chooseImageIndex(6, 4, 30), '00')" />

    <!--
        ******************************************************************************************************
        Добавляет содержимое страницы Логин
        ******************************************************************************************************
    -->
    <xsl:template name="PageLogin">
        <!--Слой для отображения рабочего пространства-->
        <div class="page__layer page__layer_workspace">
            <xsl:element name="div">
                <xsl:attribute name="class">
                    <!--Используем "случайный" фон для страницы-->
                    <xsl:text>login login_bg-</xsl:text>
                    <xsl:value-of select="$BG_IMAGE_INDEX"/>
                </xsl:attribute>


                <div class="login__brand">
                    <!--
                        ******************************************************
                        Бренд
                        ******************************************************
                    -->
                    <xsl:element name="a">
                        <xsl:attribute name="class">login__brand-link</xsl:attribute>
                        <xsl:attribute name="href">
                            <xsl:text>https://www.ufin.online/</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="title">
                            <xsl:choose>
                                <!-- Русский -->
                                <xsl:when test="contains(/response/@language, 'ru')">
                                    <xsl:text>Официальный сайт UFIN</xsl:text>
                                </xsl:when>
                                <!-- В остальных случаях - Английский -->
                                <xsl:when test="not(contains(/response/@language, 'ru'))">
                                    <xsl:text>UFIN official website</xsl:text>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:element>
                </div>


                <!--
                    ******************************************************
                    Основной диалог ввода пароля
                    ******************************************************
                -->
                <form class="login__dialog" action="login.aspx" method="post">
                    <input type="hidden" name="a" value="login" />
                    <div class="login__dialog-title">
                        <a class="logo__link" href="https://www.ufin.online" title="" target="_blank">
                            <span class="logo__link_u">U</span>fin
                        </a>
                    </div>
                    <div class="login__dialog-content">
                        <div class="control-list control-list_vert">
                            <div class="control-list__body">
                                <div class="control control_aside control_aside_100 control_aside-label input" id="username">
                                    <div class="control__label input__label">
                                        <xsl:choose>
                                            <!-- Русский -->
                                            <xsl:when test="contains(/response/@language, 'ru')">
                                                <xsl:text>Логин:</xsl:text>
                                            </xsl:when>
                                            <!-- В остальных случаях - Английский -->
                                            <xsl:when test="not(contains(/response/@language, 'ru'))">
                                                <xsl:text>Name:</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </div>
                                    <div class="input__box">
                                        <input class="control__elem input__elem" type="text" name="login" />
                                        <div class="control__selector input__selector"></div>
                                    </div>
                                </div>
                                <div class="control control_aside control_aside_100 control_aside-label input" id="password">
                                    <div class="control__label input__label">
                                        <xsl:choose>
                                            <!-- Русский -->
                                            <xsl:when test="contains(/response/@language, 'ru')">
                                                <xsl:text>Пароль:</xsl:text>
                                            </xsl:when>
                                            <!-- В остальных случаях - Английский -->
                                            <xsl:when test="not(contains(/response/@language, 'ru'))">
                                                <xsl:text>Password:</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </div>
                                    <div class="input__box">
                                        <input class="control__elem input__elem" type="password" name="password" />
                                    </div>
                                </div>
                                <div class="control control_aside control_aside_100 button control_default control_opaque button_link" id="enter">
                                    <button class="control__elem button__elem" style="width:220px">
                                        <span class="control__label button__label">
                                            <xsl:choose>
                                                <!-- Русский -->
                                                <xsl:when test="contains(/response/@language, 'ru')">
                                                    <xsl:text>Войти</xsl:text>
                                                </xsl:when>
                                                <!-- В остальных случаях - Английский -->
                                                <xsl:when test="not(contains(/response/@language, 'ru'))">
                                                    <xsl:text>Login</xsl:text>
                                                </xsl:when>
                                            </xsl:choose>
                                        </span>
                                    </button>
                                </div>
                                <div class="control login__options">
                                    <a class="login__link login__link_registration" href="#register">
                                        <xsl:choose>
                                            <xsl:when test="contains(/response/@language, 'ru')">
                                                <xsl:text>Регистрация</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="not(contains(/response/@language, 'ru'))">
                                                <xsl:text>Register now</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </a>
                                    <a class="login__link login__link_password-recovery" href="#password">
                                        <xsl:choose>
                                            <xsl:when test="contains(/response/@language, 'ru')">
                                                <xsl:text>Забыли пароль?</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="not(contains(/response/@language, 'ru'))">
                                                <xsl:text>Forgot password?</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </a>
                                </div>
                                <div class="control login__support">
                                    <div><xsl:choose>
                                        <xsl:when test="contains(/response/@language, 'ru')">
                                            <xsl:text>Тех. поддержка</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="not(contains(/response/@language, 'ru'))">
                                            <xsl:text>Support</xsl:text>
                                        </xsl:when>
                                    </xsl:choose></div>
                                    <a href="mailto:support@ufin.online">support@ufin.online</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </xsl:element>
        </div>

        <!--Слой для диалоговых окон -->
        <div class="page__layer page__layer_dialogs"></div>
    </xsl:template>

</xsl:stylesheet>