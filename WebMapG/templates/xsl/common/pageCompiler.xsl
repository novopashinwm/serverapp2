<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:local="urn:local" extension-element-prefixes="msxsl">

	<xsl:import href="scriptResponse.xsl" />
    <xsl:import href="scriptModernizr.xsl" />
    <xsl:import href="helperTemplates.xsl" />
    <xsl:import href="pageLogin.xsl" />

    <msxsl:script language="CSharp" implements-prefix="local">
        public string dateTimeNow()
        {
        return DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    </msxsl:script><xsl:param name="timing" select="local:dateTimeNow()"/>
	<!--
        ******************************************************************************************************
		Основной модуль формирования HTML-страницы для клиента.
		В качестве параметра page принимает название формируемой страницы.
        ******************************************************************************************************
    -->

	<xsl:template name="PageCompiler">
        <xsl:param name="app" select="'ufin.v2'" />
        <xsl:param name="page" />
		<xsl:param name="includeModernizr" select="true()" />
		<xsl:param name="includeResponse" select="true()" />
		<xsl:param name="includeNoScript" select="true()" />
		<xsl:param name="includeBadBrowser" select="true()" />
		<xsl:param name="includeLoading" select="true()" />


		<!-- Создаем элемент html с перечнем классов, зависящих от настроек, переданных сервером в response. -->
		<xsl:element name="html">
			<xsl:attribute name="class">
				<xsl:text>app</xsl:text>
				<!-- 
					******************
					Язык приложения 
					******************
				-->
				<xsl:text> app_lang_</xsl:text>
				<xsl:choose>
					<!-- Русский -->
					<xsl:when test="contains(/response/@language, 'ru')">
						<xsl:text>ru</xsl:text>
					</xsl:when>
					<!-- В остальных случаях - Английский -->
					<xsl:when test="not(contains(/response/@language, 'ru'))">
						<xsl:text>en</xsl:text>
					</xsl:when>
				</xsl:choose>
				<!-- 
					******************
					Текущая страница
					******************
				-->
				<xsl:text> app_page_</xsl:text>
				<xsl:value-of select="$page" />
				<!-- 
					******************
					Логотип заказчика
					******************
				-->
				<xsl:text> app_logo_</xsl:text>
                <xsl:choose>
                    <xsl:when test="not(contains(/response/@logo, 'XXX'))">
                        <xsl:text>ufin</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <!--
					******************
					Тема оформления
					******************
				-->
				<xsl:text> app_theme_dark</xsl:text>
			</xsl:attribute>
			
			<head>
                <!--Добавляем заголовок страницы-->
                <title><xsl:value-of select="/response/@title"/></title>
                <!--Добавляем (если разрешено) скрипты с ответом сервера и определения функциональных возможностей браузера-->
                <xsl:if test="$includeResponse = true()"><xsl:call-template name="ScriptResponse" /></xsl:if>
                <xsl:if test="$includeModernizr = true()"><xsl:call-template name="ScriptModernizr" /></xsl:if>

                <!--
                    ******************************************************
                    Настраиваем необходимые пути для загрузки ресурсов
                    ******************************************************
                -->
                <!--Корневой путь к серверу -->
                <xsl:variable name="rootPath">
                    <xsl:call-template name="RemoveTrailSlash">
                        <xsl:with-param name="path" select="/response/@baseWebAppPath" />
                    </xsl:call-template>
                </xsl:variable>
                <!--Абсолютный путь к стилям -->
                <!-- (!) После тестирования заменить на /response/@mainCss -->
                <xsl:variable name="cssUrl">
                    <xsl:value-of select="$rootPath" />
                    <xsl:call-template name="AddLeadingSlash">
                        <xsl:with-param name="path" select="concat('css/', $app, '.css')" />
                    </xsl:call-template>
                </xsl:variable>
                <!--Абсолютный путь к костылям для IE -->
                <xsl:variable name="cssIE6Url">
                    <xsl:value-of select="$rootPath" />
                    <xsl:call-template name="AddLeadingSlash">
                        <xsl:with-param name="path" select="'css/ie/ie6.css'" />
                    </xsl:call-template>
                </xsl:variable>
                <!--Относительный путь к основному скрипту приложения -->
                <!-- (!) После тестирования заменить на /response/@mainJavascript -->
                <xsl:variable name="jsUrl">
                    <xsl:value-of select="$rootPath" />
                    <xsl:call-template name="AddLeadingSlash">
                        <xsl:with-param name="path" select="concat('js/', $app, '.js', '?v=', $timing)" />
                    </xsl:call-template>
                </xsl:variable>

                <!--Относительный путь к скрипту видеонаблюдения -->
                <!-- (!) После тестирования заменить на /response/@mainJavascript -->
                <xsl:variable name="jsVideoUrl">
                    <xsl:value-of select="$rootPath" />
                    <xsl:call-template name="AddLeadingSlash">
                        <xsl:with-param name="path" select="concat('player/swfobject-all.js', '?v=', $timing)" />
                    </xsl:call-template>
                </xsl:variable>

                <!--Абсолютный путь к иконке сайта -->
                <xsl:variable name="faviconUrl">
                    <xsl:value-of select="$rootPath" />
                    <xsl:call-template name="AddLeadingSlash">
                        <xsl:with-param name="path" select="/response/@favicon" />
                    </xsl:call-template>
                </xsl:variable>

                <!--Добавляем иконку-->
                <link rel="shortcut icon" href="{$faviconUrl}" />

                <!--Грузим стили-->
                <link rel="stylesheet" type="text/css" href="{$cssUrl}" />
                <!--Грузим костыли для IE-->
                <xsl:comment>
                    <xsl:text disable-output-escaping="yes"><![CDATA[[if lte IE 6]><link rel="stylesheet" type="text/css" href="]]></xsl:text>
                    <xsl:value-of select="$cssIE6Url" />
                    <xsl:text disable-output-escaping="yes"><![CDATA["><![endif]]]></xsl:text>
                </xsl:comment>

                <!--Загружаем приложение-->
                <script src="{$rootPath}/js/libs/require.js" data-main="{$jsUrl}"></script>
                <!--Загружаем видеоплеер-->
                <script src="{$jsVideoUrl}"></script>


<!--                <script src="{$rootPath}/player/js/jquery.min.js"></script>-->
<!--                <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
<!--                <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>-->

            </head>
            <body>
                <!--Базовая разметка страницы-->
				<div class="page">
                    <!--
                        ******************************************************
                        Предупреждение об отсутствии JavaScript
                        ******************************************************
                    -->
                    <xsl:if test="$includeNoScript = true()">
                    <noscript>
                        <div class="page__layer page__layer_material page__layer_no-script">
                            <div class="error">
                                <xsl:choose>
                                    <!-- Русский -->
                                    <xsl:when test="contains(/response/@language, 'ru')">
                                        <xsl:text>Для корректной работы приложения требуется JavaScript. Пожалуйста, включите поддержку JavaScript в вашем браузере и обновите эту страницу.</xsl:text>
                                    </xsl:when>
                                    <!-- В остальных случаях - Английский -->
                                    <xsl:when test="not(contains(/response/@language, 'ru'))">
                                        <xsl:text>This web application requires JavaScript to work properly. Please enable JavaScript in your browser and refresh the page.</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </div>
                            <div class="page__logo"><span class="page__logo_u">U</span>fin</div>
                        </div>
                    </noscript>
                    </xsl:if>
                    <!--
                        ******************************************************
                        Предупреждение об устаревшем браузере
                        ******************************************************
                    -->
                    <xsl:if test="$includeBadBrowser = true()">
                    <div class="page__layer page__layer_material page__layer_bad-browser">
                        <div class="error">
                            <xsl:choose>
                                <!-- Русский -->
                                <xsl:when test="contains(/response/@language, 'ru')">
                                    <p class="error__title">Уважаемый пользователь!</p>
                                    <p class="error__text">Для корректной работы с сервисом Ufin просим Вас использовать следующие браузеры:</p>
                                </xsl:when>
                                <!-- В остальных случаях - Английский -->
                                <xsl:when test="not(contains(/response/@language, 'ru'))">
                                    <p class="error__title">Dear user,</p>
                                    <p class="error__text">Please use one of listed below browsers to get access to Ufin:</p>
                                </xsl:when>
                            </xsl:choose>

                            <!--Список браузеров для скачивания-->
                            <ul class="error__browser-list">
                                <li class="error__browser-item error__browser-item_chrome"><a class="error__browser-link" href="http://www.google.com/chrome/">Google Chrome</a></li>
                                <li class="error__browser-item error__browser-item_firefox"><a class="error__browser-link" href="http://www.mozilla.org/firefox/">Mozila Firefox</a></li>
                                <li class="error__browser-item error__browser-item_opera"><a class="error__browser-link" href="http://www.opera.com/">Opera</a></li>
                            </ul>

                            <xsl:choose>
                                <!-- Русский -->
                                <xsl:when test="contains(/response/@language, 'ru')">
                                    <p class="error__text">Спасибо!</p>
                                </xsl:when>
                                <!-- В остальных случаях - Английский -->
                                <xsl:when test="not(contains(/response/@language, 'ru'))">
                                    <p class="error__text">Thank you!</p>
                                </xsl:when>
                            </xsl:choose>
                        </div>
                        <div class="page__logo"><span class="page__logo_u">U</span>fin</div>
                    </div>
                    </xsl:if>
                    <!--
                        ******************************************************
                        Слой отображения процесса загрузки страницы
                        ******************************************************
                    -->
                    <xsl:if test="$includeLoading = true()">
                    <div class="page__layer page__layer_material page__layer_loading">
                        <div class="page__indicator page__indicator_start"><span class="page__indicator-text"><span class="page__indicator-text_u">U</span>fin</span></div>
                        <div class="page__logo"><span class="page__logo_u">U</span>fin</div>
                    </div>
                    </xsl:if>

                    <!--
                        ******************************************************
                        Персональное содержимое для отдельных страниц
                        ******************************************************
                    -->
                    <xsl:choose>
                        <!--Контент для страницы Логин-->
                        <xsl:when test="$page = 'login'">
                            <xsl:call-template name="PageLogin" />
                        </xsl:when>
                    </xsl:choose>

                </div>
            </body>
		</xsl:element>
		
	</xsl:template>

</xsl:stylesheet>