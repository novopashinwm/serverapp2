﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:import href="common.xsl" />
  <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="/response" />
  </xsl:template>

  <xsl:template match="response">
    <html>
      <head>
        <title>
          Заявка на расчёт стоимости зарегистрирована
        </title>
        <xsl:apply-templates select="." mode="headContent" />
      </head>
      <body class="exceptionMessagePage">
        <xsl:call-template name="common-page-header"></xsl:call-template>
        <div id="additionalPageContent">
          <div class="main-content normal-message">
            <h1>
              Заявка на расчёт стоимости зарегистрирована
            </h1>
            <p>
              Благодарим за проявленный интерес.
            </p>
            <p>
              Мы свяжемся с Вами в самое ближайшее время.
            </p>
          </div>
        </div>
        <xsl:apply-templates select="." mode="pageFooter" />
          <xsl:call-template name="websso"/>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
