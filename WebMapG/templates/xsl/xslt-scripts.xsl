﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
                exclude-result-prefixes="msxsl"
                xmlns:user="urn:my-scripts"
>
  <msxsl:script language="C#" implements-prefix="user">
    <![CDATA[
    public string GetTimeSpanUnit(XPathNodeIterator node)
    {  		
      if (node != null && node.MoveNext() && node.Current != null && !string.IsNullOrEmpty(node.Current.Value)) {
        System.TimeSpan ts = System.Xml.XmlConvert.ToTimeSpan(node.Current.Value);
        if (ts.Minutes == 0) {
          if (ts.Hours != 0)
            return "hour";
          if (ts.Days != 0)
            return "day";
        }
        return "minute";
      }
      return null;
    }

    public string GetTimeSpanValue(XPathNodeIterator node)
    {  	
      if (node != null && node.MoveNext() && node.Current != null && !string.IsNullOrEmpty(node.Current.Value)) {
        System.TimeSpan ts = System.Xml.XmlConvert.ToTimeSpan(node.Current.Value);
        if (ts.Minutes == 0) {
          if (ts.Hours != 0)
            return ((int)ts.TotalHours).ToString();
          if (ts.Days != 0)
            return ((int)ts.TotalDays).ToString();
        }
        return ((int)ts.TotalMinutes).ToString();
      }
      return null;
    }

    public string GetDate(string s)
    {
      if (string.IsNullOrEmpty(s))
        return null;
      return System.Xml.XmlConvert.ToDateTime(s).Date.ToString("dd.MM.yyyy");
    }
    
    public string GetDate(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null)
        return null;
      return GetDate(node.Current.Value);
    }

    public string GetTime(string s)
    {
        if (string.IsNullOrEmpty(s))
            return null;
        return ToString(System.Xml.XmlConvert.ToDateTime(s).TimeOfDay);
    }
    
    public string GetTime(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null)
        return null;
      return GetTime(node.Current.Value);
    }

    public bool IsDateTimeMax(string s)
    {
        if (string.IsNullOrEmpty(s))
            return true;
        return System.Xml.XmlConvert.ToDateTime(s) == System.DateTime.MaxValue;
    }
    
    public bool IsDateTimeMax(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null)
        return true;
      return IsDateTimeMax(node.Current.Value);
    }
    
    public string GetTimeSpan(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null || string.IsNullOrEmpty(node.Current.Value))
        return null;
        
      System.TimeSpan timeSpan;
      try
      {
        timeSpan = System.Xml.XmlConvert.ToTimeSpan(node.Current.Value);
      }
      catch (System.Exception)
      {
        return string.Empty;
      }
        
      return ToString(timeSpan);
    }
    
    public string ToString(System.TimeSpan timeSpan)
    {
      return timeSpan.Hours.ToString("00") + ":" + timeSpan.Minutes.ToString("00");
    }
    
    public string DateTimeToString(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null || string.IsNullOrEmpty(node.Current.Value))
        return null;
      System.DateTime dateTime = System.Xml.XmlConvert.ToDateTime(node.Current.Value);
      if (dateTime == System.DateTime.MinValue)
        return null;
      return dateTime.ToString("dd.MM.yyyy HH:mm");
    }
    
    public string LogTimeToDateTimeString(XPathNodeIterator logTimeNode, XPathNodeIterator timeZoneInfoNode)
    {
      int logTime;
      if (logTimeNode == null || !logTimeNode.MoveNext() || logTimeNode.Current == null || string.IsNullOrEmpty(logTimeNode.Current.Value) ||
          !int.TryParse(logTimeNode.Current.Value, out logTime))
        return null;
      
      string timeZoneInfoString = timeZoneInfoNode != null && timeZoneInfoNode.MoveNext() && timeZoneInfoNode.Current != null 
        ? timeZoneInfoNode.Current.Value
        : null;
      
      System.TimeZoneInfo timeZone = string.IsNullOrEmpty(timeZoneInfoString) 
        ? System.TimeZoneInfo.Local 
        : System.TimeZoneInfo.FromSerializedString(timeZoneInfoString);
      
      DateTime year1970 = new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc );
      DateTime utcDateTime = year1970.AddSeconds(logTime);
      
      DateTime localDateTime = System.TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, timeZone);
      
      return localDateTime.ToString("dd.MM.yyyy HH:mm");
    }
    
    public string DateToString(XPathNodeIterator node)
    {
      if (node == null || !node.MoveNext() || node.Current == null || string.IsNullOrEmpty(node.Current.Value))
        return null;
      System.DateTime dateTime = System.Xml.XmlConvert.ToDateTime(node.Current.Value);
      if (dateTime == System.DateTime.MinValue)
        return null;
      return dateTime.ToString("dd.MM.yyyy");
    }
    
    public bool Contains(string x, string y) {
      if (x == null || y == null)
        return false;
      return x.IndexOf(y, StringComparison.OrdinalIgnoreCase) != -1;
    }
]]>
  </msxsl:script>
</xsl:stylesheet>
