<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <xsl:import href="common.xsl" />
    <xsl:import href="uielements.xsl" />
    <xsl:import href="dateTime.xsl" />
    <xsl:import href="strings.xsl" />
    <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <html class="page_delivery">
            <head>
                <script>
                    //В новом интерфейса разрешаем отображать старую страницу только внутри фрейма.
                    //Если это не так - используем правильную ссылку.
                    if (window.top === window) location.replace('map.aspx#page/reports');
                </script>
                <xsl:apply-templates select="./response" mode="headContent">
                    <xsl:with-param name="title" select="$locale/data[@name='deliveryList']/value" />
                </xsl:apply-templates>

                <script type="text/javascript">
                    UFIN.pageType = 'reportDelivery';
                </script>
            </head>
            <body>
                <xsl:call-template name="commonBodyIncludes" />

                <xsl:apply-templates select="./response" mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'report'" />
                </xsl:apply-templates>
                <div id="additionalPageContent">
                    <!-- AS: выбор шаблона -->
                    <xsl:apply-templates select="./response" mode="topSelector" />
                    <xsl:apply-templates select="./response" />
                </div>
                <xsl:apply-templates select="." mode="pageFooter" />

                <xsl:call-template name="requireJS" />
                <xsl:call-template name="websso"/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="response" mode="topSelector">
        <xsl:if test="./message">
            <p style="margin:10px" class="message Warning">
                <xsl:call-template name="replace">
                    <xsl:with-param name="str" select="./message/MessageBody" />
                </xsl:call-template>
            </p>
        </xsl:if>
    </xsl:template>


    <xsl:template match="response[@template='deliveryList']">
        <xsl:variable name="language" select="./@language" />


        <div>
                <div id="report-menu" class="aside-block">
                    <div class="legend">
                        <xsl:value-of select="$locale/data[@name='reports']/value" />
                    </div>
                    <div class="menu-right">
                        <ul>
                            <li>
                                <a href="Report.aspx">
                                    <xsl:value-of select="$locale/data[@name='reports']/value" />
                                </a>
                            </li>
                            <li class="selected">
                                <a>
                                    <xsl:value-of select="$locale/data[@name='deliveryList']/value" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="main-content delivery">
                    <div class="delivery__info">
                    </div>
                    <div class="delivery__container">
                        <div id="deliveryListContainer"></div>
                    </div>
                </div>
        </div>

    </xsl:template>
</xsl:stylesheet>
