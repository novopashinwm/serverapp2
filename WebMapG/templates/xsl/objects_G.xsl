﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
    <!ENTITY nbsp "&#160;">
    <!ENTITY hellip "&#8230;">
    <!ENTITY laquo "&#171;">
    <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:user="urn:my-scripts">
    <xsl:import href="xslt-scripts.xsl" />
    <xsl:import href="common.xsl" />
    <xsl:import href="trackingObject_G.xsl" />
    <xsl:import href="dateTime.xsl" />
    <xsl:import href="charts.xsl" />
    <xsl:import href="geozone.xsl" />
    <xsl:import href="uielements.xsl" />

    <!--xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/-->
    <!--xsl:output method="xhtml" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" /-->
    <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//@template = 'objectInfo'">
                <xsl:apply-templates select="." mode="objectInfo" />
            </xsl:when>
            <xsl:when test="//@template = 'objectEdit'">
                <xsl:apply-templates select="." mode="objectEdit" />
            </xsl:when>
            <xsl:when test="//@template = 'iconSelection'">
                <xsl:apply-templates select="." mode="iconSelection" />
            </xsl:when>
            <xsl:when test="//@template = 'shortList'">
                <xsl:apply-templates select="." mode="shortList" />
            </xsl:when>
            <xsl:when test="//@template = 'journal'">
                <xsl:apply-templates select="." mode="journal" />
            </xsl:when>
            <xsl:when test="//@template = 'getHistory'">
                <xsl:apply-templates select="." mode="getHistory" />
            </xsl:when>
            <xsl:when test="//@template = 'getHistoryDetails'">
                <xsl:apply-templates select="." mode="getHistoryDetails" />
            </xsl:when>
            <xsl:when test="//@template = 'fuelChartContent'">
                <xsl:apply-templates select="." mode="fuelChartContent" />
            </xsl:when>
            <xsl:when test="//@template = 'speedChartContent'">
                <xsl:apply-templates select="." mode="speedChartContent" />
            </xsl:when>
            <xsl:when test="//@template = 'mlp_tracking_schedule'">
                <xsl:apply-templates select="." mode="mlp_tracking_schedule" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:variable name="path" select="/response/@ApplicationPath" />

    <xsl:template match="response" mode="objectInfo">
        <html class="page_object" xmlns="http://www.w3.org/1999/xhtml">
            <head>

                <xsl:apply-templates select="." mode="headContent" />
              <xsl:call-template name="link-css" />

              <xsl:call-template name="applicationPath" />
                <script type="text/javascript">
                    <xsl:text>var dataObjects = {</xsl:text>
                    <xsl:text>legends:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="./@legendsJSON">
                            <xsl:value-of select="./@legendsJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            ''
                        </xsl:otherwise>
                    </xsl:choose>



                    <xsl:text>}</xsl:text>

                    var UFIN = UFIN || {};
                    UFIN.vehicleId = '<xsl:value-of select="./vehicle/@id"/>';
                    UFIN.promoUrl = "<xsl:value-of select="$pathAbout"/>";
                    
                    UFIN.pageType = 'object';
                    UFIN.pageSubType = 'objectsList';
                    UFIN.customerType = '<xsl:value-of select="/response/Departments/Department/Type" />'.toLowerCase();

                </script>
            </head>

            <body>
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
                <xsl:call-template name="commonBodyIncludes" />

                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'objects'" />
                </xsl:apply-templates>
                <!--<form action="{./@ApplicationPath}/objects.aspx" method="post" style="height:80%;width:100%;">-->
                <!--<div class="clear"></div>-->

                <xsl:variable name="showPersonalData">
                    <xsl:choose>
                        <xsl:when test="/response/@loggedUser = 'guest'">false</xsl:when>
                        <xsl:otherwise>true</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <div class="b-content">

                  <!-- Вертикальное меню располагается здесь -->
                  
                    <!--<div>
                        <xsl:apply-templates select="./message" />
                    </div> -->
                    <xsl:choose>
                        <xsl:when test="./vehicle">
                            <xsl:apply-templates select="./vehicle" mode="objectInformation" />
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- Отключаем возможность случайного назначения контроллера гостевому пользователю -->
                            <xsl:if test="/response/@loggedUser != 'guest'">

                                <div id="actionsDiv">
                                </div>
                                <div class="clear"></div>
                            </xsl:if>

                            <div id="bigList">

                            </div>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:call-template name="clear" />
                </div>
                <!--</form>-->
                <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />



            </body>
        </html>
    </xsl:template>

    <xsl:template match="vehicle" mode="iconPath">
        <xsl:choose>
            <xsl:when test="./settings/icon and ./settings/icon != '' ">
                <xsl:value-of select="./settings/icon"/>
              <xsl:if test="substring(./settings/icon, string-length(./settings/icon)-4+1, 4) != '.png'">.png</xsl:if>
            </xsl:when>
            <xsl:when test="./@vehicleKind = 'Tracker'">
                <xsl:value-of select="/response/@ApplicationPath"/>
                <xsl:text>/img/userdata/defaults/default-tracker.png</xsl:text>
            </xsl:when>
            <xsl:when test="./@vehicleKind = 'MobilePhone'">
                <xsl:value-of select="/response/@ApplicationPath"/>
                <xsl:text>/img/userdata/defaults/default-cellphone.png</xsl:text>
            </xsl:when>
            <xsl:when test="./@vehicleKind = 'WebCamera'">
                <xsl:value-of select="/response/@ApplicationPath"/>
                <xsl:text>/img/userdata/defaults/default-web-camera.png</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/response/@ApplicationPath"/>
                <xsl:value-of select="/response/@DefaultVehicleIcon" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: верхние переключалки -->
    <xsl:template match="response" mode="topSelector">
        <xsl:if test="./vehicle/@id &gt; 0">
            <xsl:variable name="vehicleIcon">
                <xsl:apply-templates select="./vehicle" mode="iconPath" />
            </xsl:variable>

            <div class="main-navigation" id="objectInfoNav">
                <div>
                    <xsl:if test="./@template = 'journal' and $vehicleIcon != ''">
                        <img src="{$vehicleIcon}" alt="{$locale/data[@name='vehicleIcon']/value}" class="vehicle-icon" />
                    </xsl:if>

                    <b>
                        <xsl:apply-templates select="./vehicle" mode="garage-number" />
                    </b>

                    <xsl:if test="./@template = 'iconSelection'" >
                        <a href="javascript:jQuery.monitoringObject.onChangeIcon()" class="btn">
                            <xsl:value-of select="$locale/data[@name='save']/value" />
                        </a>
                        <a href="javascript:history.back(-1)" style="margin-left:30px;">
                            <xsl:value-of select="$locale/data[@name='cancel']/value" />
                        </a>
                    </xsl:if>

                    <xsl:if test="./@template = 'journal' or ./@template ='objectInfo'">
                        <xsl:if test="./@template = 'objectInfo'">
                            <xsl:choose>
                                <xsl:when test="/response/@loggedUser != 'guest'">
                                    <a class="btn" href="{/response/@ApplicationPath}/rules.aspx?objectIdType=Vehicle&amp;objectId={./vehicle/@id}" title="{$locale/data[@name='rules']/description}">
                                        <xsl:value-of select="$locale/data[@name='rules']/value" />
                                    </a>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:if>

                        <!--На карте-->
                        <a class="btn" href="{./@ApplicationPath}/map.aspx?moid={./vehicle/@id}" title="{$locale/data[@name='map']/description}">
                            <xsl:value-of select="$locale/data[@name='map']/value" />
                        </a>

                        <xsl:if test="./vehicle/Rights/Right[@Name='ManageSensors'] and ./vehicle/controllers/Controller/Type[@HasInputs='true']">
                            <a class="btn" href="{/response/@ApplicationPath}/objects.aspx?id={./vehicle/@id}#10" title="{$locale/data[@name='sensors']/description}">
                                <xsl:value-of select="$locale/data[@name='sensors']/value" />
                            </a>
                        </xsl:if>

                    </xsl:if>
                </div>
            </div>
        </xsl:if>
        <!--end of string-length(./vehicle/@GarageNumber) &gt; 0-->
    </xsl:template>

    <!-- AS: инфа об объекте -->
    <xsl:template match="vehicle" mode="objectInformation">
        <xsl:variable name="showPersonalData">
            <xsl:choose>
                <xsl:when test="/response/@loggedUser = 'guest'">false</xsl:when>
                <xsl:otherwise>true</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>



        <div class="main-content">
            <div  class="tabbable aaa" id="vehicle-info-tabs" data-tab-tabs="#objectTabs">
                <div class="tab-content" tab-index="1">
                    <div class="objectInfo"></div>
                </div>

                <div class="tab-content" tab-index="2">
                    <div class="driverInfo">
                        <div class="loading"></div>
                    </div>
                </div>
                <div class="tab-content" tab-index="3">
                    <div class="ownerInfo">
                        <div class="laoding"></div>
                    </div>
                </div>

                <div class="tab-content" tab-index="4">
                    <div class="trackerInfo">
                        <div class="loading"></div>
                    </div>
                    
                  <!-- TODO: вернуть и отладить -->
                  <xsl:if test="false">
                    <div class="controlBlock mapRow">
                        <div>
                            <div title="Для установки положения камеры, кликните по карте или введите адрес.">
                                <xsl:value-of select="$locale/data[@name='PlacedOnMap']/value"/>
                            </div>
                            <div class="addCoord" >
                                <div class="input-group">
                                    <input type="text" id="mapSearchCriteriaBox" title="{$locale/data[@name='PlacedOnMapAddress']/value}" placeholder="{$locale/data[@name='PlacedOnMapAddress']/value}" class="inp" />
                                    <span class="input-group-btn">
                                        <button class="btn" id="mapSearchButton">
                                            <span class="typcn typcn-zoom"></span>
                                        </button>
                                    </span>
                                </div>
                                <div class="not-found">
                                    Адрес не найден
                                </div>
                            </div>
                            <p></p>
                            <div class="clear"></div>
                            <p></p>
                            <p>
                                <em>Для установки положения камеры, кликните по карте или введите адрес.</em>
                            </p>
                            <p></p>
                            <div id="map_wrapper">
                                <div id="mapG"></div>
                            </div>
                        </div>
                    </div>
                    </xsl:if>
                </div>
                <div class="tab-content" tab-index="5">
                    <div class="dateInfo">
                        <div class="loading"></div>
                    </div>
                </div>
                <div class="tab-content" tab-index="6">
                    <div class="groupInfo">
                        <div class="loading"></div>
                    </div>
                </div>
                <div class="tab-content" tab-index="7">
                    <div class="accessInfo">
                        <div class="loading"></div>
                    </div>
                </div>
                <div class="tab-content" tab-index="8">
                    <div class="scheduleInfo vertical"></div>
                    </div>
                <div class="tab-content" tab-index="9">
                    <div class="ruleInfo"></div>
                </div>

				<div class="tab-content" tab-index="10">
					<div class="sensorInfo"></div>
				</div>
            </div>
        </div>

        <xsl:call-template name="clear" />
    </xsl:template>

    <xsl:template match="vehicle" mode="selected-icon">
        <xsl:choose>
            <xsl:when test="./settings and ./settings/icon">
                <xsl:call-template name="formatImage">
                    <xsl:with-param name="browser" select="/response/@browser" />
                    <xsl:with-param name="width" select="'32'" />
                    <xsl:with-param name="height" select="'32'" />
                    <xsl:with-param name="src" select="./settings/icon" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="showDefaultIcon" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: ифа о контроллере -->
    <xsl:template match="Controller">
        <xsl:param name="showData" select="'true'" />

        <div style="width:100%;margin-bottom:10px;">
            <div style="float:left;width:30%;">
                <xsl:value-of select="$locale/data[@name='controllerNumber']/value" />:
            </div>
            <div style="float:left;width:70%">
                <xsl:choose>
                    <xsl:when test="$showData = 'true'">
                        <xsl:value-of select="./Number" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$locale/data[@name='notAvailable']/value" />
                    </xsl:otherwise>
                </xsl:choose>
            </div>
            <xsl:call-template name="clear"/>
        </div>
    </xsl:template>

    <!-- AS: ифа о владельце -->
    <xsl:template match="Person">
        <xsl:param name="showData" select="'true'" />
        <fieldset>
            <table class="object-info">
                <tr>
                    <td>
                        <label>
                            <xsl:value-of select="$locale/data[@name='ownerName']/value" />:
                        </label>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when test="$showData = 'true'">
                                <xsl:value-of select="concat(./FirstName, ' ', ./SecondName, ' ', ./LastName)" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$locale/data[@name='notAvailable']/value" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <xsl:value-of select="$locale/data[@name='ownerPhone']/value" />:
                        </label>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when test="$showData = 'true'">
                                <xsl:for-each select="./Phones/Contact">
                                    <div>
                                        <xsl:value-of select="./Value" />
                                    </div>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$locale/data[@name='notAvailable']/value" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
            </table>
        </fieldset>
    </xsl:template>

    <!-- AS: сериализация объектов в списке -->
    <xsl:template match="response" mode="shortList">
        <xsl:apply-templates select="./objects/vehicle" mode="shortList" />
    </xsl:template>

    <xsl:template match="vehicle" mode="shortList">
        <div style="padding:10px;">
            <a href="{/response/@ApplicationPath}/objects.aspx?id={./@id}">
                <xsl:value-of select="./@GarageNumber" />
            </a>
            <div class="grey">
            </div>
        </div>
    </xsl:template>

    <!-- AS: список объектов на странице -->
    <xsl:template match="objects" mode="bigList">
        <div style="float:left; width:49%;padding-left:5px;border-bottom:1px solid #ccc;padding-bottom:10px;">
            <b>
                <xsl:value-of select="$locale/data[@name='objectName']/value" />
            </b>
        </div>
        <div style="float:left; width:49%;padding-left:5px;border-bottom:1px solid #ccc;padding-bottom:10px;">
            <b>
                <xsl:value-of select="$locale/data[@name='objectName']/value" />
            </b>
        </div>
        <div class="clear"></div>

        <div style="float:left; width:49%">
            <xsl:apply-templates select="./vehicle[position() mod 2 = 0]" mode="shortList" />
        </div>
        <div style="float:left; width:49%">
            <xsl:apply-templates select="./vehicle[position() mod 2 != 0]" mode="shortList" />
        </div>
        <div class="clear"></div>
    </xsl:template>

    <!-- AS: журнал -->
    <xsl:template match="response" mode="journal">
        <html class="page_history" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <script>
                    //В новом интерфейса разрешаем отображать старую страницу только внутри фрейма.
                    //Если это не так - используем правильную ссылку.
                    if (window.top === window) {
                        //Извлекаем индекс объекта из адресной строки
                        var match = location.href.match(/[?\x26]id=(\d+)/);
                        var id = +(match ? match[1] : 0);
                        location.replace('map.aspx#page/history' + (id ? '/' + id : ''));
                    }
                </script>
                <xsl:apply-templates select="." mode="headContent">
                    <xsl:with-param name="title" select="./@title" />
                </xsl:apply-templates>
                <xsl:call-template name="link-css" />
                
                <!--jstorage-->
                <script type="text/javascript">
                    UFIN.pageType = 'journal';
                    UFIN.pageType2 = 'journal';
                    var autoSearch = false;
                    <xsl:if test="not(./HistoryParams) or (./HistoryParams/from = $minDateTime) or (./HistoryParams/to = $minDateTime) or (./HistoryParams/interval = 0)">
                        UFIN.noHistory = true;
                    </xsl:if>

                    UFIN.vehicleId = '<xsl:value-of select="/response/vehicle/@id"/>';
                    UFIN.mapMode = "history";

                    var adobeSvgInstallPath = '<xsl:value-of select="$resourceStrings/data[@name='adobeSvgPlayerInstallPath']/value" />';
                    var supportedSvg = false;
                </script>
            </head>

            <body style="overflow:hidden;">
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
                <xsl:call-template name="commonBodyIncludes" />

                <!-- AS: парсим параметры истории, и если они заданы, отображаем её -->
                <xsl:apply-templates select="./HistoryParams" />


                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'map'" />
                </xsl:apply-templates>

                <div id="additionalPageContentJournal" class="b-content">
                    <div id="panelTD">
                        <div id="panel" class="b-content__panel b-content__path">
                            <div id="trackingObjectList">
                                <div class="tabs">
                                    <div class="sub-tabs">
                                        <ul  id="objectTabs2">
                                            <li id="pathListSelector">
                                                <a href="javascript: void(0);">
                                                    <xsl:value-of select="$locale/data[@name='path']/value" />
                                                </a>
                                            </li>
                                            <li id="geozonesListSelector">
                                                <a href="javascript: void(0);">
                                                    <xsl:value-of select="$locale/data[@name='geozones']/value" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="sub-content">
                                        <div id="pathContent" class="sub-content-page">
                                            <div id="objectPanel" class="object_filter" style="margin-bottom:5px;">
                                                <div class="b-searchVehicle-label"></div>
                                                <div class="b-searchVehicle">
                                                    <div class="b-searchVehicle__navigation">
                                                        <a href="{./@ApplicationPath}/objects.aspx?id={./vehicle/@id}" title="{$locale/data[@name='properties']/value}" class="btn small inverted icon">
                                                            <span class="typcn typcn-th-list"></span>
                                                        </a>
                                                        <xsl:if test="./vehicle/Rights/Right[@Name='ManageSensors'] and ./vehicle/controllers/Controller/Type[@HasInputs='true']">
                                                            <a href="{/response/@ApplicationPath}/objects.aspx?id={./vehicle/@id}#10" title="{$locale/data[@name='sensors']/description}" class="btn small inverted icon">
                                                                <span class="typcn typcn-speed"></span>
                                                            </a>
                                                        </xsl:if>
                                                    </div>
                                                    <!--<div class="b-searchVehicle__icon">
                                                        <xsl:variable name="vehicleIcon">
                                                            <xsl:apply-templates select="./vehicle" mode="iconPath" />
                                                        </xsl:variable>
                                                        <xsl:if test="$vehicleIcon != ''">
                                                            <img src="{$vehicleIcon}" alt="{$locale/data[@name='vehicleIcon']/value}" />
                                                        </xsl:if>
                                                    </div>-->
                                                    <div class="b-searchVehicle__name">
                                                        <span class="text">
                                                            <xsl:apply-templates select="./vehicle" mode="garage-number" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <xsl:call-template name="journalInterval" />
                                            </div>
                                            <div id="trackingObjects">
                                            </div>
                                        </div>
                                        <div id="geozonesContent" class="sub-content-page">
                                            <div class="laodInProgress"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mapTD">
                        <div id="mapG"></div>
                        <xsl:call-template name="copyrights" />
                        <div class="clear"></div>
                        <div id="chartBar" style="width:100%;display:none;"></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <!--xsl:call-template name="slideshow" /-->

                <div id="saveZoneDlgContainer" style="display:none;">
                    <xsl:call-template name="saveZoneDlg" />
                </div>

                <xsl:call-template name="loginPopup">
                    <xsl:with-param name="path" select="./@ApplicationPath"/>
                </xsl:call-template>

                <xsl:call-template name="requireJS" />
            </body>
        </html>
    </xsl:template>

    <!-- AS: получение истории -->
    <xsl:template match="response" mode="getHistory">
        <response>
            <xsl:if test="./@renderList = 'true'">
                <div>
                    <xsl:if test="./message">
                        <p style="margin:10px" class="message Warning">
                            <xsl:call-template name="replace">
                                <xsl:with-param name="str" select="./message/MessageBody" />
                            </xsl:call-template>
                        </p>
                    </xsl:if>
                    <xsl:choose>
                        <xsl:when test="count(./history/point) &gt; 0">
                            <xsl:apply-templates select="./history/point" mode="list" />
                        </xsl:when>
                        <xsl:otherwise>
                            <p style="margin:10px" class="message Warning">
                                <xsl:value-of select="$locale/data[@name='noHistoryData']/value" />
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <splitter />

                <!-- AS: статистика -->
                <div style="margin:0;padding:0;">
                    <!--xsl:apply-templates select="." mode="maxSpeed" /-->
                    <xsl:apply-templates select="./history/statistics" />
                </div>
                <splitter />
            </xsl:if>
            <!--forMap-->
            <div id="objectJournalLayer">
                <!--
                <xsl:choose>
                                                <xsl:when test="count(./history/point) &lt; 3">
                                                        <xsl:apply-templates select="./history/point" mode="map" />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                        <xsl:apply-templates select="./history/point[1]" mode="mapFirst" />
                                                        <xsl:apply-templates select="./history/point[position() != 1 and position() != last()]" mode="map" />
                                                        <xsl:apply-templates select="./history/point[last()]" mode="mapLast" />
                                                </xsl:otherwise>
                                        </xsl:choose>
                <input type="hidden" id="latitude1" value="{./history/point[1]/unit[1]/LatitudeCP}"></input>
                                        <input type="hidden" id="longitude1" value="{./history/point[1]/unit[1]/LongitudeCP}"></input>
                -->
            </div>
            <splitter />
            <!-- AS: а здесь - график -->
            <!--xsl:if test="./charts/fuel"-->
            <div>
                <xsl:apply-templates select="." mode="charts" />
            </div>
            <!--/xsl:if-->
            <splitter />

            <!--Список для отображения на Google maps-->
            <omap>
                <xsl:apply-templates select="./history/point" mode="raw" >
                    <xsl:sort select="Time"/>
                </xsl:apply-templates>
            </omap>
        </response>
    </xsl:template>

    <!-- AS: все точки истории как есть -->
    <xsl:template match="point" mode="raw">
        <xsl:copy-of select="." />
    </xsl:template>

    <xsl:template match="unit" mode="list">
        <xsl:param name="dateFrom" select="''" />
        <xsl:param name="dateTo" select="''" />
        <div class="toggle" onclick="jQuery.centerClosestImageOnMap('{./Longitude}', '{./Latitude}')">
            <xsl:if test="./@badPosition = 'true'">
                <xsl:attribute name="class">incorrect</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="./Time" />
        </div>
    </xsl:template>

    <xsl:template match="point" mode="mapFirst">
        <xsl:choose>
            <xsl:when test="./@type = 'Stop'">
                <xsl:apply-templates select="./unit[1]" mode="mapFirst" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="./unit[1]" mode="mapFirst" />
                <xsl:apply-templates select="./unit[position() != 1]" mode="map" />
                <!--xsl:apply-templates select="./unit[last()]" mode="map" /-->
                <!--xsl:apply-templates select="./unit" mode="map" /-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="point" mode="mapLast">
        <xsl:choose>
            <xsl:when test="./@type = 'Stop'">
                <xsl:apply-templates select="./unit[1]" mode="mapLast" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="./unit[position() != last()]" mode="map" />
                <xsl:apply-templates select="./unit[last()]" mode="mapLast" />
                <!--xsl:apply-templates select="./unit" mode="map" /-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="point" mode="map">
        <xsl:choose>
            <xsl:when test="./@type = 'Stop'">
                <xsl:apply-templates select="./unit[1]" mode="map" />
            </xsl:when>
            <xsl:otherwise>
                <!--xsl:apply-templates select="./unit[1]" mode="map" /-->
                <xsl:apply-templates select="./unit" mode="map" />
                <!--xsl:apply-templates select="./unit[last()]" mode="map" /-->
                <!--xsl:apply-templates select="./unit" mode="map" /-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="unit" mode="mapFirst">
        <!--xsl:variable name="imageUrl">
                <xsl:apply-templates select="concat(/response/@ApplicationPath, '/img/icong1.png')" />
        </xsl:variable-->
        <xsl:variable name="imageUrl">
            <xsl:apply-templates select="." mode="imageJournalUrlFirst" />
        </xsl:variable>
        <xsl:variable name="imageUrlSelected">
            <xsl:apply-templates select="." mode="imageJournalUrlSelected" />
        </xsl:variable>

        <xsl:variable name="currentPosition">
            <xsl:value-of select="position()" />
        </xsl:variable>

        <xsl:variable name="xDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <xsl:value-of select="./X - ./preceding-sibling::unit/X" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="yDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <xsl:value-of select="./Y - ./preceding-sibling::unit/Y" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="jpDescriptionId">
            <xsl:value-of select="concat('journalPointDescription_', ./@groupNumber, position())" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="((position() != 1) and ($xDiff &gt; 20) and ($yDiff &gt; 20)) or position() = 1">
                <div class="jpDescriptionParent" style="position:absolute; left:{./X - 10}px; top:{./Y - 30}px; width:32px; height:32px" id="objectmap_{./ID}{./@groupNumber}{position()}">
                    <span class="toggle" onmouseover="jQuery.monitoringObject.showJournalPointDescription(event)" onmouseout="jQuery.monitoringObject.closeJournalPointDescription(event)">
                        <span class="imageRegular">
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'20'" />
                                <xsl:with-param name="height" select="'34'" />
                                <xsl:with-param name="src" select="$imageUrl" />
                            </xsl:call-template>
                        </span>
                        <span class="imageSelected" style="display:none;">
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'32'" />
                                <xsl:with-param name="height" select="'32'" />
                                <xsl:with-param name="src" select="$imageUrlSelected" />
                            </xsl:call-template>
                        </span>
                    </span>
                    <div id="{$jpDescriptionId}" class="jpDescription" style="display:none; position:relative;left:-75px;top:-80px;background-color:#fff;border:1px solid #ccc;width:150px; height:35px; padding:5px;">
                        <div class="utcDate3">
                            <xsl:variable name="timePeriod">
                                <xsl:variable name="timefrom">
                                    <xsl:call-template name="time-width-day">
                                        <xsl:with-param name="current-date" select="/response/@serverDate" />
                                        <xsl:with-param name="format-date" select="./Time" />
                                        <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                        <xsl:with-param name="lang" select="/response/@language" />
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:variable name="timeto">
                                    <xsl:call-template name="time-width-day">
                                        <xsl:with-param name="current-date" select="/response/@serverDate" />
                                        <xsl:with-param name="format-date" select="../unit[last()]/Time" />
                                        <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                        <xsl:with-param name="lang" select="/response/@language" />
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:value-of select="$timefrom" />
                                <xsl:if test="../@type = 'Stop'">
                                    <xsl:text>&#160;&#8212;</xsl:text>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="$timeto" />
                                </xsl:if>
                            </xsl:variable>
                            <xsl:value-of select="$timePeriod" />
                            <!--xsl:call-template name="time-width-day">
                                    <xsl:with-param name="current-date" select="/response/@serverDate" />
                                    <xsl:with-param name="format-date" select="./Time" />
                                    <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                    <xsl:with-param name="lang" select="/response/@language" />
                            </xsl:call-template-->
                        </div>
                        <div>
                            <xsl:if test="../@type != 'Stop'">
                                <xsl:value-of select="./Speed" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
                            </xsl:if>
                        </div>
                    </div>
                    <div style="display:none" class="longitudeVal">
                        <xsl:value-of select="./Longitude" />
                    </div>
                    <div style="display:none" class="latitudeVal">
                        <xsl:value-of select="./Latitude" />
                    </div>
                </div>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="unit" mode="mapLast">
        <!--xsl:variable name="imageUrl">
                <xsl:apply-templates select="concat(/response/@ApplicationPath, '/img/icong2.png')" />
        </xsl:variable-->
        <xsl:variable name="imageUrl">
            <xsl:apply-templates select="." mode="imageJournalUrlLast" />
        </xsl:variable>
        <xsl:variable name="imageUrlSelected">
            <xsl:apply-templates select="." mode="imageJournalUrlSelected" />
        </xsl:variable>

        <xsl:variable name="currentPosition">
            <xsl:value-of select="position()" />
        </xsl:variable>

        <xsl:variable name="xDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <xsl:value-of select="./X - ./preceding-sibling::unit/X" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="yDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <xsl:value-of select="./Y - ./preceding-sibling::unit/Y" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="jpDescriptionId">
            <xsl:value-of select="concat('journalPointDescription_', ./@groupNumber, position())" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="((position() != 1) and ($xDiff &gt; 20) and ($yDiff &gt; 20)) or position() = 1">
                <div style="position:absolute; left:{./X - 10}px; top:{./Y - 30}px; width:32px; height:32px" id="objectmap_{./ID}{./@groupNumber}{position()}">
                    <span class="toggle" onmouseover="jQuery.monitoringObject.showJournalPointDescription(event)" onmouseout="jQuery.monitoringObject.closeJournalPointDescription(event)">
                        <span class="imageRegular">
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'20'" />
                                <xsl:with-param name="height" select="'34'" />
                                <xsl:with-param name="src" select="$imageUrl" />
                            </xsl:call-template>
                        </span>
                        <span class="imageSelected" style="display:none;">
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'32'" />
                                <xsl:with-param name="height" select="'32'" />
                                <xsl:with-param name="src" select="$imageUrlSelected" />
                            </xsl:call-template>
                        </span>
                    </span>
                    <div id="{$jpDescriptionId}" class="jpDescription" style="display:none; position:relative;left:-75px;top:-80px;background-color:#fff;border:1px solid #ccc;width:150px; height:35px; padding:5px;">
                        <div class="utcDate3">
                            <!--xsl:call-template name="time-width-day">
                                    <xsl:with-param name="current-date" select="/response/@serverDate" />
                                    <xsl:with-param name="format-date" select="./Time" />
                                    <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                    <xsl:with-param name="lang" select="/response/@language" />
                            </xsl:call-template-->
                            <xsl:variable name="timePeriod">
                                <xsl:variable name="timefrom">
                                    <xsl:call-template name="time-width-day">
                                        <xsl:with-param name="current-date" select="/response/@serverDate" />
                                        <xsl:with-param name="format-date" select="./Time" />
                                        <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                        <xsl:with-param name="lang" select="/response/@language" />
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:variable name="timeto">
                                    <xsl:call-template name="time-width-day">
                                        <xsl:with-param name="current-date" select="/response/@serverDate" />
                                        <xsl:with-param name="format-date" select="../unit[last()]/Time" />
                                        <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                        <xsl:with-param name="lang" select="/response/@language" />
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:value-of select="$timefrom" />
                                <xsl:if test="../@type = 'Stop'">
                                    <xsl:text>&#160;&#8212;</xsl:text>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="$timeto" />
                                </xsl:if>
                            </xsl:variable>
                            <xsl:value-of select="$timePeriod" />
                        </div>
                        <div>
                            <xsl:if test="../@type != 'Stop'">
                                <xsl:value-of select="./Speed" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
                            </xsl:if>
                        </div>
                    </div>
                    <div style="display:none" class="longitudeVal">
                        <xsl:value-of select="./Longitude" />
                    </div>
                    <div style="display:none" class="latitudeVal">
                        <xsl:value-of select="./Latitude" />
                    </div>
                </div>
            </xsl:when>
        </xsl:choose>

    </xsl:template>



    <xsl:template match="unit" mode="map">
        <xsl:variable name="imageUrl">
            <xsl:apply-templates select="." mode="imageJournalUrl" />
        </xsl:variable>
        <xsl:variable name="imageUrlSelected">
            <xsl:apply-templates select="." mode="imageJournalUrlSelected" />
        </xsl:variable>

        <xsl:variable name="currentPosition">
            <xsl:value-of select="position()" />
        </xsl:variable>

        <xsl:variable name="xDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <!--xsl:value-of select="../unit[position() = $currentPosition]/X - ../unit[position() = $currentPosition - 1]/X" /-->
                        <xsl:value-of select="./X - ./preceding-sibling::unit/X" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="yDiff">
            <xsl:variable name="tmpRes">
                <xsl:choose>
                    <xsl:when test="position() != 1">
                        <!--xsl:value-of select="../unit[position() = $currentPosition]/Y - ../unit[position() = $currentPosition - 1]/Y" /-->
                        <xsl:value-of select="./Y - ./preceding-sibling::unit/Y" />
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$tmpRes &lt; 0">
                    <xsl:value-of select="$tmpRes * (-1)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$tmpRes" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="jpDescriptionId">
            <xsl:value-of select="concat('journalPointDescription_', ./@groupNumber, position())" />
        </xsl:variable>
        <div style="position:absolute; left:{./X - 16}px; top:{./Y - 16}px; width:32px; height:32px" id="objectmap_{./ID}{./@groupNumber}{position()}">
            <span class="toggle" onmouseover="jQuery.monitoringObject.showJournalPointDescription(event)" onmouseout="jQuery.monitoringObject.closeJournalPointDescription(event)">
                <span class="imageRegular">
                    <xsl:call-template name="formatImage">
                        <xsl:with-param name="browser" select="/response/@browser" />
                        <xsl:with-param name="width" select="'32'" />
                        <xsl:with-param name="height" select="'32'" />
                        <xsl:with-param name="src" select="$imageUrl" />
                    </xsl:call-template>
                </span>
                <span class="imageSelected" style="display:none;">
                    <xsl:call-template name="formatImage">
                        <xsl:with-param name="browser" select="/response/@browser" />
                        <xsl:with-param name="width" select="'32'" />
                        <xsl:with-param name="height" select="'32'" />
                        <xsl:with-param name="src" select="$imageUrlSelected" />
                    </xsl:call-template>
                </span>
            </span>
            <div id="{$jpDescriptionId}" class="jpDescription" style="display:none; position:relative;left:-75px;top:-80px;background-color:#fff;border:1px solid #ccc;width:150px; height:35px; padding:5px;">
                <div class="utcDate3">
                    <xsl:variable name="timePeriod">
                        <xsl:variable name="timefrom">
                            <xsl:call-template name="time-width-day">
                                <xsl:with-param name="current-date" select="/response/@serverDate" />
                                <xsl:with-param name="format-date" select="./Time" />
                                <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                <xsl:with-param name="lang" select="/response/@language" />
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:variable name="timeto">
                            <xsl:call-template name="time-width-day">
                                <xsl:with-param name="current-date" select="/response/@serverDate" />
                                <xsl:with-param name="format-date" select="../unit[last()]/Time" />
                                <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                <xsl:with-param name="lang" select="/response/@language" />
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:value-of select="$timefrom" />
                        <xsl:if test="../@type = 'Stop'">
                            <xsl:text>&#160;&#8212;</xsl:text>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="$timeto" />
                        </xsl:if>
                    </xsl:variable>
                    <xsl:value-of select="$timePeriod" />
                    <!--xsl:call-template name="time-width-day">
                            <xsl:with-param name="current-date" select="/response/@serverDate" />
                            <xsl:with-param name="format-date" select="./Time" />
                            <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                            <xsl:with-param name="lang" select="/response/@language" />
                    </xsl:call-template-->
                </div>
                <div>
                    <xsl:if test="../@type != 'Stop'">
                        <xsl:value-of select="./Speed" />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
                    </xsl:if>
                </div>
            </div>
            <div style="display:none" class="longitudeVal">
                <xsl:value-of select="./Longitude" />
            </div>
            <div style="display:none" class="latitudeVal">
                <xsl:value-of select="./Latitude" />
            </div>
        </div>
    </xsl:template>

    <!-- AS: длительность события, форматирование вывода -->
    <xsl:template match="point" mode="formatDuration">
        <xsl:variable name="minutes">
            <xsl:value-of select="round(./@duration div 60)" />
        </xsl:variable>
        <xsl:variable name="formattedOutput">
            <xsl:choose>
                <xsl:when test="$minutes = 0">
                    <xsl:value-of select="$locale/data[@name='lessThenMinute']/value" />
                </xsl:when>
                <xsl:when test="$minutes &gt; 59">
                    <xsl:variable name="hours">
                        <xsl:value-of select="floor($minutes div 60)" />
                    </xsl:variable>
                    <xsl:variable name="remainder">
                        <xsl:value-of select="$minutes - ($hours * 60)" />
                    </xsl:variable>
                    <xsl:value-of select="$hours" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$locale/data[@name='hourShort']/value" />
                    <!--xsl:if test="$remainer &gt; 0"-->
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$remainder" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$locale/data[@name='minuteShort']/value" />
                    <!--/xsl:if-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$minutes" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$locale/data[@name='minuteShort']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$formattedOutput" />
    </xsl:template>

    <!-- AS: Состояние, форматирование вывода -->
    <xsl:template match="point" mode="formatStatus">
        <xsl:variable name="formattedOutput">
            <xsl:choose>
                <xsl:when test="./@type = 'Stop'">
                    <xsl:variable name="minutes">
                        <xsl:value-of select="round(./@duration div 60)" />
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$minutes &gt; 15">
                            <xsl:value-of select="$locale/data[@name='stopLong']/value" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$locale/data[@name='stop']/value" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="./@type = 'Slow'">
                    <xsl:value-of select="$locale/data[@name='moving']/value" />
                </xsl:when>
                <xsl:when test="./@type = 'Motion'">
                    <xsl:value-of select="$locale/data[@name='moving']/value" />
                </xsl:when>
                <xsl:when test="./@type = 'Yandex'">
                    <xsl:value-of select="$locale/data[@name='yandex']/value" />
                </xsl:when>
                <xsl:when test="./@type = 'YandexGsm'">
                    <xsl:value-of select="$locale/data[@name='yandexGsm']/value" />
                </xsl:when>
                <xsl:when test="./@type = 'YandexWifi'">
                    <xsl:value-of select="$locale/data[@name='yandexWifi']/value" />
                </xsl:when>
                <xsl:when test="./@type = 'UserDefined'">
                    <xsl:value-of select="$locale/data[@name='userDefined']/value" />
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$formattedOutput" />
    </xsl:template>

    <!-- AS: Выдерем время из полной даты. Только часы и минуты -->
    <xsl:template match="unit" mode="getLocalTimeOnly">
        <span class="localDate">
            <xsl:value-of select="./Time" />
        </span>
    </xsl:template>

    <!-- AS: Выдерем время из полной даты. Только часы и минуты -->
    <xsl:template match="unit" mode="getTimeOnly">
        <!--xsl:variable name="formattedOutput">
                <xsl:variable name="time">
                        <xsl:value-of select="substring-after(./Time, ' ')" />
                </xsl:variable>
                <xsl:value-of select="substring($time, 1, string-length($time) - 3)" />
        </xsl:variable>
        <xsl:value-of select="$formattedOutput" /-->
        <!--xsl:call-template name="getTimeOnly">
                <xsl:with-param name="dateAndTime" select="./Time" />
        </xsl:call-template-->
        <!--xsl:value-of select="./Time" /-->
        <span class="utcDate">
            <xsl:value-of select="./Time" />
        </span>
    </xsl:template>

    <!-- AS: выдерем минимальное значение уровня топлива -->
    <xsl:template name="getMinFuel">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first" select="$list[1]/fuel" />
                <xsl:variable name="rest">
                    <xsl:call-template name="getMinFuel">
                        <xsl:with-param name="list" select="$list[position() != 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &lt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: посчитаем минимальное значение уровня топлива для всех позиций -->
    <xsl:template name="getMinFuelTotal">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first">
                    <xsl:call-template name="getMinFuel">
                        <xsl:with-param name="list" select="$list[1]/unit" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="rest">
                    <xsl:call-template name="getMinFuelTotal">
                        <xsl:with-param name="list" select="$list[position() != 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &gt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- AS: выдерем максимальное значение уровня топлива -->
    <xsl:template name="getMaxFuel">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first" select="$list[1]/Fuel" />
                <xsl:variable name="rest">
                    <xsl:call-template name="getMaxFuel">
                        <xsl:with-param name="list" select="$list[position() &gt; 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &gt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: посчитаем максимальное значение уровня топлива для всех позиций -->
    <xsl:template name="getMaxFuelTotal">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first">
                    <xsl:call-template name="getMaxFuel">
                        <xsl:with-param name="list" select="$list[1]/unit" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="rest">
                    <xsl:call-template name="getMaxFuelTotal">
                        <xsl:with-param name="list" select="$list[position() != 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &gt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: выдерем максимальную скорость -->
    <xsl:template name="getMaxSpeed">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first" select="$list[1]/Speed" />
                <xsl:variable name="rest">
                    <xsl:call-template name="getMaxSpeed">
                        <xsl:with-param name="list" select="$list[position() != 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &gt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: посчитаем максимальную скорость для всех позиций -->
    <xsl:template name="getMaxSpeedTotal">
        <xsl:param name="list" />
        <xsl:choose>
            <xsl:when test="$list">
                <xsl:variable name="first">
                    <xsl:call-template name="getMaxSpeed">
                        <xsl:with-param name="list" select="$list[1]/unit" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="rest">
                    <xsl:call-template name="getMaxSpeedTotal">
                        <xsl:with-param name="list" select="$list[position() != 1]" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$first &gt; $rest">
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rest"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- AS: выбор интревала истории -->
    <xsl:template name="journalInterval">
        <div id="additionalJournalParameters" style="display:block;">
            <div>
                <div>
                    <!-- Без этого div'а не будут отображаться иконки ниже -->
                </div>
                <div class="showButton">
                    <div class="checkboxes">
                        <xsl:if test="/response/vehicle/@vehicleKind!='WebCamera' and /response/vehicle/@vehicleKind!='ResourceMeter'">
                            <div class="checkbox">
                                <input type="checkbox" id="showHideStops" class="inp"/>
                                <label for="showHideStops">
                                    <span>
                                        <xsl:value-of select="$locale/data[@name='showStops']/value" />
                                    </span>
                                </label>
                                <span class="wrapper">
                                    <span class="input-group">
                                        <input type="text" class="inp form-control" id="intervalStop" />
                                        <select  class="inp form-control" id="intervalStopDim">
                                            <option value="1">
                                                <xsl:value-of select="$locale/data[@name='secondShort']"></xsl:value-of>
                                            </option>
                                            <option value="60">
                                                <xsl:value-of select="$locale/data[@name='minuteShort']" />
                                            </option>
                                            <option value="3600">
                                                <xsl:value-of select="$locale/data[@name='hourShort']" />
                                            </option>
                                        </select>
                                    </span>
                                </span>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="showAddresses" title="{$locale/help/data[@name='addressesOnPathSummary']/value}" class="inp"/>
                                <label for="showAddresses" title="{$locale/help/data[@name='addressesOnPathSummary']/value}">
                                    <span>
                                        <xsl:value-of select="$locale/data[@name='addresses']/value" />
                                    </span>
                                </label>
                            </div>
                        </xsl:if>
                    </div>
                    <xsl:if test="/response/vehicle/@vehicleKind='WebCamera' or /response/vehicle/@vehicleKind='ResourceMeter'">
                        <xsl:attribute name="style">position: relative; bottom:-1.5em;</xsl:attribute>
                    </xsl:if>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </xsl:template>

    <!-- AS: краткая инфа по событиям -->
    <xsl:template match="point" mode="shortInfo">
        <xsl:param name="position" />
        <div id="objectJournalInfo_{$position}">
            <xsl:variable name="ignitionSrc">
                <xsl:value-of select="/response/@ApplicationPath" />/img/infocard/ign-<xsl:value-of select="./unit[1]/DigitalSensors" />.png
            </xsl:variable>

            <xsl:variable name="digitalSensorValue">
                <xsl:value-of select="./unit[1]/DigitalSensors" />
            </xsl:variable>

            <div class="grey" style="width:100%;">
                <div style="float:left;width:30%;">
                    <!--xsl:apply-templates select="./unit[1]" mode="getTimeOnly" /-->
                    <xsl:call-template name="time-width-day">
                        <xsl:with-param name="current-date" select="/response/@serverDate" />
                        <xsl:with-param name="format-date" select="./unit[1]/Time" />
                        <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                        <xsl:with-param name="lang" select="/response/@language" />
                    </xsl:call-template>
                </div>
                <div style="float:left;width:69%;">
                    <xsl:call-template name="formatImage">
                        <xsl:with-param name="browser" select="/response/@browser" />
                        <xsl:with-param name="width" select="'16'" />
                        <xsl:with-param name="height" select="'16'" />
                        <xsl:with-param name="src" select="$ignitionSrc" />
                    </xsl:call-template>
                    <!--xsl:choose>
                            <xsl:when test="/response/@browser = 'IE6'">
                                    <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{./@Id}')">
                                            <xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$ignitionSrc" />', sizingMethod='scale')";width:16px; height:16px;</xsl:attribute>
                                    </span>
                            </xsl:when>
                            <xsl:otherwise>
                                    <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{./@Id}')"><img src="{$ignitionSrc}" border="0" /></span>
                            </xsl:otherwise>
                    </xsl:choose-->
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$locale/data[@name='ignition']/value" />
                    <xsl:text> </xsl:text>
                    <xsl:choose>
                        <xsl:when test="./unit[1]/DigitalSensors = 'on'">
                            <xsl:value-of select="$locale/data[@name='on1']/value" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$locale/data[@name='off1']/value" />
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
            </div>
            <div class="grey" style="clear:both;">
                <xsl:call-template name="formatDigitalSensors">
                    <xsl:with-param name="list" select="./unit" />
                    <xsl:with-param name="currentPosition" select="1" />
                    <xsl:with-param name="currentVal" select="./unit[1]/DigitalSensors" />
                </xsl:call-template>
            </div>
        </div>
    </xsl:template>

    <!-- AS: получение истории -->
    <xsl:template match="response" mode="getHistoryDetails">
        <xsl:apply-templates select="./history[1]/point" mode="getHistoryDetails">
            <xsl:with-param name="position" select="./history[1]/point[position()]" />
        </xsl:apply-templates>
        <script type="text/javascript">
            myDateExtension.formatAllUtcDates("trackingObjects", true);
        </script>
    </xsl:template>

    <xsl:template match="point" mode="getHistoryDetails">
        <xsl:param name="position" select="'1'" />
        <xsl:variable name="containerId" select="concat('journalPtPos_', $position)" />
        <div class="grey" style="clear:both;">
            <xsl:call-template name="formatFullPointOutput">
                <xsl:with-param name="list" select="./unit" />
                <xsl:with-param name="currentPosition" select="1" />
                <xsl:with-param name="currentVal" select="./unit[1]/DigitalSensors" />
            </xsl:call-template>
        </div>
    </xsl:template>


    <!-- AS: показать изменения датчиков -->
    <xsl:template name="formatDigitalSensors">
        <xsl:param name="list" />
        <xsl:param name="currentPosition" />
        <xsl:param name="currentVal" />
        <!--xsl:variable name="currentVal">
                <xsl:value-of select="$list[position() = $currentPosition]/DigitalSensors" />
        </xsl:variable-->
        <xsl:if test="($list) and ($currentPosition &lt; count($list))">
            <!--[<xsl:value-of select="$list[$currentPosition]/DigitalSensors" /> - <xsl:value-of select="$currentPosition" /> (<xsl:value-of select="count($list)" />)] VS [<xsl:value-of select="$currentVal" />]<br/-->
            <xsl:choose>
                <xsl:when test="$list[$currentPosition]/DigitalSensors != $currentVal">
                    <!--[Bitch! <xsl:value-of select="$list[$currentPosition]/DigitalSensors" />] VS [<xsl:value-of select="$currentVal" />]<br/-->
                    <xsl:variable name="ignitionSrc">
                        <xsl:value-of select="/response/@ApplicationPath" />/img/infocard/ign-<xsl:value-of select="$list[$currentPosition]/DigitalSensors" />.png
                    </xsl:variable>

                    <div style="width:100%;">
                        <div style="float:left;width:20%">
                            <!--xsl:apply-templates select="$list[$currentPosition]" mode="getTimeOnly" /-->
                            <xsl:call-template name="time-width-day">
                                <xsl:with-param name="current-date" select="/response/@serverDate" />
                                <xsl:with-param name="format-date" select="$list[$currentPosition]/Time" />
                                <xsl:with-param name="dateEqualMessage" select="$locale/data[@name='today']/value" />
                                <xsl:with-param name="lang" select="/response/@language" />
                            </xsl:call-template>
                        </div>
                        <div style="float:left;width:79%">
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'16'" />
                                <xsl:with-param name="height" select="'16'" />
                                <xsl:with-param name="src" select="$ignitionSrc" />
                            </xsl:call-template>
                            <!--xsl:choose>
                                    <xsl:when test="/response/@browser = 'IE6'">
                                            <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{$list[$currentPosition]/@Id}')">
                                                    <xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$ignitionSrc" />', sizingMethod='scale')";width:16px; height:16px;</xsl:attribute>
                                            </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                            <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{$list[$currentPosition]/@Id}')"><img src="{$ignitionSrc}" border="0" /></span>
                                    </xsl:otherwise>
                            </xsl:choose-->
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="$locale/data[@name='ignition']/value" />
                            <xsl:text> </xsl:text>
                            <xsl:choose>
                                <xsl:when test="$list[$currentPosition]/DigitalSensors = 'on'">
                                    <xsl:value-of select="$locale/data[@name='on1']/value" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$locale/data[@name='off1']/value" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </div>
                    <xsl:call-template name="formatDigitalSensors">
                        <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                        <xsl:with-param name="currentVal" select="$list[$currentPosition]/DigitalSensors" />
                        <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="formatDigitalSensors">
                        <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                        <xsl:with-param name="currentVal" select="$currentVal" />
                        <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <!-- AS: полный вывод инфы по точкам истории для одной группировки -->
    <xsl:template name="formatFullPointOutput">
        <xsl:param name="list" />
        <xsl:param name="currentPosition" />
        <xsl:param name="currentVal" />
        <xsl:if test="($list) and ($currentPosition &lt; count($list))">
            <xsl:variable name="ignitionSrc">
                <xsl:value-of select="/response/@ApplicationPath" />/img/infocard/ign-<xsl:value-of select="$list[$currentPosition]/DigitalSensors" />.png
            </xsl:variable>

            <!--xsl:variable name="onMapId" select="concat('objectmap_', ./@ID, @groupNumber, position())" /-->

            <div style="width:100%;clear:both;">
                <div style="float:left;width:40%">
                    <!--span class="toggle underlined" onclick="jQuery.centerGeoPoint({$list[$currentPosition]/Longitude}, {$list[$currentPosition]/Latitude})"-->
                    <span class="toggle underlined" onclick="jQuery.monitoringObject.findClosestByTime(myDateExtension.utcDateStringToDateObject('{$list[$currentPosition]/Time}'))">
                        <xsl:apply-templates select="$list[$currentPosition]" mode="getLocalTimeOnly" />
                    </span>
                </div>
                <div style="float:left;width:59%">
                    <xsl:if test="$list[$currentPosition]/DigitalSensors != $currentVal or $currentPosition = 1">
                        <div>
                            <xsl:call-template name="formatImage">
                                <xsl:with-param name="browser" select="/response/@browser" />
                                <xsl:with-param name="width" select="'16'" />
                                <xsl:with-param name="height" select="'16'" />
                                <xsl:with-param name="src" select="$ignitionSrc" />
                            </xsl:call-template>
                            <!--xsl:choose>
                                    <xsl:when test="/response/@browser = 'IE6'">
                                            <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{$list[$currentPosition]/@Id}')">
                                                    <xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$ignitionSrc" />', sizingMethod='scale')";width:16px; height:16px;</xsl:attribute>
                                            </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                            <span class="toggle" onclick="jQuery.geopoint.showGeopointDescription('{$list[$currentPosition]/@Id}')"><img src="{$ignitionSrc}" border="0" /></span>
                                    </xsl:otherwise>
                            </xsl:choose-->
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="$locale/data[@name='ignition']/value" />
                            <xsl:text> </xsl:text>
                            <xsl:choose>
                                <xsl:when test="$list[$currentPosition]/DigitalSensors = 'on'">
                                    <xsl:value-of select="$locale/data[@name='on1']/value" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$locale/data[@name='off1']/value" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </xsl:if>
                    <div>
                        <xsl:value-of select="$locale/data[@name='speed']/value" />:<xsl:text> </xsl:text>
                        <xsl:apply-templates select="$list[$currentPosition]/Speed" mode="getTimeOnly" />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="$locale/data[@name='speedMeasure']/value" />
                    </div>
                    <!--xsl:choose>
                    <xsl:when test="$list[$currentPosition]/DigitalSensors != $currentVal"-->
                    <!--xsl:call-template name="formatFullPointOutput">
                                            <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                                            <xsl:with-param name="currentVal" select="$list[$currentPosition]/DigitalSensors" />
                                            <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                                    </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                    <xsl:call-template name="formatFullPointOutput">
                                            <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                                            <xsl:with-param name="currentVal" select="$currentVal" />
                                            <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                                    </xsl:call-template>
                            </xsl:otherwise>
                    </xsl:choose-->
                </div>
            </div>
            <xsl:choose>
                <xsl:when test="$list[$currentPosition]/DigitalSensors != $currentVal">
                    <xsl:call-template name="formatFullPointOutput">
                        <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                        <xsl:with-param name="currentVal" select="$list[$currentPosition]/DigitalSensors" />
                        <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="formatFullPointOutput">
                        <xsl:with-param name="list" select="$list[position() &gt; $currentPosition]" />
                        <xsl:with-param name="currentVal" select="$currentVal" />
                        <xsl:with-param name="currentPosition" select="$currentPosition + 1" />
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <!-- AS: UI для слайдшоу -->
    <xsl:template name="slideshow">
        <div id="slideshow" style="position:absolute;height:25px;width:100px;background-color:#ccc;padding:2px" class="transpLite">
            <span class="toggle" onclick="jQuery.monitoringObject.selectFirst(event)">
                <xsl:call-template name="formatImage">
                    <xsl:with-param name="browser" select="/response/@browser" />
                    <xsl:with-param name="width" select="'21'" />
                    <xsl:with-param name="height" select="'21'" />
                    <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/history-btn-back.png')" />
                </xsl:call-template>
            </span>
            <xsl:text>&#160;</xsl:text>
            <span class="toggle" onclick="jQuery.monitoringObject.startSlideShow(event)" id="startSlideshowBtn">
                <xsl:call-template name="formatImage">
                    <xsl:with-param name="browser" select="/response/@browser" />
                    <xsl:with-param name="width" select="'40'" />
                    <xsl:with-param name="height" select="'21'" />
                    <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/history-btn-play.png')" />
                </xsl:call-template>
            </span>
            <span class="toggle" onclick="jQuery.monitoringObject.stopSlideShow(event)" style="display:none" id="stopSlideshowBtn">
                <xsl:call-template name="formatImage">
                    <xsl:with-param name="browser" select="/response/@browser" />
                    <xsl:with-param name="width" select="'40'" />
                    <xsl:with-param name="height" select="'21'" />
                    <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/history-btn-stop.png')" />
                </xsl:call-template>
            </span>
            <xsl:text>&#160;</xsl:text>
            <span class="toggle" onclick="jQuery.monitoringObject.selectLast(event)">
                <xsl:call-template name="formatImage">
                    <xsl:with-param name="browser" select="/response/@browser" />
                    <xsl:with-param name="width" select="'21'" />
                    <xsl:with-param name="height" select="'21'" />
                    <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/history-btn-fwd.png')" />
                </xsl:call-template>
            </span>
        </div>
    </xsl:template>


    <xsl:template match="response" mode="objectEdit">
        <xsl:variable name="pageName" select="concat(./vehicle/@GarageNumber, ': ', $locale/data[@name='editProcess']/value)" />
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <xsl:call-template name="pageHtmlHeader">
                    <xsl:with-param name="title" select="concat($pageName, ' - ', $locale/data[@name='ApplicationMainTitle']/value)" />
                </xsl:call-template>


                <xsl:apply-templates select="." mode="header2" />
                <xsl:call-template name="applicationPath" />
                <script type="text/javascript">
                    <xsl:text>var dataObjects = {</xsl:text>
                    <xsl:text>legends:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="./@legendsJSON">
                            <xsl:value-of select="./@legendsJSON"/>
                        </xsl:when>
                        <xsl:otherwise>
                            ''
                        </xsl:otherwise>
                    </xsl:choose>



                    <xsl:text>}</xsl:text>

                    var UFIN = UFIN || {};
                    UFIN.vehicleId = '<xsl:value-of select="./vehicle/@id"/>';
                    UFIN.promoUrl = "<xsl:value-of select="$pathAbout"/>";
                    UFIN.pageType = 'object';
                    UFIN.editObject = 'true';
                    UFIN.vehicleId = '<xsl:value-of select="./vehicle/@id"/>';
                    UFIN.promoUrl = "<xsl:value-of select="$pathAbout"/>";

                </script>
            </head>
            <body class="b">
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>

                <xsl:call-template name="websso"/>
                <xsl:call-template name="commonBodyIncludes" />

                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'objects'" />
                </xsl:apply-templates>

                <div class="b-content">

                    <xsl:apply-templates select="." mode="topSelector" />
                    <xsl:apply-templates select="./message" />
                    <div class="main-content">
                        <!--Форма редактирования свойств объекта-->
                        <xsl:apply-templates select="./vehicle" mode="objectEditInformation" />

                    </div>
                </div>
                <xsl:call-template name="clear" />
                <xsl:apply-templates select="." mode="pageFooter" />

                <script type="text/javascript">
                    var selectedTabAfterPageLoad = '<xsl:value-of select="./@tab"/>';
                    var focusAfterPageLoad = '<xsl:value-of select="./@focus"/>';
                </script>

              <xsl:call-template name="requireJS" />

            </body>
        </html>
    </xsl:template>

    <xsl:template match="response" mode="mlp_tracking_schedule">
        <xsl:variable name="vehicleName" select="./VEHICLE/GARAGE_NUMBER[1]" />
        <xsl:variable name="vehicleGroupName" select="./VEHICLEGROUP/NAME[1]" />
        <xsl:variable name="pageName" select="concat($vehicleName, $vehicleGroupName, ' : ', $locale/data[@name='control'])" />
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <xsl:call-template name="pageHtmlHeader">
                    <xsl:with-param name="title" select="concat($pageName, ' - ', $locale/data[@name='ApplicationMainTitle']/value)" />
                </xsl:call-template>
                <xsl:call-template name="commonBodyIncludes" />
              <xsl:call-template name="link-css" />

                <link rel="stylesheet" type="text/css" href="{./@ApplicationPath}/includes/scripts/lib/jquery/plugins/jquery-spinbox/jquery.spinbox.css" />


                <style type="text/css">
                    select#times, .schedule-type-byday select
                    {
                    height: 10em;
                    width: 10em;
                    }

                    #mlpTrackingType table
                    {
                    width: 100%;
                    }

                    div#addRemoveTime
                    {
                    text-align: center;
                    height: 100%;
                    border: 0;
                    margin: 1em;
                    }
                    div#addRemoveTime .button-wrapper
                    {
                    width: 3em;
                    display: block;
                    margin: 0 auto; /*Центрирует элемент*/
                    margin-top: 1em;
                    margin-bottom: 1em;
                    }
                    div#addRemoveTime .text-field-wrapper
                    {
                    width: 5em;
                    display: inline-block;
                    }
                    #scheduleForm
                    {
                    max-width: 40em;
                    }
                    div.spin
                    {
                    margin-right: 1em;
                    }

                </style>

              <xsl:call-template name="applicationPath" />
                <script type="text/javascript">
                    var UFIN = UFIN || {};
                    UFIN.promoUrl = "<xsl:value-of select="$pathAbout"/>";
                    UFIN.pageType = "object";
                    $(document).ready(function(){
                    $('div.mlp-tracking-type').hide();
                    });

                </script>
            </head>
            <body>
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'objects'" />
                </xsl:apply-templates>
                <xsl:apply-templates select="." mode="topSelector" />
                <div class="b-content">
                    <div class="main-content no-margin">
                        <xsl:apply-templates select="./message" />
                        <form name="scheduleForm" id="scheduleForm" method="post" enctype="multipart/form-data" accept-charset="UTF-8" action="{./@ApplicationPath}/objects.aspx">
                            <input type="hidden" name="a" value="mlp_tracking_schedule__save" />
                            <input type="hidden" name="id" value="{./@id}" />
                            <input type="hidden" name="idType" value="{./@idType}" />
                            <xsl:apply-templates select="./repetition" mode="mlp_tracking_schedule" />
                        </form>
                    </div>
                </div>
                <xsl:call-template name="clear" />
              <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />


            </body>
        </html>
    </xsl:template>

    <xsl:template match="repetition" mode="mlp_tracking_schedule" xmlns:r="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces">

        <!-- Радио-кнопки с выбором типа расписания-->
        <div id="mlpTrackingType">
            <span>
                <xsl:value-of select="$locale/data[@name='controlModeType']" />:
            </span>
            <table cellpadding="0" cellspacing="0" class="aa">
                <tr>
                    <td>
                        <input type="radio" name="mlpTrackingType" id="mlpTrackingType_none" value="none" class="inp">
                            <xsl:if test="not(./*)">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="mlpTrackingType_none">
                            <span>
                                <xsl:value-of select="$locale/data[@name='notSet']" />
                            </span>
                        </label>
                    </td>
                    <td>
                        <input type="radio" name="mlpTrackingType" id="mlpTrackingType_interval" value="interval"  class="inp">
                            <xsl:if test=".//r:RepeatEveryTime">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="mlpTrackingType_interval">
                            <span>
                                <xsl:value-of select="$locale/data[@name='interval']" />
                            </span>
                        </label>
                    </td>
                    <td>
                        <input type="radio" name="mlpTrackingType" id="mlpTrackingType_schedule" value="common"  class="inp">
                            <xsl:if test=".//r:RepeatEveryWeekGivenTimes">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="mlpTrackingType_schedule">
                            <span>
                                <xsl:value-of select="$locale/data[@name='schedule']" />
                            </span>
                        </label>
                    </td>
                    <td>
                        <input type="radio" name="mlpTrackingType" id="mlpTrackingType_schedule_day" value="byDay"  class="inp">
                            <xsl:if test=".//r:RepeatCustomWeekDayCustomTime">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="mlpTrackingType_schedule_day">
                            <span>
                                <xsl:value-of select="$locale/data[@name='scheduleByDay']" />
                            </span>
                        </label>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Расписание типа "Интервал":
        * частота запросов
        * ежедневно "время с" и "время по"
        * время действия расписания (дата с и дата по) -->
        <div id="interval" class="mlp-tracking-type">
            <xsl:variable name="intervalUnit" select="user:GetTimeSpanUnit(.//r:RepeatEveryTime/tsInterval)" />
            <xsl:variable name="intervalValue" select="user:GetTimeSpanValue(.//r:RepeatEveryTime/tsInterval)" />

            <xsl:variable name="datetime-from-value">
                <xsl:choose>
                    <xsl:when test=".//r:RepeatEveryTime/Start">
                        <xsl:value-of select=".//r:RepeatEveryTime/Start"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="../@DefaultStartDateTime"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="datetime-to-value" select=".//r:RepeatEveryTime/End"/>
            <xsl:variable name="is-date-to-indefinitely" select="user:IsDateTimeMax($datetime-to-value)" />
            <div>
                <div class="controlBlock">
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='determinePositionEvery']"/>
                        <xsl:text>:</xsl:text>
                    </div>
                    <div class="control">
                        <div class="input-group intervalSelect">
                            <input type="text" name="intervalValue" id="intervalValue" class="inp">
                                <xsl:attribute name="value">
                                    <xsl:if test="$intervalValue">
                                        <xsl:value-of select="$intervalValue" />
                                    </xsl:if>
                                    <xsl:if test="not($intervalValue)">20</xsl:if>
                                </xsl:attribute>
                            </input>

                            <select name="intervalUnit" id="intervalUnit" class="inp">
                                <option value="minute">
                                    <xsl:if test="$intervalUnit='minute'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="$locale/data[@name='minutes']"></xsl:value-of>
                                </option>
                                <option value="hour">
                                    <xsl:if test="$intervalUnit='hour'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="$locale/data[@name='hours']" />
                                </option>
                                <option value="day">
                                    <xsl:if test="$intervalUnit='day'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="$locale/data[@name='days']" />
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="controlBlock">
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='timeFrom']" />
                        <xsl:text>:</xsl:text>
                    </div>
                    <div class="control">
                        <input type="text" id="interval_timeFrom" name="interval_timeFrom" class="inp">
                            <xsl:attribute name="value">
                                <xsl:value-of select="user:GetTimeSpan(.//r:RepeatEveryTime/StartTime)"/>
                            </xsl:attribute>
                        </input>
                    </div>
                </div>
                <div class="controlBlock">
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='timeTo']" />
                        <xsl:text>:</xsl:text>
                    </div>
                    <div class="control">
                        <input type="text" id="interval_timeFrom" name="interval_timeTo" class="inp">
                            <xsl:attribute name="value">
                                <xsl:value-of select="user:GetTimeSpan(.//r:RepeatEveryTime/EndTime)"/>
                            </xsl:attribute>
                        </input>
                    </div>
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateFrom']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="interval_dateFrom_out"  data-finish="interval_dateTo">
                        <input type="text" class="dateControl dateFrom data date inp" id="interval_dateFrom" name="interval_dateFrom">
                            <xsl:if test="$datetime-from-value">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-from-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'interval_dateFrom_out', '%d.%m.%Y');">
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateTo']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="interval_dateTo_out"  data-start="interval_dateFrom" >
                        <input type="text" class="dateControl dateTo data date inp" id="interval_dateTo" name="interval_dateTo">
                            <xsl:if test="$datetime-to-value and not($is-date-to-indefinitely)">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-to-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="disabled">disabled</xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'interval_dateTo_out', '%d.%m.%Y');">
                                <xsl:if test="$is-date-to-indefinitely">
                                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                                </xsl:if>
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                    <div class="help indefinitely">
                        <input type="checkbox" name="interval_dateToType" class="inp interval_to_indefinitely" data-target="#interval_dateTo">
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="interval_dateToType-indefinitely">
                            <span>
                                <xsl:value-of select="$locale/data[@name='timeIndefinitely']/value" />
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <!-- Расписание по дням недели -->
        <div id="common" class="mlp-tracking-type">

            <!-- Таблица для вертки в три колонки:
            1. список чч-мм,
            2. контрол для ввода чч-мм
            3. контрол для выбора дня недели-->
            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='timeReport']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <xsl:variable name="href" select=".//r:RepeatEveryWeekGivenTimes/_times/@href" />
                    <input type="text" id="times" name="times" class="timeSelectors inp">
                        <xsl:attribute name="value">
                            <xsl:for-each select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item">
                                <xsl:value-of select="user:GetTimeSpan(.)"/>
                                <xsl:if test="not(position() = last())"> </xsl:if>
                            </xsl:for-each>
                        </xsl:attribute>
                    </input>
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='DaysOfTheWeek']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <xsl:call-template name="WeekDaySelector">
                        <xsl:with-param name="id" select="DaysSelector" />
                    </xsl:call-template>
                </div>
            </div>

            <xsl:variable name="datetime-from-value">
                <xsl:choose>
                    <xsl:when test=".//r:RepeatCustomWeekDayCustomTime/Start">
                        <xsl:value-of select=".//r:RepeatCustomWeekDayCustomTime/Start"/>
                    </xsl:when>
                    <xsl:when test=".//r:RepeatEveryWeekGivenTimes/Start">
                        <xsl:value-of select=".//r:RepeatEveryWeekGivenTimes/Start"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="../@DefaultStartDateTime"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="datetime-to-value">
                <xsl:choose>
                    <xsl:when test=".//r:RepeatCustomWeekDayCustomTime/End">
                        <xsl:value-of select=".//r:RepeatCustomWeekDayCustomTime/End"/>
                    </xsl:when>
                    <xsl:when test=".//r:RepeatEveryWeekGivenTimes/End">
                        <xsl:value-of select=".//r:RepeatEveryWeekGivenTimes/End"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="is-date-to-indefinitely" select="user:IsDateTimeMax($datetime-to-value)" />

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateFrom']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="schedule_dateFrom_outC" >
                        <input type="text" class="dateControl dateFrom data date inp" id="schedule_dateFromC" name="schedule_dateFromC">
                            <xsl:if test="$datetime-from-value">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-from-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'schedule_dateFrom_outC', '%d.%m.%Y');">
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateTo']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="schedule_dateTo_outC"  data-start="schedule_dateFrom" >
                        <input type="text" class="dateControl dateTo data date inp" id="schedule_dateToD" name="schedule_dateToC">
                            <xsl:if test="$datetime-to-value and not($is-date-to-indefinitely)">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-to-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="disabled">disabled</xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'schedule_dateTo_outC', '%d.%m.%Y');">
                                <xsl:if test="$is-date-to-indefinitely">
                                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                                </xsl:if>
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                    <div class="help indefinitely">
                        <input type="checkbox" name="schedule_dateToTypeC" class="inp interval_to_indefinitely" data-target="#schedule_dateToC">
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="interval_dateToType-indefinitely">
                            <span>
                                <xsl:value-of select="$locale/data[@name='timeIndefinitely']/value" />
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <!-- Расписание по дням недели -->
        <div id="byDay" class="mlp-tracking-type">

            <!-- Таблица для вертки в три колонки:
            1. список чч-мм,
            2. контрол для ввода чч-мм
            3. контрол для выбора дня недели-->
            <xsl:variable name="href" select=".//r:RepeatCustomWeekDayCustomTime/_schedule/@href" />

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'monday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[2]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'tuesday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[3]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'wednesday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[4]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'thursday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[5]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'friday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[6]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'saturday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[7]/@href" />
            </xsl:apply-templates>

            <xsl:apply-templates select="." mode="prettyDayTime">
                <xsl:with-param name="name" select="'sunday'" />
                <xsl:with-param name="href" select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item[1]/@href" />
            </xsl:apply-templates>

            <xsl:variable name="datetime-from-value">
                <xsl:choose>
                    <xsl:when test=".//r:RepeatCustomWeekDayCustomTime/Start">
                        <xsl:value-of select=".//r:RepeatCustomWeekDayCustomTime/Start"/>
                    </xsl:when>
                    <xsl:when test=".//r:RepeatEveryWeekGivenTimes/Start">
                        <xsl:value-of select=".//r:RepeatEveryWeekGivenTimes/Start"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="../@DefaultStartDateTime"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="datetime-to-value">
                <xsl:choose>
                    <xsl:when test=".//r:RepeatCustomWeekDayCustomTime/End">
                        <xsl:value-of select=".//r:RepeatCustomWeekDayCustomTime/End"/>
                    </xsl:when>
                    <xsl:when test=".//r:RepeatEveryWeekGivenTimes/End">
                        <xsl:value-of select=".//r:RepeatEveryWeekGivenTimes/End"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="is-date-to-indefinitely" select="user:IsDateTimeMax($datetime-to-value)" />

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateFrom']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="schedule_dateFrom_outD">
                        <input type="text" class="dateControl dateFrom data date inp" id="schedule_dateFrom" name="schedule_dateFromD">
                            <xsl:if test="$datetime-from-value">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-from-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'schedule_dateFrom_outD', '%d.%m.%Y');">
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='dateTo']/value" />
                    <xsl:text>:</xsl:text>
                </div>
                <div class="control">
                    <div class="input-group" id="schedule_dateTo_outD" >
                        <input type="text" class="dateControl dateTo data date inp" id="schedule_dateToD" name="schedule_dateToD">
                            <xsl:if test="$datetime-to-value and not($is-date-to-indefinitely)">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="user:GetDate($datetime-to-value)"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="disabled">disabled</xsl:attribute>
                            </xsl:if>
                        </input>
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="return showCalendar(event, 'schedule_dateTo_outD', '%d.%m.%Y');">
                                <xsl:if test="$is-date-to-indefinitely">
                                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                                </xsl:if>
                                <span class="typcn typcn-calendar-outline"></span>
                            </button>
                        </span>
                    </div>
                    <div class="help indefinitely">
                        <input type="checkbox" name="schedule_dateToTypeD" class="inp interval_to_indefinitely" data-target="#schedule_dateTo">
                            <xsl:if test="$is-date-to-indefinitely">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <label for="interval_dateToType-indefinitely">
                            <span>
                                <xsl:value-of select="$locale/data[@name='timeIndefinitely']/value" />
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div>

            <div class="button">
                <button type="button" id="save" name="save" class="btn">
                    <xsl:value-of select="$locale/data[@name='save']" />
                </button>
            </div>

            <div class="button">
                <button type="button" id="cancel" name="cancel" class="btn">
                    <xsl:value-of select="$locale/data[@name='cancel']" />
                </button>
            </div>

        </div>

    </xsl:template>

    <xsl:template match="item" mode="timeSpanArrayToOption">
        <option value="{user:GetTimeSpan(.)}">
            <xsl:value-of select="user:GetTimeSpan(.)"/>
        </option>
    </xsl:template>

    <xsl:template match="repetition" mode="timeSpanArrayArrayToOption">
        <xsl:param name="name" select="." />
        <xsl:param name="href" select="." />
        <select id="times_{$name}"     name="times_{$name}" multiple="multiple">
            <xsl:apply-templates select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item" mode="timeSpanArrayToOption" />
        </select>
    </xsl:template>

    <xsl:template match="repetition" mode="prettyDayTime">
        <xsl:param name="name" select="." />
        <xsl:param name="href" select="." />
        <div class="controlBlock">
            <div class="controlDescription">
                <xsl:value-of select="$locale/weekdays/full/data[@name=$name]" />
                <xsl:text>:</xsl:text>
            </div>
            <div class="control">
                <input type="text" id="times_{$name}" name="times_{$name}" class="timeSelectors inp">
                    <xsl:attribute name="value">
                        <xsl:for-each select=".//SOAP-ENC:Array[@id=substring-after($href, '#')]/item">
                            <xsl:value-of select="user:GetTimeSpan(.)"/>
                            <xsl:if test="not(position() = last())"> </xsl:if>
                        </xsl:for-each>
                    </xsl:attribute>
                </input>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="editPageControls">
        <div style="width:100%;">
            <div class="right-column" style="margin-left:5px;">
                <button type="button" onclick="javascript:history.back(-1)">
                    <xsl:value-of select="$locale/data[@name='cancel']/value" />
                </button>
            </div>
            <div class="right-column">
                <button type="submit">
                    <xsl:value-of select="$locale/data[@name='saveChanges']/value" />
                </button>
            </div>

        </div>
    </xsl:template>

    <!-- AS: Редактирование инфы об объекте -->
    <xsl:template match="vehicle" mode="objectEditInformation" >

        <div class="tabbable avc" id="vehicle-info-tabs" data-tab-tabs="#objectTabs">
            <form name="objectEditForm" id="objectEditForm" action="{../@ApplicationPath}/objects.aspx" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                <div tab-index="1" class="tab-content">
                    <table>
                        <tr>
                            <td style="width: 389px; padding-top:10px;">
                                <xsl:apply-templates select="." mode="editCommonVehicleInfo" />
                            </td>
                            <td style="padding-left: 40px; padding-top:10px;">
                                <xsl:apply-templates select="." mode="editImage" />
                            </td>
                        </tr>
                    </table>
                    <xsl:apply-templates select="." mode="iconSelection" />
                </div>
                <div tab-index="2" class="tab-content">
                    <div class="drivers" data-vehicle-id="{./@id}">
                        <a href="#" class="add-new-driver btn">
                            <xsl:value-of select="$locale/data[@name='addNewDriver']/value" />
                        </a>&#160;
                        <a href="#" class="select-free-driver btn">
                            <xsl:value-of select="$locale/data[@name='selectDriver']/value" />
                        </a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <xsl:value-of select="$locale/data[@name='shortName']/value" />
                                    </th>
                                    <th>
                                        <xsl:value-of select="$locale/data[@name='initials']/value" />
                                    </th>
                                    <th>
                                        <xsl:value-of select="$locale/data[@name='phones']/value" />
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <br clear="all"/>
                        <div style="width:100%; height:15px;" class="spicer"></div>
                    </div>
                </div>
                <div class="tab-content" tab-index="3">
                    <xsl:apply-templates select="/response" mode="editPerson" />
                    <xsl:apply-templates select="/response" mode="editPersonPhones" />
                </div>
                <div class="tab-content" tab-index="4">
                    <fieldset class="longLabelShortInput">
                        <legend>
                            <xsl:value-of select="$locale/data[@name='Tracker']/value" />
                        </legend>
                        <xsl:choose>
                            <xsl:when test="./Rights/Right[@Name='EditControllerDeviceID']">
                                <div class="controlBlock">
                                    <div class="controlDescription">
                                        <xsl:value-of select="$locale/data[@name='trackerType']/value"/>
                                    </div>
                                    <div class="control">
                                        <select name="trackerType" id="trackerType" class="inp">
                                            <xsl:for-each select="/response/controllerTypes/controllerType">
                                                <option value="{Name}" data-supports-password="{SupportsPassword}">
                                                    <xsl:if test="UserFriendlyName = /response/vehicle/controllers/Controller/Type/@UserFriendlyName">
                                                        <xsl:attribute name="selected">
                                                            selected
                                                        </xsl:attribute>
                                                    </xsl:if>
                                                    <xsl:value-of select="UserFriendlyName"/>
                                                </option>

                                            </xsl:for-each>
                                        </select>
                                    </div>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="controlBlock">
                                    <div class="controlDescription">
                                        <xsl:value-of select="$locale/data[@name='trackerType']/value"/>
                                    </div>
                                    <div class="control">
                                        <input value="{Name}" disabled="disabled" class="inp">
                                            <xsl:attribute name="value">
                                                <xsl:for-each select="/response/controllerTypes/controllerType">
                                                    <xsl:choose>
                                                        <xsl:when test="Name = /response/vehicle/controllers/Controller/Type/@Name">
                                                            <xsl:value-of select="Name"/>
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </xsl:for-each>
                                            </xsl:attribute>
                                        </input>
                                    </div>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:variable name="SupportsDeviceId" select="./controllers/Controller/Type/@SupportsDeviceId" />
                        <xsl:choose>
                            <xsl:when test="./Rights/Right[@Name='ViewControllerDeviceID'] and $SupportsDeviceId ='true'">
                                <div class="controlBlock">
                                    <div class="controlDescription">
                                        <xsl:value-of select="$locale/data[@name='trackerIMEI']/value" />
                                    </div>
                                    <div class="control">
                                        <xsl:choose>
                                            <xsl:when test="./Rights/Right[@Name='EditControllerDeviceID']">
                                                <input type="text"
                                                       id="deviceId"
                                                       name="deviceId"
                                                       value="{./controllers/Controller/@DeviceId}"
                                                       class="inp"
                                                />
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <input type="text"
                                                       id="deviceId"
                                                       name="deviceId"
                                                       disabled="disabled"
                                                       value="{./controllers/Controller/@DeviceId}"
                                                       class="inp"
                                                />
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </div>
                                </div>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="./Rights/Right[@Name='ViewControllerPhone']">
                                <div class="controlBlock">
                                    <div class="controlDescription">
                                        <xsl:value-of select="$locale/data[@name='devicePhone']/value"/>
                                    </div>
                                    <div class="control">
                                        <xsl:comment>
                                            <xsl:value-of select="./controllers/Controller/@PhoneIsEditable"/>
                                        </xsl:comment>
                                        <xsl:choose>
                                            <xsl:when test="./Rights/Right[@Name='EditControllerPhone'] and ./controllers/Controller/@PhoneIsEditable = 'true'">
                                                <input type="text"
                                                       id="trackerPhone"
                                                       name="trackerPhone"
                                                       value="{./controllers/Controller/@Phone}"
                                                       class="inp"
                                                />
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <input type="text"
                                                       id="trackerPhone"
                                                       name="trackerPhone"
                                                       disabled="disabled"
                                                       value="{./controllers/Controller/@Phone}"
                                                       class="inp"
                                                />
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </div>
                                </div>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:if test="./controllers/Controller/Type/@Name = 'FtpWebCam' ">
                            <div class="controlBlock">
                                <div class="controlDescription">
                                    <xsl:value-of select="$locale/data[@name='IP/Host']/value"/>
                                </div>
                                <div class="control">
                                    <input type="text" id="ip" name="ip" class="cameraInfo inp" value="{./controllers/Controller/@IP}"/>
                                </div>
                            </div>
                            <div class="controlBlock">
                                <div class="controlDescription">
                                    <xsl:value-of select="$locale/data[@name='Port']/value"/>
                                </div>
                                <div class="control">
                                    <input type="text" id="port" class="cameraInfo inp"/>
                                </div>
                            </div>
                            <div class="controlBlock">
                                <div class="controlDescription">
                                    <xsl:value-of select="$locale/data[@name='OperatorLogin']/value"/>
                                </div>
                                <div class="control">
                                    <input type="text" id="login" class="cameraInfo inp"/>
                                </div>
                            </div>
                        </xsl:if>
                        <xsl:if test="./Rights/Right[@Name='ControllerPasswordAccess'] and ./controllers/Controller/Type[@SupportsPassword='true']">
                            <div class="controlBlock">
                                <div class="controlDescription">
                                    <xsl:value-of select="$locale/data[@name='ControllerPassword']/value"/>
                                </div>
                                <div class="control">
                                    <xsl:choose>
                                        <xsl:when test="./controllers/Controller/Type/@Name = 'FtpWebCam' ">
                                            <input type="text" id="cameraPassword" class="cameraInfo inp"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <input type="text"
                                                   id="trackerPassword"
                                                   name="trackerPassword"
                                                   value="{./controllers/Controller/@Password}"
                                                   class="cameraInfo inp"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </div>
                        </xsl:if>
                        <xsl:if test="./controllers/Controller/Type/@Name = 'FtpWebCam' ">
                            <div class="controlBlock mapRow">
                                <div>
                                    <div title="Для установки положения камеры, кликните по карте или введите адрес.">
                                        <xsl:value-of select="$locale/data[@name='PlacedOnMap']/value"/>
                                    </div>
                                    <div class="addCoord" >
                                        <div class="input-group">
                                            <input type="text" id="mapSearchCriteriaBox"
                                                   title="{$locale/data[@name='PlacedOnMapAddress']/value}"
                                                   class="inp"
                                                   placeholder="{$locale/data[@name='PlacedOnMapAddress']/value}"/>
                                            <span class="input-group-btn">
                                                <button class="btn" id="mapSearchButton">
                                                    <span class="typcn typcn-zoom"></span>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="not-found">
                                            Адрес не найден
                                        </div>
                                    </div>
                                    <p></p>
                                    <p></p>
                                    <p>
                                        <em>Для установки положения камеры, кликните по карте или введите адрес.</em>
                                    </p>
                                    <p></p>
                                    <div id="map_wrapper">
                                        <div id="mapG"></div>
                                    </div>

                                </div>
                            </div>

                        </xsl:if>
                    </fieldset>


                </div>
                <xsl:if test="contains(/response/vehicle/Capabilities, 'ChangeDeviceSettings')
                  and /response/vehicle/Rights/Right[@Name='ChangeDeviceConfigBySMS']">
                    <xsl:apply-templates select="/response/vehicle/Sensors/Sensor" mode="vehicleCommandControls">
                        <xsl:with-param name="vehicleId" select="/response/vehicle/@id" />
                    </xsl:apply-templates>
                </xsl:if>

                <xsl:if test="./@vehicleKind != 'MobilePhone' and ./@vehicleKind != 'Tracker' and ./@vehicleKind != 'WebCamera' and ./@vehicleKind != 'Smatrphone' ">
                    <div class="tab-content as" tab-index="5">

                        <fieldset class="longLabelShortInput">
                            <legend>
                                <xsl:value-of select="$locale/data[@name='ControlDates']/value" />
                            </legend>

                            <table id="vehicle-events-table" class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <xsl:value-of select="$locale/data[@name='name']/value"/>
                                        </th>
                                        <th>
                                            <xsl:value-of select="$locale/data[@name='date']/value"/>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="./VehicleControlDates/VehicleControlDate">
                                        <tr data-type="{./@Type}" data-value="{./@Value}">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <a href="#" class="add-date-controll btn" >
                                <xsl:value-of select="$locale/data[@name='add']/value"/>
                            </a>
                        </fieldset>
                    </div>
                </xsl:if>

                <xsl:call-template name="clear" />
                <input type="hidden" name="a" id="a" value="save"></input>
                <input type="hidden" name="id" id="id" value="{./@id}" ></input>
                <input type="hidden" name="icon" id="icon">
                    <xsl:attribute name="value">
                        <xsl:value-of select="./settings/icon"/>
                    </xsl:attribute>
                </input>
            </form>
        </div>
    </xsl:template>

    <!--Вывод элементов управления на странице редактирования-->
    <xsl:template match="Sensor" mode="vehicleCommandControls">
        <xsl:param name="vehicleId" />
        <xsl:if test="contains(@Name, 'ControllerMode')">
            <br/>
            <p>
                <xsl:value-of select="$locale/data[@name='DeviceMode']/value" /> (<xsl:value-of select="$locale/data[@name='LatestUpdate']/value" />&#160;
                <xsl:value-of select="@Date"/>):
            </p>
            <select style="width:200px; margin-right:10px;" id="tracking-mode-change" data-present-value="{@Value}">
                <option value="0">
                    <xsl:if test="contains(@Value, '0.')">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='NoActive']/value" />
                </option>
                <option value="1">
                    <xsl:if test="contains(@Value, '1')">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='Simple']/value" />
                </option>
                <option value="2">
                    <xsl:if test="contains(@Value, '2')">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='GpsTracking']/value" />
                </option>
                <option value="3">
                    <xsl:if test="contains(@Value, '3')">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='ManDown']/value" />
                </option>
            </select>
            <button name="apply-mode" data-value="{$vehicleId}" disabled="disabled">
                <xsl:value-of select="$locale/data[@name='Apply']/value" />
            </button>
            <p style="color:#6E6E6E; width:600px;">
                <div class="mode mode0">
                    <xsl:if test="not(contains(@Value, '0.'))">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='ControllerModes']/noactive" />
                </div>
                <div class="mode mode1">
                    <xsl:if test="not(contains(@Value, '1'))">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='ControllerModes']/simple" />
                </div>
                <div class="mode mode2">
                    <xsl:if test="not(contains(@Value, '2'))">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='ControllerModes']/gpstracking" />
                </div>
                <div class="mode mode3">
                    <xsl:if test="not(contains(@Value, '3'))">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="$locale/data[@name='ControllerModes']/mandown" />
                </div>
            </p>
        </xsl:if>
    </xsl:template>

    <!-- AS: основная инфа о машине - редактирование -->
    <xsl:template match="vehicle" mode="editCommonVehicleInfo">
        <div style="width:100%; clear:both;" data-aaaa="">
            <fieldset class="longLabelShortInput">
                <legend>
                    <xsl:value-of select="$locale/data[@name='object']/value" />
                </legend>
                <div class="controlBlock">
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='objectName']/value" /> :
                    </div>
                    <div class="control">
                        <input type="text" id="garageNumber" name="garageNumber" value="{./@GarageNumber}" class="inp" />
                    </div>
                </div>
                <div class="controlBlock">
                    <div class="controlDescription" >
                        <xsl:value-of select="$locale/data[@name='objectType']/value" />:
                    </div>
                    <div class="control">
                        <select class="inp" name="vehicleKind" id="vehicleKind">
                            <xsl:variable name="vehicleKind" select="./@vehicleKind" />
                            <xsl:for-each select="/response/VehicleKinds/VehicleKind">
                                <xsl:variable name="current" select="." />
                                <option value="{$current}">
                                    <xsl:if test="$current = $vehicleKind">
                                        <xsl:attribute name="selected" />
                                    </xsl:if>
                                    <xsl:value-of select="$locale/vehicleKind/data[@name=$current]/value"/>
                                </option>
                            </xsl:for-each>
                        </select>
                    </div>
                </div>

                <xsl:if test="./@vehicleKind = 'Bus' and count(/response/ZoneGroups/Group) &gt; 0">
                    <div class="controlBlock">
                        <div class="controlDescription">
                            <xsl:value-of select="$locale/data[@name='Route']/value" />:
                        </div>
                        <div class="control">
                            <select name="route" id="route" class="inp">
                                <option value="">
                                    <xsl:value-of select="$locale/data[@name='NotAssigned']/value" />
                                </option>

                                <xsl:variable name="selected" select="./ZoneGroup/@Id" />
                                <xsl:for-each select="/response/ZoneGroups/Group">
                                    <xsl:variable name="current" select="./@Id" />
                                    <option value="{$current}" title="{./@Description}" >
                                        <xsl:if test="$current = $selected">
                                            <xsl:attribute name="selected" />
                                        </xsl:if>
                                        <xsl:value-of select="./@Name"/>
                                    </option>
                                </xsl:for-each>
                            </select>
                        </div>
                    </div>
                </xsl:if>

                <div class="controlBlock">
                    <xsl:if test="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera' or ./@vehicleKind = 'Smartphone'  ">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='registrationNumber']/value" /> :
                    </div>
                    <div class="control">
                        <input type="text" id="regNumber"
                               name="regNumber"
                               value="{./@RegNumber}"
                               class="inp"
                        />
                    </div>
                </div>

                <div class="controlBlock" display-block="">
                    <xsl:if test="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera'or ./@vehicleKind = 'Smartphone'  ">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='brandName']/value" /> :
                    </div>
                    <div class="control">
                        <input type="text" id="brand"
                               name="brand"
                               value="{./@type}"
                               class="inp"
                        />
                    </div>
                </div>

                <xsl:variable name="FuelTypeID" select="./@FuelTypeID" />
                <xsl:variable name="unit" select="./@FuelUnit" />
                <xsl:variable name="tankUnit" select="./@FuelTankUnit" />
                <div class="controlBlock" display-block="">
                    <xsl:if test="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera'or ./@vehicleKind = 'Smartphone'  ">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='fuelSpentStandartLabel']/value" />
                        (<xsl:value-of select="$locale/data[@name=$unit]/value" /> ):
                    </div>
                    <div class="control">
                        <xsl:call-template name="textEdit">
                            <xsl:with-param name="id" select="'fuelSpentStandart'" />
                            <xsl:with-param name="value" select="./@FuelSpentStandart" />
                            <xsl:with-param name="class" select="'inp'" />
                        </xsl:call-template>
                    </div>
                </div>

                <div class="controlBlock" display-block="">
                    <xsl:if test="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera'or ./@vehicleKind = 'Smartphone' or /response/FuelTypes/FuelType[@ID=$FuelTypeID]/@Name = 'Gas' ">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='fuelSpentStandardMH']/value" /> :
                        (<xsl:value-of select="$locale/data[@name=$tankUnit]/value" />/<xsl:value-of select="$locale/data[@name='MotoHour']/value" />) :
                    </div>
                    <div class="control">
                        <input type="text"
                               id="fuelSpentStandardMH"
                               name="fuelSpentStandardMH"
                               value="{./@FuelSpentStandardMH}"
                               class="inp"
                        />
                    </div>

                </div>

                <div class="controlBlock" display-block="">
                    <xsl:if test="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera'or ./@vehicleKind = 'Smartphone'  ">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='fuelType']/value" /> :
                    </div>
                    <div class="control">
                        <select id="fuelType" class="inp" name="fuelTypeId">
                            <option>
                                <xsl:if test="not($FuelTypeID)">
                                    <xsl:attribute name="selected">selected</xsl:attribute>
                                </xsl:if>
                                <xsl:value-of select="$locale/data[@name='notSet']/value" />
                            </option>

                            <xsl:for-each select="/response/FuelTypes/FuelType">
                                <option value="{./@ID}">
                                    <xsl:if test="./@ID = $FuelTypeID">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="./@Name"/>
                                </option>
                            </xsl:for-each>
                        </select>
                    </div>
                </div>

                <xsl:variable name="isVehicle" select="./@vehicleKind = 'MobilePhone' or ./@vehicleKind = 'Tracker' or ./@vehicleKind = 'WebCamera' or ./@vehicleKind = 'Smartphone' " />

                <div class="controlBlock" display-block="">
                    <xsl:if test="$isVehicle">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='fuelTankVolume']/value" />
                        (<xsl:value-of select="$locale/data[@name=$tankUnit]/value" />) :
                    </div>
                    <div class="control">
                        <input type="text"
                               id="fuelTankVolume"
                               name="fuelTankVolume"
                               value="{./fuelTankVolume}"
                               class="inp"
                        />
                    </div>
                </div>

                <div class="controlBlock" display-block="">
                    <xsl:if test="$isVehicle">
                        <xsl:attribute name="style">display:none;</xsl:attribute>
                    </xsl:if>
                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='MaxAllowedSpeed']/value" /> :
                    </div>
                    <div class="control">
                        <input type="text"
                               id="maxAllowedSpeed"
                               name="maxAllowedSpeed"
                               value="{./MaxAllowedSpeed}"
                               class="inp"
                        />
                    </div>
                </div>

                <div class="controlBlock">


                    <div class="controlDescription">
                        <xsl:value-of select="$locale/data[@name='objectImageOnMap']/value" /> :
                    </div>
                    <div class="control">
                        <div style="float:left;width:35px" id="selected-icon">
                            <xsl:apply-templates select="." mode="selected-icon" />
                        </div>
                        <div style="float:left;padding-top:10px;">
                            <span>
                                <a href="javascript: void(0);" class="showObjectIconsDialog">
                                    <xsl:value-of select="$locale/data[@name='change']/value" />
                                </a>
                            </span>
                        </div>
                    </div>
                </div>

                <xsl:choose>
                    <xsl:when test="/response/@isSuperAdministrator = 'true'">
                        <div class="controlBlock">
                            <div class="controlDescription">
                                <xsl:value-of select="$locale/data[@name='Notes']/value" /> :
                            </div>
                            <div class="control">
                                <input type="text" id="notes" name="notes" value="{./settings/notes}" class="inp" />
                            </div>
                        </div>
                    </xsl:when>
                </xsl:choose>
            </fieldset>
        </div>
    </xsl:template>

    <!-- AS: редактирование изображения машины -->
    <xsl:template match="vehicle" mode="editImage">
        <fieldset>
            <legend>
                <xsl:value-of select="$locale/data[@name='image']/value" />
            </legend>
            <div class="message" style="margin:10px;display:none;"></div>
            <div style="width:100%;">
                <div>
                    <xsl:choose>
                        <xsl:when test="./@hasImage = 'true'">
                            <img id="vehicleImage" src="{/response/@ApplicationPath}/objects.aspx?a=getimage&amp;id={./@id}" title="{./@type}"></img>
                        </xsl:when>
                    </xsl:choose>
                    <div id="noImageBlock">
                        <xsl:if test="./@hasImage = 'true'">
                            <xsl:attribute name="style">display:none;</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="$locale/data[@name='noImage']/value" />
                    </div>
                </div>

                <xsl:if test="./@hasImage = 'true'">
                    <a href="#" onclick="jQuery.monitoringObject.removeImage(event, {./@id}); return false;">
                        <xsl:value-of select="$locale/data[@name='removeImage']/value" />
                    </a>
                </xsl:if>

                <div style="margin-top:10px;">
                    <label class="block" style="width:100%; text-align:left;">
                        <div>
                            <b>
                                <xsl:value-of select="$locale/data[@name='loadNewImage']/value" />
                            </b>:
                        </div>
                        <p class="tooltip">
                            <xsl:value-of select="$locale/data[@name='objectImageRestrictions']/value" />
                        </p>
                        <div>
                            <input type="file" id="image" name="image" ></input>
                        </div>
                    </label>
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!-- AS: редактирование параметров владельца машины -->
    <xsl:template match="response" mode="editPerson">
        <fieldset>
            <legend>
                <xsl:value-of select="$locale/data[@name='ownerName']/value" />
            </legend>
            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='lastName']/value" /> :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerLastName"
                           name="ownerLastName"
                           value="{./vehicle/owners/Person/LastName}"
                           class="inp"
                    />
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='firstName']/value" /> :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerFirstName"
                           name="ownerFirstName"
                           value="{./vehicle/owners/Person/FirstName}"
                           class="inp"
                    />
                </div>
            </div>

            <div class="controlBlock">
                <div class="controlDescription">
                    <xsl:value-of select="$locale/data[@name='secondName']/value" /> :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerSecondName"
                           name="ownerSecondName"
                           value="{./vehicle/owners/Person/SecondName}"
                           class="inp"
                    />
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <xsl:template match="response" mode="editPersonPhones">
        <fieldset>
            <legend>
                <xsl:value-of select="$locale/data[@name='ownerPhones']/value" />
            </legend>
            <div class="controlBlock">
                <div class="controlDescription">
                    1 :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerPhone1"
                           name="ownerPhone1"
                           value="{./vehicle/owners/Person/Phones/Contact[1]/Value}"
                           class="inp"
                    />
                </div>
            </div>
            <div class="controlBlock">
                <div class="controlDescription">
                    2 :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerPhone2"
                           name="ownerPhone2"
                           value="{./vehicle/owners/Person/Phones/Contact[2]/Value}"
                           class="inp"
                    />
                </div>
            </div>
            <div class="controlBlock">
                <div class="controlDescription">
                    3 :
                </div>
                <div class="control">
                    <input type="text"
                           id="ownerPhone3"
                           name="ownerPhone3"
                           value="{./vehicle/owners/Person/Phones/Contact[3]/Value}"
                           class="inp"
                    />
                </div>
            </div>
        </fieldset>
    </xsl:template>

    <!--
   AS: выбор кастомных иконок
    -->
    <xsl:template match="vehicle" mode="iconSelection">
        <div id="iconSelection">
            <div style="margin-bottom:10px;">
                <xsl:apply-templates select="." mode="defaultIcon" />
            </div>
            <xsl:apply-templates select="." mode="customIcon" />
            <div class="clear"></div>
        </div>
    </xsl:template>

    <xsl:template match="vehicle" mode="customIcon">
        <div style="width:100%;">
            <p style="margin-left:10px;">
                <xsl:value-of select="$locale/data[@name='selectObjectIconTooltip']/value" />
            </p>
            <div style="width:100%;float:left;">
                <fieldset>
                    <div id="customIcons">
                        <!--xsl:value-of select="$locale/data[@name='loading']/value" />...-->
                    </div>
                </fieldset>
            </div>
            <div class="clear"></div>
        </div>
        <xsl:call-template name="clear" />
    </xsl:template>

    <!--
    AS: иконка по умолчанию
    -->
    <xsl:template match="vehicle" mode="defaultIcon">
        <div style="float:left;padding-top:10px;">
            <span style="vertical-align:top;" class="toggle underlined changeIconToDefault" data-defaultId="{./@id}">
                <xsl:value-of select="$locale/data[@name='selectDefaultObjectIcon']/value" />:
            </span>
        </div>
        <div style="float:left;padding-left:10px;" id="default-icon">
            <span>
                <xsl:apply-templates select="." mode="showDefaultIcon" />
            </span>
        </div>
        <xsl:call-template name="clear"/>
    </xsl:template>

    <xsl:template match="vehicle" mode="showDefaultIcon">
        <xsl:call-template name="formatImage">
            <xsl:with-param name="browser" select="/response/@browser" />
            <xsl:with-param name="src">
                <xsl:apply-templates select="." mode="iconPath" />
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- AS: распарсим параметры истории, чтобы получить ее -->
    <xsl:template match="HistoryParams">
        <xsl:if test="(./from != $minDateTime) and (./to != $minDateTime) and (./interval &gt; 0)">

            <xsl:variable name="dateFrom">
                <xsl:variable name="dateFromTmp">
                    <xsl:call-template name="getDateOnly">
                        <xsl:with-param name="dateAndTime" select="./from" />
                    </xsl:call-template>
                </xsl:variable>

                <!--<xsl:choose>
                <xsl:when test="/response/@language = 'ru-RU'">-->
                <xsl:call-template name="dateToRu">
                    <xsl:with-param name="date" select="$dateFromTmp" />
                </xsl:call-template>
                <!--</xsl:when>
                        <xsl:otherwise>
                                <xsl:call-template name="dateToEn">
                                        <xsl:with-param name="date" select="$dateFromTmp" />
                                </xsl:call-template>
                        </xsl:otherwise>
                </xsl:choose>-->
            </xsl:variable>

            <xsl:variable name="timeFrom">
                <xsl:call-template name="getTimeOnly">
                    <xsl:with-param name="dateAndTime" select="./from" />
                    <xsl:with-param name="delimiter" select="'T'" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="dateTo">
                <xsl:variable name="dateToTmp">
                    <xsl:call-template name="getDateOnly">
                        <xsl:with-param name="dateAndTime" select="./to" />
                    </xsl:call-template>
                </xsl:variable>

                <!--<xsl:choose>
                <xsl:when test="/response/@language = 'ru-RU'">-->
                <xsl:call-template name="dateToRu">
                    <xsl:with-param name="date" select="$dateToTmp" />
                </xsl:call-template>
                <!--</xsl:when>
                        <xsl:otherwise>
                                <xsl:call-template name="dateToEn">
                                        <xsl:with-param name="date" select="$dateToTmp" />
                                </xsl:call-template>
                        </xsl:otherwise>
                </xsl:choose>-->
            </xsl:variable>

            <xsl:variable name="timeTo">
                <xsl:call-template name="getTimeOnly">
                    <xsl:with-param name="dateAndTime" select="./to" />
                    <xsl:with-param name="delimiter" select="'T'" />
                </xsl:call-template>
            </xsl:variable>

            <script type="text/javascript">
                UFIN.autoSearch = true;
                UFIN.objCoord = ['<xsl:value-of select="$dateFrom" />', '<xsl:value-of select="$timeFrom" />', '<xsl:value-of select="$dateTo" />', '<xsl:value-of select="$timeTo" />', <xsl:value-of select="./interval" />];

            </script>
        </xsl:if>


    </xsl:template>

    <xsl:template match="vehicle" mode="garage-number">
        <xsl:choose>
            <xsl:when test="./@name and ./@name != ''">
                <xsl:value-of select="./@name" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>[</xsl:text>
                <xsl:value-of select="$locale/data[@name='noname']/value" />
                <xsl:text>]</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="localTitle">
        <xsl:if test="./vehicle/@GarageNumber != ''">
            <xsl:value-of select="./vehicle/@GarageNumber"/>
            <xsl:text> / </xsl:text>
        </xsl:if>

    </xsl:template>



</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\page\page.xmlobjects.xsl_147.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="" ><advancedProp name="sInitialMode" value=""/><advancedProp name="bXsltOneIsOkay" value="true"/><advancedProp name="bSchemaAware" value="false"/><advancedProp name="bXml11" value="true"/><advancedProp name="iValidation" value="0"/><advancedProp name="bExtensions" value="true"/><advancedProp name="iWhitespace" value="0"/><advancedProp name="sInitialTemplate" value=""/><advancedProp name="bTinyTree" value="true"/><advancedProp name="bWarnings" value="true"/><advancedProp name="bUseDTD" value="false"/></scenario></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no" ><SourceSchema srcSchemaPath="..\..\..\..\..\temp\page\page.xmlobjects.xsl.xml" srcSchemaRoot="response" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/></MapperInfo><MapperBlockPosition><template match="/"><block path="xsl:choose" x="378" y="0"/><block path="xsl:choose/=[0]" x="332" y="0"/><block path="xsl:choose/xsl:when/xsl:apply&#x2D;templates" x="328" y="0"/><block path="xsl:choose/=[1]" x="332" y="22"/><block path="xsl:choose/xsl:when[1]/xsl:apply&#x2D;templates" x="288" y="0"/><block path="xsl:choose/=[2]" x="332" y="30"/><block path="xsl:choose/xsl:when[2]/xsl:apply&#x2D;templates" x="248" y="0"/><block path="xsl:choose/=[3]" x="332" y="38"/><block path="xsl:choose/xsl:when[3]/xsl:apply&#x2D;templates" x="208" y="0"/><block path="xsl:choose/=[4]" x="332" y="46"/><block path="xsl:choose/xsl:when[4]/xsl:apply&#x2D;templates" x="168" y="0"/><block path="xsl:choose/=[5]" x="332" y="54"/><block path="xsl:choose/xsl:when[5]/xsl:apply&#x2D;templates" x="128" y="0"/><block path="xsl:choose/=[6]" x="332" y="62"/><block path="xsl:choose/xsl:when[6]/xsl:apply&#x2D;templates" x="88" y="0"/></template></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
