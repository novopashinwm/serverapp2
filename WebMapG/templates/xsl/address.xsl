<?xml version='1.0'?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:v="urn:schemas-microsoft-com:vml" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="common.xsl" />
	<xsl:import href="uielements.xsl" />
	<xsl:import href="trackingObject_G.xsl" />
	<xsl:import href="route.xsl" />
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="no" />


<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//@template = 'addressList'">
			<xsl:apply-templates select="." mode="addressList" />
		</xsl:when>
		<xsl:when test="//@template = 'show'">
			<xsl:apply-templates select="." mode="show" />
		</xsl:when>
		<xsl:when test="//@template = 'showRadius'">
			<xsl:apply-templates select="." mode="showRadius" />
		</xsl:when>
	</xsl:choose>
</xsl:template>

<!-- AS: формирует HTML со списком найденных адресов -->
<xsl:template match="response" mode="addressList">
	<div style="color:#fff;margin-bottom:5px;width:100%;font-weight:bold;">
		<div style="float:left;width:5%">
			<span class="toggle">
				<span class="expand" style="display:inline;" onclick="jQuery.searchAddress.collapseAddressResults(event)" id="collapseAddressList">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="./@browser" />
						<xsl:with-param name="width" select="'16'" />
						<xsl:with-param name="height" select="'16'" />
						<xsl:with-param name="src" select="concat(./@ApplicationPath, '/img/controls/panel-collapse.png')" />
					</xsl:call-template>
				</span>
				<span class="collapse" style="display:none;" onclick="jQuery.searchAddress.expandAddressResults(event)" id="expandAddressList">
					<xsl:call-template name="formatImage">
						<xsl:with-param name="browser" select="./@browser" />
						<xsl:with-param name="width" select="'16'" />
						<xsl:with-param name="height" select="'16'" />
						<xsl:with-param name="src" select="concat(./@ApplicationPath, '/img/controls/panel-expand-vert.png')" />
					</xsl:call-template>
				</span>
			</span>
		</div>
		<div style="float:left;width:83%">
			<xsl:value-of select="$locale/data[@name='searchResults']/value" />:
			<xsl:text> </xsl:text>
			<xsl:value-of select="./@searchValue" />
		</div>
		<div style="float:left;width:9%" class="right-side">
			<!--span class="closeWin" onclick="jQuery.searchAddress.closeAddressResults()"><span><xsl:text>&#160;x</xsl:text></span></span-->
			<span class="closeWin" onclick="jQuery.searchAddress.closeAddressResults()"><span><xsl:text>&#160;x</xsl:text></span></span>
		</div>
		<xsl:call-template name="clear" />
	</div>
	<div id="addressList">
		<xsl:if test="./message">
			<p style="margin:10px" class="message Warning">
				<xsl:call-template name="replace">
					<xsl:with-param name="str" select="./message/MessageBody" />
				</xsl:call-template>
			</p>
		</xsl:if>
		<xsl:apply-templates select="./addressList/address" mode="list" />
	</div>
	<script type="text/javascript">
		var addressList = jQuery("#addressList");
		addressList.height(jQuery(window).height() - addressList.offset().top - 20 + "px");
	</script>
</xsl:template>

<xsl:template match="address" mode="list">
	<div style="margin-top:10px;">
		<span class="toggle underlined" onclick="jQuery.searchAddress.showAddress({./ExtentMax/@longitude}, {./ExtentMax/@latitude}, {./ExtentMin/@longitude}, {./ExtentMin/@latitude}, event)"><xsl:value-of select="./@addressValue" /></span>
	</div>
</xsl:template>


<!-- AS: формирует HTML со списком найденных адресов -->
<xsl:template match="response" mode="show">
	<xsl:apply-templates select="./address" mode="baloon" />
</xsl:template>

<xsl:template match="address" mode="baloon">
	<div xmlns="http://www.w3.org/1999/xhtml" id="addressBalloonTotal" style="position:absolute; left:{./@x - 25}px; top:{./@y - 132}px; width:300px;cursor:default;" class="ontop">
		<div id="addressBaloon">
			<div class="contentB">
				<div style="width:100%;height:70px;">
					<!--div style="float:left;width:10%">
						<div style="float:left; width:15%" class="right-side">
							<span onclick="jQuery.searchAddress.collapseBalloon()" class="toggle">
								<xsl:call-template name="formatImage">
									<xsl:with-param name="browser" select="/response/@browser" />
									<xsl:with-param name="width" select="16" />
									<xsl:with-param name="height" select="16" />
									<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/controls/panel-collapse.png')" />
								</xsl:call-template>
							</span>
						</div>
					</div-->
					<div style="float:left; width:80%" id="addressValue">
						<b><xsl:value-of select="./@search" /></b>
					</div>
					<div style="float:left; width:20%" class="right-side">
						<span class="toggle" onclick="jQuery.searchAddress.collapseBalloon()">
							<xsl:call-template name="formatImage">
								<xsl:with-param name="browser" select="/response/@browser" />
								<xsl:with-param name="width" select="14" />
								<xsl:with-param name="height" select="13" />
								<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/collapse.gif')" />
							</xsl:call-template>
						</span>
						<!--xsl:text>&#160;</xsl:text-->
						<span class="closeWin" onclick="jQuery.searchAddress.closeBalloon()" style="margin-left:5px;"><span><xsl:text>&#160;x&#160;</xsl:text></span></span>
					</div>
					<!--div style="float:left; width:15%" class="right-side"><span class="closeWin" onclick="jQuery.searchAddress.collapseBalloon()"><span><xsl:text>&#160;x</xsl:text></span></span></div-->
				</div>
				<div class="clear"><xsl:text> </xsl:text></div>
				<!--xsl:apply-templates select="." mode="baloonBottomControls" /-->
				<xsl:apply-templates select="." mode="baloonBottomControls2" />
			</div>

			<xsl:variable name="imageUrl">
				<!--xsl:value-of select="concat(/response/@ApplicationPath, '/img/myballoon.png')" /-->
				<xsl:value-of select="concat(/response/@ApplicationPath, '/img/clouds/search_balloon.png')" />
			</xsl:variable>
			<div style="position:absolute;top:30px;left:0;z-index:-1;">
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="250" />
					<xsl:with-param name="height" select="100" />
					<xsl:with-param name="src" select="$imageUrl" />
				</xsl:call-template>
			</div>
		</div>
		<div id="addressPoint" style="position:absolute;left:5px;top:80px;display:none;" class="toggle" onclick="jQuery.searchAddress.expandBalloon()">
			<!--div style="position:absolute;left:0;top:0;">
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="26" />
					<xsl:with-param name="height" select="46" />
					<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/application/searchpt.png')" />
					<xsl:with-param name="title" select="./@search" />
				</xsl:call-template>
			</div>
			<div style="position:absolute;left:20px;top:20px;">
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="25" />
					<xsl:with-param name="height" select="23" />
					<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/application/ptshad.png')" />
					<xsl:with-param name="title" select="./@search" />
				</xsl:call-template>
			</div-->
			<div style="position:absolute;left:15px;top:15px;">
				<xsl:call-template name="formatImage">
					<xsl:with-param name="browser" select="/response/@browser" />
					<xsl:with-param name="width" select="30" />
					<xsl:with-param name="height" select="30" />
					<xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/application/addressFlag.png')" />
					<xsl:with-param name="title" select="./@search" />
				</xsl:call-template>
			</div>
		</div>
	</div>
</xsl:template>

<!-- AS: контролы внизу балона адреса -->
<xsl:template match="address" mode="baloonBottomControls">
	<div xmlns="http://www.w3.org/1999/xhtml">
		<xsl:value-of select="$locale/data[@name='create']/value" />:
		<xsl:text> </xsl:text>
    <xsl:variable name="createGeopointShort" select="concat(/response/@pointPrefix, 'createGeopointShort')" />
    <span class="toggle underlined"><xsl:value-of select="$locale/data[@name='$createGeopointShort']/value" /></span>
		<span class="divider"><xsl:text> </xsl:text></span>
		<span class="toggle underlined"><xsl:value-of select="$locale/data[@name='createGeozoneShort']/value" /></span>
	</div>
</xsl:template>

<!-- AS: контролы внизу балона адреса:
	Отобразить объекты в заданном радиусе (радиус в километрах)
-->
<xsl:template match="address" mode="baloonBottomControls2">
	<div xmlns="http://www.w3.org/1999/xhtml">
		<span class="toggle underlined" onclick="jQuery.searchAddress.showClosestObjectsParamsDlg(event)" id="objectsCloseByToggle"><xsl:value-of select="$locale/data[@name='objectsCloseBy']/value" /></span>
		<!--span class="toggle underlined grey"><xsl:value-of select="$locale/data[@name='objectsCloseBy']/value" /></span-->
	</div>
	<xsl:apply-templates select="." mode="showClosestObjectsParamsDlg" />
</xsl:template>

<xsl:template match="address" mode="showClosestObjectsParamsDlg">
	<div xmlns="http://www.w3.org/1999/xhtml" style="display:none;width:100%;" id="showClosestObjectsParamsDlg">
		<b><xsl:value-of select="$locale/data[@name='objectsCloseBy']/value" /></b>
		<div style="width:100%;">
			<div style="float:left;width:30%;">
				<xsl:value-of select="$locale/data[@name='showObjectsInsideOfRadius']/value" />:
			</div>
			<div style="float:left;width:30%;">
				<xsl:variable name="radiusVal">
					<xsl:choose>
						<xsl:when test="./radiusMeters"><xsl:value-of select="./radiusMeters" /></xsl:when>
						<xsl:otherwise>500</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<input type="text" id="closeObjectsRadius" style="width:50px;" value="{$radiusVal}" />&#160;<xsl:text /><xsl:value-of select="$locale/data[@name='metresMeasure']/value" />
			</div>
			<div style="float:left;width:40%;">
				<button type="button" onclick="jQuery.searchAddress.showClosestObjects(event, true)"><xsl:value-of select="$locale/data[@name='show']/value" /></button>
			</div>
		</div>
		<div style="clear:both;">
			<span class="toggle underlined small" onclick="jQuery.searchAddress.showSearchDlgParameters(event)"><xsl:text>«&#160;</xsl:text><xsl:value-of select="$locale/data[@name='getback']/value" /></span>
		</div>
	</div>
	<!--div class="clear">&#160;<xsl:text /></div-->
</xsl:template>

<!-- AS: формирует HTML со списком найденных адресов -->
<xsl:template match="response" mode="showRadius">
	<response>
		<xsl:choose>
			<xsl:when test="contains(./@browser, 'IE')">
				<v:arc id="addressRadius" style="position:absolute;left:{./radius/x - ./radius/radius div 2}px;top:{./radius/y - ./radius/radius div 2}px;width:{./radius/radius}; height:{./radius/radius}px;z-index:-15" startangle="0" endangle="360" strokecolor="#000000">
					<v:fill color="#c0c0c0" opacity="30%" />
				</v:arc>
			</xsl:when>
			<xsl:otherwise>
				<div id="addressRadius" xmlns="http://www.w3.org/1999/xhtml" style="position:absolute;left:{./radius/x - ./radius/radius div 2}px;top:{./radius/y - ./radius/radius div 2}px;width:{./radius/radius}px;height:{./radius/radius}px;">
				    <svg xmlns="http://www.w3.org/2000/svg" width="{./radius/radius}" height="{./radius/radius}">
				      <circle cx="{./radius/radius div 2}" cy="{./radius/radius div 2}" r="{./radius/radius div 2}" fill-opacity="0.3" stroke="#000000" fill="#c0c0c0" />
				    </svg>
				</div>
			</xsl:otherwise>
		</xsl:choose>
		<splitter />
		<div xmlns="http://www.w3.org/1999/xhtml" id="closestObjects" style="height:120px;overflow-y:scroll;">
			<xsl:choose>
				<xsl:when test="./closest">
					<xsl:apply-templates select="./closest" />
				</xsl:when>
				<xsl:otherwise>
					<p class="message Warning">
						<xsl:value-of select="$locale/data[@name='thereAreNoObjects']/value" />
					</p>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<splitter />

		<!-- AS: сдесь отрисовка маршрутов до найденных машин -->
		<div>
			<xsl:if test="./closest">
				<xsl:apply-templates select="./closest/distanceToVehicle" mode="route" />
			</xsl:if>
		</div>
	</response>
</xsl:template>

<!-- AS: отображение ближайших к адресу объектов  -->
<xsl:template match="closest">
	<div>
		<b><xsl:value-of select="$locale/data[@name='closestObjects']/value" /></b> (<xsl:value-of select="count(./distanceToVehicle)" />)
	</div>
	<xsl:apply-templates select="./distanceToVehicle" mode="vehicle" />
</xsl:template>

<!-- AS: отображение в списке найденных объектов -->
<xsl:template match="distanceToVehicle" mode="vehicle">
	<div>
		<div>
			<xsl:apply-templates select="./vehicle" mode="listLite" />
		</div>
		<div style="margin-bottom:10px;">
			<xsl:variable name="distance">
				<xsl:choose>
					<xsl:when test="./distance = 0">0</xsl:when>
					<xsl:when test="./distance &lt; 1000"><xsl:value-of select="concat('0', format-number(round(./distance) div 1000, '### ### ###.##'))" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="format-number(round(./distance) div 1000, '### ### ###.##')" /></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="directDistance">
				<xsl:choose>
					<xsl:when test="./directDistance = 0">0</xsl:when>
					<xsl:when test="./directDistance &lt; 1000"><xsl:value-of select="concat('0', format-number(round(./directDistance) div 1000, '### ### ###.##'))" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="format-number(round(./directDistance) div 1000, '### ### ###.##')" /></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<div>
				<xsl:value-of select="$locale/data[@name='directDistance']/value" />: <xsl:value-of select="$directDistance" />&#160;<xsl:value-of select="$locale/data[@name='distanceMeasure']/value" /><xsl:text>.&#160;</xsl:text>
			</div>
			<div>
				<xsl:variable name="middleLongitude" select="(./route/sections/maxLongitude + ./route/sections/minLongitude) div 2" />
				<xsl:variable name="middleLatitude" select="(./route/sections/maxLatitude + ./route/sections/minLatitude) div 2" />
				<span class="toggle underlined" onclick="Monitoring.ShowRoute({./vehicle/@id}, event, {$middleLatitude}, {$middleLongitude}, '{./vehicle/@GarageNumber}')"><xsl:value-of select="$locale/data[@name='roadDistance']/value" /></span>: <xsl:value-of select="$distance" />&#160;<xsl:value-of select="$locale/data[@name='distanceMeasure']/value" /><xsl:text>.&#160;</xsl:text>
			</div>
		</div>
		<xsl:call-template name="clear" />
	</div>
</xsl:template>

<!-- AS: отрисовка маршрутов до найденных объектов -->
<xsl:template match="distanceToVehicle" mode="route">
	<xsl:variable name="routeStyle">
		<xsl:choose>
			<xsl:when test="./route/@showMode = 'show'">display:block;</xsl:when>
			<xsl:otherwise>display:none;</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<div id="routeToVehicle_{./vehicle/@id}" style="{$routeStyle}" class="routeToVehicle">
		<xsl:apply-templates select="./route/sections" mode="map">
			<xsl:with-param name="browser" select="/response/@browser" />
		</xsl:apply-templates>
	</div>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\page\page.xmladdress.xsl_1.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->