﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE xhtml [
    <!ENTITY nbsp "&#160;">
    <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:svg="http://www.w3.org/2000/svg">
    <xsl:import href="common.xsl" />

    <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:variable name="path" select="/response/@ApplicationPath" />


    <xsl:variable name="localMessageFile">
        <xsl:text>./res/messages</xsl:text>
        <!--xsl:choose>
			<xsl:when test="contains(/response/@language, 'ru-') != 'true'">
				<xsl:text>.</xsl:text><xsl:value-of select="/response/@language" />
			</xsl:when>
		</xsl:choose-->
        <xsl:text>.</xsl:text>
        <xsl:value-of select="/response/@language" />
        <xsl:text>.xml</xsl:text>
    </xsl:variable>
    <xsl:variable name="locale" select="document($localMessageFile)/root" />

    <!-- AS: JS-файл с  сообщениями, выводящимися на клиенте без применения ajax'a -->
    <xsl:variable name="localJsName">
        <xsl:value-of select="/response/@baseWebAppPath"/>
        <xsl:text>/js/lang/messages</xsl:text>
        <xsl:choose>
            <xsl:when test="contains(/response/@language, 'ru-') != 'true'">
                <xsl:text>.</xsl:text>
                <xsl:value-of select="/response/@language" />
            </xsl:when>
        </xsl:choose>
        <xsl:text>.js</xsl:text>
    </xsl:variable>
    
    
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <title>
                  <xsl:value-of select="/response/@title"/>
                </title>
                <link rel="shortcut icon" href="{./@baseWebAppPath}/{./@favicon}"/>
                <script type="text/javascript" src="{$localJsName}"></script>
              <xsl:call-template name="link-css" />
            </head>
            <body>
                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>

                <xsl:call-template name="websso"/>
               <xsl:call-template name="loginPopup">
                  <xsl:with-param name="path" select="$path"/>
               </xsl:call-template>
               <xsl:apply-templates select="." mode="commonVariables"></xsl:apply-templates> 
                <xsl:call-template name="requireJS">
                    <xsl:with-param name="path" select="$path"/>
                </xsl:call-template>
            </body>
        </html>
    </xsl:template>
    
    
</xsl:stylesheet>