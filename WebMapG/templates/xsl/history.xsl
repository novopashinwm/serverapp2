<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
	<!ENTITY amp "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="common.xsl" />
	<xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="//@template = 'historyList'">
				<xsl:apply-templates select="." mode="historyList" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="response" mode="historyList">
		<script type="text/javascript">
			//var hlb = $("#historyListBody");
			//var angle = 0;
			jQuery.createHistoryList();
		</script>
		<xsl:apply-templates select="./message" />
		<xsl:choose>
			<xsl:when test="count(./history/point) &gt; 0">
				<ul class="history" id="historyTree">
					<xsl:apply-templates select="./history/point" />
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$locale/data[@name='noHistoryData']/value" />
			</xsl:otherwise>
		</xsl:choose>
		<script type="text/javascript">
			jQuery.setHistoryHandlers();
			//$("#historyTab").tablesorter();
		</script>
	</xsl:template>

	<xsl:template match="point">
		<li class="treeNode">
			<table cellpadding="0" cellspacing="0" style="background-color:#ccc;" border="0" class="historyTab">
				<tbody>
					<tr>
						<td valign="top" align="left" style="width:16px">
							<div class="blockExpanded toggle_{./@type}" onclick="jQuery.operateHistoryNode(event, 'historyTab_{position()}', {position()})">
								<xsl:text>&#160;&#160;</xsl:text>
							</div>
						</td>
						<td valign="top" align="left">
							<div class="{./@type}">
								<xsl:text>&#160;&#160;</xsl:text>
							</div>
						</td>
						<td>
							<div class="hide" id="historyBlockInfo_{position()}">
								<div><b><xsl:value-of select="$locale/data[@name='from']/value" /><xsl:text>:&#160;</xsl:text></b><xsl:value-of select="./unit[1]/Time" /></div>
								<div><b><xsl:value-of select="$locale/data[@name='to']/value" /><xsl:text>:&#160;</xsl:text></b><xsl:value-of select="./unit[last()]/Time" /></div>
								<div><b><xsl:value-of select="$locale/data[@name='positionNumber']/value" /><xsl:text>:&#160;</xsl:text></b><xsl:value-of select="count(./unit)" /></div>
							</div>
							<div id="historyTab_{position()}">
								<table border="0" cellpadding="0" cellspacing="0" class="history">
									<thead>
										<tr>
											<!--th style="background-color: #dddddd"><xsl:text>&#160;</xsl:text></th-->
											<th style="background-color: #dddddd"><xsl:value-of select="$locale/data[@name='date']/value" /></th>
											<th style="background-color: #dddddd"><!--xsl:value-of select="$locale/data[@name='speed']/value" /--><xsl:value-of select="$locale/data[@name='speedMeasure']/value" /></th>
											<th style="background-color: #dddddd"><!--xsl:value-of select="$locale/data[@name='ignition']/value" /-->&#160;</th>
										</tr>
									</thead>
									<tbody>
										<xsl:variable name="dateFrom">
											<xsl:if test="./@type = 'Stop'">
												<xsl:value-of select="./unit[1]/Time" />
											</xsl:if>
										</xsl:variable>
										<xsl:variable name="dateTo">
											<xsl:if test="./@type = 'Stop'">
												<xsl:value-of select="./unit[last()]/Time" />
											</xsl:if>
										</xsl:variable>
										<xsl:apply-templates select="./unit">
											<xsl:with-param name="dateFrom" select="$dateFrom" />
											<xsl:with-param name="dateTo" select="$dateTo" />
										</xsl:apply-templates>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</li>
	</xsl:template>

	<xsl:template match="unit">
		<xsl:param name="dateFrom" select="''" />
		<xsl:param name="dateTo" select="''" />
		<tr>
			<!--td style="background-color: #dddddd"><xsl:text>&#160;</xsl:text></td-->
			<td>
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="class">historyOdd</xsl:attribute>
				</xsl:if>
				<span class="toggle" onclick="jQuery.centerClosestImageOnMap('{./Longitude}', '{./Latitude}')">
					<xsl:if test="./@badPosition = 'true'">
						<xsl:attribute name="class">incorrect</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="./Time" />
				</span>
			</td>
			<td>
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="class">historyOdd</xsl:attribute>
				</xsl:if>
				<span>
					<xsl:if test="./@badPosition = 'true'">
						<xsl:attribute name="class">incorrect</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="./Speed" />
				</span>
			</td>
			<td style="padding-right:10px;">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="class">historyOdd</xsl:attribute>
				</xsl:if>
				<xsl:variable name="ignitionValue">
					<xsl:value-of select="/response/@ApplicationPath" />/img/icons/objects2/ign_<xsl:value-of select="./DigitalSensors" />.png
				</xsl:variable>
				<span class="png2"><img src="{$ignitionValue}" border="0" width="16" /></span>
				<xsl:variable name="imgSourceComponent">
					<xsl:value-of select="concat(/response/@ApplicationPath, '/img/icons/history/h_rest')" />
				</xsl:variable>
				<script type="text/javascript">
					var angle = jQuery.getAngle(<xsl:value-of select="./Course" />);
					<xsl:variable name="title">
						<!--xsl:if test="string-length($dateFrom) &gt; 0 and string-length($dateTo) &gt; 0">
								<div><b><xsl:value-of select="$locale/data[@name='from']/value" /><xsl:text>:&#160;</xsl:text></b><xsl:value-of select="$dateFrom" /></div>
								<xsl:text>&#160;</xsl:text>
								<div><b><xsl:value-of select="$locale/data[@name='to']/value" /><xsl:text>:&#160;</xsl:text></b><xsl:value-of select="$dateTo" /></div>
						</xsl:if-->
					</xsl:variable>

					var hp = new jQuery.prototype.listItem();
					//hp.create(false, "", <xsl:value-of select="./Unique" /><xsl:value-of select="position()" />);
					hp.create(false, '<xsl:value-of select="$title" />', '<xsl:value-of select="./@groupNumber" />_<xsl:value-of select="position()" />');
					//hp.setSource('<xsl:value-of select="$imgSourceComponent" />' + jQuery.correctAngle(angle) + '.png');
					hp.setGeoCoords('<xsl:value-of select="./Longitude" />', '<xsl:value-of select="./Latitude" />');
					hp.setCourse(angle);
					hp.ReplaceProperty(jQuery.messages.date, '<xsl:value-of select="./Time" />');
					hp.ReplaceProperty(jQuery.messages.speed, '<xsl:value-of select="./Speed" />');

					hp.setSource(jQuery.getMobilUnitImage(hp));

					var description = '<div>';
					description += '<strong><xsl:value-of select="$locale/data[@name='date']/value" /></strong>: <xsl:value-of select="./Time" />';
					description += '</div>';
					description += '<div>';
					description += '<strong><xsl:value-of select="$locale/data[@name='speed']/value" /></strong>: <xsl:value-of select="./Speed" /><xsl:value-of select="$locale/data[@name='speedMeasure']/value" />';
					description += '</div>';
					hp.setDescription(description);
					jQuery.getHistoryList().Add(hp);
				</script>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->