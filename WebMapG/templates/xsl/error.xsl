﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:import href="common.xsl" />
  <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="/response" />
  </xsl:template>

  <xsl:variable name="path">/about/</xsl:variable>
    
  <!--Сообщение об ошибке на сервере-->
  <xsl:template match="response[@template='exception']">
    <html>
      <head>
        <title>
          <xsl:value-of select="$locale/data[@name='exceptionMessage']/value" />
        </title>
        <xsl:apply-templates select="." mode="headContent" />
          
      </head>
      <body class="exceptionMessagePage">
          <xsl:if test="/response/@color-schema = 'blue' ">
              <xsl:attribute name="class">
                  blue-schema
              </xsl:attribute>
          </xsl:if>
          <xsl:if test="/response/@WebSSO">
              <xsl:attribute name="class">websso</xsl:attribute>
          </xsl:if>
          <xsl:call-template name="websso"/>
        <xsl:apply-templates mode="pageHeader" select="."/>
          
        <div id="additionalPageContent">
          <div class="main-content no-margin">
            <h1>
              <xsl:value-of select="$locale/data[@name='exceptionMessage']/value" />
            </h1>
            <p>
              <a>
                <xsl:attribute name="href">
                  <xsl:choose>
                    <xsl:when test="/response/@ApplicationPath = ''">/</xsl:when>
                    <xsl:otherwise><xsl:value-of select="$path"/></xsl:otherwise>
                  </xsl:choose>                  
                </xsl:attribute>
                <xsl:value-of select="$locale/data[@name='gotoStartPage']/value" />
              </a>
            </p>
          </div>
        </div>
        <!--xsl:call-template name="fstPageFooter" /-->
        <!--xsl:call-template name="pageFooter" /-->
        <xsl:call-template name="requireJS" />

          <xsl:apply-templates select="." mode="pageFooter" />
          
      </body>
    </html>
  </xsl:template>


    <xsl:template match="response" mode="pageHeader">
        <xsl:param name="selectedTab" select="'map'" />
        <xsl:param name="additional-main-content" />
        <script type="text/javascript">
            var CurrentOperator = {
            name:"<xsl:value-of select="./Person/Name"/>",
            email:"<xsl:value-of select="./Person/Email"/>"
            };
        </script>



        <xsl:call-template name="navTop"></xsl:call-template>

        <div class="header less">
            <a href="http://www.ufin.online" class="mts {$lang}" title="UFIN" target="_blank"></a>

            <a href="{$pathAbout}" class="nika {$lang}" title="UFIN.ONLINE"></a>

        </div>


    </xsl:template>


  <xsl:template match="response" mode="pageFooter">
    <xsl:call-template name="clear" />
    <!-- footer -->
    <div class="empty"></div>
    <div class="footer">
      <div class="inner-footer">
        <xsl:variable name="copyright" select="/response/@Copyright.Locale" />
        <div class="inner-footer">
          <xsl:if test="$copyright">
            <div class="info">
              <p>
                &#169; <span><xsl:value-of select="$locale/data[@name=$copyright]/value" /></span>
              </p>
            </div>
          </xsl:if>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="response[@template='accessIsDenied']">
    <html>
      <head>
        <title>
          <xsl:value-of select="$locale/data[@name='accessIsDenied']/value" />
        </title>
        <xsl:apply-templates select="." mode="headContent" />
      </head>
      <body class="exceptionMessagePage">
        <xsl:call-template name="common-page-header"></xsl:call-template>
        <div id="additionalPageContent">
          <div class="main-content">
            <h1>
              <xsl:value-of select="$locale/data[@name='accessIsDenied']/value" />
            </h1>
          </div>
        </div>
        <xsl:call-template name="requireJS" />

          <xsl:apply-templates select="." mode="pageFooter" />
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->