﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:chart="http://web.ufin.online"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
    <xsl:import href="common.xsl" />
    <xsl:import href="trackingObject_G.xsl" />
    <xsl:import href="uielements.xsl" />
    <xsl:import href="dateTime.xsl" />
    <xsl:import href="charts.xsl" />
    <xsl:import href="geozone.xsl" />
    <xsl:output method="xml" indent="yes"/>
    <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//@template = 'abonentPage'">
                <xsl:apply-templates select="." mode="abonentPage" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:variable name="path" select="/response/@ApplicationPath" />
    
    <xsl:template match="response" mode="abonentPage">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>

                <xsl:apply-templates select="." mode="headContent" />

                <script type="text/javascript">
                    UFIN.pageType = 'abonent';
                    UFIN.customerType = '<xsl:value-of select="/response/Departments/Department/Type" />'.toLowerCase();
                    UFIN.departmentExtIDIsRequired = '<xsl:value-of select="/response/@DepartmentExtIDIsRequired" />' === 'true';

                </script>         
        
            </head>

            <body>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
                <xsl:call-template name="commonBodyIncludes" />

                <xsl:apply-templates select="." mode="pageHeader">
                    <xsl:with-param name="selectedTab" select="'abonent'" />
                </xsl:apply-templates>

                <xsl:variable name="menuItem">
                    <xsl:choose>
                        <xsl:when test="./@menuItem and ./@menuItem != ''">
                            <xsl:value-of select="./@menuItem"/>
                        </xsl:when>
                        <xsl:when test="./systemRights[right = 'SecurityAdministration']">
                            <xsl:value-of select="'operatorRights'"/>
                        </xsl:when>
                        <xsl:when test="./systemRights[right = 'ServiceManagement']">
                            <xsl:value-of select="'newCustomer'"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:variable>

                <div class="b-content customer">
                    <xsl:choose>
                        <xsl:when test="./systemRights[right = 'ServiceManagement']">
                            <div class="loadInProgress"></div>
                        </xsl:when>
                    </xsl:choose>
                </div>

                <xsl:call-template name="commonPopupMessage" />
              <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />
                <!--xsl:call-template name="waitDialog" /-->
            </body>
        </html>
    </xsl:template>


    
</xsl:stylesheet>
