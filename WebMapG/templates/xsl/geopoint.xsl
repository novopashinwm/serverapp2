<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="xml" indent="no" encoding="utf-8" omit-xml-declaration="yes" />

	<!--
        ******************************************************************************************************
		Формирует JSON-ответ на запрос к странице map.aspx с параметром a=getGeoPoints, в котором
		перечисляется список Точек Интереса (POI) текущего пользователя.
		
		(!) В текущей реализации серверного API отсутствует метод, возвращающий список Точек Интереса в 
		JSON-формате (как это сделано, к примеру, для списка объектов, групп или геозон). Вместо этого старая 
		реализация web-приложения ожидала, что сервер сформирует кусок html-страницы со списком этих точек.
        ******************************************************************************************************
    -->
	
    <xsl:template match="/">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates select="/response/userPoints/PointsWeb" />
		<xsl:text>]</xsl:text>
    </xsl:template>
	
	<xsl:template match="PointsWeb">
		<xsl:text>{</xsl:text>
			<!-- Перечисляем свойства объекта POI -->
			<xsl:text>"id":</xsl:text><xsl:value-of select="POINT_ID" /><xsl:text>,</xsl:text>
			<xsl:text>"name":"</xsl:text><xsl:value-of select="NAME" /><xsl:text>",</xsl:text>
			<xsl:text>"description":"</xsl:text><xsl:value-of select="DESCRIPTION" /><xsl:text>",</xsl:text>
			<xsl:text>"lat":</xsl:text><xsl:value-of select="Y" /><xsl:text>,</xsl:text>
			<xsl:text>"lng":</xsl:text><xsl:value-of select="X" /><xsl:text>,</xsl:text>
			<xsl:text>"editable":</xsl:text><xsl:value-of select="Editable" />
		<xsl:text>}</xsl:text>
		<!-- Если это не последний элемент в наборе - добавляем запятую -->
		<xsl:if test="position()!=count(../*)">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>