<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:user="urn:my-scripts">
    <xsl:output method="xml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <!-- AS: шаблон поля для редактирования текста -->
    <xsl:template name="textEdit">
        <xsl:param name="multiline" select="'false'" />
        <xsl:param name="rows" select="'5'" />
        <!-- AS: ставится в случае многострочного ввода -->
        <xsl:param name="required" select="'false'" />
        <xsl:param name="id" select="''" />
        <xsl:param name="satteliteText" select="''" />
        <xsl:param name="class" select="''" />
        <xsl:param name="style" select="''" />
        <xsl:param name="value" select="''" />
        <xsl:param name="maxlen" select="'20000'" />
        <xsl:param name="disabled" select="''" />
        <!-- AS: event handlers go here -->
        <xsl:param name="onkeypress" />

        <strong>
            <xsl:value-of select="$satteliteText" />
        </strong>
        <xsl:if test="$required = 'true'">
            <sup class="required">*</sup>
        </xsl:if>
        <div>
            <xsl:variable name="fullClass">
                <xsl:value-of select="$class" />
                <xsl:text> </xsl:text>data<xsl:if test="$required = 'true'">
                    <xsl:text> </xsl:text>required
                </xsl:if>
            </xsl:variable>

            <xsl:choose>
                <xsl:when test="$multiline = 'true'">
                    <textarea id="{$id}" name="{$id}" class="{$fullClass}" rows="{$rows}" onkeypress="{$onkeypress}" style="{$style}">
                        <xsl:value-of select="$value" />
                    </textarea>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$id = 'phone'">
                            <input type="text" id="{$id}" name="{$id}" class="{$fullClass}" value="{$value}" maxlength="{$maxlen}" style="{$style}" disabled="disabled" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$disabled = 'disabled'">
                                    <input type="text" id="{$id}" name="{$id}" class="{$fullClass}" value="{$value}" maxlength="{$maxlen}" style="{$style}" disabled="disabled"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <input type="text" id="{$id}" name="{$id}" class="{$fullClass}" value="{$value}" maxlength="{$maxlen}" style="{$style}"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <!-- AS: заголовок окна -->
    <xsl:template name="windowHeader">
        <xsl:param name="windowName" select="''" />
        <xsl:param name="windowId" select="''" />
        <!--table border="0" cellpadding="0" cellspacing="0">
                <tbody>
                        <tr>
                                <td>
                                        <span class="windowHeader"><xsl:value-of select="$windowName" /></span>
                                </td>
                                <xsl:if test="string-length($windowId) &gt; 0">
                                        <td align="right" valign="top" style="text-align:right; width:16px">
                                                <span class="closeWin" onclick="jQuery.geopoint.closeGeopointDescription('{$windowId}')"><span><xsl:text>&#160;x</xsl:text></span></span>
                                        </td>
                                </xsl:if>
                        </tr>
                </tbody>
        </table-->
        <div style="width:100%;">
            <div style="float:left;width:80%;">
                <span class="windowHeader" style="text-overflow:ellipsis!important;white-space:nowrap;overflow:hidden;">
                    <xsl:value-of select="$windowName" />
                </span>
            </div>
            <xsl:if test="string-length($windowId) &gt; 0">
                <div style="float:left;width:20%;text-align:right;">
                    <span class="closeWin" onclick="jQuery.geopoint.closeGeopointDescription('{$windowId}')">
                        <span>
                            <xsl:text>&#160;x</xsl:text>
                        </span>
                    </span>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <!-- AS: заголовок окна - редактирование -->
    <xsl:template name="windowHeaderEdit">
        <xsl:param name="windowName" select="''" />
        <xsl:param name="windowId" select="''" />
        <xsl:param name="windowTitleEditId" select="''" />
        <xsl:param name="onclose" select="''" />
        <!--table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                        <tr>
                                <td>
                                        <span class="windowHeader">
                                        <xsl:call-template name="textEdit">
                                                <xsl:with-param name="id" select="$windowTitleEditId" />
                                                <xsl:with-param name="value" select="$windowName" />
                                        </xsl:call-template>
                                        </span>
                                </td>
                                <xsl:if test="string-length($windowId) &gt; 0">
                                        <td align="right" valign="top" style="text-align:right; width:16px">
                                                <span class="closeWin" onclick="{$onclose}"><span><xsl:text>&#160;x</xsl:text></span></span>
                                        </td>
                                </xsl:if>
                        </tr>
                </tbody>
        </table-->
        <div style="width:100%;">
            <div style="float:left;width:80%;">
                <span class="windowHeader">
                    <xsl:call-template name="textEdit">
                        <xsl:with-param name="id" select="$windowTitleEditId" />
                        <xsl:with-param name="value" select="$windowName" />
                        <xsl:with-param name="style" select="'border:0;'" />
                    </xsl:call-template>
                </span>
            </div>
            <xsl:if test="string-length($windowId) &gt; 0">
                <div style="float:left;width:20%;text-align:right;">
                    <span class="closeWin" onclick="{$onclose}">
                        <span>
                            <xsl:text>&#160;x</xsl:text>
                        </span>
                    </span>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <!-- Линейка масштаба -->
    <xsl:template name="scaleControl">
        <div id="scaleimg" style="width:28px;z-index:10000;">
            <!--div class="png" style="width:28px; height:22px;">
                    <img id="scale10" src="{//@ApplicationPath}/img/polzunok/polzunok_max.png" width="28" height="22" />
            </div>
            <div class="png" style="width:28px; height:22px;">
                    <img id="scale9" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div-->
            <div class="png toggle" style="width:28px; height:22px;">
                <img id="scaleStart" src="{//@ApplicationPath}/img/polzunok/polzunok_max.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(9)">
                <img id="scale8" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(8)">
                <img id="scale7" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(7)">
                <img id="scale6" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(6)">
                <img id="scale5" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(5)">
                <img id="scale4" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(4)">
                <img id="scale3" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(3)">
                <img id="scale2" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(2)">
                <img id="scale1" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;" onclick="Tss.changeScale(1)">
                <img id="scale0" src="{//@ApplicationPath}/img/polzunok/polzunok_normal.png" width="28" height="22" />
            </div>
            <div class="png toggle" style="width:28px; height:22px;">
                <img id="scaleStart" src="{//@ApplicationPath}/img/polzunok/polzunok_min.png" width="28" height="22" />
            </div>
        </div>
    </xsl:template>

    <!-- AS: форматирует картинку PNG - обрабатывает случай с ебучим IE -->
    <xsl:template name="formatImage">
        <xsl:param name="width" select="'32'" />
        <xsl:param name="height" select="'32'" />
        <xsl:param name="src" />
        <xsl:param name="browser" />
        <xsl:param name="title" select="''" />
        <xsl:param name="style" />
        <!--	<xsl:choose>
        <xsl:when test="contains($src, '.png')">
                <xsl:choose>
                        <xsl:when test="contains($browser, 'IE')">
                                <span title="{$title}">
                                <xsl:attribute name="style">
                                        filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$src" />', sizingMethod='scale')";width:<xsl:value-of select="$width" />px; height:<xsl:value-of select="$height" />px;
                                        <xsl:if test="$style"><xsl:value-of select="$style" /></xsl:if>
                                </xsl:attribute></span>
                        </xsl:when>
                        <xsl:otherwise>
                                <img src="{$src}" border="0" title="{$title}" style="{$style}" width="{$width}px" height="{$height}px" />
                        </xsl:otherwise>
                </xsl:choose>
        </xsl:when>
        <xsl:otherwise>-->
        <img src="{$src}" border="0" title="{$title}" style="{$style}" width="{$width}px" height="{$height}px" />
        <!--</xsl:otherwise>
        </xsl:choose>-->
    </xsl:template>

    <!-- AS: печать карты -->
    <xsl:template name="print">
        <span class="toggle" onclick="jQuery.printDocument()">
            <xsl:call-template name="formatImage">
                <xsl:with-param name="browser" select="/response/@browser" />
                <xsl:with-param name="width" select="'18'" />
                <xsl:with-param name="height" select="'18'" />
                <xsl:with-param name="title" select="$locale/data[@name='print']/value" />
                <xsl:with-param name="src" select="concat(/response/@ApplicationPath, '/img/toolbar/print.png')" />
            </xsl:call-template>
        </span>
    </xsl:template>

    <!-- AS: выпадающий список интервала обновления -->
    <xsl:template name="refreshInterval2">
        <xsl:param name="controlId" select="'refresh'" />
        <xsl:variable name="seconds">
            <xsl:value-of select="$locale/data[@name='SecondsTitle']/value" />
        </xsl:variable>
        <select id="{$controlId}" class="data" style="width:40%!important; margin:0; padding: 0;">
            <option value="5">
                5 <xsl:value-of select="$seconds" />
            </option>
            <option value="10">
                10 <xsl:value-of select="$seconds" />
            </option>
            <option value="30" selected="selected">
                30 <xsl:value-of select="$seconds" />
            </option>
            <option value="60">
                60 <xsl:value-of select="$seconds" />
            </option>
            <option value="120">
                <xsl:value-of select="$locale/data[@name='2minutes']/value" />
            </option>
            <option value="300">
                <xsl:value-of select="$locale/data[@name='5minutes']/value" />
            </option>
        </select>
    </xsl:template>

    <!-- AS: Контрол выбора интервалов времени -->
    <xsl:template name="timeInterval">
        <xsl:param name="dateFrom" />
        <xsl:param name="dateTo" />
        <xsl:param name="timeFrom" />
        <xsl:param name="timeTo" />
        <div class="intervalToggler">
            <div class="toggleButtons">
                <span class="toggle" data-interval="1" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='1hour']/value" />
                </span>
                <span class="toggle"  data-interval="2" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='2hours']/value" />
                </span>
                <span class="toggle"  data-interval="3" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='3hours']/value" />
                </span>
                <span class="toggle"  data-interval="6" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='6hours']/value" />
                </span>
                <span class="toggle"  data-interval="12" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='12hours']/value" />
                </span>
                <span class="toggle"  data-interval="24" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='24hours']/value" />
                </span>
                <span class="toggle"  data-interval="36" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='36hours']/value" />
                </span>
                <span class="toggle"  data-interval="48" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='2days']/value" />
                </span>
                <span class="toggle"  data-interval="72" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='3days']/value" />
                </span>
                <span class="toggle"  data-interval="168" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='1week']/value" />
                </span>
                <span class="toggle"  data-interval="336" data-timeswitch="hour" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                    <xsl:value-of select="$locale/data[@name='2week']/value" />
                </span>
            </div>
        </div>
    </xsl:template>

    <!-- AS: Контрол выбора интервалов дат
            dateTimeTools.js required!
    -->
    <xsl:template name="dateIntervalForUserCommonPeriods">
        <xsl:param name="dateFrom" />
        <xsl:param name="dateTo" />
        <xsl:param name="timeFrom" />
        <xsl:param name="timeTo" />
        <div class="intervalToggler">
            <div class="toggleButtons">

                <xsl:choose>
                    <xsl:when test="$timeFrom and $timeTo">
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 1, '{$dateFrom}', '{$dateTo}', '{$timeFrom}', '{$timeTo}')">
                            <xsl:value-of select="$locale/data[@name='today']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 2, '{$dateFrom}', '{$dateTo}', '{$timeFrom}', '{$timeTo}')">
                            <xsl:value-of select="$locale/data[@name='yesterday']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 3, '{$dateFrom}', '{$dateTo}', '{$timeFrom}', '{$timeTo}')">
                            <xsl:value-of select="$locale/data[@name='thisMonth']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 4, '{$dateFrom}', '{$dateTo}', '{$timeFrom}', '{$timeTo}')">
                            <xsl:value-of select="$locale/data[@name='lastMonth']/value" />
                        </span>
                    </xsl:when>
                    <xsl:otherwise>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 1, '{$dateFrom}', '{$dateTo}')">
                            <xsl:value-of select="$locale/data[@name='today']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 2, '{$dateFrom}', '{$dateTo}')">
                            <xsl:value-of select="$locale/data[@name='yesterday']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 3, '{$dateFrom}', '{$dateTo}')">
                            <xsl:value-of select="$locale/data[@name='thisMonth']/value" />
                        </span>
                        <span class="toggle" onclick="DateTimeTools.setIntervalForNewPeriod(event, 4, '{$dateFrom}', '{$dateTo}')">
                            <xsl:value-of select="$locale/data[@name='lastMonth']/value" />
                        </span>
                    </xsl:otherwise>
                </xsl:choose>


            </div>
        </div>
    </xsl:template>


    <!-- AS: Контрол выбора интервалов дат
            dateTimeTools.js required!
    -->
    <xsl:template name="dateInterval">
        <xsl:param name="dateFrom" />
        <xsl:param name="dateTo" />
        <xsl:param name="timeFrom" />
        <xsl:param name="timeTo" />
        <xsl:param name="timeDirection" />
        <div class="intervalToggler">
        <div class="toggleButtons">
            <xsl:choose>
                <xsl:when test="$timeDirection = ''">
                    <xsl:choose>
                        <xsl:when test="$timeFrom and $timeTo">
                            <span class="toggle"  data-interval="1" data-timeswitch="day" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}" >
                                <xsl:value-of select="$locale/data[@name='day']/value" />
                            </span>
                            <span class="toggle"  data-interval="7" data-timeswitch="day" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                                <xsl:value-of select="$locale/data[@name='week']/value" />
                            </span>
                            <span class="toggle" data-interval="1" data-timeswitch="month" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}" >
                                <xsl:value-of select="$locale/data[@name='month']/value" />
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, 1, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='day']/value" />
                            </span>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, 7, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='week']/value" />
                            </span>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, 30, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='month']/value" />
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$timeFrom and $timeTo">
                            <span class="toggle"  data-interval="-1" data-timeswitch="month" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}" >
                                <xsl:value-of select="$locale/data[@name='month']/value" />
                            </span>
                            <span class="toggle"  data-interval="-3" data-timeswitch="month" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}">
                                <xsl:value-of select="$locale/data[@name='3 month']/value" />
                            </span>
                            <span class="toggle" data-interval="-6" data-timeswitch="month" data-datefrom="{$dateFrom}" data-dateto="{$dateTo}" data-timefrom="{$timeFrom}" data-timeto="{$timeTo}" >
                                <xsl:value-of select="$locale/data[@name='6 month']/value" />
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, -30, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='30 days']/value" />
                            </span>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, -90, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='90 days']/value" />
                            </span>
                            <span class="toggle" onclick="DateTimeTools.setInterval(event, -180, '{$dateFrom}', '{$dateTo}')">
                                <xsl:value-of select="$locale/data[@name='180 days']/value" />
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </div>
        </div>
    </xsl:template>

    <!-- AS: контрол выбора времени -->
    <xsl:template name="dateTimeFromTo">
        <xsl:param name="dateFromName" select="'dateFrom'" />
        <xsl:param name="timeFromName" select="'timeFrom'" />
        <xsl:param name="dateToName" select="'dateTo'" />
        <xsl:param name="timeToName" select="'timeTo'" />
        <xsl:param name="intervalName" select="'interval'" />
        <div>
            <xsl:call-template name="timeInterval">
                <xsl:with-param name="dateFrom" select="$dateFromName" />
                <xsl:with-param name="dateTo" select="$dateToName" />
                <xsl:with-param name="timeFrom" select="$timeFromName" />
                <xsl:with-param name="timeTo" select="$timeToName" />
            </xsl:call-template>
        </div>
        <div>
            <div class="row">
                <div class="name">
                    <xsl:value-of select="$locale/data[@name='from']/value" />:
                </div>
                <div class="leftInput">
                    <div class="input-group input-group-sm" id="{$dateFromName}_out" data-datepicker="From">
                        <input type="text" class="data date dateFrom inp form-control" id="{$dateFromName}" data-checkdate="" />
                        <span class="input-group-addon">
                            <span class="typcn typcn-calendar-outline"></span>
                        </span>
                    </div>
                    <xsl:text> </xsl:text>
                </div>
                <div class="rightInput">
                    <div class="input-group input-group-sm" id="{$timeFromName}_out" data-timepicker="From">
                        <input type="text" class="data time timeFrom inp form-control" id="{$timeFromName}"  />
                        <span class="input-group-addon">
                            <span class="typcn typcn-time"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div  class="row">
                <div class="name">
                    <xsl:value-of select="$locale/data[@name='to']/value" />:
                </div>
                <div class="leftInput">
                    <div class="input-group input-group-sm" id="{$dateToName}_out"  data-datepicker="To">
                        <input type="text" class="data date dateTo inp form-control" id="{$dateToName}" />
                        <span class="input-group-addon">
                            <span class="typcn typcn-calendar-outline"></span>
                        </span>
                    </div>
                </div>
                <div class="rightInput" >
                    <div class="input-group input-group-sm" id="{$timeToName}_out" data-timepicker="To">
                        <input type="text" class="data timeTo inp form-control" id="{$timeToName}"  />
                        <span class="input-group-addon">
                            <span class="typcn typcn-time"></span>
                        </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                UFIN.uiElem = 'true';
            </script>
            <!--div style="display:none">
                          <div style="float:left;width:30%">
                                  <b><xsl:value-of select="$locale/data[@name='interval']/value" />:</b>
                          </div>
                          <div style="float:left;width:70%">
                                  <xsl:call-template name="refreshInterval2">
                                          <xsl:with-param name="controlId" select="$intervalName" />
                                  </xsl:call-template>
                          </div>
            </div-->
        </div>
    </xsl:template>

    <!-- AS: Контрол выбора дат - "с" и "по" -->
    <xsl:template name="dateFromToCtl">
        <xsl:param name="dateFrom" />
        <xsl:param name="dateTo" />
        <xsl:param name="timeFrom" />
        <xsl:param name="timeTo" />

        <xsl:param name="refreshCtlId" />
        <xsl:param name="parkingCtlId" />
        <xsl:param name="forHistory" select="'false'" />
        <xsl:param name="forParking" select="'false'" />
        <xsl:param name="withTime" select="'true'" />
        <xsl:param name="timeSelector" select="'time'" />

        <xsl:param name="datetime-from-value" />
        <xsl:param name="datetime-to-value" />
        <xsl:param name="date-to-type-name" select="false()" />

        <xsl:variable name="is-date-to-indefinitely" select="user:IsDateTimeMax($datetime-to-value)" />

        <xsl:if test="not($date-to-type-name)">
            <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <td>
                            <xsl:value-of select="$locale/data[@name='SelectLast']/value" />
                        </td>
                    </tr>
                    <tr>
                        <xsl:choose>
                            <xsl:when test="$timeSelector = 'time'">
                                <td>
                                    <xsl:call-template name="timeInterval">
                                        <xsl:with-param name="dateFrom" select="$dateFrom" />
                                        <xsl:with-param name="dateTo" select="$dateTo" />
                                        <xsl:with-param name="timeFrom" select="$timeFrom" />
                                        <xsl:with-param name="timeTo" select="$timeTo" />
                                    </xsl:call-template>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td>
                                    <xsl:call-template name="dateInterval">
                                        <xsl:with-param name="dateFrom" select="$dateFrom" />
                                        <xsl:with-param name="dateTo" select="$dateTo" />
                                    </xsl:call-template>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </tr>
                </tbody>
            </table>
        </xsl:if>
        <table cellpadding="0" cellspacing="0" border="0" class="dateTimeControl">
            <tbody>
                <tr>
                    <td class="label">
                        <label class="form">
                            <span>
                            <xsl:value-of select="$locale/data[@name='dateFrom']/value" />
                            <xsl:text>: </xsl:text>
                            </span>
                        </label>
                    </td>
                    <td class="date input">
                        <div class="input-group" id="{$dateFrom}_out"  data-finish="{$dateTo}" data-datepicker="From">
                            <input type="text" class="dateControl dateFrom data date inp" id="{$dateFrom}" name="{$dateFrom}">
                                <xsl:if test="$datetime-from-value">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="user:GetDate($datetime-from-value)"/>
                                    </xsl:attribute>
                                </xsl:if>
                            </input>
                            <span class="input-group-btn">
                                <button type="button" class="btn">
                                    <span class="typcn typcn-calendar-outline"></span>
                                </button>
                            </span>
                        </div>
                    </td>
                    <xsl:if test="$withTime = 'true'">
                        <td align="left" class="time label">
                            <xsl:value-of select="$locale/data[@name='timeFrom']/value" />
                            <xsl:text>: </xsl:text>
                        </td>
                        <td class="time input">
                            <div class="input-group" id="{$timeFrom}_out" data-timepicker="From">
                                <input type="text" class="dateControl timeFrom data time inp" id="{$timeFrom}" name="{$timeFrom}"  >
                                    <xsl:if test="$datetime-from-value">
                                        <xsl:attribute name="value">
                                            <xsl:value-of select="user:GetTime($datetime-from-value)"/>
                                        </xsl:attribute>
                                    </xsl:if>
                                </input>
                                <span class="input-group-btn">
                                    <button class="btn">
                                        <span class="typcn typcn-time"></span>
                                    </button>
                                </span>
                                <input type="hidden" id="utc_{$dateFrom}_{$timeFrom}" name="utc_{$dateFrom}_{$timeFrom}" class="data" />
                            </div>
                        </td>
                    </xsl:if>
                </tr>
                <tr class="dateTo">
                    <td class="label">                         
                        <label for="{$date-to-type-name}-interval_time">
                            <span>
                            <xsl:value-of select="$locale/data[@name='dateTo']/value" />
                            <xsl:text>: </xsl:text>
                            </span>
                        </label>
                    </td>
                    <td class="date input">
                        <div class="input-group" id="{$dateTo}_out"  data-datepicker="To" >
                            <input type="text" class="dateControl dateTo data date inp" id="{$dateTo}" name="{$dateTo}">
                                <xsl:if test="$datetime-to-value and not($is-date-to-indefinitely)">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="user:GetDate($datetime-to-value)"/>
                                    </xsl:attribute>
                                </xsl:if>
                                <xsl:if test="$is-date-to-indefinitely">
                                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                                </xsl:if>
                            </input>
                            <span class="input-group-btn">
                                <button type="button" class="btn" >
                                    <span class="typcn typcn-calendar-outline"></span>
                                </button>
                            </span>
                        </div>
                        <xsl:if test="$date-to-type-name">
                            <div class="help indefinitely">
                                <input type="checkbox" name="interval_dateToType" class="inp interval_to_indefinitely" data-target="#{$dateTo}">
                                    <xsl:if test="$is-date-to-indefinitely">
                                        <xsl:attribute name="checked">checked</xsl:attribute>
                                    </xsl:if>
                                </input>
                                <label for="interval_dateToType-indefinitely">
                                    <span>
                                        <xsl:value-of select="$locale/data[@name='timeIndefinitely']/value" />
                                    </span>
                                </label>
                            </div>
                        </xsl:if>
                    </td>
                    <xsl:if test="$withTime = 'true'">
                        <td align="left" class="time label">
                            <xsl:value-of select="$locale/data[@name='timeTo']/value" />
                            <xsl:text>: </xsl:text>
                        </td>
                        <td class="time input">
                            <div class="input-group" id="{$timeTo}_out" data-timepicker="To">
                                <input type="text" class="dateControl timeTo data time inp" id="{$timeTo}" name="{$timeTo}" >
                                    <xsl:if test="$datetime-to-value and not($is-date-to-indefinitely)">
                                        <xsl:attribute name="value">
                                            <xsl:value-of select="user:GetTime($datetime-to-value)"/>
                                        </xsl:attribute>
                                    </xsl:if>
                                </input>
                                <span class="input-group-btn">
                                    <button class="btn"  >
                                        <span class="typcn typcn-time"></span>
                                    </button>
                                </span>
                                <input type="hidden" id="utc_{$dateTo}_{$timeTo}" name="utc_{$dateTo}_{$timeTo}" class="data" />
                            </div>
                        </td>
                    </xsl:if>
                </tr>
            <xsl:if test="$refreshCtlId">
                    <tr>
                        <td class="interval label">
                            <xsl:value-of select="$locale/data[@name='interval']/value" />
                            <xsl:text>: </xsl:text>
                        </td>
                        <td colspan="3" class="interval input">
                            <!-- AS: время в секундах между позициями -->
                            <!--xsl:apply-templates select="/response" mode="refresh" /-->
                            <xsl:call-template name="refreshInterval">
                                <xsl:with-param name="controlId" select="$refreshCtlId" />
                                <xsl:with-param name="forHistory" select="$forHistory" />
                            </xsl:call-template>
                        </td>
                    </tr>
            </xsl:if>
            <xsl:if test="$parkingCtlId">
                    <tr>
                        <td class="interval label">
                            <xsl:value-of select="$locale/data[@name='intervalParking']/value" />
                            <xsl:text> &gt; </xsl:text>
                        </td>
                        <td colspan="3" class="interval input">
                            <xsl:call-template name="refreshInterval">
                                <xsl:with-param name="controlId" select="$parkingCtlId" />
                                <xsl:with-param name="forParking" select="$forParking" />
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:if>
                </tbody>
        </table>
    </xsl:template>



    <!-- AS: Выбор цвета -->
    <xsl:template name="colorPick">
        <xsl:param name="colorPickerContainerId" select="'color'" />
        <div>
            <span style="margin-bottom:5px;">
                <xsl:value-of select="$locale/data[@name='colorSelection']/value" />:
            </span>
            <div style="width:100%;">
                <div class="wrapper" style="float:left;width:16px;margin-right:5px;">
                    <span class="color black">
                        <span>bl</span>
                    </span>
                </div>
                <div class="wrapper" style="float:left;width:16px;margin-right:5px;">
                    <span class="color red">
                        <span>re</span>
                    </span>
                </div>
                <div class="wrapper" style="float:left;width:16px;margin-right:5px;">
                    <span class="color green">
                        <span>gr</span>
                    </span>
                </div>
                <div class="wrapper" style="float:left;width:16px;margin-right:5px;">
                    <span class="color blue">
                        <span>be</span>
                    </span>
                </div>
                <div class="wrapper" style="float:left;width:16px;margin-right:5px;">
                    <span class="color yellow">
                        <span>ye</span>
                    </span>
                </div>
            </div>
            <div style="clear:both;">
                <input type="hidden" id="{$colorPickerContainerId}" value="red" />
            </div>
        </div>
    </xsl:template>


    <!-- AS: Выбор карты -->
    <xsl:template match="response" mode="mapSelection">
        <xsl:param name="url" />
        <div id="mapSelection">
            <span id="currentMapName" onclick="Tss.openSelectMapMenu(event)">
                <xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapinfo" />
            </span>
        </div>
        <div style="position:absolute; display:none" id="mapSelectionMenu">
            <xsl:apply-templates select="./mapdescriptions/MapDescription" mode="mapSelection">
                <xsl:with-param name="url" select="$url" />
            </xsl:apply-templates>
        </div>
    </xsl:template>

    <!-- AS: рендерит один пункт меню выбора карт - соответствующую карту (описание карты, вернее) -->
    <xsl:template match="MapDescription" mode="mapSelection">
        <xsl:param name="url" />
        <div style="width:100%;">
            <xsl:choose>
                <xsl:when test="./current = 'true'">
                    <span class="mandatory" style="width:100%;">
                        <xsl:choose>
                            <xsl:when test="contains(/response/@language, 'ru-')">
                                <xsl:value-of select="./names/ru" />
                            </xsl:when>
                            <xsl:when test="contains(/response/@language, 'en-')">
                                <xsl:value-of select="./names/en" />
                            </xsl:when>
                        </xsl:choose>
                    </span>
                </xsl:when>
                <xsl:otherwise>
                    <!--a class="menuItem" href="{//@ApplicationPath}/map.aspx?a=selectMap&amp;map={./shortname}"-->
                    <a class="menuItem" href="{$url}?a=selectMap&amp;map={./shortname}">
                        <xsl:choose>
                            <xsl:when test="contains(/response/@language, 'ru-')">
                                <xsl:value-of select="./names/ru" />
                            </xsl:when>
                            <xsl:when test="contains(/response/@language, 'en-')">
                                <xsl:value-of select="./names/en" />
                            </xsl:when>
                        </xsl:choose>
                    </a>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <!-- AS: шаблон отображения сообщений -->
    <xsl:template match="message">
        <xsl:if test=". and string-length(./MessageTitle) &gt; 0">
            <div id="pageMessageContainer">
                <p>
                    <xsl:choose>
                        <xsl:when test="./Severity = 'Warning'">
                            <xsl:attribute name="class">
                                message<xsl:text> </xsl:text>Warning
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="./Severity = 'Error'">
                            <xsl:attribute name="class">
                                message<xsl:text> </xsl:text>Error
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class">
                                message<xsl:text> </xsl:text>ok
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <strong>
                        <xsl:call-template name="replace">
                            <xsl:with-param name="str" select="./MessageTitle" />
                        </xsl:call-template>
                    </strong>
                    <xsl:if test="./MessageBody and ./MessageBody != ''">
                        <p>
                            <xsl:call-template name="replace">
                                <xsl:with-param name="str" select="./MessageBody" />
                            </xsl:call-template>
                        </p>
                    </xsl:if>
                </p>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="messageContainerOnPage">
        <div id="pageMessageContainer">
        </div>
    </xsl:template>

    <!-- AS: хлебные, сука, крошки, блять -->
    <xsl:template name="breadCrumbs">
        <div id="breadcrumbs"></div>
    </xsl:template>

    <!-- DC: отрисовывает ссылку на попап редактирование текущего телефона. -->
    <xsl:template name="phoneChangeLink">
        <a href="javascript:void(0);" onclick="UserSettings.changePhonePopup()" id="phoneChangeLinkId">
            <xsl:value-of select="$locale/data[@name='phoneChange']/value" />
        </a>
    </xsl:template>

    <!-- DC: отрисовывает ссылку на удаление текущего телефона. -->
    <xsl:template name="phoneDeleteLink">
        <a href="javascript:void(0);" onclick="UserSettings.deletePhonePopup()" id="phoneDeleteLinkId">
            <xsl:value-of select="$locale/data[@name='phoneDelete']/value" />
        </a>
    </xsl:template>

    <!-- AS: шаблон для clear -->
    <xsl:template name="clear">
        <div class="clear"></div>
    </xsl:template>

    <xsl:template name="pageFader">
        <div id="pageFade" style="display:none;" class="fade"></div>
    </xsl:template>

    <xsl:template name="horizontalDivider">
        <div style="height:5px;border-top:1px solid #c0c0c0;margin-top:5px;margin-bottom:5px;padding:0;margin-right:5px;margin-left:5px;">
            <xsl:text>&#160;</xsl:text>
        </div>
    </xsl:template>

    <xsl:template name="note">
        <xsl:param name="text" select="''" />

        <div class="note2" style="padding-left:5px;border-left:5px solid #6666FF;">
            <!--table border="0">
                    <tbody>
                            <tr>
                                    <td width="5%" style="border-left:5px solid #6666FF;"><xsl:text>&#160;</xsl:text></td>
                                    <td width="*">
                                            <p>
                                                    <xsl:copy-of select="$text" />
                                            </p>
                                    </td>
                            </tr>
                    </tbody>
            </table-->
            <p class="first">
                <xsl:copy-of select="$text" />
            </p>
        </div>
    </xsl:template>

    <!-- AS: выбор дня недели -->
    <xsl:template name="weekDaySelection">
        <xsl:param name="useShortNames" select="false()" />
        <xsl:param name="groupName" />
        <xsl:param name="selectedDay" select="'monday'" />

        <xsl:variable name="dayList" select="$locale/weekdays" />
        <div style="width:100%;text-align:left;margin:0;">
            <div style="float:left;width:49%;">
                <xsl:apply-templates select="$dayList/full/data[position() mod 2 != 0]" mode="weekDay">
                    <xsl:with-param name="groupName" select="$groupName" />
                    <xsl:with-param name="selectedDay" select="'monday'" />
                </xsl:apply-templates>
            </div>
            <div style="float:left;width:49%;">
                <xsl:apply-templates select="$dayList/full/data[position() mod 2 = 0]" mode="weekDay">
                    <xsl:with-param name="groupName" select="$groupName" />
                    <xsl:with-param name="selectedDay" select="'monday'" />
                </xsl:apply-templates>
            </div>
            <xsl:call-template name="clear"/>
        </div>
    </xsl:template>

    <!-- AS: выбор дня недели -->
    <xsl:template name="weekDaySelectionPretty">
        <xsl:param name="useShortNames" select="false()" />
        <xsl:param name="groupName" />
        <xsl:param name="selectedDay" select="'monday'" />

        <xsl:variable name="dayList" select="$locale/weekdays" />
        <div style="width:100%;text-align:left;margin:0;">
            <div style="float:left;width:49%;">
                <xsl:apply-templates select="$dayList/full/data[position() mod 2 != 0]" mode="weekDayPretty">
                    <xsl:with-param name="groupName" select="$groupName" />
                    <xsl:with-param name="selectedDay" select="'monday'" />
                </xsl:apply-templates>
            </div>
            <div style="float:left;width:49%;">
                <xsl:apply-templates select="$dayList/full/data[position() mod 2 = 0]" mode="weekDayPretty">
                    <xsl:with-param name="groupName" select="$groupName" />
                    <xsl:with-param name="selectedDay" select="'monday'" />
                </xsl:apply-templates>
            </div>
            <xsl:call-template name="clear"/>
        </div>
    </xsl:template>

    <xsl:template match="data" mode="weekDay">
        <xsl:param name="groupName" />
        <xsl:param name="selectedDay" select="'monday'" />
        <div class="outerLabel">
            <input type="radio" style="width:25px;" name="{$groupName}" class="deliverydata day" disabled="disabled" id="{$groupName}_{./@name}">
                <xsl:if test="./@name = $selectedDay">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
            </input>
            <label class="lableAfterInput" for="{$groupName}_{./@name}">  
                <xsl:value-of select="./value/node()" />
            </label>          
        </div>

    </xsl:template>
    
    <xsl:template match="data" mode="weekDayPretty">
        <xsl:param name="groupName" />
        <xsl:param name="selectedDay" select="'monday'" />
        <div class="">
            <xsl:choose>
                <xsl:when test="./@name = $selectedDay">
                    <input type="radio" name="{$groupName}" class="deliverydata day inp disabled" disabled="disabled" id="{$groupName}_{./@name}" checked="checked" />
                </xsl:when>
                <xsl:otherwise>
                    <input type="radio" name="{$groupName}" class="deliverydata day inp disabled" disabled="disabled" id="{$groupName}_{./@name}"  />
                </xsl:otherwise>
            </xsl:choose>
            <label class="lableAfterInput" for="{$groupName}_{./@name}">  
                <span class="label-pretty">
                    <xsl:value-of select="./value/node()" />
                </span>
            </label>          
        </div>

    </xsl:template>

</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
