<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:import href="common/pageCompiler.xsl" />
    <xsl:output method="html" indent="no" encoding="utf-8" />

    <xsl:template match="/">
        <xsl:call-template name="PageCompiler">
            <xsl:with-param name="app" select="'ufin.v2'" />
            <xsl:with-param name="page" select="'map'" />
		</xsl:call-template>
    </xsl:template>

</xsl:stylesheet>