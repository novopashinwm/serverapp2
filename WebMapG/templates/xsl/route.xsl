<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
	<xsl:import href="common.xsl" />
	<xsl:import href="uielements.xsl" />
	<xsl:output method="html" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//@template = 'displayRoutesInList'">
			<xsl:apply-templates select="." mode="displayRoutesInList" />
		</xsl:when>
		<xsl:when test="//@template = 'displayRouteOnMap'">
			<xsl:apply-templates select="." mode="displayRouteOnMap" />
		</xsl:when>
		<xsl:when test="//@template = 'calculatePoint'">
			<xsl:apply-templates select="." mode="calculatePoint" />
		</xsl:when>
	</xsl:choose>
</xsl:template>

<!-- AS: шаблон для диалога выбора отчета -->
<xsl:template name="saveRoute">
	<div id="routeSaveDlg" class="hide ontop" style="height:350px;">
		<div class="cols" style="margin:0; padding:0">
			<div class="col-left" style="margin:0; padding:0;border-bottom:1px solid #ccc;">
				<xsl:text>&#160;</xsl:text>
			</div>
			<div class="col-main2" style="margin:0; padding:0;border-bottom:1px solid #ccc;">
				<b><xsl:value-of select="$locale/data[@name='routeSaving']/value" /></b>
			</div>
			<div class="col-right2 right-side" style="margin:0; padding:0;border-bottom:1px solid #ccc;">
				<span class="closeWin" onclick="jQuery.closePopup('routeSaveDlg')"><span><xsl:text>&#160;x</xsl:text></span></span>
			</div>
		</div>
		<div class="clear"><xsl:text>&#160;</xsl:text></div>
		<!--xsl:call-template name="embeddedMessage">
			<xsl:with-param name="messageId" select="'routeEditMessage'" />
		</xsl:call-template-->
		<form>
			<fieldset class="noborder">
				<xsl:call-template name="routeNameEdit" />
				<!--div class="divider"><xsl:text> </xsl:text></div-->
				<xsl:call-template name="routeDescriptionEdit" />
				<div>
					<button type="button" class="defaultAction"><xsl:value-of select="$locale/data[@name='ok']/value" /></button>
				</div>
			</fieldset>
		</form>
	</div>
</xsl:template>

<!-- AS: название маршрута -->
<xsl:template name="routeNameEdit">
	<label class="mandatory block">
		<xsl:value-of select="concat($locale/data[@name='name']/value, ':')" /><sup class="required">*</sup>
		<input type="text" name="routename" id="routename" class="data default required" style="width:90%" title="{$locale/data[@name='required']/value}" />
	</label>
</xsl:template>

<!-- AS: описание маршрута -->
<xsl:template name="routeDescriptionEdit">
	<label class="mandatory block">
		<xsl:value-of select="concat($locale/data[@name='description']/value, ':')" />
	</label>
	<div>
		<textarea name="routedescription" id="routedescription" class="data" rows="5" style="width:90%"></textarea>
	</div>
</xsl:template>

<!-- AS: Список маршрутов -->
<xsl:template match="response" mode="displayRoutesInList">
	<response>
		<div>
			<xsl:if test="./message">
				<p style="margin:10px" class="message Warning">
					<xsl:call-template name="replace">
						<xsl:with-param name="str" select="./message/MessageTitle" />
					</xsl:call-template>
					<xsl:call-template name="replace">
						<xsl:with-param name="str" select="./message/MessageBody" />
					</xsl:call-template>
				</p>
			</xsl:if>
			<xsl:apply-templates select="./routes/LinesWeb" mode="description" />
		</div>
	</response>
</xsl:template>

<!-- AS: маршруты в списке -->
<xsl:template match="LinesWeb" mode="description">
	<div>
		<span class="toggle underlined" onclick="jQuery.getMyInfo({./LINE_ID})"><xsl:value-of select="./NAME" /></span>
		<div>
			<xsl:value-of select="./DESCRIPTION" />
		</div>
	</div>
</xsl:template>

<!-- AS: Оптимальный маршрут - на карте -->
<xsl:template match="response" mode="displayRouteOnMap">
	<response>
		<div style="z-index:20">
			<xsl:variable name="imageUrl">
				<xsl:value-of select="concat(./@ApplicationPath, '/img/point-b.png')" />
			</xsl:variable>
			<div style="position:absolute;left:{./sections/startRouteX}px; top:{./sections/startRouteY}px">
				<xsl:choose>
					<xsl:when test="./@browser = 'IE6'">
						<div>
							<xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$imageUrl" />', sizingMethod='scale')";width:21px; height:21px;</xsl:attribute>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<img src="{$imageUrl}" border="0" />
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div style="position:absolute;left:{./sections/finishRouteX}px; top:{./sections/finishRouteY}px">
				<xsl:choose>
					<xsl:when test="/response/@browser = 'IE6'">
						<div>
							<xsl:attribute name="style">filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<xsl:value-of select="$imageUrl" />', sizingMethod='scale')";width:21px; height:21px;</xsl:attribute>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<img src="{$imageUrl}" border="0" />
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<xsl:apply-templates select="./sections/points" mode="map" />
		</div>
		<splitter />
		<div>
			<div class="divider"><xsl:text> </xsl:text></div>
			<h2><xsl:value-of select="$locale/data[@name='routeSaving']/value" /></h2>
			<label class="mandatory block">
				<xsl:value-of select="concat($locale/data[@name='name']/value, ':')" />
				<xsl:call-template name="textEdit">
					<xsl:with-param name="id" select="'routename'" />
					<xsl:with-param name="required" select="'true'" />
				</xsl:call-template>
			</label>
			<label class="mandatory block">
				<xsl:value-of select="concat($locale/data[@name='description']/value, ':')" />
				<xsl:call-template name="textEdit">
					<xsl:with-param name="id" select="'routedescription'" />
					<xsl:with-param name="multiline" select="'true'" />
					<xsl:with-param name="rows" select="10" />
				</xsl:call-template>
			</label>
			<div>
				<button type="button" onclick="jQuery.route.save(event)"><xsl:value-of select="$locale/data[@name='saveRoute']/value" /></button>
			</div>
		</div>
	</response>
</xsl:template>

<!-- AS: отображение секций (отрезков) маршрута на карте -->
<xsl:template match="points" mode="map">
	<xsl:variable name="startX">
		<xsl:value-of select="/response/sections/minLongitudeW" />
	</xsl:variable>
	<xsl:variable name="startY">
		<xsl:value-of select="/response/sections/maxLatitudeW" />
	</xsl:variable>
	<xsl:choose>
		<xsl:when test="(contains(/response/@browser, 'IE'))">
			<xsl:variable name="path">
				<xsl:text>M </xsl:text><xsl:value-of select="./point[1]/@WX - $startX" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="./point[1]/@WY - $startY" /><xsl:text> L </xsl:text>
				<xsl:for-each select="./point[position() &gt; 1]">
					<xsl:value-of select="./@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./@WY - $startY" /><xsl:text> </xsl:text>
				</xsl:for-each>
				<xsl:text> E </xsl:text>
			</xsl:variable>
			<xsl:variable name="width">
				<xsl:value-of select="/response/sections/maxLongitudeW - /response/sections/minLongitudeW" />
			</xsl:variable>
			<xsl:variable name="height">
				<xsl:value-of select="/response/sections/minLatitudeW - /response/sections/maxLatitudeW" />
			</xsl:variable>
			<div style="position:absolute; left:{/response/sections/minLongitudeW}px; top:{/response/sections/maxLatitudeW}px; width:{$width}px; height:{$height}px; border:0px solid #0000ff;">
				<!--v:shape style="position:relative; left:0; top:0; width:{$width}px; height:{$height}px" coordorigin="0 0" coordsize="{$width} {$height}" path="{$path}" strokecolor="blue">
					<v:fill color="#6666ff" opacity="20%"/>
				</v:shape-->
				<!--script type="text/javascript">
					alert('<xsl:value-of select="$path" />');
				</script-->
				<!--xsl:value-of select="$width" /><xsl:text> : </xsl:text><xsl:value-of select="$height" /><br/-->
				<!--xsl:value-of select="$path" /-->
			</div>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="path">
				<xsl:text>M </xsl:text><xsl:value-of select="./point[1]/@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./point[1]/@WY - $startY" /><xsl:text> L </xsl:text>
				<xsl:for-each select="./point[position() &gt; 1]">
					<xsl:value-of select="./@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./@WY - $startY" /><xsl:text> </xsl:text>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="width">
				<xsl:value-of select="/response/sections/maxLongitudeW - /response/sections/minLongitudeW" />
			</xsl:variable>
			<xsl:variable name="height">
				<xsl:value-of select="/response/sections/minLatitudeW - /response/sections/maxLatitudeW" />
			</xsl:variable>
			<div style="position:absolute; left:{/response/sections/minLongitudeW}px; top:{/response/sections/maxLatitudeW}px; width:{$width}px; height:{$height}px; border:0px solid #0000ff;">
			    <svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$height}" viewBox="0 0 {$width} {$height}" style="position: relative; left:0; top:0">
					<path d="{$path}" stroke-linejoin="round" stroke-linecap="round" stroke="blue" fill-opacity="0.1" stroke-width="5px" fill="none" />
			    </svg>
			</div>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: отображение маршрута на карте -->
<xsl:template match="sections" mode="map">
	<xsl:param name="browser" select="''" />
	<xsl:variable name="startX">
		<xsl:value-of select="./minLongitudeW" />
	</xsl:variable>
	<xsl:variable name="startY">
		<xsl:value-of select="./maxLatitudeW" />
	</xsl:variable>

	<div style="position:absolute;left:{./startRouteX}px; top:{./startRouteY}px;">
		<xsl:call-template name="formatImage">
			<xsl:with-param name="browser" select="$browser" />
			<xsl:with-param name="width" select="20" />
			<xsl:with-param name="height" select="34" />
			<xsl:with-param name="src" select="concat(/response/@ApplicationPath, 'img/icong1.png')" />
		</xsl:call-template>
	</div>
	<div style="position:absolute;left:{./finishRouteX}px; top:{./finishRouteY}px;">
		<xsl:call-template name="formatImage">
			<xsl:with-param name="browser" select="$browser" />
			<xsl:with-param name="width" select="20" />
			<xsl:with-param name="height" select="34" />
			<xsl:with-param name="src" select="concat(/response/@ApplicationPath, 'img/icong2.png')" />
		</xsl:call-template>
	</div>
	<xsl:choose>
		<xsl:when test="(contains($browser, 'IE'))">
			<xsl:variable name="path">
				<xsl:for-each select="./points">
					<xsl:text>M </xsl:text><xsl:value-of select="./point[1]/@WX - $startX" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="./point[1]/@WY - $startY" /><xsl:text> L </xsl:text>
					<xsl:for-each select="./point[position() &gt; 1]">
						<xsl:value-of select="./@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./@WY - $startY" /><xsl:text> </xsl:text>
					</xsl:for-each>
					<xsl:text> E </xsl:text>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="width">
				<xsl:value-of select="./maxLongitudeW - ./minLongitudeW" />
			</xsl:variable>
			<xsl:variable name="height">
				<xsl:value-of select="./minLatitudeW - ./maxLatitudeW" />
			</xsl:variable>
			<div style="position:absolute; left:{./minLongitudeW}px; top:{./maxLatitudeW}px; width:{$width}px; height:{$height}px; border:0px solid #0000ff;">
				<v:shape style="position:absolute; left:0; top:0; width:{$width}px; height:{$height}px" coordorigin="0 0" coordsize="{$width} {$height}" path="{$path}" strokecolor="#6696FF" strokeweight="5px">
					<v:fill color="#6696FF" opacity="0%"/>
				</v:shape>
				<!--script type="text/javascript">
					alert('<xsl:value-of select="$path" />');
				</script-->
				<!--xsl:value-of select="$width" /><xsl:text> : </xsl:text><xsl:value-of select="$height" /><br/-->
				<!--xsl:value-of select="$path" /-->
			</div>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="path">
				<xsl:for-each select="./points">
					<xsl:text>M </xsl:text><xsl:value-of select="./point[1]/@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./point[1]/@WY - $startY" /><xsl:text> L </xsl:text>
					<xsl:for-each select="./point[position() &gt; 1]">
						<xsl:value-of select="./@WX - $startX" /><xsl:text> </xsl:text><xsl:value-of select="./@WY - $startY" /><xsl:text> </xsl:text>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="width">
				<xsl:value-of select="./maxLongitudeW - ./minLongitudeW" />
			</xsl:variable>
			<xsl:variable name="height">
				<xsl:value-of select="./minLatitudeW - ./maxLatitudeW" />
			</xsl:variable>
			<div style="position:absolute; left:{./minLongitudeW}px; top:{./maxLatitudeW}px; width:{$width + 5}px; height:{$height + 5}px; border:0px solid #0000ff;">
			    <svg xmlns="http://www.w3.org/2000/svg" width="{$width + 5}" height="{$height + 5}" viewBox="-5 -5 {$width + 5} {$height + 5}" style="position: absolute; left:0; top:0">
					<path d="{$path}" stroke-linejoin="round" stroke-linecap="round" stroke="#6696FF" fill-opacity="0.1" stroke-width="5px" fill="none" />
			    </svg>
			</div>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- AS: проложить оптимальный маршрут - контрол -->
<xsl:template name="addBestRoute">
	<xsl:variable name="imageUrl">
		<xsl:value-of select="concat(/response/@ApplicationPath, '/img/point-b.png')" />
	</xsl:variable>
	<span xmlns="http://www.w3.org/1999/xhtml" class="toggle" style="margin-left:5px;">
		<span onclick="jQuery.route.onStartSetPoint(event)" class="underlined" id="addBestRouteToggle"><xsl:value-of select="$locale/data[@name='bestRoute']/value" /></span>
		<div style="display:none;position:absolute" id="routeSetPoint">
			<xsl:call-template name="formatImage">
				<xsl:with-param name="browser" select="/response/@browser" />
				<xsl:with-param name="width" select="'21'" />
				<xsl:with-param name="height" select="'21'" />
				<xsl:with-param name="src" select="$imageUrl" />
			</xsl:call-template>
		</div>
		<!--xsl:call-template name="newGeopointInfoEdit" /-->
	</span>
</xsl:template>


<!-- AS: получение истории -->
<xsl:template match="response" mode="calculatePoint">
	<response>
			<!-- AS: адрес. Да, блять, вот так неровно -->
			<xsl:apply-templates select="./point[1]/address" mode="list" />
			<splitter />
			<!-- AS: сама точка -->
			<xsl:apply-templates select="./point" mode="map" />
	</response>
</xsl:template>

<xsl:template match="address" mode="list">
	<div>
		<b>
			<xsl:choose>
				<xsl:when test="./@type = 'from'">
					<xsl:value-of select="$locale/data[@name='routeFrom']/value" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$locale/data[@name='routeTo']/value" />
				</xsl:otherwise>
			</xsl:choose>
		:</b><xsl:text> </xsl:text><xsl:value-of select="." />
	</div>
</xsl:template>

<xsl:template match="point" mode="map">
	<xsl:copy-of select="." />
</xsl:template>

</xsl:stylesheet>


<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->