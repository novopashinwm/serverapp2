﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:import href="common.xsl" />
    <xsl:import href="login.xsl" />
    <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />
    <!--xsl:output method="html" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="no" omit-xml-declaration="yes" standalone="omit" /-->

    <xsl:param name="date" />

    <xsl:template match="/">
        <xsl:variable name="selectedPage">
            <xsl:apply-templates select="." mode="selectedPage" />
        </xsl:variable>
        <html>
            <head>
                <xsl:apply-templates select="response" mode="headContent" />
                <xsl:apply-templates select="response" mode="pageSpecificHeadContent" />
            </head>
            <body class="registration {/response/@template} {/response/@logo}">
                <xsl:apply-templates select="response" mode="common-page-header" />
            
                <div id="pageContent" class="content b-content">
                    <div class="cols">
                        <div class="col-left3">
                            <xsl:apply-templates select="response" mode="loginForm" />
                            <div class="row">
                                <xsl:call-template name="briefDescriptionTitle">
                                    <xsl:with-param name="selectedPage" select="$selectedPage" />
                                </xsl:call-template>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="main-content">
                            <xsl:apply-templates select="/response" mode="content-head" />
                            <xsl:apply-templates select="/response/message" />
                            <xsl:apply-templates select="/response" />
                        </div>
                    </div>
                    <div class="clear"></div>
                </div> 
                <xsl:apply-templates select="response" mode="pageFooter" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="response" mode="common-page-header">
        <xsl:call-template name="common-page-header" />
    </xsl:template>

    <xsl:template match="response[@template = 'start']">
    </xsl:template>

    <xsl:template match="response[@template = 'email-has-been-sent']">
        <p />
        <p>
            <xsl:value-of select="$locale/data[@name='email-with-confirmation-has-been-sent-to']/value" />
            <xsl:text> </xsl:text>
            <b>
                <xsl:value-of select="./@email"/>
            </b>
        </p>
    </xsl:template>

    <xsl:template match="response[@template = 'email-with-password-recovery-has-been-sent']">
        <p />
        <p>
            <xsl:value-of select="$locale/data[@name='email-with-password-recovery-has-been-sent-to']/value" />
            <xsl:text> </xsl:text>
            <b>
                <xsl:value-of select="./@email"/>
            </b>
        </p>
    </xsl:template>

    <xsl:template match="response[@template = 'email-has-not-been-sent']">
        <p />
        <p class="alert alert-danger">
            <xsl:value-of select="./@email"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$locale/data[@name='email-cannot-be-used-for-password-restore']/value" />
        </p>
    </xsl:template>

    <xsl:template match="response[@template = 'new-tracker-adding']">
        <form id="newTrackerForm" action="{//@ApplicationPath}/register.aspx" method="POST">
            <input type="hidden" name="a" value="add-new-tracker" />
            <input type="hidden" name="confirmation-key">
                <xsl:attribute name="value">
                    <xsl:value-of select="./@confirmation-key"/>
                </xsl:attribute>
            </input>
            <fieldset>
                <table>
                    <tr>
                        <td>
                            <label>
                                <xsl:value-of select="$locale/data[@name='trackerNumber']/value" /> (IMEI)
                            </label>
                        </td>
                        <td>
                            <input type="text" name="number">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="@number"/>
                                </xsl:attribute>
                            </input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <xsl:value-of select="$locale/data[@name='controlCode']/value" />
                            </label>
                        </td>
                        <td>
                            <input type="text" name="control-code">
                                <xsl:attribute name="value">
                                    <xsl:value-of select="@control-code"/>
                                </xsl:attribute>
                            </input>
                        </td>
                    </tr>
                    <xsl:if test="./@CaptchaEnabled = 'true'">
                        <tr>
                            <td>
                                <label>
                                    <xsl:value-of select="$locale/data[@name='captcha']/value" />
                                </label>
                            </td>
                            <td>
                                <div id="captcha-div"></div>
                            </td>
                        </tr>
                    </xsl:if>
                    <tr>
                        <td />
                        <td>
                            <button type="submit">
                                <xsl:value-of select="$locale/data[@name='register']/value" />
                            </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </xsl:template>

    <xsl:template match="response" mode="content-head">
        <xsl:if test="@title">
            <h1>
                <xsl:value-of select="@title"/>
            </h1>
        </xsl:if>
    </xsl:template>

    <xsl:template match="message">
        <p class="alert alert-danger">
            <xsl:value-of select="MessageBody"/>
        </p>
    </xsl:template>

    <xsl:template match="response[@template = 'new-tracker-adding']" mode="pageSpecificHeadContent">
        <script type="text/javascript" src="{/response/@ApplicationPath}/includes/scripts/register.js"></script>
        <xsl:if test="./@CaptchaEnabled = 'true'">
            <script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
            <script type="text/javascript">
                $(document).ready(function() { $.register.initCaptcha(); });
            </script>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="response" mode="pageSpecificHeadContent">
    </xsl:template>

    <xsl:template match="response[@template = 'password-recovery-form']">
        <div class="promoDescription">
            <h2>
                <xsl:value-of select="$locale/data[@name='reset-password-by-email']/value" />
            </h2>
            <xsl:apply-templates select="." mode="emailForm">
                <xsl:with-param name="action-url" select="'PasswordRecovery.aspx'" />
            </xsl:apply-templates>
        </div>
        <hr />
        <div class="promoDescription">
            <h2>
                <xsl:value-of select="$locale/data[@name='by-sms-to-nika-admin']/value" />
            </h2>
            <br />  
            <p>
                <xsl:value-of select="$locale/data[@name='send-login-to-reset-password']/value" />
            </p>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="emailForm">
        <xsl:param name="action-url" />
        <form id="emailForm" action="{//@ApplicationPath}/{$action-url}" method="POST">
            <fieldset>
                <input type="hidden" name="a" value="send-confirmation-email" />
                <p />
                <p>
                    <xsl:value-of select="$locale/data[@name='enter-email-and-press-send']/value" />
                </p>
                <p>
                    <label>
                        <xsl:value-of select="$locale/data[@name='emailFull']/value" />:
                    </label>
                    <input type="email" name="email" required="required" class="inp">
                        <xsl:attribute name="value">
                            <xsl:value-of select="./@email"/>
                        </xsl:attribute>
                    </input>
                    <button type="submit" class="btn">
                        <xsl:value-of select="$locale/data[@name='send']/value" />
                    </button>
                </p>
            </fieldset>
        </form>
    </xsl:template>

</xsl:stylesheet>
