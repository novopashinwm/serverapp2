<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
        <!ENTITY nbsp "&#160;">
        <!ENTITY hellip "&#8230;">
        ]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt">
    <xsl:import href="uielements.xsl" />
    <!--xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" /-->
    <xsl:output method="xhtml" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />

    <!-- AS: переменная, определяющая текущий язык. Переменную зовут locale -->
    <xsl:variable name="localMessageFile">
        <xsl:text>./res/messages</xsl:text>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="/response/@language" />
        <xsl:text>.xml</xsl:text>
    </xsl:variable>
    <xsl:variable name="locale" select="document($localMessageFile)/root" />

    <xsl:variable name="resourceStringsFile">
        <xsl:text>./res/strings.xml</xsl:text>
    </xsl:variable>
    <xsl:variable name="resourceStrings" select="document($resourceStringsFile)/root" />

    <!-- AS: JS-файл с  сообщениями, выводящимися на клиенте без применения ajax'a -->
    <xsl:variable name="localJsName">
        <xsl:value-of select="/response/@baseWebAppPath"/>
        <xsl:text>/js/lang/messages</xsl:text>
        <xsl:choose>
            <xsl:when test="contains(/response/@language, 'ru-') != 'true'">
                <xsl:text>.</xsl:text>
                <xsl:value-of select="/response/@language" />
            </xsl:when>
        </xsl:choose>
        <xsl:text>.js</xsl:text>
    </xsl:variable>

    <!-- AS: CSS со стилизацией лого -->
    <xsl:variable name="localLogoCSS">
        <xsl:choose>
            <xsl:when test="(/response/@logo != 'XXX')">
                <xsl:value-of select="/response/@baseWebAppPath" />
                <xsl:text>/css/</xsl:text>
                <xsl:value-of select="/response/@logo" />
                <xsl:choose>
                    <xsl:when test="contains(/response/@language, 'ru-') != 'true'">
                        <xsl:text>.</xsl:text>
                        <xsl:value-of select="/response/@language" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>.ru</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>.css</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/response/@baseWebAppPath" />
                <xsl:text>/css/logo</xsl:text>
                <xsl:choose>
                    <xsl:when test="contains(/response/@language, 'ru-') != 'true'">
                        <xsl:text>.</xsl:text>
                        <xsl:value-of select="/response/@language" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>.ru</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>.css</xsl:text>
            </xsl:otherwise>
        </xsl:choose>


    </xsl:variable>

    <xsl:template match="response" mode="header">
        <head>
            <xsl:apply-templates select="." mode="headContent" />
        </head>
    </xsl:template>

    <xsl:template match="response" mode="headContent">
        <xsl:param name="title" select="./@title" />

        <xsl:call-template name="titleConstructor">
            <xsl:with-param name="title" select="$title"/>
        </xsl:call-template>
        <!-- Было бы логично добавить в шаблон mode=header параметр для подключения дополнительных скриптов и стилей,
        но это приводит к тому, что <script></script> превращается в <script /> и браузер не подключает скрипты. -->
        <meta name="description" content="Онлайн мониторинг и позиционирование транспортых средств" />
        <meta name="keywords" content="мониторинг, транспортных, средств, позиционирование, карта, местности, контроль, местонахождения, объектов, маршрут, движения" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />


        <!--
        Файлы включены в единую таблицу стилей ufin.css и ufin_light.css

        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/global.css"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/print.css" media="print"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/menu.css"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/themes/jquery-ui-my.css"/>
        -->


        <link rel="shortcut icon" href="{./@baseWebAppPath}/{./@favicon}"/>


        <xsl:call-template name="link-css" />

        <!--jstorage-->

        <script type="text/javascript" src="{$localJsName}"></script>

        <script type="text/javascript" >
            var require = { waitSeconds : 59 };
        </script>

        <xsl:apply-templates select="." mode="commonVariables" />



        <!-- AS: для IE, чтобы он мог отображать VML -->
        <style type="text/css">
            v\:* { behavior:url(#default#VML); display:inline-block }
            o\:* { behavior:url(#default#VML); }
        </style>
    </xsl:template>


    <xsl:template name="titleConstructor">
        <xsl:param name="title"></xsl:param>
        <xsl:variable name="tabsHeader" select="/response/@template"/>

        <title>
            <xsl:call-template name="localTitle"/>
            <xsl:if test="(/response/@language='ru-RU')  or (/response/@language='en-US')">
                <xsl:choose>
                    <xsl:when test="$tabsHeader = 'journal'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='journal']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'mapPageFull'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='map']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'reports'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='reports']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'objectInfo'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='objects']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'userSettings'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='settings']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'adminPage'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='admin']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'abonentPage'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='abonent']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                    <xsl:when test="$tabsHeader = 'picturePage'">
                        <xsl:value-of select="$locale/data[@name='titles']/value[@name='pictures']"/>
                        <xsl:text> / </xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:value-of select="$title"/>
            </xsl:if>

        </title>
    </xsl:template>

    <xsl:template match="response" mode="header2">
        <meta name="description" content="Онлайн мониторинг и позиционирование транспортых средств"/>
        <meta name="keywords" content="мониторинг, транспортных, средств, позиционирование, карта, местности, контроль, местонахождения, объектов, маршрут, движения"/>

        <!--
        Файлы включены в единую таблицу стилей ufin.css и ufin_light.css

        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/global.css"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/print.css" media="print"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/menu.css"/>
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/themes/smoothness/jquery-ui-1.10.3.custom.min.css"/>
        -->

        <xsl:call-template name="link-css" />
        <link rel="shortcut icon" href="{./@baseWebAppPath}/{./@favicon}"/>

        <script type="text/javascript" >
            var require = { waitSeconds : 60 };
        </script>

        <xsl:apply-templates select="." mode="commonVariables" />
        <!-- AS: поебень для IE, чтобы он мог отображать VML -->
        <style type="text/css">
            v\: * { behavior:url(#default#VML); display:inline-block }
            o\:* { behavior:url(#default#VML); }
        </style>

    </xsl:template>

    <xsl:template match="response" mode="simpleHeader">
        <meta name="description" content="Онлайн мониторинг и позиционирование транспортых средств"/>
        <meta name="keywords" content="мониторинг, транспортных, средств, позиционирование, карта, местности, контроль, местонахождения, объектов, маршрут, движения"/>

        <!--
        Файлы включены в единую таблицу стилей ufin.css и ufin_light.css
        <link type="text/css" rel="stylesheet" href="{./@baseWebAppPath}/css/global.css"/>
        -->
    </xsl:template>


    <xsl:template match="response" mode="commonVariables">

        <xsl:call-template name="applicationPath" />

        <script type="text/javascript">
            var serverName = "<xsl:value-of select="./@ServerName"/>";
            var departmentCountry = "<xsl:value-of select="./@DepartmentCountryName"/>";
            var currentMapName = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapinfoShort"/>";
            var currentMapShortName = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapShortName"/>";
            var currentMapGuid = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapGuid"/>";
            var currentCulture = "<xsl:value-of select="substring-before(./@language, '-')" />";
            var useDialer = ('<xsl:value-of select="./@useDialer" />' == 'true' ? true : false);
            if('<xsl:value-of select="./@loggedUser" />' == 'guest'){useDialer = false;}
            var loggedUser = "<xsl:value-of select="./@loggedUser"/>";
            var loggedUserId = parseInt('<xsl:value-of select="./@loggedUserId"/>');
            var serverDate = "<xsl:value-of select="./@serverDate" />";
            var defaultVehicleIcon = applicationPath + "<xsl:value-of select="./@DefaultVehicleIcon"/>";
            var sessionStateTimeout = <xsl:value-of select="./@sessionStateTimeout"/>;
            var UFIN = UFIN || {};
            UFIN.promoUrl = "<xsl:value-of select="$pathAbout"/>";

            UFIN.language = '';
            UFIN.customerType = '<xsl:value-of select="/response/Departments/Department/Type"/>';

            // Имя пользователя, от лица которого работает администратор.
            UFIN.customerName = '<xsl:value-of select="/response/Departments/Department/Name"/>';
            UFIN.customerCountry = '<xsl:value-of select="/response/Departments/Department/Country"/>';

            UFIN.appClientDownloadLink = '<xsl:value-of select="/response/@appClientDownloadLink"/>';
            UFIN.operatorSystemRights = <xsl:value-of select="/response/@OperatorSystemRights"/>;

            // Ключи от Google Maps API и Yandex Maps API
            UFIN.googleApiKey = '<xsl:value-of select="./@googleMapsScriptAuth"/>';
            UFIN.yandexApiKey = '<xsl:value-of select="/response/@yandexApiKey" />';

            UFIN.maps = <xsl:value-of select="/response/@maps" />
            UFIN.defMap = '<xsl:value-of select="/response/@defaultMap" />';
            UFIN.mapUrl = '<xsl:value-of select="./mapdescriptions/MapDescription[current = 'true']/url" />';
            UFIN.mapMode = 'map';
            UFIN.playSound = '<xsl:value-of select="/response/@playSound" />';
            UFIN.updateDataOnFocus = <xsl:value-of select="/response/@updateDataOnFocus" />

            <xsl:if test="/response/@PushNotifications.Enabled = 'true'">
                UFIN.signalR = true;
            </xsl:if>

            <xsl:if test="/response/@WebSSO = 'true'">
                UFIN.websso = true;
            </xsl:if>

            <xsl:choose>
                <xsl:when test="(/response/@language='ru-RU')">
                    UFIN.language = 'ru';
                </xsl:when>
                <xsl:otherwise>
                    UFIN.language = 'en';
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="(/response/@ForcedToChangePassword='true')">
                    window.selectedTabAfterPageLoad = 'settingsPassword'
                </xsl:when>
                <xsl:when test="(/response/@ForcedToSetEmail='true')">
                    window.selectedTabAfterPageLoad = 'settingsEmail'
                </xsl:when>
                <xsl:when test="(/response/@ForcedToSetPhone='true')">
                    window.selectedTabAfterPageLoad = 'settingsPhone'
                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
            </xsl:choose>

        </script>

    </xsl:template>

    <xsl:template name="commonBodyIncludes">
        <script type="text/javascript">
            var UFIN = UFIN || {};
            UFIN.operatorRights = {
            <xsl:for-each select="/response/systemRights/right">
                <xsl:if test="position() != 1">,</xsl:if>
                <xsl:value-of select="."/>:true
            </xsl:for-each>
            }
        </script>
    </xsl:template>

    <xsl:template name="loginDialog">
        <div title="Время сессии истекло" id="loginDialog" class="dialog" style="min-height:300px">
            <div style="padding-bottom:5px;">
                <span>
                    Введите пароль<br/>
                </span>
                <a href="{./@ApplicationPath}/default.aspx?a=logout">или зайдите под другим логином.</a>
            </div>

            <div id="loginDialogMsg" class="ui-widget" style="display:none;">
                <div class="ui-state-error ui-corner-all" style="padding: .2em .7em .2em;">
                    <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    <span id="loginDialogMsgText"></span>
                </div>
            </div>
            <fieldset class="noborder">
                <label for ="txtLogin">
                    <xsl:value-of select="$locale/data[@name='Login']/value" />
                </label>
                <input type="text" name="txtLogin" id="txtLogin" disabled="disabled" class="inp" />

                <label for="txtPassword">
                    <xsl:value-of select="$locale/data[@name='Password']/value" />
                </label>
                <input type="password" name="txtPassword" id="txtPassword" class="inp" />

                <div class="right-column">
                    <button id="dialogLoginBtn" type="button" style="width:70px" class="btn" onclick ="dialogLogin();">
                        <xsl:value-of select="$locale/data[@name='GetIn']/value"/>
                    </button>
                </div>
                <input type="hidden" name="loginDialogResult" id="loginDialogResult" value="false" />
            </fieldset>
        </div>

        <script type="text/javascript">
            var dlg = jQuery("#loginDialog");
            if (dlg) {
                dlg.dialog({ modal: true,
                    bgiframe: true,
                    stack: true,
                    position: 'center',
                    closeOnEscape: false,
                    draggable: true,
                    resizable: false,
                    autoOpen: false,
                    width: 330,
                    beforeclose: function(event, ui) { return jQuery("#loginDialogResult").val()=="true"; }
                });
            }

            jQuery("#txtPassword").keydown(function(event){
                if (event.keyCode==13) {
                    dialogLogin();
                }
            });

        </script>
    </xsl:template>

    <xsl:template name="common-head-support">
        <xsl:choose>
            <xsl:when test="contains(/response/@language,'en-US')"></xsl:when>
            <xsl:otherwise>
                <div class="hlp">
                    <span  class="basket">
                        <a href="?page=calculator">Рассчитать стоимость</a>
                    </span>&#160;
                    <br/>
                    <span class="quest">
                        <a href="?page=question-support">Вопрос техподдержке</a>
                    </span>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="common-head-mobile">
        <span id="mobileVersionLink" >
            <xsl:attribute name="style">
                <xsl:choose>
                    <xsl:when test="not(//./Person/Id) and contains(/response/@location, 'gu-')">
                        <xsl:text>left: auto;right: 124px;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <a href="{//@ApplicationPath}/mobile">
                <xsl:value-of select="$locale/data[@name='mobileVersion']/value" />
            </a>
        </span>
    </xsl:template>

    <xsl:template name="common-page-header">
        <xsl:param name="additional-main-content" />
        <div class="pageHeader header" id="loginHeader">
            <xsl:call-template name="navTop"></xsl:call-template>
            <xsl:if test="./logo != '' or ./@showAppName = 'true'">
                <div class="logos">
                    <div class="png main_logo">
                        <xsl:choose>

                            <xsl:when test="./@logo = 'logoMTS'">
                                <a id="app-old-logo" href="" target="_blank">
                                    <xsl:attribute name="href">

                                                <xsl:text>http://ufin.online</xsl:text>


                                    </xsl:attribute>
                                    <img height="50"
                                         alt="{$locale/data[@name='mts']/value}"
                                         title="{$locale/data[@name='mts']/value}">
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="./@baseWebAppPath"/>
                                            <xsl:choose>
                                                <xsl:when test="contains(/response/@language, 'ru-') = 'true'">
                                                    <xsl:text>/img/logo.gif</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:text>/img/application/service-provider-logo-en-US.gif</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                    </img>
                                </a>
                            </xsl:when>
                        </xsl:choose>
                    </div>
                    <xsl:if test="./@showAppName = 'true'">
                        <div class="project">
                            <h1 class="applicationName">
                                <xsl:attribute name="style">
                                    <xsl:choose>
                                        <xsl:when test="not(//./Person/Id) and contains(/response/@IsUserGuest, 'false') and contains(/response/@location, 'gu-')">
                                            <xsl:text>left: auto;right: 217px;</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise></xsl:otherwise>
                                    </xsl:choose>
                                </xsl:attribute>
                                <a href="{./@MainPageUrl}" target="_blank">
                                    <img>
                                        <xsl:attribute name="alt">
                                            <xsl:value-of select="/response/@applicationShortName" />
                                        </xsl:attribute>
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="./@baseWebAppPath"/>
                                            <xsl:text>/img/application/</xsl:text>
                                            <xsl:choose>
                                                <xsl:when test="contains(/response/@language, 'en-')">
                                                    <xsl:text>app_name-en.png</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:text>app_name-ru.png</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                    </img>
                                </a>
                            </h1>
                            <xsl:call-template name="common-head-mobile" />
                        </div>
                        <xsl:choose>
                            <xsl:when test="contains(/response/@language,'en-US')"></xsl:when>
                            <xsl:otherwise>
                                <div class="hlp">
                                    <span  class="basket">
                                        <a href="?page=calculator">Рассчитать стоимость</a>
                                    </span>&#160;<br/>
                                    <span class="quest">
                                        <a href="?page=question-support">Вопрос техподдержке</a>
                                    </span>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                        <div class="home_logo">
                            <a id="content-provider-logo" href="{./@ApplicationPath}" target="_blank">
                                <img src="{./@baseWebAppPath}/img/less/logo_ufin_ru.png">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="./@baseWebAppPath"/>
                                        <xsl:text>/img/less/</xsl:text>
                                        <xsl:choose>
                                            <xsl:when test="contains(/response/@language, 'ru-') = 'true'">
                                                <xsl:text>logo_ufin_ru.png</xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>header_en.png</xsl:text>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </img>
                            </a>
                        </div>
                    </xsl:if>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="pageHeader">
        <xsl:param name="selectedTab" select="'map'" />
        <xsl:param name="additional-main-content" />

        <xsl:call-template name="header">
            <xsl:with-param name="additional-main-content" />
            <xsl:with-param name="selectedTab" select="$selectedTab" />
        </xsl:call-template>


    </xsl:template>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="//@template = 'map'">
                <xsl:apply-templates select="." mode="map" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="response" mode="map">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <xsl:apply-templates select="." mode="header" />
            <body>

                <xsl:if test="/response/@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="/response/@WebSSO">
                    <xsl:attribute name="class">websso</xsl:attribute>
                </xsl:if>
                <xsl:call-template name="websso"/>
                <xsl:apply-templates select="." mode="pageHeader" />
                <div id="left-col">
                    left
                </div>
                <div id="center-col">
                    center
                </div>
                <div id="right-col">
                    right
                </div>


            </body>
        </html>
    </xsl:template>


    <!-- AS: замена символов в строке -->
    <!--xsl:template name="replace" match="text()" mode="replace"-->
    <xsl:template name="replace">
        <xsl:param name="str" select="." />
        <xsl:param name="search-for" select="'&#xA;'" />
        <xsl:param name="replace-with">
            <xsl:element name="br" />
            <xsl:text>&#xA;</xsl:text>
        </xsl:param>
        <xsl:choose>
            <xsl:when test="contains($str, $search-for)">
                <xsl:value-of select="substring-before($str, $search-for)" />
                <xsl:copy-of select="$replace-with" />
                <xsl:call-template name="replace">
                    <xsl:with-param name="str" select="substring-after($str, $search-for)" />
                    <xsl:with-param name="search-for" select="$search-for" />
                    <xsl:with-param name="replace-with" select="$replace-with " />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$str" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- AS: название текущей карты -->
    <xsl:template match="MapDescription" mode="mapinfo">
        <span>
            <span>

                <xsl:apply-templates select="." mode="mapinfoShort" />
            </span>
        </span>
    </xsl:template>

    <xsl:template match="MapDescription" mode="mapinfoShort">
        <xsl:choose>
            <xsl:when test="contains(/response/@language, 'ru-')">
                <xsl:value-of select="./names/ru" />
            </xsl:when>
            <xsl:when test="contains(/response/@language, 'en-')">
                <xsl:value-of select="./names/en" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="MapDescription" mode="mapShortName">
        <xsl:value-of select="./shortname" />
    </xsl:template>

    <xsl:template match="MapDescription" mode="mapGuid">
        <xsl:value-of select="./guid" />
    </xsl:template>


    <!-- AS: Отображение основных "закладок" -->
    <xsl:template name="mainTabs">
        <xsl:param name="selected" select="'map'" />
        <ul class="mainMenu" id="mainMenu">
            <xsl:if test="/response/systemRights[./right = 'SecurityMapGis']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='monitoring']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'map'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/map.aspx">
                        <xsl:value-of select="$locale/data[@name='monitoring']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="/response/systemRights[./right = 'ReportsAccess']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='reports']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'report'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/Report.aspx">
                        <xsl:value-of select="$locale/data[@name='reports']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="/response/systemRights[./right = 'VehicleAccess']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='objects']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'objects'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/objects.aspx">
                        <xsl:value-of select="$locale/data[@name='TrackingObjectsShort']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="./@loggedUser != 'guest'">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='settings']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'settings'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/settings.aspx">
                        <xsl:value-of select="$locale/data[@name='userSettings']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="(./@showAdminPage = 'True')">

                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='administration']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'admin'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/admin.aspx">
                        <xsl:value-of select="$locale/data[@name='administration']/value" />
                    </a>
                </li>

            </xsl:if>

        </ul>

    </xsl:template>

    <xsl:template match="response" mode="underConstruction">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <xsl:apply-templates select="." mode="header" />
            <body id="underConstruction">
                <xsl:if test="./@color-schema = 'blue' ">
                    <xsl:attribute name="class">
                        blue-schema
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="websso"/>
                <xsl:apply-templates select="." mode="pageHeader" />
                <p>В разработке</p>
                <xsl:call-template name="requireJS" />

                <xsl:apply-templates select="." mode="pageFooter" />

            </body>
        </html>
    </xsl:template>

    <xsl:template name="waitDialog">
        <div id="fadeAll" class="ontop">
        </div>
        <div class="waitDialog ontop hide" id="waitDialog">
            <span class="wait">
                <xsl:value-of select="$locale/data[@name='loading']/value" />
            </span>
        </div>
    </xsl:template>

    <!-- AS: шаблон, содержащий контрол обновления и контрол поиска объекта по адресу -->
    <xsl:template match="response" mode="topControls">

    </xsl:template>

    <!-- AS: шаблон с контролом обновления -->
    <xsl:template match="response" mode="refresh">
        <div id="refreshControl" class="screen">

            <!--label class="mandatory"-->
            <xsl:value-of select="$locale/data[@name='refreshInterval']/value" />:
            <xsl:text>&#160;</xsl:text>
            <xsl:call-template name="refreshInterval" />
            <!--/label-->
        </div>
    </xsl:template>

    <!-- AS: шаблон контрола поиска по адресу -->
    <xsl:template match="response" mode="addressSearch">
        <!--div class="block"-->
        <span class="nowrap">
            <input type="text" id="addressSearchBar" value="{./@addressSearchVal}" />
            <xsl:text>&#160;</xsl:text>
            <button onclick="jQuery.searchAddress.search()" type="button" title="{$locale/data[@name='findAddress']/value}">
                <xsl:value-of select="$locale/data[@name='find']/value" />
            </button>
        </span>
        <!--/div-->
    </xsl:template>

    <!-- AS: Шаблон контрола вывода "похожих" на введенный адресов -->
    <xsl:template name="addressList">
        <div class="popup hide ontop" id="addressListDialog">
            <div class="cols2" style="margin:0; padding:0">
                <div class="col-left" style="margin:0; padding:0; height:16px;">
                    <xsl:text>&#160;</xsl:text>
                </div>
                <div class="col-main2" style="margin:0; padding:0; height:16px;">
                    <b>
                        <xsl:value-of select="$locale/data[@name='addressSearch']/value" />
                    </b>
                </div>
                <div class="col-right2 right-side" style="margin:0; padding:0; height:16px;">
                    <span class="closeWin" onclick="jQuery.closePopup('addressListDialog')">
                        <span>
                            <xsl:text>&#160;x</xsl:text>
                        </span>
                    </span>
                </div>
            </div>
            <div class="clear" style="border-bottom:1px solid #ccc;">
                <xsl:text>&#160;</xsl:text>
            </div>
            <div id="numberFoundAddress">
            </div>
            <div class="divider">
                <xsl:text> </xsl:text>
            </div>
            <div class="popupContent">
            </div>
            <!--div class="divider"><xsl:text>&#160;</xsl:text></div>
            <div>
                    <button class="closeDlg" onclick="jQuery.closeDlg('addressListDialog', 'fast')"><xsl:value-of select="$locale/data[@name='cancel']/value" /></button>
            </div-->
        </div>
    </xsl:template>

    <!-- AS: выпадающий список интервала обновления -->
    <xsl:template name="refreshInterval">
        <xsl:param name="forHistory" select="'false'" />
        <xsl:param name="forParking" select="'false'" />

        <xsl:param name="controlId" select="'refresh'" />

        <xsl:variable name="seconds">
            <xsl:value-of select="$locale/data[@name='SecondsTitle']/value" />
        </xsl:variable>
        <select id="{$controlId}" name="{$controlId}" class="data inp">
            <option value="0">
                <xsl:value-of select="$locale/data[@name='notSet']/value" />
            </option>
            <xsl:if test="$forHistory = 'false'">
                <option value="10">
                    10 <xsl:value-of select="$seconds" />
                </option>
                <option value="30">
                    <xsl:if test="$forHistory != 'true'">
                        <xsl:attribute name="selected">
                            <xsl:text>selected</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    30 <xsl:value-of select="$seconds" />
                </option>
            </xsl:if>
            <option value="60">
                <xsl:if test="$forHistory = 'true'">
                    <xsl:attribute name="selected">
                        <xsl:text>selected</xsl:text>
                    </xsl:attribute>
                </xsl:if>
                60 <xsl:value-of select="$seconds" />
            </option>
            <xsl:if test="$forHistory = 'true'">
                <option value="120">
                    <xsl:value-of select="$locale/data[@name='2minutes']/value" />
                </option>
                <option value="300">
                    <xsl:value-of select="$locale/data[@name='5minutes']/value" />
                </option>
                <option value="600">
                    <xsl:value-of select="$locale/data[@name='10minutes']/value" />
                </option>
                <option value="1800">
                    <xsl:value-of select="$locale/data[@name='30minutes']/value" />
                </option>
                <option value="3600">
                    <xsl:value-of select="$locale/data[@name='1hour']/value" />
                </option>
                <option value="21600">
                    <xsl:value-of select="$locale/data[@name='6hours']/value" />
                </option>
                <option value="43200">
                    <xsl:value-of select="$locale/data[@name='12hours']/value" />
                </option>
            </xsl:if>
            <xsl:if test="$forParking = 'true'">
                <option value="300">
                    <xsl:value-of select="$locale/data[@name='5minutes']/value" />
                </option>
                <option value="600">
                    <xsl:value-of select="$locale/data[@name='10minutes']/value" />
                </option>
                <option value="1800">
                    <xsl:value-of select="$locale/data[@name='30minutes']/value" />
                </option>
                <option value="3600">
                    <xsl:value-of select="$locale/data[@name='1hour']/value" />
                </option>
                <option value="21600">
                    <xsl:value-of select="$locale/data[@name='6hours']/value" />
                </option>
                <option value="43200">
                    <xsl:value-of select="$locale/data[@name='12hours']/value" />
                </option>
            </xsl:if>
        </select>
    </xsl:template>

    <!-- AS: контрол смены языка -->
    <xsl:template match="response" mode="language">
        <xsl:param name="mode" select="'client'" />
        <span class="right-side white3">
            <!--xsl:value-of select="$mode" /-->
            <xsl:choose>
                <xsl:when test="contains(/response/@language, 'ru-') != 'true'">
                    <xsl:choose>
                        <xsl:when test="$mode = 'client'">
                            <span class="languageUnit">
                                <!--Рус.-->
                                <span onclick="jQuery.changeLanguage('ru')" class="toggle underlined" title="Нажмите для переключения на русский язык">
                                    <span class="png" style="width:24px; height:18px">
                                        <img src="{./@baseWebAppPath}/img/lang-ru.png" class="language" width="24" height="18" alt="Русский" />
                                    </span>
                                    <xsl:text>&#160;</xsl:text>
                                    <span class=" underlined white">Русский</span>
                                </span>
                                <xsl:text>&#160;</xsl:text>
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <span>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=ru" class="white" title="Нажмите для переключения на русский язык">
                                    <!--Рус.-->
                                    <span class="png" style="width:24px; height:18px">
                                        <img src="{./baseWebAppPath}/img/lang-ru.png" width="24" height="18" alt="Русский" />
                                    </span>
                                </a>
                            </span>
                        </xsl:otherwise>
                    </xsl:choose>
                    <span>
                        <xsl:text>&#160;</xsl:text>
                    </span>
                    <span class="languageUnit">
                        <!--Eng.-->
                        <span class="png" style="width:24px; height:18px" title="Current language is english">
                            <img src="{./@baseWebAppPath}/img/lang-en.png" class="selected" width="24" height="18" alt="English" />
                        </span>
                        <xsl:text>&#160;</xsl:text>
                        <span class="white">English</span>
                    </span>
                </xsl:when>
                <xsl:otherwise>
                    <span class="languageUnit">
                        <!--Рус.-->
                        <span class="png" style="width:24px; height:18px" title="Текущий язык - русский">
                            <img src="{./@baseWebAppPath}/img/lang-ru.png" class="selected" width="24" height="18" alt="Русский" />
                        </span>
                        <xsl:text>&#160;Русский</xsl:text>
                        <xsl:text>&#160;</xsl:text>
                    </span>
                    <span>
                        <xsl:text>&#160;</xsl:text>
                    </span>
                    <span class="languageUnit">
                        <xsl:choose>
                            <xsl:when test="$mode = 'client'">
                                <span onclick="jQuery.changeLanguage('en')" class="toggle">
                                    <!--Eng.-->
                                    <span class="png" style="width:24px; height:18px" title="Current language is english">
                                        <img src="{./@baseWebAppPath}/img/lang-en.png" width="24" height="18" alt="English" />
                                    </span>
                                    <xsl:text>&#160;</xsl:text>
                                    <span class=" underlined white">English</span>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=en" title="Click to switch to english">
                                    <!--Eng.-->
                                    <span class="png" style="width:24px; height:18px">
                                        <img src="{./@baseWebAppPath}/img/lang-en.png" width="24" height="18" alt="English" />
                                    </span>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
                    </span>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>

    <!-- AS: контрол смены языка -->
    <xsl:template match="response" mode="languageShort">
        <xsl:param name="mode" select="'client'" />
        <span class="right-side">
            <span class="languageUnit">
                <!--Рус.-->
                <span onclick="jQuery.changeLanguage('ru')" class="toggle">
                    <span class="png" style="width:24px;height:18px;">
                        <img src="{./@baseWebAppPath}/img/lang-ru.png" border="0" title="Русский" width="24" height="18" />
                    </span>
                </span>
            </span>
            <xsl:text>&#160;</xsl:text>
            <span onclick="jQuery.changeLanguage('en')" class="toggle">
                <!--Eng.-->
                <span class="png" style="width:24px;height:18px;">
                    <img src="{./@baseWebAppPath}/img/lang-en.png" border="0" title="English" width="24" height="18" />
                </span>
            </span>
        </span>
    </xsl:template>

    <!-- AS: календарь - шаблон подключения скриптов календаря -->
    <xsl:template match="response" mode="addCalendar">


    </xsl:template>

    <!-- AS: копирайты -->
    <xsl:template name="copyrights">
        <!-- <div id="copyright" class="transp ontop copyright screen a11">
             <!-div><a href="http://www.geocenter-consulting.ru/" target="blank"><xsl:value-of select="$locale/data[@name='geoCenterCopyright']/value" /></a></div->
             <div>

                 <xsl:value-of select="$locale/data[@name='stsCopyright']/value" />

             </div>
         </div> -->
    </xsl:template>
    <xsl:template name="copyrightSitels">
        <div id="copyright" class="transp ontop copyright screen a12">
            <div>

                <xsl:value-of select="$locale/data[@name='stsCopyright']/value" />

            </div>
        </div>
    </xsl:template>



    <xsl:template name="copyrightSitelsText">
        <div class="copyright">
            <xsl:variable name="copyrightLocale" select="/response/@Copyright.Locale" />
            <xsl:value-of select="$locale/data[@name=$copyrightLocale]/value" />
        </div>
    </xsl:template>

    <!-- AS: шаблон контекстного меню -->
    <xsl:template name="contextMenu">
        <div class="contextMenu hide ontop" id="contextMenu">
        </div>
    </xsl:template>

    <!-- AS: скрыть экран, пока все не загрузится -->
    <xsl:template name="globalLoading">
        <div id="globalLoading" class="ontop hide">
            <span class="wait">
                <xsl:value-of select="$locale/data[@name='loading']/value" />
            </span>
        </div>
    </xsl:template>

    <!-- AS: Шаблон контрола сообщения -->
    <xsl:template name="commonPopupMessage">
        <div class="hide modal ontop" id="commonPopupMessage">
            <div class="cols2" style="margin:0; padding:0">
                <div class="col-left" style="margin:0; padding:0;">
                    <xsl:text>&#160;</xsl:text>
                </div>
                <div class="col-main2" style="margin:0; padding:0;">
                    <h2></h2>
                </div>
                <div class="col-right2 right-side" style="margin:0; padding:0;">
                    <span class="closeWin" onclick="jQuery.closePopup('commonPopupMessage')">
                        <span>
                            <xsl:text>&#160;x</xsl:text>
                        </span>
                    </span>
                </div>
            </div>
            <div class="clear" style="border-bottom:1px solid #ccc;">
                <xsl:text>&#160;</xsl:text>
            </div>
            <p></p>
            <div>
                <button type="button" class="default" onclick="jQuery.closePopup('commonPopupMessage')">
                    <xsl:value-of select="$locale/data[@name='ok']/value" />
                </button>
            </div>
        </div>
    </xsl:template>

    <!-- AS: шаблон сообщения, встраевоемого в текстовый блок -->
    <xsl:template name="embeddedMessage">
        <xsl:param name="messageId" select="''" />
        <div class="hide" id="{$messageId}"></div>
    </xsl:template>

    <!--
            AS: хедер HTML документа.
            Сделан специально без вылваливания какого-то конкретного элемента, чтобы можно было передавать параметры.
            Не хочется, чтобы серверный софт занимался выдачей строк, т.к. нужна перекомпиляция. По возможности все строковые ресурсы
            хранятся в XML, и XSL их достанет, сука блять
    -->
    <xsl:template name="pageHtmlHeader">
        <xsl:param name="title" />
        <title>
            <xsl:choose>
                <xsl:when test="$title">
                    <xsl:value-of select="$title"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$locale/data[@name='ApplicationMainTitle']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </title>
        <xsl:apply-templates select="/response" mode="headContent" />
    </xsl:template>

    <!--
    AS: футер страниц
    -->
    <xsl:template match="response" mode="pageFooter">
        <xsl:call-template name="clear" />
        <!-- footer -->
        <div class="empty"></div>
        <div class="footer">

            <xsl:variable name="copyright" select="/response/@Copyright.Locale" />
            <div class="inner-footer">
                <xsl:if test="$copyright">
                    <div class="info">
                        <p>
                            &#169; <span><xsl:value-of select="$locale/data[@name=$copyright]/value" /></span>.<br/>
                        </p>
                    </div>
                </xsl:if>
            </div>
        </div>


        <xsl:call-template name="loginPopup">
            <xsl:with-param name="path" select="./@ApplicationPath"/>
        </xsl:call-template>

    </xsl:template>

    <!-- AS: шаблон геозон пользователя -->
    <xsl:template name="geozones">
        <div id="geozoneMenu" class="panel_menu">
            <button  class="btn" data-geozone-create="">
                <xsl:value-of select="$locale/data[@name='drawZone']/value" />
            </button>
            <button class="btn" data-geozone-creategroup="">
                <xsl:value-of select="$locale/data[@name='createGroupZone']/value" />
            </button>
        </div>
        <div id="geozones" class="panel_list">
            <div id="geozoneList" style="width:100%;"></div>
        </div>
        <div class="divider">
            <xsl:text />
        </div>
    </xsl:template>

    <xsl:template name="join">
        <xsl:param name="delimeter" select="','" />
        <xsl:param name="items" />

        <xsl:for-each select="msxsl:node-set($items)/*">
            <xsl:if test="position() != 1">
                <xsl:value-of select="$delimeter"/>
            </xsl:if>
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:template>

    <!-- change language -->
    <xsl:template match="response" mode="languageSelector">
        <xsl:choose>
            <xsl:when test="not(contains(./@location,'gu-'))">
                <ul class="languge_selector">
                    <xsl:choose>
                        <xsl:when test="contains(./@language, 'ru-') = 'true'">
                            <li title="Текущий язык - русский">
                                <span>Русский</span>
                            </li>
                            <li>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=en" title="Switch to English language">English</a>
                            </li>

                        </xsl:when>
                        <xsl:otherwise>
                            <li>
                                <a href="{./@ApplicationPath}/services/language.aspx?a=lang&amp;lang=ru" title="Переключиться на русский язык">Русский</a>
                            </li>
                            <li title="Current language is English">
                                <span >English</span>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </ul>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--Обновление инфы на странице мониторинга-->
    <xsl:template name="refreshData">
        <xsl:param name="onclick" select="'Monitoring.updateTotalInfo();'"></xsl:param>
        <xsl:param name="tracker" select="'false'"></xsl:param>
        <xsl:choose>
            <xsl:when test="$onclick = 'none'">
                <div class="refreshData">
                    <span id="refreshImg" class="toggle" style="display: none;width:16px;">
                        <xsl:call-template name="formatImage">
                            <xsl:with-param name="browser" select="/response/@browser" />
                            <xsl:with-param name="height" select="16" />
                            <xsl:with-param name="width" select="16" />
                            <xsl:with-param name="src" select="concat(/response/@baseWebAppPath, '/img/controls/refresh.png')" />
                            <xsl:with-param name="title" select="$locale/data[@name='refresh']/value" />
                        </xsl:call-template>
                    </span>
                    <span id="loadingImg" style ="display:none;width:16px;">
                        <xsl:call-template name="formatImage">
                            <xsl:with-param name="browser" select="/response/@browser" />
                            <xsl:with-param name="height" select="16" />
                            <xsl:with-param name="width" select="16" />
                            <xsl:with-param name="src" select="concat(/response/@baseWebAppPath, '/img/wait.gif')" />
                            <xsl:with-param name="title" select="$locale/data[@name='loading']/value" />
                        </xsl:call-template>
                    </span>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="refreshData">
                    <span id="refreshImg" class="toggle" style="display: none;width:16px;" onclick="{$onclick}">
                        <xsl:call-template name="formatImage">
                            <xsl:with-param name="browser" select="/response/@browser" />
                            <xsl:with-param name="height" select="16" />
                            <xsl:with-param name="width" select="16" />
                            <xsl:with-param name="src" select="concat(/response/@baseWebAppPath, '/img/controls/refresh.png')" />
                            <xsl:with-param name="title" select="$locale/data[@name='refresh']/value" />
                        </xsl:call-template>
                    </span>
                    <span id="loadingImg" style ="display:none;width:16px;">
                        <xsl:call-template name="formatImage">
                            <xsl:with-param name="browser" select="/response/@browser" />
                            <xsl:with-param name="height" select="16" />
                            <xsl:with-param name="width" select="16" />
                            <xsl:with-param name="src" select="concat(/response/@baseWebAppPath, '/img/wait.gif')" />
                            <xsl:with-param name="title" select="$locale/data[@name='loading']/value" />
                        </xsl:call-template>
                    </span>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="TrackerAdd">
        <div id="addTrackerDiv">
            <button id="addTrackerToggle"  class="btn" data-tracker="">
                <xsl:value-of select="$locale/data[@name='add']/value" />
            </button>
            <div id="addTrackerFormPlaceholder"></div>
        </div>
    </xsl:template>

    <xsl:template name="WeekDaySelector">
        <xsl:param name="id" />
        <div class ="weekday-selector">
            <div>
                <label>
                    <input type="checkbox" checked="checked" name="weekday"/>
                    <xsl:value-of select="$locale/data[@name='weekdays']/value"/>
                </label>
                <label>
                    <input type="checkbox" checked="checked" name="weekend"/>
                    <xsl:value-of select="$locale/data[@name='weekend']/value"/>
                </label>
            </div>
            <div class="days"></div>
        </div>
    </xsl:template>

    <xsl:template name="FuelCostSelector">
        <xsl:param name="id" />
        <div class="fuel-cost-per-litre">
            <table cellspacing="0" cellspadding="0">
                <xsl:for-each select="/response/FuelTypes/FuelType">
                    <tr>
                        <td>
                            <xsl:value-of select="./@Name" />
                        </td>
                        <td>
                            <input type="text" class="data" name="{$id}{./@ID}" id="{$id}{./@ID}" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </div>
    </xsl:template>

    <!-- Шаблон аналог функции string.Split(';') в .NET -->
    <xsl:template name="output-tokens">
        <xsl:param name="list" />
        <xsl:variable name="newlist" select="concat(normalize-space($list), ' ')" />
        <xsl:variable name="first" select="substring-before($newlist, ' ')" />
        <xsl:variable name="remaining" select="substring-after($newlist, ' ')" />
        <id>
            <xsl:value-of select="$first" />
        </id>
        <xsl:if test="$remaining">
            <xsl:call-template name="output-tokens">
                <xsl:with-param name="list" select="$remaining" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>



    <xsl:template name="navTop">
        <div class="nav top">
            <xsl:apply-templates mode="languageSelector" select="."/>
            <!--Если есть ссылка на новую версию сайта - отображаем в шапке-->
            <xsl:if test="/response/@AlternateSiteUrl">
                <div class="nav__message">
                    <xsl:element name="a">
                        <xsl:attribute name="class">nav__message-link</xsl:attribute>
                        <xsl:attribute name="href"><xsl:value-of select="/response/@AlternateSiteUrl" /></xsl:attribute>
                        <xsl:choose>
                            <xsl:when test="contains(/response/@language, 'ru')">
                                <xsl:text>Новая версия сайта</xsl:text>
                            </xsl:when>
                            <xsl:when test="not(contains(/response/@language, 'ru'))">
                                <xsl:text>New site version</xsl:text>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:element>
                </div>
            </xsl:if>
        </div>
        <div class="clear"></div>

    </xsl:template>





    <xsl:variable name="lang">
        <xsl:choose>
            <xsl:when test="contains(/response/@language, 'en') = 'true'">
                <xsl:text>en</xsl:text>
            </xsl:when>
            <xsl:when test="contains(/response/@language, 'ru') = 'true'">
                <xsl:text>ru</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/response/@language" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="pathAbout">
        <xsl:value-of select="/response/@MainPageUrl"/>
    </xsl:variable>

    <!-- header -->
    <xsl:template name="header">
        <xsl:param name="selectedTab"/>

        <div id="headerUfin">
            <xsl:if test="not(/response/@WebSSO)">
                <xsl:call-template name="navTop"></xsl:call-template>
                <div class="header less">
                    <xsl:choose>
                        <xsl:when test="./@logo = 'logoKompas' ">
                            <a href="https://www.ufin.online/" title="LLC Kompas" target="_blank" id="logoKompas"></a>
                        </xsl:when>

                        <xsl:when test="./@logo = 'logoKompas2'">
                            <xsl:choose>
                                <xsl:when test="$lang = 'en' ">
                                    <a href="http://www.ufin.online/en/" title="UFIN" target="_blank" id="logoKompasEn"></a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a href="http://www.ufin.online/" title="UFIN" target="_blank" id="logoKompasRu"></a>
                                </xsl:otherwise>
                            </xsl:choose>

                        </xsl:when>

                        <xsl:otherwise>
                        </xsl:otherwise>
                    </xsl:choose>


                    <xsl:call-template name="loggedUser"/>
                    <div class="rightButtons">
                        <xsl:call-template name="helpButton" />

                    </div>

                    <xsl:if test="./logo != '' or ./@showAppName = 'true'">
                        <a href="{$pathAbout}" class="nika {$lang}" title="{./@title}"></a>
                    </xsl:if>
                </div>
            </xsl:if>
            <nav class="outerMenu">
                <xsl:choose>
                    <xsl:when test="/response/@WebSSO">
                        <xsl:call-template name="mainTabsWebSSO">
                            <xsl:with-param name="selected" select="$selectedTab"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="mainTabsLess">
                            <xsl:with-param name="selected" select="$selectedTab"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </nav>
            <div class="menu__notification">
                <xsl:if test="/response/message">
                    <p class="message error" style="text-align: center; display: block">
                        <xsl:call-template name="replace">
                            <xsl:with-param name="str" select="/response/message/MessageBody" />
                        </xsl:call-template>
                    </p>
                </xsl:if>
            </div>
        </div>

    </xsl:template>

    <xsl:template name="helpButton">
        <div class="helpButton">
            <a href="#" class="question-support">
                <xsl:attribute name="title">
                    <xsl:value-of select ="$locale/help/data[@name='questionToSupport']/value"/>
                </xsl:attribute>
                <xsl:value-of select ="$locale/data[@name='questionToSupport']/value"/>
            </a>
        </div>
    </xsl:template>




    <xsl:template name="mainTabsLess">
        <xsl:param name="selected" select="'map'" />

        <ul class="psevdoTabs" id="mainMenu">
            <xsl:if test="/response/systemRights[./right = 'SecurityMapGis']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='monitoring']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'map'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/map.aspx">
                        <xsl:value-of select="$locale/data[@name='monitoring']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="/response/systemRights[./right = 'ReportsAccess']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='reports']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'report'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/report.aspx">
                        <xsl:value-of select="$locale/data[@name='reports']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="/response/systemRights[./right = 'VehicleAccess']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='objects']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'objects'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/objects.aspx">
                        <xsl:value-of select="$locale/data[@name='TrackingObjectsShort']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="./@loggedUser != 'guest'">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='settings']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'settings'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/settings.aspx">
                        <xsl:value-of select="$locale/data[@name='userSettings']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="(./@showAdminPage = 'True')">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='administration']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'admin'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/admin.aspx">
                        <xsl:value-of select="$locale/data[@name='administration']/value" />
                    </a>
                </li>
                <xsl:if test="/response/systemRights[right = 'ServiceManagement']">
                    <li>
                        <xsl:if test="$selected = 'abonent'">
                            <xsl:attribute name="class">current</xsl:attribute>
                        </xsl:if>
                        <a href="{./@ApplicationPath}/Abonent.aspx">
                            <xsl:value-of select="$locale/data[@name='newCustomer2']/value" />
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-sms="">
                            <xsl:value-of select="$locale/data[@name='sms']/value" />
                        </a>
                    </li>
                </xsl:if>
            </xsl:if>
            <xsl:if test="/response/@WebSSO">

                <li class="helpButton">
                    <a href="#" class="question-support">
                        <xsl:attribute name="title">
                            <xsl:value-of select ="$locale/help/data[@name='questionToSupport']/value"/>
                        </xsl:attribute>
                        <xsl:value-of select ="$locale/data[@name='questionToSupport']/value"/>
                    </a>
                </li>

            </xsl:if>
            <xsl:if test="./@loggedUser != 'guest'">
                <li id="menuTracker">
                    <a href="#addTracker" data-tracker="">
                        <xsl:value-of select="$locale/data[@name='add']/value" />
                    </a>
                </li>
            </xsl:if>
        </ul>
    </xsl:template>

    <xsl:template name="mainTabsWebSSO">
        <xsl:param name="selected" select="'map'" />

        <ul class="psevdoTabs">
            <xsl:if test="/response/systemRights[./right = 'SecurityMapGis']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='monitoring']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'map'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/map.aspx">
                        <xsl:value-of select="$locale/data[@name='monitoring']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="/response/systemRights[./right = 'ReportsAccess']">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='reports']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'report'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/report.aspx">
                        <xsl:value-of select="$locale/data[@name='reports']/value" />
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="(./@showAdminPage = 'True')">
                <xsl:if test="/response/systemRights[right = 'ServiceManagement']">
                    <li>
                        <xsl:if test="$selected = 'abonent'">
                            <xsl:attribute name="class">current</xsl:attribute>
                        </xsl:if>
                        <a href="{./@ApplicationPath}/Abonent.aspx">
                            <xsl:value-of select="$locale/data[@name='newCustomer2']/value" />
                        </a>
                    </li>
                </xsl:if>
            </xsl:if>
            <xsl:if test="/response/@WebSSO">
                <li class="helpButton">
                    <a href="#" class="question-support">
                        <xsl:attribute name="title">
                            <xsl:value-of select ="$locale/help/data[@name='questionToSupport']/value"/>
                        </xsl:attribute>
                        <xsl:value-of select ="$locale/data[@name='questionToSupport']/value"/>
                    </a>
                </li>

            </xsl:if>
        </ul>
        <ul class="psevdoTabs pull-right">
            <xsl:if test="./@loggedUser != 'guest'">
                <li>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$locale/help/data[@name='settings']/value" />
                    </xsl:attribute>
                    <xsl:if test="$selected = 'settings'">
                        <xsl:attribute name="class">current</xsl:attribute>
                    </xsl:if>
                    <a href="{./@ApplicationPath}/settings.aspx">
                        <xsl:value-of select="$locale/data[@name='userSettings']/value" />
                    </a>
                </li>
            </xsl:if>
        </ul>
    </xsl:template>

    <xsl:template name="loggedUser">
        <xsl:param name="selectedTab" select="'map'" />
        <div id="loggedUser" class="loggedUser less">

            <div class="middle">
                <span id="logged-user">
                    <xsl:value-of select="$locale/data[@name='UserTitle']/value" />: <span class="red">
                    <xsl:value-of select="./@loggedUser" />
                </span>

                    <a href="{./@ApplicationPath}/default.aspx?a=logout">
                        <xsl:attribute name="title">
                            <xsl:value-of select ="$locale/help/data[@name='logout']/value"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">
                            <xsl:text>logoutAjax</xsl:text>
                        </xsl:attribute>
                        <xsl:value-of select="$locale/data[@name='Logout']/value"/>
                    </a>
                </span>
                <xsl:if test="/response/Departments/Department">
                    <span>
                        <xsl:variable name="customerId" select="/response/Departments/Department/Id" />
                        <xsl:variable name="customerName" select="/response/Departments/Department/Name" />
                        <xsl:variable name="customerType" select="/response/Departments/Department/Type" />

                        <xsl:choose>
                            <xsl:when test="/response/Departments/Department/TypeIsChangeable = 'true'">
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:choose>
                                            <xsl:when test="/response/@isSuperAdministrator = 'true'">
                                                <xsl:value-of select="/response/@ApplicationPath"/>/department.aspx?a=approveCorporativeForm
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="/response/@ApplicationPath"/>/department.aspx?a=requestCorporativeForm
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:attribute name="title">
                                        <xsl:choose>
                                            <xsl:when test="/response/@isSuperAdministrator = 'true'">
                                                <xsl:value-of select="$locale/data[@name='goToCorporativeDescriptionForSuperAdministrator']/value"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$locale/data[@name='goToCorporativeDescription']/value"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>

                                    <xsl:value-of select="$locale/customerType/data[@name=$customerType]/value" />
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$locale/customerType/data[@name=$customerType]/value" />
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:text>: </xsl:text>

                        <xsl:choose>
                            <xsl:when test="/response/Departments/@Count = 1">
                                <xsl:value-of select="$customerName"/>
                                <xsl:text> </xsl:text>
                            </xsl:when>
                            <xsl:when test ="/response/Departments/@Count > 1">
                                <a href="#" id="departmentSelector">
                                    <xsl:attribute name="data-value">
                                        <xsl:value-of select="$customerId"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$customerName"/>
                                </a>
                            </xsl:when>
                        </xsl:choose>
                    </span>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="/response/Departments/@Count > 1">
                        <div id="searchDepartment" class="popup_commands"></div>
                    </xsl:when>
                </xsl:choose>
            </div>
            <div class="clear"></div>
        </div>
    </xsl:template>

    <!-- Templates for requireJS-->
    <xsl:template name="requireJS">
        <xsl:variable name="path" select="/response/@baseWebAppPath" />
        <script data-main="{$path}{/response/@mainJavascript}" src="{$path}/js/libs/require.js"></script>
    </xsl:template>

    <xsl:template name="link-css">
        <link type="text/css" rel="stylesheet" href="{/response/@baseWebAppPath}{/response/@mainCss}"/>
    </xsl:template>



    <xsl:template name="loginPopup">
        <xsl:param name="path"></xsl:param>


        <div class="b-login b-login__modal"></div>
        <div class="b-login__shadow"></div>
        <script id="login2PopupIE" type="text/html">

            <form id="loginForm" class="b-login__form" action="login.aspx" method="POST" data-fType="loginForm">
                <input type="hidden" name="a" id="a" value="loginajax" />
                <input type="hidden" name="tz" id="tz" value="" />
                <input type="hidden" name="dst" id="dst" value=""/>
                <input type="hidden" name="departmentId" id="departmentId" value="" />
                <div class="b-login__header">
                    <xsl:value-of select="$locale/data[@name='systemEntrance']/value" />
                </div>
                <div class="vertical b-login__container">
                    <div class="b-login__error"></div>


                    <div class="controlBlock">
                        <div class="controlDescription">
                            <xsl:value-of select="$locale/data[@name='Login']/value" />:
                        </div>
                        <div class="control">
                            <input type="text" name="login" id="txtLogin" tabindex="1" class="inp" />
                        </div>
                    </div>

                    <div class="controlBlock">
                        <div class="controlDescription">
                            <label for="password" class="mandatory">
                                <xsl:value-of select="$locale/data[@name='Password']/value" />:
                            </label>
                        </div>
                        <div class="control">
                            <input type="password" name="password" id="password" tabindex="2" class="inp" />
                            <xsl:if test="/response/@Login.PasswordRecoveryLink">
                                <span class="help">
                                    <a href="{/response/@ApplicationPath}/{/response/@Login.PasswordRecoveryLink}" tabindex="5">
                                        <xsl:value-of select="$locale/data[@name='forgotPassword']/value" />?
                                    </a>
                                </span>
                            </xsl:if>
                        </div>
                    </div>

                    <div class="controlBlock">
                        <div class="control">
                            <button type="button" class="b-login__submit btn" tabindex="3">
                                <span class="status"></span>
                                <xsl:value-of select="$locale/data[@name='GetIn']/value" />
                            </button>
                            <span class="pull-right">
                                <a href="{/response/@MainPageUrl}">
                                    <xsl:value-of select="$locale/data[@name='startPage']/value" />
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </script>
    </xsl:template>

    <xsl:template name="localTitle"/>
    <xsl:template name="websso">
    </xsl:template>

    <xsl:template match="response" mode="loginForm">
        <xsl:variable name="IE" >
            <xsl:choose>
                <xsl:when test="/response/@BrowserIE8Allowed = 'true'">8</xsl:when>
                <xsl:otherwise>9</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div class="row">
            <xsl:comment>[if lt IE <xsl:value-of select="$IE" />]&gt;
                &lt;div id="noIE" class="b-block" &gt;
                &lt;div class="b-block__legend" style="text-transform: uppercase" &gt;<xsl:value-of select="$locale/data[@name='attention']/value" />!&lt;/div&gt;
                &lt;div class="b-block__container"&gt;

                &lt;p><xsl:value-of select="$locale/data[@name='dearUser']/value" />!&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='pleaseUseModernBrowsersWithTheService']/value" />&lt;/p&gt;
                &lt;p>&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='thanks']/value" />&lt;/p&gt;


                &lt;ul class="b-browser"&gt;
                &lt;li class="b-browser__item"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" href="http://www.mozilla-europe.org/firefox"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-firefox.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;li class="b-browser__item"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" href="http://www.google.com/chrome"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-chrome.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;li class="b-browser__item"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" href="http://www.opera.com/"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-opera.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;/ul&gt;


                &lt;/div&gt;
                &lt;/div&gt;

                &lt;/div&gt;
                &lt;![endif]</xsl:comment>
            <div class="b-login">
                <form id="loginForm" action="{//@ApplicationPath}/login.aspx" method="POST" class="b-login__form vertical">
                    <input type="hidden" name="a" id="a" value="login" />
                    <input type="hidden" name="tz" id="tz" value="" />
                    <input type="hidden" name="dst" id="dst" value=""/>
                    <input type="hidden" name="departmentId" id="departmentId" value="" />
                    <div class="b-login__header">
                        <xsl:value-of select="$locale/data[@name='systemEntrance']/value" />
                    </div>
                    <div class="b-login__container">
                        <xsl:apply-templates select="/response/message" />
                        <div class="controlBlock">
                            <div class="controlDescription">

                                <xsl:value-of select="$locale/data[@name='Login']/value" />:
                                <xsl:if test="/response/@Login.RegistrationLink">
                                    <a href="{/response/@ApplicationPath}/{/response/@Login.RegistrationLink}" tabindex="4" class="info">
                                        <xsl:value-of select="$locale/data[@name='registration']/value" />
                                    </a>
                                </xsl:if>
                            </div>
                            <div class="control">
                                <input type="text" name="login" class="inp" id="txtLogin" value="{//@previousLogin}" tabindex="1" />
                            </div>
                        </div>
                        <div class="controlBlock">
                            <div class="controlDescription">
                                <xsl:if test="/response/@Login.PasswordRecoveryLink">
                                    <a href="{/response/@ApplicationPath}/{/response/@Login.PasswordRecoveryLink}" tabindex="5" class="info">
                                        <xsl:value-of select="$locale/data[@name='forgotPassword']/value" />?
                                    </a>
                                </xsl:if>
                                <xsl:value-of select="$locale/data[@name='Password']/value" />:
                            </div>
                            <div class="control">
                                <input type="password" class="inp" name="password" id="password" tabindex="2" />
                            </div>
                        </div>

                        <div class="controlBlock">
                            <div class="control">
                                <button type="button" id="login-submit" class="b-login__submit btn" tabindex="3">
                                    <xsl:value-of select="$locale/data[@name='GetIn']/value" />
                                </button>

                                <div class="pull-right">
                                    <ul class="buttons">
                                        <li>
                                            <xsl:call-template name="demoLoginLink" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="loginFormPretty">
        <xsl:variable name="IE" >
            <xsl:choose>
                <xsl:when test="/response/@BrowserIE8Allowed = 'true'">8</xsl:when>
                <xsl:otherwise>9</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="row">
            <xsl:comment>[if lt IE <xsl:value-of select="$IE" />]&gt;
                &lt;div id="noIE" class="b-block" &gt;
                &lt;div class="b-block__legend" style="text-transform: uppercase" &gt;<xsl:value-of select="$locale/data[@name='attention']/value" />!&lt;/div&gt;
                &lt;div class="b-block__container"&gt;

                &lt;p><xsl:value-of select="$locale/data[@name='dearUser']/value" />!&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='pleaseUseModernBrowsersWithTheService']/value" />&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='useOneOfLinksListedBelowToGetRecomendedBrowser']/value" />&lt;/p&gt;
                &lt;p>&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='thanks']/value" />&lt;/p&gt;



                &lt;ul class="b-browser"&gt;
                &lt;li class="b-browser__item"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" href="http://www.mozilla-europe.org/firefox"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-firefox.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;li class="b-browser__item&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" href="http://www.google.com/chrome"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-chrome.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;li class="b-browser__item&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" href="http://www.opera.com/"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" style="border: none;" src="<xsl:value-of select="/response/@baseWebAppPath" />/img/warnings/ie6no-opera.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/li&gt;
                &lt;/ul&gt;


                &lt;/div&gt;
                &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class="row" style="display: none"&gt;
                &lt;![endif]</xsl:comment>
            <div class="b-login">
                <form id="loginForm" class="b-login__form vertical" action="{//@ApplicationPath}/login.aspx" method="POST">
                    <input type="hidden" name="a" id="a" value="login" />
                    <input type="hidden" name="tz" id="tz" value="" />
                    <input type="hidden" name="dst" id="dst" value=""/>
                    <input type="hidden" name="departmentId" id="departmentId" value="" />

                    <div class="b-login__header">
                        <xsl:value-of select="$locale/data[@name='systemEntrance']/value" />
                    </div>

                    <div class="b-login__container">
                        <xsl:apply-templates select="/response/message" />

                        <div class="controlBlock">
                            <label for="login" class="controlDescription">
                                <xsl:value-of select="$locale/data[@name='Login']/value" />:
                            </label>
                            <xsl:if test="/response/@Login.RegistrationLink">
                                <div class="right-column">
                                    <a href="{/response/@ApplicationPath}/{/response/@Login.RegistrationLink}" tabindex="4">
                                        <xsl:value-of select="$locale/data[@name='registration']/value" />
                                    </a>
                                </div>
                            </xsl:if>
                            <div class="control">
                                <input type="text" class="inp" name="login" id="txtLogin" value="{//@previousLogin}" tabindex="1" />
                            </div>
                        </div>

                        <div class="controlBlock">
                            <label for="password" class="controlDescription">
                                <xsl:value-of select="$locale/data[@name='Password']/value" />:
                            </label>
                            <xsl:if test="/response/@Login.PasswordRecoveryLink">
                                <div class="right-column">
                                    <a href="{/response/@ApplicationPath}/{/response/@Login.PasswordRecoveryLink}" tabindex="5">
                                        <xsl:value-of select="$locale/data[@name='forgotPassword']/value" />?
                                    </a>
                                </div>
                            </xsl:if>
                            <div class="control">
                                <input type="password" name="password" class="inp" id="password" tabindex="2" />
                            </div>
                        </div>

                        <div class="controlBlock">
                            <div class="control">
                                <button type="button" id="login-submit" tabindex="3" class="btn">
                                    <xsl:value-of select="$locale/data[@name='GetIn']/value" />
                                </button>
                                <img src="{/response/@baseWebAppPath}/img/wait.gif" style="position: relative;top: 5px;left: 3px; display:none;"/>
                            </div>

                            <div class="right-column">
                                <ul class="buttons">
                                    <li>
                                        <xsl:call-template name="demoLoginLink" />
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </form>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="briefDescriptionTitle">
        <xsl:param name="selectedPage"></xsl:param>
        <xsl:if test="/response/pageContent/promo">
            <div class="row">
                <div class="briefDescriptionTitle 123">
                    <div class="legend">
                        <xsl:choose>
                            <xsl:when test="/response/pageContent/promo/page">
                                <a href="{/response/@ApplicationPath}">
                                    <xsl:value-of select="/response/pageContent/promo/@title" />
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="/response/pageContent/promo/@title" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                    <ul>
                        <xsl:for-each select="/response/pageContent/promo/menu">
                            <li id="{./@id}-bookmark">
                                <xsl:variable name="is-selected" select="./page[@id=$selectedPage]" />
                                <xsl:if test="$is-selected">
                                    <xsl:choose>
                                        <xsl:when test="count(./page)>1">
                                            <xsl:attribute name="class">selected</xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="class">selected-free</xsl:attribute>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:if>
                                <xsl:choose>
                                    <xsl:when test="./@default">
                                        <xsl:variable name="defaultPage" select="./@default" />
                                        <xsl:apply-templates select="./page[@id=$defaultPage]" mode="hyperlink">
                                            <xsl:with-param name="text" select="./@title" />
                                        </xsl:apply-templates>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="./page[1]" mode="hyperlink">
                                            <xsl:with-param name="text" select="./@title" />
                                        </xsl:apply-templates>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="count(./page)>1">
                                    <xsl:if test="$is-selected">
                                        <ul>
                                            <xsl:for-each select="./page">
                                                <li id="{./@id}-bookmark">
                                                    <xsl:if test="./@id = $selectedPage">
                                                        <xsl:attribute name="class">selected</xsl:attribute>
                                                    </xsl:if>

                                                    <xsl:apply-templates select="." mode="hyperlink" />

                                                </li>
                                            </xsl:for-each>
                                        </ul>
                                    </xsl:if>
                                </xsl:if>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template match="response" mode="selectedPage">
        <xsl:variable name="urlReferrer" select="./@UrlReferrer" />

        <xsl:choose>
            <xsl:when test="./@selectedPage">
                <xsl:variable name="selectedPageValue" select="./@selectedPage" />
                <xsl:choose>
                    <xsl:when test="./pageContent/promo/menu/page[@id=$selectedPageValue]">
                        <xsl:value-of select="./@selectedPage" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="count(./pageContent/promo/UrlReferrer[user:Contains($urlReferrer, @from)]) &gt; 0">
                <xsl:value-of select="./pageContent/promo/UrlReferrer[user:Contains($urlReferrer, @from)]/@to"/>
            </xsl:when>
            <xsl:when test="./pageContent/promo/page">
                <xsl:value-of select="./pageContent/promo/page[1]/@id" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="applicationPath">
        <script type="text/javascript">
            function ensureEndsWithSlash(s) {
            if (!s.length)
            return s;
            if (s[s.length-1] === '/')
            return s;
            return s + '/';
            }
            var UFIN = UFIN || {};
            UFIN.apiPath = ensureEndsWithSlash("<xsl:value-of select="/response/@ApplicationPath"/>");
            UFIN.applicationPath = ensureEndsWithSlash("<xsl:value-of select="/response/@baseWebAppPath"/>");
            var applicationPath = UFIN.applicationPath;
        </script>
    </xsl:template>

</xsl:stylesheet>
        <!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
        <metaInformation>
        <scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="page.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
        </metaInformation>
        -->
