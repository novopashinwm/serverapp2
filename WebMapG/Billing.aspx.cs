﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using Compass.Ufin.Billing.Core;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class BillingPage : BasePage
	{
		[JsonPageAction("android", Method = HttpMethod.Post)]
		public void PostAndroid(string purchase, string signature)
		{
			IWebPS.TryAttachAndroidSubscription(purchase, Convert.FromBase64String(signature));
		}
		[JsonPageAction("apple", Method = HttpMethod.Post, ResponseType = DataType.Empty)]
		public void PostApple(string purchase)
		{
			IWebPS.TryAttachAppleSubscription(purchase);
		}
		[JsonPageAction("billing-services")]
		public List<BillingService> GetBillingServices()
		{
			return IWebPS.GetBillingServices();
		}
		[JsonPageAction("get-balance-info")]
		[JsonPageAction("get-account-balance-info")]
		public CommonActionResponse<BalanceInfo> GetAccountBalanceInfo()
		{
			return InvokeBilling<IPaymentProvider, BalanceInfo>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetAccountBalanceInfo(confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		[JsonPageAction("get-current-tariff")]
		[JsonPageAction("get-object-current-product")]
		public CommonActionResponse<ProductItem> GetObjectCurrentProduct(int vehicleId)
		{
			return InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				// Проверка прав Администратора и Управленца услуг
				if (!IWebPS.IsAllowedVehicleAny(vehicleId, SystemRight.SecurityAdministration, SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetObjectCurrentProduct(vehicleId, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		/// <summary> Возвращает набор тарифов доступных или действующих для указанного объекта наблюдения </summary>
		/// <param name="vehicleId"> Объект наблюдения </param>
		/// <returns></returns>
		[JsonPageAction("get-object-tariffs")]
		[JsonPageAction("get-object-available-products")]
		public CommonActionResponse<List<ProductItem>> GetObjectAvailableProducts(int vehicleId)
		{
			return InvokeBilling<IPaymentProvider, List<ProductItem>>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				// Проверка прав Администратора и Управленца услуг
				if (!IWebPS.IsAllowedVehicleAny(vehicleId, SystemRight.SecurityAdministration, SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetObjectAvailableProducts(vehicleId, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		/// <summary> Получить цены для клиента для указанного тарифа и дополнений </summary>
		/// <param name="vehicleId"> Объект наблюдения </param>
		/// <param name="product"></param>
		/// <returns></returns>
		[JsonPageAction("get-object-tariff-price", Method = HttpMethod.Post)]
		[JsonPageAction("get-account-product-price", Method = HttpMethod.Post)]
		public CommonActionResponse<ProductItem> GetAccountProductPrice(ProductItem product)
		{
			return InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetAccountProductPrice(product, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		/// <summary> Установить указанный тариф и его дополнения для объекта наблюдения </summary>
		/// <param name="vehicleId"> Объект наблюдения </param>
		/// <param name="product"></param>
		/// <returns></returns>
		[JsonPageAction("set-object-tariff", Method = HttpMethod.Post)]
		[JsonPageAction("set-object-current-product", Method = HttpMethod.Post)]
		public CommonActionResponse<ProductItem> SetObjectCurrentProduct(ProductItem product, int vehicleId)
		{
			return InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
			{
				if (null == product)
					throw new ArgumentNullException(nameof(product));
				var productParams = product?.Params;
				if (null == productParams)
					productParams = (product.Params = new List<ProductParamItem>());
				var paramId = productParams
					?.FirstOrDefault(p => p.Code == ProductParamItem.MonitoringObjectIdenKey);
				if (null == paramId)
					productParams.Add(new ProductParamItem { Code = ProductParamItem.MonitoringObjectIdenKey, Value = vehicleId.ToString() });
				var paramName = productParams
					?.FirstOrDefault(p => p.Code == ProductParamItem.MonitoringObjectNameKey);
				if (null == paramName)
					productParams.Add(new ProductParamItem { Code = ProductParamItem.MonitoringObjectNameKey, Value = IWebPS.GetVehicleName(vehicleId) });
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				// Проверка наличия подтвержденной почты
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));
				// Проверка прав Администратора или Управленца услуг, у пользователя
				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));
				// Проверка прав Администратора или Управленца услуг, на объект
				if (!IWebPS.IsAllowedVehicleAny(vehicleId, SystemRight.SecurityAdministration, SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));
				// Вызов метода billing
				return factory.CreateChannel()
					.SetObjectCurrentProduct(product, vehicleId, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		/// <summary> Возвращает одноразовую ссылку для входа в billing </summary>
		[JsonPageAction("get-one-time-login-link")]
		public CommonActionResponse<string> GetOneTimeLoginLink()
		{
			return InvokeBilling<IPaymentProvider, string>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
						.GetOneTimeLoginLink(confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		/// <summary> Возвращает html как строку для перехода на оплату созданного платежа </summary>
		[JsonPageAction("get-new-payment-html")]
		public CommonActionResponse<string> GetNewPaymentHtml(decimal amount)
		{
			return InvokeBilling<IPaymentProvider, string>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
						.GetNewPaymentHtml(amount, confirmedEmail);
			});
		}
		[JsonPageAction("get-account-available-products")]
		public CommonActionResponse<List<ProductItem>> GetAccountAvailableProducts()
		{
			return InvokeBilling<IPaymentProvider, List<ProductItem>>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetAccountAvailableProducts(confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		[JsonPageAction("get-account-current-products")]
		public CommonActionResponse<List<ProductItem>> GetAccountCurrentProducts()
		{
			return InvokeBilling<IPaymentProvider, List<ProductItem>>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetAccountCurrentProducts(confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		[JsonPageAction("get-account-products")]
		public CommonActionResponse<List<ProductItem>> GetAccountProducts(params ProductStatus[] productStatuses)
		{
			return InvokeBilling<IPaymentProvider, List<ProductItem>>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.GetAccountProducts(productStatuses, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
		[JsonPageAction("set-account-product", Method = HttpMethod.Post)]
		public CommonActionResponse<ProductItem> SetAccountProduct(ProductItem product)
		{
			return InvokeBilling<IPaymentProvider, ProductItem>((factory) =>
			{
				var confirmedEmail = IWebPS
					?.GetOperatorEmails(IWebPS.OperatorId)
					?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
					?.Value;
				if (string.IsNullOrWhiteSpace(confirmedEmail))
					throw new SecurityException(GetMessageData("EmailNotConfirmed"));

				if (!OperatorSystemRights.Any(r => r == SystemRight.SecurityAdministration || r == SystemRight.ServiceManagement))
					throw new SecurityException(GetMessageData("InsufficientRights"));

				return factory.CreateChannel()
					.SetAccountProduct(product, confirmedEmail, IWebPS.CultureInfo.TwoLetterISOLanguageName);
			});
		}
	}
}