﻿using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;
using GroupDTO = FORIS.TSS.BusinessLogic.DTO.Group;

namespace FORIS.TSS.UI.WebMapG
{
	public class Group : BasePage
	{
		[PageAction()]
		public void GroupPage()
		{
			var id = int.Parse(GetParamValue("id"));
			var group = GetVehicleGroupsDTO(new []{id}).First();
			GroupDTO[] groups = group.AllowEditContent ? GetVehicleGroupsDTO() : new[] {group};
			TemplateFile = "group";
			AddProperty("template", "groupPage");
			AddProperty("name", group.Name);
			AddProperty("vehiclesJSON", JsonHelper.SerializeObjectToJson(
				group.AllowEditContent || group.AllowAddToGroup
					? GetVehiclesDTO(false)
					: GetVehiclesDTO(false, group.ObjectIds)));
			AddProperty("groupsJSON", JsonHelper.SerializeObjectToJson(groups));
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[JsonPageAction("list", Method = HttpMethod.Get)]
		public GroupDTO[] GetList(int[] ids, bool includeNotInGroups = false, bool includeAll = false, bool includeStandard = false, bool includeIgnored = false)
		{
			return GetVehicleGroupsDTO(ids, includeNotInGroups, includeAll, includeStandard, includeIgnored);
		}
		[JsonPageAction("create", HttpMethod.Post)]
		public int CreateVehicleGroup(string name)
		{
			return IWebPS.AddVehicleGroup(name);
		}
		[JsonPageAction("update", HttpMethod.Post)]
		public bool UpdateVehicleGroup(GroupDTO group)
		{
			IWebPS.UpdateVehicleGroup(group);
			return true;
		}
		[JsonPageAction("add-vehicle", HttpMethod.Post)]
		public bool AddVehicleToGroup(int vehicleId, int groupId)
		{
			IWebPS.AddVehicleToGroup(vehicleId, groupId);
			return true;
		}
		[JsonPageAction("remove-vehicle", HttpMethod.Post)]
		public bool RemoveVehicleFromGroup(int vehicleId, int groupId)
		{
			IWebPS.RemoveVehicleFromGroup(vehicleId, groupId);
			return true;
		}
		[JsonPageAction("remove", HttpMethod.Post)]
		public bool Remove(int id)
		{
			IWebPS.RemoveVehicleGroup(id);
			return true;
		}
	}
}