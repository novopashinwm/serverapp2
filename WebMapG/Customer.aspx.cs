﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Enums;
using Interfaces.Web;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Customer : BasePage
	{
		#region CustomerPagePart
		[PageAction("default")]
		public void ShowPage()
		{
			RenderCustomerPage("customer", "customerPage");
		}

		private void RenderCustomerPage(string templateFileName, string templateName)
		{
			try
			{
				TemplateFile = templateFileName;
				AddProperty("template", templateName);
				if (OperatorId != null)
					AddProperty("AdminId", OperatorId.Value.ToString(CultureInfo.InvariantCulture));
				
				if (IWebPS.DepartmentExtIDIsAllowed)
					AddProperty("DepartmentExtIDIsRequired", "true");

				ResponseSerializationBeg();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
			}
			ResponseSerializationEnd();
		}

		#endregion CustomerPagePart

		#region CustomerWebApiPart
		[JsonPageAction("get-customer-operators", Method = HttpMethod.Get)]
		[JsonPageAction("get-customer-operators", Method = HttpMethod.Post)]
		public List<Operator> GetCustomerOperators(int id, CancellationToken cancellationToken)
		{
			if (!IWebPS.IsSuperAdministrator && !IWebPS.IsSalesManager)
				throw new SecurityException("Access denied");
			return IWebPS.GetDepartmentOperators(id);
		}
		[JsonPageAction("search-customers", Method = HttpMethod.Get)]
		[JsonPageAction("search-customers", Method = HttpMethod.Post)]
		public GetListResponse<SearchHits<CustomerItem>> SearchCustomers(string searchText, int? page, int? recordsPerPage)
		{
			if (!IWebPS.IsSuperAdministrator && !IWebPS.IsSalesManager)
				throw new SecurityException("Access denied");

			var pagination = recordsPerPage.HasValue
				? new Pagination
				{
					Page           = page ?? 1,
					RecordsPerPage = recordsPerPage.Value,
				}
				: null;
			var customers = default(IEnumerable<SearchHits<CustomerItem>>);
			using (var source = CancellationTokenSource.CreateLinkedTokenSource(Response.ClientDisconnectedToken, Request.TimedOutToken))
			{
				customers = IWebPS
					?.SearchCustomers(searchText, pagination, source.Token);
			}
			// Вызов поиска списка департаментов/клиентов
			return new GetListResponse<SearchHits<CustomerItem>>
			{
				Items      = customers?.ToArray(),
				Pagination = pagination
			};
		}
		#endregion CustomerWebApiPart
	}
}