﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.UI.WebMapG;
using Interfaces.Geo;
using RU.NVG.UI.WebMapG.Model.Common;

public partial class address : BasePage
{
	/// <summary> Найти адрес по координатам </summary>
	[PageAction("findbycoords")]
	public void FindByCoords()
	{
		SetXmlContentType();
		ResponseSerializationBeg();
		try
		{
			System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
			nfi.NumberDecimalSeparator = ".";
			var x = default(float);
			var y = default(float);
			float.TryParse(GetParamValue("geox"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
			float.TryParse(GetParamValue("geoy"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);
			var address = IWebPS.GetAddressByPoint(lat: y, lng: x, CurrentLanguage, MapGuid);
			// <address>
			Writer.WriteElementString("address", address);
			// </address>
		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, GetMessageData("error"), myEx.Message));
			WriteLog(myEx);
		}
		ResponseSerializationEnd();
	}
	/// <summary> Найти адрес по IP адресу </summary>
	[JsonPageAction("getlocationbyip", Method = HttpMethod.Get)]
	public LocationInfo GetLocationByIp()
	{
		return IWebPS.GetLocationByIP(Request.UserHostAddress);
	}
	/// <summary> Найти адрес по координатам (метод по умолчанию) </summary>
	[JsonPageAction("default", Method = HttpMethod.Get)]
	public string GetAddress(double lat, double lng, string mapGuid)
	{
		var mapGuidVal = default(Guid?);
		var mapGuidTmp = default(Guid);
		if (Guid.TryParse(mapGuid, out mapGuidTmp))
			mapGuidVal = mapGuidTmp;
		return IWebPS.GetAddressByPoint(lat: lat, lng: lng, CurrentLanguage, mapGuidVal);
	}
	[JsonPageAction("geocode", Method = HttpMethod.Get)]
	[JsonPageAction("geocode", Method = HttpMethod.Post)]
	public CommonActionResponse<AddressResponse[]> GetAddressesByText(string address, string mapGuid)
	{
		var mapGuidVal = default(Guid?);
		var mapGuidTmp = default(Guid);
		if (Guid.TryParse(mapGuid, out mapGuidTmp))
			mapGuidVal = mapGuidTmp;
		var response = new CommonActionResponse<AddressResponse[]>();
		try
		{
			response.Object = IWebPS.GetAddressesByText(address, CurrentLanguage, mapGuidVal).ToArray();
			if (null == response.Object)
				response.Result = CommonActionResult.ObjectNotFound;
		}
		catch (Exception ex)
		{
			response.Object = null;
			response.Result = CommonActionResultExceptionMap
				.GetCommonActionResultByExceptionType(ex.GetType());
			response.ResultText = ex.Message;
		}
		return response;
	}
}