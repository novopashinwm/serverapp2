﻿using FORIS.TSS.UI.WebMapG;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SignalRStartup))]

namespace FORIS.TSS.UI.WebMapG
{
	internal class SignalRStartup
	{
		public void Configuration(IAppBuilder app)
		{
			app.MapSignalR(new HubConfiguration { EnableDetailedErrors = true });
		}
	}
}