﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DTO;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.UI.WebMapG.admin
{
	public class Admin : BasePage
	{
		#region AdminPage
		[PageAction]
		public void ShowPage()
		{
			RenderAdminPage("admin", "adminPage");
		}
		private void RenderAdminPage(string templateFileName, string templateName)
		{
			try
			{
				TemplateFile = templateFileName;
				AddProperty("template", templateName);
				if (OperatorId != null)
					AddProperty("AdminId", OperatorId.Value.ToString(CultureInfo.InvariantCulture));
				
				string menuItem = GetParamValue("menuItem");
				AddProperty("menuItem", menuItem);

				ResponseSerializationBeg();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
			}
			ResponseSerializationEnd();
		}
		#endregion AdminPage

		#region OperatorRights
		[PageAction("deleteOperator")]
		public void DeleteOperator()
		{
			JsonResponseSerializationBeg();
			try
			{
				var deleteOperatorId = int.Parse(GetParamValue("deleteOperatorId"));

				if (deleteOperatorId > 0)
				{
					IWebPS.DeleteOperator(deleteOperatorId);
				}

				JsonResponse.resCode = ResponseCode.OK;
				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		[PageAction("SaveOperatorBaseChanges")]
		public void SaveOperatorBaseChanges()
		{
			WrapJsonAction<object>(() =>
				{
					var jsonData = GetParamValue("jsonData");
					var oper = JsonHelper.DeserializeObjectFromJson<Operator>(jsonData);
					if (oper == null)
						throw new ApplicationException("Unable to parse operator json");
					
					if (oper.id > 0)
					{
						var result = SaveOperatorChanges(oper);

						var message = GetMessage(result, oper.login);

						if (message != null)
							return message;

						return oper.id;
					}
					
					var newOper = CreateDepartmentOperator(oper);
					if (newOper == null)
						return Messages.FirstOrDefault();

					return newOper.id;
				});
		}
		[PageAction("SaveOperatorRightsChanges")]
		public void SaveOperatorRightsChanges()
		{
			JsonResponseSerializationBeg();
			try
			{
				var jsonData = GetParamValue("jsonData");
				var fromJson = JsonHelper.DeserializeObjectFromJson<Operator[]>(jsonData);
				if (null != fromJson)
				{
					var data = fromJson;
					foreach (var oper in data)
					{
						bool saveOperSuccess = false;
						if (oper.id > 0)
						{
							// Сохраняем изменения оператора
							var operSaveRes = SaveOperatorChanges(oper);
							if (operSaveRes == SetNewLoginResult.Success)
							{
								saveOperSuccess = true;
							}
						}
						else // Создаем нового оператора
						{
							var newOper = CreateDepartmentOperator(oper);
							if (newOper != null)
							{
								saveOperSuccess = true;
								oper.id         = newOper.id;
							}
						}
						// Если удачно сохранили оператора, сохраняем его права
						if (saveOperSuccess)
						{
							ChangeOperatorVehiclesRights(oper);
							JsonResponse.value   = oper;
							JsonResponse.resCode = ResponseCode.OK;
						}
						else
						{
							JsonResponse.resCode = ResponseCode.WARNING;
						}
					}
				}
				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		private Operator CreateDepartmentOperator(Operator oper)
		{
			Operator newOperInfo;

			if (IWebPS.Department == null)
			{
				AddMessage(
					new Message
					{
						Severity     = Severity.Error,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody  = string.Format(GetMessageData("NoDepartment"), oper.login)
					});
				return null;
			}

			var res = IWebPS.CreateDepartmentOperator(oper.login, oper.pwd, oper.name, out newOperInfo);

			switch (res)
			{
				case CreateOperatorResult.Success:
					return newOperInfo;
				case CreateOperatorResult.InvalidLogin:
					AddMessage(new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = string.Format(GetMessageData("InvalidLogin"), oper.login)
					});
					return null;
				case CreateOperatorResult.LoginAlreadyExists:
					AddMessage(new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = string.Format(GetMessageData("LoginAlreadyExists"), oper.login)
					});
					return null;
				case CreateOperatorResult.InvalidPassword:
					AddMessage(new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = GetMessageData("WrongPassword")
					});
					return null;
				case CreateOperatorResult.EmailBelongsToAnotherOperator:
					AddMessage(new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = GetMessageData("EmailBelongsToAnotherOperator")
					});
					return null;
				case CreateOperatorResult.PhoneAlreadyRegistered:
					AddMessage(new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = GetMessageData("PhoneAlreadyRegistered")
					});
					return null;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		private Message GetMessage(SetNewLoginResult res, string login)
		{
			switch (res)
			{
				case SetNewLoginResult.Success:
					return null;
				case SetNewLoginResult.InvalidLogin:
					return new Message
					{
						Severity = Severity.Warning,
						MessageTitle = GetMessageData("ServerError"),
						MessageBody = string.Format(GetMessageData("InvalidLogin"), login)
					};
				case SetNewLoginResult.LoginAlreadyExists:
					return new Message
						{
							Severity = Severity.Warning,
							MessageTitle = GetMessageData("ServerError"),
							MessageBody = string.Format(GetMessageData("LoginAlreadyExists"), login)
						};
				case SetNewLoginResult.Forbidden:
					return new Message
						{
							Severity = Severity.Warning,
							MessageTitle = GetMessageData("ServerError"),
							MessageBody = GetMessageData("LoginChangeForbidden")
						};
				case SetNewLoginResult.EmailBelongsToAnotherOperator:
					return new Message
						{
							Severity = Severity.Warning,
							MessageTitle = GetMessageData("ServerError"),
							MessageBody = GetMessageData("EmailBelongsToAnotherOperator")
						};
				case SetNewLoginResult.LoginAsEmailDoesNotMatchToEmail:
					return new Message
						{
							Severity = Severity.Warning,
							MessageTitle = GetMessageData("ServerError"),
							MessageBody = GetMessageData("LoginAsEmailDoesNotMatchToEmail")
						};
				case SetNewLoginResult.InvalidEmailFormat:
					return new Message
						{
							Severity = Severity.Warning,
							MessageTitle = GetMessageData("ServerError"),
							MessageBody = GetMessageData("InvalidEmailFormat")
						};
				default:
					throw new ArgumentOutOfRangeException("res", res, @"Value is not supported");
			}
		}
		private SetNewLoginResult SaveOperatorChanges(Operator oper)
		{
			var res = IWebPS.SaveOperatorInfo(oper);
			var message = GetMessage(res, oper.login);
			if (message != null)
				AddMessage(message);
			return res;
		}
		private void ChangeOperatorVehiclesRights(Operator oper)
		{
			IWebPS.ChangeOperatorVehicleRights(oper);
		}
		[JsonPageAction("getOperatorsList", Method = HttpMethod.Get)]
		public Dictionary<string, object> GetOperatorsList()
		{
			return new Dictionary<string, object>
			{
				{ "allowAddOperator", IWebPS.AllowAddOperator        },
				{ "operators",        IWebPS.GetAllBelongOperators() },
				{ "friends",          IWebPS.GetFriends()            },
				{ "partners",         IWebPS.GetPartners()           },
			};
		}
		[JsonPageAction("get-operator-details", Method = HttpMethod.Get)]
		public Operator GetOperatorDetails(int? operatorId = null)
		{
			if (operatorId == null)
				operatorId = OperatorId;
			if (operatorId == null)
				throw new InvalidOperationException("OperatorId is not assigned");

#if !DEBUG
			CurrentBillingAccountExistsAndCreateIfNeeded();
#endif

			return IWebPS.GetOperatorDetails(operatorId.Value);
		}
		[JsonPageAction("get-operator-rights", Method = HttpMethod.Get)]
		public Operator GetOperatorRights(int operatorId)
		{
			return IWebPS.GetOperatorRights(operatorId);
		}
		[JsonPageAction("get-operator-vehicle-rights", Method = HttpMethod.Get)]
		public Operator GetOperatorVehicleRights(int? operatorId = null)
		{
			return IWebPS.GetOperatorVehicleRights(operatorId ?? IWebPS.OperatorId);
		}
		[JsonPageAction("get-operator-zone-rights", Method = HttpMethod.Get)]
		public Operator GetOperatorZoneRights(int? operatorId = null)
		{
			return IWebPS.GetOperatorZoneRights(operatorId ?? IWebPS.OperatorId);
		}
		[PageAction("getOperatorRights")]
		public void GetOperatorRights()
		{
			JsonResponseSerializationBeg();
			try
			{
				var sOperatorId = GetParamValue("operator_id");
				int oid;
				if (string.IsNullOrWhiteSpace(sOperatorId))
				{
					oid = IWebPS.OperatorId;
				}
				else
				{
					if (!int.TryParse(sOperatorId, out oid))
						throw new ArgumentException(@"Unable to parse operator_id: " + sOperatorId);
				}

				var sCurrentTab = GetParamValue("tab");
				Tabs activeTab;
				if (!Enum.TryParse(sCurrentTab, out activeTab))
					throw new ArgumentException(@"Unable to parse tab: " + sCurrentTab);

				switch (activeTab)
				{
					case Tabs.Base:
						JsonResponse.value = IWebPS.GetOperatorDetails(oid);
						break;
					case Tabs.Vehicles:
						JsonResponse.value = IWebPS.GetOperatorVehicleRights(oid);
						break;
					case Tabs.Zones:
						JsonResponse.value = IWebPS.GetOperatorZoneRights(oid);
						break;
					case Tabs.OperatorRights:
						JsonResponse.value = IWebPS.GetOperatorRights(oid);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				JsonResponse.resCode = ResponseCode.OK;
				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		[PageAction("SaveOperatorZoneRights")]
		public void SaveOperatorZoneRights()
		{
			JsonResponseSerializationBeg();
			try
			{
				var jsonData = GetParamValue("jsonData");
				var fromJson = JsonHelper.DeserializeObjectFromJson<Operator>(jsonData);
				if (fromJson != null)
				{
					var oper = (Operator)fromJson;

					IWebPS.ChangeOperatorZoneRights(oper);
					JsonResponse.value   = oper;
					JsonResponse.resCode = ResponseCode.OK;
				}

				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		[PageAction("SaveOperatorVehicleRights")]
		public void SaveOperatorVehicleRights()
		{
			JsonResponseSerializationBeg();
			try
			{
				var jsonData = GetParamValue("jsonData");
				var fromJson = JsonHelper.DeserializeObjectFromJson<Operator>(jsonData);
				if (fromJson != null)
				{
					var oper = fromJson;
					ChangeOperatorVehiclesRights(oper);
					JsonResponse.value = oper;
					JsonResponse.resCode = ResponseCode.OK;
				}
				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		[PageAction("SaveOperatorRights")]
		public void SaveOperatorRights()
		{
			JsonResponseSerializationBeg();
			try
			{
				var jsonData = GetParamValue("jsonData");
				var fromJson = JsonHelper.DeserializeObjectFromJson<Operator>(jsonData);
				if (fromJson != null)
				{
					var oper = fromJson;
					IWebPS.SaveOperatorRights(oper);
					JsonResponse.value   = oper;
					JsonResponse.resCode = ResponseCode.OK;
				}
				JsonResponseSerializationEnd();
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
				JsonResponse.resCode = ResponseCode.ERROR;
				JsonResponseSerializationEnd();
			}
		}
		#endregion OperatorRights
		[JsonPageAction("who-can-see-vehicle", Method = HttpMethod.Get)]
		public List<Operator> WhoCanSeeVehicle(int vehicleId)
		{
			return IWebPS.WhoCanSeeVehicle(vehicleId);
		}
		[JsonPageAction("who-can-see-vehicle-group", Method = HttpMethod.Get)]
		public List<Operator> WhoCanSeeVehicleGroup(int vehicleGroupId)
		{
			return IWebPS.WhoCanSeeVehicleGroup(vehicleGroupId);
		}
		[PageAction("disallow-vehicle-access", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DisallowVehicleAccess(int friendOperatorId, int vehicleId)
		{
			IWebPS.DisallowVehicleAccess(friendOperatorId, vehicleId);
		}
		[PageAction("propose-view-vehicle", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void ProposeViewVehicle(int friendOperatorId, int vehicleId)
		{
			IWebPS.ProposeViewVehicle(friendOperatorId, vehicleId);
		}
		/// <summary> 17.6. Отправка запроса другу с целью получить доступ его местоположения </summary>
		/// <param name="msisdn"> Номер телефона друга в международном формате </param>
		/// <param name="msisdnName"> Имя друга, опционально </param>
		/// <returns> Результат операции </returns>
		[JsonPageAction("propose-friendship", Method = HttpMethod.Post)]
		public ProposeFriendshipResult ProposeFriendship(string msisdn, string msisdnName)
		{
			var result = IWebPS.ProposeFriendship(msisdn);

			UpdateMsisdnName(msisdn, msisdnName);

			return result;
		}
		[PageAction("recall-view-vehicle-proposation", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void RecallViewVehicleProposation(int friendOperatorId, int vehicleId)
		{
			IWebPS.RecallViewVehicleProposation(friendOperatorId, vehicleId);
		}
		[JsonPageAction("find-users", Method = HttpMethod.Get)]
		public List<Operator> FindUsers(string contact)
		{
			return IWebPS.FindUsers(contact);
		}
		[PageAction("friend", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void AddFriend(int operatorId)
		{
			IWebPS.AddFriend(operatorId);
		}
		[PageAction("partner", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void AddPartner(int departmentId)
		{
			IWebPS.AddPartner(departmentId);
		}
		[PageAction("friend", Method = HttpMethod.Delete, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		[PageAction("delete-friend", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteFriend(int operatorId)
		{
			IWebPS.DeleteFriend(operatorId);
		}
		[PageAction("partner", Method = HttpMethod.Delete, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		[PageAction("delete-partner", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeletePartner(int departmentId)
		{
			IWebPS.DeletePartner(departmentId);
		}
		[PageAction("set-access-to-vehicle", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void SetAccessToVehicle(int operatorId, int vehicleId, SystemRight right, bool allowed)
		{
			IWebPS.SetReplacedAccessToVehicle(operatorId, vehicleId, right, allowed);
		}
		[PageAction("delete-access-to-vehicle", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteAccessToVehicle(int operatorId, int vehicleId, SystemRight right)
		{
			IWebPS.DelReplacedAccessToVehicle(operatorId, vehicleId, right);
		}
		[JsonPageAction("set-msisdn-access-to-vehicle", Method = HttpMethod.Post)]
		[Obsolete("Необходимо использовать 'set-access-to-vehicle'")]
		public SetAccessToVehicleResult SetMsisdnAccessToVehicle(string msisdn, int vehicleId, SystemRight right, bool allowed = true, string msisdnName = null)
		{
			var result = IWebPS.SetReplacedAccessToVehicle(msisdn, vehicleId, right, allowed);
			UpdateMsisdnName(msisdn, msisdnName);
			return result;
		}
		/// <summary> Изменить имя для номера телефона </summary>
		/// <param name="msisdn"></param>
		/// <param name="msisdnName"></param>
		private void UpdateMsisdnName(string msisdn, string msisdnName)
		{
			if (string.IsNullOrWhiteSpace(msisdnName))
				return;
			IWebPS.UpdatePhoneBookContactName(msisdn, msisdnName);
		}
		[PageAction("delete-msisdn-access-to-vehicle", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		[Obsolete("Необходимо использовать 'delete-access-to-vehicle'")]
		public void DelMsisdnAccessToVehicle(string msisdn, int vehicleId, SystemRight? right = null)
		{
			IWebPS.DelReplacedAccessToVehicle(msisdn, vehicleId, right);
		}
		/// <summary> 17.7. Получение списка пользователей, имеющих доступ к заданному объекту наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <returns> Возвращает коллекцию DTO.Operator </returns>
		[JsonPageAction("get-accesses-to-vehicle", Method = HttpMethod.Get)]
		public List<Operator> GetAccessesToVehicle(int vehicleId)
		{
			return IWebPS.GetAccessesToVehicle(vehicleId);
		}
		[JsonPageAction("get-accesses-to-vehicle-group", Method = HttpMethod.Get)]
		public List<Operator> GetAccessesToVehicleGroup(int vehicleGroupId)
		{
			return IWebPS.GetAccessesToVehicleGroup(vehicleGroupId);
		}
		[PageAction("set-access-to-vehicle-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void SetAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight right, bool allowed)
		{
			IWebPS.SetReplacedAccessToVehicleGroup(operatorId, vehicleGroupId, right, allowed);
		}
		[PageAction("delete-access-to-vehicle-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight right)
		{
			IWebPS.DelReplacedAccessToVehicleGroup(operatorId, vehicleGroupId, right);
		}
		[PageAction("set-access-to-zone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void SetAccessToZone(int operatorId, int zoneId, SystemRight right, bool allowed)
		{
			IWebPS.SetAccessToZone(operatorId, zoneId, right, allowed);
		}
		[PageAction("delete-access-to-zone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteAccessToZone(int operatorId, int zoneId, SystemRight right)
		{
			IWebPS.DeleteAccessToZone(operatorId, zoneId, right);
		}
		[PageAction("set-access-to-zone-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void SetAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right, bool allowed)
		{
			IWebPS.SetAccessToZoneGroup(operatorId, zoneGroupId, right, allowed);
		}
		[PageAction("delete-access-to-zone-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right)
		{
			IWebPS.DeleteAccessToZoneGroup(operatorId, zoneGroupId, right);
		}
		[JsonPageAction("get-accessible-rights-on-vehicle", Method = HttpMethod.Get)]
		public SystemRight[] GetAccessibleRightsOnVehicleV1()
		{
			return IWebPS.GetReplacedAccessOnVehicle();
		}
		[JsonPageAction("get-accessible-rights-on-vehicle-v2", Method = HttpMethod.Get)]
		public DictionaryEnum<SystemRight>[] GetAccessibleRightsOnVehicleV2()
		{
			return GetAccessibleRightsOnVehicleV1()
				.Select(r => r.ToDictionaryEnum(IWebPS.CultureInfo))
				.ToArray();
		}
		[JsonPageAction("get-accessible-rights-on-zone", Method = HttpMethod.Get)]
		public SystemRight[] GetAccessibleRightsOnZoneV1()
		{
			return IWebPS.GetAccessibleRightsOnZone();
		}
		[JsonPageAction("get-accessible-rights-on-zone-v2", Method = HttpMethod.Get)]
		public DictionaryEnum<SystemRight>[] GetAccessibleRightsOnZoneV2()
		{
			return GetAccessibleRightsOnZoneV1()
				.Select(r => r.ToDictionaryEnum(IWebPS.CultureInfo))
				.ToArray();
		}
		[JsonPageAction("get-operator-accesses-on-vehicles", Method = HttpMethod.Get)]
		public Operator GetOperatorAccessesOnVehicles(int operatorId)
		{
			return IWebPS.GetOperatorVehicleRights(operatorId);
		}
		[JsonPageAction("get-operator-accesses-on-zones", Method = HttpMethod.Get)]
		public Operator GetOperatorAccessesOnZones(int operatorId)
		{
			return IWebPS.GetOperatorZoneRights(operatorId);
		}
		[Obsolete("Vehicles/Default.aspx.Delete")]
		[PageAction("ignore-vehicle", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void IgnoreVehicle(int vehicleId, bool ignored)
		{
			IWebPS.IgnoreVehicle(vehicleId, ignored);
		}
		[PageAction("ignore-vehicle-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void IgnoreVehicleGroup(int vehicleGroupId, bool ignored)
		{
			IWebPS.IgnoreVehicleGroup(vehicleGroupId, ignored);
		}
		[PageAction("ignore-zone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void IgnoreZone(int zoneId, bool ignored)
		{
			IWebPS.IgnoreZone(zoneId, ignored);
		}
		[PageAction("ignore-zone-group", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void IgnoreZoneGroup(int zoneGroupId, bool ignored)
		{
			IWebPS.IgnoreZoneGroup(zoneGroupId, ignored);
		}
		[JsonPageAction("get-service-types-allowed-to-add", Method = HttpMethod.Get)]
		public List<BillingServiceType> GetServiceTypesAllowedToAdd(int vehicleId)
		{
			return IWebPS.GetServiceTypesAllowedToAdd(vehicleId);
		}
		[PageAction("add-service", Method = HttpMethod.Post, RequestType = DataType.Json,ResponseType = DataType.Empty)]
		public void AddService(int vehicleId, int billingServiceTypeId)
		{
			IWebPS.AddService(vehicleId, billingServiceTypeId);
		}
		[PageAction("delete-service", Method = HttpMethod.Post, RequestType = DataType.Json,ResponseType = DataType.Empty)]
		public void DeleteService(int billingServiceId)
		{
			IWebPS.DelService(billingServiceId);
		}
		[PageAction("add-blocking", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void AddBlocking(int vehicleId)
		{
			IWebPS.AddBlocking(vehicleId);
		}
		[PageAction("delete-blocking", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void DeleteBlocking(int vehicleId)
		{
			IWebPS.DelBlocking(vehicleId);
		}
		[JsonPageAction("lock-department", Method = HttpMethod.Post)]
		public BusinessLogic.DTO.Department LockDepartment(int departmentId)
		{
			return IWebPS.LockDepartment(departmentId);
		}
		[JsonPageAction("unlock-department", Method = HttpMethod.Post)]
		public BusinessLogic.DTO.Department UnlockDepartment(int departmentId)
		{
			return IWebPS.UnlockDepartment(departmentId);
		}
	}
}