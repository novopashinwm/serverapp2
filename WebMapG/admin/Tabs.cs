﻿namespace FORIS.TSS.UI.WebMapG.admin
{
	public enum Tabs
	{
		Base           = 1,
		Vehicles       = 2,
		Zones          = 3,
		OperatorRights = 4,
	}
}