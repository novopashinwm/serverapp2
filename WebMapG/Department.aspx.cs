﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using FORIS.TSS.UI.WebMapG.Old_App_Code;

namespace FORIS.TSS.UI.WebMapG
{
	public class Department : BasePage
	{
		[PageAction("default", Method = HttpMethod.Get, ResponseType = DataType.Json)]
		public BusinessLogic.DTO.Department Default()
		{
			return IWebPS.Department;
		}
		[PageAction("switchTo")]
		public void SwitchTo()
		{
			var backUrl = Request.ApplicationPath + "/";
			if (Request.UrlReferrer != null && Request.UrlReferrer != Request.Url)
				backUrl = Request.UrlReferrer.ToString();
			SwitchToAndGo(backUrl);
		}
		[PageAction("switchToAndGo")]
		public void SwitchToAndGo()
		{
			var backUrl = GetParamValue("backUrl");
			SwitchToAndGo(backUrl);
		}
		private void SwitchToAndGo(string backUrl)
		{
			if (IsMobile && !Request.IsAjaxRequest())
			{
				Response.Redirect(backUrl, true);
				return;
			}
			var departmentId = int.Parse(GetParamValue("departmentId"));

			if (TrySelectDepartment(departmentId))
			{
				var cookie = new HttpCookie("SelectedDepartmentId")
				{
					Value   = departmentId.ToString(CultureInfo.InvariantCulture),
					Expires = DateTime.Now.AddDays(10)
				};
				Response.Cookies.Add(cookie);

				SetObjectToSession("Allow_Vehicles", LoadVehicles());

				if (Request.IsAjaxRequest())
				{
					EndResponseAndReturnJson(backUrl);
				}
				else
					Response.Redirect(backUrl, true);
			}
			else
			{
				EndResponseAndReturnJson(false);
			}
		}
		[PageAction("requestCorporativeSubmit")]
		public void CreateSwitchingToCorporativeRequest()
		{
			var message = IWebPS.SendCustomerTypeChangeRequest();

			RenderSwitchToCorporativePage(message);
		}
		[PageAction("requestCorporativeForm")]
		public void SwitchToCorporative()
		{
			if (IWebPS.Department == null ||
				IWebPS.Department.TypeIsChangeable == null ||
				!IWebPS.Department.TypeIsChangeable.Value)
				return;

			var alreadySentMessage = IWebPS
				.GetMessages(MessageType.Outgoing, MessageTemplate.SwitchToCorporativeRequest)
				.OrderBy(m => m.Time)
				.FirstOrDefault();

			RenderSwitchToCorporativePage(alreadySentMessage);
		}
		[PageAction("approveCorporativeForm")]
		public void ApproveCorporativeForm()
		{
			if (IWebPS.Department == null)
				AddMessage(new Message(Severity.Error, RM.GetString("OperationIsNotApplicable", CurrentCulture), string.Empty));

			TemplateFile = "department";
			AddProperty("template", "approveCorporativeForm");

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[PageAction("approveCorporativeSubmit")]
		public void ApproveCorporativeSubmit()
		{
			IWebPS.ChangeDepartmentType(DepartmentType.Corporate);

			ApproveCorporativeForm();
		}
		private void RenderSwitchToCorporativePage(Message alreadySentMessage)
		{
			TemplateFile = "department";
			AddProperty("template", "requestCorporativeForm");

			if (alreadySentMessage != null)
				AddProperty("sentDate", TimeZoneInfo.ConvertTimeFromUtc(alreadySentMessage.Time, TimeZoneInfo).ToString(TimeHelper.DefaultTimeFormatUpToMinutes));

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[PageAction("get-department-rights", Method = HttpMethod.Get, ResponseType = DataType.Json)]
		public List<SystemRight> GetDepartmentRights()
		{
			return IWebPS.GetDepartmentRights();
		}
		[PageAction("name", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void SetCustomerName(string value)
		{
			IWebPS.SetDepartmentName(value);
		}
		[PageAction("extID", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void SetCustomerExtID(string value)
		{
			IWebPS.SetDepartmentExtID(value);
		}
		[PageAction("description", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void SetCustomerDescription(string value)
		{
			IWebPS.SetDepartmentDescription(value);
		}
	}
}