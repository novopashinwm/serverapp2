﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common.Http;
using FORIS.TSS.UI.WebMapG;

public partial class Logout : BaseLoginPage
{
	protected override void Page_Load(object sender, EventArgs e)
	{
		MandatoryPageLoad();

		if (Page.IsPostBack) return;
		
		Process();
		UpdateSessionDateTime();
	}

	private static readonly Dictionary<string, string> SuccessfulLogoutResult = new Dictionary<string, string>
	{
		{"status", "true"}
	};

	[PageAction("default", ResponseType = DataType.Json)]
	public Dictionary<string, string> LogoutAjax()
	{
		LogOutInternal();

		return SuccessfulLogoutResult;
	}
}