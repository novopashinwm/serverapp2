﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Web.Security;
using DTO;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.Infrastructure.Authentication;
using RU.NVG.UI.WebMapG.Model.Common;
using LoginResult = RU.NVG.UI.WebMapG.Model.Common.LoginResult;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	public class Login : BaseLoginPage
	{
		string _login    = "";
		string _password = "";

		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();
			if (!Page.IsPostBack)
			{
				Process();
				UpdateSessionDateTime();
			}
		}
		[PageAction()]
		public void ShowLoginForm()
		{
			RenderLoginPage();
		}
		[PageAction("question-support", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void QuestionSupport(string clientName, string trackerNumber, string question, string clientEmail, string clientPhone)
		{
			var clientContacts = clientEmail + " " + clientPhone;
			if (IsAuthorized)
			{
				IWebPS.SendQuestionToSupport(clientContacts, trackerNumber, question);
				return;
			}
			WebLoginProvider.SendQuestionToSupport(clientName, trackerNumber, clientContacts, question);
		}
		[PageAction("logoutajax", ResponseType = DataType.Json)]
		public Dictionary<string, string> LogoutAjax()
		{
			var redirectUrl = SessionSwitched && Request.UrlReferrer != null ? Request.UrlReferrer.PathAndQuery : MainPageUrl;
			LogOutInternal();

			return new Dictionary<string, string>
			{
				{ "status",      "true"      },
				{ "redirectUrl", redirectUrl },
			};
		}
		/// <summary> Выполним вход в систему пользователем и перенаправим его на страницу карты </summary>
		[PageAction("login")] 
		[PageAction("loginajax")]
		public virtual void LoginUser(string a)
		{
			_login    = GetParamValue("login");
			_password = GetParamValue("password");
			LogIn(_login, _password, a);
		}
		[PageAction("loginByOperatorId", ResponseType = DataType.Json)]
		public Dictionary<string, string> LogInAsUser(string operatorId, string reason)
		{
			if (!IWebPS.IsSuperAdministrator && !IWebPS.IsSalesManager)
				throw new SecurityException("Access denied");

			int id;
			if (!int.TryParse(operatorId, out id))
				return new Dictionary<string, string>
				{
					{ "result",      LoginResult.UserNotFound.ToString() },
					{ "redirectUrl", MainPageUrl                         },
				};

			if (!IWebPS.IsSuperAdministrator && IWebPS.IsSalesManager &&
				!IWebPS.GetAvailableDepartments()
				.SelectMany(d => IWebPS.GetDepartmentOperators(d.id))
				.Any(o => o.id == id))
				throw new SecurityException("Access denied");

			var sessionId        = IWebPS.SessionInfo.SessionId;
			var initOperatorId   = IWebPS.OperatorId;
			var initDepartmentId = IWebPS.Department != null ? IWebPS.Department.id : (int?) null;
			LogOutInternal();

			var result = TryLogIn(id, reason, initOperatorId, initDepartmentId);
			if (result == LoginResult.Success)
				IWebPS.SetSessionParameters(new SessionParameters
				{
					Reason          = reason,
					ParentSessionId = sessionId
				});

			return new Dictionary<string, string>
			{
				{ "result",      result.ToString() },
				{ "redirectUrl", MainPageUrl       },
			};
		}
		[PageAction("getmyinfo", Method = HttpMethod.Get, RequestType = DataType.Empty, ResponseType = DataType.Json)]
		public Dictionary<string, string> GetMyInfoAjax()
		{
			string userName = IWebPS.SessionInfo.OperatorInfo.Name;
			const string userPhoto = "default";
			var phones = IWebPS.GetOperatorPhones(IWebPS.OperatorId);
			var emails = IWebPS.GetOperatorEmails(IWebPS.OperatorId);

			var contactPhone = (from i in phones where i.IsConfirmed ?? false select i.Value).FirstOrDefault();
			var contactEmail = (from i in emails where i.IsConfirmed ?? false select i.Value).FirstOrDefault();
#if !DEBUG
			CurrentBillingAccountExistsAndCreateIfNeeded();
#endif
			return new Dictionary<string, string>
			{
				{ "login",        IWebPS.SessionInfo.OperatorInfo.Login },
				{ "userPhoto",    userPhoto                             },
				{ "userName",     userName                              },
				{ "contactEmail", contactEmail                          },
				{ "contactPhone", contactPhone                          },
			};
		}
		/// <summary> Для логина пользователя через ajax </summary>
		[PageAction("relogin")]
		public virtual void ReLoginUser()
		{
			SetJsonContentType();
			if (GetParamValue("demo") == "1")
			{
				_login    = GetMessageData("demoLogin");
				_password = GetMessageData("demoPassword");
			}
			else
			{
				_login    = GetParamValue("login");
				_password = GetParamValue("password");
			}

			LoginResult res = TryLogIn(_login, _password);

			var dto = new DTO.LoginResult();
		
			switch (res){
				case LoginResult.Success:
					dto.success = true;
					break;
				case LoginResult.WrongPassword:
					dto.message = GetMessageData("WrongPassword");
					break;
				case LoginResult.SessionAlreadyExists:
					dto.message =  GetMessageData("SessionAlreadyExists");
					break;
				case LoginResult.CannotConnectToServer:
					dto.message = GetMessageData("CannotConnectToServer");
					break;
				case LoginResult.Error:
					dto.message = GetMessageData("error");
					break;
				case LoginResult.NoAvailableVehicles:
					dto.message = "Ошибка авторизации! Нет доступных объектов для слежения!";
					break;
			}

			string resStr = JsonHelper.SerializeObjectToJson(new CommonDTOResponse(dto));
			Response.Write(resStr);
		}
		/// <summary> Выполним вход в систему пользователя и перенаправим его на страницу карты </summary>
		[PageAction("demo")]
		public virtual void LoginDemo()
		{
			_login    = GetMessageData("demoLogin");
			_password = GetMessageData("demoPassword");

			LogIn(_login, _password);
		}
		protected LoginResult TryLogInBySimId(string simId, string name)
		{
			ServerApplication.Server.Instance().AcceptRequestWithLimit(BanLimit.TryLogin, Request.UserHostAddress);

			var webLoginProvider = WebLoginProvider;

			if (!webLoginProvider.SimIdExists(simId))
				return LoginResult.UserNotFound;

			var operatorId = webLoginProvider.GetOperatorIdBySimId(simId, name);
			if (operatorId == null)
				return LoginResult.Wait;

			var loginResult = TryLogIn(operatorId.Value);

			return loginResult;
		}
		protected LoginResult TryLogIn(string userName, string password)
		{
			ServerApplication.Server.Instance().AcceptRequestWithLimit(BanLimit.TryLogin, Request.UserHostAddress);

			if (userName == null)
				throw new ArgumentNullException("userName");
			userName = userName.Trim();

			CheckPasswordResult result;
			try
			{
				string clientName;

				clientName = ConfigurationManager.AppSettings["Client"];

				if (string.IsNullOrEmpty(clientName))
				{
					return LoginResult.WrongPassword;
				}

				var gi = new GenericIdentity(userName);
				Thread.CurrentPrincipal = new GenericPrincipal(gi, null);

				var webLoginProvider = WebLoginProvider;

				//Количество записей в базе данных об операторе д.б.= 1
				result = webLoginProvider.CheckPasswordForWeb(userName, password);

				if (result == null)
					return LoginResult.WrongPassword;

				if (result.IsLocked)
					return LoginResult.AccessLocked;
			}
			catch (InvalidOperationException ex)
			{
				WriteLog(ex);
				return LoginResult.SessionAlreadyExists;
			}
			catch (SessionExistException ex)
			{
				WriteLog(ex);
				return LoginResult.SessionAlreadyExists;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				Session.Clear();
				return LoginResult.Error;
			}

			var loginResult = TryLogIn(result.OperatorId);

			if (loginResult == LoginResult.Success &&
				result.OneTimePassword)
			{
				ForceChangePassword();
			}

			return loginResult;
		}
		protected void LogIn(string userName, string password, string typelogin = "login")
		{
			ForcedToChangePassword = false;
			ForcedToSetEmail       = false;
			ForcedToSetPhone       = false;

			LoginResult res = TryLogIn(userName, password);

			if (res != LoginResult.Success)
			{
				if(typelogin == "loginajax")
				{
					JsonResponseSerializationBeg();
					ProcessedLoginAjaxError(res);
					JsonResponseSerializationEnd();
				}
				else
				{
					ProcessLoginError(res);
				}
				return;
			}

			ProcessLoginResult(res, typelogin);
		}
		protected void ProcessLoginResult(LoginResult res, string typelogin)
		{
			if (typelogin == "login" && !IsMobile)
				FormsAuthentication.RedirectFromLoginPage(_login, false);

			if (res == LoginResult.Success)
			{
				var departmentIdString = GetParamValue("departmentId");

				int departmentId;
				if (int.TryParse(departmentIdString, out departmentId))
				{
					try
					{
						IWebPS.SelectDepartment(departmentId);
						DepartmentId = departmentId;
					}
					catch (SecurityException)
					{
						var departmentCookie = Response.Cookies.Get("SelectedDepartmentId");
						if (departmentCookie != null)
							departmentCookie.Expires = DateTime.Today.AddDays(-1);
					}
				}
			}

			string redirectUrl;
			if (IsMobile)
			{
				//RedirectResponse(@"mobile/Default.aspx?a=getvehicles&includeAddresses=true");

				redirectUrl = "Default.aspx?a=getvehicles&includeAddresses=true";
				if (Request.UrlReferrer!=null && Request.UrlReferrer.ToString().Contains("backUrl="))
				{
					redirectUrl = Request.UrlReferrer.ToString().Substring(Request.UrlReferrer.ToString().IndexOf("backUrl=", StringComparison.Ordinal)).Replace("backUrl=", "");
				}
			}
			else
			{
				string showMode = "";
				if (Request.Cookies["showmode"] != null)
				{
					showMode = Request.Cookies["showmode"].Value;
				}
				if (ForcedToChangePassword)
				{
					redirectUrl = "settings.aspx";
				}
				else if(IWebPS.IsSuperAdministrator)
					redirectUrl = "customer.aspx";
				else
				{
					var defaultPageAfterLogin = ConfigurationManager.AppSettings["DefaultPageAfterLogin"];
					if (!string.IsNullOrWhiteSpace(defaultPageAfterLogin))
						redirectUrl = defaultPageAfterLogin;
					else
						redirectUrl = @"map.aspx" + (!string.IsNullOrWhiteSpace(showMode) ? @"?show=" + showMode : string.Empty);
				}
			}

			if (typelogin == "login")
				RedirectResponse(redirectUrl);
			else
			{
				WrapJsonAction<object>(
					() => new Dictionary<string, string>
					{
						{ "result",      res.ToString() },
						{ "redirectUrl", redirectUrl    },
					});
			}
		}
		private class RequestFile
		{
			public string Name;
			public byte[] Bytes;
		}
		private RequestFile GetRequestFileBytes(int maxSize, params string[] allowedExtensions)
		{
			if (Request.Files.Count == 0)
				return null;
			for (var i = 0; i != Request.Files.Count; ++i)
			{
				var file = Request.Files[i];
				if (maxSize < file.ContentLength)
					continue;

				var ext = Path.GetExtension(file.FileName);
				if (string.IsNullOrWhiteSpace(ext))
					continue;
				ext = ext.TrimStart('.');
				if (allowedExtensions.Length != 0 &&
					(!allowedExtensions.Contains(ext, StringComparer.OrdinalIgnoreCase)))
					continue;

				var buffer = new byte[file.ContentLength];
				file.InputStream.Read(buffer, 0, buffer.Length);

				return new RequestFile { Name = file.FileName, Bytes = buffer };
			}

			return null;
		}
		[PageAction("calculate")]
		public void Calculate()
		{
			var customerForm = new CalculationRequest
			{
				VehicleQuantity       = GetIntegerParam("cars_quantity"),
				EmployeeQuantity      = GetIntegerParam("people_quantity"),
				Vehicles              = GetParamValue("vehicles"),
				InstallByMTSPartner   = GetBooleanParam("installing_in_partner_MTS"),
				Glonass               = GetBooleanParam("glonass"),
				FuelLevelSensor       = GetBooleanParam("fuel-level-sensor"),
				IsAlreadyMTSCustomer  = GetBooleanParam("is-already-mts-customer"),
				CompanyName           = GetParamValue("company-name"),
				City                  = GetParamValue("city"),
				UserName              = GetParamValue("user-name"),
				Email                 = GetParamValue("email"),
				Phone                 = GetParamValue("phone"),
				AdditionalInformation = GetParamValue("additional-information"),
			};

			var attachment = GetRequestFileBytes(100*1024, "doc", "xls", "docx", "xlsx", "pdf", "txt");
			if (attachment != null)
				customerForm.Attachment = new Attachment
				{
					Name  = attachment.Name,
					Bytes = attachment.Bytes
				};

			if (IsAuthorized)
			{
				IWebPS.SendCalculationRequest(customerForm);
				return;
			}

			WebLoginProvider.SendCalculationRequest(customerForm);
		}
		private int GetIntegerParam(string paramName)
		{
			var s = GetParamValue(paramName);

			int result;
			if (!int.TryParse(s, out result))
				return 0;

			return result;
		}
		private bool GetBooleanParam(string paramName)
		{
			return !string.IsNullOrWhiteSpace(GetParamValue(paramName));
		}
		[PageAction("sign_in", Method = HttpMethod.Post, ResponseType = DataType.Json)]
		public SignInResult SignIn(
			string login,
			string password,
			string dst,
			string appId,
			string gcmRegistrationId = null,
			string apnDeviceToken    = null,
			string clientVersion     = null,
			string sim_id            = null,
			string name              = null)
		{
			ClientVersion = clientVersion;
			var loginResult = string.IsNullOrWhiteSpace(sim_id) ? TryLogIn(login, password) : TryLogInBySimId(sim_id, name);
			if (loginResult == LoginResult.Success)
			{
				string apnDeviceTokenHexString;
				var debug = false;
				if (string.IsNullOrWhiteSpace(apnDeviceToken))
				{
					apnDeviceTokenHexString = null;
				}
				else
				{
					var apnDeviceTokenBytes = Convert.FromBase64String(apnDeviceToken);
					apnDeviceTokenHexString = apnDeviceTokenBytes.ToHexString();
					debug = IsDebugClientVersion(clientVersion);
				}
				IWebPS.UpdateClientRegistration(appId, gcmRegistrationId, apnDeviceTokenHexString, debug);
			}

			var result = new SignInResult { Result = loginResult };
			if (loginResult == LoginResult.Success)
			{
				result.SimId            = IWebPS.GetOrCreateSimId();
				result.Rights           = OperatorSystemRights.ToArray();
				result.OperatorId       = IWebPS.OperatorId;
				result.DepartmentRights = IsAuthorized ? IWebPS.GetDepartmentRights().ToArray() : null;
				result.VehicleId        = IWebPS.OwnVehicleId;
				result.AppId            = IWebPS.AppId;
			}
			return result;
		}
		[JsonPageAction("set-apn-device-token", Method = HttpMethod.Post)]
		public void SetApnDeviceToken(string appId, string value, bool debug)
		{
			var contactType = debug ? ContactType.iOsDebug : ContactType.Apple;
			ServerApplication.Server.Instance().SetAppClientContact(appId, contactType, value);
		}
		[PageAction("UpdateClientRegistration", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void UpdateClientRegistration(string appId, string gcmRegistrationId = null, string apnDeviceToken = null)
		{
			var webPS = IWebPS;
			if (webPS == null)
			{
				SysDiags::Trace.TraceInformation("{0}.UpdateClientRegistration: IWebPS is null", GetType().Name);
				return;
			}

			string apnDeviceTokenHexString;
			var debug = false;
			if (string.IsNullOrWhiteSpace(apnDeviceToken))
			{
				apnDeviceTokenHexString = null;
			}
			else
			{
				var apnDeviceTokenBytes = Convert.FromBase64String(apnDeviceToken);
				if (apnDeviceTokenBytes != null)
				{
					apnDeviceTokenHexString = apnDeviceTokenBytes.ToHexString();
					var clientVersion       = webPS.SessionInfo.ClientVersion;
					debug                   = IsDebugClientVersion(clientVersion);
				}
				else
				{
					SysDiags::Trace.TraceWarning(
						"{0}.UpdateClientRegistration: apnDeviceTokenBytes is null, apnDeviceToken was {1}",
						GetType().Name, apnDeviceToken);
					apnDeviceTokenHexString = null;
				}
			}
			webPS.UpdateClientRegistration(appId, gcmRegistrationId, apnDeviceTokenHexString, debug);
		}
		private static bool IsDebugClientVersion(string clientVersion)
		{
			return clientVersion != null && clientVersion.EndsWith("debug", StringComparison.OrdinalIgnoreCase);
		}
	}
}