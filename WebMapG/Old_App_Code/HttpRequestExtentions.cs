﻿using System.Web;

namespace FORIS.TSS.UI.WebMapG.Old_App_Code
{
	public static class HttpRequestExtentions
	{
		public static bool IsAjaxRequest(this HttpRequest request)
		{
			if (request == null || request.ServerVariables["HTTP_X_REQUESTED_WITH"] == null)
				return false;
			return request.ServerVariables["HTTP_X_REQUESTED_WITH"].ToLower() == "xmlhttprequest";
		}
	}
}