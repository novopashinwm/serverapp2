﻿using System;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// AS: базовый класс для объектов, поддерживающих сериализацию в XML
/// PS. Поскольку (пока) нужен только в веб-приложении, в отдельную сборку не выносится
/// </summary>
[Serializable]
public partial class BaseSerializable
{
	public BaseSerializable()
	{
	}
	public virtual void WriteXml(XmlTextWriter writer)
	{
		new XmlSerializer(GetType()).Serialize(writer, this);
	}
}