﻿using System;
using System.Collections.Generic;
using System.Web;

namespace DTO
{
	/// <summary> Summary description for LoginResult </summary>
	public class LoginResult
	{
		public LoginResult()
		{
		}

		public bool   success = false;
		public string message = string.Empty;
	}
}