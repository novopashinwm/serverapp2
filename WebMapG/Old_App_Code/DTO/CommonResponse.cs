﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;

namespace DTO
{
	/// <summary> Summary description for CommonDTOResponse </summary>
	public class CommonDTOResponse
	{
		public CommonDTOResponse()
		{
		}
		public CommonDTOResponse(object value)
		{
			this.value = value;
		}
		public List<Message> messages;
		public ResponseCode resCode = ResponseCode.OK;
		public object value;
	}
	public enum ResponseCode
	{
		ERROR    = 0,
		OK       = 1,
		WARNING  = 2,
		NOT_AUTH = 3,
	}
}