﻿using System;
using System.Xml;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.UI.WebMapG
{
	/// <summary> Модель терминала, устанавливаемого на объект наблюдения </summary>
	[Serializable]
	public class Controller : BaseSerializable
	{
		protected string phone;
		protected int number;
		protected string type;
		public string DeviceId { get; set; }
		public string Phone
		{
			get { return phone; }
			set { phone = value; }
		}
		public int Number
		{
			get { return number; }
			set { number = value; }
		}
		/// <summary> IMSI симкарты </summary>
		public string Imsi { get; set; }
		/// <summary> пароль устройства </summary>
		public string Password { get; set; }
		public string IP { get; set; }
		public ControllerType Type { get; set; }
		public bool PhoneIsEditable { get; set; }
		public override void WriteXml(System.Xml.XmlTextWriter writer)
		{
			writer.WriteStartElement("Controller");
			writer.WriteAttributeString("Phone", this.Phone);
			writer.WriteAttributeString("PhoneIsEditable", XmlConvert.ToString(PhoneIsEditable));
			writer.WriteAttributeString("Number", this.Number.ToString());

			writer.WriteAttributeString("DeviceId", this.DeviceId);
			writer.WriteAttributeString("Password", this.Password);
			writer.WriteAttributeString("Imsi", this.Imsi);
			writer.WriteAttributeString("IP", IP);

			writer.WriteStartElement("Type");
			writer.WriteAttributeString("Name", Type.Name);
			writer.WriteAttributeString("UserFriendlyName", Type.UserFriendlyName);
			if (Type.HasInputs != null)
				writer.WriteAttributeString("HasInputs", XmlConvert.ToString(Type.HasInputs.Value));

			if (Type.SupportsPassword != null)
				writer.WriteAttributeString("SupportsPassword", XmlConvert.ToString(Type.SupportsPassword.Value));
			if (Type.SupportsDeviceId != null)
				writer.WriteAttributeString("SupportsDeviceId", XmlConvert.ToString(Type.SupportsDeviceId.Value));
			writer.WriteEndElement(); //Type

			writer.WriteEndElement(); //Controller
		}
	}
}