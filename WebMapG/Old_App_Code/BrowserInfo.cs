﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public enum BrowserTypes
{
    unknown,
    mobilePhone,
    pda,
    pc,
    searchEngineBot
}

public partial class MobilePlatforms
{
    public static String[] platforms = null;

    static MobilePlatforms()
    {
        platforms = new String[]{
            "symbian"
        };
    }

    public static bool isMobilePlatform(String platform)
    {
        bool res = false;
        for (int i = 0; i < MobilePlatforms.platforms.Length && !res; i++)
        {
            if (MobilePlatforms.platforms[i] == platform)
            {
                res = true;
            }
        }

        return res;
        //return true;
    }
}


/// <summary>
/// Summary description for BrowserInfo
/// </summary>
public class BrowserInfo
{
    public static BrowserTypes getBrowserType(HttpBrowserCapabilities browser)
    {
        BrowserTypes type = BrowserTypes.pc;
        if (browser.IsMobileDevice)
        {
            type = BrowserTypes.mobilePhone;
        }
        else if (browser.EcmaScriptVersion == null || MobilePlatforms.isMobilePlatform(browser.Platform.ToLower()))
        {
            type = BrowserTypes.pda;
        }
        else if (browser.Crawler)
        {
            type = BrowserTypes.searchEngineBot;
        }

        return type;
    }
}
