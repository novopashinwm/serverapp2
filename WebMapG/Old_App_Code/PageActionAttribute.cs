﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG
{
	/// <summary> Указывает, что данный метод будет вызываться при обнаружении в запросе параметра "a" равного полю Action </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class PageActionAttribute : Attribute
	{
		public string     Action        { get; set; }
		public DataType   RequestType   { get; set; }
		public DataType   ResponseType  { get; set; }
		public HttpMethod Method        { get; set; }
		public PageActionAttribute(string act = "default")
		{
			Action        = act;
		}
	}
}