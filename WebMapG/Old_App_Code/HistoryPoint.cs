﻿using System;
using System.Reflection;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using Interfaces.Web;

/// <summary> Объект сериализации точки истории </summary>

[Serializable]
[System.Xml.Serialization.XmlRoot("historyPoint")]
public class HistoryPoint : BaseSerializable
{
	public HistoryPoint()
	{
	}

	/// <summary>
	/// AS: Сериализация объекта в XML
	/// </summary>
	/// <param name="writer">XML writer</param>
	/// <param name="mobileUnit">объект сериализации</param>
	/// <param name="groupSerialNumber"></param>
	/// <param name="timeZoneInfo">Информация о часовом поясе клиента</param>
	public void Serialize(
		XmlWriter writer, 
		Vehicles.VehiclesInfo v, 
		IWebMobileInfoCP mobileUnit, 
		int groupSerialNumber, 
		TimeZoneInfo timeZoneInfo)
	{
		var nfi = new System.Globalization.NumberFormatInfo {NumberDecimalSeparator = "."};

		writer.WriteStartElement("unit");
		writer.WriteAttributeString("groupNumber", groupSerialNumber.ToString());
		writer.WriteAttributeString("angle", (mobileUnit.Course / 10).ToString());
		if (mobileUnit.Latitude >= 180.0 || mobileUnit.Longitude >= 180.0)
		{
			writer.WriteAttributeString("badPosition", "true");
		}

		Type t = mobileUnit.GetType();
		
		foreach (PropertyInfo propInfo in t.GetProperties())
		{
			Type propType = propInfo.PropertyType;
			if ((propType != typeof (String) && propType != typeof (int)) && propType != typeof (double)) 
				continue;

			Object val = propInfo.GetValue(mobileUnit, null);
			if (val == null) continue;

			// AS: пишем отдельным элементом в XML

			// AS: тестовая примочка для конвертации времени
			switch (propInfo.Name)
			{
				case "Time":
					{
						var dateTimeUTC = TimeHelper.GetDateTimeUTC((int)val);

						writer.WriteStartElement(propInfo.Name);
						writer.WriteString(TimeZoneInfo.ConvertTimeFromUtc(dateTimeUTC, timeZoneInfo).ToString("s"));
						writer.WriteEndElement();
						writer.WriteStartElement("TimeUtc");
						writer.WriteString(dateTimeUTC.ToString("s"));
						writer.WriteEndElement();
					}
					break;
				case "DigitalSensors":
					{
						if (v.Capabilities.Contains(DeviceCapability.IgnitionMonitoring))
						{
							writer.WriteStartElement(propInfo.Name);
							// AS: таким хитрожопым способом мы обработаем зажигание
							int sensors = -1;
							try
							{
								sensors = Convert.ToInt32(val);
							}
							catch (FormatException) { }
							catch (InvalidCastException) {}
							catch (ArgumentException) {}
							writer.WriteString((sensors & 0x20) == 0 ? "on" : "off");
							writer.WriteEndElement();
						}
					}
					break;
				default:
					writer.WriteStartElement(propInfo.Name);
					if ((propType == typeof(double)) || propType == typeof(float))
					{
						writer.WriteString(((double)(val)).ToString(nfi));
					}
					else
					{
						writer.WriteString(val.ToString());
					}
					writer.WriteEndElement();
					break;
			}
		}

		writer.WriteEndElement();
	}

	public void Serialize(
		XmlWriter writer, 
		Vehicles.VehiclesInfo v,
		GroupingMobileUnit mobUnit, 
		int serialNumber, 
		int prevUnitTime, 
		int nextUnitTime, 
		TimeZoneInfo timeZoneInfo)
	{
		GroupingMobileUnitNode firstNode = mobUnit.Nodes[0];
		GroupingMobileUnitNode lastNode = mobUnit.Nodes[mobUnit.Nodes.Count - 1];

		int d = (nextUnitTime > 0 ? nextUnitTime : lastNode.Time) - (prevUnitTime > 0 ? prevUnitTime : firstNode.Time);

		//для стоянок, чтобы не сбивать с толку пользователей, ставим время первой позиции равной предыдущей дате
		if (prevUnitTime > 0)
		{
			mobUnit.Nodes[0].MobileUnit.Time = prevUnitTime;
		}

		writer.WriteStartElement("point");
		writer.WriteAttributeString("type", mobUnit.GroupingType.ToString());
		writer.WriteAttributeString("duration", d.ToString());
		writer.WriteAttributeString("groupNumber", serialNumber.ToString());

		foreach (GroupingMobileUnitNode node in mobUnit.Nodes)
		{
			Serialize(writer, v, node.MobileUnit, serialNumber, timeZoneInfo);
		}
		writer.WriteEndElement();
	}
}