﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

public struct MapPictureWithOffset
{
	public int picNumX;
	public int picNumY;

	public int picOffsetX;
	public int picOffsetY;
}

/*public struct MyPoint
{
	public float x;
	public float y;
}*/

/// <summary>
/// AS: Описание масштаба карты
/// </summary>
public struct MapScale
{
	public int width;
	public int height;
	public float val;
	public PointF rightBottomPoint;
}

/// <summary>
/// AS: определяет описание карты
/// </summary>
[Serializable()]
public class MapDescription : BaseSerializable
{
	/// <summary>
	/// AS: названия карты. На разных языках
	/// </summary>
	protected Dictionary<String, String> names;

	protected String url;
	protected String shortName;
	protected String guid;

	protected int maxWidth;
	protected int maxHeight;

	protected List<MapScale> scales;

	protected bool current;

	public bool Current
	{
		get { return current; }
		set { current = value; }
	}

	public int MaxWidth
	{
		get
		{
			return Scales[Scales.Count - 1].width;
		}
	}

	public int MaxHeight
	{
		get
		{
			return Scales[Scales.Count - 1].height;
		}
	}

	public Dictionary<String, String> Names
	{
		get
		{
			if (names == null)
			{
				names = new Dictionary<String, String>(2);
			}
			return names;
		}
		set { throw new NotSupportedException(); }
	}

	/// <summary>
	/// AS: URL к карте
	/// </summary>
	public String Url
	{
		get { return url; }
		set { url = value; }
	}

	/// <summary>
	/// AS: короткое название карты - используется для передачи между клиентом и сервером, как параметр.
	/// Идентифицирует карту
	/// </summary>
	public String ShortName
	{
		get { return shortName; }
		set { shortName = value; }
	}

	/// <summary>
	/// Глобальный ID карты
	/// </summary>
	public String Guid
	{
		get { return guid; }
		set { guid = value; }
	}

	public List<MapScale> Scales
	{
		get { return scales; }
		set { throw new NotSupportedException(); }
	}

	/// <summary>
	/// AS: Вычисления связанные с картой и географическими координатами.
	/// Используются для мобильных устройств, которые не могут выполнять JavaScript
	/// </summary>
	#region Calculations

	public const int kx = 63166;
	public const int ky = 111411;
	public const int pixelsPerMeter = 3800;
	/*public const float origCenterX = 37.6216558848906f;
	public const float origCenterY = 55.751712922759f;*/

	protected float origCenterX;
	protected float origCenterY;

	protected float origCenterX2;
	protected float origCenterY2;

	protected float ox;
	protected float oy;

	protected float OffsetX
	{
		get { return ox; }
		set { ox = value; }
	}

	protected float OffsetY
	{
		get { return oy; }
		set { oy = value; }
	}

	public float OrigCenterX2
	{
		get { return origCenterX2; }
		set { origCenterX2 = value; }
	}

	public float OrigCenterY2
	{
		get { return origCenterY2; }
		set { origCenterY2 = value; }
	}

	public /*static*/ PointF GeoToPlanar(float geox, float geoy)
	{
		PointF planar = new PointF();
		planar.X = MapDescription.kx * (geox - origCenterX);
		planar.Y = MapDescription.ky * (geoy - origCenterY);

		return planar;
	}

	public /*static*/ PointF PlanarToGeo(float planarX, float planarY)
	{
		PointF geo = new PointF();
		geo.X = planarX / MapDescription.kx + origCenterX;
		geo.Y = planarY / MapDescription.ky + origCenterY;
		
		return geo;
	}

	public virtual PointF coordWindowToMap(PointF windowPoint, float fKWinToMap, float origCenterX2, float origCenterY2, int corrx, int corry)
	{
		PointF mapPoint = new PointF();
		PointF centerPlanar = GeoToPlanar(origCenterX2, origCenterY2);
		mapPoint.X = centerPlanar.X + (windowPoint.X + corrx) * fKWinToMap;
		mapPoint.Y = centerPlanar.Y - (windowPoint.Y + corry) * fKWinToMap;
		return mapPoint;
	}

	/// <summary>
	/// AS: Преобразует оконные координаты в WGS84
	/// </summary>
	/// <param name="windowPoint">Оконные координаты</param>
	/// <param name="fKWinToMap">Коэффициент масштаба</param>
	/// <param name="origCenterX2">Долгота текущего центра карты</param>
	/// <param name="origCenterY2">Широта текущего центра карты</param>
	/// <param name="corrx">Нах не нужен, передавать 0</param>
	/// <param name="corry">Нах не нужен, передавать 0</param>
	/// <returns></returns>
	public virtual PointF coordWindowToGeo(PointF windowPoint, float fKWinToMap, float origCenterX2, float origCenterY2, int corrx, int corry)
	{
		//PointF geoCoords = new PointF();
		PointF planar = coordWindowToMap(windowPoint, fKWinToMap, origCenterX2, origCenterY2, corrx, corry);
		return PlanarToGeo(planar.X, planar.Y);
	}

	/// <summary>
	/// AS: автоподбор масштаба для заданного экстента
	/// </summary>
	/// <param name="leftTop"></param>
	/// <param name="rightBottom"></param>
	/// <returns></returns>
	public virtual int autoSelectScale(PointF leftTop, PointF rightBottom)
	{
		return Scales.Count - 2;
	}

	/// <summary>
	/// AS: Получить макс. ширину для данного масштаба
	/// </summary>
	/// <param name="scale">Номер масштаба</param>
	/// <returns></returns>
	public virtual int GetWidthOfScale(int scale)
	{
		return Scales[scale - 1].width < 35000 ? Scales[scale - 1].width : 35000;
	}

	/// <summary>
	/// AS: Получить макс. высоту для данного масштаба
	/// </summary>
	/// <param name="scale">Номер масштаба</param>
	/// <returns></returns>
	public virtual int GetHeightOfScale(int scale)
	{
		return Scales[scale - 1].height < 35000 ? Scales[scale - 1].height : 35000;
	}

	public virtual PointF coordMapToWindow(PointF mapPoint, float fKWinToMap, int corrx, int corry)
	{
		PointF point = new PointF();
		point.X = 0.0f;
		point.Y = 0.0f;
		PointF centerPlanar = this.GeoToPlanar(OrigCenterX2, OrigCenterY2);
		//float x = (mapPoint.x + centerPlanar.x) / fKWinToMap + jQuery(window).width() / 2  + offset.x * 2;
		//float y = -(mapPoint.y + centerPlanar.y) / fKWinToMap + jQuery(window).height() / 2 + offset.y * 2;
		float x = (mapPoint.X + centerPlanar.X) / fKWinToMap;
		float y = -(mapPoint.Y + centerPlanar.Y) / fKWinToMap;
		point.X = (float)Math.Round((double)x) + corrx;// +jQuery.ox;// - 12;
		point.Y = (float)Math.Round((double)y) + corry;// +jQuery.oy;// - 12;
		return point;
	}
	/*
	/// <summary>
	/// AS: преоьразует WGS84 в оконные координаты
	/// </summary>
	/// <param name="geoX">долгота</param>
	/// <param name="geoY">широта</param>
	/// <param name="fKWinToMap">коэффициент масштаба</param>
	/// <param name="corrx">нах не нужен, передавать 0</param>
	/// <param name="corry">нах не нужен, передавать 0</param>
	/// <param name="scale">Индекс текущего масштаба</param>
	/// <returns></returns>
	public  PointF coordGeoToWindow(float geoX, float geoY, float fKWinToMap, int corrx, int corry, int scale)
	{
		PointF planar = GeoToPlanar(geoX, geoY);
		PointF windowPoint = coordMapToWindow(planar, fKWinToMap, corrx, corry);
		return windowPoint;
	}
	*/

	// AS: вычисляет номер и положение на экране первой картинки карты
	public virtual MapPictureWithOffset GetMapPicture(int scale, float geox, float geoy)
	{
		//int scaleNum = Scales.Count - 1;
		int scaleNum = scale;
		PointF currentCenterPoint = GeoToPlanar(OrigCenterX2, OrigCenterY2);
		PointF planarRB = GeoToPlanar(Scales[scaleNum].rightBottomPoint.X, Scales[scaleNum].rightBottomPoint.Y);

		float fKWinToMap = Scales[scaleNum].val / MapDescription.pixelsPerMeter;
		
		// Количество картинок
		int pnumx = Scales[scaleNum].width / 256;
		int pnumy = Scales[scaleNum].height / 256;

		// Найти картинку с этой геоточкой
		PointF centerPlanar = GeoToPlanar(geox, geoy);

		//Номер картинки
		float picNumx = (planarRB.X - centerPlanar.X) / (256 * fKWinToMap);
		float picNumy = (centerPlanar.Y - planarRB.Y) / (256 * fKWinToMap);

		// Смещение в пикселях
		int pixOffsx = (int)Math.Round((currentCenterPoint.X - centerPlanar.X) / fKWinToMap);
		int pixOffsy = (int)Math.Round((centerPlanar.Y - currentCenterPoint.Y) / fKWinToMap);
		
		// AS: offset - глобальная переменная
		/*if(!offset){
			offset = {x: 0, y:0};
		}
		offset.x += pixOffsx;
		offset.y += pixOffsy;*/
		
		// Координаты центра экрана
		/*var wincx = Math.round($(window).width() / 2);
		var wincy = Math.round($(window).height() / 2);*/
		
		// Смещение в картинке
		int corrx = 0;
		int corry = 0;
		int picNumxRounded = (int)Math.Round(picNumx);
		int picNumyRounded = (int)Math.Round(picNumy);
		
		/** Some new purposes **/
		//var ox = oy = 0;
		//var pnumCenterx = Math.round(pnumx / 2.0);
		//var pnumCentery = Math.round(pnumy / 1.8);
		//if(currentMapShortName != 'ru'){
			float pnumCenterx = (float)(pnumx / 2.0);
			float pnumCentery = (float)(pnumy / 1.8);
			OffsetX = picNumxRounded - pnumCenterx;
			OffsetY = picNumyRounded - pnumCentery;
			OffsetX = (float)Math.Round(OffsetX / 0.8);
			OffsetY = (float)Math.Round(OffsetY / 0.7);
		//}
		/** Some new purposes ends **/
		corrx = (picNumxRounded < picNumx ? 256 : 0);
		corry = (picNumyRounded < picNumy ? 256 : 0);
		
		int offsx = (int)Math.Round(Math.Abs(corrx - (Math.Abs(picNumxRounded - picNumx) * 256)));
		int offsy = (int)Math.Round(Math.Abs(corry - (Math.Abs(picNumyRounded - picNumy) * 256)));
		
		/*if(offsx > 256){
			picNumxRounded += Math.round(offsx / 256);
		}
		if(offsy > 256){
			picNumyRounded += Math.round(offsy / 256);
		}*/

		// Координаты точки для вывода картинки
		//var point = {x: wincx - offsx, y: wincy - offsy};
		//var point = {x: wincx, y: wincy};
		/*MyPoint point = new MyPoint();
		point.x = -offsx;
		point.y = -offsy;*/
		
		int pnx = (int)Math.Floor(picNumx);
		int pny = (int)Math.Floor(picNumy);
		/*var pnx = picNumxRounded;
		var pny = picNumyRounded;*/
		/*var width = jQuery.MapSettings.Scales[jQuery.MapSettings.maxScale].x;
		var height = jQuery.MapSettings.Scales[jQuery.MapSettings.maxScale].y;
		var halfw = Math.round(width / 2) - offset.x;
		var halfh = Math.round(height / 2) - offset.y;*/
		/*var res = jQuery.getMaxMapDimensions();
		var width = res.width;
		var height = res.height;
		var halfw = res.halfw;
		var halfh = res.halfh;*/
		/*return{
			point: {x: point.x + halfw, y: point.y + halfh},
			picNumber: {x: pnx, y: pny}
		}*/
		MapPictureWithOffset pic = new MapPictureWithOffset();
		pic.picNumX = pnx;
		pic.picNumY = pny;
		pic.picOffsetX = -offsx - (int)OffsetX;
		pic.picOffsetY = -offsy - (int)OffsetY;
		return pic;
	}
	#endregion

	public MapDescription()
	{
		OrigCenterX2 = origCenterX;
		OrigCenterY2 = origCenterY;
	}

	public void AddName(String nkey, String nval)
	{
		Names.Add(nkey, nval);
	}

	/// <summary>
	/// AS: Добавим описание масштаба для карты
	/// </summary>
	/// <param name="scale"></param>
	public void AddScale(MapScale scale)
	{
		if (scales == null)
		{
			scales = new List<MapScale>();
		}
		scales.Add(scale);
	}

	public virtual String BuildMapPictureUrl(int picNumX, int picNumY, int scaleNum)
	{
		StringBuilder url = new StringBuilder(this.Url);
		//int scaleNum = Scales.Count - 1;
		if (picNumX < 0 || picNumY < 0 || picNumX > Scales[scaleNum].width || picNumY > Scales[scaleNum].height)
		{
			url.Append("/img/map/nodata.png");
		}
		else
		{
			url.Append("/Scale");
			url.Append(scaleNum.ToString());
			url.Append("/");
			url.Append(picNumY.ToString());
			url.Append("/map_");
			url.Append(fillSrcComponent(picNumX.ToString()));
			url.Append("_");
			url.Append(fillSrcComponent(picNumY.ToString()));
			url.Append(".png");
		}

		return url.ToString();
	}

	protected virtual String fillSrcComponent(String num)
	{
		String newval = "";
		String oldval = num + "";
		if (oldval.Length < 2)
		{
			newval += "000";
		}
		else if (oldval.Length < 4)
		{
			newval += "00";
		}
		else
		{
			newval += "0";
		}
		newval += oldval;
		return newval;

		/*String newval = "";
		String oldval = num + "";
		if (oldval.Length < 2)
		{
			newval += "000";
		}
		else
		{
			newval += "00";
		}
		newval += oldval;
		return newval;*/
	}

	public override void WriteXml(XmlTextWriter writer)
	{
		writer.WriteStartElement(this.ToString());

		writer.WriteStartElement("shortname");
		writer.WriteString(ShortName);
		writer.WriteEndElement();

		writer.WriteStartElement("url");
		writer.WriteString(url);
		writer.WriteEndElement();

		writer.WriteStartElement("current");
		writer.WriteString(Current ? "true" : "false");
		writer.WriteEndElement();

		writer.WriteStartElement("names");
		foreach (String key in Names.Keys)
		{
			writer.WriteStartElement(key);
			writer.WriteString(Names[key]);
			writer.WriteEndElement();
		}
		writer.WriteEndElement();

		if (Scales != null)
		{
			writer.WriteStartElement("scales");
			foreach (MapScale scale in Scales)
			{
				writer.WriteStartElement("scale");
				writer.WriteAttributeString("width", scale.width.ToString());
				writer.WriteAttributeString("height", scale.height.ToString());
				writer.WriteAttributeString("val", scale.val.ToString());

				writer.WriteStartElement("RB");
				writer.WriteAttributeString("x", scale.rightBottomPoint.X.ToString());
				writer.WriteAttributeString("y", scale.rightBottomPoint.Y.ToString());
				writer.WriteEndElement();

				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		writer.WriteStartElement("guid");
		writer.WriteString(Guid);
		writer.WriteEndElement();

		writer.WriteEndElement();
	}

	/// <summary>
	/// Создание списка описаний карт из потока, содержащего XML-данные
	/// (переделать на сериализацию нормально, так заебешся каждое новое свойство описывать)
	/// </summary>
	/// <param name="data">Поток, содержащий XML данные</param>
	/// <returns></returns>
	public static List<MapDescription> Create(Stream data)
	{
		// Это ппц, разбор сделан не через XPath, а через XAss. Блять.

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ".";
		List<MapDescription> list = new List<MapDescription>();

		XPathDocument xdoc = new XPathDocument(data);
		XPathNavigator nav = xdoc.CreateNavigator();
		nav.MoveToRoot();
		XPathNodeIterator maps = nav.Select("/maps/map");
		while (maps.MoveNext())
		{
			MapDescription md = new MapDescription();
			maps.Current.MoveToChild("shortname", "");
			md.ShortName = maps.Current.Value;

			maps.Current.MoveToParent();
			maps.Current.MoveToChild("url", "");
			md.Url = maps.Current.Value;

			try
			{
				maps.Current.MoveToParent();
				maps.Current.MoveToChild("X", "");
				float.TryParse(maps.Current.Value, System.Globalization.NumberStyles.AllowDecimalPoint, nfi, out md.origCenterX);
				md.OrigCenterX2 = md.origCenterX;

				maps.Current.MoveToParent();
				maps.Current.MoveToChild("Y", "");
				float.TryParse(maps.Current.Value, System.Globalization.NumberStyles.AllowDecimalPoint, nfi, out md.origCenterY);
				md.OrigCenterY2 = md.origCenterY;
			}
			catch (Exception)
			{
			}

			maps.Current.MoveToParent();
			maps.Current.MoveToChild("guid", "");
			md.Guid = maps.Current.Value;

			maps.Current.MoveToParent();
			maps.Current.MoveToChild("current", "");
			if (maps.Current.LocalName == "current")
			{
				md.Current = maps.Current.Value == "true" ? true : false;
				maps.Current.MoveToParent();
			}

			maps.Current.MoveToChild("fullname", "");
			XPathNodeIterator names = maps.Current.SelectChildren(XPathNodeType.Element);
			while (names.MoveNext())
			{
				//md.Names.Add(names.Current.LocalName, names.Current.Value);
				md.AddName(names.Current.LocalName, names.Current.Value);
			}

			// AS: прочитаем масштабы
			maps.Current.MoveToParent();
			if (maps.Current.MoveToChild("scales", ""))
			{
				XPathNodeIterator mapScales = maps.Current.SelectChildren(XPathNodeType.Element);
				if (mapScales != null && mapScales.Count > 0)
				{
					while (mapScales.MoveNext())
					{
						//PointF pt = new PointF();
						MapScale s = new MapScale();
						Int32.TryParse(mapScales.Current.GetAttribute("width", ""), out s.width);
						Int32.TryParse(mapScales.Current.GetAttribute("height", ""), out s.height);
						float.TryParse(mapScales.Current.GetAttribute("val", ""),System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out s.val);
						if (s.width > 0) s.width *= 256;
						if (s.height > 0) s.height *= 256;

						XPathNavigator rb = mapScales.Current.SelectSingleNode("./RB");
						float x;
						float y;
						float.TryParse(rb.GetAttribute("x", ""), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
						float.TryParse(rb.GetAttribute("y", ""), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);
						s.rightBottomPoint = new PointF(x, y);

						md.AddScale(s);
					}
				}
			}

			list.Add(md);
		}

		return list;
	}
}