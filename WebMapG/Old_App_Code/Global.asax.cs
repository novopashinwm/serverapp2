﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Runtime.Caching;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Web;
using System.Web.Hosting;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.Terminal;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RU.NVG.UI.WebMapG.Push;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG.Old_App_Code
{
	public class Global : HttpApplication
	{
		public const string ShareLinksApplicationKey     = "ShareLinksApplication";
		public const string SessionManagerApplicationKey = "SessionManagerApplication";
		private readonly ConcurrentDictionary<Guid, ShareLink> _shareLinks =
			new ConcurrentDictionary<Guid, ShareLink>();
		private TDServer         _tdServer;
		private TDSessionManager _tdSessionManager;

		public Global()
		{
			InitializeComponent();
		}
		protected void Application_Start(object sender, EventArgs e)
		{
			if (0 == ChannelServices.RegisteredChannels.Count())
				// Чтение конфигурации Remoting, для доступа к клиентам (например: GeoClient и TerminalClient)
				RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);

			// Сохраняем в приложении словарь общих ссылок
			Application.Set(ShareLinksApplicationKey, _shareLinks);

			// Создание объектов сервера
			_tdSessionManager        = new TDSessionManager();
			_tdServer                = new TDServer(_tdSessionManager, ConfigurationManager.AppSettings);
			_tdSessionManager.Server = _tdServer;
			// Сохраняем в приложении объекты сервера
			Application.Set(SessionManagerApplicationKey, _tdSessionManager);
			// Загрузка картинок
			var vehicleIconPath = GetVehicleIconPath();
			if (!string.IsNullOrEmpty(vehicleIconPath))
				ServerApplication.Server.Instance().ImportVehicleIcons(vehicleIconPath);
			// Настраиваем сериализацию Json, всегда т.к. события терминала отправляем всегда
			{
				var serializer = new JsonSerializer
				{
					ContractResolver  = new SignalRContractResolver(),
					NullValueHandling = NullValueHandling.Ignore,
				};
				serializer.Converters.Add(new StringEnumConverter());
				serializer.Converters.Add(new IsoDateTimeConverter
				{
					DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'"
				});
				GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
			}
			// Запускаем отправку изменений в перечне сообщений для телефонов
			if (PushSmsMessageProcessor.Instance.Enabled)
				PushSmsMessageProcessor.Instance.Start();
			// Запускаем отправку сообщений для Web, страница с картой, список сообщений внизу
			if (PushWebMessageProcessor.Instance.Enabled)
				PushWebMessageProcessor.Instance.Start();
			// Запускаем отправку сообщений о событиях сервера (обновление объектов наблюдения и тревоги)
			// Т.к. обработки простые отдельный Processor не используем, подписываемся на события, а в случае усложнения нужно сделать рефакторинг
			ServerApplication.Server.Instance().AlarmReceived += OnReceiveAlarmEvent;
			ServerApplication.Server.Instance().ReceiveEvent  += OnMobilUnitReceived;
			// Запускаем сервер, текущего приложения
			ServerApplication.Server.Instance().Start();
		}
		protected void Application_End(object sender, EventArgs e)
		{
			StopServer();
		}
		protected void Application_Error(object sender, EventArgs e)
		{
			if (Context.Request.IsAjaxRequest())
			{
				SysDiags::Trace.TraceError("{0}: Error {1}", Context.Request.RawUrl, Server.GetLastError());
				Context.Response.Clear();
				Context.Response.StatusCode = 500;
				Context.Response.StatusDescription = "Internal server error";
				Context.Response.End();
				return;
			}

			// AS: Доделать
			var rm = new ResourceManager("Resources.Messages", Assembly.Load("App_GlobalResources"));
			try
			{
				var lastError = Server.GetLastError();
				Server.ClearError();
				if (lastError != null)
				{
					SysDiags::Trace.TraceError(lastError.ToString());
					if (lastError.InnerException != null)
						SysDiags::Trace.TraceError(lastError.InnerException.ToString());
				}

				if (lastError is HttpException)
				{
					if (((HttpException)(lastError)).GetHttpCode() != 0)
					{
						Server.Transfer("~/Error.aspx?SetStatus=" + ((HttpException)(lastError)).GetHttpCode());
						return;
					}
				}

				Server.Transfer("~/Error.aspx");
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				var errMsg = rm.GetString("ServerError");
				if (errMsg != null)
				{
					Response.Write(errMsg);
				}
				else
				{
					Response.Write("Critical server error");
				}
			}
		}
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
		}
		protected void Application_EndRequest(object sender, EventArgs e)
		{
			CheckAcceptTypes();
		}
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}
		protected void Session_Start(object sender, EventArgs e)
		{
		}
		protected void Session_End(object sender, EventArgs e)
		{
		}
		private void StopServer()
		{
			// Останавливаем отправку сообщений о событиях сервера (обновление объектов наблюдения и тревоги)
			ServerApplication.Server.Instance().ReceiveEvent  -= OnMobilUnitReceived;
			ServerApplication.Server.Instance().AlarmReceived -= OnReceiveAlarmEvent;
			// Останавливаем отправку сообщений для Web, страница с картой, список сообщений внизу
			if (PushWebMessageProcessor.Instance.Enabled)
				PushWebMessageProcessor.Instance.Stop();
			// Останавливаем отправку изменений в перечне сообщений для телефонов, закладка SMS
			if (PushSmsMessageProcessor.Instance.Enabled)
				PushSmsMessageProcessor.Instance.Stop();
			try
			{
				var application = Application;
				if (application != null)
					application.Remove(SessionManagerApplicationKey);
				if (_tdSessionManager != null)
					_tdSessionManager.Dispose();
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
		}
		private string GetVehicleIconPath()
		{
			var vehicleIconPath = ConfigHelper.GetAppSettingAsString("vehicleIconPath");
			if (string.IsNullOrEmpty(vehicleIconPath))
				return null;
			var applicationPath = string.Empty;
			if (!string.IsNullOrEmpty(HostingEnvironment.ApplicationVirtualPath))
				applicationPath = HostingEnvironment.ApplicationVirtualPath;
			var path = Server.MapPath(applicationPath + vehicleIconPath);
			return path;
		}
		private void CheckAcceptTypes()
		{
			if (Response.StatusCode != (int)HttpStatusCode.OK)
				return;

			if (Request.AcceptTypes == null) return;
			if (Request.AcceptTypes.Contains("*/*")) return;
			if (Request.AcceptTypes.SelectMany(x => x.Split(';')).Contains("*/*")) return;

			var responseContentTypes = Response.ContentType.Split(';');

			if (responseContentTypes.Any(response => Request.AcceptTypes.Contains(response, StringComparer.OrdinalIgnoreCase)))
				return;

			SysDiags::Trace.TraceWarning("{0} {1} {2}: Something wrong about content type '{3}'. It is not compatible with '{4}'\n\tUser agent: {5}",
				Request.HttpMethod,
				Request.RawUrl,
				Request["a"] ?? "default",
				Response.ContentType,
				Request.AcceptTypes != null
					? string.Join(",", Request.AcceptTypes)
					: string.Empty,
				Request.UserAgent);
		}
		////////////////////////////////////////////////////////////////////////////////
		public static MemoryCache GlobalCache => ServerApplication.Server.Instance().GlobalCache;
		////////////////////////////////////////////////////////////////////////////////
		private void OnMobilUnitReceived(ReceiveEventArgs args)
		{
			////////////////////////////////////////////////
			// Отправляем уведомления клиентам
			foreach (var mu in args.MobilUnits)
				MessagesHubSecurity.Instance.UpdateVehicle(mu);
		}
		private void OnReceiveAlarmEvent(object sender, ReceiveAlarmArgs args)
		{
			MessagesHubSecurity.Instance.AlarmVehicle(args.MobilUnit, args.OperatorIds);
		}

		#region Web Form Designer generated code
		/// <summary> Required method for Designer support - do not modify the contents of this method with the code editor </summary>
		private void InitializeComponent()
		{
			// 
			// Global
			// 
			PreRequestHandlerExecute += new EventHandler(Global_PreRequestHandlerExecute);
		}
		#endregion Web Form Designer generated code
		private void Global_PreRequestHandlerExecute(object sender, EventArgs e)
		{
		}
		protected void Application_PostMapRequestHandler(object sender, EventArgs e)
		{
			var tempAspNetSessionCookie = Request.Cookies["ASP.NET_SessionIdTemp"];
			if (tempAspNetSessionCookie == null)
				return;

			var aspNetSessionCookie = Request.Cookies["ASP.NET_SessionId"];
			if (aspNetSessionCookie == null)
				Request.Cookies.Add(new HttpCookie("ASP.NET_SessionId", tempAspNetSessionCookie.Value));
			else
				aspNetSessionCookie.Value = tempAspNetSessionCookie.Value;
		}
		protected void Application_PostRequestHandlerExecute(object sender, EventArgs e)
		{
			string sessionId;
			try
			{
				sessionId = Session.SessionID;
			}
			catch (Exception)
			{
				return;
			}
			var cookie = new HttpCookie("ASP.NET_SessionIdTemp", sessionId)
			{
				Expires  = DateTime.Now.AddMinutes(Session.Timeout),
				HttpOnly = true
			};
			try
			{
				Response.Cookies.Add(cookie);
			}
			catch (HttpException ex)
			{
				if ((uint)ex.ErrorCode != 0x80004005)
					throw;
			}
		}
	}
}