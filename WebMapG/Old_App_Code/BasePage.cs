﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Security;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Xsl;
using Compass.Ufin.Billing.Core;
using Compass.Ufin.Billing.Core.Enums;
using DTO;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.UI.WebMapG;
using FORIS.TSS.UI.WebMapG.Old_App_Code;
using Interfaces.Geo;
using Interfaces.Web;
using Jayrock.Json;
using RU.NVG.UI.WebMapG.Model.Common;
using Group = FORIS.TSS.BusinessLogic.DTO.Group;
using HttpResponse = System.Web.HttpResponse;
using LoginResult = RU.NVG.UI.WebMapG.Model.Common.LoginResult;
using SysDiags = System.Diagnostics;

public partial class BasePage : System.Web.UI.Page
{
	private static SysDiags::TraceSource webApiTraceSource = new SysDiags::TraceSource("WebApi", SysDiags::SourceLevels.Off) { Switch = new SysDiags::SourceSwitch("WebApi") };
	protected List<Message> messages;
	protected bool contentTypeSet = false;
	protected HttpResponse response;
	protected CommonDTOResponse JsonResponse;
	protected List<Message> Messages
	{
		get
		{
			if (messages == null)
			{
				messages = new List<Message>();
			}
			return messages;
		}
	}
	protected ISessionManager SessionManager
	{
		get { return contextHelper.SessionManager; }
	}
	protected IWebLoginProvider WebLoginProvider
	{
		get { return contextHelper.WebLoginProvider; }
	}
	public ConcurrentDictionary<Guid, ShareLink> ApplicationShareLinks
	{
		get { return contextHelper.ApplicationShareLinks; }
	}
	public ShareLink SessionShareLink
	{
		get { return contextHelper.SessionShareLink; }
	}
	protected int? OperatorId
	{
		get         { return contextHelper.OperatorId;  }
		private set { contextHelper.OperatorId = value; }
	}
	protected string AppId
	{
		get { return contextHelper.AppId;  }
		set { contextHelper.AppId = value; }
	}
	protected IWebPersonalServer IWebPS
	{
		get
		{
			var result = contextHelper.PersonalWebServer;
			if (result == null)
				Unauthorized();
			return result;
		}
	}
	protected bool IsAuthorized
	{
		get
		{
			return contextHelper.PersonalWebServer != null;
		}
	}
	private int sessionStateTimeout = -1;
	protected int SessionStateTimeout
	{
		get
		{
			if (sessionStateTimeout == -1)
				sessionStateTimeout = ConfigHelper.GetAppSettingAsInt32("sessionStateTimeout", sessionStateTimeout);
			return sessionStateTimeout;
		}
	}
	protected Dictionary<string, string> properties;
	protected string SessionId
	{
		get { return contextHelper.SessionId;  }
		set { contextHelper.SessionId = value; }
	}
	protected bool SessionSwitched
	{
		get { return contextHelper.SessionSwitched; }
	}
	protected virtual bool IsUserLogged
	{
		get
		{
			var user = OperatorId;
			return user != null;
		}
	}
	protected Guid? MapGuid
	{
		get
		{
			var mapGuidPar = GetParamValue("mapGuid");

			Guid result;
			if (!string.IsNullOrWhiteSpace(mapGuidPar) && Guid.TryParse(mapGuidPar, out result))
				return result;

			var cookie = Request.Cookies["typ"];
			if (cookie == null)
				return null;
			var typString = cookie.Value;

			switch (typString)
			{
				case "osm":
					return MapGuids.OsmMapGuid;
				default:
					return null;
			}
		}
	}
	protected string LoggedUser
	{
		get { return contextHelper.LoggedUser; }
	}
	protected bool ShowAdminPage
	{
		get
		{
			bool result = false;
			object ores = GetObjectFromSession("ShowAdminPage");
			if (ores != null)
			{
				result = (bool)ores;
			}
			return result;
		}
	}
	/// <summary> Сюда сериализуем ответ сервера (страницу) </summary>
	private XmlTextWriter writer;
	protected XmlTextWriter Writer
	{
		get { return writer; }
		set { writer = value; }
	}
	/// <summary> Файл, содержащий XSL шаблон для обработки сериализованного в XML ответа сервера </summary>
	private string _templateFile = "";
	public bool IsAskPositionAvailable;
	public bool IsIginitionMonitoringAvailable;
	public bool HasSpeedMonitoring;
	public bool HasDigitalSensors;
	protected virtual string TemplateFile
	{
		get { return _templateFile; }
		set
		{
			/*
			 * Пока не работает удаленная загрузка Xsl
				var configValue = ConfigurationManager.AppSettings["baseWebAppPath"]; // При отсутствии возвращает null (исключения нет).
				if (!IsMobile)
				{
					if (null == configValue)
						_templateFile =
							Server.MapPath(@"/" + Request.ApplicationPath + @"/templates/xsl/" + value + @".xsl");
					else
						_templateFile =
							Server.MapPath(Request.Url.MakeRelativeUri(new Uri(configValue)) + @"/templates/xsl/" + value + @".xsl");
				}
				else
				{
					if (null == configValue)
						_templateFile =
							Server.MapPath(@"/" + Request.ApplicationPath + @"/templates/xsl/mobile/" + value + @".xsl");
					else
						_templateFile =
							Server.MapPath(Request.Url.MakeRelativeUri(new Uri(configValue)) + @"/templates/xsl/mobile/" + value + @".xsl");
				}
			*/
			if (!IsMobile)
				_templateFile = Server.MapPath($@"/{Request.ApplicationPath}/templates/xsl/{value}.xsl");
			else
				_templateFile = Server.MapPath($@"/{Request.ApplicationPath}/templates/xsl/mobile/{value}.xsl");
		}
	}
	protected virtual Dictionary<string, string> Properties
	{
		get
		{
			if (properties == null)
			{
				properties = new Dictionary<string, string>();
			}

			return properties;
		}
	}
	public BasePage()
	{
		writer = null;
	}
	/// <summary> Добавление свойств страницы </summary>
	protected virtual void AddProperty(string key, string val)
	{
		Properties[key] = val;
	}
	protected virtual void AddProperty(string key, bool val)
	{
		Properties[key] = XmlConvert.ToString(val);
	}
	/// <summary> Начало формирования Json ответа </summary>
	protected virtual void JsonResponseSerializationBeg()
	{
		SetJsonContentType();
		JsonResponse = new CommonDTOResponse();
	}
	/// <summary> Окончание формирования Json ответа </summary>
	protected virtual void JsonResponseSerializationEnd()
	{
		JsonResponse.messages = Messages;
		response.Write(JsonHelper.SerializeObjectToJson(JsonResponse));
	}
	protected virtual void ResponseSerializationBeg()
	{
		ResponseSerializationBegInternal();
	}
	protected void InitProperties()
	{
		AddProperty("ApplicationPath", Request.ApplicationPath != null && Request.ApplicationPath.Length == 1 ? string.Empty : Request.ApplicationPath);
		AddProperty("pageTitleNum", "1");
		TranslateConfig("googleMapsScriptAuth");
		TranslateConfig("yandexApiKey");

		TranslateConfig("BrowserIE8Allowed");
		TranslateConfig("maps", "['YaMap', 'GoMap', 'OsmMap']");
		TranslateConfig("defaultMap", "YaMap");
		TranslateConfig("playSound", "true");

		TranslateConfig("baseWebAppPath");
		TranslateConfig("mainJavascript");
		TranslateConfig("mainCss");

		TranslateConfig("idletime", "0");
		TranslateConfig("appClientDownloadLink", string.Empty);
		TranslateConfig("showAppName");
		TranslateConfig("updateDataOnFocus", "false");

		AddTitleProperty();

		// Имя сервера
		AddProperty("ServerName", Request.Url.Host); //Ранее использовалось значение Request.ServerVariables["HTTP_HOST"]);

		// Адрес клиента
		AddProperty(nameof(Request.UserHostAddress).ToCamel(), Request.UserHostAddress);

		// Параметры вошедшего пользователя
		if (OperatorId != null)
		{
			AddProperty("loggedUserId",         OperatorId.Value.ToString(CultureInfo.InvariantCulture));
			AddProperty("isSuperAdministrator", IWebPS.IsSuperAdministrator);
			AddProperty("isSalesManager",       IWebPS.IsSalesManager);
		}
		AddProperty("loggedUser", LoggedUser);

		// Количество доступных департаментов
		var departmentAccessCount = IsAuthorized && IWebPS.Department != null
			? IWebPS.GetAvailableDepartmentsCount()
			: 0;
		AddProperty(nameof(departmentAccessCount).FirstCharToLowercase(), departmentAccessCount.ToString());

		// Тип департамента (абонента)
		var departmentType = IsAuthorized && IWebPS.Department != null
			? IWebPS.Department.Type
			: DepartmentType.Physical;
		AddProperty("departmentType", departmentType.ToString());

		// Текущий язык
		AddProperty("language", CurrentCulture.ToString());

		AddProperty("location", ConfigurationManager.AppSettings["defaultPromoCulture"]);
		if (IsAuthorized && IWebPS.Department != null && IWebPS.Department.countryName != null)
			AddProperty("DepartmentCountryName", IWebPS.Department.countryName);

		var departmentLocked = IsAuthorized && IWebPS.IsLocked();
		AddProperty("DepartmentLocked", departmentLocked);

		TranslateConfig("pointPrefix");

		AddProperty("IsUserGuest",     IsUserGuest);
		AddProperty("allowAddTracker", IsAuthorized && IWebPS.AllowAddTracker);

		// Тип браузера
		AddProperty("browser", Request.Browser.Type);

		AddProperty("logo", ConfigurationManager.AppSettings["logo"] ?? "logo");
		AddProperty("color-schema", ConfigurationManager.AppSettings["color-schema"] ?? "color-schema");
		AddProperty("favicon",
			ConfigurationManager.AppSettings["favicon"] ?? "favicon.ico");

		TranslateConfig("sessionStateTimeout");

		// Дата и время сервера, переведенное во временную зону клиента
		if (IsAuthorized && IWebPS.SessionInfo != null)
		{
			AddProperty("serverDate",
				TimeZoneInfo.ConvertTimeFromUtc(
					DateTime.UtcNow,
					TimeZoneInfo).ToString("s"));
		}

		// Показывать ли страницу Администрирование
		AddProperty("showAdminPage", ShowAdminPage.ToString());

		AddProperty("DefaultVehicleIcon", "/img/userdata/defaults/default-any.png");

		// Использовать ли звонилку
		if (response != null)
		{
			HttpCookie useDialer = Request.Cookies["useDialer"];
			if (useDialer != null)
				AddProperty("useDialer", useDialer.Value);
		}


		if (Request.UrlReferrer != null)
		{
			AddProperty("UrlReferrer", Request.UrlReferrer.ToString());
		}

		AddProperty("MainPageUrl", MainPageUrl);
		TranslateConfig("Copyright.Locale");

		AddProperty("OperatorSystemRights", JsonHelper.SerializeObjectToJson(OperatorSystemRights));
		if (null != SessionShareLink)
		{
			// Добавляем свойство указывающее на то, что это общая ссылка
			AddProperty("IsShareLink", null != SessionShareLink);
			// Меняем время истечения, на окончание ссылки
			if (!Response.Cookies.AllKeys.Contains(ContextHelper.SessionCookieName))
				Response.Cookies[ContextHelper.SessionCookieName].Expires = SessionShareLink.LogTimeEnd.ToUtcDateTime();
			// Меняем время истечения, на окончание ссылки
			if (!Response.Cookies.AllKeys.Contains("ASP.NET_SessionId"))
				Response.Cookies["ASP.NET_SessionId"].Expires = SessionShareLink.LogTimeEnd.ToUtcDateTime();
			// Меняем время истечения, на окончание ссылки
			if (!Response.Cookies.AllKeys.Contains("ASP.NET_SessionIdTemp"))
				Response.Cookies["ASP.NET_SessionIdTemp"].Expires = SessionShareLink.LogTimeEnd.ToUtcDateTime();
		}
	}
	/// <summary> Начало сериализации ответа сервера </summary>
	private void ResponseSerializationBegInternal()
	{
		try
		{
			InitProperties();

			Writer.Formatting = Formatting.None;
			Writer.WriteStartDocument();
			Writer.WriteStartElement("response");

			// Сериализуем все свойства. Без рефлексии быстрее
			foreach (string key in properties.Keys)
			{
				Writer.WriteAttributeString(key, properties[key]);
			}
		}
		catch (Exception ex)
		{
			WriteLog(ex);
			Logout();
		}

	}
	/// <summary> Окончание сериализации ответа сервера </summary>
	protected virtual void ResponseSerializationEnd()
	{
		ResponseSerializationEndInternal();
	}
	private void ResponseSerializationEndInternal()
	{
		// Сериализация сообщений
		SerializeMessages();

		if (ContentType != "text/xml")
		{
			// Сериализация прав
			SerializeSystemRights();

			//Данные оператора
			RenderOperatorInfo();

			//Доступные клиенты
			RenderDepartments();
		}
		Writer.WriteEndElement(); // response
		Writer.WriteEndDocument();
	}
	private void RenderDepartments()
	{
		if (!IsAuthorized)
			return;
		var currentDepartment = IWebPS.Department;
		if (currentDepartment != null)
		{
			Writer.WriteStartElement("Departments");
			var dCount = IWebPS.GetAvailableDepartmentsCount().ToString(CultureInfo.InvariantCulture);
			Writer.WriteAttributeString("Count", dCount);

			currentDepartment.WriteXml(Writer);

			Writer.WriteEndElement(); //Departments
		}

	}
	protected void SerializeSystemRights()
	{
		var systemRights = contextHelper.OperatorSystemRights;
		if (systemRights == null)
			return;
		Writer.WriteStartElement("systemRights");
		foreach (var right in systemRights)
		{
			Writer.WriteStartElement("right");
			Writer.WriteAttributeString("value", ((int)right).ToString());
			Writer.WriteString(right.ToString());
			Writer.WriteEndElement();
		}

		Writer.WriteEndElement();
	}
	/// <summary> Сериализация описаний карт </summary>
	protected virtual void SerializeMapDescriptions()
	{
		// Сериализация описаний карт
		//TODO: раскомментировать или удалить
		//if (MapDescriptions != null)
		//{
		//    Writer.WriteStartElement("mapdescriptions");
		//    foreach (MapDescription md in MapDescriptions)
		//    {
		//        md.WriteXml(Writer);
		//    }
		//    Writer.WriteEndElement(); // mapdescriptions
		//}
	}
	/// <summary> Сериализация сообщений </summary>
	protected virtual void SerializeMessages()
	{
		foreach (Message msg in Messages)
		{
			msg.WriteXml(Writer);
		}
	}
	/// <summary> Преобразование данных, находящихся в потоке, в текст </summary>
	protected virtual string GetStringOfStream(Stream s)
	{
		if (s == null) throw new ArgumentNullException();
		s.Position = 0;
		byte[] buffer = new byte[s.Length];
		s.Read(buffer, 0, (int)s.Length);
		UTF8Encoding utf8 = new UTF8Encoding();
		StringBuilder sb = new StringBuilder();
		//StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
		sb.Append(utf8.GetString(buffer));
		return sb.ToString();
	}
	/// <summary> Запись сериализованного ответа сервера в выходной поток для передачи клиенту </summary>
	protected virtual void WriteXmlToStream(Stream stm, Stream outputStream)
	{
		XmlReader reader = null;
		XmlTextWriter writer = null;
		using (new Stopwatcher(this.GetTypeName(false).GetCallerMethodNameMessage(), null, ContextHelper.StopwatchLoggingThreshold))
			try
			{
#if (DEBUG && XML)
				FileStream fs = null;
				try
				{
					var tfile = TemplateFile.Length > 0 ? TemplateFile : "page";
					tfile = Path.GetFileName(tfile);
					var fileName = ConfigurationManager.AppSettings["debugOutputFile"] + tfile + "_" + OperatorId.ToString() + ".xml";
					try
					{
						fs = new FileStream(
							fileName,
							File.Exists(fileName) ? FileMode.Truncate : FileMode.Create);
					}
					catch (Exception)
					{
						fs = new FileStream(fileName, FileMode.Create);
					}

					var xmlDocumentToWrite = new XmlDocument();

					stm.Position = 0;
					xmlDocumentToWrite.Load(stm);

					using (var xmlTextWriter = new XmlTextWriter(fs, Encoding.UTF8) { Formatting = Formatting.Indented })
						xmlDocumentToWrite.WriteTo(xmlTextWriter);
				}
				catch (Exception ex)
				{
					SysDiags::Trace.TraceError(this.GetTypeName() + ": {0} raise '{1}'\n{2}",
						CallStackHelper.GetCallerMethodName(), ex.GetType().Name, ex.Message);
				}
				finally
				{
					if (fs != null)
					{
						fs.Close();
					}
				}
#endif

				stm.Position = 0;

				// AS: через вот такую хитрую жопу присваиваем XmlDocument'у свойство BaseURI,
				// потому что XML создаем в потоке, и у него нет своего URI, как у XML из файла.
				// Без baseURI функция document() пошлет все запросы нахуй, потому что не знает свой URI.
				// Вот такая херотень...
				XmlNameTable xnt = new NameTable();
				XmlParserContext xparCon = new XmlParserContext(xnt, new XmlNamespaceManager(xnt), null, XmlSpace.Preserve);
				xparCon.BaseURI = Request.Url.ToString();

				/*
				XmlReaderSettings settings = new XmlReaderSettings();
				settings.ProhibitDtd = false;
				reader = XmlTextReader.Create(stm, settings, xparCon);
				*/
				reader = new XmlTextReader(stm, XmlNodeType.Document, xparCon);

				// AS: если не указать ресолвер с какими-то крендетиалами (у меня по умолчанию),
				// AS: то в XSL функция document() работать ни хуя не будет, втыкать MSDN
				XmlUrlResolver resolver = new XmlUrlResolver();
				resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

				var xslt = GetOrLoadXslt(TemplateFile);

				var xdoc = new XmlDocument();
				xdoc.Normalize();
				xdoc.Load(reader);

				outputStream.Flush();
				writer = new XmlTextWriter(outputStream, Encoding.UTF8);

				var args = new XsltArgumentList();
				DateTime nowDate = DateTime.Now;
				args.AddParam("date", "", nowDate.ToString());

				//
				if (string.Equals(Response.ContentType, "application/xhtml+xml", StringComparison.OrdinalIgnoreCase)
					&& IsMobile)
				{
					Response.ContentType = "text/html";
				}

				if (string.Equals(Response.ContentType, "text/html", StringComparison.OrdinalIgnoreCase)
					|| string.Equals(Response.ContentType, "application/xhtml+xml", StringComparison.OrdinalIgnoreCase))
				{
					var brVer = GetInternetExplorerVersion();
					//if (brVer >= 7 || brVer < 0)
					if (brVer >= 8)
						writer.WriteRaw(
							"<!DOCTYPE html>");
					else if (brVer >= 7)
						writer.WriteRaw(
							"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
					else
						writer.WriteRaw("<!DOCTYPE html>");
				}
				using (new Stopwatcher($"{GetType().Name}: Document transformation by " + Path.GetFileName(TemplateFile), null, ContextHelper.StopwatchLoggingThreshold))
					xslt.Transform(xdoc, args, writer, resolver); //(xdoc, args, writer, resolver);

#if (DEBUG && XML)
				string tfileout = TemplateFile.Length > 0 ? TemplateFile : "page";
				tfileout = Path.GetFileName(tfileout);
				string fnOutput = ConfigurationManager.AppSettings["debugOutputFile"] +
					tfileout + "_" +
					OperatorId.ToString() +
					".output";
				FileStream fsOutput = null;
				try
				{
					try
					{
						fsOutput = new FileStream(fnOutput, FileMode.Truncate);
					}
					catch (Exception)
					{
						fsOutput = new FileStream(fnOutput, FileMode.Create);
					}
					if (fsOutput != null)
					{
						XmlTextWriter writerd = new XmlTextWriter(fsOutput, Encoding.UTF8);
						xslt.Transform(xdoc, args, writerd, resolver);
						writerd.Close();
					}
				}
				catch (Exception)
				{
				}
				finally
				{
					if (fsOutput != null)
						fsOutput.Close();
				}
#endif
				outputStream.Flush();
			}
			catch (ArgumentException ex)
			{
				WriteLog(ex);
				throw;
			}
			catch (XsltException ex)
			{
				WriteLog(ex);
				throw;
			}
			catch (XmlException ex)
			{
				WriteLog(ex);
				throw;
			}
			catch (OutOfMemoryException ex)
			{
				WriteLog(ex);
				throw;
			}
			catch (FileNotFoundException ex)
			{
				WriteLog(ex);
				throw;
			}
			catch (Exception ex)
			{
				WriteLog(ex);
				throw;
			}
			finally
			{
				if (reader != null)
					reader.Close();
				if (writer != null)
					writer.Close();
			}
	}
	private static readonly ConcurrentDictionary<string, XsltBucket> XslTemplates =
		new ConcurrentDictionary<string, XsltBucket>();
	private class XsltBucket
	{
		public DateTime     FileDateTime;
		public XslTransform Xslt;
	}
	private static XslTransform LoadXslt(string templateFile)
	{
		SysDiags::Trace.TraceInformation("Loading template " + templateFile);
		var xslt = new XslTransform();
		xslt.Load(templateFile);
		return xslt;
	}
	private static XslTransform GetOrLoadXslt(string templateFile)
	{
		XsltBucket xsltBucket;
		var lastWriteTime = File.GetLastWriteTime(templateFile);
		if (XslTemplates.TryGetValue(templateFile, out xsltBucket))
		{
			lock (xsltBucket)
			{
#if !DEBUG
				if (lastWriteTime != xsltBucket.FileDateTime)
#endif
				{
					xsltBucket.FileDateTime = lastWriteTime;
					xsltBucket.Xslt         = LoadXslt(templateFile);
				}
			}
		}
		else
		{
			xsltBucket = new XsltBucket
			{
				FileDateTime = lastWriteTime,
				Xslt         = LoadXslt(templateFile)
			};
			XslTemplates.TryAdd(templateFile, xsltBucket);
		}
		return xsltBucket.Xslt;
	}
	private float GetInternetExplorerVersion()
	{
		// Возвращает версию Internet Explorer или -1, если это другой браузер
		float rv = -1;
		HttpBrowserCapabilities browser = Request.Browser;
		if (browser.Browser == "IE")
			rv = (float)(browser.MajorVersion + browser.MinorVersion);
		return rv;
	}
	protected virtual string GetParamValue(string paramKey, string defaultValue = "")
	{
		return contextHelper.GetParamValue(paramKey, defaultValue);
	}
	/// <summary> Возвращает список значений параметров, ключи которых содержат шаблон paramKeyTemplate </summary>
	/// <param name="paramKeyTemplate"> Шаблон поиска ключа </param>
	/// <param name="value"> Значение параметра (если пустая строка, возвращает все значения, удовлетворяющие шаблону ключа) </param>
	/// <returns></returns>
	protected virtual Dictionary<string, string> GetParamValues(string paramKeyTemplate, string value)
	{
		Dictionary<string, string> res = new Dictionary<string, string>();
		System.Collections.Specialized.NameValueCollection pars = null;
		switch (Request.HttpMethod.ToLower())
		{
			case "get":
				pars = Request.QueryString;
				break;
			case "post":
				pars = Request.Form;
				break;
		}

		foreach (string key in pars.AllKeys)
		{
			if (key.IndexOf(paramKeyTemplate) >= 0)
			{
				string paramval = pars[key];
				if (value == string.Empty || paramval == value)
				{
					res.Add(key, paramval);
				}
			}
		}

		return res;
	}
	protected virtual List<int> GetParamListValues(string paramTemplate)
	{
		var res = new List<int>();
		System.Collections.Specialized.NameValueCollection pars = null;
		switch (Request.HttpMethod.ToLower())
		{
			case "get":
				pars = Request.QueryString;
				break;
			case "post":
				pars = Request.Form;
				break;
		}

		foreach (var key in pars.AllKeys)
		{
			if (key.IndexOf(paramTemplate, StringComparison.Ordinal) < 0)
				continue;

			var paramVal = pars[key];
			var idString = key.Replace(paramTemplate + "_", string.Empty);
			int id;
			if (paramVal == "on" && int.TryParse(idString, out id))
				res.Add(id);
		}

		return res;
	}
	/// <summary> Возврат значения по ключу из ресурса </summary>
	protected virtual string GetMessageData(string key)
	{
		return contextHelper.GetMessageData(key);
	}
	protected virtual string GetLocaleMessage(string key)
	{
		return contextHelper.GetLocalMessage(key);
	}
	protected virtual string GetErrorMessageFormat(Enum value)
	{
		var enumName = value.GetType().Name;
		var fullKey = string.Format(CurrentCulture, "{0}_{1}", enumName, value);
		var message = GetMessageData(fullKey);
		if (string.IsNullOrEmpty(message))
			message = GetMessageData(value.ToString());
		if (string.IsNullOrEmpty(message))
			message = GetMessageData("ServerError");
		return message;
	}
	protected virtual string GetMessageData(string key, CultureInfo cultInfo)
	{
		return contextHelper.GetMessageData(key, cultInfo);
	}
	protected virtual string GetMessageData<T>(string key)
		where T : class
	{
		return contextHelper.GetMessageData<T>(key);
	}
	protected virtual string GetMessageData<T>(string key, CultureInfo culture)
		where T : class
	{
		return contextHelper.GetMessageData<T>(key, culture);
	}
	private static HashSet<string> _obsoleteHosts;
	private static readonly object ObsoleteHostsLock = new object();
	private static HashSet<string> ObsoleteHosts
	{
		get
		{
			if (_obsoleteHosts != null)
				return _obsoleteHosts;

			lock (ObsoleteHostsLock)
			{
				if (_obsoleteHosts != null)
					return _obsoleteHosts;
				_obsoleteHosts = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
				var config = ConfigurationManager.AppSettings["ObsoleteHosts"];
				if (!string.IsNullOrWhiteSpace(config))
				{
					foreach (var item in config.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
						_obsoleteHosts.Add(item);
				}
			}

			return _obsoleteHosts;
		}
	}
	/// <summary> Инициализация страницы </summary>
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		response = HttpContext.Current.Response;
		var siteUrl = ConfigurationManager.AppSettings["webSiteUrl"];

		if (ObsoleteHosts.Contains(Request.Url.Host))
		{
			response.Clear();
			response.Status = "302 Found";
			response.AddHeader("Location",
				siteUrl + (Request.ApplicationPath == null || Request.ApplicationPath.Length == 1
					? "/"
					: Request.ApplicationPath + "/") +
				(Request.Url.OriginalString.Contains("mobile") ? "mobile" : ""));
			response.End();
			response.Flush();
		}

		MemoryStream mem = new MemoryStream();
		writer = new XmlTextWriter(mem, Encoding.UTF8);

		PreRenderComplete += new EventHandler(OnLoadComplete);
		PreRender         += new EventHandler(documentHeaderRenderer);
	}
	/// <summary> Базовый вызов Page_Load'a </summary>
	protected virtual void Page_Load(object sender, EventArgs e)
	{
		MandatoryPageLoad();
		PageLoadInternal();
	}
	protected void MandatoryPageLoad()
	{
		CheckOddSlash();
	}
	private void CheckOddSlash()
	{
		var url = Request.Url;
		var lastUrlSegment = url.Segments.Last();
		if (!lastUrlSegment.EndsWith(".aspx/", StringComparison.OrdinalIgnoreCase))
			return;

		var newUrl = url.Segments.Take(url.Segments.Length - 1).Join(string.Empty);
		newUrl += lastUrlSegment.Substring(0, lastUrlSegment.Length - 1);
		newUrl += url.Query;
		SysDiags::Trace.TraceInformation("Redirecting to {0} because of odd slash", newUrl);
		Response.Redirect(newUrl);
	}
	private void PageLoadInternal()
	{
		if (!string.IsNullOrEmpty(GetParamValue("isMobile"))               ||
			Request.QueryString["isMobile"] != null                        ||
			Request.Path.Contains(Request.Url.OriginalString + @"/mobile") ||
			(Session["isMobile"] != null && Session["isMobile"].ToString() == "true"))
			IsMobile = true;
		else
			IsMobile = false;

		var a = GetParamValue("a");
		if (!IsUserLogged && (string.IsNullOrEmpty(a) || a != "isalive"))
		{
			if (Request.IsAjaxRequest())
			{
				Unauthorized();
				return;
			}

			string applicationPath = (Request.ApplicationPath == "/" ? "" : Request.ApplicationPath);

			if (IsMobile)
			{
				Response.ContentType = "text/html";
				Response.Buffer = false;
				var responseResult = new StringBuilder();
				responseResult.Append("<script type='text/javascript'>location.href='" + applicationPath +
					"/mobile/?backUrl=" + Server.UrlEncode(Request.Url.ToString()) + "'</script>");
				Response.Write(responseResult);
				Response.Flush();
				Response.End();
			}
			else
			{
				RenderUnloggedPage();
			}
			return;
		}

		contextHelper.RedirectToSettingsPageIfNeeded();

		if (!Page.IsPostBack)
		{
			Process();
			UpdateSessionDateTime();
		}
	}
	protected void Unauthorized()
	{
		contextHelper.Unauthorized();
	}
	protected virtual void documentHeaderRenderer(object sender, EventArgs e)
	{
	}
	protected virtual void OnLoadComplete(object sender, EventArgs e)
	{
		FinishPageRender();
	}
	protected virtual void InsertDOCTypeDeclaration(bool strict)
	{
		/*
		if (strict)
		{
			response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
		}
		else
		{
			response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		}
		*/
	}
	protected virtual void FinishPageRender()
	{
		//text/plain is used for json
		if (response.ContentType != "text/plain" &&
			response.ContentType != "application/json" &&
			response.ContentType != JpegContentType)
		{
			Writer.Flush();
			if ((TemplateFile != null) && (TemplateFile.Length > 0))
			{
				if (!contentTypeSet)
				{
					SetHtmlContentType();
				}

				/*byte[] buffer = System.Text.Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
				response.OutputStream.Write(buffer, 0, (int)buffer.Length);*/
				/*byte[] buffer = System.Text.Encoding.UTF8.GetBytes("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n");
				response.OutputStream.Write(buffer, 0, (int)buffer.Length);*/

				WriteXmlToStream(Writer.BaseStream, response.OutputStream);
			}
			else
			{
				try
				{
					if (!contentTypeSet)
					{
						SetXmlContentType();
					}
					response.Write(GetStringOfStream(Writer.BaseStream));
				}
				catch (ArgumentNullException aex)
				{
					WriteLog(aex);
				}
			}
		}
		Writer.Close();
	}
	/// <summary> Найдем методы, соответствующие указанному в атрибуте PageActionAttribute действию и выполним их </summary>
	protected virtual void Process()
	{
		// Получить HTTP метод запроса (GET,POST...)
		var httpMethod = GetHttpMethod();
		// Получить имя действия
		var httpAction = GetParamValue("a", "default")
			?.Trim();
#if DEBUG
		if (webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Verbose))
		{
			if (ConfigurationManager.AppSettings.AllKeys.Contains("debugOutputFile"))
			{
				var fileName = ConfigurationManager.AppSettings["debugOutputFile"] +
					DateTime.Now.ToString("yyMMdd-HHmmss.fff") +
					"_" +
					Request.Url.AbsolutePath
						.Replace("/", "_")
						.Replace(".", "_")
						.Replace("?", "_") +
					"_" + httpAction + "_Req" +
					".txt";
				Request.SaveAs(fileName, true);
			}
		}
#endif
		try
		{
			// Получить метод обработчик метода API
			var pageAction = GetType().GetMethods()
				?.SelectMany(m =>
					m.GetCustomAttributes<PageActionAttribute>(false)
						?.Where(a => a.Action == httpAction)
						?.Select(a => new { Method = m, Action = a }))
				?.FirstOrDefault(x => x.Action.Method == HttpMethod.Unspecified || x.Action.Method == httpMethod);

			if (pageAction == null)
			{
				SendHttpResult(400, "Bad Request");
				return;
			}

			var method = pageAction.Method;
			var action = pageAction.Action;

			using (new Stopwatcher($"{Request.Url.GetLeftPart(UriPartial.Path)}/a={httpAction}", null, ContextHelper.StopwatchLoggingThreshold))
			{
				var results = default(object);
				try
				{
					WebApiTraceBeg(httpAction);
					//get method parameters
					switch (action.ResponseType)
					{
						case DataType.Json:
							SetJsonContentType();
							break;
						case DataType.Empty:
							SetTextContentType();
							break;
					}

					results = CallMethod(method, action.RequestType);

					if (null != results && action.ResponseType == DataType.Empty)
						SysDiags::Trace.TraceWarning("{0} marked with DataType.Empty, but return non-null value", method.Name);

					if (action.ResponseType == DataType.Json)
					{
						response.Write(JsonHelper.SerializeObjectToJson(results));
					}
				}
				catch (ThreadAbortException)
				{
					// При перенаправлении(redirect), вызванном в method.Invoke
					return;
				}
				catch (SecurityException ex)
				{
					SysDiags::Trace.TraceError("{0}: {1}", method.Name, ex);
					Forbidden(ex);
				}
				catch (OperationBanException ex)
				{
					LogError(ex);
					SendHttpResult(429, "Banned");
				}
				catch (ArgumentException ex)
				{
					SysDiags::Trace.TraceError("{0}: {1}", method.Name, ex);
					SendHttpResult(400, "Bad Request");
				}
				catch (TargetInvocationException ex)
				{
					if (ex.InnerException != null)
					{
						LogError(ex.InnerException, method, action);

						if (ex.InnerException is SecurityException)
						{
							Forbidden(ex.InnerException as SecurityException);
							return;
						}

						if (ex.InnerException is ArgumentException)
						{
							SendHttpResult(400, "Bad Request");
							return;
						}

						if (ex.InnerException is OperationBanException)
						{
							SendHttpResult(429, "Banned");
							return;
						}

						if (ex.InnerException is ValidationException)
						{
							var validationException = ex.InnerException as ValidationException;
							SendHttpResult(400, validationException.Message);
							return;
						}
					}
					else
					{
						LogError(ex, method, action);
					}
					SendHttpResult(500, "Server Error");
				}
				catch (Exception ex)
				{
					LogError(ex, method, action);
					SendHttpResult(500, "Server Error");
				}
				finally
				{
					WebApiTraceEnd(httpAction, results);
				}
			}
		}
		catch (ThreadAbortException)
		{
			//При редиректе, вызванном в method.Invoke
		}
		catch (Exception ex)
		{
			LogError(ex);
		}
	}
	[SysDiags::Conditional("TRACE")]
	private void WebApiTraceAll(string action)
	{
		var hasInfo = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Information);
		if (!hasInfo)
			return;

		var hasVerb = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Verbose);
		// Get Request data
		var reqHeadersNvc = Request.Headers;
		var reqHeadersTxt = hasVerb && 0 < reqHeadersNvc.Count
			? string.Join("\n\t\t", reqHeadersNvc.AllKeys.Select(h => $"{h}: {reqHeadersNvc[h.ToString()]}"))
			: string.Empty;
		var reqCookiesNoc = Request.Cookies;
		var reqCookiesTxt = hasVerb && 0 < reqCookiesNoc.Count
			? string.Join("\n\t\t", reqCookiesNoc.AllKeys.Select(c => $"{c}: {reqCookiesNoc[c.ToString()].Value}, { reqCookiesNoc[c.ToString()].Expires.ToString("yyMMdd-HHmmss")}"))
			: string.Empty;
		var reqQryParsNvc = Request.QueryString;
		var reqQryParsTxt = hasVerb && 0 < reqQryParsNvc.Count
			? string.Join("\n\t\t", reqQryParsNvc.AllKeys.Select(q => $"{q}: {reqQryParsNvc[q.ToString()]}"))
			: string.Empty;
		var reqPstParsNvc = Request.Form;
		var reqPstParsTxt = hasVerb && 0 < reqPstParsNvc.Count
			? string.Join("\n\t\t", reqPstParsNvc.AllKeys.Select(p => $"{p}: {HttpUtility.HtmlDecode(reqPstParsNvc[p.ToString()])}"))
			: string.Empty;

		// Get Response data
		var resHeadersNvc = Response.Headers;
		var resHeadersTxt = hasVerb && 0 < resHeadersNvc.Count
			? string.Join("\n\t\t", resHeadersNvc.AllKeys.Select(h => $"{h}: {resHeadersNvc[h.ToString()]}"))
			: string.Empty;
		var resCookiesNoc = Response.Cookies;
		var resCookiesTxt = hasVerb && 0 < resCookiesNoc.Count
			? string.Join("\n\t\t", resCookiesNoc.AllKeys.Select(c => $"{c}: {resCookiesNoc[c.ToString()].Value}, Expires: {resCookiesNoc[c.ToString()].Expires.ToString("yyyyMMdd-HHmmss")}"))
			: string.Empty;

		var resSesParsNoc = Session.Keys.OfType<string>().ToList();
		var resSesParsTxt = hasVerb && 0 < resSesParsNoc.Count
			? string.Join("\n\t\t", resSesParsNoc.Select(c => $"{c}: {Session[c]}"))
			: string.Empty;

		webApiTraceSource.TraceEvent(SysDiags::TraceEventType.Information, 0,
			" {0} {1}?a={2}\n\tASP.NET_SessionId={3} (Timeout={4}min), ssid={5}, Login={6} (OperatorId={7}), AppId={8}, ClientVersion={9}{10}",
			Request.HttpMethod, Request.Url.GetLeftPart(UriPartial.Path), action, Session.SessionID, Session.Timeout, SessionId, LoggedUser, OperatorId, AppId, ClientVersion,
			(0 < reqHeadersTxt.Length ? $"\n\tReq.Headers(Count={reqHeadersNvc.Count}):\n\t\t" + reqHeadersTxt : "") +
			(0 < reqCookiesTxt.Length ? $"\n\tReq.Cookies(Count={reqCookiesNoc.Count}):\n\t\t" + reqCookiesTxt : "") +
			(0 < reqQryParsTxt.Length ? $"\n\tReq.QryPars(Count={reqQryParsNvc.Count}):\n\t\t" + reqQryParsTxt : "") +
			(0 < reqPstParsTxt.Length ? $"\n\tReq.PstPars(Count={reqPstParsNvc.Count}):\n\t\t" + reqPstParsTxt : "") +
			(0 < resHeadersTxt.Length ? $"\n\tRes.Headers(Count={resHeadersNvc.Count}):\n\t\t" + resHeadersTxt : "") +
			(0 < resCookiesTxt.Length ? $"\n\tRes.Cookies(Count={resCookiesNoc.Count}):\n\t\t" + resCookiesTxt : "") +
			(0 < resSesParsTxt.Length ? $"\n\tRes.SesPars(Count={resSesParsNoc.Count}):\n\t\t" + resSesParsTxt : ""));
	}
	[SysDiags::Conditional("TRACE")]
	private void WebApiTraceBeg(string action)
	{
		var hasInfo = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Information);
		if (!hasInfo)
			return;

		var hasVerb = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Verbose);
		// Get Request data
		var reqHeadersNvc = Request.Headers;
		var reqHeadersTxt = hasVerb && 0 < reqHeadersNvc.Count
			? string.Join("\n\t\t", reqHeadersNvc.AllKeys.Select(h => $"{h}: {reqHeadersNvc[h.ToString()]}"))
			: string.Empty;
		var reqCookiesNoc = Request.Cookies;
		var reqCookiesTxt = hasVerb && 0 < reqCookiesNoc.Count
			? string.Join("\n\t\t", reqCookiesNoc.AllKeys.Select(c => $"{c}: {reqCookiesNoc[c.ToString()].Value}, Expires: {reqCookiesNoc[c.ToString()].Expires.ToString("yyyyMMdd-HHmmss")}"))
			: string.Empty;
		var reqQryParsNvc = Request.QueryString;
		var reqQryParsTxt = hasVerb && 0 < reqQryParsNvc.Count
			? string.Join("\n\t\t", reqQryParsNvc.AllKeys.Select(q => $"{q}: {reqQryParsNvc[q.ToString()]}"))
			: string.Empty;
		var reqPstParsNvc = Request.Form;
		var reqPstParsTxt = hasVerb && 0 < reqPstParsNvc.Count
			? string.Join("\n\t\t", reqPstParsNvc.AllKeys.Select(p => $"{p}: {HttpUtility.HtmlDecode(reqPstParsNvc[p.ToString()])}"))
			: string.Empty;
		webApiTraceSource.TraceEvent(SysDiags::TraceEventType.Information, 0, string.Empty.PadRight(30, '='));
		webApiTraceSource.TraceEvent(SysDiags::TraceEventType.Information, 0,
			" BEG {0} {1}?a={2}\n\tASP.NET_SessionId={3} (Timeout={4}min), ssid={5}, Login={6} (OperatorId={7}), AppId={8}, ClientVersion={9}{10}",
			Request.HttpMethod, Request.Url.GetLeftPart(UriPartial.Path), action, Session.SessionID, Session.Timeout, SessionId, LoggedUser, OperatorId, AppId, ClientVersion,
			(0 < reqHeadersTxt.Length ? $"\n\tReq.Headers(Count={reqHeadersNvc.Count}):\n\t\t" + reqHeadersTxt : "") +
			(0 < reqCookiesTxt.Length ? $"\n\tReq.Cookies(Count={reqCookiesNoc.Count}):\n\t\t" + reqCookiesTxt : "") +
			(0 < reqQryParsTxt.Length ? $"\n\tReq.QryPars(Count={reqQryParsNvc.Count}):\n\t\t" + reqQryParsTxt : "") +
			(0 < reqPstParsTxt.Length ? $"\n\tReq.PstPars(Count={reqPstParsNvc.Count}):\n\t\t" + reqPstParsTxt : ""));
	}
	[SysDiags::Conditional("TRACE")]
	private void WebApiTraceEnd(string action, object results = null)
	{
		var hasInfo = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Information);
		if (!hasInfo)
			return;

		var hasVerb = webApiTraceSource.Switch.Level.HasFlag(SysDiags::SourceLevels.Verbose);
		// Get Response data
		var resHeadersNvc = Response.Headers;
		var resHeadersTxt = hasVerb && 0 < resHeadersNvc.Count
			? string.Join("\n\t\t", resHeadersNvc.AllKeys.Select(h => $"{h}: {resHeadersNvc[h.ToString()]}"))
			: string.Empty;
		var resCookiesNoc = Response.Cookies;
		var resCookiesTxt = hasVerb && 0 < resCookiesNoc.Count
			? string.Join("\n\t\t", resCookiesNoc.AllKeys.Select(c => $"{c}: {resCookiesNoc[c.ToString()].Value}, Expires: {resCookiesNoc[c.ToString()].Expires.ToString("yyyyMMdd-HHmmss")}"))
			: string.Empty;

		var resSesParsNoc = Session.Keys.OfType<string>().ToList();
		var resSesParsTxt = hasVerb && 0 < resSesParsNoc.Count
			? string.Join("\n\t\t", resSesParsNoc.Select(c => $"{c}: {Newtonsoft.Json.JsonConvert.SerializeObject(Session[c], Newtonsoft.Json.Formatting.Indented)}"))
			: string.Empty;

		var resResultsObj = results;
		var resResultsTxt = hasVerb && null != resResultsObj
			? string.Join("\n\t\t", Newtonsoft.Json.JsonConvert.SerializeObject(resResultsObj, Newtonsoft.Json.Formatting.Indented))
			: string.Empty;

		webApiTraceSource.TraceEvent(SysDiags::TraceEventType.Information, 0,
			" END {0} {1}?a={2}\n\tASP.NET_SessionId={3} (Timeout={4}min), ssid={5}, Login={6} (OperatorId={7}), AppId={8}, ClientVersion={9}{10}",
			Request.HttpMethod, Request.Url.GetLeftPart(UriPartial.Path), action, Session.SessionID, Session.Timeout, SessionId, LoggedUser, OperatorId, AppId, ClientVersion,
			(0 < resHeadersTxt.Length ? $"\n\tRes.Headers(Count={resHeadersNvc.Count}):\n\t\t" + resHeadersTxt : "") +
			(0 < resCookiesTxt.Length ? $"\n\tRes.Cookies(Count={resCookiesNoc.Count}):\n\t\t" + resCookiesTxt : "") +
			(0 < resSesParsTxt.Length ? $"\n\tRes.SesPars(Count={resSesParsNoc.Count}):\n\t\t" + resSesParsTxt : "") +
			(0 < resResultsTxt.Length ? $"\n\tRes.Results" +                         ":\n"     + resResultsTxt : ""));
		webApiTraceSource.TraceEvent(SysDiags::TraceEventType.Information, 0, string.Empty.PadRight(30, '='));
	}
	private void LogError(Exception e, MethodInfo method = null, PageActionAttribute pageActionAttribute = null)
	{
		if (e is ThreadAbortException)
			return;
		var innerThreadAbortException = e.InnerException as ThreadAbortException;
		if (innerThreadAbortException != null)
			return;

		if (method != null)
		{
			object[] parameters = null;
			if (pageActionAttribute != null)
			{
				try
				{
					parameters = GetMethodParameters(method, pageActionAttribute.RequestType);
				}
				catch (Exception)
				{
					SysDiags::Trace.TraceError("{0}.LogError - unable to retrieve parameters for method {1}", this, method);
				}
			}

			SysDiags::Trace.TraceError(
				"{0} - {1} ({2}): {3}",
				Request.Url,
				method.Name,
				parameters != null ? parameters.Select(p => p != null ? p.ToString() : "").Join(",") : null,
				e);
		}
		else
		{
			SysDiags::Trace.TraceError("{0}: {1}", Request.Url, e);
		}
	}
	/// <summary> Завершает обработку вызова отправкой кода 403 </summary>
	protected void Forbidden(SecurityException exception = null)
	{
		contextHelper.Forbidden(exception?.Message);
	}
	/// <summary> Завершает обработку вызова баном </summary>
	protected void Ban(DateTime banDate)
	{
		SetJsonContentType();
		var localDate = TimeHelperEx.GetLocalTime(banDate, TimeZoneInfo);
		var banResponse = new BanResponse(localDate);
		var serializeObjectToJson = JsonHelper.SerializeObjectToJson(banResponse);
		response.Write(serializeObjectToJson);
	}
	/// <summary> Завершает обработку вызова отправкой кода 400 </summary>
	protected void BadRequest(string description)
	{
		contextHelper.BadRequest(description);
	}
	/// <summary> Завершает обработку вызова отправкой кода 404 </summary>
	protected void NotFound(string description)
	{
		contextHelper.NotFound(description);
	}
	private void SendHttpResult(int statusCode, string status)
	{
		contextHelper.SendHttpResult(statusCode, status);
	}
	private HttpMethod GetHttpMethod()
	{
		switch (Request.HttpMethod)
		{
			case "GET":
				return HttpMethod.Get;
			case "POST":
				return HttpMethod.Post;
			case "DELETE":
				return HttpMethod.Delete;
			default:
				throw new NotSupportedException("Http method " + Request.HttpMethod + " is not supported");
		}
	}
	private object[] GetMethodParameters(MethodInfo method, DataType requestType)
	{
		var @params = method.GetParameters();
		if (@params.Length == 0)
			return null;

		var methodParams = new List<object>();
		methodParams.AddRange(
			@params.Select(
				delegate (ParameterInfo p)
				{
					var paramValueString = GetParamValue(
						p.Name, p.IsOptional ? p.DefaultValue as string : string.Empty);

					if (requestType == DataType.Json)
					{
						if (paramValueString == null && p.IsOptional)
							return p.DefaultValue;

						if (string.IsNullOrWhiteSpace(paramValueString))
							return null;

						if (p.ParameterType == typeof(string))
							return paramValueString;
						try
						{
							try
							{
								return JsonHelper2.FromJson(paramValueString, p.ParameterType);
							}
							catch (Exception ex)
							{
								string.Concat(
									$"Unable deserialize parameter '{p.Name}' from Json with '{nameof(JsonHelper2)}.{nameof(JsonHelper2.FromJson)}'",
									$"\nMETHOD NAME: {CallStackHelper.GetMethodFullName(method)}(..., {p.ParameterType.FullName} {p.Name}, ...)",
									$"\nPARAMETER VALUE:",
									$"\n{paramValueString}")
									.WithException(ex, true)
									.CallTraceWarning();
							}
							return JsonHelper.DeserializeObjectFromJson(p.ParameterType, paramValueString);
						}
						catch (JsonException ex)
						{
							string.Concat(
								$"Unable deserialize parameter '{p.Name}' from Json with '{nameof(JsonHelper)}.{nameof(JsonHelper.DeserializeObjectFromJson)}'",
								$"\nMETHOD NAME: {CallStackHelper.GetMethodFullName(method)}(..., {p.ParameterType.FullName} {p.Name}, ...)",
								$"\nPARAMETER VALUE:",
								$"\n{paramValueString}")
								.WithException(ex, true)
								.CallTraceError();

							throw new ArgumentException($"Unable deserialize parameter '{p.Name}' from Json", ex);
						}
					}
					return GetParamValue(p.Name, p.IsOptional ? p.DefaultValue as string : string.Empty);
				}));
		return methodParams.ToArray();
	}
	private object CallMethod(MethodInfo method, DataType requestType)
	{
		var @params = GetMethodParameters(method, requestType);

		return method.Invoke(this, @params);
	}
	/// <summary> Записать в переменную сессии объект </summary>
	protected virtual void SetObjectToSession(string key, object val)
	{
		contextHelper.SetObjectToSession(key, val);
	}
	/// <summary> Вытащить значение из сессии по ключу </summary>
	protected virtual object GetObjectFromSession(string key)
	{
		return contextHelper.GetObjectFromSession(key);
	}
	/// <summary> Получение даты последнего обновления сессии </summary>
	protected virtual DateTime GetSessionDateTime()
	{
		DateTime sessionDate = (DateTime)GetObjectFromSession("_TSS_SessionDateTime");

		return sessionDate;
	}
	/// <summary> Обновление даты сессии </summary>
	protected virtual void UpdateSessionDateTime()
	{
		SetObjectToSession("_TSS_SessionDateTime", DateTime.Now);
	}
	/// <summary> Проверяет, не устарела ли сессия </summary>
	protected virtual bool IsSessionOld()
	{
		bool result = false;
		return result;
	}
	[PageAction("deletesubscription", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
	public void DeleteSubscription(int subscriptionid)
	{
		IWebPS.DeleteSubscription(subscriptionid);
	}
	/// <summary> Получение списка подписки на рассылку отчетов </summary>
	[PageAction("getSubscriptionsJSON", RequestType = DataType.Empty, ResponseType = DataType.Json)]
	public List<ReportSubscription> GetSubscriptionsJSON()
	{
		return IWebPS.GetSubscriptions();
	}
	/// <summary> Получение списка объектов слежения - общее для мобильной и обычной версий </summary>
	[PageAction("getvehiclesJSON")]
	public virtual void GetVehiclesJSON()
	{
		WrapJsonAction(() =>
		{
			var vehicles = GetVehiclesDTO(true);
			var resObj = new Dictionary<string, object> { { "value", vehicles } };
			if (!string.IsNullOrEmpty(GetParamValue("loadVehicleGroups")))
				resObj.Add("vehicleGroups", GetVehicleGroupsDTO());
			if (IWebPS.IsGuest)
				resObj.Add("isGuest", true);
			return resObj;
		});
	}
	protected Group[] GetVehicleGroupsDTO(
		int[] vehicleGroupIds      = null,
		bool includeNotInGroups    = false,
		bool includeAll            = false,
		bool includeStandardGroups = false,
		bool includeIgnored        = false)
	{
		return IWebPS.GetVehicleGroups(
			new GetGroupsArguments
			{
				Ids                = vehicleGroupIds,
				IncludeNotInGroups = includeNotInGroups,
				IncludeAll         = includeAll,
				IncludeStandard    = includeStandardGroups,
				IncludeIgnored     = includeIgnored
			}).ToArray();
	}
	protected TimeSpan? GetOnlineTime()
	{
		int hrs;
		var onlineHoursCookie = Request.Cookies["onlinehrs"];

		if (onlineHoursCookie == null || !int.TryParse(onlineHoursCookie.Value, out hrs))
			hrs = 24;

		return TimeSpan.FromHours(hrs);
	}
	protected Dictionary<int, Vehicle> GetVehiclesDTO(bool includeLastData)
	{
		return GetVehiclesDTO(includeLastData, Enumerable.Empty<SystemRight>());
	}
	protected Dictionary<int, Vehicle> GetVehiclesDTO(bool includeLastData, params int[] vehicleIds)
	{
		return GetVehiclesDTO(includeLastData, Enumerable.Empty<SystemRight>(), vehicleIds);
	}
	protected Dictionary<int, Vehicle> GetVehiclesDTO(bool includeLastData, IEnumerable<SystemRight> rights, params int[] vehicleIds)
	{
		var showMode = GetParamValue("show");
		var onlineFilter = showMode == "offline"
			? VehicleOnlineFilter.Offline
			: showMode == "online"
				? VehicleOnlineFilter.Online
				: (VehicleOnlineFilter?)null;

		var arguments = new GetVehiclesArgs
		{
			IncludeAddresses = GetParamValue("includeAddresses") == "true",
			VehicleIDs       = vehicleIds,
			IncludeLastData  = includeLastData,
			MapGuid          = MapGuid,
			OnlineTime       = GetOnlineTime(),
			OnlineFilter     = onlineFilter
		};

		var resVehicles = IWebPS.GetVehicles(arguments)
			.Where(item => rights == null || rights.All(item.rights.Contains))
			.Select(item =>
			{
				item.iconUrl = Vehicles.VehiclesInfo.ProcessIconUrl(item.iconUrl);
				return item;
			}).ToList();

		return resVehicles.ToDictionary(v => v.id);
	}
	/// <summary> Получение списка объектов слежения - общее для мобильной и обычной версий </summary>
	//[PageAction("getvehicles")]
	public virtual void GetVehicles()
	{
		string formated = GetParamValue("formated");
		if (formated == "")
		{
			if (IsMobile)
			{
				SetHtmlContentType();
				TemplateFile = "default";
				AddProperty("template", "mobileObjectList");
			}
			else
			{
				// AS: здесь так же все прогонится через XSL
				SetXmlContentType();
				TemplateFile = "trackingObject_G";
				AddProperty("template", "getVehicles");
				/*TemplateFile = "google";
				AddProperty("template", "getVehicles");*/
			}
		}

		var vehicles = GetVehiclesDTO(true);

		var showMode = GetParamValue("show");
		if (string.IsNullOrEmpty(showMode))
			showMode = "all";

		AddProperty("showMode", showMode);

		var sid = GetParamValue("selectedid");
		//AddProperty("selected", sid);

		ResponseSerializationBeg();
		int selectedId;
		int.TryParse(sid, out selectedId);

		Writer.WriteStartElement("vehicles");
		Writer.WriteAttributeString("selected", selectedId.ToString());
		foreach (var vi in vehicles.Values)
			vi.WriteXml(Writer);
		Writer.WriteEndElement(); // vehicles

		SerializeMapDescriptions();

		ResponseSerializationEnd();
	}
	protected void RenderLoginPage()
	{
		if (IsMobile)
			TemplateFile = "default";
		else
		{
			RedirectToAboutPageIfNeeded();
			TemplateFile = "login";
		}

		AddProperty("template", "login");

		var page = GetParamValue("page");
		if (!string.IsNullOrWhiteSpace(page))
		{
			AddProperty("selectedPage", page.ToLower());
		}

		AddTitleProperty();

		AddProperty("previousLogin", GetLogin());
		AddProperty("counters-enabled", ConfigurationManager.AppSettings["counters-enabled"] ?? "false");
		TranslateConfig("online-assistant");
		TranslateConfig("GuestLogin");

		RenderPromo();
	}
	protected void RenderPromo()
	{
		ResponseSerializationBeg();
		WritePromoXml();
		ResponseSerializationEnd();
	}
	private void WritePromoXml()
	{
		if (Request.PhysicalApplicationPath == null)
		{
			SysDiags::Trace.WriteLine("Request.ApplicationPath is null");
			return;
		}

		string cultureName = CurrentCulture.Name;

		var promoPrefix = WebConfigurationManager.AppSettings["promoPrefix"];

		if (promoPrefix == null)
			return;

		var promoPagePath = Path.Combine(Request.PhysicalApplicationPath,
			@"templates\xsl\res\promo." + promoPrefix + cultureName + ".xml");

		if (string.IsNullOrWhiteSpace(promoPagePath))
			return;
		if (!File.Exists(promoPagePath))
		{
			SysDiags::Trace.TraceError("File not found: " + promoPagePath);
			promoPagePath = Path.Combine(
				Request.PhysicalApplicationPath,
				@"templates\xsl\res\promo." + promoPrefix + WebConfigurationManager.AppSettings["defaultPromoCulture"] +
				".xml");
			if (!File.Exists(promoPagePath))
				return;
		}

		Writer.WriteStartElement("pageContent");
		Writer.WriteRaw(File.ReadAllText(promoPagePath));
		Writer.WriteEndElement();
	}
	protected void RenderUnloggedPage()
	{
		TemplateFile = "logout";
		AddProperty("template", "logout");
		AddTitleProperty();
		ResponseSerializationBegInternal();
		Writer.WriteStartElement("pageContent");
		Writer.WriteEndElement();
		ResponseSerializationEndInternal();
	}
	public string MainPageUrl
	{
		get
		{
			if (IsMobile)
				return Context.Request.ApplicationPath + "/mobile";
			var promoUrlString = ConfigurationManager.AppSettings["PromoSiteUrl"];

			if (!string.IsNullOrWhiteSpace(promoUrlString))
				return promoUrlString;

			if (Request.ApplicationPath != null)
				return Request.ApplicationPath;

			return Request.Url.Host;
		}
	}
	protected virtual void RedirectToAboutPage()
	{
		Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
		Context.Response.Redirect(MainPageUrl, true);
	}
	[PageAction("SessionClosed")]
	public virtual void RenderSessionClosedPage()
	{
		Response.Cookies.Add(new HttpCookie(ContextHelper.SessionCookieName) { Expires = DateTime.Now.AddDays(-1) });
		Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId")             { Expires = DateTime.Now.AddDays(-1) });
		if (!Request.IsAjaxRequest())
		{
			TemplateFile = "sessionclosed";
			AddProperty("template", "sessionClosed");
			AddTitleProperty();

			SetHtmlContentType();
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		else
		{
			Unauthorized();
		}
	}
	/// <summary> Запись логина в Cookies </summary>
	protected virtual void SetLogin(string login)
	{
		if (login != string.Empty)
		{
			response.Cookies.Add(new HttpCookie("WebGisLogin") { Expires = DateTime.Now.AddYears(1), Value = login });
		}
	}
	/// <summary> Достает логин из Cookies </summary>
	protected virtual string GetLogin()
	{
		if (Request.Cookies["WebGisLogin"] != null)
			return Request.Cookies["WebGisLogin"].Value;

		return string.Empty;
	}
	/// <summary> Установка типа содержимого в XML </summary>
	protected virtual void SetXmlContentType()
	{
		const string applicationXml = "application/xml";
		if (Request.AcceptTypes == null ||
			Request.AcceptTypes.Contains("*/*") ||
			Request.AcceptTypes.Any(t => t == applicationXml) ||
			Request.AcceptTypes.Any(t => t.StartsWith(applicationXml)))
			response.ContentType = applicationXml;
		else
			response.ContentType = Request.AcceptTypes.First();

		response.ContentEncoding = Encoding.UTF8;
		contentTypeSet = true;
	}
	/// <summary> Установка типа содержимого в Json </summary>
	protected virtual void SetJsonContentType()
	{
		const string applicationJson = "application/json";
		if (Request.AcceptTypes == null ||
			!Request.AcceptTypes.Any() ||
			Request.AcceptTypes.Contains("*/*") ||
			Request.AcceptTypes.Any(t => t == applicationJson) ||
			Request.AcceptTypes.Any(t => t.StartsWith(applicationJson)))
			response.ContentType = applicationJson;
		else
			response.ContentType = Request.AcceptTypes.First();

		response.ContentEncoding = Encoding.UTF8;
		contentTypeSet           = true;
	}
	/// <summary> Установка типа содержимого в text/plan </summary>
	protected virtual void SetTextContentType()
	{
		const string textPlain = "text/plain";
		if (Request.AcceptTypes == null ||
			Request.AcceptTypes.Contains("*/*") ||
			Request.AcceptTypes.Any(t => t == textPlain) ||
			Request.AcceptTypes.Any(t => t.StartsWith(textPlain)))
			response.ContentType = textPlain;
		else
			response.ContentType = Request.AcceptTypes.First();

		response.ContentEncoding = Encoding.UTF8;
		contentTypeSet = true;
	}
	/// <summary> Установка типа содержимого в HTML </summary>
	protected virtual void SetHtmlContentType()
	{
		response.ContentType     = "text/html";
		response.ContentEncoding = Encoding.UTF8;
		contentTypeSet           = true;
	}
	protected void SetJpegContentType()
	{
		Response.ContentType = "image/JPEG";
		contentTypeSet = true;
	}
	/// <summary> Установка типа содержимого в XHTML </summary>
	protected virtual void SetXhtmlContentType()
	{
		/*if (Request.Browser.Type.Contains("IE"))
			response.ContentType = "application/xml";
		else*/
		response.ContentType = "application/xhtml+xml";
		//response.ContentType = "text/xml";
		response.ContentEncoding = Encoding.UTF8;
		contentTypeSet = true;
	}
	protected virtual void SetImageGifContentType()
	{
		response.ContentType = "image/gif";
		contentTypeSet = true;
	}
	protected virtual void SetImageJpgContentType()
	{
		response.ContentType = JpegContentType;
		contentTypeSet = true;
	}
	protected virtual void SetSvgContentType()
	{
		response.ContentType = "image/svg+xml";
		contentTypeSet = true;
	}
	protected virtual void SetApplicationMultipartContentType()
	{
		response.ContentType = "application/multipart";
		contentTypeSet = true;
	}
	/// <summary> Отмена кеширования страницы </summary>
	protected virtual void dontCacheMe()
	{
		//response.AppendHeader();
		response.Cache.SetCacheability(HttpCacheability.NoCache);
		response.Cache.SetLastModified(DateTime.UtcNow);
		response.Cache.SetNoServerCaching();
		response.Cache.SetNoStore();
	}
	/// <summary> Добавление сообщения в стек сообщений </summary>
	protected void AddMessage(Message msg)
	{
		Messages.Add(msg);
	}
	protected string CurrentLanguage
	{
		get { return CurrentCulture.TwoLetterISOLanguageName; }
	}
	/// <summary> Запись в лог сообщения </summary>
	/// <param name="msg"></param>
	protected void WriteLog(string msg)
	{
		contextHelper.WriteLog(msg);
	}
	protected void WriteLog(Exception ex)
	{
		contextHelper.WriteLog(ex);
	}
	/// <summary> Получение текущей карты </summary>
	/// <returns></returns>
	protected virtual MapDescription GetCurrentMapDescription()
	{
		return null;
	}
	/// <summary> Записывает адрес в выходной XML </summary>
	/// <param name="addr"></param>
	/// <param name="longitude"></param>
	/// <param name="latitude"></param>
	protected void SerializeAddress(float longitude, float latitude, Guid? mapGuid = null)
	{
		var frmt = new NumberFormatInfo { NumberDecimalSeparator = "." };
		var addr = IWebPS.GetAddressByPoint(lat: latitude, lng: longitude, CurrentLanguage, mapGuid);

		// address
		Writer.WriteStartElement("address");
		Writer.WriteAttributeString("longitude", longitude.ToString(frmt));
		Writer.WriteAttributeString("latitude",  latitude.ToString(frmt));

		Writer.WriteAttributeString("coordinates",
			new Latitude(Convert.ToDecimal(latitude)).ToString() + ", " +
			new Longitude(Convert.ToDecimal(longitude)).ToString());

		Writer.WriteString(addr);
		Writer.WriteEndElement(); // address
	}
	[PageAction("logout")]
	public void Logout()
	{
		LogOutInternal();
		RedirectToAboutPage();
	}
	public void LogOutInternal()
	{
		contextHelper.Logout();
	}
	protected List<SystemRight> GetOperatorSystemRights()
	{
		return contextHelper.GetOperatorSystemRights();
	}
	public Vehicles LoadVehicles(params int[] vehicleIds)
	{
		return contextHelper.LoadVehicles(vehicleIds);
	}
	/// <summary>Информация о временной зоне оператора: данные о переводе часов</summary>
	/// <remarks>Требуется для корректного отображения дат в отчетах</remarks>
	protected TimeZoneInfo TimeZoneInfo
	{
		get { return contextHelper.TimeZoneInfo; }
		set { contextHelper.TimeZoneInfo = value; }
	}
	protected void EndResponseAndReturnJson(object result)
	{
		SetJsonContentType();
		response.Write(JsonHelper.SerializeObjectToJson(result));
	}
	protected void WrapJsonAction<T>(Func<T> func)
	{
		SetJsonContentType();
		T result = default(T);
		try
		{
			result = func();
		}
		catch (Exception ex)
		{
			WriteLog(ex);
		}
		response.Write(JsonHelper.SerializeObjectToJson(result));
	}
	protected Vehicle GetVehicleById(int vehicleId, bool includeLastData = false)
	{
		return
			IWebPS.GetVehicles(new GetVehiclesArgs
			{
				VehicleIDs      = new[] { vehicleId },
				IncludeLastData = includeLastData
			}).FirstOrDefault();
	}
	protected GeoZone GetGeoZoneDTO(int zoneId, bool includeVertices = true, bool includeIgnored = false)
	{
		return IWebPS.GetGeoZones(includeVertices, zoneId, includeIgnored, true).FirstOrDefault();
	}
	protected Dictionary<int, GeoZone> GetGeoZonesDTO(bool includeVertices = true, bool includeIgnored = false)
	{
		return IWebPS.GetGeoZones(includeVertices, includeIgnored: includeIgnored).ToDictionary(z => z.Id);
	}
	/// <summary> Возвращает список геозон или точки конкретной геозоны </summary>
	/// <param name="mapGuid"></param>
	/// <param name="getVertexes"></param>
	/// <param name="zoneId"></param>
	/// <returns></returns>
	protected DataSet GetGeozonesFromServer(bool getVertexes, int zoneId)
	{
		if (!IsAuthorized)
			return null;

		DataSet zones = IWebPS.GetZonesWebForOperator(getVertexes, zoneId);

		return zones;
	}
	protected Group[] GetGeoZoneGroupsDTO(bool includeIgnored = false)
	{
		return IWebPS.GetGeoZoneGroups(new GetGroupsArguments { IncludeIgnored = includeIgnored });
	}
	protected bool IsUserGuest
	{
		get
		{
			return contextHelper.IsUserGuest;
		}
	}
	/// <summary> Сериализация текущего оператора </summary>
	private void RenderOperatorInfo()
	{
		if (!IsAuthorized || !IsUserLogged || IsUserGuest)
			return;

		// AS: Нарисуем текущего юзера
		try
		{
			var oper = CurrentOperator;
			oper.WriteXml(Writer);
		}
		catch (Exception ex)
		{
			WriteLog(ex);
			AddMessage(new Message(Severity.Error, GetMessageData("error"), ex.Message));
		}
	}
	protected Person CurrentOperator
	{
		get { return contextHelper.CurrentOperator; }
	}
	protected void UpdateCurrentOperator()
	{
		contextHelper.UpdateCurrentOperator();
	}
	protected string UrlPrefix
	{
		get { return Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port != 80 ? ":" + Request.Url.Port : string.Empty); }
	}
	protected void WriteFuelTypes(XmlTextWriter xmlTextWriter)
	{
		xmlTextWriter.WriteStartElement("FuelTypes");
		foreach (var fuelType in IWebPS.GetFuelTypes())
		{
			fuelType.WriteXml(xmlTextWriter);
		}
		xmlTextWriter.WriteEndElement();
	}
	protected void WriteCurrency(XmlTextWriter xmlTextWriter)
	{
		xmlTextWriter.WriteElementString("Currency", IWebPS.GetCurrency());
	}
	protected void AccessIsDenied()
	{
		Response.Redirect(Request.ApplicationPath + "/error/403.aspx");
	}
	protected bool TryParseDateTime(string s, out DateTime result)
	{
		return TimeHelper.TryParseDateTime(s, out result);
	}
	protected void TranslateConfig(string key, string defaultValue = null)
	{
		var configValue = ConfigurationManager.AppSettings[key];
		if (configValue == null)
		{
			if (defaultValue != null)
				AddProperty(key, defaultValue);
			return;
		}
		AddProperty(key, configValue);
	}
	protected bool TrySelectDepartment(int departmentId)
	{
		return contextHelper.TrySelectDepartment(IWebPS, departmentId);
	}
	protected void RedirectToAboutPageIfNeeded()
	{
		if (string.Equals(ConfigurationManager.AppSettings["TransferResponseToPromoWhenUnauthenticated"],
				"true", StringComparison.OrdinalIgnoreCase))
			RedirectToAboutPage();
	}
	private static readonly string JpegContentType = "image/jpeg";
	private ContextHelper contextHelperValue = null;
	private ContextHelper contextHelper
	{
		get
		{
			return contextHelperValue ?? (contextHelperValue = new ContextHelper(Context, GetType()));
		}
	}
	protected CultureInfo CurrentCulture
	{
		get { return contextHelper.CurrentCulture; }
	}
	public bool IsMobile
	{
		get { return contextHelper.IsMobile; }
		set { contextHelper.IsMobile = value; }
	}
	public void RedirectResponse(string address, string reason = null)
	{
		contextHelper.RedirectResponse(address, reason);
	}
	public ResourceManager RM { get { return contextHelper.RM; } }
	public LoginResult TryLogIn(int operatorId)
	{
		return contextHelper.TryLogIn(operatorId);
	}
	public LoginResult TryLogIn(int operatorId, string reason, int? parentSessionId, int? initDepartmentId)
	{
		return contextHelper.TryLogIn(operatorId, reason, parentSessionId, initDepartmentId);
	}
	public void ForceChangePassword()
	{
		contextHelper.ForceChangePassword();
	}
	public List<SystemRight> OperatorSystemRights
	{
		get { return contextHelper.OperatorSystemRights; }
	}
	public bool isImageOk(Stream imgStream)
	{
		bool isok = false;
		//isok = true;
		try
		{
			Bitmap bmp = new Bitmap(imgStream);
			if (bmp.Width <= 400 && bmp.Height <= 400)
			{
				isok = true;
			}
		}
		catch (Exception) { }

		return isok;
	}
	protected int? DepartmentId
	{
		get { return contextHelper.DepartmentId; }
		set { contextHelper.DepartmentId = value; }
	}
	protected bool ForcedToChangePassword
	{
		get { return contextHelper.ForcedToChangePassword; }
		set { contextHelper.ForcedToChangePassword = value; }
	}
	protected bool ForcedToSetEmail
	{
		get { return contextHelper.ForcedToSetEmail; }
		set { contextHelper.ForcedToSetEmail = value; }
	}
	protected bool ForcedToSetPhone
	{
		get { return contextHelper.ForcedToSetPhone; }
		set { contextHelper.ForcedToSetPhone = value; }
	}
	protected void UpdateSettingsRestrictions()
	{
		contextHelper.UpdateSettingsRestrictions();
	}
	protected void UnForceChangePassword()
	{
		contextHelper.UnForceChangePassword();
	}
	protected string ClientVersion
	{
		get { return contextHelper.ClientVersion; }
		set { contextHelper.ClientVersion = value; }
	}
	protected void SetCultureByLanguage(string language)
	{
		contextHelper.SetCultureByLanguage(language);
	}
	private void AddTitleProperty()
	{
		AddProperty("title", ConfigurationManager.AppSettings["ApplicationName-" + CurrentCulture.TwoLetterISOLanguageName]);
	}
	protected string GetBase64StringForUrl(string webPath)
	{
		if (string.IsNullOrWhiteSpace(webPath))
			return null;

		var localPath = Request.MapPath(webPath);
		if (!File.Exists(localPath))
			return null;
		var bytes = File.ReadAllBytes(localPath);
		return Convert.ToBase64String(bytes);
	}
	public IEnumerable<HttpPostedFile> GetRequestFiles()
	{
		for (var i = 0; i < Request.Files.Count; i++)
			yield return Request.Files.Get(i);
	}
	public CommonActionResponse<T> InvokeBilling<P, T>(Func<ChannelFactory<P>, T> body)
	{
		var response = new CommonActionResponse<T> { Object = default(T) };
		using (var factory = new ChannelFactory<P>("*"))
		{
			try
			{
				response.Object = body(factory);
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				var exceptionDetail = (ex as FaultException<ExceptionDetail>)
					?.Detail ?? new ExceptionDetail(ex);
				if (exceptionDetail.Type == typeof(AggregateException).FullName)
					exceptionDetail = exceptionDetail.InnerException;

				string.Concat(
					!string.IsNullOrWhiteSpace(LoggedUser)               ? $"User '{LoggedUser}'({OperatorId})"       : string.Empty,
					!string.IsNullOrWhiteSpace(IWebPS?.Department?.name) ? $" logged to '{IWebPS?.Department?.name}'" : string.Empty,
					$"\nERROR:\n{exceptionDetail?.Type}('{exceptionDetail?.Message?.Trim()}')",
					$"\nSTACK:\n{exceptionDetail?.StackTrace?.Trim()}")
					.TraceError();

				response.Object = default(T);
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = exceptionDetail.Message;
				factory.Abort();
			}
		}
		return response;
	}
	public bool CurrentBillingAccountExistsAndCreateIfNeeded()
	{
		var result = false;
		try
		{
			// Если нет авторизации, то выходим
			if (!IsAuthorized)
				return result;
			// Если логин не является номером телефона, то выходим
			var phone = LoggedUser;
			if (string.IsNullOrWhiteSpace(phone) || !ContactHelper.IsValidPhone(ContactHelper.GetNormalizedPhone(phone) ?? string.Empty))
				return result;
			// Если нет подтвержденной почты, то выходим
			var email = IWebPS
				?.GetOperatorEmails(IWebPS.OperatorId)
				?.FirstOrDefault(c => c.IsConfirmed.HasValue && c.IsConfirmed.Value)
				?.Value;
			if (string.IsNullOrWhiteSpace(email))
				return result;
			// Если тип клиента/департамента не является физическим лицом, то выходим
			var accountType = IWebPS?.Department?.Type == DepartmentType.Corporate
				? AccountType.Corporate
				: AccountType.Private;
			if (AccountType.Private != accountType)
				return result;
			// Пытаемся создать или получить готового пользователя биллинга
			result = InvokeBilling<IPaymentProvider, bool>((factory) =>
			{
				var userExists = factory
					.CreateChannel()
					.UserExists(email);

				if (userExists)
					return userExists;

				userExists = factory
					.CreateChannel()
					.CreateUser(email, System.Web.Security.Membership.GeneratePassword(10, 2), phone, accountType);

				if (userExists)
					$@"Billing account '{email}' created with phone '{phone}'."
						.TraceInformation();

				return userExists;
			})
				// Ошибки специально не обрабатываем глушим, чтобы не мешать выполнятся методам дальше
				?.Object ?? false;
		}
		catch (Exception ex)
		{
			var usrMess = !string.IsNullOrWhiteSpace(LoggedUser)               ? $"User '{LoggedUser}'({OperatorId})"       : string.Empty;
			var depMess = !string.IsNullOrWhiteSpace(IWebPS?.Department?.name) ? $" logged to '{IWebPS?.Department?.name}'" : string.Empty;
			string.Concat(usrMess, depMess)
				.WithException(ex)
				.CallTraceError();
		}
		return result;
	}
}