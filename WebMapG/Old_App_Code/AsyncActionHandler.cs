﻿using System;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	/// <summary> Асинхронный обработчик действий </summary>
	public class AsyncActionHandler : IHttpAsyncHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			throw new NotImplementedException();
		}

		public bool IsReusable { get { return false; } }

		public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
		{
			var asynch = new AsynchOperation(cb, context, extraData);
			asynch.StartAsyncWork();
			return asynch;
		}

		public void EndProcessRequest(IAsyncResult result)
		{
		}

		private class AsynchOperation : IAsyncResult
		{
			private bool _completed;
			private readonly Object _state;
			private readonly AsyncCallback _callback;
			private readonly HttpContext _context;
			private readonly ContextHelper _contextHelper;
			private bool _completedSynchronously;
			private readonly int _vehicleId;
			private readonly CmdType _cmdType;
			private readonly object _completedLock = new object();
			private Timer _timer;

			bool IAsyncResult.IsCompleted { get { return _completed; } }
			WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
			Object IAsyncResult.AsyncState { get { return _state; } }
			bool IAsyncResult.CompletedSynchronously { get { return _completedSynchronously; } }

			public AsynchOperation(AsyncCallback callback, HttpContext context, Object state)
			{
				_callback = callback;
				_context = context;

				_state = state;
				_completed = false;

				_contextHelper = new ContextHelper(context);

				if (_contextHelper.PersonalWebServer == null)
				{
					_completedSynchronously = true;
					_contextHelper.Unauthorized();
					return;
				}

				if (_contextHelper.GetParamValue("a") != "SendInstantCommand")
				{
					BadRequest();
					return;
				}

				if (!int.TryParse(_contextHelper.GetParamValue("vehicleId"), out _vehicleId))
				{
					BadRequest();
					return;
				}

				if (!Enum.TryParse(_contextHelper.GetParamValue("type"), true, out _cmdType))
				{
					BadRequest();
				}
			}

			public void StartAsyncWork()
			{
				if (_completedSynchronously)
					return;

				try
				{
					_contextHelper.PersonalWebServer.SendInstantCommand(_cmdType, _vehicleId, ResultReady);
				}
				catch (Exception ex)
				{
					SysDiags::Trace.TraceError(GetType().Name + ": {0} error\nMessage:\n{1}\nStackTrace:\n{2}",
						nameof(StartAsyncWork),
						ex.Message,
						ex.StackTrace);
					_completedSynchronously = true;
					_contextHelper.Error();
					return;
				}

				_timer = new Timer(delegate { ResultReady(null); }, null, TimeSpan.FromSeconds(5), TimeSpan.FromMilliseconds(-1));
			}

			private void ResultReady(object obj)
			{
				if (_completed)
					return;

				lock (_completedLock)
				{
					if (_completed)
						return;
					_completed = true;

					if (_timer != null)
					{
						_timer.Dispose();
						_timer = null;
					}
				}

				try
				{
					var videoRecord = obj as VideoRecord;

					_contextHelper.SetJsonResponseContentType();
					_context.Response.Write(JsonHelper.SerializeObjectToJson(videoRecord));
					_callback(this);
				}
				catch (Exception ex)
				{
					SysDiags::Trace.TraceError(GetType().Name + ": {0} error\nMessage:\n{1}\nStackTrace:\n{2}",
						nameof(ResultReady),
						ex.Message,
						ex.StackTrace);
					_contextHelper.Error();
				}
			}

			private void BadRequest()
			{
				_completedSynchronously = true;
				_contextHelper.BadRequest();
			}
		}
	}
}