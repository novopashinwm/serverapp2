﻿using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Interfaces.Web;

/// <summary>
/// AS: Работа с маршрутами
/// </summary>
public class Route
{
    public Route()
    {
    }

    /// <summary>
    /// AS: Сериализация точек маршрута в XMLWriter
    /// </summary>
    /// <param name="points">Точки маршрута</param>
    /// <param name="scale">Масштаб карты</param>
    /// <param name="fk">Соотношение метров к масштабу</param>
    /// <param name="startP">Точка начала маршрута</param>
    /// <param name="endP">Точка конца маршрута</param>
    /// <param name="md">Карта, для которой строить маршрут. Для карты определен дорожный граф</param>
    /// /// <param name="writer">Writer, в который записывается инфа</param>
    public static void SerializeRoutePoints(
            Dictionary<int, List<PointF>> points,
            int scale,
            float fk,
            PointF startP,
            PointF endP,
            MapDescription md,
            XmlTextWriter writer
        )
    {
        writer.WriteStartElement("sections");
        System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
        format.NumberDecimalSeparator = ".";
        float minx = -1;
        float miny = -1;

        float maxx = -1;
        float maxy = -1;
        foreach (int key in points.Keys)
        {
            writer.WriteStartElement("points");
            foreach (PointF point in points[key])
            {
                //PointF WPoint = md.coordGeoToWindow(point.X, point.Y, fk, 0, 0, scale);

                writer.WriteStartElement("point");
                writer.WriteAttributeString("long", point.X.ToString(format));
                writer.WriteAttributeString("lat", point.Y.ToString(format));

                //writer.WriteAttributeString("WX", WPoint.X.ToString(format));
                //writer.WriteAttributeString("WY", WPoint.Y.ToString(format));

                writer.WriteEndElement();

                if (minx == -1 || point.X < minx) minx = point.X;
                if (miny == -1 || point.Y < miny) miny = point.Y;

                if (maxx == -1 || point.X > maxx) maxx = point.X;
                if (maxy == -1 || point.Y > maxy) maxy = point.Y;
            }
            writer.WriteEndElement(); // points
        }
        writer.WriteElementString("minLongitude", minx.ToString(format));
        writer.WriteElementString("minLatitude", miny.ToString(format));

        writer.WriteElementString("maxLongitude", maxx.ToString(format));
        writer.WriteElementString("maxLatitude", maxy.ToString(format));

        /*
        PointF minWPoint = md.coordGeoToWindow(minx, miny, fk, 0, 0, scale);
        PointF maxWPoint = md.coordGeoToWindow(maxx, maxy, fk, 0, 0, scale);

        writer.WriteElementString("minLongitudeW", minWPoint.X.ToString(format));
        writer.WriteElementString("minLatitudeW", minWPoint.Y.ToString(format));

        writer.WriteElementString("maxLongitudeW", maxWPoint.X.ToString(format));
        writer.WriteElementString("maxLatitudeW", maxWPoint.Y.ToString(format));

        PointF startWPoint = md.coordGeoToWindow(startP.X, startP.Y, fk, 0, 0, scale);
        PointF finishWPoint = md.coordGeoToWindow(endP.X, endP.Y, fk, 0, 0, scale);

        writer.WriteElementString("startRouteX", startWPoint.X.ToString(format));
        writer.WriteElementString("startRouteY", startWPoint.Y.ToString(format));

        writer.WriteElementString("finishRouteX", finishWPoint.X.ToString(format));
        writer.WriteElementString("finishRouteY", finishWPoint.Y.ToString(format));
        */
        writer.WriteEndElement(); //sections
    }
}
