﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.UI.WebMapG;

/// <summary>
/// Класс обрабатывает запросы к XML файлам
/// Например, он может генерить разметку для сложной графики: маршрутов, геозон и т.п.
/// </summary>
public class XmlHandler : HttpHandlerBase, IHttpHandler, IRequiresSessionState
{
	public new bool IsReusable
	{
		get { return false; }
	}
	[PageAction("default")]
	public void DefaultMethod()
	{
		TemplateFile = "geozone";
		AddProperty("template", "test");
		SetXhtmlContentType();
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}
	/// <summary> Получить все геозоны </summary>
	[PageAction("getallgeozones")]
	public void GetGeozones()
	{
		TemplateFile = "geozone";
		AddProperty("template", "displayZonesInList");

		ResponseSerializationBeg();
		try
		{
			// Поскольку надо отобразить сразу все геозоны, тащим здесь всю инфу по ним.
			// Впоследствии нужно переделать ХП, чтобы она возвращала всю инфу полностью по всем зонам

			DataSet zones = GetGeozonesFromServer(false, 0);
			zones.DataSetName = "zones";

			SetObjectToSession("geoZones", zones);

			Writer.WriteStartElement("zones");
			foreach (DataTable zone in zones.Tables)
			{
				foreach (DataRow zr in zone.Rows)
				{
					int zoneId;
					Int32.TryParse(zr["ZONE_ID"].ToString(), out zoneId);

					Writer.WriteStartElement("Zone");

					Writer.WriteElementString("ZONE_ID", zoneId.ToString());
					Writer.WriteElementString("NAME", zr["NAME"].ToString());
					Writer.WriteElementString("ZONE_TYPE_ID", zr["ZONE_TYPE_ID"].ToString());
					Writer.WriteEndElement(); // Zone
				}
			}
			Writer.WriteEndElement(); // zones
		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, myEx.Message, ""));
			WriteLog(myEx);
		}
		ResponseSerializationEnd();
	}
	/// <summary> Получение всей информации о геозонах, включая точки </summary>
	[JsonPageAction("getallgeozonesinfo", HttpMethod.Unspecified)]
	public Dictionary<int, GeoZone> GetAllGeozonesInfo()
	{
		var result = default(Dictionary<int, GeoZone>);
		try
		{
			result = GetGeoZonesDTO();
		}
		catch (Exception ex)
		{
			WriteLog(ex);
		}
		return result;
	}
	/// <summary> Сериализовать информацию о геозоне </summary>
	/// <param name="zone"></param>
	private void SerializeGeoZoneInfo(DataSet zone, int zoneId, float fKWinToMap, int corrx, int corry, int scale)
	{
		if (zone.Tables.Count < 2 || zone.Tables[1].Rows.Count == 0)
		{
			return;
		}

		System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
		format.NumberDecimalSeparator = ".";

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ",";

		float minx = -1;
		float miny = -1;

		float maxx = -1;
		float maxy = -1;

		float maxr = -1;

		Writer.WriteStartElement("Zone");
		/*Writer.WriteElementString("ZONE_ID", zoneId.ToString());
		Writer.WriteElementString("NAME", zr["NAME"].ToString());
		Writer.WriteElementString("ZONE_TYPE_ID", zr["ZONE_TYPE_ID"].ToString());*/
		DataRow[] zoneRows = zone.Tables[0].Select("ZONE_ID = " + zoneId.ToString());
		int zoneTypeId = 0;
		int color = 0;
		foreach (DataRow zrow in zoneRows)
		{
			Writer.WriteElementString("ZONE_ID", zoneId.ToString());
			Writer.WriteElementString("NAME", zrow["NAME"].ToString());
			Writer.WriteElementString("DESCRIPTION", zrow["DESCRIPTION"].ToString());
			Int32.TryParse(zrow["ZONE_TYPE_ID"].ToString(), out zoneTypeId);
			Writer.WriteElementString("ZONE_TYPE_ID", zoneTypeId.ToString());

			Int32.TryParse(zrow["COLOR"].ToString(), out color);
			Color c = Color.FromArgb(color);

			StringBuilder colorText = new StringBuilder("rgb(");
			colorText.Append(c.R.ToString());
			colorText.Append(",");
			colorText.Append(c.G.ToString());
			colorText.Append(",");
			colorText.Append(c.B.ToString());
			colorText.Append(")");
			Writer.WriteElementString("COLOR", colorText.ToString());

			Writer.WriteElementString("COLOR_RRGGBB", ColorTranslator.ToHtml(c));
		}
		//PointF prevPoint;
		//foreach (DataRow row in zone.Tables[1].Rows)
		DataRow row = zone.Tables[1].Rows[0];
		float x;
		float y;
		float r;
		float.TryParse(row["X"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
		float.TryParse(row["Y"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);
		float.TryParse(row["RADIUS"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out r);
		/*
		PointF pt = this.GetCurrentMapDescription().coordGeoToWindow(x, y, fKWinToMap, corrx, corry, scale);

		if (minx == -1 || x < minx) minx = x;
		if (miny == -1 || y < miny) miny = y;

		if (maxx == -1 || x > maxx) maxx = x;
		if (maxy == -1 || y > maxy) maxy = y;

		if (maxr == -1 || r > maxr) maxr = r;
		*/
		Writer.WriteStartElement("ZonePoint");

		Writer.WriteElementString("X", x.ToString(format));
		Writer.WriteElementString("Y", y.ToString(format));
		Writer.WriteElementString("RADIUS", r.ToString(format));

		r = (float)Math.Round(r / fKWinToMap);

		Writer.WriteElementString("WR", (2 * r).ToString(format));

		/*
		Writer.WriteElementString("WX", pt.X.ToString(format));
		Writer.WriteElementString("WY", pt.Y.ToString(format));

		Writer.WriteElementString("WX1", "-1");
		Writer.WriteElementString("WY1", "-1");
		Writer.WriteElementString("WX2", "-1");
		Writer.WriteElementString("WY2", "-1");
		Writer.WriteElementString("WX3", "-1");
		Writer.WriteElementString("WY3", "-1");
		Writer.WriteElementString("WX4", "-1");
		Writer.WriteElementString("WY4", "-1");
		*/
		Writer.WriteEndElement(); // ZonePoint
		for (int i = 1; i < zone.Tables[1].Rows.Count; i++)
		{
			float r2 = r;
			//PointF point1 = new PointF(pt.X, pt.Y);

			row = zone.Tables[1].Rows[i];

			float.TryParse(row["X"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
			float.TryParse(row["Y"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);
			float.TryParse(row["RADIUS"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out r);

			Writer.WriteStartElement("ZonePoint");

			if (minx == -1 || x < minx) minx = x;
			if (miny == -1 || y < miny) miny = y;

			if (maxx == -1 || x > maxx) maxx = x;
			if (maxy == -1 || y > maxy) maxy = y;

			if (maxr == -1 || r > maxr) maxr = r;

			Writer.WriteElementString("X", x.ToString(format));
			Writer.WriteElementString("Y", y.ToString(format));
			Writer.WriteElementString("RADIUS", r.ToString(format));

			//pt = this.GetCurrentMapDescription().coordGeoToWindow(x, y, fKWinToMap, corrx, corry, scale);
			r = (float)Math.Round(r / fKWinToMap);

			/*
			if (zoneTypeId == (int)ZoneTypes.corridor)
			{
				// AS: Сдесь добавляем экстра логику точек - это коридор, епть

				PointF point2 = new PointF(pt.X, pt.Y);
				double l =
					Math.Sqrt(
						(point1.X - point2.X) * (point1.X - point2.X) +
						(point1.Y - point2.Y) * (point1.Y - point2.Y)
					);
				if (l > Math.Abs(r2 - r))
				{
					double cos = (point2.X - point1.X) / l;
					double sin = (point1.Y - point2.Y) / l;

					double s = Math.Sign(r2 - r);
					double BC =
						Math.Sqrt(l * l - (r2 - r) * (r2 - r));
					double CE = r2 * BC / l;
					double DE = Math.Sqrt(Math.Abs(r * r - CE * CE));
					double x1 = s * DE * cos + CE * sin + point2.X;
					double y1 = -s * DE * sin + CE * cos + point2.Y;
					double x2 = s * DE * cos - CE * sin + point2.X;
					double y2 = -s * DE * sin - CE * cos + point2.Y;
					CE = r2 * BC / l;
					DE = Math.Sqrt(Math.Abs(r2 * r2 - CE * CE));
					double x3 = s * DE * cos - CE * sin + point1.X;
					double y3 = -s * DE * sin - CE * cos + point1.Y;
					double x4 = s * DE * cos + CE * sin + point1.X;
					double y4 = -s * DE * sin + CE * cos + point1.Y;

					String wx1 = Math.Round(x1).ToString(format);
					String wy1 = Math.Round(y1).ToString(format);
					String wx2 = Math.Round(x2).ToString(format);
					String wy2 = Math.Round(y2).ToString(format);
					String wx3 = Math.Round(x3).ToString(format);
					String wy3 = Math.Round(y3).ToString(format);
					String wx4 = Math.Round(x4).ToString(format);
					String wy4 = Math.Round(y4).ToString(format);

					if (wx1.Length > 0)
					{

						Writer.WriteElementString("WX1", wx1);
						Writer.WriteElementString("WY1", wy1);

						Writer.WriteElementString("WX2", wx2);
						Writer.WriteElementString("WY2", wy2);

						Writer.WriteElementString("WX3", wx3);
						Writer.WriteElementString("WY3", wy3);

						Writer.WriteElementString("WX4", wx4);
						Writer.WriteElementString("WY4", wy4);
					}
				}
				else
				{
					Writer.WriteElementString("WX1", "-1");
					Writer.WriteElementString("WY1", "-1");
					Writer.WriteElementString("WX2", "-1");
					Writer.WriteElementString("WY2", "-1");
					Writer.WriteElementString("WX3", "-1");
					Writer.WriteElementString("WY3", "-1");
					Writer.WriteElementString("WX4", "-1");
					Writer.WriteElementString("WY4", "-1");
				}
			}
			Writer.WriteElementString("WX", pt.X.ToString(format));
			Writer.WriteElementString("WY", pt.Y.ToString(format));
			Writer.WriteElementString("WR", (2 * r).ToString(format));
			*/
			Writer.WriteEndElement(); // ZonePoint
		}
		Writer.WriteElementString("minLongitude", minx.ToString(format));
		Writer.WriteElementString("minLatitude", miny.ToString(format));

		Writer.WriteElementString("maxLongitude", maxx.ToString(format));
		Writer.WriteElementString("maxLatitude", maxy.ToString(format));

		Writer.WriteElementString("maxRadius", maxr.ToString(format));

		//PointF minWPoint = this.GetCurrentMapDescription().coordGeoToWindow(minx, miny, fKWinToMap, corrx, corry, scale);
		//PointF maxWPoint = this.GetCurrentMapDescription().coordGeoToWindow(maxx, maxy, fKWinToMap, corrx, corry, scale);
		maxr = (float)Math.Round(maxr / fKWinToMap);

		//Writer.WriteElementString("minLongitudeW", minWPoint.X.ToString(format));
		//Writer.WriteElementString("minLatitudeW", minWPoint.Y.ToString(format));

		//Writer.WriteElementString("maxLongitudeW", maxWPoint.X.ToString(format));
		//Writer.WriteElementString("maxLatitudeW", maxWPoint.Y.ToString(format));

		Writer.WriteElementString("maxRadiusW", maxr.ToString(format));

		PointF centerGeozonePoint = new PointF((minx + maxx) / 2, (miny + maxy) / 2);
		Writer.WriteElementString("centerX", centerGeozonePoint.X.ToString(format));
		Writer.WriteElementString("centerY", centerGeozonePoint.Y.ToString(format));

		Writer.WriteEndElement(); // Zone
	}
}