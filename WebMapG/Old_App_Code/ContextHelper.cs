﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Infrastructure.Authentication;
using FORIS.TSS.UI.WebMapG.Old_App_Code;
using FORIS.TSS.UI.WebMapG.services;
using Interfaces.Web;
using RU.NVG.UI.WebMapG.Model.Common;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	public class ContextHelper
	{
		public const string          SessionCookieName = "ssid";
		private readonly HttpContext _context;
		private readonly Type        _pageType;
		private IWebPersonalServer   _webPS;
		public ContextHelper(HttpContext context, Type pageType = null)
		{
			_context  = context;
			_pageType = pageType;
		}
		public IWebPersonalServer PersonalWebServer
		{
			get
			{
				if (!(_webPS?.IsAlive() ?? false))
				{
					using (new Stopwatcher(GetType().Name + ": PersonalWebServer.CreateSession " + SessionId, null, StopwatchLoggingThreshold))
					{
						_webPS = GetPersonalServer();
						// Завершаем, если департамент заблокирован, а пользователь не имеет права управлять услугами
						if (null != _webPS && _webPS.IsLocked() && !_webPS.GetOperatorSystemRights(_webPS.OperatorId).Contains(SystemRight.ServiceManagement))
							Logout();
						// Завершаем, если сессия помечена как остановленная/прекращенная/закрытая, но закрытия еще не произошло
						if (null != _webPS && _webPS.SessionInfo.StopOn < DateTime.UtcNow)
							Logout();
						// Если не завершено обновляем текущие параметры
						if (null != _webPS)
						{
							var userAgent = GetObjectFromSession(UserAgentKey) as string;
							if (!string.Equals(userAgent, _context.Request.UserAgent))
							{
								userAgent = _context.Request.UserAgent;
								SetObjectToSession(UserAgentKey, userAgent);
								_webPS.LogUserAgent(userAgent);
							}
							var lastActivity = GetObjectFromSession(SessionLastActivityKey) as DateTime?;
							if (lastActivity == null || TimeSpan.FromMinutes(1) < DateTime.UtcNow - lastActivity.Value)
							{
								SetObjectToSession(SessionLastActivityKey, DateTime.UtcNow);
							}
							if (!SessionSwitched && _webPS.UpdateCulture(CurrentCulture.Name))
							{
								SysDiags::Trace.TraceInformation(GetType().Name + ": Culture update requested by user {0} to {1}, raw url {2}",
									_webPS.OperatorId, CurrentCulture, _context.Request.RawUrl);
							}
						}
					}
				}
				return _webPS;
			}
		}
		public bool TrySelectAppId(IWebPersonalServer server, string appId)
		{
			var result = true;
			try
			{
				server.SelectAppId(appId);
				AppId = appId;
			}
			catch (SecurityException)
			{
				AppId = !string.IsNullOrEmpty(server.AppId) ? server.AppId : null;
				result = false;
			}

			return result;
		}
		public bool TrySelectDepartment(IWebPersonalServer server, int departmentId)
		{
			try
			{
				server.SelectDepartment(departmentId);
				DepartmentId = departmentId;
				return true;
			}
			catch (SecurityException)
			{
				DepartmentId = server.Department != null ? server.Department.id : (int?)null;
				return false;
			}
		}
		/// <summary> Идентификатор текущего департамента по данным веб-сессии </summary>
		public int? DepartmentId
		{
			get { return GetObjectFromSession(nameof(DepartmentId)) as int?; }
			set { SetObjectToSession(nameof(DepartmentId), value); }
		}
		private string _sessionId;
		public string SessionId
		{
			get
			{
				if (null == _sessionId)
					_sessionId = GetObjectFromSession(SessionCookieName) as string;
				return _sessionId ?? string.Empty;
			}
			set
			{
				SetObjectToSession(SessionCookieName, _sessionId = value);
			}
		}
		public int? OperatorId
		{
			get { return GetObjectFromSession(nameof(OperatorId)) as int?; }
			set { SetObjectToSession(nameof(OperatorId), value); }
		}
		public string AppId
		{
			get { return GetObjectFromSession(nameof(AppId)) as string; }
			set { SetObjectToSession(nameof(AppId), !string.IsNullOrWhiteSpace(value) ? value : null); }
		}
		/// <summary> Идентификатор оператора родительской сессии </summary>
		private int? InitOperatorId
		{
			get { return GetObjectFromSession(nameof(InitOperatorId)) as int?; }
			set { SetObjectToSession(nameof(InitOperatorId), value); }
		}
		private string Reason
		{
			get { return GetObjectFromSession(nameof(Reason)) as string; }
			set { SetObjectToSession(nameof(Reason), !string.IsNullOrWhiteSpace(value) ? value : null); }
		}
		private int? InitDepartmentId
		{
			get { return GetObjectFromSession(nameof(InitDepartmentId)) as int?; }
			set { SetObjectToSession(nameof(InitDepartmentId), value); }
		}
		public bool SessionSwitched
		{
			get { return InitOperatorId.HasValue; }
		}
		public ISessionManager SessionManager
		{
			get
			{
				return GetObjectFromApplication(Global.SessionManagerApplicationKey) as ISessionManager;
			}
		}
		public IWebLoginProvider WebLoginProvider
		{
			get
			{
				return GetObjectFromApplication(Global.SessionManagerApplicationKey) as IWebLoginProvider;
			}
		}
		/// <summary> Все общие ссылки приложения </summary>
		public ConcurrentDictionary<Guid, ShareLink> ApplicationShareLinks
		{
			get
			{
				return GetObjectFromApplication(Global.ShareLinksApplicationKey) as ConcurrentDictionary<Guid, ShareLink>;
			}
		}
		/// <summary> Общая ссылка конкретной сессии </summary>
		public ShareLink SessionShareLink
		{
			get { return GetObjectFromSession(nameof(SessionShareLink)) as ShareLink; }
			set { SetObjectToSession(nameof(SessionShareLink), value); }
		}
		public LoginResult TryLogIn(int operatorId)
		{
			return TryLogIn(operatorId, Reason, InitOperatorId, InitDepartmentId);
		}
		public LoginResult TryLogIn(int operatorId, string reason, int? initOperatorId, int? initDepartmentId)
		{
			try
			{
				var sessionGuid  = GenerateNewSessionUid();
				InitOperatorId   = initOperatorId;
				Reason           = reason;
				InitDepartmentId = initDepartmentId;
				CreateSession(sessionGuid, operatorId);

				if (PersonalWebServer == null)
					return LoginResult.Error;

				// Вход выполнен от имени супер администратора под пользователем с заблокированным клиентом
				if (initOperatorId.HasValue && PersonalWebServer.OperatorId == initOperatorId.Value)
					return LoginResult.AccessLocked;

				UpdateSettingsRestrictions();

				// Часовой пояс по-умолчанию - для мобильной версии - локальный, т.к. пользователи мобильной версии в основном локальные
				// TODO: сохранять часовой пояс для оператора в БД в настройках оператора
				// TODO: определять на основании данных браузера, местоположения абонента etc
				TimeZoneInfo timeZoneInfo = null;
				try
				{
					var paramValue = GetParamValue("dst");
					timeZoneInfo   = TimeZoneHelper.ParseDstString(paramValue);
				}
				catch (Exception ex)
				{
					WriteLog(ex);
				}

				if (timeZoneInfo == null)
					timeZoneInfo = PersonalWebServer.TimeZoneInfo;
				else
				{
					var equalTimeZoneInfo = TimeZoneHelper.GetEqualTimeZoneInfos(timeZoneInfo).FirstOrDefault();
					if (equalTimeZoneInfo != null)
						timeZoneInfo = equalTimeZoneInfo;
				}

				if (timeZoneInfo != null)
					TimeZoneInfo = timeZoneInfo;

				if (timeZoneInfo != null && !IsUserGuest && !SessionSwitched)
					PersonalWebServer.UpdateTimeZoneInfo(timeZoneInfo, false);

				try
				{
					var culture = CurrentCulture;
					if (culture != null && !SessionSwitched)
					{
						if (PersonalWebServer.UpdateCulture(culture.Name))
						{
							SysDiags::Trace.TraceInformation(GetType().Name + ": TryLogin culture update requested by user {0} to {1}, raw url {2}",
								PersonalWebServer.OperatorId, culture, _context.Request.RawUrl);
						}
					}
				}
				catch (Exception ex)
				{
					SysDiags::Trace.TraceError(GetType().Name + ": {0}", ex);
				}
			}
			catch (InvalidOperationException ex)
			{
				_context.Response.Cookies.Add(new HttpCookie(SessionCookieName) { Expires = DateTime.Now.AddDays(-1) });
				WriteLog(ex);
				return LoginResult.SessionAlreadyExists;
			}
			catch (SessionExistException ex)
			{
				_context.Response.Cookies.Add(new HttpCookie(SessionCookieName) { Expires = DateTime.Now.AddDays(-1) });
				SysDiags::Trace.TraceError(GetType().Name + ": {0}", ex);
				return LoginResult.SessionAlreadyExists;
			}
			catch (Exception ex)
			{
				// AS: Этот exception выбрасывается в случае существования сессии (других случаев не обнаружил)
				// при попытке показать окно с сообщением (видимо о том, что сессия существует).
				// Выбрасывается исключение "окно нельзя показать без запущенного приложения"
				_context.Response.Cookies.Add(new HttpCookie(SessionCookieName) { Expires = DateTime.Now.AddDays(-1) });
				SysDiags::Trace.TraceError(GetType().Name + ": {0}", ex);
				_context.Session.Clear();

				return LoginResult.Error;
			}

			if (PersonalWebServer != null)
			{
				//узнаем доступные машины
				var vehicles = default(Vehicles);
				using (new Stopwatcher($"{GetType().Name}: TryLogIn::LoadVehicles", null, StopwatchLoggingThreshold))
					vehicles = LoadVehicles();
				SetObjectToSession("Allow_Vehicles", vehicles);

				// Узнаем имя оператора
				SetObjectToSession("OperatorName", PersonalWebServer.SessionInfo.OperatorInfo.Name);

				var rights = default(List<SystemRight>);
				using (new Stopwatcher($"{GetType().Name}: TryLogIn::GetOperatorSystemRights", null, StopwatchLoggingThreshold))
					OperatorSystemRights = rights = GetOperatorSystemRights();

				// Определяем показывать ли страницу "Администрирование"
				var showAdminPage = null != rights &&
				(
					rights.Contains(SystemRight.SecurityAdministration) ||
					rights.Contains(SystemRight.ServerAdministration)
				);
				SetObjectToSession("ShowAdminPage", showAdminPage);
				SetObjectToSession(SessionCookieName, SessionId);
				UpdateCurrentOperator();

				var sessionCookie = _context.Response.Cookies.Get(SessionCookieName);
				if (null != sessionCookie)
				{
					sessionCookie.Value   = SessionId;
					sessionCookie.Expires = DateTime.UtcNow.AddDays(3);
				}
			}
			else
			{
				var sb = new StringBuilder(OperatorId.ToString());
				sb.Append(Environment.NewLine);
				sb.Append(GetMessageData("CannotConnectToServer"));
				WriteLog(sb.ToString());
				return LoginResult.CannotConnectToServer;
			}

			return LoginResult.Success;
		}
		private string GetVersion()
		{
			return ClientVersion ?? (AssemblyVersion  + ":" + _context.Request.ApplicationPath);
		}
		public string ClientVersion
		{
			get { return GetObjectFromSession(nameof(ClientVersion)) as string; }
			set { SetObjectToSession(nameof(ClientVersion), value); }
		}
		#region Object from|to session
		/// <summary> Получить объект из сессии по ключу </summary>
		/// <param name="key"> Ключ </param>
		/// <returns></returns>
		public object GetObjectFromSession(string key)
		{
			var session = _context?.Session;
			if (session == null)
				return null;
			try
			{
				return session[key];
			}
			catch (Exception ex)
			{
				SysDiags::Trace.TraceError(GetType().Name + ": Error when getting Session value by key {0}: {1}", key, ex);
			}
			return null;
		}
		/// <summary> Записать объект в сессию по ключу и удалить если значения нет </summary>
		/// <param name="key"> Ключ </param>
		/// <param name="val"> Объект </param>
		public void SetObjectToSession(string key, object val)
		{
			var session = _context?.Session;
			if (session == null)
				return;
			if (val == null)
				session.Remove(key);
			else
				session[key] = val;
		}
		#endregion Object from|to session

		#region Object from|to application
		/// <summary> Получить объект из приложения по ключу </summary>
		/// <param name="key"> Ключ </param>
		/// <returns></returns>
		public object GetObjectFromApplication(string key)
		{
			var application = _context?.Application;
			if (null == application)
				return null;
			try
			{
				return application[key];
			}
			catch (Exception ex)
			{
				SysDiags::Trace.TraceError(GetType().Name + ": Error when getting Application value by key {0}: {1}", key, ex);
			}
			return null;
		}
		/// <summary> Записать объект в приложение по ключу </summary>
		/// <param name="key"> Ключ </param>
		/// <param name="val"> Объект </param>
		public void SetObjectToApplication(string key, object val)
		{
			var application = _context?.Application;
			if (application == null)
				return;
			if (val == null)
				application.Remove(key);
			else
				application[key] = val;
		}
		#endregion Object from|to application
		private static readonly string AssemblyVersion;

		static ContextHelper()
		{
			AssemblyVersion = typeof(ContextHelper).Assembly.GetName().Version.ToString();
		}

		public static readonly TimeSpan StopwatchLoggingThreshold = TimeSpan.FromSeconds(1);
		public string LoggedUser
		{
			get
			{
				var server = PersonalWebServer;
				if (server == null)
					return null;
				return server.SessionInfo?.OperatorInfo?.Login;
			}
		}
		protected CultureInfo currentCulture;
		public CultureInfo CurrentCulture
		{
			get
			{
				// Язык уже определен, возвращаем
				if (null != currentCulture)
					return currentCulture;

				// Поиск языка, в HTTP заголовке "Preferred-Language"
				var preferredLanguage = _context.Request.Headers["Preferred-Language"];
				if (!string.IsNullOrWhiteSpace(preferredLanguage))
					return currentCulture = GetCultureByLanguage(preferredLanguage);

				// Поиск языка, в HTTP Cookie "language"
				var languageFromCookie = GetLanguageFromCookie();
				if (languageFromCookie != null)
					return currentCulture = GetCultureByLanguage(languageFromCookie);

				// Поиск языка, в HTTP заголовке "Accept-Language"
				var languageFromAcceptLanguageHeader = GetLanguageFromAcceptLanguageHeader();
				return currentCulture = GetCultureByLanguage(languageFromAcceptLanguageHeader);
			}

			set { throw new NotSupportedException(); }
		}
		private string GetLanguageFromAcceptLanguageHeader()
		{
			if (_context.Request.UserLanguages != null && _context.Request.UserLanguages.Length > 0)
				return _context.Request.UserLanguages[0].Substring(0, 2);
			return "en";
		}
		private string GetLanguageFromCookie()
		{
			var languageCookie = default(HttpCookie);
			try
			{
				languageCookie = _context.Request.Cookies["language"];
			}
			catch (Exception)
			{
			}
			if (null == languageCookie || null == languageCookie.Value)
				return null;
			return languageCookie.Value.ToLower();
		}
		private CultureInfo GetCultureByLanguage(string language)
		{
			switch (language)
			{
				case "en":
					return new CultureInfo("en-US");
				case "ru":
					return new CultureInfo("ru-RU");
				default:
					return new CultureInfo("en-US");
			}
		}

		private const           string SessionIdGuidFormatString = "N";
		private static readonly object SessionGuidGeneratorLock  = new object();

		public static Guid GenerateNewSessionUid()
		{
			Guid sessionGuid;
			//в методе генерации GUID есть ошибка, из-за чего могут возникать одни и те же Guid (см. http://www.gotdotnet.ru/blogs/denish/1965/)
			//TODO: убрать блокировку, генерировать sessionID не как Guid, а гарантировать уникальность сессии как-то по-другому
			lock (SessionGuidGeneratorLock)
				sessionGuid = Guid.NewGuid();
			return sessionGuid;
		}
		protected void CreateSession(Guid sessionId, int operatorId)
		{
			SessionId  = sessionId.ToString(SessionIdGuidFormatString);
			OperatorId = operatorId;
		}
		private IWebPersonalServer GetPersonalServer()
		{
			return GetPersonalServer(Reason, InitOperatorId, InitDepartmentId);
		}
		private IWebPersonalServer GetPersonalServer(string reason, int? initOperatorId, int? initDepartmentId)
		{
			// Читаем идентификатор текущей сессии (наша) в сессии запроса (IIS)
			var sessionIdString = SessionId;
			// Если нашей сессии нет, то пытаемся выполнить логин оператором
			if (string.IsNullOrWhiteSpace(sessionIdString))
			{
				if (OperatorId == null)
					return null;
				// Выполняем логин оператором, в случае успеха, получаем отсутствовавший SessionId
				TryLogIn(OperatorId.Value, reason, initOperatorId, initDepartmentId);
				sessionIdString = SessionId;
			}

			var sessionId = Guid.Parse(sessionIdString);
			var server = GetPersonalServer(sessionId, reason, initOperatorId, initDepartmentId);
			return server;
		}
		protected IWebPersonalServer GetPersonalServer(Guid sessionId, string reason, int? initOperatorId, int? initDepartmentId)
		{
			var operatorId = OperatorId;
			if (operatorId == null)
				return null;

			var server = GetPersonalServer(operatorId.Value, sessionId, reason, initOperatorId, initDepartmentId);

			return server;
		}
		protected IWebPersonalServer GetPersonalServer(int operatorId, Guid sessionId, string reason, int? initOperatorId, int? initDepartmentId)
		{
			IPAddress ipAddress;
			if (_context.Request.UserHostAddress == null ||
				!IPAddress.TryParse(_context.Request.UserHostAddress, out ipAddress))
			{
				SysDiags::Trace.TraceWarning(GetType().Name + ": Unable to retrieve client IP from {0}", _context.Request.UserHostAddress);
				ipAddress = IPAddress.None;
			}

			IWebPersonalServer server = null;
			var sessionManager = SessionManager;
			if (null != sessionManager)
			{
				try
				{
					Reason           = reason;
					InitOperatorId   = initOperatorId;
					InitDepartmentId = initDepartmentId;
					server = sessionManager.CreateSession(
						operatorId,
						sessionId,
						GetVersion(),
						ipAddress) as IWebPersonalServer;
				}
				catch (SecurityException ex)
				{
					SysDiags::Trace.TraceError(GetType().Name + ": GetPersonalServer error {0}\n{1}", ex.Message, ex.StackTrace);
				}
			}
			if (null == server)
			{
				OperatorId       = null;
				AppId            = null;
				Reason           = null;
				InitOperatorId   = null;
				InitDepartmentId = null;
				return null;
			}

			if (!string.IsNullOrEmpty(AppId))
			{
				if (string.IsNullOrEmpty(server.AppId) || server.AppId != AppId)
					TrySelectAppId(server, AppId);
			}
			else
			{
				if (!string.IsNullOrEmpty(server.AppId))
					AppId = server.AppId;
			}

			if (null != DepartmentId)
			{
				if (null == server.Department || server.Department.id != DepartmentId.Value)
					TrySelectDepartment(server, DepartmentId.Value);
			}
			else
			{
				if (null != server.Department)
					DepartmentId = server.Department.id;
			}
			return server;
		}
		/// <summary> Установка культуры по умолчанию по языку </summary>
		/// <param name="language"> Язык (ru, en) </param>
		public void SetCultureByLanguage(string language)
		{
			switch (language)
			{
				case "ru":
					currentCulture = null;
					currentCulture = new CultureInfo("ru-RU");
					break;
				case "en":
					currentCulture = null;
					currentCulture = new CultureInfo("en-US");
					break;
				default:
					currentCulture = null;
					currentCulture = new CultureInfo("en-US");
					break;
			}
		}
		public void ForceChangePassword()
		{
			ForcedToChangePassword = true;

			RedirectToSettingsPageIfNeeded();
		}
		public void RedirectToSettingsPageIfNeeded()
		{
			if (_pageType == null)
				return;

			if (typeof(Login).IsAssignableFrom(_pageType) ||
				typeof(ServicesSettings).IsAssignableFrom(_pageType) ||
				_context.Request.IsAjaxRequest())
				return;

			if (ForcedToChangePassword)
			{
				RedirectResponse("settings.aspx", "Change password");
			}

			if (ForcedToSetEmail)
			{
				RedirectResponse("settings.aspx", "Change email");
			}

			if (ForcedToSetPhone)
			{
				RedirectResponse("settings.aspx", "Set phone");
			}
		}
		public void UnForceChangePassword()
		{
			ForcedToChangePassword = false;
		}
		public void UpdateSettingsRestrictions()
		{
			var operatorDetails = PersonalWebServer.GetOperatorDetails(PersonalWebServer.OperatorId);

			ForcedToSetEmail = (operatorDetails.mandatoryEmail ?? false) && !operatorDetails.Emails.Any(e => e.IsConfirmed ?? false);
			ForcedToSetPhone = (operatorDetails.mandatoryPhone ?? false) && !operatorDetails.Phones.Any(p => p.IsConfirmed ?? false);
		}
		public bool ForcedToChangePassword
		{
			get { return GetObjectFromSession(nameof(ForcedToChangePassword)) as bool? ?? false; }
			set { SetObjectToSession(nameof(ForcedToChangePassword), value); }
		}
		public bool ForcedToSetEmail
		{
			get { return GetObjectFromSession(nameof(ForcedToSetEmail)) as bool? ?? false; }
			set { SetObjectToSession(nameof(ForcedToSetEmail), value); }
		}
		public bool ForcedToSetPhone
		{
			get { return GetObjectFromSession(nameof(ForcedToSetPhone)) as bool? ?? false; }
			set { SetObjectToSession(nameof(ForcedToSetPhone), value); }
		}
		public void RedirectResponse(string localUrl, string reason)
		{
			SysDiags::Trace.TraceInformation(GetType().Name + ": Redirect from {0} to {1} because of {2}", _context.Request.RawUrl, localUrl, reason);

			if (localUrl.Contains("http://"))
				_context.Response.Redirect(localUrl);
			var url =
				(_context.Request.ApplicationPath.Length == 1
					 ? "/"
					 : _context.Request.ApplicationPath + "/");

			if (IsMobile)
			{
				url += "mobile/";
			}
			url = url + localUrl;
			_context.Response.Redirect(url, true);
		}
		/// <summary> Получение значение параметра, получаемого из запроса </summary>
		/// <param name="paramKey"> Ключ параметра </param>
		/// <param name="defaultValue"> Возвращаемое значение по-умолчанию, если параметр отсутствует в http-запросе </param>
		/// <returns>Значение параметра</returns>
		public virtual string GetParamValue(string paramKey, string defaultValue = "")
		{
			string val;
			var httpRequest = _context.Request;
			var server = _context.Server;

			switch (httpRequest.HttpMethod.ToLower())
			{
				case "get":
					val = httpRequest.QueryString[paramKey] != null
						? server.HtmlDecode(httpRequest.QueryString[paramKey])
						: defaultValue;
					break;
				case "post":
					val = httpRequest.Form[paramKey] != null
						? server.HtmlDecode(httpRequest.Form[paramKey])
						: defaultValue;
					break;
				default:
					val = httpRequest.Params[paramKey] != null
						? server.HtmlDecode(httpRequest.Params[paramKey])
						: defaultValue;
					break;
			}

			return val;
		}
		public bool IsMobile
		{
			get
			{
				try
				{
					return
						_context.Request.Url.OriginalString.Contains("/mobile") ||
						_context.Request.QueryString["ismobile"] != null        ||
						bool.Parse(GetObjectFromSession(nameof(IsMobile))
							?.ToString() ?? bool.FalseString);
				}
				catch (Exception)
				{
					return false;
				}
			}
			set
			{
				SetObjectToSession(nameof(IsMobile), value);
			}
		}
		/// <summary>Информация о временной зоне оператора: данные о переводе часов</summary>
		/// <remarks>Требуется для корректного отображения дат в отчетах</remarks>
		public TimeZoneInfo TimeZoneInfo
		{
			get
			{
				var timeZoneFromSession = GetObjectFromSession(nameof(this.TimeZoneInfo)) as TimeZoneInfo;
				if (timeZoneFromSession != null)
					return timeZoneFromSession;
				timeZoneFromSession = PersonalWebServer.TimeZoneInfo;
				TimeZoneInfo = timeZoneFromSession;
				return timeZoneFromSession;
			}
			set { SetObjectToSession(nameof(this.TimeZoneInfo), value); }
		}
		public bool IsUserGuest
		{
			get { return PersonalWebServer == null || PersonalWebServer.IsGuest; }
		}
		public void WriteLog(string msg)
		{
			try
			{
				SysDiags::Trace.WriteLine(msg);
			}
			catch (Exception)
			{
			}
		}
		public void WriteLog(Exception ex)
		{
			var sbMsg = new StringBuilder(_context.Request.RawUrl);
			sbMsg.AppendLine();
			while (ex != null)
			{
				sbMsg.AppendLine(ex.Message);
				sbMsg.AppendLine(ex.Source);
				sbMsg.AppendLine(ex.StackTrace);
				ex = ex.InnerException;
			}
			SysDiags::Trace.TraceError(sbMsg.ToString());
		}
		public Vehicles LoadVehicles(params int[] vehicleIds)
		{
			var vehicles = new Vehicles();


			if (PersonalWebServer == null)
				return vehicles;

			DataSet ds = PersonalWebServer.GetDataForWeb(vehicleIds);

			var vehicleKindColumn = ds.Tables["ALLOW_VEHICLE"].Columns["VEHICLE_KIND_ID"];
			var vidToVehicle = new Dictionary<int, Vehicles.VehiclesInfo>();
			foreach (DataRow row in ds.Tables["ALLOW_VEHICLE"].Rows)
			{
				int vId = Convert.ToInt32(row["VEHICLE_ID"]);
				var vi = new Vehicles.VehiclesInfo(vId, row["GARAGE_NUMBER"].ToString().Trim(), true)
				{
					RegNumber = row["public_number"] as string,
					Type = row["vehicle_type"] as string,
					FuelTankVolume = row["fuel_tank"] != DBNull.Value ? Convert.ToDecimal(row["fuel_tank"]) : 0,
					ControllerPhoneNumber = row["phone"].ToString(),
					Capabilities = new List<DeviceCapability>(),
					VehicleKind = (VehicleKind)(int)row[vehicleKindColumn]
				};
				vidToVehicle.Add(vId, vi);
				vehicles.Add(vi);
			}

			foreach (DataRow r in ds.Tables["VEHICLE_PROFILE"].Rows)
			{
				var vid = (int)r["vehicle_id"];
				string pname = r["property_name"].ToString();
				vidToVehicle[vid].AddPropertyToCollection(pname, r["value"] as string);
			}

			var vehicleCapabilityTable = ds.Tables["VEHICLE_CAPABILITY"];
			foreach (DataRow capabilityRow in vehicleCapabilityTable.Rows)
			{
				var vId = (int)capabilityRow["vehicle_id"];
				var vi = vidToVehicle[vId];
				for (var i = 1; i != vehicleCapabilityTable.Columns.Count; ++i)
				{
					var column = vehicleCapabilityTable.Columns[i];
					if (Convert.ToBoolean(capabilityRow[column]))
						vi.Capabilities.Add((DeviceCapability)Enum.Parse(typeof(DeviceCapability), column.ColumnName));
				}
			}

			return vehicles;
		}
		public List<SystemRight> GetOperatorSystemRights()
		{
			return null != OperatorId
				? PersonalWebServer?.GetOperatorSystemRights(OperatorId.Value)
				: null;
		}
		public List<SystemRight> OperatorSystemRights
		{
			get { return (List<SystemRight>)GetObjectFromSession(nameof(OperatorSystemRights)); }
			set { SetObjectToSession(nameof(OperatorSystemRights), value); }
		}
		public List<SystemRight> DepartmentRights
		{
			get { return PersonalWebServer.GetDepartmentRights(); }
		}
		public Person CurrentOperator
		{
			get
			{
				var currentOperator = GetObjectFromSession(nameof(CurrentOperator));
				if (null == currentOperator)
					UpdateCurrentOperator();
				return GetObjectFromSession(nameof(CurrentOperator)) as Person;
			}
		}
		public void UpdateCurrentOperator()
		{
			SetObjectToSession(nameof(CurrentOperator), new Person(PersonalWebServer.OperatorId, PersonalWebServer));
		}
		/// <summary> Возврат значения по ключу из ресурса </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public string GetMessageData(string key)
		{
			return GetMessageData(key, CurrentCulture);
		}
		public string GetMessageData(string key, CultureInfo cultInfo)
		{
			return RM.GetString(key, cultInfo);
		}
		public string GetLocalMessage(string key)
		{
			return GetLocaleMessage(key, CurrentCulture);
		}
		public string GetLocaleMessage(string key, CultureInfo cultInfo)
		{
			return LocaleRM.GetString(key, cultInfo);
		}
		public string GetMessageData<T>(string key)
			where T : class
		{
			return GetMessageData<T>(key, CurrentCulture);
		}
		public string GetMessageData<T>(string key, CultureInfo cultureInfo)
		{
			return GetResourceManager<T>()?.GetString(key, cultureInfo);
		}
		private readonly IDictionary<Type, ResourceManager> _typeResourceManagers = new ConcurrentDictionary<Type, ResourceManager>();
		private ResourceManager _resourceManager;
		private ResourceManager _localeResourceManager;
		private const string    SessionLastActivityKey = "SessionLastActivity";
		private const string    UserAgentKey           = "UserAgent";
		public ResourceManager RM
		{
			get
			{
				if (_resourceManager == null)
				{
					_resourceManager = new ResourceManager("Resources.Messages", Assembly.Load("App_GlobalResources"));
				}
				return _resourceManager;
			}
		}
		public ResourceManager LocaleRM
		{
			get
			{
				if (_localeResourceManager == null)
				{
					_localeResourceManager = new ResourceManager("Resources.LocaleMessage", Assembly.Load("App_GlobalResources"));
				}
				return _localeResourceManager;
			}
		}
		public ResourceManager GetResourceManager<T>()
		{
			var type = typeof(T);
			ResourceManager rm;
			if (!_typeResourceManagers.TryGetValue(type, out rm))
			{
				var resourceName = string.Format(CultureInfo.InvariantCulture, "Resources.{0}", type.Name);
				rm = new ResourceManager(resourceName, Assembly.Load("App_GlobalResources"));
				_typeResourceManagers.Add(type, rm);
			}
			return rm;
		}
		public void SendHttpResult(int statusCode, string status)
		{
			_context.Response.Clear();
			_context.Response.StatusCode        = statusCode;
			_context.Response.StatusDescription = status;
			_context.Response.End();
		}
		public void Logout()
		{
			var initOperatorId   = InitOperatorId;
			var initDepartmentId = InitDepartmentId;
			// Очищаем нашу серверную сессию;
			RemoveSession();
			// Очищаем текущий контекст
			_context.Session.Clear();
			FormsAuthentication.SignOut();
			if (_context.Request.Cookies[SessionCookieName] != null)
				_context.Response.Cookies.Add(new HttpCookie(SessionCookieName) { Expires = DateTime.Now.AddDays(-2) });

			if (initOperatorId.HasValue)
				TryLogIn(initOperatorId.Value);
			if (initDepartmentId.HasValue)
				TrySelectDepartment(PersonalWebServer, initDepartmentId.Value);
		}
		protected void RemoveSession()
		{
			var server = _webPS;
			_webPS = null;
			if (server != null && server.IsAlive())
			{
				server.CleanupBeforeLogout();
				server.Close();
			}

			if (string.IsNullOrEmpty(SessionId))
				return;
			SessionId = "";
		}
		public void Unauthorized()
		{
			SendHttpResult(401, "Unauthorized");
		}
		/// <summary> Завершает обработку вызова отправкой кода 403 </summary>
		public void Forbidden(string reason = null)
		{
			SendHttpResult(403, reason ?? "Forbidden");
		}
		public void NotFound(string reason = null)
		{
			SendHttpResult(404, reason ?? "Not Found");
		}
		public void BadRequest(string description = null)
		{
			SendHttpResult(400, description ?? "Bad request");
		}
		public void Error()
		{
			SendHttpResult(500, "Server error");
		}
		public void SetJsonResponseContentType()
		{
			_context.Response.ContentType = "application/json";
			_context.Response.ContentEncoding = Encoding.UTF8;
		}
	}
}