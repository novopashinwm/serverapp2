﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using FORIS.TSS.BusinessLogic.DTO;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	/// <summary> Асинхронный прокси для трансляции трафика от IP-камер пользователям </summary>
	public class MjpegAsyncHttpHandler : IHttpAsyncHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			throw new NotImplementedException();
		}
		public bool IsReusable { get { return false; } }
		public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
		{
			var asynch = new AsynchOperation(cb, context, extraData);
			asynch.StartAsyncWork();
			return asynch;
		}
		public void EndProcessRequest(IAsyncResult result)
		{
		}
		private class AsynchOperation : IAsyncResult
		{
			private bool _completed;
			private readonly Object _state;
			private readonly AsyncCallback _callback;
			private readonly HttpContext _context;
			private HttpWebRequest _inputRequest;
			private Stream _inputResponseStream;
			private byte[] _buffer;
			private readonly ContextHelper _contextHelper;
			private bool _completedSynchronously;
			private readonly Uri _cameraUrl;

			private byte[] Buffer
			{
				get { return _buffer ?? (_buffer = new byte[4000]); }
			}

			bool IAsyncResult.IsCompleted { get { return _completed; } }
			WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
			Object IAsyncResult.AsyncState { get { return _state; } }
			bool IAsyncResult.CompletedSynchronously { get { return _completedSynchronously; } }
			public AsynchOperation(AsyncCallback callback, HttpContext context, Object state)
			{
				SysDiags::Trace.TraceInformation("MJPEG requested: {0}", context.Request.Url);

				_callback = callback;
				_context = context;

				_state = state;
				_completed = false;

				_contextHelper = new ContextHelper(context, GetType());

				if (_contextHelper.PersonalWebServer == null)
				{
					_completedSynchronously = true;
					_contextHelper.Unauthorized();
					return;
				}

				var objectIdString = _contextHelper.GetParamValue("object_id");
				int id;
				if (!int.TryParse(objectIdString, out id))
				{
					_completedSynchronously = true;
					_contextHelper.BadRequest("Unable to parse object_id");
					return;
				}

				var vehicle = _contextHelper.PersonalWebServer
					?.GetVehicles(new GetVehiclesArgs { VehicleIDs = new[] { id } })
					?.FirstOrDefault();

				var ip = vehicle?.ipAddress;
				if (ip == null)
				{
					_completedSynchronously = true;
					_contextHelper.NotFound("Camera IP not found");
					return;
				}

				_cameraUrl = new Uri(string.Format("http://{0}/mjpg/video.mjpg", ip));

				context.Response.Clear();
				context.Response.ClearContent();
				context.Response.ClearHeaders();
			}
			public void StartAsyncWork()
			{
				_inputRequest = (HttpWebRequest)WebRequest.CreateDefault(_cameraUrl);

				_inputRequest.Timeout = 5000;
				_inputRequest.Method = "GET";
				_inputRequest.KeepAlive = true;
				_inputRequest.BeginGetResponse(GetResponseCallback, this);
			}
			private void Close()
			{
				try
				{
					_inputResponseStream.Close();
				}
				catch (Exception e)
				{
					SysDiags::Trace.TraceError("{0}", e);
				}

				try
				{
					_context.Response.OutputStream.Close();
				}
				catch (Exception e)
				{
					SysDiags::Trace.TraceError("{0}", e);
				}

				_completed = true;
				_callback(this);
			}
			private void Continue()
			{
				_inputResponseStream.BeginRead(Buffer, 0, Buffer.Length, ReadCallback, this);
			}
			private void ReadCallback(IAsyncResult ar)
			{
				try
				{
					var bytesRead = _inputResponseStream.EndRead(ar);
					if (bytesRead == 0)
					{
						Close();
						return;
					}

					_context.Response.OutputStream.BeginWrite(Buffer, 0, bytesRead, WriteCallback, this);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					Close();
				}
			}
			private void GetResponseCallback(IAsyncResult result)
			{
				try
				{
					var inputResponse = _inputRequest.EndGetResponse(result);
					_inputResponseStream = inputResponse.GetResponseStream();
					if (_inputResponseStream == null)
					{
						Close();
						return;
					}

					foreach (string key in inputResponse.Headers.Keys)
					{
						_context.Response.Headers[key] = inputResponse.Headers[key];

						if (key == "Content-Type")
							_context.Response.ContentType = inputResponse.Headers[key];
					}

					Continue();
				}
				catch (Exception e)
				{
					SysDiags::Trace.TraceError("{0}", e);
					Close();
				}
			}
			private void WriteCallback(IAsyncResult ar)
			{
				try
				{
					_context.Response.OutputStream.EndWrite(ar);
					_context.Response.OutputStream.Flush();
					_context.Response.Flush();
					Continue();
				}
				catch (HttpException e)
				{
					if ((uint)e.ErrorCode != 0x800704CD) //The remote host closed the connection.
						SysDiags::Trace.TraceError("{0}", e);
					Close();
				}
				catch (Exception e)
				{
					SysDiags::Trace.TraceError("{0}", e);
					Close();
				}
			}
		}
	}
}