﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Resources;
using FORIS.TSS.UI.WebMapG;
using Interfaces.Web;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;
using Controller = FORIS.TSS.UI.WebMapG.Controller;
using Group = FORIS.TSS.BusinessLogic.DTO.Group;
using VehicleControlDate = FORIS.TSS.BusinessLogic.DTO.VehicleControlDate;

/// <summary> Summary description for VehiclesInfo </summary>
[Serializable()]
public partial class Vehicles
{
	[Serializable]
	[System.Xml.Serialization.XmlRoot("vehicle")]
	public partial class VehiclesInfo : BaseSerializable
	{
		/// <summary> Идентификатор ТС </summary>
		public int ID;
		public string departmentCountryName = "";
		/// <summary> Гаражный номер ТС </summary>
		public string GarageNumber;
		/// <summary> Название ТС </summary>
		public string Name;
		/// <summary> Государственный номер </summary>
		protected string regNumber;
		public string RegNumber
		{
			get { return regNumber; }
			set { regNumber = value; }
		}
		protected decimal fuelTankVolume;
		/// <summary> Размер топливного бака </summary>
		public virtual decimal FuelTankVolume
		{
			get { return fuelTankVolume; }
			set { fuelTankVolume = value; }
		}
		/// <summary> Если трекер куплен в салоне </summary>
		public bool? CanBeDisactivated {
			get; set;
		}
		public void IncludeVehicleControlDates(IWebPersonalServer wps)
		{
			if (_vehicleControlDates != null) return;
			_vehicleControlDates = wps.GetVehicleControlDates(ID);
		}
		private List<VehicleControlDate> _vehicleControlDates = null;
		public List<VehicleControlDate> VehicleControlDates
		{
			get
			{
				return _vehicleControlDates;
			}
			set
			{
				_vehicleControlDates = value;
			}
		}
		protected decimal fuelSpentStandartKilometer, fuelSpentStandartLiter;
		/// <summary> Норма расхода топлива в сотых литра на 100км (или можно считать как л./10 000км.) </summary>
		public decimal FuelSpentStandart
		{
			get
			{
				if ((FuelUnit ?? UnitOfMeasure.LitrePer100Km) == UnitOfMeasure.LitrePer100Km)
					return this.fuelSpentStandartLiter;
				else
					return this.fuelSpentStandartKilometer;
			}
		}
		/// <summary> Норма расхода топлива в литрах на моточас </summary>
		public decimal FuelSpentStandardMH
		{
			get; set;
		}
		public int? FuelTypeID { get; set; }
		public UnitOfMeasure? FuelUnit { get; set; }
		public UnitOfMeasure? TankUnit { get; set; }
		protected string controllerPhoneNumber;
		/// <summary> Номер телефона контроллера </summary>
		public string ControllerPhoneNumber
		{
			get { return controllerPhoneNumber; }
			set { controllerPhoneNumber = value; }
		}
		/// <summary> Местоположение </summary>
		public IWebMobileInfoCP PositionInfo;
		/// <summary> Свойства объекта мониторинга </summary>
		protected Dictionary<string, string> settings;
		/// <summary> Свойства объекта мониторинга </summary>
		public Dictionary<string, string> Settings
		{
			get { return settings; }
			set { settings = value; }
		}
		protected bool isOld;
		/// <summary> Устанавливает признак устаревания инфы об объекте </summary>
		public bool IsOld
		{
			get { return isOld; }
			set { isOld = value; }
		}
		/// <summary>
		/// Признак получаемости
		/// </summary>
		public bool Get;
		/// <summary>
		/// Картинка
		/// </summary>
		public ImageRepresentation Image;
		/// <summary>
		/// Метка
		/// </summary>
		public string Label;
		/// <summary>
		/// Идентификатор
		/// </summary>
		public string TextID;
		/// <summary>
		/// Скорость
		/// </summary>
		public string currSpeed;
		/// <summary>
		/// Зажигание
		/// </summary>
		public string ignition;
		/// <summary>
		/// Статус
		/// </summary>
		public ImageRepresentation status;
		/// <summary>
		/// Статус
		/// </summary>
		public string ctrlNumber;
		protected List<Controller> controllers;
		protected string currentAddress;
		protected byte[] vehicleImage;
		protected string type;
		protected List<Person> owners;
		public List<Controller> Controllers
		{
			get { return controllers; }
		}
		public byte[] VehicleImage
		{
			get { return vehicleImage; }
		}
		public string Type
		{
			get { return type; }
			set { type = value; }
		}
		public List<Person> Owners
		{
			get { return owners; }
		}
		public string CurrentAddress
		{
			get { return currentAddress; }
			set { currentAddress = value; }
		}
		/// <summary>
		/// AS: Дата и время. Нахуй эту хуйню форматировать в коде, блять.
		/// Выводиться дата должна в таком ниипическом формате:
		/// YYYY-MM-DDThh:mm:ssTZD
		/// Если кому-то интересно, можно посмотреть вот это: http://www.w3.org/TR/NOTE-datetime
		/// Там вся эта хуета описана
		/// </summary>
		protected DateTime? myDateTime2;
		// AS: вдруг эту еботу нужно будет перегрузить, например, запретить устанавливать значение
		public virtual DateTime? MyDateTime2
		{
			// AS: а почему собственно нет?
			set { myDateTime2 = value; }
			get { return myDateTime2; }
		}
		/// <summary>
		/// AS: Дата и время, отформатированные для вывода
		/// </summary>
		protected string mydateTime;
		public string myDateTime
		{
			get { return mydateTime; }
			set { throw new NotSupportedException(); }
		}
		protected int x;
		protected int y;
		// Корректные позиции
		protected int xCp;
		protected int yCp;
		private Group ZoneGroup { get; set; }
		public List<DeviceCapability> Capabilities { get; set; }
		public VehicleKind VehicleKind
		{
			get; set;
		}
		/// <summary> Оконная X-координата </summary>
		public int X
		{
			get { return x; }
			set { x = value; }
		}
		/// <summary> Оконная Y-координата </summary>
		public int Y
		{
			get { return y; }
			set { y = value; }
		}
		public int XCp
		{
			get { return xCp; }
			set { xCp = value; }
		}
		public int YCp
		{
			get { return yCp; }
			set { yCp = value; }
		}
		public int? MaxAllowedSpeed { get; set; }
		public List<Command> Commands { get; set; }
		public List<Sensor> Sensors { get; set; }
		/// <summary> Права текущего пользователя на этот объект наблюдения </summary>
		public List<SystemRight> Rights { get; set; }
		public VehiclesInfo(int ID, string GarageNumber, bool Get)
		{
			this.ID = ID;
			this.GarageNumber = GarageNumber;
			this.Get = Get;
		}
		/// <summary> Конструктор без параметров - нужен для сериализации </summary>
		public VehiclesInfo() : base() { }
		/// <summary> Загружает данные из БД </summary>
		/// <param name="id"></param>
		/// <param name="wps"></param>
		public VehiclesInfo(int id, IWebPersonalServer wps)
		{
			var vehicle = wps.GetVehicles(new GetVehiclesArgs
			{
				FilterByDepartment = false,
				VehicleIDs         = new[] { id },
			})
				.FirstOrDefault();
			if (vehicle == null)
				return;

			DataSet vehicleInfoSet = wps.GetDataFromDB(
				new ArrayList { new FORIS.TSS.BusinessLogic.Data.ParamValue("@vehicle_id", id) },
				"dbo.getVehicleProfile",
				new [] { "pic", "2", "3", "4", "profile"}
				);

			ZoneGroup = wps.GetVehicleZoneGroup(id);

			if (vehicleInfoSet != null)
			{
				ParseDataSet(id, vehicleInfoSet);
			}
			if (Controllers != null && Controllers.Any())
			{
				Capabilities = vehicle.capabilities.Where(v => v.Value.Allowed).Select(v => v.Key).ToList();
				Sensors = vehicle.Sensors;
				var recources = ResourceContainers.Get(wps.CultureInfo);
				foreach (var controller in Controllers)
				{
					var controllerType = controller.Type;
					if (controllerType != null)
					{
						controllerType.UserFriendlyName = recources.GetLocalizedText<ControllerType>(controllerType.Name);
					}
				}
			}
			Editable = vehicle.rights.Contains(SystemRight.EditVehicles);
			Rights = vehicle.rights.ToList();
			IsFriendVehicle = vehicle.isFriendVehicle;
			Name = vehicle.Name;
		}
		public bool Editable { get; set; }
		public bool IsFriendVehicle { get; set; }
		protected void ParseDataSet(int id, DataSet objectInfo)
		{
			this.ID = id;
			if (objectInfo.Tables.Count != 5) return;

			// AS: картинка
			if (objectInfo.Tables[0].Rows.Count > 0)
			{
				vehicleImage = (byte[])objectInfo.Tables[0].Rows[0][0];
			}

			// AS: инфа об объекте
			var vehicleDataRow = objectInfo.Tables[1].Rows[0];

			GarageNumber = vehicleDataRow["GARAGE_NUMBER"].ToString();
			RegNumber = vehicleDataRow["PUBLIC_NUMBER"].ToString();
			Type = vehicleDataRow["VEHICLE_TYPE"].ToString();
			//FuelSpentStandart = (int)objectInfo.Tables[1].Rows[0]["FUEL_SPENT_STANDART"];
			this.fuelSpentStandartKilometer = vehicleDataRow.Field<decimal?>("FuelSpendStandardKilometer").HasValue ?
				vehicleDataRow.Field<decimal>("FuelSpendStandardKilometer") : 0;
			this.fuelSpentStandartLiter = vehicleDataRow.Field<decimal?>("FuelSpendStandardLiter").HasValue ?
				vehicleDataRow.Field<decimal>("FuelSpendStandardLiter") : 0;

			var unitId = vehicleDataRow["Fuel_Unit_Id"] as short?;
			var tankUnitId = vehicleDataRow["Fuel_Tank_Unit_Id"] as short?;
			FuelUnit = (UnitOfMeasure?)unitId ?? UnitOfMeasure.LitrePer100Km;
			TankUnit = (UnitOfMeasure?)tankUnitId ?? UnitOfMeasure.Litre;
			var fuelSpentStandartMh = (decimal) vehicleDataRow["FUEL_SPENT_STANDARD_MH"];
			FuelSpentStandardMH = MeasureUnitHelper.ConvertTo(fuelSpentStandartMh, UnitOfMeasure.Litre, TankUnit.Value);
			FuelTypeID = vehicleDataRow["Fuel_Type_ID"] as int?;
			VehicleKind = (VehicleKind) (int) vehicleDataRow["VEHICLE_KIND_ID"];
			FuelTankVolume = vehicleDataRow.IsNull("Fuel_Tank")
								 ? 0m
								 : Convert.ToDecimal(vehicleDataRow["Fuel_Tank"]);
			MaxAllowedSpeed = vehicleDataRow["MaxAllowedSpeed"] as int?;


			this.departmentCountryName = objectInfo.Tables["2"].Rows[0]["departmentCountryName"].ToString();
			// AS: Инфа о владельце
			if (objectInfo.Tables[2].Rows.Count > 0)
			{
				var owner = new Person
				{
					FirstName = objectInfo.Tables[2].Rows[0]["FIRST_NAME"].ToString(),
					SecondName = objectInfo.Tables[2].Rows[0]["SECOND_NAME"].ToString(),
					LastName = objectInfo.Tables[2].Rows[0]["LAST_NAME"].ToString(),

				};

				Contact[] phones = new List<Contact>()
				{
					new Contact { Type=ContactType.Phone, Value = objectInfo.Tables["3"].Rows[0]["PHONE_NUMBER1"].ToString() },
					new Contact { Type=ContactType.Phone, Value = objectInfo.Tables["3"].Rows[0]["PHONE_NUMBER2"].ToString() },
					new Contact { Type=ContactType.Phone, Value = objectInfo.Tables["3"].Rows[0]["PHONE_NUMBER3"].ToString() }
				}.ToArray();
				owner.SetPhones(phones);
				owners = new List<Person>(1) {owner};
			}


			// AS: инфа о контроллере
			if (objectInfo.Tables[3].Rows.Count > 0)
			{
				var controllerRow = objectInfo.Tables[3].Rows[0];
				var ctl = new Controller();
				controllers = new List<Controller>(1);
				Controllers.Add(ctl);
				ctl.Phone = controllerRow["PHONE"] as string ?? string.Empty;
				ctl.PhoneIsEditable =  controllerRow["PhoneIsEditable"] as bool? ?? false;
				int num;
				int.TryParse(controllerRow["NUMBER"].ToString(), out num);
				ctl.Number = num;
				ctl.DeviceId = controllerRow["DeviceID"].ToString();
				ctl.Type = new ControllerType
					{
						Id = (int)controllerRow["CONTROLLER_TYPE_ID"],
						Name = (string)controllerRow["TYPE_NAME"],
						HasInputs = (bool)controllerRow["HasInputs"],
						SupportsPassword = (bool)controllerRow["SupportsPassword"],
						DeviceIdIsRequiredForSetup = (bool)controllerRow["DeviceIdIsRequiredForSetup"],
					};

				ctl.Imsi = controllerRow["IMSI"].ToString();
				ctl.Password = controllerRow["PASSWORD"].ToString();
				ctl.IP = controllerRow["IP"] as string;
			}

			// AS: 'профиль' объекта
			if (objectInfo.Tables.Contains("profile"))
			{
				LoadSettings(objectInfo.Tables["profile"]);
			}

		}
		protected void LoadSettings(DataTable settingsTab)
		{
			Settings = new Dictionary<string, string>();
			foreach (DataRow row in settingsTab.Rows)
			{
				var propertyName = (string)row["PROPERTY_NAME"];
				var propertyValue = row["VALUE"] as string;

				Settings.Add(propertyName, propertyValue);
			}
		}
		/// <summary> Добавляет свойство в коллекцию без сохранения в БД </summary>
		/// <param name="propName"></param>
		/// <param name="propValue"></param>
		public virtual void AddPropertyToCollection(string propName, string propValue)
		{
			if (Settings == null)
			{
				Settings = new Dictionary<string, string>();
			}

			if (propValue == null)
			{
				Settings.Remove(propName);
			}
			// AS: эту мудацкую проверку потом убрать. Тащить из базы только уникальные значения
			if (!Settings.ContainsKey(propName))
			{
				Settings.Add(propName, propValue);
			}
			else
			{
				Settings[propName] = propValue;
			}
		}
		public void CreateImageForPPC(ref Bitmap bm, System.Web.HttpContext context)
		{
			//GDI.POINT winPoint = new GDI.POINT();
			//mtWindow.coordGeoToWindow(this.PositionInfo.Longitude, this.PositionInfo.Latitude, out winPoint);
			/*int vehicleX = winPoint.X - 12;
			int vehicleY = winPoint.Y - 12;*/

			/*Graphics gr = Graphics.FromImage(bm);
			if (bm.Size.Width >= vehicleX && bm.Size.Height >= vehicleY)
			{
				int angle = (int)Math.Round((double)this.PositionInfo.Course / 10, 0);
				ArrayList arrayCourse = (ArrayList)context.Application["arrayCourse"];
				Bitmap arrow = (Bitmap)arrayCourse[angle];
				gr.DrawImageUnscaled(arrow, vehicleX, vehicleY, 12, 12);
			}*/
		}
		/// <summary> Получение адреса для объекта мониторинга </summary>
		/// <param name="mapGuid"></param>
		/// <param name="iwebps"></param>
		/// <param name="language"> Язык отображения адреса </param>
		public virtual void UpdateVehicle(Guid mapGuid, IWebPersonalServer iwebps, string language)
		{
			if (PositionInfo == null)
				return;
			// AS: И текущий адрес
			CurrentAddress = iwebps.GetAddressByPoint(this.PositionInfo.Latitude, this.PositionInfo.Longitude, language, mapGuid);
		}
		public virtual void UpdateVehicle(TimeZoneInfo timeZoneInfo)
		{
			if (PositionInfo == null)
				return;

			var localDateTimeText = TimeZoneInfo.ConvertTimeFromUtc(
				TimeHelper.GetDateTimeUTC(PositionInfo.Time),
				timeZoneInfo).ToString();
			this.TextID = localDateTimeText;
			mydateTime = localDateTimeText;

			MyDateTime2 = TimeHelper.GetDateTimeUTC(this.PositionInfo.Time);

			ctrlNumber = this.PositionInfo.ID.ToString();
			this.currSpeed = this.PositionInfo.Speed.ToString();
			this.status = new ImageRepresentation {Id = "status" + "_" + ID};
		}
		public Vehicle GetDTOObject()
		{
			var v = new Vehicle
						{
							id = ID,
							commands = Commands != null
										   ? Commands.ToArray()
										   : null,
							vehicleKind = VehicleKind
						};

			v.capabilities = new Dictionary<DeviceCapability, DeviceCapabilityOption>(Capabilities.Count);
			foreach (var capability in Capabilities)
				v.capabilities.Add(capability, new DeviceCapabilityOption { Allowed = true } );

			if (this.PositionInfo != null)
			{
				v.lat = this.PositionInfo.Latitude;
				v.lng = this.PositionInfo.Longitude;
				v.latCP = this.PositionInfo.LatitudeCP;
				v.lngCP = this.PositionInfo.LongitudeCP;
				v.radius = this.PositionInfo.Radius;
				v.course = (int)(this.PositionInfo.Course / 10);
				v.status = (Status)this.PositionInfo.Status;
				v.isValid = this.PositionInfo.ValidPosition;
				v.speed = this.PositionInfo.Speed;
				v.fuelTankVolume = this.fuelTankVolume;
				v.fuel = PositionInfo.FuelVolume;

			}

			v.Name = GarageNumber;
			v.regNum = RegNumber;
			v.brand = Type;
			v.ignition = ignition=="1";
			v.posDate = MyDateTime2;
			v.ctrlNum = ctrlNumber;
			v.address = CurrentAddress;
			v.isOld = IsOld;
			v.canBeDisactivated = CanBeDisactivated;

			if (Settings != null &&
				Settings.ContainsKey("icon") &&
				Settings["icon"]!=null)
			{
				v.iconUrl = Settings["icon"];
			}

			return v;
		}
		public override void WriteXml(System.Xml.XmlTextWriter writer)
		{
			// AS: Случилась небольшая жопа при сериализации сериалайзером, придется (пока) делать рукаме
			var nfi = new System.Globalization.NumberFormatInfo {NumberDecimalSeparator = "."};

			writer.WriteStartElement("vehicle");
			writer.WriteAttributeString("id", this.ID.ToString());
			writer.WriteAttributeString("old", this.IsOld.ToString().ToLower());
			writer.WriteAttributeString("vehicleKind", VehicleKind.ToString());
			writer.WriteAttributeString("canBeDisactivated", (CanBeDisactivated.HasValue && CanBeDisactivated.Value ? "1" : "0"));
			writer.WriteAttributeString("editable", XmlConvert.ToString(Editable));
			writer.WriteAttributeString("name", Name);

			if (this.PositionInfo != null)
			{
				int rest = this.PositionInfo.Course % 5;
				int course = (int)(Math.Round((double)(this.PositionInfo.Course) - rest));
				writer.WriteAttributeString("positionInfoId", this.PositionInfo.ID.ToString());
				writer.WriteAttributeString("angle", course.ToString());
				writer.WriteAttributeString("angle2", ((int)(this.PositionInfo.Course / 10)).ToString());
				writer.WriteAttributeString("longitude", this.PositionInfo.Longitude.ToString(nfi));
				writer.WriteAttributeString("latitude", this.PositionInfo.Latitude.ToString(nfi));
				writer.WriteAttributeString("status", ((int)(this.PositionInfo.Status)).ToString());
				writer.WriteAttributeString("validPosition", (PositionInfo.ValidPosition).ToString().ToLower());

				writer.WriteAttributeString("latitudeCP", this.PositionInfo.LatitudeCP.ToString(nfi));
				writer.WriteAttributeString("longitudeCP", this.PositionInfo.LongitudeCP.ToString(nfi));

				// AS: если позиция плохая, отобразим последнюю хорошую
				if (!PositionInfo.ValidPosition)
				{
					writer.WriteAttributeString("xcp", XCp.ToString());
					writer.WriteAttributeString("ycp", YCp.ToString());
				}

				writer.WriteAttributeString("radius", PositionInfo.Radius.ToString());
			}

			writer.WriteAttributeString("DepartmentCountryName", this.departmentCountryName);
			writer.WriteAttributeString("GarageNumber", GarageNumber);
			writer.WriteAttributeString("RegNumber", RegNumber);
			writer.WriteAttributeString("FuelSpentStandart", FuelSpentStandart.ToString("##0.####", CultureInfo.InvariantCulture));
			writer.WriteAttributeString("FuelSpentStandardMH", FuelSpentStandardMH.ToString("##0.####", CultureInfo.InvariantCulture));
			if (FuelTypeID != null)
				writer.WriteAttributeString("FuelTypeID", FuelTypeID.Value.ToString());
			if(FuelUnit != null)
				writer.WriteAttributeString("FuelUnit", FuelUnit.Value.ToString());
			if(TankUnit != null)
				writer.WriteAttributeString("FuelTankUnit", TankUnit.Value.ToString());
			writer.WriteAttributeString("TextId", TextID);
			writer.WriteAttributeString("Date", myDateTime);

			// AS: вывод даты в формате s, о котором говорилось ранее
			if (myDateTime2 != null)
				writer.WriteAttributeString("Date2", myDateTime2.Value.ToString("s"));

			writer.WriteAttributeString("currSpeed", currSpeed);
			writer.WriteAttributeString("ignition", ignition);
			writer.WriteAttributeString("ctrlNumber", ctrlNumber);
			writer.WriteAttributeString("x", X.ToString());
			writer.WriteAttributeString("y", Y.ToString());

			// AS: адрес
			writer.WriteAttributeString("address", CurrentAddress);

			// AS: сериализация невьебенно крутой картинки из базы блять
			// Если картинка есть, сообщим об этом
			if (VehicleImage != null)
			{
				writer.WriteAttributeString("hasImage", "true");
				/*System.IO.MemoryStream imageMem = new System.IO.MemoryStream(VehicleImage);
				Bitmap bmp = new Bitmap(imageMem);*/
			}

			// AS: тип объекта
			writer.WriteAttributeString("type", Type);

			// AS: Сериализация картинки
			/*if (Image != null)
			{
				this.Image.WriteXml(writer);
			}*/

			if (Owners != null)
			{
				// <owners>
				writer.WriteStartElement("owners");
				foreach (Person o in owners)
				{
					o.WriteXml(writer);
				}
				writer.WriteEndElement(); // </owners>
			}

			if (Controllers != null)
			{
				// <controllers>
				writer.WriteStartElement("controllers");
				foreach (Controller c in Controllers)
				{
					c.WriteXml(writer);
				}
				writer.WriteEndElement(); // </controllers>
			}

			if (Settings != null)
			{
				//<settings>
				writer.WriteStartElement("settings");

				foreach (string key in Settings.Keys)
				{
					string val =  Settings[key];

					//Обработка иконок
					if (key == "icon")
					{
						//Убираем старый абсолютный путь
						val = ProcessIconUrl(val);
					}
					writer.WriteElementString(key, val);
				}

				writer.WriteEndElement();
				//</settings>
			}

			// AS: объем топливного бака в литрах. 0, если топливный датчик не подключен
			var fuelTankInUnit = MeasureUnitHelper.ConvertTo(FuelTankVolume, UnitOfMeasure.Litre, TankUnit ?? UnitOfMeasure.Litre);
			writer.WriteElementString("fuelTankVolume", fuelTankInUnit.ToString("##0.####", CultureInfo.InvariantCulture));

			if (MaxAllowedSpeed != null)
			{
				writer.WriteElementString("MaxAllowedSpeed", MaxAllowedSpeed.Value.ToString(CultureInfo.InvariantCulture));
			}

			// AS: телефон контроллера
			writer.WriteElementString("ControllerPhoneNumber", ControllerPhoneNumber);

			// Возможности контроллера
			if (Capabilities != null)
			{
				writer.WriteStartElement("Capabilities");
				foreach (var capability in Capabilities)
					writer.WriteElementString("Capability", capability.ToString());
				writer.WriteEndElement(); // Возможности контроллера
			}

			if (Sensors!=null)
			{
				writer.WriteStartElement("Sensors");
				foreach (var sensor in Sensors)
				{
					var sensorLogRecords = sensor.LogRecords;

					writer.WriteStartElement("Sensor");
					writer.WriteAttributeString("Name",sensor.ToString());

					if (sensorLogRecords != null && sensorLogRecords.Count != 0)
					{
						var date = TimeHelper.GetDateTimeUTC(sensorLogRecords.Last().LogTime).ToLocalTime();
						writer.WriteAttributeString("Date", date.ToString());
						writer.WriteAttributeString("Value", sensorLogRecords.Last().Value.ToString());
					}

					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}

			if (_vehicleControlDates!=null)
			{
				writer.WriteStartElement("VehicleControlDates");
				foreach (var d in _vehicleControlDates)
					d.WriteXml(writer);
				writer.WriteEndElement();
			}

			if (Rights != null)
			{
				writer.WriteStartElement("Rights");
				foreach (var r in Rights)
				{
					writer.WriteStartElement("Right");
					writer.WriteAttributeString("Name", r.ToString());
					writer.WriteEndElement();

				}
				writer.WriteEndElement();

			}

			if (ZoneGroup != null)
			{
				ZoneGroup.WriteXml(writer, "ZoneGroup");
			}

			//TODO: реализовать сериализацию команд
			writer.WriteEndElement(); // vehicle
		}
		public static string ProcessIconUrl(string iconUrl)
		{
			if (string.IsNullOrEmpty(iconUrl))
				return iconUrl;

			string res = iconUrl;
			string basePath = ConfigurationManager.AppSettings["vehicleIconPath"];
			if (string.IsNullOrEmpty(basePath))
			{
				basePath = "/img/userdata/";
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
				{
					basePath = System.Web.HttpContext.Current.Request.ApplicationPath == "/"
						? basePath
						: System.Web.HttpContext.Current.Request.ApplicationPath + basePath;
				}
			}
			// Убираем старый абсолютный путь
			var parts = iconUrl.Split('/');
			if (parts.Length > 0)
			{
				res = VirtualPathUtility.Combine(
					VirtualPathUtility.AppendTrailingSlash(basePath),
					parts[parts.Length - 1]);
			}
			return res;
		}
		public virtual IWebMobileHistory GetHistory(IWebPersonalServer wps, DateTime from, DateTime to, int interval)
		{
			IWebMobileHistory args = null;
			try
			{
				args = wps.GetMobileHistory((int)(from - TimeHelper.year1970).TotalSeconds, (int)(to - TimeHelper.year1970).TotalSeconds, this.ID, interval, 2001);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return args;
		}
		public static string GetStatusIcon(Vehicle vehicle)
		{
			var vehicleStatusImageExtUrl = ConfigurationManager.AppSettings["VehicleStatusImageExtUrl"];
			if (!string.IsNullOrWhiteSpace(vehicleStatusImageExtUrl))
				return vehicleStatusImageExtUrl + vehicle.id;

			var leftUrlPart = ConfigurationManager.AppSettings["VehicleStatusIconBaseExtPath"];

			if ((vehicle.GetSensorValue(SensorLegend.Alarm) ?? 0) != 0)
				return leftUrlPart + "/images/alarmImage.gif";

			if (!vehicle.Supports(DeviceCapability.SpeedMonitoring))
				return null;


			var speed = vehicle.speed;
			var status = vehicle.status;
			var isvalid = vehicle.isValid;

			if (!(isvalid ?? false))
				return leftUrlPart + "/includes/images/object-car/off.png";


			if (status == null)
				return null;

			var anglePictureSuffix = GetAnglePictureSuffix(vehicle);

			switch (status.Value) {
				case Status.Alarm:
					if (anglePictureSuffix == null)
						return leftUrlPart + "/images/alarmImage.gif";
					return leftUrlPart + "/includes/icons/objects2/rest" + anglePictureSuffix.Value + ".png";
				case Status.ParcingMoving:
					return leftUrlPart + "/includes/icons/objects2/h_parkingW.png";
				case Status.Parcing:
					return leftUrlPart + "/includes/icons/objects/park.png";
				default:
					if (speed == 0)
						return leftUrlPart + "/includes/icons/objects2/parking3.png";

					if (speed > 0 && speed < 5 || anglePictureSuffix == null)
						return leftUrlPart + "/includes/icons/objects2/rest.png";
					return leftUrlPart + "/includes/icons/online/rest" + anglePictureSuffix.Value + ".png";
			}

		}
	}
	private static decimal? GetAnglePictureSuffix(Vehicle vehicle)
	{
		if (vehicle.course == null)
			return null;

		var angle = vehicle.course.Value;

		var ang = Math.Round(angle / 10m);

		if (ang < 0)
			ang += 36;

		if (ang == 36)
			ang = 0;

		return ang;
	}
	private Hashtable m_AllowVehicles = new Hashtable();
	public Vehicles()
	{
		//
		// TODO: Add constructor logic here
		//
	}
	public IEnumerator GetEnumerator()
	{
		foreach (VehiclesInfo vi in m_AllowVehicles.Values)
			yield return vi;
	}
	/// <summary> Отключить все </summary>
	public void UnCheckAll()
	{
		foreach (Vehicles.VehiclesInfo vi in m_AllowVehicles.Values)
			vi.Get = false;
	}
	/// <summary> Включить ТС </summary>
	public void SetCheckVehicle(int id)
	{
		if (m_AllowVehicles.ContainsKey(id))
		{
			((Vehicles.VehiclesInfo)m_AllowVehicles[id]).Get = true;
		}
	}
	/// <summary> Получить список  выделенных машин </summary>
	/// <returns></returns>
	public int[] GetCheckedVehicles()
	{
		ArrayList list = new ArrayList();
		foreach (Vehicles.VehiclesInfo vi in this.m_AllowVehicles.Values)
		{
			if (vi.Get)
			{
				list.Add(vi.ID);
			}
		}
		int[] result = new int[list.Count];
		list.CopyTo(result);
		return result;
	}
	public Vehicles.VehiclesInfo this[int id]
	{
		get
		{
			return GetVehicleByID(id);
		}
		set
		{
			m_AllowVehicles[id] = value;
		}
	}
	/// <summary> Получить ТС по идентификатору </summary>
	/// <param name="id"></param>
	/// <returns></returns>
	public VehiclesInfo GetVehicleByID(int id)
	{
		VehiclesInfo result = null;
		if (m_AllowVehicles.ContainsKey(id))
		{
			result = (Vehicles.VehiclesInfo)m_AllowVehicles[id];
		}
		return result;

	}
	public ICollection GetValues()
	{
		return this.m_AllowVehicles.Values;
	}
	public void Add(VehiclesInfo vi)
	{
		this.m_AllowVehicles.Add(vi.ID, vi);
	}
	/// <summary> Возвращает объект слежения по номеру контроллера
	/// Используется как "определитель номера" при входящем звонке с объекта мониторинга
	/// </summary>
	/// <param name="controllerNumber"> Номер контроллера искомого объекта </param>
	/// <returns></returns>
	public virtual Vehicles.VehiclesInfo GetVehicleByControllerNumber(string controllerNumber)
	{
		Vehicles.VehiclesInfo foundvi = null;
		foreach (Vehicles.VehiclesInfo vi in m_AllowVehicles.Values)
		{
			if (vi.ControllerPhoneNumber == controllerNumber)
			{
				foundvi = vi;
				break;
			}
		}
		return foundvi;
	}
	public int Count
	{
		get
		{
			return m_AllowVehicles.Count;
		}
	}
}