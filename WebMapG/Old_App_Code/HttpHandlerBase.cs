﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using System.Xml.Xsl;
using FORIS.TSS.Common.Helpers;
#if (DEBUG && XML)
using System.Configuration;
#endif
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	public class HttpHandlerBase : BasePage, IHttpHandler, IRequiresSessionState
	{
		private HttpContext context;
		protected HttpContext myContext
		{
			get { return context; }
		}
		public new bool IsReusable
		{
			get { return true; }
		}
		protected override string TemplateFile
		{
			get { return base.TemplateFile; }
			set
			{
				if (!IsMobile)
				{
					base.TemplateFile = context.Server.MapPath(@"/" + context.Request.ApplicationPath + @"/templates/xsl/" + value + ".xsl");
				}
				else
				{
					base.TemplateFile = context.Server.MapPath(@"/" + context.Request.ApplicationPath + @"/templates/xsl/mobile/" + value + ".xsl");
				}
			}
		}
		public override void ProcessRequest(HttpContext cont)
		{
			response = HttpContext.Current.Response;

			MemoryStream mem = new MemoryStream();
			Writer = new XmlTextWriter(mem, System.Text.Encoding.UTF8);

			//writer = null;
			context = cont;
			//session = myContext.Session;

			Object tmpMobileSelector = IsMobile;
			if (tmpMobileSelector != null)
			{
				IsMobile = (bool)tmpMobileSelector;
			}
			else
			{
				System.Web.HttpBrowserCapabilities browser = context.Request.Browser;
				String mobileDescr = browser.MobileDeviceModel.ToLower();
				BrowserTypes bi = BrowserInfo.getBrowserType(browser);
				if (bi == BrowserTypes.mobilePhone || bi == BrowserTypes.pda)
				{
					IsMobile = true;
				}
				//IsMobile = true;
			}

			UpdateSessionDateTime();

			if (!IsUserLogged)
			{
				//RedirectToMainPage();
				RenderSessionClosedPage();
				FinishPageRender();
				return;
			}

			Process();
		
			// AS: Если было что-то записано во внутренний поток, пишем все в выходной поток
			if (Writer != null)
			{
				FinishPageRender();
			}
		}
		/// <summary> Получить значение параметра из запроса </summary>
		/// <param name="paramKey"> Ключ параметра </param>
		/// <param name="defaultValue"> Значение по умолчанию </param>
		/// <returns> Значение параметра </returns>
		protected override string GetParamValue(string paramKey, string defaultValue = "")
		{
			return myContext.Request.Params[paramKey] ?? defaultValue;
		}
		/// <summary> Найдем методы, соответствующие указанному в атрибуте PageActionAttribute действию и выполним их </summary>
		protected override void Process()
		{
			// Получить имя действия
			var httpAction = GetParamValue("a", "default")
				?.Trim();
			try
			{
				// Получить метод обработчика действия API
				var pageAction = GetType().GetMethods()
					?.SelectMany(m =>
						m.GetCustomAttributes<PageActionAttribute>(false)
							?.Where(a => a.Action == httpAction)
							?.Select(a => new { Method = m, Action = a }))
					?.FirstOrDefault();
				// Выполнить метод обработчика действия API
				if (null != pageAction)
					pageAction.Method.Invoke(this, null);
			}
			catch (Exception ex)
			{
				WriteLog(ex);
			}
		}
		/// <summary> Начало сериализации ответа сервера </summary>
		/// <returns></returns>
		protected override void ResponseSerializationBeg()
		{
			Writer.Formatting = Formatting.None;
			Writer.WriteStartDocument();
			Writer.WriteStartElement("response");

			// AS: добавим имя приложения в свойства - пусть сериализуется
			AddProperty("ApplicationPath", Request.ApplicationPath != null && Request.ApplicationPath.Length == 1 ? string.Empty : Request.ApplicationPath);

			// AS: Так же добавим логин вошедшего юзера
			AddProperty("loggedUser", LoggedUser);

			// AS: И текущий язык
			AddProperty("language", CurrentCulture.ToString());

			// AS: Тип браузера
			AddProperty("browser", context.Request.Browser.Type);

			// AS: Сериализуем все свойства. Без рефлексии быстрее
			foreach (String key in properties.Keys)
			{
				Writer.WriteAttributeString(key, properties[key]);
			}
		}
		/// <summary> Установка типа содержимого в Json </summary>
		protected override void SetJsonContentType()
		{
			context.Response.ContentType     = "text/plain";
			context.Response.ContentEncoding = Encoding.UTF8;
			contentTypeSet                   = true;
		}
		/// <summary> Установка типа содержимого в XHTML </summary>
		protected override void SetXhtmlContentType()
		{
			/*if (Request.Browser.Type.Contains("IE"))
			Response.ContentType = "application/xml";
		else*/
			context.Response.ContentType = "application/xhtml+xml";
			//Response.ContentType = "text/xml";
			context.Response.ContentEncoding = System.Text.Encoding.UTF8;
			contentTypeSet = true;
		}
		/// <summary> Установка типа содержимого в XML </summary>
		protected override void SetXmlContentType()
		{
			context.Response.ContentType     = "text/xml";
			context.Response.ContentEncoding = Encoding.UTF8;
			contentTypeSet                   = true;
		}
		protected override void SetSvgContentType()
		{
			context.Response.ContentType = "image/svg+xml";
			contentTypeSet = true;
		}
		/// <summary> Вывод в выходной поток получившегося ответа, HTTP хендлерах устанавливаем тип документа сами в каждой функции-обработчике запроса </summary>
		protected override void FinishPageRender()
		{
			//text/plain is used for json
			if (context.Response.ContentType != "text/plain")
			{
				Writer.Flush();
				if ((TemplateFile != null) && (TemplateFile.Length > 0))
				{
					/*byte[] buffer = System.Text.Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
				Response.OutputStream.Write(buffer, 0, (int)buffer.Length);
				buffer = System.Text.Encoding.UTF8.GetBytes("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n");
				Response.OutputStream.Write(buffer, 0, (int)buffer.Length);*/

					WriteXmlToStream(Writer.BaseStream, context.Response.OutputStream);
				}
				else
				{
					try
					{
						context.Response.Write(GetStringOfStream(Writer.BaseStream));
					}
					catch (ArgumentNullException aex)
					{
						WriteLog(aex);
					}
				}
			}
			Writer.Close();
		}
		/// <summary> Запись сериализованного ответа сервера в выходной поток для передачи клиенту </summary>
		/// <param name="stm"></param>
		/// <param name="outputStream"></param>
		protected override void WriteXmlToStream(Stream stm, Stream outputStream)
		{
			XmlReader reader = null;
			XmlTextWriter writer = null;
			try
			{
				#region Debug stuff
#if (DEBUG && XML)
				FileStream fs = null;
				try
				{
					var tfile = TemplateFile.Length > 0 ? TemplateFile : "page";
					tfile = Path.GetFileName(tfile);
					var fileName = ConfigurationManager.AppSettings["debugOutputFile"] + tfile + "_" + OperatorId.ToString() + ".xml";
					try
					{
						fs = new FileStream(
							fileName,
							File.Exists(fileName) ? FileMode.Truncate : FileMode.Create);
					}
					catch (Exception)
					{
						fs = new FileStream(fileName, FileMode.Create);
					}

					var xmlDocumentToWrite = new XmlDocument();

					stm.Position = 0;
					xmlDocumentToWrite.Load(stm);

					using (var xmlTextWriter = new XmlTextWriter(fs, Encoding.UTF8) { Formatting = Formatting.Indented })
						xmlDocumentToWrite.WriteTo(xmlTextWriter);
				}
				catch (Exception ex)
				{
					SysDiags::Trace.TraceError(this.GetTypeName() + ": {0} raise '{1}'\n{2}",
						CallStackHelper.GetCallerMethodName(), ex.GetType().Name, ex.Message);
				}
				finally
				{
					if (fs != null)
					{
						fs.Close();
					}
				}
#endif
				#endregion

				stm.Position = 0;

				// AS: через вот такую хитрую жопу присваиваем XmlDocument'у свойство BaseURI,
				// потому что XML создаем в потоке, и у него нет своего URI, как у XML из файла.
				// Без baseURI функция document() пошлет все запросы нахуй, потому что не знает свой URI.
				// Вот такая херотень...
				XmlNameTable xnt = new NameTable();
				XmlParserContext xparCon = new XmlParserContext(xnt, new XmlNamespaceManager(xnt), null, XmlSpace.Preserve);
				xparCon.BaseURI = context.Request.Url.ToString();

				XmlReaderSettings settings = new XmlReaderSettings();
				settings.DtdProcessing = DtdProcessing.Parse;
				reader = XmlTextReader.Create (stm, settings, xparCon);
			

				// AS: если не указать ресолвер с какими-то крендетиалами (у меня по умолчанию),
				// AS: то в XSL функция document() работать ни хуя не будет, втыкать MSDN
				XmlUrlResolver resolver = new XmlUrlResolver();
				resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

				XslTransform xslt = new XslTransform();
				xslt.Load(TemplateFile);


				XmlDocument xdoc = new XmlDocument();
				xdoc.Normalize();
				xdoc.Load(reader);

				outputStream.Flush();
				writer = new XmlTextWriter(outputStream, Encoding.UTF8);
				//writer.Formatting = Formatting.Indented;

				XsltArgumentList args = new XsltArgumentList();
				DateTime nowDate = DateTime.Now;
				args.AddParam("date", "", nowDate.ToString());
				xslt.Transform(xdoc, args,  writer, resolver);

				outputStream.Flush();
			}
			catch (ArgumentException ex)
			{
				WriteLog(ex);
			}
			catch (XsltException ex)
			{
				throw ex;
			}
			catch (XmlException ex)
			{
				throw ex;
			}
			catch (OutOfMemoryException ex)
			{
				throw ex;
			}
			catch (FileNotFoundException ex)
			{
				WriteLog(ex);
			}
			catch (Exception appEx)
			{
				WriteLog(appEx);
			}
			finally
			{
				if (reader != null)
					reader.Close();
				if (writer != null)
					writer.Close();
			}
		}
		/// <summary> Установка кеширования страницы </summary>
		protected override void dontCacheMe()
		{
			//Response.AppendHeader();
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.Cache.SetLastModified(DateTime.Now);
			context.Response.Cache.SetNoServerCaching();
			context.Response.Cache.SetNoStore();
		}
	}
}