﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class JsonPageActionAttribute : PageActionAttribute
	{
		public JsonPageActionAttribute(string act = "default", HttpMethod method = HttpMethod.Get)
			: base(act)
		{
			Method       = method;
			RequestType  = DataType.Json;
			ResponseType = DataType.Json;
		}
	}
}