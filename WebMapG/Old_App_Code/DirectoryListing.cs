﻿using System;
using System.Web;
using FORIS.TSS.Config;

/// <summary>
/// AS: базовый класс для страниц, отображающих список файлов в своих каталогах.
/// Этот класс не содержит метода по умолчанию, такой метод должен быть определен в производном классе
/// </summary>
public class DirectoryListing : BasePage
{
	public DirectoryListing() : base() {}

	protected virtual void Listing()
	{
		string[] files = GetFiles();
		// <files>
		Writer.WriteStartElement("files");
		
		string webSiteUrl = GlobalsConfig.AppSettings["webSiteUrl"];
		if (string.IsNullOrEmpty(webSiteUrl)) webSiteUrl = "http://tss.sitels.ru";

		foreach (string f in files)
		{
			//String urlPath = "http://" + Request.ServerVariables["HTTP_HOST"] + VirtualPathUtility.GetDirectory(Request.Path) + System.IO.Path.GetFileName(f);
			string urlPath = webSiteUrl + VirtualPathUtility.GetDirectory(Request.Path) + System.IO.Path.GetFileName(f);
			//String urlPath = VirtualPathUtility.GetDirectory(Request.Path) + System.IO.Path.GetFileName(f);
			Writer.WriteElementString("file", urlPath);
		}
		Writer.WriteEndElement();
		// </files>

	}

	protected virtual string[] GetFiles(string path)
	{
		string[] files = System.IO.Directory.GetFiles(path);
		return files;
	}

	protected virtual string[] GetFiles()
	{
		return GetFiles(System.IO.Path.GetDirectoryName(Server.MapPath(Request.Path)));
	}
}