﻿using System;
using System.Data;
using System.Xml;
using Interfaces.Web;

/// <summary> Summary description for Geozone </summary>
[Serializable]
public class Geozone : BaseSerializable
{
	public Geozone()
	{
	}
	/// <summary> Сериализует список всех доступных для оператора геозон </summary>
	/// <param name="ps"> Ссылка на персональный сервер </param>
	/// <param name="writer"> куда сериализовать список </param>
	public static void GeozonesToXml(IWebPersonalServer ps, XmlTextWriter writer)
	{
		try
		{
			DataSet zones = ps.GetZonesWebForOperator(false, 0);

			if (zones != null)
			{
				zones.DataSetName = "zones";
				zones.WriteXml(writer);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
}