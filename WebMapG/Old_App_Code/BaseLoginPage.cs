﻿using System;
using System.Collections.Generic;
using System.Configuration;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Helpers;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG
{
	public class BaseLoginPage : BasePage
	{
		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();
			//Еще не вошли в систему, поэтому обработчик из базового класса работает неправильно
			Process();
		}

		protected void ProcessLoginError(LoginResult loginResult)
		{
			switch (loginResult)
			{
				case LoginResult.WrongPassword:
					AddMessage(new Message(Severity.Error, GetMessageData("WrongPassword"), ""));
					break;
				case LoginResult.SessionAlreadyExists:
					AddMessage(new Message(Severity.Error, GetMessageData("SessionAlreadyExists"), ""));
					break;
				case LoginResult.CannotConnectToServer:
					AddMessage(new Message(Severity.Error, GetMessageData("CannotConnectToServer"), ""));
					break;
				case LoginResult.NoAvailableVehicles:
					AddMessage(new Message(Severity.Error, GetMessageData("error"), "Ошибка авторизации! Нет доступных объектов для слежения!"));
					break;
				case LoginResult.AccessLocked:
					AddMessage(new Message(Severity.Error, GetMessageData("AccessLocked"), ""));
					break;
				default:
					AddMessage(new Message(Severity.Error, GetMessageData("error"), ""));
					break;
			}
			RenderLoginPage();
		}

		protected void ProcessedLoginAjaxError(LoginResult loginResult)
		{
			switch (loginResult)
			{
				case LoginResult.WrongPassword:
					AddMessage(new Message(Severity.Error, GetMessageData("WrongPassword"), ""));
					break;
				case LoginResult.SessionAlreadyExists:
					AddMessage(new Message(Severity.Error, GetMessageData("SessionAlreadyExists"), ""));
					break;
				case LoginResult.CannotConnectToServer:
					AddMessage(new Message(Severity.Error, GetMessageData("CannotConnectToServer"), ""));
					break;
				case LoginResult.AccessLocked:
					AddMessage(new Message(Severity.Error, GetMessageData("AccessLocked"), ""));
					break;
				case LoginResult.NoAvailableVehicles:
					AddMessage(new Message(Severity.Error, GetMessageData("error"), "Ошибка авторизации! Нет доступных объектов для слежения!"));
					break;
				default:
					AddMessage(new Message(Severity.Error, GetMessageData("error"), ""));
					break;
			}
			
			var jsonres = new Dictionary<string, string> {{"result", loginResult.ToString()}};
			JsonResponse.value = JsonHelper.SerializeObjectToJson(jsonres);
		}

		protected void LoginByConfirmationKey(string confirmationKey)
		{
			var operatorID = WebLoginProvider.GetOperatorIdByConfirmationKey(confirmationKey);

			if (operatorID == null)
			{
				AddMessage(
					new Message
					{
						Severity = Severity.Error,
						MessageBody = GetMessageData("MessageForInvalidConfirmationKey")
					});
				return;
			}

			var loginResult = TryLogIn(operatorID.Value);
			if (loginResult != LoginResult.Success)
			{
				ProcessLoginError(loginResult);
				return;
			}

			// Перенаправление в настройки пользователя в настройки для смены пароля
			ForceChangePassword();

			RedirectResponse("settings.aspx");
		}

		protected bool CheckCaptcha()
		{
			if (!string.Equals(ConfigurationManager.AppSettings["CaptchEnabled"], "true", StringComparison.OrdinalIgnoreCase))
				return true;

			var remoteIP  = Request.ServerVariables["REMOTE_ADDR"] as string;
			var challenge = GetParamValue("recaptcha_challenge_field");
			var response  = GetParamValue("recaptcha_response_field");

			return WebLoginProvider.CheckCaptcha(remoteIP, challenge, response);
		}

		protected override void ResponseSerializationBeg()
		{
			TranslateConfig("SalesEmail");
			TranslateConfig("SalesPhone");
			TranslateConfig("GuestLogin");
			TranslateConfig("Login.RegistrationLink");
			TranslateConfig("Login.PasswordRecoveryLink");

			base.ResponseSerializationBeg();
		}
	}
}