﻿using System;
using System.Data;
using System.Collections;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.UI.WebMapG
{
    [Serializable]
    public class Person : BaseSerializable
    {
        private int _id;

        private String _firstName;
        private String _secondName;
        private String _lastName;

        private String _email;

        private Contact[] _phones;

        private Contact[] _emails;

        private bool? _hasBillingBlockings;

        private String _timeZoneInfoString;

        [XmlIgnore]
        private TimeZoneInfo _timeZoneInfo;

    
        private string _phone;

    
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public String SecondName
        {
            get { return _secondName; }
            set { _secondName = value; }
        }

        public String LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public String Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public Contact[] Phones
        {
            get { return _phones ?? (_phones = new Contact[0]); }
            set { _phones = value; }
        }

        public Contact[] Emails
        {
            get { return _emails ?? (_emails = new Contact[0]); }
            set
            {
                throw new NotSupportedException();
            }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public bool? HasBillingBlockings
        {
            get { return _hasBillingBlockings; }
            set { _hasBillingBlockings = value; }
        }

        [XmlIgnore]
        public TimeZoneInfo TimeZoneInfo
        {
            get
            {
                if (_timeZoneInfo == null)
                {
                    _timeZoneInfo = string.IsNullOrEmpty(_timeZoneInfoString)
                                       ? TimeZoneInfo.Local
                                       : TimeZoneInfo.FromSerializedString(_timeZoneInfoString);
                }

                return _timeZoneInfo;
            }
            set
            {
                _timeZoneInfo = value;
                _timeZoneInfoString = _timeZoneInfo == null ? null : _timeZoneInfo.ToSerializedString();
            }
        }

        public string TimeZoneInfoString
        {
            get
            {
                return _timeZoneInfoString;
            }
            set
            {
                _timeZoneInfoString = value;
                _timeZoneInfo = null;
            }
        }

        public Person()
        {
        }

        /// <summary>
        /// AS: Загрузка свойств оператора из БД
        /// </summary>
        /// <param name="id">ID оператора</param>
        /// <param name="ps">персональный сервер</param>
        public Person(int id, global::Interfaces.Web.IWebPersonalServer ps)
        {
        
            var arParams = new ArrayList();

            arParams.Add(new BusinessLogic.Data.ParamValue("@operator_id", id));
            arParams.Add(new BusinessLogic.Data.ParamValue("@GetBillingBlockings", true));

            DataSet personds = ps.GetDataFromDB(
                arParams,
                "dbo.getoperator",
                new[] { "OPERATOR", "Billing_Blocking" }
                );

            personds.DataSetName = "operator";

            TimeZoneInfo = ps.TimeZoneInfo ?? TimeZoneInfo.Local;

            ParseData(personds);

            GetOperatorPhones(id, ps);
            GetOperatorEmails(id, ps);
        }

        /// <summary>
        /// AS: разбираем датасет из БД
        /// </summary>
        /// <param name="personds"></param>
        protected void ParseData(DataSet personds)
        {
            if (personds.Tables.Count == 0)
                return;

            DataTable p = personds.Tables[0];

            DataRow r = p.Rows[0];
            _id = (int) r["OPERATOR_ID"];
            Email = r["EMAIL"] as string;

            Phone = r["PHONE"] as string;

            _hasBillingBlockings = personds.Tables.Contains("Billing_Blocking")
                                      ? personds.Tables["Billing_Blocking"].Rows.Count != 0
                                      : (bool?) null;
        }

        private void GetOperatorEmails(Int32 operatorId, global::Interfaces.Web.IWebPersonalServer ps)
        {
            _emails = ps.GetOperatorEmails(operatorId);
        }

        private void GetOperatorPhones(Int32 operatorId, global::Interfaces.Web.IWebPersonalServer ps)
        {
            _phones = ps.GetOperatorPhones(operatorId);
        }

        public void SetPhones(Contact[] phones)
        {
            _phones = phones;
        }
    }
}
