using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// AS: ������ HTMLImage
/// </summary>
/// 
[Serializable]
[System.Xml.Serialization.XmlRoot("image")]
public partial class ImageRepresentation : BaseSerializable
{
    private String id;
    private String src;
    private String alternativeText;
    private String description;

    private int left;
    private int top;

    //private Dictionary<String, String> style;

    /// <summary>
    /// AS: ������ - ������ ��� HTML Image ����� � �������� ����� ��������������� ������
    /// - ���-��� �� ������ �����?
    /// - ���������������, ����
    /// 
    /// ���... ��� � ����� ������..
    /// </summary>
    public String Id
    {
        get { return id; }
        set { id = value; }
    }

    public String Src
    {
        get { return src; }
        set { src = value; }
    }

    /// <summary>
    /// AS: �������������� ����� �����������
    /// </summary>
    public String Alt
    {
        get { return alternativeText; }
        set { alternativeText = value; }
    }

    /// <summary>
    /// AS: ��������. ���������� ����� ��� ����� ��� �������
    /// </summary>
    public String Description
    {
        get { return description; }
        set { description = value; }
    }

    public int Left
    {
        get { return left; }
        set { left = value; }
    }

    public int Top
    {
        get { return top; }
        set { top = value; }
    }

    /*public Dictionary<String, String> Style
    {
        get
        {
            if (style == null)
            {
                style = new Dictionary<string, string>();
            }

            return style;
        }
    }*/

	/// <summary>
	/// AS: default .ctor
	/// </summary>
    public ImageRepresentation()
	{
	}

    public ImageRepresentation(String nid, String nsrc)
    {
        id = nid;
        src = nsrc;
    }
}
