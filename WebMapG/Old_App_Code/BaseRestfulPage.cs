﻿namespace FORIS.TSS.UI.WebMapG
{
	public class BaseRestfulPage : BasePage
	{
		protected override void Page_Load(object sender, System.EventArgs e)
		{
			MandatoryPageLoad();

			if (!IsAuthorized)
			{
				Unauthorized();
				return;
			}
			base.Page_Load(sender, e);
		}
	}
}