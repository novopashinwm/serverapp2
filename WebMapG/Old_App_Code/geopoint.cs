﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Xml;
using System.Xml.Serialization;
using Interfaces.Web;

[Serializable]
public class geopoint
{
	/// <summary>
	/// AS: прокси к серверу
	/// </summary>
	protected IWebPersonalServer IWebPS;

	/// <summary>
	/// AS: установлено в тру, если эта поебень выбрана в списке
	/// </summary>
	protected bool selected;

	/// <summary>
	/// AS: тип точки
	/// </summary>
	protected int type;

	protected int id;

	/// <summary>
	/// AS: долгота в системе WGS84
	/// </summary>
	protected float wgsX;

	/// <summary>
	/// AS: широта в системе WGS84
	/// </summary>
	protected float wgsY;

	/// <summary>
	/// AS: имя
	/// </summary>
	protected String name;

	/// <summary> Описание </summary>
	protected String description;

	/// <summary>
	/// AS: x-координата точки в окне
	/// </summary>
	protected int winX;

	/// <summary>
	/// AS: y-координата точки в окне
	/// </summary>
	protected int winY;

	protected bool editable;

	[XmlAttribute]
	public bool Editable { get { return editable; } private set { editable = value; } }

	[XmlAttribute]
	public int Type
	{
		get { return type; }
		set { type = value; }
	}

	[XmlAttribute]
	public int Id
	{
		get { return id; }
		set { id = value; }
	}

	[XmlAttribute]
	public float WgsX
	{
		get { return wgsX; }
		set { wgsX = value; }
	}

	[XmlAttribute]
	public float WgsY
	{
		get { return wgsY; }
		set { wgsY = value; }
	}

	[XmlAttribute]
	public String Name
	{
		get { return name; }
		set { name = value; }
	}

	[XmlAttribute]
	public String Description
	{
		get { return description; }
		set { description = value; }
	}

	[XmlAttribute]
	public int WinX
	{
		get { return winX; }
		set { winX = value; }
	}

	[XmlAttribute]
	public int WinY
	{
		get { return winY; }
		set { winY = value; }
	}

	[XmlAttribute]
	public bool Selected
	{
		get { return selected; }
		set { selected = value; }
	}

	public geopoint(IWebPersonalServer ps)
	{
		IWebPS = ps;
	}

	/// <summary>
	/// AS: просто конструктор.
	/// </summary>
	public geopoint()
	{
	}

	/*
	/// <summary>
	/// AS: Пересчет координат из WGS84 в координаты окна в браузере
	/// </summary>
	/// <param name="corrx"></param>
	/// <param name="corry"></param>
	/// <param name="fkWinToMap"></param>
	/// <param name="currentScale"></param>
	public virtual void CalculateCurrentPosition(int corrx, int corry, float fkWinToMap, int currentScale, MapDescription md)
	{
		PointF WinCoord = md.coordGeoToWindow(WgsX, WgsY, fkWinToMap, corrx, corry, currentScale);
		WinX = (int)Math.Round(WinCoord.X);
		WinY = (int)Math.Round(WinCoord.Y);
	}
	*/

	public virtual void Delete()
	{
		geopoint.Delete(Id, this.IWebPS);
	}

	/// <summary>
	/// AS: Сохранение существующей точки интереса (после редактирования)
	/// </summary>
	public virtual void Save()
	{
		PointF[] points = new PointF[1];
		points[0].X = this.WgsX;
		points[0].Y = this.WgsY;

		try
		{
			this.IWebPS.EdtPointWebForOperator(this.Id, points[0], Name, Description, Type);
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	public virtual void Save(Guid mg)
	{
		PointF point = new PointF(this.WgsX, this.WgsY);
		try
		{
			IWebPS.AddPointWebForOperator(point, Name, Description, Type);
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	public virtual void WriteXml(XmlTextWriter writer)
	{
		XmlSerializer serializer = new XmlSerializer(typeof(geopoint));
		serializer.Serialize(writer, this);
	}


	/*
	 * AS: Статические методы
	 */

	public static void Delete(int pointId, IWebPersonalServer IWebPS)
	{
		try
		{
			IWebPS.DelPointWebForOperator(pointId);
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	public static List<geopoint> GetPoints(DataSet ds, int selectedId)
	{
		List<geopoint> l = new List<geopoint>();

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ",";

		/*System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
		format.NumberDecimalSeparator = ".";*/

		foreach (DataTable tab in ds.Tables)
		{
			if (tab.Rows.Count == 0) continue;
			foreach (DataRow row in tab.Rows)
			{
				float wgsX;
				float wgsY;
				int id;
				float.TryParse(row["X"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out wgsX);
				float.TryParse(row["Y"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out wgsY);
				Int32.TryParse(row["POINT_ID"].ToString(), out id);

				String Name = row["NAME"].ToString();
				String Description = row["DESCRIPTION"].ToString();

				geopoint gp = new geopoint();
				gp.Id = id;
				gp.WgsX = wgsX;
				gp.WgsY = wgsY;
				gp.Name = Name;
				gp.Description = Description;

				gp.Selected = (gp.Id == selectedId);

				gp.Editable = Convert.ToBoolean(row["Editable"]);

				l.Add(gp);
			}
		}
		return l;
	}

	public static void WriteXml(List<geopoint> geopoints, XmlTextWriter writer, int corrx, int corry, float fkWinToMap, int scale, MapDescription md, int selectedId)
	{
		// <geopoints>
		writer.WriteStartElement("geopoints");
		foreach (geopoint gp in geopoints)
		{
			gp.Selected = (gp.Id == selectedId);
			//gp.CalculateCurrentPosition(corrx, corry, fkWinToMap, scale, md);
			gp.WriteXml(writer);
		}
		writer.WriteEndElement();
		// </geopoints>
	}
}