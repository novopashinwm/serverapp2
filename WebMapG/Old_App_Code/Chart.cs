﻿using System;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// AS: базовый класс для графиков
/// </summary>
[Serializable]
public class Chart
{
    /// <summary>
    /// AS: значения по оси Y. Строковые, потому что может быть всякая хуйня
    /// </summary>
    /*private List<String> yvalues;

    /// <summary>
    /// AS: значения по оси X. Строковые, потому что может быть всякая хуйня типа времени
    /// Да и все равно сериализуется в XML, так что пох
    /// </summary>
    private List<String> xvalues;*/

    private String name;

    /// <summary>
    /// AS: название графика
    /// </summary>
    public String Name
    {
        get { return name; }
        set { name = value; }
    }

    /// <summary>
    /// AS: Значения по оси Y
    /// </summary>
    /*public List<String> yValues
    {
        get { return yvalues; }
        set { yvalues = value; }
    }

    /// <summary>
    /// AS: значения по оси X
    /// </summary>
    public List<String> xValues
    {
        get { return xvalues; }
        set { xvalues = value; }
    }*/

    /// <summary>
    /// AS: создание графика
    /// </summary>
    /// <param name="name">Название графика</param>
    public Chart(String name)
    {
        Name = name;
    }

    public Chart()
    {
    }

    public virtual void BuildChart(XmlTextWriter writer)
    {
        // AS: этот класс сериализуется в XML, при этом значения должны быть заполнены,
        // видимо, в конструкторе наследника

        XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();
        nameSpaces.Add("nsTss", "http://tss.ru/chart");

        XmlSerializer serializer = new XmlSerializer(this.GetType());
        serializer.Serialize(writer, this, nameSpaces);
    }

    /*public virtual void AddXValue(String val)
    {
        if (xValues == null)
        {
            xValues = new List<string>();
        }
        xValues.Add(val);
    }

    public virtual void AddYValue(String val)
    {
        if (yValues == null)
        {
            yValues = new List<string>();
        }
        yValues.Add(val);
    }*/
}
