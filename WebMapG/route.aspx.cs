﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.UI.WebMapG;
using Interfaces.Geo;

public partial class route : BasePage
{
	/// <summary> Пересчитать оконные координаты точке уже найденного маршрута. Вызывается при изменении масштаба </summary>
	[PageAction("recalculateroute")]
	public void RecalculateBestRoute()
	{
		SetXmlContentType();

		TemplateFile = "route";
		AddProperty("template", "displayRouteOnMap");

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ".";

		// AS: Корректировка - сдвиг относительно всей карты и относительно центра окна (суммируется на клиенте)
		int corrx;
		int corry;
		int scale;
		int.TryParse(GetParamValue("corrx"), out corrx);
		int.TryParse(GetParamValue("corry"), out corry);
		int.TryParse(GetParamValue("scale"), out scale);

		// AS: коэффициент - количество пикселей в метре, зависит от масштаба
		float fKWinToMap;
		float.TryParse(GetParamValue("fk"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out fKWinToMap);

		float sx;
		float sy;
		float ex;
		float ey;

		float.TryParse(GetParamValue("splong"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out sx);
		float.TryParse(GetParamValue("splat"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out sy);
		float.TryParse(GetParamValue("eplong"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out ex);
		float.TryParse(GetParamValue("eplat"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out ey);

		var begP = new PointF(sx, sy);
		var endP = new PointF(ex, ey);

		ResponseSerializationBeg();

		SerializeCommonRouteInformation(begP, endP, MapGuid);

		Dictionary<int, List<PointF>> points = (Dictionary<int, List<PointF>>)GetObjectFromSession("bestRoutePoints");
		if (points != null)
		{
			SerializeRoutePoints(points, corrx, corry, scale, fKWinToMap, begP, endP);
		}
		ResponseSerializationEnd();
	}
	/// <summary> Сохранение маршрута нового маршрута </summary>
	[PageAction("saveroute")]
	public void SaveRoute()
	{
		var name = GetParamValue("routename");
		var desc = GetParamValue("routedescription");

		Dictionary<int, List<PointF>> points = (Dictionary<int, List<PointF>>)GetObjectFromSession("bestRoutePoints");
		if (points != null)
		{
			//PointF[] pts = new PointF[points.Count];
			List<PointF> resultPts = new List<PointF>();
			foreach (int key in points.Keys)
			{
				List<PointF> section = points[key];
				foreach (PointF p in section)
				{
					resultPts.Add(p);
				}
			}
			PointF[] pts = new PointF[resultPts.Count];
			int i = 0;
			foreach (PointF p in pts)
			{
				pts[i++] = p;
			}
			try
			{
				IWebPS.AddLineWebForOperator(MapGuid ?? MapGuids.OsmMapGuid, pts, name, desc);
			}
			catch (Exception myEx)
			{
				AddMessage(new Message(Severity.Error, myEx.Message, ""));
				WriteLog(myEx);
			}
		}
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}
	/// <summary> Получить список всех сохраненных маршрутов </summary>
	[PageAction("getallroutes")]
	public void GetAllRoutes()
	{
		TemplateFile = "route";
		AddProperty("template", "displayRoutesInList");

		int routeId;
		int.TryParse(GetParamValue("id"), out routeId);

		ResponseSerializationBeg();
		try
		{
			DataSet routes = IWebPS.GetLinesWebForOperator(MapGuid ?? MapGuids.OsmMapGuid, false, -1);
			if (routeId > 0)
			{
				routes.DataSetName = "points";
			}
			else
			{
				routes.DataSetName = "routes";
			}
			routes.WriteXml(Writer);
		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, myEx.Message, ""));
			WriteLog(myEx);
		}
		ResponseSerializationEnd();
	}
	/// <summary> Получить информацию конкретного маршрута </summary>
	[PageAction("getrouteinfo")]
	public void GetRouteInfo()
	{
		TemplateFile = "route";
		AddProperty("template", "displayRouteOnMap");

		int routeId;
		int.TryParse(GetParamValue("id"), out routeId);

		ResponseSerializationBeg();
		try
		{
			DataSet routes = IWebPS.GetLinesWebForOperator(MapGuid ?? MapGuids.OsmMapGuid, true, routeId);

			System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
			format.NumberDecimalSeparator = ".";

			System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
			nfi.NumberDecimalSeparator = ",";
			System.Globalization.NumberFormatInfo nfi2 = new System.Globalization.NumberFormatInfo();
			nfi2.NumberDecimalSeparator = ".";

			// Корректировка - сдвиг относительно всей карты и относительно центра окна (суммируется на клиенте)
			int corrx;
			int corry;
			int scale;
			int.TryParse(GetParamValue("corrx"), out corrx);
			int.TryParse(GetParamValue("corry"), out corry);
			int.TryParse(GetParamValue("scale"), out scale);

			// AS: коэффициент - количество пикселей в метре, зависит от масштаба
			float fKWinToMap;
			float.TryParse(GetParamValue("fk"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi2, out fKWinToMap);
			
			float minx = -1;
			float miny = -1;

			float maxx = -1;
			float maxy = -1;
			foreach (DataTable tab in routes.Tables)
			{
				Writer.WriteStartElement("points");
				foreach (DataRow row in tab.Rows)
				{
					float x;
					float y;
					float.TryParse(row["X"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
					float.TryParse(row["Y"].ToString(), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);

					Writer.WriteStartElement("point");
					Writer.WriteAttributeString("long", x.ToString(format));
					Writer.WriteAttributeString("lat", y.ToString(format));

					Writer.WriteEndElement();

					if (minx == -1 || x < minx) minx = x;
					if (miny == -1 || y < miny) miny = y;

					if (maxx == -1 || x > maxx) maxx = x;
					if (maxy == -1 || y > maxy) maxy = y;
				}
				Writer.WriteEndElement(); // points
			}
			Writer.WriteElementString("minLongitude", minx.ToString(format));
			Writer.WriteElementString("minLatitude", miny.ToString(format));

			Writer.WriteElementString("maxLongitude", maxx.ToString(format));
			Writer.WriteElementString("maxLatitude", maxy.ToString(format));

		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, myEx.Message, ""));
			WriteLog(myEx);
		}
		ResponseSerializationEnd();
	}
	/// <summary> Редактирование маршрута </summary>
	[PageAction("editroute")]
	public void EditRoute()
	{
		int routeId;
		int.TryParse(GetParamValue("id"), out routeId);
		PointF[] pts = GetPoints(GetParamValue("points"));

		var routeName = GetParamValue("routename");
		var routeDesc = GetParamValue("routedescription");
		try
		{
			IWebPS.EdtLineWebForOperator(routeId, pts, routeName, routeDesc);
		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, myEx.Message, ""));
			WriteLog(myEx);
		}
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}
	/// <summary> Удаление маршрута </summary>
	[PageAction("removeroute")]
	public void RemoveRoute()
	{
		int routeId;
		int.TryParse(GetParamValue("routeid"), out routeId);

		try
		{
			IWebPS.DelLineWebForOperator(routeId);
		}
		catch (Exception myEx)
		{
			AddMessage(new Message(Severity.Error, myEx.Message, ""));
			WriteLog(myEx);
		}
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}
	/// <summary> Вычисляет виндовые координаты точки WGS84 и ее адрес </summary>
	[PageAction("calcpoint")]
	public void CalculatePoint()
	{
		int corrx;
		int corry;
		int wx;
		int wy;
		int scale;
		int.TryParse(GetParamValue("corrx"), out corrx);
		int.TryParse(GetParamValue("corry"), out corry);
		string swx = GetParamValue("wx");
		string swy = GetParamValue("wy");
		int.TryParse(swx, out wx);
		int.TryParse(swy, out wy);
		int.TryParse(GetParamValue("scale"), out scale);

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ".";
		float fKWinToMap;
		float origX2;
		float origY2;
		float.TryParse(GetParamValue("fk"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out fKWinToMap);
		float.TryParse(GetParamValue("origx2"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out origX2);
		float.TryParse(GetParamValue("origy2"), System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out origY2);

		PointF winPoint = new PointF(wx, wy);
		PointF wgsPoint = GetCurrentMapDescription().coordWindowToGeo(winPoint, fKWinToMap, origX2, origY2, corrx, corry);

		SetXmlContentType();

		TemplateFile = "route";
		AddProperty("template", "calculatePoint");

		ResponseSerializationBeg();

		string CurrentAddress = IWebPS.GetAddressByPoint(wgsPoint.Y, wgsPoint.X, CurrentLanguage);

		// <point>
		Writer.WriteStartElement("point");
		Writer.WriteElementString("x", wgsPoint.X.ToString(nfi));
		Writer.WriteElementString("y", wgsPoint.Y.ToString(nfi));
		Writer.WriteElementString("wx", swx);
		Writer.WriteElementString("wy", swy);
		//Writer.WriteElementString("address", CurrentAddress);
		
		//<address>
		Writer.WriteStartElement("address");
		Writer.WriteAttributeString("type", GetParamValue("type"));
		Writer.WriteString(CurrentAddress);
		Writer.WriteEndElement();
		//</address>

		Writer.WriteEndElement();
		// </point>

		ResponseSerializationEnd();
	}
	/// <summary> Преобразует строку, содержащую точки, разделенные запятыми, в массив PointF </summary>
	/// <param name="pointString"> строка с (гео)точками </param>
	/// <returns></returns>
	private PointF[] GetPoints(string pointString)
	{
		string[] points = pointString.Split(new char[1]{','});
		int pointsPairCount = (int)points.Length/2;

		PointF[] pts = new PointF[pointsPairCount];

		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ".";
		int j = 0;
		for (int i = 0; i < pointsPairCount; i++)
		{
			float x;
			float y;
			float.TryParse(points[j++], System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out x);
			float.TryParse(points[j++], System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.Float, nfi, out y);
			pts[i] = new PointF(x, y);
		}

		return pts;
	}
	private void SerializeCommonRouteInformation(PointF begPoint, PointF endPoint, Guid? mapGuid)
	{
		try
		{
			var begAddr = IWebPS.GetAddressByPoint(begPoint.Y, begPoint.X, CurrentLanguage, mapGuid);
			var endAddr = IWebPS.GetAddressByPoint(endPoint.Y, endPoint.X, CurrentLanguage, mapGuid);

			Writer.WriteStartElement("addresses");

			Writer.WriteStartElement("address");
			Writer.WriteAttributeString("type", "from");
			Writer.WriteCData(begAddr);
			Writer.WriteEndElement(); //address

			Writer.WriteStartElement("address");
			Writer.WriteAttributeString("type", "to");
			Writer.WriteCData(endAddr);
			Writer.WriteEndElement(); //address

			Writer.WriteEndElement(); //addresses
		}
		catch (Exception exAddr)
		{
			AddMessage(new Message(Severity.Error, exAddr.Message, ""));
			WriteLog(exAddr);
		}
	}
	/// <summary> Сериализация маршрута </summary>
	/// <param name="points"></param>
	private void SerializeRoutePoints(Dictionary<int, List<PointF>> points, int corrx, int corry, int scale, float fk, PointF begP, PointF endP)
	{
		Writer.WriteStartElement("sections");
		System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
		format.NumberDecimalSeparator = ".";
		float minx = -1;
		float miny = -1;

		float maxx = -1;
		float maxy = -1;
		foreach (int key in points.Keys)
		{
			Writer.WriteStartElement("points");
			foreach (PointF point in points[key])
			{
				Writer.WriteStartElement("point");
				Writer.WriteAttributeString("long", point.X.ToString(format));
				Writer.WriteAttributeString("lat", point.Y.ToString(format));
				Writer.WriteEndElement();

				if (minx == -1 || point.X < minx) minx = point.X;
				if (miny == -1 || point.Y < miny) miny = point.Y;

				if (maxx == -1 || point.X > maxx) maxx = point.X;
				if (maxy == -1 || point.Y > maxy) maxy = point.Y;
			}
			Writer.WriteEndElement(); // points
		}
		Writer.WriteElementString("minLongitude", minx.ToString(format));
		Writer.WriteElementString("minLatitude", miny.ToString(format));

		Writer.WriteElementString("maxLongitude", maxx.ToString(format));
		Writer.WriteElementString("maxLatitude", maxy.ToString(format));

		Writer.WriteEndElement(); //sections
	}
}