﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Exceptions;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Rules : BasePage
	{
		[PageAction(Method = HttpMethod.Get)]
		public void ListAlerts()
		{
			if (!IWebPS.IsRuleServerAllowed)
			{
				AccessIsDenied();
				return;
			}

			TemplateFile = "rules";
			AddProperty("template", "rulesPage");
			var vehicles = GetVehiclesDTO(false);
			var vehicleGroups = GetVehicleGroupsDTO();
			var idType = (IdType)Enum.Parse(typeof(IdType), GetParamValue("objectIdType"));
			var id = int.Parse(GetParamValue("objectId"));
			string objectName;
			switch (idType)
			{
				case IdType.Vehicle:

					BusinessLogic.DTO.Vehicle vehicle;
					if (!vehicles.TryGetValue(id, out vehicle))
					{
						if (!GetVehiclesDTO(false, id).TryGetValue(id, out vehicle))
						{
							Forbidden();
							return;
						}
					}

					objectName = vehicle.Name;
					break;
				case IdType.VehicleGroup:
					var vehicleGroup = vehicleGroups.FirstOrDefault(vg => vg.Id == id);
					if (vehicleGroup == null)
						vehicleGroup = GetVehicleGroupsDTO(new [] { id }).FirstOrDefault();
					if (vehicleGroup == null)
					{
						Forbidden();
						return;
					}
					objectName = vehicleGroup.Name;
					break;
				default:
					throw new NotSupportedException(idType.ToString());
			}

			AddProperty("objectName", objectName);

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[JsonPageAction("sensors", Method = HttpMethod.Get)]
		public Sensor[] GetSensorLegends(IdType objectIdType, int objectId)
		{
			return IWebPS.GetConfiguredSensorLegends(null, objectIdType == IdType.Vehicle ? objectId : (int?) null);
		}
		private Dictionary<string, Operator> GetOperatorsDTO(IdType idType, int id)
		{
			return IWebPS.GetOperators(idType, id).ToDictionary(o => o.id.ToString(CultureInfo.InvariantCulture));
		}
		[JsonPageAction("getRules", Method=HttpMethod.Get)]
		public Rule[] GetRules(IdType objectIdType, int objectId)
		{
			if (!IWebPS.IsRuleServerAllowed)
			{
				AccessIsDenied();
				return null;
			}

			return IWebPS.GetRules(objectIdType, objectId).ToArray();
		}
		[JsonPageAction("remove", Method = HttpMethod.Post)]
		public bool Remove(int id)
		{
			if (!IWebPS.IsRuleServerAllowed)
			{
				AccessIsDenied();
				return false;
			}

			IWebPS.RemoveRule(id);
			return true;
		}
		[PageAction("save", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Rule Save(Rule rule)
		{
			try
			{
				return IWebPS.SaveRule(rule);
			}
			catch (ValidationException e)
			{
				var commonResponse = new CommonResponse<Rule>();
				FillErrorResponse(commonResponse, e);
				var description = JsonHelper.SerializeObjectToJson(commonResponse);
				BadRequest(description);
				return rule;
			}
		}
		[PageAction(Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonResponse<Rule> SaveV2(Rule rule)
		{
			var saveRuleResponse = new CommonResponse<Rule>();
			try
			{
				saveRuleResponse.Item = IWebPS.SaveRule(rule);
			}
			catch (ValidationException ex)
			{
				saveRuleResponse.Item = IWebPS.GetRules(rule.ObjectIdType, rule.ObjectId).FirstOrDefault(r => r.Id == rule.Id);
				FillErrorResponse(saveRuleResponse, ex);
			}

			return saveRuleResponse;
		}
		private static void FillErrorResponse(CommonResponse<Rule> saveRuleResponse, ValidationException ex)
		{
			saveRuleResponse.Result       = ResultCode.Error;
			saveRuleResponse.ErrorType    = ex.Type;
			saveRuleResponse.ErrorValue   = ex.Value;
			saveRuleResponse.ErrorMessage = ex.Message;
		}
	}
}