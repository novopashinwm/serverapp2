﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using Interfaces.Geo;
using RU.NVG.UI.WebMapG.Model.Common;
using RU.NVG.UI.WebMapG.Model.Common.Types;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Map : BasePage
	{
		/// <summary> Отображение всей страницы - начало загрузки </summary>
		[PageAction]
		public void ShowPage()
		{
			TemplateFile = "map_G";
			AddProperty("template", "mapPageFull");
			if (Request.Cookies["onlinehrs"] != null)
			{
				AddProperty("onlinehrs", Request.Cookies["onlinehrs"].Value);
			}
			string showMode = GetParamValue("show");
			if (string.IsNullOrEmpty(showMode))
				showMode = "all";

			AddProperty("showMode", showMode);

			if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["vehicleProfileExtUrl"]))
				AddProperty("vehicleProfileExtUrl", ConfigurationManager.AppSettings["vehicleProfileExtUrl"]);
			if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["vehicleStatusImageExtUrl"]))
				AddProperty("vehicleStatusImageExtUrl", ConfigurationManager.AppSettings["vehicleStatusImageExtUrl"]);
			if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["callProfileExtUrl"]))
				AddProperty("callProfileExtUrl", ConfigurationManager.AppSettings["callProfileExtUrl"]);

			dontCacheMe();

			InsertDOCTypeDeclaration(true);
			ResponseSerializationBeg();

			SerializeMapDescriptions();
			ResponseSerializationEnd();
		}
		/// <summary> Продление сессии </summary>
		[JsonPageAction("isalive")]
		[JsonPageAction("isalive", HttpMethod.Post)]
		public bool IsAlive()
		{
			try
			{
				if (!IsAuthorized)
					return false;

				var server = IWebPS;

				var localTimeBefore = DateTime.UtcNow;
				var serverTime = server.GetServerDateTime();
				var localTimeAfter = DateTime.UtcNow;
				if (TimeSpan.FromMilliseconds(500) < localTimeAfter - localTimeBefore)
				{
					System.Diagnostics.Trace.TraceWarning(
						"Something is slow: web->business has taken " + (serverTime - localTimeBefore) +
						" and business->web " + (localTimeAfter - serverTime));
				}

				return server.IsAlive();
			}
			catch (Exception exception)
			{
				System.Diagnostics.Trace.TraceError(exception.ToString());
				return false;
			}
		}
		/// <summary> Добавление пользовательской геоточки </summary>
		[PageAction("addGeoPoint")]
		public void AddGeoPoint()
		{
			SetJsonContentType();

			if (IsAuthorized)
			{
				var paramName = GetParamValue("name").Trim();
				var paramDesc = GetParamValue("description").Trim();
				var paramLng  = GetParamValue("lng");
				var paramLat  = GetParamValue("lat");

				try
				{
					var dtoWebPoint = IWebPS.WebPointCreate(new PointOfInterest
					{
						Name = paramName,
						Desc = paramDesc,
						Lng  = float.Parse(paramLng, CultureInfo.InvariantCulture),
						Lat  = float.Parse(paramLat, CultureInfo.InvariantCulture),
					}.ToWebPoint());
					response.Write(JsonHelper.SerializeObjectToJson("OK"));
				}
				catch (Exception ex)
				{
					WriteLog(ex);
					AddMessage(new Message(Severity.Normal, GetMessageData("error"), ex.Message));
				}
			}
			else
			{
				AddMessage(new Message(Severity.Normal, GetMessageData("error"), GetMessageData("error")));
			}
		}
		/// <summary> Редактирование пользовательской геоточки </summary>
		[PageAction("editGeoPoint")]
		public void EditGeoPoint()
		{
			SetJsonContentType();

			if (IsAuthorized)
			{
				var paramId   = GetParamValue("id").Trim();
				var paramName = GetParamValue("name").Trim();
				var paramDesc = GetParamValue("description").Trim();
				var paramLng  = GetParamValue("lng");
				var paramLat  = GetParamValue("lat");

				try
				{
					var dtoWebPoint = IWebPS.WebPointUpdate(new PointOfInterest
					{
						Id   = int.Parse(paramId, CultureInfo.InvariantCulture),
						Name = paramName,
						Desc = paramDesc,
						Lng  = float.Parse(paramLng, CultureInfo.InvariantCulture),
						Lat  = float.Parse(paramLat, CultureInfo.InvariantCulture),
					}.ToWebPoint());
					response.Write(JsonHelper.SerializeObjectToJson("OK"));
				}
				catch (Exception ex)
				{
					WriteLog(ex);
					AddMessage(new Message(Severity.Normal, GetMessageData("error"), ex.Message));
				}
			}
			else
			{
				AddMessage(new Message(Severity.Normal, GetMessageData("error"), GetMessageData("error")));
			}
		}

		/// <summary> Удаление пользовательской геоточки </summary>
		[PageAction("deleteGeoPoint")]
		public void deleteGeoPoint()
		{
			SetJsonContentType();

			if (IsAuthorized)
			{
				var paramId = GetParamValue("pointid");
				try
				{
					IWebPS.WebPointDelete(int.Parse(paramId, CultureInfo.InvariantCulture));
					response.Write(JsonHelper.SerializeObjectToJson("OK"));
				}
				catch (Exception ex)
				{
					WriteLog(ex);
					AddMessage(new Message(Severity.Normal, GetMessageData("error"), ex.Message));
				}
			}
			else
			{
				AddMessage(new Message(Severity.Normal, GetMessageData("error"), GetMessageData("error")));
			}
		}
		[PageAction("getgeopointdescription")]
		[Obsolete("Уже не используется, можно убрать")]
		public void GetGeopointDescription()
		{
			TemplateFile = "geopoint";
			AddProperty("template", "getGeopointDescription");

			int pointId;
			int.TryParse(GetParamValue("pointid"), out pointId);

			ResponseSerializationBeg();
			if (IsAuthorized)
			{
				try
				{
					List<geopoint> geopoints = (List<geopoint>)GetObjectFromSession("geopoints");
					if (geopoints != null)
					{
						foreach (geopoint pt in geopoints)
						{
							if (pt.Id == pointId)
							{
								pt.WriteXml(Writer);
								break;
							}
						}
					}
				}
				catch (Exception ex)
				{
					WriteLog(ex);
					AddMessage(new Message(Severity.Normal, GetMessageData("error"), ex.Message));
				}
			}
			else
			{
				AddMessage(new Message(Severity.Normal, GetMessageData("error"), GetMessageData("error")));
			}
			ResponseSerializationEnd();
		}
		/// <summary>Метод возвращает все геоточки (точки интереса) </summary>
		[PageAction("getGeoPoints")]
		public void GetGeoPoints()
		{
			// AS: здесь так же все прогонится через XSL
			SetXmlContentType();
			TemplateFile = "geopoint";
			AddProperty("template", "getGeopoints");

			// AS: генерить список нужно только в первую загрузку данных. При изменении масштаба - не нужно
			AddProperty("renderList", GetParamValue("renderlist"));

			var userPoints = IWebPS.GetPointsWebForOperator();

			userPoints.DataSetName = "userPoints";
			AddProperty("json", JsonHelper.SerializeObjectToJson(userPoints));

			ResponseSerializationBeg();
			userPoints.WriteXml(Writer);
			SerializeMapDescriptions();
			ResponseSerializationEnd();
		}
		#region WebApi Poi New
		/// <summary> Метод возвращает точки интереса </summary>
		[PageAction("get-poi-list", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<PointOfInterest> GetPointOfInterestList()
		{
			return IWebPS.GetPointsWeb()
				.Select(p => PointOfInterest.FromWebPoint(p))
				.ToList();
		}
		[PageAction("poi-read", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<PointOfInterest> PointOfInterestRead(int id)
		{
			var response = new CommonActionResponse<PointOfInterest>();
			try
			{
				response.Object = PointOfInterest.FromWebPoint(IWebPS
					.WebPointRead(id));
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[PageAction("poi-create", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<PointOfInterest> PointOfInterestCreate(PointOfInterest obj)
		{
			var response = new CommonActionResponse<PointOfInterest>();
			try
			{
				response.Object = PointOfInterest.FromWebPoint(IWebPS
					.WebPointCreate(obj.ToWebPoint()));
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[PageAction("poi-update", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<PointOfInterest> PointOfInterestUpdate(PointOfInterest obj)
		{
			var response = new CommonActionResponse<PointOfInterest>();
			try
			{
				response.Object = PointOfInterest.FromWebPoint(IWebPS
					.WebPointUpdate(obj.ToWebPoint()));
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[PageAction("poi-delete", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommonActionResponse<bool?> PointOfInterestDelete(int id)
		{
			var response = new CommonActionResponse<bool?> { Object = null };
			try
			{
				IWebPS
					.WebPointDelete(id);
				response.Object = true;
			}
			catch (Exception ex)
			{
				response.Object = false;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		#endregion WebApi Poi New
		/// <summary> Возвращает адрес по координатам </summary>
		[PageAction("getlastaddr")]
		public void GetLastAddress()
		{
			var lngPar = GetParamValue("longitude");
			var latPar = GetParamValue("latitude");
			var lngVal = default(float);
			var latVal = default(float);
			var nfi = new NumberFormatInfo { NumberDecimalSeparator = "." };
			float.TryParse(lngPar, NumberStyles.AllowDecimalPoint | NumberStyles.Float, nfi, out lngVal);
			float.TryParse(latPar, NumberStyles.AllowDecimalPoint | NumberStyles.Float, nfi, out latVal);

			ResponseSerializationBeg();
			SerializeAddress(lngVal, latVal, MapGuid);
			ResponseSerializationEnd();
		}

		/// <summary> Пересчет оконных координат в WGS84 </summary>
		[PageAction("wtogeo")]
		public void WindowToGeo()
		{
			int corrx;
			int corry;
			int wx;
			int wy;
			int scale;
			int.TryParse(GetParamValue("corrx"), out corrx);
			int.TryParse(GetParamValue("corry"), out corry);
			int.TryParse(GetParamValue("wx"), out wx);
			int.TryParse(GetParamValue("wy"), out wy);
			int.TryParse(GetParamValue("scale"), out scale);

			NumberFormatInfo nfi = new NumberFormatInfo();
			nfi.NumberDecimalSeparator = ".";
			float fKWinToMap;
			float origX2;
			float origY2;
			float.TryParse(GetParamValue("fk"),     NumberStyles.AllowDecimalPoint | NumberStyles.Float, nfi, out fKWinToMap);
			float.TryParse(GetParamValue("origx2"), NumberStyles.AllowDecimalPoint | NumberStyles.Float, nfi, out origX2);
			float.TryParse(GetParamValue("origy2"), NumberStyles.AllowDecimalPoint | NumberStyles.Float, nfi, out origY2);

			PointF winPoint = new PointF(wx, wy);
			PointF wgsPoint = GetCurrentMapDescription().coordWindowToGeo(winPoint, fKWinToMap, origX2, origY2, corrx, corry);

			SetXmlContentType();

			ResponseSerializationBeg();
		
			// <point>
			Writer.WriteStartElement("point");
			Writer.WriteElementString("x", wgsPoint.X.ToString(nfi));
			Writer.WriteElementString("y", wgsPoint.Y.ToString(nfi));
			Writer.WriteEndElement();
			// </point>

			ResponseSerializationEnd();
		}

		[PageAction("getVehicleGroups", RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<BusinessLogic.DTO.Group> GetVehicleGroups(bool includeIgnored = false)
		{
			return IWebPS.GetVehicleGroups(new GetGroupsArguments { IncludeIgnored = includeIgnored });
		}

		/// <summary> Получить оценочные времена прибытия в указанную точку объектов наблюдения </summary>
		/// <param name="lat"> Широта указанной точки </param>
		/// <param name="lng"> Долгота указанной точки </param>
		/// <param name="vehicleIds"> Идентификаторы объектов наблюдения, если null, то все </param>
		/// <param name="mapGuid"> Идентификатор карты </param>
		/// <returns></returns>
		[PageAction("getarrivaltimestopoint",     Method = HttpMethod.Get,  RequestType = DataType.Json, ResponseType = DataType.Json)]
		[PageAction("get-arrival-times-to-point", Method = HttpMethod.Get,  RequestType = DataType.Json, ResponseType = DataType.Json)]
		[PageAction("get-arrival-times-to-point", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public ArrivalTimesToPoint GetArrivalTimesToPoint(decimal lat, decimal lng, int[] vehicleIds = null, Guid? mapGuid = null)
		{
			var timeOffsetMinute = 5;
			var arguments = new GetVehiclesArgs
			{
				IncludeZones      = false,
				IncludeAddresses  = false,
				VehicleIDs        = vehicleIds,
				IncludeLastData   = true,
				IncludeAttributes = true,
				//MapGuid           = mapGuid ?? MapGuid,
				MapGuid           = MapGuid,
				OnlineFilter      = null,
				ObjectsFilter     = VehicleObjectsFilter.Active,
				OnlineTime        = GetOnlineTime()
			};
			// Выбираем объекты наблюдения у которых есть свежие координаты.
			var vehicles = IWebPS.GetVehicles(arguments)
				.Where(v =>
					v.lat.HasValue                                                           &&
					v.lng.HasValue                                                           &&
					v.PositionType == PositionType.GPS                                       &&
					v.posLogTime > DateTime.UtcNow.AddMinutes(-timeOffsetMinute).ToLogTime() &&
					v.rights.Contains(SystemRight.EstimatedTimeArrival))
				// Рассчитываем расстояние от указанной точки до объекта
				.Select(v => new { v, d = GeoHelper.DistanceBetweenLocations((double)lat, (double)lng, v.lat.Value, v.lng.Value) })
				// Фильтруем объекты находящиеся более чем в 100км от указанной точки
				//.Where(v => 100 * 1000 > v.d)
				// Сортируем объекты по расстоянию от указанной точки
				.OrderBy(v =>v.d)
				// Берем 10 ближайших объектов
				.Take(10)
				// Выбираем только объекты без расстояния
				.Select(v => v.v);
			return IWebPS.GetArrivalTimesToPoint(
				new LatLng { Lat = lat, Lng = lng },
				vehicles
					.Select(v =>
						new ArrivalTimeForObject
						{
							ObjectId = v.id,
							ObjectLatLng =
								new LatLng
								{
									Lat = (decimal)v.lat.Value,
									Lng = (decimal)v.lng.Value
								}
						})
					.ToArray(),
				// Подставляем текущий язык пользователя
				CurrentLanguage);
		}
		/// <summary> Получить оценочные времена прибытия в указанную точку объектов наблюдения, без учета машин находящихся внутри геозоны </summary>
		/// <param name="lat"> Широта указанной точки </param>
		/// <param name="lng"> Долгота указанной точки </param>
		/// <param name="zoneId"> Идентификатор геозоны </param>
		/// <param name="mapGuid"> Идентификатор карты </param>
		/// <returns></returns>
		[PageAction("get-arrival-times-to-zone-point", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public ArrivalTimesToPoint GetArrivalTimesToZonePoint(decimal lat, decimal lng, int zoneId, Guid? mapGuid = null)
		{
			var timeOffsetMinute = 10;
			var arguments = new GetVehiclesArgs
			{
				IncludeZones      = true,
				IncludeAddresses  = false,
				VehicleIDs        = null,
				IncludeLastData   = true,
				IncludeAttributes = true,
				//MapGuid           = mapGuid ?? MapGuid,
				MapGuid           = MapGuid,
				OnlineFilter      = null,
				ObjectsFilter     = VehicleObjectsFilter.Active,
				OnlineTime        = GetOnlineTime()
			};
			// Выбираем объекты наблюдения у которых есть свежие координаты.
			var vehicles = IWebPS.GetVehicles(arguments)
				.Where(v =>
					v.lat.HasValue &&
					v.lng.HasValue &&
					v.PositionType == PositionType.GPS &&
					v.posLogTime > DateTime.UtcNow.AddMinutes(-timeOffsetMinute).ToLogTime() &&
					v.rights.Contains(SystemRight.EstimatedTimeArrival) &&
					!v.ZoneIds.Contains(zoneId))
				// Рассчитываем расстояние от указанной точки до объекта
				.Select(v => new { v, d = GeoHelper.DistanceBetweenLocations((double)lat, (double)lng, v.lat.Value, v.lng.Value) })
				// Фильтруем объекты находящиеся более чем в 100км от указанной точки
				//.Where(v => 100 * 1000 > v.d)
				// Сортируем объекты по расстоянию от указанной точки
				.OrderBy(v => v.d)
				// Берем 10 ближайших объектов
				.Take(10)
				// Выбираем только объекты без расстояния
				.Select(v => v.v);
			return IWebPS.GetArrivalTimesToPoint(
				new LatLng { Lat = lat, Lng = lng },
				vehicles
					.Select(v =>
						new ArrivalTimeForObject
						{
							ObjectId     = v.id,
							ObjectLatLng =
								new LatLng
								{
									Lat = (decimal)v.lat.Value,
									Lng = (decimal)v.lng.Value
								}
						})
					.ToArray(),
				// Подставляем текущий язык пользователя
				CurrentLanguage);
		}
	}
}