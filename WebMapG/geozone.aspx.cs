﻿using System;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using Interfaces.Web.Enums;
using RU.NVG.UI.WebMapG.Model.GeoZone.Response;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class GeozonePage : BasePage
	{
		//[PageAction("default")]
		public void ShowGeozoneEditor()
		{
			dontCacheMe();
			TemplateFile = "geozone";
			AddProperty("template", "editorPage");
			ResponseSerializationBeg();
			SerializeMapDescriptions();
			ResponseSerializationEnd();
		}
		[JsonPageAction(Method = HttpMethod.Get)]
		public GetZoneObjectsResponse GetZones(bool includeIgnored = false)
		{
			var geoZones = GetGeoZonesDTO(includeIgnored: includeIgnored);
			var geozoneGroups = GetGeoZoneGroupsDTO(includeIgnored);
			return new GetZoneObjectsResponse(geoZones, geozoneGroups);
		}
		[JsonPageAction("zones-and-groups", Method = HttpMethod.Get)]
		public GetZoneObjectsResponse2 GetZonesAndGroups(bool includeVertices = true, bool includeIgnored = false)
		{
			var geoZones = IWebPS.GetGeoZones(includeVertices, includeIgnored: includeIgnored).ToArray();
			var geozoneGroups = GetGeoZoneGroupsDTO(includeIgnored);
			return new GetZoneObjectsResponse2(geoZones, geozoneGroups);
		}
		/// <summary> Сохранить геозону </summary>
		/// <param name="zone"> Геозона </param>
		/// <returns></returns>
		[JsonPageAction(Method = HttpMethod.Post)]
		public SaveZoneResponse SaveZone(GeoZone zone)
		{
			var zoneId = zone.Id;
			SaveZoneResult result;
			try
			{
				result = zone.Id > 0
					? IWebPS.EdtZoneWebForOperator(zone)
					: IWebPS.AddZoneWebForOperator(zone, out zoneId);
			}
			catch (Exception ex)
			{
				string.Empty
					.WithException(ex, true)
					.CallTraceError();
				result = SaveZoneResult.UnknownError;
			}

			var saveZoneResponse = new SaveZoneResponse
			{
				Result = result
			};

			if (zoneId > 0)
			{
				saveZoneResponse.Zone = GetGeoZoneDTO(zoneId, includeIgnored: true);
			}

			return saveZoneResponse;
		}
		/// <summary> Получить доступные цвета для геозон </summary>
		/// <returns></returns>
		[JsonPageAction("get-colors", Method = HttpMethod.Get)]
		public Dictionary<string, string> GetZoneColors()
		{
			return new Dictionary<string, string>
			{
				{"black",  "#000"   },
				{"red",    "#ff0000"},
				{"green",  "#00ff00"},
				{"blue",   "#0000ff"},
				{"yellow", "#ffff00"}
			};
		}
		[JsonPageAction("remove", Method = HttpMethod.Post)]
		public void Remove(int id)
		{
			IWebPS.DelZoneWebForOperator(id);
		}
		[JsonPageAction("createZoneGroup", Method = HttpMethod.Post)]
		public BusinessLogic.DTO.Group CreateZoneGroup(string groupName)
		{
			return IWebPS.ZoneGroupCreate(groupName ?? string.Empty);
		}
		[JsonPageAction("updatezonegoup", Method = HttpMethod.Post)]
		public BusinessLogic.DTO.Group UpdateZonegroup(int id, string name)
		{
			return IWebPS.ZoneGroupUpdate(name ?? string.Empty, id);
		}
		[JsonPageAction("deletezonegroup", Method = HttpMethod.Post)]
		public void DeleteZonegroup(int id)
		{
			IWebPS.ZoneGroupDelete(id);
		}
		[JsonPageAction("removeFromGroup", Method = HttpMethod.Post)]
		public void RemoveFromGroup(int zoneId, int groupId)
		{
			IWebPS.ZoneGroupExcludeZone(groupId, zoneId);
		}
		[JsonPageAction("addToGroup", Method = HttpMethod.Post)]
		public void AddToGroup(int zoneId, int groupId)
		{
			IWebPS.ZoneGroupIncludeZone(groupId, zoneId);
		}
		/// <summary> Задать норматив </summary>
		[PageAction("updateDistanceStandart")]
		public void UpdateDistanceStandart()
		{
			int zoneIDFrom;
			int.TryParse(GetParamValue("zone_id_from"), out zoneIDFrom);
			int zoneIDTo;
			int.TryParse(GetParamValue("zone_id_to"), out zoneIDTo);


			decimal distance;
			decimal.TryParse(
				GetParamValue("distance"),
				NumberStyles.AllowDecimalPoint,
				CultureInfo.InvariantCulture,
				out distance);

			try
			{
				IWebPS.UpdateZoneDistanceStandart(zoneIDFrom, zoneIDTo, Convert.ToInt32(distance * 1000));
				Response.Write("OK");
			}
			catch (Exception myEx)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), myEx.Message));
				//WriteLog(myEx);
				Response.Write(myEx.Message);
				//throw;
			}
		}
	}
}