﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.UI.WebMapG.message
{
	public partial class Default : BasePage
	{
		[JsonPageAction("default", Method = HttpMethod.Get)]
		public List<Message> Get(DateTime? from, DateTime? to)
		{
			return IWebPS.GetMessages(null, from, to);
		}
	}
}