﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG
{
	public partial class Register : BaseLoginPage
	{
		private const string NewTrackerActionKey = "new-tracker";
		[PageAction]
		public void StartRegistration()
		{
			TemplateFile = "register";
			AddProperty("template", "start");
			AddProperty("title", GetMessageData("Registration"));
			AddProperty("email", GetParamValue("email"));
			RenderPromo();
		}
		private string GetConfirmationKey()
		{
			return GetParamValue("confirmation-key");
		}
		[PageAction(NewTrackerActionKey)]
		public void NewTracker()
		{
			var confirmationKey = GetConfirmationKey();
			var email = WebLoginProvider.GetEmailByConfirmationKey(confirmationKey);

			if (email == null)
			{
				AddMessage(
					new Message
					{
						Severity = Severity.Error,
						MessageBody = GetMessageData("MessageForInvalidConfirmationKey")
					});
				StartRegistration();
				return;
			}

			TemplateFile = "register";
			AddProperty("template", "new-tracker-adding");
			AddProperty("title", GetMessageData("NewTrackerAdding"));
			AddProperty("confirmation-key", confirmationKey);
			AddProperty("email", email);
			AddProperty("control-code", GetParamValue("control-code"));
			AddProperty("number", GetParamValue("number"));

			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[PageAction("send-confirmation-code", RequestType = DataType.Json, ResponseType = DataType.Json, Method = HttpMethod.Post)]
		public SendConfirmationCodeResult SendConfirmationCode(ContactType type, string value)
		{
			return ServerApplication.Server.Instance().SendConfirmationCode(type, value, CurrentCulture, Request.UserHostAddress);
		}
		[PageAction("verify-contact", RequestType = DataType.Json, ResponseType = DataType.Json, Method = HttpMethod.Post)]
		public VerifyContactResult VerifyContact(ContactType type, string value, string code)
		{
			return ServerApplication.Server.Instance().VerifyContact(type, value, code, Request.UserHostAddress);
		}
	}
}