﻿using System;
using System.Linq;
using System.Collections.Generic;
using Compass.Ufin.Movireg.Provider;
using Compass.Ufin.Movireg.Provider.Models;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using RU.NVG.UI.WebMapG.Model.Common;
using System.Runtime.Serialization;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class Movireg : BaseRestfulPage
	{
		[Serializable]
		[DataContract]
		public class MoviregVihecle : QueryUserVehicleVehicle
		{
			[DataMember] public string MoviregSession          { get; set;  }
			[DataMember] public string MoviregServerUrl        { get; set; }
			[DataMember] public int    MoviregServerTimeOffset { get; set; }
		}

		[JsonPageAction("get-movireg-vehicles", Method = HttpMethod.Get)]
		public CommonActionResponse<MoviregVihecle[]> GetMoviregVehicles()
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}'".CallTraceMessage()}");

			var response = new CommonActionResponse<MoviregVihecle[]>();
			try
			{
				using var entities = new Entities();
				var moviregAccounts = entities.DEPARTMENT_MOVIREG_ACCOUNT
					.Where(t => t.DEPARTMENT_ID == DepartmentId)
					.ToArray();

				var vehicles = new List<MoviregVihecle>();
				foreach (var account in moviregAccounts)
				{
					var provider = new MoviregApiProvider(
						account.MOVIREG_ACCOUNT,
						account.MOVIREG_PASSWORD,
						account.MOVIREG_SERVER_URL);
					if (provider.IsLoggedIn || provider.StandardApiAction_login())
					{
						var quvr = provider.StandardApiAction_queryUserVehicle();
						if (quvr.result == 0)
						{
							foreach (var vehicleItem in quvr.vehicles)
							{
								var dst = JsonHelper2.Convert<QueryUserVehicleVehicle, MoviregVihecle>(vehicleItem);
								dst.MoviregSession          = provider.Session;
								dst.MoviregServerUrl        = account.MOVIREG_SERVER_URL;
								dst.MoviregServerTimeOffset = account.MOVIREG_SERVER_TIMEOFFSET;
								vehicles.Add(dst);
							}
						}
					}
					// Logout не делаем, т.к. сессию отправили за пределы сервера
				}

				response.Object = vehicles.ToArray();
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
	}
}