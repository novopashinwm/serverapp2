﻿using System;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using RU.NVG.UI.WebMapG.Model.Common;
using RU.NVG.UI.WebMapG.Model.Messages;
using RU.NVG.UI.WebMapG.Model.Services.Messages;
using RU.NVG.UI.WebMapG.Push;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class Messages : BaseRestfulPage
	{
		[JsonPageAction("get-messages", Method = HttpMethod.Get)]
		public GetListResponse<MessageFormat> GetMessages(DateTime? from, DateTime? to, int? lastMessageId, int? page, int? recordsPerPage)
		{
			var pagination = recordsPerPage.HasValue
				? new Pagination
				{
					Page           = page ?? 1,
					RecordsPerPage = recordsPerPage.Value
				} : null;
			return new GetListResponse<MessageFormat>
			{
				Items = IWebPS.GetMessagesForAppClient(lastMessageId, from, to, pagination)
					.Select(m => new MessageFormat(m))
					.ToArray(),
				Pagination = pagination
			};
		}
		[JsonPageAction("get-messages-web", Method = HttpMethod.Get )]
		[JsonPageAction("get-messages-web", Method = HttpMethod.Post)]
		public GetListResponse<MessageFormat> GetMessages(DateTime? from, DateTime? to, int? lastMessageId, int? page, int? recordsPerPage, int[] vehicleIds)
		{
			var pagination = recordsPerPage.HasValue
				? new Pagination
				{
					Page           = page ?? 1,
					RecordsPerPage = recordsPerPage.Value
				} : null;
			return new GetListResponse<MessageFormat>
			{
				Items = IWebPS.GetMessagesForOperator(lastMessageId, from, to, pagination, vehicleIds)
					.Select(m => new MessageFormat(m))
					.ToArray(),
				Pagination = pagination
			};
		}
		/// <summary> Используется для получения начального списка сообщений и начала мониторинга изменений сообщений по конкретному номеру </summary>
		/// <param name="msisdn">Номер телефона, для которого происходит поиск сообщений.</param>
		/// <param name="page">Номер страницы.</param>
		/// <param name="recordsPerPage">Количество сообщений на одной странице.</param>
		/// <param name="messageId">Идентификатор сообщения первого сообщения на первой странице.</param>
		/// <returns></returns>
		[JsonPageAction("get-messages-to-msisdn", Method = HttpMethod.Get)]
		public GetListWithHistoryResponse<MessageLocale> GetMessages(string msisdn, int? messageId, int? page, int? recordsPerPage)
		{
			var pagination = recordsPerPage.HasValue
				? new Pagination { Page = page ?? 1, RecordsPerPage = recordsPerPage.Value }
				: null;

			var result = IWebPS.GetShortMessagesByMsisdn(msisdn, messageId, pagination);
			// Начинаем следить за обновлениями сообщений для контакта.
			var getMessageResponse = new GetListWithHistoryResponse<MessageLocale>
			{
				Items      = result.Items
					.Select(item => new MessageLocale(item, IWebPS.CultureInfo))
					.ToArray(),
				Pagination = pagination,
				HistoryId  = result.HistoryId
			};
			return getMessageResponse;
		}
		[JsonPageAction("get-short-messages-by-vehicle", Method = HttpMethod.Get)]
		public GetListWithHistoryResponse<MessageLocale> GetShortMessagesByVehicle(int vehicleId, int? messageId, int? page, int? recordsPerPage)
		{
			var pagination = recordsPerPage.HasValue
				? new Pagination { Page = page ?? 1, RecordsPerPage = recordsPerPage.Value }
				: null;

			var result = IWebPS.GetShortMessagesByVehicle(vehicleId, messageId, pagination);
			// Начинаем следить за обновлениями сообщений для контакта.
			var getMessageResponse = new GetListWithHistoryResponse<MessageLocale>
			{
				Items      = result.Items
					.Select(item => new MessageLocale(item, IWebPS.CultureInfo))
					.ToArray(),
				Pagination = pagination,
				HistoryId  = result.HistoryId
			};
			return getMessageResponse;

		}
		[JsonPageAction("send-configuration-short-message-to-vehicle", Method = HttpMethod.Post)]
		public void SendConfigurationSmsToVehicle(int vehicleId, string message)
		{
			IWebPS.SendConfigurationSmsToVehicle(vehicleId, message);
		}
		/// <summary> Отправляет смс с текстом на введенный номер </summary>
		/// <param name="msisdn">Номер телефона на который отправляется сообщение.</param>
		/// <param name="source">Телефон с которого отправляется сообщение.</param>
		/// <param name="body">Текст сообщения.</param>
		/// <returns></returns>
		[JsonPageAction("send-message-msisdn", Method = HttpMethod.Post)]
		public void SendMessage(string msisdn, string source, string body)
		{
			IWebPS.SendSms(msisdn, source, body);
		}
		private static MessageProcessingResult ToMessageProcessingResult(MessageStatus status)
		{
			switch (status)
			{
				case MessageStatus.Read:
					return MessageProcessingResult.Read;
				case MessageStatus.Received:
					return MessageProcessingResult.Delivered;
				default:
					throw new ArgumentOutOfRangeException("status", status, @"Value is not supported yet");
			}
		}
		[JsonPageAction("update-messages-status")]
		[JsonPageAction("update-messages-status", Method = HttpMethod.Post)]
		public UpdateMessageStatusResponse UpdateMessageStatus(int[] messageIds, MessageStatus status, string appId)
		{
			SysDiags::Trace.TraceInformation(
				"MessageStatus {0} from {1}: {2}",
				status, appId, messageIds.Join(","));

			var messageProcessingResult = ToMessageProcessingResult(status);

			int[] items = null;
			if (messageIds != null)
			{
				if (string.IsNullOrWhiteSpace(appId))
					items = IWebPS.UpdateMessageStatus(messageIds, messageProcessingResult);
				else
					items =
						ServerApplication.Server.Instance()
							.SetMessageProcessingResult(appId, messageProcessingResult, messageIds)
							.ToArray();
			}

			var updateMessageStatusResponse = new UpdateMessageStatusResponse
			{
				Items = items
			};

			return updateMessageStatusResponse;
		}
		/// <summary> Получения token для авторизации через SignalR </summary>
		/// <returns></returns>
		[JsonPageAction("get-token", Method = HttpMethod.Get)]
		public GetTokenResponse GetToken()
		{
			if (!IsAuthorized)
				throw new SecurityException("Not allowed");

			var userToken = Guid.NewGuid().ToString("N");
			MessagesHubSecurity.Instance.Register(userToken, new TokenRegistrationInfo
			{
				SessionId    = SessionId,
				CultureInfo  = CurrentCulture,
				OperatorId   = OperatorId.Value,
			});
			var getTokenResponse = new GetTokenResponse
			{
				Token = userToken
			};

			return getTokenResponse;
		}
		[JsonPageAction("wrong-message-destination", Method = HttpMethod.Post)]
		public void WrongMessageDestination(int messageId, string wrongAppId)
		{
			SysDiags::Trace.TraceInformation(
				"WrongMessageDestination({0}, {1}", messageId, wrongAppId);

			IWebPS.WrongMessageDestination(messageId, wrongAppId);
		}
	}
}