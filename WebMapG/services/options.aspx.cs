﻿using System.Xml;
using FORIS.TSS.UI.WebMapG;

public partial class services_options : BasePage
{
	/// <summary> Страница настроек </summary>
	[PageAction("default")]
	public void ShowPage()
	{
		RenderOptionsForm();
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}

	private void RenderOptionsForm()
	{
		TemplateFile = "options";
		AddProperty("template", "userSettings");
		AddProperty("title", GetMessageData("UserSettings"));
		var allowSmsSend = IWebPS.AllowSmsSend;
		AddProperty("allowUserSmsSend", XmlConvert.ToString(allowSmsSend));
		if (Request.Cookies["onlinehrs"] != null)
		{
			AddProperty("onlinehrs", Request.Cookies["onlinehrs"].Value);
		}
		AddProperty("ForcedToChangePassword", XmlConvert.ToString(ForcedToChangePassword));
	}
}