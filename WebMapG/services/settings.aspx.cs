﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.UI.WebMapG.Old_App_Code;
using Interfaces.Web.Enums;
using Jayrock.Json;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class ServicesSettings : BasePage
	{
		/// <summary> Страница настроек </summary>
		[PageAction]
		public void ShowPage()
		{
			if (ForcedToChangePassword)
			{
				AddMessage(new Message
				{
					Severity    = Severity.Warning,
					MessageBody = GetMessageData("ChangePasswordReminder")
				});
			}

			AddProperty("template", "userSettings");
			RenderSettingsForm();
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		[PageAction("saveuserchanges")]
		public void SaveChanges()
		{
			ChangePassword();
			ChangeLogin();
			RenderSettingsForm();
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
		private void ChangeLogin()
		{
			var newLogin = GetParamValue("newlogin");
			if (newLogin != null)
			{
				newLogin = newLogin.Trim();
				if (!string.IsNullOrEmpty(newLogin))
				{
					try
					{
						ChangeLogin(newLogin);
					}
					catch (Exception ex)
					{
						WriteLog(ex);
						AddMessage(new Message(Severity.Error, GetMessageData("error"), ex.Message));
					}
				}
			}
		}
		private void ChangeLogin(string login)
		{
			switch (IWebPS.SetNewLogin(login))
			{
				case SetNewLoginResult.Success:
					Logout();
					break;
				case SetNewLoginResult.InvalidLogin:
					AddMessage(new Message
						{
							MessageTitle = GetMessageData("LoginChange"),
							MessageBody = string.Format(GetMessageData("InvalidLogin"), login)
						});
					break;
				case SetNewLoginResult.LoginAlreadyExists:
					AddMessage(new Message
						{
							MessageTitle = GetMessageData("LoginChange"),
							MessageBody = string.Format(GetMessageData("LoginAlreadyExists"), login)
						});
					break;
				case SetNewLoginResult.LoginAsEmailDoesNotMatchToEmail:
					AddMessage(new Message
						{
							MessageTitle = GetMessageData("LoginChange"),
							MessageBody = string.Format(GetMessageData("LoginAsEmailDoesNotMatchToEmail"), login)
						});
					break;
				case SetNewLoginResult.Forbidden:
					AddMessage(new Message
						{
							MessageTitle = GetMessageData("LoginChange"),
							MessageBody = GetMessageData("LoginChangeForbidden")
						});
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		/// <summary> Смена пароля пользователя </summary>
		/// <param name="oldPass"> Старый пароль </param>
		/// <param name="newPass1"> Новый пароль </param>
		/// <param name="newPass2"> Новый пароль-подтверждение </param>
		private bool ChangePassword(string oldPass, string newPass1, string newPass2)
		{
			if (string.IsNullOrWhiteSpace(oldPass) && !ForcedToChangePassword)
				return false;

			if (string.IsNullOrWhiteSpace(newPass1) ||
				string.IsNullOrWhiteSpace(newPass2))
				return false;

			bool ok = true;

			if (!CanChangePasswords(oldPass, newPass1, newPass2))
				return false;

			if (!IsAuthorized)
			{
				var msg = new Message(Severity.Error, GetMessageData("ServerError"), GetMessageData("PasswordCantBeChanged"));
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
					return false;
				}
				AddMessage(msg);
				return false;
			}

			SetNewPasswordResult? result = null;
			try
			{
				result = IWebPS.SetPassword(newPass1);
			}
			catch (Exception e)
			{
				SysDiags::Trace.TraceError("{0}", e);
				ok = false;
				var msg = new Message(Severity.Error, GetMessageData("ServerError"), GetMessageData("PasswordCantBeChanged"));
				AddMessage(msg);
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
					return false;
				}
			}
			if (result == SetNewPasswordResult.NoConfirmedContact)
			{
				var msg = new Message(Severity.Error, GetMessageData("PasswordCantBeChanged"), GetMessageData("NoConfirmedContact"));
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
					return false;
				}
				AddMessage(msg);
				return false;
			}

			if (!ok)
				return false;

			AddMessage(new Message(Severity.Normal, GetMessageData("PasswordChangedSuccessfuly"), GetMessageData("PasswordChangedSuccessfuly")));

			UnForceChangePassword();
			return true;
		}
		/// <summary> Проверяет возможность смены пароля </summary>
		/// <param name="oldPass"> Старый пароль </param>
		/// <param name="newPass1"> Новый пароль </param>
		/// <param name="newPass2"> Новый пароль-подтверждение </param>
		/// <returns></returns>
		private bool CanChangePasswords(string oldPass, string newPass1, string newPass2)
		{
			bool result = true;

			if (IsUserGuest)
			{
				var msg = new Message(Severity.Error, GetMessageData("error"), GetMessageData("PasswordChangeForbidden"));
				AddMessage(msg);
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
				}
				return false;
			}

			if (!ForcedToChangePassword)
			{
				var login = IWebPS.GetOperatorDetails(IWebPS.OperatorId).login;

				string clientName;

				clientName = ConfigurationManager.AppSettings["Client"];

				if (string.IsNullOrEmpty(clientName))
					return false;

				var webLoginProvider = WebLoginProvider;
				if (webLoginProvider?.CheckPasswordForWeb(login, oldPass) == null)
				{
					var msg = new Message(Severity.Error, GetMessageData("error"), GetMessageData("WrongPassword"));
					AddMessage(msg);
					if (Request.IsAjaxRequest())
					{
						WrapJsonAction(() => msg);
						return false;
					}
					result = false;
				}
			}

			if (newPass1.Length == 0)
			{
				var msg = new Message(Severity.Error, GetMessageData("error"), GetMessageData("NewPasswordNotSet"));
				AddMessage(msg);
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
					return false;
				}
				result = false;
			}

			if (newPass1 != newPass2)
			{
				var msg = new Message(Severity.Error, GetMessageData("error"), GetMessageData("newPasswordsDoesNotConcur"));
				AddMessage(msg);
				if (Request.IsAjaxRequest())
				{
					WrapJsonAction(() => msg);
					return false;
				}
				result = false;
			}

			return result;
		}
		private void RenderSettingsForm()
		{
			TemplateFile = "settings";
			AddProperty("template", "userSettings");
			var allowSmsSend = IWebPS.AllowSmsSend;
			AddProperty("allowUserSmsSend", XmlConvert.ToString(allowSmsSend));
			if (Request.Cookies["onlinehrs"] != null)
			{
				AddProperty("onlinehrs", Request.Cookies["onlinehrs"].Value);
			}

			AddProperty("ForcedToChangePassword", XmlConvert.ToString(ForcedToChangePassword));
			AddProperty("ForcedToSetEmail", XmlConvert.ToString(ForcedToSetEmail));
			AddProperty("ForcedToSetPhone", XmlConvert.ToString(ForcedToSetPhone));

			//Пока что нет бизнес-процессов, которые бы позволяли оператору изменять свой собственный логин
			AddProperty("AllowedToChangeLogin", XmlConvert.ToString(IWebPS.AllowChangeOwnLogin));
		}
		[PageAction("send-confirmation-code-to-Email", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SetNewContactResult SendConfirmationEmail(int id)
		{
			return IWebPS.SendConfirmationCode(
				id,
				GetMessageData("SetNewEmailEmailSubject"),
				GetMessageData("SetNewEmailEmailBody")).Code;
		}
		[PageAction("send-confirmation-code-to-Phone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SetNewContactResult SendConfirmationSmsCode(string id)
		{
			return IWebPS.SendConfirmationCode(int.Parse(id), null, GetMessageData("ConfirmationSmsTemplate")).Code;
		}
		[PageAction("send-confirmation-code", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SendConfirmationCodeResult SendConfirmationCode(int id)
		{
			return IWebPS.SendConfirmationCode(id, null, GetMessageData("ConfirmationSmsTemplate"));
		}
		[PageAction("send-confirmation-code-to-msisdn", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SendConfirmationCodeResult SendConfirmationCodeToMsisdn(string msisdn)
		{
			return IWebPS.SendConfirmationCode(ContactType.Phone, msisdn, null, GetMessageData("ConfirmationSmsTemplate"));
		}
		[PageAction("verify-Phone-confirmation", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool VerifyConfirmationSmsCode(int id, string value)
		{
			var verifyContactResult = IWebPS.VerifyContact(contactId: id, confirmationCode: value);
			var success = verifyContactResult.Code == SetNewContactResult.Success;

			if (success)
				UpdateSettingsRestrictions();

			return success;
		}
		[JsonPageAction("verify-msisdn", Method = HttpMethod.Post)]
		public VerifyContactResult VerifyMsisdn(string msisdn, string value)
		{
			return IWebPS.VerifyMsisdn(msisdn: msisdn, confirmationCode: value);
		}
		[JsonPageAction("verify-Email-confirmation", Method = HttpMethod.Post)]
		public bool VerifyConfirmationEmail(int id, string value)
		{
			var success = IWebPS.VerifyContact(contactId: id, confirmationCode: value).Code == SetNewContactResult.Success;

			if (success)
			{
#if !DEBUG
				CurrentBillingAccountExistsAndCreateIfNeeded();
#endif
				UpdateSettingsRestrictions();
			}

			return success;
		}
		[PageAction("add-Email", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public object AddEmail(string value)
		{
			int id;
#pragma warning disable 612
			var result = IWebPS.AddContact(ContactType.Email, value, out id);
#pragma warning restore 612
			return result == SetNewContactResult.Success ? (object) id : result;
		}
		[PageAction("delete-Email", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SetNewContactResult DeleteEmail(int id)
		{
			var result = IWebPS.DeleteContact(id);
			if (result == SetNewContactResult.SuccessfullyDeleted)
				UpdateSettingsRestrictions();
			return result;
		}
		[PageAction("add-Phone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public object AddPhone(string value)
		{
			int id;
#pragma warning disable 612
			var result = IWebPS.AddContact(ContactType.Phone, value, out id);
#pragma warning restore 612
			return result == SetNewContactResult.Success ? (object)id : result;
		}
		[PageAction("delete-Phone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public SetNewContactResult DeletePhone(int id)
		{
			var result = IWebPS.DeleteContact(id);
			if (result == SetNewContactResult.SuccessfullyDeleted)
				UpdateSettingsRestrictions();
			return result;
		}
		[PageAction("change-password", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CallResult<SetNewPasswordResult> ChangePassword(string oldPassword, string newPassword)
		{
			var result = IWebPS.ChangePassword(oldPassword, newPassword);
			return new CallResult<SetNewPasswordResult>
			{
				Result     = result,
				ResultText = GetText(result)
			};
		}
		[PageAction("set-password", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CallResult<SetNewPasswordResult> SetPassword(string value)
		{
			var setNewPasswordResult = IWebPS.SetPassword(value);
			return new CallResult<SetNewPasswordResult>
			{
				Result     = setNewPasswordResult,
				ResultText = GetText(setNewPasswordResult)
			};
		}
		private string GetText(SetNewPasswordResult result)
		{
			switch (result)
			{
				case SetNewPasswordResult.Success:
					return GetMessageData("PasswordChangedSuccessfuly");
				case SetNewPasswordResult.NewPasswordIsInvalid:
					return GetMessageData("NewPasswordNotSet");
				case SetNewPasswordResult.OldPasswordIsWrong:
					return GetMessageData("WrongPassword");
				case SetNewPasswordResult.NoConfirmedContact:
					return GetMessageData("NoConfirmedPhone");
				default:
					return string.Empty;
			}
		}
		[PageAction("changePassword")]
		public void ChangePassword()
		{
			var oldPass  = GetParamValue("currentpwd");
			var newPass1 = GetParamValue("newpwd1");
			var newPass2 = GetParamValue("newpwd2");

			if (ForcedToChangePassword || oldPass.Length > 0 && newPass1.Length > 0 && newPass2.Length > 0)
			{
				try
				{
					bool result = ChangePassword(oldPass, newPass1, newPass2);
					if (Request.IsAjaxRequest() && result)
					{
						var msg = new Message(Severity.Normal, GetMessageData("PasswordChangedSuccessfuly"), GetMessageData("PasswordChangedSuccessfuly"));
						WrapJsonAction(() => msg);
					}
				}
				catch (Exception ex)
				{
					WriteLog(ex);
					var msg = new Message(Severity.Error, GetMessageData("error"), ex.Message);
					AddMessage(msg);

					if (Request.IsAjaxRequest())
					{
						WrapJsonAction(() => msg);
					}
				}
			}

			if (IsMobile)
			{
				ShowPage();
			}
		}
		private new void WrapJsonAction<T>(Func<T> action)
		{
			base.WrapJsonAction(action);
			UpdateCurrentOperator();
		}
		protected override void  ResponseSerializationEnd()
		{
			base.ResponseSerializationEnd();

			AddProperty("AllowUseDialer", XmlConvert.ToString(AllowUseDialer));
		}
		private bool AllowUseDialer
		{
			get
			{
				var o = GetObjectFromSession("AllowUseDialer");
				bool result;
				if (o == null)
				{
					result = IWebPS.AllowUseDialer;
					SetObjectToSession("AllowUseDialer", result);
				}
				else
				{
					result = (bool) o;
				}
				return result;
			}
		}
		[PageAction("ConfirmedEmailIsRequired")]
		public void ConfirmedEmailIsRequired()
		{
			WrapJsonAction(() => ForcedToSetEmail);
		}
		[PageAction("ConfirmedPhoneIsRequired")]
		public void ConfirmedPhoneIsRequired()
		{
			WrapJsonAction(() => ForcedToSetPhone);
		}
		[PageAction("GetContacts", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Contact[] GetContacts(ContactType contactType)
		{
			return IWebPS.GetContacts(contactType);
		}
		[PageAction("set-Email-name", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool SetEmailName(int id, string value)
		{
			IWebPS.SetContactName(id, value);
			return true;
		}
		[PageAction("set-Email-comment", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public void SetEmailComment(int id, string value)
		{
			IWebPS.SetContactComment(id, value);
		}
		[PageAction("set-Phone-name", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool SetPhoneName(int id, string value)
		{
			IWebPS.SetContactName(id, value);
			return true;
		}
		[PageAction("set-Phone-comment", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool SetPhoneComment(int id, string value)
		{
			IWebPS.SetContactComment(id, value);
			return true;
		}
		[PageAction("set-user-name", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public bool SetUserName(string name)
		{
			IWebPS.SetUserName(name);
			return true;
		}
		[PageAction("timezone", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public TimeZoneFingerprint GetTimeZone()
		{
			return IWebPS.TimeZoneInfo.ToFingerprint();
		}
		[PageAction("timezone", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool SetTimeZone(TimeZoneFingerprint value, bool manual = false)
		{
			var tz = TimeZoneHelper.FromFingerprint(value);
			return IWebPS.UpdateTimeZoneInfo(tz, manual);
		}
		[PageAction("time", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public DateTime GetUtcTime()
		{
			return DateTime.UtcNow;
		}
		[PageAction("contact-list", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Dictionary<string, object> UploadPhoneBook(string value)
		{
			PhoneBook phoneBook;
			try
			{
				phoneBook = new PhoneBook
				{
					Contacts = JsonHelper.DeserializeObjectFromJson<PhoneBookContact[]>(value)
				};
			}
			catch (JsonException ex)
			{
				// Вывести в лог содержимое JSON со списком контактов пользователя только при включенной настройке, чтобы не загромождать лог приложения
				if (ConfigHelper.GetAppSettingAsBoolean("LogInvalidPhoneBook"))
					$"Invalid phone book format OperatorId={OperatorId}\nPHONEBOOK:\n{value}"
						.WithException(ex, true)
						.CallTraceWarning();
				return WrapSavePhoneBookResult(SavePhoneBookResult.InvalidFormat);
			}

			var result = IWebPS.SavePhoneBook(phoneBook);

			return WrapSavePhoneBookResult(result);
		}
		private static Dictionary<string, object> WrapSavePhoneBookResult(SavePhoneBookResult result)
		{
			return new Dictionary<string, object>
			{
				{ "result", result.ToString() }
			};
		}
		[PageAction("add", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public AddResult<Contact> AddContact(ContactType type, string value)
		{
			return IWebPS.AddContact(type, value);
		}
	}
}