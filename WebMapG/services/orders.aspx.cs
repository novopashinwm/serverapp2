﻿using System;
using System.Collections.Generic;
using Compass.Ufin.Orders.Core;
using Compass.Ufin.Orders.Core.Models;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.Extensions.DependencyInjection;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class Orders : BaseRestfulPage
	{
		private IOrdersManagement           GetOrdersManagementService()
		{
			var ordersManagementService = ServerApplication.Server.Instance()
				?.Services                                 // Получаем доступ к сервисам
				?.GetRequiredService<IOrdersManagement>(); // Получаем экземпляр сервиса управления заказами или ошибку
			// Заполняем контекст сервиса идентификатором оператора
			ordersManagementService.OperatorId   = OperatorId;
			// Заполняем контекст сервиса идентификатором департамента
			ordersManagementService.DepartmentId = DepartmentId;

			return ordersManagementService;
		}
		[JsonPageAction("get-orders", Method = HttpMethod.Get)]
		public CommonActionResponse<GetListResponse<Order>> GetOrders(DateTime intervalBegUtc, DateTime intervalEndUtc, int? page, int? recordsPerPage)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}', from(Utc):{intervalBegUtc}, to(Utc):{intervalEndUtc}".CallTraceMessage()}");

			var response = new CommonActionResponse<GetListResponse<Order>>();
			try
			{
				response.Object = new GetListResponse<Order>
				{
					Pagination = recordsPerPage.HasValue
					? new Pagination
					{
						Page           = page ?? 1,
						RecordsPerPage = recordsPerPage.Value
					} : null
				};
				var ordersManagmentService = GetOrdersManagementService();
				response.Object.Items = ordersManagmentService
					.GetOrders(intervalBegUtc, intervalEndUtc, response.Object.Pagination);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("order-create", Method = HttpMethod.Post)]
		public CommonActionResponse<OrderCreationResponse> OrderCreate(OrderCreationRequest request)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}'".CallTraceMessage()}");

			var response = new CommonActionResponse<OrderCreationResponse>();
			try
			{
				if (request == null)
					throw new ArgumentNullException($"{nameof(request)} not be empty.", nameof(request));
				var ordersManagmentService = GetOrdersManagementService();
				var order = ordersManagmentService.GetOrderByExternalId(request.ExternalOrderId);
				if (order != null)
					throw new ArgumentException($"Order '{request.ExternalOrderId}' already exists. Created at '{order.CreatedAtUtc}'.", nameof(request.ExternalOrderId));
				response.Object = ordersManagmentService.Create(request);
				if (null == response.Object)
					response.Result = CommonActionResult.Error;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("order-read", Method = HttpMethod.Get)]
		public CommonActionResponse<Order> OrderRead(long orderId)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}', orderId:{orderId}".CallTraceMessage()}");

			var response = new CommonActionResponse<Order>();
			try
			{
				var ordersManagmentService = GetOrdersManagementService();
				response.Object = ordersManagmentService.GetOrder(orderId);
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("order-update", Method = HttpMethod.Post)]
		public CommonActionResponse<Order> OrderUpdate(OrderUpdateRequest request)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}'".CallTraceMessage()}");

			var response = new CommonActionResponse<Order>();
			try
			{
				if (request == null)
					throw new ArgumentNullException($"{nameof(request)} not be empty.", nameof(request));
				var ordersManagmentService = GetOrdersManagementService();
				var updOrder = ordersManagmentService.GetOrder(request.OrderId);
				if (updOrder == default)
					throw new KeyNotFoundException($"{nameof(Order)} with Id={request.OrderId} not found for Operator='{LoggedUser}'");

				response.Object = ordersManagmentService.Update(request);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("order-delete", Method = HttpMethod.Post)]
		public CommonActionResponse<bool> OrderDelete(long orderId)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{SessionId}|{$"login = '{LoggedUser}', orderId:{orderId}".CallTraceMessage()}");

			var response = new CommonActionResponse<bool> { Object = false };
			try
			{
				var ordersManagmentService = GetOrdersManagementService();
				var delOrder = ordersManagmentService.GetOrder(orderId);
				if (delOrder == default)
					throw new KeyNotFoundException($"{nameof(Order)} with Id={orderId} not found for Operator='{LoggedUser}'");

				response.Object = ordersManagmentService.Delete(orderId);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = false;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
	}
}