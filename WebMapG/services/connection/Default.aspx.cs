﻿using System;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.UI.WebMapG;

/// <summary>
/// Страница настроек соединения.
/// По сути содержит только текст и ссылку на DLL от Sun, позволяющую java-апплету
/// работать с COM-портом.
/// </summary>
public partial class services_connection_Default : BasePage
{
	[PageAction("default")]
	public void ShowPage()
	{
		if (!IWebPS.AllowUseDialer)
		{
			AccessIsDenied();
			return;
		}

		TemplateFile = "settings";
		AddProperty("template", "connectionSettings");

		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}

	/// <summary>
	/// AS: Определяет по номеру, кто звонит. Если определить не удается, возвращает телефонный номер,
	/// иначе - гаражный номер
	/// </summary>
	[PageAction("whocallme")]
	public void WhoCallMe()
	{
		SetXmlContentType();
		/*TemplateFile = "dialer";
		AddProperty("template", "WhoCallMe");*/

		var phoneNumber = GetParamValue("phonenumber");

		phoneNumber = phoneNumber.Replace(' ', '+');
		ResponseSerializationBeg();
		Vehicles vehicles = (Vehicles)GetObjectFromSession("Allow_Vehicles");
		if (vehicles != null)
		{
			Vehicles.VehiclesInfo vi = vehicles.GetVehicleByControllerNumber(phoneNumber);
			if (vi != null)
			{
				try
				{
					vi.WriteXml(Writer);
				}
				catch (Exception ex)
				{
					AddMessage(new Message(Severity.Error, GetMessageData("error"), ex.Message));
					WriteLog(ex);
				}
			}
		}
		ResponseSerializationEnd();
	}
}