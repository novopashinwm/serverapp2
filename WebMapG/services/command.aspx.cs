﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class ServicesCommand : BaseRestfulPage
	{
		[PageAction(Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public Command CreateCommand(Command command)
		{
			if (command == null)
				return null;

			if (command.Type == CmdType.AskPositionOverMLP)
				command.Type = CmdType.AskPosition;

			return IWebPS.RegisterCommand(command);
		}

		[PageAction(Method = HttpMethod.Delete, RequestType = DataType.Json, ResponseType = DataType.Json)]
		[PageAction("cancel", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public bool CancelCommand(int id)
		{
			return IWebPS.CancelCommand(id);
		}

		/// <summary> Отправка команды на устройство </summary>
		/// <param name="vehicleIds"> Список идентификаторов объектов наблюдения через запятую </param>
		/// <param name="a"> Команда </param>
		[PageAction("ChangeMode")]
		[PageAction("MandownMode")]
		[PageAction("AlarmMode")]
		public void SendCommand(string vehicleIds, string a)
		{
			var arrVehicleIds   = vehicleIds.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
			var arIntVehicleIds = Array.ConvertAll(arrVehicleIds, int.Parse);

			var cmdTypeEnum = (CmdType)Enum.Parse(typeof(CmdType), a);
			string newmode = GetParamValue("newmode");
			var @params = new Dictionary<string, string>();
			if (!string.IsNullOrEmpty(newmode))
				@params.Add("newmode", newmode);

			foreach (var vehicleId in arIntVehicleIds)
			{
				try
				{
					IWebPS.RegisterCommand(new Command
					{
						TargetID   = vehicleId,
						Type       = cmdTypeEnum,
						Parameters = @params
					});
				}
				catch (Exception ex)
				{
					AddMessage(new Message(Severity.Error, "", ex.Message));
					WriteLog(ex);
				}
			}

			SetXmlContentType();
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}

		[PageAction("types", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public List<CommandType> GetTypes(int vehicleId, bool includeScenario = false)
		{
		return IWebPS.GetCommandTypes(vehicleId, includeScenario)
				.ToList();
		}

		[PageAction("scenario", Method = HttpMethod.Get, RequestType = DataType.Json, ResponseType = DataType.Json)]
		public CommandScenario GetScenario(int vehicleId, CmdType type)
		{
			return IWebPS.GetCommandScenario(vehicleId, type);
		}
	}
}