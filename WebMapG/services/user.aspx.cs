﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.UI.WebMapG.services
{
	public partial class ServicesUser : BasePage
	{
		[JsonPageAction("getadmdepartments", HttpMethod.Unspecified)]
		public Dictionary<string, object> GetAdminDepartments(string searchText)
		{
			var departments = IWebPS.SearchAdminDepartments(searchText);
			var operators   = IWebPS.SearchOperators(searchText) ?? new List<BusinessLogic.DTO.OperatorRights.Operator>();
			var res = new Dictionary<string, object>
			{
				{ "value",      departments },
				{ "searchText", searchText  },
			};
			if (operators != null)
				res.Add("operators", operators);
			return res;
		}
		[PageAction("sendquestion")]
		public void SendQuestion(string trackerNumber, string contacts, string question)
		{
			IWebPS.SendQuestionToSupport(contacts, trackerNumber, question);
		}
	}
}