﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG.services
{
	public class Share : BaseRestfulPage
	{
		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();
			if (!Page.IsPostBack)
			{
				Process();
				UpdateSessionDateTime();
			}
		}
		[PageAction()]
		public void RedirectToShareLink()
		{
			// Получить параметр идентификатора ссылки
			var linkText = GetParamValue("link");
			if (!string.IsNullOrWhiteSpace(linkText))
			{
				// Получить значение идентификатора ссылки
				var linkGuid = default(Guid);
				if (Guid.TryParse(linkText, out linkGuid))
				{
					// Получить объект ссылки
					var dtoShareLink = ServerApplication.Server.Instance()
						.GetShareLinkByGuid(linkGuid);
					if (default(ShareLink) != dtoShareLink)
					{
						// Проверить время ссылки, наступило и не просрочено
						var logTimeUtc = DateTime.UtcNow.ToLogTime();
						if (dtoShareLink.LogTimeBeg <= logTimeUtc && logTimeUtc <= dtoShareLink.LogTimeEnd)
						{
							// Получить идентификатор оператора
							var operatorId = WebLoginProvider.GetOperatorId4ShareLink(dtoShareLink.VehicleId);
							if (operatorId.HasValue)
							{
								// Выполняем вход
								var loginResult = TryLogIn(operatorId.Value);
								if (LoginResult.Success == loginResult)
								{
									if (IsAuthorized)
									{
										// Меняем время истечения, на окончание ссылки
										if (Response.Cookies.AllKeys.Contains(ContextHelper.SessionCookieName))
											Response.Cookies[ContextHelper.SessionCookieName].Expires = dtoShareLink.LogTimeEnd.ToUtcDateTime();
										RedirectResponse($@"map.aspx");
										return;
									}
								}
							}
						}
					}
				}
			}
			if (IsAuthorized)
				Context.Session.Abandon();
			throw new SecurityException("Not allowed");
		}
		/// <summary> Метод генерации ссылки для временного наблюдения за объектом </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="linkStartDate"> Дата начала действия ссылки (logTime UTC) </param>
		/// <param name="linkEndDate"> Дата окончания действия ссылки (logTime UTC) </param>
		/// <param name="name"> Название объекта наблюдения (в рамках действия ссылки) </param>
		/// <param name="comment"> Название объекта наблюдения (в рамках действия ссылки) </param>
		/// <returns></returns>
		[JsonPageAction("create-link", Method = HttpMethod.Post)]
		[Obsolete("Use 'share-link-create' instead.")]
		public CommonActionResponse<string> CreateLink(CancellationToken cancellationToken,
			int vehicleId, int linkStartDate, int linkEndDate, string name = null, string comment = null)
		{
			var response = new CommonActionResponse<string> { Object = null };
			try
			{
				// Блокировка множества запросов
				ServerApplication.Server.Instance()
					.AcceptRequestWithLimit(BanLimit.TryLogin, Request.UserHostAddress);
				// Определяем параметры ссылки и текущего времени
				var logTimeBeg = linkStartDate > linkEndDate ? linkEndDate   : linkStartDate;
				var logTimeEnd = linkStartDate > linkEndDate ? linkStartDate : linkEndDate;
				var logTimeUtc = DateTime.UtcNow.ToLogTime();
				// Проверяем попытку создания ссылки в прошлом
				if (logTimeUtc > logTimeEnd)
					throw new ArgumentOutOfRangeException("The share link finish datetime is earlier than current time. The share link will be invalid. Please enter a valid time period.");
				// Проверяем vehicleId на присутствие в системе
				var vehicle = GetVehicleById(vehicleId);
				if (null == vehicle)
					throw new KeyNotFoundException(nameof(Vehicle) + $" with Id={vehicleId} not found");
				// Получаем или создаем ссылку
				var dtoShareLink = IWebPS?.ShareLinkCreate(new ShareLink
				{
					LinkGuid    = ShareLink.EmptyGuid,
					CreatorId   = OperatorId.Value,
					LogTimeBeg  = logTimeBeg,
					LogTimeEnd  = logTimeEnd,
					VehicleId   = vehicleId,
					VehicleName = name,
					VehicleDesc = comment,
				});
				response.Object = new UriBuilder(Request.Url.GetLeftPart(UriPartial.Path))
				{
					Query = $@"link={dtoShareLink.LinkGuid}"
				}
				.Uri
				.ToString();
			}
			catch (Exception ex)
			{
				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("get-share-links", Method = HttpMethod.Get)]
		public GetListResponse<ShareLink> GetShareLinks(int? page, int? recordsPerPage)
		{
			// Создание объекта деления на страницы
			var pagination = recordsPerPage.HasValue
				? new Pagination
				{
					Page           = page ?? 1,
					RecordsPerPage = recordsPerPage.Value
				} : null;
			using (var source = CancellationTokenSource.CreateLinkedTokenSource(Response.ClientDisconnectedToken, Request.TimedOutToken))
			{
				return new GetListResponse<ShareLink>
				{
					Items      = IWebPS
						?.GetShareLinks(pagination, source.Token)
						?.ToArray(),
					Pagination = pagination
				};
			}
		}
		[JsonPageAction("share-link-create", Method = HttpMethod.Post)]
		public CommonActionResponse<ShareLink> ShareLinkCreate(ShareLink obj)
		{
			var response = new CommonActionResponse<ShareLink>();
			try
			{
				response.Object = IWebPS?.ShareLinkCreate(obj);
			}
			catch (Exception ex)
			{
				response.Object     = null;
				response.Result     = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("share-link-read", Method = HttpMethod.Get)]
		public CommonActionResponse<ShareLink> ShareLinkRead(Guid linkGuid)
		{
			var response = new CommonActionResponse<ShareLink>();
			try
			{
				response.Object = IWebPS?.ShareLinkRead(linkGuid);
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				response.Object     = null;
				response.Result     = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("share-link-update", Method = HttpMethod.Post)]
		public CommonActionResponse<ShareLink> ShareLinkUpdate(ShareLink obj)
		{
			var response = new CommonActionResponse<ShareLink>();
			try
			{
				response.Object = IWebPS?.ShareLinkUpdate(obj);
			}
			catch (Exception ex)
			{
				response.Object     = null;
				response.Result     = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[JsonPageAction("share-link-delete", Method = HttpMethod.Post)]
		public CommonActionResponse<bool?> ShareLinkDelete(Guid linkGuid)
		{
			var response = new CommonActionResponse<bool?> { Object = null };
			try
			{
				IWebPS?.ShareLinkDelete(linkGuid);
				response.Object = true;
			}
			catch (Exception ex)
			{
				response.Object     = false;
				response.Result     = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
	}
}