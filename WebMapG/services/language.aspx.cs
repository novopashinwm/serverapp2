﻿using System;
using System.Web;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.UI.WebMapG.services
{
	/// <summary> Работа с языковыми настройками </summary>
	public class ServicesLanguage : BasePage
	{
		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();

			if (Page.IsPostBack)
				return;

			Process();
			UpdateSessionDateTime();
		}

		[PageAction("lang")]
		public void SetLanguage()
		{
			var language = GetParamValue("lang").Trim().ToLower();
			var backUrl  = Request.ApplicationPath + "/";
			if (Request.UrlReferrer != null)
				backUrl = Request.UrlReferrer.ToString();

			SysDiags::Trace.TraceInformation("{0}: language: {1}, back url: {2}",
				Request.RawUrl, language, backUrl);

			HttpCookie cookie = new HttpCookie("language", language);
			cookie.Expires = DateTime.Now.AddYears(1);
			Response.Cookies.Add(cookie);
			SetCultureByLanguage(language);
			if (IsAuthorized)
			{
				var culture = CurrentCulture;
				if (culture != null)
				{
					IWebPS.UpdateCulture(culture.Name);
				}
			}
			Response.Redirect(backUrl, true);
		}
	}
}