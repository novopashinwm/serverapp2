﻿namespace FORIS.TSS.UI.WebMapG.error
{
	public partial class _403 : BasePage
	{
		[PageAction("default")]
		public void Default()
		{
			TemplateFile = "error";
			AddProperty("template", "accessIsDenied");
			ResponseSerializationBeg();
			ResponseSerializationEnd();
		}
	}
}