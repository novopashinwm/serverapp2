delete a
from H_Billing_Service a
where a.action = 'INSERT'
  and not exists (select * from Billing_Service e where e.ID = a.ID)
  and not exists (select * from H_Billing_Service d where d.ID = a.ID and d.action = 'DELETE')
  
delete a
from H_Billing_Blocking a
where a.action = 'INSERT'
  and not exists (select * from Billing_Blocking e where e.ID = a.ID)
  and not exists (select * from H_Billing_Blocking d where d.ID = a.ID and d.action = 'DELETE')