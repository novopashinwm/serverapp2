declare @sys table(name varchar(255))
insert into @sys values ('TimeZone'), ('CONTROLLER_SENSOR'), ('CONTROLLER_SENSOR_LEGEND'), ('CONTROLLER_SENSOR_TYPE'), ('CONTROLLER_TYPE'), 
('MAPS'), ('MESSAGE_TEMPLATE'), ('MESSAGE_TEMPLATE_FIELD'), ('RIGHT'), ('ZONE_TYPE'), ('ORDER_TYPE'), ('DOTNET_TYPE'), ('Billing_Service_Provider'), 
('Billing_Service_Provider_Host'), ('Billing_Service_Type'), ('BLADING_TYPE'), ('CommandTypes'), ('CONSTANTS'), ('ContactType'), ('Country'), 
('Country_Code'), ('Culture'), ('Fuel_Type'), ('Controller_Type_CommandTypes'), ('CounterType'), ('DRIVER_MSG_TEMPLATE'), ('DRIVER_STATUS'), ('IPRegions'), 
('IPValues'), ('MEDIA'), ('MEDIA_TYPE'), ('MessageContactType'), ('Rendered_Service_Type'), ('REPORT'), ('RULE'), ('Scale'), ('SMS_TYPE'), ('Unit'), 
('VEHICLE_KIND'), ('VEHICLE_STATUS'), ('ZONE_PRIMITIVE_TYPE'), ('SCHEDULEREVENT')

declare @dep table (parent varchar(255), referenced varchar(255))

insert into @dep
	select po.name, ro.name
		from sys.foreign_keys fk
		join sys.foreign_key_columns fkc on fkc.constraint_object_id = fk.object_id
		join sys.objects po on po.object_id = fkc.parent_object_id
		join sys.objects ro on ro.object_id = fkc.referenced_object_id
		where po.name <> ro.name

declare @table table(name varchar(255), i int)

insert into @table (name)
	select o.name
		from sys.objects o
		join sys.objects h on h.name = 'H_' + o.name
		where o.type = 'u' and h.type = 'u'
		  and o.name not in (select s.name from @sys s)

insert into @table (name)
	select o.name
		from sys.objects o
		where o.type = 'u'
		  and substring(o.name, 1, 2) = 'H_'

insert into @table (name)
select o.name
	from sys.objects o
	where o.type = 'u'
		and o.name not in (select t.name from @table t)
		and not exists (select 1 from sys.objects e where e.type = 'u' and e.name = 'H_' + o.name)
		and substring(o.name, 1, 2) <> 'H_'
		and o.name not in (select s.name from @sys s)
	order by o.name
	
declare @referenced table (id int)
insert into @referenced
	select d.referenced_id
		from sys.sql_expression_dependencies d
		join sys.objects o on o.object_id = d.referencing_id
		where (o.type = 'u' or o.type = 'v' and exists (select 1 from sys.indexes i where i.object_id = o.object_id))
			and d.referencing_id <> d.referenced_id
union all
	select fkc.referenced_object_id
	from sys.foreign_keys fk
	join sys.foreign_key_columns fkc on fkc.constraint_object_id = fk.object_id
	join sys.objects po on po.object_id = fkc.parent_object_id
	join sys.objects ro on ro.object_id = fkc.referenced_object_id

declare @i int = 0
while exists (select * from @table where i is null)
begin
	
	update t
		set i = @i
		from @table t
		where not exists (
			select 1
				from @dep d
				join @table rt on rt.name = parent
				where d.referenced = t.name
				  and rt.i is null)
		and t.i is null
	
	if @@ROWCOUNT = 0
		break

	set @i = @i + 1
end

update @table set @i = (select max(i) from @table) + 1 where i is null

select 'alter table [' + name + '] NOCHECK CONSTRAINT all ' from @table
union all
select * 
from (
	select top 100 percent sql = 'print ''' + t.name + '''; ' + case t.referenced when 1 then 'delete ' else 'truncate table ' end + t.name
		from (
			select	name = '[' + t.name + ']',
					referenced = 
						case when object_id(t.name) in (select r.id from @referenced r)
								then 1 else 0 end
					, i
				from @table t
		) t
		order by t.referenced asc, i asc
) t
union all
select 'alter table [' + name + '] WITH CHECK CHECK CONSTRAINT all ' from @table
union all 
select 'DBCC SHRINKDATABASE (' + DB_NAME() + ')'
union all
select 'ALTER DATABASE ' +  + DB_NAME() + ' SET RECOVERY Simple ;'
union all 
select 'dbcc shrinkfile (' + name + ')' 
	from sys.database_files where type = 1