delete odd_od
	from country c 
	join DEPARTMENT d on d.Country_ID = c.Country_ID
	join OPERATOR_DEPARTMENT od on od.DEPARTMENT_ID = d.DEPARTMENT_ID and od.RIGHT_ID = 104 and od.ALLOWED = 1
	join operator o on o.OPERATOR_ID = od.OPERATOR_ID
	join OPERATOR_DEPARTMENT odd_od on odd_od.OPERATOR_ID = o.OPERATOR_ID and odd_od.RIGHT_ID <> 104 and odd_od.ALLOWED = 1
	join [right] odd_r on odd_r.RIGHT_ID = odd_od.RIGHT_ID
	where c.Name = 'India'
	  and o.LOGIN <> 'ppJha'

delete odd_ro
	from country c 
	join DEPARTMENT d on d.Country_ID = c.Country_ID
	join OPERATOR_DEPARTMENT od on od.DEPARTMENT_ID = d.DEPARTMENT_ID and od.RIGHT_ID = 104 and od.ALLOWED = 1
	join operator o on o.OPERATOR_ID = od.OPERATOR_ID
	join RIGHT_OPERATOR odd_ro on odd_ro.OPERATOR_ID= od.OPERATOR_ID and odd_ro.RIGHT_ID = 2 and odd_ro.ALLOWED = 1
	where c.Name = 'India'
	  and o.LOGIN <> 'ppJha'