--insert into operatorgroup_department 
--select og_by_name.operatorgroup_id, d.department_id, 104, 1
--insert into operatorgroup (name, COMMENT)
--select d.name, 'Added by department name 2012.12.03'
select d.name, og_by_right.name, og_by_name.name, og_by_name.operatorgroup_id
	from department d
	left outer join operatorgroup og_by_right on
		exists (
			select *	
				from operatorgroup_department ogd 
				where ogd.department_id = d.department_id 
				  and ogd.operatorgroup_id = og_by_right.operatorgroup_id
				  and ogd.right_id = 104)
		and not exists (
			select *	
				from operatorgroup_department ogd 
				where ogd.department_id = d.department_id 
				  and ogd.operatorgroup_id = og_by_right.operatorgroup_id
				  and ogd.right_id = 2)
	left outer join operatorgroup og_by_name on og_by_name.name = d.name
											 or og_by_name.name = d.description + d.name
where og_by_right.operatorgroup_id is null
  and og_by_name.operatorgroup_id is not null
order by d.department_id desc
