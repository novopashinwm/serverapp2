select name
	into #sp
	from sys.objects where type = 'p'

declare @sp_name varchar(255)
	
while 1=1
begin

	set @sp_name = (select top(1) name from #sp)
	if @sp_name is null
		break
	
	delete from #sp where name = @sp_name
	
	exec sp_recompile @sp_name
end