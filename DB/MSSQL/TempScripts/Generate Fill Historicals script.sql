select t.t as [text()] from (
	select t = '
	declare @trail_id int
	insert into trail(trail_time) select getutcdate()
	set @trail_id = @@identity
	'
	union all
	select
		'
		print '' ' + ho.name + '''
		
		insert into ' + ho.name + ' ( ' + (oc.names) + ', action, actual_time, trail_id)' + '
		select o.' + oc.names + ', ''upload'', getutcdate(), @trail_id
			from [' + o.name + '] o where not exists (select * from [' + ho.name + '] h where h.' + ic.name + ' = o.' + ic.name + ')
		' as [text()]
			from sys.objects o 
			join sys.columns ic on ic.object_id = o.object_id and ic.is_identity = 1
			join sys.objects ho on ho.name = 'h_' + o.name
			join sys.columns hoic on hoic.object_id = ho.object_id and hoic.name = ic.name and hoic.is_identity = 0
			cross apply (
				select names = SUBSTRING(t.names, 1, len(t.names)-1)
					from (select names = (select '[' + name + '],' as [text()] from sys.columns where object_id = o.object_id for xml path(''))) t
			) oc
) t for xml path('')

