begin tran --DO NOT FORGET TO __COMMIT__

create table #imei (
	Value			varchar(32) collate Cyrillic_General_CI_AI, 
	Vehicle_Type	nvarchar(255) collate Cyrillic_General_CI_AI, 
	Garage_Number	nvarchar(32) collate Cyrillic_General_CI_AI, 
	Phone			varchar(255) collate Cyrillic_General_CI_AI, 
	Type_Name		varchar(255) collate Cyrillic_General_CI_AI, 
	Public_Number	nvarchar(255) collate Cyrillic_General_CI_AI)

insert into #imei (Value, Type_Name /*, Phone*/)
	  select '352599041294237'	Value, 'TR600' Type_Name --, Phone = '79196202427'
union select '352599042036777'	Value, 'TR600' Type_Name --, Phone = '79110644872'
--union select '011412001272396'	Value, 'TR203' Type_Name, Phone = '79110644799'
--select * from controller_type where type_name = 'TR203'

declare @department_name nvarchar(255)
set @department_name = '�� ������'

print 'select * from department '
select * from department where name = @department_name
print 'select * from vehiclegroup '
select * from vehiclegroup where name = @department_name
print 'select * from zonegroup '
select * from zonegroup where name = @department_name
print 'select * from zonegroup '
select * from operatorgroup where name = @department_name

declare @operator_login varchar(255)
set @operator_login = 'maurer'
print 'select * from operator where login ='
select * from operator where login = @operator_login

declare @operator_password varchar(255)
set @operator_password = @operator_login

print 'insert into dbo.department(name) '
insert into dbo.department(name) 
	select @department_name
	where not exists (select * from dbo.Department d where d.name = @department_name)
declare @department_id int
set @department_id = (select department_id from dbo.Department d where d.name = @department_name)

print 'insert into dbo.Vehicle '
insert into dbo.Vehicle (Garage_Number, Public_Number, Vehicle_Type, Vehicle_Kind_ID, Department)
	select isnull(imei.Garage_Number, imei.Value), 
		   isnull(imei.Public_Number, isnull(imei.Garage_Number, imei.Value)), 
		   imei.Vehicle_Type,
		   vk.Vehicle_Kind_ID, @department_id
		from #imei imei, dbo.Vehicle_Kind vk
		where vk.Name = '��������'
		  and not exists (
			select 1 
			from dbo.Vehicle v
			join dbo.Controller c on c.Vehicle_ID = v.Vehicle_ID
			join dbo.Controller_Info ci on ci.Controller_ID = c.Controller_ID
			where ci.Device_ID = convert(varbinary(32), imei.Value))

print 'insert into dbo.Controller (Vehicle_ID, Controller_Type_ID, Number, PHONE)'
insert into dbo.Controller (Vehicle_ID, Controller_Type_ID, Number, PHONE)
	select v.Vehicle_ID, 
		   ct.Controller_Type_ID, 
		   case when imei.Type_Name = 'AvtoGraf' then convert(int, imei.Value) else 0 end,
		   imei.Phone
		from #imei imei
		join dbo.Vehicle v on v.Garage_Number = isnull(imei.Garage_Number, imei.Value) and v.Department = @department_id
		join dbo.Controller_Type ct on ct.Type_Name = imei.Type_Name
		where not exists (
			select 1
			from dbo.Controller c
			where c.Vehicle_ID = v.Vehicle_ID)
		order by v.vehicle_id

print 'insert into dbo.Controller_Info (Controller_ID, Device_ID, [Time])'
insert into dbo.Controller_Info (Controller_ID, Device_ID, [Time])
	select c.Controller_ID, convert(varbinary(32), imei.Value), getutcdate()
		from #imei imei
		join dbo.Vehicle v on v.Garage_Number = isnull(imei.Garage_Number, imei.Value) and v.Department = @department_id
		join dbo.Controller c on c.Vehicle_ID = v.Vehicle_ID
		where not exists (
			select 1 
				from dbo.Controller_Info ci
				where ci.Controller_ID = c.Controller_ID)

print 'update v set department = @department_id'
update v
	set department = @department_id
	from dbo.Vehicle v
	join dbo.Controller c on c.Vehicle_ID = v.Vehicle_ID
	join dbo.Controller_Info ci on ci.Controller_ID = c.Controller_ID
	join #imei imei on ci.Device_ID = convert(varbinary(32), imei.Value)
	where v.Department is null

print 'insert into dbo.VehicleGroup (Name)'
insert into dbo.VehicleGroup (Name)
	select @department_name
	where not exists (select * from dbo.VehicleGroup vg where vg.name = @department_name)


declare @vehiclegroup_id int
set @vehiclegroup_id = (select vehiclegroup_id from dbo.VehicleGroup vg where vg.name = @department_name)

print 'insert into dbo.VehicleGroup_Vehicle (Vehicle_ID, VehicleGroup_ID)'
insert into dbo.VehicleGroup_Vehicle (Vehicle_ID, VehicleGroup_ID)
	select v.Vehicle_ID, @vehiclegroup_id
	from dbo.Vehicle v
	join dbo.Controller c on c.Vehicle_ID = v.Vehicle_ID
	join dbo.Controller_Info ci on ci.Controller_ID = c.Controller_ID
	join #imei imei on ci.Device_ID = convert(varbinary(32), imei.Value)
	where not exists (select * from vehiclegroup_vehicle e where e.vehicle_id = v.vehicle_id and e.vehiclegroup_id = @vehiclegroup_id)

print 'insert into dbo.OperatorGroup (name)'
insert into dbo.OperatorGroup (name)
	select @department_name 
		where not exists (select * from operatorgroup where name = @department_name)

declare @operatorgroup_id int
set @operatorgroup_id = (select operatorgroup_id from operatorgroup where name = @department_name)

print 'insert into dbo.OperatorGroup_Department (OperatorGroup_ID, Department_ID, Right_ID, Allowed)'
insert into dbo.OperatorGroup_Department (OperatorGroup_ID, Department_ID, Right_ID, Allowed)
	select @operatorgroup_id, @department_id, 104, 1
	where not exists (select * from dbo.OperatorGroup_Department
						where operatorgroup_id = @operatorgroup_id
						  and department_id = @department_id
						  and right_id = 104)

-- ������ ���������� ������ ��� � ���������� ������ ���������� � ������� "������".
--print 'insert into dbo.ZoneGroup (Name)'
--insert into dbo.ZoneGroup (Name)
--	select @department_name
--	where not exists (select * from dbo.ZoneGroup where name = @department_name)
	
--declare @zonegroup_id int
--set @zonegroup_id = (select zonegroup_id from dbo.ZoneGroup where name = @department_name)

--print 'insert into dbo.OperatorGroup_ZoneGroup'
--insert into dbo.OperatorGroup_ZoneGroup
--	select @operatorgroup_id, @zonegroup_id, 105, 1
--	where not exists (select * from dbo.OperatorGroup_ZoneGroup
--						where operatorgroup_id = @operatorgroup_id
--						  and zonegroup_id = @zonegroup_id
--						  and right_id = 105)

print 'insert into dbo.OPERATOR (Name, LOGIN, PASSWORD)'
insert into dbo.OPERATOR (Name, LOGIN, PASSWORD)
	select @operator_login, @operator_login, @operator_password
	where not exists (select * from dbo.Operator o where login = @operator_login)
	
	declare @operator_id int 
	set @operator_id = (select operator_id from dbo.Operator o where login = @operator_login)
	
	if @operator_login like '%@%'
	begin
		print 'insert into dbo.email (operator_id, email)'
		insert into dbo.email (operator_id, email)
			select @operator_id, @operator_login
			where not exists (select * from dbo.Email e where e.email = @operator_login and e.operator_id = @operator_id)
	end;
	
	print 'insert into right_operator (operator_id, right_id, allowed)'
	insert into right_operator (operator_id, right_id, allowed)
		select @operator_id, r.right_id, 1
		from [right] r
		where r.right_id in (2, 102, 103, 16, 19)
		and not exists (select * from right_operator ro where ro.operator_id = @operator_id
							and ro.right_id = r.right_id)

	print 'insert into OPERATOR_DEPARTMENT (OPERATOR_ID, DEPARTMENT_ID, RIGHT_ID, ALLOWED)'
	insert into OPERATOR_DEPARTMENT (OPERATOR_ID, DEPARTMENT_ID, RIGHT_ID, ALLOWED)
		select @operator_id, @department_id, r.right_id, 1
		from [right] r 
		where r.right_id in (2)
		and not exists (select * from OPERATOR_DEPARTMENT od 
							where od.operator_id = @operator_id
							  and od.department_id = @department_id)
							  
	print 'insert into OperatorGroup_Department'
	insert into OperatorGroup_Department
		select og.OperatorGroup_ID, d.Department_ID, r.right_id, 1
			from OperatorGroup og, Department d, [Right] r
			where og.OperatorGroup_ID = 137 --og.Name = 'Sales'
			  and r.Right_ID in (2, 104) --department access
			  and d.department_id = @department_id
			  and not exists (select 1 from OperatorGroup_Department e
								  where e.OperatorGroup_ID = og.OperatorGroup_ID
									and e.Department_ID = d.Department_ID
									and e.Right_ID = r.Right_ID)
	
	print 'insert into operatorgroup_vehiclegroup (operatorgroup_id, vehiclegroup_id, right_id, allowed)'
	insert into operatorgroup_vehiclegroup (operatorgroup_id, vehiclegroup_id, right_id, allowed)
		select og.operatorgroup_id, @vehiclegroup_id, right_id, 1
		from [right] r, operatorgroup og
		where og.Name = 'Sales'
		and r.right_id in (2, 102, 109, 20, 21, 22)
		and not exists (select * from operatorgroup_vehiclegroup e 
							where e.operatorgroup_id = og.operatorgroup_id 
							  and e.vehiclegroup_id = @vehiclegroup_id
							  and e.right_id = r.right_id)

	print 'insert into operatorgroup_operator (operator_id, operatorgroup_id)'
	insert into operatorgroup_operator (operator_id, operatorgroup_id)
		select @operator_id, @operatorgroup_id
		where not exists (select * from operatorgroup_operator e 
							where e.operator_id = @operator_id 
							  and e.operatorgroup_id = @operatorgroup_id)
	
	print 'insert into operator_vehiclegroup (operator_id, vehiclegroup_id, right_id, allowed)'
	insert into operator_vehiclegroup (operator_id, vehiclegroup_id, right_id, allowed)
		select @operator_id, @vehiclegroup_id, right_id, 1
		from [right] r
		where r.right_id in (7, 102, 109, 20, 21, 22)
		and not exists (select * from operator_vehiclegroup e 
							where e.operator_id = @operator_id 
							  and e.vehiclegroup_id = @vehiclegroup_id
							  and e.right_id = r.right_id)

		
drop table #imei

--commit, rollback