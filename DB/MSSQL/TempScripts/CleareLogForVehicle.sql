--������� ������ �� ������� ������ ��� ��������� ������
--set nocount on
--select count(*) from log_time where vehicle_id = 442
--select dbo.getdatefromint(log_time), * from monitoree_log (nolock) where monitoree_id = 434
--select dbo.getdatefromint(log_time), * from gps_log (nolock) where vehicle_id = 434
--select count(*) from dbo.monitoree_log_ignored where log_time < 1273104000 and monitoree_id = 442 
--select dbo.getintfromdatestring('2010-05-06 00:00')
--select dbo.getdatefromint(datediff(ss, cast('1970-01-01' as datetime), cast('2010-05-05 10:00' as datetime)))
declare @vehicle_id int, @interval int, @to int, @dtTo datetime, @limitDT int;
select @interval = 20555;

select @vehicle_id = 435

--����, �� ������� ����� ������� ������
declare @deleteToDateTime datetime;
set @deleteToDateTime = cast('2010-05-05 10:10' as datetime)

select @limitDT = datediff(ss, cast('1970-01-01' as datetime), @deleteToDateTime)

select @to = min(log_time) from monitoree_log (nolock) where monitoree_id = @vehicle_id

print convert(varchar(40), dateadd(ss, @to, '1970-01-01'));
print convert(varchar(40), dateadd(ss, @limitDT, '1970-01-01'));

declare @sql nvarchar(max);

set @sql = '
	delete from dbo.geo_log
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.statistic_log
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.gps_log
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.sensors_log
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.can_info
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.log_time
	where vehicle_id = @vehicle_id and log_time < @to

	delete from dbo.monitoree_log_ignored
	where monitoree_id = @vehicle_id and log_time < @to


';

while @to <= @limitDT
begin
	
	set @dtTo = dateadd(ss, @to, '1970-01-01');
	exec sp_executesql @sql, N'@to int, @vehicle_id int', @to = @to, @vehicle_id = @vehicle_id;

	--select @to = @to + @interval;
	select @to = min(log_time) from monitoree_log (nolock) where monitoree_id = @vehicle_id
	if ((@to + @interval) > @limitDT and @to < @limitDT)
		set @to = @limitDT;
	else
		set @to = @to + @interval;

	print convert(varchar(40), @dtTo);
end;
