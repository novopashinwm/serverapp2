select '
insert into Controller_Sensor_Log (Vehicle_ID, Log_Time, Number, Value)
	select v.Vehicle_ID, ci.Log_Time, cs.Number, ci.' + sc.Name + ' / cs.Default_Multiplier
	from Department d 
	join Vehicle v on v.Department = d.Department_ID
	join Controller c on c.Vehicle_ID = v.Vehicle_ID
	join Controller_Type ct on ct.Controller_Type_ID = c.Controller_Type_ID
	join Controller_Sensor cs on cs.Descript = ''' + cs.Descript + ''' and cs.Controller_Type_ID = c.Controller_Type_ID
	join CAN_Info ci on ci.Vehicle_ID = v.Vehicle_ID
	where d.name like ''%�������%''
	  and ct.type_name = ''AvtoGrafCan''
	  and not exists (
		select 1 
			from Controller_Sensor_Log e
			where e.Vehicle_ID = v.Vehicle_ID
			  and e.Log_Time = ci.Log_Time
			  and e.Number = cs.Number
	  )
	  and ci.Log_Time between 0 and datediff(ss, ''1970'', getutcdate())
	  and ci.' + sc.name + ' is not null
'	
	from Controller_Sensor cs
	left outer join sys.columns sc on 
		sc.object_id = object_id('Can_Info') 
		and (substring(Descript, 5, len(Descript)-4) = replace(name, '_', '')
		 or  cs.Descript = 'CAN_RunToCarMaintenance' and sc.Name = 'RUN_TO_MNTNC')
	where cs.Controller_Type_ID = 10 
	  and cs.Descript like 'CAN%' 
	  and cs.Mandatory = 1
	  