select '
declare @vehicle table(id int primary key clustered)
insert into @vehicle 
	select v.vehicle_id 
		from vehicle v 
		join department d on d.department_id = v.department 
		join controller c on c.vehicle_id = v.vehicle_id
		join Controller_Type ct on ct.Controller_Type_ID = c.Controller_Type_ID
		where d.name like ''%�������%''
	     and ct.type_name = ''AvtoGrafCan''

declare @vehicle_id int
declare @number int = (select number from controller_sensor cs where cs.controller_type_id = 10 and cs.descript = ''' + cs.Descript + ''')
declare @multiplier numeric(18,9) = (select Default_Multiplier from controller_sensor cs where cs.controller_type_id = 10 and cs.descript = ''' + cs.Descript + ''')

while 1=1
begin
	set nocount on
	
	set @vehicle_id = (select top(1) id from @vehicle order by id asc)
	if (@vehicle_id is null)
		break;

	delete from @vehicle where id = @vehicle_id
	
	set nocount off
	
	print ''Processing: '' + convert(varchar(12), @vehicle_id)
	
	update csl
		set value = convert(bigint, ci.' + sc.Name + ') / @multiplier
		from can_info ci
		join controller_sensor_log csl
			on    csl.vehicle_id = ci.vehicle_id 
			  and csl.log_time = ci.log_Time 
			  and csl.number = @number
		where   ci.vehicle_id = @vehicle_id
		    and ci.' + sc.Name + ' = csl.value
			and ci.log_time < datediff(ss, ''1970'', ''2012-12-18'')	

	WAITFOR DELAY ''00:00:05''
	
end
go
'	
	from Controller_Sensor cs
	left outer join sys.columns sc on 
		sc.object_id = object_id('Can_Info') 
		and (substring(Descript, 5, len(Descript)-4) = replace(name, '_', '')
		 or  cs.Descript = 'CAN_RunToCarMaintenance' and sc.Name = 'RUN_TO_MNTNC')
	where cs.Controller_Type_ID = 10 
	  and cs.Descript like 'CAN%' 
	  and cs.Mandatory = 1
	  and cs.Default_Multiplier <> 1
	  