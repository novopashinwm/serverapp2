insert into CONTROLLER_SENSOR_MAP (controller_id, CONTROLLER_SENSOR_ID, CONTROLLER_SENSOR_LEGEND_ID, MULTIPLIER, CONSTANT)
	select c.controller_id, cs.CONTROLLER_SENSOR_ID, cs.Default_Sensor_Legend_ID, cs.Default_Multiplier, cs.Default_Constant
	from controller c
	join controller_sensor cs on cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
	where cs.Mandatory = 1
	  and c.VEHICLE_ID = 38010
	  and not exists (Select * from controller_Sensor_Map e where e.CONTROLLER_ID = c.CONTROLLER_ID and e.CONTROLLER_SENSOR_LEGEND_ID = cs.Default_Sensor_Legend_ID)