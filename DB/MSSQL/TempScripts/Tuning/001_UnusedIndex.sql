﻿SELECT TOP(25)
	 ObjectName       = o.[name]
	,IndexName        = i.[name]
	,IndexID          = i.[index_id]
	,UserSeeks        = dm_ius.[user_seeks]
	,UserScans        = dm_ius.[user_scans]
	,UserLookups      = dm_ius.[user_lookups]
	,UserUpdates      = dm_ius.[user_updates]
	,TableRows        = p.[TableRows]
	,[drop statement] = 
		  'DROP INDEX ' + QUOTENAME(i.[name]) 
		+ ' ON '        + QUOTENAME(s.[name])
		+ '.'           + QUOTENAME(OBJECT_NAME(dm_ius.[object_id]))
FROM sys.dm_db_index_usage_stats dm_ius  
	INNER JOIN sys.indexes i
		ON i.index_id = dm_ius.index_id AND dm_ius.object_id = i.object_id
	INNER JOIN sys.objects o
		ON dm_ius.object_id = o.object_id
	INNER JOIN sys.schemas s
		ON o.schema_id = s.schema_id
	INNER JOIN 
	(
		SELECT SUM(p.rows) TableRows, p.index_id, p.object_id 
		FROM sys.partitions p
		GROUP BY p.index_id, p.object_id
	) p 
		ON p.index_id = dm_ius.index_id AND dm_ius.object_id = p.object_id
WHERE OBJECTPROPERTY(dm_ius.object_id,'IsUserTable') = 1
AND   dm_ius.database_id     = DB_ID()   
AND   i.type_desc            = 'nonclustered'
AND   i.is_primary_key       = 0
AND   i.is_unique_constraint = 0
ORDER BY (dm_ius.user_seeks + dm_ius.user_scans + dm_ius.user_lookups) ASC
GO