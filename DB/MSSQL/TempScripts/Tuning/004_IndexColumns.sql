﻿WITH IndexInfo AS
(
	SELECT
		 [SchemaName] = Sch.[name]
		,[TableName]  = Obj.[name]
		,[IndexName]  = Idx.[name]
		,[Col01]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  1)
		,[Col02]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  2)
		,[Col03]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  3)
		,[Col04]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  4)
		,[Col05]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  5)
		,[Col06]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  6)
		,[Col07]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  7)
		,[Col08]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  8)
		,[Col09]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  9)
		,[Col10]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 10)
		,[Col11]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 11)
		,[Col12]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 12)
		,[Col13]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 13)
		,[Col14]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 14)
		,[Col15]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 15)
		,[Col16]      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 16)
	FROM sys.indexes Idx
		INNER JOIN sys.objects Obj
			ON Obj.[object_id] = Idx.[object_id]
				INNER JOIN sys.schemas Sch
					ON Sch.[schema_id] = Obj.[schema_id]
	WHERE 1 = 1
	AND   Idx.[index_id]  > 0
	AND   Obj.[object_id] > 1024
	AND   Obj.[type]      = N'U'
)
SELECT
	*
FROM IndexInfo
WHERE 1 = 1
AND   [Col01] = N'Vehicle_ID'
AND   [Col02] = N'LOG_TIME'
ORDER BY [SchemaName], [TableName], [IndexName]
GO