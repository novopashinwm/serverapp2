﻿DECLARE @TOPROWS int = 5
--Потерянные индексы первая пятерка по стоимости
SELECT TOP(@TOPROWS)
	 CAST(migs.avg_total_user_cost * migs.avg_user_impact * (migs.user_seeks + migs.user_scans) AS money)
	,migs.group_handle
	,migs.user_scans
	,migs.user_seeks
	,mid.*
FROM sys.dm_db_missing_index_group_stats AS migs
	INNER JOIN sys.dm_db_missing_index_groups AS mig
		ON (migs.group_handle = mig.index_group_handle)
			INNER JOIN sys.dm_db_missing_index_details AS mid
				ON (mig.index_handle = mid.index_handle)
WHERE database_id = DB_ID()
ORDER BY migs.avg_total_user_cost * migs.avg_user_impact * (migs.user_seeks + migs.user_scans) DESC;
--Статистика по использованию индексов первая пятерка по стоимости
SELECT TOP(@TOPROWS)
	 TotalCost  =	
		(user_scans   + user_seeks   + user_lookups   + user_updates)
		+
		(system_scans + system_seeks + system_lookups + system_updates)
	,UsersCost  =
		(user_scans   + user_seeks   + user_lookups   + user_updates)
	,SystemCost =
		(system_scans + system_seeks + system_lookups + system_updates)
	,OBJECT_NAME(object_id)
,* 
FROM sys.dm_db_index_usage_stats
WHERE database_id = DB_ID()
ORDER BY
		(user_scans   + user_seeks   + user_lookups   + user_updates)
		+
		(system_scans + system_seeks + system_lookups + system_updates)
DESC;
--Статистика использования индексов
SELECT TOP(@TOPROWS)
	 [OBJECT NAME] = OBJECT_NAME(S.[OBJECT_ID])
	,[INDEX NAME]  = I.[NAME]
	,USER_SEEKS
	,USER_SCANS
	,USER_LOOKUPS
	,USER_UPDATES
FROM SYS.DM_DB_INDEX_USAGE_STATS AS S
	INNER JOIN SYS.INDEXES AS I
		ON  I.[OBJECT_ID] = S.[OBJECT_ID]
		AND I.[INDEX_ID]  = S.[INDEX_ID]
WHERE database_id = DB_ID()
AND   OBJECTPROPERTY(S.[OBJECT_ID], 'IsUserTable') = 1 
ORDER BY (USER_SEEKS + USER_SCANS + USER_LOOKUPS + USER_UPDATES) DESC;
--Участие индексов в операциях изменения данных
SELECT TOP(@TOPROWS)
 [OBJECT NAME] = OBJECT_NAME(A.[OBJECT_ID])
,[INDEX NAME]  = I.[NAME]
,A.LEAF_INSERT_COUNT
,A.LEAF_UPDATE_COUNT
,A.LEAF_DELETE_COUNT
FROM SYS.DM_DB_INDEX_OPERATIONAL_STATS (NULL, NULL, NULL, NULL) A
	INNER JOIN SYS.INDEXES AS I
		ON  I.[OBJECT_ID] = A.[OBJECT_ID]
		AND I.[INDEX_ID]  = A.[INDEX_ID]
WHERE database_id = DB_ID()
AND   OBJECTPROPERTY(A.[OBJECT_ID], 'IsUserTable') = 1 
ORDER BY (A.LEAF_INSERT_COUNT + A.LEAF_UPDATE_COUNT + A.LEAF_DELETE_COUNT) DESC;
--Самые большие индексы
SELECT TOP(@TOPROWS)
 [OBJECT NAME] = OBJECT_NAME(A.[OBJECT_ID])
,[INDEX NAME]  = I.[NAME]
,A.*
FROM SYS.DM_DB_INDEX_PHYSICAL_STATS(NULL, NULL, NULL, NULL, NULL) A
	INNER JOIN SYS.INDEXES AS I
		ON  I.[OBJECT_ID] = A.[OBJECT_ID]
		AND I.[INDEX_ID]  = A.[INDEX_ID]
WHERE database_id = DB_ID()
AND   OBJECTPROPERTY(A.[OBJECT_ID], 'IsUserTable') = 1 
ORDER BY (A.page_count) DESC;
-- Самые большие объекты БД
SELECT TOP(@TOPROWS)
	 table_name       = o.name
	,index_id         = p.index_id
	,index_name       = i.name
	,allocation_type  = au.type_desc
	,data_pages       = au.data_pages
	,partition_number = p.partition_number
FROM sys.allocation_units AS au
	JOIN sys.partitions AS p 
		ON au.container_id = p.partition_id
	JOIN sys.objects AS o
		ON p.object_id = o.object_id
	JOIN sys.indexes AS i
		ON  p.index_id  = i.index_id
		AND i.object_id = p.object_id
WHERE OBJECTPROPERTY(O.[OBJECT_ID], 'IsUserTable') = 1 
ORDER BY au.data_pages DESC;
-- Скрипты создания отсутствующих индексов
SELECT
	[Impact] = (avg_total_user_cost * avg_user_impact) * (user_seeks + user_scans),  
	[Table] = [statement],
	[CreateIndexStatement] = 'CREATE NONCLUSTERED INDEX ix_' 
		+ sys.objects.name COLLATE DATABASE_DEFAULT 
		+ '_' 
		+ REPLACE(REPLACE(REPLACE(ISNULL(mid.equality_columns,'')+ISNULL(mid.inequality_columns,''), '[', ''), ']',''), ', ','_')
		+ ' ON ' 
		+ [statement] 
		+ ' ( ' + IsNull(mid.equality_columns, '') 
		+ CASE WHEN mid.inequality_columns IS NULL THEN '' ELSE 
			CASE WHEN mid.equality_columns IS NULL THEN '' ELSE ',' END 
		+ mid.inequality_columns END + ' ) ' 
		+ CASE WHEN mid.included_columns IS NULL THEN '' ELSE 'INCLUDE (' + mid.included_columns + ')' END 
		+ ';', 
	mid.equality_columns,
	mid.inequality_columns,
	mid.included_columns
FROM sys.dm_db_missing_index_group_stats AS migs 
	INNER JOIN sys.dm_db_missing_index_groups AS mig ON migs.group_handle = mig.index_group_handle 
	INNER JOIN sys.dm_db_missing_index_details AS mid ON mig.index_handle = mid.index_handle 
	INNER JOIN sys.objects WITH (nolock) ON mid.OBJECT_ID = sys.objects.OBJECT_ID 
WHERE (migs.group_handle IN 
		(SELECT TOP (500) group_handle 
		FROM sys.dm_db_missing_index_group_stats WITH (nolock) 
		ORDER BY (avg_total_user_cost * avg_user_impact) * (user_seeks + user_scans) DESC))  
	AND OBJECTPROPERTY(sys.objects.OBJECT_ID, 'isusertable') = 1 
ORDER BY [Impact] DESC , [CreateIndexStatement] DESC