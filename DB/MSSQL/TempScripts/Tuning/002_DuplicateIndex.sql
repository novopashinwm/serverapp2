﻿WITH IndexInfo AS
(
	SELECT
		 SchemaName = Sch.[name]
		,TableName  = Obj.[name]
		,IndexName  = Idx.[name]
		,Col01      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  1)
		,Col02      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  2)
		,Col03      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  3)
		,Col04      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  4)
		,Col05      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  5)
		,Col06      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  6)
		,Col07      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  7)
		,Col08      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  8)
		,Col09      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id],  9)
		,Col10      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 10)
		,Col11      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 11)
		,Col12      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 12)
		,Col13      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 13)
		,Col14      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 14)
		,Col15      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 15)
		,Col16      = INDEX_COL(Sch.[name] + '.' + Obj.[name], Idx.[index_id], 16)
	FROM sys.indexes Idx
		INNER JOIN sys.objects Obj
			ON Obj.[object_id] = Idx.[object_id]
				INNER JOIN sys.schemas Sch
					ON Sch.[schema_id] = Obj.[schema_id]
	WHERE Idx.[index_id]  > 0
	AND   Obj.[object_id] > 1024
)
SELECT
	 II1.[SchemaName]
	,II1.[TableName]
	,II1.[IndexName]
	,II2.[IndexName] AS OverLappingIndex
	,II1.[Col01], II1.[Col02], II1.[Col03], II1.[Col04]
	,II1.[Col05], II1.[Col06], II1.[Col07], II1.[Col08]
	,II1.[Col09], II1.[Col10], II1.[Col11], II1.[Col12]
	,II1.[Col13], II1.[Col14], II1.[Col15], II1.[Col16]
FROM IndexInfo II1
	INNER JOIN IndexInfo II2
		ON  II1.[TableName] =  II2.[TableName]
		AND II1.[IndexName] <> II2.[IndexName]
		AND (II1.[Col01] = II2.[Col01])
		AND (II1.[Col02] IS NULL OR II2.[Col02] IS NULL OR II1.[Col02] = II2.[Col02])
		AND (II1.[Col03] IS NULL OR II2.[Col03] IS NULL OR II1.[Col03] = II2.[Col03])
		AND (II1.[Col04] IS NULL OR II2.[Col04] IS NULL OR II1.[Col04] = II2.[Col04])
		AND (II1.[Col05] IS NULL OR II2.[Col05] IS NULL OR II1.[Col05] = II2.[Col05])
		AND (II1.[Col06] IS NULL OR II2.[Col06] IS NULL OR II1.[Col06] = II2.[Col06])
		AND (II1.[Col07] IS NULL OR II2.[Col07] IS NULL OR II1.[Col07] = II2.[Col07])
		AND (II1.[Col08] IS NULL OR II2.[Col08] IS NULL OR II1.[Col08] = II2.[Col08])
		AND (II1.[Col09] IS NULL OR II2.[Col09] IS NULL OR II1.[Col09] = II2.[Col09])
		AND (II1.[Col10] IS NULL OR II2.[Col10] IS NULL OR II1.[Col10] = II2.[Col10])
		AND (II1.[Col11] IS NULL OR II2.[Col11] IS NULL OR II1.[Col11] = II2.[Col11])
		AND (II1.[Col12] IS NULL OR II2.[Col12] IS NULL OR II1.[Col12] = II2.[Col12])
		AND (II1.[Col13] IS NULL OR II2.[Col13] IS NULL OR II1.[Col13] = II2.[Col13])
		AND (II1.[Col14] IS NULL OR II2.[Col14] IS NULL OR II1.[Col14] = II2.[Col14])
		AND (II1.[Col15] IS NULL OR II2.[Col15] IS NULL OR II1.[Col15] = II2.[Col15])
		AND (II1.[Col16] IS NULL OR II2.[Col16] IS NULL OR II1.[Col16] = II2.[Col16])
ORDER BY
	II1.[SchemaName], II1.[TableName], II1.[IndexName]
GO