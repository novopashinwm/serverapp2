﻿SELECT TOP(25)
	 [object_id]
	,[Database]            = DB_NAME    (             [database_id])
	,[proc name]           = OBJECT_NAME([object_id], [database_id])
	,[cached_time]
	,[last_execution_time]
	,[execution_count]
	,[last_elapsed_time]
	,[min_elapsed_time]
	,[max_elapsed_time]
	,[total_elapsed_time]
	,[avg_elapsed_time]   = [total_elapsed_time]/[execution_count]
	--,[exec_per_minute]    = [execution_count] / DATEDIFF(MINUTE, [cached_time], [last_execution_time])
FROM sys.dm_exec_procedure_stats
WHERE [database_id] = DB_ID()
--AND [execution_count] > 10000
--AND 	OBJECT_NAME([object_id], [database_id]) LIKE '%storedprocname%'
AND
(
	   OBJECT_NAME([object_id], [database_id]) LIKE '%AddMonitoreeLog%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%AddSensorValues%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%AddPositionRadius%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%AddCell_Network_Log%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%AddWLAN_Log%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%AddPictureLog%'
	OR OBJECT_NAME([object_id], [database_id]) LIKE '%UpdateInsertTime%'
)
ORDER BY [total_worker_time] DESC;

