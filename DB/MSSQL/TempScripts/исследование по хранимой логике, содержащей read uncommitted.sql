drop table #objects;
drop table #text;

create table #objects ([name] varchar(768), id int identity, idfrom int, idto int);
create table #text ([text] nvarchar(max), id int identity);

insert into #objects ([name])
	select name from sys.objects where type in ('p', 'FN')

--select * from #objects

declare @i int, @maxI int
select @i = min(id), @maxI = max(id) from #objects
while @i <= @maxI
begin
	
	declare @name nvarchar(768);
	select @name = [name] from #objects where id = @i
	update #objects 
		set idfrom = (select max(id) + 1 from #text)
		where id = @i

	insert into #text 
		exec sp_helptext @name

	update #objects 
		set idto = (select max(id) from #text)
		where id = @i
	
	set @i = @i + 1
end;

select * from #text t
join #objects o on t.id between o.idfrom and o.idto
where [text] like '%uncommitted%'

