SELECT
	[RGT].[NAME],
	[CommandTypes]                           = ISNULL([CommandTypes]                           ,0),
	[OPERATOR_DEPARTMENT]                    = ISNULL([OPERATOR_DEPARTMENT]                    ,0),
	[OPERATOR_DRIVER]                        = ISNULL([OPERATOR_DRIVER]                        ,0),
	[OPERATOR_DRIVERGROUP]                   = ISNULL([OPERATOR_DRIVERGROUP]                   ,0),
	[Operator_OperatorGroup]                 = ISNULL([Operator_OperatorGroup]                 ,0),
	[OPERATOR_REPORT]                        = ISNULL([OPERATOR_REPORT]                        ,0),
	[OPERATOR_ROUTE]                         = ISNULL([OPERATOR_ROUTE]                         ,0),
	[OPERATOR_ROUTEGROUP]                    = ISNULL([OPERATOR_ROUTEGROUP]                    ,0),
	[OPERATOR_VEHICLE]                       = ISNULL([OPERATOR_VEHICLE]                       ,0),
	[OPERATOR_VEHICLEGROUP]                  = ISNULL([OPERATOR_VEHICLEGROUP]                  ,0),
	[OPERATOR_ZONE]                          = ISNULL([OPERATOR_ZONE]                          ,0),
	[OPERATOR_ZONEGROUP]                     = ISNULL([OPERATOR_ZONEGROUP]                     ,0),
	[OperatorGroup_Billing_Service_Provider] = ISNULL([OperatorGroup_Billing_Service_Provider] ,0),
	[OPERATORGROUP_DEPARTMENT]               = ISNULL([OPERATORGROUP_DEPARTMENT]               ,0),
	[OPERATORGROUP_DRIVER]                   = ISNULL([OPERATORGROUP_DRIVER]                   ,0),
	[OPERATORGROUP_DRIVERGROUP]              = ISNULL([OPERATORGROUP_DRIVERGROUP]              ,0),
	[OPERATORGROUP_REPORT]                   = ISNULL([OPERATORGROUP_REPORT]                   ,0),
	[OPERATORGROUP_ROUTE]                    = ISNULL([OPERATORGROUP_ROUTE]                    ,0),
	[OPERATORGROUP_ROUTEGROUP]               = ISNULL([OPERATORGROUP_ROUTEGROUP]               ,0),
	[OPERATORGROUP_VEHICLE]                  = ISNULL([OPERATORGROUP_VEHICLE]                  ,0),
	[OPERATORGROUP_VEHICLEGROUP]             = ISNULL([OPERATORGROUP_VEHICLEGROUP]             ,0),
	[OPERATORGROUP_ZONE]                     = ISNULL([OPERATORGROUP_ZONE]                     ,0),
	[OPERATORGROUP_ZONEGROUP]                = ISNULL([OPERATORGROUP_ZONEGROUP]                ,0),
	[RIGHT_OPERATOR]                         = ISNULL([RIGHT_OPERATOR]                         ,0),
	[RIGHT_OPERATORGROUP]                    = ISNULL([RIGHT_OPERATORGROUP]                    ,0)
FROM
(
	-- ������ ����������� �� RIGHT
	      SELECT [TBL]=RTRIM('CommandTypes                                                '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[CommandTypes]                                                 GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_DEPARTMENT                                         '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_DEPARTMENT]                                          GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_DRIVER                                             '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_DRIVER]                                              GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_DRIVERGROUP                                        '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_DRIVERGROUP]                                         GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('Operator_OperatorGroup                                      '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[Operator_OperatorGroup]                                       GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_REPORT                                             '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_REPORT]                                              GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_ROUTE                                              '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_ROUTE]                                               GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_ROUTEGROUP                                         '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_ROUTEGROUP]                                          GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_VEHICLE                                            '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_VEHICLE]                                             GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_VEHICLEGROUP                                       '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_VEHICLEGROUP]                                        GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_ZONE                                               '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_ZONE]                                                GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATOR_ZONEGROUP                                          '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATOR_ZONEGROUP]                                           GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OperatorGroup_Billing_Service_Provider                      '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OperatorGroup_Billing_Service_Provider]                       GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_DEPARTMENT                                    '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_DEPARTMENT]                                     GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_DRIVER                                        '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_DRIVER]                                         GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_DRIVERGROUP                                   '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_DRIVERGROUP]                                    GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_REPORT                                        '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_REPORT]                                         GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_ROUTE                                         '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_ROUTE]                                          GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_ROUTEGROUP                                    '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_ROUTEGROUP]                                     GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_VEHICLE                                       '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_VEHICLE]                                        GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_VEHICLEGROUP                                  '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_VEHICLEGROUP]                                   GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_ZONE                                          '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_ZONE]                                           GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('OPERATORGROUP_ZONEGROUP                                     '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[OPERATORGROUP_ZONEGROUP]                                      GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('RIGHT_OPERATOR                                              '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[RIGHT_OPERATOR]                                               GROUP BY [RIGHT_ID]
	UNION SELECT [TBL]=RTRIM('RIGHT_OPERATORGROUP                                         '), [RGT]=[RIGHT_ID], [CNT]=COUNT(*) FROM [dbo].[RIGHT_OPERATORGROUP]                                          GROUP BY [RIGHT_ID]
) SRC
PIVOT
(
	SUM([CNT]) FOR [TBL] IN
	(
		[CommandTypes],
		[OPERATOR_DEPARTMENT],
		[OPERATOR_DRIVER],
		[OPERATOR_DRIVERGROUP],
		[Operator_OperatorGroup],
		[OPERATOR_REPORT],
		[OPERATOR_ROUTE],
		[OPERATOR_ROUTEGROUP],
		[OPERATOR_VEHICLE],
		[OPERATOR_VEHICLEGROUP],
		[OPERATOR_ZONE],
		[OPERATOR_ZONEGROUP],
		[OperatorGroup_Billing_Service_Provider],
		[OPERATORGROUP_DEPARTMENT],
		[OPERATORGROUP_DRIVER],
		[OPERATORGROUP_DRIVERGROUP],
		[OPERATORGROUP_REPORT],
		[OPERATORGROUP_ROUTE],
		[OPERATORGROUP_ROUTEGROUP],
		[OPERATORGROUP_VEHICLE],
		[OPERATORGROUP_VEHICLEGROUP],
		[OPERATORGROUP_ZONE],
		[OPERATORGROUP_ZONEGROUP],
		[RIGHT_OPERATOR],
		[RIGHT_OPERATORGROUP]
	)
) PVT
	LEFT JOIN [dbo].[RIGHT] RGT
		ON RGT.[RIGHT_ID] = PVT.[RGT]
WHERE 1 = 1
AND RGT.[NAME] = N'ManageSensors'
ORDER BY PVT.[RGT];