insert into MESSAGE (SUBJECT, BODY, DESTINATION_ID, DestinationType_ID, TYPE, TIME, ProcessingResult)
	select convert(varchar, getdate()) + ': subject', 'Very very long long body here, one two three, how many text should I write to test the case? It seems to be working!', 
	ac.ID, 9, CONVERT(int, 0x4000) + CONVERT(int, 0x08), GETUTCDATE(), 1
		from AppClient ac
		where ac.Operator_ID = 1424
		  and ac.GcmRegistrationId is not null
		order by ac.ID desc
	