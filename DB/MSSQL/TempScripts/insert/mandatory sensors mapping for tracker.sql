declare @type_name nvarchar(255) = 'TM140'

insert into Controller_Sensor_Map (Controller_ID, Controller_Sensor_Legend_ID, Controller_Sensor_ID, Multiplier, Constant)
	select c.Controller_ID
	     , cs.Default_Sensor_Legend_ID
	     , cs.Controller_Sensor_ID
	     , cs.Default_Multiplier
	     , cs.Default_Constant
	     from Controller c
	     join Controller_Type ct on ct.Controller_Type_ID = c.Controller_Type_ID
	     join Controller_Sensor cs on cs.Controller_Type_ID = ct.Controller_Type_ID
	     join Controller_Info ci on ci.Controller_ID = c.Controller_ID
	     where ct.Type_Name = @type_name
	       and cs.Mandatory = 1
	       and len(ci.Device_ID) > 0
	       and not exists (
			select * 
				from Controller_Sensor_Map e 
				where e.Controller_ID = c.Controller_ID 
				  and e.Controller_Sensor_Legend_ID = cs.Default_Sensor_Legend_ID)
				  
