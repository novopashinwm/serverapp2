declare @log_time int = datediff(ss, '1970', getutcdate()-1)

insert into VehicleNetmonitoring
select v.Vehicle_ID, @log_time, newid()
from Vehicle v
where 
(
exists (select * from Cell_Network_Log l where l.Vehicle_ID = v.Vehicle_ID and log_time > @log_time)
or
exists (select * from WLAN_Log l where l.Vehicle_ID = v.Vehicle_ID and log_time > @log_time)
)
and v.Vehicle_ID not in (select Vehicle_ID from VehicleNetmonitoring )