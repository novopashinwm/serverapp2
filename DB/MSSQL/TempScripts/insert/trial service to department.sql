insert into billing_service (Billing_Service_Type_ID, StartDate, Department_ID, MaxQuantity)
	select bst.ID, getutcdate(), d.Department_ID, bst.MaxQuantity
		from department d, Billing_Service_Type bst
		where d.name = '�� �������'
		  and bst.Service_Type = 'FRNIKA.SMS.Trial'

declare @bs_id int = @@identity

--(select bs.ID from Billing_Service bs 
--						join Department d on d.DEPARTMENT_ID = bs.Department_ID
--						join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
--						where d.name = '�� �������'
--						  and bst.Service_Type = 'FRNIKA.SMS.Trial')

insert into schedulerqueue (SCHEDULEREVENT_ID, NEAREST_TIME, REPETITION_XML, Enabled)
	select 
		se.SchedulerEvent_ID
		, dateadd(month, 1, GETUTCDATE())
		, '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:clr="http://schemas.microsoft.com/soap/encoding/clr/1.0" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">  <SOAP-ENV:Body>  <a1:RepeatOnNthDayOfMonth id="ref-1" xmlns:a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces">  <n>1</n>  <month>0</month>  <comment xsi:null="1"/>  </a1:RepeatOnNthDayOfMonth>  </SOAP-ENV:Body>  </SOAP-ENV:Envelope>'
		, 1
	from SCHEDULEREVENT se
	where se.COMMENT = 'Reset billing service counter value'
	
declare @sq_id int = @@identity

insert into Billing_Service_SchedulerQueue select @bs_id, @sq_id
