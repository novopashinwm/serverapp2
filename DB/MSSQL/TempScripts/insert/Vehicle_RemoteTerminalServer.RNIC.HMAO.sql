--with Device_ID as (
--select Value = convert(varbinary(32), t.Value)
--	from (
--		select Value = '' where 1=0
--		union all select '200716'
--		union all select '201001'
--		union all select '200715'
--		union all select '200718'
--		union all select '201303'
--		union all select '201000'
--		union all select '201004'
--		union all select '201008'
--		union all select '201002'
--		union all select '201003'
--	) t
--)
insert into Vehicle_RemoteTerminalServer
	select v.Vehicle_ID, rts.RemoteTerminalServer_ID
		from Vehicle v
		left outer join Department d on d.Department_ID = v.Department
		join Controller c on c.Vehicle_ID = v.Vehicle_ID
		join Controller_Info ci on ci.Controller_ID = c.Controller_ID
		join RemoteTerminalServer rts on rts.Name = N'���� ���� ����������� ��'
		where 1=1
		--ci.Device_ID in (select Device_ID.Value from Device_ID)
		and d.Name = '�����������'
		and not exists (select * from Vehicle_RemoteTerminalServer e where e.Vehicle_ID = v.Vehicle_ID and e.RemoteTerminalServer_ID = rts.RemoteTerminalServer_ID)