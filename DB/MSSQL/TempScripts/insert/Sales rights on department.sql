select * 

insert into OPERATORGROUP_DEPARTMENT (OPERATORGROUP_ID, DEPARTMENT_ID, RIGHT_ID, ALLOWED)
	select og.OPERATORGROUP_ID, d.DEPARTMENT_ID, r.RIGHT_ID, 1
	from OPERATORGROUP og, [RIGHT] r, DEPARTMENT d
	where og.NAME = 'sales' and r.NAME = 'managesensors'
	and not exists (
		select * 
			from OPERATORGROUP_DEPARTMENT e 
			where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID 
			  and e.DEPARTMENT_ID = d.DEPARTMENT_ID 
			  and e.RIGHT_ID = r.RIGHT_ID)