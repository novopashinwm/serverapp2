insert into OperatorGroup (Name, Comment)
	select t.Name, t.Comment
		from (select Name = 'India.Admin', Comment = 'Administrators and sales of ex-SitronicsIndia account') t
		where not exists (
			select * from OperatorGroup e where e.Name = t.Name)
			

insert into OPERATORGROUP_Country (OperatorGroup_ID, Country_ID)
	select og.OperatorGroup_ID, c.Country_ID
		from OPERATORGROUP og
		join Country c on c.Name = 'India'
		where og.NAME = 'India.Admin'
		  and not exists (select 1 from OPERATORGROUP_Country e where e.operatorGroup_ID = og.OPERATORGROUP_ID and e.Country_ID = c.Country_ID)
		  
	
insert into RIGHT_OPERATORGROUP
	select r.right_id, og.operatorgroup_id, 1
		from [right] r 
		join operatorgroup og on og.NAME = 'India.Admin'
		where not exists (select 1 from RIGHT_OPERATORGROUP e where e.RIGHT_ID = r.RIGHT_ID and e.OPERATORGROUP_ID = og.OPERATORGROUP_ID)
		  and r.NAME = 'ServiceManagement'

insert into OPERATORGROUP_DEPARTMENT 
	select og.OperatorGroup_ID, od.Department_ID, od.Right_ID, od.Allowed
		from Operator o
		join OPERATOR_DEPARTMENT od on od.OPERATOR_ID = o.OPERATOR_ID
		join Department d on d.DEPARTMENT_ID = od.DEPARTMENT_ID
		join OPERATORGROUP og on og.NAME = 'India.Admin'
		join Country c on c.Country_ID = d.Country_ID
		where o.LOGIN = 'ppjha'
		  and c.Name = 'India'
		  and not exists (
			select *
				from OPERATORGROUP_DEPARTMENT e
				where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID
				  and e.DEPARTMENT_ID = od.DEPARTMENT_ID
				  and e.RIGHT_ID = od.RIGHT_ID)

insert into OPERATORGROUP_DEPARTMENT 
	select ogd.OPERATORGROUP_ID, d.DEPARTMENT_ID, ogd.Right_ID, ogd.ALLOWED
		from OperatorGroup og
		join DEPARTMENT d on d.Country_ID = (select c.Country_ID from Country c where c.Name = 'India')
		join OPERATORGROUP_DEPARTMENT ogd on ogd.OPERATORGROUP_ID = og.OPERATORGROUP_ID and ogd.RIGHT_ID = 104
		where og.NAME = 'India.Admin'
		  and not exists (
			select *
				from OPERATORGROUP_DEPARTMENT e
				where e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID
				  and e.DEPARTMENT_ID = d.DEPARTMENT_ID
				  and e.RIGHT_ID = ogd.RIGHT_ID)
	
insert into OPERATORGROUP_OPERATOR 
	select og.OperatorGroup_ID, o.Operator_ID
		from OperatorGroup og
		join Operator o on o.LOGIN = 'ppjha'
		where og.NAME = 'India.Admin'
		  and not exists (Select * from OPERATORGROUP_OPERATOR e where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID and e.OPERATOR_ID = o.operator_id)
