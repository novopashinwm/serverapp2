declare @contractNumber varchar(100) = '123306870619'
declare @departmentName varchar(100) = '������-���������'

declare @phone_asid table (phone varchar(32), asid varchar(255))

insert into @phone_asid 
select '79883665410', 'p3krad79762976d1b3e4982350cfcc78d4584'	union all
select '79883673488', 'p3kra89535f89ade89d2c79aeb9fa1fd95f93'	union all
select '79883673491', 'p3kra89535f89ade89d2cb39e1e46bdc4cfc4'	union all
select '+79898262618', 'p3kra92b27cc1db1d7cbad8ce2459b383ab3c'	union all
select '+79898262619', 'p3kra92b27cc1db1d7cbaa3ac68ac1c9e5ae1'	union all
select '+79883665416', 'p3krad79762976d1b3e492d611f08e05478c0'
begin tran

print 'delete mlpc'	
	
delete mlpc
	from MLP_Controller mlpc
	join Asid a on a.ID = mlpc.Asid_ID
	join @phone_asid pa on pa.asid = a.Value
	join CONTROLLER c on c.CONTROLLER_ID = mlpc.Controller_ID
	where isnull(c.PHONE, '') <> pa.phone
	
print 'insert into MLP_Controller'

insert into MLP_Controller
	select c.CONTROLLER_ID, a.ID
	from 	VEHICLE v
	join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
	join @phone_asid pa on pa.phone = c.PHONE
	join DEPARTMENT d on d.DEPARTMENT_ID = v.DEPARTMENT and d.NAME = @departmentName
	join Asid a on a.Value = pa.asid
	where not exists (
		select * from MLP_Controller e where e.Asid_ID = a.ID and e.Controller_ID = c.CONTROLLER_ID)



print 'update v '

update v 
	set department = t.department_id
	from Vehicle v
	join Department s on s.Department_ID = v.Department
	join Department t on t.Name = @departmentName
	where s.ExtID = @contractNumber
	  and t.Name = @departmentName
	  and not exists (
		select * 
			from VEHICLE v
			join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
			join @phone_asid pa on pa.phone = c.PHONE
			join DEPARTMENT d on d.DEPARTMENT_ID = v.DEPARTMENT and d.NAME = @departmentName
			join Asid a on a.Value = pa.asid
	)

print 'update department (old)'

update department
	set Name = Name + ' ���������� � ' + @departmentName,
	    ExtID = null 
	where ExtID = @contractNumber
	  and Name <> @departmentName
	  
print 'update department (new)'

update department
	set ExtID = @contractNumber
	where Name = @departmentName
	
select a.Contract_Number, a.SimID, a.Terminal_Device_Number, a.Value, bs.Name, bs.StartDate, v.GARAGE_NUMBER, v.Device_ID, v.phone
	from Asid a
	outer apply (
		select bst.Name, bs.StartDate 
			from Billing_Service bs 
			join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
			
			where bs.Asid_ID = a.ID) bs
	outer apply (
		select v.GARAGE_NUMBER, Device_ID = CONVERT(varchar, ci.DEVICE_ID), c.PHONE
			from MLP_Controller mlpc
			join CONTROLLER c on c.CONTROLLER_ID = mlpc.Controller_ID
			join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
			left outer join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
			where mlpc.Asid_ID = a.ID) v
	where a.Value in (select pa.Asid from @phone_asid pa)
	
--rollback
commit