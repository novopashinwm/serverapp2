declare @vehicle_id int = 4773

insert into Vehicle_Rule (Vehicle_ID, Rule_ID, Value)
	select @vehicle_id, r.Rule_ID, 60
		from [RULE] r
		where r.RULE_ID in (16, 17)
		  and not exists (select * from VEHICLE_RULE e where e.VEHICLE_ID = @vehicle_id and e.RULE_ID = r.RULE_ID)
	