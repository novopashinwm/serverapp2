

select v.VEHICLE_ID, v.GARAGE_NUMBER, vg.NAME, ct.TYPE_NAME, CONVERT(varchar(32), ci.device_id)
from VEHICLE v
join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
left outer join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLE_ID = v.VEHICLE_ID
left outer join VEHICLEGROUP vg on vg.VEHICLEGROUP_ID = vgv.VEHICLEGROUP_ID
	where 
		  v.DEPARTMENT is null 
	  and ci.DEVICE_ID is not null
	  and v.VEHICLE_ID not in (select t.VEHICLE_ID from Tracker t where t.Vehicle_ID is not null)
	  and v.VEHICLE_ID not in (select c.vehicle_id
								from CONTROLLER c
								join MLP_Controller mlpc on mlpc.Controller_ID = c.CONTROLLER_ID
								where c.VEHICLE_ID is not null)
	  and vg.NAME like '%��%'
order by v.VEHICLE_ID
	  and c.NUMBER between 145196 and 145220 and ct.CONTROLLER_TYPE_ID = 10

select * from DEPARTMENT order by NAME

begin tran
update VEHICLE set DEPARTMENT = 52 where VEHICLE_ID between 1983 and 2007
commit
		

select d.DEPARTMENT_ID, d.name, vg.NAME from DEPARTMENT d
join VEHICLE v on v.DEPARTMENT = d.DEPARTMENT_ID
join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLE_ID = v.VEHICLE_ID
join VEHICLEGROUP vg on vg.VEHICLEGROUP_ID = vgv.VEHICLEGROUP_ID
where d.DEPARTMENT_ID = 8
where NAME like '%�����%'

select COUNT(1), ct.TYPE_NAME 
from VEHICLE v
join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
where DEPARTMENT = 8
group by ct.TYPE_NAME


select v.GARAGE_NUMBER, ct.TYPE_NAME
from VEHICLE v
join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
where ct.type_name in ('GPS608 CDMA','JB100','MeiligaoMVT380','MeiligaoVT300')

update v
--select v.GARAGE_NUMBER, vg.NAME
set v.DEPARTMENT = d.DEPARTMENT_ID
from VEHICLE v
join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLE_ID = v.VEHICLE_ID
join VEHICLEGROUP vg on vgv.VEHICLEGROUP_ID = vg.VEHICLEGROUP_ID
join DEPARTMENT d on d.NAME = vg.NAME
where v.DEPARTMENT is null
---group by v.VEHICLE_ID
having COUNT(1) > 1

begin tran

update v
	set DEPARTMENT = (
	select
		DEPARTMENT
	from
		(
			select
				h1.DEPARTMENT
				, h1.VEHICLE_ID
			from
				H_VEHICLE h1
			inner join (
				select
					MAX(id) as trid
					, VEHICLE_ID
				from	
					H_VEHICLE
				where
					DEPARTMENT is not null and DEPARTMENT in (select DEPARTMENT_id from DEPARTMENT)
				group by
					VEHICLE_ID ) t2
			on
				t2.trid = h1.id
		) tt2
	where
		tt2.VEHICLE_ID = v.vehicle_id)
from VEHICLE v
1
commit tran
