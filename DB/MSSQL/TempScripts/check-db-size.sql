select convert(varchar(255), [name]) [name]
	into #name
	from sys.objects
	where type in ('u', 'v')

create table #space_usage (
	name varchar(255) collate Cyrillic_General_CI_AI, 
	rows			bigint, 
	reserved		varchar(255), 
	data			varchar(255), 
	index_size		varchar(255), 
	unused			varchar(255),
	reservedBytes	bigint, 
	dataBytes		bigint, 
	index_sizeBytes bigint, 
	unusedBytes		bigint)

set nocount on
while 1=1
begin
	declare @name nvarchar(255)
	set @name = (select top(1) name from #name)

	if (@name is null)
		break

	delete #name where [name] = @name

	set @name = '[' + @name + ']'

	print @name

	insert into #space_usage ([name], rows, reserved, data, index_size, unused)
		exec sp_spaceused @name	
			
end

update #space_usage
	set reservedBytes =		1024 * convert(bigint, substring(reserved, 1, len(reserved)-3)),
		dataBytes =			1024 * convert(bigint, substring(data, 1, len(data)-3)),
		index_SizeBytes =	1024 * convert(bigint, substring(index_Size, 1, len(index_Size)-3)),
		unusedBytes =		1024 * convert(bigint, substring(unused, 1, len(unused)-3))

select top(20) [name], 
rows / 1024.0 / 1024.0						"Rows, M",
reservedBytes  / 1024.0 / 1024.0 / 1024.0	"Total, GB",
index_SizeBytes / 1024.0 / 1024.0 / 1024.0	"Index, GB",
dataBytes / 1024.0 / 1024.0 / 1024.0		"Data, GB",
unusedBytes / 1024.0 / 1024.0 / 1024.0		"Unused, GB",
reservedBytes / convert(float, rows)		"Total Bytes per row",
dataBytes / convert(float, rows)			"Data Bytes per row",
index_SizeBytes / convert(float, rows)		"Index Bytes per row"
from #space_usage
order by reservedBytes desc

select	sum(reservedBytes) / 1024.0 / 1024.0 / 1024.0,
		sum(unusedBytes) / 1024.0 / 1024.0 / 1024.0
from #space_usage where [name] in (select top(6) [name] from #space_usage order by reservedBytes desc)

drop table #name
drop table #space_usage