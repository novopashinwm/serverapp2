EXEC master.dbo.sp_addumpdevice
	@devtype = N'disk'
	, @logicalname = N'Remote_VirtualFS_1TB_full'
	, @physicalname = N'\\NIKA-BACKUP\NIKA\MED_TSS_full.bak'
EXEC master.dbo.sp_addumpdevice
	@devtype = N'disk'
	, @logicalname = N'Remote_VirtualFS_1TB_diff'
	, @physicalname = N'\\NIKA-BACKUP\NIKA\MED_TSS_diff.bak'
EXEC master.dbo.sp_addumpdevice
	@devtype = N'disk'
	, @logicalname = N'Remote_VirtualFS_1TB_tran'
	, @physicalname = N'\\NIKA-BACKUP\NIKA\MED_TSS_tran.bak'
	
/*
EXEC sp_dropdevice 'Remote_VirtualFS_1TB_full';
EXEC sp_dropdevice 'Remote_VirtualFS_1TB_diff';
EXEC sp_dropdevice 'Remote_VirtualFS_1TB_tran';
*/