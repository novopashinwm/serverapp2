declare @lng float, @lat float, @vehicle_id int, @operator_id int;


---������ ��������� ����
insert into #Primitives
	select z.zone_id, ROW_NUMBER() OVER (order by z.zone_id, gzp.[ORDER]), gzp.primitive_id
	from #ZonesList z
	join dbo.GEO_ZONE_PRIMITIVE as gzp (nolock) on gzp.ZONE_ID = z.zone_id
	order by z.zone_id, gzp.[ORDER];

---������ ����� ��������
insert into #PoligonePoints
	select zpv.PRIMITIVE_ID,
	ROW_NUMBER() OVER (order by zpv.PRIMITIVE_ID, zpv.[ORDER]), 	
	dbo.GeoToPlanarX(mv.X),
	dbo.GeoToPlanarY(mv.Y)
	from 
		#Primitives p 
		inner join ZONE_PRIMITIVE_VERTEX as zpv (nolock) on zpv.PRIMITIVE_ID = p.PRIMITIVE_ID
		inner join MAP_VERTEX as mv (nolock) on mv.VERTEX_ID = zpv.VERTEX_ID
	order by zpv.PRIMITIVE_ID, zpv.[ORDER];


with edges (minn , maxx, primitive_id) as
		(select min(pos), max(pos), primitive_id
		from #PoligonePoints 
		group by primitive_id
		)
insert into #AllPoligonePoints
select p1.pos, p1.x x1, p2.x x2, p1.y y1, p2.y y2, p.primitive_id, p.zone_id
		from #Primitives p
		join edges e on e.primitive_id = p.primitive_id
		join #PoligonePoints p1 on p1.primitive_id = p.primitive_id
		join #PoligonePoints p2 on 
			(p2.primitive_id = p.primitive_id
			and ((p2.pos = p1.pos-1 and p1.pos <> e.minn) or (p2.pos = e.maxx and p1.pos = e.minn))
			)