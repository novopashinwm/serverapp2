/* ��������� ���������, ������������� � ������ */
create table #Controller_Sensor_Map (
	Controller_ID int, 
	Controller_Sensor_ID int, 
	Controller_Sensor_Legend_ID int, 
	Description nvarchar(255), 
	Multiplier numeric(18,9), 
	Constant numeric(18,9));

insert into #Controller_Sensor_Map
(
	 CONTROLLER_ID
	,CONTROLLER_SENSOR_ID
	,CONTROLLER_SENSOR_LEGEND_ID
	,DESCRIPTION
	,MULTIPLIER
	,CONSTANT
)
select 
	c.controller_id,
	cs.controller_sensor_id,
	csl.controller_sensor_legend_id,
	t.Sensor_Name, 
	t.Multiplier,
	t.Constant
from (
		select 'LastPublishingDurationMilliseconds' Sensor_Name, 'LastPublishingDurationMilliseconds' Legend_Name, 1 Multiplier, 0 Constant
union	select 'ErrorSource'						Sensor_Name, 'ErrorSource' 						  Legend_Name, 1 Multiplier, 0 Constant
union   select 'ErrorCode'							Sensor_Name, 'ErrorCode' 						  Legend_Name, 1 Multiplier, 0 Constant
union   select 'TimeToFirstGPSFix'					Sensor_Name, 'TimeToFirstGPSFix'				  Legend_Name, 1 Multiplier, 0 Constant
	) t
join dbo.Controller c on 1=1
join dbo.Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID 
							 and cs.Descript = t.Sensor_Name
join dbo.Controller_Sensor_Legend csl on csl.Name = t.Legend_Name
join dbo.Vehicle v on v.Vehicle_ID = c.Vehicle_ID
where
v.Vehicle_ID = 780

insert into dbo.Controller_Sensor_Map
(
	 CONTROLLER_ID
	,CONTROLLER_SENSOR_ID
	,CONTROLLER_SENSOR_LEGEND_ID
	,DESCRIPTION
	,MULTIPLIER
	,CONSTANT
)
select * from #Controller_Sensor_Map csmt
where not exists (
	select 1 
		from dbo.Controller_Sensor_Map csm
		where csm.Controller_ID = csmt.Controller_ID
		and	  csm.Controller_Sensor_ID = csmt.Controller_Sensor_ID
		and	  csm.Controller_Sensor_Legend_ID = csmt.Controller_Sensor_Legend_ID
	)

update csm
	set Description = csmt.Description,
		Multiplier  = csmt.Multiplier,
		Constant    = csmt.Constant
	from dbo.Controller_Sensor_Map csm
	join #Controller_Sensor_Map csmt on 
			  csm.Controller_ID					= csmt.Controller_ID
		and	  csm.Controller_Sensor_ID			= csmt.Controller_Sensor_ID
		and	  csm.Controller_Sensor_Legend_ID	= csmt.Controller_Sensor_Legend_ID
