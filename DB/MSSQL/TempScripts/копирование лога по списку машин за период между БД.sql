/*	������ ��� ������� ������ �� ����� ���� � ������. 
	���� ����� ���� ����� ������� ��������� ��, ������ ������ ����������� �� �������-��������� ������.
	������ �� ��������� ������ � ��������� �� ���� (vehicle_id, log_time)
���������:
	1. @vehicle - ������� � ������������� vehicle_id �����, �� ������� ������� ������ ������
	2. @timeFrom
	3. @timeTo
	4. @sourceDatabase
	5. @targetDatabase
*/

declare @vehicle table(id int primary key clustered, idTo int);
insert into @vehicle (id, idTo) values (4010, 8214);


declare @targetDataBase varchar(max);
declare @sourceDataBase varchar(max);
declare @timeFromMain int, @timeToMain int

set @timeFromMain = datediff(ss, '1970-01-01', '2013.03.01') - 4 * 3600;
set @timeToMain   = datediff(ss, '1970-01-01', '2013.10.08') - 4 * 3600;

set @sourceDatabase = '[MED_TSS]';
set @targetDatabase = '[MED_TSS]';

declare @vehicleID int; 
set @vehicleID = (select min(id) from @vehicle);
declare @vehicleIDTo int

while @vehicleID is not null
begin
	set @vehicleIDTo = (select idTo from @vehicle where id = @vehicleID);

	declare @partTimeFrom int, @partTimeTo int;
	set @partTimeFrom = @timeFromMain;

	exec CopyLog @vehicleID, @vehicleIDTo, @timeFromMain, @timeToMain, @sourceDatabase, @targetDatabase, 1	
	
	set @vehicleID = (select min(id) from @vehicle where @vehicleID < id);
end;
