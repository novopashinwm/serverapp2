insert into Command  (Sender_ID, Target_ID, Type_ID, Date_Received)
	select top(1) lastCmd.Sender_ID, v.Vehicle_ID, lastCmd.Type_ID, dateadd(minute, 5, getutcdate())
	from Vehicle v
	cross apply (
		select top(1) c.Sender_ID, c.Type_ID, cr.Date_Received, cr.Result_Type_ID
			from Command c
			join Command_Result cr on cr.Command_ID = c.ID
			where c.Type_ID = 33 /*Capture Picture*/
			  and c.Target_ID = v.Vehicle_ID
			  order by c.Date_Received desc
	) lastCmd
	where lastCmd.Result_Type_ID not in (4 /*Cancelled*/, 1 /*Processing*/, 0 /*Received*/)
	  and lastCmd.Date_Received < getutcdate()
	  and lastCmd.Date_Received > dateadd(day, -1, getutcdate())

if (@@rowcount <> 0)
begin	  

	declare @command_id int = @@identity
	
	insert into Command_Result (Command_ID, Date_Received, Result_Type_ID, Processed)
		select c.ID, c.Date_Received, 0, 0
		from Command c where c.ID = @command_id
	
end

