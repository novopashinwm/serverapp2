select Phone_Confirmation_ID = pc.ID, m.message_id
	into #t
	from Phone p 
	cross apply (
		select top(1) *
			from Phone_Confirmation pc 
			where pc.Phone_ID = p.Phone_ID
			order by pc.Creation_Date desc
	) pc	
	cross apply (
		select top(1) * 
			from v_message m
			where m.body like '%' + pc.[Key]
			  and m.Template = 'GenericConfirmationSms' 
			  and [Contact Type] = 'SMS / SMPP'
			  and [Contact] = p.Phone
			order by m.message_id desc) m
	where dbo.GetPhoneInvariant(p.Phone) like '+7%'
	  and p.Confirmed = 0
	  and pc.Creation_Date > '2013.09.01'  --GETUTCDATE()-14
	  and m.Failed = 'yes'
	  --and m.message_id = 1066412

begin tran

	update pc
		set Creation_Date = GETUTCDATE()
		from Phone_Confirmation pc
		join #t t on t.Phone_Confirmation_ID = pc.ID
	
	update m
		set TYPE = CONVERT(int, 0x4000) + CONVERT(int, 0x08)
		from MESSAGE m
		join #t t on t.message_id = m.MESSAGE_ID

commit

	select * 
		from v_message m
		join #t t on t.message_id = m.MESSAGE_ID


drop table #t