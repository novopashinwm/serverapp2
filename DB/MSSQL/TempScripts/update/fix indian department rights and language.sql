insert into OPERATOR_DEPARTMENT
	select odr.operator_id, odr.department_id, 110, 1
		from v_operator_department_right odr
		join DEPARTMENT d on d.DEPARTMENT_ID = odr.DEPARTMENT_ID
		where odr.operator_id = 404
		  and odr.right_id = 2
		  and not exists (Select 1 from v_operator_department_right e where e.operator_id = odr.operator_id and e.department_id = odr.department_id and e.right_id = 110)

	update d set Country_ID = 1
		from v_operator_department_right odr
		join DEPARTMENT d on d.DEPARTMENT_ID = odr.DEPARTMENT_ID
		where odr.operator_id = 404
		  and odr.right_id = 110
		  and (d.Country_ID is null or d.Country_ID <> 1)
