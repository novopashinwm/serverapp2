begin tran

declare @phone_asid table (phone varchar(32), asid varchar(255))
set nocount on
insert into @phone_asid 
select '79148406741', 'p3bur15b3ce857e2cc526446aeaaf6c1c62d8'
set nocount off
--select c.CONTROLLER_ID, c.PHONE, pa.*, cmc.Asid_ID
	update mc set Controller_ID = c.CONTROLLER_ID
	from @phone_asid pa
	join CONTROLLER c on dbo.GetPhoneInvariant(c.phone) = dbo.GetPhoneInvariant(pa.phone)
	join Asid a on a.Value = pa.asid
	join MLP_Controller mc on mc.Asid_ID = a.ID
	left outer join MLP_Controller cmc on cmc.Controller_ID = c.CONTROLLER_ID
	where mc.Controller_ID <> c.CONTROLLER_ID
	  and cmc.ID is null
	  
	update d set ExtID = mtsD.ExtID		
	--select d.NAME, mtsD.ExtID
	from @phone_asid pa
	join CONTROLLER c on dbo.GetPhoneInvariant(c.phone) = dbo.GetPhoneInvariant(pa.phone)
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	join DEPARTMENT d on d.DEPARTMENT_ID = v.DEPARTMENT
	join Asid a on a.Value = pa.asid
	join DEPARTMENT mtsD on mtsD.DEPARTMENT_ID = a.Department_ID
	where d.ExtID is null
	  and mtsD.ExtID is not null

	update mtsD
		set ExtID = null,
		    NAME = mtsD.NAME + ' �������� � ' + d.NAME
	from @phone_asid pa
	join CONTROLLER c on dbo.GetPhoneInvariant(c.phone) = dbo.GetPhoneInvariant(pa.phone)
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	join DEPARTMENT d on d.DEPARTMENT_ID = v.DEPARTMENT
	join Asid a on a.Value = pa.asid
	join DEPARTMENT mtsD on mtsD.DEPARTMENT_ID = a.Department_ID
	where mtsD.ExtID is not null

	update a
		set Department_ID = d.DEPARTMENT_ID
	from @phone_asid pa
	join CONTROLLER c on dbo.GetPhoneInvariant(c.phone) = dbo.GetPhoneInvariant(pa.phone)
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	join DEPARTMENT d on d.DEPARTMENT_ID = v.DEPARTMENT
	join Asid a on a.Value = pa.asid
	join DEPARTMENT mtsD on mtsD.DEPARTMENT_ID = a.Department_ID
	where a.Department_ID <> d.DEPARTMENT_ID


commit