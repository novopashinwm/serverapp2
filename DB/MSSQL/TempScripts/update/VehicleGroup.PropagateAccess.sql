update vg set PropagateAccess = 1
	from department d
	join vehiclegroup vg on vg.department_id = d.department_id
	where d.name = 'SS Solutions'
	  and vg.PropagateAccess = 0
