select v.GARAGE_NUMBER, oldD.NAME, ' -> ', newD.NAME
	--update v set DEPARTMENT = newD.DEPARTMENT_ID
	from CONTROLLER_INFO ci 
	join CONTROLLER c on c.CONTROLLER_ID = ci.CONTROLLER_ID
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	join DEPARTMENT oldD on oldD.DEPARTMENT_ID = v.DEPARTMENT
	join DEPARTMENT newD on newD.NAME = 'Sitronics India Cluster 1'
	where ci.DEVICE_ID = CONVERT(varbinary(32), '30100000721')
	  and oldD.DEPARTMENT_ID <> newD.DEPARTMENT_ID