begin tran

declare @login varchar(255)
declare @department_name nvarchar(255)

set @login = 'maximus'
set @department_name = 'Максимус'

declare @operator_id int
declare @department_id int

set @operator_id = (select operator_id from operator o where login = @login)
set @department_id = (select top(1) department_id from department where name = @department_name order by department_id desc)

select operator_id = @operator_id, department_id = @department_id

	insert into OPERATOR_DEPARTMENT (OPERATOR_ID, DEPARTMENT_ID, RIGHT_ID, ALLOWED)
		select @operator_id, @department_id, r.right_id, 1
		from [right] r 
		where r.right_id in (2, 104)
		and not exists (select * from OPERATOR_DEPARTMENT od 
							where od.operator_id = @operator_id
							  and od.department_id = @department_id
							  and od.right_id = r.right_id)

		update od 
			set allowed = 1
		from OPERATOR_DEPARTMENT od 
		where od.operator_id = @operator_id
		  and od.department_id = @department_id
		  and od.right_id in (2, 104)
		  and od.allowed = 0
	

	insert into RIGHT_OPERATOR (OPERATOR_ID, RIGHT_ID, ALLOWED)
		select @operator_id, r.right_id, 1
		from [right] r 
		where r.right_id in (2)
		and not exists (select * from RIGHT_OPERATOR od 
							where od.operator_id = @operator_id
							  and od.right_id = r.right_id)
							  
	update right_operator 
		set allowed = 1 
		where operator_id = @operator_id and right_id in (2) and allowed = 0

commit
--rollback
--select r.right_id, r.name 
--from v_operator_rights [or]
--join operator o on o.operator_id = [or].operator_id
--join [right] r on r.right_id = [or].right_id
--where login = 'a427700'

--select r.name
--from v_operator_department_right [or]
--join operator o on o.operator_id = [or].operator_id
--join [right] r on r.right_id = [or].right_id
--join department d on d.department_id = [or].department_id
--where login = 'a427700'
