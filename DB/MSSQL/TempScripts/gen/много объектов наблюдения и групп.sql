begin tran

declare @department_id int = (select top(1) DEPARTMENT_ID from DEPARTMENT order by DEPARTMENT_ID desc)

declare @i int
declare @vehicle_count int = 2000
declare @group_count int = 50

set @i = 0
while @i <> @vehicle_count
begin

	insert into VEHICLE (GARAGE_NUMBER, DEPARTMENT)
		select * 
			from (select Garage_Number = 'Vehicle #' + CONVERT(nvarchar(255), @i), Department = @department_id) t
		where not exists (select * from Vehicle e where e.GARAGE_NUMBER = t.Garage_Number and e.DEPARTMENT = t.Department)
	
	set @i = @i + 1

end

insert into CONTROLLER (VEHICLE_ID, CONTROLLER_TYPE_ID, NUMBER)
	select v.Vehicle_ID
	     , ct.Controller_Type_ID
	     , 0
      from Vehicle v, CONTROLLER_TYPE ct
      where v.DEPARTMENT = @department_id
        and ct.TYPE_NAME = 'GenericTracker'
        and not exists (
			select * from Controller c where c.VEHICLE_ID = v.VEHICLE_ID)

declare @vehiclegroup_id int
declare @vehiclegroup_name nvarchar(255)
set @i = 0
while @i <> @group_count
begin

	set @vehiclegroup_name = 'Group #' + CONVERT(nvarchar(255), @i)

	insert into VEHICLEGROUP (NAME, Department_ID)
		select *
			from (select Name = @vehiclegroup_name, Department_ID = @department_id) t
			where not exists (
				select *
					from VEHICLEGROUP e
					where e.NAME = t.Name
					  and e.Department_ID = t.Department_ID)
	
	set @vehiclegroup_id = (select vehiclegroup_id from VEHICLEGROUP vg where vg.NAME = @vehiclegroup_name and vg.Department_ID = @department_id) 
	
	insert into VEHICLEGROUP_VEHICLE
		select top(@vehicle_count / @group_count) 
			@vehiclegroup_id, v.vehicle_id
			from VEHICLE v
			where v.DEPARTMENT = @department_id
			  and not exists (select * from VEHICLEGROUP_VEHICLE e where e.VEHICLE_ID = v.VEHICLE_ID)

	set @i = @i + 1
end

select * from DEPARTMENT d where d.DEPARTMENT_ID = @department_id
commit