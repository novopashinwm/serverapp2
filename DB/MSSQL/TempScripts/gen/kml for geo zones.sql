select name = z.NAME
	 , description = z.DESCRIPTION
	 , [Polygon/outerBoundaryIs/LinearRing/coordinates] = (
			select convert(varchar(30), X) + ',' + convert(varchar(30), Y) + ' '
				from GEO_ZONE_PRIMITIVE gzp 
				join ZONE_PRIMITIVE_VERTEX zpv on zpv.PRIMITIVE_ID = gzp.PRIMITIVE_ID
				join MAP_VERTEX v on v.VERTEX_ID = zpv.VERTEX_ID
				where gzp.ZONE_ID = z.ZONE_ID
				order by gzp.[ORDER], zpv.[ORDER]
				for xml path (''))
	, [Style/PolyStyle/color] = '#' + replace(sys.fn_varbintohexstr(CONVERT(varbinary, color)), '0x', '')
	from GEO_ZONE z
	where z.ZONE_ID in (select ozr.ZONE_ID
						from OPERATOR o
						join v_operator_zone_right ozr on ozr.operator_id = o.OPERATOR_ID and ozr.right_id = 105
						where o.NAME = 'Немоляева Бекасово')
	for xml path ('Placemark')

