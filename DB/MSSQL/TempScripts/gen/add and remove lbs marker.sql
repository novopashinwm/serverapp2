select
'FORIS.SPA.ConsoleEmulator AddServiceF' + 
' ContractNumber=' + a.Contract_Number + 
' TerminalDeviceNumber=' + convert(varchar(100), a.Terminal_Device_Number) + 
' FMSISDN=' + a.Value +
' ServiceCode=FRNIKA.MLP' +
' SendSmsNotification=false' +
'
FORIS.SPA.ConsoleEmulator DeleteService ' + 
' TerminalDeviceNumber=' + convert(varchar(100), a.Terminal_Device_Number) + 
' ContractNumber=' + a.Contract_Number + 
' ServiceCode=FRNIKA.MLP' + 
' SendSmsNotification=false' +
'
'
from Billing_Service bs
join Asid a on a.ID = bs.Asid_ID
join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
where 1=1
and bst.Service_Type_Category = 'NIKAAdmin'
--and bs.StartDate < dateadd(hour, -4, '2013.07.15 10:55')
--and a.Value = '5024350b4a2f90637befb6ad327e576e'
and a.ID not in (select mlpc.Asid_ID from MLP_Controller mlpc)

