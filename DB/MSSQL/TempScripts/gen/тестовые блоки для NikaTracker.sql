declare @a table (i int)

declare @count int = 10

while @count <> 0
begin
	insert into @a select @count
	set @count = @count - 1
end

select 'DEB' + CONVERT(varchar, i) from @a order by i