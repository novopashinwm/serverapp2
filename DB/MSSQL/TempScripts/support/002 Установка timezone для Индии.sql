﻿DECLARE @IndiaOperatorId int = (SELECT TOP(1) [OPERATOR_ID] FROM [dbo].[OPERATOR] WHERE [LOGIN] = N'NIKA_India')
DECLARE @IndiaTimezoneId int = (SELECT TOP(1) [TimeZone_ID] FROM [dbo].[TimeZone] WHERE [Code]  = N'India Standard Time')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT DISTINCT
			O.*
		FROM [dbo].[DEPARTMENT] D
			INNER JOIN [dbo].[v_operator_department_right] R
				ON R.[DEPARTMENT_ID] = D.[DEPARTMENT_ID]
					INNER JOIN [dbo].[OPERATOR] O
						ON O.[OPERATOR_ID] = R.[OPERATOR_ID]
		WHERE D.[DEPARTMENT_ID] IN
		(
				-- Департаменты, для которых @IndiaOperatorId, является администратором
				SELECT DISTINCT
					[department_id]
				FROM [dbo].[v_operator_department_right]
				WHERE 1=1
				AND [operator_id] = @IndiaOperatorId
				AND [right_id]    = 002 --SecurityAdministration
		)
		AND O.[LOGIN] NOT IN
		(
			N'admin'
		)
	) AS SRC
		ON  DST.[OPERATOR_ID]           = SRC.[OPERATOR_ID]
		AND DST.[TimeZone_ID]          <> @IndiaTimezoneId
		AND DST.[IsTimeZoneInfoManual] <> 1
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[TimeZone_ID]          = @IndiaTimezoneId
				,DST.[IsTimeZoneInfoManual] = 0
	OUTPUT
		 GETDATE()                       AS [Timestamp]
		,INSERTED.[OPERATOR_ID]          AS [OPERATOR_ID]
		,INSERTED.[NAME]                 AS [NAME]
		,INSERTED.[LOGIN]                AS [LOGIN]
		,INSERTED.[TimeZone_ID]          AS [TimeZoneId]
		,DELETED.[TimeZone_ID]           AS [TimeZoneId_Old]
		,INSERTED.[IsTimeZoneInfoManual] AS [IsTimeZoneInfoManual]
		,DELETED.[IsTimeZoneInfoManual]  AS [IsTimeZoneInfoManual_Old]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH