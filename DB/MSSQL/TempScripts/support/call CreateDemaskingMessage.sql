select id, value
	into #t
	from Contact ac		
	where ac.Type = 3
	  and ac.Demasked_ID is null
	  and ac.Valid = 1
	 -- and exists (
		--select *
		--	from Message_Contact mc 
		--	where mc.Type in (1,2)
		--	  and mc.Contact_ID = ac.ID)
	  and ac.LockedCount = 0

select * from #t
	  
while 1=1
begin
	
	declare @id int
	set @id = (select top(1) id from #t order by ID desc)
	if @id is null
		break
	delete #t where ID = @id
	exec dbo.CreateDemaskingMessage @id
	
end

drop table #t