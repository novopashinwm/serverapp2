with sq as (
select type = substring(REPETITION_XML, 386, 32), REPETITION_XML, SCHEDULERQUEUE_ID
	from SCHEDULERQUEUE with (nolock)
)
select i.REPETITION_XML
	from (
		select type
			from sq
			group by type
	) g
	cross apply (select top(1) i.Repetition_XML from sq i where i.type = g.type order by i.schedulerqueue_id desc) i
