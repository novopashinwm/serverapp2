:SETVAR LOGIN "Enter login"
:SETVAR DEPRT "Enter department name"
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------

	DECLARE @login           nvarchar(255) = N'$(LOGIN)'
	DECLARE @department_name nvarchar(255) = N'$(DEPRT)'

	DECLARE @operator_id   int = (SELECT TOP(1) [OPERATOR_ID]   FROM [dbo].[OPERATOR]   WHERE [LOGIN] = @login)
	DECLARE @department_id int = (SELECT TOP(1) [DEPARTMENT_ID] FROM [dbo].[DEPARTMENT] WHERE [NAME]  = @department_name ORDER BY [department_id] DESC)

	SELECT
		 [OPERATOR_LOGIN]  = @login
		,[OPERATOR_ID]     = @operator_id
		,[DEPARTMENT_NAME] = @department_name
		,[DEPARTMENT_ID]   = @department_id

	IF (COALESCE(@operator_id, 0) > 0 AND COALESCE(@department_id, 0) > 0)
	BEGIN
		--Добавление прав или обновление если они есть, но выключены
		MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
		USING
		(
			SELECT
				@operator_id, @department_id, [RIGHT_ID], 1
			FROM [dbo].[RIGHT]
			-- 7/*EditVehicles*/ нужно для разрешения добавлять трекеры
			WHERE [RIGHT_ID] IN (2/*SecurityAdministration*/, 104/*DepartmentsAccess*/, 7/*EditVehicles*/)
		) AS SRC ([OPERATOR_ID], [DEPARTMENT_ID], [RIGHT_ID], [ALLOWED])
			ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
			AND DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
			AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
		WHEN MATCHED AND DST.[ALLOWED] = 0 THEN
			UPDATE
				SET
					DST.[ALLOWED] = 1
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ([OPERATOR_ID], [DEPARTMENT_ID], [RIGHT_ID], [ALLOWED]) VALUES (SRC.[OPERATOR_ID], SRC.[DEPARTMENT_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
		OUTPUT $action, INSERTED.*, DELETED.*;
		--Добавление прав или обновление если они есть, но выключены
		MERGE [dbo].[RIGHT_OPERATOR] AS DST
		USING
		(
			SELECT
				@operator_id, [RIGHT_ID], 1
			FROM [dbo].[RIGHT]
			WHERE [RIGHT_ID] IN (2)
		) AS SRC ([OPERATOR_ID], [RIGHT_ID], [ALLOWED])
			ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
			AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
		WHEN MATCHED AND DST.[ALLOWED] = 0 THEN
			UPDATE
				SET
					DST.[ALLOWED] = 1
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ([OPERATOR_ID], [RIGHT_ID], [ALLOWED]) VALUES (SRC.[OPERATOR_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
		OUTPUT $action, INSERTED.*, DELETED.*;
	END
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH