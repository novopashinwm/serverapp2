drop table #t

select lt.*
	into #t
	from v_vehicle v
	join log_time lt (nolock) on 
			lt.Vehicle_ID = v.vehicle_id 
		and lt.InsertTime between '2016.01.14' and '2016.01.15' 
		--and lt.InsertTime > dbo.GetDateFromInt(3600 + lt.Log_Time)
	where len(v.DEVICE_ID) > 0

select * 
	into #t1
	from #t lt
	where lt.InsertTime > dbo.GetDateFromInt(3600 + lt.Log_Time)

create clustered index #ix_t1 on #t1 (vehicle_id, log_time)

;with vp (vehicle_Id, log_time, d01, d12, d23, d34, d13)
as (
select p0.VEHICLE_ID
	, lt.Log_Time
	, D01 = dbo.GetTwoGeoPointsDistance(p0.lng, p0.lat, p1.lng, p1.lat)
	, D12 = dbo.GetTwoGeoPointsDistance(p1.lng, p1.lat, p2.lng, p2.lat)
	, D23 = dbo.GetTwoGeoPointsDistance(p2.lng, p2.lat, p3.lng, p3.lat)
	, D34 = dbo.GetTwoGeoPointsDistance(p3.lng, p3.lat, p4.lng, p4.lat)
	, D13 = dbo.GetTwoGeoPointsDistance(p1.lng, p1.lat, p3.lng, p3.lat)
	from #t1 lt 
	join geo_log p0 (nolock) on p0.Vehicle_ID = lt.Vehicle_ID and p0.LOG_TIME = lt.Log_Time
	cross apply (select top(1) * from Geo_Log p (nolock) where p.Vehicle_ID = p0.Vehicle_ID and p.Log_Time < p0.Log_Time order by p.log_time desc) p1
	cross apply (select top(1) * from Geo_Log p (nolock) where p.Vehicle_ID = p1.Vehicle_ID and p.Log_Time < p1.Log_Time order by p.log_time desc) p2
	cross apply (select top(1) * from Geo_Log p (nolock) where p.Vehicle_ID = p2.Vehicle_ID and p.Log_Time < p2.Log_Time order by p.log_time desc) p3
	cross apply (select top(1) * from Geo_Log p (nolock) where p.Vehicle_ID = p3.Vehicle_ID and p.Log_Time < p3.Log_Time order by p.log_time desc) p4
	where lt.InsertTime > dbo.GetDateFromInt(3600 + lt.Log_Time)
)
select
	vp.*
	from vp vp
	where 1=1
	  and vp.log_time between dbo.msk2lt('2016.01.14') and dbo.msk2lt('2016.01.15') -- and lt.media = 52
	  and vp.d01 > 0 and vp.d12 > 0 and vp.d23 > 0 and vp.d34 > 0 and vp.d13 > 0
	  and vp.d01 + vp.d13 + vp.d34 < vp.d12
	  and vp.d01 + vp.d13 + vp.d34 < vp.d23
	order by vp.vehicle_Id, vp.log_time
