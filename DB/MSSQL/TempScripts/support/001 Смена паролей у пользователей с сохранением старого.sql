﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @SuspendedPass sysname = N'Suspended141118';
	------------------------------------------------------------------
	IF (OBJECT_ID(N'[dbo].[OPERATOR_SUSPENDED]') IS NULL)
	BEGIN
		SELECT
			 [Timestamp]       = GETDATE()
			,[OPERATOR_ID]     = CAST(NULL AS int)
			,[NEW_NAME]        = [NAME]
			,[NEW_LOGIN]       = [LOGIN]
			,[NEW_PASSWORD]    = [PASSWORD]
			,[OLD_NAME]        = [NAME]
			,[OLD_LOGIN]       = [LOGIN]
			,[OLD_PASSWORD]    = [PASSWORD]
		INTO [dbo].[OPERATOR_SUSPENDED]
		FROM [dbo].[OPERATOR]
		WHERE 0 = 1
	END
	-- Синхронизация [dbo].[MEDIA_TYPE]
	MERGE [dbo].[OPERATOR] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT * FROM [dbo].[OPERATOR]
		WHERE [LOGIN] IN
		(
			 N'vezet'
			,N'Taxi'
			,N'Samara'
			,N'Rostov'
			,N'Kazan'
			,N'Kras24'
			,N'Voronezh'
			,N'Perm'
			,N'Omsk'
		)
	) AS SRC
		ON DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[PASSWORD] = @SuspendedPass
	OUTPUT
		 GETDATE()              AS [Timestamp]
		,INSERTED.[OPERATOR_ID] AS [OPERATOR_ID]
		,INSERTED.[NAME]        AS [NEW_NAME]
		,INSERTED.[LOGIN]       AS [NEW_LOGIN]
		,INSERTED.[PASSWORD]    AS [NEW_PASSWORD]
		,DELETED.[NAME]         AS [OLD_NAME]
		,DELETED.[LOGIN]        AS [OLD_LOGIN]
		,DELETED.[PASSWORD]     AS [OLD_PASSWORD]
	INTO [dbo].[OPERATOR_SUSPENDED]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	SELECT * FROM [dbo].[OPERATOR_SUSPENDED]
	--COMMIT TRAN
	ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH