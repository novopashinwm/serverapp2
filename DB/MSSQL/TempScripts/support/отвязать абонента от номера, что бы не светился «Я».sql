declare @operator_id int = 43242

update a set Operator_ID = null
	from Asid a
	where a.Operator_ID = @operator_id

delete oc
	from Operator_Contact oc
	join Contact c on c.ID = oc.Contact_ID
	where oc.operator_id = 43242 and oc.confirmed = 1 and c.Type = 2
