select min(T), avg(T), stdev(T), max(T)
	from (

select T = datediff(second, Created, Sent), *
	from message m
	where DestinationType_ID = 2
	  and status = 3
	  and time between dbo.msk2utc('2016.02.12 15:30') and dbo.msk2utc('2016.02.12 16:00')

) t

select *
	from message m
	where DestinationType_ID = 2
	  and status = 3
	  and time between dbo.msk2utc('2016.02.12') and dbo.msk2utc('2016.02.12 12:00')

sp_spaceused 'Message'

select * from v_message where message_id = 199951249

select * from v_phone where msisdn = '79154015388'

---

select 
	[Created to delivered seconds] = datediff(second, created, Delivered),
	[Created to sent seconds] = datediff(second, created, sent),
	dbo.describeMessageStatus(status),
	dbo.describeMessageProcessingResult(ProcessingResult),
	Created, sent, Delivered, body, mc.value, m.MESSAGE_ID
	from MESSAGE m (nolock)
	left outer join v_message_contact mc (nolock) on mc.message_id = m.MESSAGE_ID and mc.type_name = 'Destination'
	where DestinationType_ID = 2
	  and Created > '2016.02.13'
	  and Delivered is not null
	order by Created desc