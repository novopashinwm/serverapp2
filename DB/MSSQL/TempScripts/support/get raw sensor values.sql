declare @vehicle_id int = 52869

;with Input (Number) as (
	select distinct cs.Number
		from dbo.Controller_Sensor cs
		join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
		join CONTROLLER c on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
		where c.VEHICLE_ID = @vehicle_id
)
select 	
	csl.Log_Time,
	dbo.lt2msk(log_time),
	i.Number,
	cs.DESCRIPT,
	csl.Value
from Input i
cross apply (
	select top(1) *
		from Controller_Sensor_Log csl (nolock)
		where csl.Vehicle_ID = @vehicle_id
		  and csl.Number = i.Number
		order by csl.Log_Time desc) csl
join controller c on c.VEHICLE_ID = @vehicle_id
join CONTROLLER_SENSOR cs on cs.NUMBER = csl.Number and cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID