select vg.NAME, [Count] = COUNT(1)
from DEPARTMENT d
join VEHICLE v on v.DEPARTMENT = d.DEPARTMENT_ID
join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLE_ID = v.VEHICLE_ID
join VEHICLEGROUP vg on vg.VEHICLEGROUP_ID = vgv.VEHICLEGROUP_ID
where d.NAME = 'sitronicsindia'
group by vg.NAME
order by vg.NAME

select o.NAME, o.LOGIN, [Count] = (select COUNT(1) from v_operator_vehicle_right ovr where ovr.operator_id = o.OPERATOR_ID and ovr.right_id = 102)
from OPERATORGROUP og
join OPERATORGROUP_OPERATOR ogo on ogo.OPERATORGROUP_ID = og.OPERATORGROUP_ID
join OPERATOR o on o.OPERATOR_ID = ogo.OPERATOR_ID
where og.NAME = 'sitronicsindia'
order by o.NAME