declare @log_time_from int = datediff(ss, '1970', '2012-11-30') 
declare @log_time_to   int = datediff(ss, '1970', '2012-12-05 23:59')

declare @vehicle Id_Param
insert into @vehicle
	select v.vehicle_id
		from vehicle v 
		join department d on d.department_id = v.department
		where d.name like '%�������%'

declare @zone dbo.id_param
insert into @zone 
	select z.zone_id
		from geo_zone z
		where z.name like 'M10 ���� ������� ��������'

create table #z (vehicle_id int, log_time int, zone_id int, primary key clustered (vehicle_id, zone_id, log_time))
insert into #z
exec dbo.GetGeoZonesForVehicles @vehicle, @zone, @log_time_from, @log_time_to

create table #z1 (vehicle_id int, log_time int, in_zone bit, id int identity)

insert into #z1 (vehicle_id, log_time, in_zone)
	select gl.vehicle_id, 
	       gl.log_time,
		   (select count(1) from #z z where z.vehicle_id = gl.vehicle_id and z.zone_id = (select id from @zone) and z.log_time = gl.log_time)
	from geo_log gl (nolock)
	where gl.vehicle_id in (select id from @vehicle)
	  and gl.log_time between @log_time_from and @log_time_to
	order by gl.vehicle_id, gl.log_time

create table #z2 (vehicle_id int, prev_log_time int, prev_in_zone bit, next_log_time int, next_in_zone int, id int identity)

insert into #z2 (vehicle_id, prev_log_time, prev_in_zone, next_log_time, next_in_zone)
	select z1.vehicle_id, z2.log_time, z2.in_zone, z1.log_time, z1.in_zone
	from #z1 z1
	left outer join #z1 z2 on z2.id = z1.id - 1 and z2.vehicle_id = z1.vehicle_id 
	where z2.in_zone <> z1.in_zone
	order by z1.vehicle_id, z1.log_time

select
      "�������" = d.name
    , "�.�." = v.garage_number 
	, "����� �����" = dbo.GetDateFromInt(z2.next_log_time+4*3600)
	, "����� ������" = dbo.GetDateFromInt(z1.prev_log_time+4*3600)
	, "������" = dbo.CalculateDistanceFN(z2.next_log_time, z1.prev_log_time, z1.vehicle_id) / 1000
	, "����" = convert(int, round((z1.prev_log_time-z2.next_log_time)/3600.0, 0))
	, "������� ��������, ��/�" = convert(int, round(
		dbo.CalculateDistanceFN(z2.next_log_time, z1.prev_log_time, z1.vehicle_id) / 1000 / ((z1.prev_log_time-z2.next_log_time)/3600.0)
		, 0))
	
	from #z2 z1
	join #z2 z2 on z2.id = z1.id - 1 and z2.vehicle_id = z1.vehicle_id
	join vehicle v on v.vehicle_id = z1.vehicle_id
	left outer join department d on d.department_id = v.department
	where z1.prev_in_zone = 1
	  and dbo.CalculateDistanceFN(z2.next_log_time, z1.prev_log_time, z1.vehicle_id) / 1000 > 400
	  and (z1.prev_log_time-z2.next_log_time)/3600.0 > 10

select 
	v.garage_number,
	sum(t.Max_Log_Time - t.Min_Log_Time) / 3600
from #z z
join vehicle v on v.vehicle_id = z.vehicle_id
join #t t on t.vehicle_id = z.vehicle_id and t.Min_Log_Time = z.Log_Time
join Geo_Zone gz on gz.Zone_ID = z.Zone_ID
group by v.garage_number

drop table #z1
drop table #z2
drop table #z