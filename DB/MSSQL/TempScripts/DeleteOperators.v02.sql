;WITH CLEAR_TABLES AS
(
	SELECT
		o.[object_id],
		[object_name] =
		QUOTENAME(OBJECT_SCHEMA_NAME(o.[object_id], DB_ID())) +
		N'.' +
		QUOTENAME(OBJECT_NAME       (o.[object_id], DB_ID())) COLLATE Cyrillic_General_CI_AI,
		[object_refs] = (SELECT COUNT(*) FROM sys.foreign_keys WHERE [referenced_object_id] = o.[object_id])
	FROM sys.objects o
	WHERE o.[type] = 'U'
	----------------------------------
	AND
	(
		--��� ������� ������� ���� �� ������ ����� 'OPERATOR_ID'
		(
			EXISTS(SELECT * FROM sys.columns WHERE [object_id] = o.[object_id] AND UPPER([name]) LIKE N'%OPERATOR_ID%')
		)
	)
)
SELECT
	[object_refs],
	N'DELETE FROM ' +
	[object_name] +
	N' WHERE [OPERATOR_ID] IN (' +
	STUFF((SELECT N',' + CAST([OPERATOR_ID] AS nvarchar) FROM [dbo].[OPERATOR] WHERE [NAME] LIKE N'%ShareLink%' FOR XML PATH ('')), 1, 1, '') +
	N')'
FROM CLEAR_TABLES
ORDER BY [object_refs]