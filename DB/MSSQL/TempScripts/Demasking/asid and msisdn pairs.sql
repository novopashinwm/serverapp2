declare @smsServicePhone varchar(20) = '6452'
declare @modemAsid		varchar(255) = '654948fab7f6e545e623eacee8afbf6c'

declare @templateName	nvarchar(50) = 'DemaskAsidRequest'
declare @uidFieldId	int = (select mtf.MESSAGE_TEMPLATE_FIELD_ID 
								from MESSAGE_TEMPLATE_FIELD mtf 
								join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
								where mt.NAME = @templateName 
								  and mtf.NAME = 'RequestUid')

declare @m_in table (request_uid varchar(32) primary key clustered, msisdn varchar(11), time datetime)
insert into @m_in
	select SUBSTRING(body, 1, 32), SUBSTRING(BODY, 32+1, 11), time
		from Message m_in
		where m_in.SOURCE_ID in (select p.Phone_ID from Phone p where p.Phone = @smsServicePhone)
		  and m_in.SourceType_ID = CONVERT(int, 0x4000)
		  and len(m_in.BODY) <= 300
		  and BODY is not null
		  and LEN(body) = 32 + 1 + 11

;with Demasking as (
select t.Asid, t.Msisdn, ID = (select a.ID from Asid a where a.Value = t.Asid)
	from (
		select Dummy = null
			, Msisdn = (select m_in.Msisdn from @m_in m_in where m_in.request_uid = convert(varchar(32), m_out_uid.CONTENT))
			, m_out.TIME
			, Asid = CONVERT(varchar(255), SUBSTRING(m_out.BODY, LEN(m_out_uid.CONTENT) + 1 + 2 + 1, len(m_out.body) - LEN(m_out_uid.CONTENT) - 1 - 2))
			from Asid a
			join Message m_out on 
					m_out.DestinationType_ID = 3 /*Mpx*/ 
				and m_out.DESTINATION_ID = a.ID
				and m_out.TEMPLATE_ID = (select mt.Message_Template_ID from MESSAGE_TEMPLATE mt where mt.NAME = @templateName)
			join MESSAGE_FIELD m_out_uid on m_out_uid.MESSAGE_ID = m_out.MESSAGE_ID 
										and m_out_uid.MESSAGE_TEMPLATE_FIELD_ID = @uidFieldId
			where a.Value = @modemAsid
	) t
)
	select d.Asid, d.Msisdn, d.ID
		from Demasking d
		where d.Msisdn is not null
		order by Msisdn
		
		--select SUM(case when d.ID is not null and d.Msisdn is null then 1 else 0 end)
		--		, ' / '
		--		, sum(case when d.ID is not null then 1 else 0 end)
		--from Demasking d
		--where d.ID is not null
		 