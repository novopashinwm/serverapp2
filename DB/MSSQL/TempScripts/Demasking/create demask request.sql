declare @modemAsid		varchar(255) = '654948fab7f6e545e623eacee8afbf6c'
declare @templateId	nvarchar(50) = (select mt.Message_Template_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'DemaskAsidRequest')
declare @uidFieldId	int = (select mtf.MESSAGE_TEMPLATE_FIELD_ID 
								from MESSAGE_TEMPLATE_FIELD mtf 
								where mtf.MESSAGE_TEMPLATE_ID = @templateId
								  and mtf.NAME = 'RequestUid')
declare @asidDestinationTypeId int = 3

declare @asids table (value varchar(255))

insert into @asids
select a.Value
	from Asid a
	where a.Value is not null 
	  and not exists (
		select *
			from Message m
			where m.Template_ID = @templateId
			  and m.Destination_ID = (select modem.ID from Asid modem where modem.Value = @modemAsid)
			  and m.DestinationType_ID = @asidDestinationTypeId
			  and m.BODY like '%##' + a.Value)

select * from @asids

while (1=1)
begin

	declare @asid			varchar(255)
	set @asid = (select top(1) value from @asids)
	if @asid is null
		break;
	delete @asids where value = @asid

	print @asid

	declare @uidFieldValue varchar(32) 
	set @uidFieldValue = (select replace(NEWID(), '-', ''))

	begin tran

		insert into MESSAGE (Time, BODY, DESTINATION_ID, DestinationType_ID, TYPE, TEMPLATE_ID)
			select top(1) 
				getutcdate(), 
				@uidFieldValue + ' ##' + @asid, 
				a.ID, 
				3, 
				convert(int, 0x8000) + CONVERT(int, 0x08)
				, @templateId
				from Asid a
				where a.Value = @modemAsid
				
		declare @message_id int 
		set @message_id = @@identity
		insert into Message_Field (Message_ID, Message_Template_Field_ID, Content)
			select @message_id, @uidFieldId, @uidFieldValue
		
	commit

end