declare @adminLogin varchar(255) = 'admin'
declare @serviceLogin varchar(255) = 'NVG\ykoshcheev'

insert into operatorgroup (name)
	select t.Name
		from (select Name = 'Sales') t
		where not exists (Select * from operatorgroup e where e.name = t.Name)

if @@ROWCOUNT <> 0
	exec dbo.SetConstant 'SalesOperatorGroupID', @@identity

declare @sales int = (select convert(int, value) from constants where name = 'SalesOperatorGroupID')

insert into operator (Name, Login, Password, OneTimePassword)
	select t.Login, t.Login, t.Login, 1
		from (select Login = @adminLogin) t
		where not exists (Select * from operator e where e.LOGIN = t.Login)

insert into RIGHT_OPERATORGROUP
	select t.right_id, t.operatorgroup_id, 1
		from (
			select r.right_id, operatorgroup_id = @sales
				from [right] r
				where r.NAME in ('SecurityAdministration', 'ServiceManagement', 'Map_GIS', 'ReportsAccess')) t
		where not exists (select * from RIGHT_OPERATORGROUP e where e.OPERATORGROUP_ID = t.OPERATORGROUP_ID and e.RIGHT_ID = t.right_id)

insert into operator (Name, Login)
	select t.Login, t.Login
		from (select Login = @serviceLogin) t
		where not exists (Select * from operator e where e.LOGIN = t.Login)

insert into OPERATORGROUP_OPERATOR
	select t.operatorgroup_id, t.operator_id
	from (select operatorgroup_id = @sales, o.operator_id from operator o where o.login = @adminLogin) t
	where not exists (Select * From operatorgroup_operator e where e.operatorgroup_id = t.operatorgroup_id and e.operator_id = t.operator_id)

insert into operator (name, login, password)
	select t.login, t.login, t.login
		from (select login = 'guest') t
		where not exists (select 1 from operator e where e.login = t.login)

insert into RIGHT_OPERATOR
	select t.right_id, t.operator_id, 1
		from (
			select r.right_id, operator_id = o.OPERATOR_ID
				from [right] r
				join operator o on o.login = 'guest'
				where r.NAME in ('Map_GIS', 'ReportsAccess')) t
		where not exists (select * from RIGHT_OPERATOR e where e.OPERATOR_ID = t.OPERATOR_ID and e.RIGHT_ID = t.right_id)
