/* ���������

select 'insert into Media_Acceptors (Media_ID, Initialization, Name, Description) select m.Media_ID, ''' + ma.INITIALIZATION + ''', ''' + ma.Name + ''', ''' + ma.description + ''' from Media m where m.Name = ''' + m.NAME + ''' and not exists (select 1 from Media_Acceptors e where e.Name = ''' + ma.Name + ''')'
	from media_acceptors ma
	join media m on m.MEDIA_ID = ma.MEDIA_ID
	where ma.name in ('TCP', 'MLP', 'FTP', 'Order285')

*/

insert into Media_Acceptors (Media_ID, Initialization, Name, Description) select m.Media_ID, 'TCP|0.0.0.0|12350', 'TCP', '!' from Media m where m.Name = 'TCP' and not exists (select 1 from Media_Acceptors e where e.Name = 'TCP')
insert into Media_Acceptors (Media_ID, Initialization, Name, Description) select m.Media_ID, 'mlpUri=random://some-init-staff|mlpLogin=|mlpPassword=|httpRequestTimeout=|useProxy=|smsUriPrefix=http://localhost:11232/', 'MLP', 'MLPUrl|MLPLogin|MLPPassword - 8 alphanumeric symbols|MaximumLocationAge - see System.TimeSpan.Parse|Proxy enabled (true/false)|Throttling retry interval (TimeSpan)' from Media m where m.Name = 'MLP' and not exists (select 1 from Media_Acceptors e where e.Name = 'MLP')
insert into Media_Acceptors (Media_ID, Initialization, Name, Description) select m.Media_ID, '127.0.0.1', 'FTP', '������ IP-�����, �� ������� ������� ftp-������' from Media m where m.Name = 'FTP' and not exists (select 1 from Media_Acceptors e where e.Name = 'FTP')
insert into Media_Acceptors (Media_ID, Initialization, Name, Description) select m.Media_ID, '', 'Order285', '' from Media m where m.Name = 'Order285' and not exists (select 1 from Media_Acceptors e where e.Name = 'Order285')
