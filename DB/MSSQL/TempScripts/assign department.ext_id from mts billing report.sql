with contractPhone (contract, phone)
as (
	select min([Контракт]), MSISDN
		from Q2_2014_06
		group by  MSISDN
		having COUNT(1) = 1
),
departmentContract(department_id, contract)
as (
	select distinct
		   d.Department_ID
	     , cp.contract
		from Department d
		join VEHICLE v on v.DEPARTMENT = d.DEPARTMENT_ID
		join Controller c on c.VEHICLE_ID = v.VEHICLE_ID
		join Contact pc on pc.ID = c.PhoneContactID
		join contractPhone cp on pc.Value = cp.phone
),
singleDepartmentContract(department_id, contract)
as (
	select dc.department_id, MIN(dc.contract)
		from departmentContract dc
		group by dc.department_id
		having COUNT(1) = 1
)
--update d
--	set name = name + ' [' + ltrim(rtrim(dc.contract)) + ']'
--	  , extid = dc.contract
select dc.department_id, dc.contract, d.NAME, d.ExtID
	from singleDepartmentContract dc
	join department d on d.DEPARTMENT_ID = dc.department_id
--	where d.ExtID is null
	order by d.NAME