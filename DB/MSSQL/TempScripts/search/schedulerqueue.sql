select * 
from schedulerqueue sq
outer apply (
	select e.email
		from email_schedulerqueue esq
		join email e on e.email_Id = esq.email_id
		where esq.schedulerqueue_id = sq.schedulerqueue_id
) e
outer apply (
	select top(1) h.config_xml
		from h_schedulerqueue h
		where h.schedulerqueue_id = sq.schedulerqueue_id
		order by h.id desc) h
where schedulerqueue_id = 22190