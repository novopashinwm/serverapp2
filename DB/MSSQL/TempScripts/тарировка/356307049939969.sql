begin tran

declare @controller_id int
declare @sensor varchar(255)
declare @legend varchar(255)

set @controller_id = (
	select c.controller_id 
		from controller c
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
		where ci.DEVICE_ID = CONVERT(varchar(32), '356307049939969'))
		
set @sensor = 'Analog Input 1'
set @legend = '������ �������'

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

print 'Insert into #t'
insert into #t (RawValue, ActualValue)
select t.RawValue, t.ActualValue from (
		  select ActualValue = 0, RawValue = 0 where 1 = 0
union select 0	, 4036
union select 5	, 3384union select 10	, 3020union select 15	, 2805union select 20	, 2493union select 25	, 2273union select 30	, 2043union select 35	, 1851union select 40	, 1653union select 45	, 1350union select 50	, 1137union select 55	, 937union select 60	, 617union select 63	, 275
) t
order by RawValue

--���������� ������� �����
/*
insert into #t (ActualValue, RawValue)
select 2*2697 - ActualValue, 2*591 - RawValue
	from #t
	where RawValue <> 591
	order by ID desc
*/

print 'delete csm'
delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.descript = @sensor
	  and csl.name = @legend

print 'insert into dbo.Controller_Sensor_Map '

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Descript = @sensor
		join Controller_Sensor_Legend csl on csl.Name = @legend
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

--rollback
commit