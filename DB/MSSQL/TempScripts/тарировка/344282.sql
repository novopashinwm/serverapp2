begin tran

declare @controller_id int
declare @sensor varchar(255)
declare @legend varchar(255)

set @controller_id = (
	select c.controller_id 
		from controller c
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
		where ci.DEVICE_ID = CONVERT(varchar(32), '344282')
)
		
set @sensor = 'LLS1'
set @legend = '������ �������'

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

print 'Insert into #t'
insert into #t (RawValue, ActualValue)
select t.RawValue, t.ActualValue from (
		  select RawValue = 0, ActualValue = 0 where 1 = 0
	union all select 0		, 10	
	union all select 34      , 20	
	union all select 70      , 30	
	union all select 106     , 40	
	union all select 140     , 50	
	union all select 176     , 60	
	union all select 211     , 70	
	union all select 245     , 80	
	union all select 279     , 90	
	union all select 302     , 100	
	union all select 337     , 110	
	union all select 371     , 120	
	union all select 406     , 130	
	union all select 440     , 140	
	union all select 475     , 150	
	union all select 510     , 160	
	union all select 544     , 170	
	union all select 578     , 180	
	union all select 607     , 190	
	union all select 703     , 220	
	union all select 738     , 230	
	union all select 772     , 240	
	union all select 806     , 250	
	union all select 840     , 260	
	union all select 874     , 270	
	union all select 908     , 280	
	union all select 933     , 290	
	union all select 965     , 300	
	union all select 1001    , 310	
) t

--���������� ������� �����
/*
insert into #t (ActualValue, RawValue)
select 2*2697 - ActualValue, 2*591 - RawValue
	from #t
	where RawValue <> 591
	order by ID desc
*/

print 'delete csm'
delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.descript = @sensor
	  and csl.name = @legend

print 'insert into dbo.Controller_Sensor_Map '

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Descript = @sensor
		join Controller_Sensor_Legend csl on csl.Name = @legend
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

--rollback
commit