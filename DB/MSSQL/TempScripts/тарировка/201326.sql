begin tran

declare @controller_id int
declare @sensor varchar(255)
declare @legend varchar(255)

set @controller_id = (
	select c.controller_id 
		from controller c
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
		where ci.DEVICE_ID = CONVERT(varchar(32), '201326'))
		
set @sensor = 'LLS1'
set @legend = '������ �������'

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

print 'Insert into #t'
insert into #t (RawValue, ActualValue)
select t.RawValue, t.ActualValue from (
		  select ActualValue = 0, RawValue = 0 where 1 = 0
union all select 0  ,  0
union all select 10 , 26
union all select 20 , 76
union all select 30 , 113
union all select 40 , 148
union all select 50 , 183
union all select 60 , 223
union all select 70 , 263
union all select 80 , 298
union all select 90 , 333
union all select 100, 366
union all select 110, 398
union all select 120, 430
union all select 140, 489
union all select 160, 546
union all select 180, 599
union all select 190, 625
union all select 200, 648
union all select 210, 674
union all select 220, 699
union all select 230, 722
union all select 250, 768
union all select 270, 813
union all select 290, 863
union all select 310, 883
union all select 330, 932
union all select 350, 978
union all select 360, 1004
union all select 370, 1010
union all select 374, 1012

) t

--���������� ������� �����
/*
insert into #t (ActualValue, RawValue)
select 2*2697 - ActualValue, 2*591 - RawValue
	from #t
	where RawValue <> 591
	order by ID desc
*/

print 'delete csm'
delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.descript = @sensor
	  and csl.name = @legend

print 'insert into dbo.Controller_Sensor_Map '

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Descript = @sensor
		join Controller_Sensor_Legend csl on csl.Name = @legend
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

--rollback
commit