begin tran

declare @controller_id int
declare @sensor varchar(255)
declare @legend varchar(255)

set @controller_id = (
	select c.controller_id 
		from controller c
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
		where ci.DEVICE_ID = CONVERT(varchar(32), '354660042562184'))
		
set @sensor = '���������� ���� 0 AN0'
set @legend = '������ �������'

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

print 'Insert into #t'
insert into #t (RawValue, ActualValue)
select t.RawValue, t.ActualValue from (
		  select RawValue = 0, ActualValue = 0 where 1 = 0
union all select 1		*1000, 0
union all select 1.36	*1000, 10
union all select 1.86	*1000, 20
union all select 2.32	*1000, 30
union all select 2.76	*1000, 40
union all select 3.18	*1000, 50
union all select 4.01	*1000, 70
union all select 4.79	*1000, 90
union all select 5.55	*1000, 110
union all select 6.29	*1000, 130
union all select 7		*1000, 150
union all select 7.71	*1000, 170
union all select 8.42	*1000, 190
union all select 9.09	*1000, 210
union all select 9.77	*1000, 230
union all select 10.46	*1000, 250
union all select 11.15	*1000, 270
union all select 11.84	*1000, 290
union all select 12.55	*1000, 310
union all select 13.26	*1000, 330
union all select 13.97	*1000, 350
union all select 14.68	*1000, 370
union all select 15.43	*1000, 390
union all select 16.19	*1000, 410
union all select 16.86	*1000, 450
union all select 17.67	*1000, 470
union all select 18.2	*1000, 490
union all select 18.7	*1000, 510
union all select 19.2	*1000, 530
union all select 19.6	*1000, 550
) t

--���������� ������� �����
/*
insert into #t (ActualValue, RawValue)
select 2*2697 - ActualValue, 2*591 - RawValue
	from #t
	where RawValue <> 591
	order by ID desc
*/

print 'delete csm'
delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.descript = @sensor
	  and csl.name = @legend

print 'insert into dbo.Controller_Sensor_Map '

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Descript = @sensor
		join Controller_Sensor_Legend csl on csl.Name = @legend
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

--rollback
commit