begin tran

declare @controller_id int
declare @sensor varchar(255)
declare @legend varchar(255)

set @controller_id = (
	select c.controller_id 
		from controller c
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
		where ci.DEVICE_ID = CONVERT(varbinary(32), '354660042562184'))
		
set @sensor = '���������� ���� 0 AN0'
set @legend = '������ �������'

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

print 'Insert into #t'
insert into #t (RawValue, ActualValue)
select t.RawValue * 1000, t.ActualValue from (
		  select ActualValue = 0, RawValue = 0 where 1 = 0
union all select 9.69, 1.28	
union all select 18.25, 1.79	
union all select 40.09, 2.77	
union all select 49.8, 3.18	
union all select 70.21, 4.01	
union all select 90.33, 4.79	
union all select 110.25, 5.54	
union all select 129.9, 6.27	
union all select 150.14, 6.99	
union all select 170.27, 7.71	
union all select 190.36, 8.43	
union all select 200.18, 8.71	
union all select 219.55, 9.38	
union all select 240.28, 10.1	
union all select 260.28, 10.79	
union all select 280.21, 11.48	
union all select 299.9, 12.16	
union all select 320.29, 12.88	
union all select 340.12, 13.58	
union all select 360.21, 14.31	
union all select 380.16, 15.05	
union all select 400.22, 15.81	
union all select 420.31, 16.6	
union all select 440.32, 17.42	
union all select 450.25, 17.85	
union all select 460.25, 18.31	
union all select 470.41, 18.81	
union all select 473.66, 18.96	
union all select 476.67, 19.12	
union all select 480.23, 19.36	
) t
order by RawValue asc

--���������� ������� �����
/*
insert into #t (ActualValue, RawValue)
select 2*2697 - ActualValue, 2*591 - RawValue
	from #t
	where RawValue <> 591
	order by ID desc
*/

print 'delete csm'
delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.descript = @sensor
	  and csl.name = @legend

print 'insert into dbo.Controller_Sensor_Map '

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Descript = @sensor
		join Controller_Sensor_Legend csl on csl.Name = @legend
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

--rollback
commit