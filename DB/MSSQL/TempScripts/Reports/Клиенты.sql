declare @log_time_from int
declare @log_time_to int

set @log_time_from = datediff(ss, '1970', getutcdate()-90)
set @log_time_to = datediff(ss, '1970', getutcdate())

;with [last] as (
	select v.vehicle_id
		   , department_id = v.DEPARTMENT
		   , [time] = (select top(1) lt.InsertTime from log_time lt (nolock) where lt.Vehicle_ID = v.vehicle_id order by lt.InsertTime desc)
		from vehicle v
),
[first] as (
	select v.vehicle_id
		   , department_id = v.DEPARTMENT
		   , [time] = (select top(1) lt.InsertTime from log_time lt (nolock) where lt.Vehicle_ID = v.vehicle_id order by lt.InsertTime asc)
		from vehicle v
)
select "�������� �������" = d.Name
	, "���� ��������� � �������" = CONVERT(date,
		(select actual_time from h_department h where h.DEPARTMENT_ID = d.department_id and h.action = 'insert'))
	, "���� ������� ������ �� ������ ����������" = convert(date,
		(select MIN([Time]) from [first] f where f.department_id = d.department_id))
	, "���������� ���������� ���������" = (select count(1) from Vehicle v where v.Department = d.Department_ID)
	, "���������� ���������, ������������ ������ �� ��������� 3 ������" = (
		select COUNT(1) from [last] l where l.department_id = d.department_id and l.[time] between getutcdate()-90 and getutcdate())
	from Department d
	where d.NAME not like '��� �������%' and d.NAME not like '�����%' and NAME <> 'SitronicsIndia'
	
	