--select 
--o.operator_id,
--HoursFromUtc = case when 
--   o.name like '%������%'
--or o.name like '%���������%'
--or o.name like '%�������%'
--or o.name like '%���������%'
--or o.name like '%��������%'
--or o.name like '%�������%'
--or o.name like '%�������%'
--or o.name like '%�������%'
--then 6
--else 4
--end
--into #tz
--from operatorgroup og 
--join operatorgroup_operator ogo on ogo.operatorgroup_id = og.operatorgroup_id
--join operator o on o.operator_id = ogo.operator_id
--where og.name = '��� ������� ���������'

select o.operator_id, HoursFromUtc = 3.5
	into #tz
	from operator o
	where LOGIN like 'cowi%'
--	where o.login = 'brpl'

;with localized_session (operator_id, session_start, session_end) as (
select	tz.operator_id, 
		dateadd(hour, tz.HoursFromUtc, session_start),
		dateadd(hour, tz.HoursFromUtc, session_end)
	from #tz tz
	join session s on s.operator_id = tz.operator_id)
select 
"�����" = o.login, 
"���"   = o.name, 
"���"   = ts.year, 
"�����" = ts.month, 
"����"  = ts.day, 
"���������� ������" = ts.[count],
"����� � �������" = [minutes] / 60, 
"����� � �������" = [minutes] % 60,
"���������� ���������� ������ � ���� ����" = ucs.[count]
from operator o
cross apply (
	select	[year] = year(session_start), 
			[month] = month(session_start), 
			[day] = day(session_start), 
			[count] = count(1)
		from localized_session s 
		where s.operator_id = o.operator_id
		  and s.session_start between '2013.09.01' and '2013.10.15'
		  and s.session_end is not null
		group by year(session_start), month(session_start), day(session_start)
) ts
outer apply (
	select	[minutes] = sum(datediff(minute, session_start, session_end))
		from localized_session s 
		where s.operator_id = o.operator_id
		  and ts.[year] = year(session_start)
	      and ts.[month] = month(session_start)
	      and ts.[day] = day(session_start)
		  and s.session_end is not null
) cs
outer apply (
	select	[count] = count(1)
		from localized_session s 
		where s.operator_id = o.operator_id
		  and ts.[year] = year(session_start)
	      and ts.[month] = month(session_start)
	      and ts.[day] = day(session_start)
		  and s.session_end is null
) ucs
--where o.login = 'brpl'
--og.name like '%��� ������� ���������%'
order by 1,2,3,4,5

drop table #tz