/*
	� ������� Log_Time ��������� ������, ��� ������� ��� �� ����� ��������������� ������ � �������� Geo_Log, GPS_Log, Sensors_Log � Statistics_Log.
	������ ���������� ����� ������ � ������� Log_Time_empty.

	NB: ������� "������" ������� �������� � ����, ��� ��������� GetLogNew ���������� �������, 
	� ������� ������������ �������, ��� ������� ���������, ��� ������ ���; �� ����� ���� ������ ����, 
	�� ��� �� �������� � ������� ��-�� ���������� �� ��������� @interval.
*/

create table dbo.Log_Time_empty (
 ID			bigint primary key clustered 
,Vehicle_ID	int
,Log_Time	int
,Media		tinyint
,InsertTime	datetime);

insert into dbo.Log_Time_empty ( ID			
								,Vehicle_ID	
								,Log_Time	
								,Media		
								,InsertTime	)
select lt.*
from dbo.Log_Time lt (nolock)
left outer join dbo.Geo_Log gl  (nolock) on gl.Log_Time_ID = lt.ID
left outer join dbo.GPS_Log gpsl  (nolock) on gpsl.Log_Time_ID = lt.ID
left outer join dbo.Sensors_Log sl  (nolock) on sl.Log_Time_ID = lt.ID
left outer join dbo.Statistic_Log stl  (nolock) on stl.Log_Time_ID = lt.ID
where 
gl.Log_Time_ID is null and 
gpsl.Log_Time_ID is null and 
sl.Log_Time_ID is null and 
stl.Log_Time_ID is null 

begin tran
	delete from dbo.Log_Time where ID in (select ID from dbo.Log_Time_empty)
commit