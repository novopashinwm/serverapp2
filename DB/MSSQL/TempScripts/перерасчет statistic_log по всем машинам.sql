--1. ���������� Statistic_Log_Recalc �� ����������� ��������� ��� ������� ���������� ����
update slr
	set Log_Time = (select min(lt.Log_Time) from dbo.Log_Time lt where lt.Vehicle_ID = slr.Vehicle_ID)
from dbo.Statistic_Log_Recalc slr;

--2. � ����� ��� ������� ������� ��������������� ����������
declare @vehicleID int;
set @vehicleID = (select min(v.vehicle_id) from vehicle v);

while @vehicleID is not null
begin

	declare @maxLogTimeLogTime int
	set @maxLogTimeLogTime = (select max(log_time) from log_time where vehicle_id = @vehicleID);

	declare @maxSLRLogTime int
	set @maxSLRLogTime = (select log_time from statistic_log_recalc slr where vehicle_id = @vehicleID);

	if @maxSLRLogTime < @maxLogTimeLogTime
	begin
		declare @interval int
		set @interval = 5 * 365 * 24 * 3600

		exec dbo.RecalcStatisticLog @vehicleID, @interval, 1
	end;
	set @vehicleID = (select min(vehicle_id) 
						from vehicle v 
						where @vehicleID < v.vehicle_id )
end;
