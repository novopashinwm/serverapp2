﻿-------------------------------------------------
-- [dbo].[State_Log#Lasts]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[State_Log#Lasts]
-- Заполнение
MERGE [dbo].[State_Log#Lasts] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT TOP(1)
				[Vehicle_ID], [Type], [Log_Time#Last] = [Log_Time]
			FROM [dbo].[State_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = V.[VEHICLE_ID]
			AND   [Type]       IN (1)
			ORDER BY [Log_Time] DESC
		) L
	WHERE L.[Log_Time#Last] IS NOT NULL
) AS SRC ([Vehicle_ID], [Type], [Log_Time#Last])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Type]       = SRC.[Type]
WHEN     MATCHED AND DST.[Log_Time#Last] < SRC.[Log_Time#Last] THEN
	UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Type],     [Log_Time#Last])
	VALUES (SRC.[Vehicle_ID], SRC.[Type], SRC.[Log_Time#Last])
OUTPUT $action, INSERTED.*, DELETED.*
;