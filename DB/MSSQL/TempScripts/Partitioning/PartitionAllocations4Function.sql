	DECLARE
		@partitionFun    sysname = N'LogsPartitionFunction', -- ������� ���������������
		@partitionSch    sysname = N'LogsPartitionScheme',   -- ����� ���������������
		--@partitionFlg    sysname = N'',                      -- �������� ������ ���������������
		@partitionStg    sysname = N'#Stage';                -- ������� ������� ������������ ������
	-------------------------------------------------------------
	WITH
	B AS
	(
		SELECT
			[boundary_id]             = R.[boundary_id],
			[boundary_value_on_right] = F.[boundary_value_on_right],
			[boundary_value]          = CAST(R.[value] AS int)
		FROM sys.partition_functions F WITH(NOLOCK, NOWAIT)
			INNER JOIN sys.partition_range_values R WITH(NOLOCK, NOWAIT)
				ON F.[function_id] = R.[function_id]
		WHERE F.[name] = @partitionFun
	),
	P AS
	(
		SELECT
			P.[partition_number],
			[rows] = SUM(P.[rows])
		FROM sys.partitions P WITH(NOLOCK, NOWAIT)
			INNER JOIN sys.indexes             I ON I.[object_id]     = P.[object_id] AND I.[index_id] = P.[index_id]
			INNER JOIN sys.partition_schemes   S ON S.[data_space_id] = I.[data_space_id]
			INNER JOIN sys.partition_functions F ON F.[function_id]   = S.[function_id]
		WHERE 1=1
			-- ��������� ������� ���������������
			AND F.[name] = @partitionFun
			-- ��������� ����� ���������������
			AND S.[name] = @partitionSch
			-- ��� ������� �� �������� �������� #Stage
			AND OBJECT_NAME(I.[object_id]) NOT LIKE N'%' + @partitionStg
			-- � ������� � ������ ������� � ��������� #Stage ������ ������������
			AND OBJECT_ID(OBJECT_NAME(I.[object_id]) + @partitionStg, N'U') IS NOT NULL
		GROUP BY P.[partition_number]
	)
	SELECT
		[PartitionNumber]          = P.[partition_number],
		[PartitionBoundaryNum]     = ISNULL(B.[boundary_id],    0),
		[PartitionBoundaryVal]     = ISNULL(B.[boundary_value], 0),
		[PartitionBoundaryValDate] = dbo.lt2utc(CAST(ISNULL(B.[boundary_value], 0) AS int)),
		[PartitionMinDate]         = COALESCE(CONVERT(nvarchar, LEAD(DATEADD(second, +0, dbo.lt2utc(CAST(B.[boundary_value] AS int))), 0) OVER(ORDER BY B.[boundary_value]), 121), '-infinity'),
		[PartitionMaxDate]         = COALESCE(CONVERT(nvarchar, LEAD(DATEADD(second, -1, dbo.lt2utc(CAST(B.[boundary_value] AS int))), 1) OVER(ORDER BY B.[boundary_value]), 121), '+infinity'),
		[PartitionRows]            = P.[rows]
	FROM P
		LEFT JOIN B
			ON B.[boundary_id] + B.[boundary_value_on_right] = P.[partition_number]
	ORDER BY P.[partition_number] DESC;
GO