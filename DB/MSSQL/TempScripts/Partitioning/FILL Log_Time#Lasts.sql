TRUNCATE TABLE [dbo].[Log_Time#Lasts];
-- SET STATISTICS TIME OFF;
SET NOCOUNT ON;
DECLARE
	@Cnts int = 0,
	@PBid int, -- Идентификатор секции
	@SVid int, -- Идентификатор объекта наблюдения
	@SNum int; -- Номер датчика внутри объекта наблюдения
DECLARE
	@TVid TABLE ([Vehicle_Id] int PRIMARY KEY CLUSTERED);
INSERT INTO @TVid SELECT [VEHICLE_ID] FROM [dbo].[VEHICLE];

DECLARE @endTimeStamp datetime      = GETDATE();
DECLARE @begTimeStamp datetime      = @endTimeStamp;
DECLARE @durMessage   nvarchar(500) = NULL;

DECLARE BIDS_CUR CURSOR
FORWARD_ONLY READ_ONLY FAST_FORWARD LOCAL
FOR
SELECT
	[partition_number]
FROM sys.partitions
WHERE [object_id] = OBJECT_ID(N'[dbo].[Log_Time]', N'U')
AND   [index_id]  = 1
AND   [rows]      > 0
ORDER BY [partition_number] ASC;
OPEN BIDS_CUR;
FETCH NEXT FROM BIDS_CUR INTO @PBid;
WHILE (0 = @@FETCH_STATUS)
BEGIN
	RAISERROR('Partition=%d', 0, 1, @PBid) WITH NOWAIT;
	SET @Cnts = 0;
	DECLARE VEHS_CUR CURSOR
	FORWARD_ONLY READ_ONLY FAST_FORWARD LOCAL
	FOR
	SELECT [Vehicle_Id] FROM @TVid ORDER BY [VEHICLE_ID] ASC
	OPEN VEHS_CUR
	FETCH NEXT FROM VEHS_CUR INTO @SVid
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		IF (0 = @Cnts % 10000)
			SET @begTimeStamp = GETDATE();
		-------------------------------------------------------------
		MERGE [dbo].[Log_Time#Lasts] WITH (HOLDLOCK) AS DST
		USING
		(
			SELECT
				[Vehicle_ID], [Log_Time#Last] = MAX([Log_Time]), [InsertTime#Last] = MAX(COALESCE([InsertTime], dbo.lt2utc([Log_Time])))
			FROM [dbo].[Log_Time] WITH (NOLOCK)
			WHERE @PBid = $PARTITION.LogsPartitionFunction([Log_Time])
			AND   @SVid = [Vehicle_ID]
			GROUP BY [Vehicle_ID]
		) AS SRC ([Vehicle_ID], [Log_Time#Last], [InsertTime#Last])
			ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		WHEN     MATCHED AND(DST.[Log_Time#Last] < SRC.[Log_Time#Last] OR DST.[InsertTime#Last] < SRC.[InsertTime#Last]) THEN
			UPDATE
				SET
					DST.[Log_Time#Last]   =
						CASE
							WHEN (DST.[Log_Time#Last] IS NULL OR DST.[Log_Time#Last] < SRC.[Log_Time#Last])
							THEN SRC.[Log_Time#Last]
							ELSE DST.[Log_Time#Last]
						END,
					DST.[InsertTime#Last] =
						CASE
							WHEN (DST.[InsertTime#Last] IS NULL OR DST.[InsertTime#Last] < SRC.[InsertTime#Last])
							THEN SRC.[InsertTime#Last]
							ELSE DST.[InsertTime#Last]
						END
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (    [Vehicle_ID],     [Log_Time#Last],     [InsertTime#Last])
			VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[InsertTime#Last])
		--OUTPUT $action, INSERTED.*, DELETED.*
		;
		-------------------------------------------------------------
		SET @Cnts = @Cnts + 1;
		IF (0 = @Cnts % 10000)
		BEGIN
			SET @endTimeStamp = GETDATE();
			SET @durMessage   = CONVERT(varchar, DATEDIFF(second, @begTimeStamp, @endTimeStamp));
			RAISERROR(N'''%d'' Duration: %s second', 0, 1, @Cnts, @durMessage) WITH NOWAIT;
		END
		-------------------------------------------------------------
		FETCH NEXT FROM VEHS_CUR INTO @SVid
	END
	CLOSE      VEHS_CUR;
	DEALLOCATE VEHS_CUR;
	FETCH NEXT FROM BIDS_CUR INTO @PBid;
END
CLOSE      BIDS_CUR;
DEALLOCATE BIDS_CUR;
SELECT * FROM [dbo].[Log_Time#Lasts]