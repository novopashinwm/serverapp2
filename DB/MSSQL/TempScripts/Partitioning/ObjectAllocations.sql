SELECT
	[FileGroupId]   = a.[data_space_id],
	[FileGroupName] = FILEGROUP_NAME(a.[data_space_id]),
	[TableName]     = OBJECT_NAME(p.[object_id]),
	[IndexId]       = p.[index_id],
	--[Partition]     = p.[partition_number],
	[IndexName]     = COALESCE(i.[name], N'!!!HEAP without clustered index!!!'),
	[Rows]          = SUM(p.[rows]),
	[Size(KB)]      = 8 * SUM(a.[used_pages]),
	[Size(MB)]      = 8 * SUM(a.[used_pages]) / 1024,
	[Size(GB)]      = 8 * SUM(a.[used_pages]) / 1024 / 1024
FROM sys.allocation_units a
	INNER JOIN sys.partitions p
		ON a.[container_id] =
			CASE WHEN a.[type] IN (1, 3)
				THEN p.[hobt_id]
				ELSE p.[partition_id]
			END
		AND p.[object_id] > 1024
			LEFT JOIN sys.indexes i
				ON  i.[object_id] = p.[object_id]
				AND i.[index_id]  = p.[index_id]
-- ������� ��� ���������, ������ ����� �������� ������
WHERE 1=1
AND   a.[data_space_id] = 3
AND OBJECT_NAME(p.[object_id]) LIKE N'%[_]Log%'
--AND   i.[name] IS NULL
GROUP BY
	a.[data_space_id],
	p.[object_id],
	p.[index_id],
	--p.[partition_number],
	i.[name]
	--,
	--p.[rows]
ORDER BY
	--8 * SUM(a.[used_pages]) DESC,
	--p.[rows] DESC,
	OBJECT_NAME(p.[object_id]),
	i.[name],
	FILEGROUP_NAME(a.[data_space_id]);