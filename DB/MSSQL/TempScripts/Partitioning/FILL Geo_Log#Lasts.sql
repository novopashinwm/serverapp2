TRUNCATE TABLE [dbo].[Geo_Log#Lasts];
-- SET STATISTICS TIME OFF;
SET NOCOUNT ON;
DECLARE
	@Cnts int = 0,
	@PBid int, -- Идентификатор секции
	@SVid int, -- Идентификатор объекта наблюдения
	@SNum int; -- Номер датчика внутри объекта наблюдения
DECLARE
	@TVid TABLE ([Vehicle_Id] int PRIMARY KEY CLUSTERED);
INSERT INTO @TVid SELECT [VEHICLE_ID] FROM [dbo].[VEHICLE];

DECLARE @endTimeStamp datetime      = GETDATE();
DECLARE @begTimeStamp datetime      = @endTimeStamp;
DECLARE @durMessage   nvarchar(500) = NULL;

DECLARE BIDS_CUR CURSOR
FORWARD_ONLY READ_ONLY FAST_FORWARD LOCAL
FOR
SELECT
	[partition_number]
FROM sys.partitions
WHERE [object_id] = OBJECT_ID(N'[dbo].[Geo_Log]', N'U')
AND   [index_id]  = 1
AND   [rows]      > 0
ORDER BY [partition_number] ASC;
OPEN BIDS_CUR;
FETCH NEXT FROM BIDS_CUR INTO @PBid;
WHILE (0 = @@FETCH_STATUS)
BEGIN
	RAISERROR('Partition=%d', 0, 1, @PBid) WITH NOWAIT;
	SET @Cnts = 0;
	DECLARE VEHS_CUR CURSOR
	FORWARD_ONLY READ_ONLY FAST_FORWARD LOCAL
	FOR
	SELECT [Vehicle_Id] FROM @TVid ORDER BY [VEHICLE_ID] ASC
	OPEN VEHS_CUR
	FETCH NEXT FROM VEHS_CUR INTO @SVid
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		IF (0 = @Cnts % 10000)
			SET @begTimeStamp = GETDATE();
		-------------------------------------------------------------
		MERGE [dbo].[Geo_Log#Lasts] WITH (HOLDLOCK) AS DST
		USING
		(
			SELECT TOP(1)
				[Vehicle_ID],
				[LOG_TIME#Last] = [LOG_TIME],
				[LOG_TIME#Prev] = LAG([Log_Time]) OVER (ORDER BY [Log_Time] ASC),
				[Lng#Last]      = [Lng],
				[Lng#Prev]      = LAG([Lng])      OVER (ORDER BY [Log_Time] ASC),
				[Lat#Last]      = [Lat],
				[Lat#Prev]      = LAG([Lat])      OVER (ORDER BY [Log_Time] ASC)
			FROM [dbo].[Geo_Log] WITH (NOLOCK)
			WHERE @PBid <= $PARTITION.LogsPartitionFunction([Log_Time])
			AND   @SVid  = [Vehicle_ID]
			ORDER BY [Log_Time] DESC
		) AS SRC ([Vehicle_ID], [Log_Time#Last], [LOG_TIME#Prev], [Lng#Last], [Lng#Prev], [Lat#Last], [Lat#Prev])
			ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		WHEN     MATCHED AND DST.[Log_Time#Last] < SRC.[Log_Time#Last] THEN
			UPDATE
				SET
					DST.[LOG_TIME#Last] = SRC.[LOG_TIME#Last],
					DST.[LOG_TIME#Prev] = SRC.[LOG_TIME#Prev],
					DST.[Lng#Last]      = SRC.[Lng#Last],
					DST.[Lng#Prev]      = SRC.[Lng#Prev],
					DST.[Lat#Last]      = SRC.[Lat#Last],
					DST.[Lat#Prev]      = SRC.[Lat#Prev]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (    [Vehicle_ID],     [Log_Time#Last],     [LOG_TIME#Prev],     [Lng#Last],     [Lng#Prev],     [Lat#Last],     [Lat#Prev])
			VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[LOG_TIME#Prev], SRC.[Lng#Last], SRC.[Lng#Prev], SRC.[Lat#Last], SRC.[Lat#Prev])
		--OUTPUT $action, INSERTED.*, DELETED.*
		;
		-------------------------------------------------------------
		SET @Cnts = @Cnts + 1;
		IF (0 = @Cnts % 10000)
		BEGIN
			SET @endTimeStamp = GETDATE();
			SET @durMessage   = CONVERT(varchar, DATEDIFF(second, @begTimeStamp, @endTimeStamp));
			RAISERROR(N'''%d'' Duration: %s second', 0, 1, @Cnts, @durMessage) WITH NOWAIT;
		END
		-------------------------------------------------------------
		FETCH NEXT FROM VEHS_CUR INTO @SVid
	END
	CLOSE      VEHS_CUR;
	DEALLOCATE VEHS_CUR;
	FETCH NEXT FROM BIDS_CUR INTO @PBid;
END
CLOSE      BIDS_CUR;
DEALLOCATE BIDS_CUR;
SELECT * FROM [dbo].[Geo_Log#Lasts]