DECLARE
	@PFun sysname = N'LogsPartitionFunction',
	--@PLog sysname = N'Cell_Network_Log';
	--@PLog sysname = N'Controller_Sensor_Log#Partitions';
	--@PLog sysname = N'Geo_Log#Partitions';
	--@PLog sysname = N'Geo_Log_Ignored';
	--@PLog sysname = N'GPS_Log#Partitions';
	--@PLog sysname = N'Log_Time#Partitions';
	@PLog sysname = N'Log_Time';
	--@PLog sysname = N'Picture_Log';
	--@PLog sysname = N'State_Log#Partitions';
	--@PLog sysname = N'Statistic_Log#Partitions';
WITH
B AS
(
	SELECT
		[boundary_id]             = R.[boundary_id] + F.[boundary_value_on_right],
		[boundary_value]          = dbo.lt2utc(CAST(R.[value] AS int))
	FROM sys.partition_functions F WITH(NOLOCK, NOWAIT)
		INNER JOIN sys.partition_range_values R WITH(NOLOCK, NOWAIT)
			ON F.[function_id] = R.[function_id]
	WHERE F.[name] = @PFun
),
P AS
(
	SELECT
		[partition_number],
		[hobt_id],
		[rows]
	FROM sys.partitions WITH(NOLOCK, NOWAIT)
	WHERE [object_id] = OBJECT_ID(@PLog)
	AND   [index_id]  < 2
),
F AS
(
	SELECT
		[PartitionFilegroup] = G.[name], P.*
	FROM P
		LEFT JOIN sys.allocation_units  U
			ON P.[hobt_id] = U.[container_id]
	LEFT JOIN sys.filegroups G
		ON U.[data_space_id] = G.[data_space_id]
)
SELECT TOP(SELECT COUNT(*) FROM F)
	[PartitionNumber]    = F.[partition_number],
	[PartitionFilegroup] = F.[PartitionFilegroup],
	[PartitionMinDate]   = COALESCE(CONVERT(nvarchar, LEAD(DATEADD(second, +0, B.[boundary_value]), 0) OVER(ORDER BY B.[boundary_value]), 121), '-infinity'),
	[PartitionMaxDate]   = COALESCE(CONVERT(nvarchar, LEAD(DATEADD(second, -1, B.[boundary_value]), 1) OVER(ORDER BY B.[boundary_value]), 121), '+infinity'),
	[PartitionRows]      = F.[rows],
	[PartitionSqls]      = 'TRUNCATE TABLE [dbo].[' + @PLog + '#Stage];ALTER TABLE [dbo].[' + @PLog + '] SWITCH PARTITION ' + CAST(F.[partition_number] AS nvarchar(max)) + ' TO [dbo].[' + @PLog + '#Stage] PARTITION ' + CAST(F.[partition_number] AS nvarchar(max)) + ';TRUNCATE TABLE [dbo].[' + @PLog + '#Stage];'
FROM F
	LEFT JOIN B
		ON B.[boundary_id]     = F.[partition_number]
--WHERE F.[rows] > 0
ORDER BY F.[partition_number] DESC