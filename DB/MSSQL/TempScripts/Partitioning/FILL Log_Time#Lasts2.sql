﻿-- Очистка
TRUNCATE TABLE [dbo].[Log_Time#Lasts]
-- Заполнение
MERGE [dbo].[Log_Time#Lasts] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT
				V.[VEHICLE_ID],
				[Log_Time#Last]   = (SELECT TOP(1) [Log_Time]   FROM [dbo].[Log_Time] WHERE [Vehicle_Id] = V.[VEHICLE_ID] ORDER BY [Log_Time]   DESC),
				[InsertTime#Last] = (SELECT TOP(1) [InsertTime] FROM [dbo].[Log_Time] WHERE [Vehicle_Id] = V.[VEHICLE_ID] ORDER BY [InsertTime] DESC)
		) L
	WHERE L.[Log_Time#Last]   IS NOT NULL
	AND   L.[InsertTime#Last] IS NOT NULL
) AS SRC ([Vehicle_ID], [Log_Time#Last], [InsertTime#Last])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
WHEN     MATCHED AND(DST.[Log_Time#Last] < SRC.[Log_Time#Last] OR DST.[InsertTime#Last] < SRC.[InsertTime#Last]) THEN
	UPDATE
		SET
			DST.[Log_Time#Last]   =
				CASE
					WHEN (DST.[Log_Time#Last] IS NULL OR DST.[Log_Time#Last] < SRC.[Log_Time#Last])
					THEN SRC.[Log_Time#Last]
					ELSE DST.[Log_Time#Last]
				END,
			DST.[InsertTime#Last] =
				CASE
					WHEN (DST.[InsertTime#Last] IS NULL OR DST.[InsertTime#Last] < SRC.[InsertTime#Last])
					THEN SRC.[InsertTime#Last]
					ELSE DST.[InsertTime#Last]
				END
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time#Last],     [InsertTime#Last])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[InsertTime#Last])
OUTPUT $action, INSERTED.*, DELETED.*
;