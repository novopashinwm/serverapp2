﻿INSERT INTO [dbo].[Controller_Sensor_Log#Partitions] SELECT L.* FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[Controller_Sensor_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[Controller_Sensor_Log#Partitions] WHERE [Vehicle_ID] = V.[VEHICLE_ID]                               ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L
INSERT INTO [dbo].[State_Log#Partitions]             SELECT L.* FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[State_Log]             WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Type] = 1 AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[State_Log#Partitions]             WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Type] = 1 ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L
INSERT INTO [dbo].[Log_Time#Partitions]              SELECT L.* FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[Log_Time]              WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[Log_Time#Partitions]              WHERE [Vehicle_ID] = V.[VEHICLE_ID]                               ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L
INSERT INTO [dbo].[GPS_Log#Partitions]               SELECT
L.[Vehicle_ID], L.[Log_Time], L.[Satellites], L.[Firmware], L.[Altitude], L.[Speed], L.[Course]
																FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[GPS_Log]               WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[GPS_Log#Partitions]               WHERE [Vehicle_ID] = V.[VEHICLE_ID]                               ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L
INSERT INTO [dbo].[Geo_Log#Partitions]               SELECT L.* FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[Geo_Log]               WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[Geo_Log#Partitions]               WHERE [Vehicle_ID] = V.[VEHICLE_ID]                               ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L
INSERT INTO [dbo].[Statistic_Log#Partitions]         SELECT L.* FROM [dbo].[VEHICLE] V CROSS APPLY (SELECT * FROM [dbo].[Statistic_Log]         WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Log_Time] > (SELECT [LOG_TIME] FROM [dbo].[Statistic_Log#Partitions]         WHERE [Vehicle_ID] = V.[VEHICLE_ID]                               ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)) L

-- Cell_Network_Log
-- Picture_Log
-- State_Log
-- Geo_Log_Ignored
-- Statistic_Log
-- Geo_Log
-- GPS_Log
-- Log_Time
-- Controller_Sensor_Log