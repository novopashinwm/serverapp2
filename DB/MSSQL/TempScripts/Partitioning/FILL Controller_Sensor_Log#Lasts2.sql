﻿-------------------------------------------------
-- [dbo].[Controller_Sensor_Log#Lasts] -- PROCEDURE [dbo].[AddSensorValues]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Controller_Sensor_Log#Lasts]
-- Заполнение
MERGE [dbo].[Controller_Sensor_Log#Lasts] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT DISTINCT
				[Vehicle_ID], [Number]
			FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = V.[VEHICLE_ID]
		) N
			CROSS APPLY
			(
				SELECT TOP(1)
					[Vehicle_ID], [Number], [Log_Time#Last] = [Log_Time]
				FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK)
				WHERE [Vehicle_ID] = N.[VEHICLE_ID]
				AND   [Number]     = N.[Number]
				ORDER BY [Log_Time] DESC
			) L
	WHERE L.[Log_Time#Last] IS NOT NULL
) AS SRC ([Vehicle_ID], [Number], [Log_Time#Last])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Number]     = SRC.[Number]
WHEN     MATCHED AND DST.[Log_Time#Last] < SRC.[Log_Time#Last] THEN
	UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Number],     [Log_Time#Last])
	VALUES (SRC.[Vehicle_ID], SRC.[Number], SRC.[Log_Time#Last])
OUTPUT $action, INSERTED.*, DELETED.*
;