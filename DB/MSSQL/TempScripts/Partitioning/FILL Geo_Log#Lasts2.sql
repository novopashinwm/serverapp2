﻿-------------------------------------------------
-- [dbo].[Geo_Log#Lasts] -- PROCEDURE [dbo].[AddMonitoreeLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Geo_Log#Lasts]
-- Заполнение
MERGE [dbo].[Geo_Log#Lasts] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT TOP(1)
				[Vehicle_ID]    = S.[Vehicle_ID],
				[LOG_TIME#Last] = S.[LOG_TIME],
				[LOG_TIME#Prev] = LAG(S.[LOG_TIME]) OVER (ORDER BY S.[LOG_TIME] ASC),
				[Lng#Last]      = S.[Lng],
				[Lng#Prev]      = LAG(S.[Lng])      OVER (ORDER BY S.[LOG_TIME] ASC),
				[Lat#Last]      = S.[Lat],
				[Lat#Prev]      = LAG(S.[Lat])      OVER (ORDER BY S.[LOG_TIME] ASC)
			FROM
			(
				SELECT TOP(2)
					[Vehicle_ID],
					[LOG_TIME],
					[Lng],
					[Lat]
				FROM [dbo].[Geo_Log] WITH (NOLOCK)
				WHERE [Vehicle_ID] = V.[VEHICLE_ID]
				ORDER BY [Log_Time] DESC
			) S
			ORDER BY S.[LOG_TIME] DESC
		) L
	WHERE L.[LOG_TIME#Last]   IS NOT NULL
) AS SRC ([Vehicle_ID], [Log_Time#Last], [LOG_TIME#Prev], [Lng#Last], [Lng#Prev], [Lat#Last], [Lat#Prev])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
WHEN     MATCHED AND DST.[Log_Time#Last] < SRC.[Log_Time#Last] THEN
	UPDATE
		SET
			DST.[LOG_TIME#Last] = SRC.[LOG_TIME#Last],
			DST.[LOG_TIME#Prev] = SRC.[LOG_TIME#Prev],
			DST.[Lng#Last]      = SRC.[Lng#Last],
			DST.[Lng#Prev]      = SRC.[Lng#Prev],
			DST.[Lat#Last]      = SRC.[Lat#Last],
			DST.[Lat#Prev]      = SRC.[Lat#Prev]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time#Last],     [LOG_TIME#Prev],     [Lng#Last],     [Lng#Prev],     [Lat#Last],     [Lat#Prev])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[LOG_TIME#Prev], SRC.[Lng#Last], SRC.[Lng#Prev], SRC.[Lat#Last], SRC.[Lat#Prev])
OUTPUT $action, INSERTED.*, DELETED.*
;