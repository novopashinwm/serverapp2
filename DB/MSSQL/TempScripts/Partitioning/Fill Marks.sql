﻿/*
SET STATISTICS IO   ON;
SET STATISTICS TIME ON;
-------------------------------------------------
SELECT * FROM [dbo].[Log_Time#Marks]              -- PROCEDURE [dbo].[AddMonitoreeLog]
SELECT * FROM [dbo].[Geo_Log#Marks]               -- PROCEDURE [dbo].[AddMonitoreeLog]
SELECT * FROM [dbo].[GPS_Log#Marks]               -- PROCEDURE [dbo].[AddMonitoreeLog]
SELECT * FROM [dbo].[Controller_Sensor_Log#Marks] -- PROCEDURE [dbo].[AddSensorValues]
SELECT * FROM [dbo].[Cell_Network_Log#Marks]      -- PROCEDURE [dbo].[AddCell_Network_Log]
SELECT * FROM [dbo].[Geo_Log_Ignored#Marks]       -- PROCEDURE [dbo].[Add_Geo_Log_Ignored]
SELECT * FROM [dbo].[Picture_Log#Marks]           -- PROCEDURE [dbo].[AddPictureLog]
SELECT * FROM [dbo].[State_Log#Marks]             -- PROCEDURE [dbo].[UpdateStateLog]
SELECT * FROM [dbo].[Statistic_Log#Marks]         -- PROCEDURE [dbo].[RecalcStatisticLog]
-------------------------------------------------
*/
-------------------------------------------------
-- [dbo].[Log_Time#Marks] -- PROCEDURE [dbo].[AddMonitoreeLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Log_Time#Marks]
-- Заполнение
MERGE [dbo].[Log_Time#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			-- Индексные записи по Log_Time
			SELECT [Vehicle_ID],    [Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]    ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID],    [Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]   DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
			--	UNION
			---- Индексные записи по InsertTime
			--SELECT [Vehicle_ID], -1*[Log_Time], [Media], [InsertTime]
			--FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [InsertTime]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
			--	UNION
			--SELECT [Vehicle_ID], -1*[Log_Time], [Media], [InsertTime]
			--FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [InsertTime] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Media], [InsertTime])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Media],     [InsertTime])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Media], SRC.[InsertTime])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Geo_Log#Marks] -- PROCEDURE [dbo].[AddMonitoreeLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Geo_Log#Marks]
-- Заполнение
MERGE [dbo].[Geo_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [LOG_TIME], [Lng], [Lat]
			FROM [dbo].[Geo_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [LOG_TIME]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [LOG_TIME], [Lng], [Lat]
			FROM [dbo].[Geo_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [LOG_TIME], [Lng], [Lat])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Lng],     [Lat])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Lng], SRC.[Lat])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[GPS_Log#Marks] -- PROCEDURE [dbo].[AddMonitoreeLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[GPS_Log#Marks]
-- Заполнение
MERGE [dbo].[GPS_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course]
			FROM [dbo].[GPS_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course]
			FROM [dbo].[GPS_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Satellites],     [Firmware],     [Altitude],     [Speed],     [Course])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Satellites], SRC.[Firmware], SRC.[Altitude], SRC.[Speed], SRC.[Course])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Controller_Sensor_Log#Marks] -- PROCEDURE [dbo].[AddSensorValues]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Controller_Sensor_Log#Marks]
-- Заполнение
MERGE [dbo].[Controller_Sensor_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Number], [Value]
			FROM [dbo].[Controller_Sensor_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Number], [Value]
			FROM [dbo].[Controller_Sensor_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Number], [Value])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
	AND DST.[Number]     = SRC.[Number]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Number],     [Value])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Number], SRC.[Value])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Cell_Network_Log#Marks] -- PROCEDURE [dbo].[AddCell_Network_Log]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Cell_Network_Log#Marks]
-- Заполнение
MERGE [dbo].[Cell_Network_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Number], [Country_Code_ID], [Network_Code_ID], [Cell_ID], [LAC], [SignalStrength], [SAC], [ECI]
			FROM [dbo].[Cell_Network_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Number], [Country_Code_ID], [Network_Code_ID], [Cell_ID], [LAC], [SignalStrength], [SAC], [ECI]
			FROM [dbo].[Cell_Network_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Number], [Country_Code_ID], [Network_Code_ID], [Cell_ID], [LAC], [SignalStrength], [SAC], [ECI])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Log_Time]   = SRC.[Log_Time]
		AND DST.[Number]     = SRC.[Number]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Number],     [Country_Code_ID],     [Network_Code_ID],     [Cell_ID],     [LAC],     [SignalStrength],     [SAC],     [ECI])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Number], SRC.[Country_Code_ID], SRC.[Network_Code_ID], SRC.[Cell_ID], SRC.[LAC], SRC.[SignalStrength], SRC.[SAC], SRC.[ECI])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Geo_Log_Ignored#Marks] -- PROCEDURE [dbo].[Add_Geo_Log_Ignored]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Geo_Log_Ignored#Marks]
-- Заполнение
MERGE [dbo].[Geo_Log_Ignored#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Reason], [Lng], [Lat], [Speed], [Course], [Height], [HDOP]
			FROM [dbo].[Geo_Log_Ignored] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Reason], [Lng], [Lat], [Speed], [Course], [Height], [HDOP]
			FROM [dbo].[Geo_Log_Ignored] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Reason], [Lng], [Lat], [Speed], [Course], [Height], [HDOP])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Log_Time]   = SRC.[Log_Time]
		AND DST.[Reason]     = SRC.[Reason]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Reason],     [Lng],     [Lat],     [Speed],     [Course],     [Height],     [HDOP])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Reason], SRC.[Lng], SRC.[Lat], SRC.[Speed], SRC.[Course], SRC.[Height], SRC.[HDOP])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Picture_Log#Marks] -- PROCEDURE [dbo].[AddPictureLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Picture_Log#Marks]
-- Заполнение
MERGE [dbo].[Picture_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Camera_Number], [Photo_Number], [Picture], [Url]
			FROM [dbo].[Picture_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Camera_Number], [Photo_Number], [Picture], [Url]
			FROM [dbo].[Picture_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Camera_Number], [Photo_Number], [Picture], [Url])
		ON  DST.[Vehicle_ID]    = SRC.[Vehicle_ID]
		AND DST.[Log_Time]      = SRC.[Log_Time]
		AND DST.[Camera_Number] = SRC.[Camera_Number]
		AND DST.[Photo_Number]  = SRC.[Photo_Number]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Camera_Number],     [Photo_Number],     [Picture],     [Url])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Camera_Number], SRC.[Photo_Number], SRC.[Picture], SRC.[Url])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[State_Log#Marks] -- PROCEDURE [dbo].[UpdateStateLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[State_Log#Marks]
-- Заполнение
MERGE [dbo].[State_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Type], [Value]
			FROM [dbo].[State_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Type] = 1 ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Type], [Value]
			FROM [dbo].[State_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] AND [Type] = 1 ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Type], [Value])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Type]       = SRC.[Type]
		AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Type],     [Value])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Type], SRC.[Value])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Statistic_Log#Marks]         -- PROCEDURE [dbo].[RecalcStatisticLog]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Statistic_Log#Marks]
-- Заполнение
MERGE [dbo].[Statistic_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Odometer], [IgnitionOnTime]
			FROM [dbo].[Statistic_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Odometer], [IgnitionOnTime]
			FROM [dbo].[Statistic_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Odometer], [IgnitionOnTime])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Odometer],     [IgnitionOnTime])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Odometer], SRC.[IgnitionOnTime])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;