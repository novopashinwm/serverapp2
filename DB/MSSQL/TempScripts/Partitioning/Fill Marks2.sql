﻿-------------------------------------------------
-- [dbo].[Log_Time#Marks]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Log_Time#Marks]
-- Заполнение
MERGE [dbo].[Log_Time#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT L.*
	FROM
	(
		SELECT
			[Vehicle_ID],
			[MIN_Log_Time]   = MIN([Log_Time]),
			[MIN_InsertTime] = MIN([InsertTime]),
			[MAX_Log_Time]   = MAX([Log_Time]),
			[MAX_InsertTime] = MAX([InsertTime])
		FROM [dbo].[Log_Time#Partitions]
		GROUP BY [Vehicle_ID]
	) A
		CROSS APPLY
		(
			-- Первые
			SELECT TOP(1) [Vehicle_ID],    [Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [Log_Time]   = A.[MIN_Log_Time]
				UNION
			SELECT TOP(1) [Vehicle_ID], -1*[Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [InsertTime] = A.[MIN_InsertTime]
				UNION
			-- Последние
			SELECT TOP(1) [Vehicle_ID],    [Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [Log_Time]   = A.[MAX_Log_Time]
				UNION
			SELECT TOP(1) [Vehicle_ID], -1*[Log_Time], [Media], [InsertTime]
			FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [InsertTime] = A.[MAX_InsertTime]
				UNION
			-- Предпоследние
			SELECT TOP(1) * FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [Log_Time]   < A.[MAX_Log_Time]   ORDER BY [Log_Time]   DESC
				UNION
			SELECT TOP(1) * FROM [dbo].[Log_Time#Partitions] WHERE [Vehicle_ID] = A.[Vehicle_ID] AND [InsertTime] < A.[MAX_InsertTime] ORDER BY [InsertTime] DESC
		) L([Vehicle_ID], [Log_Time], [Media], [InsertTime])
) AS SRC ([Vehicle_ID], [Log_Time], [Media], [InsertTime])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Media],     [InsertTime])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Media], SRC.[InsertTime])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Geo_Log#Marks]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Geo_Log#Marks]
-- Заполнение
MERGE [dbo].[Geo_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [LOG_TIME], [Lng], [Lat]
			FROM [dbo].[Geo_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [LOG_TIME]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [LOG_TIME], [Lng], [Lat]
			FROM [dbo].[Geo_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [LOG_TIME] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [LOG_TIME], [Lng], [Lat])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Lng],     [Lat])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Lng], SRC.[Lat])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[GPS_Log#Marks]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[GPS_Log#Marks]
-- Заполнение
MERGE [dbo].[GPS_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course]
			FROM [dbo].[GPS_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course]
			FROM [dbo].[GPS_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Satellites], [Firmware],[Altitude], [Speed], [Course])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Satellites],     [Firmware],     [Altitude],     [Speed],     [Course])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Satellites], SRC.[Firmware], SRC.[Altitude], SRC.[Speed], SRC.[Course])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;
-------------------------------------------------
-- [dbo].[Controller_Sensor_Log#Marks]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Controller_Sensor_Log#Marks]
-- Заполнение
MERGE [dbo].[Controller_Sensor_Log#Marks] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT [Vehicle_ID], [Log_Time], [Number], [Value]
			FROM [dbo].[Controller_Sensor_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time]  ASC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
				UNION
			SELECT [Vehicle_ID], [Log_Time], [Number], [Value]
			FROM [dbo].[Controller_Sensor_Log] WHERE [Vehicle_ID] = V.[VEHICLE_ID] ORDER BY [Log_Time] DESC OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY
		) L
) AS SRC ([Vehicle_ID], [Log_Time], [Number], [Value])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	AND DST.[Log_Time]   = SRC.[Log_Time]
	AND DST.[Number]     = SRC.[Number]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time],     [Number],     [Value])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Number], SRC.[Value])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $action, INSERTED.*, DELETED.*;