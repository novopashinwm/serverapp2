﻿----------------------------------------------------------------------------------------------------------------
-- Cell_Network_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Cell_Network_Log#Template]
(
	[Vehicle_ID]      int      NOT NULL,
	[Log_Time]        int      NOT NULL,
	[Number]          tinyint  NOT NULL,
	[Country_Code_ID] smallint NOT NULL,
	[Network_Code_ID] smallint NOT NULL,
	[Cell_ID]         int      NOT NULL,
	[LAC]             int      NOT NULL,
	[SignalStrength]  smallint NOT NULL,
	[ECI]             int          NULL,
	[SAC]             int          NULL,
		CONSTRAINT [PK_Cell_Network_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC,
			[Number]     ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Cell_Network_Log#Template]
	ON [dbo].[Cell_Network_Log#Template] ([Vehicle_ID], [Log_Time], [Number])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Cell_Network_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Cell_Network_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Cell_Network_Log#Template.CX_Cell_Network_Log#Template', 'CX_Cell_Network_Log#Stage', 'INDEX'
	EXEC sp_rename 'Cell_Network_Log#Template.PK_Cell_Network_Log#Template', 'PK_Cell_Network_Log#Stage', 'INDEX'
	EXEC sp_rename                              'Cell_Network_Log#Template',    'Cell_Network_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Cell_Network_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Cell_Network_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Cell_Network_Log#Template.CX_Cell_Network_Log#Template', 'CX_Cell_Network_Log#Marks', 'INDEX'
	EXEC sp_rename 'Cell_Network_Log#Template.PK_Cell_Network_Log#Template', 'PK_Cell_Network_Log#Marks', 'INDEX'
	EXEC sp_rename                              'Cell_Network_Log#Template',    'Cell_Network_Log#Marks'
	DROP INDEX                    [CX_Cell_Network_Log#Marks] ON [dbo].[Cell_Network_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Cell_Network_Log#Marks] ON [dbo].[Cell_Network_Log#Marks]
		([Vehicle_ID], [Log_Time], [Number]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Cell_Network_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Cell_Network_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Cell_Network_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Cell_Network_Log#Template.CX_Cell_Network_Log#Template', 'CX_Cell_Network_Log#Partitions', 'INDEX'
	EXEC sp_rename 'Cell_Network_Log#Template.PK_Cell_Network_Log#Template', 'PK_Cell_Network_Log#Partitions', 'INDEX'
	EXEC sp_rename                              'Cell_Network_Log#Template',    'Cell_Network_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Cell_Network_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Cell_Network_Log#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Picture_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Picture_Log#Template]
(
	[Vehicle_ID]    int            NOT NULL,
	[Log_Time]      int            NOT NULL,
	[Picture]       varbinary(max)     NULL,
	[Camera_Number] int            NOT NULL
		CONSTRAINT [DF_Picture_Log#Template_Camera_Number] DEFAULT (0),
	[Photo_Number]  int            NOT NULL
		CONSTRAINT [DF_Picture_Log#Template_Photo_Number]  DEFAULT (0),
	[Url]           varchar(255) NULL,
		CONSTRAINT [PK_Picture_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID]    ASC,
			[Log_Time]      ASC,
			[Camera_Number] ASC,
			[Photo_Number]  ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Picture_Log#Template]
	ON [dbo].[Picture_Log#Template] ([Vehicle_ID], [Log_Time], [Camera_Number], [Photo_Number])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Picture_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Picture_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Picture_Log#Template.CX_Picture_Log#Template',               'CX_Picture_Log#Stage', 'INDEX'
	EXEC sp_rename 'Picture_Log#Template.PK_Picture_Log#Template',               'PK_Picture_Log#Stage', 'INDEX'
	EXEC sp_rename                         'Picture_Log#Template',                  'Picture_Log#Stage'
	EXEC sp_rename                      'DF_Picture_Log#Template_Camera_Number', 'DF_Picture_Log#Stage_Camera_Number'
	EXEC sp_rename                      'DF_Picture_Log#Template_Photo_Number',  'DF_Picture_Log#Stage_Photo_Number'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Picture_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Picture_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Picture_Log#Template.CX_Picture_Log#Template',               'CX_Picture_Log#Marks', 'INDEX'
	EXEC sp_rename 'Picture_Log#Template.PK_Picture_Log#Template',               'PK_Picture_Log#Marks', 'INDEX'
	EXEC sp_rename                         'Picture_Log#Template',                  'Picture_Log#Marks'
	EXEC sp_rename                      'DF_Picture_Log#Template_Camera_Number', 'DF_Picture_Log#Marks_Camera_Number'
	EXEC sp_rename                      'DF_Picture_Log#Template_Photo_Number',  'DF_Picture_Log#Marks_Photo_Number'
	DROP INDEX                    [CX_Picture_Log#Marks] ON [dbo].[Picture_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Picture_Log#Marks] ON [dbo].[Picture_Log#Marks]
		([Vehicle_ID], [Log_Time], [Camera_Number], [Photo_Number]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Picture_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Picture_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Picture_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Picture_Log#Template.CX_Picture_Log#Template',               'CX_Picture_Log#Partitions', 'INDEX'
	EXEC sp_rename 'Picture_Log#Template.PK_Picture_Log#Template',               'PK_Picture_Log#Partitions', 'INDEX'
	EXEC sp_rename                         'Picture_Log#Template',                  'Picture_Log#Partitions'
	EXEC sp_rename                      'DF_Picture_Log#Template_Camera_Number', 'DF_Picture_Log#Partitions_Camera_Number'
	EXEC sp_rename                      'DF_Picture_Log#Template_Photo_Number',  'DF_Picture_Log#Partitions_Photo_Number'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Picture_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Picture_Log#Template]
END
GO 3
IF (EXISTS(SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'[dbo].[Picture_Log]', N'U') AND [name] = 'PK_Picture_Log_VID_LT_CN_PN'))
	EXEC sp_rename 'Picture_Log.PK_Picture_Log_VID_LT_CN_PN', 'PK_Picture_Log', 'INDEX';
GO
----------------------------------------------------------------------------------------------------------------
-- State_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[State_Log#Template]
(
	[Vehicle_ID] int     NOT NULL,
	[Log_Time]   int     NOT NULL,
	[Type]       tinyint NOT NULL,
	[Value]      int     NOT NULL,
		CONSTRAINT [PK_State_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Type]       ASC,
			[Log_Time]   ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_State_Log#Template]
	ON [dbo].[State_Log#Template] ([Vehicle_ID], [Type], [Log_Time])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[State_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[State_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'State_Log#Template.CX_State_Log#Template', 'CX_State_Log#Stage', 'INDEX'
	EXEC sp_rename 'State_Log#Template.PK_State_Log#Template', 'PK_State_Log#Stage', 'INDEX'
	EXEC sp_rename                       'State_Log#Template',    'State_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[State_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[State_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'State_Log#Template.CX_State_Log#Template', 'CX_State_Log#Marks', 'INDEX'
	EXEC sp_rename 'State_Log#Template.PK_State_Log#Template', 'PK_State_Log#Marks', 'INDEX'
	EXEC sp_rename                       'State_Log#Template',    'State_Log#Marks'
	DROP INDEX                    [CX_State_Log#Marks] ON [dbo].[State_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_State_Log#Marks] ON [dbo].[State_Log#Marks]
		([Vehicle_ID], [Type], [Log_Time]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[State_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[State_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[State_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'State_Log#Template.CX_State_Log#Template', 'CX_State_Log#Partitions', 'INDEX'
	EXEC sp_rename 'State_Log#Template.PK_State_Log#Template', 'PK_State_Log#Partitions', 'INDEX'
	EXEC sp_rename                       'State_Log#Template',    'State_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[State_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[State_Log#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Geo_Log_Ignored
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Geo_Log_Ignored#Template]
(
	[Vehicle_ID] int           NOT NULL,
	[Log_Time]   int           NOT NULL,
	[Reason]     tinyint       NOT NULL,
	[Lng]        numeric(8, 5) NOT NULL,
	[Lat]        numeric(8, 5) NOT NULL,
	[Speed]      smallint          NULL,
	[Course]     smallint          NULL,
	[Height]     smallint          NULL,
	[HDOP]       numeric(4, 2)     NULL,
	[InsertTime] datetime      NOT NULL CONSTRAINT [DF_Geo_Log_Ignored#Template]  DEFAULT (GETUTCDATE()),
		CONSTRAINT [PK_Geo_Log_Ignored#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC,
			[Reason]     ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time])
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Geo_Log_Ignored#Template]
	ON [dbo].[Geo_Log_Ignored#Template] ([Vehicle_ID], [Log_Time], [Reason])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Geo_Log_Ignored#Template.CX_Geo_Log_Ignored#Template', 'CX_Geo_Log_Ignored#Stage', 'INDEX'
	EXEC sp_rename 'Geo_Log_Ignored#Template.PK_Geo_Log_Ignored#Template', 'PK_Geo_Log_Ignored#Stage', 'INDEX'
	EXEC sp_rename                            'Geo_Log_Ignored#Template',     'Geo_Log_Ignored#Stage'
	EXEC sp_rename                          'DF_Geo_Log_Ignored#Template', 'DF_Geo_Log_Ignored#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Geo_Log_Ignored#Template.CX_Geo_Log_Ignored#Template', 'CX_Geo_Log_Ignored#Marks', 'INDEX'
	EXEC sp_rename 'Geo_Log_Ignored#Template.PK_Geo_Log_Ignored#Template', 'PK_Geo_Log_Ignored#Marks', 'INDEX'
	EXEC sp_rename                            'Geo_Log_Ignored#Template',     'Geo_Log_Ignored#Marks'
	EXEC sp_rename                          'DF_Geo_Log_Ignored#Template', 'DF_Geo_Log_Ignored#Marks'
	DROP INDEX                    [CX_Geo_Log_Ignored#Marks] ON [dbo].[Geo_Log_Ignored#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Geo_Log_Ignored#Marks] ON [dbo].[Geo_Log_Ignored#Marks]
		([Vehicle_ID], [Log_Time], [Reason]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Geo_Log_Ignored]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Geo_Log_Ignored#Template.CX_Geo_Log_Ignored#Template', 'CX_Geo_Log_Ignored#Partitions', 'INDEX'
	EXEC sp_rename 'Geo_Log_Ignored#Template.PK_Geo_Log_Ignored#Template', 'PK_Geo_Log_Ignored#Partitions', 'INDEX'
	EXEC sp_rename                            'Geo_Log_Ignored#Template',     'Geo_Log_Ignored#Partitions'
	EXEC sp_rename                          'DF_Geo_Log_Ignored#Template', 'DF_Geo_Log_Ignored#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Geo_Log_Ignored#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Statistic_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Statistic_Log#Template]
(
	[Vehicle_ID]     int    NOT NULL,
	[Log_Time]       int    NOT NULL,
	[Odometer]       bigint NOT NULL,
	[IgnitionOnTime] int    NOT NULL,
		CONSTRAINT [PK_Statistic_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC
		) ON [LogsPartitionScheme]([Log_Time]),
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Statistic_Log#Template]
	ON [dbo].[Statistic_Log#Template] ([Vehicle_ID], [Log_Time])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED INDEX [IX_Statistic_Log#Template_Vehicle_ID_Odometer_Log_Time] ON [dbo].[Statistic_Log#Template]
(
	[Vehicle_ID] ASC,
	[Odometer]   ASC,
	[Log_Time]   ASC
) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Statistic_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Statistic_Log#Stage]', N'U') IS NULL)
BEGIN
	-- Возможно не нужен
	EXEC sp_rename 'Statistic_Log#Template.IX_Statistic_Log#Template_Vehicle_ID_Odometer_Log_Time', 'IX_Statistic_Log#Stage_Vehicle_ID_Odometer_Log_Time', 'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.CX_Statistic_Log#Template',                              'CX_Statistic_Log#Stage',                              'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.PK_Statistic_Log#Template',                              'PK_Statistic_Log#Stage',                              'INDEX'
	EXEC sp_rename                           'Statistic_Log#Template',                                 'Statistic_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Statistic_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Statistic_Log#Marks]', N'U') IS NULL)
BEGIN
	-- Возможно не нужен
	EXEC sp_rename 'Statistic_Log#Template.IX_Statistic_Log#Template_Vehicle_ID_Odometer_Log_Time', 'IX_Statistic_Log#Marks_Vehicle_ID_Odometer_Log_Time', 'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.CX_Statistic_Log#Template',                              'CX_Statistic_Log#Marks',                              'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.PK_Statistic_Log#Template',                              'PK_Statistic_Log#Marks',                              'INDEX'
	EXEC sp_rename                           'Statistic_Log#Template',                                 'Statistic_Log#Marks'
	DROP INDEX                    [CX_Statistic_Log#Marks]                              ON [dbo].[Statistic_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Statistic_Log#Marks]                              ON [dbo].[Statistic_Log#Marks]
		([Vehicle_ID], [Log_Time])             WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
	CREATE     NONCLUSTERED INDEX [IX_Statistic_Log#Marks_Vehicle_ID_Odometer_Log_Time] ON [dbo].[Statistic_Log#Marks]
		([Vehicle_ID], [Odometer], [Log_Time]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Statistic_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Statistic_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Statistic_Log]', N'U') AND [partition_number] > 1))
BEGIN
	-- Возможно не нужен
	EXEC sp_rename 'Statistic_Log#Template.IX_Statistic_Log#Template_Vehicle_ID_Odometer_Log_Time', 'IX_Statistic_Log_Log#Partitions_Vehicle_ID_Odometer_Log_Time', 'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.CX_Statistic_Log#Template',                              'CX_Statistic_Log#Partitions',                                  'INDEX'
	EXEC sp_rename 'Statistic_Log#Template.PK_Statistic_Log#Template',                              'PK_Statistic_Log#Partitions',                                  'INDEX'
	EXEC sp_rename                           'Statistic_Log#Template',                                 'Statistic_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Statistic_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Statistic_Log#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Geo_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Geo_Log#Template]
(
	[Vehicle_ID] int           NOT NULL,
	[LOG_TIME]   int           NOT NULL,
	[Lng]        numeric(8, 5)     NULL,
	[Lat]        numeric(8, 5)     NULL,
		CONSTRAINT [PK_Geo_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[LOG_TIME]   ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Geo_Log#Template]
	ON [dbo].[Geo_Log#Template] ([Vehicle_ID], [LOG_TIME])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Geo_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Geo_Log#Template.CX_Geo_Log#Template', 'CX_Geo_Log#Stage', 'INDEX'
	EXEC sp_rename 'Geo_Log#Template.PK_Geo_Log#Template', 'PK_Geo_Log#Stage', 'INDEX'
	EXEC sp_rename                     'Geo_Log#Template',    'Geo_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Geo_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Geo_Log#Template.CX_Geo_Log#Template', 'CX_Geo_Log#Marks', 'INDEX'
	EXEC sp_rename 'Geo_Log#Template.PK_Geo_Log#Template', 'PK_Geo_Log#Marks', 'INDEX'
	EXEC sp_rename                     'Geo_Log#Template',    'Geo_Log#Marks'
	DROP INDEX                    [CX_Geo_Log#Marks] ON [dbo].[Geo_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Geo_Log#Marks] ON [dbo].[Geo_Log#Marks]
		([Vehicle_ID], [LOG_TIME]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Geo_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Geo_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Geo_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Geo_Log#Template.CX_Geo_Log#Template', 'CX_Geo_Log#Partitions', 'INDEX'
	EXEC sp_rename 'Geo_Log#Template.PK_Geo_Log#Template', 'PK_Geo_Log#Partitions', 'INDEX'
	EXEC sp_rename                     'Geo_Log#Template',    'Geo_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Geo_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Geo_Log#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- GPS_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[GPS_Log#Template]
(
	[Vehicle_ID]    int      NOT NULL,
	[Log_Time]      int      NOT NULL,
	[Satellites]    tinyint      NULL,
	[Firmware]      smallint     NULL,
	[Altitude]      int          NULL,
	[Speed]         tinyint      NULL,
	[Course]        tinyint      NULL,
	[CourseDegrees]  AS (CONVERT(int,([Course]/(256.0))*(360),(0))),
		CONSTRAINT [PK_GPS_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_GPS_Log#Template]
	ON [dbo].[GPS_Log#Template] ([Vehicle_ID], [Log_Time])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[GPS_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[GPS_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'GPS_Log#Template.CX_GPS_Log#Template', 'CX_GPS_Log#Stage', 'INDEX'
	EXEC sp_rename 'GPS_Log#Template.PK_GPS_Log#Template', 'PK_GPS_Log#Stage', 'INDEX'
	EXEC sp_rename                     'GPS_Log#Template',    'GPS_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[GPS_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[GPS_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'GPS_Log#Template.CX_GPS_Log#Template', 'CX_GPS_Log#Marks', 'INDEX'
	EXEC sp_rename 'GPS_Log#Template.PK_GPS_Log#Template', 'PK_GPS_Log#Marks', 'INDEX'
	EXEC sp_rename                     'GPS_Log#Template',    'GPS_Log#Marks'
	DROP INDEX                    [CX_GPS_Log#Marks] ON [dbo].[GPS_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_GPS_Log#Marks] ON [dbo].[GPS_Log#Marks]
		([Vehicle_ID], [Log_Time]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[GPS_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[GPS_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[GPS_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'GPS_Log#Template.CX_GPS_Log#Template', 'CX_GPS_Log#Partitions', 'INDEX'
	EXEC sp_rename 'GPS_Log#Template.PK_GPS_Log#Template', 'PK_GPS_Log#Partitions', 'INDEX'
	EXEC sp_rename                     'GPS_Log#Template',    'GPS_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[GPS_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[GPS_Log#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Log_Time
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Log_Time#Template]
(
	[Vehicle_ID] int      NOT NULL,
	[Log_Time]   int      NOT NULL,
	[Media]      tinyint      NULL,
	[InsertTime] datetime     NULL,
		CONSTRAINT [PK_Log_Time#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Log_Time#Template]
	ON [dbo].[Log_Time#Template] ([Vehicle_ID], [Log_Time], [InsertTime])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED INDEX [IX_Log_Time#Template_InsertTime] ON [dbo].[Log_Time#Template]
(
	[Vehicle_ID] ASC,
	[InsertTime] ASC
) ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Log_Time#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Log_Time#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Log_Time#Template.IX_Log_Time#Template_InsertTime', 'IX_Log_Time#Stage_InsertTime', 'INDEX'
	EXEC sp_rename 'Log_Time#Template.CX_Log_Time#Template',            'CX_Log_Time#Stage',            'INDEX'
	EXEC sp_rename 'Log_Time#Template.PK_Log_Time#Template',            'PK_Log_Time#Stage',            'INDEX'
	EXEC sp_rename                      'Log_Time#Template',               'Log_Time#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Log_Time#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Log_Time#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Log_Time#Template.IX_Log_Time#Template_InsertTime', 'IX_Log_Time#Marks_InsertTime', 'INDEX'
	EXEC sp_rename 'Log_Time#Template.CX_Log_Time#Template',            'CX_Log_Time#Marks',            'INDEX'
	EXEC sp_rename 'Log_Time#Template.PK_Log_Time#Template',            'PK_Log_Time#Marks',            'INDEX'
	EXEC sp_rename                      'Log_Time#Template',               'Log_Time#Marks'
	DROP INDEX                    [CX_Log_Time#Marks]            ON [dbo].[Log_Time#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Log_Time#Marks]            ON [dbo].[Log_Time#Marks]
		([Vehicle_ID], [Log_Time])   WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
	CREATE     NONCLUSTERED INDEX [IX_Log_Time#Marks_InsertTime] ON [dbo].[Log_Time#Marks]
		([Vehicle_ID], [InsertTime]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Log_Time#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Log_Time#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Log_Time]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Log_Time#Template.IX_Log_Time#Template_InsertTime', 'IX_Log_Time#Partitions_InsertTime', 'INDEX'
	EXEC sp_rename 'Log_Time#Template.CX_Log_Time#Template',            'CX_Log_Time#Partitions',            'INDEX'
	EXEC sp_rename 'Log_Time#Template.PK_Log_Time#Template',            'PK_Log_Time#Partitions',            'INDEX'
	EXEC sp_rename                      'Log_Time#Template',               'Log_Time#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Log_Time#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Log_Time#Template]
END
GO 3
----------------------------------------------------------------------------------------------------------------
-- Controller_Sensor_Log
----------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Controller_Sensor_Log#Template]
(
	[Vehicle_ID] int    NOT NULL,
	[Log_Time]   int    NOT NULL,
	[Number]     int    NOT NULL,
	[Value]      bigint NOT NULL,
		CONSTRAINT [PK_Controller_Sensor_Log#Template] PRIMARY KEY CLUSTERED
		(
			[Vehicle_ID] ASC,
			[Log_Time]   ASC,
			[Number]     ASC
		) ON [LogsPartitionScheme]([Log_Time])
) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED COLUMNSTORE INDEX [CX_Controller_Sensor_Log#Template]
	ON [dbo].[Controller_Sensor_Log#Template] ([Vehicle_ID], [Log_Time], [Number])
WITH (COMPRESSION_DELAY = 60) ON [LogsPartitionScheme]([Log_Time]);
CREATE NONCLUSTERED INDEX [IX_Controller_Sensor_Log#Template_Vehicle_ID_Number_Log_Time] ON [dbo].[Controller_Sensor_Log#Template]
(
	[Vehicle_ID] ASC,
	[Number]     ASC,
	[Log_Time]   ASC
)
INCLUDE ([Value])
ON [LogsPartitionScheme]([Log_Time]);
-- Создание Stage, если его нет
IF (OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Stage]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Controller_Sensor_Log#Template.IX_Controller_Sensor_Log#Template_Vehicle_ID_Number_Log_Time', 'IX_Controller_Sensor_Log#Stage_Vehicle_ID_Number_Log_Time', 'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.CX_Controller_Sensor_Log#Template',                            'CX_Controller_Sensor_Log#Stage',                            'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.PK_Controller_Sensor_Log#Template',                            'PK_Controller_Sensor_Log#Stage',                            'INDEX'
	EXEC sp_rename                                   'Controller_Sensor_Log#Template',                               'Controller_Sensor_Log#Stage'
END
-- Создание Marks, если его нет
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Marks]', N'U') IS NULL)
BEGIN
	EXEC sp_rename 'Controller_Sensor_Log#Template.IX_Controller_Sensor_Log#Template_Vehicle_ID_Number_Log_Time', 'IX_Controller_Sensor_Log#Marks_Vehicle_ID_Number_Log_Time', 'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.CX_Controller_Sensor_Log#Template',                            'CX_Controller_Sensor_Log#Marks',                            'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.PK_Controller_Sensor_Log#Template',                            'PK_Controller_Sensor_Log#Marks',                            'INDEX'
	EXEC sp_rename                                   'Controller_Sensor_Log#Template',                               'Controller_Sensor_Log#Marks'
	DROP INDEX                    [CX_Controller_Sensor_Log#Marks]                            ON [dbo].[Controller_Sensor_Log#Marks];
	CREATE UNIQUE CLUSTERED INDEX [PK_Controller_Sensor_Log#Marks]                            ON [dbo].[Controller_Sensor_Log#Marks]
		([Vehicle_ID], [Log_Time], [Number])                   WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
	CREATE     NONCLUSTERED INDEX [IX_Controller_Sensor_Log#Marks_Vehicle_ID_Number_Log_Time] ON [dbo].[Controller_Sensor_Log#Marks]
		([Vehicle_ID], [Number], [Log_Time]) INCLUDE ([Value]) WITH (DROP_EXISTING = ON) ON [TSS_DATA_LOGS];
END
-- Создание Partitions, если таблица основная есть, но она не секционирована
IF (OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Template]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Partitions]', N'U') IS NULL
	AND NOT EXISTS(SELECT * FROM sys.partitions WHERE [object_id] = OBJECT_ID(N'[dbo].[Controller_Sensor_Log]', N'U') AND [partition_number] > 1))
BEGIN
	EXEC sp_rename 'Controller_Sensor_Log#Template.IX_Controller_Sensor_Log#Template_Vehicle_ID_Number_Log_Time', 'IX_Controller_Sensor_Log#Partitions_Vehicle_ID_Number_Log_Time', 'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.CX_Controller_Sensor_Log#Template',                            'CX_Controller_Sensor_Log#Partitions',                            'INDEX'
	EXEC sp_rename 'Controller_Sensor_Log#Template.PK_Controller_Sensor_Log#Template',                            'PK_Controller_Sensor_Log#Partitions',                            'INDEX'
	EXEC sp_rename                                   'Controller_Sensor_Log#Template',                               'Controller_Sensor_Log#Partitions'
END
-- Удаление шаблона таблицы если он есть (т.е. не был переименован в Stage или Marks или Partitions)
IF (OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Template]', N'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Controller_Sensor_Log#Template]
END
GO 3
IF (EXISTS(SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'[dbo].[Controller_Sensor_Log]', N'U') AND [name] = 'IX_VehicleSensorValue'))
BEGIN
	EXEC sp_rename 'Controller_Sensor_Log.IX_VehicleSensorValue', 'IX_Controller_Sensor_Log_Vehicle_ID_Number_Log_Time', 'INDEX';
END