﻿-------------------------------------------------
-- [dbo].[Statistic_Log#Lasts]
-------------------------------------------------
-- Очистка
TRUNCATE TABLE [dbo].[Statistic_Log#Lasts]
-- Заполнение
MERGE [dbo].[Statistic_Log#Lasts] WITH (HOLDLOCK) AS DST
USING
(
	SELECT
		L.*
	FROM [dbo].[VEHICLE] V
		CROSS APPLY
		(
			SELECT TOP(1)
				[Vehicle_ID], [Log_Time#Last] = [Log_Time], [Odometer#Last] = [Odometer], [IgnitionOnTime#Last] = [IgnitionOnTime]
			FROM [dbo].[Statistic_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = V.[VEHICLE_ID]
			ORDER BY [Log_Time] DESC
		) L
	WHERE L.[Log_Time#Last] IS NOT NULL
) AS SRC ([Vehicle_ID], [Log_Time#Last], [Odometer#Last], [IgnitionOnTime#Last])
	ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
WHEN     MATCHED AND DST.[Log_Time#Last] < SRC.[Log_Time#Last] THEN
	UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
WHEN NOT MATCHED BY TARGET THEN
	INSERT (    [Vehicle_ID],     [Log_Time#Last],     [Odometer#Last],     [IgnitionOnTime#Last])
	VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[Odometer#Last], SRC.[IgnitionOnTime#Last])
OUTPUT $action, INSERTED.*, DELETED.*
;