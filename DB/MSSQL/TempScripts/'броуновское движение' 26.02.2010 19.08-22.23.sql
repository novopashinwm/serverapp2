drop table #tt

select lt.inserttime, lt.log_time, gl.lng, gl.lat, sl.ds, gpsl.speed , lt.id log_time_id
	into #tt
from dbo.log_time lt
join dbo.geo_log gl on gl.log_time_id = lt.id
join gps_log gpsl on gpsl.log_time_id = lt.id
join sensors_log sl on sl.log_time_id = lt.id
where lt.vehicle_id = 497
and lt.log_time between datediff(ss, '1970.01.01', '2010.02.26 12:15') - 3 * 3600
                    and datediff(ss, '1970.01.01', '2010.02.27 08:27') - 3 * 3600
order by lt.log_time

begin tran
	update gpsl
		set speed = 0
--		set lat = 55.5850094,
--			lng = 37.3617621
	--select sum(dbo.GetTwoGeoPointsDistance(t.lat, t.lng, f.lat, f.lng)), avg((t.lat + f.lat) / 2), avg((t.lng + f.lng) / 2), count(1)
	from
	(
		select t.log_time t_log_time, max(f.log_time) f_log_time from #tt t
		join #tt f on f.log_time < t.log_time
		group by t.log_time
	) s
	join #tt t on t.log_time = s.t_log_time
	join #tt f on f.log_time = s.f_log_time
--	join dbo.Geo_Log gl on gl.log_time_id = t.log_time_id
	join dbo.Gps_Log gpsl on gpsl.log_time_id = t.log_time_id
	where dbo.GetTwoGeoPointsDistance(t.lat, t.lng, f.lat, f.lng) < 200
		  and (
				  t.speed < 40 
			  and f.speed < 40 
			  and (t.ds is null or t.ds&32 <> 0)
			  and (f.ds is null or f.ds&32 <> 0)
				or
				  t.speed < 5
			  and f.speed < 5 
			  and (t.ds is not null or t.ds&32 = 0)
			  and (f.ds is not null or f.ds&32 = 0)
			)
commit

select min(log_time_id) from #tt


exec dbo.AddStatisticLog 211471920

exec dbo.RecalcStatisticLog 497, 2592000, 1

select count(1) from #tt 
order by speed desc

select * from #tt

select  (dbo.GetTwoGeoPointsDistance(t.lat, t.lng, f.lat, f.lng)),
		t.speed,
		t.log_time,
		mli.reason,
		t.ds,
		t.insertTime
from (
	select t.log_time t_log_time, max(f.log_time) f_log_time from #tt t
	join #tt f on f.log_time < t.log_time
	group by t.log_time
) s
join #tt t on t.log_time = s.t_log_time
join #tt f on f.log_time = s.f_log_time
left outer join monitoree_log_ignored mli on mli.monitoree_id = 497 and mli.log_time = t.log_time 
--where t.ds is null
order by t.log_time

select * from monitoree_log_ignored where monitoree_id = 497 and log_time in (select log_time from #tt where ds is null)

select * from controller_type where controller_type_id = (select controller_type_id from controller where controller_id = 200 /* where vehicle_id = 497*/)

declare
 @uniqueID  int,
 @id    int,   -- unit no.  
 @phone   varchar(20), -- phone  
 @controller int,  -- controller id  
 @device_id varbinary(32)

set @uniqueID = 497

exec dbo.GetUnitInfo @uniqueID out, @id out, @phone out, @controller out, @device_id

select
 @uniqueID,
 @id    ,   -- unit no.  
 @phone , -- phone  
 @controller,
 @device_id 





/*���������� �� ds is null*/


select count(1) /*, c.controller_type_id, */ ds, ds&32 /*, gpsl.speed*/
from controller c
join controller_type ct on ct.controller_type_id = c.controller_type_id
join sensors_log sl (nolock) on sl.vehicle_id = c.vehicle_id
join gps_log gpsl (nolock) on gpsl.log_time_id = sl.log_time_id
where 1=1
and ct.POS_SMOOTH_SUPPORT = 1
and c.controller_type_id = 6 
and sl.log_time between datediff(ss, '1970.01.01', '2010.01.01')
   		            and datediff(ss, '1970.01.01', '2010.03.11')
--and ds is null
group by /*c.controller_type_id, */ds /*, gpsl.speed*/
order by 1 desc


select top(10000) dbo.getDateFromInt(log_time), * from sensors_log 
where vehicle_id = 522 and (v is not null or v1 is not null)
and log_time > datediff(ss, '1970.01.01', '2010.03.10')
order by log_time desc

select * from controller where vehicle_id = 519
select * from controller_type where controller_type_id = 10


select * from sys.databases


select top(100) *, dbo.getdatefromint(log_time + 3 * 3600) 
from dbo.log_time 
where vehicle_id = 453 --520  --567 --573
order by id desc


select * from vehiclegroup