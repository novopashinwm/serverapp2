select top(100)
 m.message_id
--,(select top(1) Actual_Time from H_Message hm where hm.Message_ID = m.Message_ID order by ID desc)
, dateadd(hour, 4, m.Time)
, m.body
, m.source_id
, m.destination_id
, "Contact Type" = 
	case destinationType_ID
		when 1 then 'SMS / HTTP'
		when 2 then 'SMS / SMPP'
		when 3 then 'SMS / MPX'
		when 6 then 'Email'
		else 'Unknown'
	end	
, "Contact" = 
	case destinationType_ID 
		when 1 then p.phone
		when 2 then p.phone
		when 3 then a.value
		when 6 then e.email
		else null
	end
, Confirmed = isnull(p.Confirmed, e.Confirmed)
, "Owner Login" = o.login
, Accepted  = case m.TYPE & 0x08 when 0 then 'no' else 'yes' end
, Delivered = case m.TYPE & 0x04 when 0 then 'no' else 'yes' end
, Failed    = case m.TYPE & 0x80 when 0 then 'no' else 'yes' end
, Pending   = case m.TYPE & 0x10 when 0 then 'no' else 'yes' end
, Throttled = case m.TYPE & 0x40 when 0 then 'no' else 'yes' end
, MQF       = case m.TYPE & 0x20 when 0 then 'no' else 'yes' end
, m.DestinationType_ID
, m.ErrorCount
--update m
--	set m.TYPE = m.TYPE &  (~(convert(int, 0x08)))
 from message m
 left outer join phone p on p.phone_id = m.destination_id and destinationType_ID in (1,2)
 left outer join email e on e.email_id = m.destination_id and destinationType_ID in (6)
 left outer join asid a  on a.id       = m.destination_id and destinationType_ID in (3)
 left outer join operator o on o.operator_id = 
	(case destinationType_ID 
		when 1 then p.operator_id
		when 2 then p.operator_id
		when 3 then a.operator_id
		when 6 then e.operator_id
		else null
	 end)
 --where DestinationType_ID = 2 --and destination_id = 554
 --where m.message_id > 780748
 order by time desc, message_id desc