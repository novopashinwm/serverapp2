declare @vehicle_id int = 3438
declare @log_time_from int = datediff(ss, '1970', '2014.02.25 03:35:00') - 4 * 3600
declare @log_time_to   int = datediff(ss, '1970', '2014.02.26 04:10:00') - 4 * 3600

insert monitoree_log_ignored (insertTime, reason, monitoree_id, log_time, X, Y, SPEED)
	select getutcdate(), 4, gl.vehicle_id, gl.log_time, gl.lng, gl.lat, speed.value
	from geo_log gl 
	cross apply (
		select top(1) gps.speed, distance = dbo.GetTwoGeoPointsDistance(gl.lng, gl.lat, gl1.lng, gl1.lat)
			from geo_log gl1
			outer apply (
				select top(1) speed
					from gps_log gps1
					where gps1.vehicle_id = gl1.vehicle_id
					  and gps1.log_time <= gl1.log_time
					  order by gps1.log_time desc) gps
			where gl1.vehicle_id = gl.vehicle_id and gl1.log_time between gl.log_time + 1 and gl.log_time + 600
			order by gl1.log_time desc) glp
	cross apply (
		select top(1) gps.speed, distance = dbo.GetTwoGeoPointsDistance(gl.lng, gl.lat, gl1.lng, gl1.lat)
			from geo_log gl1
			outer apply (
				select top(1) speed
					from gps_log gps1
					where gps1.vehicle_id = gl1.vehicle_id
					  and gps1.log_time <= gl1.log_time
					  order by gps1.log_time desc) gps
			where gl1.vehicle_id = gl.vehicle_id and gl1.log_time between gl.log_time - 600 and gl.log_time -1
			order by gl1.log_time asc) gln
	outer apply (
		select top(1) value = speed
			from gps_log gps1
			where gps1.vehicle_id = gl.vehicle_id
			  and gps1.log_time <= gl.log_time
			  order by gps1.log_time desc) speed
	where gl.vehicle_id = @vehicle_id
	  and gl.log_time between @log_time_from
					      and @log_time_to
	  and glp.distance < 200
	  and gln.distance < 200
	  and glp.speed < 15
	  and gln.speed < 15
	  and speed.value < 15
	  and not exists (
		select 1 
			from monitoree_log_ignored e
			where e.monitoree_id = gl.vehicle_id
			  and e.log_time = gl.log_time)

declare 
	@prevlng		real,
	@prevlat		real,
	@currLogTime int

while 1=1
begin

	set @currLogTime = null
	set @prevLat = null
	set @prevLng = null

	select top(1) 
			@currLogTime = mli.Log_Time, 
			@prevLat = glp.lat, 
			@prevLng = glp.lng
		from monitoree_log_ignored mli 
		join geo_log gl on gl.vehicle_id = mli.monitoree_id and gl.log_time = mli.log_time
		cross apply (
			select top(1) gl1.lat, gl1.lng, gps.speed, distance = dbo.GetTwoGeoPointsDistance(gl.lng, gl.lat, gl1.lng, gl1.lat)
				from geo_log gl1
				outer apply (
					select top(1) speed
						from gps_log gps1
						where gps1.vehicle_id = gl1.vehicle_id
						  and gps1.log_time <= gl1.log_time
						  order by gps1.log_time desc) gps
				where gl1.vehicle_id = gl.vehicle_id and gl1.log_time between gl.log_time + 1 and gl.log_time + 600
				order by gl1.log_time desc) glp
		cross apply (
			select top(1) gps.speed, distance = dbo.GetTwoGeoPointsDistance(gl.lng, gl.lat, gl1.lng, gl1.lat)
				from geo_log gl1
				outer apply (
					select top(1) speed
						from gps_log gps1
						where gps1.vehicle_id = gl1.vehicle_id
						  and gps1.log_time <= gl1.log_time
						  order by gps1.log_time desc) gps
				where gl1.vehicle_id = gl.vehicle_id and gl1.log_time between gl.log_time - 600 and gl.log_time -1
				order by gl1.log_time asc) gln
		  where mli.monitoree_id = @vehicle_id 
			and mli.reason = 4
			and (mli.log_time > @currLogTime or @currLogTime is null)
		  and glp.distance < 200
		  and gln.distance < 200
		  and glp.distance > 0
		  and glp.speed < 15
		  and gln.speed < 15
		  and mli.speed < 15
		  order by mli.log_time asc

	if @currLogTime is null
		break;

	print convert(varchar(32), dbo.GetDateFromInt(@currLogTime + 4*3600))

	update geo_log set lat = @prevLat, lng = @prevLng where vehicle_id = @vehicle_id and log_time = @currLogTime
	update gps_log set speed = 0 where vehicle_id = @vehicle_id and log_time = @currLogTime

end


declare @log_time int = @log_time_from
declare @max_interval int = datediff(ss, '1970', getutcdate()) - @log_time_from

exec dbo.AddStatisticLog @vehicle_id, @log_time

exec dbo.RecalcStatisticLog @vehicleID=@vehicle_id,@maxInterval=@max_interval;  