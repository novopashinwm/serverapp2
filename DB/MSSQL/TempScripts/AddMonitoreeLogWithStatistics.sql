set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go









ALTER PROCEDURE [dbo].[AddMonitoreeLog]
	@uniqueID	int,				-- vehicle pkey
	@id			int,				-- unitid
	@phone		varchar(20),		-- think
	@device_id 	varbinary(32) = null,
	@log_time	int,				-- UTC time 
	@long		real,				-- longtitude
	@lat		real,				-- latitude
	@speed		int,				-- speed
	@media		int,				-- MEDIA_ID
	@v			int	= null,			-- пиът░╖░
	@v1			int	= null,
	@v2			int	= null,
	@v3			int	= null,
	@v4			int	= null,
	@satellites int	= null,			-- SATELLITES qty
	@firmware	int	= null,			-- FIRMWARE
	@ds			int = null,			-- digital sensors
	@run		int = null,			-- пробег
	@height		int = null
AS

/*
RaisError('Test Error in AddMonitoreeLog', 16, 1)
		return -2;
*/

begin

set nocount on;

-- заплатка для записи борт. напряжения // Зирюкин //
set @v = @v / 200
-- // --
if (@v > 255) begin select @v = null; end
if (@v1 > 255) begin select @v1 = null; end
if (@v2 > 255) begin select @v2 = null; end
if (@v3 > 255) begin select @v3 = null; end
if (@v4 > 255) begin select @v4 = null; end
END

---update "-" x-y
begin
if (@lat< 0 )
set @lat= @lat * (-1)

if (@long < 0)
set @long= @long * (-1)
END
---END update "-" x-y

	
declare @controller int
	-- !! OBLIGATORY !! fill unit info
	--if @uniqueID < 1
	begin
		declare @err int
		set @err = 0
		exec @err = dbo.GetUnitInfo @uniqueID out, @id out, @phone out, @controller out, @device_id
		if @err <> 0 return @err
	end
	--
	if @uniqueID is null or @uniqueID < 1 return -1

	-- all recs newer more than hour are discarded
	if dateadd(s, @log_time - 3600, '1970') > getutcdate() return -3
	-- coord errors
	-- фъи ашжк┐ш╖и а┐ш┐д┐м┐ди к ъ┐ш╣░диvv 
	-- if @long <= 0 or @long >= 180 or @lat <= 0 or @lat >= 90 return -4

	-- get type of channel: gprs, gsm, sms
	select @media = TYPE from dbo.MEDIA where MEDIA_ID = @media 

	declare @skipPosition bit;
	set @skipPosition = 0;
	
	--Если скорость для перемещения от предыдущей точки > 800км/ч, то отбраковываем позицию
	--Предыдущая позиция
	declare @prevLogTime int, @prevLng real, @prevLat real;
	select @prevLogTime = max(log_time), @prevLng = max(X), @prevLat = max(Y)
	from dbo.MONITOREE_LOG ml(nolock) 
	where MONITOREE_ID = @uniqueID and log_time = (select max(log_time) 
													from dbo.MONITOREE_LOG ml2 (nolock) 
													where MONITOREE_ID = @uniqueID
													and log_time < @log_time)
	if (@prevLogTime is not null and @prevLng is not null and @prevLat is not null)
	begin
		declare @prevDistance float(53);
		select @prevDistance = dbo.GetTwoGeoPointsDistance(@prevLng, @prevLat, @long, @lat);
		if ((@prevDistance/1000) / (cast(@log_time - @prevLogTime as float)/3600) > 800)
		begin
			set @skipPosition = 1;
			--сохраняем в таблицу отбракованных или измененных записей
			if exists(select * from dbo.MONITOREE_LOG_IGNORED (nolock) 
					where MONITOREE_ID = @uniqueID and LOG_TIME= @log_time)
			begin
				delete dbo.MONITOREE_LOG_IGNORED
				where MONITOREE_ID = @uniqueID and LOG_TIME= @log_time
			end

			insert dbo.MONITOREE_LOG_IGNORED(MONITOREE_ID, LOG_TIME, X, Y, SPEED, MEDIA, V, V1, V2, V3, V4, 
			SATELLITES, FIRMWARE, ODOMETER, HEIGHT) 
			values(@uniqueID, @log_time, @long, @lat, @speed, @media, @v, @v1, @v2, @v3, @v4,
			@satellites, @firmware, @run, @height)		
		end
	end

	if (@skipPosition = 0)
	begin
		/*
			Сглаживание скачков координат при стоянке.
			. Только для тех у кого скорость < 10
			. Определяем относится ли тип контроллера к тем, для которых указана необходимость этого сглаживания
			. Если зажигание выключено и скорость = 0, то радиус округления = 300м,
			  Если зажигание выключено и скорость > 0 и < 10, то радиус округления = 200м,
			  если зажигание включено и скорость = 0, то радиус округления = 100м,
			  если зажигание включено и скорость > 0 и < 5, то радиус округления = 70м
		*/

		declare @stopSpeed int, @stopSpeedNoIgnition int;
		set @stopSpeed = 5;
		set @stopSpeedNoIgnition = 10;
		--Если скорость меньше 5 км.ч или нет зажигания и скорость < 10 км.ч
		if (@speed < @stopSpeed or (@speed < @stopSpeedNoIgnition and @ds is not null and @ds&32 = 0))
		begin
			if exists (select *
				from CONTROLLER c (nolock) 
				join CONTROLLER_TYPE ct (nolock) on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				where CONTROLLER_ID = @controller and ct.POS_SMOOTH_SUPPORT = 1)
			begin
				declare @prev_log_time	int, @prev_long	real, @prev_lat	real,@prev_speed int;
				select @prev_long = X, @prev_lat = Y, 
						@prev_speed = SPEED, @prev_log_time = LOG_TIME
				from dbo.MONITOREE_LOG (nolock) 
				where MONITOREE_ID = @uniqueID 
				and LOG_TIME = (select max(LOG_TIME) 
								from dbo.MONITOREE_LOG (nolock) 
								where MONITOREE_ID = @uniqueID
								and LOG_TIME < @log_time);

				if (@prev_log_time is not null and @prev_speed < @stopSpeedNoIgnition)
				begin
					declare @smooth_radius int;
					--если зажигание включено mu.DigitalSensors |= 0x20;
					if (@ds is not null and @ds&32 > 0)
					begin 
						if (@speed = 0 and @prev_speed = 0)
							set @smooth_radius = 100;
						else
							set @smooth_radius = 50;
					end
					else --если зажигание выключено
					begin 
						if (@speed = 0 and @prev_speed = 0)
							set @smooth_radius = 300;
						else
							set @smooth_radius = 200;
					end

					--считаем расстояние между новой и предыдущей точками
					declare @dist real;
					select @dist = dbo.GetTwoGeoPointsDistance(@prev_long, @prev_lat, @long, @lat);
					if (@dist < @smooth_radius)
					begin
						--сохраняем в таблицу отбракованных или измененных записей
						if exists(select * from dbo.MONITOREE_LOG_IGNORED (nolock) 
								where MONITOREE_ID = @uniqueID and LOG_TIME= @log_time)
						begin
							delete dbo.MONITOREE_LOG_IGNORED
							where MONITOREE_ID = @uniqueID and LOG_TIME= @log_time
						end

						insert dbo.MONITOREE_LOG_IGNORED(MONITOREE_ID, LOG_TIME, X, Y, SPEED, MEDIA, V, V1, V2, V3, V4, 
						SATELLITES, FIRMWARE, ODOMETER, HEIGHT) 
						values(@uniqueID, @log_time, @long, @lat, @speed, @media, @v, @v1, @v2, @v3, @v4,
						@satellites, @firmware, @run, @height)					

						select @long = @prev_long, @lat = @prev_lat;
						--если время меньше сглаживаемыми точками меньше 10 минут, то не пишем новую позицию вообще
						if (@log_time - @prev_log_time < 600 
							and (@v is null or @v = 2147483647)
							and (@v1 is null or @v1 = 2147483647)
							and (@v2 is null or @v2 = 2147483647)
							and (@v3 is null or @v3 = 2147483647)
							and (@v4 is null or @v4 = 2147483647)
							)
								set @skipPosition = 1;

					end
				end
			end
		end
	end


	if (@skipPosition = 0)
	begin
		if not exists(	-- add rec
			select 0 from dbo.MONITOREE_LOG (nolock) 
			where MONITOREE_ID = @uniqueID and LOG_TIME = @log_time and 
				isnull(MEDIA, 0) = isnull(@media, 0))
			begin
				declare @time datetime
				set @time = dateadd(s, @log_time, '1970')
				exec dbo.ProcessControllerInfo @controller, @speed out, @v out, @v1 out, 
					@v2 out, @v3 out, @v4 out, @satellites out, @firmware out, @time, @ds

				insert dbo.MONITOREE_LOG(MONITOREE_ID, LOG_TIME, X, Y, SPEED, MEDIA, V, V1, V2, V3, V4, 
					SATELLITES, FIRMWARE, ODOMETER, HEIGHT) 
				values(@uniqueID, @log_time, @long, @lat, @speed, @media, @v, @v1, @v2, @v3, @v4,
					@satellites, @firmware, @run, @height)
			end
		else	-- update existing
			update MONITOREE_LOG 
			set X = @long, Y = @lat, SPEED = @speed, ODOMETER = @run,
				V = 
				case isnull(V, 0)
					when 0 then @v
					else V
				end, 
				V1 = 
				case isnull(V1, 0)
					when 0 then @v1
					else V1
				end, 
				V2 = 
				case isnull(V2, 0)
					when 0 then @v2
					else V2
				end, 
				V3 = 
				case isnull(V3, 0)
					when 0 then @v3
					else V3
				end, 
				V4 = 
				case isnull(V4, 0)
					when 0 then @v4
					else V4
				end, 
				SATELLITES = 
				case isnull(SATELLITES, 0)
					when 0 then @satellites
					else SATELLITES
				end, 
				FIRMWARE = 
				case isnull(@firmware, 0)
					when 0 then FIRMWARE
					else @firmware
				end,
				HEIGHT = 
				case isnull(@height, 0)
					when 0 then HEIGHT
					else @height
				end
			where MONITOREE_ID = @uniqueID and LOG_TIME = @log_time and 
				isnull(MEDIA, 0) = isnull(@media, 0)

		if @@rowcount <> 1 or @@error <> 0
		begin
			RaisError('Unable add record to MONITOREE_LOG', 16, 1)
			return -2
		end

		--Считаем статистику
		exec dbo.AddStatisticLog @uniqueID, @log_time, @long, @lat, @height, 0;
	end

/*
	update dbo.SCHEDULE_PASSAGE set STATUS = 1
	from dbo.WAYBILL_HEADER wh, dbo.SCHEDULE_POINT sp, dbo.ROUTE_POINT rp, dbo.POINT p 
	where wh.VEHICLE_ID = @uniqueID 
		and dbo.SCHEDULE_PASSAGE.WH_ID = wh.WAYBILL_HEADER_ID
		and dbo.SCHEDULE_PASSAGE.SP_ID = sp.SCHEDULE_POINT_ID 
		and sp.ROUTE_POINT_ID = rp.ROUTE_POINT_ID 
		and rp.POINT_ID = p.POINT_ID 
		and dateadd(s, @log_time, '1970') > 
			isnull(dbo.SCHEDULE_PASSAGE.TIME_OUT, dbo.SCHEDULE_PASSAGE.TIME_IN) 
		and dbo.SCHEDULE_PASSAGE.STATUS & 3 = 2
		and dbo.GET_DISTANCE(@long, @lat, X, Y) > RADIUS*/





