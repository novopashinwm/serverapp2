insert into statistic_log_recalc (vehicle_id, log_time, pending)
		select v.vehicle_id, (select min(log_time) from log_time lt (nolock) where lt.vehicle_id = v.vehicle_id) log_time, 0
			from dbo.vehicle v 			
			where not exists (select 1 from statistic_log_recalc slr where v.vehicle_id = slr.vehicle_id)
			group by v.vehicle_id
