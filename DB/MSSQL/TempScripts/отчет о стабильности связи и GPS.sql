--��� ������� ����������, ������� ���� �� ���� ������ � ����������, � ������ �� ����� ������ ��� ������ ��������
declare @from int = datediff(ss, '1970', '2012-08-10 00:00')-4*3600
declare @to   int = datediff(ss, '1970', '2012-08-10 09:00')-4*3600

select *
	into #t
	from (
		select 
		  v.Vehicle_ID
		, Log_Time_From = 
				(select top(1) lte.Log_Time
					from dbo.Log_Time lte (nolock)
					where lte.Vehicle_ID = v.Vehicle_ID
					  and lte.Log_Time >= @from
					order by lte.Log_Time asc)
		, Log_Time_To = 
				(select top(1) lte.Log_Time
					from dbo.Log_Time lte (nolock)
					where lte.Vehicle_ID = v.Vehicle_ID
					  and lte.Log_Time <= @to
					order by lte.Log_Time desc)
			from dbo.Vehicle v 
			join controller c on c.vehicle_id = v.vehicle_id 		
			join controller_info ci on ci.controller_id = c.controller_id
			where convert(varchar(32), ci.device_id) like '20100%' and c.controller_type_id = 23
	) t
	where t.Log_Time_From is not null 
	  and t.Log_Time_To is not null
	  and t.Log_Time_From <> t.Log_Time_To

--������� ��������� ������ ����� 30 �����
select
  t.Vehicle_ID
, Log_Time_From = lt.Log_Time
, Log_Time_To = (select top(1) ltn.Log_Time 
					from dbo.Log_Time ltn (nolock) 
					where ltn.Vehicle_ID = lt.Vehicle_ID 
					  and ltn.Log_Time > lt.Log_Time 
					order by ltn.Log_Time asc)
into #anything_absent
from #t t
join dbo.Log_Time lt (nolock) on lt.Vehicle_ID = t.Vehicle_ID and lt.Log_Time between t.Log_Time_From and t.Log_Time_To
where 
	not exists (
		select 1 from dbo.Log_Time ltn (nolock) 
			where ltn.Vehicle_ID = lt.Vehicle_ID 
			  and ltn.Log_Time > lt.Log_Time
			  and ltn.Log_Time < lt.Log_Time + 30*60)

create clustered index TMP_IX_Anything_Absent on #anything_absent (Vehicle_ID, Log_Time_From)

--������� ��������� GPS ����� 10 �����
select
  t.Vehicle_ID
, Log_Time_From = lt.Log_Time
, Log_Time_To = (select top(1) ltn.Log_Time 
					from dbo.GPS_Log ltn (nolock) 
					where ltn.Vehicle_ID = lt.Vehicle_ID 
					  and ltn.Log_Time > lt.Log_Time 
					order by ltn.Log_Time asc)
into #gps_absent
from #t t
join dbo.GPS_Log lt (nolock) on lt.Vehicle_ID = t.Vehicle_ID and lt.Log_Time between t.Log_Time_From and t.Log_Time_To
where 
	not exists (
		select 1 from #anything_absent aa
			where aa.Vehicle_ID = lt.Vehicle_ID
			  and aa.Log_Time_From = lt.Log_Time)
	and
	not exists (
		select 1 from dbo.GPS_Log ltn (nolock) 
			where ltn.Vehicle_ID = lt.Vehicle_ID 
			  and ltn.Log_Time > lt.Log_Time
			  and ltn.Log_Time < lt.Log_Time + 30*60)

create clustered index TMP_IX_GPS_Absent on #gps_absent (Vehicle_ID, Log_Time_From)

select
      d.name 
	, v.garage_number 
	, Date_From = dbo.GetDateFromInt(t.Log_Time_From + 4*3600)
	, Date_To   = dbo.GetDateFromInt(t.Log_Time_To + 4*3600)
	, RunKMs    = dbo.CalculateDistanceFN(t.Log_Time_From, t.Log_Time_To, t.Vehicle_ID) / 1000
	, CAN_Count = (select count(1) 
						from dbo.CAN_Info ci (nolock) 
						where ci.Vehicle_ID = t.Vehicle_ID 
						  and ci.Log_Time between t.Log_Time_From and t.Log_Time_To)
	, V1_Count  = (select count(1) 
						from dbo.Sensors_Log sl (nolock) 
						where sl.Vehicle_ID = t.Vehicle_ID 
						  and sl.Log_Time between t.Log_Time_From and t.Log_Time_To
						  and sl.V1 is not null and sl.V1 not in (0,1))
	, V4_Count  = (select count(1) 
						from dbo.Sensors_Log sl (nolock) 
						where sl.Vehicle_ID = t.Vehicle_ID 
						  and sl.Log_Time between t.Log_Time_From and t.Log_Time_To
						  and sl.V4 is not null and sl.V4 not in (0,1))
	into #r
	from #t t
	join vehicle v on v.vehicle_id = t.vehicle_id	
	join department d on d.department_id = v.department
	where t.vehicle_id not in (select aa.Vehicle_ID from #Anything_Absent aa)
	  and t.vehicle_id not in (select ga.Vehicle_ID from #GPS_Absent ga)
	
select * from #r 
where datediff(dd, Date_From, Date_To) > 0
and RunKMs / datediff(dd, Date_From, Date_To) > 50
order by V4_Count desc

drop table #r
drop table #gps_absent
drop table #anything_absent
drop table #t