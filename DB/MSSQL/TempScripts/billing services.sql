select a.Contract_Number
, a.SimID
, a.Terminal_Device_Number
, a.Value
, [Billing_Service.ID] = bs.id
, bs.Name
, bs.StartDate
, bs.MaxQuantity
, bs.Next_Reset_Time_Of_Counter
, v.GARAGE_NUMBER
, v.Device_ID
, v.phone
, bsp.Name
, o.login
, o.password
	from Asid a
	outer apply (
		select bs.ID, bst.Name, bs.StartDate, bs.MaxQuantity, Next_Reset_Time_Of_Counter = sq.Nearest_Time
			from Billing_Service bs 
			join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
			left outer join Billing_Service_SchedulerQueue bssq on bssq.billing_service_Id = bs.ID
			left outer join SchedulerQueue sq on sq.SchedulerQueue_ID = bssq.SchedulerQueue_ID			
			where bs.Asid_ID = a.ID) bs
	outer apply (
		select v.GARAGE_NUMBER, Device_ID = CONVERT(varchar, ci.DEVICE_ID), c.PHONE
			from MLP_Controller mlpc
			join CONTROLLER c on c.CONTROLLER_ID = mlpc.Controller_ID
			join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
			left outer join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
			where mlpc.Asid_ID = a.ID) v
	left outer join Billing_Service_Provider bsp on bsp.Billing_Service_Provider_ID = a.Billing_Service_Provider_ID
	left outer join operator o on o.operator_id = a.operator_id
	where 1=1
	--and bs.Name = '����-�������'
	--a.Value in (
	--'p4sakhf87a2d41fa51d42d3f17077f15d58a3c', 
	--'p3khvcca0c15a78b448b087f7a33fc446d80a', 
	--'p4sakh477c76b9916604134e7ac9bdea0b8d5d', 
	--'p4sakh64fa97cc22a13b5319e05fb50074c6ec')
	and a.Contract_Number = '165301151541'
	--and a.Contract_Number = '177373157136'
	--and bs.name = '���� LBS ������'
	--and bs.RenderedQuantity >= MaxQuantity
	--and v.GARAGE_NUMBER = '_���'
	