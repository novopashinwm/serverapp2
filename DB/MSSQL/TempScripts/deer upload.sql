select c.Vehicle_ID 
, Log_Time = datediff(ss, '1970', l.Time)
, l.Lng, l.Lat, Course, Speed
	into #Deer_Collar_Log
	from (
		select 
		  Phone = '+' + substring(Device, 2, len(Device)-1)
		, Time  = convert(datetime,
					substring(Timestamp, 7, 4) + '.' + --year
					  substring(TimeStamp, 4, 2) + '.' + --month
					  substring(TimeStamp, 1, 2) + ' ' + --day
					  substring(Timestamp,12, len(Timestamp) - 11)--time of day
				)
		, Lat = convert(numeric(8,5), Lat)
		, Lng = convert(numeric(8,5), Lon)
		, Course = convert(int, Heading)
		, Speed  = convert(int, substring(Speed, 1, len(Speed)-len(' km/h')))
			from [dogps_79116804906_2012-10-25(1)]
	) l
	join Controller c on c.Phone = l.Phone


insert into Geo_Log (Vehicle_ID, Log_Time, Lng, Lat)
	select l.Vehicle_ID, l.Log_Time, l.Lng, l.Lat
		from #Deer_Collar_Log l

insert into Gps_Log (Vehicle_ID, Log_Time, Satellites, Speed, Course)
	select l.Vehicle_ID, l.Log_Time, 4, l.Speed, l.Course * 256 / 360
		from #Deer_Collar_Log l

insert into Log_Time (Vehicle_ID, Log_Time, Media, InsertTime)
	select c.Vehicle_ID, l.Log_Time, m.Media_ID, getutcdate()
		from #Deer_Collar_Log l
		join Media m on m.Name = 'SMS'

--TODO: пересчитать пробеги
declare @vehicle_id int = (select distinct Vehicle_ID from #Deer_Collar_Log)
declare @min_log_time int = (select min(Log_Time) from log_time where vehicle_id = @vehicle_id)
select @vehicle_id, @min_log_time
exec dbo.AddStatisticLog @vehicle_id, @min_log_time

declare @max_interval int = datediff(ss, '1970', getutcdate()) - @min_log_time + 3600

--delete from statistic_log_recalc where vehicle_id = 3708
--delete from statistic_log where vehicle_id = 3708

exec dbo.RecalcStatisticLog @vehicle_id, @max_interval, 1
select * from statistic_log where vehicle_id = 3708 and log_time = 1351127760
drop table #Deer_Collar_Log

select 1349254789 - 1351127760