declare @vehicle_id int = 84192

delete from VEHICLE_PROFILE where vehicle_id = @vehicle_id 

delete from OPERATORGROUP_VEHICLE where vehicle_id = @vehicle_id 

delete from dbo.VEHICLEGROUP_VEHICLE where vehicle_id = @vehicle_id 

delete from dbo.VEHICLE_PICTURE where vehicle_id = @vehicle_id 

delete from ZONE_VEHICLE where vehicle_id = @vehicle_id 

delete from VEHICLE_RULE where vehicle_id = @vehicle_id 

delete from MLP_Controller where Controller_ID in (select Controller_ID from CONTROLLER where vehicle_id = @vehicle_id )

delete csm
	from CONTROLLER_SENSOR_MAP csm
	join COntroller c on c.controller_id = csm.controller_id
	where c.vehicle_id = @vehicle_id 
	
delete from CONTROLLER where vehicle_id = @vehicle_id 

delete from BRIGADE_DRIVER where  BRIGADE in (select BRIGADE_ID from BRIGADE where vehicle = @vehicle_id )

delete from dbo.BRIGADE where vehicle = @vehicle_id 

delete from OPERATOR_VEHICLE where vehicle_id = @vehicle_id 

delete from dbo.VEHICLE_OWNER where vehicle_id = @vehicle_id 

delete from WB_TRIP where  WAYBILL_HEADER_ID in (select WAYBILL_HEADER_ID from dbo.WAYBILL_HEADER where vehicle_id = @vehicle_id )

delete from dbo.WAYBILL_HEADER where vehicle_id = @vehicle_id 

delete from VEHICLE where vehicle_id = @vehicle_id 



