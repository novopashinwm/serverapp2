restore database MED_TSS_SMALL
from disk = 'd:\Work\Data\Backup\MED_TSS_SMALL.bak'
--WITH REPLACE 

with 
	move 'TSS_DATA'		to	'd:\Work\Data\Nika\MED_TSS_SMALL.mdf',
	move 'TSS_DATA_2'	to	'd:\Work\Data\Nika\MED_TSS_SMALL_2.ndf', 
	move 'TSS_Log'		to	'd:\Work\Data\Nika\MED_TSS_SMALL_Log.ldf'
	
--sp_detach_db 'med_tss_small'

--sp_who2 'active'

--kill 54   