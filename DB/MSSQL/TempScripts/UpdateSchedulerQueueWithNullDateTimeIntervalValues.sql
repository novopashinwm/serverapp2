declare @schedulerqueue_id int = 12653
declare @templateFrom nvarchar(max)
declare @templateTo nvarchar(max)

select
	@templateFrom = firstPartDateFrom + gimmeRealDateFromValue + secondPartDateFrom,
	@templateTo = firstPartDateTo + gimmeRealDateToValue + secondPartDateTo
from 
(
select
	-- Real Report DateFrom
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'<ReportParameters_x002B_dtDateFrom>',
			cast(CONFIG_XML as nvarchar(max))) + 35,
		charindex(
			'</ReportParameters_x002B_dtDateFrom>',
			cast(CONFIG_XML as nvarchar(max))) - (charindex('<ReportParameters_x002B_dtDateFrom>',cast(CONFIG_XML as nvarchar(max))) + 35)
		) as gimmeRealDateFromValue,
	
	-- Real Report DateTo
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'<ReportParameters_x002B_dtDateTo>',
			cast(CONFIG_XML as nvarchar(max))) + 33,
		charindex(
			'</ReportParameters_x002B_dtDateTo>',
			cast(CONFIG_XML as nvarchar(max))) - (charindex('<ReportParameters_x002B_dtDateTo>',cast(CONFIG_XML as nvarchar(max))) + 33)
		) as gimmeRealDateToValue,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		0,
		charindex(
			'<_x003C_DateFrom_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))) + 39
		) as firstPartDateFrom,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'</_x003C_DateFrom_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))),
		10000000000000
		) as secondPartDateFrom,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		0,
		charindex(
			'<_x003C_DateTo_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))) + 37
		) as firstPartDateTo,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'</_x003C_DateTo_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))),
		10000000000000
		) as secondPartDateTo,
	-- ***** summary ****
	CONFIG_XML
	from schedulerqueue
where
	schedulerqueue_id = @schedulerqueue_id
	) t

--select @templateFrom, @templateTo

update schedulerqueue
set config_xml = @templateFrom
where schedulerqueue_id = @schedulerqueue_id

select
	@templateFrom = firstPartDateFrom + gimmeRealDateFromValue + secondPartDateFrom,
	@templateTo = firstPartDateTo + gimmeRealDateToValue + secondPartDateTo
from 
(
select
	-- Real Report DateFrom
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'<ReportParameters_x002B_dtDateFrom>',
			cast(CONFIG_XML as nvarchar(max))) + 35,
		charindex(
			'</ReportParameters_x002B_dtDateFrom>',
			cast(CONFIG_XML as nvarchar(max))) - (charindex('<ReportParameters_x002B_dtDateFrom>',cast(CONFIG_XML as nvarchar(max))) + 35)
		) as gimmeRealDateFromValue,
	
	-- Real Report DateTo
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'<ReportParameters_x002B_dtDateTo>',
			cast(CONFIG_XML as nvarchar(max))) + 33,
		charindex(
			'</ReportParameters_x002B_dtDateTo>',
			cast(CONFIG_XML as nvarchar(max))) - (charindex('<ReportParameters_x002B_dtDateTo>',cast(CONFIG_XML as nvarchar(max))) + 33)
		) as gimmeRealDateToValue,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		0,
		charindex(
			'<_x003C_DateFrom_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))) + 39
		) as firstPartDateFrom,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'</_x003C_DateFrom_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))),
		10000000000000
		) as secondPartDateFrom,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		0,
		charindex(
			'<_x003C_DateTo_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))) + 37
		) as firstPartDateTo,
	substring(
		cast(CONFIG_XML as nvarchar(max)),
		charindex(
			'</_x003C_DateTo_x003E_k__BackingField>',
			cast(CONFIG_XML as nvarchar(max))),
		10000000000000
		) as secondPartDateTo,
	-- ***** summary ****
	CONFIG_XML
	from schedulerqueue
where
	schedulerqueue_id = @schedulerqueue_id
	) t

update schedulerqueue
set config_xml = @templateTo
where schedulerqueue_id = @schedulerqueue_id



select * from schedulerqueue where schedulerqueue_id = 12653

select schedulerqueue_id, errorCount, datalength(config_xml) from schedulerqueue 
where config_xml like '%datetimeinterval%'
and enabled = 1
order by datalength(config_xml) asc

