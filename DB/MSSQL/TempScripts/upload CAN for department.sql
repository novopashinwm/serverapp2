insert into controller_sensor_map (controller_id, controller_sensor_legend_id, controller_sensor_id, multiplier, constant)
	select c.controller_id, cs.Default_Sensor_Legend_ID, cs.Controller_Sensor_ID, Default_Multiplier, Default_Constant
	from department d
	join vehicle v on v.department = d.department_id
	join controller c on c.vehicle_id = v.vehicle_id
	join controller_type ct on ct.controller_type_id = c.controller_type_id
	join controller_sensor cs on cs.controller_type_id = ct.controller_type_id
	where d.name like '%�������%'
	  and ct.type_name = 'AvtoGrafCan'
	  and not exists (
		select * from controller_sensor_map e 
			where e.controller_id = c.controller_id 
			  and e.controller_sensor_id = cs.controller_sensor_id)
	  and cs.mandatory = 1
	  and cs.descript like 'can%'
	  
