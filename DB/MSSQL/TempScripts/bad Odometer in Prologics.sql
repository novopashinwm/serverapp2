declare @from int
declare @to   int

set @from = datediff(ss, '1970', '2014-07-01')
set @to   = datediff(ss, '1970', '2014-07-30')

select v.vehicle_id
, Log_Time = (select top(1) can.Log_Time
	from Can_Info can (nolock)
	where can.Vehicle_ID = v.Vehicle_ID
	  and can.Log_Time between @from and @to
	  and can.Total_Run is not null
	order by can.Log_Time desc)
, Total_Run = (select top(1) can.Total_Run
	from Can_Info can (nolock)
	where can.Vehicle_ID = v.Vehicle_ID
	  and can.Log_Time between @from and @to
	  and can.Total_Run is not null
	order by can.Log_Time desc)
into #t	
from vehicle v
join department d on d.department_id = v.department
where d.name = 'Пролоджикс'

select t.Vehicle_ID
into #t1
from #t t
join Can_Info can (nolock) on 
	can.Vehicle_ID = t.Vehicle_ID 
and can.Log_Time between t.Log_Time - 24*3600 
and t.Log_Time - 1 and can.Total_Run > t.Total_Run
group by t.Vehicle_ID
having count(1) > 1

select * from #t

begin tran
update ci
set Firmware = convert(varbinary(32), '9.52')
from controller c 
join controller_info ci on ci.controller_id = c.controller_id
where c.vehicle_id in (select t.vehicle_id from #t1 t)

commit

drop table #t
drop table #t1