declare @serverName nvarchar(255) = 'ЕГТС Тестовый сервер'
declare @serverUri varchar(1024) = 'egts://213.221.0.198:7023'
declare @vehicleId int = 821

declare @rtsId int = (select RemoteTerminalServer_ID from RemoteTerminalServer where Name = @serverName)
if @rtsId is null
begin
	insert into RemoteTerminalServer(Name, Url) values (@serverName, @serverUri)
	set @rtsId = @@IDENTITY
end

if not exists (select * from Vehicle_RemoteTerminalServer where Vehicle_ID = @vehicleId and RemoteTerminalServer_ID = @rtsId)
	insert into Vehicle_RemoteTerminalServer(RemoteTerminalServer_ID, Vehicle_ID)
	values (@rtsId, @vehicleId)
go
