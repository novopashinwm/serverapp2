--set nocount on

create table #can_i(i int identity(1,1) primary key clustered, vehicle_id int, log_time int, Odometer int)

insert into #can_i(vehicle_id, log_time, odometer)
select ci.Vehicle_ID, ci.Log_Time, ci.Total_Run
from dbo.CAN_Info ci (nolock)
	where ci.Vehicle_ID in (select v.Vehicle_ID from Vehicle v 
								join department d on d.department_id = v.department
								where d.name = '������� ���������'
								and v.Vehicle_ID not in (2724, 2725))
		  
		--(select vehicle_id 
		--	from vehiclegroup_vehicle vgv
		--	join vehiclegroup vg on vg.vehiclegroup_id = vgv.vehiclegroup_id
		--	where vg.name like '������� �������� ���')
	and  Log_Time between DATEDIFF(ss, '1970', '2013-01-01 00:00') - 4*3600
					  and DATEDIFF(ss, '1970', '2013-03-31 23:59') - 4*3600
order by vehicle_id, log_time

select 
[��������]= convert(nvarchar(30), 
	(select garage_number from vehicle v where v.vehicle_id = c.vehicle_id)),
[��������, ��] = 
	c.Odometer-p.Odometer,
[���� �] =
	dbo.GetDateFromInt(p.Log_Time + 4*3600),
[���� ��] = dbo.GetDateFromInt(c.Log_Time + 4*3600)
from #can_i c
join #can_i p on p.i = c.i-1 and p.vehicle_id = c.vehicle_id
where c.Odometer-p.Odometer > 10 
	and (select top(1) Speed
		from gps_log gpsl (nolock)
		where gpsl.vehicle_id = c.vehicle_id and gpsl.log_time <= c.log_time
		order by gpsl.log_time desc) = 0
order by 1, 3

select 
[�������� �������] = convert(nvarchar(30), 
	(select garage_number from vehicle v where v.vehicle_id = c.vehicle_id)),
[���������� ��������, ��] = count(c.Odometer-p.Odometer),
[����� ��������, ��] = sum(c.Odometer-p.Odometer)
from #can_i c
join #can_i p on p.i = c.i-1 and p.vehicle_id = c.vehicle_id
where c.Odometer-p.Odometer > 10
	and (select top(1) Speed
		from gps_log gpsl (nolock)
		where gpsl.vehicle_id = c.vehicle_id and gpsl.log_time <= c.log_time
		order by gpsl.log_time desc) = 0
group by c.vehicle_id, p.vehicle_id
order by 1 desc

drop table #can_i