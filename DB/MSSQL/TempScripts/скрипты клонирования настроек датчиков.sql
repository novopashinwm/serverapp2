declare @targetName nvarchar(255) = 'Teltonika FM1110'
declare @sourceName nvarchar(255) = 'Teltonika FM1100'
select 'exec AddOrUpdateControllerSensor ''' + @targetName + ''', ' + convert(varchar, cs.InputNumber) + ', ''' + cs.InputName + ''', ' + convert(varchar, cs.IsDigital) + ', ' + isnull('''' + cs.DefaultLegend + '''', 'NULL')
	from v_controller_sensor cs
	where ControllerTypeName like @sourceName
	order by ControllerTypeName, InputName

