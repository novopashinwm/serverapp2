----------------------------------------------------
create table #v( v_id int primary key, d_id int )
create table #d( d_id int )
create table #o( d_id int , o_id int)
create table #ovr( operator_id int, vehicle_id int, right_id int )
----------------------------------------------------
create clustered index idx#ovr on #ovr(vehicle_id)

--create clustered index idx#v on #v(v_id)

declare @splittingDepartmentId int = (select DEPARTMENT_id from DEPARTMENT where NAME = 'Sitronics India Cluster 1')
declare @admin_right_id		int = 2
declare @edit_right_id		int = 7
declare @access_right_id	int = @edit_right_id

	
insert into #ovr
select ovr.operator_id, ovr.vehicle_id, ovr.right_id
from v_operator_vehicle_right ovr
inner join vehicle v on v.vehicle_id = ovr.vehicle_id
where (ovr.right_id = @access_right_id or ovr.right_id = @admin_right_id)
and department = @splittingDepartmentId

DECLARE @d_id int 

while (1=1)
begin
	set @d_id = isnull((select max(d_id) from #v), 0) +1
	print '@d_id = ' + convert(varchar, @d_id)
				
	insert into #v
	select 
		top (1) 
		v.vehicle_id, @d_id 
		from vehicle v 
		where v.department = @splittingDepartmentId 
		--Объект наблюдения не сопоставлен ещё ни с одним клиентом
		and v.vehicle_id not in ( select #v.v_id
								  from #v)

	if @@rowcount = 0
		break;								  

	insert into #d
		select @d_id
	
	while 1=1
	begin
				
			--заполнение тадлицы #o
			insert into #o (d_id, o_id) select @d_id, operator_id
			from operator o
	      where 
			  --выбор оператора, имеющего доступ к машинам из текущего клиента 
			  exists ( select 1 
						  from #ovr 
						  where 
			 				  o.operator_id = #ovr.operator_id 
							  and right_id = @access_right_id 
							  and #ovr.vehicle_id in ( select v_id from #v where #v.d_id = @d_id ) )
			 -- не является администратором
			  and not exists (select *
								from #ovr 								
								where o.operator_id = #ovr.operator_id and #ovr.right_id = 2)
			 -- исключаем заведомо неподходящие логины.
			 and o.login not in ( 
				'Android1000', 'Android10', 'sitronicsdemo','', 
				'camera','sts\aosipov','sts\mtolkachev',
				'guest','Misha','mmg', 'india','mt3','klop','demo','evgeny','keshav', 'ds7', 'sitronics-kasu',
				'DUMP', 'CARZONRENT',
				'KMTH'
				)
			 -- не существует ни в одном клиенте
			 and  not exists (select *
								from #o
								where o.operator_id = #o.o_id)
			 
			-- по рез-там заполнения табл. #O, прерывается цикл работы с текущим клиентом
			if @@rowcount = 0
				break; --внутренний цикл
			 				  
			-- заполнение табл. #v
			insert into #v (v_id, d_id) select v.vehicle_id, @d_id
											from vehicle v
											where 
												v.GARAGE_NUMBER not in (
													'HR 26BM 0751', 'HR 55 HT 7800', 'HR 55 PT 6923', 'DL1 PC 5664', 'HR 55 RT 3341', 'HR 55 RT 3341'
													, 'DL 1 ZZ 2043' --[17:47:07] PASHUPATI JHA: ok this id belongsto dump id
												)
											-- машины еще не добавлялись в таблицу #v
												and not exists ( select *
																from #v
																where #v.v_id = v.vehicle_id )
											-- есть доступ пользователей, которые принадлежат данному клиенту.
												and exists ( select *
																from #ovr
																where v.vehicle_id = #ovr.vehicle_id 
																and #ovr.operator_id in (select o_id
																							from #o
																							where #o.d_id = @d_id))
			-- при окончании выборки, работа с текущим клиентом заканчивается.
			
				if @@rowcount = 0
						break; --внутренний цикл
					 
					
		end--внутренний цикл
end--внешний цикл	
			
---------------------------------------------------
select #d.d_id
,  'Number of cars' =
	( select count ( #v.v_id) 
	  from #v 
	  Where #v.d_id = #d.d_id )
,	'Users' =
	(select ( convert (nvarchar(max), 
				(
					 select name + ' | '  
					 from operator o
					 Where o.operator_id in (select o_id
												 from #o
												 where #o.d_id =  #d.d_id
											 )
					 for xml path(''))))
	)
, 'Logins' =
	(select ( convert (nvarchar(max), 
				(
					 select '''' + o.LOGIN + ''','  
					 from operator o
					 Where o.operator_id in (select o_id
												 from #o
												 where #o.d_id =  #d.d_id
											 )
					 for xml path(''))))
	)
			
, 'Cars' = 
	( select (  convert(nvarchar(max), 
		(select ve.garage_number + ' ( ' + convert( varchar(max), cont_i.device_id) + ' ) ' + ' | '
		 from #v
		inner join vehicle ve on #v.v_id = ve.vehicle_id
		inner join controller cont on ve.vehicle_id = cont.vehicle_id
		inner join controller_info cont_i on cont.controller_id = cont_i.controller_id
		where #d.d_id = #v.d_id
		for xml path(''))))
	)
, 'groups cars' = 
	--Названия всех групп автомобилей, у которых хотя бы один автомобиль входит в данного клиента
	( select (  convert(nvarchar(max),
		( select distinct vg.name + ' | '
			from vehiclegroup vg 
			inner join operator_vehiclegroup ovg on vg.vehiclegroup_id = ovg.vehiclegroup_id
			inner join operator op on op.operator_id = ovg.operator_id
			inner join #o on #o.o_id = op.operator_id
			where #o.d_id = #d.d_id
		  for xml path(''))))
	
	)
, 'Number of users' = 
	--Количество пользователей из данного клиента
	( select count ( #o.o_id )
		from #o
		where #o.d_id = #d.d_id 
	)
	  
from #d	
where #d.d_id in (
	select #o.d_id from #o 
		join OPERATOR o on o.OPERATOR_ID = #o.o_id
)
order by 'Number of cars' desc, 'Users' desc

select * 
	from (
		select 
		      UserLogin = o.LOGIN
		    , [User] = o.NAME
			, v.GARAGE_NUMBER
			, OtherUser = (select other_o.Name + ' / ' + other_o.LOGIN + ', '
				from #o oo
				join #ovr oovr on oovr.operator_id = oo.o_id
				join OPERATOR other_o on other_o.OPERATOR_ID = oo.o_id
				where oovr.vehicle_id = v.VEHICLE_ID
				  and oovr.operator_id <> o.OPERATOR_ID
				  --and not (other_o.NAME like 'R U TRAVELS')
				for xml path (''))
			, Groups = (
				select vg.Name + ', ' 
					from VEHICLEGROUP vg 
					join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLEGROUP_ID = vg.VEHICLEGROUP_ID 
					where vgv.VEHICLE_ID = ovr.vehicle_id 
					  and vg.NAME <> 'SitronicsIndia'
					for xml path (''))
			from OPERATOR o 
			join #ovr ovr on ovr.operator_id = o.OPERATOR_ID
			join VEHICLE v on v.VEHICLE_ID = ovr.vehicle_id
			where o.OPERATOR_ID in (select o_id from #o)
			  and o.LOGIN like 'kpmgnoida'
			  --and (o.NAME like 'ss%' or o.NAME like 's k%' or o.NAME like '%jaipur%')
	) t
	--where t.OtherUser is not null
	order by t.[User], t.UserLogin
		
		
		
select COUNT(1) 
	from #v v
	where v.v_id in (select ovr.vehicle_id from OPERATOR o join #ovr ovr on o.operator_id = ovr.operator_id where o.LOGIN in ('sktour', 'sssolutions'))
	
declare 	  
      @tmp_department_id int = 12
	, @department_name	nvarchar(255) = 'DELHI METRO'
	, @operator_ids		Id_Param
	, @vehicle_ids		Id_Param  
	, @vehiclegroup_ids	Id_Param 
	, @zone_ids			Id_Param 
	, @zonegroup_ids	Id_Param 

insert into @operator_ids select o.o_id from #o o where o.d_id = @tmp_department_id
insert into @vehicle_ids  select v.v_id from #v v where v.d_id = @tmp_department_id
insert into @vehiclegroup_ids
	select vg.vehiclegroup_id
		from VEHICLEGROUP vg
		where vg.VEHICLEGROUP_ID     in (select vgv.VehicleGroup_ID from VEHICLEGROUP_VEHICLE vgv where vgv.VEHICLE_ID in (select Id from @vehicle_ids))
		  and vg.VEHICLEGROUP_ID not in (select vgv.VehicleGroup_ID from VEHICLEGROUP_VEHICLE vgv where vgv.VEHICLE_ID not in (select Id from @vehicle_ids))

insert into @zone_ids
	select z.Zone_ID
		from GEO_ZONE z
		where z.ZONE_ID in (
			select ozr.Zone_ID 
				from v_operator_zone_right ozr 
				where ozr.operator_id in (select id from @operator_ids) 
			      and ozr.right_id = (select right_id from [RIGHT] where NAME = 'editzone'))

insert into @zonegroup_ids
	select zg.ZONEGROUP_ID
		from ZONEGROUP zg
		where zg.ZONEGROUP_ID in (
			select ozgr.ZONEGROUP_ID 
				from v_operator_zone_groups_right ozgr 
				where ozgr.operator_id in (select id from @operator_ids) 
			      and ozgr.right_id = (select right_id from [RIGHT] where NAME = 'editgroup'))

select * from @operator_ids id		join OPERATOR o			on o.OPERATOR_ID = id.Id
select * from @vehicle_ids  id		join VEHICLE v			on v.VEHICLE_ID = id.Id
select * from @vehiclegroup_ids id	join VEHICLEGROUP vg	on vg.VEHICLEGROUP_ID = id.Id
select * from @zone_ids id			join GEO_ZONE z			on z.ZONE_ID = id.Id
select * from @zonegroup_ids id		join ZONEGROUP zg		on zg.ZONEGROUP_ID = id.Id

begin tran

exec ExtractNewDepartment 
	@department_name = @department_name,
	@operator_ids = @operator_ids,
	@vehicle_ids = @vehicle_ids,
	@vehiclegroup_ids = @vehiclegroup_ids,
	@zone_ids = @zone_ids,
	@zonegroup_ids = @zonegroup_ids

commit

/*
drop table #v
drop table #d
drop table #o
drop table #ovr
*/