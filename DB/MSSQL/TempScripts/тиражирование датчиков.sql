declare @vehicle_id_source int
set @vehicle_id_source = 1048

--�������� ������������ �������� ��������
--delete csm_target
--	from dbo.Operator o
--	join v_operator_vehicle_right ovr_target on ovr_target.operator_id = o.operator_id and ovr_target.right_id = 102
--	join dbo.Controller c_target on c_target.Vehicle_ID = ovr_target.Vehicle_ID
--	join dbo.Controller_Sensor_Map csm_target on csm_target.Controller_ID = c_target.Controller_ID
--	where o.login = 'NIIT'
--	  and ovr_target.Vehicle_ID <> @vehicle_id_source

--���������� ����� �������� ��������
insert into dbo.Controller_Sensor_Map
	select
		 c_target.CONTROLLER_ID
		,csm_source.CONTROLLER_SENSOR_ID
		,csm_source.CONTROLLER_SENSOR_LEGEND_ID
		,csm_source.DESCRIPTION
		,csm_source.MULTIPLIER
		,csm_source.CONSTANT
		,csm_source.Min_Value
		,csm_source.Max_Value	
		from dbo.Controller c_target
		join dbo.Controller c_source on c_source.Controller_Type_ID = c_target.Controller_Type_ID
		join dbo.Controller_Sensor_Map csm_source on csm_source.Controller_ID = c_source.Controller_ID
		where c_source.Vehicle_ID = @vehicle_id_source
		  and c_target.Controller_Type_ID = 10 and c_target.Number between 0168064 and 0168076
		  and not exists (
			select 1 
				from dbo.Controller_Sensor_Map csm_e
				where csm_e.Controller_ID = c_target.Controller_ID
				  and csm_e.Controller_Sensor_ID = csm_source.Controller_Sensor_ID
				  and csm_e.Controller_Sensor_Legend_ID = csm_source.Controller_Sensor_Legend_ID
				  and (csm_e.Min_Value is not null and csm_source.Min_Value is not null and csm_e.Min_Value = csm_source.Min_Value or csm_e.Min_Value is null and csm_source.Min_Value is null)
				  and (csm_e.Max_Value is not null and csm_source.Max_Value is not null and csm_e.Max_Value = csm_source.Max_Value or csm_e.Max_Value is null and csm_source.Max_Value is null)
			)
		
