DECLARE @UTC_BEG datetime = '2019-12-17 19:00:00'
DECLARE @UTC_END datetime = '2019-12-18 18:59:59'
;WITH
[MID] AS
(
	SELECT
		[Vehicle_ID],
		[Log_Time],
		[Data]     = CAST([Data]    AS nvarchar(max)),
		[Sensors]  = CAST([Sensors] AS nvarchar(max))
	FROM [dbo].[Emulator_Source_89012_20191201_20191220]
	WHERE [Log_Time] BETWEEN [dbo].[utc2lt](@UTC_BEG) AND [dbo].[utc2lt](@UTC_END)
)
--SELECT * FROM [MID] WHERE [Data] = N'<common><Lng>59.94641</Lng><Lat>57.92741</Lat><SPEED>21</SPEED><SATELLITES>18</SATELLITES></common>'
,
--SELECT dbo.lt2utc(1576659176)
[BEG] AS
(
	SELECT TOP(1)
		[Vehicle_ID],
		[Log_Time] = [dbo].[utc2lt](@UTC_BEG),
		[Data],
		[Sensors] = NULL
	FROM [MID]
	WHERE [Data] IS NOT NULL
	ORDER BY [MID].[Vehicle_ID] ASC, [MID].[Log_Time] ASC
),
[END] AS
(
	SELECT TOP(1)
		[Vehicle_ID],
		[Log_Time] = [dbo].[utc2lt](@UTC_END),
		[Data],
		[Sensors]  = NULL
	FROM [MID]
	WHERE [Data] IS NOT NULL
	ORDER BY [MID].[Vehicle_ID] ASC, [MID].[Log_Time] DESC
)
SELECT
	dbo.lt2utc([Log_Time]),
	dbo.lt2msk([Log_Time]),
	N',(@SrcRealVehicleId, '
	+                   CAST([Log_Time] AS nvarchar(max))
	+ N', '
	+ COALESCE(N'N''' + CAST([Data]     AS nvarchar(max)) + N'''', N'NULL')
	+ N', '
	+ COALESCE(N'N''' + CAST([Sensors]  AS nvarchar(max)) + N'''', N'NULL')
	+ N')'
FROM
(
	SELECT * FROM [BEG]
	UNION ALL 
	SELECT * FROM [MID]
	UNION ALL 
	SELECT * FROM [END]
) T
ORDER BY [Vehicle_ID] ASC, [Log_Time] ASC