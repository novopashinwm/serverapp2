DECLARE
	@VEHICLE_ID int      = 89012,
	@DATEBEG    datetime = N'2019-12-01',
	@DATEEND    datetime = N'2019-12-20'

SELECT
	[Vehicle_ID],
	[Log_Time],
	[Data] = CAST(
	(
		select 
			 g.Lng,
			 g.Lat,
			 gps.SPEED,
			 pr.Radius,
			 gps.SATELLITES
		from geo_log g                          with (nolock)
			left outer join gps_log gps         with (nolock) on gps.vehicle_id = lt.vehicle_ID and gps.log_time = lt.Log_Time    
			left outer join Position_Radius pr  with (nolock) on pr.vehicle_id  = lt.vehicle_ID and pr.log_time  = lt.Log_Time
		where g.vehicle_id   = lt.vehicle_ID and g.log_time   = lt.Log_Time
		for xml path('common')
	) AS xml),
	[Sensors] = CAST(
	(
		select
			csl.Number,
			csl.Value
			from dbo.Controller_Sensor_Log csl with (nolock)
		where csl.Vehicle_ID = lt.Vehicle_ID
		and   csl.log_time = lt.Log_Time
		AND   csl.Number   NOT IN -- ��� ������������� ������� � �� ����� �� �����
		(
			11005, 11006, -- �����/����� ����� ����� 1
			11105, 11106, -- �����/����� ����� ����� 2
			11205, 11206, -- �����/����� ����� ����� 3
			11305, 11306, -- �����/����� ����� ����� 4
			11405, 11406, -- �����/����� ����� ����� 5
			11505, 11506, -- �����/����� ����� ����� 6
			11605, 11606  -- �����/����� ����� ����� 7
		)
		for xml path ('sensors')
	) AS xml)
--INTO [dbo].[Emulator_Source_89012_20191201_20191220]
FROM [dbo].[Log_Time] lt with (nolock)
WHERE (1 = 1)
AND lt.[Vehicle_ID] = @VEHICLE_ID
AND   [Log_Time]   BETWEEN [dbo].[msk2lt](@DATEBEG) AND ([dbo].[msk2lt](@DATEEND) - 1)
AND
(
	EXISTS (SELECT 1 FROM [dbo].[Geo_Log]               (NOLOCK) WHERE [Vehicle_ID] = lt.[Vehicle_ID] AND [LOG_TIME] = lt.[LOG_TIME])
	OR
	EXISTS (SELECT 1 FROM [dbo].[Controller_Sensor_Log] (NOLOCK) WHERE [Vehicle_ID] = lt.[Vehicle_ID] AND [LOG_TIME] = lt.[LOG_TIME])
)

/*
;WITH
T AS
(
	SELECT
		 [Vehicle_ID]
		,[Log_Time]
		,[Lat] = [Data].value('./*:common[1]/*:Lat[1]', 'float')
		,[Lng] = [Data].value('./*:common[1]/*:Lng[1]', 'float')
	FROM [dbo].[Emulator_Source_89012_20191201_20191220]
	WHERE 1=1
	AND [Data]    IS NOT NULL
	AND [Sensors] IS NOT NULL
	AND [Sensors].exist('./*:sensors[*:Number=11005]') = 1
	AND dbo.lt2utc([Log_Time]) BETWEEN '2019-12-19' AND '2019-12-20'
)
SELECT --TOP(100)
	T1.[Vehicle_ID],
	T1.[Log_Time],
	T1.[Lat],
	T1.[Lng],
	T2.[Log_Time],
	T2.[Lat],
	T2.[Lng],
	geography::Point(T1.[Lat], T1.[Lng], 4326).STDistance(geography::Point(T2.[Lat], T2.[Lng], 4326)),
	dbo.lt2msk(T1.[Log_Time]),
	dbo.lt2msk(T2.[Log_Time]),
	DATEDIFF(hour, dbo.lt2msk(T1.[Log_Time]), dbo.lt2msk(T2.[Log_Time])),
	T2.[Log_Time] - T1.[Log_Time]
FROM T T1
	CROSS APPLY
	(
		SELECT TOP(1)
			[Vehicle_ID],
			[Log_Time],
			[Lat],
			[Lng]
		FROM T
			WHERE 1=1
			AND [Vehicle_ID] = T1.[Vehicle_ID]
			AND [Log_Time]   > T1.[Log_Time]
			AND geography::Point(T1.[Lat], T1.[Lng], 4326).STDistance(geography::Point([Lat], [Lng], 4326)) < 100
		ORDER BY [Log_Time] DESC
	) T2
ORDER BY T1.[Vehicle_ID], T1.[Log_Time]
--ORDER BY T1.[Vehicle_ID], DATEDIFF(hour, dbo.lt2msk(T1.[Log_Time]), dbo.lt2msk(T2.[Log_Time])) DESC
*/
