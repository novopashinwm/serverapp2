DECLARE
	@FakeVehicleID int = 84060,
	@RealVehicleID int,
	@MINLT         int,
	@MAXLT         int;
SELECT
	@RealVehicleID = [RealVehicleID]
FROM [dbo].[Emulator]
WHERE [FakeVehicleID] = @FakeVehicleID
SELECT
	@MINLT = MIN([Log_Time]),
	@MAXLT = MAX([Log_Time])
FROM [dbo].[Emulator_Source] WHERE [Vehicle_ID] = @RealVehicleID
--INSERT [dbo].[Emulator_Source] ([Vehicle_ID], [Log_Time], [Data], [Sensors])
SELECT
	*
FROM
(
	SELECT
		[Vehicle_ID],
		[Log_Time] = ABS([Log_Time] - @MAXLT) + @MAXLT,
		[Data],
		[Sensors]
	FROM [dbo].[Emulator_Source]
	WHERE [Vehicle_ID] = @RealVehicleID
) T
--ORDER BY [Log_Time] ASC OFFSET 1 ROWS
--���
WHERE NOT EXISTS
(
	SELECT 1 FROM [dbo].[Emulator_Source] WHERE [Vehicle_ID] = T.[Vehicle_ID] AND [Log_Time] = T.[Log_Time]
)
