--DECLARE @vehicle_id int = 83423
--DECLARE @vehicle_id int = 82409
DECLARE @vehicle_id int = 82411
--DECLARE @log_time   int = [dbo].[msk2lt](CAST(N'2019-07-22T18:54:00'AS datetime))
--DECLARE @log_time   int = [dbo].[msk2lt](CAST(N'2019-06-01T00:00:00'AS datetime))
--DECLARE @log_time   int = (SELECT TOP(1) [LOG_TIME] FROM [dbo].[Geo_Log] WHERE [Vehicle_ID] = @vehicle_id ORDER BY [LOG_TIME])
--DECLARE @log_time   int = (SELECT TOP(1) [LOG_TIME] FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = @vehicle_id ORDER BY [LOG_TIME])
DECLARE @log_time   int = 0 -- ������� ���
SELECT [dbo].[lt2msk](@log_time)


SELECT MIN([LOG_TIME]), MAX([LOG_TIME]) FROM [dbo].[Log_Time] WHERE [Vehicle_ID] = @vehicle_id
SELECT MIN([LOG_TIME]), MAX([LOG_TIME]) FROM [dbo].[Geo_Log]  WHERE [Vehicle_ID] = @vehicle_id

SET NOCOUNT ON;
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DELETE FROM [dbo].[Geo_Log_Ignored]       OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [LOG_TIME] > @log_time
	DELETE FROM [dbo].[State_Log]             OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [LOG_TIME] > @log_time
	------------------------------------------------------------------
	DELETE FROM [dbo].[Geo_Log]               OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [LOG_TIME] > @log_time
	DELETE FROM [dbo].[Geo_Log#Lasts]         OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id
	MERGE [dbo].[Geo_Log#Lasts] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT TOP(1)
			[Vehicle_ID],
			[LOG_TIME#Last] = [LOG_TIME],
			[LOG_TIME#Prev] = LAG([Log_Time]) OVER (ORDER BY [Log_Time] ASC),
			[Lng#Last]      = [Lng],
			[Lng#Prev]      = LAG([Lng])      OVER (ORDER BY [Log_Time] ASC),
			[Lat#Last]      = [Lat],
			[Lat#Prev]      = LAG([Lat])      OVER (ORDER BY [Log_Time] ASC)
		FROM [dbo].[Geo_Log] WITH (NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		ORDER BY [Log_Time] DESC
	) AS SRC ([Vehicle_ID], [Log_Time#Last], [LOG_TIME#Prev], [Lng#Last], [Lng#Prev], [Lat#Last], [Lat#Prev])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	WHEN     MATCHED           THEN
		UPDATE
			SET
				DST.[LOG_TIME#Last] = SRC.[LOG_TIME#Last],
				DST.[LOG_TIME#Prev] = SRC.[LOG_TIME#Prev],
				DST.[Lng#Last]      = SRC.[Lng#Last],
				DST.[Lng#Prev]      = SRC.[Lng#Prev],
				DST.[Lat#Last]      = SRC.[Lat#Last],
				DST.[Lat#Prev]      = SRC.[Lat#Prev]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Log_Time#Last],     [LOG_TIME#Prev],     [Lng#Last],     [Lng#Prev],     [Lat#Last],     [Lat#Prev])
		VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[LOG_TIME#Prev], SRC.[Lng#Last], SRC.[Lng#Prev], SRC.[Lat#Last], SRC.[Lat#Prev])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	DELETE FROM [dbo].[Statistic_Log]         OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [Log_Time] > @log_time
	DELETE FROM [dbo].[Statistic_Log#Lasts]   OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id
	MERGE [dbo].[Statistic_Log#Lasts] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT
			L.*
		FROM
		(
			SELECT TOP(1)
				[Vehicle_ID], [Log_Time#Last] = [Log_Time], [Odometer#Last] = [Odometer], [IgnitionOnTime#Last] = [IgnitionOnTime]
			FROM [dbo].[Statistic_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = @vehicle_id
			ORDER BY [Log_Time] DESC
		) L
		WHERE L.[Log_Time#Last] IS NOT NULL
	) AS SRC ([Vehicle_ID], [Log_Time#Last], [Odometer#Last], [IgnitionOnTime#Last])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	WHEN     MATCHED           THEN
		UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Log_Time#Last],     [Odometer#Last],     [IgnitionOnTime#Last])
		VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[Odometer#Last], SRC.[IgnitionOnTime#Last])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	DELETE FROM [dbo].[GPS_Log]               OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [Log_Time] > @log_time
	DELETE FROM [dbo].[GPS_Log#Lasts]         OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id
	MERGE [dbo].[GPS_Log#Lasts] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT TOP(1)
			[Vehicle_ID], [Log_Time#Last] = [Log_Time]
		FROM [dbo].[GPS_Log] WITH (NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		ORDER BY [Log_Time] DESC
	) AS SRC ([Vehicle_ID], [Log_Time#Last])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	WHEN     MATCHED           THEN
		UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Log_Time#Last])
		VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	DELETE FROM [dbo].[Position_Radius]       OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [Log_Time] > @log_time
	------------------------------------------------------------------
	DELETE FROM [dbo].[Controller_Sensor_Log]       OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [Log_Time] > @log_time
	DELETE FROM [dbo].[Controller_Sensor_Log#Lasts] OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id
	MERGE [dbo].[Controller_Sensor_Log#Lasts] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT
			L.*
		FROM
		(
			SELECT DISTINCT
				[Vehicle_ID], [Number]
			FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = @vehicle_id
		) N
			CROSS APPLY
			(
				SELECT TOP(1)
					[Vehicle_ID], [Number], [Log_Time#Last] = [Log_Time]
				FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK)
				WHERE [Vehicle_ID] = N.[VEHICLE_ID]
				AND   [Number]     = N.[Number]
				ORDER BY [Log_Time] DESC
			) L
	WHERE L.[Log_Time#Last] IS NOT NULL
	) AS SRC ([Vehicle_ID], [Number], [Log_Time#Last])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Number]     = SRC.[Number]
	WHEN     MATCHED           THEN
		UPDATE SET DST.[Log_Time#Last] = SRC.[Log_Time#Last]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Number],     [Log_Time#Last])
		VALUES (SRC.[Vehicle_ID], SRC.[Number], SRC.[Log_Time#Last])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	DELETE FROM [dbo].[CAN_INFO]              OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [LOG_TIME] > @log_time
	------------------------------------------------------------------
	DELETE FROM [dbo].[Log_Time]              OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id AND [Log_Time] > @log_time
	DELETE FROM [dbo].[Log_Time#Lasts]        OUTPUT DELETED.* WHERE [Vehicle_ID] = @vehicle_id
	MERGE [dbo].[Log_Time#Lasts] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT
			*
		FROM
		(
			SELECT
				[Vehicle_Id]      = @vehicle_id,
				[Log_Time#Last]   = (SELECT TOP(1) [Log_Time]   FROM [dbo].[Log_Time] WHERE [Vehicle_Id] = @vehicle_id ORDER BY [Log_Time]   DESC),
				[InsertTime#Last] = (SELECT TOP(1) [InsertTime] FROM [dbo].[Log_Time] WHERE [Vehicle_Id] = @vehicle_id ORDER BY [InsertTime] DESC)
		) L
		WHERE L.[Log_Time#Last]   IS NOT NULL
		AND   L.[InsertTime#Last] IS NOT NULL
	) AS SRC ([Vehicle_ID], [Log_Time#Last], [InsertTime#Last])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
	WHEN     MATCHED           THEN
		UPDATE
			SET
				DST.[Log_Time#Last]   =
					CASE
						WHEN (DST.[Log_Time#Last] IS NULL OR DST.[Log_Time#Last] < SRC.[Log_Time#Last])
						THEN SRC.[Log_Time#Last]
						ELSE DST.[Log_Time#Last]
					END,
				DST.[InsertTime#Last] =
					CASE
						WHEN (DST.[InsertTime#Last] IS NULL OR DST.[InsertTime#Last] < SRC.[InsertTime#Last])
						THEN SRC.[InsertTime#Last]
						ELSE DST.[InsertTime#Last]
					END
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Log_Time#Last],     [InsertTime#Last])
		VALUES (SRC.[Vehicle_ID], SRC.[Log_Time#Last], SRC.[InsertTime#Last])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH