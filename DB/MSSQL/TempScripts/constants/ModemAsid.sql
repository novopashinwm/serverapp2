declare @asid nvarchar(255) = '654948fab7f6e545e623eacee8afbf6c'

insert into Contact (Type, Value)
	select t.Type, t.Value
		from (select Type = 3, Value = @asid) t
		where not exists (select * from Contact c where c.Type = t.Type and c.Value = t.Value)
		
exec dbo.SetConstant 'ModemAsid', @asid