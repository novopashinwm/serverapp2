insert into CONSTANTS 
	select t.Name, t.Value
	from (
		select Name = 'ServerIP', Value = '212.248.32.181'
		union
		select 'ServerPort', '12350'
		union
		select 'NetworkAPN', 'internet.mts.ru'
		union 
		select 'NetworkAPNUser', 'mts'
		union
		select 'NetworkAPNPassword', 'mts'
		union 
		select 'ServiceShortNumber', '6452'
		union 
		select 'ServiceInternationalPhoneNumber', '79154897058'
	) t
	where not exists (
		select *
			from CONSTANTS e
			where e.NAME = t.Name)