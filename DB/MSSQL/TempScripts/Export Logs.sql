USE [MED_TSS_SMALL]
GO

if not exists (select * from sys.databases where name = 'BugLog')
	create database BugLog
go
use BugLog
go
if not exists (select * from sys.tables where name='Log_Time')
	CREATE TABLE [dbo].[Log_Time](
		[Vehicle_ID] [int] NOT NULL,
		[Log_Time] [int] NOT NULL,
		[Media] [tinyint] NULL,
		[InsertTime] [datetime] NULL,

	)
go
if not exists (select * from sys.tables where name='GPS_Log')
	CREATE TABLE dbo.GPS_Log (
		[Vehicle_ID] [int] NOT NULL,
		[Log_Time] [int] NOT NULL,
		[Satellites] [tinyint] NULL,
		[Firmware] [smallint] NULL,
		[Altitude] [int] NULL,
		[Speed] [tinyint] NULL,
		[Course] [tinyint] NULL,
		[CourseDegrees]  AS (CONVERT([numeric](3),([Course]*(256))/(360),0)),
	)
go
if not exists (select * from sys.tables where name='Geo_Log')
	CREATE TABLE [dbo].[Geo_Log](
		[Vehicle_ID] [int] NOT NULL,
		[LOG_TIME] [int] NOT NULL,
		[Lng] [numeric](8, 5) NULL,
		[Lat] [numeric](8, 5) NULL,
	)
GO
if not exists (select * from sys.tables where name='Statistic_Log')
	CREATE TABLE [dbo].[Statistic_Log](
		[Vehicle_ID] [int] NOT NULL,
		[Log_Time] [int] NOT NULL,
		[Odometer] [bigint] NOT NULL,
		[IgnitionOnTime] [int] NOT NULL,
 
	)
go
if not exists (select * from sys.tables where name='Controller_Sensor_Log')
	CREATE TABLE [dbo].[Controller_Sensor_Log](
		[Vehicle_ID] [int] NOT NULL,
		[Log_Time] [int] NOT NULL,
		[Number] [int] NOT NULL,
		[Value] [bigint] NOT NULL
	)
go

truncate table BugLog.dbo.Log_Time
truncate table BugLog.dbo.GPS_Log
truncate table BugLog.dbo.Geo_Log
truncate table BugLog.dbo.Statistic_Log
truncate table BugLog.dbo.Controller_Sensor_Log

declare @vehicleId int = 821
declare @startDate int = datediff(ss, '1970', convert(datetime, '01.06.2014', 104))
declare @endDate int = datediff(ss, '1970', convert(datetime, '01.07.2015', 104))

insert into BugLog.dbo.Log_Time([Vehicle_ID], [Log_Time], Media, InsertTime)
select [Vehicle_ID], [Log_Time], Media, InsertTime
from MED_TSS_SMALL.dbo.Log_Time
where Vehicle_ID = @vehicleId and Log_Time between @startDate and @endDate

insert into BugLog.dbo.GPS_Log([Vehicle_ID], [Log_Time], [Satellites], [Firmware], [Altitude], [Speed], [Course])
select [Vehicle_ID], [Log_Time], [Satellites], [Firmware], [Altitude], [Speed], [Course]
from MED_TSS_SMALL.dbo.GPS_Log
where Vehicle_ID = @vehicleId and Log_Time between @startDate and @endDate

insert into BugLog.dbo.Geo_Log([Vehicle_ID], [Log_Time], Lat, Lng)
select [Vehicle_ID], [Log_Time], Lat, Lng
from MED_TSS_SMALL.dbo.Geo_Log
where Vehicle_ID = @vehicleId and Log_Time between @startDate and @endDate

insert into BugLog.dbo.Statistic_Log([Vehicle_ID], [Log_Time], IgnitionOnTime, Odometer)
select [Vehicle_ID], [Log_Time], IgnitionOnTime, Odometer
from MED_TSS_SMALL.dbo.Statistic_Log
where Vehicle_ID = @vehicleId and Log_Time between @startDate and @endDate

insert into BugLog.dbo.Controller_Sensor_Log([Vehicle_ID], [Log_Time], Number, Value)
select [Vehicle_ID], [Log_Time], Number, Value
from MED_TSS_SMALL.dbo.Controller_Sensor_Log
where Vehicle_ID = @vehicleId and Log_Time between @startDate and @endDate
go