--������� ������ �� ������� ������ ������ ���������
set nocount on

declare @interval int, @to int, @dtTo datetime, @limitDT int;
select @interval = 5555;

--����, �� ������� ����� ������� ������
declare @deleteToDateTime datetime;
set @deleteToDateTime = cast('2007-01-01 00:00' as datetime)

select @limitDT = datediff(ss, cast('1970-01-01' as datetime), @deleteToDateTime)

select @to = min(log_time) from monitoree_log (nolock)

print convert(varchar(40), dateadd(ss, @to, '1970-01-01'));

declare @sql nvarchar(max);

set @sql = '
	delete from dbo.monitoree_log
	where log_time < @to

	delete from dbo.controller_stat
	where [time] < @dtTo
';

while @to < @limitDT
begin
	
	set @dtTo = dateadd(ss, @to, '1970-01-01');
	print 'delete start'

	exec sp_executesql @sql, N'@to int, @dtTo datetime', @to = @to, @dtTo = @dtTo

	select @to = @to + @interval;

	print convert(varchar(40), @to);
	print convert(varchar(40), @dtTo);
end;
