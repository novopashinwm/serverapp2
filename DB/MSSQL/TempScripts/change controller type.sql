begin tran

declare @vehicle_id int = 4668
declare @new_controller_type_id int = (select Controller_Type_ID from CONTROLLER_TYPE where TYPE_NAME = 'AvtoGrafCan')

update c
	set controller_type_id = @new_controller_type_id
	from CONTROLLER c
	where c.VEHICLE_ID = @vehicle_id
	  and c.CONTROLLER_TYPE_ID <> @new_controller_type_id

	  
update csm
	set controller_sensor_id = new_cs.controller_sensor_id
	from CONTROLLER_SENSOR_MAP csm
	join CONTROLLER c on c.CONTROLLER_ID = csm.controller_id
	join controller_sensor old_cs on old_cs.controller_sensor_Id = csm.controller_sensor_id
	join controller_sensor new_cs on new_cs.number = old_cs.number 
	                             and new_cs.descript = old_cs.descript
	                             and new_cs.descript <> ''
	where c.vehicle_id = @vehicle_Id
	  
delete csm
	from CONTROLLER_SENSOR_MAP csm
	join CONTROLLER c on c.CONTROLLER_ID = csm.controller_id
	join controller_sensor cs on cs.controller_sensor_Id = csm.controller_sensor_id
	where cs.controller_type_id <> c.controller_type_id
	  and c.vehicle_id = @vehicle_Id
	  
	  
commit