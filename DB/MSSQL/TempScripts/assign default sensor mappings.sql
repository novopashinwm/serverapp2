update updatedCS
	set Mandatory = 1,
	    Default_Sensor_Legend_ID = sourceCSM.Controller_Sensor_Legend_ID,
	    Default_Multiplier = sourceCSM.Multiplier,
	    Default_Constant = sourceCSM.Constant
	from Controller_Sensor updatedCS
	join (
		select ct.type_name, legend.Controller_Sensor_Legend_ID, legend.Name, pop.Controller_Sensor_ID, pop.DESCRIPT, pop.Multiplier, pop.Constant, pop.Popularity
		from controller_type ct 
		join controller_sensor_legend legend on 1=1
		cross apply (
			select top(1) 
						  cs.Controller_Sensor_ID,
						  cs.Descript, 
						  csm.Multiplier, 
						  csm.Constant, 
						  Popularity = count(1)
				from controller c
				join controller_sensor_map csm on csm.controller_id = c.controller_id
				join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
				where c.controller_type_id = ct.controller_type_id
				  and cs.controller_type_id = ct.controller_type_id 
				  and csm.controller_sensor_legend_id = legend.controller_Sensor_legend_id
				group by cs.Controller_Sensor_ID, cs.Descript, csm.Multiplier, csm.Constant
				order by count(1) desc
		) pop
		where pop.Popularity > 5	
	) sourceCSM on sourceCSM.Controller_Sensor_ID = updatedCS.Controller_Sensor_ID
	where updatedCS.Mandatory = 0
