/* ���������� ������ � ������ �� �������, ��������� � �������������������� ����������� �� ��������*/
--set nocount on
--1. ���������� ������ ����� ��� ����������� ������.
create table #vehicle (id int primary key clustered);

insert into #vehicle (id) values (3438)
--	select vehicle_id 
--		from dbo.Controller c
--		join dbo.Controller_Type ct on ct.Controller_Type_ID = c.Controller_Type_ID
--		where ct.Pos_Smooth_Support = 1

-- ����� � ����� �� ������ ������

declare @vehicleID int
declare @from int
declare @to int

set @from = datediff(ss, '1970', '2014.02.25 03:35:00') - 4 * 3600
set @to   = datediff(ss, '1970', '2014.02.26 04:10:00') - 4 * 3600

declare @stopSpeed int, @stopSpeedNoIgnition int;
set @stopSpeed = 10;
set @stopSpeedNoIgnition = 15;

declare 
	@Current_Log_Time int, 
	@Current_Speed tinyint, 
	@Current_Ignition bit,
	@Current_Lat numeric(8,5),
	@Current_Lng numeric(8,5),
	@Current_Vs_Present bit,
	@Current_Satellites int,
	@Previous_Log_Time int, 
	@Previous_Speed tinyint, 
	@Previous_Ignition bit,
	@Previous_Lat numeric(8,5),
	@Previous_Lng numeric(8,5),
	@Previous_Satellites int

declare @minLogTime int;

set @vehicleID = (select min(id) from #vehicle);

while @vehicleID is not null
begin	
	print convert(varchar(20), getdate()) + ' : ' + '������: ' + convert(varchar(10), @vehicleID)

	while (1=1) 
	begin
		declare @deleted bit, @updated bit;
		set @deleted = 0;
		set @updated = 0;
	
		select top(1)
			@Current_Log_Time = lt.Log_Time, 
			@Current_Speed = gpsl.Speed, 
			@Current_Ignition = case when sensl.ds is not null and sensl.ds&32 = 0 then 1 else 0 end,
			@Current_Lat = geol.Lat,
			@Current_Lng = geol.Lng,
			@Current_Vs_Present = case when sensl.V is not null or 
										    sensl.V1 is not null or
										    sensl.V2 is not null or
										    sensl.V3 is not null or
										    sensl.V4 is not null
										then 1
										else 0
								  end,
			@Current_Satellites = gpsl.Satellites
		from dbo.Log_Time lt
			join dbo.GPS_Log gpsl        on gpsl.Vehicle_ID = @vehicleID and gpsl.Log_Time  = lt.Log_Time
			join dbo.Sensors_Log sensl   on gpsl.Vehicle_ID = @vehicleID and sensl.Log_Time  = lt.Log_Time
			join dbo.Geo_Log geol        on gpsl.Vehicle_ID = @vehicleID and geol.Log_Time  = lt.Log_Time
		where lt.Vehicle_ID = @vehicleID
		  and lt.Log_Time between isnull(@Previous_Log_Time+1, @from) and @to
		order by lt.Log_Time


		if @@ROWCOUNT = 0 
			break;

		--3. ���������, �� ����� �� �������������
		
		if (@Previous_Log_Time is not null)
		begin
			if (@Current_Satellites < 4)
			begin
				--��������� ������ 4, ������, ���������� ���������� (��������, ������ ��� �������)
				delete from dbo.Geo_Log 
					where Vehicle_ID = @vehicleID 
					  and Log_Time = @Current_Log_Time
				set @deleted = 1
			end
			else 
				--����������� �� ��������
				if ((@Current_Speed < @stopSpeed or (@Current_Speed < @stopSpeedNoIgnition and @Current_Ignition = 1))
					and @Previous_Speed < @stopSpeedNoIgnition)
				begin
					declare @smooth_radius int;

					if (@Current_Ignition = 1)
					begin 
						if (@Current_Speed = 0 and @Current_Speed = 0)
							set @smooth_radius = 200;
						else
							set @smooth_radius = 100;
					end
					else
					begin 
						if (@Current_Speed = 0 and @Previous_Speed = 0)
							set @smooth_radius = 300;
						else
							set @smooth_radius = 200;
					end

					--������� ���������� ����� ����� � ���������� �������
					declare @dist real;
					select @dist = dbo.GetTwoGeoPointsDistance(@Previous_Lng, @Previous_Lat, @Current_Lng, @Current_Lat);
					if (@dist < @smooth_radius)
					begin
						--4. �������� ������: ������ ������� ������� � ������ ��� ������� �������, ���� ����� ����� ������������� ��������� ������ 10 �����
						if (@Current_Vs_Present = 0 and (@Current_Log_Time - @Previous_Log_Time) < 600)
						begin
							delete from dbo.Geo_Log 
								where Vehicle_ID = @vehicleID 
								  and Log_Time = @Current_Log_Time
							set @deleted = 1
						end
						else
						begin
							if @dist > 0 
							begin
								update dbo.Geo_Log 
									set Lat = @Previous_Lat,
										Lng = @Previous_Lng
									where Vehicle_ID = @vehicleID 
									  and Log_Time = @Current_Log_Time
								set @updated = 1;
							end;
						end;
					end;
				end;
			if (@minLogTime is null and (@deleted = 1 or @updated = 1))
				set @minLogTime = @Previous_Log_Time
		end;

		if (@deleted = 0)
		begin			
			set @Previous_Log_Time	= @Current_Log_Time;
			set @Previous_Speed		= @Current_Speed;
			set @Previous_Ignition	= @Current_Ignition;
			if (@updated = 0) 
			begin
				set @Previous_Lat		= @Current_Lat;
				set @Previous_Lng		= @Current_Lng;
				set @Previous_Satellites= @Current_Satellites;
			end;
		end;
	end;	

	--5. ����������� �������.

	if @minLogTime is not null 
	begin
		declare @Log_Time_ID bigint;
		set @Log_Time_ID = (select top(1) ID from dbo.Log_Time where Vehicle_ID = @vehicleID and Log_Time = @minLogTime)

		exec dbo.AddStatisticLog @Log_Time_ID, 0;

		declare @interval int
		set @interval = (datediff(ss, '1970', getutcdate())) - @minLogTime

		print convert(varchar(20), getdate()) + ' : ' + '���������� ����������: ' + convert(varchar(10), @vehicleID);
		exec dbo.RecalcStatisticLog @vehicleID, @interval, 1
	end;

	set @vehicleID = (select min(id) from #vehicle where @vehicleID < id)
	set @Previous_Log_Time = null;
	set @minLogTime = null;
end

drop table #vehicle;