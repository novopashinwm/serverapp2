--set statistics time off
set nocount on
--������� ������ �� ������� ������ ������ ���������
declare @interval int, @to int, @dtTo datetime, @limitDT int;
select @interval = 5555;
select @limitDT = datediff(s, cast('1970-01-01' as datetime), cast('2007-01-01 00:00' as datetime))

select @to = datediff(s, cast('1970-01-01' as datetime), cast('2000-01-01 00:00' as datetime))

select @to = min(log_time) from monitoree_log (nolock)

while @to < @limitDT
begin
	select @dtTo = dbo.getdatefromint(@to);

	delete from dbo.monitoree_log
	where log_time < @to

	delete from dbo.can_info 
	where log_time < @to

	delete from dbo.controller_stat
	where [time] < @dtTo

	select @to = @to + @interval;

	print @dtTo;
end


/*
monitoree_log
controller_stat
can_info
*/