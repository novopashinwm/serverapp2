--======================================================================================
---�������� ����� ����
--======================================================================================
declare @from int, @TOfrom int, @to int;
declare @vehicle_from int, @vehicle_to int;

set @vehicle_from = 538;
set @from = 1264425163; --datediff(s, cast('1970-01-01' as datetime), cast('2009-09-02 10:58:00' as datetime));
set @to = 1264455905; --datediff(s, cast('1970-01-01' as datetime), cast('2009-09-02 13:50:18' as datetime));

set @vehicle_to = 496;
set @TOfrom = 1264776140; --datediff(s, cast('1970-01-01' as datetime), cast('2009-09-14 23:58:00' as datetime));

insert into log_time (vehicle_id, log_time, media, insertTime)
select @vehicle_to, lt.LOG_TIME + (@TOfrom - @from), media, insertTime + (@TOfrom - @from)
from log_time lt (nolock)
where lt.vehicle_id = @vehicle_from
and lt.log_time >= @from and lt.log_time <= @to
and not exists (select vehicle_id from log_time lt2 (nolock) where lt2.vehicle_id = @vehicle_to and lt2.log_time = lt.LOG_TIME + (@TOfrom - @from))
order by lt.log_time asc


insert into geo_log (vehicle_id, log_time, log_time_id, Lng, Lat)
select @vehicle_to, lt2.log_time, lt2.id, g.Lng, g.Lat
from geo_log g (nolock)
join log_time lt2 (nolock) on lt2.log_time = (g.log_time + (@TOfrom - @from))
where g.vehicle_id = @vehicle_from
and g.log_time >= @from and g.log_time <= @to
and lt2.vehicle_id = @vehicle_to
and not exists (select vehicle_id from geo_log g2 
				where g2.log_time = lt2.log_time 
				and g2.vehicle_id = @vehicle_to)

insert into gps_log(vehicle_id, log_time, log_time_id, Satellites, Altitude, Speed)
select @vehicle_to, lt2.log_time, lt2.id, g.Satellites, g.Altitude, g.Speed
from gps_log g (nolock)
join log_time lt2 (nolock) on lt2.log_time = (g.log_time + (@TOfrom - @from))
where g.vehicle_id = @vehicle_from
and g.log_time >= @from and g.log_time <= @to
and lt2.vehicle_id = @vehicle_to
and not exists (select vehicle_id from gps_log g2 
				where g2.log_time = lt2.log_time 
				and g2.vehicle_id = @vehicle_to)


if not exists (select vehicle_id from dbo.Statistic_Log_Recalc 
				where vehicle_id = @vehicle_to and log_time < @TOfrom)
begin
	delete from dbo.Statistic_Log_Recalc where vehicle_id = @vehicle_to;

	insert into dbo.Statistic_Log_Recalc (vehicle_id, log_time)
	values (@vehicle_to, @TOfrom);

	exec dbo.RecalcStatisticLog @vehicle_to;
end

/*

--��������
select * from geo_log g (nolock) 
where g.vehicle_id = @vehicle_to
and g.log_time >= @from + (@TOfrom - @from) 
and g.log_time <= @to + (@TOfrom - @from)

select * from log_time
where vehicle_id = @vehicle_to
and log_time >= @from + (@TOfrom - @from) 
and log_time <= @to + (@TOfrom - @from)

*/


/*
712 = 538
select top 10 * from gps_log
*/

/*
4769
2010-01-27 17:36:44.000	2010-01-28 02:09:06.000

select top 100
v.[garage_NUMBER], dbo.getdatefromint(ml.log_time), ml.*
from MONITOREE_LOG ml (nolock)
join vehicle v (nolock) on v.vehicle_id = ml.MONITOREE_id
WHERE 1=1
and MONITOREE_id = 496
and log_time > datediff(s, cast('1970-01-01' as datetime), cast('2010-01-29 14:40' as datetime))
--and log_time < datediff(s, cast('1970-01-01' as datetime), cast('2010-02-09 05:25' as datetime))
order by log_time asc

--TOto = 1265692831 30,00014 59,46249
--select 1265692831 - 1265634374 = 58457
--select 1264425163 - 1264455905 = 30742
*/
--======================================================================================
