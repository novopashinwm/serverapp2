insert into dbo.Geo_Log (Log_Time_ID,    Vehicle_ID,    Log_Time,   Lng,   Lat)
	select					   lt.ID, lt.Vehicle_ID, lt.Log_Time, mli.X, mli.Y
		from dbo.Log_Time lt
		join dbo.Monitoree_Log_Ignored mli on mli.Monitoree_ID = lt.Vehicle_ID 
										  and mli.Log_Time = lt.Log_Time
		left outer join dbo.Geo_Log gl on gl.Log_Time_ID = lt.ID
		where lt.Vehicle_ID = 611 and			  
			 mli.Reason = 1	and
			  gl.Log_Time_ID is null

insert into dbo.Gps_Log (Log_Time_ID,    Vehicle_ID,    Log_Time,	  Satellites,	  Firmware,     Altitude,     Speed)
	select					   lt.ID, lt.Vehicle_ID, lt.Log_Time, mli.Satellites, mli.Firmware,   mli.Height, mli.Speed
		from dbo.Log_Time lt
		join dbo.Monitoree_Log_Ignored mli on mli.Monitoree_ID = lt.Vehicle_ID 
										  and mli.Log_Time = lt.Log_Time
		left outer join dbo.Gps_Log gl on gl.Log_Time_ID = lt.ID
		where lt.Vehicle_ID = 611 and			  
			 mli.Reason = 1 and
			  gl.Log_Time_ID is null

select min(id) from log_Time where vehicle_id = 611
exec dbo.AddStatisticLog 611