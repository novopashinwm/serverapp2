
IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'DelDepartment'
        AND 	  type = 'P')
    DROP PROCEDURE DelDepartment
GO

-----------------------------------------------------------------------------------
--����������: ������� ������� ��� �������� �� �� �������� ��������� ��������
--�����: ����������� �.�.
-----------------------------------------------------------------------------------

CREATE PROCEDURE dbo.DelDepartment
	@department_id			int 
AS

-----------------------------------------------------------------------------------
/*
-- Test
declare 
	@department_id	int 

set @department_id = 9
*/
-----------------------------------------------------------------------------------

-- ���������� �� � ���������
update VEHICLE Set DEPARTMENT = null where DEPARTMENT = @department_id
update DRIVER Set DEPARTMENT = null where DEPARTMENT = @department_id
delete OPERATOR_DEPARTMENT where DEPARTMENT_ID = @department_id
delete OPERATORGROUP_DEPARTMENT where DEPARTMENT_ID = @department_id

--������� DECADE, �� ������ ��� ��� � ��� �������
--update DECADE Set DEPARTMENT = null where DEPARTMENT = @department_id

-- ��������� ������� decade_id
create table #decade(	
	--id int identity primary key, 
	decade_id 	int
	)
-- ��������� ������� decade_id
create table #brigade(	
	--id int identity primary key, 
	brigade_id 	int
	)
-- ��������� ������� decade_id
create table #brigade_driver(	
	--id int identity primary key, 
	brigade_driver_id 	int
	)
-- ��������� ������� decade_id
create table #journal_day(	
	--id int identity primary key, 
	journal_day_id 	int
	)

insert into #decade select distinct DECADE_ID from DECADE where DEPARTMENT = @department_id
if ((select count(*) from #decade) > 0)
begin
	
	insert into #brigade select distinct BRIGADE_ID from BRIGADE where DECADE in (select DECADE_ID from #decade)
	if ((select count(*) from #brigade) > 0)
	begin
		
		insert into #brigade_driver select distinct BRIGADE_DRIVER_ID from BRIGADE_DRIVER where BRIGADE in (select BRIGADE_ID from #brigade)
		if((select count(*) from #brigade_driver) > 0)
		begin

			insert into #journal_day select distinct JOURNAL_DAY_ID from JOURNAL_DAY where BRIGADE_DRIVER in (select BRIGADE_DRIVER_ID from #brigade_driver)
			if((select count(*) from #journal_day) > 0)
			begin
				update ORDER_TRIP Set JOURNAL_DAY = null where JOURNAL_DAY in (select JOURNAL_DAY_ID from #journal_day)
			end 
			
			delete from BRIGADE_DRIVER where BRIGADE_DRIVER_ID in (select BRIGADE_DRIVER_ID from #brigade_driver)
		end

		delete from BRIGADE where BRIGADE_ID in (select BRIGADE_ID from #brigade)
	end

	delete from DECADE where DECADE_ID in (select DECADE_ID from #decade)
end

-- ������� ���������� DEPARTMENT
delete from DEPARTMENT where DEPARTMENT_ID = @department_id

drop table #decade
drop table #brigade
drop table #brigade_driver
drop table #journal_day


GO


