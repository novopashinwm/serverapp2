﻿IF (OBJECT_ID(N'[dbo].[ProcessControllerInfo]') IS NOT NULL)
	DROP PROCEDURE [dbo].[ProcessControllerInfo];
GO

CREATE PROCEDURE [dbo].[ProcessControllerInfo]
(
	@controller int,
	@speed      int out,
	@sat        int out,
	@fw         int out,
	@tm         datetime = NULL
)
AS
BEGIN
	IF (@tm IS NULL) SET @tm = GETUTCDATE()

	IF (@sat < 1) SET @sat = NULL
	IF (@fw  < 1) SET @fw  = NULL

	-- fit vals into fields format
	IF (@speed > 0xff   OR @speed < 0) SET @speed = 0xff
	IF (@sat   > 0xff   OR @sat   < 0) SET @sat   = NULL
	IF (@fw    > 0xffff OR @fw    < 0) SET @fw    = NULL
END