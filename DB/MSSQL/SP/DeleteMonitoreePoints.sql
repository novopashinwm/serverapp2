SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER      PROCEDURE [dbo].[DeleteMonitoreePoints]
	@ids_table_name nvarchar(50)
AS

exec('

declare @monitoree_id int


set @monitoree_id = (select top 1 id_vehicle from ['+@ids_table_name+'])



if ((select count (distinct id_vehicle) from ['+@ids_table_name+']) > 1)
begin
RAISERROR (''multiple id_vehicle found... must be one!'', 16, 1)
RETURN
end

select * from monitoree_log
where monitoree_id = @monitoree_id 
and log_time in (select time from ['+@ids_table_name+'])

update monitoree_log
set SATELLITES = 0
where monitoree_id = @monitoree_id 
and log_time in (select time from ['+@ids_table_name+'])

')



