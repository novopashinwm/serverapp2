IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetWayoutInfo]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetWayoutInfo] 

GO


-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�������� �.�.
-- Create date: 7.11.2008
-- Description:	������ �� ������� ��� ������
-- =============================================
CREATE PROCEDURE [dbo].[GetWayoutInfo] 
	@date datetime,
	@route_number varchar(10)
AS

set transaction isolation level read uncommitted

BEGIN
	SET NOCOUNT ON;

	declare @route_id int;
	
	select @route_id = route_id from ROUTE r where r.EXT_NUMBER = @route_number;

    select [SCHEDULE_ID],ROUTE_ID into #sch from schedule where day_kind_ID = dbo.DayKind(dateadd(hh, 8, @date), @route_id)

	select distinct t.TRIP_ID, s.SCHEDULE_ID, s.ROUTE_ID, t.BEGIN_TIME, 
	t.END_TIME,s.WAYOUT, t.TRIP, t.ROUTE_TRIP_ID, wo.EXT_NUMBER as WO_EXT_NUMBER 
	into #tmp_sch from #sch
	join dbo.SCHEDULE_DETAIL sd on sd.SCHEDULE_ID = #sch.SCHEDULE_ID
	join TRIP t on t.SCHEDULE_DETAIL_ID = sd.SCHEDULE_DETAIL_ID
	join [ROUTE] r on  #sch.ROUTE_ID = r.ROUTE_ID
	join SCHEDULE s on s.SCHEDULE_ID = sd.SCHEDULE_ID
	join WAYOUT wo on wo.WAYOUT_ID = s.WAYOUT
	where r.EXT_NUMBER = @route_number and (t.TRIP_KIND_ID = 5 or t.TRIP_KIND_ID = 6)
	
	create table #t(TRIP_ID int, TIME_IN int, TIME_OUT int)

	declare @crs cursor, @tripid int
	set @crs = cursor local read_only fast_forward for 
		select distinct
		TRIP_ID from #tmp_sch
	open @crs
	while 1 = 1
	begin
		fetch next from @crs into @tripid
		if @@fetch_status <> 0 
			break
		insert #t select TRIP_ID, MIN(TIME_IN), MAX(TIME_OUT) from SCHEDULE_POINT where TRIP_ID = @tripid
		group by TRIP_ID
	end
	close @crs
	deallocate @crs
	
	select #tmp_sch.TRIP_ID,
		   #tmp_sch.SCHEDULE_ID,
		   #tmp_sch.ROUTE_ID,
		   #tmp_sch.WAYOUT,
		   #tmp_sch.TRIP,
		   #tmp_sch.ROUTE_TRIP_ID,
		   #tmp_sch.WO_EXT_NUMBER,
		   #t.TIME_IN,
		   #t.TIME_OUT	
	 into #schedule from #tmp_sch join #t on #t.TRIP_ID = #tmp_sch.TRIP_ID

	select wbt.WB_TRIP_ID,wbt.TRIP_ID,wbh.VEHICLE_ID,w.DRIVER_ID, wbh.WAYBILL_DATE 
	into #wbt from dbo.WAYBILL_HEADER wbh
	join dbo.WB_TRIP wbt on wbt.WAYBILL_HEADER_ID = wbh.WAYBILL_HEADER_ID
	join SCHEDULE s on s.SCHEDULE_ID = wbh.SCHEDULE_ID
	join [ROUTE] r on  s.ROUTE_ID = r.ROUTE_ID
	join WAYBILL w on wbt.WAYBILL_ID = w.WAYBILL_ID
	where datepart(dy, @date) = datepart(dy, wbh.WAYBILL_DATE)
	and datepart(y, @date) = datepart(y, wbh.WAYBILL_DATE)
	and r.EXT_NUMBER = @route_number

	select 
	WO_EXT_NUMBER, COMMENTS, TIME_IN as BEGIN_TIME, TIME_OUT as END_TIME, 
		d.EXT_NUMBER + ' / ' + d.SHORT_NAME + ' ' + d.NAME as DRIVER_NAME,
		v.GARAGE_NUMBER, gt.[LENGTH], WB_TRIP_ID, TRIP, WAYBILL_DATE 
	from #wbt
	right outer join #schedule on #wbt.TRIP_ID = #schedule.TRIP_ID
	join ROUTE_TRIP rt on rt.ROUTE_TRIP_ID = #schedule.ROUTE_TRIP_ID
	join GEO_TRIP gt on gt.GEO_TRIP_ID = rt.GEO_TRIP_ID
	left join DRIVER d on d.DRIVER_ID = #wbt.DRIVER_ID
	left join VEHICLE v on v.VEHICLE_ID = #wbt.VEHICLE_ID
	order by wayout, TRIP


	drop table #sch
	drop table #wbt
	drop table #tmp_sch
	drop table #t
	drop table #schedule
END
GO
