if exists (select * from sys.procedures where name = 'GetMedia')
	drop procedure GetMedia
go

CREATE PROCEDURE dbo.GetMedia  
AS  
 select		
	  m.MEDIA_ID
	, ma.MA_ID
	, Media_Type = mt.Name
	, Media_Name = m.NAME
	, ma.Name
	, INITIALIZATION = convert(varchar, ma.MA_ID) + '|' + INITIALIZATION  
 from MEDIA m 
 join MEDIA_ACCEPTORS ma on m.MEDIA_ID = ma.MEDIA_ID  
 join MEDIA_TYPE mt on mt.ID = m.TYPE
  
  
  
  
  
  
  
  
  
  