if exists (select * from sys.objects where name = 'EscapeLikeTemplate')
	drop function EscapeLikeTemplate
go

create function EscapeLikeTemplate
(
	@s nvarchar(max),
	@escape varchar(1) = '\'
)
returns nvarchar(max)
begin
	return replace(replace(replace(@s, '\', '\\'), '_', '\_'), '%', '\%')
end