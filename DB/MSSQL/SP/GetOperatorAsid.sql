if exists (
	select * from sys.procedures where name = 'GetOperatorAsid'
)
	drop procedure GetOperatorAsid
go

create procedure GetOperatorAsid
(
	@operator_id int
)
as

set nocount on

select top(1) a.ID, bb.BlockingDate
	from Asid a
	left outer join Billing_Blocking bb on bb.Asid_ID = a.ID
	where a.Operator_ID = @operator_id
	order by a.ID desc