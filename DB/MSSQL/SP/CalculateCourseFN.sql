﻿IF (OBJECT_ID(N'[dbo].[CalculateCourseFN]') IS NOT NULL)
	DROP FUNCTION [dbo].[CalculateCourseFN];
GO

CREATE FUNCTION [dbo].[CalculateCourseFN]
(
	@lat1 numeric(8,5),
	@lng1 numeric(8,5),
	@lat2 numeric(8,5),
	@lng2 numeric(8,5)
)
RETURNS int
AS
BEGIN
	DECLARE
		@EarthRadius      real = 6367444.0,
		@DegreesToRadians real = PI()  / 180.0,
		@RadiansToDegrees real = 180.0 / PI();

	IF ( @lat1 > 90 or @lng1 > 180)
		RETURN 0;

	DECLARE
		@dx float = @DegreesToRadians * (@lng2 - @lng1),
		@dy float = @DegreesToRadians * (@lat2 - @lat1);
	DECLARE
		@xm float = @EarthRadius * COS(@DegreesToRadians * (@lat1)) * @dx,
		@ym float = @EarthRadius * @dy;
	DECLARE
		@norm  float = SQRT(@xm * @xm + @ym * @ym),
		@arg   float,
		@arc   float,
		@signx int,
		@signy int;

	IF (0 = @norm)
		RETURN 0;

	SET @arg = @EarthRadius * ABS(@DegreesToRadians * (@lat2 - @lat1)) / @norm;
	IF (@arg < -1)
		SET @arg = -1;
	ELSE
	IF (@arg > 1)
		SET @arg = +1;

	SET @arc   = ASIN(@arg) * @RadiansToDegrees;
	SET @signx = 1;
	IF (@lng2 - @lng1 < 0)
		SET @signx = -1;
	SET @signy = 1;
	IF (@lat2 - @lat1 < 0)
		SET @signy = -1;

	IF (@signx > 0 AND @signy > 0)
		SET @arc = 90.0 - @arc;
	IF (@signx > 0 AND @signy < 0)
		SET @arc = 90.0 + @arc;
	IF (@signx < 0 AND @signy < 0)
		SET @arc = 270.0 - @arc;
	IF (@signx < 0 AND @signy > 0)
		SET @arc = 270.0 + @arc;
	return @arc;
END
GO