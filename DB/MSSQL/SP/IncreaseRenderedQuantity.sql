if exists (
	select * from sys.objects where name = 'IncreaseRenderedQuantity'
)
drop procedure IncreaseRenderedQuantity;
go
SET QUOTED_IDENTIFIER ON
GO

create procedure IncreaseRenderedQuantity
(
	@billingServiceID int,
	@type varchar(32),
	@max_quantity int = null
)
as
begin
	set nocount on

	if @max_quantity < 1
		return;

	exec dbo.ActivateService @billingServiceID

	declare @now datetime = GETUTCDATE()
	declare @action nvarchar(6)
	declare @type_ID int = (select ID from Rendered_Service_Type where Name = @type)

	insert into Rendered_Service (Billing_Service_ID, Rendered_Service_Type_ID, Quantity, LastTime)
		select @billingServiceID, @type_ID, 1, @now
			where not exists (	
					select 1
						from Rendered_Service e with (XLOCK, SERIALIZABLE) 
						where e.Billing_Service_ID = @billingServiceID 
						  and e.Rendered_Service_Type_ID = @type_ID)
						  
	if @@ROWCOUNT <> 0
	begin
		set @action = 'INSERT'
		
		select 1
	end
	else
	begin
		set @action = 'UPDATE'
	
		update rs
			set Quantity = isnull(Quantity, 0) + 1,
				LastTime = case when rs.LastTime < @now or rs.LastTime is null then @now else rs.LastTime end
			output inserted.Quantity
			from Rendered_Service rs with (XLOCK, SERIALIZABLE)
			where rs.Billing_Service_ID = @billingServiceID
			  and rs.Rendered_Service_Type_ID = @type_ID
			  and (@max_quantity is null or Quantity < @max_quantity)

		if @@ROWCOUNT = 0
			return;
	end

	insert into H_Rendered_Service 
			  (ID, Billing_Service_ID, Rendered_Service_Type_ID, Quantity, LastTime, ACTION, ACTUAL_TIME)
		select ID, Billing_Service_ID, Rendered_Service_Type_ID, Quantity, LastTime,
				ACTION = @action, 
				ACTUAL_TIME = @now
		from Rendered_Service rs
		where rs.Billing_Service_ID = @billingServiceID
		  and rs.Rendered_Service_Type_ID = @type_ID

end