set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetLogAndZonesForVehicleForOperator' and type='p'))
	drop procedure dbo.GetLogAndZonesForVehicleForOperator;
go


/*
declare 
@vehicle_zone dbo.Vehicle_Zone_Param,
@vehicle_position dbo.Vehicle_Position_Param 

insert into @vehicle_zone
select 33538, 4521

insert into @vehicle_position
select top(5000) vehicle_id, Log_Time, lat, lng
from Geo_Log where Vehicle_ID = 33538
and Log_Time between dbo.msk2lt('2015-09-01 14:00') and dbo.msk2lt('2015-09-01 14:40')
order by Log_Time desc

create table #ZonesForPositions(
	zone_id		int, 
	vehicle_id		int,
	Log_Time		int,
		PRIMARY KEY CLUSTERED 
		(
			Log_Time asc, zone_id asc
		)
	)
insert into #ZonesForPositions
exec dbo.GetGeoZonesForPositions   @vehicle_zone, @vehicle_position

--select dbo.msk2lt('2015-09-01 14:00') and dbo.msk2lt('2015-09-01 14:40')

declare 	@dtFrom			int, 
	@dtTo			int

select @dtFrom = dbo.msk2lt('2015-09-01 14:00'), @dtTo = dbo.msk2lt('2015-09-01 14:40')

exec GetLogAndZonesForVehicleForOperator @dtFrom, @dtTo, 33538, 187, '4521'

*/


CREATE  PROCEDURE [dbo].[GetLogAndZonesForVehicleForOperator]
	@dtFrom			int, 
	@dtTo			int, 
	@vehicle_id int,
	@operatorId int = null,
	@zoneIDList nvarchar(max) = null
AS

set nocount on;

declare 
	@vehicle_zone dbo.Vehicle_Zone_Param,
	@vehicle_position dbo.Vehicle_Position_Param 

-- �������� ��������� ������
create table #ZonesForPositions(
	zone_id		int, 
	vehicle_id		int,
	Log_Time		int,
		PRIMARY KEY CLUSTERED 
		(
			Log_Time asc, zone_id asc
		)
	)


create table #MonitoreeLog(	
	id int identity primary key CLUSTERED, 
	Log_Time		int,
	X				float,
	Y				float,
	Speed			int,
	zone_id int,
	group_id int,
	move_status int,
	move_status_group_id int,
	)

create table #ZonesList(	
	pos int identity primary key,
	zone_id int, 
	zone_name nvarchar(100)
	)

create table #Primitives(	
	zone_id int,
	pos int, 
	primitive_id int,
	PRIMARY KEY CLUSTERED 
		(
			primitive_id asc, zone_id asc
		)WITH (IGNORE_DUP_KEY = OFF)
	)

create table #PoligonePoints(
	primitive_id int,
	pos int, 
	X real, 
	Y real,
	PRIMARY KEY CLUSTERED 
		(
			PRIMITIVE_ID asc, pos asc
		)WITH (IGNORE_DUP_KEY = OFF)
	)

create table #AllPoligonePoints(
	pos int,	
	X1 real, 
	X2 real,
	Y1 real, 
	Y2 real,
	primitive_id int,
	zone_id int, 
	PRIMARY KEY CLUSTERED 
		(
			primitive_id asc, zone_id asc, pos asc
		)WITH (IGNORE_DUP_KEY = OFF)
	)

create table #Result(
	id int identity PRIMARY KEY CLUSTERED,
	group_id int,	
	zone_name nvarchar(100) COLLATE Cyrillic_General_CI_AI,
	zone_id int,
	dt_from int,
	dt_to int, 
	distance real,
	avg_distance real,
	standart_distance real,
	stops_time int,
	zone_id_from int null,
	zone_id_to int null
	)

-- AS: ���������� ��� ���� - ������� ������ ����������� ��� ��� ���������
create table #zones(
	zone_id int,
	[name] nvarchar(100),
	zone_type_id int,
	type_name nvarchar(100),
	color int,
	description nvarchar(100),
	Editable bit
)

create table #vehicles(
	vehicle_id int primary key
)

create table #zone_vehicle(
	zone_id int, 
	vehicle_id int
)

if (@operatorId is not null)
	begin
		insert into #vehicles(vehicle_id)
			select vehicle_id 
				from dbo.v_operator_vehicle_right ovr 
				where ovr.operator_id = @operatorID
				  and ovr.right_id = 102

		insert into #zones
			exec dbo.getZone @operatorId, 0, 0
	end
else
	begin
		insert into #vehicles (vehicle_id)
			values (@vehicle_id);

		insert into #zones
		select distinct  
			gz.ZONE_ID,
			gz.[NAME],
			3 as ZONE_TYPE_ID,
			'' as TYPE_NAME,
			gz.COLOR,
			gz.DESCRIPTION
			from dbo.v_operator_zone_right zv
			inner join dbo.v_operator_vehicle_right ov on ov.operator_id = zv.operator_id
			inner join GEO_ZONE gz on gz.zone_id = zv.zone_id
			where ov.vehicle_id = @vehicle_id;
	end

if (@zoneIDList is not null)
begin
	delete #zones where zone_id not in (select number from dbo.split_int(@zoneIDList));
end

/*
insert into #zone_vehicle(zone_id, vehicle_id)
select	z.zone_id,	v.vehicle_id
from dbo.v_zone_vehicle zv
inner join #zones z on zv.zone_id = z.zone_id
inner join #vehicles v on zv.vehicle_id = v.vehicle_id
where v.vehicle_id = @vehicle_id
*/

-- ������� ��������
insert into #MonitoreeLog (Log_Time, X,	Y,	Speed)
select /*top (@maxPosCount)*/
	gl.Log_Time, 
	gl.lng as X, 
	gl.lat as Y, 
	gps.Speed
from geo_log gl (nolock)
join gps_log gps (nolock) on gps.vehicle_id = gl.vehicle_id and gps.log_time = gl.log_time
Where 	
	gl.vehicle_id = @vehicle_id
	and gl.Log_Time between @dtFrom and @dtTo 
	and gl.lng < 180 and gl.lat < 180
order by 
	gl.Log_Time

if((select count(*) from #MonitoreeLog) < 1)
begin
	delete from #MonitoreeLog
	set @vehicle_id = -1
end

--����
insert into @vehicle_position
select @vehicle_id, Log_Time, Y, X 
from #MonitoreeLog

---������ ���
insert into #ZonesList (zone_id, zone_name)
	select gz.zone_id, gz.[Name]
	from GEO_ZONE as gz (nolock) 
	join #zones as zv on gz.ZONE_ID = zv.ZONE_ID

insert into @vehicle_zone
select @vehicle_id, zone_id from #ZonesList

insert into #ZonesForPositions
exec dbo.GetGeoZonesForPositions   @vehicle_zone, @vehicle_position

declare @mlID int, @ml_count int;
set @mlID = 1;
select @ml_count = max(id) from #MonitoreeLog;

declare @pp int, @pp_max int;
declare @group_id int, @group_rows_count int;
set @group_id = 1;
set @group_rows_count = 0;
declare @prev_zone_id int, @prev_mlog_id int;
set @prev_zone_id = null;
declare @speed int, @curr_move_status int, @curr_move_status_group_id int;
declare @prev_move_status int;
declare @curr_log_time int;
set @curr_move_status_group_id = 0;

while @mlID <= @ml_count
BEGIN
	declare @x real, @y real;
	--��������� ��� ���������� � ������� �������� X Y;
	select @speed = speed, @x = dbo.GeoToPlanarX(X), @y = dbo.GeoToPlanarY(Y), @curr_log_time = Log_Time from #MonitoreeLog where id = @mlID;
	if (@x is not null and @y is not null)
	begin 
		--������ ������� ��������
		if (@speed = 0)
			set @curr_move_status = 0;
		else
			begin
				if (@speed < 5)
					set @curr_move_status = 1;
				else
					set @curr_move_status = 2;
			end

		if (@curr_move_status <> isnull(@prev_move_status,-1))
			set @curr_move_status_group_id = @curr_move_status_group_id + 1;

		set @prev_move_status = @curr_move_status;

		--���������� �������������� � ����
		declare @zone_id int;
		set @zone_id = (select top 1 zone_id from #ZonesForPositions where Log_Time = @curr_log_time order by zone_id)

		if (isnull(@zone_id,-1) <> isnull(@prev_zone_id,-1))
			begin
				set @group_id = @group_id + 1;
			end
		
		update #MonitoreeLog
		set zone_id = @zone_id, 
			group_id = @group_id,
			move_status_group_id = @curr_move_status_group_id,
			move_status = @curr_move_status
		where id = @mlID;

		set @prev_zone_id = @zone_id;
	end --if (@geoX is not null and @geoY is not null) ���� ������� �� ���� ������� �� #MonitoreeLog
	set @mlID = @mlID + 1;
END --while @mlID <= @ml_count ���� �� ���� ������ ������� ��� ����������� �������������� � ����


insert into #Result (group_id, zone_name, zone_id, dt_from, dt_to)
select ml.group_id, z.Name, ml.zone_id, min(ml.log_time), max(ml.log_time)
from #MonitoreeLog ml
left join GEO_ZONE z on z.zone_id = ml.zone_id
group by ml.group_id, ml.zone_id, z.Name
order by ml.group_id;

--������� ������� � ����� ������ ��� ���������� ������ 60 ������ ��� ���������� �� �����
update #Result
set distance = dbo.CalculateDistanceFN(dt_from-1, dt_to, @vehicle_id)
where (#Result.dt_to - #Result.dt_from) < 60

--������� �������� ������ �� ���� (����� 60 ������ � 200 ������ ���� ��� ����)
delete #Result
where exists( 
	select * from 
	#Result r_prev
	join #Result r_next on r_next.group_id = r_prev.group_id + 2
	where r_prev.group_id = #Result.group_id - 1
	and r_prev.zone_id = r_next.zone_id
	and #Result.distance < 200 and (#Result.dt_to - #Result.dt_from) < 60
	);

--��������� ��� ���������� ���� ������������� ����� ������������ 
--(������ � ����� ��� ��� ����� ���� �������� ������� �� ���� � ����� ��� �����������)
declare @IdToDel int;
select @IdToDel = min(id)
from #Result r 
where exists (select * from #Result r_prev where r_prev.id = r.id-2)
	and not exists (select * from #Result r_btw where r_btw.id = r.id-1)

while (@IdToDel is not null)
begin
	--��������� ���� ������ � ����������� �����
	update #Result
	set dt_from = r_prev.dt_from, 
	distance = null
	from #Result
	join #Result r_prev on r_prev.group_id = #Result.group_id-2
	where #Result.id = @IdToDel

	--������� �������� ������
	delete #Result
	where id = @IdToDel - 2;

	select @IdToDel = min(id)
	from #Result r 
	where exists (select * from #Result r_prev where r_prev.id = r.id-2)
		and not exists (select * from #Result r_btw where r_btw.id = r.id-1)
end --while (@IdToDel is not null)

--����� ������� ����������
update #Result
set distance = dbo.CalculateDistanceFN(dt_from-1, dt_to, @vehicle_id)
---where distance is null;


---������� ���������� ������ � ����������� ���������� ���������� � �������� �����
-- � ��������� ����������� � �������� ���� ��� ������� ����� ������
update #Result
set avg_distance = #Result.distance + isnull(prev.distance, 0) / 2 + isnull(next.distance ,0) / 2,
zone_id_from = prev.zone_id, zone_id_to = next.zone_id
from #Result
left join #Result prev on (prev.id = (select max(id) from #Result r1 where r1.id < #Result.id) 
							and isnull(prev.zone_id, -1) <> -1)
left join #Result next on (next.id = (select min(id) from #Result r2 where r2.id > #Result.id)
							and isnull(next.zone_id, -1) <> -1)
where #Result.zone_id is null 
;

--������� ����������� ���������� ������
update #Result
set standart_distance = zds.distance
from #Result r
join dbo.ZONE_DISTANCE_STANDART zds on (( zds.ZONE_FROM = r.zone_id_from and zds.ZONE_TO = r.zone_id_to)
										or (zds.ZONE_FROM = r.zone_id_to and zds.ZONE_TO = r.zone_id_from))
where r.avg_distance is not null

--������� ����� ������� ��� ������ ������
;with stops (dt_from, dt_to, move_status_group_id) as
(
	select min(log_time), max(log_time), move_status_group_id
	from #MonitoreeLog ml
	where move_status = 0
	group by move_status_group_id
	having max(log_time) - min(log_time) > 300 ---����� ������� ������ 5 �����
)
update #Result
set stops_time = (select sum(stops.dt_to - stops.dt_from) 
				from stops 
				where stops.dt_to <= #Result.dt_to
				and stops.dt_from >= #Result.dt_from
				)
where zone_id is null

---���������� ������
select ROW_NUMBER() over (order by r.group_id asc) as 'id', 
r.dt_from as 'dtIn',
r.dt_to as 'dtOut',
isnull(r.zone_name, '-') as 'zone',
isnull(r.zone_id, -1) as 'zone_id',
round(r.distance, 0)/1000 as 'Distance',
cast(round(r.avg_distance, 0)/1000 as float) as 'AvgDistance',
r.stops_time as 'StopsTime',
cast(round(r.standart_distance /1000, 3) as float) as 'StandartDistance',    
cast(round(r.avg_distance - standart_distance, 0)/1000 as float) as 'StandartDistanceDiff',
r.zone_id_from, r.zone_id_to, gz_from.[Name] 'zone_name_from', gz_to.[Name] 'zone_name_to'
, Odometer = isnull(
	  (select top(1) Value
		from v_controller_sensor_log csl with (nolock)
		where csl.Vehicle_ID = @vehicle_id
		  and csl.Log_Time between r.dt_from and r.dt_from + 30000
		  and csl.SensorLegend = 8 --CAN.Total_Run
		order by csl.Log_Time asc)
	, (select min(TOTAL_RUN) 
		from CAN_INFO ci (nolock)
		where r.zone_id is not null 
		and ci.vehicle_id = @vehicle_id 
		and ci.log_time between r.dt_from and r.dt_from + 30000
		and ci.TOTAL_RUN > 0))
from #Result r
left join  dbo.GEO_ZONE as gz_from (nolock) on (gz_from.zone_id = r.zone_id_from)
left join  dbo.GEO_ZONE as gz_to (nolock) on (gz_to.zone_id = r.zone_id_to)
order by r.group_id

declare @fuelStandartPer100 float;
set @fuelStandartPer100 = cast(isnull([dbo].[GetRuleValue](@vehicle_id, 10),0) as float)/100;

---���������� ��������� ���-��
select 
@vehicle_id as 'vehicle_id',
round(sum(distance),0)/1000 as 'total_distance',
(select isnull(sum(dt_to - dt_from),0) from #Result where zone_id is not null) as 'total_in_zone_time',
(select isnull(sum(dt_to - dt_from),0) from #Result where zone_id is null) as 'total_out_zone_time',
@fuelStandartPer100 as 'FuelStandartPer100',
sum(isnull(stops_time, 0)) 'total_stops_time',
round(sum(isnull(standart_distance, 0)),0)/1000 'total_standart_distance',
round((sum(isnull(avg_distance - standart_distance, 0))),0)/1000 'total_standart_distance_diff'
from #Result;

drop table #MonitoreeLog;
drop table #ZonesList;
drop table #Primitives;
drop table #PoligonePoints;
drop table #AllPoligonePoints;
drop table #vehicles;
drop table #zones;
drop table #zone_vehicle;
drop table #Result;
