IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEngineSeconds]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetEngineSeconds]
GO

create FUNCTION GetEngineSeconds
(
	@from int,
	@to int,
	@vehicle_id int
)
RETURNS real
AS
BEGIN
	
	declare @value1 bigint;
	declare @value2 bigint;

	--��������� ���������� ������ ������� ��� ���������
	select top 1 @value1 = IgnitionOnTime
		from dbo.Statistic_Log (nolock)
		where  vehicle_id = @vehicle_id and log_time <= @from
		order by log_time desc

	--������ ������ ������� �� ���������
	if @value1 is null
		select top 1 @value1 = IgnitionOnTime
			from dbo.Statistic_Log (nolock)
			where  vehicle_id = @vehicle_id and log_time between @from and @to
			order by log_time asc

	--��������� � ������� ������ �������
	select top 1 @value2 = IgnitionOnTime
		from dbo.Statistic_Log (nolock)
		where  vehicle_id = @vehicle_id and log_time between @from and @to
		order by log_time desc
		
	-- ��. ����������� ���������� � �������� ���������: CalculateDistanceFN
	return (isnull(isnull(@value2,@value1),0) - isnull(@value1,0)) / 3600.0;
END
GO