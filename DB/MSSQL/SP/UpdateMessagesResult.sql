if exists (select * from sys.procedures where name='UpdateMessagesResult')
	drop procedure UpdateMessagesResult
go

create procedure UpdateMessagesResult 
(
	@ids dbo.Id_Param readonly,
	@result int,
	@appId varchar(255) = null
) as

set nocount on

declare @updatedId table (id int)

declare @filteredIds Id_Param

if @appId is not null
begin
	insert into @filteredIds
		select ids.Id
		from @ids ids
		join Message_Contact mc on mc.Type = 1 and mc.Message_ID = ids.Id
		join AppClient ac on ac.Contact_ID = mc.Contact_ID
		where ac.AppId = @appId	
end
else
begin
	insert into @filteredIds
		select ids.Id 
			from @ids ids 
end


update m
	set 
		  ProcessingResult = @result
		, [STATUS] = 3
		, [TIME] = getutcdate()
		, [Sent] = case when [Sent] is null and @result = 3 then GETUTCDATE() else [Sent] end
		, [Delivered] = case when [Delivered] is null and @result in (20,28,25) then GETUTCDATE() else [Delivered] end
	output INSERTED.Message_ID
	into @updatedId
	from @filteredIds ids
	join [MESSAGE] m with (xlock, serializable) on m.MESSAGE_ID = ids.Id 
	where dbo.IsProcessingResultChangeAllowed(m.ProcessingResult, @result) = 1

select Id from @updatedId

go