set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAsid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAsid]
GO

CREATE PROCEDURE [dbo].[GetAsid]
	@uniqueID	int
AS
BEGIN
	if @uniqueID < 1 return -1

	select asid.ID, contact.Value asid
		from dbo.MLP_Controller mlpc
		join dbo.Controller c on c.Controller_ID = mlpc.Controller_ID
		join dbo.Asid asid on asid.ID = mlpc.Asid_ID
		join Contact contact on contact.ID = asid.Contact_ID and contact.Type = 3
		where vehicle_id = @uniqueID	

	return 1
END





