IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'GPRSstat2'
        AND 	  type = 'P')
    DROP PROCEDURE GPRSstat2
GO


-- �--v �-��-�� �-���v ���--��-�� T���-��� L.L.


CREATE   PROCEDURE dbo.GPRSstat2	-- for local time use
	@date	datetime	= null,	-- check date, clear without any shifts
	@ttype	char(1)		= 'L',	-- result time form: 'L' - local, 'U' - UTC
	@operator_id int = null
AS


-------------------------------------------------------------------------
/*
--Test
declare
	@date	datetime,	-- check date, clear without any shifts
	@ttype	char(1)		-- result time form: 'L' - local, 'U' - UTC

set @date = '2008-03-14 21:00:00'
set @ttype = 'L'
*/
-------------------------------------------------------------------------

	-- cut time
	set @date = floor(convert(float, isnull(@date, getdate() - 1)))
	-- shift between local and UTC in secs
	declare @ShiftUTC int
	set @ShiftUTC = datediff(s, getutcdate(), getdate())
	-- time_t utc date
	declare @TimeStart int
	set @TimeStart = datediff(s, dateadd(s, @ShiftUTC, '1970'), @date)
	-- Time allow in seconds between positions
	declare @TimeAllow int
	set @TimeAllow = 61

	--Test
	--Select @date, @ShiftUTC, @TimeStart, dateadd(s, @TimeStart, '1970')


	-- MONITOREE_LOG
	create table #MonLog(
		id			int identity primary key, 
		mid			int, 
		log_time	int, 
		diff		int
		)

	-- fill with
	insert #MonLog(mid, log_time)
	-- work pts
	select 
		MONITOREE_ID, 
		LOG_TIME 
	from MONITOREE_LOG (nolock) 
	left join v_operator_vehicle_right vr on vr.vehicle_id = MONITOREE_LOG.MONITOREE_ID
	where 
		LOG_TIME between @TimeStart and @TimeStart + 3600 * 24 
		and isnull(MEDIA, 1) = 1	--GPRS
		and (@operator_id is null or vr.operator_id = @operator_id)

	union select 
		v.VEHICLE_ID	as MONITOREE_ID, 
		@TimeStart		as LOG_TIME 
	from VEHICLE v (nolock) 
	left join v_operator_vehicle_right vr on vr.vehicle_id = v.VEHICLE_ID
	where (@operator_id is null or vr.operator_id = @operator_id)
	
	union select 
		v.VEHICLE_ID			as MONITOREE_ID, 
		@TimeStart + 3600 * 24	as LOG_TIME
	from VEHICLE v (nolock) 
	left join v_operator_vehicle_right vr on vr.vehicle_id = v.VEHICLE_ID
	where (@operator_id is null or vr.operator_id = @operator_id)
	ORDER by 
		MONITOREE_ID, LOG_TIME

	-- diff LOG_TIME
	update #MonLog set diff = #MonLog.log_time - Pre.log_time from (select * from #MonLog (nolock)) Pre where Pre.id = #MonLog.id - 1 and Pre.mid = #MonLog.mid

	--Test
	--Select #MonLog.*, dateadd(s, LOG_TIME, '1970')  from #MonLog


	-- full res
	create table #r(
		mid					int, 
		[AbsenceFull%]		decimal(18, 2), 
		[AbsencePart%]		decimal(18, 2), 
		maxTime				int, 
		dtMax				smalldatetime
		)
	
	insert #r(mid, [AbsenceFull%], maxTime, dtMax) 
	select 
		MonLog.*, 
		dateadd(s, #MonLog.log_time, /*dateadd(s, @utcshift, */'1970'/*)*/) 
	from 
		(select 
			mid, 
			sum(diff) * 100. / (3600 * 24)	AbsenceFull, 
			max(diff)						maxTime 
		from #MonLog (nolock) 
		where diff > @TimeAllow 
		group by mid
		) MonLog 
		join #MonLog (nolock) on MonLog.mid = #MonLog.mid and MonLog.maxTime = #MonLog.diff

	--Test
	--Select #r.* from #r


	-- intermid res 07:00-21:00
	create table #MonLog1(
		id			int identity primary key, 
		mid			int, 
		log_time	int, 
		diff		int, 
		type		int
	)
	CREATE UNIQUE INDEX IX_MID_LT ON #MonLog1(mid, log_time)

	-- fill with
	insert #MonLog1(mid, log_time, diff, type)
	select 
		mid, 
		log_time, 
		max(diff), 
		min(type)
	from 
		(select 
			mid, 
			log_time, 
			diff, 
			1			type 
		from #MonLog (nolock)		 
		where 
			log_time between @TimeStart + 3600 * 7 and @TimeStart + 3600 * 21
		-- fictious pts 07:00, 21:00
		union select 
			v.VEHICLE_ID			as MONITOREE_ID,  
			@TimeStart + 3600 * 7, 
			null, 
			2 
		from VEHICLE as v (nolock) 
		left join v_operator_vehicle_right vr on vr.vehicle_id = v.VEHICLE_ID
		where (@operator_id is null or vr.operator_id = @operator_id)
	
		union select 
			v.VEHICLE_ID			as MONITOREE_ID, 
			@TimeStart + 3600 * 21, 
			null, 
			2 
		from VEHICLE v (nolock) 
		left join v_operator_vehicle_right vr on vr.vehicle_id = v.VEHICLE_ID
		where (@operator_id is null or vr.operator_id = @operator_id)
	
		) MonLog 
	group by mid, log_time 
	order by mid, log_time

	-- diff LOG_TIME
	update #MonLog1 set diff = #MonLog1.log_time - Pre.log_time from (select * from #MonLog1 (nolock)) Pre where Pre.id = #MonLog1.id - 1 and Pre.mid = #MonLog1.mid 


	update #r set [AbsencePart%] = MonLog1.diff from (
		select mid, sum(diff) * 100. / (3600 * 14) diff from #MonLog1 (nolock) where diff > @TimeAllow group by mid
		) MonLog1	
	where #r.mid = MonLog1.mid

	
	--Result
	select distinct 
		v.VEHICLE_ID			ID, 
		PUBLIC_NUMBER			[No.], 
		GARAGE_NUMBER			[Garage no.], 
		[AbsenceFull%]			[Absence 0-24, %], 
		[AbsencePart%]			[Absence 7-21, %], 
		dbo.TimeToStr(maxTime)	[Max interval], 
		[From] = case @ttype when 'L' then dateadd(s, -maxTime + @ShiftUTC, dtMax) else dateadd(s, -maxTime, dtMax) end, 
		[To] = case @ttype when 'L' then dateadd(s, @ShiftUTC, dtMax) else dtMax end
	from VEHICLE v (nolock) 
		--join VEHICLEGROUP_VEHICLE vgv (nolock) on v.VEHICLE_ID = vgv.VEHICLE_ID 
		inner join #r on v.VEHICLE_ID = mid
		

	drop table #MonLog
	drop table #MonLog1
	drop table #r

GO
