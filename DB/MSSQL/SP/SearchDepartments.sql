set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'SearchDepartments' and type='p'))
	drop procedure dbo.SearchDepartments;
go

--SearchDepartments '79831307397', 187, 1, 1
-- =============================================
-- Description: получение списка департаметов по ключевому слову
-- =============================================
CREATE PROCEDURE [dbo].[SearchDepartments]
	@searchText VARCHAR(500),
	@operatorId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	if (len(@searchText) = 0)
	begin
	select top(25)
		  d.DEPARTMENT_ID
		, d.NAME
		, searchDevices = null
		, countryId = d.Country_ID
		, countryName = c.Name
		, searchDevicesType = 'Empty'
		from v_operator_department_right odr with (nolock)
		join Department d on d.DEPARTMENT_ID = odr.DEPARTMENT_ID
		left outer join Country c ON c.Country_ID = ISNULL(d.Country_ID, 0)
		where odr.right_id = 104
		  and odr.operator_id = @operatorId 
		order by d.Name asc
		return;
	end
	
	declare @searchTemplate varchar(500) = '%' + dbo.EscapeLikeTemplate(@searchText, '\') + '%'

	declare @phoneInvariant varchar(255) = dbo.GetPhoneInvariant(@searchText)
	
	declare @department table (id int primary key clustered)
	
	insert into @department
		select odr.DEPARTMENT_ID
		from v_operator_department_right odr with (nolock)
		where odr.right_id = 104
		  and odr.operator_id = @operatorId 	
	
	;with Search (Department_ID, Value, ValueType)
	as
	(
	select v.DEPARTMENT, v.GARAGE_NUMBER, 'VEHICLE.GARAGE_NUMBER'
		from Vehicle v
		where v.DEPARTMENT is not null
		  and v.GARAGE_NUMBER like @searchTemplate escape '\'		
	union all
	select v.DEPARTMENT, convert(varchar(32), ci.DEVICE_ID), 'CONTROLLER_INFO.DEVICE_ID'
		from Vehicle v
		join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
		join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID	
		where v.DEPARTMENT is not null
		  and ci.Device_ID = convert(varbinary(32), @searchText)
	union all
	select v.Department, pc.Value, 'CONTROLLER.PhoneContactID'
		from Vehicle v
		join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
		join Contact pc on pc.ID = c.PhoneContactID
		where pc.Value = @phoneInvariant
		  and pc.Type = 2
	union all
	select odr.Department_ID, o.Login, 'Operator.Login'
		from v_operator_department_right odr with (nolock)
		join Operator o on o.OPERATOR_ID = odr.operator_id	  
		where odr.right_id = 104
		  and o.login = @searchText
	union all
	select d.Department_ID, null, 'Department.Name'
		from Department d	
		where d.Name like @searchTemplate escape '\'
	union all
	select d.Department_ID, d.ExtID, 'Department.ExtID'
		from Department d	
		where d.ExtID like @searchTemplate escape '\'
		  and d.NAME not like @searchTemplate escape '\'
	)
	select top(25)
		  t.DEPARTMENT_ID
		, NAME = case when d.ExtID is null or d.NAME like '%' + d.ExtID + '%' then d.NAME else d.NAME + ' [' + d.ExtID + ']' end
		, searchDevices = t.Value
		, countryId = d.Country_ID
		, countryName = c.Name
		, searchDevicesType = (select top(1) ts.ValueType from Search ts where ts.Department_ID = t.Department_ID and ts.Value = t.Value)
		from (
			select s.Department_ID, Value = min(s.Value)
				from Search s
				where s.Department_ID in (select adminedDepartment.id from @department adminedDepartment)
				group by s.Department_ID
		) t
		join Department d on d.Department_ID = t.Department_ID
		left outer join Country c ON c.Country_ID = ISNULL(d.Country_ID, 0)
		order by ABS(len(isnull(t.Value, d.name)) - len(@searchText)) asc, d.Name asc

END
