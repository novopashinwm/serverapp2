/****** Object:  StoredProcedure [dbo].[GetZones]    Script Date: 10/14/2008 15:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetZones]
	@vehicle_id int
AS

	select * from dbo.ZONEGROUP
	select * from dbo.GEO_ZONE
	select * from dbo.ZONEGROUP_ZONE
	select * from dbo.DEPARTMENT
	select * from dbo.VEHICLE
	select * from dbo.VEHICLEGROUP
	select * from dbo.VEHICLEGROUP_VEHICLE
	select * from dbo.ZONE_VEHICLE	
	select * from dbo.ZONE_VEHICLEGROUP

--	select zv.* from VEHICLE v 
--		inner join dbo.ZONE_VEHICLE zv on v.VEHICLE_ID=zv.VEHICLE_ID
--	where v.VEHICLE_ID=@vehicle_id


