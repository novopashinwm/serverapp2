if exists (
	select *
		from sys.procedures 
		where name = 'GetOrCreateSimIDByAsidId'
)
	drop procedure GetOrCreateSimIDByAsidId
go

set QUOTED_IDENTIFIER on
go

create procedure GetOrCreateSimIDByAsidId
(
	@asidId int
)
as
begin

	set nocount on

	declare @SimID varchar(32)

	set @SimID = (select SimID from Asid where ID = @asidId)

	if (@SimID is null)
	begin
		update a
			set SimID = replace(convert(varchar(36), NEWID()), '-', '')
			from Asid a with (XLOCK, SERIALIZABLE)
			where ID = @asidId
			  and SimID is null
		
		set @SimID = (select SimID from Asid where ID = @asidId)
	end

	select SimId = @SimID

end