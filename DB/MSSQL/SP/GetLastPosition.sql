if exists (select * from sys.procedures where name = 'GetLastPosition')
	DROP PROCEDURE [dbo].[GetLastPosition]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE    PROCEDURE [dbo].[GetLastPosition] --[GetLastPosition] 442
	@vehicle_id int
AS

set nocount on

	select * from v_vehicle_last_position where Monitoree_ID = @vehicle_id

GO
