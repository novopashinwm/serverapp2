if exists (
	select * from sys.objects where name = 'describeMessageProcessingResult'
)
	drop function describeMessageProcessingResult;
go

create function describeMessageProcessingResult (@processingResult int) returns varchar(255)
as
begin
	return 
						case @processingResult   
							  when 0 then 'Unknown'  
							  when 1 then 'Received'  
							  when 2 then 'Processing'  
							  when 3 then 'Sent'  
							  when 4 then 'ContactDoesNotExist'  
							  when 5 then 'UnsuccessfulBillingResult'  
							  when 6 then 'Throttled'  
							  when 7 then 'MessageQueueFull'  
							  when 8 then 'InvalidMessageLength'  
							  when 9 then 'InvalidDestinationAddress'  
							  when 10 then 'NoConnectionWithGateway'  
							  when 11 then 'HttpError'   
							  when 12 then 'Timeout'      
							  when 13 then 'ServiceTurnedOff'  
							  when 14 then 'Error'     
							  when 15 then 'DestinationIsNotSpecified'  
							  when 16 then 'Queued'  
							  when 17 then 'DestinationContactIsLocked'
							  when 19 then 'DestinationContactIsNotValid'
							  when 20 then 'Delivered'
							  when 21 then 'Undeliverable'
							  when 22 then 'Deleted'
							  when 23 then 'Rejected'
							  when 24 then 'Delivered'
							  when 25 then 'Read'
							  when 26 then 'CancelledByCommand'
							  when 27 then 'MissingAppId'
							  when 28 then 'MismatchAppId'
							  when 29 then 'NoApproriateProcessor'
							  when 30 then 'UnsupportedTemplate'
							  else CONVERT(varchar, @processingResult)  
							 end  
end