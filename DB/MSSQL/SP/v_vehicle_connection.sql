﻿IF (OBJECT_ID(N'[dbo].[v_vehicle_connection]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[v_vehicle_connection]
GO

CREATE VIEW [dbo].[v_vehicle_connection]
AS
	SELECT
		 ipl.[Vehicle_ID]
		,ipl.[LastTime]
		,ipl.[IP]
		,ipl.[Protocol]
		,mas.[Name]
	FROM [dbo].[IP_Log] ipl
		OUTER APPLY
		(
			SELECT TOP(1)
				lgt.[Media]
			FROM [dbo].[Log_Time#Lasts] lst WITH (NOLOCK)
				OUTER APPLY
				(
					SELECT TOP(1)
						[Media]
					FROM [dbo].[Log_Time] WITH (NOLOCK)
					WHERE [Vehicle_ID] = lst.[Vehicle_ID]
					AND   [InsertTime] = lst.[InsertTime#Last]
				) lgt
			WHERE lst.[Vehicle_ID] = ipl.[Vehicle_ID]
		) lgt
			LEFT JOIN [dbo].[Media_Acceptors] mas
				ON mas.[MA_ID] = lgt.[Media]
GO