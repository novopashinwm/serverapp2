IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParseLogTimeSequence]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParseLogTimeSequence]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[ParseLogTimeSequence]
	@logTimeList	varchar(max)
as
begin
	/*	������ ������:
		exec dbo.[ParseLogTimeSequence] '10;20;30;40'	
	*/

	set nocount on;

	declare @logTime table(rn int identity primary key clustered, value int);

	insert into @logTime (value)
		select distinct number 
			from dbo.split_int(@logTimeList)
			order by number

	select lt.value [From], lt_next.value [To]
		from @logTime lt
		join @logTime lt_next on lt_next.rn = lt.rn+1
		order by lt.rn
	
end;