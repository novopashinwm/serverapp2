if not exists (
	select * from sys.objects where name = 'GetLatCell' and type = 'FN'
)
begin

exec sp_executesql N'
	alter function dbo.GetLatCell
	(
		@lat float
	) returns int with schemabinding
	as
	begin
		declare @cellSizeInMeters float = 15
		declare @latStep float = 180/((40000000/2)/@cellSizeInMeters)
		set @lat = case	
			when @lat < 0 then @lat+180*(round(-@lat/180,0)+1)
			when @lat >= 180 then @lat-180
			else @lat
		end
		declare @latCell int = round(@lat/@latStep,0)
		return @latCell
	end
'

end