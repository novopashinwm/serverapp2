set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetRulesForProcessing' and type='p'))
	drop procedure dbo.GetRulesForProcessing;
go
