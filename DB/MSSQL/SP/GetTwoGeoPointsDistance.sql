IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTwoGeoPointsDistance]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetTwoGeoPointsDistance]
GO

CREATE FUNCTION dbo.GetTwoGeoPointsDistance
(
	@x1 real, -- (�������1, �������)
	@y1 real, -- (������1, �������)
	@x2 real, -- (�������2, �������)
	@y2 real  -- (������2, �������)
)
RETURNS float(53) -- (����������, �����)
AS
BEGIN
	if (@x1 = @x2 and @y1 = @y2)
		return 0 --���������� �� ����� 37.60822, 55.80262 �� �� �� ������ ��������� 95�� ��-�� ��������������� ������

	declare @Lat1Radians float(53);
	declare @Lon1Radians float(53);
	declare @Lat2Radians float(53);
	declare @Lon2Radians float(53);

	set @Lat1Radians = @y1 * PI() / 180
	set @Lon1Radians = @x1 * PI() / 180
	set @Lat2Radians = @y2 * PI() / 180
	set @Lon2Radians = @x2 * PI() / 180

	declare @a float(53);
	set @a = @Lon1Radians - @Lon2Radians;

	if (@a < 0.0) set @a = -@a

	if (@a > PI()) set @a = 2.0 * PI() - @a;

	declare @cos float(53)

	set @cos = 
		Sin(@Lat2Radians) * Sin(@Lat1Radians) +
        Cos(@Lat2Radians) * Cos(@Lat1Radians) * Cos(@a)

	--� �������� ���������� ����� ������������� ������, ��� ��� @cos ������� �� ������� [-1;1] �� ��������� �����
	declare @acos float(53)
	set @acos = case when @cos >= 1  then 0
					 when @cos <= -1 then PI()
					 else Acos(@cos)
				end

    return 40075160 * @acos / (2.0 * PI())
		
END
GO