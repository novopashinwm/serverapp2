if exists (
	select * from sys.procedures where name = 'CopyLog'
)
	drop procedure CopyLog
go

/*���� ����� ���� ����� ������� ��������� ��, ��������� ������ ����������� �� �������-��������� ������.
  ��������� �� ��������� ������ � ��������� �� ���� (vehicle_id, log_time)*/
create procedure CopyLog
(
	@vehicle_id_from	int,
	@vehicle_id_to		int,
	@timeFromMain		int,
	@timeToMain			int,
	@sourceDatabase		varchar(255) = null,
	@targetDatabase		varchar(255) = null,
	@debug bit = 0
)
as

if @debug is null	
	set @debug = 0

if (@debug = 0)
	set nocount on

if (@sourceDatabase is null)
	set @sourceDatabase = 'dbo.'
else
	set @sourceDatabase = @sourceDatabase + '.dbo.'

if (@targetDatabase is null)
	set @targetDatabase = 'dbo.'
else
	set @targetDatabase = @targetDatabase + '.dbo.'	

if @sourceDatabase = @targetDatabase and @vehicle_id_from = @vehicle_id_to
	return;
	
declare @sql nvarchar(max), @sqlprev nvarchar(max);		

set @sql = '';
--���������� � ������� ������������ ������.

set @sqlprev = '

delete from ' + @targetDataBase + 'log_time 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo

delete from ' + @targetDataBase + 'geo_log 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo
	
delete from ' + @targetDataBase + 'Position_Radius 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo
	
delete from ' + @targetDataBase + 'gps_log 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo

delete from ' + @targetDataBase + 'CAN_Info 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo

delete from ' + @targetDataBase + 'controller_sensor_log 
	where vehicle_id = @vehicleIDTo
	  and log_time between @timeFrom and @timeTo
';

-- ������� ������.
set @sql = @sql + '
insert into ' + @targetDataBase + 'log_time (vehicle_id, log_time, media, inserttime)
	select @vehicleIDTo, lt.log_time, lt.media, lt.InsertTime
	from ' + @sourceDataBase + 'log_time lt (nolock)
	where lt.vehicle_id = @vehicleID
	and 
		lt.log_time between @timeFrom
					   and  @timeTo
	and not exists (select 1 from ' + @targetDataBase + 'log_time lt_existing (nolock) where lt_existing.vehicle_id = @vehicleIDTo
																                     and lt_existing.log_time   = lt.log_time)

insert into ' + @targetDataBase + 'geo_log (Vehicle_ID, LOG_TIME, Lng, Lat)
	select lt.Vehicle_ID, gl.log_time, gl.lng, gl.lat
		from ' + @targetDataBase + 'Log_Time lt (nolock) 
		join ' + @sourceDataBase + 'geo_log gl on gl.vehicle_id = @vehicleID 
												  and gl.log_time = lt.log_time
		where lt.vehicle_id = @vehicleIDTo
		and 
			lt.log_time between @timeFrom
						   and  @timeTo
		and not exists (select 1 from ' + @targetDataBase + 'geo_log geo_log_existing  (nolock) where geo_log_existing.vehicle_id = lt.vehicle_id and geo_log_existing.log_time = lt.log_time)
		
insert into ' + @targetDataBase + 'Position_Radius (Vehicle_ID, LOG_TIME, Radius)
	select lt.Vehicle_ID, pr.log_time, pr.Radius
		from ' + @targetDataBase + 'Log_Time lt (nolock) 
		join ' + @sourceDataBase + 'Position_Radius pr on pr.vehicle_id = @vehicleID 
												     and pr.log_time = lt.log_time
		where lt.vehicle_id = @vehicleIDTo
		and 
			lt.log_time between @timeFrom
						   and  @timeTo
		and not exists (select 1 from ' + @targetDataBase + 'Position_Radius pr_existing  (nolock) where pr_existing.vehicle_id = lt.vehicle_id and pr_existing.log_time = lt.log_time)

insert into ' + @targetDataBase + 'gps_log (Vehicle_ID,Log_Time,Satellites,Firmware,Altitude,Speed)
	select lt.Vehicle_ID, lt.Log_Time, gpsl.Satellites, gpsl.Firmware, gpsl.Altitude, gpsl.Speed
		from ' + @targetDataBase + 'Log_Time lt (nolock) 
		join ' + @sourceDataBase + 'gps_log gpsl  (nolock) on gpsl.vehicle_id = @vehicleID
												    and gpsl.log_time = lt.log_time
		where lt.vehicle_id = @vehicleIDTo
		and 
			lt.log_time between @timeFrom
						   and  @timeTo
		and not exists (select 1 from ' + @targetDataBase + 'gps_log gps_log_existing  (nolock) where gps_log_existing.vehicle_id = lt.vehicle_id and gps_log_existing.log_time = lt.log_time)

insert into ' + @targetDataBase + 'CAN_Info (Vehicle_ID, LOG_TIME, CAN_TIME, SPEED, CRUISE_CONTROL, BRAKE, PARKING_BRAKE, CLUTCH, ACCELERATOR, FUEL_RATE, FUEL_LEVEL1, FUEL_LEVEL2, FUEL_LEVEL3, FUEL_LEVEL4, FUEL_LEVEL5, FUEL_LEVEL6, REVS, RUN_TO_MNTNC, ENG_HOURS, COOLANT_T, ENG_OIL_T, FUEL_T, TOTAL_RUN, DAY_RUN, AXLE_LOAD1, AXLE_LOAD2, AXLE_LOAD3, AXLE_LOAD4, AXLE_LOAD5, AXLE_LOAD6)
	select lt.Vehicle_ID, lt.LOG_TIME, ci.CAN_TIME, ci.SPEED, ci.CRUISE_CONTROL, ci.BRAKE, ci.PARKING_BRAKE, ci.CLUTCH, ci.ACCELERATOR, ci.FUEL_RATE, ci.FUEL_LEVEL1, ci.FUEL_LEVEL2, ci.FUEL_LEVEL3, ci.FUEL_LEVEL4, ci.FUEL_LEVEL5, ci.FUEL_LEVEL6, ci.REVS, ci.RUN_TO_MNTNC, ci.ENG_HOURS, ci.COOLANT_T, ci.ENG_OIL_T, ci.FUEL_T, ci.TOTAL_RUN, ci.DAY_RUN, ci.AXLE_LOAD1, ci.AXLE_LOAD2, ci.AXLE_LOAD3, ci.AXLE_LOAD4, ci.AXLE_LOAD5, ci.AXLE_LOAD6
		from ' + @targetDataBase + 'Log_Time lt (nolock) 
		join ' + @sourceDataBase + 'CAN_Info ci  (nolock)   on ci.vehicle_id = @vehicleID
												      and ci.log_time = lt.log_time		
		where lt.vehicle_id = @vehicleIDTo
		and 
			lt.log_time between @timeFrom
						   and  @timeTo
		and not exists (select 1 from ' + @targetDataBase + 'CAN_Info CAN_Info_existing  (nolock) where CAN_Info_existing.vehicle_id = lt.vehicle_id and CAN_Info_existing.log_time = lt.log_time)


insert into ' + @targetDataBase + 'Controller_Sensor_Log (Vehicle_ID, LOG_TIME, Number, Value)
	select lt.Vehicle_ID, lt.LOG_TIME, ci.Number, ci.Value
		from ' + @targetDataBase + 'Log_Time lt (nolock) 
		join ' + @sourceDataBase + 'Controller_Sensor_Log ci  (nolock)   on ci.vehicle_id = @vehicleID
												      and ci.log_time = lt.log_time		
		where lt.vehicle_id = @vehicleIDTo
		and 
			lt.log_time between @timeFrom
						   and  @timeTo
		and not exists (select 1 from ' + @targetDataBase + 'Controller_Sensor_Log cie  (nolock) where cie.vehicle_id = lt.vehicle_id and cie.log_time = lt.log_time)
		
';

--[debug]
select garage_number, getDate() from vehicle where vehicle_id = @vehicle_id_to
--[/debug]

declare @partTimeFrom int, @partTimeTo int;
set @partTimeFrom = @timeFromMain;

while (@partTimeFrom < @timeToMain)
begin
	set @partTimeTo = @partTimeFrom + 24*3600;
	if @timeToMain < @partTimeTo 
		set @partTimeTo = @timeToMain;

	print 'delete on ' + convert(varchar(20), dbo.GetDateFromInt(@partTimeTo+4*3600))
	exec sp_executesql @sqlprev, N'@vehicleID int, @vehicleIDTo int, @timeFrom int, @timeTo int',
		@vehicleID = @vehicle_id_from,
		@vehicleIDTo = @vehicle_id_to,
		@timeFrom = @partTimeFrom, 
		@timeTo = @partTimeTo

	print 'iinsert on ' + convert(varchar(20), dbo.GetDateFromInt(@partTimeTo+4*3600))
	exec sp_executesql @sql, N'@vehicleID int, @vehicleIDTo int, @timeFrom int, @timeTo int', 
		@vehicleID = @vehicle_id_from, 
		@vehicleIDTo = @vehicle_id_to,
		@timeFrom = @partTimeFrom, 
		@timeTo = @partTimeTo;	
	
	set @partTimeFrom = @partTimeTo + 1
end;	
	
set @sql = '
	exec dbo.AddStatisticLog @vehicleIDTo, @timeFrom;
	exec dbo.RecalcStatisticLog @vehicleIDTo, 31536000 --�� ���
';

if @debug = 1
	select garage_number, getDate() from vehicle where vehicle_id = @vehicle_id_to

exec sp_executesql @sql, N'@vehicleIDTo int, @timeFrom int', 
	@vehicleIDTo = @vehicle_id_to,
	@timeFrom = @timeFromMain;

