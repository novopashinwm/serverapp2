CreateProcedure 'DropTable'
go

alter procedure DropTable
(
	@name nvarchar(255)
)
as

declare @sql nvarchar(max)

if exists (select * from sys.tables where name = @name)
begin
	print 'Deleting ' + @name + '...'

	set @sql = 'drop table [' + @name + ']'
	exec sp_executesql @sql
end

set @name = 'H_' + @name

if exists (select * from sys.tables where name = @name)
begin
	print 'Deleting ' + @name + '...'

	set @sql = 'drop table [' + @name + ']'
	exec sp_executesql @sql
end
