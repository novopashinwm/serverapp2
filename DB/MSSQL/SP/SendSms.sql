if exists (
	select *
		from sys.procedures
		where name = 'SendSms'
)
	drop procedure SendSms;	
go

--SendSms 'hello1', '79852579565', '79267315495'
create procedure SendSms
(
	@text nvarchar (255),
	@destination_phone varchar(32),
	@source_phone varchar(32) = null
)
as

declare @contact table (destination_id int, source_id int)

insert into @contact (destination_id)
	exec dbo.GetContactID 2, @destination_phone
	
if (@source_phone is not null)
begin
	insert into @contact(source_id)
		exec dbo.GetContactID 2, @source_phone
end

begin tran

insert into MESSAGE (Status, Time, BODY, TEMPLATE_ID)
	select 1, getutcdate(), 
		@text,
		Template_ID = (select mt.Message_Template_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'GenericNotification')

declare @message_id int 
set @message_id = @@identity

insert into Message_Contact 
	select @message_id, 1, c.destination_id
		from @contact c
		where c.destination_id is not null
	union all
	select @message_id, 5, c.source_id
		from @contact c	
		where c.source_id is not null

commit