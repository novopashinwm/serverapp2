
/****** Object:  StoredProcedure [dbo].[GetDataForGeoTrip]    Script Date: 12/11/2008 15:27:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetDataForGeoTrip]
@operatorId int = null
AS

if (@operatorID is null)
begin
	--ROUTE - ╣иш-шжъv
	select
		R.*,
		S.ROUTE_ID as S
	from [ROUTE] R
		left join (select distinct ROUTE_ID from dbo.SCHEDULE) S on S.ROUTE_ID = R.ROUTE_ID
	order by
		case
			when isnumeric(RTRIM(R.EXT_NUMBER))=1
				then convert( numeric(9), RTRIM(R.EXT_NUMBER))
			when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
				then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
			else 
				999999
		end
		,R.EXT_NUMBER

	--ROUTE_TRIP - ╣иш-шжъv ░ ви╖ш┐аv┐ддv┐ ви д░╣░ ║┐жш┐Хмv
	select 
		RTR.ROUTE_TRIP_ID, 
		RTR.ROUTE_ID, 
		GT.GEO_TRIP_ID,
		GT.COMMENTS
	from 
		ROUTE_TRIP as RTR
		inner join GEO_TRIP as GT on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
		
	--GEO_TRIP - км┐ ║┐жш┐Хмv	
	select * from GEO_TRIP

	--GEO_SEGMENT - ║┐жм┐║╣┐дъv 
	select 
		GES.*
	from 
		GEO_SEGMENT as GES
		inner join GEO_TRIP as GT on GES.GEO_TRIP_ID = GT.GEO_TRIP_ID
		inner join POINT as P on GES.POINT = P.POINT_ID
	Order by 
		GES.GEO_TRIP_ID, GES.[ORDER]

	-- POINT - км┐ жмъиджк╖░
	select 
		P.*
	from 
		POINT as P

	--GEO_SEGMENT_POINT - ║┐жм┐║╣┐дъv м жмъиджк╖и╣░
	select 
		GES.GEO_TRIP_ID,
		GES.[ORDER],
		GES.GEO_SEGMENT_ID,
		P.POINT_ID,
		P.RADIUS,
		P.[NAME]
	from 
		GEO_SEGMENT as GES
		inner join GEO_TRIP as GT on GES.GEO_TRIP_ID = GT.GEO_TRIP_ID
		inner join POINT as P on GES.POINT = P.POINT_ID
	Order by 
		GES.GEO_TRIP_ID, GES.[ORDER]

	--UNSIGNPOINT - км┐ д┐ аш░крвиддv┐ жмъиджк╖░
	select 
		P.POINT_ID,
		P.[NAME],
		VX.X,
		VX.Y
	from 
		POINT as P
		left join MAP_VERTEX as VX on P.VERTEX_ID = VX.VERTEX_ID
		left join GEO_SEGMENT as GES on P.POINT_ID = GES.Point
	where 
		GES.Point is null

	--GEO_SEGMENT_VERTEX - мкжпдир ъи╕v░си ║┐жш┐Хмжк ░ ъжт┐╖ 
	select * 
	from 
		GEO_SEGMENT_VERTEX as GV
	order by
		GV.GEO_TRIP_ID, ORDER_GT --, GV.GEO_SEGMENT_ID, ORDER_GS, 

	--MAP_VERTEX - км┐ ъжт╖░ 
	select * from MAP_VERTEX

	--MAPS - ╖ишъv
	select * from MAPS

	-- BUSSTOP
	select * from BUSSTOP
end
else
begin
	--Если указан оператор
	
	--ROUTE - ╣иш-шжъv
	select distinct	R.*,
		S.ROUTE_ID as S, 
		case
			when isnumeric(RTRIM(R.EXT_NUMBER))=1
				then convert( numeric(9), RTRIM(R.EXT_NUMBER))
			when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
				then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
			else 
				999999
		end as 'orderBYNum'
	from [ROUTE] R
		join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
		left join (select distinct ROUTE_ID from dbo.SCHEDULE) S on S.ROUTE_ID = R.ROUTE_ID
	where rr.OPERATOR_ID = @operatorId
	order by
		case
			when isnumeric(RTRIM(R.EXT_NUMBER))=1
				then convert( numeric(9), RTRIM(R.EXT_NUMBER))
			when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
				then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
			else 
				999999
		end
		,R.EXT_NUMBER

	--ROUTE_TRIP - ╣иш-шжъv ░ ви╖ш┐аv┐ддv┐ ви д░╣░ ║┐жш┐Хмv
	select distinct
		RTR.ROUTE_TRIP_ID, 
		RTR.ROUTE_ID, 
		GT.GEO_TRIP_ID,
		GT.COMMENTS
	from 
		ROUTE_TRIP as RTR
		inner join GEO_TRIP as GT on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
		join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
		where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
	
	--GEO_TRIP - км┐ ║┐жш┐Хмv	
	select distinct gt.* from GEO_TRIP gt
	left join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
		where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)

	--GEO_SEGMENT - ║┐жм┐║╣┐дъv 
	select distinct
		GES.*
	from 
		GEO_SEGMENT as GES
		inner join GEO_TRIP as GT on GES.GEO_TRIP_ID = GT.GEO_TRIP_ID
		inner join POINT as P on GES.POINT = P.POINT_ID
		left join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
		left join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
		where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
	Order by 
		GES.GEO_TRIP_ID, GES.[ORDER]

	-- POINT - км┐ жмъиджк╖░
	select distinct p.* 
	from point p 
	join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
	join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operatorID
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)

	--GEO_SEGMENT_POINT - ║┐жм┐║╣┐дъv м жмъиджк╖и╣░
	select distinct
		GES.GEO_TRIP_ID,
		GES.[ORDER],
		GES.GEO_SEGMENT_ID,
		P.POINT_ID,
		P.RADIUS,
		P.[NAME]
	from 
		GEO_SEGMENT as GES
		inner join GEO_TRIP as GT on GES.GEO_TRIP_ID = GT.GEO_TRIP_ID
		inner join POINT as P on GES.POINT = P.POINT_ID
		left join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
		left join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
		where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
	Order by 
		GES.GEO_TRIP_ID, GES.[ORDER]

	--UNSIGNPOINT - км┐ д┐ аш░крвиддv┐ жмъиджк╖░
	select distinct
		P.POINT_ID,
		P.[NAME],
		VX.X,
		VX.Y
	from 
		POINT as P
		left join MAP_VERTEX as VX on P.VERTEX_ID = VX.VERTEX_ID
		left join GEO_SEGMENT as GES on (ges.POINT = p.POINT_ID or ges.POINTTO = p.POINT_ID)
	where 
		GES.Point is null and 2=1

	--GEO_SEGMENT_VERTEX - мкжпдир ъи╕v░си ║┐жш┐Хмжк ░ ъжт┐╖ 
	select distinct GV.* 
	from 
		GEO_SEGMENT_VERTEX as GV
		join GEO_SEGMENT gs on gs.GEO_SEGMENT_ID = GV.GEO_SEGMENT_ID
		join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
		left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
		left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
	order by GV.GEO_TRIP_ID, gv.ORDER_GT --, GV.GEO_SEGMENT_ID, ORDER_GS, 

	--MAP_VERTEX - км┐ ъжт╖░ 
	select distinct 
		 MV.VERTEX_ID
		,MV.MAP_ID
		,MV.X
		,MV.Y
		,MV.VISIBLE
		,MV.ENABLE	
	from MAP_VERTEX MV
	join GEO_SEGMENT_VERTEX as GV on GV.VERTEX_ID = MV.VERTEX_ID
	join GEO_SEGMENT gs on gs.GEO_SEGMENT_ID = GV.GEO_SEGMENT_ID
	join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
		
		
	--MAPS - ╖ишъv
	select * from MAPS

	-- BUSSTOP
	select distinct b.* 
	from busstop b
	join v_operator_department_right dr on dr.DEPARTMENT_ID = b.DEPARTMENT_ID
	where dr.operator_id= @operatorID 
	and dr.right_id=104
end



