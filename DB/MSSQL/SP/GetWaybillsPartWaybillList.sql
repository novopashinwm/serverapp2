/****** Object:  StoredProcedure [dbo].[GetWaybillsPartWaybillList]    Script Date: 07/11/2010 13:39:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[GetWaybillsPartWaybillList]
	@whd		datetime,
	@delta_seconds	int,
	@operator_id int = null
AS
	-- °¤кїиЄЁ√ Єиї№ї¤°
	declare @actdt_end datetime
	set @actdt_end = getutcdate()
	declare @actdt_begin datetime
	set @actdt_begin = dateadd(second, -@delta_seconds, @actdt_end)

	-- ¤Ё░Ё ЇЁкЁ Є ёїў■к¤■╕°кї√╣¤■№ Є°Її
	declare @dt datetime
	set @dt = dateadd(hh, 24-datepart(hh, @whd), @whd)

--========================================================================
-- ¤ЁвЁ√■ ё√■·Ё:  ║кїЄvї √°╕кv
--========================================================================
	-- °Її¤к°п°·Ёк■иv ¤ї■ё┐■Ї°№v┐  ║кїЄv┐ √°╕к■Є
	select distinct waybill_header_id
	into #whids
	from h_waybill_header
	left join v_operator_vehicle_right vr on vr.vehicle_id = h_waybill_header.VEHICLE_ID
	where WAYBILL_DATE between dateadd(hour, -1, @whd) and dateadd(hour, 1, @whd)
		and actual_time between @actdt_begin and @actdt_end
		and [action] <> 'DELETE'
		and (@operator_id is null or vr.operator_id = @operator_id)

	-- °Її¤к°п°·Ёк■иv ¤ї■ё┐■Ї°№v┐  ║кїЄv┐ √°╕к■Є  ■ иї∙╕Ё№
	if @delta_seconds < 86400
		insert into #whids
		select distinct wt.waybill_header_id
		from wb_trip wt
			join waybill_header wh on wt.waybill_header_id = wh.waybill_header_id
			left join v_operator_vehicle_right vr on vr.vehicle_id = wh.VEHICLE_ID
		where wh.WAYBILL_DATE between dateadd(hour, -1, @whd) and dateadd(hour, 1, @whd)
			and wt.actual_time between @actdt_begin and @actdt_end
			and (@operator_id is null or vr.operator_id = @operator_id)

	/* H_WAYBILL_HEADER */
	-- °Її¤к°п°·Ёк■иv ║ЇЁ√м¤¤v┐  ║кїЄv┐ √°╕к■Є
	select distinct waybill_header_id
	from h_waybill_header
	left join v_operator_vehicle_right vr on vr.vehicle_id = h_waybill_header.VEHICLE_ID
	where WAYBILL_DATE between dateadd(hour, -1, @whd) and dateadd(hour, 1, @whd)
		and actual_time between @actdt_begin and @actdt_end
		and [action] = 'DELETE'
		and (@operator_id is null or vr.operator_id = @operator_id)

	-- ¤ї■ё┐■Ї°№vї  ║кїЄvї √°╕кv
	select *
	into #whs
	from WAYBILL_HEADER 
	where WAYBILL_HEADER_ID in (select * from #whids)

	-- °Її¤к°п°·Ёк■иv ¤ї■ё┐■Ї°№v┐ ╕№ї¤
	select distinct waybill_id
	into #wids
	from h_waybill
	left join v_operator_driver_right dr on dr.driver_id = H_WAYBILL.DRIVER_ID
	where WAYBILL_DATE = @dt
		and actual_time between @actdt_begin and @actdt_end
		and [action] <> 'DELETE'
		and (@operator_id is null or dr.operator_id = @operator_id)

	-- °Її¤к°п°·Ёк■иv ¤ї■ё┐■Ї°№v┐ ╕№ї¤  ■ иї∙╕Ё№
	if @delta_seconds < 86400
		insert into #wids
		select distinct wt.waybill_id
		from wb_trip wt
		join waybill w on wt.waybill_id = w.waybill_id
		left join v_operator_driver_right dr on dr.driver_id = w.DRIVER_ID		
		where w.WAYBILL_DATE = @dt
			and wt.actual_time between @actdt_begin and @actdt_end
			and (@operator_id is null or dr.operator_id = @operator_id)

	/* H_WAYBILL */
	-- °Її¤к°п°·Ёк■иv ║ЇЁ√м¤¤v┐ ╕№ї¤
	select distinct waybill_id
	from h_waybill
	left join v_operator_driver_right dr on dr.driver_id = H_WAYBILL.DRIVER_ID
	where WAYBILL_DATE = @dt
		and actual_time between @actdt_begin and @actdt_end
		and [action] = 'DELETE'
		and (@operator_id is null or dr.operator_id = @operator_id)

	-- ¤ї■ё┐■Ї°№vї ╕№ї¤v
	select distinct wb.*
	into #ws
	from WAYBILL wb
	left join v_operator_driver_right dr on dr.driver_id = wb.DRIVER_ID		
	where wb.WAYBILL_ID in (select * from #wids)
	and (@operator_id is null or dr.operator_id = @operator_id)

	-- Є╕ї ¤ї■ё┐■Ї°№vї иї∙╕v
	select *
	into #wts
	from wb_trip
	where waybill_header_id in (select * from #whids)

	set IDENTITY_INSERT #wts ON

	insert into #wts
	(WB_TRIP_ID, TRIP, TRIP_ID, STATUS, BEGIN_TIME, END_TIME, COMMENT, WAYBILL_ID, TRIP_KIND_ID, WAYBILL_HEADER_ID, OPERATOR_ID, ACTUAL_TIME, WORKTIME_REASON_ID, GEO_TRIP_ID, PENALTY, ORDER_TRIP_ID)
	select distinct wt.*
	from wb_trip wt
	left join TRIP t on t.TRIP_ID = wt.TRIP_ID
	left join ROUTE_TRIP rt on rt.ROUTE_TRIP_ID = t.ROUTE_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where waybill_id in (select * from #wids) 
	and waybill_header_id is null
	/*and (waybill_header_id not in (select * from #whids) 
		or waybill_header_id is null
		)*/
	and (
		wt.TRIP_ID is null
		or @operator_id is null
		or rr.operator_id = @operator_id
		)

	drop table #wids

	/* WAYBILL_HEADER */
	select * 
	from #whs

	drop table #whs

	/* WAYBILL */
	select * 
	from #ws

	drop table #ws

	/* WB_TRIP */
	select *
	from #wts

	/* GEO_TRIP */
	select *
	from geo_trip
	where geo_trip_id in (select distinct geo_trip_id from #wts)

	drop table #wts

	/* WAYBILLHEADER_WAYBILLMARK */
	select * 
	from WAYBILLHEADER_WAYBILLMARK 
	where WAYBILLHEADER_ID in (select * from #whids)

	drop table #whids

	/* DAY */
	select *
	from [DAY]
	where [DATE] = @dt
--========================================================================
-- ·■¤їб ё√■·Ё:  ║кїЄvї √°╕кv
--========================================================================

