set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetLastLogTimes' and type='p'))
	drop procedure dbo.GetLastLogTimes;
go

create procedure dbo.GetLastLogTimes
	@vehicle_ids varchar(max),
	@time datetime = null,
	@debug bit = NULL
as
begin

	if isnull(@debug, 0) = 0
		set nocount on;

	declare @logTime int = datediff(ss, '1970', isnull(@time, getutcdate()))

	create table #vehicle (Vehicle_ID int primary key clustered);
	insert into #vehicle 
		select number
			from dbo.split_int(@vehicle_ids)

	--OUTPUT: last log times
	select 
		v.Vehicle_ID,
		last_lt.Log_Time
		from #vehicle v
		cross apply (
			select top(1) lt.Log_Time
			from dbo.Log_Time lt with (nolock)
			where 
				lt.Vehicle_ID = v.Vehicle_ID
			and lt.Log_Time <= @logTime
			order by lt.Log_Time desc) last_lt
			
	drop table #vehicle

end