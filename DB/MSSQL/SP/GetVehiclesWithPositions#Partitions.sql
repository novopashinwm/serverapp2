﻿IF (OBJECT_ID(N'[dbo].[GetVehiclesWithPositions#Partitions]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetVehiclesWithPositions#Partitions];
GO

/*
Sample:
DECLARE @vid dbo.Id_Param
INSERT into @vid select 80736
EXEC dbo.GetVehiclesWithPositions#Partitions @operatorID=28549, @vehicleIDs=@vid, @departmentId=null, @includeLastData=1, @includeAttributes=1
GO 10
*/
CREATE PROCEDURE [dbo].[GetVehiclesWithPositions#Partitions]
(
	@operatorID        int = NULL,
	@vehicleIDs        dbo.Id_Param readonly,
	@departmentId      int = NULL,
	@includeLastData   bit = NULL,
	@includeAttributes bit = NULL,
	@includeZone       bit = NULL,
	@currentLogTime    int = NULL,
	@debug             bit = NULL
)
AS
BEGIN
	IF (ISNULL(@debug, 0) = 0)
		SET NOCOUNT ON;
		
	IF (@includeAttributes IS NULL)
		SET @includeAttributes = 1

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @vid TABLE([value] int PRIMARY KEY CLUSTERED);

	IF (@debug = 1)
		PRINT '@vid fill'

	if @operatorID is null
	begin
		if (not exists (select * from @vid))
			insert into @vid(value)
				select Vehicle_ID
					from dbo.Vehicle v
					where (@departmentId is null or v.department = @departmentId)
		else
			insert into @vid (value)
				select distinct Id 
					from @vehicleIDs vid
					join vehicle v on v.vehicle_id = vid.Id
					where (@departmentId is null or v.department = @departmentId)				
	end
	else
	begin
		if (not exists (select * from @vehicleIDs))
		begin
			if (@debug = 1)
				print '@vid fill by operator and department'
			IF (@currentLogTime IS NULL)
			BEGIN
				if @departmentId is not null
					insert into @vid (value)
						select ovr.Vehicle_ID
						from dbo.v_operator_vehicle_right ovr
						where ovr.Right_ID = 102	--VehicleAccess
						  and ovr.Operator_ID =	@operatorID
						  and ovr.vehicle_id in (select v.vehicle_id from vehicle v where v.DEPARTMENT = @departmentId)
				else
					insert into @vid (value)
						select ovr.Vehicle_ID
							from dbo.v_operator_vehicle_right ovr
							where ovr.Right_ID = 102	--VehicleAccess
								and ovr.Operator_ID =	@operatorID
			END
			ELSE
				insert into @vid (value)
				select v.Vehicle_ID
				from VEHICLE v
				where 2 = (
					select count(*) 
					from v_operator_vehicle_right ovr
					where ovr.operator_id = @operatorID
						and ovr.vehicle_id = v.VEHICLE_ID
						and ovr.right_id in (102, 115)
				) and (@departmentId is null or v.DEPARTMENT = @departmentId)
		end
		else
			IF (@currentLogTime IS NULL)
				insert into @vid (value)
				select distinct vid.Id
				from @vehicleIDs vid
					join dbo.v_operator_vehicle_right ovr on ovr.Vehicle_ID = vid.Id
					join vehicle v on v.vehicle_id = ovr.vehicle_id
				where ovr.Right_ID = 102	--VehicleAccess
					and ovr.Operator_ID =	@operatorID
					and (@departmentId is null or v.department = @departmentId)
			ELSE
				insert into @vid (value)
				select distinct vid.Id
				from @vehicleIDs vid
					join Vehicle v on vid.Id = v.VEHICLE_ID
				where 2 = (
					select count(*)
					from v_operator_vehicle_right ovr
					where ovr.operator_id = @operatorID
						and ovr.vehicle_id = v.VEHICLE_ID
						and ovr.right_id in (102, 115)
				) and (@departmentId is null or v.DEPARTMENT = @departmentId)
	end

	if (@debug = 1)
		print 'create @vehicle_right'
	declare @vehicle_right table (vehicle_id int, right_id int, primary key clustered (vehicle_id, right_id))
	
	--OUTPUT: ALLOW_VEHICLE

	if @includeAttributes = 1
	begin
		if (@debug = 1)
			print 'insert into @vehicle_right'
		
		insert into @vehicle_right
			select ovr.vehicle_id, ovr.right_id
				from v_operator_vehicle_right ovr
				where ovr.operator_id = @operatorID
				  and ovr.vehicle_id in (select vid.Value from @vid vid)
		
		if (@debug = 1)
			print 'select common vehicle attributes'

		declare @askPositionCommandId int = (select id from CommandTypes where code = 'AskPositionOverMLP')
		select v.Vehicle_ID
			   , v.Garage_Number
			   , v.fuel_tank
			   , fuel_tank_gas = 0
			   , phone = 
					case 
						when 
							1=1 --заплатка для ошибки Bug 1006241 - Разрешение не выдается и Bug 1006242 - У друга в приложении меня стало два
							or ct.TYPE_NAME in ('Nika.Tracker', 'MLP')
							or exists (
							select 1
								from @vehicle_right ovr
								where ovr.vehicle_id = v.vehicle_id
								  and ovr.right_id = 20 /*ViewControllerPhone*/
							) then 
								case 
									when phoneContact.Value is not null then phoneContact.Value 
									else c.phone
								end
							else null
						end
			   , phoneIsEditable =
				case when exists (
							select 1
								from @vehicle_right ovr
								where ovr.vehicle_id = v.vehicle_id
								  and ovr.right_id = 20 /*ViewControllerPhone*/
							)
					then convert(bit, 1) else convert(bit, 0) end
			   , v.vehicle_kind_id
			   , vehicle_type
			   , public_number
			   , controller_number =
					case when exists (
					select 1 
						from @vehicle_right ovr 
						where ovr.vehicle_id = v.vehicle_id
						  and ovr.right_id = 21 /*ViewControllerDeviceID*/
					) then 
						   case
							  when ci.Device_ID is not null 
							  and ci.Device_ID <> 0x00
							  and len(rtrim(ltrim(convert(varchar(32), ci.Device_ID)))) <> 0
								then rtrim(ltrim(convert(varchar(32), ci.Device_ID)))
							  else null
						   end
					else
						null
					end
			   , a.ID Asid_ID
			   , d.DEPARTMENT_ID Department_ID
			   , a.AllowMlpRequest
			   , KnownMaskedMsisdn = convert(bit, case when a.Contact_ID is not null then 1 else 0 end)
			   , c.CONTROLLER_TYPE_ID
			   , CONTROLLER_TYPE_NAME = ct.TYPE_NAME
			   , CONTROLLER_TYPE_HasInputs = convert(bit, case when exists (select * from CONTROLLER_SENSOR cs where cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID) then 1 else 0 end)
			   , CONTROLLER_TYPE_SupportsPositionAsking = convert(bit, case when exists (select * from Controller_Type_CommandTypes ctct where ctct.Controller_Type_ID = c.CONTROLLER_TYPE_ID and ctct.CommandTypes_ID = @askPositionCommandId) then 1 else 0 end)
			   , CONTROLLER_TYPE_DeviceIdIsImei = ct.DeviceIdIsImei
			   , CONTROLLER_HasIP = convert(bit, case when ci.IP is not null and LEN(ci.IP) > 0 then 1 else 0 end)
			   , OwnerOperator_Operator_ID = ownerOperator.Operator_ID
			   , OwnerOperator_Login = ownerOperator.Login
			   , OwnerOperator_Name = ownerOperator.Name
			   , OwnerOperator_AsidId = (select top(1) a.ID from Asid a where a.Operator_ID = ownerOperator.Operator_ID order by a.ID asc)
			   , OwnerOperatorPhone = ownerOperator.Phone
			   , OwnerOperatorEmail = ownerOperator.Email
			   , v.MaxAllowedSpeed
			   , cast (case 
					when @operatorId is null then 0
					when 
						exists (
							select *
							from v_operator_department_right odr
							where odr.department_id=v.department 
							  and odr.right_id=104 
							  and odr.operator_id=@operatorId) 
					   or 
						exists (
							select *
								from @vehicle_right vr
								where vr.right_id = 2
								  and vr.vehicle_id = v.VEHICLE_ID)
				   then 0 
				   else 1 end as bit) isFriendVehicle
				, isOwnVehicle = convert(bit, case when a.Operator_ID = @operatorID then 1 else 0 end)
		from dbo.Vehicle v
		join dbo.controller c on c.vehicle_id = v.vehicle_id
		join dbo.controller_type ct on ct.controller_type_id = c.controller_type_id		
		left outer join dbo.MLP_Controller mlpc on mlpc.Controller_ID = c.Controller_ID
		left outer join dbo.Asid a on a.ID = mlpc.Asid_ID
		left outer join Contact asidContact on asidContact.ID = a.Contact_ID
		left outer join Contact phoneContact on phoneContact.ID = isnull(asidContact.Demasked_ID, c.PhoneContactID)
		left outer join dbo.Controller_Info ci on ci.Controller_ID = c.Controller_ID
		left outer join Department d on d.DEPARTMENT_ID = v.DEPARTMENT
		outer apply (
			select top(1)
					o.Operator_ID
					, o.Login
					, o.Name
					, Email = (
					select top(1) oc.Value
						from v_Operator_Contact oc
						where oc.Operator_ID = o.Operator_ID
							and oc.Confirmed = 1
							and oc.Type = 1
						order by oc.Value asc
					)
					, Phone = isnull(
						(
							select top(1) pc.Value
								from Asid a
								join Contact ac on ac.ID = a.Contact_ID
								join Contact pc on pc.ID = ac.Demasked_ID
								where a.Operator_ID = o.Operator_ID
								order by a.ID asc
						),
						(
							select top(1) oc.Value
								from v_Operator_Contact oc
								where oc.Operator_ID = o.Operator_ID
									and oc.Confirmed = 1
									and oc.Type = 2
								order by oc.Value asc
						)
					)
				from v_operator_vehicle_right ovr
				join Operator o on o.operator_id = ovr.operator_id
				where ovr.right_id = 2
					and ovr.vehicle_id = v.vehicle_id
				order by ovr.priority asc, ovr.operator_id asc
		) ownerOperator
		where v.vehicle_id in (select vid.Value from @vid vid)	 

		if (@debug = 1)
			print 'OUTPUT: VEHICLE_PROFILE'

		--OUTPUT: VEHICLE_PROFILE
		select vp.vehicle_id, vp.property_name, vp.value
			from [dbo].vehicle_profile vp 
			where vp.vehicle_id in (select vid.value from @vid vid)

		if (@debug = 1)
			print 'OUTPUT: Vehicle_Right'

		--Дополнительные права на устройства
		--OUTPUT: Vehicle_Right
		select 
			ovr.vehicle_id,
			ovr.right_id
			from @vehicle_right ovr
			where ovr.right_id <> 102

		declare @monthBegin date = getutcdate()
		set @monthBegin = DATEADD(day, 1-datepart(day, @monthBegin), @monthBegin)
		--OUTPUT: RenderedServiceItem
		select 
				  Vehicle_ID = vid.value
				, Rendered_Service_Type_Name = rst.Name
				, rsi.Quantity
			from @vid vid
			join Controller c on c.VEHICLE_ID = vid.value
			join MLP_Controller mc on mc.Controller_ID = c.Controller_ID
			cross apply (
				select rsi.Rendered_Service_Type_ID, Quantity = COUNT(1)
				from RenderedServiceItem rsi 
				where rsi.Asid_ID = mc.Asid_ID 
					and rsi.Billing_Service_ID is null
					and rsi.RenderDate >= @monthBegin
				group by rsi.Rendered_Service_Type_ID) rsi
			join Rendered_Service_Type rst on rst.ID = rsi.Rendered_Service_Type_ID

		--OUTPUT: Billing_Blocking
		select
			bb.Asid_ID,
			ID = 1,
			Name = 'Block'
			, bb.BlockingDate
			from @vid vid
			join dbo.Controller c on c.Vehicle_ID = vid.value
			join dbo.MLP_Controller mlpc on mlpc.Controller_ID = c.Controller_ID
			join dbo.Billing_Blocking bb on bb.Asid_ID = mlpc.Asid_ID	
	end
	else
	begin
		select Vehicle_ID = vid.Value from @vid vid
	end;
	
	--OUTPUT: SENSOR_MAPPING
		select
			 VEHICLE_ID = t.Vehicle_ID
			,LegendNumber = t.Sensor_Legend
			,Controller_Sensor_Type_ID = (
				select Controller_Sensor_Type_ID 
					from dbo.Controller_Sensor_Legend csl 
					where csl.Number = t.Sensor_Legend)
			from v_controller_sensor_legend t with (noexpand)
			where (t.Vehicle_ID in (select vid.Value from @vid vid))
	
	if (isnull(@includeLastData, 1) = 0)
		return;
	
	--Данные о местоположении машин и состояниях входов контроллеров: XXX_Log-таблицы

	declare @interval int, @canInterval int, @controllerSensorInterval int

	set @interval = 90; --1:30
	set @canInterval = 86400; --24:00:00
	set @controllerSensorInterval = 30 * 60; --30:00 - @noDataInterval, см. GetFullLog

	declare @dumpingInsertTimeThreshold datetime = dateadd(minute, -1, getutcdate())
	declare @dumpingLogTimeThreshold int = datediff(ss, '1970', getutcdate())-3600

	--Местоположение объекта, скорость и точность
	if (@debug = 1)
		print 'fill #position'

	--OUTPUT: #positions
	select Vehicle_ID = vid.Value
		,lgt.Log_Time
		,Geo_Log_Time = geo.Log_Time
		,geo.[Lat]
		,geo.[Lng]
		,Prev_Lat = geo.[Prev_Lat]
		,Prev_Lng = geo.[Prev_Lng]
		,Course = dbo.CalculateCourseFN(geo.[Prev_Lat], geo.[Prev_Lng], geo.[Lat], geo.[Lng])
		,GPS_Course = gps.CourseDegrees
		,IsLBSPosition = cast (case rad.[Type] when 1 then 1 else 0 end as bit)
		,Speed = convert(tinyint, case isnull(rad.[Type], 0) when 0 then gps.Speed else null end)
		,rad.Radius
		,PositionType = case when geo.[LOG_TIME] is not null and rad.[Type] is null then cast(0 /*GPS default*/ as tinyint) else rad.[Type] end
		,Dumping = convert(bit, case when lgt.Log_Time < @dumpingLogTimeThreshold and lgt.InsertTime > @dumpingInsertTimeThreshold then 1 else 0 end)
		,InsertDelayS = DATEDIFF(SECOND, lgt.InsertTime, GETUTCDATE())
		into #positions
	from @vid vid
	CROSS APPLY
	(
		SELECT TOP(1)
			lgt_log.*
		FROM [dbo].[Log_Time#Lasts] lgt_lst WITH (NOLOCK)
			INNER JOIN [dbo].[Log_Time] lgt_log WITH (NOLOCK)
				ON  lgt_log.[Vehicle_ID] = lgt_lst.[Vehicle_ID]
				AND lgt_log.[Log_Time]   = lgt_lst.[Log_Time#Last]
		WHERE lgt_lst.[Vehicle_ID] = vid.[Value]
		ORDER BY lgt_log.[Log_Time] DESC
	) lgt
	OUTER APPLY
	(
		SELECT TOP(1)
			[LOG_TIME] = [LOG_TIME#Last],
			[Lat]      = [Lat#Last],
			[Lng]      = [Lng#Last],
			[Prev_Lat] = [Lat#Prev],
			[Prev_Lng] = [Lng#Prev]
		FROM [dbo].[Geo_Log#Lasts] WITH (NOLOCK)
		WHERE [Vehicle_ID] = vid.[Value]
		ORDER BY [LOG_TIME#Last] DESC
	) geo
	OUTER APPLY
	(
		SELECT TOP(1)
			gps_log.*, [IsLBSPosition] = 0
		FROM [dbo].[GPS_Log#Lasts] gps_lst WITH (NOLOCK)
			INNER JOIN [dbo].[GPS_Log] gps_log WITH (NOLOCK)
				ON  gps_log.[Vehicle_ID] = gps_lst.[Vehicle_ID]
				AND gps_log.[Log_Time]   = gps_lst.[Log_Time#Last]
		WHERE gps_lst.[Vehicle_ID] = vid.[Value]
		ORDER BY gps_log.[Log_Time] DESC
	) gps
	OUTER APPLY
	(
		SELECT TOP(1)
			*
		FROM [dbo].[Position_Radius] WITH (NOLOCK)
		WHERE [Vehicle_ID] = vid.[Value]
		AND   [Log_Time]   = geo.[LOG_TIME]
		ORDER BY [LOG_TIME] DESC
	) rad
	-- Обновим значения LBS запросов при наличии более точных данных
	;with cte as
	(
		SELECT
			 geo.Vehicle_ID
			,geo.LOG_TIME
			,geo.Lat
			,geo.Lng
			,rad.Radius
			,rad.[Type]
			,Speed = convert(tinyint, case isnull(rad.[Type], 0) when 0 then gps.Speed else null end)
			,PositionType = case when geo.LOG_TIME is not null and rad.Type is null then cast(0 /*GPS default*/ as tinyint) else rad.[Type] end
			,IsLBSPosition = cast (case rad.[Type] when 1 then 1 else 0 end as bit)
		FROM [dbo].[Geo_Log] geo WITH (NOLOCK)
			OUTER APPLY
			(
				SELECT TOP(1) Speed
				FROM [dbo].[GPS_Log] gps_temp WITH (NOLOCK)
				WHERE gps_temp.Vehicle_ID = geo.Vehicle_ID
				AND   gps_temp.Log_Time  <= geo.LOG_TIME
				ORDER BY gps_temp.Log_Time DESC
			) gps
			OUTER APPLY
			(
				SELECT TOP(1) rad_temp.Radius, rad_temp.[Type]
				FROM [dbo].[Position_Radius] rad_temp WITH (NOLOCK)
				WHERE rad_temp.Vehicle_ID = geo.Vehicle_ID
				AND   rad_temp.Log_Time   = geo.LOG_TIME
			) rad
	)
	UPDATE pos
		SET  pos.Lat           = upd.Lat
			,pos.Lng           = upd.Lng
			,pos.Course        = upd.Course
			,pos.Speed         = upd.Speed
			,pos.Radius        = upd.Radius
			,pos.PositionType  = upd.PositionType
			,pos.IsLBSPosition = upd.IsLBSPosition
			,pos.Geo_Log_Time  = upd.LOG_TIME
	FROM #positions pos
		CROSS APPLY
		(
			SELECT TOP(1)
				 Lat
				,Lng
				,LOG_TIME
				,Speed
				,Radius
				,PositionType
				,IsLBSPosition
				,dbo.CalculateCourseFN(pos.Prev_Lat, pos.Prev_Lng, cte.Lat, cte.Lng) Course
			FROM cte
			WHERE cte.Vehicle_ID = pos.Vehicle_ID 
			AND   cte.LOG_TIME   > pos.Geo_Log_Time - dbo.GetLbsRewriteInterval()
			AND   cte.LOG_TIME   < pos.Geo_Log_Time
			ORDER BY cte.LOG_TIME DESC
		) upd
	WHERE pos.IsLBSPosition = 1
	AND   upd.PositionType  = 0
	SELECT * FROM #positions

	-- OUTPUT State
	SELECT
		[Vehicle_ID] = vid.[value],
		stt.[Type],
		stt.[Log_Time],
		stt.[Value]
	FROM @vid vid
		OUTER APPLY
		(
			SELECT 1 [Type]
		) typ
		CROSS APPLY
		(
			SELECT TOP(1)
				*
			FROM [dbo].[State_Log] WITH (NOLOCK)
			WHERE [Vehicle_ID] = vid.[Value]
			AND   [Type]       = typ.[Type]
			ORDER BY [Log_Time] DESC
/*
			SELECT TOP(1)
				stl.*
			FROM [dbo].[State_Log#Lasts] lst WITH (NOLOCK)
				INNER JOIN [dbo].[State_Log] stl WITH (NOLOCK)
					ON  stl.[Vehicle_ID] = lst.[Vehicle_ID]
					AND stl.[Type]       = lst.[Type]
			WHERE lst.[Vehicle_ID] = vid.[Value]
			AND   lst.[Type]       = typ.[Type]
*/
		) stt

	--Датчики из таблицы Controller_Sensor_Log
	if (@debug = 1)
		print 'OUTPUT: MAPPED_SENSOR'

	--OUTPUT: MAPPED_SENSOR
	SELECT --All except Alarm
		 [VEHICLE_ID]   = [Vehicle_ID]
		,[LegendNumber] = [Sensor_Legend]
		,[Log_Time]     = [Log_Time]
		,[Value]        = SUM([Value] * [Multiplier] + [Constant])
	FROM
	(
		SELECT
			csm.[Vehicle_ID],
			csm.[Sensor_Legend],
			csm.[Multiplier],
			csm.[Constant],
			csl.[Log_Time],
			csl.[Value]
		FROM @vid vid
			INNER JOIN [dbo].[v_controller_sensor_map] csm WITH (NOEXPAND)
				ON csm.[Vehicle_ID] = vid.[value]
				CROSS APPLY
				(
					SELECT TOP(1)
						vls.[Log_Time], vls.[Value]
					FROM [dbo].[Controller_Sensor_Log#Lasts] lst WITH (NOLOCK)
						INNER JOIN [dbo].[Controller_Sensor_Log] vls WITH (NOLOCK)
							ON  vls.[Vehicle_ID] = lst.[Vehicle_ID]
							AND vls.[Number]     = lst.[Number]
							AND vls.[Log_Time]   = lst.[Log_Time#Last]
					WHERE lst.[Vehicle_ID] = vid.[value]
					AND   lst.[Number]     = csm.[Sensor_Number]
				) csl
/*
		SELECT
			csm.[Vehicle_ID],
			csm.[Sensor_Legend],
			csm.[Multiplier],
			csm.[Constant],
			csl.[Log_Time],
			csl.[Value]
		FROM [dbo].[v_controller_sensor_map] csm WITH (NOEXPAND)
			CROSS APPLY
			(
				SELECT TOP(1)
					csl.[Log_Time],
					[Value] = csl.[Value] + ISNULL(sbl.[Value], 0)
				FROM dbo.Controller_Sensor_Log csl with (nolock)
					OUTER APPLY
					(
						SELECT TOP(1)
							[Value] = sbl.Multiplier * ISNULL(
								sbl.[Value],
								(
									SELECT TOP(1)
										base_csl.[Value]
									FROM [dbo].[Controller_Sensor_Log] base_csl WITH (NOLOCK)
									WHERE base_csl.[Vehicle_ID] = sbl.[Vehicle_ID]
									AND   base_csl.[Number]     = sbl.[Number]
									AND   base_csl.[Log_Time]  <= sbl.[Log_Time]
									ORDER BY base_csl.[Log_Time] DESC
								))
						FROM [dbo].[Sensor_Base_Log] sbl WITH (NOLOCK)
						WHERE sbl.[Vehicle_ID] = csl.[Vehicle_ID]
						AND   sbl.[Number]     = csl.[Number]
						AND   sbl.[Log_Time]  <= csl.[Log_Time]
						ORDER BY sbl.[Log_Time] DESC
					) sbl
				WHERE csl.[Vehicle_ID] = csm.[Vehicle_ID]
				AND   csl.[Number]     = csm.[Sensor_Number]
				AND  (csl.[Log_Time]  <= @currentLogTime OR @currentLogTime IS NULL)
				ORDER BY csl.[Log_Time] desc
			) csl
			-- Отсутствие значения или выход за пределы снизу
			WHERE (csm.[Min_Value]  IS NULL OR csm.[Min_Value] <= csl.[Value])
			-- Отсутствие значения или выход за пределы сверху
			AND   (csm.[Max_Value]  IS NULL OR csl.[Value]     <= csm.[Max_Value])
			--Фильтр по машинам
			AND   (csm.[Vehicle_ID] IN (SELECT vid.[Value] FROM @vid vid))
*/
	) t
	GROUP BY t.[Vehicle_ID], t.[Sensor_Legend], t.[Log_Time]

	if (@debug = 1)
		print 'OUTPUT: Command'
	
	SELECT
		 id = command.ID
		,Result_Date = command.Date_Received
		,Target_ID = v.value
		,[Type_ID] = cmdTypes.id
		,Created = command.Created
		,Result_Type_ID
		,command.Status
		,command.Payer_Vehicle_ID
	FROM @vid v
		JOIN dbo.CommandTypes cmdTypes ON 1=1
		CROSS APPLY (
			SELECT TOP(1) c.Result_Date_Received Date_Received, c.ID, Created = c.Date_Received, c.Result_Type_ID, c.Payer_Vehicle_ID, c.Status
			FROM dbo.Command c
			WHERE 
				c.Type_ID = cmdTypes.id 
				AND c.Target_ID = v.value 
			ORDER BY c.Date_Received DESC
		) as command

	if (@debug = 1)
		print 'OUTPUT: Picture_Log'

	--OUTPUT: Picture_Log
	select plog.Vehicle_ID, plog.Log_Time, plog.Camera_Number, plog.Photo_Number, plog.Url
		from @vid vid
		cross apply (
			select top(1) * 
				from dbo.Picture_Log plog_last with (nolock) 
				where plog_last.Vehicle_ID = vid.Value
				order by plog_last.Log_Time desc) plog

	if @includeZone = 1 and @operatorId is not null
	begin
		if @debug = 1
			print 'OUTPUT: GeoZone'
		declare @vehicle_zone dbo.Vehicle_Zone_Param
		declare @vehicle_position dbo.Vehicle_Position_Param

		insert into @vehicle_zone(Vehicle_ID, Zone_ID)
		select ids.value, zr.ZONE_ID
		from v_operator_zone_right zr
			cross join @vid ids
		where zr.right_id = 105 and zr.operator_id = @operatorID

		insert into @vehicle_position(Vehicle_ID, Log_Time, Lat, Lng)
		select p.Vehicle_ID, p.Log_Time, p.Lat, p.Lng
		from #positions p

		--OUTPUT: GeoZone
		exec GetGeoZonesForPositions @vehicle_zone = @vehicle_zone, @vehicle_position = @vehicle_position
	end

	drop table #positions
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
end
go