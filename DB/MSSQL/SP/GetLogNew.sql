set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

alter  procedure [dbo].[GetLogNew]
	@count int, 
	@interval int,
	@from int,
	@to int,
	@vehicle_id int,
	@debug bit = 0
as
BEGIN

if (@debug is null or @debug = 0)
	set nocount on;

/*
exec DBO.GETLOGNEW @count=2001,@interval=5,@from=1270109940,@to=1272622740,@vehicle_id=529,@debug=default;
*/
declare @controller_id int, @temperature_period int
set @controller_id = (select controller_id from dbo.controller where vehicle_id = @vehicle_id)
set @temperature_period = 1200

create table #t (log_time int, rn int primary key clustered)
CREATE INDEX [IX_LT] ON #t
(
	[LOG_TIME] ASC
)

insert into #t
	select log_time, row_number() over (order by log_time asc) 
	from dbo.Log_Time lt (nolock)  
	where lt.Vehicle_ID = @vehicle_id
	  and lt.log_time between @from and @to

--для вэба - 2001 передается. сделаем чтобы можно было в хранимой процедуре менять
if (@count=2001) set @count = 10000;

--Прореживаем записи, чтобы получилось нужное число точек ~= @count
--Расчитываем интервал, чтобы было не больше заданного кол-ва точек. 
--Точки с интервалом более 10 минут не прореживаем
--select @interval = (max(log_time) - min(log_time))/@count from #t;
declare @total_recs int, @deleteRow int, @leaveRow int;
select @deleteRow = 0, @leaveRow = 0, @total_recs = count(*) from #t;

if (cast(@total_recs as real)/@count > 2)
	set @leaveRow = round(cast(@total_recs as real)/@count, 0, 1);
else
	set @deleteRow = round(cast(@total_recs as real)/(@total_recs - @count),0);

if (@deleteRow > 1 or @leaveRow > 1)
begin
	declare @i int, @innerCounter int;
	set @i = 1
	set @innerCounter = 1;
	while @i <= @total_recs
	begin
		if (@i>1 and @i<@total_recs)
		begin
			if (@leaveRow > 1)
				if (@innerCounter <> @leaveRow)
					delete from #t where rn = @i and exists(select * from #t t2 where t2.rn < @i and t2.log_time > #t.log_time - 600);
				else
					set @innerCounter = 0;			
			else
				if (@innerCounter = @deleteRow)
				begin
					delete from #t where rn = @i and exists(select * from #t t2 where t2.rn < @i and t2.log_time > #t.log_time - 600);;
					set @innerCounter = 0;			
				end
		end
		set @innerCounter = @innerCounter + 1;
		set @i = @i + 1
	end
end

if (@debug=1)
begin
	print @total_recs;
	print @deleteRow;
	print @leaveRow;
	select @total_recs = count(*) from #t;
	print @total_recs;
end

CREATE TABLE #TBL(
	[MONITOREE_ID] [int] NOT NULL,
	[LOG_TIME] [int] NOT NULL,
	[X] numeric(8,5) NULL,
	[Y] numeric(8,5) NULL,
	[SPEED] [tinyint] NULL,
	[MEDIA] [tinyint] NULL,
	[V] int NULL,
	[V1] int NULL,
	[V2] int NULL,
	[V3] int NULL,
	--[V4] [tinyint] NULL,
	[SATELLITES] [tinyint] NULL,
	--[FIRMWARE] [smallint] NULL,
	--[FLAG] [tinyint] NOT NULL,
	[T] int null,
	[prevT] int null,
	[nextT] int null,
	[F] int null,
	[DS] int null,
	Radius int null,
	IsMLP bit null,
	[ROW] [int] NOT NULL
) ON [PRIMARY]

/****** Object:  Index [IX_LT]    Script Date: 01/24/2008 18:06:32 ******/
CREATE CLUSTERED INDEX [IX_LT] ON #TBL 
(
[ROW] ASC--,
--[LOG_TIME] ASC
	)	

CREATE TABLE #CAN(
	[LOG_TIME] [int] NOT NULL PRIMARY KEY,
	[F1] [smallint] NULL,
	TOTAL_RUN int null
)

create table #FUEL
(
	[LOG_TIME] [int] NOT NULL PRIMARY KEY,
	F int null
)

create table #Temperature
(
	[LOG_TIME] [int] NOT NULL PRIMARY KEY,
	T int null
)

insert into #TBL
select     @vehicle_id MONITOREE_ID,
		   lt.[LOG_TIME],
           gl.[Lng] X,
           gl.[Lat],
           gpsl.[SPEED],
           lt.[MEDIA],
	       0,
	       0,
	       0,
	       0,
	       gpsl.[SATELLITES], 
		   0,
		   null, null,
		   0,
		   0,
		   pr.Radius,
		   lt.ID,
		   null,
           ROW_NUMBER() over (order by lt.log_time)
	from #t
	join dbo.Log_Time lt (nolock) on lt.log_time = #t.log_time and lt.Vehicle_ID = @vehicle_id
	left outer join  dbo.Geo_Log gl			(nolock)	on gl.Log_Time = lt.Log_Time and gl.Vehicle_ID = lt.Vehicle_ID
	left outer join  dbo.GPS_Log gpsl		(nolock)    on gpsl.Log_Time = lt.Log_Time and gpsl.Vehicle_ID = lt.Vehicle_ID
	left outer join  dbo.Position_Radius pr (nolock)	on pr.Log_Time = lt.Log_Time and pr.Vehicle_ID = lt.Vehicle_ID
	where 1=1 -- and lt.log_time between @from and @to
order by lt.log_time

if @debug = 1
	select * from #tbl

--Удалеяем дубли времени
delete from #TBL
where exists (select * from #TBL t2 where t2.[LOG_TIME] = #TBL.[LOG_TIME] and t2.[ROW] > #TBL.[ROW])

declare @avgT int;
select @avgT = avg(T) from #TBL;

update #TBL
set T = null
where abs(T - @avgT) > 30

--предыдущая температура
update tbl
	set prevT = (select top(1) sl.T
				 from #Temperature sl
				 where sl.Log_Time between (@from - @temperature_period) and (tbl.Log_Time - 1)
				 order by sl.Log_Time desc)




	from #TBL tbl
	where tbl.T is not null

--последующая температура
update tbl
	set nextT = (select top(1) sl.T
				 from #Temperature sl
				 where sl.Log_Time between (tbl.Log_Time + 1) and (@to + @temperature_period)
				 order by sl.Log_Time asc)




	from #TBL tbl
	where tbl.T is not null

--если были какие-то сильные отклонения - то берем предыдущее значение
update #TBL
	set T = prevT
	where T is not null 
		and prevT is not null
		and abs(T - prevT) > 10
		and abs(T - nextT) > 10

---заполняем пропуски показателей температуры (данными из прошлого)
update tbl
	set T = (select top(1) sl.T
				 from #Temperature sl
				 where sl.Log_Time between (tbl.log_time - @temperature_period) and tbl.log_time
				 order by sl.Log_Time desc)




	from #TBL tbl
		where T is null 
 	 
--для первых позиций без температуры - показываем температуру первой значимой записи (из будущего)
update tbl
	set T = (select top(1) sl.T
				 from #Temperature sl 
				 where sl.Log_Time between tbl.log_time and (tbl.log_time + @temperature_period)
				 order by sl.Log_Time asc)




	from #TBL tbl
		where T is null 


--получение датчика топлива
declare @controller_sensor_legend_id int;
set @controller_sensor_legend_id = 2;

declare @fuelFrom int;

insert #FUEL (LOG_TIME, F)
select sv.log_time, sum((map.multiplier * sv.value + map.constant)/isnull(ci.fuel_factor,1)) value
from controller c
join CONTROLLER_SENSOR_MAP map (nolock)  on map.controller_id = c.controller_id
join CONTROLLER_SENSOR cs (nolock)  on cs.CONTROLLER_SENSOR_id = map.CONTROLLER_SENSOR_id
join CONTROLLER_SENSOR_LOG sv (nolock) on sv.vehicle_id = @vehicle_id
								 and sv.log_time between (@from - 43200) and @to 
								 and sv.number = cs.number
left join dbo.CONTROLLER_INFO ci (nolock)  on c.CONTROLLER_ID = ci.CONTROLLER_ID								 
where c.vehicle_id = @vehicle_id
and map.controller_sensor_legend_id = @controller_sensor_legend_id
group by sv.log_time

/*
insert #FUEL (LOG_TIME, F)
select LOG_TIME, F from #FUEL;
*/
if (@@rowcount > 0)
begin
	select @fuelFrom = max(sl.LOG_TIME) 
	from #FUEL sl (nolock)
	where sl.LOG_TIME <= @from;

	if (@fuelFrom is null) set @fuelFrom = @from;
	delete from #FUEL where  LOG_TIME < @fuelFrom;
end

--получение данных CAN шины (топливо пока только один бак так как пока есть настройка объема только на один бак)
declare @canFrom int;
select @canFrom = max(c.LOG_TIME) from CAN_INFO c (nolock)
where c.Vehicle_ID = @vehicle_id 
and c.LOG_TIME <= @from
and c.LOG_TIME > (@from - 43200); ---12 часов

if (@canFrom is null) set @canFrom = @from;

--для расчета топлива по CAN сразу в литрах (в CAN_INFO указывается в десятых долях процента от бака)
declare @fuelTank smallint;
select @fuelTank = FUEL_TANK from dbo.VEHICLE where VEHICLE_ID = @vehicle_id

insert into #CAN ([LOG_TIME], [F1], TOTAL_RUN)
select ci.LOG_TIME, (cast(ci.FUEL_LEVEL1 as int) * @fuelTank / 1000), TOTAL_RUN
from dbo.CAN_INFO ci (nolock)
left join dbo.GPS_Log gpsl (nolock) on gpsl.Log_Time = ci.Log_Time and gpsl.Vehicle_ID = ci.Vehicle_ID
where ci.Vehicle_ID = @vehicle_id and ci.LOG_TIME between @canFrom and @to
and (isnull(ci.FUEL_LEVEL1, 0) > 0 or isnull(ci.TOTAL_RUN,0) > 0) 
and (gpsl.speed is null or gpsl.speed > 5);

if (select count(*) from #CAN) > 0
	begin
		---TODO: Сглаживание отклонений по топливу
		set @canFrom = @canFrom;
	end

--выборка параметров контроллера
select v.PUBLIC_NUMBER, v.GARAGE_NUMBER,
	ci.FUEL_LVAL, ci.FUEL_UVAL, ci.FUEL_FACTOR, ci.FUEL_SENSOR_DIRECTION, 
	@total_recs 'RecsCountInDB'
from VEHICLE v (nolock) 
	left join dbo.CONTROLLER c (nolock)  on c.VEHICLE_ID = v.VEHICLE_ID
	left join dbo.CONTROLLER_INFO ci (nolock)  on c.CONTROLLER_ID = ci.CONTROLLER_ID
where v.VEHICLE_ID = @vehicle_id

	create table #geo_log (log_time int, lng numeric(8,5), lat numeric(8,5))
	create clustered index IX_Tmp_Geo_Log on #geo_log(log_time)
	
	if @debug = 1
		print 'selecting into #geo_log'

	insert into #geo_log (log_time, lng, lat)
		select log_time, lng, lat
			from dbo.Geo_Log (nolock)
			where vehicle_id = @vehicle_id
			  and log_time between @from and @to
			  and lng is not null
			  and lat is not null

	insert into #geo_log (log_time, lng, lat)
		select top(1) log_time, lng, lat
			from dbo.Geo_Log (nolock)
			where vehicle_id = @vehicle_id
			  and log_time < @from
			order by log_time desc

	update t
		set 
			X 		= (select top(1) t1.Lng	from #geo_log t1 where t1.log_time <= t.log_time order by t1.log_time desc),
			Y 		= (select top(1) t1.Lat	from #geo_log t1 where t1.log_time <= t.log_time order by t1.log_time desc)
		from #tbl t
	where X is null 
	   or Y is null

	update t 
		set Speed = isnull(
			(select top(1) t1.Speed 
				from dbo.GPS_Log t1 (nolock)   
				where t1.log_time between t.log_time - 300 and t.log_time and vehicle_id = @vehicle_id 
				order by t1.log_time desc)
			, 0)
		from #tbl t
		where Speed is null

	delete from #tbl
		where x is null
		   or y is null

update t
	set IsMLP = 1
	from #tbl t
	where exists (
		select 1 from dbo.Command c
			where c.Log_Time = t.Log_Time
			  and c.Target_ID = t.Vehicle_ID)

create table #ignition (Log_Time int primary key clustered, Value bit)

insert into #ignition (Log_Time, Value)
	select csl.Log_Time, case sum(csl.Value * csm.Multiplier + csm.Constant) when 0 then 0 else 1 end
		from dbo.Controller_Sensor_Map csm (nolock)  
		join dbo.Controller_Sensor     cs	(nolock)  		on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
		join dbo.Controller_Sensor_Log csl (nolock) on csl.Number = cs.Number
		where	1=1
			and csm.Controller_ID = @controller_id
			and csl.Vehicle_ID = @vehicle_id
			and	csl.Log_Time between @from - 42300 and @to
		    and csm.Controller_Sensor_Legend_ID = 4 --зажигание
		group by Log_Time
		


if @debug = 1
	select dbo.GetDateFromInt(Log_Time), * from #ignition
    
if @debug = 1
	select *, ds & 32 ignition_flag from #tbl
	--Єvё°иЁї№ ¤║Ў¤■ї ·■√°вї╕кЄ■  ■ў°б°∙	
	--Set ROWCOUNT @count

	-- V1 - temperature
	-- V4 - fuel
	--select t.*, cs.V1 as T, cs.V4 as F from  #tbl t
      select distinct t.[MONITOREE_ID],t.[LOG_TIME],t.[X],t.[Y],
             t.[SPEED],t.[MEDIA],t.[V],t.[V1],t.[V2],t.[V3],t.[SATELLITES],
             t.T, isnull(f.F,0) as F, 
			case i.Value 
				when 1    then isnull(t.DS, 0) & ~32	--бит сброшен - зажигание включено
				when 0    then isnull(t.DS, 0) | 32		--бит установлен - зажигание выключено
				else t.DS
			end as DS,
			cast(isnull(ci.F1,0) as smallint) as CAN_fuel,
			isnull(ci.TOTAL_RUN,0) as CAN_totalRun,
			t.Radius,
			t.IsMLP
			from  #tbl t
		--left join dbo.CONTROLLER c on c.VEHICLE_ID = t.MONITOREE_ID
		--left join dbo.CONTROLLER_STAT cs on cs.CONTROLLER_ID = c.CONTROLLER_ID and cs.TIME = dateadd(s, log_time, '1970')
		left join #CAN ci on ci.log_time = (select max(cii.[LOG_TIME]) from #CAN cii where cii.log_time <= t.[LOG_TIME])
		left join #FUEL f on f.log_time = (select max(f2.[LOG_TIME]) from #FUEL f2 where f2.log_time <= t.[LOG_TIME])
		left join #ignition i on i.log_time = (select max(i2.log_time) from #ignition i2 where i2.log_time <= t.Log_Time)
		order by t.[LOG_TIME] --t.[ROW]
		Option (Keepfixed Plan)

	Set ROWCOUNT 0;

	drop table #geo_log
	drop table #ignition;
	drop table #t;
	drop table #Temperature
	Drop table #TBL;
	Drop table #CAN;
	drop table #fuel;
	
END
