if exists (select * from sys.objects where name = 'GetUnregisteredCompoundRuleEvents')
	drop procedure GetUnregisteredCompoundRuleEvents
go

create procedure GetUnregisteredCompoundRuleEvents
(
	@compoundRuleLog dbo.CompoundRuleLogParam readonly,
	@delta int
)
as
begin

	select *
		from @compoundRuleLog p
		where not exists (
			select 1
				from CompoundRule_Log crl
				where crl.Vehicle_ID = p.Vehicle_ID
				  and crl.Log_Time between p.Log_Time - @delta and p.Log_Time + @delta
				  and crl.CompoundRule_ID = p.CompoundRule_ID)

end