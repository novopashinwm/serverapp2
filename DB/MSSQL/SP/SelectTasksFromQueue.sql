

IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'SelectTasksFromQueue' 
)
   DROP PROCEDURE [dbo].[SelectTasksFromQueue]
GO

CREATE PROCEDURE [dbo].[SelectTasksFromQueue] AS
begin

	declare @schedulerqueueID int
	set @schedulerqueueID = (select top 1 sq.SCHEDULERQUEUE_ID 
							FROM SCHEDULERQUEUE sq
							left outer join EMAIL_SCHEDULERQUEUE es on es.SCHEDULERQUEUE_ID = sq.SCHEDULERQUEUE_ID
							where es.EMAIL_ID is not null 
							or sq.Vehicle_ID is not null
							or sq.VehicleGroup_ID is not null
							ORDER BY sq.NEAREST_TIME ASC)

	select se.* FROM SCHEDULEREVENT se
		join  SCHEDULERQUEUE sq on 	se.SCHEDULEREVENT_ID = sq.SCHEDULEREVENT_ID
		WHERE sq.SCHEDULERQUEUE_ID = @schedulerqueueID	

    SELECT * FROM SCHEDULERQUEUE 
		WHERE SCHEDULERQUEUE_ID = @schedulerqueueID
		ORDER BY NEAREST_TIME ASC
end













