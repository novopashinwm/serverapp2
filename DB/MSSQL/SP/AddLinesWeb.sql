
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF EXISTS ( SELECT * FROM sysobjects WHERE name = N'AddLinesWeb' AND type = 'P')
    DROP PROCEDURE AddLinesWeb
GO

-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Add Lines for Web
-- =============================================

CREATE PROCEDURE [dbo].[AddLinesWeb]  
	@map_guid		uniqueidentifier,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@description	nvarchar(500),
	@operator_id	int
AS

------------------------------------------------
/*
-- Test
declare
	@map_guid		uniqueidentifier,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@description	nvarchar(500),
	@operator_id	int
*/
------------------------------------------------
	
--
set @vertex = replace(@vertex, ',', '.')

	
-- MAP_ID
declare @map_id	int
set @map_id = (select MAP_ID from MAPS where GUID = @map_guid)
if (@map_id is null)
	return


--
insert dbo.WEB_POINT
	([NAME], [DESCRIPTION], [OPERATOR_ID]) 
values
	(@name, @description, @operator_id) 

declare @line_id int
set @line_id = (select @@identity)


-- выделение в строке VERTEX подстроки с координатами для каждой точки линии
declare 
	@vertex1	varchar(100),
 	@x1			float,
 	@y1			float,
	@order		int,
	@vertex_id	int
set @order = 0

while (len(@vertex) > 1 and charindex(';', @vertex) > 1) 
begin
	set @order = @order + 1
	
	-- выделение координат каждой точки и ее направления
	set @vertex1 = substring(@vertex, 1, (charindex(';', @vertex) - 1))
	set @x1 = convert(float, substring(@vertex1, 1, (charindex(':', @vertex1) - 1))) 
	set @vertex1 = substring(@vertex1, (charindex(':', @vertex1) + 1), len(@vertex1))
	set @y1 = convert(float, @vertex1) 

	--запись полученных значений	
	insert dbo.MAP_VERTEX
		([MAP_ID], [X], [Y], [VISIBLE], [ENABLE]) 
	values
		(@map_id, @x1, @y1, 'true', 'true')

	set @vertex_id = (select @@identity)

	insert dbo.WEB_LINE_VERTEX
		([LINE_ID], [VERTEX_ID], [ORDER]) 
	values
		(@line_id, @vertex_id, @order) 
 
	set @vertex = substring(@vertex, (charindex(';', @vertex) + 1), len(@vertex))
end



