if exists (select * from sys.procedures where name='UpdatePhoneBookContact')
	drop procedure UpdatePhoneBookContact
go
create procedure UpdatePhoneBookContact
(
	@msisdn varchar(20), 
	@name nvarchar(max),
	@operatorId int
)
as
begin

	insert into Operator_PhoneBook (OperatorId, PhoneBook)
		select @operatorId, convert(xml, '<contacts />') 
			where not exists (select 1 from Operator_PhoneBook e where e.OperatorId = @operatorId)

	update Operator_PhoneBook set PhoneBook.modify('delete (/contacts/contact[Value/text()=sql:variable("@msisdn")])[1]') where OperatorId = @operatorId
	update Operator_PhoneBook set PhoneBook.modify('insert <contact Updated="true"><Name>{sql:variable("@name")}</Name><Value>{sql:variable("@msisdn")}</Value></contact> into (/contacts)[1]') where OperatorId = @operatorId
end
go