IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EditPointsWeb]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[EditPointsWeb] 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Edit Points for Web
-- =============================================
CREATE PROCEDURE [dbo].[EditPointsWeb]  
	@point_id		int,
	@x				float,
	@y				float,
	@name			nvarchar(250),
	@description	nvarchar(500),
	@type_id		int
AS
BEGIN
	
	declare @vertex_id int
	set @vertex_id = (select VERTEX_ID from WEB_POINT where POINT_ID = @point_id)

	update dbo.WEB_POINT set [NAME] = @name, [DESCRIPTION] = @description, [TYPE_ID] = @type_id where POINT_ID = @point_id

	update dbo.MAP_VERTEX set X = @x, Y = @y where VERTEX_ID = @vertex_id


END
GO
