
IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'GET_SECURITY' AND type = 'P')
    DROP PROCEDURE GET_SECURITY
GO


CREATE PROCEDURE [dbo].[GET_SECURITY] 
	@oid	int
AS

-----------------------------------------------------------------------------------
/*
-- Test
declare @oid int
set @oid = 2
*/
-----------------------------------------------------------------------------------

BEGIN
	/* OPERATOR */
	select OPERATOR_ID 
	from OPERATOR 
	where OPERATOR_ID = @oid

	/* OPERATORGROUP */
	select og.OPERATORGROUP_ID 
	from OPERATORGROUP og 
		join OPERATORGROUP_OPERATOR og_o 
			on og.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_OPERATOR */
	select og_o.* 
	from OPERATORGROUP_OPERATOR og_o 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* RIGHT */
	select RIGHT_ID, [SYSTEM] 
	from [RIGHT]

	/* RIGHT_OPERATOR */
	select r_o.* 
	from RIGHT_OPERATOR r_o 
		join OPERATOR o 
			on r_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* RIGHT_OPERATORGROUP */
	select r_og.* 
	from RIGHT_OPERATORGROUP r_og 
		join OPERATORGROUP_OPERATOR og_o 
			on r_og.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* DRIVER */
	select DRIVER_ID, DEPARTMENT 
	from DRIVER

	/* DRIVERGROUP_DRIVER */
	select * 
	from DRIVERGROUP_DRIVER 

	/* DRIVERGROUP */
	select DRIVERGROUP_ID 
	from DRIVERGROUP

	/* OPERATOR_DRIVER */
	select o_d.* 
	from OPERATOR_DRIVER o_d 
		join OPERATOR o 
			on o_d.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATOR_DRIVERGROUP */
	select o_dg.* 
	from OPERATOR_DRIVERGROUP o_dg 
		join OPERATOR o 
			on o_dg.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_DRIVER */
	select og_d.* 
	from OPERATORGROUP_DRIVER og_d 
		join OPERATORGROUP_OPERATOR og_o 
			on og_d.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_DRIVERGROUP */
	select og_dg.* 
	from OPERATORGROUP_DRIVERGROUP og_dg 
		join OPERATORGROUP_OPERATOR og_o 
			on og_dg.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* VEHICLE */
	select VEHICLE_ID, DEPARTMENT 
	from VEHICLE

	/* VEHICLEGROUP_VEHICLE */
	select * 
	from VEHICLEGROUP_VEHICLE 

	/* VEHICLEGROUP */
	select VEHICLEGROUP_ID 
	from VEHICLEGROUP

	/* OPERATOR_VEHICLE */
	select o_v.* 
	from OPERATOR_VEHICLE o_v 
		join OPERATOR o 
			on o_v.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATOR_VEHICLEGROUP */
	select o_vg.* 
	from OPERATOR_VEHICLEGROUP o_vg 
		join OPERATOR o 
			on o_vg.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_VEHICLE */
	select og_v.* 
	from OPERATORGROUP_VEHICLE og_v 
		join OPERATORGROUP_OPERATOR og_o 
			on og_v.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_VEHICLEGROUP */
	select og_vg.* 
	from OPERATORGROUP_VEHICLEGROUP og_vg 
		join OPERATORGROUP_OPERATOR og_o 
			on og_vg.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* ROUTE */
	select ROUTE_ID 
	from ROUTE

	/* ROUTEGROUP_ROUTE */
	select * 
	from ROUTEGROUP_ROUTE 

	/* ROUTEGROUP */
	select ROUTEGROUP_ID 
	from ROUTEGROUP

	/* SCHEDULE */
	select SCHEDULE_ID, ROUTE_ID, WAYOUT -- цж ░п┐┐ к фъжХ ъи╕v░с┐ д┐ пжvбдж ╕vъж ажvр ROUTE_ID
	from SCHEDULE

	/* WAYOUT */
	select WAYOUT_ID, ROUTE
	from WAYOUT

	/* OPERATOR_ROUTE */
	select o_r.* 
	from OPERATOR_ROUTE o_r 
		join OPERATOR o 
			on o_r.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATOR_ROUTEGROUP */
	select o_rg.* 
	from OPERATOR_ROUTEGROUP o_rg 
		join OPERATOR o 
			on o_rg.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_ROUTE */
	select og_r.* 
	from OPERATORGROUP_ROUTE og_r 
		join OPERATORGROUP_OPERATOR og_o 
			on og_r.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_ROUTEGROUP */
	select og_rg.* 
	from OPERATORGROUP_ROUTEGROUP og_rg 
		join OPERATORGROUP_OPERATOR og_o 
			on og_rg.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o 
			on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	-----
	/* ZONE */
	select ZONE_ID from GEO_ZONE

	/* ZONEGROUP */
	select ZONEGROUP_ID from ZONEGROUP

	/* ZONEGROUP_ZONE */
	select * from ZONEGROUP_ZONE 

	/* OPERATOR_ZONE */
	select o_z.* 
		from OPERATOR_ZONE o_z 
		join OPERATOR o on o_z.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATOR_ZONEGROUP */
	select o_zg.* 
		from OPERATOR_ZONEGROUP o_zg 
		join OPERATOR o on o_zg.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_ZONE */
	select og_z.* 
		from OPERATORGROUP_ZONE og_z 
		join OPERATORGROUP_OPERATOR og_o on og_z.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	/* OPERATORGROUP_ZONEGROUP */
	select og_zg.* 
		from OPERATORGROUP_ZONEGROUP og_zg 
		join OPERATORGROUP_OPERATOR og_o on og_zg.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	----

	-- REPORT
	select REPORT_ID from REPORT

	-- OPERATOR_REPORT
	select * from OPERATOR_REPORT 
		join OPERATOR on OPERATOR_REPORT.OPERATOR_ID = OPERATOR.OPERATOR_ID 
	where OPERATOR.OPERATOR_ID = @oid

	-- OPERATORGROUP_REPORT
	select * from OPERATORGROUP_REPORT  
		join OPERATORGROUP_OPERATOR on OPERATORGROUP_REPORT.OPERATORGROUP_ID = OPERATORGROUP_OPERATOR.OPERATORGROUP_ID 
		join OPERATOR on OPERATORGROUP_OPERATOR.OPERATOR_ID = OPERATOR.OPERATOR_ID 
	where OPERATOR.OPERATOR_ID = @oid

	--DEPARTMENT	
	select DEPARTMENT_ID from dbo.DEPARTMENT

	-- OPERATOR_DEPARTMENT
	select o_d.* from OPERATOR_DEPARTMENT o_d
		join OPERATOR o on o_d.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid

	-- OPERATORGROUP_DEPARTMENT
	select og_d.* from OPERATORGROUP_DEPARTMENT og_d 
		join OPERATORGROUP_OPERATOR og_o on og_d.OPERATORGROUP_ID = og_o.OPERATORGROUP_ID 
		join OPERATOR o on og_o.OPERATOR_ID = o.OPERATOR_ID 
	where o.OPERATOR_ID = @oid


END

