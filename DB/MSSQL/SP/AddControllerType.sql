exec CreateProcedure 'AddControllerType'
go
alter procedure AddControllerType
(
	@type_name nvarchar(50),
	@DeviceIdIsImei bit = 1,
	@SupportsPassword bit = 0,
	@AllowedToAddByCustomer bit = 1,
	@ImagePath nvarchar(250) = ''
)
as

insert into controller_type (TYPE_NAME, AllowedToAddByCustomer, DeviceIdIsImei, SupportsPassword, ImagePath)
	select @type_name, 1, @DeviceIdIsImei, @SupportsPassword, @ImagePath
		where not exists (select 1 from controller_type e where e.type_name = @type_name)

if @@ROWCOUNT = 0
	update CONTROLLER_TYPE
		set AllowedToAddByCustomer = @AllowedToAddByCustomer
		  , DeviceIdIsImei = @DeviceIdIsImei
		  , SupportsPassword = @SupportsPassword
		  , ImagePath = @ImagePath
		  where TYPE_NAME = @type_name
		    and (
				AllowedToAddByCustomer <> @AllowedToAddByCustomer or
				DeviceIdIsImei <> @DeviceIdIsImei or
				SupportsPassword <> @SupportsPassword or
				isnull(ImagePath, '') <> @ImagePath			
			)