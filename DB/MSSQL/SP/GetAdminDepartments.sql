set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAdminDepartments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAdminDepartments]
GO

CREATE PROCEDURE [dbo].[GetAdminDepartments]
	@operatorId int
AS

/*
declare @operatorId int, @departmentId int
select @operatorId = 2, @departmentId = 9;
--select * from v_operator_department_right where operator_id = 2
*/

begin
	set nocount on;

	select distinct d.DEPARTMENT_ID, d.NAME, d.ExtID, d.Description
	from v_operator_department_right o
	join department d on d.department_id = o.department_id
	where o.operator_id = @operatorId
	and o.right_id = 2 --SecurityAdministration
	order by d.DEPARTMENT_ID;
end