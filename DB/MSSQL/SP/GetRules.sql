IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRules]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetRules] 

GO

create   PROCEDURE [dbo].[GetRules]
AS
begin
	select * from [RULE]

	select * from VEHICLE_RULE 
		where XML_Condition is null 
		  and XML_Action is null

	select * from VEHICLE

	select * from VEHICLEGROUP_RULE
		where XML_Condition is null 
		  and XML_Action is null

	select * from VEHICLEGROUP

	select * from VEHICLEGROUP_VEHICLE


	select * from CONTROLLER
	select * from CONTROLLER_INFO

end