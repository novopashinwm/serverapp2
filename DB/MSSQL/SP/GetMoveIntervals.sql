/****** Object:  StoredProcedure [dbo].[GetMoveIntervals]    Script Date: 12/04/2008 13:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



----------------------------------------------------------------------------
-- цшжс┐пжши пvр ажмъшж┐д░р жът┐ъи "Lдъ┐шкиvv пк░б┐д░р TT аж ╣иш-шжъи╣"
----------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetMoveIntervals]
	@Date as DateTime,
	@operator_id int = null
AS

SET @date = dateadd(hh, -2, @date)

declare 
	@wbhDate 	as datetime, 
	@wbDate 	as datetime, 
	@wbhDateTime 	as datetime,
	-- цивд░си к а┐ш░жп v┐ъд┐║ж ░ в░╣д┐║ж кш┐╣┐д░ 
	@TimeDiff 	as int

set @wbhDateTime = @Date
set @wbhDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime, 1), 1)
set @wbDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime + 1, 1), 1)
set @TimeDiff = datediff(s, @wbhDateTime, @wbDate)

----------------------------------------------------------------------------

select distinct	
	RT.[ROUTE_ID],
	RT.[EXT_NUMBER] 	as RouteNum,
	GT.GEO_TRIP_ID,
	GS.[GEO_SEGMENT_ID],
	GS.[STATUS]		as PointStatus,
	GS.[ORDER],
	PT.[POINT_ID],
	PT.[NAME]		as PointName,
	PT.POINT_KIND_ID,
	BS.[BUSSTOP_ID],
	BS.[NAME]		as BusStop
into
	#RoutePoint
from 
	dbo.ROUTE as RT
	inner join dbo.ROUTE_TRIP as RTT on RTT.ROUTE_ID = RT.ROUTE_ID
	inner join dbo.GEO_TRIP as GT on GT.GEO_TRIP_ID = RTT.GEO_TRIP_ID
	inner join dbo.GEO_SEGMENT as GS on GS.GEO_TRIP_ID = GT.GEO_TRIP_ID
	inner join dbo.POINT as PT on PT.POINT_ID = GS.POINT
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
	left join BUSSTOP as BS on BS.BUSSTOP_ID = PT.BUSSTOP_ID
where 
	(GS.[STATUS] & 0x8) = 0x8
	and (@operator_id is null or rr.operator_id = @operator_id)
order by 
	RT.[EXT_NUMBER], GT.[GEO_TRIP_ID], GS.[ORDER]
	
--select * from #RoutePoint




select 
distinct	
	SC.SCHEDULE_ID,
	SC.EXT_NUMBER,
	T.TRIP_ID,
	T.TRIP 				as TripNum,
	T.TRIP_KIND_ID,
	sp.SCHEDULE_POINT_ID,
	isnull(SP.TIME_IN, SP.TIME_OUT) 	as TimeInOut,
	SPS.TIME_IN,				
	SPS.TIME_OUT,				
	SPS.LOG_TIME_IN,				
	SPS.LOG_TIME_OUT,
	sp.GEO_SEGMENT_ID,
	sc.ROUTE_ID
into #schedule
from dbo.SCHEDULE sc
left join v_operator_route_right rr on rr.route_id = sc.ROUTE_ID
inner join dbo.SCHEDULE_DETAIL sd on sd.SCHEDULE_ID = sc.SCHEDULE_ID
inner join TRIP t on t.SCHEDULE_DETAIL_ID = sd.SCHEDULE_DETAIL_ID
inner join SCHEDULE_POINT sp on sp.TRIP_ID = t.TRIP_ID
inner join SCHEDULE_PASSAGE sps on sps.SP_ID = sp.SCHEDULE_POINT_ID
where	
	sc.DAY_KIND_ID = dbo.DayKind(@wbDate, sc.ROUTE_ID)
	and sps.WH_ID in (select distinct WAYBILL_HEADER_ID from WAYBILL_HEADER where WAYBILL_DATE = @wbhDateTime)
	and t.TRIP_KIND_ID in (5, 6) -- was 5	
	and (@operator_id is null or rr.operator_id = @operator_id)	
order by	
	isnull(sp.[TIME_IN], sp.[TIME_OUT])
	
--select * from #schedule

select
	distinct
	rt.ROUTE_ID,
	RT.EXT_NUMBER 				as RouteNum,
    sch.SCHEDULE_ID,
	sch.EXT_NUMBER,
	sch.TRIP_ID,
	sch.TripNum,
	sch.TRIP_KIND_ID,
	sch.SCHEDULE_POINT_ID,
	sch.TimeInOut,
	RP.GEO_SEGMENT_ID,
	RP.POINT_ID,
	RP.PointName,
	RP.BUSSTOP_ID,
	RP.BUSSTOP,
	RP.PointStatus,
	RP.POINT_KIND_ID,
	sch.TIME_IN,				
	sch.TIME_OUT,				
	sch.LOG_TIME_IN,				
	sch.LOG_TIME_OUT,
	null 					as IntervalPlan,	
	null 					as IntervalFact
into
	#Interval
from #schedule sch 
inner join [ROUTE] rt on sch.ROUTE_ID = rt.ROUTE_ID
inner join #RoutePoint as RP on sch.GEO_SEGMENT_ID = RP.GEO_SEGMENT_ID
--where RP.PointStatus = 41
order by
	RT.EXT_NUMBER, 
	RP.GEO_SEGMENT_ID
	
	
--select * from #Interval

drop table #RoutePoint
drop table #schedule

update #Interval Set 
	#Interval.IntervalPlan = datediff(s, 
		(select top 1 isnull(i.TIME_IN, i.TIME_OUT) from #Interval as i where i.ROUTE_ID = #Interval.ROUTE_ID 
		and (i.GEO_SEGMENT_ID = #Interval.GEO_SEGMENT_ID OR 
		(i.GEO_SEGMENT_ID != #Interval.GEO_SEGMENT_ID and i.TRIP_KIND_ID != #Interval.TRIP_KIND_ID)) 
		and isnull(i.TIME_IN, i.TIME_OUT) < isnull(#Interval.TIME_IN, #Interval.TIME_OUT) order by isnull(i.TIME_IN, i.TIME_OUT) DESC), 
		isnull(#Interval.TIME_IN, #Interval.TIME_OUT)
		), 
	#Interval.IntervalFact = datediff(s, 
		(select top 1 isnull(i.LOG_TIME_IN, i.LOG_TIME_OUT) from #Interval as i where i.ROUTE_ID = #Interval.ROUTE_ID 
		and (i.GEO_SEGMENT_ID = #Interval.GEO_SEGMENT_ID OR 
		(i.GEO_SEGMENT_ID != #Interval.GEO_SEGMENT_ID and i.TRIP_KIND_ID != #Interval.TRIP_KIND_ID))  
		and isnull(i.LOG_TIME_IN, i.LOG_TIME_OUT) < isnull(#Interval.LOG_TIME_IN, #Interval.LOG_TIME_OUT) order by isnull(i.LOG_TIME_IN, i.LOG_TIME_OUT) DESC), 
		isnull(#Interval.LOG_TIME_IN, #Interval.LOG_TIME_OUT)
		) 

--select * from #Interval where RouteNum = 11 and PointName like('%УСЗН%') order by isnull([TIME_IN], [TIME_OUT])


--ц┐вжvжъиъ
select distinct
	i.RouteNum,
	--i.GEO_SEGMENT_ID,
	
	--i.POINT_ID,
	
	i.BUSSTOP as 'PointName',--i.PointName,
	i.BUSSTOP_ID,
	i.BUSSTOP,
	--
	min(i_00_07.IntervalPlan)	as Plan_00_07_Min,
	max(i_00_07.IntervalPlan)	as Plan_00_07_Max,
	min(i_00_07.IntervalFact)	as Fact_00_07_Min,
	max(i_00_07.IntervalFact)	as Fact_00_07_Max,
	--
	min(i_07_09.IntervalPlan)	as Plan_07_09_Min,
	max(i_07_09.IntervalPlan)	as Plan_07_09_Max,
	min(i_07_09.IntervalFact)	as Fact_07_09_Min,
	max(i_07_09.IntervalFact)	as Fact_07_09_Max,
	--
	min(i_09_16.IntervalPlan)	as Plan_09_16_Min,
	max(i_09_16.IntervalPlan)	as Plan_09_16_Max,
	min(i_09_16.IntervalFact)	as Fact_09_16_Min,
	max(i_09_16.IntervalFact)	as Fact_09_16_Max,
	--
	min(i_16_19.IntervalPlan)	as Plan_16_19_Min,
	max(i_16_19.IntervalPlan)	as Plan_16_19_Max,
	min(i_16_19.IntervalFact)	as Fact_16_19_Min,
	max(i_16_19.IntervalFact)	as Fact_16_19_Max,
	--
	min(i_19_00.IntervalPlan)	as Plan_19_00_Min,
	max(i_19_00.IntervalPlan)	as Plan_19_00_Max,
	min(i_19_00.IntervalFact)	as Fact_19_00_Min,
	max(i_19_00.IntervalFact)	as Fact_19_00_Max
from
	#Interval as i
	-- пж 07:00
	left join #Interval as i_00_07 
		on i_00_07.ROUTE_ID = i.ROUTE_ID and i_00_07.SCHEDULE_ID = i.SCHEDULE_ID and i_00_07.SCHEDULE_POINT_ID = i.SCHEDULE_POINT_ID
		and i_00_07.TimeInOut <= 25200 
	-- c 07:00 пж 09:00
	left join #Interval as i_07_09 
		on i_07_09.ROUTE_ID = i.ROUTE_ID and i_07_09.SCHEDULE_ID = i.SCHEDULE_ID and i_07_09.SCHEDULE_POINT_ID = i.SCHEDULE_POINT_ID
		and i_07_09.TimeInOut between 25200 and 32400
	-- c 09:00 пж 16:00
	left join #Interval as i_09_16 
		on i_09_16.ROUTE_ID = i.ROUTE_ID and i_09_16.SCHEDULE_ID = i.SCHEDULE_ID and i_09_16.SCHEDULE_POINT_ID = i.SCHEDULE_POINT_ID
		and i_09_16.TimeInOut between 32400 and 57600
	-- c 16:00 пж 19:00
	left join #Interval as i_16_19 
		on i_16_19.ROUTE_ID = i.ROUTE_ID and i_16_19.SCHEDULE_ID = i.SCHEDULE_ID and i_16_19.SCHEDULE_POINT_ID = i.SCHEDULE_POINT_ID
		and i_16_19.TimeInOut between 57600 and 68400
	-- ажмv┐ 19:00
	left join #Interval as i_19_00 
		on i_19_00.ROUTE_ID = i.ROUTE_ID and i_19_00.SCHEDULE_ID = i.SCHEDULE_ID and i_19_00.SCHEDULE_POINT_ID = i.SCHEDULE_POINT_ID
		and i_19_00.TimeInOut >= 68400  
group by 
	i.BUSSTOP_ID,i.RouteNum,
	--i.GEO_SEGMENT_ID,
	--i.POINT_ID,
	--i.PointName,
	
	i.BUSSTOP
order by
	i.RouteNum, i.BUSSTOP --, r.BUSSTOP


drop table #Interval
