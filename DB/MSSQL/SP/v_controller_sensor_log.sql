IF (OBJECT_ID(N'[dbo].[v_controller_sensor_log]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[v_controller_sensor_log];
GO

CREATE VIEW [dbo].[v_controller_sensor_log]
AS
	SELECT
		[Vehicle_ID]   = leg.[Vehicle_ID],
		[SensorLegend] = leg.[Sensor_Legend],
		[Log_Time]     = ltm.[Log_Time],
		[Value]        =
		(
			SELECT
				SUM(csm.[Multiplier] * inp.[Value] + csm.[Constant])
			FROM
			(
				SELECT DISTINCT csm.[Sensor_Number]
				FROM [dbo].[v_controller_sensor_map] csm WITH (NOEXPAND)
				WHERE csm.[Vehicle_ID]    = leg.[Vehicle_ID]
				AND   csm.[Sensor_Legend] = leg.[Sensor_Legend]
			) usd
				CROSS APPLY
				(
					SELECT TOP(1) [Value]
					FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK)
					WHERE [Vehicle_ID]  = ltm.[Vehicle_ID]
					AND   [Number]      = usd.[Sensor_Number]
					AND   [Log_Time]   <= ltm.[Log_Time]
					ORDER BY [Log_Time] DESC
				) inp
			JOIN [dbo].[v_controller_sensor_map] csm WITH (NOEXPAND)
				ON  (csm.[Vehicle_ID]    = ltm.[Vehicle_ID])
				AND (csm.[Sensor_Legend] = leg.[Sensor_Legend])
				AND (csm.[Sensor_Number] = usd.[Sensor_Number])
				AND (csm.[Min_Value] IS NULL OR csm.[Min_Value] <= inp.[Value])
				AND (csm.[Max_Value] IS NULL OR csm.[Max_Value] >= inp.[Value])
		)
	FROM [dbo].[v_controller_sensor_legend] leg WITH (NOEXPAND)
		JOIN [dbo].[Log_Time] ltm WITH (NOLOCK)
			ON ltm.[Vehicle_ID] = leg.[Vehicle_ID]
GO