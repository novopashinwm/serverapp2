set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetDataForRuleProcessor' and type='p'))
	drop procedure dbo.GetDataForRuleProcessor;
go

