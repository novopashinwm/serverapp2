/****** Object:  StoredProcedure [dbo].[GetDataForJournals]    Script Date: 07/11/2010 12:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[GetDataForJournals]
	@operator_id int = null
AS
begin

	select distinct d.* 
	from DEPARTMENT d
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where @operator_id is null 
	or (dr.operator_id= @operator_id and dr.right_id=104)

	select distinct d.* 
	from CUSTOMER d
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where @operator_id is null 
	or (dr.operator_id= @operator_id and dr.right_id=104)

	select * from ORDER_TYPE

end









