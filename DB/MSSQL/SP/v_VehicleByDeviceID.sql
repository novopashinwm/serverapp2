if not exists (select * from sys.views where name = 'v_VehicleByDeviceID')
begin
exec sp_executesql N'
	create view v_VehicleByDeviceID 
		with schemabinding 
		as 
		select N = 0 where 1=0
'
end
go

alter view v_VehicleByDeviceID
with schemabinding
as
select 
	  c.Vehicle_ID
	, c.CONTROLLER_ID
	, Device_ID = 
		case 
			when 	ct.DeviceIdStartIndex is not null and
					ct.DeviceIdLength is not null and
					len(ci.Device_ID ) >= ct.DeviceIdLength + ct.DeviceIdStartIndex - 1
			then	substring(ci.Device_ID, ct.DeviceIdStartIndex, ct.DeviceIdLength)
			else 	ci.Device_ID
		end
	from dbo.Controller c
	join dbo.Controller_Info ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
	join dbo.Controller_Type ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
	where len(ci.DEVICE_ID) > 0

go

create unique clustered index PK_v_VehicleByDeviceID on v_VehicleByDeviceID (Device_ID)
