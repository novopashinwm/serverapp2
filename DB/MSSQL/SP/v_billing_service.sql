if exists (
	select *
		from sys.views
		where name = 'v_billing_service'
)
	drop view v_billing_service;
go

create view v_billing_service
as
	select Phone = pc.Value
		, Service_Name = bst.Name
		, o.OPERATOR_ID
		, o.LOGIN
		, o.PASSWORD
		, Department_ID = d.Department_ID
		, Department_Name = d.NAME
		, Department_ExtID = d.ExtID
		, a.Terminal_Device_Number
		, bs.StartDate
		, bs.EndDate
		, Billing_Service_ID = bs.ID
		, Msisdn = pc.Value
		, c.VEHICLE_ID
		, Asid_ID = bs.Asid_ID
		from Billing_Service bs
		join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
		left outer join Asid a on a.ID = bs.Asid_ID 
		left outer join Contact ac on ac.ID = a.Contact_ID
		left outer join Contact pc on pc.ID = ac.Demasked_ID
		left outer join Operator o on o.OPERATOR_ID = a.Operator_ID
		left outer join Department d on d.DEPARTMENT_ID = a.Department_ID
		left outer join MLP_Controller mc on mc.Asid_ID = bs.Asid_ID
		left outer join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID