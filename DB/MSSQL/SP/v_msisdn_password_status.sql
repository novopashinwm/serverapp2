﻿IF (OBJECT_ID(N'[dbo].[v_msisdn_password_status]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[v_msisdn_password_status];
GO

CREATE VIEW [dbo].[v_msisdn_password_status]
AS
SELECT
	 [Тип]              = ct.[TYPE_NAME]
	,[IMEI / номер]     = CONVERT(varchar(32), ci.[DEVICE_ID])
	,[Vehicle name]     = v.[GARAGE_NUMBER]
	,[Абонент ]         = ISNULL(d.[NAME], '')
	,[Наличие телефона] = CASE WHEN LEN(c.[PHONE]) >= 10 THEN 1 ELSE 0 END
	,[Наличие пароля]   =
		CASE
			WHEN ct.[SupportsPassword] = 0 THEN NULL
			WHEN LEN(ci.[PASSWORD])    > 0 THEN 1
			ELSE 0
		END
	FROM [dbo].[CONTROLLER] c
		JOIN [dbo].[CONTROLLER_INFO] ci
			ON ci.[CONTROLLER_ID] = c.[CONTROLLER_ID]
		JOIN [dbo].[VEHICLE] v
			ON v.[VEHICLE_ID] = c.[VEHICLE_ID]
		JOIN [dbo].[CONTROLLER_TYPE] ct
			ON ct.[CONTROLLER_TYPE_ID] = c.[CONTROLLER_TYPE_ID]
		LEFT OUTER JOIN [dbo].[DEPARTMENT] d
			ON d.[DEPARTMENT_ID] = v.[DEPARTMENT]
		OUTER APPLY
		(
			SELECT TOP(1)
				[InsertTime]
			FROM [dbo].[Log_Time] WITH (NOLOCK)
			WHERE [Vehicle_ID] = c.[Vehicle_ID]
			ORDER BY [InsertTime] DESC
		) last_lt
	WHERE 1=1
	AND ci.[DEVICE_ID] IS NOT NULL
	AND ci.[DEVICE_ID] <> 0x00
	AND c.[VEHICLE_ID] NOT IN (SELECT [FakeVehicleID] FROM [dbo].[EMULATOR])
	AND c.[VEHICLE_ID] NOT IN (SELECT [Vehicle_ID]    FROM [dbo].[Tracker] WHERE [Vehicle_ID] IS NOT NULL)
	AND (d.[NAME] IS NULL OR d.[NAME] <> 'SitronicsIndia')
	AND (last_lt.[InsertTime] > GETUTCDATE() - 90)
GO