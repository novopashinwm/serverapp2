﻿IF (OBJECT_ID(N'[dbo].[v_vehicle_log_actuality]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[v_vehicle_log_actuality];
GO

CREATE VIEW [dbo].[v_vehicle_log_actuality]
AS
	--Машины, по которым строится отчёт
	WITH vids ([vehicle_id]) AS
	(
		SELECT DISTINCT
			v.[VEHICLE_ID]
		FROM [dbo].[DEPARTMENT] d
			JOIN [dbo].[VEHICLE] v
				ON v.[DEPARTMENT] = d.[DEPARTMENT_ID]
					JOIN [dbo].[VEHICLE_KIND] k
						ON v.[VEHICLE_KIND_ID] = v.[VEHICLE_KIND_ID]
		WHERE
		(d.[Type] = 0 OR d.[Type] IS NULL) --Корпоративный абонент
		AND k.[Name]     IN ('автобус', 'легковой', 'грузовой', 'прицеп')
		AND d.[Name] NOT IN ('SitronicsIndia', 'Гость')
	)
	--Информация о последних данных
	SELECT
		v.[vehicle_id],
		[Type] = 'Insert',
		[Time] = l.[Time]
	FROM vids v
		CROSS APPLY
		(
			SELECT TOP(1)
				[Time] = [InsertTime#Last]
			FROM [dbo].[Log_Time#Lasts] WITH (NOLOCK)
			WHERE [Vehicle_ID] = v.[vehicle_id]
			ORDER BY [InsertTime#Last] DESC
		) l
	UNION ALL
	SELECT
		v.[vehicle_id],
		[Type] = 'AnyLog',
		[Time] = l.[Time]
	FROM vids v
		CROSS APPLY
		(
			SELECT TOP(1)
				[Time] = [dbo].[GetDateFromInt]([Log_Time#Last])
			FROM [dbo].[Log_Time#Lasts] WITH (NOLOCK)
			WHERE [Vehicle_ID] = v.[vehicle_id]
			ORDER BY [Log_Time#Last] DESC
		) l
	UNION ALL
	SELECT
		v.[vehicle_id],
		[Type] = 'Geo',
		[Time] = l.[Time]
	FROM vids v
		CROSS APPLY
		(
			SELECT TOP(1)
				[Time] = [dbo].[GetDateFromInt]([LOG_TIME#Last])
			FROM [dbo].[Geo_Log#Lasts] WITH (NOLOCK)
			WHERE [Vehicle_ID] = v.[vehicle_id]
			ORDER BY [LOG_TIME#Last] DESC
		) l
	UNION ALL
	SELECT
		v.[vehicle_id],
		[Type] = 'CAN',
		[Time] = l.[Time]
	FROM vids v
		CROSS APPLY
		(
			SELECT TOP(1)
				[Time] = [dbo].[GetDateFromInt]([Log_Time])
			FROM [dbo].[CAN_INFO] WITH (NOLOCK)
			WHERE [Vehicle_ID] = v.[vehicle_id]
			ORDER BY [LOG_TIME] DESC
		) l
	UNION ALL
	SELECT
		v.[vehicle_id],
		[Type] = 'Fuel',
		[Time] = l.[Time]
	FROM vids v
		CROSS APPLY
		(
			SELECT TOP(1)
				[Time] = [dbo].[GetDateFromInt](L.[Log_Time#Last])
			FROM [dbo].[v_controller_sensor_map] M
				INNER JOIN [dbo].[Controller_Sensor_Log#Lasts] L WITH (NOLOCK)
					ON  L.[Vehicle_ID] = M.[Vehicle_ID]
					AND L.[Number]     = M.[Sensor_Number]
			WHERE M.[Vehicle_ID]    = v.[vehicle_id]
			AND   M.[Sensor_Legend] = (SELECT [Number] FROM [dbo].[CONTROLLER_SENSOR_LEGEND] WHERE [NAME] = 'Fuel')
			ORDER BY L.[Log_Time#Last] DESC
		) l
GO