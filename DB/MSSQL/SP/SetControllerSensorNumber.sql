IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetControllerSensorNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SetControllerSensorNumber]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SetControllerSensorNumber]
(
	@CONTROLLER_TYPE_NAME nvarchar(50),
	@NUMBER int,
	@DESCRIPTION nvarchar(512),
	@CONTROLLER_SENSOR_TYPE_ID int,
	@DEFAULT_VALUE bigint,
	@MAX_VALUE bigint,
	@MIN_VALUE bigint,
	@BITS int
)
as
begin
	declare @CONTROLLER_TYPE_ID int;
	select @CONTROLLER_TYPE_ID = controller_type_id from controller_type where [type_name] = @CONTROLLER_TYPE_NAME;

	declare @CONTROLLER_SENSOR_ID int;
	select @CONTROLLER_SENSOR_ID = CONTROLLER_SENSOR_ID 
	from controller_sensor 
	where CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID and NUMBER = @NUMBER;
	
	if (@CONTROLLER_SENSOR_ID is not null)
		begin
			update controller_sensor
			set DESCRIPT = @DESCRIPTION,
				CONTROLLER_SENSOR_TYPE_ID = @CONTROLLER_SENSOR_TYPE_ID,
				DEFAULT_VALUE = @DEFAULT_VALUE,
				MAX_VALUE = @MAX_VALUE,
				MIN_VALUE = @MIN_VALUE,
				BITS = @BITS 
			where CONTROLLER_SENSOR_ID = @CONTROLLER_SENSOR_ID;
		end
	else
		begin
			insert into controller_sensor(CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, 
			NUMBER, MAX_VALUE, MIN_VALUE, 
			DEFAULT_VALUE, BITS, DESCRIPT)
			values(@CONTROLLER_TYPE_ID, @CONTROLLER_SENSOR_TYPE_ID,
			@NUMBER, @MAX_VALUE, @MIN_VALUE, 
			@DEFAULT_VALUE, @BITS, @DESCRIPTION);
			
			select @CONTROLLER_SENSOR_ID = @@IDENTITY;
		end
		
	select 	@CONTROLLER_SENSOR_ID;
end



GO


