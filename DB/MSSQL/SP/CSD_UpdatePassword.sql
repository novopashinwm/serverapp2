IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSD_UpdatePassword]') AND type in (N'P', N'PC'))
   DROP PROCEDURE dbo.CSD_UpdatePassword
GO

CREATE PROCEDURE [dbo].[CSD_UpdatePassword]
	@guid uniqueidentifier,
	@password nvarchar(50)
AS
	if exists (select * from [CSD].dbo.CSD where OPERATOR_GUID = @guid)
		begin
			update [CSD].dbo.CSD
			set OPERATOR_PASSWORD = @password
			where OPERATOR_GUID = @guid
		end
