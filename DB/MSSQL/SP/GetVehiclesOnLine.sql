/****** Object:  StoredProcedure [dbo].[GetVehiclesOnLine]    Script Date: 07/15/2010 10:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
--=Ёў¤Ёвї¤°ї: ж■√║вї¤°ї ЇЁ¤¤v┐ Ї√а ■квїкЁ "TЄїЇї¤°а ■ Єv ║╕·ї TT ¤Ё √°¤°ж"
--------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetVehiclesOnLine]
	@ReportDateTime	datetime,
	@operator_id int = null		
AS

--------------------------------------------------------------------------------------------
/*
--кї╕к
declare @ReportDateTime	datetime
set @ReportDateTime = '2006-09-19 06:00:00'
--set @ReportDateTime = '2007-06-18 20:00:00'
		--8 --иїўїиЄ
		--9 --ўЁ·Ёў
		--11 -- и■╕к■∙  ■ иї№■¤к║
		--12 --иї№■¤к ёїў Є■Ї°кї√а
		--13 -- ёїў ЄvїўЇЁ
		--17 --жїўїиЄ ёїў №Ё░°¤v
*/
--------------------------------------------------------------------------------------------


--+и║  v TT
select distinct --VG.* 
	VG.VEHICLEGROUP_ID,
	VG.[NAME] 	as VehicleGroup,
	0 		as VehiclesCount,
	0 		as PlannedVehiclesCount,
	0 		as FactualVehiclesCount,
	0 		as OnLineVehiclesCount,
	0 		as WithoutExitVehiclesCount,
	0 		as InRepairVehiclesCount,
	0 		as ReservedVehiclesCountMorning,
	0 		as ReservedVehiclesCountEvening,
	0 		as OrderedVehiclesCount,
	0 		as PlannedSchedulesCount,
	0 		as FactualSchedulesCount,
	0 		as OpenExitsCount,
	0 		as PDTechnicalOutsCount,
	0 		as PDPlannedTripsCount,
	0 		as PDLostTripsCount
into 
	#VehicleGroup 
from 
	VEHICLEGROUP as VG 
	left join v_operator_vehicle_groups_right rr on rr.vehiclegroup_id = VG.VEHICLEGROUP_ID
	where @operator_id is null or rr.operator_id = @operator_id

--select * from #VehicleGroup order by VehicleGroup


--TC ° ╕■╕к■а¤°ї
create table #VehiclesByState(	
	--id int identity primary key, 
	WaybillNumber		varchar(50),
	GarageNumber		varchar(50),
	VehicleGroupID		int,
	DriverExtNumber		varchar(50),
	DriverShortName		varchar(200),
	DriverName		varchar(200),
	WorkStartTime		datetime,
	WorkFinishTime		datetime,
	State			int
	)
--8 - иїўїиЄ
insert into #VehiclesByState exec dbo.VehiclesListByStateGet @ReportDateTime, 0, 8
--9 - ўЁ·Ёў
insert into #VehiclesByState exec dbo.VehiclesListByStateGet @ReportDateTime, 0, 9
--11 -  и■╕к■∙  ■ иї№■¤к║ 
insert into #VehiclesByState exec dbo.VehiclesListByStateGet @ReportDateTime, 0, 11
--12 - иї№■¤к ёїў Є■Ї°кї√а
insert into #VehiclesByState exec dbo.VehiclesListByStateGet @ReportDateTime, 0, 12
--13 - ёїў ЄvїўЇЁ
insert into #VehiclesByState exec dbo.VehiclesListByStateGet @ReportDateTime, 0, 13   	

--select * from #VehiclesByState order by VehicleGroupID, State, GarageNumber
--select count(*) from #VehiclesByState where State = 8
--select count(*) from #VehiclesByState where State = 12

update #VehicleGroup set
	--¤Ё ёЁ√Ё¤╕ї
	#VehicleGroup.VehiclesCount = 
		(select count(*) from dbo.VEHICLEGROUP_VEHICLE as VGV 
			left join VEHICLE as V on V.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where VGV.VEHICLEGROUP_ID = #VehicleGroup.VEHICLEGROUP_ID and V.[VEHICLE_STATUS_ID] = 1 ), 	--T °╕Ё¤¤vї TT ¤ї иЁ╕╕№Ёки°ЄЁжк╕а.
	--ўЁ √Ё¤°и■ЄЁ¤¤vї
	#VehicleGroup.PlannedVehiclesCount = 
		(select count(*) from dbo.WAYBILL_HEADER as WBH
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 1 and WBH.[WAYBILL_DATE] <= @ReportDateTime 
			and WBH.[CANCELLED] != 1				
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID ),
	--¤Ё √°¤°°
	#VehicleGroup.OnLineVehiclesCount = 
		(select count(*) from dbo.WAYBILL_HEADER as WBH
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			inner join dbo.WB_TRIP as WBT on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 1 and WBH.[WAYBILL_DATE] <= @ReportDateTime 
			and WBH.[CANCELLED] != 1	
			and WBT.[BEGIN_TIME] <= @ReportDateTime	and WBT.[END_TIME] >= @ReportDateTime	--Tvё°иЁжк╕а к■√╣·■ иї∙╕v,  и■°ўЄїЇї¤¤vї Є ║·ЁўЁ¤¤■ї Єиї№а
			and WBT.[TRIP_KIND_ID] in (1, 5, 6, 7, 9)	--Tvё°иЁжк╕а к■√╣·■ иї∙╕v, ■к¤■╕аХ°ї╕а · иЁё■кї ¤Ё √°¤°°
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID ),
	--ёїў ЄvїўЇЁ 13
	#VehicleGroup.WithoutExitVehiclesCount = (select count(*) from #VehiclesByState as S where S.VehicleGroupID = #VehicleGroup.VEHICLEGROUP_ID and S.State = 13),
	-- и■╕к■∙  ■ иї№■¤к║ 11 иї№■¤к ёїў Є■Ї°кї√а 12
	#VehicleGroup.InRepairVehiclesCount = (select count(*) from #VehiclesByState as S where S.VehicleGroupID = #VehicleGroup.VEHICLEGROUP_ID and S.State in (11, 12)),
	--иїўїиЄ 8 
	#VehicleGroup.ReservedVehiclesCountMorning = (select count(*) from #VehiclesByState as S where S.VehicleGroupID = #VehicleGroup.VEHICLEGROUP_ID and S.State = 8),
	--#VehicleGroup.ReservedVehiclesCountEvening = ,
	--ўЁ·Ёў 9
	#VehicleGroup.OrderedVehiclesCount = (select count(*) from #VehiclesByState as S where S.VehicleGroupID = #VehicleGroup.VEHICLEGROUP_ID and S.State = 9),
	--ўЁ √Ё¤°и■ЄЁ¤¤vї Єv┐■Їv
	#VehicleGroup.PlannedSchedulesCount = 
		(select count(distinct S.SCHEDULE_ID) from SCHEDULE as S
			--1
			--inner join dbo.SCHEDULE_DETAIL as SCD on SCD.[SCHEDULE_ID] = S.[SCHEDULE_ID]
			--inner join dbo.TRIP as TR on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]
			--left join dbo.WB_TRIP as WBT on WBT.[TRIP_ID] = TR.[TRIP_ID]
			--inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			--2
			inner join dbo.WAYBILL_HEADER as WBH on S.SCHEDULE_ID = WBH.SCHEDULE_ID
			--
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 1 and WBH.[WAYBILL_DATE] <= @ReportDateTime 
			and WBH.[CANCELLED] != 1	
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID ),
	--пЁ·к°вї╕·°ї Єv┐■Їv
	#VehicleGroup.FactualSchedulesCount = 
		(select count(distinct S.SCHEDULE_ID) from SCHEDULE as S
			inner join dbo.SCHEDULE_DETAIL as SCD on SCD.[SCHEDULE_ID] = S.[SCHEDULE_ID]
			inner join dbo.TRIP as TR on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]
			inner join dbo.WB_TRIP as WBT on WBT.[TRIP_ID] = TR.[TRIP_ID]
			inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 1 and WBH.[WAYBILL_DATE] <= @ReportDateTime 
			and WBH.[CANCELLED] != 1	
			and WBT.[BEGIN_TIME] <= @ReportDateTime	and WBT.[END_TIME] >= @ReportDateTime	--Tvё°иЁжк╕а к■√╣·■ иї∙╕v,  и■°ўЄїЇї¤¤vї Є ║·ЁўЁ¤¤■ї Єиї№а
			and WBT.[TRIP_KIND_ID] in (1, 2, 3, 4, 5, 6, 7, 9)	--Tvё°иЁжк╕а к■√╣·■ иї∙╕v, ■к¤■╕аХ°ї╕а · иЁё■кї ¤Ё √°¤°°
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID ),
	--╕┐■Ї  ■ кї┐¤°вї╕·■∙  и°в°¤ї ўЁ  иїЇvЇ║Х°∙ Її¤╣
	#VehicleGroup.PDTechnicalOutsCount = 
		(select count(distinct WBH.WAYBILL_HEADER_ID) from dbo.WAYBILL_HEADER as WBH
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			inner join dbo.WB_TRIP as WBT on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 2 and WBH.[WAYBILL_DATE] <= @ReportDateTime - 1 
			and WBH.[CANCELLED] != 1	
			and WBT.[TRIP_KIND_ID] in (11, 12)	--Tvё°иЁжк╕а к■√╣·■ иї∙╕v, ■к¤■╕аХ°ї╕а · иЁё■кї ¤Ё √°¤°°
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID ),
	--ўЁЇЁ¤¤vї иї∙╕v ўЁ  иїЇvЇ║Х°∙ Її¤╣
	#VehicleGroup.PDPlannedTripsCount = 
		(select count(distinct TR.TRIP_ID) from dbo.TRIP as TR
			--1
			--left join dbo.WB_TRIP as WBT on WBT.TRIP_ID = TR.TRIP_ID
			--left join dbo.WAYBILL_HEADER as WBH on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID
			--left join dbo.VEHICLEGROUP_VEHICLE as VGV on VGV.[VEHICLE_ID] = WBH.[VEHICLE_ID]
			--where WBH.[WAYBILL_DATE] > @ReportDateTime - 2 and WBH.[WAYBILL_DATE] <= @ReportDateTime - 1 
			--and WBH.[CANCELLED] != 1
			--2
			--inner join dbo.SCHEDULE_DETAIL as SCD on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]
			--inner join dbo.SCHEDULE as SC on SCD.[SCHEDULE_ID] = SC.[SCHEDULE_ID]
			--inner join dbo.ROUTE as R on SC.[ROUTE_ID] = R.[ROUTE_ID]
			--where SC.DAY_KIND_ID = dbo.DayKind(@ReportDateTime - 1, R.ROUTE_ID)
			--3
			inner join dbo.SCHEDULE_DETAIL as SCD on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]
			inner join dbo.SCHEDULE as SC on SCD.[SCHEDULE_ID] = SC.[SCHEDULE_ID]
			inner join dbo.WAYBILL_HEADER as WBH on SC.SCHEDULE_ID = WBH.SCHEDULE_ID
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 2 and WBH.[WAYBILL_DATE] <= @ReportDateTime - 1 
			and WBH.[CANCELLED] != 1
			--
			and TR.TRIP_KIND_ID in (5, 6)					
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID )/*,
	-- ■кїиа¤¤vї иї∙╕v ўЁ  иїЇvЇ║Х°∙ Її¤╣ - ўЁЄїЇї¤v Є ¤їЁ¤║√√°и■ЄЁ¤¤v┐ жT ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║ ° ¤ї °№їжк  ■Ї¤акv∙  їиЄv∙ ё°к Є  ■√ї STATUS.
	#VehicleGroup.PDLostTripsCount = 
		(select count(distinct TR.TRIP_ID) from dbo.TRIP as TR
			left join dbo.WB_TRIP as WBT on WBT.TRIP_ID = TR.TRIP_ID
			inner join dbo.WAYBILL_HEADER as WBH on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on VGV.[VEHICLE_ID] = WBH.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 2 and WBH.[WAYBILL_DATE] <= @ReportDateTime - 1 
			and WBH.[CANCELLED] != 1
			and TR.TRIP_KIND_ID in (5, 6)		
			--=ї Єvё°иЁжк╕а иї∙╕v ╕■ ╕кЁк║╕■№ "Єv ■√¤ї¤", "ўЁёиЁ·■ЄЁ¤" ° "Єv ■√¤ї¤ ╕ ёиЁ·■№".
			and (WBT.[STATUS] & 0x1) != 1				
			and (WBT.[STATUS] & 0x2) != 1
			and (WBT.[STATUS] & 0x4) != 1
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID )*/

--Єvв°╕√аї№vї  ЁиЁ№їкиv
update #VehicleGroup set
	-- LЁ·к = ёЁ√Ё¤╕ - иї№■¤к - ёїў ЄvїўЇЁ
	#VehicleGroup.FactualVehiclesCount = (#VehicleGroup.VehiclesCount - #VehicleGroup.InRepairVehiclesCount - #VehicleGroup.WithoutExitVehiclesCount),
	--■к·иvкvї Єv┐■Їv = ўЁ √Ё¤°и■ЄЁ¤¤vї - пЁ·к°вї╕·°ї
	#VehicleGroup.OpenExitsCount = (#VehicleGroup.PlannedSchedulesCount - #VehicleGroup.FactualSchedulesCount)
	-- ■кїиа¤¤vї иї∙╕v ўЁ  иїЇvЇ║Х°∙ Її¤╣ 
	,#VehicleGroup.PDLostTripsCount = (#VehicleGroup.PDPlannedTripsCount
		-- °Її¤к°п°·Ёк■иv ўЁ·иvкv┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а
		- (select count(distinct WBT.TRIP_ID) from dbo.WB_TRIP as WBT
			inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			inner join dbo.VEHICLEGROUP_VEHICLE as VGV on WBH.[VEHICLE_ID] = VGV.[VEHICLE_ID]
			where WBH.[WAYBILL_DATE] > @ReportDateTime - 2 and WBH.[WAYBILL_DATE] <= @ReportDateTime - 1 
			and WBH.[CANCELLED] != 1
			and WBT.TRIP_KIND_ID in (5, 6)	
			and WBT.WAYBILL_ID is not null
			and VGV.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID )
		) 


	


select * from #VehicleGroup order by VehicleGroup


drop table #VehiclesByState
drop table #VehicleGroup

