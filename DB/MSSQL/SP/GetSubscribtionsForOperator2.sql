IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetSubscribtionsForOperator2' 
)
   DROP PROCEDURE [dbo].GetSubscribtionsForOperator2
GO
  
CREATE PROCEDURE [dbo].[GetSubscribtionsForOperator2]   
 @operatorID int  
AS  
BEGIN  
   
 --select @operatorID  
select * from operator where operator_id = @operatorID  
select * from email where operator_id = @operatorID  
--select distinct schedulerqueue_id from email_schedulerqueue where email_id in (select email_id from email where operator_id = @operatorID)  
select * from email_schedulerqueue where email_id in (select email_id from email where operator_id = @operatorID)  
  
select se.*, r.REPORT_GUID
from schedulerevent se  
join [Report] r on r.REPORT_ID = se.Report_Id
where se.schedulerevent_id in (  
 select sq.schedulerevent_id  
 from schedulerqueue sq  
 where sq.schedulerqueue_id in (  
  select distinct schedulerqueue_id from email_schedulerqueue where email_id in (select email_id from email where operator_id = @operatorID)  
 )  
)  
  
select * from schedulerqueue sq where sq.schedulerqueue_id in (  
 select distinct schedulerqueue_id from email_schedulerqueue where email_id in (select email_id from email where operator_id = @operatorID)  
)  
order by sq.nearest_time  
END  
  
