set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetNewLogTimes' and type='p'))
	drop procedure dbo.GetNewLogTimes;
go

create procedure dbo.GetNewLogTimes
	@vehicle_ids varchar(max),
	@max_datetime    datetime,
	@debug bit = NULL
as
begin

	if isnull(@debug, 0) = 0
		set nocount on;

	create table #vehicle (Vehicle_ID int primary key clustered);
	insert into #vehicle 
		select number
			from dbo.split_int(@vehicle_ids)

	--OUTPUT: new log times
	select 
		lt.Vehicle_ID,
		lt.Log_Time
		from dbo.Vehicle_Rule_Processing vrp 
		join dbo.Log_Time lt (nolock) on 
				lt.Vehicle_ID = vrp.Vehicle_ID
			and vrp.LastTime < lt.InsertTime and lt.InsertTime <= @max_datetime
		where vrp.Vehicle_ID in (select v.Vehicle_ID from #vehicle v)
		order by lt.Vehicle_ID, lt.Log_Time
			
	drop table #vehicle

end