IF (OBJECT_ID(N'[dbo].[UpdateInsertTime]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[UpdateInsertTime];
GO

CREATE PROCEDURE [dbo].[UpdateInsertTime]
(
	@vehicle_id int,
	@log_time int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @utcNow datetime = GETUTCDATE()

	DECLARE @vrpNow datetime =
	(
		SELECT [CurrentTime]
		FROM [dbo].[Vehicle_Rule_Processing] WITH (NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [Host_ID]    IS NOT NULL
	);

	IF (@utcNow <= @vrpNow)
		SET @utcNow = DATEADD(second, 1, @vrpNow);

	UPDATE [dbo].[Log_Time]
		SET [InsertTime] = @utcNow
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [Log_Time]   = @log_time;

	EXEC [dbo].[Log_Time#Lasts_Update]
		@vehicle_id  = @vehicle_id,
		@log_time    = @log_time,
		@insert_time = @utcNow;
END