if exists (
	select * from sys.procedures where name = 'AddSensorLegend'
)
	drop procedure AddSensorLegend
go

create procedure AddSensorLegend
(
	@number		int,
	@name		nvarchar(50),
	@digital	bit
)
as

declare @controller_sensor_type_id int = case @digital when 1 then 2 else 1 end

insert into controller_sensor_legend (Number, Name, CONTROLLER_SENSOR_TYPE_ID)
	select @number, @name, case @digital when 1 then 2 else 1 end
		where not exists (select 1 from CONTROLLER_SENSOR_LEGEND e where e.Number = @number)

update CONTROLLER_SENSOR_LEGEND 
	set NAME = @name
	  , CONTROLLER_SENSOR_TYPE_ID = @controller_sensor_type_id
	where Number = @number and 
		  ( Name <> @name or CONTROLLER_SENSOR_TYPE_ID <> @controller_sensor_type_id)
