IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSensorValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetSensorValues]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[GetSensorValues]  
 @vehicleIds as id_list_table readonly,  
 @fromInt int,  
 @toInt int,  
 @sensorLegendId int,  
 @debug int = 0  
AS  
begin  
 set nocount on;  
  
select csl.Vehicle_ID, csl.log_time, csl.Value
from v_controller_sensor_log csl
where 
	vehicle_id in (select id from @vehicleIds)
	and SensorLegend = @sensorLegendId
	and log_time between @fromInt and @toInt
	and value is not null
order by csl.Vehicle_ID, csl.log_time

   
end  
  