if exists (select * from sys.views where name = 'v_operator_zone_right_operator_zone')
	drop view v_operator_zone_right_operator_zone
go
create view v_operator_zone_right_operator_zone
with schemabinding
as
	select operator_id, ZONE_ID, right_id
	from dbo.OPERATOR_ZONE 
	where allowed = 1
go
create unique clustered index PK_v_operator_zone_right_operator_zone
	on v_operator_zone_right_operator_zone(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_operator_zone_zid_rid_oid
	on v_operator_zone_right_operator_zone(zone_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_right_zonegroup_zone')
	drop view v_operator_zone_right_zonegroup_zone
go
create view v_operator_zone_right_zonegroup_zone
with schemabinding
as
	select o_vg.operator_id, vg_v.ZONE_ID, o_vg.right_id, [count] = count_big(*)
	from dbo.OPERATOR_ZONEGROUP o_vg
		join dbo.ZONEGROUP_ZONE vg_v on vg_v.ZONEGROUP_ID = o_vg.ZONEGROUP_ID
		join dbo.ZoneGroup zg on zg.ZoneGroup_ID = o_vg.ZoneGroup_ID and zg.PropagateAccess = 1
	where allowed = 1
	group by o_vg.operator_id, vg_v.ZONE_ID, o_vg.right_id
go
create unique clustered index PK_v_operator_zone_right_zonegroup_zone
	on v_operator_zone_right_zonegroup_zone(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_zonegroup_zone_zid_rid_oid
	on v_operator_zone_right_zonegroup_zone(zone_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_right_operatorgroup_zone')
	drop view v_operator_zone_right_operatorgroup_zone
go
create view v_operator_zone_right_operatorgroup_zone
with schemabinding
as
	select og_o.operator_id, og_v.ZONE_ID, og_v.right_id, [count] = count_big(*)
	from dbo.OPERATORGROUP_ZONE og_v
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
	where allowed = 1
	group by og_o.operator_id, og_v.ZONE_ID, og_v.right_id
go
create unique clustered index PK_v_operator_zone_right_operatorgroup_zone
	on v_operator_zone_right_operatorgroup_zone(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_operatorgroup_zone_zid_rid_oid
	on v_operator_zone_right_operatorgroup_zone(zone_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_right_operatorgroup_zonegroup')
	drop view v_operator_zone_right_operatorgroup_zonegroup
go
create view v_operator_zone_right_operatorgroup_zonegroup
with schemabinding
as
	select og_o.operator_id, vg_v.ZONE_ID, og_vg.right_id, [count] = count_big(*)
	from dbo.OPERATORGROUP_ZONEGROUP og_vg
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
		join dbo.ZONEGROUP_ZONE vg_v on vg_v.ZONEGROUP_ID = og_vg.ZONEGROUP_ID
		join dbo.ZoneGroup zg on zg.ZoneGroup_ID = og_vg.ZoneGroup_ID and zg.PropagateAccess = 1
	where allowed = 1
	group by og_o.operator_id, vg_v.ZONE_ID, og_vg.right_id
go
create unique clustered index PK_v_operator_zone_right_operatorgroup_zonegroup
	on v_operator_zone_right_operatorgroup_zonegroup(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_operatorgroup_zonegroup_zid_rid_oid
	on v_operator_zone_right_operatorgroup_zonegroup(zone_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_right_operator_department_admin')
	drop view v_operator_zone_right_operator_department_admin
go
create view v_operator_zone_right_operator_department_admin
with schemabinding
as
	select o_d.operator_id, z.ZONE_ID, r.right_id
	from dbo.OPERATOR_DEPARTMENT o_d
		join dbo.GEO_ZONE z on z.department_id = o_d.department_id
		join dbo.[Right] r on r.Right_ID in (2, 19, 105)
	where o_d.right_id = 2 /*SecurityAdministrator*/
		and o_d.allowed = 1
go
create unique clustered index PK_v_operator_zone_right_operator_department_admin
	on v_operator_zone_right_operator_department_admin(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_operator_department_admin_zid_rid_oid
	on v_operator_zone_right_operator_department_admin(zone_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_right_operatorgroup_department_admin')
	drop view v_operator_zone_right_operatorgroup_department_admin
go
create view v_operator_zone_right_operatorgroup_department_admin
with schemabinding
as
	select og_o.operator_id, z.zone_id, r.right_id, [count] = count_big(*)
	from dbo.OperatorGroup_Operator og_o
		join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
		join dbo.GEO_ZONE z on z.Department_ID = og_d.department_id
		join dbo.[Right] r on r.Right_ID in (2, 19, 105)
	where og_d.right_id = 2 /*SecurityAdministrator*/
		and og_d.allowed = 1
	group by og_o.operator_id, z.zone_id, r.right_id
go
create unique clustered index PK_v_operator_zone_right_operatorgroup_department_admin
	on v_operator_zone_right_operatorgroup_department_admin(operator_id, zone_id, right_id)
go
create nonclustered index IX_v_operator_zone_right_operatorgroup_department_admin_zid_rid_oid
	on v_operator_zone_right_operatorgroup_department_admin(zone_id, right_id, operator_id)
go


if exists (Select * from sys.views where name = 'v_operator_zone_right')
	drop view v_operator_zone_right
go
create view v_operator_zone_right
as
select operator_id, zone_id, right_id, priority = min(priority)
	from (
		select operator_id, zone_id, right_id, priority = 1 from v_operator_zone_right_operator_zone with (noexpand) 
		union		
		select operator_id, zone_id, right_id, priority = 2 from v_operator_zone_right_zonegroup_zone with (noexpand) 
		union		
		select operator_id, zone_id, right_id, priority = 3 from v_operator_zone_right_operatorgroup_zone with (noexpand) 
		union		
		select operator_id, zone_id, right_id, priority = 4 from v_operator_zone_right_operatorgroup_zonegroup with (noexpand) 
		union		
		select operator_id, zone_id, right_id, priority = 7 from v_operator_zone_right_operator_department_admin with (noexpand) 
		union		
		select operator_id, zone_id, right_id, priority = 8 from v_operator_zone_right_operatorgroup_department_admin with (noexpand)
	) t
	group by operator_id, zone_id, right_id