/****** Object:  StoredProcedure [dbo].[GetCountVehiclesOnRoutes]    Script Date: 07/14/2010 17:51:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetCountVehiclesOnRoutes] 
	@Date 		datetime,	-- -ЁкЁ ¤Ё ·■к■и║ж киїё║їк╕а  ■√║в°к╣ ■квїк
	@DateTime 	datetime,	-- -ЁкЁ ° Єиї№а ¤Ё ·■к■иvї киїё║їк╕а  ■√║в°к╣ ■квїк
	@operator_id int = null
AS


--------------------------------------------------------------------------------------------
/*
--Test
declare @Date datetime, @DateTime datetime
set @Date = '2006-09-20 20:00:00'
set @DateTime = '2006-09-21 18:00:00'
*/
--------------------------------------------------------------------------------------------

-- ЇЁкЁ Є √Ё·Ё√╣¤■№ Є°Її (Ї√а п-б°° DayKind())
declare @LocalDate datetime
set @LocalDate = dateadd(hh, 24-datepart(hh, @Date), @Date)

-- L¤п■и№Ёб°а  ■ пЁ·к°вї╕·°№ Єv┐■ЇЁ№ 
select distinct 
	RT.EXT_NUMBER 		RouteNumber, 	-- ¤■№їи №Ёи░и║кЁ
	SC.EXT_NUMBER,
	WBH.WAYBILL_HEADER_ID,
	max(WBT.BEGIN_TIME) 	as BEGIN_TIME,
	0 			as TripKindID
into 
	#fact
from 
	dbo.WAYBILL_HEADER as WBH
	inner join dbo.SCHEDULE as SC on WBH.SCHEDULE_ID = SC.SCHEDULE_ID  
	inner join dbo.ROUTE as RT on SC.ROUTE_ID = RT.ROUTE_ID  
	inner join dbo.WB_TRIP as WBT on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID  
	left join v_operator_vehicle_right rr on rr.vehicle_id = WBH.VEHICLE_ID
where	
	SC.DAY_KIND_ID = dbo.DayKind(@LocalDate, RT.ROUTE_ID)
	and WBH.WAYBILL_DATE = @Date 
	and WBH.CANCELLED = 0 
	and WBT.BEGIN_TIME <= @DateTime
	and WBT.BEGIN_TIME < WBT.END_TIME
	and (@operator_id is null or rr.operator_id = @operator_id)
group by RT.EXT_NUMBER, SC.EXT_NUMBER, WBH.WAYBILL_HEADER_ID
order by 
	RT.EXT_NUMBER, SC.EXT_NUMBER


update #fact set TripKindID = ( 
	select top 1 
		WBT.TRIP_KIND_ID 
	from 
		dbo.WB_TRIP as WBT   	
	where 
		WBT.WAYBILL_HEADER_ID = #fact.WAYBILL_HEADER_ID
		and WBT.BEGIN_TIME = #fact.BEGIN_TIME
		and WBT.BEGIN_TIME < WBT.END_TIME
	)


-- L¤п■и№Ёб°а  ■ ўЁ √Ё¤°и■ЄЁ¤¤v№ Єv┐■ЇЁ№ 
select distinct 
	RT.EXT_NUMBER 	RouteNumber, 	-- ¤■№їи №Ёи░и║кЁ
	count(*) 	VehiclesPlan, 
	VehiclesFact = (
		select count(*) 
		from 
			#fact 
		where 
			#fact.RouteNumber = RT.EXT_NUMBER
			and #fact.TripKindID in (5, 6)
		)
from 
	dbo.SCHEDULE as SC
	inner join dbo.ROUTE as RT on SC.ROUTE_ID = RT.ROUTE_ID  
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where	
	SC.DAY_KIND_ID = dbo.DayKind(@LocalDate, RT.ROUTE_ID)
	and (@operator_id is null or rr.operator_id = @operator_id)
group by 
	RT.EXT_NUMBER 
order by 
	RT.EXT_NUMBER



drop table #fact


