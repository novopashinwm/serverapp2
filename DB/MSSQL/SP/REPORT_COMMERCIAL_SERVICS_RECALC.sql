USE [NGS]
GO

/****** Object:  StoredProcedure [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]    Script Date: 02.12.2020 15:38:59 ******/
DROP PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]
GO

/****** Object:  StoredProcedure [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]    Script Date: 02.12.2020 15:38:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @to datetime
	DECLARE @from datetime

	set @to = dateadd(day, datediff(day, 0, getdate()), 0);
	set @from = DATEADD (DAY, -30, @to)

	PRINT 'Start @from ' + (CONVERT( VARCHAR(24), @from, 121))
	PRINT 'Start @to ' + (CONVERT( VARCHAR(24), @to, 121))

	DECLARE @current datetime = @from
	WHILE @current < @to
	BEGIN
		
		PRINT 'START ' + (CONVERT( VARCHAR(24), @from, 121)) + ' at ' + (CONVERT( VARCHAR(24), getDate(), 121))

		exec dbo.REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS
			@from = @current
	
		PRINT 'END ' +(CONVERT( VARCHAR(24), @from, 121)) + ' at ' + (CONVERT( VARCHAR(24), getDate(), 121))


		SET @current = DATEADD(day, 1, @current)

		

		WAITFOR DELAY '00:01'
	END

END
GO

