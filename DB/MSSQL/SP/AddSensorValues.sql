﻿IF (OBJECT_ID(N'[dbo].[AddSensorValues]') IS NOT NULL)
	DROP PROCEDURE [dbo].[AddSensorValues];
GO

CREATE PROCEDURE [dbo].[AddSensorValues]
(
	@vehicle_id   int,
	@log_time     int,
	@sensorValues dbo.SensorValue readonly
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @rows_affected int = 0;
	DECLARE @rows          int = 0;
	DECLARE @i             int = 1;
	DECLARE @count         int;

	-- Ищем новые и обновленные данные, существующие отбрасываем
	DECLARE @v TABLE ([Id] int NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED, [Number] int, [Value] bigint)
	INSERT INTO @v ([Number], [Value])
	SELECT
		[Number], [Value]
	FROM @sensorValues v
	WHERE NOT EXISTS
	(
		SELECT 1
		FROM [dbo].[Controller_Sensor_Log] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id 
		AND   [Log_Time]   = @log_time
		AND   [Number]     = v.[Number]
		AND   [Value]      = v.[Value]
	)
	SELECT @rows = @@ROWCOUNT, @count = @rows

	DECLARE @number int, @value bigint
	WHILE (@i <= @count) -- Пока данные существуют, перебираем каждое значение датчика
	BEGIN
		SELECT @number = [Number], @value = [Value] FROM @v WHERE [Id] = @i
		SET @i = @i + 1
		---------------------------------------------------------------------------------------------
		-- Делаем вставку, если такого значения нет
		INSERT INTO [dbo].[Controller_Sensor_Log]
			([Vehicle_ID], [Log_Time], [Number], [Value])
		SELECT
			 @vehicle_id,  @log_time,  @number,  @value
		WHERE NOT EXISTS
		(
			SELECT 0
			FROM [dbo].[Controller_Sensor_Log] WITH (XLOCK, SERIALIZABLE)
			WHERE [Vehicle_ID] = @vehicle_id
			AND   [Log_Time]   = @log_time
			AND   [Number]     = @number
		)
		SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows
		---------------------------------------------------------------------------------------------
		-- Если вставки не было, то значит это обновленная запись и её нужно обновить
		IF (0 = @rows)
		BEGIN
			UPDATE [dbo].[Controller_Sensor_Log]
				SET [Value] = @value
			WHERE [Vehicle_ID] = @vehicle_id
			and   [Log_Time]   = @log_time
			and   [Number]     = @number
			and   [Value]     <> @value
			SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows
		END
		---------------------------------------------------------------------------------------------
		-- Обновление последнего значения в таблице Controller_Sensor_Log#Lasts
		EXEC [dbo].[Controller_Sensor_Log#Lasts_Update]
			@vehicle_id = @vehicle_id,
			@log_time   = @log_time,
			@number     = @number
		----------------------------------------------------------------------------------------------
		-- Есть ли среди значений датчиков, значений исходных для вычисляемых датчиков
		IF (@number IN
		(
			11001, 11002, -- Вошло/Вышло через Дверь 1
			11101, 11102, -- Вошло/Вышло через Дверь 2
			11201, 11202, -- Вошло/Вышло через Дверь 3
			11301, 11302, -- Вошло/Вышло через Дверь 4
			11401, 11402, -- Вошло/Вышло через Дверь 5
			11501, 11502, -- Вошло/Вышло через Дверь 6
			11601, 11602  -- Вошло/Вышло через Дверь 7
		))
		BEGIN
			-- Ищем последние значения вычисляемого датчика
			DECLARE @calcNumber int = @number + 4
			DECLARE
				@lastCalcTime int    = 0,
				@lastCalcValu bigint = 0;
			SELECT
				@lastCalcTime = L.[Log_Time],
				@lastCalcValu = L.[Value]
			FROM [dbo].[Controller_Sensor_Log#Lasts] T
				INNER JOIN [dbo].[Controller_Sensor_Log] L
					ON  L.[Vehicle_ID] = T.[Vehicle_ID]
					AND L.[Log_Time]   = T.[Log_Time#Last]
					AND L.[Number]     = T.[Number]
			WHERE T.[Vehicle_ID] = @vehicle_id
			AND   T.[Number]     = @calcNumber

			INSERT INTO [dbo].[Controller_Sensor_Log]
				([Vehicle_ID], [Log_Time], [Number], [Value])
			SELECT
				*
			FROM
			(
				SELECT
					 [Vehicle_ID]   = [Vehicle_ID]
					,[Log_Time]     = [Log_Time]
					,[Number]       = @calcNumber
					-- Получаем нарастающий итог с учетом последнего значения вычисляемого датчика, со сбросом в 00:00:00 по GMT+5
					,[Value]        =
						-- Начальное значение подставляем только к тем записям которые находятся в том же дне GMT+5
						CASE
							WHEN DATEDIFF(day, 0, DATEADD(hour, 5, [dbo].[lt2utc]([Log_Time]))) = DATEDIFF(day, 0, DATEADD(hour, 5, [dbo].[lt2utc](@lastCalcTime)))
							THEN COALESCE(@lastCalcValu, 0)
							ELSE 0
						END
						+
						-- Просто нарастающий итог
						SUM(COALESCE([Value], 0)) OVER (PARTITION BY [Vehicle_ID], [Number], DATEDIFF(day, 0, DATEADD(hour, 5, [dbo].[lt2utc]([Log_Time]))) ORDER BY [Log_Time] ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
				FROM [dbo].[Controller_Sensor_Log]
				WHERE [Vehicle_ID] = @vehicle_id
				AND   [Number]     = @number
				AND   [Log_Time]   BETWEEN @lastCalcTime + 1 AND @log_time
			) S
			WHERE NOT EXISTS
			(
				SELECT 0
				FROM [dbo].[Controller_Sensor_Log] WITH (XLOCK, SERIALIZABLE)
				WHERE [Vehicle_ID] = S.[Vehicle_ID]
				AND   [Log_Time]   = S.[Log_Time]
				AND   [Number]     = S.[Number]
			)
			---------------------------------------------------------------------------------------------
			-- Обновление последнего значения в таблице GPS_Log#Lasts
			EXEC [dbo].[Controller_Sensor_Log#Lasts_Update]
				@vehicle_id = @vehicle_id,
				@log_time   = @log_time,
				@number     = @calcNumber
			----------------------------------------------------------------------------------------------
		END
		---------------------------------------------------------------------------------------------
	END

	SELECT @rows_affected
END
GO