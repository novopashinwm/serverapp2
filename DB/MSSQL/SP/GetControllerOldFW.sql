IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetControllerOldFW]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetControllerOldFW] 

GO

-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 01.04.08
-- Description:	���������� ������ ������������, ��� ������� ��������� ���������� ������� � ��
-- =============================================
CREATE PROCEDURE [dbo].[GetControllerOldFW]
	@PERIOD int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT controller.controller_id, vehicle_id, [time], number
	 FROM controller left join controller_info on 
				  controller.controller_id = controller_info.controller_id
				  join controller_type on
				  controller.controller_type_id = controller_type.controller_type_id
				  where vehicle_id is not null and number is not null and TYPE_NAME = 'SGGB'
				  and DATEDIFF(day,[time], GETDATE()) > @PERIOD 
END
GO
