if exists (select * from sys.objects where object_id = object_id('dbo.GetUtcDateTime') and type = 'p')
	drop procedure dbo.GetUtcDateTime;
go

create procedure dbo.GetUtcDateTime
as
set nocount on
select getutcdate()
