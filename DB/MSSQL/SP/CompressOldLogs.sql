if exists (
	select *
		from sys.procedures 
		where name = 'CompressOldLogs'
)
	drop procedure CompressOldLogs
go

create procedure CompressOldLogs
(
	@interval		int = 84600,
	@stop_log_time	int = null,
	@debug			bit = null
)
as

if @debug is null
	set @debug = 0
	
if @debug = 0
	set nocount on
	
--����� �������� ������� ���������� �����, �� ������� �� ������� ��������� ��� �������, ����� ��������, ����� �� ������� ��� ���

declare @stop_compress_log_time int = isnull(@stop_log_time, DATEDIFF(ss, '1970', getutcdate())-dbo.GetMaxLogAge())

insert into Vehicle_Log_Compress (Vehicle_ID, Log_Time_To)
	select Vehicle_ID, l.Log_Time
		from Vehicle v
		cross apply (select top(1) Log_Time from Controller_Sensor_Log l (nolock) where l.Vehicle_ID = v.VEHICLE_ID order by l.Log_Time asc) l
		where l.Log_Time < @stop_compress_log_time
		  and not exists (
				select * from Vehicle_Log_Compress e where e.Vehicle_ID = v.VEHICLE_ID)

declare @from int, @to int, @vehicle_id int
declare @sensor table (number int)
declare @number int

while 1=1
begin

	set @vehicle_id = (select top(1) Vehicle_ID from Vehicle_Log_Compress where Log_Time_To < @stop_compress_log_time order by Log_Time_To asc)
	
	if @vehicle_id is null
		break;	

	set @from = (select Log_Time_To from Vehicle_Log_Compress where Vehicle_ID = @vehicle_id)
	set @to = @from + @interval
	
	if @debug = 1
	begin
		print 'Compressing ' + 
			convert(varchar(10), @vehicle_id) + 
			' from ' + convert(varchar(10), convert(date, dbo.GetDateFromInt(@from))) + 
			' to '   + convert(varchar(10), convert(date, dbo.GetDateFromInt(@to)))
			
	end
	
	if @debug = 1
		print 'delete @sensor'
	
	delete @sensor
	
	if @debug = 1
		print 'insert into @sensor'
	
	insert into @sensor
		select distinct csm.Sensor_Number
			from v_controller_sensor_map csm
			where csm.Vehicle_ID = @vehicle_id
	
	while 1=1
	begin
		set @number = (select top(1) number from @sensor)
		if (@number is null)
			break;
		delete @sensor where number = number

		if @debug = 1
			print 'Compressing sensor ' + convert(varchar(10), @number)
		
		exec CompressControllerSensorLog @vehicle_id, @number, @from, @to, @debug
	end	
	
	exec CompressGeoLog @vehicle_id, @from, @to, @debug
	
	update Vehicle_Log_Compress set Log_Time_To = @to where Vehicle_ID = @vehicle_id

end