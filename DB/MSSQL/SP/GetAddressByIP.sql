

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAddressByIP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAddressByIP]

GO
CREATE PROCEDURE dbo.GetAddressByIP 
	@ip VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @baseIP bigINT = (
		select sum(convert(bigint, p.number) * power(256, 4-p.listpos))
			from  split_int(replace(@ip, '.', ';')) p)

	SELECT a.CityId, a.Name, a.Region, a.District, a.Latitude, a.Longitude FROM dbo.IPValues v
		JOIN dbo.IPRegions a ON v.CityId = a.CityId
	WHERE @baseIP BETWEEN v.RangeFromInt  AND  v.RangeToInt	
END
GO


