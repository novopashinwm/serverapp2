/****** Object:  StoredProcedure [dbo].[VehiclesStatusesListGet]    Script Date: 07/14/2010 20:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
--=Ёў¤Ёвї¤°ї: ж■√║вї¤°ї ЇЁ¤¤v┐ ■ TT ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║,
--°  иї¤ЁЇ√їЎЁХ°┐ ║·ЁўЁ¤¤■∙ Ё/·.
--LЄк■и: L√ї·╕ї∙ ж. +√°¤■Є.

--------------------------------------------------------------------------------------------
--DDL
ALTER procedure [dbo].[VehiclesStatusesListGet]
	@ReportDateTime	datetime,	---ЁкЁ ° Єиї№а, ¤Ё ·■к■иvї киїё║їк╕а  ■√║в°к╣ ЇЁ¤¤vї ■  и■ ║Хї¤¤v┐ иї∙╕Ё┐;
	@VehicleGroupID	int		--ID ЁЄк■·■√■¤¤v, Ї√а ·■к■и■∙ ╕■ўЇЁїк╕а ¤ЁиаЇ.
as

--------------------------------------------------------------------------------------------
--DML
--жЁХ°кЁ ■к  ║╕к■є■  їиїЇЁ¤¤■є■ ў¤Ёвї¤°а ЇЁкv. 
---■и■░■ ёv, вк■ёv ¤Ё дк■к ╕√║вЁ∙  ■Ї¤°№Ё√Ё╕╣ ■Ў°ЇЁї№Ёа ■░°ё·Ё, ¤■ ¤ї иїЁ√°ў■ЄЁ¤  їиї┐ЄЁк її Є ·■Її ·√°ї¤кЁ.
if @ReportDateTime is null set @ReportDateTime = getutcdate()
--жЁХ°кЁ ■к  ║╕к■є■  їиїЇЁ¤¤■є■ ў¤Ёвї¤°а ID ЁЄк■·■√■¤¤v. 
if @VehicleGroupID is null set @VehicleGroupID = 0

--------------------------------------------------------------------------------------------
---Ё¤¤vї Ї√а кї╕к°и■ЄЁ¤°а.
/*declare @ReportDateTime	datetime,
	@VehicleGroupID	int
set @ReportDateTime = '04/12/2006 19:55:00'
set @VehicleGroupID = 6*/
--------------------------------------------------------------------------------------------

-- 1. ж■√║вї¤°ї ЇЁ¤¤v┐  ■ ╕■╕к■а¤°а№ Є╕ї┐ жT, ўЁЄїЇї¤¤v┐ Є ║·ЁўЁ¤¤║ж ЇЁк║.
begin transaction
--жїиї№ї¤¤Ёа Ї√а ┐иЁ¤ї¤°а ID иЁ╕╕№Ёки°ЄЁї№■є■ Є ЇЁ¤¤v∙ №■№ї¤к ·║и╕■и■№ жT.
declare @WaybillHeaderID	int
--T■ўЇЁ¤°ї Єиї№ї¤¤■∙ кЁё√°бv Ї√а ┐иЁ¤ї¤°а Є■ўЄиЁХЁї№■є■ иїў║√╣кЁкЁ.
create table #ResultStorage
	(WaybillHeaderID	int,
	WaybillStatus		int,
	ActualTime		datetime)
--+в°╕к·Ё Єиї№ї¤¤■∙ кЁё√°бv (¤Ё Є╕а·°∙ ╕√║вЁ∙, ї╕√° ■¤Ё ║Ўї ╕║Хї╕кЄ■ЄЁ√Ё ╕ ЇЁ¤¤v№° ° ¤ї ёv√Ё  їиї╕■ўЇЁ¤Ё)
truncate table #ResultStorage
--ж║и╕■и,  їиїё°иЁжХ°∙ Є╕ї ╕║Хї╕кЄ║жХ°ї жT ўЁ ║·ЁўЁ¤¤║ж ЇЁк║.
DECLARE WaybillHeader_Cursor CURSOR FORWARD_ONLY READ_ONLY
FOR 
SELECT	WAYBILL_HEADER_ID
FROM 	dbo.WAYBILL_HEADER as WBH
where	WBH.[WAYBILL_DATE] > dateadd(hh, -1, @ReportDateTime - 1) --@ReportDateTime - 1
	and WBH.[WAYBILL_DATE] <= @ReportDateTime
OPEN WaybillHeader_Cursor
--жїиїё■и Є╕ї┐ жT ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║.
FETCH NEXT FROM WaybillHeader_Cursor 
INTO 	@WaybillHeaderID

--жїиї№ї¤¤Ёа Ї√а ┐иЁ¤ї¤°а Єиї№ї¤°  ■╕√їЇ¤ї∙  ївЁк° ╕  и°в°¤■∙ ·и■№ї "жївЁк╣ ·■ °°" ° "=ї  ївЁкЁ√╕а" иЁ╕╕№Ёки°ЄЁї№■є■ ·║и╕■и■№ жT.
declare @CurrentWaybillActualTime		datetime

WHILE @@FETCH_STATUS = 0
BEGIN
	--+ иїЇї√ї¤°ї Єиї№ї¤°  ■╕√їЇ¤ї∙  ївЁк° иЁ╕╕№Ёки°ЄЁї№■є■ Є ·║и╕■иї жT.
	---√а  ■√║вї¤°а Єиї№ї¤°  ■╕√їЇ¤ї∙  ївЁк° иЁ╕╕№Ёки°ЄЁї№■є■ жT ¤ЁЇ■ ¤Ё∙к° Є °╕к■и°° ўЁ °╕° ╕ №Ё·╕°№Ё√╣¤v№ ў¤Ёвї¤°ї№ ·■√-ЄЁ ·■ °∙ ЇЁ¤¤■є■ жT (ў¤Ёвї¤°ї Є  ■√ї PRINTED).
	--жк■  ■√║вЁїк╕а ╕■ик°и■Є·■∙  ■  ■√ж PRINTED.
	--жЁкї№ ■к╕■ик°и■ЄЁк╣  ■ ACTUAL_TIME, Єўак╣ °ў  ■√║вї¤¤■є■ ╕ °╕·Ё  їиЄ■ї ў¤Ёвї¤°ї ° кї№ ╕Ё№v№  ■√║в°к╣ №°¤°№Ё√╣¤■ї Єиї№а (к■, Є ·■к■и■ї  и■°ў■░√Ё  ■╕√їЇ¤аа  ївЁк╣).
	set @CurrentWaybillActualTime = 	(select top 1 H_WBH.ACTUAL_TIME
						from dbo.H_WAYBILL_HEADER as H_WBH
						where 	H_WBH.PRINTREASON not in (1, 3)	--жЁ╕╕№Ёки°ЄЁжк╕а к■√╣·■ жT ёїў ╕√■ЄЁ "ж■ °а".
							and H_WBH.WAYBILL_HEADER_ID = @WaybillHeaderID
						order by	H_WBH.PRINTED		desc, 
								H_WBH.ACTUAL_TIME)
	
	--Tиїё║їк╕а ЄvёиЁк╣ ╕■╕к■а¤°ї иЁ╕╕№Ёки°ЄЁї№■є■ жT ¤Ё ║·ЁўЁ¤¤v∙ №■№ї¤к Єиї№ї¤°, √°ё■ ї╕√° Ї■ дк■є■ жT ёv√ ¤Ё ївЁкЁ¤, к■ ¤Ё №■№ї¤к  ївЁк°.
	--TЁ· Ўї ( ■ Єиї№ї¤°  ївЁк°) ■ иїЇї√аїк╕а  и°ў¤Ё· к■є■, вк■ жT Ё¤¤║√°и■ЄЁ¤.
	--+╕√° иЁ╕╕№Ёки°ЄЁї№v∙ жT ¤ї ёv√ ¤Ё ївЁкЁ¤ Ї■ ║·ЁўЁ¤¤■є■ Єиї№ї¤°, к■ °ў °╕к■и°° жT ёїиїк╕а Єиї№а  ■╕√їЇ¤їє■ Її∙╕кЄ°а ╕ ЇЁ¤¤v№ жT.
	if (@CurrentWaybillActualTime > @ReportDateTime) or (@CurrentWaybillActualTime is null)
	begin
		set @CurrentWaybillActualTime = 	(select top 1 H_WBH.ACTUAL_TIME
							from dbo.H_WAYBILL_HEADER as H_WBH
							where 	H_WBH.ACTUAL_TIME <= @ReportDateTime
								and H_WBH.WAYBILL_HEADER_ID = @WaybillHeaderID
							order by	H_WBH.ACTUAL_TIME	desc)
	end

	--Tvў■Є п║¤·б°°  ■√║вї¤°а ╕■╕к■а¤°а жT  ■ Єиї№ї¤° ° ¤■№їи║ жT. T■╕к■а¤°ї жT кЁ·■ї Ўї, ·Ё· ° ║ ё√°ЎЁ∙░їє■ WB_TRIP'Ё.
	--T■┐иЁ¤ї¤°ї иїў║√╣кЁкЁ Єvё■и·° Ї√а иЁ╕╕№Ёки°ЄЁї№■є■ жT Є■ Єиї№ї¤¤║ж кЁё√°б║.
	insert into #ResultStorage
		(WaybillHeaderID,
		WaybillStatus,
		ActualTime)
	values
		(@WaybillHeaderID,
		dbo.WaybillHeaderStatusByTimeGet(@WaybillHeaderID, @CurrentWaybillActualTime),
		@CurrentWaybillActualTime)

	-- Get the next row.
	FETCH NEXT FROM WaybillHeader_Cursor 
	INTO 	@WaybillHeaderID
END

CLOSE WaybillHeader_Cursor
DEALLOCATE WaybillHeader_Cursor
commit

-- 2. Tvё■и ЇЁ¤¤v┐ ■ T■╕к■а¤°а┐ жT ¤Ё ║·ЁўЁ¤¤■ї Єиї№а Ї√а ўЁЇЁ¤¤■∙ Ё/·.
begin transaction
select 	VH.[GARAGE_NUMBER]	[GarageNumber],			-- ¤■№їи єЁиЁЎ¤v∙ TT;
	VK.[NAME] 			[VEHICLE_TYPE],
	H_WBH.[EXT_NUMBER]	[WaybillNumber],		-- ¤■№їи жT;
	RS.[WaybillStatus]	[WaybillStatusID],		-- ID ╕кЁк║╕Ё жT;
	TRK.[NAME]		[WaybillStatusName],		-- кї·╕к■Є■ї ■ °╕Ё¤°ї ╕кЁк║╕Ё жT;
	H_WBH.[CANCELLED]	[WaybillCancelledStatus],	--  и°ў¤Ё· Ё¤¤║√°и■ЄЁ¤¤■є■ жT;
	H_WBH.[PRINTED]		[PrintedQuantity]		-- ·■√°вї╕кЄ■ ╕║Хї╕кЄ║жХ°┐ ¤Ё ївЁкЁ¤¤v┐ ·■ °∙.

from #ResultStorage as RS
inner join dbo.H_WAYBILL_HEADER as H_WBH
	on H_WBH.WAYBILL_HEADER_ID = RS.WaybillHeaderID
	and H_WBH.ACTUAL_TIME = RS.ActualTime
inner join dbo.VEHICLE as VH
	on VH.[VEHICLE_ID] = H_WBH.[VEHICLE_ID]
inner join dbo.VEHICLE_KIND as VK
	on VK.[VEHICLE_KIND_ID] = VH.[VEHICLE_KIND_ID]
inner join dbo.VEHICLEGROUP_VEHICLE as VGVH
	on VGVH.[VEHICLE_ID] = VH.[VEHICLE_ID]
left join dbo.TRIP_KIND as TRK
	on TRK.[TRIP_KIND_ID] = RS.[WaybillStatus]

where 	VGVH.[VEHICLEGROUP_ID] = @VehicleGroupID	--T ╕ °╕■· Ї■√Ў¤v  ■ ЁЇЁк╣ ЇЁ¤¤vї к■√╣·■ Ї√а TT, ■к¤■╕аХ°┐╕а · ║·ЁўЁ¤¤■∙ ЁЄк■·■√■¤¤ї
	--Є ╕ °╕■· кЁ·Ўї Ї■√Ў¤v  ■ ЁЇЁк╣ Ё¤║√°и■ЄЁ¤¤vї жT.

order by VH.[GARAGE_NUMBER]

--жи°¤║Ї°кї√╣¤■ї ║ЇЁ√ї¤°ї Єиї№ї¤¤■∙ кЁё√°бv.
drop table #ResultStorage
commit


