﻿IF (OBJECT_ID(N'[dbo].[GetLatCellTolerance]', N'FN') IS NOT NULL)
	DROP FUNCTION [dbo].[GetLatCellTolerance];
GO

/*
Sample:
SELECT [dbo].[GetLatCell]         (18.57295)
SELECT [dbo].[GetLatCellTolerance](18.57295, 15)
*/

CREATE FUNCTION [dbo].[GetLatCellTolerance]
(
	@lat float, @cellSizeInMeters float
) RETURNS int WITH SCHEMABINDING
AS
BEGIN
	DECLARE @stp float = 180/(40000000/2/@cellSizeInMeters)
	SET @lat =
		CASE
			WHEN @lat <  000 THEN @lat+180*(ROUND(-@lat/180,0)+1)
			WHEN @lat >= 180 THEN @lat-180
			ELSE @lat
		END
	RETURN ROUND(@lat/@stp, 0);
END