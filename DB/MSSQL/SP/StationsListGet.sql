/****** Object:  StoredProcedure [dbo].[StationsListGet]    Script Date: 07/14/2010 16:08:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--------------------------------------------------------------------------------------------
-- =Ёў¤Ёвї¤°ї:  ■√║вї¤°ї ╕ °╕·Ё Є╕ї┐ к■вї·,  и°╕║к╕кЄ║жХ°┐ Є +-
-- LЄк■и: +иїёї¤¤°·■Є ж.

--------------------------------------------------------------------------------------------
--DDL
ALTER PROCEDURE [dbo].[StationsListGet]
	@operator_id int = null
AS

--------------------------------------------------------------------------------------------

select	distinct
	BS.BUSSTOP_ID		PointID,
	BS.[NAME]		StationName
from 
	dbo.BUSSTOP as BS
	join v_operator_department_right dr on dr.DEPARTMENT_ID = bs.DEPARTMENT_ID
where 
	(@operator_id is null or dr.operator_id = @operator_id)
order by 
	BS.[NAME]
	


