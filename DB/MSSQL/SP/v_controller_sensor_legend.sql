if (exists (select 1 from sys.objects where name = 'v_controller_sensor_legend' and type='v'))
	drop view v_controller_sensor_legend
go
set QUOTED_IDENTIFIER on
go
create view v_controller_sensor_legend
WITH SCHEMABINDING
as
	select	Vehicle_ID = c.Vehicle_ID,
			Sensor_Legend = csl.Number,
			Sensor_Count = count_big(*)
		from dbo.Controller c
		join dbo.Controller_Sensor_Map csm on csm.Controller_ID = c.Controller_ID
		join dbo.Controller_Sensor_Legend csl on csl.Controller_Sensor_Legend_ID = csm.Controller_Sensor_Legend_ID
		join dbo.Controller_Sensor cs on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
		group by c.Vehicle_ID, csl.Number
go
create unique clustered index IX_V_Controller_sensor_legend on v_controller_sensor_legend (Vehicle_ID, Sensor_Legend)
go