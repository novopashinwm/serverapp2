if exists (select * from sys.objects where name = 'RegisterCompoundRuleEvents')
	drop procedure RegisterCompoundRuleEvents
go

create procedure RegisterCompoundRuleEvents
(
	@compoundRuleLog dbo.CompoundRuleLogParam readonly
)
as
begin

	insert into CompoundRule_Log (Vehicle_ID, Log_Time, CompoundRule_ID)
		select Vehicle_ID, Log_Time, CompoundRule_ID
		from @compoundRuleLog p

end