
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Add Points for Web
-- =============================================
alter PROCEDURE [dbo].[AddPointsWeb]  
	--@map_id			int,
	@map_guid		uniqueidentifier,
	@x				float,
	@y				float,
	@name			nvarchar(250),
	@description	nvarchar(500),
	@type_id		int,
	@operator_id	int
AS
BEGIN
	
	declare @map_id	int
	--if(@map_id is null)
		set @map_id = (select MAP_ID from MAPS where GUID = @map_guid)
		
	insert dbo.MAP_VERTEX
		([MAP_ID], [X], [Y], [VISIBLE], [ENABLE]) 
	values
		(@map_id, @x, @y, 'true', 'true') 

	declare @vertex_id int
	set @vertex_id = (select @@identity)

	insert dbo.WEB_POINT
		([VERTEX_ID], [NAME], [DESCRIPTION], [TYPE_ID], [OPERATOR_ID]) 
	values
		(@vertex_id, @name, @description, @type_id, @operator_id) 


END
GO
