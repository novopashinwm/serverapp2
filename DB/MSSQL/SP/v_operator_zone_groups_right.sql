if exists (select * from sys.views where name = 'v_operator_zone_groups_right_operator_zonegroup')
	drop view v_operator_zone_groups_right_operator_zonegroup
go
create view v_operator_zone_groups_right_operator_zonegroup
with schemabinding
as
	select o_vg.operator_id, o_vg.zonegroup_id, o_vg.right_id
	from dbo.operator_zonegroup o_vg
	where allowed = 1
go
create unique clustered index PK_v_operator_zone_groups_right_operator_zonegroup
	on v_operator_zone_groups_right_operator_zonegroup(operator_id, zonegroup_id, right_id)
go
create nonclustered index IX_v_operator_zone_groups_right_operator_zonegroup_vid_rid_oid
	on v_operator_zone_groups_right_operator_zonegroup(zonegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_groups_right_operatorgroup_zonegroup')
	drop view v_operator_zone_groups_right_operatorgroup_zonegroup
go
create view v_operator_zone_groups_right_operatorgroup_zonegroup
with schemabinding
as
	select og_o.operator_id, og_vg.zonegroup_id, og_vg.right_id, [count] = count_big(*)
	from dbo.OPERATORGROUP_zoneGROUP og_vg  
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
	where allowed = 1
	group by og_o.operator_id, og_vg.zonegroup_id, og_vg.right_id
go
create unique clustered index PK_v_operator_zone_groups_right_operatorgroup_zonegroup
	on v_operator_zone_groups_right_operatorgroup_zonegroup(operator_id, zonegroup_id, right_id)
go
create nonclustered index IX_v_operator_zone_groups_right_operatorgroup_zonegroup_vid_rid_oid
	on v_operator_zone_groups_right_operatorgroup_zonegroup(zonegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_groups_right_operator_department_admin')
	drop view v_operator_zone_groups_right_operator_department_admin
go
create view v_operator_zone_groups_right_operator_department_admin
with schemabinding
as
	select o_d.operator_id, zg.ZONEGROUP_ID, r.right_id, [count] = count_big(*)
	from dbo.OPERATOR_DEPARTMENT o_d
		join dbo.ZONEGROUP zg on zg.Department_ID = o_d.department_id
		join dbo.[Right] r on r.Right_ID in (105, 19, 113, 109, 119)
	where o_d.right_id = 2 /*SecurityAdministrator*/
		and o_d.allowed = 1
	group by o_d.operator_id, zg.ZONEGROUP_ID, r.right_id
go
create unique clustered index PK_v_operator_zone_groups_right_operator_department_admin
	on v_operator_zone_groups_right_operator_department_admin(operator_id, zonegroup_id, right_id)
go
create nonclustered index IX_v_operator_zone_groups_right_operator_department_admin_vid_rid_oid
	on v_operator_zone_groups_right_operator_department_admin(zonegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_zone_groups_right_operatorgroup_department_admin')
	drop view v_operator_zone_groups_right_operatorgroup_department_admin
go
create view v_operator_zone_groups_right_operatorgroup_department_admin
with schemabinding
as
	select og_o.operator_id, zg.zonegroup_id, r.right_id, [count] = count_big(*)
	from dbo.OperatorGroup_Operator og_o
		join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
		join dbo.ZONEGROUP zg on zg.Department_ID = og_d.department_id
		join dbo.[Right] r on r.Right_ID in (105, 19, 113, 109, 119)
	where og_d.right_id = 2 /*SecurityAdministrator*/
		and og_d.allowed = 1		
	group by og_o.operator_id, zg.zonegroup_id, r.right_id
go
create unique clustered index PK_v_operator_zone_groups_right_operatorgroup_department_admin
	on v_operator_zone_groups_right_operatorgroup_department_admin(operator_id, zonegroup_id, right_id)
go
create nonclustered index IX_v_operator_zone_groups_right_operatorgroup_department_admin_vid_rid_oid
	on v_operator_zone_groups_right_operatorgroup_department_admin(zonegroup_id, right_id, operator_id)
go


if exists (Select * from sys.views where name = 'v_operator_zone_groups_right')
	drop view v_operator_zone_groups_right
go
create view v_operator_zone_groups_right
as
select operator_id, zonegroup_id, right_id, priority = min(priority)
	from (
		select operator_id, zonegroup_id, right_id, priority = 1 from v_operator_zone_groups_right_operator_zonegroup with (noexpand) 
		union		
		select operator_id, zonegroup_id, right_id, priority = 2 from v_operator_zone_groups_right_operatorgroup_zonegroup with (noexpand) 
		union		
		select operator_id, zonegroup_id, right_id, priority = 3 from v_operator_zone_groups_right_operator_department_admin with (noexpand) 
		union		
		select operator_id, zonegroup_id, right_id, priority = 4 from v_operator_zone_groups_right_operatorgroup_department_admin with (noexpand) 
	) t
	group by operator_id, zonegroup_id, right_id