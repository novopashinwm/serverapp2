if (exists (select 1 from sys.objects where name = 'v_controller_sensor_map' and type='v'))
	drop view v_controller_sensor_map
go
create view v_controller_sensor_map 
WITH SCHEMABINDING
as
	select	Vehicle_ID = c.Vehicle_ID,
			Sensor_Legend = csl.Number,
			Sensor_Number = cs.Number,
			Multiplier = csm.Multiplier,
			Constant = csm.Constant,
			VALUE_EXPIRED = isnull(cs.VALUE_EXPIRED, csl.VALUE_EXPIRED),
			Min_Value = csm.Min_Value,
			Max_Value = csm.Max_Value
		from dbo.Controller c
		join dbo.Controller_Sensor_Map csm on csm.Controller_ID = c.Controller_ID
		join dbo.Controller_Sensor_Legend csl on csl.Controller_Sensor_Legend_ID = csm.Controller_Sensor_Legend_ID
		join dbo.Controller_Sensor cs on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
go
create unique clustered index IX_V_Controller_sensor_map 
	on v_controller_sensor_map (Vehicle_ID, Sensor_Legend, Sensor_Number, Min_Value, Max_Value)
go
