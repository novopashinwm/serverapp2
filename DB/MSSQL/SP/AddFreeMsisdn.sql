if exists (
	select *
		from sys.procedures
		where name = 'AddFreeMsisdn'
)
	drop procedure AddFreeMsisdn
go

create procedure AddFreeMsisdn
(
	@name nvarchar(255),
	@value varchar(255),
	@endDate datetime = null
)
as
	declare @phoneInvariant varchar(32) = dbo.GetPhoneInvariant(@value)

	if @phoneInvariant is null
	begin
		print 'Unable convert phone to invariant representation: ' + @value
		return
	end

	insert into FreeMsisdn (name, value, endDate)
		select @name, @phoneInvariant, ISNULL(@endDate, DATEADD(year, 10, getutcdate()))
			where not exists (Select * from FreeMsisdn e where e.value = @phoneInvariant)
