IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveLastLogTimeForYandex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveLastLogTimeForYandex]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
exec SaveLastLogTimeForYandex null
*/

CREATE PROCEDURE [dbo].[SaveLastLogTimeForYandex]
@endTime int
AS
begin
	set nocount on;
	
	if not exists (select * from constants where NAME = 'LogTimeForYandex')	
		insert into constants (NAME) values ('LogTimeForYandex');
		
	update constants 
	set value = @endTime
	where NAME = 'LogTimeForYandex';

end

GO

