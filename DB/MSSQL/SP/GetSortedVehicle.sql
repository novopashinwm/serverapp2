/****** Object:  StoredProcedure [dbo].[GetSortedVehicle]    Script Date: 07/08/2010 18:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GetSortedVehicle]
	@operator_id int = null
as
begin
	select distinct v.* 
	from VEHICLE v
	join v_operator_vehicle_right vr on vr.vehicle_id = v.VEHICLE_ID
	where @operator_id is null or (vr.operator_id = @operator_id and vr.right_id = 102) --VehicleAccess
	order by GARAGE_NUMBER
end










