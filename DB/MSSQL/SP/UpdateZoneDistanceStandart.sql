IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateZoneDistanceStandart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateZoneDistanceStandart]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateZoneDistanceStandart]  
	@zone_from int, @zone_to int, @distance int, @operatorID int, @sessionID int
AS
BEGIN
	--проверяем есть ли у оператора права на редактирование нормативов
		if (not exists (select * from dbo.v_operator_rights
						where OPERATOR_ID = @operatorID
						and RIGHT_ID = 106)
			)
		begin
			RAISERROR
				(N'Нет прав на редактирование нормативов',
				12, -- Severity.
				1 -- State.
				);
			return; 
		end

	--проверяем есть ли у оператора права на указанные зоны
		if (not exists (select * from dbo.v_operator_zone_right
						where OPERATOR_ID = @operatorID
						and zone_id = @zone_from)
			or not exists (select * from dbo.v_operator_zone_right
						where OPERATOR_ID = @operatorID
						and zone_id = @zone_to)
			)
		begin
			RAISERROR
				(N'Нет прав на редактирование указанных зон',
				12, -- Severity.
				2 -- State.
				);
			return; 
		end


	declare @trailID int;
	insert into dbo.TRAIL (SESSION_ID, TRAIL_TIME)
	values (@sessionID, getdate());
	
	select @trailID = @@IDENTITY;
	
	DECLARE @trails table(ZONE_DISTANCE_STANDART_ID int, 
					ZONE_FROM int, ZONE_TO int, 
					DISTANCE int, [Action] nvarchar(6));

	DELETE dbo.ZONE_DISTANCE_STANDART
		OUTPUT DELETED.ZONE_DISTANCE_STANDART_ID, DELETED.ZONE_FROM, DELETED.ZONE_TO, DELETED.DISTANCE, 'DELETE'
		INTO @trails
	where (zone_from = @zone_from and zone_to = @zone_to)
	--or (zone_from = @zone_to and zone_to = @zone_from);

	insert into dbo.ZONE_DISTANCE_STANDART (zone_from, zone_to, distance)
		OUTPUT INSERTED.ZONE_DISTANCE_STANDART_ID, INSERTED.ZONE_FROM, INSERTED.ZONE_TO, INSERTED.DISTANCE, 'INSERT'
        INTO @trails
	values (@zone_from, @zone_to, @distance);

	insert into dbo.H_ZONE_DISTANCE_STANDART (ZONE_DISTANCE_STANDART_ID, ZONE_FROM, ZONE_TO, DISTANCE, TRAIL_ID, [Action], ACTUAL_TIME)
	select ZONE_DISTANCE_STANDART_ID, ZONE_FROM, ZONE_TO, DISTANCE, 
		@trailID, [Action], getdate()
	from @trails;

	
END