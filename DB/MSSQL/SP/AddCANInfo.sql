set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** Object:  StoredProcedure [dbo].[AddCANInfo]    Script Date: 09/02/2009 09:45:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddCANInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddCANInfo]
GO

create PROCEDURE [dbo].[AddCANInfo]
	@log_time int,
	@vehicle_id int,
	@can_time	int,				-- UTC time CAN info
    @speed tinyint = NULL,
	@cruise_control bit = NULL,
	@brake bit = NULL,
	@parking_brake bit = NULL,
	@clutch bit = NULL,
	@accelerator tinyint = NULL,
	@fuel_rate int = NULL,
	@fuel_level1 smallint = NULL,
	@fuel_level2 smallint = NULL,
	@fuel_level3 smallint = NULL,
	@fuel_level4 smallint = NULL,
	@fuel_level5 smallint = NULL,
	@fuel_level6 smallint = NULL,
	@revs tinyint = NULL,
	@run_to_mntnc int = NULL,
	@eng_hours int = NULL,
	@coolant_t smallint = NULL,
	@eng_oil_t smallint = NULL,
	@fuel_t smallint = NULL,
	@total_run int = NULL,
	@day_run smallint = NULL,
	@axle_load1 int = NULL,
	@axle_load2 int = NULL,
	@axle_load3 int = NULL,
	@axle_load4 int = NULL,
	@axle_load5 int = NULL,
	@axle_load6 int = NULL
AS
BEGIN

	set nocount on;

	insert dbo.CAN_INFO(vehicle_id, log_time,
	    [CAN_TIME], [SPEED], [CRUISE_CONTROL],[BRAKE],[PARKING_BRAKE],[CLUTCH],[ACCELERATOR],
		[FUEL_RATE],[FUEL_LEVEL1],	[FUEL_LEVEL2],[FUEL_LEVEL3],[FUEL_LEVEL4],[FUEL_LEVEL5],
		[FUEL_LEVEL6],[REVS],[RUN_TO_MNTNC],[ENG_HOURS],[COOLANT_T],[ENG_OIL_T],[FUEL_T],
		[TOTAL_RUN],[DAY_RUN],[AXLE_LOAD1],	[AXLE_LOAD2],[AXLE_LOAD3],[AXLE_LOAD4],
		[AXLE_LOAD5],[AXLE_LOAD6]) 
	select @vehicle_id, @log_time,
	    @can_time, @speed, @cruise_control, @brake, @parking_brake, @clutch, @accelerator, 
	    @fuel_rate, @fuel_level1, @fuel_level2,@fuel_level3,@fuel_level4, @fuel_level5, 
	    @fuel_level6, @revs, @run_to_mntnc, @eng_hours, @coolant_t, @eng_oil_t, @fuel_t,
		@total_run,	@day_run, @axle_load1, @axle_load2,	@axle_load3, @axle_load4, 
		@axle_load5, @axle_load6
	where not exists(
		select 0 from dbo.CAN_INFO with (XLOCK, SERIALIZABLE) 
		where Vehicle_ID = @vehicle_id and
			  Log_Time = @log_time
		)

	if @@rowcount = 0 
		update dbo.CAN_INFO 
		set 
		[CAN_TIME] = @can_time,
		vehicle_id = @vehicle_id, 
		log_time = @log_time,
		[SPEED] = @speed, 
		[CRUISE_CONTROL]=@cruise_control,
		[BRAKE]=@brake,
		[PARKING_BRAKE]=@parking_brake,
		[CLUTCH]=@clutch,
		[ACCELERATOR]=@accelerator,
		[FUEL_RATE]=@fuel_rate,
		[FUEL_LEVEL1]=@fuel_level1,
		[FUEL_LEVEL2]=@fuel_level2,
		[FUEL_LEVEL3]=@fuel_level3,
		[FUEL_LEVEL4]=@fuel_level4,
		[FUEL_LEVEL5]=@fuel_level5,
		[FUEL_LEVEL6]=@fuel_level6,
		[REVS] = @revs,
		[RUN_TO_MNTNC]=@run_to_mntnc,
		[ENG_HOURS]=@eng_hours,
		[COOLANT_T]=@coolant_t,
		[ENG_OIL_T]=@eng_oil_t,
		[FUEL_T]=@fuel_t,
		[TOTAL_RUN]=@total_run,
		[DAY_RUN]=@day_run,
		[AXLE_LOAD1]=@axle_load1,
		[AXLE_LOAD2]=@axle_load2,
		[AXLE_LOAD3]=@axle_load3,
		[AXLE_LOAD4]=@axle_load4,
		[AXLE_LOAD5]=@axle_load5,
		[AXLE_LOAD6]=@axle_load6
		where Vehicle_ID = @vehicle_id and LOG_TIME = @log_time

	if @@error <> 0
	begin
		RaisError('Unable add record to CAN_INFO', 16, 1)
		return -2
	end

	return 1
END





