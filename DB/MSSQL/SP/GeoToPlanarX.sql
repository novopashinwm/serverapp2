IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeoToPlanarX]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].GeoToPlanarX
GO

CREATE FUNCTION GeoToPlanarX
(
	@geoX float(53)
)
RETURNS float(53)
AS
BEGIN
	declare @kx float(53); set @kx = 63166;
	declare @origCenterX float(53); set @origCenterX = 38.000;

	return @kx * ( @geoX - @origCenterX);

END
GO
