/****** Object:  StoredProcedure [dbo].[UpdateControllerInfo]    Script Date: 03/04/2008 16:32:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[UpdateControllerInfo]    Script Date: 03/04/2008 16:32:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateControllerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateControllerInfo]

GO

CREATE PROCEDURE [dbo].[UpdateControllerInfo]
	@ControllerNumber int,
	@ServerIP varchar (20),
	@Port int,
	@TimeZone tinyint,
	@VoicePhone varchar (32),
	@DataPhone varchar (32),
	@Interval int,
	@Tcp bit,
	@EntryPoint varchar (32),
	@Login varchar (32),
	@Password varchar (32),
	@Firmware varchar (32)
AS

declare @ControllerID int

select @ControllerID = CONTROLLER_ID FROM CONTROLLER WHERE NUMBER = @ControllerNumber


update CONTROLLER_INFO set
	SERVER_IP	= @ServerIP,
	SERVER_PORT	= @Port,
	TIME_ZONE	= @TimeZone,
	VOICE_PHONE	= @VoicePhone,
	DATA_PHONE	= @DataPhone,
	INTERVAL	= @Interval,
	TCP		= @Tcp,
	INET_ENTRY_POINT= @EntryPoint,
	LOGIN		= @Login,
	[PASSWORD]	= @Password,
	[TIME]		= getutcdate()
where CONTROLLER_ID = @ControllerID


update CONTROLLER set
	FIRMWARE = @Firmware
where CONTROLLER_ID = @ControllerID


