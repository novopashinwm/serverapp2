set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


ALTER PROCEDURE [dbo].[DelZone]  
	@zone_id			int,
	@operator_id		int
AS
begin
	---update dbo.OPERATOR_ZONE set ALLOWED = 0 where ZONE_ID = @zone_id --and OPERATOR_ID = @operator_id   

	set nocount on;	

	DECLARE @vartexes TABLE
	(
	  VERTEX_ID int
	);

	DECLARE @primitives TABLE
	(
	  PRIMITIVE_ID int
	);

	--активная сессия пользователя
	declare @session_id int;
	select @session_id = max(session_id) from session where operator_id = @operator_id and session_end is null 

	if (@session_id is null)
	begin
			RAISERROR
				(N'Нет ни одной открытой сессии пользователя',
				12, -- Severity.
				1 -- State.
				);
			return; 
	end

	--проверка права на редактирование геозон
	if (not exists (select * from dbo.v_operator_rights
						where OPERATOR_ID = @operator_id
						and RIGHT_ID = 19)
			)
		begin
			RAISERROR
				(N'Нет прав на редактирование геозон',
				12, -- Severity.
				2 -- State.
				);
			return; 
		end

	--проверка права на работу с этой геозой
	if (not exists (select * from dbo.v_operator_zone_right
						where OPERATOR_ID = @operator_id
						and zone_id = @zone_id)
			)
		begin
			RAISERROR
				(N'Нет прав на редактирование указанной зоны',
				12, -- Severity.
				3 -- State.
				);
			return; 
		end
	
	declare @trailID int;
	insert into dbo.TRAIL (SESSION_ID, TRAIL_TIME)
	values (@session_id, getdate());
	
	select @trailID = @@IDENTITY;
		
		/*DECLARE @trails table(ZONE_DISTANCE_STANDART_ID int, 
						ZONE_FROM int, ZONE_TO int, 
						DISTANCE int, [Action] nvarchar(6));

		*/
	--print @trailID;

	insert into @primitives (PRIMITIVE_ID)
	select [PRIMITIVE_ID] from dbo.GEO_ZONE_PRIMITIVE where zone_id = @zone_id;

	insert into @vartexes (VERTEX_ID)
	select VERTEX_ID from dbo.ZONE_PRIMITIVE_VERTEX 
	where [PRIMITIVE_ID] in (select [PRIMITIVE_ID] from @primitives);

	delete from ZONE_MATRIX where zone_id = @zone_id;

	select OPERATOR_ID, ZONE_ID, RIGHT_ID, ALLOWED, TRAIL_ID, [ACTION], ACTUAL_TIME into #H_OPERATOR_ZONE from H_OPERATOR_ZONE where 1=0

	delete from dbo.OPERATOR_ZONE
	output DELETED.OPERATOR_ID, DELETED.ZONE_ID, DELETED.RIGHT_ID, DELETED.ALLOWED, @trailID, 'DELETE', getdate()
	into #H_OPERATOR_ZONE (OPERATOR_ID, ZONE_ID, RIGHT_ID, ALLOWED, TRAIL_ID, [ACTION], ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_OPERATOR_ZONE
	select * from #H_OPERATOR_ZONE;
	
	drop table #H_OPERATOR_ZONE;


	select PRIMITIVE_VERTEX_ID, PRIMITIVE_ID, VERTEX_ID, [ORDER], TRAIL_ID, [ACTION], ACTUAL_TIME into #H_ZONE_PRIMITIVE_VERTEX from H_ZONE_PRIMITIVE_VERTEX where 1=0

	delete from dbo.ZONE_PRIMITIVE_VERTEX
	output DELETED.PRIMITIVE_VERTEX_ID, DELETED.PRIMITIVE_ID, DELETED.VERTEX_ID, DELETED.[ORDER], @trailID, 'DELETE', getdate()
	into #H_ZONE_PRIMITIVE_VERTEX (PRIMITIVE_VERTEX_ID, PRIMITIVE_ID, VERTEX_ID, [ORDER], TRAIL_ID, [ACTION], ACTUAL_TIME)
	where [PRIMITIVE_ID] in (select [PRIMITIVE_ID] from @primitives);

	insert into H_ZONE_PRIMITIVE_VERTEX
	select * from #H_ZONE_PRIMITIVE_VERTEX;

	drop table #H_ZONE_PRIMITIVE_VERTEX;

	select VERTEX_ID, MAP_ID, X, Y, VISIBLE, [ENABLE], TRAIL_ID, ACTION, ACTUAL_TIME into #H_MAP_VERTEX from H_MAP_VERTEX where 1=0

	delete from dbo.MAP_VERTEX
	output DELETED.VERTEX_ID, DELETED.MAP_ID, DELETED.X, DELETED.Y, DELETED.VISIBLE, DELETED.[ENABLE], @trailID, 'DELETE', getdate()
	into #H_MAP_VERTEX(VERTEX_ID, MAP_ID, X, Y, VISIBLE, [ENABLE], TRAIL_ID, ACTION, ACTUAL_TIME)
	where VERTEX_ID in (select VERTEX_ID from @vartexes);

	insert into H_MAP_VERTEX select * from #H_MAP_VERTEX;

	drop table	#H_MAP_VERTEX;


	select GEO_ZONE_PRIMITIVE_ID, ZONE_ID, PRIMITIVE_ID, [ORDER], TRAIL_ID, ACTION, ACTUAL_TIME into #H_GEO_ZONE_PRIMITIVE from H_GEO_ZONE_PRIMITIVE where 1=0

	delete from dbo.GEO_ZONE_PRIMITIVE
	output DELETED.GEO_ZONE_PRIMITIVE_ID, DELETED.ZONE_ID, DELETED.PRIMITIVE_ID, DELETED.[ORDER], @trailID, 'DELETE', getdate()
	into #H_GEO_ZONE_PRIMITIVE(GEO_ZONE_PRIMITIVE_ID, ZONE_ID, PRIMITIVE_ID, [ORDER], TRAIL_ID, ACTION, ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_GEO_ZONE_PRIMITIVE select * from #H_GEO_ZONE_PRIMITIVE;

	drop table #H_GEO_ZONE_PRIMITIVE;

	
	select PRIMITIVE_ID, PRIMITIVE_TYPE, [NAME], RADIUS, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME into #H_ZONE_PRIMITIVE from H_ZONE_PRIMITIVE where 1=0

	delete from dbo.ZONE_PRIMITIVE
	output DELETED.PRIMITIVE_ID, DELETED.PRIMITIVE_TYPE, DELETED.[NAME], DELETED.RADIUS, DELETED.DESCRIPTION, @trailID, 'DELETE', getdate()
	into #H_ZONE_PRIMITIVE(PRIMITIVE_ID, PRIMITIVE_TYPE, [NAME], RADIUS, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME)
	where [PRIMITIVE_ID] in (select [PRIMITIVE_ID] from @primitives);

	insert into H_ZONE_PRIMITIVE(PRIMITIVE_ID, PRIMITIVE_TYPE, [NAME], RADIUS, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME) 
	select PRIMITIVE_ID, PRIMITIVE_TYPE, [NAME], RADIUS, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME from #H_ZONE_PRIMITIVE;

	drop table #H_ZONE_PRIMITIVE;

	select ZONE_VEHICLE_ID, ZONE_ID, VEHICLE_ID, TRAIL_ID, ACTION, ACTUAL_TIME into #H_ZONE_VEHICLE from H_ZONE_VEHICLE where 1=0

	delete from dbo.ZONE_VEHICLE
	output DELETED.ZONE_VEHICLE_ID, DELETED.ZONE_ID, DELETED.VEHICLE_ID, @trailID, 'DELETE', getdate()
	into #H_ZONE_VEHICLE(ZONE_VEHICLE_ID, ZONE_ID, VEHICLE_ID, TRAIL_ID, ACTION, ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_ZONE_VEHICLE select * from #H_ZONE_VEHICLE;

	drop table #H_ZONE_VEHICLE;


	select ZONE_VEHICLEGROUP_ID, ZONE_ID, VEHICLEGROUP_ID, TRAIL_ID, ACTION, ACTUAL_TIME into #H_ZONE_VEHICLEGROUP from H_ZONE_VEHICLEGROUP where 1=0

	delete from dbo.ZONE_VEHICLEGROUP
	output DELETED.ZONE_VEHICLEGROUP_ID, DELETED.ZONE_ID, DELETED.VEHICLEGROUP_ID, @trailID, 'DELETE', getdate()
	into #H_ZONE_VEHICLEGROUP(ZONE_VEHICLEGROUP_ID, ZONE_ID, VEHICLEGROUP_ID, TRAIL_ID, ACTION, ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_ZONE_VEHICLEGROUP select * from #H_ZONE_VEHICLEGROUP;
	drop table #H_ZONE_VEHICLEGROUP;

	select ZONEGROUP_ZONE_ID, ZONEGROUP_ID, ZONE_ID, TRAIL_ID, ACTION, ACTUAL_TIME into #H_ZONEGROUP_ZONE from H_ZONEGROUP_ZONE where 1=0

	delete from dbo.ZONEGROUP_ZONE
	output DELETED.ZONEGROUP_ZONE_ID, DELETED.ZONEGROUP_ID, DELETED.ZONE_ID,  @trailID, 'DELETE', getdate()
	into #H_ZONEGROUP_ZONE(ZONEGROUP_ZONE_ID, ZONEGROUP_ID, ZONE_ID, TRAIL_ID, ACTION, ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_ZONEGROUP_ZONE select * from #H_ZONEGROUP_ZONE;

	drop table #H_ZONEGROUP_ZONE;

	select ROUTE_GEOZONE_ID, ROUTE_ID, GEOZONE_ID, STATUS, TRAIL_ID, ACTION, ACTUAL_TIME into #H_ROUTE_GEOZONE from H_ROUTE_GEOZONE where 1=0

	delete from dbo.ROUTE_GEOZONE
	output DELETED.ROUTE_GEOZONE_ID, DELETED.ROUTE_ID, DELETED.GEOZONE_ID, DELETED.STATUS, @trailID, 'DELETE', getdate()
	into #H_ROUTE_GEOZONE(ROUTE_GEOZONE_ID, ROUTE_ID, GEOZONE_ID, STATUS, TRAIL_ID, ACTION, ACTUAL_TIME)
	where GEOZONE_ID = @zone_id;

	insert into H_ROUTE_GEOZONE select * from #H_ROUTE_GEOZONE;

	drop table #H_ROUTE_GEOZONE;


	select ZONE_ID, [NAME], COLOR, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME into #H_GEO_ZONE from H_GEO_ZONE where 1=0

	delete from GEO_ZONE
	output DELETED.ZONE_ID, DELETED.[NAME], DELETED.COLOR, DELETED.DESCRIPTION, @trailID, 'DELETE', getdate()
	into #H_GEO_ZONE(ZONE_ID, [NAME], COLOR, DESCRIPTION, TRAIL_ID, ACTION, ACTUAL_TIME)
	where zone_id = @zone_id;

	insert into H_GEO_ZONE select * from #H_GEO_ZONE;

	drop table #H_GEO_ZONE;
end


