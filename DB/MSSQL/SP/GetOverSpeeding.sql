/****** Object:  StoredProcedure [dbo].[GetOverSpeeding]    Script Date: 04/22/2008 11:25:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------

ALTER PROCEDURE [dbo].[GetOverSpeeding]
	@time_from 			int,
	@time_to 			int,
	@vehiclegroup_id 	int,
	@vehicle_id			int, 
	@speed_top 			int,
	@operatorID			int
AS
	--0. Получаем список машин
	create table #vehicle (id int primary key clustered);

	if @vehiclegroup_id is not null and @vehiclegroup_id > 0
		begin
			insert into #vehicle (id)
			select Vehicle_ID
				from dbo.VehicleGroup_Vehicle vg_v
				where vg_v.VehicleGroup_ID = @vehiclegroup_id		
					and 2 = (
						select count(*) 
						from v_operator_vehicle_right 
						where vehicle_id=vg_v.VEHICLE_ID 
							and operator_Id = @operatorID 
							and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/)
					)
					and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=vg_v.VEHICLE_ID and ignore.operator_id=@operatorID and ignore.right_id = 120)
		end
	else 
	if @vehicle_id is not null and @vehicle_id > 0
		begin
			insert into #vehicle (id)
			select v.vehicle_id
			from vehicle v
			where v.VEHICLE_ID = @vehicle_id 
				and 2 = (
					select count(*) 
					from v_operator_vehicle_right 
					where vehicle_id=v.VEHICLE_ID 
						and operator_Id = @operatorID 
						and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/)
				)
				and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=v.VEHICLE_ID and ignore.operator_id=@operatorID and ignore.right_id = 120)
		end
	else
		begin
			insert into #vehicle (id)
			select v.vehicle_id
			from vehicle v
			where 2 = (
				select count(*) 
				from v_operator_vehicle_right 
				where vehicle_id=v.VEHICLE_ID 
					and operator_Id = @operatorID 
					and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/)
			)
			and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=v.VEHICLE_ID and ignore.operator_id=@operatorID and ignore.right_id = 120)
		end
	
	--1. Получаем все точки с превышением скорости
	create table #OverSpeedingPoints (Vehicle_ID int, Row_ID int, Log_Time int, primary key clustered (Vehicle_ID, Row_ID));

	insert into #OverSpeedingPoints (Vehicle_ID, Row_ID, Log_Time)
		select 
			gpsl.Vehicle_ID,
			row_number () over (partition by gpsl.Vehicle_ID order by gpsl.Log_Time asc),
			gpsl.Log_Time
		from #vehicle v
		join dbo.GPS_Log gpsl (nolock) on gpsl.Vehicle_ID = v.id
		where 1=1
		  and gpsl.Log_Time between @time_from and @time_to
		  and @speed_top < gpsl.Speed

	--2. Получаем левые и правые границы областей, где обнаружено превышение скорости
	--Условие связности для области: расстояние между точками не больше @duration, например, минута
	declare @duration int;
	set @duration = 120;	--TODO: вынести в параметр отчета?

	create table #OverSpeedingFrom (Vehicle_ID int, Row_ID int, Log_Time int, primary key clustered (Vehicle_ID, Row_ID));
	create table #OverSpeedingTo   (Vehicle_ID int, Row_ID int, Log_Time int, primary key clustered (Vehicle_ID, Row_ID));

	insert into #OverSpeedingFrom (Vehicle_ID, Row_ID, Log_Time)
		select c.Vehicle_ID,
			   row_number () over (partition by c.Vehicle_ID order by c.Log_Time asc),
			   c.Log_Time
			from #OverSpeedingPoints c
			where not exists (select 1 
				from #OverSpeedingPoints p 
				where 
					p.Vehicle_ID = c.Vehicle_ID and
					p.Row_ID = c.Row_ID - 1 and 
					c.Log_Time - p.Log_Time < @duration)

	insert into #OverSpeedingTo (Vehicle_ID, Row_ID, Log_Time)
		select c.Vehicle_ID,
			   row_number () over (partition by c.Vehicle_ID order by c.Log_Time asc),
			   c.Log_Time
			from #OverSpeedingPoints c
			where not exists (select 1 
				from #OverSpeedingPoints n 
				where
					n.Vehicle_ID = c.Vehicle_ID and
					n.Row_ID = c.Row_ID + 1 and 
					n.Log_Time - c.Log_Time < @duration)

	drop table #OverSpeedingPoints;

	create table #result
	(
		Vehicle_ID int, 
		From_Log_Time int, 
		To_Log_Time int,
		Log_Date_Time datetime,
		Speed int,		
		Driver nvarchar(400)
	)

	--3. Соединяем получившиеся отрезки
	
	insert into #result (Vehicle_ID, From_Log_Time, To_Log_Time, Log_Date_Time, Speed)	
		select 
				[from].Vehicle_ID,
				[from].Log_Time,
				[to].Log_Time,
				dateadd(ss, [from].Log_Time, '1970'),
				max(gpsl.Speed)				
			from #OverSpeedingFrom [from]
			join #OverSpeedingTo [to] on [to].Vehicle_ID = [from].Vehicle_ID and 
										 [to].Row_ID = [from].Row_ID
			join dbo.GPS_Log gpsl (nolock) on gpsl.Vehicle_ID = [from].Vehicle_ID 
								          and gpsl.Log_Time between [from].Log_Time and [to].Log_Time
			where 0 < [to].Log_Time - [from].Log_Time	/*Не учитываем случайные выбросы*/
			group by 
				[from].Vehicle_ID,
				[from].Log_Time,
				[to].Log_Time;		

	drop table #OverSpeedingFrom;
	drop table #OverSpeedingTo;

	create nonclustered index [IX_MID] 
		on #result (Vehicle_ID asc, log_date_time asc);
		
	update r		
		set driver = 
			case 
				when exists (select 1 from dbo.WayBill_Header wbh where wbh.Vehicle_ID = r.Vehicle_ID) 
				then (		
					(
						select top 1 D.DRIVER_NAME 
							from (
								select 
									D.DRIVER_ID,
									D.SHORT_NAME + ' ' + D.[NAME] as DRIVER_NAME,
									WBT.BEGIN_TIME,
									WBT.END_TIME,
									WBH.VEHICLE_ID
								from 
									DRIVER as D 
									inner join WAYBILL as WB on D.DRIVER_ID = WB.DRIVER_ID
									inner join WB_TRIP as WBT on WB.WAYBILL_ID = WBT.WAYBILL_ID
									inner join WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID 
								where
									WBH.VEHICLE_ID in (select Vehicle_ID from #vehicle)
									and WBH.WAYBILL_DATE > (dateadd(s, @time_from, '1970') - 1) and WBH.WAYBILL_DATE <= dateadd(s, @time_to, '1970')
								)as D 
										where D.VEHICLE_ID = r.Vehicle_ID
											and D.BEGIN_TIME <= r.log_date_time and D.END_TIME >= r.log_date_time
										)
									)
				else
					(select top(1) isnull(o.First_Name + ' ', '') + isnull(o.Last_name, '')
						from dbo.Owner o
						join dbo.Vehicle_Owner vo on vo.Owner_ID = o.Owner_ID
						where vo.Vehicle_ID = r.Vehicle_ID
						order by vo.Vehicle_Owner_ID desc)
				end		
		from #result r
		option(MAXDOP 1,KEEPFIXED PLAN)
		
	select 
		_r.Vehicle_ID,
		_r.from_log_time, 
		_r.to_log_time,
		_r.speed,
		_r.driver,
		run = dbo.CalculateDistanceFN(from_log_time, _r.to_log_time, _r.Vehicle_ID)/1000
	from 
		#result _r
		join vehicle v on _r.Vehicle_ID = v.vehicle_id
		
	drop table #result
	drop table #vehicle

