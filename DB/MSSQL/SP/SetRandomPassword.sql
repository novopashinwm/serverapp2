if exists (
	select * from sys.procedures where name = 'SetRandomPassword'
)
	drop procedure SetRandomPassword
go

create procedure SetRandomPassword
(
	@vehicle_id int
)
as

declare @oldPassword varchar(32)
declare @phone varchar(20)
declare @alphabet varchar(255)
declare @passwordLength int
declare @smsTemplate varchar(255)
declare @controller_id int

select @oldPassword = case when len(ci.password) > 0 then ci.password else ct.DefaultPassword end
	 , @phone = p.Value
	 , @passwordLength = ct.PasswordLength
	 , @alphabet = ct.PasswordAlphabet
	 , @smsTemplate = ct.SmsTemplateForPasswordChange
	 , @controller_id = c.controller_id
	 from controller c
	 join controller_type ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
	 join controller_info ci on ci.controller_id = c.controller_id
	 join contact p on p.ID = c.PhoneContactID
	 where c.vehicle_id = @vehicle_id

if (@controller_id is null or @oldPassword is null or @phone is null or @passwordLength is null or @alphabet is null or @smsTemplate is null)
	return;

declare @newPassword varchar(32)
exec GetRandomString @passwordLength, @newPassword out, @alphabet

if @newPassword is null
	return

if not exists (
	select 1
		from H_CONTROLLER_INFO h
		where h.CONTROLLER_ID = @controller_id
		  and h.PASSWORD = @oldPassword)
begin
	insert into H_CONTROLLER_INFO (CONTROLLER_ID, PASSWORD, ACTUAL_TIME, ACTION)
		values (@controller_id, @oldPassword, GETUTCDATE(), 'UPDATE')

	if @@ROWCOUNT = 0
		return
end

insert into H_CONTROLLER_INFO (CONTROLLER_ID, PASSWORD, ACTUAL_TIME, ACTION)
	values (@controller_id, @newPassword, GETUTCDATE(), 'UPDATE')

if @@ROWCOUNT = 0
	return

update ci
	set Password = @newPassword
	from controller_info ci
	where ci.CONTROLLER_ID = @controller_id

if @@ROWCOUNT = 0
	return;

declare @smsText varchar(255) = @smsTemplate
set @smsText = replace(@smsText, '<%-old%>', @oldPassword)
set @smsText = replace(@smsText, '<%-new%>', @newPassword)

exec SendSms @smsText, @phone