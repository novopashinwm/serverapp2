
/****** Object:  StoredProcedure [dbo].[GetDataForPointPropertiesDialog]    Script Date: 12/18/2008 18:08:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetDataForPointPropertiesDialog] (

	@point_id as int

) AS

BEGIN
	SELECT [POINT_ID]
      ,[POINT_KIND_ID]
      ,[NAME]
      ,[X]
      ,[Y]
      ,[RADIUS]
      ,[ADDRESS]
      ,[COMMENT]
      ,[BUSSTOP_ID]
      ,[VERTEX_ID]
	FROM [dbo].[POINT]
	WHERE POINT_ID=@point_id

	select * from [POINT_KIND]
	
	select mv.* from dbo.MAP_VERTEX mv
		join [POINT] p on p.POINT_ID=@point_id and p.VERTEX_ID = mv.VERTEX_ID

END











