if exists (select * from sys.objects where name = 'GetOrCreateSimIDByDeviceId')
	drop procedure GetOrCreateSimIDByDeviceId;
go

create procedure GetOrCreateSimIDByDeviceId
(
	@deviceId varchar(255)
)
as
begin

	set nocount on

	declare @SimID varchar(32)

	declare @asidId int = (select a.ID from Asid a where a.DeviceId = @deviceId)

	if (@asidId is null)
	begin
		insert into Asid (DeviceId, AllowMlpRequest, AllowSecurityAdministration, WarnAboutLocation, SimID)
			select @deviceId, 1, 1, 0, replace(convert(varchar(36), NEWID()), '-', '')
				where not exists (select * from Asid a with (XLOCK, SERIALIZABLE) where a.DeviceId = @deviceId)
				
		set @asidId = (select a.ID from Asid a where a.DeviceId = @deviceId)
	end	

	set @SimID = (select SimID from Asid where ID = @asidId)

	if (@SimID is null)
	begin
		update a
			set SimID = replace(convert(varchar(36), NEWID()), '-', '')
			from Asid a with (XLOCK, SERIALIZABLE)
			where ID = @asidId
			  and SimID is null
		
		set @SimID = (select SimID from Asid where ID = @asidId)
	end

	select SimId = @SimID
	  
end