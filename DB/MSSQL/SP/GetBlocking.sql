if exists (
	select * from sys.objects where name = 'GetBlocking'
)
	drop procedure GetBlocking
go

create procedure GetBlocking
(
	@simId varchar(32) = null
) 
as 
begin

set nocount on;

declare @asidID int = (select a.ID
						from Asid a
						where a.SimID = @simId)

if @asidID is null
	return;

select bb.BlockingDate
 from Billing_Blocking bb
 where bb.Asid_ID = @asidID

end