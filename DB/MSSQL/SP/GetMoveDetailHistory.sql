
IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'GetMoveDetailHistory'
        AND 	  type = 'P')
    DROP PROCEDURE GetMoveDetailHistory
GO


CREATE  PROCEDURE [dbo].[GetMoveDetailHistory]
	@count int, 
	@interval int, 
	@time_from DateTime, 
	@time_to DateTime,  
	@vehicle_id int,
	@workingIntervals dbo.Log_Time_Span_Param readonly
as

set nocount on

if (@interval is null or @interval <= 0)
	set @interval = 60

declare
 @time_from_int int,  
 @time_to_int int  

select
	@time_from_int = datediff(s, '1970', @time_from),
	@time_to_int   = datediff(s, '1970', @time_to)

declare @WorkingIntervalLogTimeMin int	-- Содержит первую точку, рабочего интервала.
declare @DateFromLogTimeMin int			-- Содержит точку начала интервала отчета.
declare @LogTimeFirst int				-- Содержит первую точку, от которой необходимо вести отсчет пробега.

-- Задаем минимальные значения.
select
	@WorkingIntervalLogTimeMin = (select min(Log_Time_From) from @workingIntervals),
	@DateFromLogTimeMin = @time_from_int

-- Выбираем точку с которой начнем строить отчет. В случае если интервал задан(!) и шире чем указанный интервал отчета
-- берем "время отчета с", иначе точку интервала рабочих часов.
if (@WorkingIntervalLogTimeMin >= @DateFromLogTimeMin)
	set @LogTimeFirst = @WorkingIntervalLogTimeMin
else
	set @LogTimeFirst = @DateFromLogTimeMin

-- Временная таблица со всем массивом точек.
create table #MonitoreeLog (
 rowId int identity primary key,
 log_time int,
 lng float,
 lat float,
 odometer float
)

insert into #MonitoreeLog (log_time, lng, lat, odometer)
-- Веха.
select
	t1.log_time,
	t1.lng,
	t1.lat,
	t1.odometer
from (
	select top 1
		sl.log_time,
		gl.lng,
		gl.lat,
		sl.odometer
	from Statistic_Log sl with (nolock)
	left outer join geo_log gl with (nolock) on gl.Vehicle_Id = sl.Vehicle_Id and gl.log_time = sl.log_time
	where sl.vehicle_id = @vehicle_id
	  -- 10 минут актуальности данных.
	   and sl.log_time between @LogTimeFirst - 60 * 10 and @LogTimeFirst - 1
	order by sl.log_time desc) t1

insert into #MonitoreeLog (log_time, lng, lat, odometer)
-- Основные данные.
select
	lt.Log_Time,
	gl.Lng,
	gl.Lat,
	sl.Odometer
from @workingIntervals wi
inner join Log_Time lt with (nolock) on
	    lt.Vehicle_ID = @vehicle_id
	and lt.Log_Time between wi.Log_Time_From and wi.Log_Time_To
left outer join Geo_Log gl with (nolock) on
	    gl.Vehicle_Id = lt.Vehicle_Id
	and gl.Log_time = lt.Log_Time
left outer join Statistic_Log sl with (nolock) on
	    sl.Vehicle_Id = lt.Vehicle_Id
	and sl.Log_time = lt.Log_Time
where
	lt.Log_Time between @time_from_int and @time_to_int
	and sl.odometer is not null
-- Простое исправление параллельной вставки, можно решить и сортировкой по времени вставляемых данных
OPTION (MAXDOP 1)

declare @actualFirstLogTime int 
select @actualFirstLogTime = (select min(log_time) from #MonitoreeLog)

select
	--(log_time - @actualFirstLogTime) / @interval as Clause, --debug feature
	dense_rank() over (
		--partition by (log_time - @actualFirstLogTime) / @interval
		order by (log_time - @actualFirstLogTime) / @interval asc
	) as groupingInterval,
	*
into #MonitoreeLogRanked
from #MonitoreeLog
order by log_time asc

select
	m1.groupingInterval,
	m1.log_time,
	m1.lng,
	m1.lat,
	(m2.odometer - m1.odometer) / 100 / 1000 as TotalRunKm
into #MonitoreeLogTotal
from #MonitoreeLogRanked m1
join #MonitoreeLogRanked m2 on
	m2.rowId = m1.rowId + 1

-- Счетчики.
select 'AllCount' = (select Count(Log_Time) from #MonitoreeLogRanked)  
select 'AllCountWithInterval' = (select count(1) from (select groupingInterval from #MonitoreeLogTotal group by groupingInterval) t1)
Select 'From' = Convert(char(10), @time_from_int),     
 'To' = Convert(char(10), @time_to_int),  
 '' as WorkFrom, -- Блок необходим для корректного приведения  
 '' as WorkTo  -- к типизированному датасету на клиенте.  

-- ТС и водители  
select distinct
	m1.Log_Time,
	V.Garage_Number,
	V.Public_Number,
	DR.Short_Name,
	DR.Ext_number
from WB_TRIP as WBT  
join WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
join VEHICLE as V on WBH.VEHICLE_ID = V.VEHICLE_ID
join WAYBILL as W on WBT.WAYBILL_ID = W.WAYBILL_ID
join DRIVER as DR on W.DRIVER_ID = DR.DRIVER_ID
join #MonitoreeLog m1 on m1.log_time between
	datediff(s, '1970', WBT.Begin_Time) and datediff(s, '1970', WBT.End_Time)
where
	V.VEHICLE_ID = @vehicle_ID and
	WBT.BEGIN_TIME <= @time_to and WBT.END_TIME >= @time_from


-- http://stackoverflow.com/a/3088735/509450
;with cte as
(
    select
		m1.groupingInterval,
		m1.log_time,
		m1.lng,
		m1.lat,
		rn = row_number() over (partition by groupingInterval order by log_time)
    from #MonitoreeLogTotal as m1
)

-- Результирующий набор данных для отчета.
-- TODO: Избавиться от неиспользуемых полей.
select
	v.vehicle_id as Monitoree_ID,
	v.garage_number as Garage_Number,
	v.public_number as Public_Number,
	null as Short_Name,
	null as Ext_Number,
	Convert(char(10), cte1.log_time) as Log_Time,
	cte1.lng as X,
	cte1.lat as Y,
	case
		when isnull(cte2.log_time - cte1.log_time, 0) = 0
		then convert(int, g1.GroupTotalRunKm / (@time_to_int - cte1.log_time) * 60 * 60)
		else convert(int, g1.GroupTotalRunKm / (cte2.log_time - cte1.log_time) * 60 * 60)
	end as Speed,
	ROUND(g1.GroupTotalRunKm, 3) as Dist,
	'' as Address,
	null as BUSSTOP,
	null as TEMPERATURE_
from (select * from cte where rn = 1) cte1
left join (select * from cte where rn = 1) cte2 on
	cte2.groupingInterval = cte1.groupingInterval + 1
join (select
		groupingInterval,
		sum(TotalRunKm) as GroupTotalRunKm
		from #MonitoreeLogTotal
		group by groupingInterval) g1 on g1.groupingInterval = cte1.groupingInterval
join vehicle v on
	v.vehicle_id = @vehicle_id
order by cte1.groupingInterval

drop table #MonitoreeLogTotal
drop table #MonitoreeLogRanked    
drop table #MonitoreeLog