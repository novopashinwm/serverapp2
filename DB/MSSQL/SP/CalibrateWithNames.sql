CreateProcedure 'CalibrateWithNames'
go
alter procedure CalibrateWithNames
(
	@device_id varchar(32),
	@controller_sensor_legend_name nvarchar(32),
	@controller_sensor_descript nvarchar(32),
	@param dbo.Calibrate_Param readonly
)
as

declare @vehicle_id int, @controller_sensor_number int, @controller_sensor_legend_number int

select 
	  @vehicle_id = c.vehicle_id
	, @controller_sensor_number = cs.number
	, @controller_sensor_legend_number = l.number
	from controller_info ci 
	join controller c on c.CONTROLLER_ID = ci.CONTROLLER_ID
	join controller_sensor cs on cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID and cs.Descript = @controller_sensor_descript
	join controller_sensor_legend l on l.name = @controller_sensor_legend_name
	where ci.DEVICE_ID = convert(varbinary(32), @device_id)

exec Calibrate @vehicle_id, @controller_sensor_legend_number, @controller_sensor_number, @param