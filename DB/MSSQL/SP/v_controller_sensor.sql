if exists (
	select * from sys.views where name = 'v_controller_sensor'
)
	drop view v_controller_sensor
go

create view v_controller_sensor
as
select ControllerTypeName = ct.TYPE_NAME, InputNumber = cs.NUMBER, InputName = cs.DESCRIPT, InputType = cst.NAME, cs.Mandatory, cs.CONTROLLER_SENSOR_ID, DefaultLegendNumber = l.Number, DefaultLegend = l.NAME
	from controller_type ct
	join controller_sensor cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join CONTROLLER_SENSOR_TYPE cst on cst.CONTROLLER_SENSOR_TYPE_ID = cs.CONTROLLER_SENSOR_TYPE_ID
	left outer join CONTROLLER_SENSOR_LEGEND l on l.CONTROLLER_SENSOR_LEGEND_ID = cs.Default_Sensor_Legend_ID