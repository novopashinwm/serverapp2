/****** Object:  StoredProcedure [dbo].[GetDriverGroupsAndDrivers]    Script Date: 07/15/2010 09:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[GetDriverGroupsAndDrivers] 
@operator_id int = null
AS
	select distinct dg.* 
	from [DRIVERGROUP] dg
	left join v_operator_driver_groups_right rr on rr.drivergroup_id = dg.DRIVERGROUP_ID
	where (@operator_id is null or rr.operator_id = @operator_id)
	
	select distinct d.* 
	from [DRIVER] d
	left join v_operator_driver_right rr on rr.driver_id = d.DRIVER_ID
	where (@operator_id is null or rr.operator_id = @operator_id)
	order by [SHORT_NAME]
	
	select distinct dd.* 
	from [DRIVERGROUP_DRIVER] dd
	left join v_operator_driver_right rr on rr.driver_id = dd.DRIVER_ID
	left join v_operator_driver_groups_right rr2 on rr2.drivergroup_id = dd.DRIVERGROUP_ID
	where (@operator_id is null or( rr.operator_id = @operator_id and rr2.operator_id = @operator_id ))
	
	select distinct d.* 
	from [DEPARTMENT] d
	left join v_operator_department_right rr on rr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where (@operator_id is null or rr.operator_id = @operator_id)


