﻿IF (OBJECT_ID(N'[dbo].[AddMonitoreeLog]') IS NOT NULL)
	DROP PROCEDURE [dbo].[AddMonitoreeLog];
GO

CREATE PROCEDURE [dbo].[AddMonitoreeLog]
(
	@uniqueID      int,                 -- vehicle key
	@log_time      int,                 -- UTC time
	@long          real,                -- longitude
	@lat           real,                -- latitude
	@speed         int          = NULL, -- speed
	@course        int          = NULL, -- course
	@media         int,                 -- MEDIA_ID
	@satellites    int          = NULL, -- SATELLITES qty
	@hdop          numeric(4,2) = NULL,
	@firmware      varchar(32)  = NULL, -- FIRMWARE
	@height        int          = NULL,
	@validPosition bit          = NULL, --корректность позиции
	@ip            varchar(40)  = NULL, --IP, с которого пришли данные
	@protocol      varchar(80)  = NULL, -- Протокол, по которому пришли данные
	@debug         bit          = 0     --Режим отладки
)
AS
BEGIN
	IF (0 = ISNULL(@debug, 0))
		SET NOCOUNT ON;

	DECLARE @rows int = 0;

	EXEC [dbo].[AddIPLog] @uniqueID, @ip, @protocol

	IF (@speed < -1)
		SET @speed = NULL

	IF (@satellites < 0 OR 255 < @satellites)
		SET @satellites = NULL

	DECLARE @compressedCourse tinyint =
	(
		CASE
			WHEN (@course % 360) = 0 THEN 0
			WHEN (@course < 000)     THEN (@course % 360) + 360
			ELSE (@course % 360)
		END
	) * 256 / 360;

	IF (@validPosition = 1 AND 1.1 <= @hdop)
	BEGIN
		SET @validPosition = 0
		EXEC [dbo].[Add_Geo_Log_Ignored] @uniqueID, @log_time, 0, @lat, @long, @Speed, @Course, @Height, @hdop
	END

	IF (250 < @speed)
		SET @speed = 250

	DECLARE @controller int

	IF (@uniqueID IS NULL OR @uniqueID < 1)
		RETURN -1

	-- Отсекаем будущее большее чем 2 часа
	IF (DATEDIFF(ss, '1970', GETUTCDATE()) + 7200 < @log_time)
		return -3

	-- Отсекаем прошлое, старше 1970 года
	IF (@log_time < 0)
		return -4

	IF (@media < 0 OR 255 < @media)
		SET @media = 255

	DECLARE @firmwareId smallint

	IF (@firmware IS NOT NULL)
		EXEC @firmwareId = [dbo].[GetOrCreateFirmware] @firmware
	ELSE
		SET  @firmwareId = 0

	DECLARE @skipPosition bit = 0;

	--Записываются в БД только те позиции, время которых не старше полугода и не принадлежит будущему более чем на месяц
	DECLARE @currentTimeDelta int = @log_time - DATEDIFF(ss, '1970', GETUTCDATE())
	DECLARE @rows_affected    int = 0;

	IF ((@currentTimeDelta < -[dbo].[GetMaxLogAge]()) OR (30 * 24 * 3600 < @currentTimeDelta))
	BEGIN
		IF (@debug = 1)
			PRINT 'Position skipped due to time being out of range'
		SET @skipPosition = 1;
		EXEC [dbo].[Add_Geo_Log_Ignored] @uniqueID, @log_time, 3, @lat, @long, @Speed, @Course, @Height, @hdop
	END
	ELSE
	BEGIN

		DECLARE @insert_time datetime = GETUTCDATE();

		INSERT [dbo].[Log_Time]
			([Vehicle_ID], [Log_Time], [Media],           [InsertTime])
		SELECT
			 @uniqueID,    @log_time,  ISNULL(@media, 0), @insert_time
		WHERE NOT EXISTS
		(
			SELECT 1
			FROM [dbo].[Log_Time] WITH (XLOCK, SERIALIZABLE)
			WHERE [Vehicle_ID] = @uniqueID
			AND   [Log_Time]   = @log_time
		);

		SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

		----------------------------------------------------------------------------------------------
		-- Обновление последнего значения в таблице Log_Time#Lasts
		EXEC [dbo].[Log_Time#Lasts_Update]
				@vehicle_id  = @uniqueID,
				@log_time    = @log_time,
				@insert_time = @insert_time
		----------------------------------------------------------------------------------------------

		IF (0 <> @rows)
		BEGIN
			--TODO: выяснить, почему ProcessControllerInfo вызывается только при вставке
			DECLARE @time datetime = DATEADD(s, @log_time, '1970')
			EXEC [dbo].[ProcessControllerInfo] @controller, @speed out, @satellites out, @firmwareId out, @time
		END;

		--Если скорость для перемещения от предыдущей точки > 250км/ч, то отбраковываем позицию
		--Предыдущая позиция
		DECLARE @prevLogTime int, @prevLng numeric(8,5), @prevLat numeric(8,5);

		SELECT TOP(1) @prevLogTime = [Log_Time], @prevLng = [Lng], @prevLat = [Lat]
		FROM [dbo].[Geo_Log] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @uniqueID and [Log_Time] < @log_time
		ORDER BY [Log_Time] DESC

		IF (
			@skipPosition  = 0         AND
			@validPosition = 1         AND
			@prevLogTime   IS NOT NULL AND
			@prevLng       IS NOT NULL AND
			@prevLat       IS NOT NULL)
		BEGIN
			DECLARE @prevDistance float(53) = [dbo].[GetTwoGeoPointsDistance](@prevLng, @prevLat, @long, @lat);
			-- Проверка скорости движения между точками не должна превышать 250 км/ч
			IF ((@prevDistance/1000) / (CAST(@log_time - @prevLogTime AS float)/3600) > 250)
			BEGIN
				-- Если тип предыдущей позиции GPS
				IF (NOT EXISTS(
					SELECT 1
					FROM [dbo].[Position_Radius] WITH (NOLOCK)
					where [Vehicle_ID] = @uniqueID
					AND   [Log_Time]   = @prevLogTime
					AND   [Type]      <> 0))
				BEGIN
					-- Проверяем предыдущую игнорированную позицию
					DECLARE @prevIgnoredLogTime int, @prevIgnoredLng numeric(8,5), @prevIgnoredLat numeric(8,5);
					SELECT TOP(1)
						@prevIgnoredLogTime = [Log_Time],
						@prevIgnoredLng     = [Lng],
						@prevIgnoredLat     = [Lat]
					FROM [dbo].[Geo_Log_Ignored] WITH(NOLOCK)
					WHERE [Vehicle_ID] = @uniqueID and [Log_Time] < @log_time
					ORDER BY [Log_Time] DESC

					DECLARE @prevIgnoredDistance float(53) = [dbo].[GetTwoGeoPointsDistance](@prevIgnoredLng, @prevIgnoredLat, @long, @lat);
					-- Проверка скорости движения между точками не должна превышать 250 км/ч (тут проверяем игнорированную)
					IF ((@prevIgnoredDistance/1000) / (CAST(@log_time - @prevIgnoredLogTime AS float)/3600) > 250)
					BEGIN
						-- Если новая и предыдущая отличается перемещением с большой скоростью, то игнорируем её
						IF (@debug = 1)
							PRINT 'Position skipped due to speed excess'
						SET @skipPosition = 1;
						EXEC [dbo].[Add_Geo_Log_Ignored] @uniqueID, @log_time, 1, @lat, @long, @Speed, @Course, @Height, @hdop
					END

				END
			END
		END

		IF (@skipPosition = 0 AND @validPosition = 1)
		BEGIN
			DECLARE @ignition bit = 0

			SELECT TOP(1) @ignition = [Value]
			FROM [dbo].[v_controller_sensor_log] WITH (NOLOCK)
			WHERE [vehicle_id]   = @uniqueID
			AND   [SensorLegend] = 4 --Зажигание
			AND   [Log_Time]     BETWEEN @log_time - 900 AND @log_time
			ORDER BY [Log_Time] DESC

			IF ([dbo].[SmoothRequired](@uniqueID, @log_time, @lat, @long, @speed, @ignition) = 1)
			BEGIN
				EXEC [dbo].[Add_Geo_Log_Ignored] @uniqueID, @log_time, 2, @lat, @long, @speed, @course, @height, @hdop

				SELECT @long = @prevLng, @lat = @prevLat, @speed = 0;

				SELECT TOP(1)
					@long = [Lng], @lat = [Lat]
				FROM [dbo].[Geo_Log] WITH (NOLOCK)
				WHERE [Vehicle_ID] = @uniqueID
				AND   [LOG_TIME]   < @log_time
				AND   [LOG_TIME]   > @log_time - 600
				ORDER BY [LOG_TIME] DESC
			END
		END

		IF (@skipPosition = 0)--хорошая позиция - сохраняем ее
		BEGIN
			/*Судя по тому, что при вставке в dbo.Geo_Log etc происходит нарушение PK_Geo_Log etc, 
			уровень изоляции транзакций наследуется от вызовов одних процедур к вызовам других,
			что, вероятно, связано с тем, что вызовы различных процедур работают в одних и тех же сессиях,
			извлекаемых из пула сессий/подключений к SQL-серверу.*/

			IF (@validPosition = 1)
			BEGIN
				INSERT [dbo].[Geo_Log]
					([Vehicle_ID], [LOG_TIME], [Lng], [Lat])
				SELECT
					 @uniqueID,    @log_time,  @long, @lat
				WHERE NOT EXISTS
				(
					SELECT 1
					FROM [dbo].[Geo_Log] WITH (XLOCK, SERIALIZABLE)
					WHERE [Vehicle_ID] = @uniqueID
					AND   [Log_Time]   = @log_time
				)
				SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

				IF (0 = @rows)
				BEGIN
					UPDATE [dbo].[Geo_Log]
						SET
							[Lng] = @long,
							[Lat] = @lat
					WHERE [Vehicle_ID] = @uniqueID
					AND   [LOG_TIME]   = @log_time
					AND  ([Lat] <> @lat OR [Lng] <> @long)

					SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows
				END

				----------------------------------------------------------------------------------------------
				-- Обновление последнего значения в таблице Geo_Log#Lasts
				EXEC [dbo].[Geo_Log#Lasts_Update]
						@vehicle_id = @uniqueID,
						@log_time   = @log_time,
						@lng        = @long, -- longitude
						@lat        = @Lat   -- latitude
				----------------------------------------------------------------------------------------------

			END

			IF (@validPosition = 1 OR 0 < @firmwareId)
			BEGIN
				INSERT [dbo].[GPS_Log]
					([Vehicle_ID], [Log_Time], [Satellites], [Firmware], [Altitude], [Speed], [Course])
				SELECT
					@uniqueID, @log_time,  @satellites, @firmwareId,  @height, @speed, @compressedCourse
				WHERE NOT EXISTS
				(
					SELECT 1
					FROM [dbo].[GPS_Log] WITH (XLOCK, SERIALIZABLE)
					WHERE [Vehicle_ID] = @uniqueID
					AND   [Log_Time]   = @log_time
				)
				SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

				IF (0 = @rows)
				BEGIN
					UPDATE [dbo].[GPS_Log]
						SET
							[Satellites] = @satellites,
							[Firmware]   = @firmwareId,
							[Altitude]   = @height,
							[Speed]      = @speed,
							[Course]     = @compressedCourse
					WHERE [Vehicle_ID] = @uniqueID
					AND   [Log_Time]   = @log_time
					AND
					(
						[Satellites] <> @satellites OR
						[Firmware]   <> @firmwareId OR
						[Altitude]   <> @height     OR
						[Speed]      <> @speed      OR
						[Course]     <> @compressedCourse
					)
					SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows
				END

				----------------------------------------------------------------------------------------------
				-- Обновление последнего значения в таблице GPS_Log#Lasts
				EXEC [dbo].[GPS_Log#Lasts_Update]
						@vehicle_id = @uniqueID,
						@log_time   = @log_time
				----------------------------------------------------------------------------------------------
			END
		END

		IF (@skipPosition = 0 and @rows_affected <> 0)
			EXEC [dbo].[AddStatisticLog] @uniqueID, @log_time, 0; --Считаем статистику

		SELECT Rows_Affected = @rows_affected
	END;
END
GO