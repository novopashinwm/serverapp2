IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_UpdateOperator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[H_UpdateOperator]
GO

CREATE PROCEDURE [dbo].[H_UpdateOperator]
    @TRAIL_ID int, 
    @OPERATOR_ID int, 
    @NAME nvarchar (500), 
    @LOGIN nvarchar (60), 
    @PASSWORD nvarchar (100), 
    @PHONE nvarchar (40), 
    @EMAIL nvarchar (100), 
    @GUID uniqueidentifier
AS
BEGIN
    update [dbo].[OPERATOR] SET 
        [NAME]=@NAME, 
        [LOGIN]=@LOGIN, 
        [PASSWORD]=@PASSWORD, 
        --[PHONE]=@PHONE, 
        [EMAIL]=@EMAIL, 
        [GUID]=@GUID
     WHERE [OPERATOR_ID]=@OPERATOR_ID
    
    
    insert into [dbo].[H_OPERATOR] (TRAIL_ID, ACTION, ACTUAL_TIME, [OPERATOR_ID], 
        [NAME], 
        [LOGIN], 
        [PASSWORD], 
        --[PHONE], 
        [EMAIL], 
        [GUID]
    ) VALUES (@TRAIL_ID, 'UPDATE', GETUTCDATE(), @OPERATOR_ID, 
        @NAME, 
        @LOGIN, 
        @PASSWORD, 
        --@PHONE, 
        @EMAIL, 
        @GUID
    )
END



GO


