if (exists (select * from sys.objects where name = 'CheckPasswordForWeb' and type='p'))
	drop procedure dbo.CheckPasswordForWeb
go

set QUOTED_IDENTIFIER on
go

CREATE PROCEDURE dbo.CheckPasswordForWeb
 @login varchar(100), @password varchar(100)  
AS
	set nocount on	

select Operator_ID, OneTimePassword, [PASSWORD] [Password], 
	convert(bit, case when e.HasDepartment = 1 then coalesce(d.IsLocked, 1) else 0 end) IsLocked
from operator o
	outer apply (
		select top 1 1 HasDepartment
		from v_operator_department_right od
		where od.OPERATOR_ID = o.OPERATOR_ID and od.RIGHT_ID = 104
	) e
	outer apply (
		select top 1 convert(bit, 0) IsLocked
		from v_operator_department_right od
			join DEPARTMENT d on d.DEPARTMENT_ID = od.DEPARTMENT_ID
		where od.OPERATOR_ID = o.OPERATOR_ID
			and od.RIGHT_ID = 104 and d.LockDate is null
	) d
where o.login=@login
	and o.password=@password
go
