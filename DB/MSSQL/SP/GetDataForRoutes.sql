SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[GetDataForRoutes]
	@operatorID int  = null
AS
	EXEC dbo.GetSortedRoute @operatorID;
	
	select gt.*
	from geo_trip gt 
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operatorID
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorID)
	
	
	select distinct p.* 
	from point p 
	join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
	join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operatorID
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorID)
	
	select b.* 
	from busstop b
	join v_operator_department_right dr on dr.DEPARTMENT_ID = b.DEPARTMENT_ID
	where dr.operator_id= @operatorID 
	and dr.right_id=104
	
	select d.* 
	from DEPARTMENT d
	join v_operator_department_right dr on dr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where dr.operator_id= @operatorID 
	and dr.right_id=104
