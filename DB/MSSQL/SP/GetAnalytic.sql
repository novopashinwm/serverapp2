/****** Object:  StoredProcedure [dbo].[Analytic]    Script Date: 07/14/2010 18:28:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Analytic]
	@route	int		= null,	-- ROUTE_ID
	@from	datetime,
	@to	datetime,
	@rtnum	varchar(32)	= null,	-- ROUTE.EXT_NUMBER
	@schnum	varchar(256)	= null	-- WAYOUT.EXT_NUMBERs, comma-separated
	-- one of @route, @rtnum, @schnum should be meaningful
AS

------------------------------------------------------------------------
/*
--Tї╕к
declare	
	@route	int,
	@from	datetime,
	@to	datetime,
	@rtnum	varchar(32),
	@schnum	varchar(256)

set 	@route = 0
set	@from = '11/06/2006 12:00' --'11/14/2005 9:13'
set 	@to = '11/06/2006 20:00' --'11/16/2005 9:13'
set	@rtnum = '5'--'11'
set	@schnum	= '502'--'1102'
*/
------------------------------------------------------------------------

set transaction isolation level read uncommitted

if @schnum is not null 
begin
	-- to format ,sch1,sch2,...,schN,
	set @schnum = ',' + replace(replace(@schnum, ' ', ''), ';', ',') + ','
	-- find out ROUTE_ID. it must be the only one ID
	select distinct @route = ROUTE from WAYOUT where charindex(',' + EXT_NUMBER + ',', @schnum) <> 0
end

if @route is null and @rtnum is not null 
	select @route = ROUTE_ID from ROUTE where EXT_NUMBER = @rtnum

-- check pars
if @route is null
begin
	if @rtnum is null 
		RaisError('╕----ё№  ж  --Ёж ', 16, 1)
	else 
		RaisError('╕----ё№ ╕ %s  ж  --Ёж ', 16, 1, @rtnum)
	return -1
end

-- convert times ot time_t
declare @start int, @finish int
select @start = datediff(s, '1970', @from), @finish = datediff(s, '1970', @to)

declare @crs cursor, @whid int
declare @r table(vid int)

-- temp table to store res from GET_TIME_DEVIATIONS
create table #t
	(rpid int, din int, dout int, spid int, tin int, tout int, ltin int, ltout int, 
	tdin int, tdout int, trip int, wbid int, ord int, ptname varchar(50), tripname varchar(50), value varchar(50) --)
	,TRIPKINDID int, WBTRIP_ID int)

-- cursor every WAYBILL_HEADER_ID
set @crs = cursor local read_only fast_forward for 
	select distinct 
		WAYBILL_HEADER_ID 
	from 
		WAYOUT wo 
		join SCHEDULE s on WAYOUT = WAYOUT_ID 
		join SCHEDULE_DETAIL sd on s.SCHEDULE_ID = sd.SCHEDULE_ID 
		join TRIP t on sd.SCHEDULE_DETAIL_ID = t.SCHEDULE_DETAIL_ID 
		join WB_TRIP wbt on t.TRIP_ID = wbt.TRIP_ID 
		join WAYBILL wb on wbt.WAYBILL_ID = wb.WAYBILL_ID 
	where 
		(charindex(',' + wo.EXT_NUMBER + ',', @schnum) <> 0 or @schnum is null and ROUTE = @route) 
		and wbt.BEGIN_TIME between @from and @to 
		and wbt.END_TIME between @from and @to

-- get deviations for our whids
open @crs
while 1 = 1
begin
	fetch next from @crs into @whid
	if @@fetch_status <> 0 
		break
	insert #t exec dbo.GET_TIME_DEVIATIONS @whid
end
close @crs
deallocate @crs

--select * from #t

-- get all stored data and additional info
select distinct	
	WBH.WAYBILL_DATE, 
	isnull(tt.tin, tt.tout) as COLUMN1,
	rtrim(r.EXT_NUMBER) rnum, 
	s.EXT_NUMBER snum, 
	t.TRIP trip, 
	tt.ptname, 
	dateadd(s, TIME_IN, WBH.WAYBIll_DATE) tin, 
	ltin = case tt.ltin when 0 then null else dateadd(s, tt.ltin, WBH.WAYBILL_DATE) end, 
	tt.tdin, 
	dateadd(s, TIME_OUT, WBH.WAYBIll_DATE) tout, 
	ltout = case tt.ltout when 0 then null else dateadd(s, tt.ltout, WBH.WAYBILL_DATE) end, 
	tt.tdout, 
	--GARAGE_NUMBER vnum, 
	rtrim(d.SHORT_NAME) + ' ' + rtrim(d.NAME) driver, 
	rtrim(d.EXT_NUMBER) dnum 
	--,WBT.TRIP_KIND_ID
	,tt.TRIPKINDID 
	,tt.WBTRIP_ID
into
	#res
from 
	#t as tt
	-- Є■Ї°кї√╣
	inner join WAYBILL wb on wb.WAYBILL_ID = tt.wbid
	inner join dbo.DRIVER d on d.DRIVER_ID = wb.DRIVER_ID
	-- TT
	inner join dbo.WB_TRIP as WBT on WBT.WAYBILL_ID = wb.WAYBILL_ID
	inner join dbo.WAYBILL_HEADER as WBH on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID 
	inner join dbo.VEHICLE v on v.VEHICLE_ID = WBH.VEHICLE_ID 
	-- №Ёи░и║к ° иЁ╕ °╕Ё¤°ї
	inner join dbo.SCHEDULE_POINT sp on sp.SCHEDULE_POINT_ID = tt.spid 
	inner join dbo.TRIP t on t.TRIP_ID = sp.TRIP_ID 
	inner join dbo.SCHEDULE_DETAIL sd on sd.SCHEDULE_DETAIL_ID = t.SCHEDULE_DETAIL_ID 
	inner join dbo.SCHEDULE s on s.SCHEDULE_ID = sd.SCHEDULE_ID 
	inner join dbo.WAYOUT wo on wo.WAYOUT_ID = s.WAYOUT 
	inner join dbo.ROUTE r on r.ROUTE_ID = s.ROUTE_ID and r.ROUTE_ID = wo.ROUTE
where 
	r.ROUTE_ID = @route 
	and (@schnum is null or charindex(',' + wo.EXT_NUMBER + ',', @schnum) <> 0) 
	and dateadd(s, t.BEGIN_TIME, WBH.WAYBILL_DATE) between @from and @to 
	and dateadd(s, t.END_TIME, WBH.WAYBILL_DATE) between @from and @to 
order by 
	WBH.WAYBILL_DATE, 
	snum, 
	isnull(tin, tout)


select 
	r.*,
	v.GARAGE_NUMBER vnum  
from 
	#res as r
	left join dbo.WB_TRIP as WBT on WBT.WB_TRIP_ID = r.WBTRIP_ID
	left join dbo.WAYBILL_HEADER as WBH on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID 
	left join dbo.VEHICLE v on v.VEHICLE_ID = WBH.VEHICLE_ID 
order by 
	r.WAYBILL_DATE, 
	r.snum, 
	isnull(r.tin, r.tout)


drop table #t
drop table #res



