if (exists (select * from sys.objects where name = 'GetVehicleIDByAsid' and type='p'))
	drop procedure dbo.GetVehicleIDByAsid;
go
create procedure dbo.GetVehicleIDByAsid
(
	@asid varchar(255)
)
as 
set nocount on

select top(1) c.Vehicle_ID
	from dbo.Asid a
	join dbo.MLP_Controller mlpc on mlpc.Asid_ID = a.ID
	join dbo.Controller c on c.Controller_ID = mlpc.Controller_ID
	join dbo.v_operator_vehicle_right ovr on ovr.Operator_ID = a.Operator_ID 
										 and ovr.Vehicle_ID = c.Vehicle_ID 
										 and ovr.Right_ID = 2 --security admin i.e. owner
	join Contact contact on contact.ID = a.Contact_ID and contact.Type = 3
	where contact.Value = @asid
	order by c.Vehicle_ID desc