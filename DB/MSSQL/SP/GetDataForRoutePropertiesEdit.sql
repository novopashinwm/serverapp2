/****** Object:  StoredProcedure [dbo].[GetDataForRoutePropertiesEdit]    Script Date: 07/07/2010 16:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER    PROCEDURE [dbo].[GetDataForRoutePropertiesEdit]
            @route_id int,
            @operatorID int = null
AS

select distinct r.* 
from [ROUTE] r
left join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
where @operatorID is null or rr.operator_id = @operatorID

select distinct p.* 
from point p 
join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
where @operatorID is null
or gt.CREATOR_OPERATOR_ID = @operatorID
or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)


select * from [SCHEDULE] where ROUTE_ID = @route_id
	and DAY_KIND_ID not in
	(select DAY_KIND_ID from [DAY_KIND] where DK_SET is null)
												
select distinct dk.* 
from [DAY_KIND] dk
join [DK_SET] dks on dks.DK_SET_ID = dk.DK_SET
left join v_operator_department_right dr on dr.DEPARTMENT_ID = dks.DEPARTMENT_ID
where @operatorID is null or dr.operator_id = @operatorID
	
select * from [POINT_KIND]

select distinct g.* 
from [DK_SET] g
left join v_operator_department_right dr on dr.DEPARTMENT_ID = g.DEPARTMENT_ID
where @operatorID is null or dr.operator_id = @operatorID

select * from [TRIP_KIND]

select * from [WAYOUT] where ROUTE = @route_id

select distinct g.* 
from [GRAPHIC] g
left join v_operator_department_right dr on dr.DEPARTMENT_ID = g.DEPARTMENT_ID
where @operatorID is null or dr.operator_id = @operatorID

--by A.Sveshnikov
/*
SELECT * FROM [ROUTE_TRIP] WHERE ROUTE_ID = @route_id
SELECT * FROM [GEO_TRIP] WHERE 
SELECT * FROM [GEO_SEGMENT]
*/

SELECT * FROM [ROUTE_TRIP] WHERE ROUTE_ID = @route_id
-- by Tanas
exec GetSortedGeoTrip @operatorID;
--SELECT * FROM [GEO_TRIP] --WHERE GEO_TRIP_ID IN (SELECT GEO_TRIP_ID FROM [ROUTE_TRIP] WHERE ROUTE_ID = @route_id)

select GES.*
	from GEO_SEGMENT as GES
		inner join GEO_TRIP as GT on GES.GEO_TRIP_ID = GT.GEO_TRIP_ID
		inner join POINT as P on GES.POINT = P.POINT_ID
		left join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
		left join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
		where @operatorID is null 
		or gt.CREATOR_OPERATOR_ID = @operatorID
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
	Order by 
		GES.GEO_TRIP_ID, GES.[ORDER]

SELECT * FROM SEASON
SELECT * FROM DAY_TYPE
SELECT * FROM DAY_TIME
SELECT * FROM GEO_SEGMENT_RUNTIME
--SELECT gsr.* FROM GEO_SEGMENT_RUNTIME gsr join GEO_SEGMENT gs on gsr.GEO_SEGMENT_ID = gs.GEO_SEGMENT_ID
--join GEO_TRIP gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID join ROUTE_TRIP rt on gt.GEO_TRIP_ID=rt.GEO_TRIP_ID where rt.ROUTE_ID = @route_id
SELECT * FROM FACTOR_VALUES
SELECT * FROM FACTORS

select b.* 
	from busstop b
	join v_operator_department_right dr on dr.DEPARTMENT_ID = b.DEPARTMENT_ID
	where dr.operator_id= @operatorID 
	and dr.right_id=104


/* пvр а┐ш░жпжк */
select distinct p.* 
	from PERIOD p
	left join DK_SET dks on dks.DK_SET_ID = p.DK_SET
	left join DAY_KIND dk on dk.DAY_KIND_ID = p.DAY_KIND
	left join DK_SET dks2 on dks2.DK_SET_ID = dk.DK_SET
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = dks.DEPARTMENT_ID
	left join v_operator_department_right dr2 on dr2.DEPARTMENT_ID = dks2.DEPARTMENT_ID
	left join v_operator_route_right rr on rr.ROUTE_ID = p.ROUTE
	where @operatorID is null 
	or (
		dr.operator_id = @operatorID 
		or dr2.operator_id = @operatorID
	)
	and (p.ROUTE is null or (rr.operator_id = @operatorID and p.route = @route_id))



/* table schedule_count */
select distinct dk.day_kind_id,
(select count(0) from schedule where day_kind_id = dk.day_kind_id and route_id = 8
and day_kind_id not in (select day_kind_id from day_kind where dk_set is null)) as [count]
from period p
left join dk_set ds on p.dk_set = ds.dk_set_id
left join day_kind dk on dk.dk_set = ds.dk_set_id
where (p.route is null) or (p.route = 8)


/* пvр а┐ш░жпжк */

select * from SCHEDULE_DETAIL where 1=0

select * from TRIP where 1=0

select * from SCHEDULE_POINT where 1=0


