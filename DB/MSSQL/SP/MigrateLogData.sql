set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MigrateLogData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MigrateLogData]
GO

