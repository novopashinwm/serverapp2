﻿IF (OBJECT_ID(N'[dbo].[SearchCustomers]') IS NOT NULL)
	DROP PROCEDURE [dbo].[SearchCustomers];
GO

/* Example
	DECLARE @endTimeStamp datetime      = GETDATE()
	DECLARE @begTimeStamp datetime      = @endTimeStamp
	DECLARE @durMessage   nvarchar(500) = NULL
	DECLARE @searchText   nvarchar(100) = NULL
	DECLARE @operatorId   int           = (SELECT TOP(1) [OPERATOR_ID] FROM [dbo].[OPERATOR] WHERE [LOGIN] = N'admin')

	SET @begTimeStamp = GETDATE()
	----------------------------------
	SET @searchText = 'Удаленные'
	EXEC SearchCustomers @searchText, @operatorId
	----------------------------------
	SET @endTimeStamp = GETDATE()
	SET @durMessage   = CONVERT(varchar, DATEDIFF(second, @begTimeStamp, @endTimeStamp))
	RAISERROR(N'''%s'' Duration: %s second', 0, 1, @searchText, @durMessage) WITH NOWAIT;
*/
-- =============================================
-- Description: получение списка клиентов по ключевому слову, для суперадминистратора
-- =============================================
CREATE PROCEDURE [dbo].[SearchCustomers]
(
	@searchText nvarchar(100) = NULL, -- Искомый текст
	@operatorId int           = NULL,  -- Оператор, который ищет текст
	@skipCount  int           = 0000, -- Сколько записей пропустить
	@takeCount  int           = 0025 -- Сколько записей вернуть
)
AS
BEGIN
/*
	Определить тип операторра по @operatorId:
	1) Супепадмин;
	2) Опреатор департамента;
	3) Физик;
	4) Не указан (NULL)
	Понять, что за оператор суперадмин или просто пользователь департамента, если оператор физик,
	то вообще возвращать пустоту
	Для:
		1) Возвращаем все;
		2) Возвращаем только первую часть состоящую из департаментов, доступных операторов;
		3) Ничего не возвращаем;
		4) Ничего не возвращаем;
	Группа суперадминов (Код в списке констант)
		SalesOperatorGroupID
*/
	SET NOCOUNT ON;
	SELECT
		 @searchText = COALESCE(@searchText, '')
		,@skipCount  = COALESCE(@skipCount, 00)
		,@takeCount  = COALESCE(@takeCount, 25)
	DECLARE
		@SuperAdminGroupId        int = CONVERT(int, (SELECT TOP (1) [value]    FROM [dbo].[CONSTANTS]   WHERE [NAME] = 'SalesOperatorGroupID')),
		@SecurityAdministrationId int =              (SELECT TOP (1) [RIGHT_ID] FROM [dbo].[RIGHT]       WHERE [NAME] = 'SecurityAdministration'),
		@DepartmentsAccessId      int =              (SELECT TOP (1) [RIGHT_ID] FROM [dbo].[RIGHT]       WHERE [NAME] = 'DepartmentsAccess'),
		@VehicleAccessId          int =              (SELECT TOP (1) [RIGHT_ID] FROM [dbo].[RIGHT]       WHERE [NAME] = 'VehicleAccess'),
		@PhonesContactTypeId      int =              (SELECT TOP (1) [Id]       FROM [dbo].[ContactType] WHERE [Name] = 'Phone')
	DECLARE
		@OperatorIsSuperAdmin     bit =
			CASE WHEN
				EXISTS -- Суперадмин
				(
					SELECT 1
					FROM [dbo].[OPERATORGROUP_OPERATOR]
					WHERE [OPERATORGROUP_ID] = @SuperAdminGroupId
					AND   [OPERATOR_ID]      = @operatorId
				)
				THEN 1
				ELSE 0
			END

	DECLARE
		@searchLike  nvarchar(500) = '%' + dbo.EscapeLikeTemplate(@searchText, '\') + '%',
		@searchDevId varbinary(32) = CONVERT(varbinary, CONVERT(varchar(32), @searchText)),
		@searchPhone nvarchar(100) = dbo.GetPhoneInvariant(@searchText)

	-- Поиск в департаментах
	DECLARE @depHits TABLE
		([ItmId] int NOT NULL, [ItmKind] int NOT NULL, [HitText] nvarchar(100), [HitType] nvarchar(100))
	;WITH SearchDeps ([ItmId], [HitText], [HitType])
	AS
	(
		---------------------------------------------------
		-- При пустом поиске выдать все департаменты
		---------------------------------------------------
		SELECT D.[DEPARTMENT_ID], NULL, 'DEPARTMENT'
		FROM  [dbo].[DEPARTMENT] D
		WHERE 0 = LEN(@searchText)
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в гаражном номере
		---------------------------------------------------
		SELECT
			V.[DEPARTMENT], V.[GARAGE_NUMBER], 'VEHICLE.GARAGE_NUMBER'
		FROM [dbo].[VEHICLE] V
		WHERE 0 < LEN(@searchText)
		AND   V.[GARAGE_NUMBER] LIKE @searchLike ESCAPE '\'
		AND   V.[DEPARTMENT] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в публичном номере
		---------------------------------------------------
		SELECT
			V.[DEPARTMENT], V.[PUBLIC_NUMBER], 'VEHICLE.PUBLIC_NUMBER'
		FROM [dbo].[VEHICLE] V
		WHERE 0 < LEN(@searchText)
		AND   V.[PUBLIC_NUMBER] LIKE @searchLike ESCAPE '\'
		AND   V.[DEPARTMENT] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в идентификаторе устройства
		---------------------------------------------------
		SELECT V.[DEPARTMENT], CONVERT(varchar(32), I.[DEVICE_ID]), 'VEHICLE.CONTROLLER.CONTROLLER_INFO.DEVICE_ID'
		FROM [dbo].[VEHICLE] V
			INNER JOIN [dbo].[CONTROLLER] C
				ON C.[VEHICLE_ID] = V.[VEHICLE_ID]
					INNER JOIN [dbo].[CONTROLLER_INFO] I
						on I.[CONTROLLER_ID] = C.[CONTROLLER_ID]
		WHERE 0 < LEN(@searchText)
		AND   CHARINDEX(@searchText, CONVERT(varbinary(32), I.[DEVICE_ID]), 1) > 0
		AND   V.[DEPARTMENT] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в телефоне трекера
		---------------------------------------------------
		SELECT V.[DEPARTMENT], P.[Value], 'VEHICLE.CONTROLLER.Contact.Value'
		FROM [dbo].[VEHICLE] V
			INNER JOIN [dbo].[CONTROLLER] C
				ON C.[VEHICLE_ID] = V.[VEHICLE_ID]
					INNER JOIN [dbo].[Contact] P
						ON P.[ID] = C.[PhoneContactID]
		WHERE 0 < LEN(@searchText)
		AND   P.[Value] = @searchPhone
		AND   P.[Type]  = @PhonesContactTypeId
		AND   V.[DEPARTMENT] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в имени пользователя
		---------------------------------------------------
		SELECT R.[department_id], O.[LOGIN], 'v_operator_department_right.OPERATOR.LOGIN'
		FROM [dbo].[v_operator_department_right] R WITH (NOLOCK)
			INNER JOIN [dbo].[OPERATOR] O
				on O.[OPERATOR_ID] = R.[operator_id]
		WHERE 0 < LEN(@searchText)
		AND   O.[LOGIN]    = @searchText
		AND   R.[right_id] = @DepartmentsAccessId
		AND   R.[department_id] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в имени департамента
		---------------------------------------------------
		SELECT D.[DEPARTMENT_ID], D.[NAME], 'DEPARTMENT.NAME'
		FROM  [dbo].[DEPARTMENT] D
		WHERE 0 < LEN(@searchText)
		AND   D.[NAME] LIKE @searchLike ESCAPE '\'
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в наименовании договора
		---------------------------------------------------
		SELECT D.[DEPARTMENT_ID], D.[ExtID], 'DEPARTMENT.ExtID'
		FROM  [dbo].[DEPARTMENT] D
		WHERE 0 < LEN(@searchText)
		AND   D.[ExtID]    LIKE @searchLike ESCAPE '\'
		AND   D.[NAME] NOT LIKE @searchLike ESCAPE '\'
	)
	INSERT INTO @depHits
	--OUTPUT INSERTED.*
	SELECT DISTINCT
		[ItmId], [ItmKind] = 0, [HitText], [HitType]
	FROM SearchDeps
	WHERE @operatorId IS NOT NULL
	AND
	(
		@OperatorIsSuperAdmin = 1
		OR
		[ItmId] IN
		(
			SELECT [department_id]
			FROM [dbo].[v_operator_department_right] WITH (NOLOCK)
			WHERE [operator_id]   = @operatorId
			AND   [right_id]     IN (@SecurityAdministrationId, @DepartmentsAccessId)
		)
	)
	-- Поиск в операторах
	DECLARE @oprHits TABLE
		([ItmId] int NOT NULL, [ItmKind] int NOT NULL, [HitText] nvarchar(100), [HitType] nvarchar(100))
	;WITH SearchOprs ([ItmId], [HitText], [HitType])
	AS
	(
		---------------------------------------------------
		-- При пустом поиске выдать всех операторов
		---------------------------------------------------
		SELECT O.[OPERATOR_ID], NULL, 'OPERATOR'
		FROM [dbo].[OPERATOR] O
		WHERE 0 = LEN(@searchText)
		AND   0 < LEN(COALESCE(O.[LOGIN], ''))
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск полного соответствия в логине
		---------------------------------------------------
		SELECT O.[OPERATOR_ID], O.[LOGIN], 'OPERATOR.LOGIN'
		FROM [dbo].[OPERATOR] O
		WHERE 0 < LEN(@searchText)
		AND   O.[LOGIN] = @searchText
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в имени при заполненном логине
		---------------------------------------------------
		SELECT O.[OPERATOR_ID], O.[NAME], 'OPERATOR.NAME'
		FROM [dbo].[OPERATOR] O
		WHERE 0 < LEN(@searchText)
		AND   0 < LEN(COALESCE(O.[LOGIN], ''))
		AND   O.[NAME] = @searchText
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск подстроки в логине в имени при заполненном логине
		---------------------------------------------------
		SELECT O.[OPERATOR_ID], O.[LOGIN], 'OPERATOR.LOGIN'
		FROM [dbo].[OPERATOR] O
		WHERE 0 < LEN(@searchText)
		AND   O.[LOGIN] LIKE @searchLike ESCAPE '\'
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск подстроки в логине в имени при заполненном логине
		---------------------------------------------------
		SELECT O.[OPERATOR_ID], O.[NAME], 'OPERATOR.NAME'
		FROM [dbo].[OPERATOR] O
		WHERE 0 < LEN(@searchText)
		AND   0 < LEN(COALESCE(O.[LOGIN], ''))
		AND   O.[NAME] LIKE @searchLike ESCAPE '\'
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в гаражном номере через права
		---------------------------------------------------
		SELECT
			R.[operator_id], V.[GARAGE_NUMBER], 'v_operator_vehicle_right.VEHICLE.GARAGE_NUMBER'
		FROM [dbo].[VEHICLE] V
			INNER JOIN [dbo].[v_operator_vehicle_right] R
				ON  R.[vehicle_id] = V.[VEHICLE_ID]
				AND R.[right_id]   = @SecurityAdministrationId
				AND R.[priority]   = 1
		WHERE 0 < LEN(@searchText)
		AND   V.[GARAGE_NUMBER] LIKE @searchLike ESCAPE '\'
		AND   V.[DEPARTMENT] IS NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск в публичном номере через права
		---------------------------------------------------
		SELECT
			R.[operator_id], V.[PUBLIC_NUMBER], 'v_operator_vehicle_right.VEHICLE.PUBLIC_NUMBER'
		FROM [dbo].[VEHICLE] V
			INNER JOIN [dbo].[v_operator_vehicle_right] R
				ON  R.[vehicle_id] = V.[VEHICLE_ID]
				AND R.[right_id]   = @SecurityAdministrationId
				AND R.[priority]   = 1
		WHERE 0 < LEN(@searchText)
		AND   V.[PUBLIC_NUMBER] LIKE @searchLike ESCAPE '\'
		AND   V.[DEPARTMENT] IS NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск идентификатора устройства (для MLP)
		---------------------------------------------------
		SELECT A.[Operator_ID], @searchText, 'Asid.MLP_Controller.CONTROLLER_INFO.DEVICE_ID'
		FROM [dbo].[Asid] A
			INNER JOIN [dbo].[MLP_Controller] M
				ON M.[Asid_ID] = A.[ID]
					INNER JOIN [dbo].[CONTROLLER_INFO] I
						ON I.[CONTROLLER_ID] = M.[Controller_ID]
		WHERE 0 < LEN(@searchText)
		AND   I.[DEVICE_ID] = @searchDevId
		AND   A.[Operator_ID] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск идентификатора устройства (для Трекера через права)
		---------------------------------------------------
		SELECT R.[Operator_ID], @searchText, 'v_operator_vehicle_right.CONTROLLER.CONTROLLER_INFO.DEVICE_ID'
		FROM [dbo].[CONTROLLER_INFO] I
			INNER JOIN [dbo].[CONTROLLER] C
				ON I.[CONTROLLER_ID] = C.[CONTROLLER_ID]
					INNER JOIN [dbo].[v_operator_vehicle_right] R
						ON  R.[vehicle_id] = C.[VEHICLE_ID]
						AND R.[right_id]   = @VehicleAccessId
		WHERE 0 < LEN(@searchText)
		AND   I.[DEVICE_ID] = @searchDevId
		AND   R.[Operator_ID] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск идентификатора устройства (для Трекера)
		---------------------------------------------------
		SELECT A.[Operator_ID], @searchText, 'Asid.DeviceId'
		FROM [dbo].[Asid] A
		WHERE 0 < LEN(@searchText)
		AND   A.[DeviceId] = @searchDevId
		AND   A.[Operator_ID] IS NOT NULL
		---------------------------------------------------
		UNION ALL
		---------------------------------------------------
		-- Поиск телефона контакта
		---------------------------------------------------
		SELECT O.[Operator_ID], C.[Value], 'Operator_Contact.Contact.Value'
		FROM
		(
			SELECT [ID], [Value]
			FROM [dbo].[Contact]
			WHERE 0 < LEN(@searchText)
			AND   [Value] LIKE @searchLike ESCAPE '\'
			AND   [Type] = @PhonesContactTypeId
		) C
			INNER JOIN [dbo].[Operator_Contact] O
				ON  O.[Contact_ID] = C.[ID]
				AND O.[Confirmed]  = 1
		WHERE 0 < LEN(@searchText)
		AND   O.[Operator_ID] IS NOT NULL
	)
	INSERT INTO @oprHits
	--OUTPUT INSERTED.*
	SELECT DISTINCT
		[ItmId], [ItmKind] = 1, [HitText], [HitType]
	FROM SearchOprs
	WHERE @operatorId IS NOT NULL
	AND
	(
		@OperatorIsSuperAdmin = 1
	)

	DECLARE @searches TABLE ([id] int NOT NULL, [name] nvarchar(255) NOT NULL, [desc] nvarchar(255), [type] int NOT NULL, [kind] int NOT NULL, [contract] nvarchar(255))
	INSERT INTO @searches
	SELECT
		[id]       = D.[DEPARTMENT_ID],
		[name]     = CASE WHEN 0 = LEN(COALESCE(D.[NAME], '')) THEN COALESCE(D.[ExtID], '') ELSE D.[NAME] END,
		[desc]     = D.[Description],
		[type]     = COALESCE(D.[Type], 0),/*Корпоративный или физический*/
		[kind]     = S.[ItmKind],
		[contract] = D.[ExtID]
	FROM
	(
		SELECT DISTINCT [ItmId], [ItmKind] FROM @depHits
	) S
		INNER JOIN [dbo].[DEPARTMENT] D
			ON D.[DEPARTMENT_ID] = S.[ItmId]
	WHERE 0 < LEN(COALESCE(D.[NAME], D.[ExtID], ''))
	UNION ALL
	SELECT
		[id]       = O.[OPERATOR_ID],
		[name]     = CASE WHEN 0 = LEN(COALESCE(O.[NAME], '')) THEN COALESCE(O.[LOGIN], '') ELSE O.[NAME] END,
		[desc]     = NULL,
		[type]     = 1 /*Всегда физический*/,
		[kind]     = S.[ItmKind],
		[contract] = O.[LOGIN]
	FROM
	(
		SELECT DISTINCT [ItmId], [ItmKind] FROM @oprHits
	) S
		INNER JOIN [dbo].[OPERATOR] O
			ON O.[OPERATOR_ID] = S.[ItmId]
	WHERE 0 < LEN(COALESCE(O.[LOGIN], ''))
	ORDER BY [name], [id] DESC
	OFFSET @skipCount ROWS
	FETCH NEXT @takeCount ROWS ONLY
	SELECT TOP(1)
	[TOTALPAGE] =
		@@ROWCOUNT,--(SELECT COUNT(*) FROM @searches),
	[TOTALFIND] =
		(SELECT COUNT(*) FROM (SELECT DISTINCT [ItmId], [ItmKind] FROM @depHits) S) +
		(SELECT COUNT(*) FROM (SELECT DISTINCT [ItmId], [ItmKind] FROM @oprHits) S)
	SELECT
		*
	FROM @searches
	SELECT DISTINCT
		D.*
	FROM @searches S
		INNER JOIN @depHits D
			ON  D.[ItmKind] = S.[kind]
			AND D.[ItmId]   = S.[id]
			AND D.[HitText] IS NOT NULL
	UNION
	SELECT DISTINCT
		O.*
	FROM @searches S
		INNER JOIN @oprHits O
			ON  O.[ItmKind] = S.[kind]
			AND O.[ItmId]   = S.[id]
			AND O.[HitText] IS NOT NULL
END