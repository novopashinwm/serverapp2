if exists (select * from sys.procedures where name = 'GetVehicleServices')
	drop procedure GetVehicleServices
go

set quoted_identifier on
go

/*
Sample:
declare @vid dbo.Id_Param
insert into @vid select vehicle_id from vehicle order by vehicle_id desc
exec GetVehicleServices @vid
*/
create procedure GetVehicleServices
(
	@vid dbo.Id_Param readonly
)
as
begin

set nocount on

declare @bs table (ID int not null primary key clustered, vehicle_id int)

insert into @bs
	select bs.ID, vid.Id
	from @vid vid
	cross apply (
		select bs.ID
			from Billing_Service bs
			where bs.Vehicle_ID = vid.Id
		union
		select bs.ID
			from Controller c
			join dbo.MLP_Controller mlpc on mlpc.Controller_ID = c.Controller_ID
			join dbo.Billing_Service bs on bs.Asid_ID = mlpc.Asid_ID
			where c.VEHICLE_ID = vid.Id
		union
		select bs.ID
			from Controller c
			join dbo.MLP_Controller mlpc on mlpc.Controller_ID = c.Controller_ID
			join Asid a on a.ID = mlpc.Asid_ID
			join dbo.Billing_Service bs on bs.Operator_ID = a.Operator_ID
			where c.VEHICLE_ID = vid.Id
	) bs

--OUTPUT: Billing_Service
select 
	  Vehicle_ID = bsid.Vehicle_ID
	, Billing_Service_ID = bs.ID
	, bst.ID
	, Name = isnull(bst.Name, bst.Service_Type)
	, bst.Service_Type_Category
	, bs.StartDate
	, EndDate = bs.EndDate
	, bst.SMS
	, bst.LBS
	, isTrial = convert(bit, case when bst.PeriodDays is not null then 1 else 0 end)
	, isActivated = bs.Activated
	, maxQuantity = case bst.LimitedQuantity when 1 then isnull(bs.MaxQuantity, bst.MaxQuantity) else null end
	, minIntervalSeconds = isnull(bs.MinIntervalSeconds, bst.MinIntervalSeconds)
	, isShared = bst.Shared
	, isBlocked = convert(bit, 
		case when exists (select 1 from Billing_Blocking bb where bb.Asid_ID = bs.Asid_ID) then 1 else 0 end)
	, bst.MaxLogDepthHours
	, bst.TrackingSchedule
	, bst.ServiceDays
	from @bs bsid
	join Billing_Service bs on bs.ID = bsid.ID
	join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID	
			
--OUTPUT: Quantity_Left
select 
		bsid.Vehicle_ID
	, Billing_Service_ID = bs.ID
	, bst.Service_Type_Category
	, Rendered_Service_Type_Name = rst.Name
	--TODO: ������� ��� ������� �� �������� � ServiceDays
	, Quantity_Left = 
		case rst.Name 
			when 'ServiceDays' 
				then bst.ServiceDays - isnull(rs.Quantity, 0)
			else 
				case bst.LimitedQuantity 
					when 1 then isnull(bs.MaxQuantity, bst.MaxQuantity) - isnull(rs.Quantity, 0) 
					else null 
				end
		end
	, rs.LastTime
	from @bs bsid
	join Billing_Service bs on bs.ID = bsid.ID
	join dbo.Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
	join Rendered_Service_Type rst on rst.Name = 'LBS' and bst.LBS = 1 
			                        or rst.Name = 'SMS' and bst.SMS = 1
									or rst.Name = 'ServiceDays' and bst.ServiceDays > 0
	left outer join Rendered_Service rs on rs.Billing_Service_ID = bs.ID 
			                            and rs.Rendered_Service_Type_ID = rst.ID

end