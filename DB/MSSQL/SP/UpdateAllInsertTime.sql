if exists (select * from sys.objects where name = 'UpdateAllInsertTime')
	drop procedure UpdateAllInsertTime;
go

create procedure UpdateAllInsertTime
as
	update Log_Time
		set InsertTime = GETUTCDATE()
		where Vehicle_ID in (select Vehicle_ID from VEHICLE) 
		  and InsertTime is null