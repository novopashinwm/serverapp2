if exists (
	select *
		from sys.procedures 
		where name = 'GetUpdatedMessages'
)
	drop procedure GetUpdatedMessages
go


/* Usage sample:
declare @from datetime = '2015.12.01'
declare @to   datetime = dateadd(second, 10*60, @from)

exec GetUpdatedMessages @from, @to
*/
create procedure GetUpdatedMessages
(
	@from datetime,
	@to   datetime
)
as

set nocount on

select Message_ID
	, m.Created
	, m.Time
	, mcTo.*
	, mcFrom.*
	, m.SUBJECT
	, m.BODY
	, m.STATUS
	, m.ProcessingResult
	from message m with (nolock, INDEX(IX_Message_DestinationType_ID_Time)) 
	outer apply (
		select top(1) 
				  ToID = mc.Contact_ID
				, ToType = isnull(dc.Type, c.Type)
				, ToValue = isnull(dc.Value, c.Value)
			from Message_Contact mc 
			join Contact c on c.ID = mc.Contact_ID
			left outer join Contact dc on dc.ID = c.Demasked_ID
			where mc.Message_ID = m.Message_ID
			  and mc.Type = 1
			order by mc.Contact_ID asc
	) mcTo
	outer apply (
		select top(1) 
				  FromID = mc.Contact_ID
				, FromType = isnull(dc.Type, c.Type)
				, FromValue = isnull(dc.Value, c.Value)
			from Message_Contact mc 
			join Contact c on c.ID = mc.Contact_ID
			left outer join Contact dc on dc.ID = c.Demasked_ID
			where mc.Message_ID = m.Message_ID
			  and mc.Type in (2,5)
			order by mc.Contact_ID asc
	) mcFrom
	where DestinationType_ID in (0, 1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
	  and @from < m.TIME and m.Time <= @to

