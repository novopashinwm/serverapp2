set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParseLogTimeSequenceAndSkip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParseLogTimeSequenceAndSkip]
GO


create procedure [dbo].[ParseLogTimeSequenceAndSkip]  
 @logTimeList varchar(max)  
as  
begin  
 set nocount on;  
  
 declare @logTime table(rn int identity primary key clustered, value int);  
  
 insert into @logTime (value)  
  select distinct number   
   from dbo.split_int(@logTimeList)  
   order by number  
  
 select lt.value [From], lt_next.value [To]  
  from @logTime lt  
  join @logTime lt_next on lt_next.rn = lt.rn + 1  
  where lt.rn % 2 = 1
  order by lt.rn  
   
end;