/****** Object:  StoredProcedure [dbo].[MovementIntervalsThroughPpointGet]    Script Date: 07/14/2010 15:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
-- ж■√║вї¤°ї ЇЁ¤¤v┐ ■ Єиї№ї¤Ё┐  ■ иЁ╕ °╕Ё¤°ж ° пЁ·к°вї╕·°┐  и■┐■ЎЇї¤°а ўЁЇЁ¤¤¤■∙ к■в·° TT ўЁЇЁ¤¤■є■ №Ёи░и║кЁ ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║ 
-- LЄк■и: +иїёї¤¤°·■Є
--------------------------------------------------------------------------------------------

ALTER  PROCEDURE [dbo].[MovementIntervalsThroughPpointGet]
	@Date		datetime,	-- -ЁкЁ ¤Ё ·■к■и║ж киїё║їк╕а  ■╕ки■°к╣ ■квїк
	@PointID	int,		-- ID к■в·° (■╕кЁ¤■Є·°)
	@RouteID	int,		-- ID №Ёи░и║кЁ
	@Direct 	int,		-- ¤Ё иЁЄ√ї¤°ї °√° к°  иї∙╕Ё (1-L 4-+ 0-T╕ї)
	@operator_id int = null
AS

--------------------------------------------------------------------------------------------
/*
--Tї╕к
declare
	@Date		datetime,	
	@PointID	int,		
	@RouteID	int,		
	@Direct 	int

set	@Date = --'2006-12-20 21:00:00'
	--'2007-03-11 21:00:00'
	'2007-06-06 20:00:00'
set	@PointID = --1 
	--11
	--6 --14№/¤
	--90 --║√.жЁЄ■Ї╕·Ёа
	13
set	@RouteID = --8
	--11
	--22 --ж14
	--25 --ж19
	15
set @Direct = 0 
	--+4 --к°  +
	--+1 --к°  L

--select * from ROUTE where EXT_NUMBER = '8'
--select * from BUSSTOP where [NAME] = '41 ·№'
*/
--------------------------------------------------------------------------------------------

-- -Ёкv Ї√а жT ° Є■Ї°кї√а ° иЁў¤°бЁ Є■ Єиї№ї¤°
declare 
	@wbhDateTime 	datetime, 
	@wbhDate 	datetime, 
	@wbDate 	datetime, 
	@TimeDiffUTC 	int 

set @wbhDateTime = @Date
set @wbhDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime, 1), 1)
set @wbDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime + 1, 1), 1)
set @TimeDiffUTC = datediff(s, @wbhDateTime, @wbDate)   


--жЁ √Ё¤°и■ЄЁ¤¤■ї ( ■ иЁ╕ °╕Ё¤°ж) Єиї№а  и■┐■ЎЇї¤°а вїиїў ║·ЁўЁ¤¤║ж к■в·║ Єv┐■Ї■Є ║·ЁўЁ¤¤■є■ №Ёи░и║кЁ.
select	distinct	
	SP.SCHEDULE_POINT_ID							SP_ID,
	dateadd(s, SP.TIME_IN - @TimeDiffUTC, @wbDate)				PlannedTimeIn,
	dateadd(s, SP.TIME_OUT - @TimeDiffUTC, @wbDate)				PlannedTimeOut,
	dateadd(s, isnull(SP.TIME_IN, SP.TIME_OUT) - @TimeDiffUTC, @wbDate) 	PlannedTime,
	SC.EXT_NUMBER								NumberOfExit,
	TR.TRIP_ID,
	GT.GEO_TRIP_ID
	,GES.[GEO_SEGMENT_ID]
	,GES.[ORDER]
	,GES.STATUS
	,PT.POINT_ID
	,PT.[NAME]
into 
	#sp
from 
	dbo.SCHEDULE_POINT as SP
	--№Ёи░и║к ° иЁ╕ °╕Ё¤°ї
	inner join dbo.TRIP as TR on SP.[TRIP_ID] = TR.[TRIP_ID]
	inner join dbo.SCHEDULE_DETAIL as SCD on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]
	inner join dbo.SCHEDULE as SC on SCD.[SCHEDULE_ID] = SC.[SCHEDULE_ID]
	inner join dbo.ROUTE as RT on SC.[ROUTE_ID] = RT.[ROUTE_ID] 
	--■╕кЁ¤■Є·Ё
	inner join dbo.GEO_SEGMENT as GES on SP.[GEO_SEGMENT_ID] = GES.[GEO_SEGMENT_ID]
 	inner join dbo.GEO_TRIP as GT on GES.[GEO_TRIP_ID] = GT.[GEO_TRIP_ID]
	inner join dbo.POINT as PT on GES.[POINT] = PT.[POINT_ID]
	inner join dbo.BUSSTOP as BS on PT.[BUSSTOP_ID] = BS.[BUSSTOP_ID]
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where	
	RT.[ROUTE_ID] = @RouteID						
	and SC.[DAY_KIND_ID] = dbo.DayKind(@wbDate, @RouteID) 	
	and TR.[TRIP_KIND_ID] in (5, 6)		
	and BS.[BUSSTOP_ID] = @PointID	
	--■к√ЁЇ·Ё	
	--and SC.EXT_NUMBER = '803'
	and (@operator_id is null or rr.operator_id = @operator_id)
order by	
	PlannedTime

--select * from #sp


--LЁ·к°вї╕·■ї Єиї№а  и■┐■ЎЇї¤°а вїиїў ║·ЁўЁ¤¤║ж к■в·║ Єv┐■Ї■Є ║·ЁўЁ¤¤■є■ №Ёи░и║кЁ.
select distinct
	SCPS.SP_ID,
	SCPS.TIME_IN				PlannedTimeIn,
	SCPS.TIME_OUT				PlannedTimeOut,
	isnull(SCPS.TIME_IN, SCPS.TIME_OUT)	PlannedTime,
	SCPS.LOG_TIME_IN			FactualTimeIn,
	SCPS.LOG_TIME_OUT			FactualTimeOut
into 
	#scps
from
	dbo.SCHEDULE_PASSAGE as SCPS
	-- жT
	inner join dbo.WAYBILL_HEADER as WBH on SCPS.WH_ID = WBH.WAYBILL_HEADER_ID
	-- к■в·° пЁ·к°вї╕·°ї
	inner join #sp as sp on SCPS.SP_ID = sp.SP_ID
where
	WBH.WAYBILL_DATE = @wbhDateTime 
order by	
	PlannedTime

--select * from #scps


--к■в·° єї■иї∙╕■Є Ї√а ■ иїЇї√ї¤°а ¤Ё иЁЄ√ї¤°а
select distinct
	GS.GEO_SEGMENT_ID,
	GS.[ORDER],
	GS.STATUS,
	GT.GEO_TRIP_ID
	,GT.POINTA 
	,GT.POINTB
	,GT.COMMENTS 
into
	#geotrip
from 
	GEO_SEGMENT as GS
	inner join GEO_TRIP as GT on GS.GEO_TRIP_ID = GT.GEO_TRIP_ID
	inner join #sp as sp on GS.GEO_TRIP_ID = sp.GEO_TRIP_ID
order by 
	GT.GEO_TRIP_ID, GS.[ORDER]

if(@Direct > 0) delete from #geotrip where (status & @Direct) = 0 or status is null

--select * from #geotrip


--жЁ·иvкvї иї∙╕v
select distinct
	WBT.TRIP_ID,
	WBT.WB_TRIP_ID
into 
	#wbt
from
	dbo.WB_TRIP as WBT
	-- жT
	inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
	-- к■в·° 
	inner join #sp as sp on WBT.TRIP_ID = sp.TRIP_ID
where
	WBH.WAYBILL_DATE = @wbhDateTime 
order by 
	WBT.TRIP_ID, WBT.WB_TRIP_ID


-- жїў║√╣кЁк
select distinct
	sp.PlannedTimeIn,
	sp.PlannedTimeOut,
	sp.PlannedTime,
	--scps.PlannedTimeIn,
	--scps.PlannedTimeOut,
	--scps.PlannedTime,
	scps.FactualTimeIn, 
	scps.FactualTimeOut,
	sp.NumberOfExit,
	wbt.WB_TRIP_ID	
	,sp.GEO_TRIP_ID
	,sp.GEO_SEGMENT_ID  
from 
	#sp as sp
	--¤Ё иЁЄ√ї¤°а
	--ї╕√° ¤Ё иЁЄ√ї¤°ї Ї√а иї∙╕Ё Є бї√■№, к■ ї╕к╣  ■ ЁЇЁжк Є╕ї к■в·° иї∙╕Ё к° Ё L °√° к° Ё +
	inner join #geotrip as gt on sp.GEO_TRIP_ID = gt.GEO_TRIP_ID
	--√°ё■ ¤Ё иЁЄ√ї¤°ї Ї√а к■в·°, к■ ї╕к╣  ■ ЁЇЁжк к■√╣·■ к■в·° к° Ё L °√° к° Ё +
	--inner join #geotrip as gt on sp.GEO_SEGMENT_ID = gt.GEO_SEGMENT_ID
	--пЁ·к°вї╕·■ї  и■┐■ЎЇї¤°ї
	left join #scps as scps on scps.SP_ID = sp.SP_ID
	--ўЁ·иvкvї иї∙╕v
	left join #wbt as wbt on wbt.TRIP_ID = sp.TRIP_ID
order by
	sp.PlannedTime	


--select * from #sp
--select * from #scps
--select * from #geotrip
--select * from #wbt


drop table #sp
drop table #scps
drop table #geotrip
drop table #wbt



