IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRunLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRunLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetRunLog]  
(  
 @vehicles				dbo.Id_Param readonly,  
 @intervals				dbo.Log_Time_Span_Param readonly,
 @include_adjacent		bit = null
)  
as  

select
	  sl.vehicle_id
	, sl.log_time
	, sl.odometer
	from @vehicles v
	join @intervals i on 1=1
	inner join statistic_log sl with (nolock) 
		on sl.vehicle_id = v.id
	   and sl.Log_Time between i.Log_Time_From and i.Log_Time_To

if (@include_adjacent = 1)
begin
	select
		    vehicle_id = v.id
		  , prev_log.log_time
		  , prev_log.odometer
		from @vehicles v
		join @intervals i on 1=1
		cross apply (
			select top(1) sl.Log_Time, sl.Odometer
				from statistic_log sl with (nolock)
				where sl.Vehicle_ID = v.Id
				  and sl.Log_Time < i.Log_Time_From
				order by sl.Log_Time desc) prev_log
				
	union all

	select
			vehicle_id = v.id
		  , next_log.log_time
		  , next_log.odometer
		from @vehicles v
		join @intervals i on 1=1
		cross apply (
			select top(1) sl.Log_Time, sl.Odometer
				from statistic_log sl with (nolock)
				where sl.Vehicle_ID = v.Id
				  and sl.Log_Time > i.Log_Time_To
				order by sl.Log_Time asc) next_log
end