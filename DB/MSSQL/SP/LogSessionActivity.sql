﻿IF (OBJECT_ID(N'[dbo].[LogSessionActivity]') IS NOT NULL)
	DROP PROCEDURE [dbo].[LogSessionActivity];
GO
--EXEC [dbo].[LogSessionActivity] 2165287
CREATE PROCEDURE [dbo].[LogSessionActivity]
(
	@SESSION_ID int
)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[SESSION]
		SET [LastActivity] = GETUTCDATE()
	WHERE [SESSION_ID] = @SESSION_ID;
END
GO