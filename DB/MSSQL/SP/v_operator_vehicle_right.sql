﻿if exists (select * from sys.views where name = 'v_operator_vehicle_right_operator_vehicle')
	drop view v_operator_vehicle_right_operator_vehicle
go

create view v_operator_vehicle_right_operator_vehicle
with schemabinding
as
  select operator_id, vehicle_id, right_id
   from dbo.operator_vehicle 
   where allowed = 1
go

create unique clustered index PK_v_operator_vehicle_right_operator_vehicle
	on v_operator_vehicle_right_operator_vehicle(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_vehicle_vid_rid_oid
	on v_operator_vehicle_right_operator_vehicle(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operator_vehiclegroup')
	drop view v_operator_vehicle_right_operator_vehiclegroup
go

create view v_operator_vehicle_right_operator_vehiclegroup
with schemabinding
as
  select o_vg.operator_id, vg_v.vehicle_id, o_vg.right_id, [count] = count_big(*)
   from dbo.operator_vehiclegroup o_vg  
   join dbo.VEHICLEGROUP_VEHICLE vg_v on vg_v.vehiclegroup_id = o_vg.vehiclegroup_id  
   join dbo.vehiclegroup vg on vg.VEHICLEGROUP_ID = vg_v.VEHICLEGROUP_ID and vg.PropagateAccess = 1
   where o_vg.allowed = 1
   group by o_vg.operator_id, vg_v.vehicle_id, o_vg.right_id
go

create unique clustered index PK_v_operator_vehicle_right_operator_vehiclegroup
	on v_operator_vehicle_right_operator_vehiclegroup(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_vehiclegroup_vid_rid_oid
	on v_operator_vehicle_right_operator_vehiclegroup(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operatorgroup_vehicle')
	drop view v_operator_vehicle_right_operatorgroup_vehicle
go

create view v_operator_vehicle_right_operatorgroup_vehicle
with schemabinding
as
  select og_o.operator_id, og_v.vehicle_id, og_v.right_id, [count] = count_big(*)
   from dbo.operatorgroup_vehicle og_v  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id  
   where allowed = 1
   group by og_o.operator_id, og_v.vehicle_id, og_v.right_id
go

create unique clustered index PK_v_operator_vehicle_right_operatorgroup_vehicle
	on v_operator_vehicle_right_operatorgroup_vehicle(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operatorgroup_vehicle_vid_rid_oid
	on v_operator_vehicle_right_operatorgroup_vehicle(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operatorgroup_vehiclegroup')
	drop view v_operator_vehicle_right_operatorgroup_vehiclegroup
go

create view v_operator_vehicle_right_operatorgroup_vehiclegroup
with schemabinding
as
  select og_o.operator_id, vg_v.vehicle_id, og_vg.right_id, [count] = count_big(*)
   from dbo.OPERATORGROUP_VEHICLEGROUP og_vg  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id  
   join dbo.VEHICLEGROUP_VEHICLE vg_v on vg_v.vehiclegroup_id = og_vg.vehiclegroup_id  
   join dbo.vehiclegroup vg on vg.VEHICLEGROUP_ID = og_vg.VEHICLEGROUP_ID and vg.PropagateAccess = 1
   where allowed = 1     
   group by og_o.operator_id, vg_v.vehicle_id, og_vg.right_id
go

create unique clustered index PK_v_operator_vehicle_right_operatorgroup_vehiclegroup
	on v_operator_vehicle_right_operatorgroup_vehiclegroup(operator_id, vehicle_id, right_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operator_department')
	drop view v_operator_vehicle_right_operator_department
go

create view v_operator_vehicle_right_operator_department
with schemabinding
as
  select o_d.operator_id, v.vehicle_id, o_d.right_id
   from dbo.OPERATOR_DEPARTMENT o_d  
   join dbo.VEHICLE v on v.department = o_d.department_id  
   where allowed = 1
go

create unique clustered index PK_v_operator_vehicle_right_operator_department
	on v_operator_vehicle_right_operator_department(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_department_vid_rid_oid
	on v_operator_vehicle_right_operator_department(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operatorgroup_department')
	drop view v_operator_vehicle_right_operatorgroup_department
go

create view v_operator_vehicle_right_operatorgroup_department
with schemabinding
as
  select og_o.operator_id, v.vehicle_id, og_d.right_id, [count] = count_big(*)
   from dbo.OPERATORGROUP_DEPARTMENT og_d  
   join dbo.VEHICLE v on v.department = og_d.department_id  
   join dbo.OperatorGroup_Operator og_o on og_o.OperatorGroup_ID = og_d.OperatorGroup_ID  
   where allowed = 1
   group by og_o.operator_id, v.vehicle_id, og_d.right_id
go

create unique clustered index PK_v_operator_vehicle_right_operatorgroup_department
	on v_operator_vehicle_right_operatorgroup_department(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operatorgroup_department_vid_rid_oid
	on v_operator_vehicle_right_operatorgroup_department(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operator_department_admin')
	drop view v_operator_vehicle_right_operator_department_admin
go

CREATE VIEW [dbo].[v_operator_vehicle_right_operator_department_admin]
WITH SCHEMABINDING
AS
	SELECT
		o_d.[OPERATOR_ID],
		o_v.[VEHICLE_ID],
		o_r.[RIGHT_ID]
	FROM [dbo].[OPERATOR_DEPARTMENT] o_d
		JOIN [dbo].[VEHICLE] o_v
			ON o_v.[DEPARTMENT] = o_d.[DEPARTMENT_ID]
		JOIN [dbo].[RIGHT] o_r
			ON o_r.[RIGHT_ID] IN (6, 7, 20, 21, 23, 24, 31, 32, 102, 109, 111, 112, 114, 115, 116, 117, 201, 202, 203, 204, 205)
	WHERE o_d.[RIGHT_ID] = 2 /*SecurityAdministration*/
	AND   o_d.[ALLOWED]  = 1
GO

create unique clustered index PK_v_operator_vehicle_right_operator_department_admin
	on v_operator_vehicle_right_operator_department_admin(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_department_admin_vid_rid_oid
	on v_operator_vehicle_right_operator_department_admin(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name = 'v_operator_vehicle_right_operatorgroup_department_admin')
	drop view v_operator_vehicle_right_operatorgroup_department_admin
go

CREATE VIEW [dbo].[v_operator_vehicle_right_operatorgroup_department_admin]
WITH SCHEMABINDING
AS
	SELECT
		og_o.[OPERATOR_ID],
		og_v.[VEHICLE_ID],
		og_r.[RIGHT_ID],
		[count] = COUNT_BIG(*)
	FROM [dbo].[OPERATORGROUP_OPERATOR] og_o
		JOIN [dbo].[OPERATORGROUP_DEPARTMENT] og_d
			ON og_d.[OPERATORGROUP_ID] = og_o.[OPERATORGROUP_ID]
				JOIN [dbo].[VEHICLE] og_v
					ON og_v.[DEPARTMENT] = og_d.[DEPARTMENT_ID]
		JOIN [dbo].[RIGHT] og_r
			ON og_r.[RIGHT_ID] IN (6, 7, 20, 21, 23, 24, 31, 32, 102, 109, 111, 112, 114, 115, 116, 117, 201, 202, 203, 204, 205)
	WHERE og_d.[RIGHT_ID] = 2 /*SecurityAdministration*/
	AND   og_d.[ALLOWED]  = 1
	GROUP BY og_o.[OPERATOR_ID], og_v.[VEHICLE_ID], og_r.[RIGHT_ID]
GO

create unique clustered index PK_v_operator_vehicle_right_operatorgroup_department_admin
	on v_operator_vehicle_right_operatorgroup_department_admin(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operatorgroup_department_admin_vid_rid_oid
	on v_operator_vehicle_right_operatorgroup_department_admin(vehicle_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_vehicle_right_operator_vehicle_admin')
	drop view v_operator_vehicle_right_operator_vehicle_admin
go

CREATE VIEW [dbo].[v_operator_vehicle_right_operator_vehicle_admin]
WITH SCHEMABINDING
AS
	SELECT
		o_v.[OPERATOR_ID],
		o_v.[VEHICLE_ID],
		o_r.[RIGHT_ID]
	FROM [dbo].[OPERATOR_VEHICLE] o_v
		JOIN dbo.[RIGHT] o_r
			ON o_r.[RIGHT_ID] IN (6, 7, 20, 21, 23, 24, 31, 32, 102, 109, 111, 112, 114, 115, 116, 117, 201, 202, 203, 204, 205)
	WHERE o_v.[RIGHT_ID] = 2 /*SecurityAdministration*/
	AND   o_v.[ALLOWED]  = 1
GO

create unique clustered index PK_v_operator_vehicle_right_operator_vehicle_admin
	on v_operator_vehicle_right_operator_vehicle_admin(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_vehicle_admin_vid_rid_oid
	on v_operator_vehicle_right_operator_vehicle_admin(vehicle_id, right_id, operator_id)
go

if exists (select * from sys.views where name='v_operator_vehicle_right_operator_asid')
	drop view v_operator_vehicle_right_operator_asid
go

CREATE VIEW [dbo].[v_operator_vehicle_right_operator_asid]
WITH SCHEMABINDING
AS
	SELECT
		a.[Operator_ID],
		c.[VEHICLE_ID],
		r.[RIGHT_ID]
	FROM [dbo].[Asid] a
		JOIN [dbo].[MLP_Controller] mc
			ON a.[ID] = mc.[Asid_ID]
		JOIN [dbo].[CONTROLLER] c
			ON mc.[Controller_ID] = c.[CONTROLLER_ID]
		JOIN [dbo].[RIGHT] r
			ON r.[RIGHT_ID] IN (6, 7, 20, 21, 23, 24, 31, 32, 102, 109, 111, 112, 114, 115, 116, 117, 201, 202, 203, 204, 205)
	WHERE a.[Operator_ID] IS NOT NULL
GO

create unique clustered index PK_v_operator_vehicle_right_operator_asid
	on v_operator_vehicle_right_operator_asid(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_vehicle_right_operator_asid_vid_rid_oid
	on v_operator_vehicle_right_operator_asid(vehicle_id, right_id, operator_id)
go

--- v_operator_country_department_vehicle

if exists (select * from sys.views where name='v_operator_country_department_vehicle')
	drop view v_operator_country_department_vehicle
go

CREATE VIEW [dbo].[v_operator_country_department_vehicle]
WITH SCHEMABINDING
AS
	SELECT
		og_o.[OPERATOR_ID],
		v.[VEHICLE_ID],
		r.[RIGHT_ID],
		COUNT_BIG(*) c
	FROM [dbo].[DEPARTMENT] d
		JOIN [dbo].[OperatorGroup_Country] ogc
			ON ogc.[Country_ID] = d.[Country_ID]
		JOIN [dbo].[operatorgroup_operator] og_o
			ON og_o.[OPERATORGROUP_ID] = ogc.[OperatorGroup_ID]
		JOIN [dbo].[RIGHT] r
			ON r.[RIGHT_ID]  IN (6, 7, 20, 21, 23, 24, 31, 32, 102, 109, 111, 112, 114, 115, 116, 117, 201, 202, 203, 204, 205)
		JOIN [dbo].[VEHICLE] v
			ON v.[DEPARTMENT] = d.[DEPARTMENT_ID]
	GROUP BY og_o.[OPERATOR_ID], v.[VEHICLE_ID], r.[RIGHT_ID]
GO

create unique clustered index PK_v_operator_country_department_vehicle
	on v_operator_country_department_vehicle(operator_id, vehicle_id, right_id)
go

create nonclustered index IX_v_operator_country_department_vehicle_vid_rid_oid
	on v_operator_country_department_vehicle(vehicle_id, right_id, operator_id)
go

--- v_operator_vehicle_right

if exists (Select * from sys.views where name = 'v_operator_vehicle_right')
	drop view v_operator_vehicle_right
go

create view v_operator_vehicle_right
as
select operator_id, vehicle_id, right_id, priority = min(priority)
	from (
		select operator_id, vehicle_id, right_id, priority = 1 from v_operator_vehicle_right_operator_vehicle with (noexpand)
		--Удалено пагубное влияние выдачи прав на группы объектов наблюдения
		--union
		--select operator_id, vehicle_id, right_id, priority = 2 from v_operator_vehicle_right_operator_vehiclegroup with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 3 from v_operator_vehicle_right_operator_vehicle_admin lvl3 with (noexpand)
		WHERE 1=1
		-- Отсекаем корпорантов им этот уровень не нужен
		AND   NOT EXISTS(SELECT * FROM v_operator_department_right WHERE operator_id = lvl3.OPERATOR_ID)
		union
		select operator_id, vehicle_id, right_id, priority = 4 from v_operator_vehicle_right_operator_department_admin with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 5 from v_operator_vehicle_right_operatorgroup_vehicle with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 6 from v_operator_vehicle_right_operatorgroup_vehiclegroup with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 7 from v_operator_vehicle_right_operator_department with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 8 from v_operator_vehicle_right_operatorgroup_department with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 9 from v_operator_vehicle_right_operatorgroup_department_admin with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority =10 from v_operator_country_department_vehicle with (noexpand)
		union
		select operator_id, vehicle_id, right_id, priority = 1 from v_operator_vehicle_right_operator_asid with (noexpand)
	) t
	group by operator_id, vehicle_id, right_id
