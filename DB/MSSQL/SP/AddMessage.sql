  
ALTER PROCEDURE dbo.AddMessage  
 @code nvarchar(255) = null,    -- template code  
 @ma_id	int = null,
 @sourceTypeID int = null,
 @sourceValue  nvarchar(255) = null,
 @destinationTypeID int = null,
 @destinationValue nvarchar(255) = null,
 @group int = null out,   -- add to specified message group  
 @time datetime = null,  -- time  
 @text nvarchar(4000) = null, -- message text  
 @status int = 0,    -- status  
 @severity int = 4,    -- severity  
 @commandId int = null
AS  
set nocount on

 -- correct time  
 if @time is null set @time = getutcdate()  

 -- correct group  
 if @group is not null set @group = (select top 1 [GROUP] from dbo.MESSAGE where [GROUP] = @group)  
 
 -- add message  

 declare @sourceID table (value int)
 insert into @sourceID
	exec dbo.GetContactID @sourceTypeID, @sourceValue
	
 declare @destinationID table (value int)
 insert into @destinationID
	exec dbo.GetContactID @destinationTypeID, @destinationValue

 declare @message_template_id int = (select top(1) Message_Template_ID from MESSAGE_TEMPLATE where NAME = @code);
 declare @message_template_field_id int = (select top 1 MESSAGE_TEMPLATE_FIELD_ID from MESSAGE_TEMPLATE_FIELD where NAME = 'CommandId' and MESSAGE_TEMPLATE_ID is null)
begin tran

	insert dbo.MESSAGE(ma_id, TEMPLATE_ID,  TIME, SEVERITY_ID, [GROUP],  STATUS, BODY)   
			select @ma_id,       
					@message_template_id,
					@time,   
					@severity,  
					@group, 
					@status, 
					@text

	declare @id int  
	set @id = @@identity  

	insert into Message_Contact 
	select @id, 1, c.value
		from @destinationID c

	insert into Message_Contact 
	select @id, 2, c.value
		from @sourceID c

	if @message_template_field_id is not null and @commandId is not null
		insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
		values (@id, @message_template_field_id, @commandId)

commit

if @group is null  
begin  
	set @group = @id  
	update dbo.MESSAGE set [GROUP] = @id where MESSAGE_ID = @id  
end  

select @id ID, @group [GROUP]
