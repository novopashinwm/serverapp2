﻿IF (OBJECT_ID(N'[dbo].[GetLngCellTolerance]', N'FN') IS NOT NULL)
	DROP FUNCTION [dbo].[GetLngCellTolerance];
GO

/*
Sample:
SELECT [dbo].[GetLngCell]         (73.89993)
SELECT [dbo].[GetLngCellTolerance](73.89993, 15)
*/

CREATE FUNCTION [dbo].[GetLngCellTolerance]
(
	@lng float, @cellSizeInMeters float
) RETURNS int WITH SCHEMABINDING
AS
BEGIN
	DECLARE @stp float = 360/(40000000/@cellSizeInMeters)
	SET @lng =
		CASE
			WHEN @lng <  000 THEN @lng+360*(ROUND(-@lng/360,0)+1)
			WHEN @lng >= 360 THEN @lng-360
			ELSE @lng
		END
	RETURN ROUND(@lng/@stp, 0);
END