SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.views where name = 'v_vehicle_rules')
	drop view v_vehicle_rules
go 
if (exists (select * from sys.objects where name = 'v_vehicle_rule' and type = 'v'))
	drop view v_vehicle_rule;
go

if exists (select * from sys.views where name = 'v_vehicle_rule_vehicle')
	drop view v_vehicle_rule_vehicle
go 
create view dbo.v_vehicle_rule_vehicle with schemabinding 
as
	select vr.Vehicle_ID, r.Rule_ID, r.Number RuleNumber, vr.Vehicle_Rule_ID, null VehicleGroup_Rule_ID, null Department_Rule_Id, vr.[value], 1 [Priority]
		from dbo.VEHICLE_RULE vr  
	join dbo.[Rule] r on r.Rule_ID = vr.Rule_ID  
go
create unique clustered index PK_v_vehicle_rule_vehicle on dbo.v_vehicle_rule_vehicle(Vehicle_ID, Vehicle_Rule_ID)
go
create nonclustered index IX_v_vehicle_rule_vehicle on dbo.v_vehicle_rule_vehicle(Vehicle_id, Rule_ID)
go

if exists (select * from sys.views where name = 'v_vehicle_rule_vehiclegroup')
	drop view v_vehicle_rule_vehiclegroup
go 
create view dbo.v_vehicle_rule_vehiclegroup with schemabinding 
as
	select vg_v.Vehicle_id, r.Rule_ID, r.Number RuleNumber, null Vehicle_Rule_ID, vg_r.VehicleGroup_Rule_ID, null Department_Rule_Id, vg_r.[value], 2 [Priority]
		from dbo.VEHICLEGROUP_RULE vg_r  
		join dbo.[Rule] r on r.Rule_ID = vg_r.Rule_ID  
		join dbo.VEHICLEGROUP_Vehicle vg_v on vg_v.vehiclegroup_id = vg_r.vehiclegroup_id
go
create unique clustered index PK_v_vehicle_rule_vehiclegroup on dbo.v_vehicle_rule_vehiclegroup(Vehicle_id, VehicleGroup_Rule_ID)
go
create nonclustered index IX_v_vehicle_rule_vehiclegroup on dbo.v_vehicle_rule_vehiclegroup(Vehicle_id, Rule_ID)
go

if exists (select * from sys.views where name = 'v_vehicle_rule_department')
	drop view v_vehicle_rule_department
go 
create view dbo.v_vehicle_rule_department with schemabinding 
as
	select v.VEHICLE_ID, r.RULE_ID, r.NUMBER RuleNumber, null Vehicle_Rule_ID, null VehicleGroup_Rule_ID, dr.Department_Rule_ID, dr.Value, 3 [Priority]
	from dbo.Department_Rule dr
		join dbo.[RULE] r on r.RULE_ID = dr.Rule_ID
		join dbo.VEHICLE v on dr.Department_ID = v.DEPARTMENT
go
create unique clustered index PK_v_vehicle_rule_department on dbo.v_vehicle_rule_department(Vehicle_ID, Department_Rule_ID)
go
create nonclustered index IX_v_vehicle_rule_department on dbo.v_vehicle_rule_vehiclegroup(Vehicle_id, Rule_ID)
go

create view dbo.v_vehicle_rules
as
	select vehicle_id, rule_id, RuleNumber, Vehicle_Rule_ID, VehicleGroup_Rule_ID, Department_Rule_Id, [value], 1 [Priority]
	from dbo.v_vehicle_rule_vehicle with (noexpand) 

	union all

	select distinct vehicle_id, rule_id, RuleNumber, Vehicle_Rule_ID, VehicleGroup_Rule_ID, Department_Rule_Id, [value], 2 [Priority]
	from dbo.v_vehicle_rule_vehiclegroup with (noexpand) 

	union all

	select vehicle_id, rule_id, RuleNumber, Vehicle_Rule_ID, VehicleGroup_Rule_ID, Department_Rule_Id, [value], 3 [Priority]
	from dbo.v_vehicle_rule_department with (noexpand) 
go
create view [dbo].[v_vehicle_rule]
as  
select distinct
	vr.Vehicle_ID, 
	vr.Rule_ID, 
	vr.RuleNumber, 
	vr.[value], 
	[Priority]
from v_vehicle_rules vr
where vr.[Priority] = 
(
	select min([Priority]) 
	from v_vehicle_rules vr1 
	where vr.RULE_ID = vr1.RULE_ID and vr.VEHICLE_ID = vr1.VEHICLE_ID
)
go
