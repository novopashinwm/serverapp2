/****** Object:  StoredProcedure [dbo].[GetDataForOrders]    Script Date: 07/08/2010 16:35:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[GetDataForOrders]
@operator_id int = null
AS
begin
	select * from ORDER_TYPE

	select c.* 
	from CUSTOMER c
	join v_operator_department_right dr on dr.DEPARTMENT_ID = c.DEPARTMENT_ID
	where dr.operator_id= @operator_id 
	
	select o.* 
	from [ORDER] o
	join CUSTOMER c on c.CUSTOMER_ID = o.CUSTOMER
	join v_operator_department_right dr on dr.DEPARTMENT_ID = c.DEPARTMENT_ID
	where dr.operator_id= @operator_id 
	

	select ot.* 
	from ORDER_TRIP ot
	join [ORDER] o on ot.[ORDER] = o.ORDER_ID
	join CUSTOMER c on c.CUSTOMER_ID = o.CUSTOMER
	join v_operator_department_right dr on dr.DEPARTMENT_ID = c.DEPARTMENT_ID
	where dr.operator_id= @operator_id 
end









