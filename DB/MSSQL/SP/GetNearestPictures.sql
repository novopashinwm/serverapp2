IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetNearestPictures]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetNearestPictures]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetNearestPictures](
	@log_time int
	, @vehicle_id int
	, @bound int = 3
	--, @operator_id int = null
)
AS
BEGIN

SELECT
	Log_Time
FROM (
	select
		*
	from
		(select top (@bound)
			Log_Time
		from
			Picture_Log (nolock)
		where
			Vehicle_ID = @vehicle_id and Log_time < @log_time
		order by
			Log_time desc) t1
	union all
	select Log_Time from Picture_Log (nolock)
		where Log_time = @log_time AND Vehicle_ID = @vehicle_id
	union all
	select
		*
	from
		(select top (@bound)
			Log_Time
		from
			Picture_Log (nolock)
		where
			Vehicle_ID = @vehicle_id AND Log_time > @log_time 
		order by
			Log_time asc) t2
) t3
order by Log_Time asc
END
