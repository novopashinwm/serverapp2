set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddStatisticLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddStatisticLog]
GO

CREATE PROCEDURE [dbo].[AddStatisticLog]
	@vehicleID  		int,
	@logTime			int,
	@debug      		bit = 0,
	@maxAgeMonth		int = 2
	
AS
BEGIN
	set nocount on;
	
	if (@maxAgeMonth is null)
		set @maxAgeMonth = 2
	
	declare @lng		numeric(8,5),				-- longtitude
			@lat		numeric(8,5);				-- latitude

	--Более 2-х месяцев не пересчитываем
	declare @minLogHistory int;
	select @minLogHistory = datediff(ss,'01-01-1970',getutcdate()) - 3600*24*31*@maxAgeMonth;

	select 	@lng = lng, @lat = lat
	from dbo.geo_log g (nolock)
	where g.log_time = @logTime and g.vehicle_id = @vehicleID;

	if (@vehicleID is not null and @logTime > @minLogHistory)
	begin
		--Если нет записей по этой машине
		insert into dbo.Statistic_Log_Recalc (vehicle_id, log_time, pending)
			select @vehicleID, @logTime, 0
			where not exists (select 1 from dbo.Statistic_Log_Recalc 
							with (XLOCK, SERIALIZABLE) 
							where Vehicle_ID = @vehicleID)

		set transaction isolation level read committed;

		/*  При получении точки задним числом, просто делается отметка о необходимости пересчитать статистику.
			Реально же статистика пересчитывается, только если пришла самая последняя точка */		
		/*
		обновляем дату пересчета если:
			1. Пришедшая дата более ранняя чем имеющаяся запись или у записи нет даты (null) или имеющаяся запись старее 2-х месяцев
			2. Пришедшая запись новее имеющейся и имеющаяся уже находится в обработке (+3 часа)
		*/
		update slr 
			set Log_Time = @logTime,
			pending = 0
			from dbo.Statistic_Log_Recalc slr (xlock)
			where Vehicle_ID = @vehicleID 
			and (Log_Time > @logTime 
				or Log_Time < @minLogHistory 
				or Log_Time is null
				);

		declare @interval int;
		set @interval = 10800; --3 часа   
		
		update slr 
			set 
			pending = 0
			from dbo.Statistic_Log_Recalc slr (xlock)
			where Vehicle_ID = @vehicleID 
			and @logTime between Log_time + 1 and Log_time + @interval
			and pending = 1
			;

		
		/*
		Для самых последних позиций выполняем пересчет статистики сразу
		*/
		
		if exists (
			select * 
				from dbo.Statistic_Log_Recalc slr (xlock) 
				where Vehicle_ID = @vehicleID 
				  and Log_Time < @logTime)
			return;		
		
		if (not exists (select 1
						from dbo.geo_Log (nolock)
						where vehicle_id = @vehicleID 
						  and log_time > @logTime))
		begin
			--Если были задания на пересчет статистики для этой машины - выполняем их
			exec dbo.RecalcStatisticLog @vehicleID;
		end

	end --if (@vehicleID is not null and @logTime > @minLogHistory)
END