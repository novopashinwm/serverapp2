/****** Object:  StoredProcedure [dbo].[GetScheduleByDate]    Script Date: 07/09/2010 10:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetScheduleByDate]
	@DATE datetime,
	@operator_id int = null
	
/* 	
	@DATE  їиїЇЁї№ Є Є°Її ёїў■к¤■╕°кї√╣¤■є■ Єиї№ї¤°
*/
as
begin

-- жЁ╕ °╕Ё¤°а Єv┐■Ї■Є

	select distinct SCHEDULE.* 
	from SCHEDULE
		join WAYOUT w ON w.WAYOUT_ID = SCHEDULE.WAYOUT
		left join v_operator_route_right rr on rr.route_id = w.ROUTE
	where 
		SCHEDULE.DAY_KIND_ID = dbo.DayKind( @DATE, w.ROUTE )
		and (@operator_id is null or rr.operator_id = @operator_id)

-- T№ї¤v иЁ╕ °╕Ё¤°∙ Єv┐■Ї■Є
	select distinct SCHEDULE_DETAIL.*
	from SCHEDULE_DETAIL
		join SCHEDULE on SCHEDULE.SCHEDULE_ID = SCHEDULE_DETAIL.SCHEDULE_ID
		join WAYOUT on WAYOUT.WAYOUT_ID = SCHEDULE.WAYOUT
		left join v_operator_route_right rr on rr.route_id = WAYOUT.ROUTE
	where
		SCHEDULE.DAY_KIND_ID = dbo.DayKind( @DATE, WAYOUT.ROUTE )
		and (@operator_id is null or rr.operator_id = @operator_id)
	
-- жї∙╕v ╕№ї¤ иЁ╕ °╕Ё¤°∙ Єv┐■Ї■Є
	select TRIP.* 
	from TRIP
		join SCHEDULE_DETAIL on SCHEDULE_DETAIL.SCHEDULE_DETAIL_ID = TRIP.SCHEDULE_DETAIL_ID
		join SCHEDULE on SCHEDULE.SCHEDULE_ID = SCHEDULE_DETAIL.SCHEDULE_ID
		join WAYOUT on WAYOUT.WAYOUT_ID = SCHEDULE.WAYOUT
		left join v_operator_route_right rr on rr.route_id = WAYOUT.ROUTE
	where
		SCHEDULE.DAY_KIND_ID = dbo.DayKind( @DATE, WAYOUT.ROUTE )
		and (@operator_id is null or rr.operator_id = @operator_id)
end





