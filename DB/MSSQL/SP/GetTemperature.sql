/****** Object:  StoredProcedure [dbo].[GetTemperature]    Script Date: 07/14/2010 18:53:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTemperature]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTemperature]
GO



/****** Object:  StoredProcedure [dbo].[GetTemperature]    Script Date: 07/14/2010 18:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------
-- T■ўЄиЁХЁїк ЇЁ¤¤vї Ї√а  ■╕ки■ї¤°а ■квїкЁ ■ ╕■ё√жЇї¤°° кї№ їиЁк║и¤■є■ иїЎ°№Ё
-- LЄк■и: +иїёї¤¤°·■Є
----------------------------------------------------------------------------------------

CREATE procedure [dbo].[GetTemperature]
	@Date 		datetime,	-- -ЁкЁ
	@Tmin 		int,		-- №°¤°№Ё√╣¤■ Ї■ ║╕к°№Ёа кї№ їиЁк║иЁ
	@Tmax 		int,		-- №Ё·╕°№Ё√╣¤■ Ї■ ║╕к°№Ёа кї№ їиЁк║иЁ
	@Length		int,		-- Ї■ ║╕к°№Ёа  и■Ї■√Ў°кї√╣¤■╕к╣
	@VehicleGroup 	int		-- єи║  Ё TT
as

----------------------------------------------------------------------------------------
/*
-- Test
declare   
	@Date 		datetime,	-- -ЁкЁ
	@Tmin 		int,		-- №°¤°№Ё√╣¤■ Ї■ ║╕к°№Ёа кї№ їиЁк║иЁ
	@Tmax 		int,		-- №Ё·╕°№Ё√╣¤■ Ї■ ║╕к°№Ёа кї№ їиЁк║иЁ
	@Length		int,		-- Ї■ ║╕к°№Ёа  и■Ї■√Ў°кї√╣¤■╕к╣
	@VehicleGroup 	int		-- єи║  Ё TT

set @date = 
	--'2006-09-14 20:00:00'
	--'2006-09-21 20:00:00'
	'2006-11-07 21:00:00'
set @Tmin = 16 --30
set @Tmax = 16 --40
set @Length = 300
set @VehicleGroup = 15--6
*/
----------------------------------------------------------------------------------------


-- Ї√а ¤Ёё■иЁ кї№ їиЁк║иv
declare @DateTo datetime
set @DateTo = @Date + 1


-- жїў║√╣к°и║жХЁа кЁё√°бЁ Ї√а Є╕ї∙ єи║  v
create table #ResultAll(	
	--id int identity primary key, 
	GARAGE_NUMBER	nvarchar(50),
	DRIVER_NUM	nvarchar(50),
	DRIVER_SURNAME	nvarchar(50),
	DRIVER_NAME	nvarchar(50),
	Tmin		int,	
	BEGIN_Tmin	datetime,
	END_Tmin	datetime,
	Tmax		int,	
	BEGIN_Tmax	datetime,
	END_Tmax	datetime
	)


--TT Є┐■ЇаХ°ї Є єи║  ║ Ї√а ·■к■иv┐ ёv√° Єv °╕Ё¤v жT
select distinct 
	VH.VEHICLE_ID,
	VH.GARAGE_NUMBER,
	WBH.WAYBILL_HEADER_ID
into
	#Vehicles
from
	dbo.VEHICLE as VH
	inner join VEHICLEGROUP_VEHICLE as VGV on VH.VEHICLE_ID = VGV.VEHICLE_ID
	inner join WAYBILL_HEADER as WBH on VH.VEHICLE_ID = WBH.VEHICLE_ID
where
	VGV.VEHICLEGROUP_ID = @VehicleGroup 
	and WBH.[WAYBILL_DATE] = @Date
	and WBH.[CANCELLED] != 1
order by 
	VH.GARAGE_NUMBER, VH.VEHICLE_ID
	

-- ж║и╕■и  ■ кЁё√°бї TT
declare
	@Vehicle		int,	
	@GarageNum		nvarchar(50),
	@WaybillHeaderID	int	
DECLARE cursor_veh CURSOR FOR SELECT VEHICLE_ID, GARAGE_NUMBER, WAYBILL_HEADER_ID FROM #Vehicles
OPEN cursor_veh
FETCH FROM cursor_veh into @Vehicle, @GarageNum, @WaybillHeaderID
WHILE @@FETCH_STATUS = 0
BEGIN
	-- ж¤Ёвї¤°а кї№ їиЁк║иv Єv┐■ЇаХ°ї ўЁ ўЁЇЁ¤¤vї  иїЇї√v Ї√а єи║  v TT Є ║·ЁўЁ¤¤║ж ЇЁк║
	select distinct 
		--CS.CONTROLLER_ID,
		VH.VEHICLE_ID, 
		VH.GARAGE_NUMBER,
		CS.Value 		as Temper, 
		CS.Log_Time		as LogTime
		,0 			as OrderGroup
	into #temp
	from
		v_controller_sensor_log cs 
		inner join VEHICLE as VH on CS.VEHICLE_ID = VH.VEHICLE_ID
	where
		VH.VEHICLE_ID = @Vehicle 
		and CS.Log_Time between @Date and @DateTo
		and (CS.Value < @Tmin or CS.Value > @Tmax)
		and cs.SensorLegend = 5 --DigitalTemperatureSensor
	order by
		VH.GARAGE_NUMBER, CS.Log_Time
	
	-- ·║и╕■и  ■ кЁё√°бї кї№ їиЁк║и
	declare 
		@VehicleID 	int,
		@Temper 	int,
		@LogTime 	datetime,
		@VehicleIDNext 	int,
		@TemperNext 	int,
		@LogTimeNext 	datetime
	
	declare @Group int 
	set @Group = 0
	
	-- +╕√°  ■ў°б°°  и°░√° Є кївї¤°° 60 ╕ї· - ┐■и■░°ї  ■ў°б°°
	DECLARE cursor_temp CURSOR FOR SELECT VEHICLE_ID, TEMPER, LOGTIME FROM #temp
	OPEN cursor_temp
	FETCH FROM cursor_temp into @VehicleID, @Temper, @LogTime
	FETCH NEXT FROM cursor_temp into @VehicleIDNext, @TemperNext, @LogTimeNext
	WHILE @@FETCH_STATUS = 0
	BEGIN
		if (@VehicleIDNext != @VehicleID or @TemperNext != @Temper or (@LogTimeNext-@LogTime) > 60 )
		begin
			Set @Group = @Group + 1
		end
		
		update #temp Set OrderGroup = @Group where VEHICLE_ID = @VehicleIDNext and Temper = @TemperNext and LogTime = @LogTimeNext
	
		Set @VehicleID = @VehicleIDNext 
		Set @Temper = @TemperNext 
		Set @LogTime = @LogTimeNext
	
		FETCH NEXT FROM cursor_temp into @VehicleIDNext, @TemperNext, @LogTimeNext
	END
	CLOSE cursor_temp
	DEALLOCATE cursor_temp
	
	-- и■Ї■√Ў°кї√╣¤■╕к╣ кї№ їиЁк║и¤v┐ Ї°Ё ■ў■¤■Є
	select distinct 
		VEHICLE_ID,
		GARAGE_NUMBER,
		TEMPER,
		ORDERGROUP,
		min(LogTime) as TemperBegin,
		max(LogTime) as TemperEnd
	into 
		#tempres
	from 
		#temp
	group by
		VEHICLE_ID, GARAGE_NUMBER, TEMPER, ORDERGROUP
	order by 
		GARAGE_NUMBER, VEHICLE_ID, TemperBegin, TemperEnd, ORDERGROUP
	
	--║ЇЁ√аї№, ї╕√°  и■Ї■√Ў°кї√╣¤■╕к╣ кї№ї їиЁк║и¤■є■ Ї°Ё Ёў■¤Ё №ї¤її ўЁЇЁ¤¤■∙
	delete from #tempres where datediff(s, TemperBegin, TemperEnd) < @Length
	
	 
	-- T■Ї°кї√╣ ╕ TT, ·■к■иv∙ Єv ■√¤а√  и■°ўЄ■Ї╕кЄї¤¤v∙ иї∙╕ ( ■√¤v∙, ·■и■к·°∙, ўЁ·Ёў) Є ║·ЁўЁ¤¤║ж ЇЁк║
	select distinct
		VH.VEHICLE_ID,
		VH.GARAGE_NUMBER,
		DR.DRIVER_ID,
		DR.EXT_NUMBER 	as DRIVER_NUM,
		DR.[NAME]	as DRIVER_NAME,
		DR.SHORT_NAME	as DRIVER_SURNAME,
		WBT.TRIP_ID,
		WBT.BEGIN_TIME 	as BEGIN_TRIP,
		WBT.END_TIME 	as END_TRIP  
		,BEGIN_TEMP = convert(datetime, null)
		,END_TEMP = convert(datetime, null) 
	into #driver
	from 
		dbo.WB_TRIP as WBT
		inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
		-- TT
		inner join dbo.VEHICLE as VH on WBH.VEHICLE_ID = VH.VEHICLE_ID
		-- T■Ї°кї√╣
		inner join dbo.WAYBILL as WB on WBT.WAYBILL_ID = WB.WAYBILL_ID
		inner join dbo.DRIVER as DR on WB.DRIVER_ID = DR.DRIVER_ID
	where	
		WBH.[WAYBILL_HEADER_ID] = @WaybillHeaderID
		and VH.VEHICLE_ID = @Vehicle 
		and WBT.[TRIP_KIND_ID] in (5, 6, 9)
	order by
		VH.GARAGE_NUMBER, WBT.BEGIN_TIME, WBT.END_TIME 
	
		
	--иї∙╕v ° кї№ їиЁк║иЁ
	select 
		dr.*,
		t.TEMPER,
		t.TEMPERBEGIN,  
		t.TEMPEREND  
	into
		#temptrip
	from
		#tempres as t
		inner join #driver as dr on t.VEHICLE_ID = dr.VEHICLE_ID 
			and (
				dr.BEGIN_TRIP between t.TemperBegin and t.TemperEnd
				or dr.END_TRIP between t.TemperBegin and t.TemperEnd
				or t.TemperBegin between dr.BEGIN_TRIP and dr.END_TRIP
				or t.TemperEnd between dr.BEGIN_TRIP and dr.END_TRIP
			) 
	order by
		dr.GARAGE_NUMBER, dr.BEGIN_TRIP, t.TemperBegin, t.TemperEnd, t.ORDERGROUP
	
	-- ¤Ё√■Ўї¤°ї кї№ їиЁк║и¤v┐ Ї°Ё ■ў■¤■Є ¤Ё иї∙╕v
	update 
		#temptrip 
	Set 
		BEGIN_TEMP = (case when TemperBegin <= BEGIN_TRIP then BEGIN_TRIP else TemperBegin end),
		END_TEMP = (case when TemperEnd >= END_TRIP then END_TRIP else TemperEnd end)
	
	
	-- иїЇЄЁи°кї√╣¤v∙ иїў║√╣кЁк
	select distinct 
		VEHICLE_ID,
		GARAGE_NUMBER,
		DRIVER_NUM,
		DRIVER_SURNAME,
		DRIVER_NAME,
		TRIP_ID,
		null 		as Tmin,
		BEGIN_Tmin 	= convert(datetime, null),
		END_Tmin 	= convert(datetime, null),
		null 		as Tmax,
		BEGIN_Tmax	= convert(datetime, null),
		END_Tmax	= convert(datetime, null)
	into
		#temptripres
	from
		#temptrip 
	
	--№°¤°№Ё√╣¤Ёа ° №Ё·╕°№Ё√╣¤Ёа кї№ їиЁк║иv Є иї∙╕ї
	update 
		#temptripres 
	Set 
		#temptripres.Tmin = (select top 1 min(tt.TEMPER) from #temptrip as tt where tt.VEHICLE_ID = #temptripres.VEHICLE_ID and tt.TRIP_ID = #temptripres.TRIP_ID and tt.TEMPER < @Tmin),
		#temptripres.Tmax = (select top 1 max(tt.TEMPER) from #temptrip as tt where tt.VEHICLE_ID = #temptripres.VEHICLE_ID and tt.TRIP_ID = #temptripres.TRIP_ID and tt.TEMPER > @Tmax)
	
	-- T иї∙╕ї №■Ўїк ёvк╣ ¤ї╕·■√╣·■ №°¤°№Ё√╣¤v┐ °√° №Ё·╕°№Ё√╣¤v┐ Ї°Ё ■ў■¤■Є
	select 
		r.*,
		tt.TEMPER,
		tt.BEGIN_TEMP,
		tt.END_TEMP
	into
		#Result
	from 
		#temptripres as r
		inner join #temptrip as tt on r.VEHICLE_ID = tt.VEHICLE_ID and r.TRIP_ID = tt.TRIP_ID
			and(r.Tmin = tt.TEMPER or r.Tmax = tt.TEMPER) 
	
	--Tиї№ї¤¤vї °¤кїиЄЁ√v Ї√а кї№ їиЁк║и
	update 
		#Result 
	Set 
		Tmin = (case when (Tmin = TEMPER) then TEMPER else convert(int, null) end),
		BEGIN_Tmin = (case when (Tmin = TEMPER) then BEGIN_TEMP end),
		END_Tmin = (case when (Tmin = TEMPER) then END_TEMP end),
		Tmax = (case when (Tmax = TEMPER) then TEMPER else convert(int, null) end),
		BEGIN_Tmax = (case when (Tmax = TEMPER) then BEGIN_TEMP end),
		END_Tmax = (case when (Tmax = TEMPER) then END_TEMP end)
	
	
	--T·√ЁЇvЄЁї№ ¤ЁёиЁ¤¤vї к■в·° Є ■ёХ║ж иїў║√╣к°и║жХ║ж кЁё√°б║
	insert #ResultAll (	
		GARAGE_NUMBER,
		DRIVER_NUM,
		DRIVER_SURNAME,
		DRIVER_NAME,
		Tmin,	
		BEGIN_Tmin,
		END_Tmin,
		Tmax,	
		BEGIN_Tmax,
		END_Tmax
		) 
	select 
		GARAGE_NUMBER,
		DRIVER_NUM,
		DRIVER_SURNAME,
		DRIVER_NAME,
		Tmin,	
		BEGIN_Tmin,
		END_Tmin,
		Tmax,	
		BEGIN_Tmax,
		END_Tmax
	from 
		#Result
	
	drop table #driver
	drop table #temp
	drop table #tempres
	drop table #temptrip
	drop table #temptripres
	drop table #Result
	
	FETCH FROM cursor_veh into @Vehicle, @GarageNum, @WaybillHeaderID
END
CLOSE cursor_veh
DEALLOCATE cursor_veh


select * from #ResultAll


drop table #Vehicles
drop table #ResultAll

