if exists (
	select * 
		from sys.procedures 
		where name = 'CreateDemaskingMessage'
)
	drop procedure CreateDemaskingMessage
go

create procedure CreateDemaskingMessage
(
	@contact_id int
)
as

set nocount on

declare @templateId		nvarchar(50) = (select mt.Message_Template_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'DemaskAsidRequest')
if @templateId is null
	return;
	
declare @uidFieldValue	varchar(32) = (select replace(NEWID(), '-', ''))
declare @modemContactId varchar(255) = (
	select asid.ID
		from CONSTANTS const
		join Contact asid on asid.Type = 3 and asid.Value = const.VALUE
		where const.NAME = 'ModemAsid')

if @modemContactId is null
	return;

declare @uidFieldId	int = (select mtf.MESSAGE_TEMPLATE_FIELD_ID 
								from MESSAGE_TEMPLATE_FIELD mtf 
								where mtf.MESSAGE_TEMPLATE_ID = @templateId
								  and mtf.NAME = 'RequestUid')
if @uidFieldId is null
	return;

begin tran

insert into Message (Status, BODY, TEMPLATE_ID, Time)
	select /*Wait = */ 1, @uidFieldValue + ' ##' + demasking.Value, @templateId, GETUTCDATE()
		from Contact demasking
		where demasking.ID = @contact_id
		  and demasking.Type = 3 /*Asid*/

if (@@ROWCOUNT = 1)
begin

	declare @message_id int = @@identity
	insert into Message_Contact 
		select @message_id, 1 /*Destination*/, @modemContactId
	union all
		select @message_id, 6 /*Reference*/, @contact_id
		
	insert into MESSAGE_FIELD (MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
		select @message_id, @uidFieldId, @uidFieldValue		

end

commit
go
