﻿IF (OBJECT_ID(N'[dbo].[InsertSession]') IS NOT NULL)
	DROP PROCEDURE [dbo].[InsertSession];
GO

CREATE PROCEDURE [dbo].[InsertSession]
(
	@OPERATOR_ID    int,
	@HOST_IP        nvarchar(40),
	@HOST_NAME      nvarchar(510),
	@SESSION_START  datetime,
	@SESSION_END    datetime,
	@CLIENT_VERSION nvarchar(50)
)
AS
BEGIN
	INSERT INTO [dbo].[SESSION]
		([OPERATOR_ID], [HOST_IP], [HOST_NAME], [SESSION_START], [SESSION_END], [CLIENT_VERSION])
	VALUES
		(@OPERATOR_ID,  @HOST_IP,  @HOST_NAME,  @SESSION_START,  @SESSION_END,  @CLIENT_VERSION)
	SELECT [SESSION_ID] = CAST(SCOPE_IDENTITY() AS int)
END
GO