set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddZoneDistanceStandart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddZoneDistanceStandart]
go

CREATE PROCEDURE [dbo].[AddZoneDistanceStandart]  
	@from nvarchar(100), @to nvarchar(100), @distance real
AS
BEGIN
	delete dbo.ZONE_DISTANCE_STANDART
	where (zone_name_from = @from and zone_name_to = @to)
	or (zone_name_from = @to and zone_name_to = @from);

	insert into dbo.ZONE_DISTANCE_STANDART (zone_name_from, zone_name_to, distance)
	values (@from, @to, (@distance * 1000));
END

