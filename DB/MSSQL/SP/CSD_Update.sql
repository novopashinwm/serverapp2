IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CSD_Update]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[CSD_Update] 

GO

CREATE PROCEDURE [dbo].[CSD_Update]
	@guid uniqueidentifier,
	@login nvarchar(50),
	@password nvarchar(50),
	@clientID int
AS
	if exists (select * from [CSD].dbo.CSD where OPERATOR_GUID = @guid)
		begin
			update [CSD].dbo.CSD
			set OPERATOR_LOGIN = @login, OPERATOR_PASSWORD = @password, CLIENT_ID = @clientID 
			where OPERATOR_GUID = @guid
		end
	else
		begin
			insert [CSD].dbo.CSD (OPERATOR_GUID, OPERATOR_LOGIN, OPERATOR_PASSWORD, CLIENT_ID)
			values (@guid, @login, @password, @clientID)
		end	
