
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF EXISTS ( SELECT * FROM sysobjects WHERE name = N'DelLinesWeb' AND type = 'P')
    DROP PROCEDURE DelLinesWeb
GO

-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Del Lines for Web
-- =============================================

CREATE PROCEDURE [dbo].[DelLinesWeb]  
	@line_id			int
AS

------------------------------------------------
/*
-- Test
declare
	@line_id	int
set @line_id = 1
*/
------------------------------------------------
	
create table #Vertex(	
	--id int identity primary key, 
	Vertex_ID	int
	)

insert into #Vertex
select distinct	VERTEX_ID from WEB_LINE_VERTEX where LINE_ID = @line_id

delete from dbo.WEB_LINE_VERTEX where LINE_ID = @line_id
delete from dbo.MAP_VERTEX where VERTEX_ID in (select VERTEX_ID from #Vertex)
delete from dbo.WEB_LINE where LINE_ID = @line_id

drop table #Vertex









