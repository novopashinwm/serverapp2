﻿IF (OBJECT_ID(N'[dbo].[Statistic_Log#Lasts_Update]') IS NOT NULL)
	DROP PROCEDURE [dbo].[Statistic_Log#Lasts_Update];
GO

CREATE PROCEDURE [dbo].[Statistic_Log#Lasts_Update]
(
	@vehicle_id     int,
	@log_time       int,
	@odometer       bigint,
	@ignitionOnTime int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@rows   int = 0;

	UPDATE [dbo].[Statistic_Log#Lasts]
		SET
			[Log_Time#Last]       = CASE WHEN [Log_Time#Last] < @log_time THEN @log_time       ELSE [Log_Time#Last]       END,
			[Odometer#Last]       = CASE WHEN [Log_Time#Last] < @log_time THEN @odometer       ELSE [Odometer#Last]       END,
			[IgnitionOnTime#Last] = CASE WHEN [Log_Time#Last] < @log_time THEN @ignitionOnTime ELSE [IgnitionOnTime#Last] END
	WHERE [VEHICLE_ID] = @vehicle_id

	SELECT @rows = @@ROWCOUNT

	IF (0 = @rows)
	BEGIN
		INSERT INTO [dbo].[Statistic_Log#Lasts] ([Vehicle_ID], [Log_Time#Last], [Odometer#Last], [IgnitionOnTime#Last])
		VALUES                                  (@vehicle_id,  @log_time,       @odometer,       @ignitionOnTime)
	END
END
GO