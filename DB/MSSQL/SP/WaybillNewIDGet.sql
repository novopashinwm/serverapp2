/****** Object:  StoredProcedure [dbo].[WaybillNewIDGet]    Script Date: 07/15/2010 10:12:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =Ёў¤Ёвї¤°ї:
-- ж■√║вї¤°ї °ў кЁё√°бv WAYBILL_HEADER ID  ║кїЄ■є■ √°╕кЁ (waybill_header_id)  ■ ¤■№їи║  ║кїЄ■є■ √°╕кЁ ° ЇЁкї
-- LЄк■и: +иїёї¤¤°·■Є ж.T.

ALTER PROCEDURE [dbo].[WaybillNewIDGet] 
	(
	@for_Num nvarchar(30), -- =■№їи  ║кїЄ■є■ √°╕кЁ
	@for_Date DateTime,      -- -ЁкЁ
	@operator_id int = null
	) 
AS

-------------------------------------------------------
-- Tї╕к■Єvї ЇЁ¤¤vї
--set @for_Num = 61053--1560--55
--set @for_Date = '19970101'
-------------------------------------------------------

	if @for_Num is not null --and @for_Date <= '19970101'
	begin
		-- Tvё■и ID  ║кїЄ■є■ √°╕кЁ ° ╕■■кЄїк╕кЄ║жХї∙ ї№║ ЇЁкv, ї╕√° ўЁЇЁ¤ ¤■№їи  ║кїЄ■є■ √°╕кЁ ° ¤ї ўЁЇЁ¤Ё ЇЁкЁ 
		--select waybill_header_id, cast(FLOOR(cast(waybill_date as float)) as datetime) from WAYBILL_HEADER where ext_number=@for_Num order by waybill_date
		select distinct
			WH.waybill_header_id, 
			WH.waybill_date
			,VL.VEHICLE_KIND_ID 
		from 
			WAYBILL_HEADER as WH 
			left join VEHICLE as VL on VL.VEHICLE_ID = WH.VEHICLE_ID
			left join v_operator_vehicle_right rr on rr.vehicle_id = WH.VEHICLE_ID
		where WH.ext_number = @for_Num 
		and (@operator_id is null or rr.operator_id = @operator_id)
		order by WH.waybill_date
	end

	/*else if @for_Num is null and @for_Date > '19970101'
	begin
		-- Tvё■и ID  ║кїЄ■є■ √°╕кЁ ° ╕■■кЄїк╕кЄ║жХїє■ ї№║ ¤■№їиЁ, ї╕√° ўЁЇЁ¤Ё ЇЁкЁ ° ¤ї ўЁЇЁ¤ ¤■№їи  ║кїЄ■є■ √°╕кЁ
		select waybill_header_id, ext_number from WAYBILL_HEADER where waybill_date = @for_Date order by ext_number
	end*/




