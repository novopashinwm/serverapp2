set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDataForWeb]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetDataForWeb]
GO

/*
Execution sample:

	declare @ids Id_Param
--	insert @ids select 433
	exec dbo.[GetDataForWeb]  474, 1, @ids

*/

create Procedure [dbo].[GetDataForWeb]
 @operatorID int,
 @departmentID int = null,
 @vehicleIds Id_Param readonly
AS  

set nocount on
  
create table #vehicles(  
 operator_id int,  
 [name] nvarchar(250),  
 vehicle_id int,  
 garage_number nvarchar(255),  
 allow bit,  
 fuel_tank int,  
 phone nvarchar(50),  
 vehicle_kind_id int,  
 vehicle_type nvarchar(100),  
 public_number nvarchar(32)  
)  
  
declare @vidsCount int = (select COUNT(1) from @vehicleIds vids)
  
insert into #vehicles  
 select op.Operator_ID,   
     op.Name,   
     v.Vehicle_ID,   
     v.Garage_Number,   
     1 as 'Allow',   
     v.fuel_tank,   
     c.phone,  
     v.vehicle_kind_id,  
     vehicle_type,  
     public_number  
 from dbo.v_operator_vehicle_right ovr  
 join dbo.Operator op on op.Operator_ID=ovr.Operator_ID  
 join dbo.Vehicle v on v.Vehicle_ID=ovr.Vehicle_ID  
 join dbo.controller c on c.vehicle_id = v.vehicle_id  
 join dbo.controller_type ct on ct.controller_type_id = c.controller_type_id  
  where ovr.OPERATOR_ID = @operatorID  
   and ovr.right_id = 102  
    and (@departmentID is null or v.department = @departmentID and @vidsCount = 0)
   and (@vidsCount = 0 or ovr.vehicle_id in (select vids.Id from @vehicleIds vids))
    
  
--ALLOW_VEHICLE  
select * from #vehicles  
  
--VEHICLE_PROFILE  
select v.vehicle_id, vp.property_name, vp.value
 from #vehicles v  
 inner join [dbo].vehicle_profile vp on vp.vehicle_id = v.vehicle_id  
  
--VEHICLE_CAPABILITY  
select v.vehicle_id,  
  case when mlpc.ID is null     then 0 else 1 end  AllowAskPosition,  
  case when v.vehicle_kind_id <> 7   then 1 else 0 end  SpeedMonitoring,  
  case when v.vehicle_kind_id not in (5,7) then 1 else 0 end  IgnitionMonitoring  
 from #vehicles v  
 left outer join dbo.Controller c on c.vehicle_id = v.vehicle_id  
 left outer join dbo.MLP_Controller mlpc on mlpc.controller_id = c.controller_id  
 left outer join dbo.Asid a on a.id = mlpc.asid_id  
   
   
drop table #vehicles  