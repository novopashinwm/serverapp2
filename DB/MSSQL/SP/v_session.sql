if exists (
	select *
		from sys.views 
		where name = 'v_session'
)
	drop view v_session
go

create view v_session
as
select s.*, o.LOGIN, UserAgentName = ua.Name
	from session s
	left outer join operator o on o.operator_Id = s.operator_id
	left outer join Session_UserAgent sua on sua.Session_ID = s.SESSION_ID
	left outer join UserAgent ua on ua.ID = sua.UserAgent_ID