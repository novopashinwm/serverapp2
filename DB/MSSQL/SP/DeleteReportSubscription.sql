IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'DeleteReportSubscription' 
)
   DROP PROCEDURE [dbo].[DeleteReportSubscription]
GO

CREATE PROCEDURE [dbo].[DeleteReportSubscription]  
	@schedule_queue_id	int
AS
	delete from dbo.EMAIL_SCHEDULERQUEUE where SCHEDULERQUEUE_id = @schedule_queue_id
	delete from dbo.SCHEDULERQUEUE where SCHEDULERQUEUE_id = @schedule_queue_id
	

