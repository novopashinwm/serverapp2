IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_DeleteOperator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[H_DeleteOperator]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[H_DeleteOperator]
        @TRAIL_ID int, 
        @OPERATOR_ID int
AS
BEGIN
    declare @utc_date datetime
    set @utc_date = GETUTCDATE()
    insert into [dbo].[H_OPERATOR] (
    TRAIL_ID,ACTION, ACTUAL_TIME,[OPERATOR_ID], 
        [NAME], 
        [LOGIN], 
        [PASSWORD], 
        --[PHONE], 
        [EMAIL], 
        [GUID]
    ) SELECT
    @TRAIL_ID as TRAIL_ID,'DELETE' as ACTION, @utc_date, [OPERATOR_ID], 
        [NAME], 
        [LOGIN], 
        [PASSWORD], 
        --[PHONE], 
        [EMAIL], 
        [GUID]
    FROM [dbo].[OPERATOR]
    WHERE 
        [OPERATOR_ID]=@OPERATOR_ID
    
    delete [dbo].[OPERATOR] WHERE 
        [OPERATOR_ID]=@OPERATOR_ID
    
END



GO


