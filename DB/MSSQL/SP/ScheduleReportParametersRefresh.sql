/****** Object:  StoredProcedure [dbo].[ScheduleReportParametersRefresh]    Script Date: 07/14/2010 18:19:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
--=-╖а-мца-ц: м╕v╕мца-ц аци╕Ў╣-м--Ў ░---ёц╣-╕№ иv- ╕╣мц╣- "м-Ў░-Ў-а-ц" ░╕ ╕№ц -╖№цЎ╣аvё.
--L№╣╕-: LvццЎц- м. +v-а╕№.

--------------------------------------------------------------------------------------------
--DDL
ALTER procedure [dbo].[ScheduleReportParametersRefresh]
	@RouteID	int,		-- ID ё----╕╣-;
	@PeriodID	int,		-- ID ░ц--╕и- иц-Ў╣№-- --Ў░-Ў-а--;
	@DayKindID	int,		-- ID ╣-░- иа-.
	@operator_id int = null
as

--------------------------------------------------------------------------------------------
--DML
--м---╣- ╕╣ ░╕Ў╣vЎ ╖а-мца-- ░ц-ци-ааvЎ №Ў╕иаvЎ ░---ёц╣-╕№. 
---╕-╕-╕ цv, м╣╕цv а- р╣╕╣ Ўv╕м-- ░╕иа-ё-v-Ў╕ ╕№-и-цё-- ╕--цц-, а╕ ац -ц-v-╖╕№-а ░ц-цЎ№-╣ цц № ц╕иц цv-ца╣-.
if @RouteID is null 		set @RouteID = 0
if @PeriodID is null 		set @PeriodID = 0
if @DayKindID is null 		set @DayKindID = 0

--м╕v╕мца-ц Ў░-Ўц- №ЎцЎ а╕ёц-╕№ ё----╕╣╕№ -╖ +-
select distinct RT.[ROUTE_ID]		Route_ID,
		RT.[EXT_NUMBER]		RouteNumber
from dbo.ROUTE as RT
left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where (@operator_id is null or rr.operator_id = @operator_id)
order by RT.[EXT_NUMBER]

--м╕v╕мца-ц Ў░-Ўц- а-╖№-а-- №ЎцЎ ░ц--╕и╕№ -╖ +-.
select distinct	DKS.[DK_SET_ID]			PeriodID,
	DKS.[NAME]			PeriodName
from dbo.DK_SET as DKS
left join v_operator_department_right rr on rr.DEPARTMENT_ID = DKS.DEPARTMENT_ID
where (@operator_id is null or rr.operator_id = @operator_id)
order by DKS.[NAME]

--м╕v╕мца-ц Ў░-Ўц- и╕Ў╣╕░аvЎ ╣-░╕№ иац- ░╕ а-╖№-а-м ░ц--╕и- --Ў░-Ў-а--
select 	DK.[DAY_KIND_ID]		DayKindID,
	DK.[NAME]			DayKindName
from dbo.DAY_KIND as DK
join dbo.DK_SET as DKS on DKS.DK_SET_ID = DK.DK_SET
left join v_operator_department_right rr on rr.DEPARTMENT_ID = DKS.DEPARTMENT_ID
where DK.[DK_SET] = @PeriodID
and (@operator_id is null or rr.operator_id = @operator_id)
order by DK.[NAME]

--м╕v╕мца-ц Ў░-Ўц- а╕ёц-╕№ №vЎ╕и╕№ ░╕ ░ц-ци-аа╕ё╕ а╕ёц-╕ №vЎ╕и-, а╕ёц-╕ ё----╕╣- - а-╖№-а-м ╣-░- иа-
select 	SC.[EXT_NUMBER]			ScheduleNumber
from dbo.SCHEDULE as SC
inner join dbo.DAY_KIND as DK
	on DK.[DAY_KIND_ID] = SC.[DAY_KIND_ID]
inner join dbo.DK_SET as DKS on DKS.DK_SET_ID = DK.DK_SET
left join v_operator_department_right rr on rr.DEPARTMENT_ID = DKS.DEPARTMENT_ID
where 	SC.[ROUTE_ID] = @RouteID
	and DK.[DAY_KIND_ID] = @DayKindID
	and (@operator_id is null or rr.operator_id = @operator_id)
order by SC.[EXT_NUMBER]

--------------------------------------------------------------------------------------------









