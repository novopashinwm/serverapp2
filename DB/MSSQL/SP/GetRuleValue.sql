if exists (select * from sys.objects where name='GetRuleValue' and type='FN')
	drop function GetRuleValue
go
create function GetRuleValue
(
	@vehicle_id int,
	@rule_number int
)
returns decimal(18,4)
as
begin
	declare @rule_value decimal(18,4);
	
	select @rule_value = [value]
		from dbo.VEHICLE_RULE
		where VEHICLE_ID = @vehicle_id
		and RULE_ID = (select rule_id from [rule] where number = @rule_number);

	if (@rule_value is null)
	begin
		select @rule_value = [value]
		from dbo.VEHICLEGROUP_RULE vgr
		join dbo.VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLEGROUP_ID = vgr.VEHICLEGROUP_ID
		where vgv.VEHICLE_ID = @vehicle_id
		and vgr.RULE_ID = (select rule_id from [rule] where number = @rule_number);
	end

	return @rule_value;
end
go