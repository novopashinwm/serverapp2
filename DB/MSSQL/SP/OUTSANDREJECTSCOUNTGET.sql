/****** Object:  StoredProcedure [dbo].[OutsAndRejectsCountGet]    Script Date: 07/15/2010 10:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [dbo].[OutsAndRejectsCountGet]
	@ReportDateFrom	datetime,	---ЁкЁ ¤ЁвЁ√Ё  їи°■ЇЁ, ўЁ ·■к■иv∙ ╕ки■°к╕а ■квїк;
	@ReportDateTo	datetime,	---ЁкЁ ■·■¤вЁ¤°а  їи°■ЇЁ, ўЁ ·■к■иv∙ ╕ки■°к╕а ■квїк;
	@MotorcadeID	int,		--ID ЁЄк■·■√■¤¤v (єи║  v TT), Ї√а ·■к■и■∙ ╕ки■°к╕а ■квїк.
	@operator_id int = null
as

--------------------------------------------------------------------------------------------
--Tї╕к
/*declare	
	@ReportDateFrom	datetime,	
	@ReportDateTo	datetime,	
	@MotorcadeID	int		
set @ReportDateFrom = '2006-09-18 21:00:00'
set @ReportDateTo = '2006-09-19 21:00:00'
set @MotorcadeID = 0--7 */
--------------------------------------------------------------------------------------------

--+и║  v TT
select distinct
	VG.*, 
	0 as OutsCount,
	0 as DefectsCount
into #VehicleGroup 
from VEHICLEGROUP as VG 
left join v_operator_vehicle_groups_right rr on rr.vehiclegroup_id = VG.VEHICLEGROUP_ID
where @operator_id is null or rr.operator_id = @operator_id

--select * from #VehicleGroup


update #VehicleGroup set #VehicleGroup.OutsCount = (	
	--ж■√║вї¤°ї ·■√°вї╕кЄЁ жT, ║ ·■к■иv┐ ёv√° ╕┐■Їv Є кївї¤°° ║·ЁўЁ¤¤■є■  їи°■ЇЁ ° ·■к■иvї ¤ї ёv√° Ё¤¤║√°и■ЄЁ¤v.
	--·Ё·  ■  и°в°¤Ё№, ╕ЄаўЁ¤¤v№ ╕ TT (TRIP_KIND_ID = 11, 12), кЁ· °  ■  и°в°¤Ё№, ╕ЄаўЁ¤¤v№ ╕ Є■Ї°кї√ї№  (TRIP_KIND_ID = 15, 16).
	--L√є■и°к№ ║в°кvЄЁїк ¤ЁвЁ√╣¤■ї ╕■╕к■а¤°ї жT (WAYBILL_HEADER.INITIAL_TRIP_KIND).
	select	
		count (distinct	WBH.WAYBILL_HEADER_ID)	[OutsCount]
	from 	
		dbo.WB_TRIP as WBT
		right join dbo.WAYBILL_HEADER as WBH on WBH.WAYBILL_HEADER_ID = WBT.WAYBILL_HEADER_ID
		inner join dbo.VEHICLE as VH on VH.VEHICLE_ID = WBH.VEHICLE_ID
		inner join dbo.VEHICLEGROUP_VEHICLE as VHG_V on VHG_V.[VEHICLE_ID] = VH.[VEHICLE_ID]
		inner join dbo.VEHICLEGROUP as VHG on VHG.[VEHICLEGROUP_ID] = VHG_V.[VEHICLEGROUP_ID]
	where	
		WBH.[WAYBILL_DATE] > @ReportDateFrom - 1			--Tvё■и ЇЁ¤¤v┐  ■ жT, ■к¤■╕аХ°┐╕а · ║·ЁўЁ¤¤■№║  їи°■Ї║.
		and WBH.[WAYBILL_DATE] <= @ReportDateTo
		and WBT.[TRIP_KIND_ID] in (11, 12, 15, 16)			--Tvё■и к■√╣·■ иї∙╕■Є,  ■ ·■к■иv№ ёv√ ╕┐■Ї,
		and WBH.INITIAL_TRIP_KIND in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)	--Tvё■и к■√╣·■ жT ╕ "¤■и№Ё√╣¤v№" ¤ЁвЁ√╣¤v№ ╕■╕к■а¤°ї№,
		and WBH.CANCELLED = 0						--Є ╕ °╕■· ¤ї Ї■√Ў¤v  ■ ЁЇЁк╣ Ё¤║√°и■ЄЁ¤¤vї жT. 
		and VHG.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID --@MotorcadeID 	--Tvё°иЁжк╕а жT к■√╣·■ Ї√а TT ║·ЁўЁ¤¤■∙ ЁЄк■·■√■¤¤v,
)


update #VehicleGroup set #VehicleGroup.DefectsCount = (	
	--ж■√║вї¤°ї ·■√°вї╕кЄЁ ўЁёиЁ·■ЄЁ¤¤v┐ иї∙╕■Є Ї√а жT, ■к¤■╕аХ°┐╕а · ║·ЁўЁ¤¤■№║  їи°■Ї║.
	select 
		count (distinct WBT.WB_TRIP_ID)	[RejectsCount]
	from 
		dbo.WAYBILL_HEADER as WBH 
		left join dbo.WB_TRIP as WBT on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
		inner join dbo.VEHICLE as VH on VH.VEHICLE_ID = WBH.VEHICLE_ID
		inner join dbo.VEHICLEGROUP_VEHICLE as VHG_V on VHG_V.[VEHICLE_ID] = VH.[VEHICLE_ID]
		inner join dbo.VEHICLEGROUP as VHG on VHG.[VEHICLEGROUP_ID] = VHG_V.[VEHICLEGROUP_ID]
	where 	
		WBH.[WAYBILL_DATE] > @ReportDateFrom - 1	--Tvё■и ЇЁ¤¤v┐  ■ жT, ■к¤■╕аХ°┐╕а · ║·ЁўЁ¤¤■№║  їи°■Ї║.
		and WBH.[WAYBILL_DATE] <= @ReportDateTo
		and WBH.CANCELLED = 0				--Є ╕ °╕■· ¤ї Ї■√Ў¤v  ■ ЁЇЁк╣ Ё¤║√°и■ЄЁ¤¤vї жT. 
		and VHG.[VEHICLEGROUP_ID] = #VehicleGroup.VEHICLEGROUP_ID --@MotorcadeID	--Tvё°иЁжк╕а жT к■√╣·■ Ї√а TT ║·ЁўЁ¤¤■∙ ЁЄк■·■√■¤¤v,
		and WBT.TRIP_KIND_ID in(5, 6)			--L╕·√жвї¤°ї кї┐, ·■к■иvї №■є║к ўЁ иЁЄ√ак╣╕а ° к. . (║в°кvЄЁжк╕а к■√╣·■ иї∙╕v ° ·■и■к·°ї иї∙╕v)
		--TкЁк║╕ ( 0x02=ёиЁ·, 0x04=Єv ■√¤ї¤ ¤■ ╕ ё■√╣░°№° ■к·√■¤ї¤°а№°, 0x08= и■ ║Хї¤, 0x16= и■ ║Хї¤ °ў-ўЁ ўЁк■иЁ ) 
		and (	(WBT.STATUS & 0x02) = 0x02 
			or (WBT.STATUS & 0x04) = 0x04 
			or (WBT.STATUS & 0x08) = 0x08 
			or (WBT.STATUS & 0x16) = 0x16)
)


select * from #VehicleGroup order by [NAME]
drop table #VehicleGroup



