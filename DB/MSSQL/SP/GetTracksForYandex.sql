IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTracksForYandex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTracksForYandex]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON --��� ������ ��������������� �������������
GO

/*

set statistics time on
exec GetTracksForYandex 1
*/

create PROCEDURE [dbo].[GetTracksForYandex]
(
	@debug bit = null
)
AS
begin
	if (@debug is null)
		set @debug = 0

	if (@debug = 0)
		set nocount on;
		
	--TODO: startTime � endTime ������ ��������� ��� ������� ������� ��������
	declare @startTime int, @endTime int;
	
	select @startTime = value from constants where NAME = 'LogTimeForYandex';
	select @endTime = datediff(s, '1970-01-01', getutcdate());
	
	if (@startTime is null or datediff(s, '1970-01-01', getutcdate()) - @startTime > 15*60)
		select @startTime = datediff(s, '1970-01-01', getutcdate()) - 15*60;
	
	create table #t
	(
		vehicle_id int not null,
		log_time int not null,
		lng numeric(8,5),
		lat numeric(8,5),
		speed int,
		course int,
		ign bit,
		InsertTime datetime,
		primary key clustered  (vehicle_id,	Log_Time)
	);
	
	if @debug = 1
		print 'Select from Log_Time'
	
	insert into #t (vehicle_id, log_time, InsertTime)
		select v.vehicle_id, l.Log_Time, l.InsertTime
			from vehicle v
			cross apply (
				select *
					from Log_Time l with (nolock) 
					where l.Vehicle_ID = v.VEHICLE_ID
					  and l.log_time between @startTime + 1 and @endTime
			) l
			where v.vehicle_kind_id <= 4 --������ ������� �������� �������� ��� ������
			  and v.vehicle_id not in (select fakevehicleid from emulator) --�� ���������
	
	if @debug = 1
		print 'delete #t where InsertTime is null'	
	delete #t where InsertTime is null

	if @debug = 1
		print 'Update from Geo_Log'	
	update #t
		set lng = geo.lng, 
		    lat = geo.lat
	from #t t
	join geo_log geo (nolock) on geo.vehicle_id = t.vehicle_id
	                         and geo.LOG_TIME = t.log_time

	if @debug = 1
		print 'Delete empty positions'	
	delete #t where lat is null or lng is null
	
	/*
	������ ������� �� �����:
	������: 55.91586, 37.361896
			55.567047, 37.855594
	
	���������� �������:
	������� ����� ���� 35.358607,56.99575
	������ ������: 40.293911,54.252788

	�����:
	28.106511, 60.367908
	32.127507, 59.049871
	
	lng=32.93343, lat=58.05780
	
	����� ���� �������� �� ���� ��������� �������-��������� �����
	������� ����� ����
	lat = 39.680135, lng = 44.9707
	������ ������ ����
	lat = 2.161597, lng = 129.521481
	*/
	
	if @debug = 1
		print 'Delete Moscow positions'	
	delete from #t
	where lng between 37.361896 and 37.855594 
		and lat between 55.567047 and 55.91586
		
	
	if @debug = 1
		print 'Delete non-Russia positions'	
	delete from #t
	where lng between 44.9707 and 129.521481
		and lat between 52.161597 and 39.680135
			
	if @debug = 1
		print 'Load speed from GPS_Log'	
	update t
	set speed = isnull(gps.speed,0)
	from #t t
	left join gps_log gps (nolock) on gps.vehicle_id = t.vehicle_id and gps.log_time = t.log_time

	if @debug = 1
		print 'Calculate course part 1'	
	update t
	set course = dbo.CalculateCourseFN(t2.lat, t2.lng, t.lat, t.lng)
	from #t t
	cross apply (
		select top(1) *
			from #t t2 
			where t2.vehicle_id = t.vehicle_id 
			  and t2.log_time < t.log_time
			order by t2.log_time desc) t2
	
	if @debug = 1
		print 'Calculate course part 2'	
	update t
	set course = t2.course
	from #t t
	cross apply (
		select top(1) *
			from #t t2 
			where t2.vehicle_id = t.vehicle_id 
	         and t2.log_time > t.log_time
	        order by t2.log_time asc) t2

	if @debug = 1
		print 'Calculate course part 3'	
	update #t
	set course = 0 
	where course is null;
	
	--��� ��������� �� ����� ������ �������

	if @debug = 1
		print 'Read ignition from v_controller_sensor_log'	
	--��������� �� Controller_Sensor_Log
	create table #ignition(Vehicle_ID int, Log_Time int, Value bit, primary key  clustered (Vehicle_ID, Log_Time));
	insert into #ignition(Vehicle_ID, Log_Time, Value)
		select	t.Vehicle_ID, 
				t.log_time, 
				value = case ignition.value when 0 then 0 else 1 end
			from #t t
			cross apply (
				select top(1) value 
					from v_controller_sensor_log csl with (nolock)
					where   csl.vehicle_id = t.vehicle_id
						and csl.log_time between t.log_time - 900 and t.log_time
						and csl.SensorLegend = 4 --���������
					order by t.Log_Time desc) ignition


	if @debug = 1
		print 'Fill ignition'	
	update t
		set ign = i_last.Value
		from #t t
		cross apply (
			select top(1) i.Value
			from #ignition i 
			where i.Vehicle_ID = t.Vehicle_ID
			  and i.Log_Time <= t.Log_Time
			order by i.Log_Time desc) i_last

	drop table #ignition
	
	if @debug = 1
		print 'Clear vehicles without ignition'	
	delete from #t	where ign is null 
	delete from #t	where ign = 0
	
	if @debug = 1
		print 'Select result'	
	select dbo.getdatefromint(log_time) 'utcTime' ,*
	from #t 
	order by vehicle_id, log_time
	
	select @endTime 'endTime'

	if @debug = 1
		print 'Update statistics'	
	--�������� ���������� �� ���-�� ������������ ������
	insert into YandexTrackesPublishStatistic(vehicle_id, log_time,	amount)
	select t.vehicle_id, max(t.log_time), count(*)
	from #t t
	group by t.vehicle_id
	having not exists (select * from YandexTrackesPublishStatistic 
					where vehicle_id = t.vehicle_id and log_time = max(t.log_time)
					)
	
	drop table #t;
	
end

GO

