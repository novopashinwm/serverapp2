set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'SaveToEventLog' and type='p'))
	drop procedure dbo.SaveToEventLog;
go

create procedure dbo.SaveToEventLog
	--TODO: use table valued parameters instead tuple of varchar(max)
	@vehicle_ids				varchar(max),
	@vehicle_rule_ids			varchar(max),
	@vehiclegroup_rule_ids		varchar(max),
	@log_time_from				varchar(max),
	@log_time_to				varchar(max),
	@reason						varchar(max),
	@debug bit = NULL
as
begin
	
	if isnull(@debug, 0) = 0
		set nocount on

	create table #input(
		 Vehicle_ID				int
		,Vehicle_Rule_ID		int
		,VehicleGroup_Rule_ID	int
		,Log_Time_From			int
		,Log_Time_To			int
		,reason					int
		,Rule_ID				int)

--	create clustered index IX_Input on #input (
--		Vehicle_ID, Vehicle_Rule_ID, VehicleGroup_Rule_ID, reason, Log_Time_From);

	insert into #input
		select	Vehicle_ID.number,
				Vehicle_Rule_ID.number,
				VehicleGroup_Rule_ID.number,
				Log_Time_From.number,
				Log_Time_To.number,
				reason.number,
				case Vehicle_Rule_ID.number 
					when -1 then (select Rule_ID from dbo.VehicleGroup_Rule vr where vr.VehicleGroup_Rule_ID = VehicleGroup_Rule_ID.number)
					else (select Rule_ID from dbo.Vehicle_Rule vr where vr.Vehicle_Rule_ID = Vehicle_Rule_ID.number)
				end
			from dbo.split_int(@vehicle_ids)			Vehicle_ID
			join dbo.split_int(@vehicle_rule_ids)		Vehicle_Rule_ID			on Vehicle_Rule_ID.listpos = Vehicle_ID.listpos
			join dbo.split_int(@vehiclegroup_rule_ids)	VehicleGroup_Rule_ID	on VehicleGroup_Rule_ID.listpos = Vehicle_ID.listpos
			join dbo.split_int(@log_time_from)			Log_Time_From			on Log_Time_From.listpos = Vehicle_ID.listpos
			join dbo.split_int(@log_time_to)			Log_Time_To				on Log_Time_To.listpos = Vehicle_ID.listpos
			join dbo.split_int(@reason)					reason					on reason.listpos = Vehicle_ID.listpos
	
	/* ����� ��������� ���������� ����, �� ������������ �� �� ������� ����� ������� �� �������,
		� � ������ ����������� ��������� � ������, � ����� ������.
		� ����� ������, ������ ������, �������������� � ������ ��������� � ����������� ������ ����� ������.*/

	declare @continue bit
	set @continue = 1

	while @continue = 1
	begin
		set @continue = 0

		update el
			set Log_Time_From = 
					case 
						when r.AggregationType = 'Union'		and i.Log_Time_From  < el.Log_Time_From then i.Log_Time_From
						when r.AggregationType = 'Intersection' and el.Log_Time_From < i.Log_Time_From  then i.Log_Time_From
						else el.Log_Time_From
					end,
				Log_Time_To =
					case 
						when r.AggregationType = 'Union'		and el.Log_Time_To  < i.Log_Time_To then i.Log_Time_To
						when r.AggregationType = 'Intersection' and i.Log_Time_To < el.Log_Time_To  then i.Log_Time_To
						else el.Log_Time_To
					end
			from #input i
			join dbo.[Rule] r on r.Rule_ID = i.Rule_ID
			join dbo.Event_Log el on 
							   el.Vehicle_ID = i.Vehicle_ID
						  and (el.Vehicle_Rule_ID = i.Vehicle_Rule_ID or el.VehicleGroup_Rule_ID = i.VehicleGroup_Rule_ID)
						  and (el.Log_Time_From between  i.Log_Time_From - r.Interval and  i.Log_Time_To + r.Interval or
								i.Log_Time_From between el.Log_Time_From - r.Interval and el.Log_Time_To + r.Interval)
						  and  el.Log_Time_To = i.Log_Time_To
						  and  el.reason = i.reason		
			where 
				i.Log_Time_From <> el.Log_Time_From 
			 or i.Log_Time_To   <> el.Log_Time_To

		if @@rowcount <> 0
			set @continue = 1

		update el
			set Log_Time_From = 
					case 
						when r.AggregationType = 'Union'		and i.Log_Time_From  < el.Log_Time_From then i.Log_Time_From
						when r.AggregationType = 'Intersection' and el.Log_Time_From < i.Log_Time_From  then i.Log_Time_From
						else el.Log_Time_From
					end,
				Log_Time_To =
					case 
						when r.AggregationType = 'Union'		and el.Log_Time_To  < i.Log_Time_To then i.Log_Time_To
						when r.AggregationType = 'Intersection' and i.Log_Time_To < el.Log_Time_To  then i.Log_Time_To
						else el.Log_Time_To
					end
			from #input el
			join dbo.[Rule] r on r.Rule_ID = el.Rule_ID
			join dbo.Event_Log i on 
							   el.Vehicle_ID = i.Vehicle_ID
						  and (el.Vehicle_Rule_ID = i.Vehicle_Rule_ID or el.VehicleGroup_Rule_ID = i.VehicleGroup_Rule_ID)
						  and (el.Log_Time_From between  i.Log_Time_From - r.Interval and  i.Log_Time_To + r.Interval or
								i.Log_Time_From between el.Log_Time_From - r.Interval and el.Log_Time_To + r.Interval)
						  and  el.Log_Time_To = i.Log_Time_To
						  and  el.reason = i.reason
			where 
				i.Log_Time_From <> el.Log_Time_From 
			 or i.Log_Time_To   <> el.Log_Time_To

		if @@rowcount <> 0
			set @continue = 1
	end

	delete i
		from #input i
		join dbo.[Rule] r on r.Rule_ID = i.Rule_ID
		where				
			exists (
			select 1 
				from dbo.Event_Log el
				where  el.Vehicle_ID = i.Vehicle_ID
				  and (el.Vehicle_Rule_ID = i.Vehicle_Rule_ID or el.VehicleGroup_Rule_ID = i.VehicleGroup_Rule_ID)
				  and (el.Log_Time_From between  i.Log_Time_From - r.Interval and  i.Log_Time_To + r.Interval or
					    i.Log_Time_From between el.Log_Time_From - r.Interval and el.Log_Time_To + r.Interval)
				  and  el.Log_Time_To = i.Log_Time_To
				  and  el.reason = i.reason)


	insert into dbo.Event_Log(
		Vehicle_ID				
		,Vehicle_Rule_ID			
		,VehicleGroup_Rule_ID	
		,Log_Time_From			
		,Log_Time_To				
		,reason)
		select distinct
			 i.Vehicle_ID				
			,i.Vehicle_Rule_ID			
			,i.VehicleGroup_Rule_ID	
			,i.Log_Time_From			
			,i.Log_Time_To				
			,i.reason
			from #input i

	drop table #input
end