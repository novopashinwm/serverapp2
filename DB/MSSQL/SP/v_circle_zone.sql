if exists (
	select * from sys.views where name = 'v_circle_zone'
)
	drop view v_circle_zone
go

create view v_circle_zone
as
	select z.Zone_ID, z.Name, z.DESCRIPTION, zp.RADIUS, Lat = v.Y, Lng = v.X
		from GEO_ZONE z
		join GEO_ZONE_PRIMITIVE gzp on gzp.ZONE_ID = z.ZONE_ID
		join ZONE_PRIMITIVE zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
		join ZONE_PRIMITIVE_VERTEX zpv on zpv.PRIMITIVE_ID = gzp.PRIMITIVE_ID
		join MAP_VERTEX v on v.VERTEX_ID = zpv.VERTEX_ID
		where zp.PRIMITIVE_TYPE = 1
