if (exists (select * from sys.objects where object_id = object_id('dbo.ReportFuelCostCalculation') and type = 'p'))
	drop procedure dbo.ReportFuelCostCalculation;
go

create procedure [dbo].[ReportFuelCostCalculation]
(
	@dateRange	dbo.DateTimeRange READONLY,
	@operatorID 	int,
	@vehicleGroupId	int,
	@debug			bit = NULL
)
as

if isnull(@debug, 0) = 0
	set nocount on;

DECLARE @fuelSpendStandardKilometer INT
DECLARE @fuelSpendStandardLiter INT
SELECT @fuelSpendStandardLiter = CAST(VALUE as INT)  FROM dbo.CONSTANTS WHERE name = 'baseFuelSpendStandardLiter'
SELECT @fuelSpendStandardKilometer = CAST(VALUE as INT) FROM dbo.CONSTANTS WHERE name = 'baseFuelSpendStandardKilometer'

declare @vehicle_id table (value int primary key clustered)
if (@vehicleGroupId is null)
	insert into @vehicle_id
		select v.vehicle_id
			from Vehicle v
			where 2 = (select count(*) from v_operator_vehicle_right where vehicle_id=v.VEHICLE_ID and operator_id=@operatorID and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/))
			  and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=v.VEHICLE_ID and ignore.operator_id=@operatorID and ignore.right_id = 120)
			  and v.Vehicle_Kind_ID not in (5,7)
else
	insert into @vehicle_id
		select v.vehicle_id
			from dbo.VehicleGroup_Vehicle vgv
				join dbo.Vehicle v on vgv.VEHICLE_ID=v.VEHICLE_ID
			where vgv.VehicleGroup_ID = @vehicleGroupId
			  and v.Vehicle_Kind_ID not in (5,7)
			  and 2 = (select count(*) from v_operator_vehicle_right where vehicle_id=v.VEHICLE_ID and operator_id=@operatorID and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/))
			  and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=v.VEHICLE_ID and ignore.operator_id=@operatorID and ignore.right_id = 120)

select 
	v.Vehicle_ID,
	'' Garage_Number,
	v.Fuel_Type_ID,
	ft.Name Fuel_Type_Name,

	EngineHoursStandard = standardMH.Value,

	EngineHours = 
		ISNULL(
			Convert(
				numeric(18, 5), (
				select sum(t.EngineHours)
					from (
						select EngineHours = dbo.GetEngineSeconds(dateRange.dateFrom, dateRange.dateTo, vid.Value)
							from @dateRange dateRange
					) t)
				)
			/ 3600.0
		,0),

	--������ � ��
	Distance = 
		ISNULL(
			Convert(
				numeric(18, 5), (
				select sum(t.Distance)
					from (
						select Distance = dbo.CalculateDistanceFN(dateRange.dateFrom, dateRange.dateTo, vid.Value)
							from @dateRange dateRange
					) t)
				)
			/ 1000
		,0),	
	
	-- �������� ������� ������� ����������
	FuelSpendStandardKilometer = 
		CASE WHEN
			ISNULL(v.FuelSpendStandardKilometer,0) = 0 
			THEN @fuelSpendStandardKilometer 
			ELSE v.FuelSpendStandardKilometer 
		END, 
	
	-- �������� ������� ������� ������
	FuelSpendStandardLiter =
		CASE WHEN
			ISNULL(v.FuelSpendStandardLiter,0) = 0 
			THEN @fuelSpendStandardLiter 
			ELSE v.FuelSpendStandardLiter 
		END,
		
	--����������� ��� ������ ������ �������
	FuelSpentStandart = 
		case ISNULL(c.Name,'') when 'India' THEN 
			CASE WHEN
				ISNULL(v.FuelSpendStandardKilometer,0) = 0 
				THEN @fuelSpendStandardKilometer 
				ELSE v.FuelSpendStandardKilometer 
			END * 1.0 / CASE WHEN ISNULL(v.FuelSpendStandardLiter,0) = 0 
				THEN @fuelSpendStandardLiter 
				ELSE v.FuelSpendStandardLiter 
			END  --km / l	
		ELSE 
			CASE WHEN
				ISNULL(v.FuelSpendStandardLiter,0) = 0 
				THEN @fuelSpendStandardLiter 
				ELSE v.FuelSpendStandardLiter 
			END * 1.0 / CASE WHEN
				ISNULL(v.FuelSpendStandardKilometer,0) = 0 
				THEN @fuelSpendStandardKilometer 
				ELSE v.FuelSpendStandardKilometer 
			END * 100 -- l/100km
		END,
	
	ISNULL(c.Name,'') as departmentCountryName
	from @vehicle_id vid
	join dbo.Vehicle v on v.Vehicle_ID = vid.Value
	left join dbo.Fuel_Type ft on ft.ID = v.Fuel_Type_ID
	left join dbo.DEPARTMENT d on d.DEPARTMENT_ID =ISNULL(v.DEPARTMENT,0)
	left join dbo.Country c on c.Country_ID = isnull(d.Country_ID,0)
	outer apply (
		select vr.Value
			from v_vehicle_rule vr
			where vr.Vehicle_ID = vid.Value
			  and vr.RuleNumber = 12) standardMH


