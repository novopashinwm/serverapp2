IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRunLogForVehicleGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRunLogForVehicleGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetRunLogForVehicleGroup]  
(  
 @vehiclegroup_id  int,  
 @from   int,  
 @to    int  
)  
as  

select
	  vgv.vehicle_id
	, prev_log.log_time
	, prev_log.odometer
	from vehiclegroup_vehicle vgv
	cross apply (
		select top(1) sl.Log_Time, sl.Odometer
			from statistic_log sl with (nolock)
			where sl.Vehicle_ID = vgv.Vehicle_Id
			  and sl.Log_Time < @from
			order by sl.Log_Time desc) prev_log
	where	
		vgv.vehiclegroup_id = @vehiclegroup_id
union all
select
	  sl.vehicle_id
	, sl.log_time
	, sl.odometer
	from vehiclegroup_vehicle vgv
	inner join statistic_log sl with (nolock) on sl.vehicle_id = vgv.vehicle_id
											 and sl.Log_Time between @from and @to
	where vgv.vehiclegroup_id = @vehiclegroup_id

GO
