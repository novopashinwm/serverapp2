-----------------------------------------------------------------------------------
/*
--Test
Declare
	@dtfrom			DateTime, 
	@dtto			DateTime,
	@NextInHour		int,
	@time_allow		int,		-- ���������� �������� ���������� ������, ��������� 60 ���.
	@MaxSpeed		int,		-- �������� �������� - ������������ �������� ����� ����� ������� �� ����� ���� > 180 ��/�
	@del			bit			-- 0-������ 1-�������

Set @time_allow = 60 
set @del = 0 
set @MaxSpeed = 180 
set @dtfrom = '2008-03-03 21:00:00' --'2008-03-01 21:00:00'
set @dtTo = '2008-03-04 21:00:00' --'2008-04-01 21:00:00'
set @NextInHour = 24 
--select top 20 * from Monitoree_log order by Log_time DESC  
--update Monitoree_Log set X = 55, Y = 65  where Monitoree_ID = 427 and Log_time = 1204630883 --426 and Log_time = 1204625647 --1204578041
*/
-----------------------------------------------------------------------------------


IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'JobDelBadXY'
        AND		type = 'P')
    DROP PROCEDURE JobDelBadXY
GO

CREATE PROCEDURE [dbo].[JobDelBadXY]
AS
/*
-----------------------------------------------------------------------------------
--���������� ���������
Declare
	@JobDelBadXY_ID	int,
	@dtfrom			DateTime, 
	@dtto			DateTime,
	@NextInHour		int,
	@time_allow		int,		-- ���������� �������� ���������� ������, ��������� 60 ���.
	@MaxSpeed		int,		-- �������� �������� - ������������ �������� ����� ����� ������� �� ����� ���� > 180 ��/�
	@del			bit			-- 0-������ 1-�������

set @JobDelBadXY_ID = isnull((select top 1 Job_DelBadXY_ID from [Job_DelBadXY]), 0)
Set @time_allow = isnull((select time_allow from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), 60)
set @del = isnull((select Del from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), 0)
set @MaxSpeed = isnull((select MaxSpeed from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), 180)
set @dtfrom = isnull((select dtFrom from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), getutcdate() - 2)
set @dtTo = isnull((select dtTo from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), getutcdate())
set @NextInHour = isnull((select [NextInHour] from [Job_DelBadXY] where Job_DelBadXY_ID = @JobDelBadXY_ID), 24)

Declare 
	@time_from_int	int,
	@time_to_int	int

Set @time_from_int = datediff(s, '1970', @dtfrom)
Set @time_to_int = datediff(s, '1970', @dtTo)


-- ��������� ������� ��
create table #MonitoreeID(	
	id int identity primary key, 
	Monitoree_ID	int
	)

-- ��
insert into #MonitoreeID select distinct VEHICLE_ID as Monitoree_ID from VEHICLE --where VEHICLE_ID in (2, 426, 427)  --order by VEHICLE_ID


-- ��������� ������� �� ���� ������
create table #MonitoreeLog(	
	id int identity primary key, 
	Monitoree_ID	int,
	Log_Time		int,
	Log_DateTime	DateTime, 
	X				float,
	Y				float,
	Speed			int,
	Dist			float,
	Time			int,
	SpeedCalc		int,
	Problem			int,
	Del 			int
	)
begin
	CREATE NONCLUSTERED INDEX [IX_ID] ON #MonitoreeLog
	(
		[id] ASC
		--,[LOG_TIME] ASC
	)	
end

-- ��������� ������� �� ������������� ������
create table #MonitoreeLogProblem(	
	--id int identity primary key, 
	Monitoree_ID	int,
	Log_Time		int,
	Log_DateTime	DateTime, 
	X				float,
	Y				float,
	Speed			int,
	Dist			float,
	Time			int,
	SpeedCalc		int,
	Problem			int,
	Del 			int
	)


-- �������� ������� ��� ��������� �� ������������ � ������������� ���������� � #MonitoreeLogProblem
Declare
	@iCount			int, 
	@i				int,
	@iCountLog		int,
	@iLog			int,	
	@iLog_			int,	
	@monitoreeID	int,
	@monitoreeID_	int,
	@log_time		int,
	@log_time_		int,
	@x				float,
	@x_				float,
	@x_k			float,
	@y				float,
	@y_				float,
	@y_k			float,
	@speed			int,
	@dist			float,
	@time			int,
	@speedCalc		int,
	@problem		int


set	@iCount = (select count(*) from #MonitoreeID)
set	@i = 0
while @i < @iCount
begin
	set @i = @i + 1
	set	@monitoreeID = (select Monitoree_ID from #MonitoreeID where id = @i) 

	-- �������� ��� ������� �� ��������
	insert into #MonitoreeLog
	select 	
		Monitoree_ID,
		Log_Time, 
		dateadd(s, Log_Time, '1970'),
		X,
		Y,
		Speed, --0,
		Convert(float, 0),
		0, 
		0,
		0,
		0
	from Monitoree_log (nolock)
	Where 	
		Monitoree_ID = @monitoreeID
		and Log_Time between @time_from_int and @time_to_int 
		and (X < 180 and Y < 180)
		--and speed > 5
	order by 
		Log_Time


	-- ��������� ���������� ������ �� ��
	-- ���� ���� ������������ �������, ���������� �� � ������� ����������
	set	@iCountLog = (select max(id) from #MonitoreeLog)
	set	@iLog = (select min(id) from #MonitoreeLog) 
	set @problem = 0
	while @iLog < @iCountLog
	begin
		--
		set @iLog_ = @iLog
		set @iLog = @iLog + 1

		set @log_time = (select Log_Time from #MonitoreeLog where id = @iLog)
		set	@log_time_ = (select Log_Time from #MonitoreeLog where id = @iLog_)
		
		-- ����� ����� ���������
		set @time = @log_time - @log_time_
		if(@time < 1)
			continue

		set	@x = (select X from #MonitoreeLog where id = @iLog) 			
		set	@y = (select Y from #MonitoreeLog where id = @iLog)
		-- ��������� ������������ ������� �������������� �������
		set	@x_	= (select X from #MonitoreeLog where id = @iLog_)		
		set	@y_	= (select Y from #MonitoreeLog where id = @iLog_)	

		-- ������ ����� �������. �� �������, ���� ���������� ������(����� �� ��������) --� ���� �������� ������� (������ �����)
		Set @dist = SQRT(SQUARE(63166*(@x_-38)-63166*(@x-38)) + SQUARE(111411*(@y_-55.665)-111411*(@y-55.665))) / 1000
		-- �������� �������������
		set @speedCalc = @dist / (Convert(float, @time) / 3600)

		if(@speedCalc > @MaxSpeed) 
			set @problem = @problem + 1 

		--��������� ������
		update #MonitoreeLog Set 
			Dist = @dist, 
			Time = @time, 
			SpeedCalc = @speedCalc,
			Problem = @problem 
		where id = @iLog 
	end

	-- ����� ���� ��� �������� ��������� ������� �������, ������� ����� ��� � ���������� ����������� �������
	-- � ����� ������, ��� ��� �������, ��� ��� �������� �� ���� ������ ���� ����������� ������
	declare @linesCount	int
	declare @iLine		int
	declare @posCount	int
	declare @maxCount	int
	declare @maxLine	int
	set @linesCount = (select max(Problem) from #MonitoreeLog)
	set @maxCount = 0
	set @iLine = 0
	while @iLine <= @linesCount 
	begin
		set @posCount = (select count(*) from #MonitoreeLog where Problem = @iLine)
		if(@posCount > @maxCount)
		begin
			set @maxCount = @posCount
			set @maxLine = @iLine
		end
		set @iLine = @iLine + 1
	end


	-- ����� ���� ��� �������� ��������� ������� �������, ������� ����� ��� � ���������� ����������� �������
	-- �������� ����� 10 � ����� ������, ��� ��� ������, ��� ��� �������� �� ���� ������ ���� ����������� ������ ��� ������� �������
	-- ��� ���� �������������� ������� ������ ���� �� ����� 100 ��������.
	set @iLine = 0
	if(@linesCount > 1 and (select count(*) from #MonitoreeLog where Problem = @maxLine) > 100)
	begin
		while @iLine <= @linesCount 
		begin
			set @iLine = @iLine + 1
			set @posCount = (select count(*) from #MonitoreeLog where Problem = @iLine)
			if(@posCount < 10)
			begin
				update #MonitoreeLog Set Del = 1 where Problem = @iLine
			end
			--select @iLine, @posCount, @maxCount, @maxLine 
		end
	end


	insert into #MonitoreeLogProblem select 
		Monitoree_ID,
		Log_Time,
		Log_DateTime, 
		X,
		Y,
		Speed,
		Dist,
		Time,
		SpeedCalc,
		Problem,
		Del
		from #MonitoreeLog 
	where Del > 0
	
	--Test
	--if(@i = 1) select * into #MonitoreeLog_ from #MonitoreeLog else begin SET IDENTITY_INSERT #MonitoreeLog_ ON insert into #MonitoreeLog_ select * from #MonitoreeLog end
	
	delete from #MonitoreeLog
end

--Test 
--select * from Monitoree_Log ml inner join #MonitoreeLogProblem mlp on ml.Monitoree_ID = mlp.Monitoree_ID and ml.Log_Time = mlp.Log_Time


--��������� ������ ("������" �������) � Monitoree_Log 
begin
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
end

set	@i = 0
while @i < @iCount
begin
	set @i = @i + 1
	set	@monitoreeID = (select Monitoree_ID from #MonitoreeID where id = @i) 
	
	if ((Select count(Monitoree_ID) from #MonitoreeLogProblem where Monitoree_ID = @monitoreeID) < 1)
		continue 

	-- ���� ��������
	--if(@del = 1)
	--	delete from Monitoree_Log 
	--	where 
	--		Monitoree_ID = @monitoreeID 
	--		and Log_Time in (Select Log_Time from #MonitoreeLogProblem where Monitoree_ID = @monitoreeID)
	--else
		update Monitoree_Log --#MonitoreeLog_
			Set X = 3.402823E+38, Y = 3.402823E+38 --X = X * 1, Y = Y * 1 --
		where 
			Monitoree_ID = @monitoreeID 
			and Log_Time in (Select Log_Time from #MonitoreeLogProblem where Monitoree_ID = @monitoreeID)
end 

begin
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
end 

--Test
--select * from #MonitoreeLog_
--select * from #MonitoreeLogProblem



-- ��������� ��������� � ������� Job_DelBadXY ��� ���������� �������
declare @delCount int
set @delCount = (select count(*) from #MonitoreeLogProblem)
if(@NextInHour > 0)
begin
	set @dtFrom = dateadd(s, (@NextInHour * 3600), @dtFrom)
	set @dtTo = dateadd(s, (@NextInHour * 3600), @dtTo)
end
else
begin
	set @dtFrom = @dtTo
	set @dtTo = dateadd(s, (@time_to_int - @time_from_int), @dtFrom)
end
	if((select count(*) from Job_DelBadXY) > 0 and @JobDelBadXY_ID > 0)
		update Job_DelBadXY set lastDelCount = @delCount, dtFrom = @dtFrom, dtTo = @dtTo where Job_DelBadXY_ID = @JobDelBadXY_ID
	else
		insert into [Job_DelBadXY] values (@delCount, @dtFrom, @dtTo, @NextInHour, @time_allow, @MaxSpeed, @del)


drop table #MonitoreeID
drop table #MonitoreeLog
--drop table #MonitoreeLog_
drop table #MonitoreeLogProblem

*/



