if exists (
	select *
		from sys.procedures
		where name = 'ExtractNewDepartment'
)
	drop procedure ExtractNewDepartment
go

create procedure ExtractNewDepartment
(
	  @department_name	nvarchar(255)
	, @operator_ids		Id_Param readonly
	, @vehicle_ids		Id_Param readonly 
	, @vehiclegroup_ids	Id_Param readonly
	, @zone_ids			Id_Param readonly
	, @zonegroup_ids	Id_Param readonly
)
as
begin

	declare @old_department_id int = (
		select distinct department 
			from @vehicle_ids vid
			join Vehicle v on v.vehicle_id = vid.id)
	
	if @old_department_id is null
		return;
	
	declare @department_id int
	
	insert into Department (Name, Description, Country_ID, Type, IsCommercial, Billing_Service_Provider_ID)
		select	@department_name, 
				'Moved from ' + convert(varchar(12), @old_department_id), 
				d.Country_ID, 
				d.Type, 
				d.IsCommercial, 
				d.Billing_Service_Provider_ID
			from Department d
			where d.Department_ID = @old_department_id
			
	set @department_id = @@identity
	
	update vehicle		set department = @department_id where vehicle_id in (select id from @vehicle_ids)
	update vehiclegroup set department_id = @department_id where vehiclegroup_id in (select id from @vehiclegroup_ids)
	update geo_zone		set department_id = @department_id where zone_id in (select id from @zone_ids)
	update zonegroup	set department_id = @department_id where zonegroup_id in (select id from @zonegroup_ids)

	update operator_department 
		set department_id = @department_id
		where operator_id in (select id from @operator_ids)
		
	insert into OperatorGroup_Department
		select ogd.OperatorGroup_ID, @department_id, ogd.right_id, 1
			from OperatorGroup_Department ogd
			where exists (
				select * 
					from OperatorGroup_Department aogd
					where aogd.Department_ID = ogd.Department_ID
					  and aogd.OperatorGroup_ID = ogd.OperatorGroup_ID
					  and aogd.right_id = 2)	
			  and ogd.department_id = @old_department_id 
			  
	insert into OperatorGroup (NAME, COMMENT)
		select @department_name, 'Created in ExtractNewDepartment'
	
	declare @operatorgroup_id int = @@identity

	--��� ����� ������ ����� �� ����� ����������� ����� ��, 
	--��� ���� � �����, � ������� ������� ������������ ������ ������������, �� ������ �����������
	insert into OPERATORGROUP_DEPARTMENT
		select @operatorgroup_id, @department_id, ogd.right_id, 1
			from OPERATORGROUP_DEPARTMENT ogd 
			where ogd.OPERATORGROUP_ID in (
				select ogo.OperatorGroup_ID
					from OPERATORGROUP_OPERATOR ogo
					where ogo.OPERATOR_ID in (select o.id from @operator_ids o))
			  and ogd.DEPARTMENT_ID = @old_department_id
	
	--���������� ������������� � ����� ������
	update OPERATORGROUP_OPERATOR
		set OPERATORGROUP_ID = @operatorgroup_id
		where OPERATOR_ID in (select o.id from @operator_ids o)	

end