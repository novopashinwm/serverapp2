if exists (select * from sys.objects where name = 'SmoothRequired' and type = 'FN')
	drop function dbo.SmoothRequired;
go

/*
	����������� ������� ��������� ��� �������.
	. ������ ��� ��� � ���� �������� < 15
	. ���������� ��������� �� ��� ����������� � ���, ��� ������� ������� ������������� ����� �����������
	. ���� ��������� ��������� � �������� = 0, �� ������ ���������� = 300�,
	  ���� ��������� ��������� � �������� > 0 � < 15, �� ������ ���������� = 200�,
	  ���� ��������� �������� � �������� = 0, �� ������ ���������� = 200�,
	  ���� ��������� �������� � �������� > 0 � < 10, �� ������ ���������� = 100�
*/

create function dbo.SmoothRequired
(
	@vehicle_id int,
	@log_time	int,
	@lat		real,
	@lng		real,
	@speed		int,
	@ignition	bit
) 
returns bit
as 
begin

	if @speed > 20 return 0;
	
	declare @prev_log_time	int, @prev_long	real, @prev_lat	real, @prev_speed int;
	select top 1 @prev_long = gl.Lng, 
				 @prev_lat = gl.Lat, 
				 @prev_log_time = gl.LOG_TIME
		from dbo.geo_log gl (nolock) 
		where gl.vehicle_id = @vehicle_id 
		and   gl.LOG_TIME < @log_time
		order by gl.LOG_TIME desc
		
	if @prev_log_time is null return 0
	
	--������� ���������� ����� ����� � ���������� �������
	declare @dist real;
	select @dist = dbo.GetTwoGeoPointsDistance(@prev_long, @prev_lat, @lng, @lat);
	
	--���������� ����� ����������� ����� 5 ������
	if @dist < 5 return 1

	DECLARE @stopSpeed INT
	DECLARE @stopSpeedNoIgnition INT
	DECLARE @SmoothRadiusMotionWithIgnition INT
	DECLARE @SmoothRadius INT
	DECLARE @SmoothRadiusMotion INT
	DECLARE @SmoothRadiusWithIgnition INT
		
	SELECT @stopSpeed = StopSpeed , 
		@stopSpeedNoIgnition = StopSpeedNoIgnition,
		@SmoothRadiusMotionWithIgnition = SmoothRadiusMotionWithIgnition,
		@SmoothRadius = SmoothRadius,
		@SmoothRadiusMotion = SmoothRadiusMotion,
		@SmoothRadiusWithIgnition = SmoothRadiusWithIgnition
	FROM dbo.VEHICLE v
		JOIN dbo.VEHICLE_KIND vk ON vk.VEHICLE_KIND_ID = v.VEHICLE_KIND_ID
	WHERE v.VEHICLE_ID = @vehicle_id
	 
	--���� �������� ������ 10 ��.� ��� ��� ��������� � �������� < 15 ��.�
	if (@speed < @stopSpeed or 
		@speed < @stopSpeedNoIgnition and @ignition <> 1)
	begin
		--���������� ��� ���� ����� ������������, ������� ����� �������,
		--����� ������� �������� ����������� �� ���������� ����������,
		--�.�. �������� ���������� ����� �� ����� ������ � ��� ����������� ���� ����� �����������
		--TODO: ��������� ������ � ����������������� �������������
		if exists (select 1
			from CONTROLLER c (nolock) 
			join CONTROLLER_TYPE ct (nolock) on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
			join vehicle v (nolock) on v.vehicle_id = c.vehicle_id
			where v.vehicle_id = @vehicle_id and ct.POS_SMOOTH_SUPPORT = 1
			and not (v.vehicle_kind_id = 2 /*��������*/ and @ignition = 1)
			)
		begin
			set @prev_speed = (
				select top(1) Speed 
					from dbo.gps_log gpsl (nolock) 
					where gpsl.vehicle_id = @vehicle_id 
					  and gpsl.log_time <= @prev_log_time
					  order by gpsl.log_time desc)

			if (@prev_log_time is not null and @prev_speed < @stopSpeedNoIgnition)
			begin
				declare @smooth_radius int;
				if (@ignition = 1)
				begin 
					if (@speed = 0 and @prev_speed = 0)
						set @smooth_radius = @SmoothRadiusWithIgnition;
					else
						set @smooth_radius = @SmoothRadiusMotionWithIgnition;
				end
				else
				begin 
					if (@speed = 0 and @prev_speed = 0)
						set @smooth_radius = @SmoothRadius;
					else
						set @smooth_radius = @SmoothRadiusMotion;
				end

				if (@dist < @smooth_radius and 
					not (@speed = 0 and @prev_speed <> 0))--��� ��������� ����������� ��������� ������� � ������� ���������
				begin
					return 1 --����� ����������
				end
			end
		end
	end

	return 0 --�� ����� ����������
end