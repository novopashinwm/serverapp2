if exists (
	select * from sys.views where name = 'v_vehicle_firmware'
)
	drop view v_vehicle_firmware
go

create view v_vehicle_firmware
as
select v.Vehicle_ID, ID = l.Firmware, fw.Name
	from Vehicle v
	outer apply (
		select top(1) l.Firmware
			from GPS_Log l with (nolock)
			where l.Vehicle_ID = v.Vehicle_ID
			  and l.Firmware is not null
			  and l.Firmware <> 0
			order by l.Log_Time desc) l
	left outer join Firmware fw on fw.ID = l.Firmware