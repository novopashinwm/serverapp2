/****** Object:  StoredProcedure [dbo].[GetDataForCalendar]    Script Date: 07/09/2010 12:02:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[GetDataForCalendar]
	@operator_id int = null
AS
begin
	select distinct rg.* 
	from ROUTEGROUP rg
	left join v_operator_route_groups_right rgr on rgr.ROUTEGROUP_ID = rg.ROUTEGROUP_ID
	where @operator_id is null or rgr.operator_id = @operator_id
		
	select distinct r.* 
	from ROUTE r
	left join v_operator_route_right rr on rr.ROUTE_ID = r.ROUTE_ID
	where @operator_id is null or rr.operator_id = @operator_id
	
	select rgrr.* 
	from ROUTEGROUP_ROUTE rgrr
	left join v_operator_route_right rr on rr.ROUTE_ID = rgrr.ROUTE_ID
	left join v_operator_route_groups_right rgr on rgr.ROUTEGROUP_ID = rgrr.ROUTEGROUP_ID
	where @operator_id is null 
	or (rr.operator_id = @operator_id and rgr.operator_id = @operator_id)

	select distinct d.* 
	from DAY_KIND d
	join DK_SET dk on dk.DK_SET_ID = d.DK_SET
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = dk.DEPARTMENT_ID
	where @operator_id is null or dr.operator_id = @operator_id
		
	select distinct dk.* 
	from DK_SET dk
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = dk.DEPARTMENT_ID
	where @operator_id is null or dr.operator_id = @operator_id
	
	select distinct p.* 
	from PERIOD p
	left join DK_SET dks on dks.DK_SET_ID = p.DK_SET
	left join DAY_KIND dk on dk.DAY_KIND_ID = p.DAY_KIND
	left join DK_SET dks2 on dks2.DK_SET_ID = dk.DK_SET
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = dks.DEPARTMENT_ID
	left join v_operator_department_right dr2 on dr2.DEPARTMENT_ID = dks2.DEPARTMENT_ID
	left join v_operator_route_right rr on rr.ROUTE_ID = p.ROUTE
	where @operator_id is null 
	or (
		dr.operator_id = @operator_id 
		or dr2.operator_id = @operator_id
	)
	and (p.ROUTE is null or rr.operator_id = @operator_id)

	select distinct d.* 
	from DEPARTMENT d
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where @operator_id is null or dr.operator_id = @operator_id
end









