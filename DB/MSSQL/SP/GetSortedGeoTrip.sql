/****** Object:  StoredProcedure [dbo].[GetSortedGeoTrip]    Script Date: 07/07/2010 17:11:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetSortedGeoTrip]
@operatorID int = null
as

select GT.*
from GEO_TRIP GT
left join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
left join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
where @operatorID is null 
or gt.CREATOR_OPERATOR_ID = @operatorID
or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
order by 
	case 
		when  PATINDEX('%- %', COMMENTS) > 0
			then
				case
					when ISNUMERIC( REPLACE(RTRIM(LEFT(COMMENTS,PATINDEX('%- %', COMMENTS)-1)),',','-') ) = 1 
						then convert( float, RTRIM(LEFT(COMMENTS,PATINDEX('%- %', COMMENTS)-1)) )
					else '0'
				end
		else '-1'
	end, 
	COMMENTS


