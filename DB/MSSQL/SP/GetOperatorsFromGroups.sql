

IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'GetOperatorsFromGroups' AND type = 'P')
    DROP PROCEDURE GetOperatorsFromGroups
GO


CREATE PROCEDURE [dbo].[GetOperatorsFromGroups]  
	@operator_id int
AS

select o.operator_id
from
	dbo.operatorgroup_operator ogo
	inner join operator o on o.operator_id = ogo.operator_id
where ogo.operatorgroup_id in(
	select og.operatorgroup_id
	from
		dbo.operatorgroup_operator ogo
		inner join dbo.operatorgroup og on og.operatorgroup_id = ogo.operatorgroup_id
	where ogo.operator_id = @operator_id
)


