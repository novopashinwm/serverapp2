if exists (
	select *
		from sys.objects
		where name = 'utc2lt')
	drop function dbo.utc2lt;
go

create function dbo.utc2lt
(
	@s datetime
)
returns int
as
begin
	return datediff(ss, '1970', @s);
end