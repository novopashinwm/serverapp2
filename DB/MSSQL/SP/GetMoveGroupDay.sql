set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMoveGroupDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMoveGroupDay]
GO

Create Procedure [dbo].[GetMoveGroupDay]
	@vehicle int,
	@workingIntervals dbo.Log_Time_Span_Param readonly,
	@debug		bit = null
AS

declare @intervals table(
	rn			int identity primary key clustered, 
	[from]		int, 
	[to]		int,
	Dist		float,
	max_speed	int,
	motion_time int
);

insert into @intervals ([from], [to])  
select Log_Time_From, Log_Time_To from @workingIntervals

--Первый набор данных для отчета
select * from VEHICLE where VEHICLE_ID = @vehicle

declare @max_valid_speed tinyint;
set @max_valid_speed = 180;

--В отчете необходимо отображать также дни отсутствия данных

declare @fromInt int, @toInt int;
declare @dist real, @max_speed tinyint;
declare @i int

create table #actionIntervals(rowid int identity(1, 1) not null, vehicle_id int, log_time int, odometer bigint, ign int)

insert into #actionIntervals  (vehicle_id, log_time, odometer, ign)
select Vehicle_ID, Log_Time, Odometer, IgnitionOnTime
	from Statistic_Log sl with (nolock)
	join @workingIntervals wi
		on sl.Log_Time between wi.Log_Time_From and wi.Log_Time_To
	where sl.vehicle_id = @vehicle
	order by log_time asc

update @intervals
	set dist		= dbo.CalculateDistanceFN([from], [to], @vehicle)/1000,
		max_speed	= (select isnull(max(speed),0) 
						from dbo.GPS_Log gpsl (nolock)  
						join dbo.Geo_Log geol (nolock) on geol.Vehicle_ID = @vehicle and geol.Log_Time = gpsl.Log_Time
						where 
							gpsl.Vehicle_ID = @vehicle 
						and gpsl.LOG_TIME between [from] and [to]
						and -181 < Lng and Lng < 181
						and -91 < Lat  and Lat < 90
						and isnull(SATELLITES,5) >= 3
						and speed < @max_valid_speed)
update i
	set motion_time  = (select sum(a1.log_time - a2.log_time)
							from #actionIntervals a1
							join #actionIntervals a2 on a2.rowid = a1.rowid - 1
						where a1.odometer != a2.odometer
							and a1.log_time between i.[from] and i.[to])
	from @intervals i
	
	
if @debug = 1
	select [from], [to], motion_time / 3600, (motion_time % 3600) / 60 from @intervals order by rn;
							

select  [from], 
		[to], 
		Dist, 
		max_speed, 
		avg_speed = convert(int, Dist / (motion_time / 3600.0)) 
from @intervals order by rn;





