if exists (select * from sys.procedures where name='SearchOperators')
	drop procedure SearchOperators
go
create procedure SearchOperators
(
	@searchText nvarchar(100),
	@searchByPhone bit = 0,
	@searchByImei bit = 0
)
as 
	declare @max_count int = 25

	create table #ids 
	(
		OperatorId int not null,
		Num smallint not null,
		Value nvarchar(80) not null
	)

	insert into #ids 
		select top(@max_count) o.OPERATOR_ID, 0 Num, LOGIN Value
			from OPERATOR o
			where LOGIN = @searchText
			order by o.OPERATOR_ID desc

	insert into #ids 
		select top(@max_count) o.OPERATOR_ID, 1 Num, LOGIN Value
			from OPERATOR o
			where NAME = @searchText
			  and LOGIN is not null
			order by o.OPERATOR_ID desc

	declare @searchName nvarchar(81) = dbo.EscapeLikeTemplate(@searchText, '\') + '%'

	-- Login
	insert into #ids
		select top(@max_count) o.OPERATOR_ID, 2 Num, LOGIN Value
			from OPERATOR o
			where LOGIN like @searchName escape '\'
			order by o.OPERATOR_ID desc

	-- Name
	insert into #ids
		select  top(@max_count) o.OPERATOR_ID, 3 Num, NAME Value
			from OPERATOR o
			where NAME like @searchName escape '\'
			order by o.OPERATOR_ID desc

	if @searchByImei = 1
	begin
		declare @searchDeviceId varbinary(32) = convert(varbinary, convert(varchar(32), @searchText))

		insert into #ids
		-- 100% (IMEI)
		select a.Operator_ID, 4 Num, @searchText Value
		from Asid a 
			join MLP_Controller mc on mc.Asid_ID = a.ID
			join CONTROLLER_INFO ci on ci.CONTROLLER_ID = mc.Controller_ID
		where ci.DEVICE_ID = @searchDeviceId and a.Operator_ID is not null

		union all
		-- 75% (IMEI)
		select r.Operator_ID, 5 Num, @searchText Value
		from CONTROLLER_INFO ci
			join CONTROLLER c on ci.CONTROLLER_ID = c.CONTROLLER_ID
			join v_operator_vehicle_right r on r.vehicle_id = c.VEHICLE_ID and r.right_id = 102 
		where ci.DEVICE_ID = @searchDeviceId

		union all
		-- 100% (IMEI)
		select a.Operator_ID, 6 Num, @searchText Value
		from Asid a
		where DeviceId = @searchDeviceId and a.Operator_ID is not null
	end

	if @searchByPhone = 1
	begin
		select ID, Value
			into #cids
		from Contact 
		where Value like @searchName escape '\'
		  and Type = 2

		-- 100% (����� �������� Confirmed)
		insert into #ids
			select top(@max_count) oc.Operator_ID, 8 Num, cid.Value
				from #cids cid
				join Operator_Contact oc on oc.Contact_ID = cid.ID and oc.Confirmed = 1
				order by oc.Operator_ID desc

		drop table #cids
	end

	create nonclustered index IX_IDS on #ids(OperatorId, Num)
	select top 25 o.OPERATOR_ID
		,o.Culture_ID
		,'' EMAIL
		,o.GUID
		,o.IsTimeZoneInfoManual
		,o.LOGIN
		,o.NAME
		,o.OneTimePassword
		,'' [PASSWORD]
		,o.TimeZone_ID
		,o.TimeZoneInfo
		,o.MandatoryPhone
		,o.MandatoryEmail
	from Operator o
		join
		(
			select OperatorId, min(Num)
			from #ids
			group by OperatorId
		) t(OperatorId, OrderNum) on o.OPERATOR_ID = t.OperatorId
	order by OrderNum asc, o.OPERATOR_ID desc

	drop table #ids
go