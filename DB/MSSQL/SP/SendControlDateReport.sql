if exists (select * from sys.procedures where name = 'SendControlDateReport')
	drop procedure SendControlDateReport
go

  
create procedure SendControlDateReport  -- SendControlDateReport  1,1
 @daysForward int = 1,
 @debug bit = 0  
as  
begin  
  
	if @debug = 0
		set nocount on
  
	declare @date datetime = convert(date, getutcdate()+@daysForward)  

	create table #report (Vehicle_ID int, Type_ID int, ControlDate date)

	insert into #report 
		SELECT vcd.Vehicle_ID, vcd.Type_ID, convert(date, vcd.Value)
			   FROM VehicleControlDate vcd 
			   where vcd.Value < @date

	if @@ROWCOUNT = 0
		return;

	create table #email (id int identity, operator_id int, email nvarchar(300))
	insert into #email (operator_id, email)
		select distinct e.operator_id, e.email
			from #report r
			join v_operator_vehicle_right ovr on ovr.vehicle_id = r.Vehicle_ID and ovr.right_id = 102
			join email e on e.operator_id = ovr.operator_id
			where e.Confirmed = 1
			
	declare @subject nvarchar(max)

	declare @i int
	
	while (1=1)
	begin
		
		set @i = (select MIN(id) from #email where @i is null or id > @i)
		if @i is null
			break;	
	
		declare @operator_id int	= (select operator_id from #email where id = @i)
		declare @email nvarchar(300)= (select       email from #email where id = @i)
		
		declare @tableHTML nvarchar(max) =   
		  N'<H1>Expiration date report</H1>' +  
		  N'<table border="1">' +  
		  N'<tr><th>Date type</th><th>Vehicle name</th><th>Vehicle public number</th><th>Driver name</th><th>Date</th></tr>' +  
		  CAST ( ( SELECT   
			  td = case r.Type_ID  
			   when 0 then 'Insurance expiration'  
			   when 1 then 'Lisence expiration'  
			   when 2 then 'Permit expiration'  
			  end,       ''
			  , td = v.Garage_Number, ''
			  , td = v.Public_Number, ''
			  , td = isnull(d.Names, N''), ''
			  , td = convert(nvarchar(32), r.ControlDate), ''  
			   FROM #report r
			   join Vehicle v on v.VEHICLE_ID = r.Vehicle_ID
			   outer apply (
					select names = case LEN(t.names) when 0 then N'' else SUBSTRING(t.names, 1, len(t.names)-1) end
					from (
						select names = (
							select isnull(d.SHORT_NAME + ' ', '') + d.Name as [text()]
								from Driver_Vehicle vd
								join Driver d on d.Driver_ID = vd.Driver_ID
								where vd.Vehicle = v.Vehicle_ID
								for xml path ('')
						)
					) t
				) d			   
			   order by 1, 2  
			   FOR XML PATH('tr')) AS NVARCHAR(MAX) ) +   
		  N'</tr></table>'  		
	
		set @subject = 'Expiration date report on ' + convert(nvarchar(64), convert(date, @date))
	
		 if (isnull(@debug, 0) = 0)   
		 begin  
		  exec msdb.dbo.sp_send_dbmail @recipients = @email
		  , @subject = @subject
		  , @body = @tableHTML
		  , @body_format = 'HTML'
		 end  
		 else  
		 begin  
		  select @email, @subject, @tableHTML    
		 end  

	end
  
end