/****** Object:  StoredProcedure [dbo].[ActiveMotorcadeNamesByDateGet]    Script Date: 07/14/2010 17:07:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
--=°ЄЇ°кьЇ-ь: бЎvЎкьЇ-ь Ї°Є·°Ї-- °·╖ЎьЎvЎЇЇ ЁЎ Ўь°Є°ЇЇЎ-  °╖ь.
--Tvь-░°б╖№и ╖ЎvЎьЎ ╖ь °·╖ЎьЎvЎЇЇv, Ў ьЎ╖Ў░v№ №Ў╖и ьv Ї° Ў -Ї °·╖ЎцЎь-vЎ Є°·ь ьЇ бT.
--L·╖Ў░: Lvьь№ь- б. +v-ЇЎ·.
--------------------------------------------------------------------------------------------
--DDL
ALTER procedure [dbo].[ActiveMotorcadeNamesByDateGet]
	@ReportDate	datetime = null	,	---°╖°, Ї° ьЎ╖Ў░Ўб ╖░ььЎь╖№и ЁЎvЎк-╖Ў №Ё-№Ўь °·╖ЎьЎvЎЇЇ
	@operator_id int = null
as

--------------------------------------------------------------------------------------------
--DML
--б°--╖° Ў╖ ЁЎ№╖ЎцЎ Ёь░ь °ЇЇЎцЎ ЄЇ°кьЇ-и  °╖v. 
---Ў░Ў-Ў ьv, к╖Ўьv Ї° п╖Ў╖ №vЎк°- ЁЎ Ї-ц°v°№Ў Ўё- °ьц°и Ў--ьь°, ЇЎ Їь ░ь°v-ЄЎ·°Ї Ёь░ь№·°╖ ьь · ьЎ ь ьv-ьЇ╖°.
if @ReportDate is null set @ReportDate = getutcdate()

select 	distinct 	VG.[VEHICLEGROUP_ID]	'VEHICLEGROUP_ID',	-- ID °·╖ЎьЎvЎЇЇv;
			VG.[NAME]		'VehicleGroupName'	-- Ї°Є·°Ї-ь °·╖ЎьЎvЎЇЇv.

from dbo.WAYBILL_HEADER as WBH
inner join dbo.VEHICLE as VH
	on VH.[VEHICLE_ID] = WBH.[VEHICLE_ID]
inner join dbo.VEHICLEGROUP_VEHICLE as VGV
	on VGV.[VEHICLE_ID] = VH.[VEHICLE_ID]
inner join dbo.VEHICLEGROUP as VG
	on VG.[VEHICLEGROUP_ID] = VGV.[VEHICLEGROUP_ID]
left join v_operator_vehicle_groups_right vgr on vgr.vehiclegroup_id = vg.vehiclegroup_id
where 	WBH.[WAYBILL_DATE] > @ReportDate - 1		--Tvь-░°б╖№и ╖ЎvЎьЎ бT Ї° Ўь°Є°ЇЇЎб  °╖Ў
	and WBH.[WAYBILL_DATE] <= @ReportDate 
	and WBH.[CANCELLED] = 0	-- № №Ў№╖ЎиЇ-ьц "Їь °ЇЇЎv-░Ў·°Ї"
	and (@operator_id is null or vgr.operator_id = @operator_id)

--------------------------------------------------------------------------------------------





