if exists (
	select *
		from sys.objects
		where name = 'lt2msk'
)
	drop function lt2msk
go

create function dbo.lt2msk
(
	@log_time int
)
returns datetime
as
begin

	return dbo.utc2msk(dbo.GetDateFromInt(@log_Time))

end