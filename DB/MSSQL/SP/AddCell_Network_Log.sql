﻿IF (OBJECT_ID(N'[dbo].[AddCell_Network_Log]') IS NOT NULL)
	DROP PROCEDURE [dbo].[AddCell_Network_Log]
GO

CREATE PROCEDURE [dbo].[AddCell_Network_Log]
(
	@Vehicle_ID     int,
	@Log_Time       int,
	@Number         tinyint,
	@CountryCode    varchar(4) = '',
	@NetworkCode    varchar(8),
	@Cell_ID        int,
	@LAC            int,
	@SAC            int = NULL,
	@ECI            int = NULL,
	@SignalStrength smallint
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @rows          int = 0;
	DECLARE @rows_affected int = 0;

	--Определение @Country_ID
	DECLARE @Country_Code_ID smallint = (SELECT TOP(1) [ID] FROM [dbo].[Country_Code] WITH (NOLOCK) WHERE [Value] = @CountryCode)
	IF (@Country_Code_ID IS NULL)
	BEGIN
		INSERT INTO [dbo].[Country_Code] ([Value])
			SELECT @CountryCode
				WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Country_Code] WITH (XLOCK, SERIALIZABLE) WHERE [Value] = @CountryCode)
		SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

		SET @Country_Code_ID = (SELECT TOP(1) [ID] FROM [dbo].[Country_Code] WHERE [Value] = @CountryCode)
	END;

	--Определение @Network_ID
	DECLARE @Network_Code_ID smallint = (SELECT TOP(1) [ID] FROM [dbo].[Network_Code] WITH (NOLOCK) WHERE [Value] = @NetworkCode)
	IF (@Network_Code_ID IS NULL)
	BEGIN
		INSERT INTO [dbo].[Network_Code] ([Value])
			SELECT @NetworkCode
				WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Network_Code] WITH (XLOCK, SERIALIZABLE) WHERE [Value] = @NetworkCode)
		SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

		SET @Network_Code_ID = (SELECT TOP(1) [ID] FROM [dbo].[Network_Code] WHERE [Value] = @NetworkCode)
	END;
	MERGE [dbo].[Cell_Network_Log] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			(@Vehicle_ID, @Log_Time, @Number, @Country_Code_ID, @Network_Code_ID, @Cell_ID, @LAC, @SignalStrength, @SAC, @ECI)
	) AS SRC ([Vehicle_ID], [Log_Time], [Number], [Country_Code_ID], [Network_Code_ID], [Cell_ID], [LAC], [SignalStrength], [SAC], [ECI])
		ON  DST.[Vehicle_ID] = SRC.[Vehicle_ID]
		AND DST.[Log_Time]   = SRC.[Log_Time]
		AND DST.[Number]     = SRC.[Number]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[Country_Code_ID] = SRC.[Country_Code_ID],
				DST.[Network_Code_ID] = SRC.[Network_Code_ID],
				DST.[Cell_ID]         = SRC.[Cell_ID],
				DST.[LAC]             = SRC.[LAC],
				DST.[ECI]             = SRC.[ECI],
				DST.[SAC]             = SRC.[SAC],
				DST.[SignalStrength]  = SRC.[SignalStrength]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Vehicle_ID],     [Log_Time],     [Number],     [Country_Code_ID],     [Network_Code_ID],     [Cell_ID],     [LAC],     [SignalStrength],     [SAC],     [ECI])
		VALUES (SRC.[Vehicle_ID], SRC.[Log_Time], SRC.[Number], SRC.[Country_Code_ID], SRC.[Network_Code_ID], SRC.[Cell_ID], SRC.[LAC], SRC.[SignalStrength], SRC.[SAC], SRC.[ECI]);
	SELECT @rows = @@ROWCOUNT, @rows_affected = @rows_affected + @rows

	SELECT Rows_Affected = @rows_affected
END
GO