SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER Procedure [dbo].[GetZone]
	@operator_id	int,
	@vertexGet		bit,
	@zone_id		int,
	@department_id	int = null,
	@includeIgnored int = null,
	@loadSingle		bit = 0
AS

set nocount on;

if @includeIgnored is null
	set @includeIgnored = 0

select distinct  
	gz.ZONE_ID,
	gz.[NAME],
	zt.ZONE_TYPE_ID,
	zt.[NAME] as TYPE_NAME,
	gz.COLOR,
	gz.DESCRIPTION,
	case when exists (select 1 from v_operator_zone_right (nolock) where right_id = 19 and ZONE_ID = OZ.ZONE_ID and operator_id = @operator_id)
		then 1
		else 0
	end Editable
from dbo.v_operator_zone_right (nolock) as OZ 
	join GEO_ZONE as gz on gz.ZONE_ID = OZ.ZONE_ID
	left join GEO_ZONE_PRIMITIVE gzp on gzp.ZONE_ID = gz.ZONE_ID
	left join ZONE_PRIMITIVE zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
	left join ZONE_TYPE zt on zt.ZONE_TYPE_ID = zp.PRIMITIVE_TYPE
where 
	OZ.OPERATOR_ID = @operator_id
	and (@loadSingle = 0 or gz.Zone_ID=@zone_id)
	and OZ.right_id = 105 /*ZoneAccess*/
	and (@department_id is null or @department_id is not null and gz.Department_ID = @department_id)	
	and (@includeIgnored = 1 or 
			not exists (
				select * 
					from OPERATOR_ZONE ignore
					where ignore.OPERATOR_ID = @operator_id 
					  and ignore.RIGHT_ID = 120 /*Ignore*/
					  and ignore.ZONE_ID = gz.ZONE_ID
					  and ignore.ALLOWED = 1))
order by 
	gz.[NAME]

if(@vertexGet = 1)
	if(@zone_id > 0)
		select   
			gz.ZONE_ID,
			zpv.[ORDER],
			mv.X,
			mv.Y,
			zp.RADIUS 
		from dbo.v_operator_zone_right (nolock) as OZ 
			join GEO_ZONE as gz on gz.ZONE_ID = OZ.ZONE_ID
			inner join GEO_ZONE_PRIMITIVE gzp on gzp.ZONE_ID = gz.ZONE_ID
			inner join ZONE_PRIMITIVE zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
			inner join ZONE_PRIMITIVE_VERTEX zpv on zpv.PRIMITIVE_ID = zp.PRIMITIVE_ID
			inner join MAP_VERTEX mv on mv.VERTEX_ID = zpv.VERTEX_ID
		where
			gz.ZONE_ID = @zone_id 
			and	OZ.OPERATOR_ID = @operator_id
			and OZ.right_id = 105 /*ZoneAccess*/
			and (@department_id is null or @department_id is not null and gz.Department_ID = @department_id)	
			and (@includeIgnored = 1 or 
					not exists (
						select * 
							from OPERATOR_ZONE ignore
							where ignore.OPERATOR_ID = @operator_id 
							  and ignore.RIGHT_ID = 120 /*Ignore*/
							  and ignore.ZONE_ID = gz.ZONE_ID
							  and ignore.ALLOWED = 1))
		order by 
			gz.ZONE_ID, zpv.[ORDER]
	else
	begin
		select   
			gz.ZONE_ID,
			zpv.[ORDER],
			mv.X,
			mv.Y,
			zp.RADIUS
		from dbo.v_operator_zone_right (nolock) as OZ 
			join GEO_ZONE as gz on gz.ZONE_ID = OZ.ZONE_ID
			inner join GEO_ZONE_PRIMITIVE gzp on gzp.ZONE_ID = gz.ZONE_ID
			inner join ZONE_PRIMITIVE zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
			inner join ZONE_PRIMITIVE_VERTEX zpv on zpv.PRIMITIVE_ID = zp.PRIMITIVE_ID
			inner join MAP_VERTEX mv on mv.VERTEX_ID = zpv.VERTEX_ID
		where
			OZ.OPERATOR_ID = @operator_id
		and OZ.right_id = 105 /*ZoneAccess*/
		and (@department_id is null or @department_id is not null and gz.Department_ID = @department_id)	
		and (@includeIgnored = 1 or 
				not exists (
					select * 
						from OPERATOR_ZONE ignore
						where ignore.OPERATOR_ID = @operator_id 
						  and ignore.RIGHT_ID = 120 /*Ignore*/
						  and ignore.ZONE_ID = gz.ZONE_ID
						  and ignore.ALLOWED = 1))
		order by 
			gz.ZONE_ID, zpv.[ORDER]
	end



