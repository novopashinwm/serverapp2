if exists (
	select * from sys.objects where name = 'GetServices'
)
	drop procedure GetServices
go

create procedure GetServices
(
	@asidId int
) 
as 
begin

set nocount on;

select 
   id = bs.ID
 , name = bst.Name
 , type = bst.Service_Type_Category
 , startDate = bs.StartDate
 , endDate = bs.EndDate
 , isTrial = CONVERT(bit, case when bst.PeriodDays is not null then 1 else 0 end)
 , isActivated = bs.Activated
 from Billing_Service_Type bst
 join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
 where bs.Asid_ID = @asidId

end