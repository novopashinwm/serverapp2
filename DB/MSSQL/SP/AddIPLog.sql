if exists (
	select *
		from sys.procedures 
		where name = 'AddIPLog'
)
	drop procedure AddIPLog;
go

create procedure AddIPLog
(
	@vehicle_id int,
	@ip varchar(40),
	@protocol varchar(80)
)
as
begin

	set nocount on
	
	if (@ip is null)
		set @ip = ''
	if(@protocol is null)
		set @protocol = ''
	
	declare @now datetime = getutcdate()
	
	insert into IP_Log (Vehicle_ID, IP, Protocol, FirstTime, LastTime)
		select @vehicle_id, @ip, @protocol, @now, @now
		where not exists(select 1 
							from IP_Log with (XLOCK, SERIALIZABLE) 
							where Vehicle_ID = @vehicle_id);

	if @@ROWCOUNT = 0
	begin
	
		update l
			set IP = @ip,
				Protocol = @protocol,
			    FirstTime = case l.IP when @ip then l.FirstTime else @now end,
			    LastTime = @now
			from IP_Log l with (XLOCK, SERIALIZABLE) 
			where l.Vehicle_ID = @vehicle_id	
	
	end

end