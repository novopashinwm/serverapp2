IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'H_InsertOperatorgroupRoutegroup'
        AND 	  type = 'P')
    DROP PROCEDURE H_InsertOperatorgroupRoutegroup
GO
CREATE PROCEDURE dbo.H_InsertOperatorgroupRoutegroup
    @TRAIL_ID int, 
    @OPERATORGROUP_ID int, 
    @ROUTEGROUP_ID int, 
    @ALLOWED bit, 
    @RIGHT_ID int
AS
BEGIN
    insert into [dbo].[OPERATORGROUP_ROUTEGROUP] ([OPERATORGROUP_ID], 
        [ROUTEGROUP_ID], 
        [ALLOWED], 
        [RIGHT_ID]
    ) VALUES (@OPERATORGROUP_ID,@ROUTEGROUP_ID,@ALLOWED,@RIGHT_ID)
    
    insert into [dbo].[H_OPERATORGROUP_ROUTEGROUP] (TRAIL_ID, ACTION, ACTUAL_TIME, [OPERATORGROUP_ID], 
        [ROUTEGROUP_ID], 
        [ALLOWED], 
        [RIGHT_ID]
    ) VALUES (@TRAIL_ID, 'INSERT', GETUTCDATE(), @OPERATORGROUP_ID, 
        @ROUTEGROUP_ID, 
        @ALLOWED, 
        @RIGHT_ID
    )
END


