if exists (select * from sys.procedures where name = 'GetMessageHistoryUpdates')
	drop procedure GetMessageHistoryUpdates
go
create procedure GetMessageHistoryUpdates(
	@fromHistoryId int, 
	@toHistoryId int = null,
	@contactIds dbo.Id_Param readonly
) as
select 
-- ����� ����������
	act.ACTUAL_TIME,
	act.MESSAGE_ID,
	act.ACTION,
	act.ID,
-- �������� ��� ��������� �� �������

	-- ��������
	--act.TEMPLATE_ID ActualTEMPLATE_ID,
	--act.SOURCE_ID ActualSOURCE_ID,
	--act.SEVERITY_ID ActualSEVERITY_ID,
	--act.POSITION_ID ActualPOSITION_ID,
	--act.SUBJECT ActualSUBJECT,
	--act.BODY ActualBODY,
	--act.STATUS ActualSTATUS,
	--act.TIME ActualTIME,
	--act.DESTINATION_ID ActualDESTINATION_ID,
	--act.TYPE ActualTYPE,
	--act.[GROUP] ActualGROUP,
	--act.SystemId ActualSystemId,
	--act.SourceType_ID ActualSourceType_ID,
	--act.DestinationType_ID ActualDestinationType_ID,
	--act.ErrorCount ActualErrorCount,
	--act.ProcessingResult ActualProcessingResult,
	--act.MA_ID ActualMA_ID,
	--act.Owner_Operator_ID ActualOwner_Operator_ID,
	--act.Created ActualCreated,

	-- ���������� ��������
	prv.TEMPLATE_ID PreviousTEMPLATE_ID,
	prv.SOURCE_ID PreviousSOURCE_ID,
	prv.SEVERITY_ID PreviousSEVERITY_ID,
	prv.POSITION_ID PreviousPOSITION_ID,
	prv.SUBJECT PreviousSUBJECT,
	prv.BODY PreviousBODY,
	prv.STATUS PreviousSTATUS,
	prv.TIME PreviousTIME,
	prv.DESTINATION_ID PreviousDESTINATION_ID,
	prv.TYPE PreviousTYPE,
	prv.[GROUP] PreviousGROUP,
	prv.SystemId PreviousSystemId,
	prv.SourceType_ID PreviousSourceType_ID,
	prv.DestinationType_ID PreviousDestinationType_ID,
	prv.ErrorCount PreviousErrorCount,
	prv.ProcessingResult PreviousProcessingResult,
	prv.MA_ID PreviousMA_ID,
	prv.Owner_Operator_ID PreviousOwner_Operator_ID,
	prv.Created PreviousCreated,

	case when (act.[TEMPLATE_ID] = prv.[TEMPLATE_ID]) or (act.[TEMPLATE_ID] is null and prv.[TEMPLATE_ID] is null) then convert(bit,0) else convert(bit,1) end [TEMPLATE_ID],
	case when (act.[SOURCE_ID] = prv.[SOURCE_ID]) or (act.[SOURCE_ID] is null and prv.[SOURCE_ID] is null) then convert(bit,0) else convert(bit,1) end [SOURCE_ID],
	case when (act.[SEVERITY_ID] = prv.[SEVERITY_ID]) or (act.[SEVERITY_ID] is null and prv.[SEVERITY_ID] is null) then convert(bit,0) else convert(bit,1) end [SEVERITY_ID],
	case when (act.[POSITION_ID] = prv.[POSITION_ID]) or (act.[POSITION_ID] is null and prv.[POSITION_ID] is null) then convert(bit,0) else convert(bit,1) end [POSITION_ID],
	case when (act.[SUBJECT] = prv.[SUBJECT]) or (act.[SUBJECT] is null and prv.[SUBJECT] is null) then convert(bit,0) else convert(bit,1) end [SUBJECT],
	case when (act.[BODY] = prv.[BODY]) or (act.[BODY] is null and prv.[BODY] is null) then convert(bit,0) else convert(bit,1) end [BODY],
	case when (act.[STATUS] = prv.[STATUS]) or (act.[STATUS] is null and prv.[STATUS] is null) then convert(bit,0) else convert(bit,1) end [STATUS],
	case when (act.[TIME] = prv.[TIME]) or (act.[TIME] is null and prv.[TIME] is null) then convert(bit,0) else convert(bit,1) end [TIME],
	case when (act.[DESTINATION_ID] = prv.[DESTINATION_ID]) or (act.[DESTINATION_ID] is null and prv.[DESTINATION_ID] is null) then convert(bit,0) else convert(bit,1) end [DESTINATION_ID],
	case when (act.[TYPE] = prv.[TYPE]) or (act.[TYPE] is null and prv.[TYPE] is null) then convert(bit,0) else convert(bit,1) end [TYPE],
	case when (act.[GROUP] = prv.[GROUP]) or (act.[GROUP] is null and prv.[GROUP] is null) then convert(bit,0) else convert(bit,1) end [GROUP],
	case when (act.[SystemId] = prv.[SystemId]) or (act.[SystemId] is null and prv.[SystemId] is null) then convert(bit,0) else convert(bit,1) end [SystemId],
	case when (act.[SourceType_ID] = prv.[SourceType_ID]) or (act.[SourceType_ID] is null and prv.[SourceType_ID] is null) then convert(bit,0) else convert(bit,1) end [SourceType_ID],
	case when (act.[DestinationType_ID] = prv.[DestinationType_ID]) or (act.[DestinationType_ID] is null and prv.[DestinationType_ID] is null) then convert(bit,0) else convert(bit,1) end [DestinationType_ID],
	case when (act.[ErrorCount] = prv.[ErrorCount]) or (act.[ErrorCount] is null and prv.[ErrorCount] is null) then convert(bit,0) else convert(bit,1) end [ErrorCount],
	case when (act.[ProcessingResult] = prv.[ProcessingResult]) or (act.[ProcessingResult] is null and prv.[ProcessingResult] is null) then convert(bit,0) else convert(bit,1) end [ProcessingResult],
	case when (act.[MA_ID] = prv.[MA_ID]) or (act.[MA_ID] is null and prv.[MA_ID] is null) then convert(bit,0) else convert(bit,1) end [MA_ID],
	case when (act.[Owner_Operator_ID] = prv.[Owner_Operator_ID]) or (act.[Owner_Operator_ID] is null and prv.[Owner_Operator_ID] is null) then convert(bit,0) else convert(bit,1) end [Owner_Operator_ID],
	case when (act.[Created] = prv.[Created]) or (act.[Created] is null and prv.[Created] is null) then convert(bit,0) else convert(bit,1) end [Created]

from H_MESSAGE act
	join Message_Contact mc on act.MESSAGE_ID = mc.Message_ID
	outer apply (
		select top 1 hm.*
		from H_MESSAGE hm
		where hm.ID < act.ID and hm.MESSAGE_ID = act.MESSAGE_ID
		order by hm.ID desc
	) prv
where act.ID > @fromHistoryId and (@toHistoryId is null or act.ID <= @toHistoryId) and mc.Contact_ID in (select Id from @contactIds)
go