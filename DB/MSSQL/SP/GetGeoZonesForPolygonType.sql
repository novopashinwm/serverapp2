if not exists (select * from sys.table_types where name = 'Vehicle_Zone_Position_Param')
	create type Vehicle_Zone_Position_Param as table 
	(
		vehicle_id int, 
		zone_id int,
		log_time int,
		lat float,
		lng float
	)
go
if exists (select * from sys.procedures where name = 'GetGeoZonesForPolygonType')
	drop procedure GetGeoZonesForPolygonType
go
create procedure GetGeoZonesForPolygonType(
	@zoneIntersection Vehicle_Zone_Position_Param readonly
) 
as
begin
	select Zone_ID, Lat1, Lng1, Lat2, Lng2, Primitive_ID 
		into #zone_primitive_edge
	from dbo.v_zone_primitive_edge p where p.Zone_ID in (select distinct zone_id from @zoneIntersection)

	create clustered index IX_Zone_Primitive_Edge on #zone_primitive_edge(Zone_ID, Primitive_ID)

	select z.Zone_ID, z.Vehicle_ID, z.Log_Time
		from @zoneIntersection z
		where (select count(1)
					from #zone_primitive_edge p
					where p.Zone_ID = z.zone_id
						and ((p.Lat1 <= z.Lat and z.Lat < p.Lat2) or (p.Lat2 <= z.Lat and z.Lat < p.Lat1))
						and ((p.Lat2 - p.Lat1) > 0 and (z.Lng - p.Lng1) * (p.Lat2 - p.Lat1) > (p.Lng2 - p.Lng1) * (z.Lat - p.Lat1)
						or (p.Lat2 - p.Lat1) < 0 and (z.Lng - p.Lng1) * (p.Lat2 - p.Lat1) < (p.Lng2 - p.Lng1) * (z.Lat - p.Lat1))
					group by p.primitive_id) % 2 <> 0

	drop table #zone_primitive_edge
end
go