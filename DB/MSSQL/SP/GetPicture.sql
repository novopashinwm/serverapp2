IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPicture]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPicture]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetPicture](
	@vehicle_id int
	, @log_time int
)
as
begin
	set nocount on;

	select Bytes = Picture, Camera_Number, Photo_Number, Url
		from dbo.Picture_Log plog (nolock)
		where Vehicle_ID = @vehicle_id
		  and Log_Time = @log_time
	
end

