if exists (
	select *
		from sys.objects
		where name = 'IsProcessingResultChangeAllowed'
)
	drop function IsProcessingResultChangeAllowed
go

create function IsProcessingResultChangeAllowed(@old int, @new int)
returns bit
as
begin

	declare @Delivered int = 20
	declare @MismatchAppId int = 28
	declare @Read int = 25

	if @old = @Delivered and @new <> @Read
		return 0

	if @old = @Read
		return 0

	if @old = @MismatchAppId
		return 0

	return 1

end