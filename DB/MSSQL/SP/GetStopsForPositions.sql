set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetStopsForPositions' and type='p'))
	drop procedure dbo.GetStopsForPositions;
go

create procedure dbo.GetStopsForPositions
	@vehicle_log_time	dbo.Vehicle_Log_Time_Param readonly,
	@duration int,
	@debug bit = NULL
as
begin

	if isnull(@debug, 0) = 0
		set nocount on;

	create table #vehicle_log_time (Vehicle_ID int, Log_Time int, primary key clustered (Vehicle_ID, Log_Time))
	
	if @debug = 1
		print 'Fill #vehicle_log_time'
	
	insert into #vehicle_log_time
		select Vehicle_ID, Log_Time
			from @vehicle_log_time

	select vlt.Vehicle_ID, vlt.Log_Time
		from #vehicle_log_time vlt
		join Geo_Log l with (nolock) on l.Vehicle_ID = vlt.Vehicle_ID and l.Log_Time = vlt.Log_Time
		cross apply (
			select top(1) p.Log_Time, p.Lat, p.Lng
				from Geo_Log p with (nolock) 
				where p.Vehicle_ID = vlt.Vehicle_ID
				  and p.Log_Time < vlt.Log_Time - @duration
				order by p.Log_Time desc) p
		where dbo.GetTwoGeoPointsDistance(p.Lng, p.Lat, l.Lng, l.Lat) < 1
		
end