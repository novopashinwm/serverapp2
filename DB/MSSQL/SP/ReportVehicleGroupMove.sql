IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportVehicleGroupMoveV2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportVehicleGroupMoveV2]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[ReportVehicleGroupMoveV2]
	@logTimeList	DateTimeRange readonly,
	@operator_Id	int = null,
	@vehicleGroupId int = null,
	@vehicleIds     Id_Param readonly
as
begin
	set nocount on;

	create table #v 
	(
		Vehicle_ID int primary key,
		Vehicle_Name nvarchar(255),
		[1] int,
		[2] int,
		[3] int,
		[4] int,
		[5] int,
		[6] int,
		[7] int,
		[8] int,
		[9] int,
		[10] int,
		[11] int,
		[12] int,
		[13] int,
		[14] int,
		[15] int,
		[16] int,
		[17] int,
		[18] int,
		[19] int,
		[20] int,
		[21] int,
		[22] int,
		[23] int,
		[24] int,
		[25] int,
		[26] int,
		[27] int,
		[28] int,
		[29] int,
		[30] int,
		[31] int,
		OdometerStart bigint,
		OdometerEnd bigint,
		SumRun int,
		FuelStandart numeric(9, 2),
		FuelSpentByStandard numeric(9, 2)
	);

	--заполняем список доступных машин
	if (@operator_Id is not null and @operator_Id > 0)
		insert into #v (vehicle_id)
		select v.vehicle_id
		from vehicle v
		where 2 = (select count(1) from v_operator_vehicle_right where vehicle_id=v.VEHICLE_ID and operator_Id = @operator_Id and right_id in (115 /*PathAccess*/, 102 /*VehicleAccess*/))
		  and not exists (select 1 from v_operator_vehicle_right ignore where ignore.vehicle_id=v.VEHICLE_ID and ignore.operator_id=@operator_Id and ignore.right_id = 120)
	else
		insert into  #v (vehicle_id)
		select vehicle_id
		from vehicle;

	if (@vehicleGroupId is not null)
	begin
		delete from #v
		where #v.vehicle_id not in (
				select vv.vehicle_id 
				from dbo.VEHICLEGROUP_VEHICLE vv 
				where vehicleGroup_Id = @vehicleGroupId
				)
	end

	if exists (select 1 from @vehicleIds)
	begin

		delete from #v
			where #v.Vehicle_ID not in (select vid.Id from @vehicleIds vid)

	end


	declare @intervals table (
		rn		int identity primary key clustered, 
		[from]	int, 
		[to]	int);

	insert into @intervals ([from], [to])
		select dateFrom, dateTo
			from @logTimeList
	
	--debug	
	--select * from @intervals

	delete from @intervals
		where 31 < rn;

	declare @vId int;
	declare @maxDay int;
	set @maxDay = (select max(rn) from @intervals)		

	declare @dist int;
	declare @sql nvarchar(max);
	
	declare @logTimeFrom int = (select MIN([from]) from @intervals)
	declare @logTimeTo int = (select MAX([to]) from @intervals)

	DECLARE v_cur CURSOR LOCAL STATIC FORWARD_ONLY
	FOR select vehicle_id from #v;

	OPEN v_cur;

	FETCH NEXT FROM v_cur INTO @vId;

	declare @dtFrom int;
	select @dtFrom = [from] from @intervals where rn = 1

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		--Заполняем таблицу с датой начала по каждому дню в указаном диапазоне, но не более 31 дня
		declare @days int, @currDate int, @nextDate int;
		declare @sumRun real
		declare @exactDist real
		set @sumRun = 0
		set @days = 1;		
		while (@days <= @maxDay)
			BEGIN
				select	@currDate = [from],
						@nextDate = [to]
					from @intervals where rn = @days;
				
				set @exactDist = dbo.CalculateDistanceFN(@currDate, @nextDate, @vId)
				set @sumRun = @sumRun + @exactDist

				set @dist = ROUND(@exactDist/1000, 0);
				set @sql = N'update #v set [' + cast(@days as varchar) + '] = ' + cast(@dist as varchar)  
				+ ' where Vehicle_ID = ' + cast(@vId as varchar)
				
				exec sp_executesql @sql;

				select @days = @days + 1;
			END
		
		update #v 
			set SumRun = @sumRun/1000
			where Vehicle_ID = @vId;

		FETCH NEXT FROM v_cur INTO @vId;
	END

	CLOSE v_cur;
	DEALLOCATE v_cur;

	update v  
	  set 
			fuelStandart = std.RunStandard
		  , FuelSpentByStandard = std.RunStandardMultiplier * v.SumRun
	  from #v v   
	  join v_vehicle_fuel_spending_standard std on std.vehicle_id = v.Vehicle_ID
		
	-- Добавление значения реального одометра.
	create table #odometers (vid int, odometerstart bigint, odometerend bigint)
	
	insert into #odometers
	select
		t1.Vehicle_ID,
		MIN(t1.Value) value_min,
		MAX(t1.Value) value_max
	from (
	select v1.Vehicle_ID, v1.Value
		from v_controller_sensor_log v1 with (nolock)
		join #v v2 on v2.Vehicle_ID = v1.Vehicle_ID
	where v1.SensorLegend = 8
		and v1.Log_Time between @logTimeFrom and @logTimeTo
		) t1
	group by t1.Vehicle_ID
	
	update v
	set
		OdometerStart = o.odometerstart,
		OdometerEnd = o.odometerend
	from #v v
	join #odometers o on o.vid = v.Vehicle_ID

	--Data
	select * from #v
	--Header
	select (select sum(SumRun) from #v) as	'TotalRun', 
		cast((select sum(cast(isnull(SumRun,0)as real)/100 * isnull(fuelstandart,0)) from #v) as int) as 'TotalFuel'

	drop table #v;

end