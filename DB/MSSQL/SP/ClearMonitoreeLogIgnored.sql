if exists (
	select *
		from sys.procedures 
		where name = 'ClearMonitoreeLogIgnored'
)
	drop procedure ClearMonitoreeLogIgnored
go

--exec ClearMonitoreeLogIgnored @debug = 1
create procedure ClearMonitoreeLogIgnored
(
	--����, �� ������� ����� ������� ������ - �� ��������� - �� ��������� �����
	@deleteToDateTime datetime = null,
	@debug bit = null
)
as


if (@debug is null)
	set @debug = 0

if (@debug = 0)
	set nocount on
	
if @deleteToDateTime is null
	set @deleteToDateTime = GETUTCDATE() - 30

declare @interval int, @to int, @dtTo datetime, @limitDT int;
select @interval = 1*3600;
select @limitDT = datediff(SECOND, '1970-01-01', @deleteToDateTime)

set @to = (select top(1) log_time from Geo_Log_Ignored (nolock) order by LOG_TIME asc)

declare @rows_deleted bigint

while @to < @limitDT
begin
	select @dtTo = dbo.getdatefromint(@to);

	delete from dbo.Geo_Log_Ignored
	where log_time between @to - @interval and @to

	set @rows_deleted = @@ROWCOUNT

	set @to = (select top(1) log_time from Geo_Log_Ignored (nolock) where log_time > @to order by log_time asc) + @interval
	
	if (@to is null)
		break
	
	if @debug = 1
		print convert(varchar(50), @dtTo) + ': ' + convert(varchar(50), @rows_deleted)
end
