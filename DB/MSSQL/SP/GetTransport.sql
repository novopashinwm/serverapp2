/****** Object:  StoredProcedure [dbo].[GetTransport]    Script Date: 07/14/2010 18:58:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------

ALTER procedure [dbo].[GetTransport] 
	@date 		datetime,
	@VehicleGroup	int,
	@operator_id int = null
as
----------------------------------------------------------------------------------------
-- Test
--declare @date datetime 
--set @date = '2006-09-14 20:00:00'
--set @date = '2006-08-07 20:00:00'
----------------------------------------------------------------------------------------

	select distinct wt.*, t.schedule_detail_id, sd.shift_id as shift_number, 0 as total_trips
	into #temp_wt
	from wb_trip wt
		join waybill_header wh on wt.waybill_header_id = wh.waybill_header_id
		left join trip t on wt.trip_id = t.trip_id
		left join schedule_detail sd on t.schedule_detail_id = sd.schedule_detail_id
		left join v_operator_vehicle_right rr on rr.vehicle_id = wh.VEHICLE_ID
	where wh.waybill_date between dateadd(hour, -1, @date) and dateadd(hour, 1, @date)
	and (@operator_id is null or rr.operator_id = @operator_id)

	-- T┐■Їv  ■ TT ° Є■Ї°кї√а№
	select distinct WBT.*, WBH.EXT_NUMBER as WBH_NUM, DR.EXT_NUMBER as DR_NUM 
	into #Out
	from WB_TRIP as WBT
		left join WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
		left join WAYBILL as WB on WBT.WAYBILL_ID = WB.WAYBILL_ID
		left join DRIVER as DR on WB.DRIVER_ID = DR.DRIVER_ID
		left join v_operator_vehicle_right vr on vr.vehicle_id = WBH.VEHICLE_ID
		left join v_operator_driver_right rr on rr.driver_id = WB.DRIVER_ID
	where
		(
			WBH.WAYBILL_DATE between dateadd(hour, -1, @date) and dateadd(hour, 1, @date)
			or WB.WAYBILL_DATE = CONVERT(datetime, CONVERT(varchar, @date, 1), 1) + 1 
			--or WB.WAYBILL_DATE between CONVERT(datetime, CONVERT(varchar, @date, 1), 1) and (CONVERT(datetime, CONVERT(varchar, @date, 1), 1) + 1) 
		)
		and WBT.TRIP_KIND_ID in (12, 16)
		and (@operator_id is null 
				or (WB.DRIVER_ID is null or rr.operator_id = @operator_id)
				or (WBH.VEHICLE_ID is null or vr.operator_id = @operator_id)
			)
	order by 
		WBT.BEGIN_TIME	


	select twt.schedule_detail_id, count(*) as total_trips
	into #temp_count
	from (select distinct schedule_detail_id from #temp_wt) as twt
		left join trip t
			on twt.schedule_detail_id = t.schedule_detail_id
	where t.trip_kind_id in (5, 6)
	group by twt.schedule_detail_id

	update #temp_wt
	set total_trips = tc.total_trips
	from #temp_wt twt, #temp_count tc 
	where tc.schedule_detail_id = twt.schedule_detail_id

	drop table #temp_count

	select distinct 
		wh.ext_number as wh_number,
		v.vehicle_id,
		v.garage_number as vehicle_number,
		d.ext_number as driver_number,
		g.[name] as graphic_name,
		wt.begin_time,
		wt.end_time,
		w.begin_time as begin_time_shift,
		w.end_time as end_time_shift,
		wt.trip_kind_id,
		r.ext_number as route_number,
		s.ext_number as schedule_number,
		wt.shift_number,
		gt.length as length,
		wt.status,
		wt.penalty as penalty_rate,
		wt.total_trips
	into #temp_result
	from #temp_wt wt
		join waybill_header wh
			on wt.waybill_header_id = wh.waybill_header_id
		join vehicle v
			on wh.vehicle_id = v.vehicle_id
		left join graphic g
			on v.graphic = g.graphic_id
		left join waybill w
			on wt.waybill_id = w.waybill_id
		left join driver d
			on w.driver_id = d.driver_id
		left join trip t
			on wt.trip_id = t.trip_id
		left join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		left join schedule s
			on sd.schedule_id = s.schedule_id
		left join route r
			on s.route_id = r.route_id
		left join route_trip rt
			on t.route_trip_id = rt.route_trip_id
		left join geo_trip gt
			on rt.geo_trip_id = gt.geo_trip_id
	where wh.waybill_date between dateadd(hour, -1, @date) and dateadd(hour, 1, @date)

	drop table #temp_wt


	--+╕√° ўЁЇЁ¤Ё єи║  Ё, ЄvёїиЁї№ к■√╣·■ Ї√а єи║  v
	if(@VehicleGroup > 0) 
	begin
		select distinct  	
			VGV.VEHICLEGROUP_ID,
			VGV.VEHICLE_ID
		into #VehicleGroup
		from dbo.VEHICLEGROUP as VG 
		inner join dbo.VEHICLEGROUP_VEHICLE as VGV on VG.[VEHICLEGROUP_ID] = VGV.[VEHICLEGROUP_ID]
		where VG.VEHICLEGROUP_ID = @VehicleGroup

		--select * from #VehicleGroup

		delete from #temp_result where VEHICLE_ID not in (select distinct VEHICLE_ID from #VehicleGroup)  
		
		drop table #VehicleGroup
	end


	declare @wh_number nvarchar(30),
		@vehicle_number nvarchar(15),
		@driver_number nvarchar(50),
		@graphic_name nvarchar(100),
		@begin_time datetime,
		@end_time datetime,
		@begin_time_shift datetime,
		@end_time_shift datetime,
		@trip_kind_id int,
		@route_number nvarchar(9),
		@schedule_number nvarchar(20),
		@shift_number int,
		@length real,
		@status int,
		@penalty_rate int,
		@total_trips int

	declare @time_novice int, -- ╕кЁЎми
		@time_lunch int, -- ■ёїЇ
		@time_fuel int, -- ўЁ иЁЄ·Ё
		@time_repair int, -- иї№■¤к
		@time_idle int, -- ■к╕к■∙
		@time_wait int, --  и■╕к■∙
		@time_reserve int, -- иїўїиЄ
		@time_order int, -- ўЁ·Ёў
		@time_online int, -- √°¤°а
		@time_out datetime -- ╕┐■Ї
		,@penalty int -- ░киЁп

	
	DECLARE cursor_tr CURSOR FOR
	SELECT wh_number, vehicle_number, driver_number, graphic_name, begin_time, end_time, begin_time_shift, end_time_shift, trip_kind_id, route_number, schedule_number, shift_number, length, status, penalty_rate, total_trips
	FROM #temp_result
	where trip_kind_id not in (12, 13)

	OPEN cursor_tr

	FETCH NEXT FROM cursor_tr
	into @wh_number, @vehicle_number, @driver_number, @graphic_name, @begin_time, @end_time, @begin_time_shift, @end_time_shift, @trip_kind_id, @route_number, @schedule_number, @shift_number, @length, @status, @penalty_rate, @total_trips

	if @@FETCH_STATUS = 0
	begin
		set @time_novice = 0
		set @time_lunch = 0
		set @time_fuel = 0
		set @time_repair = 0
		set @time_idle = 0
		set @time_wait = 0
		set @time_reserve = 0
		set @time_order = 0
		set @time_online = 0
		set @time_out = null
		set @penalty = 0

										
		if @trip_kind_id = 3 -- ■ёїЇ
			set @time_lunch = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id = 7 -- ўЁ иЁЄ·Ё
			set @time_fuel = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id in (11, 12) -- иї№■¤к
			set @time_repair = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id = 2 -- ■к╕к■∙
			set @time_idle = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id = 10 --  и■╕к■∙
			set @time_wait = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id = 8 -- иїўїиЄ
			set @time_reserve = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id = 9 -- ўЁ·Ёў
			set @time_order = datediff(ss, @begin_time, @end_time)

		if @trip_kind_id in (4, 5, 6, 7, 9) -- √°¤°а
			set @time_online = datediff(ss, @begin_time, @end_time)

		if @status in (0x04) -- ░киЁп
			set @penalty = @status

		
		/*if exists (select *
				from #temp_result
				where vehicle_number = @vehicle_number
					and trip_kind_id in (12, 16) )
			set @time_out = (select top 1 begin_time
				from #temp_result
				where vehicle_number = @vehicle_number
					and trip_kind_id in (12, 16)
				order by begin_time)*/
		set @time_out =	
			(select top 1 BEGIN_TIME 
			from #Out
			where
				WBH_NUM = @wh_number or DR_NUM = @driver_number 
			order by 
				BEGIN_TIME) 


		select @wh_number as wh_number,
			@vehicle_number	as vehicle_number,
			@driver_number as driver_number,
			@graphic_name as graphic_name,
			--isnull(@begin_time_shift, @begin_time) as work_begin,
			--isnull(@end_time_shift, @end_time) as work_end,
			case when @trip_kind_id in (12, 16) or @time_out is not null then @begin_time else isnull(@begin_time_shift, @begin_time) end as work_begin,
			case when @trip_kind_id in (12, 16) or @time_out is not null then @end_time else isnull(@end_time_shift, @end_time) end as work_end,
				--@begin_time as work_begin,
				--@end_time as work_end,
			@time_novice as time_novice,
			@time_lunch as time_lunch,
			@time_fuel as time_fuel,
			@time_repair as time_repair,
			@time_idle as time_idle,
			@time_wait as time_wait,
			@time_reserve as time_reserve,
			@route_number as route_number,
			@schedule_number as schedule_number,
			@shift_number as shift_number,
			@total_trips as trips_plan,
			case when @trip_kind_id in (5, 6) and @status = 0 then 1 else 0 end as trips_fact,
			case when @trip_kind_id in (2, 3, 5, 6) then @length else @length-@length end as length_pass,
			case when @trip_kind_id in (2, 3, 5, 6/*, 7*/) then @length-@length else @length end as length_empty,
			@time_order as time_order,
			@time_online as time_online,
			@time_out as time_out
			,@penalty as penalty
			,@penalty_rate as penalty_rate
			,case when @trip_kind_id in (5, 6) and @status = 0x04 then 1 else 0 end as defect_count
		into #result

		FETCH NEXT FROM cursor_tr
		into @wh_number, @vehicle_number, @driver_number, @graphic_name, @begin_time, @end_time, @begin_time_shift, @end_time_shift, @trip_kind_id, @route_number, @schedule_number, @shift_number, @length, @status, @penalty_rate, @total_trips

		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @time_novice = 0
			set @time_lunch = 0
			set @time_fuel = 0
			set @time_repair = 0
			set @time_idle = 0
			set @time_wait = 0
			set @time_reserve = 0
			set @time_order = 0
			set @time_online = 0
			set @time_out = null
			set @penalty = 0

			if @trip_kind_id = 3 -- ■ёїЇ
				set @time_lunch = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id = 7 -- ўЁ иЁЄ·Ё
				set @time_fuel = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id in (11, 12) -- иї№■¤к
				set @time_repair = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id = 2 -- ■к╕к■∙
				set @time_idle = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id = 10 --  и■╕к■∙
				set @time_wait = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id = 8 -- иїўїиЄ
				set @time_reserve = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id = 9 -- ўЁ·Ёў
				set @time_order = datediff(ss, @begin_time, @end_time)

			if @trip_kind_id in (4, 5, 6, 7, 9) -- √°¤°а
				set @time_online = datediff(ss, @begin_time, @end_time)

			if @status in (0x04) -- ░киЁп
				set @penalty = @status

			/*if exists (select *
					from #temp_result
					where vehicle_number = @vehicle_number
						and trip_kind_id in (12, 16))
				set @time_out = (select top 1 begin_time
					from #temp_result
					where vehicle_number = @vehicle_number
						and trip_kind_id in (12, 16)
					order by begin_time)*/
			set @time_out =	
				(select top 1 BEGIN_TIME 
				from #Out
				where
					WBH_NUM = @wh_number or DR_NUM = @driver_number  
				order by 
					BEGIN_TIME) 


			if not exists (select *
					from #result
					where wh_number = @wh_number 
						and vehicle_number = @vehicle_number 
						and driver_number = @driver_number 
						and schedule_number = @schedule_number)
				insert into #result
				values (@wh_number, @vehicle_number, @driver_number, @graphic_name,
					--isnull(@begin_time_shift, @begin_time), isnull(@end_time_shift, @end_time),
					case when @trip_kind_id in (12, 16) or @time_out is not null then @begin_time else isnull(@begin_time_shift, @begin_time) end,
					case when @trip_kind_id in (12, 16) or @time_out is not null then @end_time else isnull(@end_time_shift, @end_time) end,
						--@begin_time, @end_time,
					@time_novice, @time_lunch, @time_fuel, @time_repair, @time_idle, @time_wait, @time_reserve,
					@route_number, @schedule_number, @shift_number,
					@total_trips,
					case when @trip_kind_id in (5, 6) and @status = 0 then 1 else 0 end,
					case when @trip_kind_id in (2, 3, 5, 6) then @length else @length-@length end,
					case when @trip_kind_id in (2, 3, 5, 6/*, 7*/) then @length-@length else @length end,
					@time_order, @time_online, @time_out, @penalty, @penalty_rate, case when @trip_kind_id in (5, 6) and @status = 0x04 then 1 else 0 end)
			else
				update #result
				set 
					--work_begin = isnull(@begin_time_shift, (case when @begin_time < work_begin then @begin_time else work_begin end)),
					--work_end = isnull(@end_time_shift, (case when @end_time > work_end then @end_time else work_end end)),
					work_begin = (case when @trip_kind_id in (12, 16) or @time_out is not null then (case when @begin_time < work_begin then @begin_time else work_begin end) else isnull(@begin_time_shift, (case when @begin_time < work_begin then @begin_time else work_begin end)) end),
					work_end = (case when @trip_kind_id in (12, 16) or @time_out is not null then (case when @end_time > work_end then @end_time else work_end end) else isnull(@end_time_shift, (case when @end_time > work_end then @end_time else work_end end)) end),
						--work_begin = case when @begin_time < work_begin then @begin_time else work_begin end,
						--work_end = case when @end_time > work_end then @end_time else work_end end,
					time_novice = time_novice+@time_novice,
					time_lunch = time_lunch+@time_lunch,
					time_fuel = time_fuel+@time_fuel,
					time_repair = time_repair+@time_repair,
					time_idle = time_idle+@time_idle,
					time_wait = time_wait+@time_wait,
					time_reserve = time_reserve+@time_reserve,
					trips_fact = trips_fact+(case when @trip_kind_id in (5, 6) and @status = 0 then 1 else 0 end),
					length_pass = length_pass+(case when @trip_kind_id in (2, 3, 5, 6) then @length else 0 end),
					length_empty = length_empty+(case when @trip_kind_id in (2, 3, 5, 6/*, 7*/) then 0 else @length end),
					time_order = time_order+@time_order,
					time_online = time_online+@time_online
					,penalty = penalty | @penalty 
					,penalty_rate = penalty_rate + @penalty_rate
					,defect_count = defect_count + (case when @trip_kind_id in (5, 6) and @status = 0x04 then 1 else 0 end)
				where wh_number = @wh_number 
					and vehicle_number = @vehicle_number 
					and driver_number = @driver_number 
					and schedule_number = @schedule_number

			FETCH NEXT FROM cursor_tr
			into @wh_number, @vehicle_number, @driver_number, @graphic_name, @begin_time, @end_time, @begin_time_shift, @end_time_shift, @trip_kind_id, @route_number, @schedule_number, @shift_number, @length, @status, @penalty_rate, @total_trips
		END

		select *
		from #result
		order by vehicle_number, wh_number, work_begin, schedule_number, driver_number 

		drop table #result
	end
	else
		select wh_number, vehicle_number, driver_number, graphic_name, begin_time, end_time, wh_number, wh_number, wh_number, wh_number, wh_number, wh_number, wh_number, route_number, schedule_number, shift_number, wh_number, wh_number, length, length, wh_number, wh_number, wh_number
		from #temp_result 

	CLOSE cursor_tr

	DEALLOCATE cursor_tr

	drop table #Out
	drop table #temp_result

