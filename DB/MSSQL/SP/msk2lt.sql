if exists (
	select *
		from sys.objects
		where name = 'msk2lt')
	drop function dbo.msk2lt;
go

create function dbo.msk2lt
(
	@s datetime
)
returns int
as
begin
	return dbo.utc2lt(@s) - 3*60*60;
end