IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteOperatorFromDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteOperatorFromDepartment]

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

create PROCEDURE [dbo].[DeleteOperatorFromDepartment]
	@operatorId int,
	@operatorIdToDelete int,
	@departamentId int
as
begin
	set nocount on
	--Исключаем оператора из департамента. Добавляем запретительные права на департамент
	update operator_department
	set allowed = 0
	where Department_id = @departamentId 
	and operator_id = @operatorIdToDelete 
	and allowed=1;

	insert into operator_department (operator_id, Department_id, right_id, allowed)
	select distinct @operatorIdToDelete, @departamentId, od.right_id, 0
	from dbo.operator_department od
	where not exists (select * from operator_department od2 
					where od2.right_id = od.right_id 
					and od2.operator_id = @operatorIdToDelete
					and od2.department_id = @departamentId);
	
    --Удаляем все права оператора на машины департамента
	update operator_vehicle
	set allowed = 0
	where vehicle_id in (select vehicle_id from vehicle v
						where v.department = @departamentId)
	and operator_id = @operatorIdToDelete 
	and allowed=1;

	insert into operator_vehicle (operator_id, vehicle_id, right_id, allowed)
	select distinct @operatorIdToDelete, v.vehicle_id, ov.right_id, 0
	from (select distinct right_id from dbo.operator_vehicle) ov
	,vehicle v
	where v.department = @departamentId
	and not exists (select * from operator_vehicle ov2 
					where ov2.right_id = ov.right_id 
					and ov2.vehicle_id = v.vehicle_id
					and ov2.operator_id = @operatorIdToDelete);

	--Удаляем все права на группы админа департамента (вроде больше никто не сможет раздавать права на группы)
	update operator_vehiclegroup
	set allowed = 0
	where operator_id = @operatorIdToDelete
	and vehiclegroup_id in (select vehiclegroup_id from dbo.v_operator_vehicle_groups_right r 
							where operator_id = @operatorId
							and right_id = 109--allowEditGroup
							);

	insert into operator_vehiclegroup (operator_id, vehiclegroup_id, right_id, allowed)
	select distinct @operatorIdToDelete, g.vehiclegroup_id, ov.right_id, 0
	from (select distinct right_id from dbo.operator_vehiclegroup) ov
	,dbo.vehiclegroup g
	join dbo.v_operator_vehicle_groups_right r on r.vehiclegroup_id = g.vehiclegroup_id
	where r.operator_id = @operatorId
	and r.right_id = 109 --allowEditGroup
	and not exists (select * from operator_vehiclegroup vg2 
					where vg2.right_id = ov.right_id 
					and vg2.operator_id = @operatorIdToDelete
					and vg2.vehiclegroup_id = g.vehiclegroup_id);


end













