IF (OBJECT_ID(N'[dbo].[GetRemoteTerminalServerUrl]') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetRemoteTerminalServerUrl]
GO

CREATE PROCEDURE [dbo].[GetRemoteTerminalServerUrl]
(
	@Vehicle_ID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		R.[Name],
		R.[Url],
		V.[DeviceID]
	FROM [dbo].[Vehicle_RemoteTerminalServer] V
		INNER JOIN [dbo].[RemoteTerminalServer] R
			ON V.[RemoteTerminalServer_ID] = R.[RemoteTerminalServer_ID]
	WHERE V.[Vehicle_ID] = @vehicle_ID
	AND   V.[Enabled]    = 1
END