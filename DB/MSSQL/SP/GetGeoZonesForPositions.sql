set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if not exists (select * from sys.table_types where name = 'Vehicle_Zone_Position_Param')
	create type Vehicle_Zone_Position_Param as table 
	(
		vehicle_id int, 
		zone_id int,
		log_time int,
		lat float,
		lng float
	)
go

CreateProcedure 'GetGeoZonesForPositions'
go

alter procedure GetGeoZonesForPositions
	@vehicle_zone		dbo.Vehicle_Zone_Param readonly,
	@vehicle_position	dbo.Vehicle_Position_Param readonly,
	@debug bit = NULL
as
begin
	if isnull(@debug, 0) = 0
		set nocount on;

	if exists (select * from @vehicle_zone)		
	begin
		create table #vehicle_zone (Vehicle_ID int, Zone_ID int, primary key clustered (Vehicle_ID, Zone_ID))
		insert into #vehicle_zone select Vehicle_ID, Zone_ID from @vehicle_zone
	end
	
	create table #zone (vehicle_id int, zone_id int, isFull bit, log_time int, Lat float, Lng float)
	create table #vehicle_positions (Vehicle_ID int,Log_Time int,Lat numeric(8,5),Lng numeric(8,5),primary key clustered (Vehicle_ID, Log_Time)	)

	insert into #vehicle_positions select Vehicle_ID, Log_Time, Lat, Lng from @vehicle_position
	
	if @debug = 1
		print '#zone is filling by zone_matrix'

	if exists (select * from @vehicle_zone)
	begin
		insert into #zone
		select vp.Vehicle_ID, z.ZONE_ID, zm.isFull, vp.Log_Time, vp.Lat, vp.Lng
		from #vehicle_zone z
			join #vehicle_positions vp on z.Vehicle_ID = vp.Vehicle_ID
			join dbo.Scale scale on 1=1
			cross apply (
				select top(1) zm.isFull
					from dbo.Zone_Matrix zm 
					where zm.Node = dbo.GetMatrixNode(vp.Lng, vp.Lat, scale.Value) 
					  and zm.ZONE_ID = z.Zone_ID
					  and zm.SCALE = scale.Value
					order by zm.IsFull desc) zm
	end
	else
	begin
		insert into #zone
		select vp.Vehicle_ID, zm.ZONE_ID, zm.isFull, vp.Log_Time, vp.Lat, vp.Lng
		from #vehicle_positions vp 
			join dbo.Scale scale on 1=1
			join dbo.Zone_Matrix zm on zm.Node = dbo.GetMatrixNode(vp.Lng, vp.Lat, scale.Value)
	end

	create clustered index IX_Zone_Type_Name on #zone(isFull)

	create table #result(zone_id int, vehicle_id int, log_time int)
	declare @circleIntersections Vehicle_Zone_Position_Param
	declare @polygonIntersections Vehicle_Zone_Position_Param

	if @debug = 1
		print 'insert into #result --only fully covered zones'

	insert into #result
	select z.zone_id, z.vehicle_id, z.log_time from #zone z where z.isFull = 1

	declare @circleZoneId int = (select Id from ZONE_PRIMITIVE_TYPE where Name = 'Circle')					
	insert into @circleIntersections(vehicle_id, zone_id, log_time, lat, lng)
		select z.vehicle_id, z.zone_id, z.log_time, z.Lat, z.Lng 
		from #zone z 
			join GEO_ZONE_PRIMITIVE gzp on z.ZONE_ID = gzp.ZONE_ID
			join ZONE_PRIMITIVE p on p.PRIMITIVE_ID = gzp.PRIMITIVE_ID
		where z.isFull = 0 and p.PRIMITIVE_TYPE = @circleZoneId
		  and not exists (select 1 from #result e where e.vehicle_id = z.vehicle_id and e.zone_id = z.zone_id and e.log_time = z.log_time)

	if @@rowcount > 0
		insert into #result
			select z.zone_id, z.vehicle_id, z.log_time
				from @circleIntersections z
				where exists 
					(
					select 1
					from GEO_ZONE_PRIMITIVE zp 
						join ZONE_PRIMITIVE p on zp.PRIMITIVE_ID = p.PRIMITIVE_ID
						join ZONE_PRIMITIVE_VERTEX v on v.PRIMITIVE_ID = p.PRIMITIVE_ID
						join MAP_VERTEX mv on mv.VERTEX_ID = v.VERTEX_ID
					where p.RADIUS >= dbo.GetTwoGeoPointsDistance(z.Lng, z.Lat, mv.X, mv.Y)
						and z.ZONE_ID = zp.ZONE_ID
					)

	
	declare @polygonZoneId int = (select Id from ZONE_PRIMITIVE_TYPE where Name = 'Polygon')					
	insert into @polygonIntersections(vehicle_id, zone_id, log_time, lat, lng)
		select z.vehicle_id, z.zone_id, z.log_time, z.Lat, z.Lng 
			from #zone z 
				join GEO_ZONE_PRIMITIVE gzp on z.ZONE_ID = gzp.ZONE_ID
				join ZONE_PRIMITIVE p on p.PRIMITIVE_ID = gzp.PRIMITIVE_ID
			where z.isFull = 0 and p.PRIMITIVE_TYPE = @polygonZoneId

	if @@rowcount > 0
	begin
		select Zone_ID, Lat1, Lng1, Lat2, Lng2, Primitive_ID 
			into #zone_primitive_edge
			from dbo.v_zone_primitive_edge p where p.Zone_ID in (select distinct zone_id from @polygonIntersections)

		create clustered index IX_Zone_Primitive_Edge on #zone_primitive_edge(Zone_ID, Primitive_ID)

		insert into #result
			select z.Zone_ID, z.Vehicle_ID, z.Log_Time
				from @polygonIntersections z
				where (select count(1)
							from #zone_primitive_edge p
							where p.Zone_ID = z.zone_id
								and ((p.Lat1 <= z.Lat and z.Lat < p.Lat2) or (p.Lat2 <= z.Lat and z.Lat < p.Lat1))
								and ((p.Lat2 - p.Lat1) > 0 and (z.Lng - p.Lng1) * (p.Lat2 - p.Lat1) > (p.Lng2 - p.Lng1) * (z.Lat - p.Lat1)
								or (p.Lat2 - p.Lat1) < 0 and (z.Lng - p.Lng1) * (p.Lat2 - p.Lat1) < (p.Lng2 - p.Lng1) * (z.Lat - p.Lat1))
							group by p.primitive_id) % 2 <> 0
				and not exists (select 1 from #result e where e.vehicle_id = z.vehicle_id and e.zone_id = z.zone_id and e.log_time = z.log_time)
	end

	select distinct * from #result

	if @debug = 1
		print 'result set'

	if exists (select * from @vehicle_zone)	
		drop table #vehicle_zone
		
	drop table #vehicle_positions
	drop table #zone;
	drop table #result
end
