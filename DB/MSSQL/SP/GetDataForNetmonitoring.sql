if exists (select * from sys.objects where type = 'p' and name = 'GetDataForNetmonitoring')
	drop procedure GetDataForNetmonitoring;
go
create procedure GetDataForNetmonitoring
(
	@debug bit = 0
)
as

set nocount on

--������ �������� ���������� GPS:
--����� ���������� ������ �� ������ ��������������, 
--������� ������� �� ���������� GPS �� ����� ��� �� @delta ������
declare @delta int = 30

--������ ����� ���������� - ���������� �� ������� ������� ���������� ���������� ������ @period ������, ��������, ��� � ���
declare @period int = 60 * 60

--�������� - ����������� �� ���������� ���������� � ��������������, � � ��������� � @delay ������, ��������, 3 ���
declare @delay int = 5 * 60 --���� �������� 5 ����� �� ����� ������������

--������� �����, unix time
declare @current_log_time int = datediff(ss, '1970', getutcdate())

declare @v table (Vehicle_ID int, Log_Time_From int, Log_Time_To int)

insert into @v
	select top(1) Vehicle_ID, Last_Log_Time + 1, Last_Log_Time + @period
		from VehicleNetmonitoring vn
		where vn.Last_Log_Time < @current_log_time - @delay
		order by vn.Last_Log_Time

if @@rowcount = 0
	return;
	
update vn
	set Last_Log_Time = v.Log_Time_To
	from VehicleNetmonitoring vn
	join @v v on v.Vehicle_ID = vn.Vehicle_ID
	
select v.Vehicle_ID, vn.Uuid, v.Log_Time_From, v.Log_Time_To
	from VehicleNetmonitoring vn
	join @v v on v.Vehicle_ID = vn.Vehicle_ID
	
declare @gps table (
	Vehicle_ID int, 
	Geo_Log_Time int, 
	Lat numeric(8,5), 
	Lng numeric(8,5), 
	Wlan_Log_Time int, 
	Cell_Log_Time int, 
	Speed int, 
	Course int,
	Next_Geo_Log_Time int
)

insert into @gps
	select geo.Vehicle_ID, geo.Log_Time, geo.Lat, geo.Lng, Wlan_Log_Time = wlan.Log_Time, Cell_Log_Time = cell.Log_Time, gps.Speed, CourseDegrees, next_geo.Log_Time
		from @v v
		join geo_log geo with (nolock) on geo.Vehicle_ID = v.Vehicle_ID 
								      and geo.Log_Time between v.Log_Time_From and v.Log_Time_To
		outer apply (
			select top(1) l.Log_Time
				from Geo_Log l with (nolock)
				where l.Vehicle_ID = geo.Vehicle_ID
				  and l.Log_Time between geo.Log_Time + 1 and geo.Log_Time + @delta
				  order by l.Log_Time asc
		) next_geo
		join GPS_Log gps with (nolock) on gps.Vehicle_ID = geo.Vehicle_ID 
									  and gps.Log_Time = geo.Log_Time
		left outer join Position_Radius pr with (nolock) on pr.Vehicle_ID = geo.Vehicle_ID and pr.Log_Time = geo.Log_Time
		outer apply (
			select top(1) wl.Log_Time
				from Wlan_Log wl with (nolock)
				where wl.Vehicle_ID = geo.Vehicle_ID 
				  and wl.Log_Time between geo.Log_Time and isnull(next_geo.Log_Time-1, geo.Log_Time + @delta)
				order by wl.Log_Time asc
		) wlan
		outer apply (
			select top(1) cl.Log_Time
				from Cell_Network_Log cl with (nolock)
				where cl.Vehicle_ID = geo.Vehicle_ID 
			      and cl.Log_Time between geo.Log_Time and isnull(next_geo.Log_Time-1, geo.Log_Time + @delta)
				order by cl.Log_Time asc
		) cell
		where 1=1
		  and (wlan.Log_Time is not null or cell.Log_Time is not null)
		  and (pr.Type is null or pr.Type = 0 /*GPS*/)
		  order by geo.Log_Time asc

select Log_Time = gps.Geo_Log_Time
	, gps.Lat
	, gps.Lng
	, gps.Speed
	, gps.Course		
	from @gps gps

if (isnull(@debug, 0) = 1)
	select * from @gps

select gps.Geo_Log_Time, l.*
	from @gps gps
	join v_cell_log l on l.Vehicle_ID = gps.Vehicle_ID and l.Log_Time = gps.Cell_Log_Time
	order by Geo_Log_Time, Cell_Log_Time


select gps.Geo_Log_Time, l.* 
	from @gps gps
	join v_wlan_log l on l.Vehicle_ID = gps.Vehicle_ID and l.Log_Time = gps.Wlan_Log_Time
