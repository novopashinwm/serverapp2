if exists (
	select * from sys.objects where name = 'RepeatCommands'
)
	drop procedure 	RepeatCommands
go

create procedure RepeatCommands
(
	@commandType int,
	@interval int,
	@debug bit = 0
)
as
begin

	if isnull(@debug, 0) = 0
		set nocount on

	while 1=1
	begin

		declare @capturePictureCommandTypeId int = (select id from CommandTypes where code='CapturePicture');
		declare @captureIntervalRuleId int = (select RULE_ID from [RULE] where NUMBER = 18)
		insert into Command  (Sender_ID, Target_ID, Type_ID, Date_Received, Result_Date_Received, [Status], Result_Type_ID)
		select top(1) 
			lastCmd.Sender_ID
			, v.Vehicle_ID
			, lastCmd.Type_ID
			, case when lastCmd.Age < lastCmd.Interval - 1
				then dateadd(SECOND, lastCmd.Interval-lastCmd.Age, getutcdate()) 
				else getutcdate() 
				end
			, case when lastCmd.Age < lastCmd.Interval - 1
				then dateadd(SECOND, lastCmd.Interval-lastCmd.Age, getutcdate()) 
				else getutcdate() 
				end
			, 0
			, 0
		from Vehicle v
			cross apply (
				select top(1) 
					c.Sender_ID
					, c.Type_ID
					, c.Result_Date_Received
					, c.Result_Type_ID
					, c.Status
					, Age = datediff(second, c.Result_Date_Received, getutcdate())
					, Interval = case 
						when c.Type_ID = @capturePictureCommandTypeId then 
						coalesce((
							select r.value
							from v_vehicle_rule r
							where r.Vehicle_ID = v.Vehicle_ID and r.Rule_ID = @captureIntervalRuleId
						), @interval)
						else @interval 
						end
					from Command c
					where c.Type_ID = @commandType
						and c.Target_ID = v.Vehicle_ID
						order by c.Date_Received desc
			) lastCmd
		where lastCmd.Result_Type_ID not in (
				0 /*Received*/
			, 1 /*Processing*/
			, 4 /*Cancelled*/
			)
			and lastCmd.Result_Date_Received < getutcdate()

		if @@rowcount = 0
		begin
			break
		end
	end
		
end