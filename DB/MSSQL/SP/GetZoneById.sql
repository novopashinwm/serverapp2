if (exists (select * from sys.objects where name = 'GetZoneById' and type = 'p'))
	drop procedure dbo.GetZoneById
go

create procedure dbo.GetZoneById
	@ZoneId int
as
set nocount on;
	
select
	g.ZONE_ID,
	g.NAME,
	g.DESCRIPTION
from GEO_ZONE g
where ZONE_ID = @ZoneId

go