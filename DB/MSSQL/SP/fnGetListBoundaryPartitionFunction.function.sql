﻿IF (OBJECT_ID(N'[dbo].[fnGetListBoundaryPartitionFunction]', N'IF') IS NOT NULL) --AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnGetListBoundaryPartitionFunction]
GO
CREATE FUNCTION [dbo].[fnGetListBoundaryPartitionFunction]
(
	@functionName sysname = N'LogsPartitionFunction'
)
RETURNS TABLE
AS
	RETURN
	(
		SELECT
			[boundary_id]    = R.[boundary_id],
			[boundary_value] = R.[value]
		FROM sys.partition_functions F WITH(NOLOCK)
			INNER JOIN sys.partition_range_values R WITH(NOLOCK)
				ON F.function_id = R.function_id
		WHERE F.[name] = @functionName
	);
GO

EXEC sys.sp_addextendedproperty
	@name       = N'MS_Description',
	@value      = N'Функция получения списка границ по имени ф-ции секционирования.',
	@level0type = N'SCHEMA',
	@level0name = N'dbo',
	@level1type = N'FUNCTION',
	@level1name = N'fnGetListBoundaryPartitionFunction';
GO
