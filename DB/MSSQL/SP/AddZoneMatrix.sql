IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddZoneMatrix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddZoneMatrix]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddZoneMatrix]  
	@zone_id	int,
	@node		bigint,
	@scale		int,
	@isFull		bit
AS
begin
	insert into dbo.ZONE_MATRIX (zone_id, node, scale, isfull) 
	values (@zone_id, @node, @scale, @isFull);
end


