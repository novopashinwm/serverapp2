/****** Object:  StoredProcedure [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]    Script Date: 03.08.2021 12:04:05 ******/
IF (OBJECT_ID(N'[dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]') IS NOT NULL)
  DROP PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS];
GO

/****** Object:  StoredProcedure [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]    Script Date: 03.08.2021 12:04:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]
(
	@from        datetime = null
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @to datetime,  @to_ETC datetime,  @from_ETC datetime, @from_int int, @to_int int

	--set @from = '2019-10-21 00:00.000'
	set @to = DATEADD (DAY, 1, @from)


	CREATE TABLE #firstinsert_sl  
		([vehicle_id] int NOT NULL,
		[from2] datetime NOT NULL, 
		[to2] datetime NOT NULL,
		[hits] int)


	select @to_ETC = dateadd(MINUTE, -DATEPART(TZoffset, SYSDATETIMEOFFSET()), @to)
	select @from_ETC = dateadd(MINUTE, -DATEPART(TZoffset, SYSDATETIMEOFFSET()), @from)


	select @to_int = CONVERT(INT, DATEDIFF(SECOND, '19700101', @to_ETC))
	select @from_int = CONVERT(INT, DATEDIFF(SECOND, '19700101', @from_ETC))

	--INSERT INTO #firstinsert_sl
	--select distinct vehicle_id, @from, @to, count(*)
	--from  Statistic_Log sl
	--where 1 = 1 --sl.Vehicle_ID = c.VEHICLE_ID 
	--	and dateadd(MINUTE, DATEPART(TZoffset, SYSDATETIMEOFFSET()), dateadd(s, sl.Log_Time, '1970-01-01')) between @from and @to  /*�������� ����� �� UTC � ������� �� ���������, ���� ������� ���� � ���*/  
	--group by vehicle_id
	
	INSERT INTO #firstinsert_sl
	select distinct vehicle_id, @from, @to, count(*)
	from Log_Time sl
	where 1 = 1  
		and sl.Log_Time between @from_int and @to_int  /*�������� ����� �� UTC � ������� �� ���������, ���� ������� ���� � ���*/  
	group by vehicle_id

	DELETE 
	FROM REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS
	WHERE DATE_FROM = @from and DATE_TO = @TO

	INSERT REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS
	SELECT vehicle_id, @from, @to, HITS
	FROM #firstinsert_sl

END
GO


