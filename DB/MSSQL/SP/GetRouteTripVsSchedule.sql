

--select * from route_trip
--where route_trip_id = 19 
----( 19, 24, 2)


/****** Object:  StoredProcedure [dbo].[GetRouteTripVsSchedule]    Script Date: 12/18/2008 09:35:20 ******/
-- Drop stored procedure if it already exists
IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetRouteTripVsSchedule' 
)
   DROP PROCEDURE dbo.GetRouteTripVsSchedule
GO


CREATE PROCEDURE [dbo].[GetRouteTripVsSchedule]
	@route_id int,
	@route_trip_id int
AS
	select distinct t.ROUTE_TRIP_ID from TRIP t
		join dbo.SCHEDULE_DETAIL sd on sd.SCHEDULE_DETAIL_ID = t.SCHEDULE_DETAIL_ID
		join dbo.SCHEDULE s on s.SCHEDULE_ID = sd.SCHEDULE_ID and s.ROUTE_ID = @route_id
	where t.ROUTE_TRIP_ID = @route_trip_id

