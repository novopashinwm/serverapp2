﻿IF (OBJECT_ID(N'[dbo].[ReportAboutDataAbsence]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[ReportAboutDataAbsence];
GO

CREATE PROCEDURE [dbo].[ReportAboutDataAbsence]
(
	@operatorID int,
	@dateFrom   datetime,
	@dateTo     datetime
)
AS
BEGIN
	SET NOCOUNT ON;

	--Отсутствие данных
	;WITH
	[absence] AS
	(
		SELECT
			 [vehicle_id]
			,[Type]
			,[Time]
		FROM [dbo].[v_vehicle_log_actuality]
		WHERE [Time] BETWEEN @dateFrom AND @dateTo
	),
	[admin] AS
	(
		SELECT
			odr.[department_id],
			odr.[operator_id]
		FROM [dbo].[v_operator_department_right] odr
			INNER JOIN [dbo].[RIGHT] rgt
				ON  rgt.[RIGHT_ID] = odr.[right_id]
				AND rgt.[NAME]     = 'SecurityAdministration'
				AND odr.[operator_id] NOT IN
				(
					SELECT
						ogo.[OPERATOR_ID]
					FROM [dbo].[OPERATORGROUP_OPERATOR] ogo
						INNER JOIN [dbo].[OPERATORGROUP] og
							ON og.[OPERATORGROUP_ID] = ogo.[OPERATORGROUP_ID]
					WHERE og.[NAME] = 'Sales'
				)
	)
	--Транспонирование выгрузки - по типу отсутствия данных
	SELECT
		 departmentID   = d.[DEPARTMENT_ID]
		,departmentName = d.[NAME]
		,emails =
		(
			SELECT
				c.[EMAIL] + '; '
			FROM [dbo].[EMAIL] c
				INNER JOIN [admin]
					ON [admin].[operator_id] = c.[Operator_ID]
			WHERE [admin].[department_id] = d.[DEPARTMENT_ID]
			AND   c.Confirmed = 1
			FOR XML PATH('')
		)
		,phones =
		(
			SELECT
				c.[Phone] + '; '
			FROM [dbo].[Phone] c
				INNER JOIN [admin]
					ON [admin].[operator_id] = c.[Operator_ID]
			WHERE [admin].[department_id] = d.[DEPARTMENT_ID]
			AND   c.[Confirmed] = 1
			FOR XML PATH('')
		)
		,vehicleID       = v.vehicle_ID
		,vehicleDeviceID =
			CASE WHEN EXISTS
			(
				SELECT
					*
				FROM [dbo].[v_operator_vehicle_right]
				WHERE [operator_id] = @operatorID
				AND   [vehicle_id]  = v.[VEHICLE_ID]
				AND   [right_id]   IN (21 /*ViewControllerDeviceID*/, 116 /*ChangeDeviceConfigBySMS*/)
			)
			THEN CONVERT(varchar(32), ci.[DEVICE_ID])
			ELSE NULL
			END
		,lastInsertTime = [INS].[Time]
		,lastLogTime    = [ANY].[Time]
		,lastGeoTime    = [GEO].[Time]
		,lastCANTime    = [CAN].[Time]
		,lastFuelTime   = [FUE].[Time]
	FROM [dbo].[VEHICLE] v
		INNER JOIN [dbo].[CONTROLLER] c
			ON c.[VEHICLE_ID] = v.[VEHICLE_ID]
		INNER JOIN [dbo].[CONTROLLER_INFO] ci
			ON ci.[CONTROLLER_ID] = c.[CONTROLLER_ID]
		LEFT OUTER JOIN [dbo].[department] d
			ON d.[DEPARTMENT_ID] = v.[DEPARTMENT]
		OUTER APPLY (SELECT * FROM [absence] WHERE [Vehicle_ID] = v.[Vehicle_ID] AND [Type] = N'Insert') [INS]
		OUTER APPLY (SELECT * FROM [absence] WHERE [Vehicle_ID] = v.[Vehicle_ID] AND [Type] = N'AnyLog') [ANY]
		OUTER APPLY (SELECT * FROM [absence] WHERE [Vehicle_ID] = v.[Vehicle_ID] AND [Type] = N'Geo')    [GEO]
		OUTER APPLY (SELECT * FROM [absence] WHERE [Vehicle_ID] = v.[Vehicle_ID] AND [Type] = N'CAN')    [CAN]
		OUTER APPLY (SELECT * FROM [absence] WHERE [Vehicle_ID] = v.[Vehicle_ID] AND [Type] = N'Fuel')   [FUE]
	WHERE ci.[device_id] IS NOT NULL
	AND v.[Vehicle_ID] IN (SELECT [vehicle_id] FROM [absence])
	AND 2 =
	(
		SELECT
			COUNT(*)
		FROM [dbo].[v_operator_vehicle_right]
		WHERE [vehicle_id]  = v.VEHICLE_ID 
		AND   [operator_id] = @operatorID 
		AND   [right_id]   IN (115 /*PathAccess*/, 102 /*VehicleAccess*/)
	)
	AND NOT EXISTS
	(
		SELECT
			1
		FROM [dbo].[v_operator_vehicle_right]
		WHERE [vehicle_id]  = v.[VEHICLE_ID]
		AND   [operator_id] = @operatorID
		AND   [right_id]    = 120 /*Ignore*/
	)
END
GO