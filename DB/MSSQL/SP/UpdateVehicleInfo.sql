set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'UpdateVehicleInfo' AND type = 'P')
    DROP PROCEDURE UpdateVehicleInfo
GO


CREATE PROCEDURE dbo.UpdateVehicleInfo
	@vehicleId as int,
	@ownerFirstName as varchar(50),
	@ownerSecondName as varchar(50),
	@ownerLastName as varchar(50),

	@ownerPhone1 as varchar(20),
	@ownerPhone2 as varchar(20),
	@ownerPhone3 as varchar(20),

	@garageNumber as varchar(15),
	@regNumber as varchar(15),
	@brand as varchar(100), -- aka vehicle_type

	@vehicleImage as image = null,
	@fuelSpentStandart int = 0,
	@fuelSpentStandardMH int = 0,
	@fuelTypeId int = NULL,
	@fuelTankVolume int = NULL,
	@vehicleKind int
AS
	declare @ownerId int
	set @ownerId = (select top 1 OWNER_ID from dbo.VEHICLE_OWNER where VEHICLE_ID = @vehicleId)
	
	--�������� ��������� �� ������� ������� (��������� ��� ������� � number = 10)
	exec dbo.InsertVehicleRuleValue @vehicle_id=@vehicleId, @rule_number=10,@value=@fuelSpentStandart;
	exec dbo.InsertVehicleRuleValue @vehicle_id=@vehicleId, @rule_number=12,@value=@fuelSpentStandardMH;

	update dbo.[vehicle]
	set
		public_number = @regNumber,
		GARAGE_NUMBER = @garageNumber,
		VEHICLE_TYPE = @brand,
		Fuel_Type_Id = @fuelTypeId,
		FUEL_TANK = @fuelTankVolume,
		Vehicle_Kind_ID = @vehicleKind
	where VEHICLE_ID = @vehicleId

	if(@ownerId is not null and @ownerId > 0)
	begin
		update dbo.OWNER
		set
		FIRST_NAME = @ownerFirstName,
		SECOND_NAME = @ownerSecondName,
		LAST_NAME = @ownerLastName,

		PHONE_NUMBER1 = @ownerPhone1,
		PHONE_NUMBER2 = @ownerPhone2,
		PHONE_NUMBER3 = @ownerPhone3
		where OWNER_ID = @ownerId
	end
	else
	begin
		insert into dbo.OWNER(FIRST_NAME, LAST_NAME, SECOND_NAME, PHONE_NUMBER1, PHONE_NUMBER2, PHONE_NUMBER3)
		values(@ownerFirstName, @ownerLastName, @ownerSecondName, @ownerPhone1, @ownerPhone2, @ownerPhone3)
		select @ownerId = scope_identity()
		
		insert into dbo.VEHICLE_OWNER(OWNER_ID, VEHICLE_ID, OWNER_NUMBER)
		values(@ownerId, @vehicleId, 1)
	end

	if(@vehicleImage is not null)
	begin
		declare @imageId int
		select @imageId = VEHICLE_PICTURE_ID from dbo.VEHICLE_PICTURE
		where VEHICLE_ID = @vehicleId
		if(@imageId is not null)
		begin
			update dbo.VEHICLE_PICTURE
			set PICTURE = @vehicleImage
			where VEHICLE_PICTURE_ID = @imageId
		end
		else
		begin
			insert into dbo.VEHICLE_PICTURE(VEHICLE_ID, PICTURE)
			values(@vehicleId, @vehicleImage)
			select @imageId = scope_identity()
		end
	end
	