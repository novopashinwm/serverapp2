if exists (
	select *
		from sys.procedures
		where name = 'ClearPhone'
)
	drop procedure ClearPhone
go

create procedure ClearPhone
(
	@phone varchar(32)
)
as

set nocount on

declare @contact table (id int, type int, value varchar(255))
insert into @contact (type, value) values 
	(2, @phone)

insert into @contact (type, value)
	select ac.Type, ac.Value
		from @contact c
		join Contact pc on pc.Type = c.type and pc.Value = c.value
		join Contact ac on ac.Demasked_ID = pc.ID

update t set id = c.id 
	from @contact t
	join Contact c on c.Type = t.type and c.Value = t.value	

select * from @contact

declare @contact_id int

while 1=1
begin
	set @contact_id = (select top(1) c.id from @contact c where c.id is not null order by id asc)
	if @contact_id is null
		break;

	update operator set login = null where login in (select value from @contact)
	update controller set phone = null where dbo.GetPhoneInvariant(Phone) in (select value from @contact)

	update Contact set Demasked_ID = null where Demasked_ID = @contact_id

	delete CompoundRule_Operator_Contact where contact_id = @contact_id
	update controller set PhoneContactID = null where PhoneContactID = @contact_id
	delete message_contact where contact_id = @contact_id
	delete operator_contact where contact_id = @contact_id

	update d 
		set extid = null 
		from DEPARTMENT d
		join Asid a on a.Department_ID = d.DEPARTMENT_ID
		where a.contact_id = @contact_id
		
	update asid set contact_id = null, terminal_device_number = null, department_id = null where contact_id = @contact_id

	delete Contact where ID = @contact_id
	
	delete @Contact where ID = @contact_id
end