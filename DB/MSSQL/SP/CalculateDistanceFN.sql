﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateDistanceFN]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CalculateDistanceFN]

GO

create FUNCTION CalculateDistanceFN
(
	@from int,
	@to int,
	@vehicle_id int
)
RETURNS real
AS
BEGIN
/*
	declare @dx float(53);
	declare @dy float(53);
	select @dx = dbo.GeoToPlanarX(@x1) - dbo.GeoToPlanarX(@x2);
	select @dy = dbo.GeoToPlanarY(@y1) - dbo.GeoToPlanarY(@y2);

	return Sqrt(@dx * @dx + @dy * @dy);
*/
	
	declare @odometer1 bigint;
	declare @odometer2 bigint;

	--Последний актуальный отсчет пробега вне интервала
/* Убрана точка вне пределов запроса (пердыдущая точка пробега, из-за ошибки если эта точка далеко)
	select top 1 @odometer1 = odometer
		from dbo.Statistic_Log (nolock)
		where  vehicle_id = @vehicle_id and log_time <= @from
		order by log_time desc
*/
	--Первый отсчет пробега из интервала
	if @odometer1 is null
		select top 1 @odometer1 = odometer
			from dbo.Statistic_Log (nolock)
			where  vehicle_id = @vehicle_id and log_time between @from and @to
			order by log_time asc

	--Последний в наличии отсчет пробега
	select top 1 @odometer2 = odometer
		from dbo.Statistic_Log (nolock)
		where  vehicle_id = @vehicle_id and log_time between @from and @to
		order by log_time desc
		
	-- см. Аналогичную реализацию в классе: FORIS.TSS.BusinessLogic.DTO.Historical.FullLog
	-- метод: GetRunDistance(int int)
	return (isnull(isnull(@odometer2,@odometer1),0) - isnull(@odometer1,0)) / 100;
END

GO
