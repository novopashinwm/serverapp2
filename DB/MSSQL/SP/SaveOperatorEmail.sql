IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'SaveOperatorEmail' 
)
   DROP PROCEDURE [dbo].[SaveOperatorEmail]
GO

CREATE procedure [dbo].[SaveOperatorEmail]
(
	@OPERATOR_ID as int,
	@EMAIL nvarchar(50)
)
as
begin
	declare @prevEmail nvarchar(300);
	declare @operName nvarchar(300);

	select @prevEmail = Email, @operName = [NAME] 
	from dbo.operator 
	where OPERATOR_ID = @OPERATOR_ID;
	
	update dbo.Email
	set Email = @EMAIL
	where Email = @prevEmail and [Name] = @operName

	update dbo.operator set EMAIL = @EMAIL
	where OPERATOR_ID = @OPERATOR_ID
end




