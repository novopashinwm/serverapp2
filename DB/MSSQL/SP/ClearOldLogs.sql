﻿if exists (
	select *
		from sys.procedures 
		where name = 'ClearOldLogs'
)
	drop procedure ClearOldLogs
go

--exec ClearOldLogs  '2012-01-01', 1
create procedure ClearOldLogs
(
	@deleteToDateTime datetime = null,
	@debug            bit      = null
)
as

if (@debug is null)
	set @debug = 0

if (@debug = 0)
	set nocount on

-- Если дата не указана, то будут удалены данные - по умолчанию - за последний год
if @deleteToDateTime is null
	set @deleteToDateTime = GETUTCDATE() - 1*365
	
CREATE TABLE #vehicle_ids (
	[vehicle_id] [int] PRIMARY KEY CLUSTERED 
)

insert into #vehicle_ids
	select t1.Vehicle_ID
	from (
		select id_from = v.vehicle_id + 1, id_to = v1.VEHICLE_ID - 1
			from VEHICLE v
			cross apply (
				select top(1) v1.VEHICLE_ID
					from vehicle v1
					where v1.VEHICLE_ID > v.VEHICLE_ID
					order by v1.VEHICLE_ID asc) v1
			where v1.VEHICLE_ID - v.VEHICLE_ID > 1
	) t
	cross apply (
		--TODO: выбирать все пропущенные ID, а не только первый, для которого есть записи в логе
		select top(1) lt.Vehicle_ID from Log_Time lt with (nolock) where lt.Vehicle_ID between t.id_from and t.id_to order by lt.Vehicle_ID asc) t1

--Удаляет записи из больших таблиц (XXX_Log)

declare @interval int, @notExistInterval int, @to int, @dtTo datetime, @limitDT int;
select @interval = 555, @notExistInterval = 5555;


select @limitDT = datediff(ss, cast('1970-01-01' as datetime), @deleteToDateTime)

set @to = @notExistInterval + (select min(Min_Log_Time) 
									from (
										select v.vehicle_id
											, Min_Log_Time = (select top(1) Log_Time from Log_Time lt (nolock) where lt.Vehicle_ID = v.Vehicle_ID  order by Log_Time asc)
											from #vehicle_ids v
									) t
									where t.Min_Log_Time is not null)

if @debug = 1
begin
	print convert(varchar(40), dateadd(ss, @to, '1970-01-01'));
	print convert(varchar(40), dateadd(ss, @limitDT, '1970-01-01'));
end

declare @sql nvarchar(max);

set @sql = '
	if (@debug = 1)
		print ''geo_log''
	delete from dbo.geo_log
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to

	if (@debug = 1)
		print ''statistic_log''
	delete from dbo.statistic_log
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to

	if (@debug = 1)
		print ''gps_log''
	delete from dbo.gps_log
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to

	if (@debug = 1)
		print ''can_info''
	delete from dbo.can_info
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to

	if (@debug = 1)
		print ''controller_sensor_log''
	delete from dbo.controller_sensor_log
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to
	
	if (@debug = 1)
		print ''geo_log_ignored''
	delete from dbo.geo_log_ignored
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to

	if (@debug = 1)
		print ''log_time''
	delete from dbo.log_time
	where vehicle_id in (select vehicle_id from #vehicle_ids) 
	and log_time < @to
';

--Удаляем записи для несуществующих машин

while @to < @limitDT
begin
	
	set @dtTo = dateadd(ss, @to, '1970-01-01');
	exec sp_executesql @sql, N'@to int, @debug bit', @to = @to, @debug = @debug

	set @to = @notExistInterval + (select min(Min_Log_Time) 
										from (
											select v.vehicle_id
												, Min_Log_Time = (select top(1) Log_Time from Log_Time lt (nolock) where lt.Vehicle_ID = v.Vehicle_ID  order by Log_Time asc)
												from #vehicle_ids v
										) t
										where t.Min_Log_Time is not null)


	if (@debug = 1)
		print 'not existing ' + convert(varchar(40), @dtTo);
end;


if DATEDIFF(year, @deleteToDateTime, GETUTCDATE()) < 1
begin
	if (@debug = 1)
		print 'Младше года не удаляются ни при каких обстоятельствах'

	return
end

--Удаляем записи для существующих машин

delete #vehicle_ids
insert into #vehicle_ids
	select v.vehicle_id
		from vehicle v
		where v.Vehicle_ID not in (select e.RealVehicleID from Emulator e) --Источники данных эмулируемых устройств не удаляются

set @to = @interval + (select min(Min_Log_Time) 
							from (
								select v.vehicle_id
									, Min_Log_Time = (select top(1) Log_Time from Log_Time lt (nolock) where lt.Vehicle_ID = v.Vehicle_ID  order by Log_Time asc)
									from Vehicle v
							) t
							where t.Min_Log_Time is not null)

while @to < @limitDT
begin
	
	set @dtTo = dateadd(ss, @to, '1970-01-01');
	exec sp_executesql @sql, N'@to int, @debug bit', @to = @to, @debug = @debug

	set @to = @interval + (select min(t.Log_Time) 
							from #vehicle_ids v 
							cross apply (
								select top(1) Log_Time 
									from Log_Time lt (nolock) 
									where lt.Vehicle_ID = v.Vehicle_ID  
									order by Log_Time asc
							) t)

	if (@debug = 1)
		print convert(varchar(40), @dtTo);
end;

drop table #vehicle_ids