/****** Object:  StoredProcedure [dbo].[SubstitutionsInfoGet]    Script Date: 07/14/2010 14:37:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
--жи■бїЇ║иЁ Ї√а ■квїкЁ Ї°╕ їквїи╕·°∙ иЁ ■ик
--LЄк■и: +иїёї¤¤°·■Є
--------------------------------------------------------------------------------------------

ALTER procedure [dbo].[SubstitutionsInfoGet]
	@ReportDateTime	datetime,	---ЁкЁ ° Єиї№а, ¤Ё ·■к■иvї киїё║їк╕а  ■√║в°к╣ ■квїк;
	@MotorcadeID	int,		--ID ЁЄк■·■√■¤¤v (єи║  v TT), Ї√а ·■к■и■∙ ╕ки■°к╕а ■квїк.
	@operator_id int = null
as


--------------------------------------------------------------------------------------------
/*
--Tї╕к
declare	
	@ReportDateTime	datetime,	---ЁкЁ ° Єиї№а, ¤Ё ·■к■иvї киїё║їк╕а  ■√║в°к╣ ■квїк;
	@MotorcadeID	int		--ID ЁЄк■·■√■¤¤v (єи║  v TT), Ї√а ·■к■и■∙ ╕ки■°к╕а ■квїк.

set @ReportDateTime = '2006-12-10 21:00:00' 
set @MotorcadeID = 0 --6
*/
--------------------------------------------------------------------------------------------

-- -Ёкv Ї√а жT ° Є■Ї°кї√а
declare 
	@wbhDate 	as datetime, 
	@wbDate 	as datetime, 
	@wbhDateTime 	as datetime,
	-- жЁў¤°бЁ Є  їи°■Ї √їк¤їє■ ° ў°№¤їє■ Єиї№ї¤° 
	@TimeDiff 	as int

set @wbhDateTime = @ReportDateTime
set @wbhDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime, 1), 1)
set @wbDate = CONVERT(datetime, CONVERT(varchar, @wbhDateTime + 1, 1), 1)
set @TimeDiff = datediff(s, @wbhDateTime, @wbDate)


--------------------------------------------------------------------------------------------
-- жї∙╕v
--------------------------------------------------------------------------------------------

-- T╕ї иї∙╕v  ■ иЁ╕ °╕Ё¤°ж ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║
select	distinct 	
	RT.EXT_NUMBER 		as ROUTE,
	SC.*,
	SCD.SCHEDULE_DETAIL_ID,
	SCD.SHIFT_ID,
	SCD.BEGIN_TIME 		as SHIFT_BEGIN,
	SCD.END_TIME 		as SHIFT_END,
	TR.TRIP_ID,
	TR.TRIP_KIND_ID,
	TR.BEGIN_TIME 		as TRIP_BEGIN,
	TR.END_TIME 		as TRIP_END,
	TR.TRIP
into	
	#ScheduleTrips
from 
	dbo.TRIP as TR
	inner join dbo.SCHEDULE_DETAIL as SCD on TR.SCHEDULE_DETAIL_ID = SCD.SCHEDULE_DETAIL_ID
	inner join dbo.SCHEDULE as SC on SCD.SCHEDULE_ID = SC.SCHEDULE_ID
	inner join dbo.ROUTE as RT on SC.ROUTE_ID = RT.ROUTE_ID
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where
	SC.DAY_KIND_ID = dbo.DayKind(@wbDate, SC.ROUTE_ID)
	--and TR.TRIP_KIND_ID in (5, 6) 
	and (@operator_id is null or rr.operator_id = @operator_id)
order by 
	RT.EXT_NUMBER, SC.EXT_NUMBER, TR.BEGIN_TIME


--T╕ї Її∙╕кЄ°кї√╣¤vї иї∙╕v ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║
select	distinct	
	WBT.*,
	VH.VEHICLE_ID,
	VH.GARAGE_NUMBER,
	VHG.VEHICLEGROUP_ID,
	DR.DRIVER_ID,
	DR.EXT_NUMBER 		as DRIVER_NUM,
	DR.[NAME]		as DRIVER_NAME,
	DR.SHORT_NAME		as DRIVER_SURNAME,
	WB.SHIFT_ID		as DRIVER_SHIFT,
	WB.BEGIN_TIME		as DRIVER_BEGIN,
	WB.END_TIME		as DRIVER_END,
	OP.[NAME]		as OPERATOR_NAME,
	ST.ROUTE as ST_ROUTE, 
	ST.EXT_NUMBER as ST_EXT_NUMBER
into 
	#WBTrips
from 
	dbo.WB_TRIP as WBT
	-- T■Ї°кї√╣
	left join dbo.WAYBILL as WB on WBT.WAYBILL_ID = WB.WAYBILL_ID
	left join dbo.DRIVER as DR on WB.DRIVER_ID = DR.DRIVER_ID
	left join v_operator_driver_right drr on drr.driver_id = DR.DRIVER_ID
	-- TT
	left join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
	left join dbo.VEHICLE as VH on WBH.VEHICLE_ID = VH.VEHICLE_ID
	left join v_operator_vehicle_right vr on vr.vehicle_id = VH.VEHICLE_ID
	left join dbo.VEHICLEGROUP_VEHICLE as VHG on VH.VEHICLE_ID = VHG.VEHICLE_ID
	left join v_operator_vehicle_groups_right vgr on vgr.vehiclegroup_id = VHG.VEHICLEGROUP_ID
	-- жЁ╕ °╕Ё¤°ї
	left join #ScheduleTrips as ST on WBT.TRIP_ID = ST.TRIP_ID
	-- TripKind
	--inner join dbo.TRIP_KIND as TK on WBT.[TRIP_KIND_ID] = TK.[TRIP_KIND_ID]
	-- + їиЁк■и
	left join dbo.OPERATOR as OP on WBT.[OPERATOR_ID] = OP.[OPERATOR_ID]
where	
	(WBH.[WAYBILL_DATE] = @wbhDateTime
	or CONVERT(datetime, CONVERT(varchar, WB.[WAYBILL_DATE], 1), 1) = @wbDate)
	--and WBH.CANCELLED != 1
	--and WBT.[TRIP_KIND_ID] in (5, 6, 8, 9, 11, 12, 13, 15, 16)
	--проверка прав
	and (
		@operator_id is null
		or (
			(VH.VEHICLE_ID is null or vr.operator_id = @operator_id)
			or (VHG.VEHICLE_ID is null or vgr.operator_id = @operator_id)
		)
	)
order by 
	ST.ROUTE, ST.EXT_NUMBER, WBT.[BEGIN_TIME]


-- жЁ·иvкvї иї∙╕v
select distinct 
	ST.SCHEDULE_ID as SCHEDUL_ID,
	WBT.*
into
	#ClosedTrips
from 
	#WBTrips as WBT
	inner join #ScheduleTrips as ST on WBT.TRIP_ID = ST.TRIP_ID
where 
	WBT.WAYBILL_HEADER_ID is not null 
	and WBT.WAYBILL_ID is not null 


-- =їўЁ·иvкvї иї∙╕v
select distinct 
	ST.*
into
	#NonClosedTrips
from 
	#ScheduleTrips as ST
where 
	ST.TRIP_ID not in (select TRIP_ID from #ClosedTrips)
	and ST.TRIP_KIND_ID in (5, 6) 


-- =їўЁ·иvкvї ° ўЁ·иvкvї иї∙╕v  ■ иЁ╕ °╕Ё¤°ж
select distinct
	SCT.ROUTE,
	SCT.SCHEDULE_ID,
	SCT.EXT_NUMBER,
	SCT.SHIFT_BEGIN,
	SCT.SHIFT_END,
	SCT.TRIP_ID		as TR_ID,
	SCT.TRIP_KIND_ID	as TR_KIND,
	SCT.TRIP_BEGIN 		as TR_BEGIN,
	SCT.TRIP_END 		as TR_END,
	SCT.TRIP		as TR_NUM,		 
	CT.*
into
	#SCTrips_WBTrips
from
	#ScheduleTrips as SCT
	left join #ClosedTrips as CT on SCT.TRIP_ID = CT.TRIP_ID
order by 
	SCT.ROUTE, SCT.EXT_NUMBER, SCT.[TRIP_BEGIN]


--------------------------------------------------------------------------------------------
-- T┐■Їv °  їиї·√жвї¤°а  ■ иЁ╕ °╕Ё¤°ж
--------------------------------------------------------------------------------------------

--TЁё√°бЁ ╕┐■Ї■Є °  їиї·√жвї¤°∙  ■ иЁ╕ °╕Ё¤°ж
create table #AllOut(	
	--id int identity primary key, 
	num_id			int,
	ROUTE			nvarchar(20), 	--collate SQL_Latin1_General_CP1251_CI_AS, 
	SCHEDULE_ID		int,
	EXT_NUMBER		nvarchar(20), 	--collate SQL_Latin1_General_CP1251_CI_AS, 
	--
	TRIP_ID			int,		
	TRIP_KIND		int,
	TRIP_BEGIN		int,
	TRIP_END		int,
	PRE_WBTRIP_END		datetime,
	TRIP_NUM		int,		 
	--
	WB_TRIP_ID		int,
	WBTRIP_KIND		int,
	WBTRIP_BEGIN		datetime,
	WBTRIP_END		datetime,
	WBTRIP_NUM		int,	
	COMMENT			nvarchar(50),	
	OPERATOR_NAME 		nvarchar(50),	
	--
	WAYBILL_HEADER_ID	int,
	--VEHICLE_ID		int,
	GARAGE_NUMBER		nvarchar(50),
	VEHICLEGROUP_ID		int,
	--
	WAYBILL_ID		int,
	--DRIVER_ID		int,	
	DRIVER_NUM 		nvarchar(50),	
	DRIVER_SURNAME 		nvarchar(50),	
	DRIVER_NAME		nvarchar(50)	
	)

/*--
create table #AllOutPre(	
	--id int identity primary key, 
	num_id			int,
	ROUTE			nvarchar(20), 	--collate SQL_Latin1_General_CP1251_CI_AS, 
	SCHEDULE_ID		int,
	EXT_NUMBER		nvarchar(20), 	--collate SQL_Latin1_General_CP1251_CI_AS, 
	--
	TRIP_ID			int,		
	TRIP_KIND		int,
	TRIP_BEGIN		int,
	TRIP_END		int,
	TRIP_NUM		int,		 
	--
	WB_TRIP_ID		int,
	WBTRIP_KIND		int,
	WBTRIP_BEGIN		datetime,
	WBTRIP_END		datetime,
	WBTRIP_NUM		int,	
	COMMENT			nvarchar(50),	
	OPERATOR_NAME 		nvarchar(50),	
	--
	WAYBILL_HEADER_ID	int,
	--VEHICLE_ID		int,
	GARAGE_NUMBER		nvarchar(50),
	VEHICLEGROUP_ID		int,
	--
	WAYBILL_ID		int,
	--DRIVER_ID		int,	
	DRIVER_NUM 		nvarchar(50),	
	DRIVER_SURNAME 		nvarchar(50),	
	DRIVER_NAME		nvarchar(50)	
	)
*/

--·║и╕■и  ■ ки° Ё№ иЁ╕ °╕Ё¤°а
declare 
	@schedule_id 		int,
	@shift_begin		int,
	@shift_end		int,
	@trip_id		int,
	@trip_begin		int,
	@wb_trip_end		datetime,
	@wb_trip_id		int,
	@trip_kind_id		int,
	@wbh_id			int,
	@wb_id			int,
	--
	@schedule_id_pre 	int,
	@shift_begin_pre	int,
	@shift_end_pre		int,
	@trip_id_pre		int,
	@trip_begin_pre		int,
	@wb_trip_end_pre	datetime,
	@wb_trip_id_pre		int,
	@trip_kind_id_pre	int,
	@wbh_id_pre		int,
	@wb_id_pre		int,
	--
	@num_id			int,
	@SkipCount		int
	

set 	@num_id			= 0

/*set	@schedule_id		= 0
set	@shift_begin		= 0
set	@shift_end		= 0
set	@trip_id		= 0
set	@trip_begin		= 0
set	@wb_trip_id		= 0 
set	@trip_kind_id		= 0
set	@wbh_id			= 0
set	@wb_id			= 0 

set	@schedule_id_pre	= 0
set	@shift_begin_pre	= 0
set	@shift_end_pre		= 0
set	@trip_id_pre		= 0
set	@trip_begin_pre		= 0
set	@wb_trip_id_pre		= 0 
set	@trip_kind_id_pre	= 0
set	@wbh_id_pre		= 0
set	@wb_id_pre		= 0 

set	@SkipCount		= 0 */

DECLARE cursor_tr CURSOR FOR 
	SELECT 
		SCHEDULE_ID, SHIFT_BEGIN, SHIFT_END, TR_ID, TR_BEGIN, END_TIME, WB_TRIP_ID, TR_KIND, WAYBILL_HEADER_ID, WAYBILL_ID 
	FROM 
		#SCTrips_WBTrips 
	WHERE 
		TR_KIND in (5, 6)
	ORDER BY 
		ROUTE, SCHEDULE_ID, TR_BEGIN
OPEN cursor_tr 
FETCH NEXT FROM cursor_tr into @schedule_id, @shift_begin, @shift_end, @trip_id, @trip_begin, @wb_trip_end, @wb_trip_id, @trip_kind_id, @wbh_id, @wb_id
WHILE @@FETCH_STATUS = 0
BEGIN
	if(@schedule_id = @schedule_id_pre)
	begin
		if((@wb_trip_id is null and @wb_trip_id_pre is not null) 
			or @wbh_id != @wbh_id_pre 
			or @wb_id != @wb_id_pre 
			--or (@wb_id != @wb_id_pre and @trip_begin != @shift_begin and @trip_begin != @shift_end_pre) 
			)
		--if(@trip_kind_id in (5, 6))
		begin
			
			set @num_id = @num_id + 1
			
			insert #AllOut( 
				num_id,
				ROUTE, 
				SCHEDULE_ID,
				EXT_NUMBER, 
				--
				TRIP_ID,		
				TRIP_KIND,
				TRIP_BEGIN,
				TRIP_END,
				PRE_WBTRIP_END,
				TRIP_NUM,		 
				--
				WB_TRIP_ID,
				WBTRIP_KIND,
				WBTRIP_BEGIN,
				WBTRIP_END,
				WBTRIP_NUM,	
				COMMENT,	
				OPERATOR_NAME,	
				--
				WAYBILL_HEADER_ID,
				--VEHICLE_ID,
				GARAGE_NUMBER,
				VEHICLEGROUP_ID,
				--
				WAYBILL_ID,
				--DRIVER_ID,	
				DRIVER_NUM,	
				DRIVER_SURNAME,	
				DRIVER_NAME--,	
				)	
			select distinct
				@num_id,
				ROUTE, 
				SCHEDULE_ID,
				EXT_NUMBER, 
				--
				TR_ID,		
				TR_KIND,
				TR_BEGIN,
				TR_END,
				@wb_trip_end_pre, --PRE_WBTRIP_END,
				TR_NUM,		 
				--
				WB_TRIP_ID,
				TRIP_KIND_ID,
				BEGIN_TIME,
				END_TIME,
				TRIP,	
				COMMENT,	
				OPERATOR_NAME,	
				--
				WAYBILL_HEADER_ID,
				--VEHICLE_ID,
				GARAGE_NUMBER,
				VEHICLEGROUP_ID,
				--
				WAYBILL_ID,
				--DRIVER_ID,	
				DRIVER_NUM,	
				DRIVER_SURNAME,	
				DRIVER_NAME
			from 
				#SCTrips_WBTrips 
			where 
				SCHEDULE_ID = SCHEDULE_ID and TR_ID = @trip_id --and WB_TRIP_ID = @wb_trip_id
			
			/*--
			insert #AllOutPre(  
				num_id,
				ROUTE, 
				SCHEDULE_ID,
				EXT_NUMBER, 
				--
				TRIP_ID,		
				TRIP_KIND,
				TRIP_BEGIN,
				TRIP_END,
				TRIP_NUM,		 
				--
				WB_TRIP_ID,
				WBTRIP_KIND,
				WBTRIP_BEGIN,
				WBTRIP_END,
				WBTRIP_NUM,	
				COMMENT,	
				OPERATOR_NAME,	
				--
				WAYBILL_HEADER_ID,
				--VEHICLE_ID,
				GARAGE_NUMBER,
				VEHICLEGROUP_ID,
				--
				WAYBILL_ID,
				--DRIVER_ID,	
				DRIVER_NUM,	
				DRIVER_SURNAME,	
				DRIVER_NAME--,	
				)	
			select distinct 
				@num_id,
				ROUTE, 
				SCHEDULE_ID,
				EXT_NUMBER, 
				--
				TR_ID,		
				TR_KIND,
				TR_BEGIN,
				TR_END,
				TR_NUM,		 
				--
				WB_TRIP_ID,
				TRIP_KIND_ID,
				BEGIN_TIME,
				END_TIME,
				TRIP,	
				COMMENT,	
				OPERATOR_NAME,	
				--
				WAYBILL_HEADER_ID,
				--VEHICLE_ID,
				GARAGE_NUMBER,
				VEHICLEGROUP_ID,
				--
				WAYBILL_ID,
				--DRIVER_ID,	
				DRIVER_NUM,	
				DRIVER_SURNAME,	
				DRIVER_NAME
			from 
				#SCTrips_WBTrips 
			where 
				SCHEDULE_ID = SCHEDULE_ID and TR_ID = @trip_id_pre --and WB_TRIP_ID = @wb_trip_id*/
			
		end
	end

	set @schedule_id_pre = @schedule_id
	set @shift_begin_pre = @shift_begin
	set @shift_end_pre = @shift_end
	set @trip_id_pre = @trip_id
	set @trip_begin_pre = @trip_begin
	set @wb_trip_end_pre = @wb_trip_end
	set @wb_trip_id_pre = @wb_trip_id
	set @trip_kind_id_pre = @trip_kind_id
	set @wbh_id_pre = @wbh_id
	set @wb_id_pre = @wb_id

	FETCH NEXT FROM cursor_tr into @schedule_id, @shift_begin, @shift_end, @trip_id, @trip_begin, @wb_trip_end, @wb_trip_id, @trip_kind_id, @wbh_id, @wb_id
END
CLOSE cursor_tr
DEALLOCATE cursor_tr

-- T┐■Ї - ўЁ№ї¤Ё °√° Є■╕╕кЁ¤■Є√ї¤°ї
/*select 
	o.ROUTE, 
	o.SCHEDULE_ID,
	o.EXT_NUMBER, 
	o.TRIP_ID,
	isnull(o.WBTRIP_BEGIN, dateadd(s, o.TRIP_BEGIN, @wbDate))	as BeginOut,  
	--null								as VehicleIDOut,
	o1.GARAGE_NUMBER						as GarageNumOut,
	o1.VEHICLEGROUP_ID						as VehicleGroupOut,
	--null								as DriverIDOut,
	o1.DRIVER_NUM							as DriverNumOut,	
	o1.DRIVER_SURNAME						as DriverSurnameOut, 
	o1.DRIVER_NAME							as DriverNameOut,
	null								as TripKindIDOut,
	null								as CommentOut,
	0								as OutInBegin,
	null								as OperatorNameOut,
	--
	o.WBTRIP_BEGIN							as BeginRe,  
	--o.VEHICLE_ID							as VehicleIDRe,
	o.GARAGE_NUMBER							as GarageNumRe,
	o.VEHICLEGROUP_ID						as VehicleGroupRe,
	--o.DRIVER_ID							as DriverIDRe,
	o.DRIVER_NUM							as DriverNumRe,	
	o.DRIVER_SURNAME 						as DriverSurnameRe, 
	o.DRIVER_NAME							as DriverNameRe,
	null								as TripKindIDRe,
	null								as CommentRe,
	0								as TripSkip,
	0								as OtherMotorcade,
	o.OPERATOR_NAME							as OperatorNameRe
into 
	#OutRe
from
	#AllOut as o
	left join #AllOutPre as o1 on o1.num_id = o.num_id
	--left join #ClosedTrips as ct on 
--order by 
	--o.ROUTE, o.EXT_NUMBER, o.TRIP_BEGIN 
*/
select 
	o.ROUTE, 
	o.SCHEDULE_ID,
	o.EXT_NUMBER, 
	o.TRIP_ID,
	o.TRIP_NUM,
	--o.TRIP_BEGIN							as BeginOut,  
	--dateadd(s, o.TRIP_BEGIN, @wbDate)				as BeginOut,  
	dateadd(s, -@TimeDiff, dateadd(s, o.TRIP_BEGIN, @wbDate))	as BeginOut,  
	o.PRE_WBTRIP_END						as WBEndPre,  
	--null								as VehicleIDOut,
	ctPre.GARAGE_NUMBER						as GarageNumOut,
	ctPre.VEHICLEGROUP_ID						as VehicleGroupOut,
	--null								as DriverIDOut,
	ctPre.DRIVER_NUM						as DriverNumOut,	
	ctPre.DRIVER_SURNAME						as DriverSurnameOut, 
	ctPre.DRIVER_NAME						as DriverNameOut,
	null								as TripKindIDOut,
	null								as CommentOut,
	0								as OutInBegin,
	null								as OperatorNameOut,
	--
	isnull(o.WBTRIP_BEGIN, ctPost.BEGIN_TIME) 			as BeginRe,  
	ctPost.TRIP_ID							as TripIDRe,
	ctPost.TRIP							as TripNumRe,
	--o.VEHICLE_ID							as VehicleIDRe,
	isnull(o.GARAGE_NUMBER, ctPost.GARAGE_NUMBER)			as GarageNumRe,
	isnull(o.VEHICLEGROUP_ID, ctPost.VEHICLEGROUP_ID)		as VehicleGroupRe,
	--o.DRIVER_ID							as DriverIDRe,
	isnull(o.DRIVER_NUM, ctPost.DRIVER_NUM)				as DriverNumRe,	
	isnull(o.DRIVER_SURNAME, ctPost.DRIVER_SURNAME)			as DriverSurnameRe, 
	isnull(o.DRIVER_NAME, ctPost.DRIVER_NAME)			as DriverNameRe,
	null								as TripKindIDRe,
	null								as CommentRe,
	0								as TripSkip,
	0								as OtherMotorcade,
	isnull(o.OPERATOR_NAME, ctPost.OPERATOR_NAME)			as OperatorNameRe
into 
	#OutRe
from
	#AllOut as o
	--T■░ї√
	left join #ClosedTrips as ctPre 
		on ctPre.SCHEDUL_ID = o.SCHEDULE_ID
		and ctPre.TRIP_ID = (select top 1 max(ct.TRIP_ID) from #ClosedTrips as ct where ct.SCHEDUL_ID = o.SCHEDULE_ID and ct.TRIP_ID < o.TRIP_ID and ct.TRIP < o.TRIP_NUM  )
	--жЁ№ї¤Ё Є■╕╕кЁ¤■Є√ї¤°ї
	left join #ClosedTrips as ctPost 
		on ctPost.SCHEDUL_ID = o.SCHEDULE_ID
		and ctPost.TRIP_ID = (select top 1 min(ct.TRIP_ID) from #ClosedTrips as ct where ct.SCHEDUL_ID = o.SCHEDULE_ID and ct.TRIP_ID > o.TRIP_ID and ct.TRIP > o.TRIP_NUM  )
order by 
	o.ROUTE, o.EXT_NUMBER, o.TRIP_BEGIN 


update 
	#OutRe
Set 
	--T┐■Ї ¤Ё Єv ║╕·ї?
	OutInBegin = (
		case when (
			select count(*) from #SCTrips_WBTrips as t
			where t.SCHEDULE_ID = #OutRe.SCHEDULE_ID and t.TR_NUM < #OutRe.TRIP_NUM and t.TR_ID < #OutRe.TRIP_ID
			) > 0 
		then 0 else 1 end
		),
	--ж■кїиа¤■ иї∙╕■Є
	TripSkip = (
		case when #OutRe.BeginRe is not null 
		then
			(select count(*) from #NonClosedTrips as nct 
			where nct.SCHEDULE_ID = #OutRe.SCHEDULE_ID 
				and nct.TRIP_ID between #OutRe.TRIP_ID and #OutRe.TripIDRe  
				and nct.TRIP between #OutRe.TRIP_NUM and #OutRe.TripNumRe) 
		else
			(select count(*) from #NonClosedTrips as nct where nct.SCHEDULE_ID = #OutRe.SCHEDULE_ID) 
		end
		),
	--жЁ№ї¤Ё °ў Їи║є■∙ ЁЄк■·■√■¤¤v?
	OtherMotorcade = (case when VehicleGroupOut != VehicleGroupRe then 1 else 0 end)

select * from #OutRe

drop table #OutRe 
drop table #AllOut 
drop table #SCTrips_WBTrips
drop table #NonClosedTrips
drop table #ClosedTrips
drop table #WBTrips
drop table #ScheduleTrips


