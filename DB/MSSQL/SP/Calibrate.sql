CreateProcedure 'Calibrate'
go
alter procedure Calibrate
(
	@vehicle_id int,
	@controller_sensor_legend_number int,
	@controller_sensor_number int,
	@param dbo.Calibrate_Param readonly
)
as

declare @controller_id int = (select controller_id from controller where vehicle_id = @vehicle_id)
if (@controller_id is null)
	return;

create table #t (ID int identity primary key clustered, RawValue bigint, ActualValue numeric(18, 9))

insert into #t (RawValue, ActualValue)
	select p.RawValue, p.ActualValue 
		from @param p
		order by p.RawValue

delete csm
	from dbo.Controller_Sensor_Map csm
	join controller_sensor cs on cs.controller_sensor_id = csm.controller_sensor_id
	join controller_sensor_legend csl on csl.controller_sensor_legend_id = csm.controller_sensor_legend_id
	where csm.controller_id = @controller_id
	  and cs.NUMBER = @controller_sensor_number
	  and csl.Number = @controller_sensor_legend_number

insert into dbo.Controller_Sensor_Map (Controller_ID, Controller_Sensor_ID, Controller_Sensor_Legend_ID, Multiplier, Constant, Min_Value, Max_Value)
	select 
		@controller_id, 
		cs.Controller_Sensor_ID,
		csl.Controller_Sensor_Legend_ID,
		(t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue),
		(t.ActualValue - (t.ActualValue - pt.ActualValue) / (t.RawValue - pt.RawValue) * t.RawValue),
		case pt.ID when 1 then pt.RawValue else pt.RawValue + 1 end,
		t.RawValue
		from Controller c
		join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID and cs.Number = @controller_sensor_number
		join Controller_Sensor_Legend csl on csl.Number = @controller_sensor_legend_number
		join #t t on 1 = 1
		join #t pt on pt.ID = t.ID - 1
		where c.Controller_ID = @controller_id
		
drop table #t

		  
