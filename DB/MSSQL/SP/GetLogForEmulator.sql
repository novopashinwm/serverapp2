IF (OBJECT_ID(N'[dbo].[GetLogForEmulator]') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetLogForEmulator];
GO

CREATE PROCEDURE [dbo].[GetLogForEmulator]
(
	@logParams Vehicle_Log_Time_Span_Param readonly
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		logPars.[Vehicle_ID],
		logPars.[Log_Time_From],
		logPars.[Log_Time_To],
		srcData.[Log_Time],
		srcData.[Data],
		srcData.[Sensors]
	FROM @logParams logPars
		CROSS APPLY
		(
			SELECT-- TOP(100)
				*
			FROM [dbo].[Emulator_Source]
			WHERE [Vehicle_ID] = logPars.[Vehicle_ID]
			AND   [Log_Time]   BETWEEN logPars.[Log_Time_From] AND logPars.[Log_Time_To]
			--ORDER BY Log_Time ASC
		) srcData
	ORDER BY logPars.[Log_Time_From] DESC, srcData.[Log_Time] ASC
END