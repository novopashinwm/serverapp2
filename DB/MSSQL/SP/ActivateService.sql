if exists (
	select * from sys.procedures 
		where name = 'ActivateService'
)
	drop procedure ActivateService
go
SET QUOTED_IDENTIFIER ON
GO

create procedure ActivateService
(
	@billing_service_id int
)
as

set nocount on

update bs
	set Activated = 1
	  , EndDate = dateadd(day, bst.PeriodDays, GETUTCDATE())
	from Billing_Service bs
	join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
	where bs.Activated = 0
	  and bs.ID = @billing_service_id
	  and bst.PeriodDays is not null
	
	