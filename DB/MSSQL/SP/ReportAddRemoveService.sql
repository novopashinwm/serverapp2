if exists (
	select * 
		from sys.objects
		where name = 'ReportAddRemoveService'
)
	drop procedure ReportAddRemoveService
go

--ReportAddRemoveService 293, '2014.03.16', '2016.03.18', 0
create procedure ReportAddRemoveService
(
	@operator_id	int,
	@from			datetime,
	@to				datetime,
	@detailed		bit = 0
)
as

set nocount on;

if @detailed is null
	set @detailed = 0

select a.Terminal_Device_Number
	   , v.VEHICLE_ID
	   , Contract_Number = d.ExtID
	   , Billing_Service_Type_ID = bst.ID
	   --TODO: ��������� ��������� ������, �.�. ������, � ������� ������� ������ ���� ��������� ����� ��������
	   , Added   = convert(bit, case when bs.StartDate between @from and @to then 1 else 0 end)
	   , Removed = convert(bit, case when bs.EndDate   between @from and @to then 1 else 0 end)
		, bs.StartDate
		, bs.EndDate
	into #detail
	from Asid a	
	cross apply (
		select bs_add.Billing_Service_Type_ID,
			   bs_add.StartDate,
			   EndDate = bs_del.Actual_Time
			from H_Billing_Service bs_add 
			left outer join H_Billing_Service bs_del on bs_del.ID = bs_add.ID 
													and bs_del.ACTION = 'DELETE'
			where bs_add.Asid_ID = a.ID 
	          and bs_add.ACTION = 'INSERT'
	) bs
	join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID	
	outer apply (
		select v.VEHICLE_ID, v.Department
			from Vehicle v 
			join Controller c on c.VEHICLE_ID = v.VEHICLE_ID
			join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
			where mc.Asid_ID = a.ID) v
	join Department d on d.DEPARTMENT_ID in (v.DEPARTMENT, a.Department_ID)
	where (
			bs.StartDate between @from and @to
			or @from between bs.StartDate and ISNULL(bs.EndDate, @to)
		  )
	   and d.IsCommercial = 1
	   and exists (select 1 
						from v_operator_department_right odr 
						where odr.operator_id = @operator_id 
						  and odr.department_id = d.DEPARTMENT_ID
						  and odr.right_id = 110 /*ServiceManagement*/)

select   bst.ID
       , bst.Name
	   , bst.MinPrice
	   , bst.MaxPrice
	   , summary.*
	from Billing_Service_Type bst
	cross apply (
		select 
			 CountOnStart	= sum(case when d.StartDate < @from then 1 else 0 end)
		   , CountOfAdded	= sum(convert(int, d.Added))
		   , CountOfRemoved	= sum(convert(int, d.Removed))
		   , CountOnEnd		= sum(case when d.EndDate is null or @to < d.EndDate then 1 else 0 end)
		from #detail d
		where d.Billing_Service_Type_ID = bst.ID) summary
	where summary.CountOnStart is not null

if @detailed = 1
	select 
		ServiceName = bst.Name
		, d.* 
		from #detail d
		join Billing_Service_Type bst on bst.ID = d.Billing_Service_Type_ID

drop table #detail