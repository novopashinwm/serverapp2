if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[GetOperator]') and type in (N'P', N'PC'))
drop procedure [dbo].[GetOperator]
go

set ansi_nulls on
go

set quoted_identifier on
go



create procedure [dbo].[GetOperator]
(
	@OPERATOR_ID as int,
	@GetBillingBlockings bit = null
)
as
begin

	set nocount on

	declare @PhoneFromTable nvarchar(255)
	set @PhoneFromTable = (select top(1) phone from dbo.Phone p where p.Operator_ID = @Operator_ID and Confirmed = 1 order by p.Phone_ID desc)
	
	select OPERATOR_ID, [NAME], login, '********' as [PASSWORD]
	, @PhoneFromTable as PHONE
	, 
		(select top(1) email from dbo.Email e where e.Operator_ID = @Operator_ID order by e.Email_ID desc) EMAIL, 
		TimeZoneInfo
	from dbo.OPERATOR o
	where o.OPERATOR_ID = @OPERATOR_ID

	if (@GetBillingBlockings = 1)
		select 
			bb.Asid_ID,
			1 ID,
			'Block' Name
			from dbo.Asid a	
			join dbo.Billing_Blocking bb on bb.Asid_ID = a.ID
			where a.Operator_ID = @OPERATOR_ID
end


go


