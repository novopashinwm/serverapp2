if exists (
	select *
		from sys.procedures
		where name = 'GetOperatorServices'
)
	drop procedure GetOperatorServices
go

set quoted_identifier on
go

/*
Sample:
exec GetOperatorServices 187
*/
create procedure GetOperatorServices
(
	@operator_id int,
	@department_id int = null,
	@debug bit = null
)
as
begin

	if @debug is null 
		set @debug = 0

	if @debug = 0
		set nocount on

	declare @vid dbo.Id_Param

	if (@department_id is null)
	begin
		insert into @vid
			select ovr.vehicle_id
				from v_operator_vehicle_right ovr
				where ovr.operator_id = @operator_id
				  and ovr.right_id = 111 --PayForCaller
			union 
				select c.Vehicle_ID
					from Asid a
					join MLP_Controller mc on mc.Asid_ID = a.ID
					join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
					where a.Operator_ID = @operator_id
					  and c.VEHICLE_ID is not null
	end
	else
	begin
		insert into @vid
			select ovr.vehicle_id
				from v_operator_vehicle_right ovr
				join vehicle v on v.VEHICLE_ID = ovr.vehicle_id
				where ovr.operator_id = @operator_id
				  and ovr.right_id = 111 --PayForCaller
				  and v.DEPARTMENT = @department_id
			union 
				select c.Vehicle_ID
					from Asid a
					join MLP_Controller mc on mc.Asid_ID = a.ID
					join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
					join vehicle v on v.VEHICLE_ID = c.VEHICLE_ID
					where a.Operator_ID = @operator_id
					  and c.VEHICLE_ID is not null
					  and (v.DEPARTMENT = @department_id or a.Department_ID = @department_id)					  
	end
		
	
	if @debug = 1
		select * from @vid
			  
	exec dbo.GetVehicleServices @vid
end