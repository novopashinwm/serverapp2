

IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'GetFuelCostAndZone'
        AND 	  type = 'P')
    DROP PROCEDURE GetFuelCostAndZone
GO


-----------------------------------------------------------------------------------
--Назначение: Для построения отчета по расходу топлива
--Автор: Гребенников М.В.
-----------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[GetFuelCostAndZone]
	@vehicle_id		int,
	@factorNoise	int,
	@factorChange	int,
	@factorDiscret	int,
	@dtFrom			DateTime, 
	@dtTo			DateTime  
AS

-----------------------------------------------------------------------------------
/*
-- Test
Declare
	@vehicle_id		int,
	@factorNoise	int,
	@factorChange	int,
	@factorDiscret	int,
	@dtfrom			DateTime, 
	@dtto			DateTime  

set @vehicle_id = 1--241
set @factorNoise = 3
set	@factorChange = 20
set @factorDiscret = 1
set @dtFrom = '2006-11-12 21:00:00'
set @dtTo = '2006-11-14 20:59:00'

set @vehicle_id = 2 
set @dtfrom = '2008-07-23 20:00:00.000'
set @dtTo = '2008-07-24 23:59:00:000'

set @vehicle_id = 417 
set @dtfrom = '2008-07-31 20:00:00.000'
set @dtTo = '2008-08-01 20:00:00:000'

set @vehicle_id = 864 
set @dtfrom = '2008-10-14 20:00:00.000'
set @dtTo = '2008-10-15 19:59:59:000'

set @dtfrom = '2008-10-21 04:00:00:000'
set @dtTo = '2008-10-21 05:30:00:000'


--select * from VEHICLE where GARAGE_NUMBER like '%мик%' VEHICLE_ID = 1 --GARAGE_NUMBER = '6625'
*/
-----------------------------------------------------------------------------------

Declare 
	@time_from_int	int,
	@time_to_int	int,
	@time_diffUTC	int,
	--допустимый интервал отсутствия данных из таблицы правил для ТС, если не задан принимаем 60 сек.
	@rule_id		int,
	@time_allow		int,
	--Для расчета топлива
	@controller_id		int,
	@fuel_Factor		float,
	@fuel_SensorDirect	bit,
	@fuel_SensorMin		int,		
	@fuel_SensorMax		int,	
	@factorDiscretPercent	int	

Set @time_from_int = datediff(s, '1970', @dtfrom)
Set @time_to_int = datediff(s, '1970', @dtTo)
Set @time_diffUTC = datediff(s, @dtfrom, cast(floor(cast((@dtfrom + 1) as float)) as datetime))
Set @rule_id = 2
Set @time_allow = isnull((select Top 1 Value from VEHICLE_RULE where vehicle_id = @vehicle_id and rule_id = @rule_id), 60)
--
Set @controller_id = (select top 1 CONTROLLER_ID from dbo.CONTROLLER where VEHICLE_ID = @vehicle_id)
Set @fuel_Factor = isnull((select top 1 FUEL_FACTOR from dbo.CONTROLLER_INFO where CONTROLLER_ID = @controller_id), 1)
Set @fuel_SensorDirect = isnull((select top 1 FUEL_SENSOR_DIRECTION from dbo.CONTROLLER_INFO where CONTROLLER_ID = @controller_id), 0)
Set @fuel_SensorMin = isnull((select top 1 FUEL_LVAL from dbo.CONTROLLER_INFO where CONTROLLER_ID = @controller_id), 0)
Set @fuel_SensorMax = isnull((select top 1 FUEL_UVAL from dbo.CONTROLLER_INFO where CONTROLLER_ID = @controller_id), 2147483647) --4294967295)
-- Коэффициент-шум задается в % от объема бака для ТС, все что ниже не учитываем. Пересчитываем в литры
Set @factorNoise = @factorNoise * isnull((select top 1 FUEL_TANK from VEHICLE where VEHICLE_ID = @vehicle_id), 100) / 100  
-- Коэффициент залива-слива задается в % от объема бака для ТС. Пересчитываем в литры
Set @factorChange = @factorChange * isnull((select top 1 FUEL_TANK from VEHICLE where VEHICLE_ID = @vehicle_id), 100) / 100
-- Коэффициент дискретизации для усреднения показаний топлива задается в % от 24 часов. Пересчитываем в сек. 
Set @factorDiscretPercent = @factorDiscret
Set @factorDiscret = @factorDiscret * 86400 / 100

-- Временная таблица по точкам
create table #MonitoreeLog(	
	id int identity primary key, 
	Log_Time		int,
	Log_Date		DateTime, 
	X				float,
	Y				float,
	Speed			int,
	V4				int,
	Dist			float,
	Data 			int,
	GroupData 		int,
	GroupDiscret	int,
	Satellites		int
	)

-- Отбираем все позиции ТС за интервал
insert into #MonitoreeLog
select 	
	Log_Time, 
	--cast(floor(cast(dateadd(s, Log_Time, '1970') as float)) as datetime),
	dateadd(hour, -@time_diffUTC / 3600, cast(floor(cast(dateadd(s, Log_Time + @time_diffUTC, '1970') as float)) as datetime) ),
	X,
	Y,
	Speed,
	0,
	Convert(float, 0), 
	1,
	0,
	0,
	Satellites
from Monitoree_log (nolock)
Where 	
	Monitoree_ID = @vehicle_ID
	and Log_Time between @time_from_int and @time_to_int 
	--and X < 180 and Y < 180
order by 
	Log_Time


-- Плохие позиции больше не портятся, нужно проверять DS из CONTROLLER_STAT
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.CONTROLLER_STAT'))  
	update #MonitoreeLog set X = 181, Y = 181 where  
		((select count(CS.[TIME]) from dbo.CONTROLLER_STAT as CS inner join CONTROLLER as C on CS.CONTROLLER_ID = C.CONTROLLER_ID where C.VEHICLE_ID = @vehicle_ID and CS.[TIME] = dateadd(s, #MonitoreeLog.Log_Time, '1970') and (CS.DS & 0x80000000) = 0 ) > 0)
update #MonitoreeLog set X = 181, Y = 181 where Satellites < 3 or (X = 0 or Y = 0)

---- Топливо
--update #MonitoreeLog Set V4 = isnull((
--	select top 1 CS.V4 from dbo.CONTROLLER_STAT as CS  
--	where CS.CONTROLLER_ID = @controller_id 
--		and CS.[TIME] = dateadd(s, #MonitoreeLog.Log_Time, '1970') 
--		--and CS.V4 > @fuel_SensorMin and CS.V4 < @fuel_SensorMax
--	), 0)
update #MonitoreeLog Set V4 = isnull((
	select top 1 dbo.V3V4toInt(ML.V3, ML.V4) as V4 from dbo.MONITOREE_LOG as ML  
	where ML.MONITOREE_ID = @vehicle_id 
		and ML.[LOG_TIME] = #MonitoreeLog.Log_Time
		--and CS.V4 > @fuel_SensorMin and CS.V4 < @fuel_SensorMax
	), 0)

--test
--select * ,dateadd(s, Log_Time, '1970') as [DateTime] from #MonitoreeLog


if(@fuel_SensorDirect > 0)
	update #MonitoreeLog Set V4 = (V4 - @fuel_SensorMin) * @fuel_Factor where V4 > 0  
else
	update #MonitoreeLog Set V4 = (@fuel_SensorMax - V4) * @fuel_Factor where V4 > 0
	

-- Обрабатываем полученные данные (считаем пробег и группируем по периодам)
Declare 
	@max_id				int,
	@i					int,
	@i_					int,
	@log_time			int,
	@log_time_			int,
	@x					float,
	@x_					float,
	@y					float,
	@y_					float,
	@dist				float,
	@data				int,
	@data_				int,
	@GroupData			int,
	@GroupDiscret		int,
	@lastTimeDiscret	int,
	@log_Date			int,
	@log_Date_			int,
	--
	@speed			int,
	@speed_			int,
	@timeBetween	int			
 
set	@max_id = (select max(id) from #MonitoreeLog)
set	@i = 1
set	@GroupData = 0
set @GroupDiscret = 0
set @lastTimeDiscret = (select Log_Time from #MonitoreeLog where id = @i)
-- Период дискретизации перещитывается в % исходя из продолжительности работы в сутках
set @factorDiscret = @factorDiscretPercent 
	* (select Max(Log_Time) - Min(Log_Time) from #MonitoreeLog where Log_Date = (select top 1 Log_Date from #MonitoreeLog where id = @i) ) / 100 

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @log_time = (select Log_Time from #MonitoreeLog where id = @i)
	set	@x = (select X from #MonitoreeLog where id = @i) 			
	set	@y = (select Y from #MonitoreeLog where id = @i)			
	set	@data = (select Data from #MonitoreeLog where id = @i)	
	set	@log_time_ = (select Log_Time from #MonitoreeLog where id = @i_)
	set	@x_	= (select X from #MonitoreeLog where id = @i_)		
	set	@y_	= (select Y from #MonitoreeLog where id = @i_)	
	set	@data_	= (select Data from #MonitoreeLog where id = @i_)
	set @log_Date = (select datediff(s, '1970', Log_Date) from #MonitoreeLog where id = @i)
	set @log_Date_ = (select datediff(s, '1970', Log_Date) from #MonitoreeLog where id = @i_)

	set @speed = (select Speed from #MonitoreeLog where id = @i)
	set @speed_ = (select Speed from #MonitoreeLog where id = @i_)
	set @timeBetween = @log_time - @log_time_

		
	--качество координат для первой позиции (Data=1 - если координаты хорошие; Data=2 - если плохие отсутствие GPS)
	if(@i_ = 1 and (@x_ > 180 or @y_ > 180))
	begin
		Set @data_ = 2
		update #MonitoreeLog Set Data = @data_ where id = @i_
	end

	-- Пробег между точками. Не считаем, если координаты плохие --и если скорость нулевая (машина стоит)
	if (@x > 180 or @y > 180 or @x_ > 180 or @y_ > 180)
		Set @dist = 0 
	else
		Set @dist = SQRT(SQUARE(63166*(@x_-38)-63166*(@x-38)) + SQUARE(111411*(@y_-55.665)-111411*(@y-55.665))) / 1000

		-- 
		if(@speed_ < 1)
		begin
			if(@speed < 1)
				if(@timeBetween < 0.02 and @dist < 0.02)
					Set @dist = 0
		end

	--Если позиции пришли в течении 60 сек
	if(@log_time - @log_time_ > @time_allow)
		Set @GroupData = @GroupData + 1

	-- Период дискретизации перещитывается в % исходя из продолжительности работы в сутках
	if(@log_Date <> @log_Date_)
		set @factorDiscret = @factorDiscretPercent 
			* (select Max(Log_Time) - Min(Log_Time) from #MonitoreeLog where Log_Date = (select top 1 Log_Date from #MonitoreeLog where id = @i) ) / 100 

	-- Разбиваем на периоды дискретизации для усреднения по топливу
	if(@log_time - @lastTimeDiscret > @factorDiscret or @log_Date <> @log_Date_)
	begin
		Set @GroupDiscret = @GroupDiscret + 1
		Set @lastTimeDiscret = @log_time
	end

	--обновляем данные
    update #MonitoreeLog Set Dist = @dist, Data = @data, GroupData = @GroupData, GroupDiscret = @GroupDiscret where id = @i
end

--test
--select * ,dateadd(s, Log_Time, '1970') as [DateTime] from #MonitoreeLog
--select Max(Log_Time) - Min(Log_Time) from #MonitoreeLog where Log_Date = '2008-10-21 04:00:00.000'

select 
	Log_Date,
	GroupData,
	Max(Log_Time) - Min(Log_Time) as TimeReceipt,
	Sum(Dist) as Dist,
	Convert(float, Count(Log_Time) * 65) / 1048576 as Size  --/ 1024 as Size
	,Convert(char(10), Min(Log_Time)) as sFrom, dateadd(s, Min(Log_Time), '1970') as dtFrom 
	,Convert(char(10), Max(Log_Time)) as sTo, dateadd(s, Max(Log_Time), '1970') as dtTo
	,Data 
into #Intervals 
from #MonitoreeLog
Group by 
	Log_Date,  
	GroupData
	,Data 
order by Log_Date, dtFrom


select --* 
	Log_Date,
	Sum(TimeReceipt) as TimeReceipt,
	Sum(Dist) as Dist,
	Sum(Size) as Size
into #Result 
from #Intervals
Group by Log_Date  
order by Log_Date

--test
--select * ,dateadd(s, Log_Time, '1970') as [DateTime] from #MonitoreeLog
--select * from #Intervals
--select * from #Result


/*--В отчете необходимо отображать также дни отсутствия данных
create table #Days(	
	--id int identity primary key, 
	[Date]		DateTime 
	)
-- 
declare 
	@dFrom	datetime,
	@dTo	datetime
set @dFrom = @dtfrom --CONVERT(datetime, CONVERT(varchar, @dtfrom, 1), 1)
set @dTo = @dtTo --CONVERT(datetime, CONVERT(varchar, @dtTo, 1), 1)
--
while @dFrom <= @dTo
begin
	insert into #Days values (@dFrom)  
	set @dFrom = @dFrom + 1
end
 
select --* 
	#Days.[Date] as Log_Date,
	isnull(TimeReceipt, 0) as TimeReceipt,
	isnull(Dist, 0) as Dist,
	isnull(Size, 0) as Size
	,(select count(TimeReceipt) from #Result where TimeReceipt > 0) as CountDaysReceipt
from #Days 
	left join #Result on #Result.Log_Date = #Days.[Date]
order by #Days.[Date]   */


-- Разбиваем на интервалы с заданной дискретизацией для нахождения заливов/сливов и расхода топлива
select 
	Log_Date,
	GroupDiscret
	,Sum(V4) / Count(V4) as V4
	,Convert(char(10), Min(Log_Time)) as sFrom, dateadd(s, Min(Log_Time), '1970') as dtFrom 
	,Convert(char(10), Max(Log_Time)) as sTo, dateadd(s, Max(Log_Time), '1970') as dtTo,
	0 as L_Cost
into #IntervalsFuel 
from #MonitoreeLog
where V4 > @factorNoise
Group by 
	Log_Date,  
	GroupDiscret
order by Log_Date, dtFrom

-- Обрабатываем полученные данные 
Declare 
	@v4		int,
	@v4_	int
 
set	@max_id = (select max(GroupDiscret) from #IntervalsFuel)
set	@i = 0
set @GroupDiscret = 0

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @log_Date = (select datediff(s, '1970', Log_Date) from #IntervalsFuel where GroupDiscret = @i)
	set @log_Date_ = (select datediff(s, '1970', Log_Date) from #IntervalsFuel where GroupDiscret = @i_)
	
	if(@log_Date = @log_Date_)
	begin
		set @v4 = (select V4 from #IntervalsFuel where GroupDiscret = @i)
		set @v4_ = (select V4 from #IntervalsFuel where GroupDiscret = @i_)
		
		update #IntervalsFuel Set L_Cost = @v4 - @v4_ where GroupDiscret = @i
	end
end


-- Заливы-Сливы
select  
	ROW_NUMBER() OVER (ORDER BY GroupDiscret) num,
	Log_Date,
	GroupDiscret,
	dtFrom,
	dtTo,
	L_Cost
into #ResultFuel 
from #IntervalsFuel
where L_Cost > @factorChange or L_Cost * (-1) > @factorChange

--Интервал для Заливы-Сливы
--update #ResultFuel Set dtFrom = (select dtTo from #IntervalsFuel where GroupDiscret = #ResultFuel.GroupDiscret - 1)

--test
--select @fuel_Factor as fuel_Factor, @fuel_SensorDirect as fuel_SensorDirect, @fuel_SensorMin as fuel_SensorMin, @fuel_SensorMax as fuel_SensorMax, @factorNoise as factorNoise, @factorChange as factorChange, @factorDiscret as factorDiscret
--select * ,dateadd(s, Log_Time, '1970') as Log_DateTime from #MonitoreeLog
--select * from #IntervalsFuel
--select * from #ResultFuel

-- Если периоды накладываются, группируем еще
Declare 
	@lcost		int,
	@lcost_		int,
	--@dtFrom		DateTime,
	@dtTo_		DateTime
set	@max_id = (select max(num) from #ResultFuel)
set	@i = 1
set @GroupDiscret = 0

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @lcost = (select L_Cost from #ResultFuel where num = @i)
	set @lcost_ = (select L_Cost from #ResultFuel where num = @i_)
	set @dtFrom = (select dtFrom from #ResultFuel where num = @i)
	set @dtTo_ = (select dtTo from #ResultFuel where num = @i_)
	
	if(@dtFrom = @dtTo_ /*or (@dtFrom > @dtTo_)*/ and ((@lcost > 0 and @lcost_ > 0) or (@lcost < 0 and @lcost_ < 0)))
	begin
		set @GroupDiscret = (select GroupDiscret from #ResultFuel where num = @i_)
		
		update #ResultFuel Set GroupDiscret = @GroupDiscret where num = @i
	end
end

--test
--select * from #ResultFuel

	
-- Результат
select 
	r.Log_Date,
	r.TimeReceipt,
	r.Dist,
	isnull((select sum(L_Cost) * (-1) from #IntervalsFuel where L_Cost < 0 and #IntervalsFuel.Log_Date = r.Log_Date), 0) as L_Cost, 
	f.L_Cost as L_Change,
	f.dtFrom,
	f.dtTo
	,GroupDiscret
into #ResultAll 
from #Result as r
	left join #ResultFuel as f on f.Log_Date = r.Log_Date

--test
--select * from #ResultAll


-- результаты
select 
	Log_Date, 
	TimeReceipt,
	Dist,
	L_Cost,
	sum(L_Change)	L_Change, 
	min(dtFrom)		dtFrom,
	max(dtTo)		dtTo
from #ResultAll
Group by 
	Log_Date, 
	TimeReceipt,
	Dist,
	L_Cost, 
	GroupDiscret
order by Log_Date, dtFrom

-- И по зонам
select ml.* 
	,dateadd(s, ml.log_time, '1970') as Log_DateTime
from #MonitoreeLog as ml
	inner join #ResultAll as r on dateadd(s, ml.log_time, '1970') >= r.dtFrom and dateadd(s, ml.log_time, '1970') <= r.dtTo
-- 
select --* 
	gz.*,
	gzp.PRIMITIVE_ID,
	gzp.[ORDER] as PrimitiveOrder,
	zpv.VERTEX_ID,
	zpv.[ORDER] as VertexOrder,
	mv.X,
	mv.Y
from GEO_ZONE as gz
	inner join GEO_ZONE_PRIMITIVE as gzp on gzp.ZONE_ID = gz.ZONE_ID
	--inner join ZONE_PRIMITIVE as zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
	inner join ZONE_PRIMITIVE_VERTEX as zpv on zpv.PRIMITIVE_ID = gzp.PRIMITIVE_ID
	inner join MAP_VERTEX as mv on mv.VERTEX_ID = zpv.VERTEX_ID
	--inner join ZONE_VEHICLE as zv on gz.ZONE_ID = zv.ZONE_ID
--where zv.vehicle_id = @vehicle_id
order by 
	gz.ZONE_ID,
	gzp.[ORDER],
	zpv.[ORDER]


drop table #MonitoreeLog
drop table #Intervals
drop table #IntervalsFuel
drop table #Result
drop table #ResultFuel
drop table #ResultAll
--drop table #Days


GO

