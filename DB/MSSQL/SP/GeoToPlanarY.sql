IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeoToPlanarY]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].GeoToPlanarY
GO

CREATE FUNCTION GeoToPlanarY
(
	@geoY float(53)
)
RETURNS float(53)
AS
BEGIN
/*
		const double kx = 63166;	//111324.72603;
		const double ky = 111411;	//197104.85314;

		//const double kxOldToNew = 0,56741;// == 63166 / 111324.72603;			kOldToNew ~= 0,566
		//const double kyOldToNew = 0,56524;// == 111411 / 197104.85314;

		const double origCenterX = 38.000;	//	( 41.00 + 35.00) / 2 = 38.00
		const double origCenterY = 55.665;	//	( 57.33 + 54.00) / 2 = 55.665
*/
	declare @ky float(53); set @ky = 111411;
	declare @origCenterY float(53); set @origCenterY = 55.665;

	return @ky * ( @geoY - @origCenterY);

END
GO
