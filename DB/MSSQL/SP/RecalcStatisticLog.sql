﻿IF (OBJECT_ID(N'[dbo].[RecalcStatisticLog]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[RecalcStatisticLog]
GO

CREATE PROCEDURE [dbo].[RecalcStatisticLog]
(
	@vehicleID   int,
	@maxInterval int = NULL,
	@debug       bit = NULL
)
AS
BEGIN
	IF (ISNULL(@debug, 0) = 0)
		SET NOCOUNT ON;
		
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	
	DECLARE @t TABLE
	(
		log_time int
	)

	/*Потокобезопасность: на одну машину не более одного вызова процедуры одновременно*/
	update slr
		set pending = 1,
		pending_date = getutcdate()
	output INSERTED.log_time
	into @t
	from dbo.Statistic_Log_Recalc slr (xlock)
	where vehicle_id = @vehicleID 
	and Log_Time is not null
	and (
		isnull(pending, 0) = 0 
		or (pending = 1 and pending_date < dateadd(hh, -1, getutcdate()))
		);

	/* Разрешается, чтобы над каждой машиной работало не более одного экземпляра процедуры */
	if @@rowcount = 0
		return;

	declare @fromTime int, @initialFromTime int

	set @initialFromTime = (select log_time from @t);

	if @initialFromTime is not null 
	begin
		declare @curr_Log_time int, @max_log_time int, @min_log_time int, 
				@odometer bigint, @ignitionOnTime int, @interval int;		
		declare @stopPeriod int = (select convert(int, value) from CONSTANTS where NAME = 'StopPeriod')

		set @interval = 3600; --1 час

		create table #pos 
		(
			r bigint identity(1,1) primary key,
			Log_Time		int,
			Lng				numeric(8,5),
			Lat				numeric(8,5)
		)	
		
		declare @ignition_log table (log_time int primary key clustered, value bit)

		select top 1 @odometer = odometer, 
				     @ignitionOnTime = ignitionOnTime
			from dbo.Statistic_Log 
			where vehicle_id = @vehicleID and log_time < @initialFromTime
			order by log_time desc

		if (@odometer is null) set @odometer = 0;
		if (@ignitionOnTime is null) set @ignitionOnTime = 0;

		declare @prevLng numeric(8,5),	@prevLat numeric(8,5), @prevLogTime int, @prevIgnition int;

		select top 1 @prevLng		= g.Lng, 
					 @prevLat		= g.Lat, 
					 @prevLogTime	= g.log_time		 
			from dbo.geo_log g (nolock)
			where g.vehicle_id = @vehicleID and g.log_time < @initialFromTime
			order by g.log_time desc

		set @prevIgnition = isnull((
			select top(1) value 
				from v_controller_sensor_log csl with (nolock)
			where csl.Vehicle_ID = @VehicleID 
			  and csl.Log_Time between @prevLogTime - 900 and @prevLogTime
			  and csl.SensorLegend = 4
			order by csl.Log_Time desc), 0)

		if @debug = 1
			print '@prevLogTime = ' + isnull(convert(varchar(12), @prevLogTime), null)

		declare @toTime int

		set @toTime = datediff(s, cast('1970-01-01' as datetime), getutcdate()) + 500;

		/*	По-умолчанию, обрабатываем данные не более, чем за неделю за один вызов процедуры, чтобы sql-соединение не отваливалось по таймауту.
			TODO: Для продолжительных обработок следует использовать job.*/

		if @maxInterval is null
			set @maxInterval = 3600 * 24 * 7

		if (@initialFromTime + @maxInterval < @toTime)
			set @toTime = @initialFromTime + @maxInterval;

		if @debug = 1 
		begin
			print '@initialFromTime = ' + convert(varchar(12), @initialFromTime)
			print '@toTime          = ' + convert(varchar(12), @toTime)
		end;

		set @fromTime = @initialFromTime;

		while (@fromTime is not null and @fromTime <= @toTime)
		begin
			delete from #pos;
			delete from @ignition_log

			insert into #pos (Log_Time, Lng, Lat)
				select	lt.log_time, 
						lt.Lng, 
						lt.Lat
				from geo_log lt (nolock)
				where lt.vehicle_id = @vehicleID 
				and lt.log_time between @fromTime and (@fromTime + @interval)
				order by lt.log_time;

			insert into @ignition_log
				select top(1) csl.Log_Time, CONVERT(bit, case csl.value when 0 then 0 else 1 end)
					from v_controller_Sensor_Log csl (nolock)
					where csl.Vehicle_ID = @VehicleID 
					  and csl.Log_Time < @fromTime
					  and csl.SensorLegend = 4 --зажигание
					order by csl.Log_Time desc

			insert into @ignition_log
				select csl.Log_Time, CONVERT(bit, case csl.value when 0 then 0 else 1 end)
					from v_controller_Sensor_Log csl (nolock)
					where csl.Vehicle_ID = @VehicleID 
					  and csl.Log_Time between @fromTime and (@fromTime + @interval)
					  and csl.SensorLegend = 4 --зажигание
					order by csl.Log_Time desc				

			declare @r bigint, @maxR bigint, @currDist bigint;
			select @r = min(r), @maxR = max(r) from #pos;

			declare @Lng numeric(8,5),
					@Lat numeric(8,5),
					@Log_Time int,
					@ignition bit,
					@isMotion bit;

			if isnull(@debug, 0) = 1
				set nocount on;		

			while (@r <= @maxR)
			begin
				select @Log_Time = Log_Time, 
					   @Lng = Lng, 
					   @Lat = Lat
					from #pos 
					where r = @r;

				select top(1) @ignition = l.value
					from @ignition_log l
					where l.log_time <= @Log_Time
					order by l.Log_Time desc

				if (@prevLng is not null and @prevLat is not null and @Log_Time <> @prevLogTime)
				begin
					select @currDist = dbo.GetTwoGeoPointsDistance(@prevLng, @prevLat, @Lng, @Lat)*100;
					--Если скорость для перемещения от предыдущей точки > 1000км/ч, то отбраковываем позицию
					if ((@currDist/100000) / (cast(@Log_Time - @prevLogTime as float)/3600) > 1000)
					begin
						set @currDist = -1;
					end

					--Если текущие координаты нулевые исключаем эту точку (т.к. скорее всего ошибочная)
					if (0.0 = @Lng AND 0.0 = @Lat)
					begin
						set @currDist = -1;
					end

					if (@currDist >= 0)
					begin	
						select @odometer = @odometer + @currDist;
						
						--Моточасы инкрементируются, если зажигание включено и в начале, и в конце интервала,
						--при этом длина отрезка не должна превышать 20 минут: это исключает ситуацию,
						--когда терминал надолго выключили.
						if (@prevIgnition = 1 and @ignition = 1 and (@Log_Time - @prevLogTime) < 20 * 60)
							set @ignitionOnTime = @ignitionOnTime + (@Log_Time - @prevLogTime);

						select @isMotion = case when @currDist = 0 or (@currDist < 30 * 100 and @stopPeriod < @Log_Time - @prevLogTime) then 0 else 1 end
						exec UpdateStateLog @vehicleID = @vehicleID, @type = 1, @Log_Time = @Log_Time, @value = @isMotion
						insert into dbo.Statistic_Log (log_time, vehicle_id, odometer, ignitionOnTime)
							select @Log_Time, @vehicleID, @odometer, @ignitionOnTime
							where not exists(
								select 1 
									from Statistic_Log with (XLOCK, SERIALIZABLE)
									where Vehicle_ID = @vehicleID and log_time = @Log_Time)
									
						if @@ROWCOUNT = 0 
							update sl
								set Odometer = @odometer
								  , IgnitionOnTime = @ignitionOnTime
								from Statistic_Log sl
							where sl.vehicle_ID = @vehicleID 
							  and sl.Log_Time = @Log_Time
							  and (sl.Odometer is null or
							       sl.IgnitionOnTime is null or							       
								   sl.Odometer <> @odometer or 
								   sl.IgnitionOnTime <> @ignitionOnTime)
						
					end	
				end
				else 
				begin
						insert into dbo.Statistic_Log (log_time, vehicle_id, odometer, ignitionOnTime)
							select @Log_Time, @vehicleID, 0, 0
							where not exists(
								select 1 
									from Statistic_Log with (XLOCK, SERIALIZABLE) 
									where Vehicle_ID = @vehicleID and log_time = @Log_Time)
									
						if @@ROWCOUNT = 0 
							update sl
								set Odometer = @odometer
								  , IgnitionOnTime = @ignitionOnTime
								from Statistic_Log sl
							where sl.vehicle_ID = @vehicleID 
							  and sl.Log_Time = @Log_Time
							  and (sl.Odometer is null or
							       sl.IgnitionOnTime is null or							       
								   sl.Odometer <> @odometer or 
								   sl.IgnitionOnTime <> @ignitionOnTime)
								  									
				end

				select @prevLogTime = @Log_Time, 
					   @prevLng = @Lng, 
					   @prevLat = @Lat, 
					   @prevIgnition = @ignition,
					   @r=@r+1;

			end --while (@r <= @maxR)

			if isnull(@debug, 0) = 1
				set nocount off;

			declare @oldFromTime int
			set @oldFromTime = @fromTime
			set @fromTime = (select top(1) log_time
								from dbo.Log_Time (nolock)
								where vehicle_id = @vehicleID 
								  and log_time > @oldFromTime + @interval
								  order by log_time asc)

			if @fromTime is not null
			begin
				--помечаем, что расчет статистики уже произведен по @fromTime
				update dbo.Statistic_Log_Recalc 
					set log_time =	@fromTime,
						pending = 1,
						pending_date = getutcdate() --чтобы job пересчёта не запустил еще одну процедуру пересчёта, пока эта не закончилась, если эта выполняется более часа
					where vehicle_id = @vehicleID
					  and (log_time = @oldFromTime or log_time > @fromTime)
					  and pending = 1
					  
				--Если ничего не проапдейтили, значит уже вставили новую позицию в перерасчитанный интервал и надо заново рассчитывать
				if (@@rowcount = 0)
				begin
					if @debug = 1 
					begin
						print '@@rowcount = 0:'
						print '@oldFromTime = ' + convert(varchar(12), @oldFromTime)
						print '@toTime          = ' + convert(varchar(12), @toTime)
					end;
					drop table #pos;

					update dbo.Statistic_Log_Recalc 
						set pending = 0
						where vehicle_id = @vehicleID
						  and pending = 1

					return;	
				end
				
			end
			else
			begin --Если больще нет записей для пересчета
				declare @lastTime int;

				update dbo.Statistic_Log_Recalc 
					set log_time =	null
					where vehicle_id = @vehicleID
					and log_time = @oldFromTime
					and pending = 1

				if @debug = 1 					
				begin
						print 'Все записи пересчитаны:'
						print '@oldFromTime = ' + convert(varchar(40), dbo.GetDateFromInt(@oldFromTime))
						print '@toTime          = ' + convert(varchar(40), dbo.GetDateFromInt(@toTime))
						select * from Statistic_Log_Recalc where vehicle_id = @vehicleID
				end;
			end;

			if @debug = 1 
			begin
				print '@oldFromTime ' + isnull(convert(varchar(40), dbo.GetDateFromInt(@oldFromTime)), 'null'); 
				print '@fromTime '    + isnull(convert(varchar(40), dbo.GetDateFromInt(@fromTime)), 'null'); 
			end;

		end--while (@fromTime is not null and @fromTime <= @toTime)

		drop table #pos;
	end --if @initialFromTime is not null

	update dbo.Statistic_Log_Recalc 
		set pending = 0
		where vehicle_id = @vehicleID
	
END