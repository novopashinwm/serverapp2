﻿IF (OBJECT_ID(N'[dbo].[GetMessagesByOperatorId]') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetMessagesByOperatorId];
GO
CREATE PROCEDURE [dbo].[GetMessagesByOperatorId]
(
	@operatorId    int,
	@lastMessageId int      = null,
	@from          datetime = null,
	@to            datetime = null,
	@skip          int      = null,
	@count         int      = null,
	@vehicleIds    dbo.Id_Param readonly,
	@includeFields bit = 1
)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@skip          IS NULL) SET @skip          = 0
	if (@count         IS NULL) SET @count         = 10
	if (@includeFields IS NULL) SET @includeFields = 1

	--Подготовка параметров фильтра по времени
	DECLARE @logTimeFrom int = ISNULL([dbo].[utc2lt](@from),          0)
	DECLARE @logTimeTo   int = ISNULL([dbo].[utc2lt](@to),   2147483647)

	--Коррекция входных параметров с учетом скорректированного фильтра по времени
	SET @from = [dbo].[lt2utc](@logTimeFrom)
	SET @to   = [dbo].[lt2utc](@logTimeTo)

	DECLARE @ids TABLE
	(
		[message_id]   int      NOT NULL,
		[message_time] datetime NOT NULL,
		[TotalCount]   int      NOT NULL,
		[SORT_KEY]     bigint   NOT NULL
	);

	IF EXISTS (SELECT 1 FROM @vehicleIds)
	BEGIN
		INSERT INTO @ids
		SELECT
			M.[MESSAGE_ID], M.[Created], [TotalCount] = COUNT(1) OVER (), [SORT_KEY] = ROW_NUMBER() OVER (ORDER BY M.[Created] DESC)
		FROM [dbo].[MESSAGE] M WITH (NOLOCK)
			INNER JOIN [dbo].[MESSAGE_FIELD] F WITH (NOLOCK)
				ON  F.[MESSAGE_ID] = M.[MESSAGE_ID]
					INNER JOIN [dbo].[MESSAGE_TEMPLATE_FIELD] T WITH (NOLOCK)
						ON  T.[MESSAGE_TEMPLATE_FIELD_ID] = F.[MESSAGE_TEMPLATE_FIELD_ID]
						AND T.[NAME]                      = N'VehicleID'
					INNER JOIN @vehicleIds V
						ON  F.[CONTENT] = CAST(V.[Id] AS nvarchar(255))
		WHERE 1 = 1
		AND M.[DestinationType_ID] = 15 --Web
		AND M.[Owner_Operator_ID]  = @operatorId
		AND M.[Created]            BETWEEN @from AND @to
		AND M.[MESSAGE_ID]         > ISNULL(@lastMessageId, 0)
		ORDER BY M.[Created] DESC
		OFFSET @skip ROWS
		FETCH NEXT @count ROWS ONLY
	END
	ELSE
	BEGIN
		INSERT INTO @ids
		SELECT
			m.[MESSAGE_ID], m.[Created], [TotalCount] = COUNT(1) OVER (), [SORT_KEY] = ROW_NUMBER() OVER (ORDER BY M.[Created] DESC)
		FROM [MESSAGE] m WITH (NOLOCK)
		WHERE 1 = 1
		AND m.[DestinationType_ID] = 15 --Web
		AND m.[Owner_Operator_ID]  = @operatorId
		AND m.[Created]            BETWEEN @from AND @to
		AND m.[MESSAGE_ID]         > ISNULL(@lastMessageId, 0)
		ORDER BY m.[Created] DESC
		OFFSET @skip ROWS
		FETCH NEXT @count ROWS ONLY
	END
	--TODO: Добавить поиск сообщений по первому подтвержденному номеру, чтобы отработать ситуацию с собщениями которые были до регистрации в системе.
	SELECT ISNULL((SELECT TOP(1) TotalCount FROM @ids), 0)

	SELECT
		M.*, I.[SORT_KEY]
	FROM @ids I
		LEFT OUTER JOIN [dbo].[MESSAGE] M
			ON M.[MESSAGE_ID] = I.[MESSAGE_ID]
	ORDER BY M.[Created] DESC


	IF (@includeFields = 1)
	BEGIN
		SELECT
			COUNT(F.[MESSAGE_FIELD_ID])
		FROM [dbo].[MESSAGE_FIELD] F
			JOIN @ids I
				ON I.[MESSAGE_ID] = F.[MESSAGE_ID]
		SELECT
			F.*, I.[SORT_KEY]
		FROM [dbo].[MESSAGE_FIELD] F
			JOIN @ids I
				ON I.[MESSAGE_ID] = F.[MESSAGE_ID]
	END
END