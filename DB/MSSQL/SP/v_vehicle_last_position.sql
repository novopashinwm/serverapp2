﻿IF (OBJECT_ID(N'[dbo].[v_vehicle_last_position]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[v_vehicle_last_position];
GO

CREATE VIEW [dbo].[v_vehicle_last_position]
AS
	SELECT
		[MONITOREE_ID] = c.[VEHICLE_ID],
		l.[LOG_TIME],
		l.[MEDIA],
		[X] = l.[Lng],
		[Y] = l.[Lat],
		l.[SPEED], l.[SATELLITES], l.[FIRMWARE], l.[Altitude],
		[Course] = CONVERT(int, ROUND(l.[CourseDegrees], 0)),
		S1 = 0, S2 = 0, S3 = 0, S4 = 0, S5 = 0, --TODO: убрать совсем
		l.[Radius],
		ct.[TYPE_NAME]
	FROM [dbo].[CONTROLLER] c WITH (NOLOCK)
		INNER JOIN [dbo].[CONTROLLER_TYPE] ct WITH (NOLOCK)
			ON ct.[CONTROLLER_TYPE_ID] = c.[CONTROLLER_TYPE_ID]
		INNER JOIN [dbo].[Geo_Log#Lasts] gll WITH (NOLOCK)
			ON gll.[Vehicle_ID] = c.[VEHICLE_ID]
		CROSS APPLY
		(
			SELECT TOP(1)
				geol.[Lat],
				geol.[Lng],
				geol.[Log_Time],
				logt.[Media],
				gpsl.[SPEED],
				gpsl.[SATELLITES],
				gpsl.[FIRMWARE],
				gpsl.[Altitude],
				gpsl.[CourseDegrees],
				posr.[radius]
			FROM [dbo].[Geo_Log] geol WITH (NOLOCK)
				LEFT OUTER JOIN [dbo].[Log_Time] logt WITH (NOLOCK)
					ON  logt.[Log_Time]   = geol.[LOG_TIME]
					AND logt.[Vehicle_ID] = geol.[Vehicle_ID]
				LEFT OUTER JOIN [dbo].[GPS_Log] gpsl WITH (NOLOCK)
					ON  gpsl.[Log_Time]   = logt.[Log_Time]
					AND gpsl.[Vehicle_ID] = logt.[Vehicle_ID]
				LEFT OUTER JOIN [dbo].[position_radius] posr WITH (NOLOCK)
					ON  posr.[Log_Time]   = logt.[Log_Time]
					AND posr.[Vehicle_ID] = logt.[Vehicle_ID]
			WHERE geol.[Vehicle_ID] = gll.[Vehicle_ID]
			AND   geol.[Log_Time]   = gll.[LOG_TIME#Last]
		) l
GO