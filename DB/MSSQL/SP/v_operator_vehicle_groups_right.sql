if exists (select * from sys.views where name = 'v_operator_vehicle_groups_right_operator_vehiclegroup')
	drop view v_operator_vehicle_groups_right_operator_vehiclegroup
go
create view v_operator_vehicle_groups_right_operator_vehiclegroup
with schemabinding
as
	select o_vg.operator_id, o_vg.vehiclegroup_id, o_vg.right_id
	from dbo.operator_vehiclegroup o_vg
	where allowed = 1
go
create unique clustered index PK_v_operator_vehicle_groups_right_operator_vehiclegroup
	on v_operator_vehicle_groups_right_operator_vehiclegroup(operator_id, vehiclegroup_id, right_id)
go
create nonclustered index IX_v_operator_vehicle_groups_right_operator_vehiclegroup_vid_rid_oid
	on v_operator_vehicle_groups_right_operator_vehiclegroup(vehiclegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_vehicle_groups_right_operatorgroup_vehiclegroup')
	drop view v_operator_vehicle_groups_right_operatorgroup_vehiclegroup
go
create view v_operator_vehicle_groups_right_operatorgroup_vehiclegroup
with schemabinding
as
	select og_o.operator_id, og_vg.vehiclegroup_id, og_vg.right_id, [count]=count_big(*)
	from dbo.OPERATORGROUP_VEHICLEGROUP og_vg
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
	where og_vg.ALLOWED = 1
	group by og_o.operator_id, og_vg.vehiclegroup_id, og_vg.right_id
go
create unique clustered index PK_v_operator_vehicle_groups_right_operatorgroup_vehiclegroup
	on v_operator_vehicle_groups_right_operatorgroup_vehiclegroup(operator_id, vehiclegroup_id, right_id)
go
create nonclustered index IX_v_operator_vehicle_groups_right_operatorgroup_vehiclegroup_vid_rid_oid
	on v_operator_vehicle_groups_right_operatorgroup_vehiclegroup(vehiclegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_vehicle_groups_right_operator_department_admin')
	drop view v_operator_vehicle_groups_right_operator_department_admin
go
create view v_operator_vehicle_groups_right_operator_department_admin
with schemabinding
as
	select o_d.operator_id, vg.VEHICLEGROUP_ID, r.right_id, [count]=count_big(*)
	from dbo.OPERATOR_DEPARTMENT o_d
		join dbo.VEHICLEGROUP vg on vg.Department_ID = o_d.department_id
		join dbo.[Right] r on r.Right_ID in (102, 109, 119)
	where o_d.right_id = 2 /*SecurityAdministrator*/
		and o_d.allowed = 1
	group by o_d.operator_id, vg.VEHICLEGROUP_ID, r.right_id
go
create unique clustered index PK_v_operator_vehicle_groups_right_operator_department_admin
	on v_operator_vehicle_groups_right_operator_department_admin(operator_id, vehiclegroup_id, right_id)
go
create nonclustered index IX_v_operator_vehicle_groups_right_operator_department_admin_vid_rid_oid
	on v_operator_vehicle_groups_right_operator_department_admin(vehiclegroup_id, right_id, operator_id)
go


if exists (select * from sys.views where name = 'v_operator_vehicle_groups_right_operatorgroup_department_admin')
	drop view v_operator_vehicle_groups_right_operatorgroup_department_admin
go
create view v_operator_vehicle_groups_right_operatorgroup_department_admin
with schemabinding
as
	select og_o.operator_id, vg.vehiclegroup_id, r.right_id, [count]=count_big(*)
	from dbo.OperatorGroup_Operator og_o
		join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
		join dbo.VEHICLEGROUP vg on vg.Department_ID = og_d.department_id
		join dbo.[Right] r on r.Right_ID in (102, 109, 119)
	where og_d.right_id = 2 /*SecurityAdministrator*/
		and og_d.allowed = 1		
	group by og_o.operator_id, vg.vehiclegroup_id, r.right_id
go
create unique clustered index PK_v_operator_vehicle_groups_right_operatorgroup_department_admin
	on v_operator_vehicle_groups_right_operatorgroup_department_admin(operator_id, vehiclegroup_id, right_id)
go
create nonclustered index IX_v_operator_vehicle_groups_right_operatorgroup_department_admin_vid_rid_oid
	on v_operator_vehicle_groups_right_operatorgroup_department_admin(vehiclegroup_id, right_id, operator_id)
go


if exists (Select * from sys.views where name = 'v_operator_vehicle_groups_right')
	drop view v_operator_vehicle_groups_right
go
create view v_operator_vehicle_groups_right
as
select operator_id, vehiclegroup_id, right_id, priority = min(priority)
	from (
		select operator_id, vehiclegroup_id, right_id, priority = 1 from v_operator_vehicle_groups_right_operator_vehiclegroup with (noexpand) 
		union		
		select operator_id, vehiclegroup_id, right_id, priority = 2 from v_operator_vehicle_groups_right_operatorgroup_vehiclegroup with (noexpand) 
		union		
		select operator_id, vehiclegroup_id, right_id, priority = 3 from v_operator_vehicle_groups_right_operator_department_admin with (noexpand) 
		union		
		select operator_id, vehiclegroup_id, right_id, priority = 4 from v_operator_vehicle_groups_right_operatorgroup_department_admin with (noexpand) 
	) t
	group by operator_id, vehiclegroup_id, right_id
go
