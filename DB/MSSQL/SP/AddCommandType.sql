﻿IF (OBJECT_ID(N'[dbo].[AddCommandType]') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[AddCommandType]
END
GO

CREATE PROCEDURE [dbo].[AddCommandType]
(
	@id                 int,
	@code               varchar(200),
	@description        varchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int
)
AS
BEGIN
	INSERT INTO [dbo].[CommandTypes]
	(
		 [id]
		,[code]
		,[description]
		,[ExecutionTimeout]
		,[Right_ID]
		,[MinIntervalSeconds]
	)
	SELECT
		 @id
		,@code
		,@description
		,@ExecutionTimeout
		,[Right_ID]
		,@MinIntervalSeconds
	FROM [dbo].[RIGHT]
	WHERE [NAME] = @right_name
	AND NOT EXISTS(SELECT 1 FROM [dbo].[CommandTypes] WHERE [id] = @id)
END
GO