/****** Object:  StoredProcedure [dbo].[GetRoster]    Script Date: 07/14/2010 16:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------
-- =Ёў¤Ёвї¤°ї: ж■√║вї¤°ї ЇЁ¤¤v┐ Ї√а  ївЁк° ■╕¤■Є¤■∙ вЁ╕к° ¤ЁиаЇЁ - ╕ °╕·Ё ЄvїўЎЁжХ°┐ TT.
-- LЄк■и: L√ї·╕Ё¤Їи L. TкїпЁ¤■Є
-- жїЇЁ·к■и: L√ї·╕ї∙ ж. +√°¤■Є.
--------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetRoster]
	@gid			int,			                   
	@ReportDateTime		datetime = null,
	@operator_id int = null	               
AS

--------------------------------------------------------------------------------------------
/*
--кї╕к
declare @ReportDateTime		datetime,
		@gid		int
set	@ReportDateTime	= '2006-12-05 21:00:00'--'2006-12-05 20:59:00'
set	@gid = 8
*/
--------------------------------------------------------------------------------------------

--жи■Єїи·Ё  їиїЇЁ¤¤■ √° ў¤Ёвї¤°ї ЇЁкv ° Єиї№ї¤°.
if @ReportDateTime is null set @ReportDateTime = getutcdate()

/*
-- 1. ж■√║вї¤°ї ЇЁ¤¤v┐  ■ ╕■╕к■а¤°а№ Є╕ї┐ жT, ўЁЄїЇї¤¤v┐ Є ║·ЁўЁ¤¤║ж ЇЁк║.
begin transaction

--жїиї№ї¤¤Ёа Ї√а ┐иЁ¤ї¤°а ID иЁ╕╕№Ёки°ЄЁї№■є■ Є ЇЁ¤¤v∙ №■№ї¤к ·║и╕■и■№ жT.
declare @WaybillHeaderID	int

--T■ўЇЁ¤°ї Єиї№ї¤¤■∙ кЁё√°бv Ї√а ┐иЁ¤ї¤°а Є■ўЄиЁХЁї№■є■ иїў║√╣кЁкЁ.
create table #ResultStorage
	(WaybillHeaderID	int,
	WaybillStatus		int)

--+в°╕к·Ё Єиї№ї¤¤■∙ кЁё√°бv (¤Ё Є╕а·°∙ ╕√║вЁ∙, ї╕√° ■¤Ё ║Ўї ╕║Хї╕кЄ■ЄЁ√Ё ╕ ЇЁ¤¤v№° ° ¤ї ёv√Ё  їиї╕■ўЇЁ¤Ё)
truncate table #ResultStorage

--ж║и╕■и,  їиїё°иЁжХ°∙ Є╕ї ╕║Хї╕кЄ║жХ°ї жT ўЁ ║·ЁўЁ¤¤║ж ЇЁк║.
DECLARE WaybillHeader_Cursor CURSOR FORWARD_ONLY READ_ONLY
FOR SELECT WAYBILL_HEADER_ID FROM dbo.WAYBILL_HEADER as WBH
where WBH.[WAYBILL_DATE] > dateadd(hh, -1, @ReportDateTime - 1) 
	and WBH.[WAYBILL_DATE] <= @ReportDateTime 
OPEN WaybillHeader_Cursor
--жїиїё■и Є╕ї┐ жT ¤Ё ║·ЁўЁ¤¤║ж ЇЁк║.
FETCH NEXT FROM WaybillHeader_Cursor INTO @WaybillHeaderID

WHILE @@FETCH_STATUS = 0
BEGIN
	--Tvў■Є п║¤·б°°  ■√║вї¤°а ╕■╕к■а¤°а жT  ■ Єиї№ї¤° ° ¤■№їи║ жT. T■╕к■а¤°ї жT кЁ·■ї Ўї, ·Ё· ° ║ ё√°ЎЁ∙░їє■ WB_TRIP'Ё.
	--T■┐иЁ¤ї¤°ї иїў║√╣кЁкЁ Єvё■и·° Ї√а иЁ╕╕№Ёки°ЄЁї№■є■ жT Є■ Єиї№ї¤¤║ж кЁё√°б║.
	insert into #ResultStorage (WaybillHeaderID, WaybillStatus)
	values (@WaybillHeaderID, dbo.WaybillHeaderStatusByTimeGet(@WaybillHeaderID, @ReportDateTime))

	-- Get the next row.
	FETCH NEXT FROM WaybillHeader_Cursor INTO @WaybillHeaderID
END

CLOSE WaybillHeader_Cursor
DEALLOCATE WaybillHeader_Cursor
commit
*/

--select * from #ResultStorage

-- 2. Tvё■и·Ё ЇЁ¤¤v┐ Ї√а ¤ЁиаЇЁ  ■ °¤кїиї╕║жХ°№  ║кїЄv№ √°╕кЁ№.
--begin transaction
select distinct
	DR.EXT_NUMBER			DriverExtNumber,
	DR.SHORT_NAME			DriverShortName,
	DR.[NAME]			DriverName,
	WB.BEGIN_TIME			StartOfWork,
	WB.END_TIME			EndOfWork,
	WBH.EXT_NUMBER 			WHNum, 
	VH.GARAGE_NUMBER		GarageNumber,
	VGV.[VEHICLEGROUP_ID]	[VehicleGroupID],	-- єи║  Ё TT;
	cast(SC.EXT_NUMBER as int)	SNum
from
	-- Єиї№а иЁё■кv ╕№ї¤  ■ жT
	dbo.WAYBILL as WB 
	inner join dbo.WB_TRIP as WBT on WB.WAYBILL_ID = WBT.WAYBILL_ID 
	inner join dbo.WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
	-- Є■Ї°кї√° 
	inner join dbo.DRIVER as DR on DR.DRIVER_ID = WB.DRIVER_ID
	-- TT
	inner join dbo.VEHICLE as VH on VH.VEHICLE_ID = WBH.VEHICLE_ID 
	inner join dbo.VEHICLEGROUP_VEHICLE as VGV on VH.VEHICLE_ID = VGV.VEHICLE_ID
	-- иЁ╕ °╕Ё¤°ї
	inner join dbo.TRIP as TR on TR.TRIP_ID = WBT.TRIP_ID
	inner join dbo.SCHEDULE_DETAIL as SCD on SCD.SCHEDULE_DETAIL_ID = TR.SCHEDULE_DETAIL_ID
	inner join dbo.SCHEDULE as SC on SC.SCHEDULE_ID = SCD.SCHEDULE_ID
	left join v_operator_route_right rr on rr.route_id = SC.ROUTE_ID
where 
	WBH.WAYBILL_DATE = @ReportDateTime	
	and WBH.CANCELLED != 1
	--and WBH.WAYBILL_HEADER_ID in (select RS.WaybillHeaderID from #ResultStorage as RS where RS.WaybillStatus in (1, 2, 3, 4, 5, 6, 7, 10))
	and WBT.TRIP_KIND_ID in (5, 6)
	--Tvё°иЁжк╕а жT к■√╣·■ Ї√а TT ║·ЁўЁ¤¤■∙ ЁЄк■·■√■¤¤v. +╕√°  їиїЇЁ¤¤v∙ ¤■№їи ЁЄк■·■√■¤¤v иЁЄї¤ ¤║√ж °√° null, к■ ЇЁ¤¤vї Єvё°иЁжк╕а Ї√а Є╕ї┐ ЁЄк■·■√■¤¤
	and ((VGV.VEHICLEGROUP_ID = @gid and @gid > 0) or (VGV.VEHICLEGROUP_ID > 0 and @gid = 0))
	and (@operator_id is null or rr.operator_id = @operator_id)
order by	
	cast(SC.[EXT_NUMBER] as int), WB.BEGIN_TIME

--drop table #ResultStorage

--commit



