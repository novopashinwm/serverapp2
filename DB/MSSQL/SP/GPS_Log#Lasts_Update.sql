﻿IF (OBJECT_ID(N'[dbo].[GPS_Log#Lasts_Update]') IS NOT NULL)
	DROP PROCEDURE [dbo].[GPS_Log#Lasts_Update];
GO

CREATE PROCEDURE [dbo].[GPS_Log#Lasts_Update]
(
	@vehicle_id int,
	@log_time   int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@rows   int = 0;

	UPDATE [dbo].[GPS_Log#Lasts]
	SET [LOG_TIME#Last] =
		CASE
			WHEN [LOG_TIME#Last] < @log_time THEN @log_time
			ELSE [LOG_TIME#Last]
		END
	WHERE [VEHICLE_ID] = @vehicle_id

	SELECT @rows = @@ROWCOUNT

	IF (0 = @rows)
	BEGIN
		INSERT INTO [dbo].[GPS_Log#Lasts] ([VEHICLE_ID], [LOG_TIME#Last])
		VALUES                            (@vehicle_id,  @log_time)
	END
END
GO