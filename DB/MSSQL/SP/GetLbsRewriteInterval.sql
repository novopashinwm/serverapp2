if exists (select * from sys.objects where object_id=object_id('dbo.GetLbsRewriteInterval'))
	drop function dbo.GetLbsRewriteInterval
go
create function dbo.GetLbsRewriteInterval() 
returns int 
as 
begin
	return 5 * 60
end
go