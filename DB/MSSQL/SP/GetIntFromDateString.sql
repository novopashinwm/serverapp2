IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIntFromDateString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetIntFromDateString]

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/*
select dbo.GetIntFromDateString ('2010-04-01 21:12:22')
*/

CREATE   FUNCTION 
	[dbo].[GetIntFromDateString](
		@DateString varchar(25)
		)  RETURNS int

AS  
BEGIN 
	RETURN datediff(s, cast('1970-01-01' as datetime), cast(@DateString as datetime));
END




















