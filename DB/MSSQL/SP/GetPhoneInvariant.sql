IF (OBJECT_ID(N'[dbo].[GetPhoneInvariant]', N'FN') IS NOT NULL)
	DROP FUNCTION [dbo].[GetPhoneInvariant];
GO
-- SELECT [dbo].[GetPhoneInvariant](    '200060876')
-- SELECT [dbo].[GetPhoneInvariant](   '6200060876')
-- SELECT [dbo].[GetPhoneInvariant](  '86200060876')
-- SELECT [dbo].[GetPhoneInvariant]( '5756200060876')
-- SELECT [dbo].[GetPhoneInvariant]('+5756200060876')
CREATE FUNCTION [dbo].[GetPhoneInvariant]
(
	@phone varchar(20)
)
RETURNS varchar(20) AS
BEGIN
	DECLARE @result varchar(20) = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@phone, '+', ''), ' ', ''), '-', ''), ')', ''), '(', '')
	-- �� ����� ��� �����?
	IF (0 = ISNUMERIC(@result))
		RETURN NULL;
	-- ��� ������� ������ 10 ���� �������, ��� ��� ������ (���������� �����)
	IF (10 = LEN(@result))
		SET @result = '7' + @result;
	-- ��� ������� 11 ���� � ������ '8' �������, ��� ��� ������ (���������� �����)
	IF (11 = LEN(@result) AND '8' = SUBSTRING(@result, 1, 1))
		SET @result = '7' + SUBSTRING(@result, 2, LEN(@result) - 1);
	-- �� ��������� ������ ����� 10 ���� � �� ����� 15 (https://ru.wikipedia.org/wiki/E.164)
	IF (11 > LEN(@result) OR 15 < LEN(@result))
		RETURN NULL;
	RETURN @result
END
GO