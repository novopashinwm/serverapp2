IF (OBJECT_ID(N'[dbo].[AddControllerTypeCommand]') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[AddControllerTypeCommand]
END
GO

CREATE PROCEDURE [dbo].[AddControllerTypeCommand]
(
	@controllerTypeName nvarchar(50),
	@commandTypeCode    varchar(200)
)
AS
BEGIN
	INSERT INTO [dbo].[Controller_Type_CommandTypes]
	SELECT ctr.[Controller_Type_ID], cmd.[id]
	FROM [dbo].[CONTROLLER_TYPE] ctr, [dbo].[CommandTypes] cmd
		WHERE ctr.[TYPE_NAME] = @controllerTypeName
		AND   cmd.[code]      = @commandTypeCode
		AND NOT EXISTS
		(
			SELECT
				1
			FROM [dbo].[Controller_Type_CommandTypes]
			WHERE [Controller_Type_ID] = ctr.[CONTROLLER_TYPE_ID]
			AND   [CommandTypes_ID]    = cmd.id
		)
END
GO