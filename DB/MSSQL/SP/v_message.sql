if exists (
	select *
		from sys.views 
		where name = 'v_message'
)
	drop view v_message
go

create view v_message as
	select m.MESSAGE_ID
	    , m.Created
	    , CreatedMsk = dateadd(hour, 3, m.Created)
	    , UtcTime = m.Time
		, MskTime = dateadd(hour, 3, m.TIME)
		, mt.NAME, m.Owner_Operator_ID
		, m.SUBJECT
		, m.BODY
		, Contacts = (select 
			mct.Name + ' ' + ct.Name + ': ' + c.Value + 
				ISNULL(' (' + (select Value from Contact dc where dc.ID = c.Demasked_ID) + ')', '')
				 + ' '
			from Message_Contact mc
			join Contact c on c.ID = mc.Contact_ID
			join ContactType ct on ct.Id = c.Type
			join MessageContactType mct on mct.Id = mc.Type
			where mc.Message_ID = m.MESSAGE_ID
			for xml path (''))
		, Fields = (select mtf.Name + ': ' + isnull(mf.Content, 'NULL') + ' '
						from MESSAGE_TEMPLATE_FIELD mtf
						join MESSAGE_FIELD mf on mf.MESSAGE_TEMPLATE_FIELD_ID = mtf.MESSAGE_TEMPLATE_FIELD_ID
						where mf.MESSAGE_ID = m.MESSAGE_ID
						for xml path (''))
		, State = case m.Status when 1 then 'Wait' when 2 then 'Pending' when 3 then 'Done' else 'Other (' + convert(varchar(10), m.Status) + ')' end
		, ProcessingResult = case ProcessingResult   
							  when 0 then 'Unknown'  
							  when 1 then 'Received'  
							  when 2 then 'Processing'  
							  when 3 then 'Sent'  
							  when 4 then 'ContactDoesNotExist'  
							  when 5 then 'UnsuccessfulBillingResult'  
							  when 6 then 'Throttled'  
							  when 7 then 'MessageQueueFull'  
							  when 8 then 'InvalidMessageLength'  
							  when 9 then 'InvalidDestinationAddress'  
							  when 10 then 'NoConnectionWithGateway'  
							  when 11 then 'HttpError'   
							  when 12 then 'Timeout'      
							  when 13 then 'ServiceTurnedOff'  
							  when 14 then 'Error'     
							  when 15 then 'DestinationIsNotSpecified'  
							  when 16 then 'Queued'  
							  when 17 then 'DestinationContactIsLocked'
							  when 19 then 'DestinationContactIsNotValid'
							  when 20 then 'Delivered'
							  when 21 then 'Undeliverable'
							  when 22 then 'Deleted'
							  when 23 then 'Rejected'
							  when 24 then 'Delivered'
							  when 25 then 'Read'
							  when 26 then 'CancelledByCommand'
							  when 27 then 'MissingAppId'
							  when 28 then 'MismatchAppId'
							  when 29 then 'NoApproriateProcessor'
							  when 30 then 'UnsupportedTemplate'
							  else CONVERT(varchar, ProcessingResult)  
							 end  
		, DestinationType_ID
		, DestinationType_Name = 
			case DestinationType_ID 
				when 0 then 'Unknown'
				when 1 then 'RedHatSmsGateway'
				when 2 then 'Smpp'
				when 3 then 'MpxSms'
				when 6 then 'Email'
				when 7 then 'Operator'
				when 8 then 'Server'
				when 9 then 'Android'
				when 10 then 'AppleiOS'
				when 11 then 'MpxLbs'
				when 12 then 'Modem'
				when 13 then 'AppleiOSDebug'
				when 14 then 'Notification'
				when 15 then 'Web'
				else 'Other: ' + CONVERT(varchar(10), DestinationType_ID)
			end
		from MESSAGE m 
		left outer join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = m.TEMPLATE_ID
		
