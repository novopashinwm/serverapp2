set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecalcStatisticLogAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RecalcStatisticLogAll]
GO

CREATE PROCEDURE [dbo].[RecalcStatisticLogAll]
	@maxLogAge int = null,
	@debug bit = null
AS
BEGIN

	if @debug is null
		set @debug = 0
		
	if @debug = 0
		set nocount on;

	declare @vehicle_id int
	
	declare @logTimeTo int; 
	select @logTimeTo = datediff(ss, '1970', getutcdate()) - 300;

	--����� 2-� ������� �� �������������
	declare @minLogHistory int = datediff(ss,'1970',getutcdate()) - isnull(@maxLogAge, 3600*24*31*2)

	if @debug = 1
		print '������ ���� ��������� � ����������� ���������'
		
	--������ ���� ��������� � ����������� ���������
	update dbo.Statistic_Log_Recalc
	set log_time = @minLogHistory + 10
	where log_time < @minLogHistory

	if @debug = 1
		print '������� ������ ��������� ��� ������� ������� ����� ����'

	update dbo.Statistic_Log_Recalc
		set pending = 0
		where pending = 1 
		and abs(datediff(minute, pending_date, getutcdate())) > 60
	
	
	declare @relaxation_timeout int = 300
	
	declare @max_interval int = 3*3600

	while 1=1
	begin
		set @vehicle_id = (
			select top 1 slr.vehicle_id 
				from dbo.Statistic_Log_Recalc slr 
				cross apply (
					select top(1) lt.InsertTime
						from dbo.Log_Time lt with (nolock) 
						where lt.Vehicle_ID = slr.Vehicle_ID 
						  and lt.Log_Time <= slr.Log_Time
					    order by lt.Log_Time desc) lt
				where isnull(slr.pending, 0) = 0
				and slr.log_time < @logTimeTo
				and @relaxation_timeout < datediff(second, lt.InsertTime, getutcdate()) --������ ��� ����������� ������ �� ������� �������������, �.�. ��� ��� � ��� ����� ����� ����������� ���� ������ ������
				order by isnull(slr.Pending_date, '1970') asc)
		
		if @debug = 1
			print 'Vehicle to recalc: ' + isnull(convert(varchar(12), @vehicle_id), 'none')
				
		if (@vehicle_id is null)
			break;

		--TODO: ���� ������ ��� ���� �� �������, � ����� ����������� �� ���������� ���� 
		--� �� ���������� ���� ��� �� ������ �� �����, ���-�� ������ ������� ������� ���������
		
		if @debug = 1
			print 'exec dbo.RecalcStatisticLog @vehicleID=' + convert(varchar(12), @vehicle_id) + ',@maxInterval=' + convert(varchar(12), @max_interval) + ', @debug = ' + convert(varchar(12), @debug)

		exec dbo.RecalcStatisticLog @vehicleID=@vehicle_id,@maxInterval=@max_interval, @debug = @debug
		
	end
END