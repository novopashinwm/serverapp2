if exists (select * from sys.procedures where name = 'SendPositionRequest')
	drop procedure SendPositionRequest
go

create procedure SendPositionRequest
(
	@sourceLogin nvarchar(32),
	@targetLogin nvarchar(32),
	@visible bit = null
)
as

set nocount on

declare @contact dbo.Id_Param
insert into @contact
	select cloudContact.ID
		from Contact cloudContact
		join AppClient ac on ac.Contact_ID = cloudContact.ID
		join OPERATOR o on o.OPERATOR_ID = ac.Operator_ID
		where o.LOGIN = @targetLogin
		order by ac.ID desc

while 1=1
begin
	declare @contact_id int
	set @contact_id = (select top(1) Id from @contact order by Id desc)
	if @contact_id is null
		break
	delete @contact where id = @contact_id
	
	begin tran

	insert into MESSAGE (Status, Time, TEMPLATE_ID, Owner_Operator_ID)
		select 1, getutcdate(), 
			mt.Message_Template_ID, 1424
		from MESSAGE_TEMPLATE mt
		where mt.NAME = 'PositionRequest'

	declare @message_id int 
	set @message_id = @@identity

	insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
		select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID
			, convert(varchar(12), (
				select c.Vehicle_ID 
					from Asid a
					join MLP_Controller mc on mc.Asid_ID = a.ID
					join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
					join OPERATOR o on o.OPERATOR_ID = a.Operator_ID
					where o.LOGIN = @targetLogin))
			from MESSAGE_TEMPLATE_FIELD mtf
			join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
			where mtf.NAME = 'VehicleID'
			  and mt.NAME = 'PositionRequest'

	insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
		select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, (select top(1) msisdn from v_operator o where o.login = @sourceLogin)
			from MESSAGE_TEMPLATE_FIELD mtf
			join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
			where mtf.NAME = 'FriendMsisdn'
			  and mt.NAME = 'PositionRequest'

	insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
		select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, case @visible when 0 then 'false' else 'true' end
			from MESSAGE_TEMPLATE_FIELD mtf
			join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
			where mtf.NAME = 'Visible'
			  and mt.NAME = 'PositionRequest'

	insert into Message_Contact 
		select top(1) @message_id, 1, @contact_id

	commit
	
end