SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[GetVehicles]
@operator_id int = null
as
	set nocount on;
	
	if @operator_id is null
begin 
	select v.*
	from VEHICLE v(nolock) 
	order by GARAGE_NUMBER
	
	select c.*
	from CONTROLLER c (nolock)
end
else
begin
	create table #t (vehicle_id int primary key);
	
	insert into #t
		select vehicle_id 
		from v_operator_vehicle_right vr (nolock) 
		where vr.operator_id = @operator_id
		  and vr.right_id = 102 --VehicleAccess
	
	select v.*
	from #t
	join VEHICLE v(nolock) on v.VEHICLE_ID = #t.vehicle_id
	order by GARAGE_NUMBER

	select c.*
	from #t
	join CONTROLLER c (nolock) on c.VEHICLE_ID = #t.vehicle_id
	
	drop table #t;
end



