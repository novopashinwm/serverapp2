IF (OBJECT_ID(N'[dbo].[Monitoree_Log]', N'V') IS NOT NULL)
	DROP VIEW [dbo].[Monitoree_Log];
GO

CREATE VIEW [dbo].[Monitoree_Log]
AS
	SELECT
			[MONITOREE_ID] = logt.[Vehicle_ID],
			[LOG_TIME]     = logt.[Log_Time],
			[X]            = geol.[Lng],
			[Y]            = geol.[Lat],
			[SPEED]        = gpsl.[Speed],
			[MEDIA]        = logt.[Media],
			[SATELLITES]   = gpsl.[Satellites],
			[FIRMWARE]     = gpsl.[Firmware],
			[HEIGHT]       = gpsl.[Altitude]
	FROM [dbo].[Log_Time] logt WITH (NOLOCK)
		LEFT JOIN [dbo].[Geo_Log] geol WITH (NOLOCK)
			ON  geol.[LOG_TIME]   = logt.[Log_Time]
			AND geol.[Vehicle_ID] = logt.[Vehicle_ID]
		LEFT JOIN [dbo].[GPS_Log] gpsl WITH (NOLOCK)
			ON  gpsl.[Log_Time]   = logt.[Log_Time]
			AND gpsl.[Vehicle_ID] = logt.[Vehicle_ID]
GO