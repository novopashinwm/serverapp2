/****** Object:  StoredProcedure [dbo].[GetListsForRoster]    Script Date: 07/08/2010 18:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetListsForRoster]
	@operator_id int = null
as
begin

	exec GetSortedWayout @operator_id
	
	exec [GetSortedDriver] @operator_id
	
	exec GetSortedVehicle @operator_id
	
	exec GetSortedController @operator_id
	
	select distinct vg.* 
	from VEHICLEGROUP vg 
	join v_operator_vehicle_groups_right vgr on vgr.vehiclegroup_id = vg.VEHICLEGROUP_ID
	where vgr.operator_id = @operator_id
	
	select distinct vgv.* 
	from VEHICLEGROUP_VEHICLE vgv
	join v_operator_vehicle_groups_right vgr on vgr.vehiclegroup_id = vgv.VEHICLEGROUP_ID
	join v_operator_vehicle_right vr on vr.vehicle_id = vgv.VEHICLE_ID
	where vgr.operator_id = @operator_id and vr.operator_id = @operator_id
		
	select distinct dg.* 
	from DRIVERGROUP dg
	join v_operator_driver_groups_right dgr on dgr.DRIVERGROUP_id = dg.DRIVERGROUP_ID
	where dgr.operator_id = @operator_id
	
	select distinct dgd.* 
	from DRIVERGROUP_DRIVER dgd
	join v_operator_driver_groups_right dgr on dgr.DRIVERGROUP_id = dgd.DRIVERGROUP_ID
	join v_operator_driver_right dr on dgd.DRIVER_ID = dr.DRIVER_ID
	where dgr.operator_id = @operator_id and dr.operator_id = @operator_id
	
	-- by Tanas L кЁ· ■¤■ ° Ї■√Ў¤■ ёvк╣
	-- select * from WAYBILL_HEADER_STATUS
	select * from TRIP_KIND
	
	select * from WAYBILLMARK

	select distinct rt.* 
	from ROUTE_TRIP rt
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where rr.operator_id = @operator_id

	
	select distinct gt.* 
	from GEO_TRIP as GT 
	inner join ROUTE_TRIP as RTR on RTR.GEO_TRIP_ID = GT.GEO_TRIP_ID
	join v_operator_route_right rr on rr.route_id = rtr.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operator_id
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operator_id)
	
end


/******************************************************************************
**		File: 
**		Name: dbo.GetListsForRoster
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/







