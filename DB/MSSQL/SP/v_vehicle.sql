if exists (
	select *
		from sys.views
		where name = 'v_vehicle'
)
	drop view v_vehicle
go

create view v_vehicle
as
select 
	  v.vehicle_id
	, Name = v.Garage_Number
	, Public_Number = REPLACE(public_number, ' ', '')
	, Department_ID = d.DEPARTMENT_ID
	, Department_Name = d.Name
	, ControllerTypeName = ct.TYPE_NAME
	, DEVICE_ID = CONVERT(varchar(32), ci.DEVICE_ID)
	, ci.Password
	, mc.Asid_ID
	, a.SimID
	, ControllerMsisdn = cpc.Value
	, AsidMsisdn = apc.Value
	, Asid = ac.Value
	, OwnOperatorId = a.Operator_ID
	from Vehicle v
	left outer join Department d on d.DEPARTMENT_ID = v.Department
	left outer join CONTROLLER c on c.VEHICLE_ID = v.VEHICLE_ID
	left outer join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
	left outer join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
	left outer join asid a on a.ID = mc.Asid_ID
	left outer join Contact cpc on cpc.ID = c.PhoneContactID
	left outer join Contact ac on ac.ID = a.Contact_ID
	left outer join Contact apc on apc.ID = ac.Demasked_ID
	left outer join Controller_Type ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID