/****** Object:  StoredProcedure [dbo].[GetRouteGroupsAndRoutes]    Script Date: 07/11/2010 13:16:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetRouteGroupsAndRoutes] 
	@operator_id int = null
AS
	select distinct r.* 
	from [ROUTE] r
	left join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
	where  @operator_id is null or rr.operator_id = @operator_id
	Order by Ext_Number
	
	 --Order by Convert(int, Ext_Number)
	select distinct rg.* 
	from [ROUTEGROUP] rg
	left join v_operator_route_groups_right rr on rr.routegroup_id = rg.ROUTEGROUP_ID
	where  @operator_id is null or rr.operator_id = @operator_id
	Order by rg.NAME
	
	select distinct rgr.* 
	from [ROUTEGROUP_ROUTE] rgr
	left join v_operator_route_groups_right rgrr on rgrr.routegroup_id = rgr.ROUTEGROUP_ID
	left join v_operator_route_right rr on rr.route_id = rgr.ROUTE_ID
	where  @operator_id is null 
	or (rr.operator_id = @operator_id and rgrr.operator_id = @operator_id)







