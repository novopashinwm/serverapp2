/****** Object:  StoredProcedure [dbo].[CreateDkSet]    Script Date: 07/09/2010 19:32:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[CreateDkSet]
	@NAME nvarchar(20),
	@departmentID int
as
begin
	set nocount on

	insert into DK_SET
		( [NAME], DEPARTMENT_ID )
	values
		( @NAME , @departmentID)

	select SCOPE_IDENTITY()
end












