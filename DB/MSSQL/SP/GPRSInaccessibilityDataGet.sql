/****** Object:  StoredProcedure [dbo].[GPRSInaccessibilityDataGet]    Script Date: 07/14/2010 19:15:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------------------
-- жи■бїЇ║иЁ Єvё■и·° ЇЁ¤¤v┐ Ї√а "+квїк ■ё °¤кїиЄЁ√Ё┐ ■к╕║к╕кЄ°а ЇЁ¤¤v┐ GPRS ■к TT"
-- LЄк■и: +иїёї¤¤°·■Є ж°┐Ё°√
--------------------------------------------------------------------------------------------

ALTER procedure [dbo].[GPRSInaccessibilityDataGet]
	(
	@ReportDate		datetime,	-- -ЁкЁ, ¤Ё ·■к■и║ж ўЁ иЁ░°ЄЁжк╕а ЇЁ¤¤vї ║Ўї Є UTC;
	@VehicleGroupID	int,		-- єи║  Ё TT;
	@ControllerTypeID	int,		-- T°  GSM-·■¤ки■√√їиЁ;
	@NeglibleInterval	int,		-- ж°¤°№Ё√╣¤v∙ ¤їиїє°╕ки°и║ї№v∙ °¤кїиЄЁ√ Є ╕ї·.
	@VehicleID		int	,	-- ·■¤·иїк¤■ї TT;
	@operator_id int = null
	)
as


--set @ReportDate = '05/10/2006 20:00'
--set @ReportDate = '06/29/2005 20:00'
--set @ReportDate = '05/06/2006 20:00'
--set @ReportDate = '11/10/2005 21:00'
--set @ReportDate = '12/31/2005 21:00'
--set @VehicleGroupID = 0 --6
--set @ControllerTypeID = 0
--set @NeglibleInterval = 300
--set @VehicleID = 335


if @ReportDate is null set @ReportDate = getutcdate()
if @VehicleGroupID is null set @VehicleGroupID = 0
if @VehicleID is null set @VehicleID = 0


-- Tиї№а (@ReportDate) Є UTC Є ╕ї·. ╕ 1970 
declare @TimeReferencePount int
set @TimeReferencePount = datediff(s, '1970', @ReportDate)


-- T■ўЇЁ¤°ї Єиї№ї¤¤■∙ кЁё√°бv Ї√а ¤Ёё■иЁ Є╕ї┐ TT
create table #VehicleTrip(	
	id int identity primary key, 
	VehicleID 		int, 
	VehicleGarageNumber	nvarchar(15),
	BeginTime     		datetime,
	EndTime     		datetime,
	TripKindID 		int,
	ControllerType 		int
	)

	-- +╕√° ║·ЁўЁ¤■ ·■¤·иїк¤■ї TT, ЄvёїиЁї№ Ї√а ■Ї¤■є■ TT
	if(@VehicleID > 0) 
	begin
		insert #VehicleTrip(
			VehicleID, 
			VehicleGarageNumber,
			BeginTime,
			EndTime,
			TripKindID,
			ControllerType
			)
		select distinct	
			VH.VEHICLE_ID, 
			VH.GARAGE_NUMBER,
			WBT.BEGIN_TIME,
			WBT.END_TIME,
			WBT.TRIP_KIND_ID,
   			CT.CONTROLLER_TYPE_ID
		from 
			dbo.VEHICLE as VH (nolock) 
			join dbo.WAYBILL_HEADER as WBH (nolock) on WBH.VEHICLE_ID = VH.VEHICLE_ID 
			join dbo.WB_TRIP as WBT (nolock) on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			join dbo.CONTROLLER as CT (nolock) on CT.VEHICLE_ID = VH.VEHICLE_ID
		where 	
			VH.VEHICLE_ID = @VehicleID
			--and WBT.BEGIN_TIME between @ReportDate and dateadd(s, 86400, @ReportDate)
            			and WBH.[WAYBILL_DATE] = @ReportDate 
			and WBH.CANCELLED != 1
		order by 	
			VH.VEHICLE_ID 
	end
	-- L¤Ёвї, ї╕√° ¤ї ║·ЁўЁ¤■ ·■¤·иїк¤■ї TT, ¤■ ║·ЁўЁ¤Ё єи║  Ё, ЄvёїиЁї№ Ї√а єи║  v
	else if(@VehicleGroupID > 0) 
	begin
		insert #VehicleTrip(
			VehicleID, 
			VehicleGarageNumber,
			BeginTime,
			EndTime,
			TripKindID,
			ControllerType
			)
		select distinct	
			VH.VEHICLE_ID, 
			VH.GARAGE_NUMBER,
			WBT.BEGIN_TIME,
			WBT.END_TIME,
			WBT.TRIP_KIND_ID,
   			CT.CONTROLLER_TYPE_ID
		from 
			dbo.VEHICLE as VH (nolock) 
			join dbo.VEHICLEGROUP_VEHICLE as VHG (nolock) on VH.VEHICLE_ID = VHG.VEHICLE_ID 
			join dbo.WAYBILL_HEADER as WBH (nolock) on WBH.VEHICLE_ID = VH.VEHICLE_ID 
			join dbo.WB_TRIP as WBT (nolock) on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			join dbo.CONTROLLER as CT (nolock) on CT.VEHICLE_ID = VH.VEHICLE_ID
		where 	
			VHG.VEHICLEGROUP_ID = @VehicleGroupID
			--and WBT.BEGIN_TIME between @ReportDate and dateadd(s, 86400, @ReportDate)
            			and WBH.[WAYBILL_DATE] = @ReportDate 
			and WBH.CANCELLED != 1
		order by 	
			VH.VEHICLE_ID 
	end
	-- L¤Ёвї, ї╕√° єи║  Ё ¤ї ║·ЁўЁ¤Ё, ЄvёїиЁї№ Ї√а Є╕ї┐
	else 
	begin
		insert #VehicleTrip(
			VehicleID, 
			VehicleGarageNumber,
			BeginTime,
			EndTime,
			TripKindID,
			ControllerType
			)
		select distinct	
			VH.VEHICLE_ID, 
			VH.GARAGE_NUMBER,
			WBT.BEGIN_TIME,
			WBT.END_TIME,
			WBT.TRIP_KIND_ID,
   			CT.CONTROLLER_TYPE_ID
		from 
			dbo.VEHICLE as VH (nolock) 
			join dbo.WAYBILL_HEADER as WBH (nolock) on WBH.VEHICLE_ID = VH.VEHICLE_ID 
			join dbo.WB_TRIP as WBT (nolock) on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
			join dbo.CONTROLLER as CT (nolock) on CT.VEHICLE_ID = VH.VEHICLE_ID
			left join v_operator_vehicle_right rr on rr.vehicle_id = VH.VEHICLE_ID
		where 	
			--WBT.BEGIN_TIME between @ReportDate and dateadd(s, 86400, @ReportDate)
            			WBH.[WAYBILL_DATE] = @ReportDate 
			and WBH.CANCELLED != 1
			and (@operator_id is null or rr.operator_id = @operator_id)
		order by 	
			VH.VEHICLE_ID 
	end

	-- +╕√° ║·ЁўЁ¤ ·■¤·иїк¤v∙ к°  ·■¤ки■√√їиЁ, ║ЇЁ√аї№ ¤ї╕■■кЄїк╕кЄ║жХ°ї ўЁ °╕°
	if(@ControllerTypeID > 0)
	begin
		delete from #VehicleTrip where #VehicleTrip.ControllerType != @ControllerTypeID
	end


-- T■ўЇЁ¤°ї Єиї№ї¤¤■∙ кЁё√°бv Ї√а ¤Ёё■иЁ Є╕ї┐ MONITOREE_LOG Ї√а ¤ЁёиЁ¤¤v┐ TT
create table #VehicleLog(	
	id int identity primary key, 
	VehicleID 		int, 
	LogTime 		int,
	X			float,
	Y			float,
	IntervalEnd		int
	)

	-- ж■√║вї¤°ї LogTime Ї√а ¤ЁёиЁ¤¤v┐ TT ( ■ #VehicleTrip)
	insert #VehicleLog
		(
		VehicleID, 
		LogTime,
		X,
		Y
		)
	select distinct	
		VT.VehicleID,
		ML.LOG_TIME,
		X,
		Y     
	from 
		dbo.MONITOREE_LOG as ML (nolock) 
		join #VehicleTrip as VT (nolock) on ML.MONITOREE_ID = VT.VehicleID 
	where 	
		ML.LOG_TIME between @TimeReferencePount and (@TimeReferencePount + 129600) --3600 * 24 + 3600 * 12)
		and isnull(ML.MEDIA, 1) = 1
		--and dateadd(s, ML.LOG_TIME, '1970') between VT.BeginTime and VT.EndTime
		and dateadd(s, ML.LOG_TIME, '1970') between 
			(select top 1 min(VT1.BeginTime) from #VehicleTrip as VT1 (nolock)
				where VT1.VehicleID = VT.VehicleID) 
			and (select top 1 max(VT2.EndTime) from #VehicleTrip as VT2 (nolock)
				where VT2.VehicleID = VT.VehicleID) 
	order by 	
		VT.VehicleID,
		ML.LOG_TIME

	-- IntervalEnd - T√їЇ║жХ°∙ LogTime Ї√а ЇЁ¤¤■є■ TT
	update #VehicleLog set 	
		#VehicleLog.IntervalEnd = (select min(VL1.LogTime) from #VehicleLog as VL1 (nolock) 
		where VL1.id = #VehicleLog.id + 1 and VL1.VehicleID = #VehicleLog.VehicleID and VL1.LogTime > #VehicleLog.LogTime)


-- T■ўЇЁ¤°ї Єиї№ї¤¤■∙ иїў║√╣к°и║жХї∙ кЁё√°бv 
create table #VehicleTripLog(	
	id int identity primary key, 
	VehicleGarageNumber	nvarchar(15),
	IntervalBegin		datetime,
	IntervalEnd		datetime,
	IntervalDuration	int,
	VehicleStateID 		int,
	reason			int	-- и°в°¤Ё ■к╕║к╕кЄ°а ЇЁ¤¤v┐
	)

	-- жЁ ■√¤ї¤°ї кЁё√°бv
	insert #VehicleTripLog
		(
		VehicleGarageNumber,
		IntervalBegin,
		IntervalEnd,
		reason
		)
	select distinct
		-- +ЁиЁЎ¤v∙ ¤■№їи TT
		VT.VehicleGarageNumber,
		-- =ЁвЁ√■ °¤кїиЄЁ√Ё ■к╕║к╕кЄ°а ЇЁ¤¤v┐ ( иї■ёиЁў■ЄЁ¤°ї Є √■·Ё√╣¤■ї Є ■квїкї)
		IntervalBegin = dateadd(s, VL.LogTime, '1970'), 
		-- ж■¤їб °¤кїиЄЁ√Ё ■к╕║к╕кЄ°а ЇЁ¤¤v┐ ( иї■ёиЁў■ЄЁ¤°ї Є √■·Ё√╣¤■ї Є ■квїкї)
		IntervalEnd = dateadd(s, VL.IntervalEnd, '1970'), 
		-- и°в°¤Ё ■к╕║к╕кЄ°а ЇЁ¤¤v┐ (0 - no GSM; 1 - no GPS)
		reason = case when VL.X > 180 or VL.Y > 180 then 1 else 0 end   
	from 
		#VehicleTrip as VT (nolock)
		left join #VehicleLog as VL (nolock) on VL.VehicleID = VT.VehicleID 
		--full join #VehicleLog as VL (nolock) on VL.VehicleID = VT.VehicleID 
	order by 
		VT.VehicleGarageNumber, 
		IntervalBegin

	-- L Ї■ёЁЄ√ї¤°ї  ■ в°╕к■∙ ╕ки■в·° Ї√а ·ЁЎЇ■є■ TT, вк■ёv ўЁ ■√¤°к╣ °┐ ЇЁ¤¤v№° Ї■  їиЄ■є■ √■єЁ, ї╕√°  їиЄv∙ √■є ёv√  ■╕√ї ¤ЁвЁ√Ё  їиЄ■є■ иї∙╕Ё
	insert #VehicleTripLog
		(
		VehicleGarageNumber
		)
	select distinct
		-- +ЁиЁЎ¤v∙ ¤■№їи TT
		VT.VehicleGarageNumber 
	from 
		#VehicleTrip as VT (nolock)
	order by 
		VT.VehicleGarageNumber


	-- L╕кЁ¤ЁЄ√°ЄЁї№ IntervalBegin (= ¤ЁвЁ√■  їиЄ■є■ иї∙╕Ё) ° IntervalEnd (=╕√їЇ║жХ°∙ №°¤°№Ё√╣¤v∙ IntervalBegin), ·■к■иvї иЁЄ¤v NULL
	-- Ї√а иї∙╕■Є, ·■к■иvї Ї■ √■єЁ
	update #VehicleTripLog set 	
		#VehicleTripLog.IntervalBegin = (select top 1 min(VT.BeginTime) from #VehicleTrip as VT (nolock)
			where VT.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber),
		#VehicleTripLog.IntervalEnd = (select min(VTL.IntervalBegin) from #VehicleTripLog as VTL (nolock) 
			where VTL.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber) 
		where #VehicleTripLog.IntervalBegin is null and #VehicleTripLog.IntervalEnd is null


	-- L к■Ўї ╕Ё№■ї - Ї■ёЁЄ√ї¤°ї  ■ в°╕к■∙ ╕ки■в·° Ї√а ·ЁЎЇ■є■ TT, вк■ёv ўЁ ■√¤°к╣ °┐ ЇЁ¤¤v№°  ■╕√ї  ■╕√їЇ¤їє■ √■єЁ, ї╕√°  ■╕√їЇ¤°∙ √■є ёv√ Ї■ ·■¤бЁ  ■╕√їЇ¤їє■ иї∙╕Ё
	insert #VehicleTripLog
		(
		VehicleGarageNumber
		)
	select distinct
		-- +ЁиЁЎ¤v∙ ¤■№їи TT
		VT.VehicleGarageNumber 
	from 
		#VehicleTrip as VT (nolock)
	order by 
		VT.VehicleGarageNumber


	-- L╕кЁ¤ЁЄ√°ЄЁї№ IntervalBegin (=  ■╕√їЇ¤°∙ IntervalEnd), ·■к■иvї иЁЄ¤v NULL
	-- кї їи╣ Ї√а иї∙╕■Є, ·■к■иvї  ■╕√ї √■єЁ
	update #VehicleTripLog set 	
		#VehicleTripLog.IntervalBegin = (select max(VTL.IntervalEnd) from #VehicleTripLog as VTL (nolock) 
			where VTL.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber) 
		where #VehicleTripLog.IntervalBegin is null and #VehicleTripLog.IntervalEnd is null

	-- Tї їи╣ ║╕кЁ¤ЁЄ√°ЄЁї№ IntervalEnd = (max)EndTime(·■¤їб  ■╕√їЇ¤їє■ иї∙╕Ё, кЁ· ·Ё· √■є■Є ё■√╣░ї ¤їк), ·■к■иv∙ иЁЄї¤ NULL, ¤■ Ї√а ·■к■и■є■ ї╕к╣ IntervalBegin
	update #VehicleTripLog set 	
		#VehicleTripLog.IntervalEnd = (select top 1 max(VT.EndTime) from #VehicleTrip as VT (nolock)  
			where VT.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber ) 
		where #VehicleTripLog.IntervalEnd is null and #VehicleTripLog.IntervalBegin is not null 

	-- Tї їи╣, Ї√а  ■√║вї¤¤v┐ °¤кїиЄЁ√■Є, иЁ╕в°кvЄЁї№ IntervalDuration 
	update #VehicleTripLog set 	
		#VehicleTripLog.IntervalDuration = datediff(s, #VehicleTripLog.IntervalBegin, #VehicleTripLog.IntervalEnd)
		where #VehicleTripLog.IntervalEnd is not null and #VehicleTripLog.IntervalBegin is not null
		and #VehicleTripLog.IntervalDuration is null  

	-- Tї їи╣, Ї√а  ■√║вї¤¤v┐ °¤кїиЄЁ√■Є,  ■√║вЁї№ VehicleStateID (= VehicleStateID  ■╕√їЇ¤їє■ иї∙╕Ё Є °¤кїиЄЁ√ї) 
	update #VehicleTripLog 
		set #VehicleTripLog.VehicleStateID = 
		-- ЄvёїиЁї№ ╕■╕к■а¤°ї Ї√а °¤кїиЄЁ√Ё
		isnull(
			(select top 1 VT.TripKindID from #VehicleTrip as VT (nolock)
  			where VT.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber
				and VT.BeginTime <= #VehicleTripLog.IntervalEnd 
				and VT.EndTime >= #VehicleTripLog.IntervalBegin
			order by VT.BeginTime desc),
			-- ї╕√°  ■√║вї¤ null, к■  ■Ї╕кЁЄ√аї№  ■╕√їЇ¤її °ўЄї╕к¤■ї ╕■╕к■а¤°ї
			(select top 1 VT.TripKindID from #VehicleTrip as VT (nolock)
  			where VT.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber
				and VT.BeginTime <= #VehicleTripLog.IntervalEnd 
			order by VT.BeginTime desc)
			)
		where #VehicleTripLog.IntervalBegin is not null and #VehicleTripLog.IntervalEnd is not null 

--select * from #VehicleTripLog order by VehicleGarageNumber, IntervalBegin, VehicleStateID

	
	--  и°в°¤Ё ■к╕║к╕кЄ°а ЇЁ¤¤v┐ (0 - no GSM; 1 - no GPS)
	update #VehicleTripLog set 	
		#VehicleTripLog.reason = (select top 1 VTL.reason from #VehicleTripLog as VTL 
			where VTL.VehicleGarageNumber = #VehicleTripLog.VehicleGarageNumber 
			and (VTL.IntervalBegin between #VehicleTripLog.IntervalBegin and #VehicleTripLog.IntervalEnd 
				or VTL.IntervalEnd between #VehicleTripLog.IntervalBegin and #VehicleTripLog.IntervalEnd))
		where #VehicleTripLog.reason is null


	-- LЇЁ√ї¤°ї ўЁ °╕ї∙, Є ·■к■иv┐ °¤кїиЄЁ√ ■к╕║к╕кЄ°а ЇЁ¤¤v┐ ¤ї  иїЄv░Ёїк №°¤°№Ё√╣¤v∙ ўЁЇЁ¤¤v∙
	delete from #VehicleTripLog where #VehicleTripLog.IntervalDuration < @NeglibleInterval        


select distinct 
	VehicleGarageNumber as VehcleGarageNumber, IntervalBegin, IntervalEnd, IntervalDuration, VehicleStateID, reason 
from 
	#VehicleTripLog 
where
	IntervalBegin is not null or IntervalEnd is not null
order by 
	VehicleGarageNumber, IntervalBegin, VehicleStateID


-- LЇЁ√ї¤°ї Єиї№ї¤¤v┐ кЁё√°б.
drop table #VehicleLog
drop table #VehicleTrip
drop table #VehicleTripLog

