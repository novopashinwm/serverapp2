if exists (
	select * from sys.procedures where name = 'WakeUpSleepingSmartphones'
)
	drop procedure WakeUpSleepingSmartphones
go

create procedure WakeUpSleepingSmartphones
(
	@timeoutSeconds int = null
)
as

if @timeoutSeconds is null
	set @timeoutSeconds = 5*60

declare @operator table (login nvarchar(32) primary key clustered)

insert into @operator
	select distinct o.LOGIN
		from TrackingSchedule ts
		outer apply (
			select top(1) l.InsertTime
				from Log_Time l with (nolock)
				where l.Vehicle_ID = ts.Vehicle_ID
				order by InsertTime desc
		) l
		cross apply (
			select a.operator_id, o.login
			from controller c 
			join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
			join asid a on a.ID = mc.Asid_ID
			join operator o on o.OPERATOR_ID = a.Operator_ID
			where c.VEHICLE_ID = ts.Vehicle_ID) o
		where ts.Enabled = 1
		  and exists (
			select 1 
				from v_operator_vehicle_right ovr 
				where ovr.operator_id = ts.Operator_ID 
				  and ovr.vehicle_id = ts.Vehicle_ID 
				  and ovr.right_id = 102)
		  and (l.InsertTime is null or @timeoutSeconds < datediff(second, l.InsertTime, GETUTCDATE()))
		  and o.LOGIN is not null and len(o.LOGIN) > 0
		  and exists (
			select 1 
				from AppClient ac 
				join Contact c on c.ID = ac.Contact_ID
				where ac.Operator_ID = o.Operator_ID 
				  and c.Type in (7,8))

set nocount on

declare @login nvarchar(32)

while 1=1
begin

	set @login = (select top(1) o.login from @operator o order by o.login asc)
	if @login is null 
		break
	delete @operator where login = @login

	exec SendPush @targetLogin=@login, @type='WakeUp', @visible=0

end