IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetOperatorInfoByLogin' 
)
   DROP PROCEDURE [dbo].GetOperatorInfoByLogin
GO

CREATE PROCEDURE [dbo].[GetOperatorInfoByLogin] 
	@login nvarchar(max)
AS
BEGIN

	set nocount on
	
	select 
		OPERATOR_ID,
		LOGIN,
		NAME
		from dbo.Operator o
		where o.Login = @login
	
END  
  
