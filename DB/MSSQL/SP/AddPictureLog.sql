IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddPictureLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddPictureLog]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[AddPictureLog]
(
	@vehicle_id int,
	@log_time	int,
	@camera_number int,
	@photo_number int,
	@picture	varbinary(max) = null,
	@url		varchar(255) = null
)
AS
BEGIN

	set nocount on

	insert dbo.Picture_Log (Vehicle_ID, Log_Time, Camera_Number, Photo_Number, Picture, Url)
		select @vehicle_id, @log_time, @camera_number, @photo_number, @picture, @url
			where not exists(select 1 
								from dbo.Picture_Log with (XLOCK, SERIALIZABLE) 
								where Vehicle_ID = @vehicle_id
								  and Log_Time = @log_time
								  and Camera_Number = @camera_number
								  and Photo_Number = @photo_number
								  );
								  
	if @@ROWCOUNT = 0
		update dbo.Picture_Log 
			set Picture = @picture
			  , Url = @url
			where Vehicle_ID = @vehicle_id
			  and Log_Time = @log_time
			  and Camera_Number = @camera_number
			  and Photo_Number = @photo_number

END
