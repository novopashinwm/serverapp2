IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DelPointsWeb]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[DelPointsWeb] 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Delete Points for Web
-- =============================================
CREATE PROCEDURE [dbo].[DelPointsWeb]  
	@point_id		int
AS
BEGIN
	
	declare @vertex_id int
	set @vertex_id = (select VERTEX_ID from WEB_POINT where POINT_ID = @point_id)

	delete from dbo.WEB_POINT where POINT_ID = @point_id 	

	delete from dbo.MAP_VERTEX where VERTEX_ID = @vertex_id


END
GO
