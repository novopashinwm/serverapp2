﻿IF (OBJECT_ID(N'[dbo].[Log_Time#Lasts_Update]') IS NOT NULL)
	DROP PROCEDURE [dbo].[Log_Time#Lasts_Update];
GO

CREATE PROCEDURE [dbo].[Log_Time#Lasts_Update]
(
	@vehicle_id  int,
	@log_time    int,
	@insert_time datetime
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@rows   int = 0;

	UPDATE [dbo].[Log_Time#Lasts]
	SET
		[LOG_TIME#Last]   = CASE WHEN [LOG_TIME#Last]   < @log_time    THEN @log_time    ELSE [LOG_TIME#Last]   END,
		[InsertTime#Last] = CASE WHEN [InsertTime#Last] < @insert_time THEN @insert_time ELSE [InsertTime#Last] END
	WHERE [VEHICLE_ID] = @vehicle_id

	SELECT @rows = @@ROWCOUNT

	IF (0 = @rows)
	BEGIN
		INSERT INTO [dbo].[Log_Time#Lasts] ([VEHICLE_ID], [LOG_TIME#Last], [InsertTime#Last])
		VALUES                             (@vehicle_id,  @log_time,       @insert_time)
	END
END
GO