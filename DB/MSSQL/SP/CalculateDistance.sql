/****** Object:  StoredProcedure [dbo].[CalculateDistance]    Script Date: 05/14/2009 17:50:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateDistance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CalculateDistance]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--dbo.CalculateDistance @from, @to, @vehicle_id
CREATE PROCEDURE [dbo].[CalculateDistance]
	@from	int = null,
	@to	int,
	@vehicle_id	int = null
AS
BEGIN
	SET NOCOUNT ON;
	select dbo.CalculateDistanceFN (@from, @to, @vehicle_id) as 'distance';
END



