/****** Object:  StoredProcedure [dbo].[GetAllStation]    Script Date: 07/07/2010 17:54:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAllStation] 
@operatorID int = null
AS
select distinct p.* 
from point p 
left join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
left join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
where @operatorID is null or gt.CREATOR_OPERATOR_ID = @operatorID
or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)
Order by Name









