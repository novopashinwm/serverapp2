if exists (
	select *
		from sys.procedures 
		where name = 'SetConstant'
)
	drop procedure SetConstant;
go

  
CREATE PROCEDURE dbo.SetConstant  
 @NAME nvarchar(50),  
 @VALUE nvarchar(3000)  
AS  
 if( EXISTS(select * from CONSTANTS where [NAME] = @NAME) )  
  update dbo.CONSTANTS set  
   [VALUE] = @VALUE  
  where  
   [NAME] = @NAME  
 else  
  insert dbo.CONSTANTS  
   ( [NAME], [VALUE] )  
  values  
   ( @NAME, @VALUE )  
  