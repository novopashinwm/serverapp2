/****** Object:  StoredProcedure [dbo].[GetVehicleGroupsAndVehicles]    Script Date: 07/14/2010 20:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------
-- Є■ўЄиЁХЁїк ЇЁ¤¤vї ■ Є╕ї┐ TT ° єи║  Ё┐ TT
-------------------------------------------

ALTER      PROCEDURE [dbo].[GetVehicleGroupsAndVehicles]
@operator_id int = null
AS
	select distinct vg.* 
	from dbo.[VEHICLEGROUP] vg
	left join v_operator_vehicle_groups_right rr on rr.vehiclegroup_id = vg.VEHICLEGROUP_ID
	where (@operator_id is null or rr.operator_id = @operator_id)
	order by vg.NAME
	
	select distinct v.* 
	from dbo.[VEHICLE] v
	left join v_operator_vehicle_right rr on rr.vehicle_id = v.VEHICLE_ID
	where (@operator_id is null or rr.operator_id = @operator_id)
	order by v.GARAGE_NUMBER
	
	select distinct vv.* 
	from dbo.[VEHICLEGROUP_VEHICLE] vv
	left join v_operator_vehicle_right rr on rr.vehicle_id = vv.VEHICLE_ID
	left join v_operator_vehicle_groups_right rr2 on rr2.vehiclegroup_id = vv.VEHICLEGROUP_ID
	where @operator_id is null 
	or (rr.operator_id = @operator_id and rr2.operator_id = @operator_id)
	
	select * from dbo.[CONTROLLER_TYPE]
	
	select distinct c.* 
	from dbo.[CONTROLLER] c
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	left join v_operator_vehicle_right rr on rr.vehicle_id = v.VEHICLE_ID
	where (@operator_id is null or rr.operator_id = @operator_id)
	
	select * from dbo.[SEATTYPE]
	select * from dbo.[VEHICLE_KIND]
	
	select distinct d.* 
	from dbo.DEPARTMENT d
	left join v_operator_department_right rr on rr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where (@operator_id is null or rr.operator_id = @operator_id)

