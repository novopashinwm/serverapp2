
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF EXISTS ( SELECT * FROM sysobjects WHERE name = N'GetLinesWeb' AND type = 'P')
    DROP PROCEDURE GetLinesWeb
GO

-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Get Lines for Web
-- =============================================

CREATE PROCEDURE [dbo].[GetLinesWeb]  
	--@map_id			int,
	@map_guid		uniqueidentifier,
	@operator_id	int
AS

------------------------------------------------
/*
-- Test
declare
	@map_guid		uniqueidentifier,
	@operator_id	int
set @map_guid = '{86aa79b0-461f-41fc-8b87-b7879ca3781a}'
set @operator_id = 22
*/
------------------------------------------------
	
declare @map_id	int
--if(@map_id is null)
	set @map_id = (select MAP_ID from MAPS where GUID = @map_guid)

select --* 
	WL.[LINE_ID],
	WL.[NAME],
	WL.[DESCRIPTION],
	WLV.[ORDER],
	V.X,
	V.Y
from [dbo].[WEB_LINE] as WL
	inner join WEB_LINE_VERTEX as WLV on WLV.LINE_ID = WL.LINE_ID
	inner join MAP_VERTEX as V on V.VERTEX_ID = WLV.VERTEX_ID 
where 
	V.MAP_ID = @map_id
	and WL.OPERATOR_ID = @operator_id
order by 
	WLV.[ORDER]










