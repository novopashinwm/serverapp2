if exists (select * from sys.procedures where name = 'SendPush')
	drop procedure SendPush
go

--SendPush @targetLogin='1424', @subject='���', @body='��� 26.06.2015 11:32: ������ �������� - ����!', @vehicle_id='30892', @date='2015-06-26T08:32:44Z', @visible=0
--SendPush @targetLogin='1424', @vehicle_id='30892', @date='2015-06-26T08:32:44Z', @visible=0, @type='DataUpdate', @dataType='Schedule', @debug=1
--select top(1) * from v_message order by message_id desc
CREATE procedure SendPush
(
	@targetLogin nvarchar(32),
	@subject nvarchar(255) = null,
	@body nvarchar(max) = null,
	@type varchar(255) = 'GenericNotification',
	@friendMsisdn varchar(255) = null,
	@vehicle_id int = null,
	@date varchar(255) = null,
	@visible bit = null,
	@dataType varchar(255) = null,
	@debug bit = null
)
as

if (@debug is null)
	set @debug = 0

if (@debug = 0)
	set nocount on

declare @contact dbo.Id_Param
insert into @contact
	select cloudContact.ID
		from Contact cloudContact
		join AppClient ac on ac.Contact_ID = cloudContact.ID
		join OPERATOR o on o.OPERATOR_ID = ac.Operator_ID
		where o.LOGIN = @targetLogin
		  and cloudContact.Valid = 1
		  and (cloudContact.LockedUntil is null or cloudContact.LockedUntil < GETUTCDATE())
		order by ac.ID desc

while 1=1
begin
	declare @contact_id int
	set @contact_id = (select top(1) Id from @contact order by Id desc)
	if @contact_id is null
		break
	delete @contact where id = @contact_id
	
	begin tran

		insert into MESSAGE (Status, Time, TEMPLATE_ID, Owner_Operator_ID, SUBJECT, BODY)
			select 1, getutcdate(), 
				mt.Message_Template_ID, o.Operator_ID
				, @subject, @body
			from MESSAGE_TEMPLATE mt
			join Operator o on o.LOGIN = @targetLogin
			where mt.NAME = @type

		if @@ROWCOUNT = 0
		begin
			print 'Message was not sent'
			rollback
			return
		end

		declare @message_id int 
		set @message_id = @@identity
	
		insert into Message_Contact 
			select top(1) @message_id, 1, @contact_id


		if @friendMsisdn is not null			
			insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
				select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, @friendMsisdn
					from MESSAGE_TEMPLATE_FIELD mtf
					join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
					where mtf.NAME = 'FriendMsisdn'
					  and mt.NAME = @type

		if @vehicle_id is not null
			insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
				select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, convert(int, @vehicle_id)
					from MESSAGE_TEMPLATE_FIELD mtf
					join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
					where mtf.NAME = 'VehicleID'
					  and mt.NAME = @type

		if @date is not null
			insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
				select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, @date
					from MESSAGE_TEMPLATE_FIELD mtf
					join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
					where mtf.NAME = 'date'
					  and mt.NAME = @type

		if @visible is not null
			insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
				select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, case @visible when 1 then 'true' else 'false' end
					from MESSAGE_TEMPLATE_FIELD mtf
					join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
					where mtf.NAME = 'visible'
					  and mt.NAME = @type

		if @dataType is not null			
			insert into MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID, CONTENT)
				select @message_id, mtf.MESSAGE_TEMPLATE_FIELD_ID, @dataType
					from MESSAGE_TEMPLATE_FIELD mtf
					join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = mtf.MESSAGE_TEMPLATE_ID
					where mtf.NAME = 'DataType'
					  and mt.NAME = @type
			

	commit
	
end