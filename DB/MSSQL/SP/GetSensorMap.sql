if exists (
	select * from sys.objects where name = 'GetSensorMap'
)
drop procedure GetSensorMap;
go

create procedure GetSensorMap
(
	@vehicleID int
)
as

	set nocount on

	select * 
		from v_controller_sensor_map
		where Vehicle_ID = @vehicleID
