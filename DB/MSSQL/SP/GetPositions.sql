set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetPositions' and type='p'))
	drop procedure dbo.GetPositions;
go

create procedure dbo.GetPositions
	@vehicle_log_time	dbo.Vehicle_Log_Time_Param readonly,
	@include_neighbour  bit = 0,
	@debug bit = NULL
as
begin
	
	if isnull(@debug, 0) = 0
		set nocount on;

	create table #vehicle_log_time (Vehicle_ID int, Log_Time int, primary key clustered (Vehicle_ID, Log_Time))
	insert into #vehicle_log_time
		select Vehicle_ID, Log_Time
			from @vehicle_log_time

	if @include_neighbour = 1
	begin
		
		insert into #vehicle_log_time	
			select distinct 
				   vlt.Vehicle_ID,
				   prev_gl.Log_Time
				from #vehicle_log_time vlt
				cross apply (select top(2) gl.Log_Time 
						from dbo.Geo_Log gl (nolock) 
						where gl.Vehicle_ID = vlt.Vehicle_ID 
						  and gl.Log_Time < vlt.Log_Time 
						order by gl.Log_Time desc) prev_gl
				where not exists (
					select 1
						from #vehicle_log_time e
						where e.vehicle_id = vlt.Vehicle_ID
						  and e.log_time = prev_gl.Log_Time)
		
		insert into #vehicle_log_time	
			select distinct 
				   vlt.Vehicle_ID,
				   next_gl.Log_Time
				from #vehicle_log_time vlt
				cross apply (select top(1) gl.Log_Time 
						from dbo.Geo_Log gl (nolock) 
						where gl.Vehicle_ID = vlt.Vehicle_ID 
						  and gl.Log_Time > vlt.Log_Time 
						order by gl.Log_Time asc) next_gl
				where not exists (
					select 1
						from #vehicle_log_time e
						where e.vehicle_id = vlt.Vehicle_ID
						  and e.log_time = next_gl.Log_Time)
	end

	select gl.Vehicle_ID, gl.Log_Time, gl.Lat, gl.Lng
		from #vehicle_log_time lt
		join dbo.Geo_Log gl (nolock) on gl.Vehicle_ID = lt.Vehicle_ID 
									and lt.Log_Time   = gl.Log_Time
									
end