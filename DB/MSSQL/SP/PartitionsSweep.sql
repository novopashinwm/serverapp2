﻿IF (OBJECT_ID(N'[dbo].[PartitionsSweep]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[PartitionsSweep];
GO
--EXEC [dbo].[PartitionsSweep]
CREATE PROCEDURE [dbo].[PartitionsSweep]
AS
BEGIN
	SET NOCOUNT ON;
	-------------------------------------------------------------
	-- Начальные параметры
	-------------------------------------------------------------
	DECLARE
		@dataLengthInDay int     = 366,                      -- Общая длина данных в днях
		@partLengthInDay int     = 005,                      -- Длина одной секции в днях
		@partitionFun    sysname = N'LogsPartitionFunction', -- Функция секционирования
		@partitionSch    sysname = N'LogsPartitionScheme',   -- Схема секционирования
		@partitionFlg    sysname = N'TSS_DATA_LOGS',         -- Файловая группа секционирования
		@partitionStg    sysname = N'#Stage',                -- Суффикс таблицы переключения секции
		@SqlText         nvarchar(max),
		@ObjName         nvarchar(max);
	-------------------------------------------------------------
	-- Вычисляем пределы необходимых секций
	-------------------------------------------------------------
	DECLARE @nowDate datetime = DATEADD(day, DATEDIFF(day, 0, GETUTCDATE()), 0);
	DECLARE @begData datetime = DATEADD(day, -1*@dataLengthInDay, @nowDate);
	DECLARE @endData datetime = DATEADD(day, +2*@partLengthInDay, @nowDate);
	-------------------------------------------------------------
	-- Параметры для вывода сообщений
	DECLARE @endTimeStamp datetime      = GETDATE();
	DECLARE @begTimeStamp datetime      = @endTimeStamp;
	DECLARE @durMessage   nvarchar(500) = NULL;
	-------------------------------------------------------------
	DECLARE @partitions TABLE
	(
		[PartitionObject]      int,
		[PartitionNumber]      int,
		[PartitionBoundaryNum] int,
		[PartitionBoundaryVal] int,
		[PartitionRows]        bigint
	);
	WITH
	B AS
	(
		SELECT
			[boundary_id]             = R.[boundary_id],
			[boundary_value_on_right] = F.[boundary_value_on_right],
			[boundary_value]          = CAST(R.[value] AS int)
		FROM sys.partition_functions F WITH(NOLOCK, NOWAIT)
			INNER JOIN sys.partition_range_values R WITH(NOLOCK, NOWAIT)
				ON F.[function_id] = R.[function_id]
		WHERE F.[name] = @partitionFun
	),
	P AS
	(
		SELECT
			P.[partition_number],
			P.[object_id],
			[rows] = SUM(P.[rows])
		FROM sys.partitions P WITH(NOLOCK, NOWAIT)
			INNER JOIN sys.indexes             I ON I.[object_id]     = P.[object_id] AND I.[index_id] = P.[index_id]
			INNER JOIN sys.partition_schemes   S ON S.[data_space_id] = I.[data_space_id]
			INNER JOIN sys.partition_functions F ON F.[function_id]   = S.[function_id]
		WHERE 1=1
			-- Указанная функция секционирования
			AND F.[name] = @partitionFun
			-- Указанная схема секционирования
			AND S.[name] = @partitionSch
			-- Имя таблицы не содержит суффикса #Stage
			AND OBJECT_NAME(I.[object_id]) NOT LIKE N'%' + @partitionStg
			-- А таблица с именем объекта и суффиксом #Stage должна существовать
			AND OBJECT_ID(OBJECT_NAME(I.[object_id]) + @partitionStg, N'U') IS NOT NULL
		GROUP BY P.[partition_number], P.[object_id]
	)
	INSERT INTO @partitions
	SELECT
		[PartitionObject]       = P.[object_id],
		[PartitionNumber]       = P.[partition_number],
		[PartitionBoundaryNum]  = ISNULL(B.[boundary_id],    0),
		[PartitionBoundaryVal]  = ISNULL(B.[boundary_value], 0),
		[PartitionRows]         = P.[rows]
	FROM P
		LEFT JOIN B
			ON B.[boundary_id] + B.[boundary_value_on_right] = P.[partition_number]
	ORDER BY P.[object_id], P.[partition_number] DESC;
	-------------------------------------------------
	-- Поиск максимальной границы, если отсутствует, то начало месяца от @begData
	DECLARE @BVal int =
	(
		SELECT ISNULL
		(
			MAX([PartitionBoundaryVal]), [dbo].[utc2lt](DATEADD(month, DATEDIFF(month, 0, @begData), 0))
		)
		FROM @partitions
	);
	-------------------------------------------------
	-- Поиск максимальной даты в данных
	DECLARE @DMax int =
	(
		SELECT ISNULL
		(
			
			MAX([Log_Time#Last]), [dbo].[utc2lt](DATEADD(month, DATEDIFF(month, 0, @begData), 0))
		)
		FROM [dbo].[Log_Time#Lasts]
	);
	-------------------------------------------------
	-- Создаем границы
	-------------------------------------------------
	WHILE (DATEDIFF(day, @endData, [dbo].[lt2utc](@BVal)) <= 0)
	BEGIN
		-------------------------------------------------
		SET @BVal = [dbo].[utc2lt](DATEADD(day, @partLengthInDay, [dbo].[lt2utc](@BVal)));
		-- Создаем границы, там где нет данных
		IF (@BVal > @DMax)
		BEGIN
			-------------------------------------------------
			SET @durMessage = CONVERT(nvarchar, [dbo].[lt2utc](@BVal), 120) + N', ' + CAST(DATEDIFF(day, DATEADD(day, @partLengthInDay, @endData), [dbo].[lt2utc](@BVal)) AS nvarchar);
			RAISERROR(N'Insert boundary: %s', 0, 1, @durMessage) WITH NOWAIT;
			-------------------------------------------------
			SET @SqlText = ''
				+ 'ALTER PARTITION SCHEME   [' + @partitionSch + '] NEXT USED [' + @partitionFlg + '];'
				+ 'ALTER PARTITION FUNCTION [' + @partitionFun + ']() SPLIT RANGE (' + CAST(@BVal AS nvarchar) + ');';
			-------------------------------------------------------------
			RAISERROR(N'%s', 0, 1, @SqlText) WITH NOWAIT;
			EXEC(@SqlText);
			-------------------------------------------------------------
		END
	END
	SELECT @ObjName = NULL, @SqlText = NULL;
	DECLARE BIDS_CUR CURSOR
	FORWARD_ONLY READ_ONLY STATIC LOCAL
	FOR
		SELECT DISTINCT [PartitionBoundaryVal] FROM @partitions ORDER BY [PartitionBoundaryVal]
	OPEN BIDS_CUR;
	FETCH NEXT FROM BIDS_CUR INTO @BVal;
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		-------------------------------------------------
		-- Удаляем данные
		IF
		(
			(DATEDIFF(day, [dbo].[lt2utc](@BVal), @begData             ) >= @partLengthInDay)
			--OR
			--(DATEDIFF(day, @endData,              [dbo].[lt2utc](@BVal)) >= @partLengthInDay)
		)
		BEGIN
			-------------------------------------------------
			SET @durMessage = CONVERT(nvarchar, [dbo].[lt2utc](@BVal), 120);
			RAISERROR(N'Delete boundary: %s', 0, 1, @durMessage) WITH NOWAIT;
			-------------------------------------------------
			DECLARE TBLS_CUR CURSOR
			FORWARD_ONLY READ_ONLY STATIC LOCAL
			FOR
				SELECT DISTINCT
					[ObjName] = QUOTENAME(OBJECT_SCHEMA_NAME(I.[object_id], DB_ID())) + '.' + QUOTENAME(OBJECT_NAME(I.[object_id], DB_ID())),
					[SqlText] = '' +
						'TRUNCATE TABLE '
						+ QUOTENAME(OBJECT_SCHEMA_NAME(I.[object_id], DB_ID())) + '.' + QUOTENAME(OBJECT_NAME(I.[object_id], DB_ID()) + @partitionStg) + '; '
						+ 'ALTER TABLE '
						+ QUOTENAME(OBJECT_SCHEMA_NAME(I.[object_id], DB_ID())) + '.' + QUOTENAME(OBJECT_NAME(I.[object_id], DB_ID()))
						+ ' SWITCH PARTITION $PARTITION.' + @partitionFun + '(' + CAST(@BVal AS nvarchar) + ') TO '
						+ QUOTENAME(OBJECT_SCHEMA_NAME(I.[object_id], DB_ID())) + '.' + QUOTENAME(OBJECT_NAME(I.[object_id], DB_ID()) + @partitionStg)
						+ ' PARTITION $PARTITION.' + @partitionFun + '(' + CAST(@BVal AS nvarchar) + ') WITH (WAIT_AT_LOW_PRIORITY (MAX_DURATION = 0 MINUTES, ABORT_AFTER_WAIT = NONE)); '
						+ 'TRUNCATE TABLE '
						+ QUOTENAME(OBJECT_SCHEMA_NAME(I.[object_id], DB_ID())) + '.' + QUOTENAME(OBJECT_NAME(I.[object_id], DB_ID()) + @partitionStg) + ';'
				FROM
				(
					SELECT DISTINCT [object_id] = [PartitionObject] FROM @partitions WHERE [PartitionBoundaryVal] = @BVal
				) I
			OPEN TBLS_CUR
			FETCH NEXT FROM TBLS_CUR INTO @ObjName, @SqlText;
			WHILE (0 = @@FETCH_STATUS)
			BEGIN
				SET @begTimeStamp = GETDATE();
				-------------------------------------------------------------
				--RAISERROR(N'%s', 0, 1, @SqlText) WITH NOWAIT;
				EXEC(@SqlText);
				-------------------------------------------------------------
				SET @ObjName = @ObjName + SPACE(30 - LEN(@ObjName));
				SET @endTimeStamp = GETDATE();
				SET @durMessage   = CONVERT(varchar, DATEDIFF(second, @begTimeStamp, @endTimeStamp));
				RAISERROR(N'SWITCH: %s Duration: %s second', 0, 1, @ObjName, @durMessage) WITH NOWAIT;
				-------------------------------------------------------------
				FETCH NEXT FROM TBLS_CUR INTO @ObjName, @SqlText;
			END
			CLOSE      TBLS_CUR;
			DEALLOCATE TBLS_CUR;
			-------------------------------------------------
			-- Удаляем границу
			IF (0 < @BVal)
			BEGIN
				SET @SqlText = 'ALTER PARTITION FUNCTION ' + @partitionFun + '() MERGE RANGE (' + CAST(@BVal AS nvarchar) + ');';
				-------------------------------------------------------------
				RAISERROR(N'%s', 0, 1, @SqlText) WITH NOWAIT;
				EXEC(@SqlText);
				-------------------------------------------------------------
			END
			-------------------------------------------------
		END
		FETCH NEXT FROM BIDS_CUR INTO @BVal;
	END
	CLOSE      BIDS_CUR;
	DEALLOCATE BIDS_CUR;
END
GO