if exists (
	select * from sys.views where name = 'v_operator_contact'
)
	drop view v_operator_contact;
go

create view v_operator_contact
WITH SCHEMABINDING
as
select Operator_ID = oc.Operator_ID
	, Contact_ID = c.ID
    , Type = c.Type
    , Value = c.Value
    , Confirmed = oc.Confirmed
	, ConfirmationSentDate
	from dbo.Operator_Contact oc 
	join dbo.Contact c on c.ID = oc.Contact_ID
	
go

create unique clustered index IX_v_operator_contact on v_operator_contact(Operator_ID, Type, Value)
