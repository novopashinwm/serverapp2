set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddPositionRadius]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddPositionRadius]
GO

CREATE PROCEDURE [dbo].[AddPositionRadius]
	 @log_time		int
	,@vehicle_id	int
	,@radius		int				--position accuracy radius, meters
	,@type			tinyint
AS
BEGIN
	set nocount on

	declare @rows_affected int = 0;

	if @type is null
		set @type = 0

	--insert if not present
	insert dbo.position_radius(vehicle_id, log_time, Radius, [Type]) 
		select @vehicle_id, @log_time, @radius, @type
		where not exists(	-- add rec
			select 0 from dbo.position_radius  with (XLOCK, SERIALIZABLE) 
			where Log_Time = @log_time and Vehicle_ID = @vehicle_id)
			
	set @rows_affected = @rows_affected + @@ROWCOUNT

	if @@rowcount = 0
	begin
		begin	-- update existing
			update dbo.position_radius 
				set radius = @radius,
					[Type] = @type
				where LOG_TIME = @log_time and 
					  Vehicle_ID = @vehicle_id and (
					  Radius <> @radius or
					  [Type] <> @type)
					  
		end -- update existing
		
		set @rows_affected = @rows_affected + @@ROWCOUNT		
	end
	
	select @rows_affected
END
