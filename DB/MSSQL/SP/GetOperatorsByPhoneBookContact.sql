if exists (select * from sys.procedures where name='GetOperatorsByPhoneBookContact')
	drop procedure GetOperatorsByPhoneBookContact
go
create procedure GetOperatorsByPhoneBookContact (
	@msisdn varchar(15)
) as

set QUOTED_IDENTIFIER on

select OperatorId
from Operator_PhoneBook
where PhoneBook.exist('/contacts/contact/Value[.=sql:variable("@msisdn")]') = 1
go