alter PROCEDURE [dbo].[GetUnitInfo]
 @uniqueID  int = null,    -- vehicle pkey. till good time  
 @id    int = null,   -- unit no.  
 @phone   varchar(20) = null, -- phone  
 @controllerID int = null,  -- controller id  
 @device_id varbinary(32) = null,
 @asid varchar(255) = null, --F(MSISDN) from AMLC
 @simId varchar(32) = null  --������������� ���������� ������������
AS  

set nocount on
	/*� ��������� �����-�� �������������� ������� �������� read uncommited, ��� ��������� � ����� ������� � ������������� ����������� � ��������� PK, �������� PK_Geo_Log
		������ ��������� ������ �������� (����� ������������� ������� ��-���������), ����� ��������� � TFS.
*/
  
 -- 1 vehicle can have few controllers. it's not implemented in app yet  
 -- so it's poor decision  
 if @uniqueID > 0  
  -- very poor decision  
  select top 1 @controllerID = CONTROLLER_ID, @id = NUMBER, @phone = pc.Value
  from dbo.CONTROLLER c
  left outer join Contact pc on pc.ID = c.PhoneContactID
  where VEHICLE_ID = @uniqueID  
 --  
 else if @asid is not null
 begin
   select @uniqueID = c.VEHICLE_ID, 
		  @controllerID = c.CONTROLLER_ID,
		  @id = c.NUMBER, 
		  @phone = pc.Value
	from Asid a
	join MLP_Controller mlpc on mlpc.Asid_ID = a.ID
	join CONTROLLER c on c.Controller_ID = mlpc.Controller_ID
	join Vehicle v on v.Vehicle_ID = c.Vehicle_ID
	join contact contact on contact.ID = a.Contact_ID
	left outer join Contact pc on pc.ID = c.PhoneContactID
	where contact.Value = @asid and contact.Type = 3

	if isnull(@uniqueID, 0) < 1
	begin
		select  @uniqueID = c.VEHICLE_ID, 
				@controllerID = c.CONTROLLER_ID,
				@id = c.NUMBER, 
				@phone = pc.Value
		from CONTROLLER c
		join contact pc on pc.ID = c.PhoneContactID
		join Contact ac on ac.Demasked_ID = pc.ID
		where ac.Value = @asid and ac.Type = 3
	end

end
else if @simId is not null
begin
  select @uniqueID = c.VEHICLE_ID, 
		 @controllerID = c.CONTROLLER_ID,
		 @id = c.NUMBER, 
		 @phone = pc.Value
	from Asid a
	join MLP_Controller mlpc on mlpc.Asid_ID = a.ID
	join CONTROLLER c on c.Controller_ID = mlpc.Controller_ID
	left outer join Contact pc on pc.ID = c.PhoneContactID
	join Vehicle v on v.Vehicle_ID = c.Vehicle_ID
	where a.SimId = @simId
end
  else if @device_id is not null and @device_id <> 0x
  select @controllerID = v.CONTROLLER_ID
	   , @uniqueID = v.VEHICLE_ID
	   , @id = c.NUMBER
	   , @phone = pc.Value
  from v_VehicleByDeviceID v (noexpand)
  left outer join dbo.CONTROLLER c on c.CONTROLLER_ID = v.Controller_ID
  left outer join Contact pc on pc.ID = c.PhoneContactID
  where v.Device_ID = @device_id  
 else if @id > 0  
  select @controllerID = CONTROLLER_ID, @uniqueID = VEHICLE_ID, @phone = pc.Value
  from dbo.CONTROLLER c
  left outer join Contact pc on pc.ID = c.PhoneContactID
  where c.NUMBER = @id  
 else if @phone is not null and @phone <> ''  
  select @controllerID = CONTROLLER_ID, @uniqueID = VEHICLE_ID, @id = NUMBER   
  from dbo.CONTROLLER c
  join Contact pc on pc.ID = c.PhoneContactID
  where pc.Type = 2 and pc.Value = @phone
 else if @controllerID > 0  
  select @uniqueID = VEHICLE_ID
	, @id = NUMBER
	, @phone = pc.Value
	from dbo.CONTROLLER c
	left outer join Contact pc on pc.ID = c.PhoneContactID
  where CONTROLLER_ID = @controllerID  
 else  
 begin  
  RaisError('Can''t find specified unit: no. = %d, phone = %s', 16, 1, @id, @phone)  
  return -1  
 end  
   
 if @@error <> 0 return -2  
 
 select @controllerID UnitID, @uniqueID [Unique], @id ID, isnull(@phone, '') Phone,   
  ct.CONTROLLER_TYPE_ID UnitType, TYPE_NAME TypeName, TYPE_NAME ObjectType
  , ci.Firmware
  , ci.[PASSWORD] as UnitPassword
  , ci.Device_ID
  , isnull(bb.BlockingDate, d.LockDate) BlockingDate
  , AsidId = asid.ID
  , Asid = asid.Value
  , StoreLog = cast(isnull(v.StoreLog, 0) as bit)
  , c.AllowDeleteLog
  , asid.SimId
  , v.StorageId
 from dbo.CONTROLLER c 
 join dbo.CONTROLLER_TYPE ct on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
 left outer join dbo.CONTROLLER_INFO ci ON c.CONTROLLER_ID = ci.CONTROLLER_ID
 left outer join Vehicle v on v.VEHICLE_ID = c.VEHICLE_ID
 left outer join DEPARTMENT d on v.DEPARTMENT = d.DEPARTMENT_ID
 outer apply (
	select a.ID, asidContact.Value, a.SimId
	from MLP_Controller mlpc 
	join Asid a on a.ID = mlpc.Asid_ID
	left outer join Contact asidContact on asidContact.ID = a.Contact_ID
	where mlpc.Controller_ID = c.CONTROLLER_ID) asid
 outer apply (
	select top(1) bb.BlockingDate
		from Billing_Blocking bb 
		where bb.Asid_ID = asid.ID
		order by bb.ID desc) bb 
  where @controllerID = c.CONTROLLER_ID  
  
 return 0  