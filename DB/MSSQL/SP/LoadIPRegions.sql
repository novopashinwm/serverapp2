set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadIPRegions]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LoadIPRegions]
GO

CREATE PROCEDURE [dbo].[LoadIPRegions] 
	@xml XML
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.IPRegions
	
	INSERT INTO dbo.IPRegions (CityId, Name, Region, District, Latitude, Longitude)
	SELECT  
		pv.ID.value('@CityId', 'INT'),
		pv.ID.value('@Name', 'VARCHAR(250)'),
		pv.ID.value('@Region', 'VARCHAR(250)'),
		pv.ID.value('@District', 'VARCHAR(250)'),
		pv.ID.value('@Latitude', 'DECIMAL(8,6)'),
		pv.ID.value('@Longitude', 'DECIMAL(9,6)')
	FROM    @xml.nodes('/Regions/Region') AS pv ( ID )
	
END






  