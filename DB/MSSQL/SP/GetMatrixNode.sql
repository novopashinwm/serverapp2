IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMatrixNode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMatrixNode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetMatrixNode]
(
	@lng float, @lat float, @scale int
)
RETURNS bigint
AS
BEGIN

--declare @lng float, @lat float, @scale int
--select @lng = 37.22322, @lat = 54.41234, @scale = 5

DECLARE @val bigint, @ten bigint;
select @val = 0, @ten = 10;
declare @intLat int, @intLng int, @ROUND_SCALE int
set @ROUND_SCALE = 100000;

if (@lat < -90)
	begin
    --���� ����� � ����������� �������� 180 | -180
		select @intLng = (360 + 180 + @lng) * @ROUND_SCALE;
    end   
else
	begin
         select @intLng = (@lng + 180) * @ROUND_SCALE;
	end
       

select  @intLat =  (@lat + 90) * @ROUND_SCALE;     


select @val = @scale*POWER(@ten, 15)
                       + Ceiling(cast(@intLng as float)/POWER(@ten, @scale))*POWER(@ten, 8 - @scale)
                       + Ceiling(cast(@intLat as float)/POWER(@ten, @scale))


return @val;
END

/*


select dbo.GetMatrixNode(37.223221, 54.412341, 5)
select dbo.GetMatrixNode(37.223221, 54.412341, 4)
select dbo.GetMatrixNode(37.223221, 54.412341, 3)
select dbo.GetMatrixNode(37.223221, 54.412341, 2)
select dbo.GetMatrixNode(37.223221, 54.412341, 1)
select dbo.GetMatrixNode(37.223221, 54.412341, 0)

5000000000218145
4000000021731445
3000002172314442
2000217224144413
1021722331444124
2172232214441234
*/