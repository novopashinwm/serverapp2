exec CreateProcedure 'RemoveControllerSensor'
go

alter procedure RemoveControllerSensor
(
	@controllerTypeName nvarchar(50),
	@name nvarchar(512)
)
as

delete map
	from CONTROLLER_TYPE ct 
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join CONTROLLER_SENSOR_MAP map on map.CONTROLLER_SENSOR_ID = cs.CONTROLLER_SENSOR_ID
	where ct.TYPE_NAME = @controllerTypeName 
	  and cs.Descript = @name

delete cs
	from CONTROLLER_TYPE ct 
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	where ct.TYPE_NAME = @controllerTypeName 
	  and cs.Descript = @name
