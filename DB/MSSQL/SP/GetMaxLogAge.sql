﻿if exists (
	select *	
		from sys.objects
		where type = 'FN'
		  and name = 'GetMaxLogAge'
)
	drop function GetMaxLogAge
go

create function GetMaxLogAge ()
returns int
as
begin
	--TODO: добавить параметр Vehicle_ID, чтобы опираться на услугу
	return  365 * 24 * 60 * 60 --полгода
end