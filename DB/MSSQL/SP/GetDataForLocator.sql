if exists (select * from sys.objects where type = 'p' and name = 'GetDataForLocator')
	drop procedure GetDataForLocator
go

set QUOTED_IDENTIFIER on

go

create procedure GetDataForLocator
as
set nocount on

declare @current_log_time int = datediff(ss, '1970', getutcdate())
declare @min_geo_delay int = 60 

declare @v table (id int primary key clustered, Cell_Log_Time int, Wlan_Log_Time int)

declare @default_lbsWiFiInterval int = 5*60

insert into @v
	select top(1) c.vehicle_id
		, cell.Log_Time
		, wlan.Log_Time
	from Controller c
	join Controller_Type ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
	left outer join v_vehicle_rule lbsGsm  on  lbsGsm.Vehicle_ID = c.Vehicle_ID  and lbsGsm.RuleNumber = 16  and lbsGsm.value > 0
	left outer join v_vehicle_rule lbsWifi on lbsWifi.Vehicle_ID = c.Vehicle_ID and lbsWifi.RuleNumber = 17 and lbsWifi.value > 0
	outer apply (
		select top(1) l.Log_Time
			from Geo_Log l with (nolock)
			where l.Vehicle_ID = c.Vehicle_ID
  			order by l.Log_Time desc
	) last_geo
	outer apply (
		select top(1) l.Log_Time
			from VehicleLocator l 
			where l.Vehicle_ID = c.Vehicle_ID
			order by l.Log_Time desc
	) last_locator
	outer apply (
		--TODO: ������ � ������������ ����� ������ � ������� ����������� ������������ ��������������� ����� 
		select top(1) l.Log_Time 
			from Cell_Network_Log l with (nolock) 
			where l.vehicle_id = c.Vehicle_ID
			  and l.log_time between isnull(last_geo.Log_Time, @current_log_time - isnull(lbsGsm.Value, ct.LocatorGsmTimeout)) and @current_log_time
			  and l.log_time > isnull(last_locator.Log_Time + isnull(lbsGsm.Value, ct.LocatorGsmTimeout), 0)
			order by l.Log_Time asc
	) cell
	outer apply (
		--TODO: ������ � ������������ ����� ������ � ������� ����������� ������������ ��������������� ����� 
		select top(1) l.Log_Time 
			from Wlan_Log l with (nolock) 
			join WLAN_MAC_Address mac on mac.ID = WLAN_MAC_Address_ID
			where l.vehicle_id = c.Vehicle_ID
			  and l.log_time between isnull(last_geo.Log_Time, @current_log_time - isnull(lbsWifi.Value, @default_lbsWiFiInterval)) and @current_log_time
			  and l.log_time > isnull(last_locator.Log_Time + isnull(lbsWifi.Value, @default_lbsWiFiInterval), 0)
			  and mac.IsMobileHotSpot = 0
			order by l.Log_Time asc
	) wlan
	where 1=1
	and (
			cell.Log_Time is not null and (last_geo.Log_Time is null or last_geo.Log_Time < @current_log_time - isnull(lbsGsm.Value, ct.LocatorGsmTimeout))
		or
			wlan.Log_Time is not null and (last_geo.Log_Time is null or last_geo.Log_Time < @current_log_time - isnull(lbsWifi.Value, @default_lbsWiFiInterval))
		)

select	Vehicle_ID = v.id, 
		Cell_Log_Time,
		Wlan_Log_Time
 from @v v

insert into VehicleLocator
	select t1.Vehicle_ID, t1.Log_Time
		from (
			select t.Vehicle_ID, Log_Time = max(t.Log_Time)
			from (
				select Vehicle_ID = v.id, Log_Time = Cell_Log_Time from @v v where v.Cell_Log_Time is not null
				union
				select Vehicle_ID = v.id, Log_Time = Wlan_Log_Time from @v v where v.Wlan_Log_Time is not null
			) t
			group by t.Vehicle_ID
		) t1
		where not exists (
			select 1 
				from VehicleLocator vl with (XLOCK, SERIALIZABLE) 
				where vl.Vehicle_ID = t1.Vehicle_ID 
				  and vl.Log_Time = t1.Log_Time)

select
	Country_Code = (select value from country_code where id = cell.country_code_id),
	Network_Code = (select value from network_code where id = cell.network_code_id),
	cell.LAC,
	cell.Cell_ID,
	SignalStrength
from @v v
join Cell_Network_Log cell (nolock) on cell.vehicle_id = v.id and cell.Log_Time = v.Cell_Log_Time
order by SignalStrength desc

select 
	SSID = (select value from wlan_ssid where id = wlan.wlan_ssid_id),
	Mac_Address = (select value from wlan_mac_address where id = wlan.wlan_mac_address_id),
	Channel_Number,
	SignalStrength
from @v v
join Wlan_Log wlan (nolock) on wlan.vehicle_id = v.id and wlan.Log_Time = v.Wlan_Log_Time
join WLAN_SSID ssid (nolock) on ssid.ID = wlan.WLAN_SSID_ID
order by SignalStrength desc
go