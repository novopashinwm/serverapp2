if exists (
	select *
		from sys.procedures
		where name = 'SendSmsThroughMpx'
)
	drop procedure SendSmsThroughMpx;	
go

--SendSms 'hello1', '79852579565', '79267315495'
create procedure SendSmsThroughMpx
(
	@text nvarchar (255),
	@destination_phone varchar(32)
)
as

declare @asid_contact_id int = (select top(1) asid_id from v_phone where Msisdn = @destination_phone)
if @asid_contact_id is null
begin

	set @asid_contact_id = (select top(1) asid_id from v_phone where FMsisdn = @destination_phone)

end

declare @contact table (destination_id int)

insert into @contact (destination_id) 
	select @asid_contact_id
	
begin tran

insert into MESSAGE (Status, Time, BODY, TEMPLATE_ID)
	select 1, getutcdate(), 
		@text,
		Template_ID = (select mt.Message_Template_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'GenericNotification')

declare @message_id int 
set @message_id = @@identity

insert into Message_Contact 
	select @message_id, 1, c.destination_id
		from @contact c
		where c.destination_id is not null

commit