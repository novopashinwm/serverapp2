set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



ALTER   procedure [dbo].[GetSortedWayout]
	@operator_id int = null
as
begin
	select
		WAYOUT.*
	from 
		WAYOUT
		inner join ROUTE R on WAYOUT.ROUTE = R.ROUTE_ID
		left join v_operator_route_right rr on rr.route_id = R.ROUTE_ID
	where @operator_id is null 
		or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operator_id)
	order by 
		case
		when isnumeric(RTRIM(R.EXT_NUMBER))=1
			then convert( numeric(9), RTRIM(R.EXT_NUMBER))
		when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
			then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
		else 
			999999
	end
	,R.EXT_NUMBER
	,RIGHT(RTRIM(WAYOUT.EXT_NUMBER),2)
		

end













