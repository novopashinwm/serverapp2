if not exists (
	select * from sys.objects where name = 'GetLngCell' and type = 'FN'
)
begin

	exec sp_executesql N'
	create function dbo.GetLngCell(
		@lng float
	) returns int with schemabinding
	as
	begin
		declare @cellSizeInMeters float = 15
		declare @lngStep float = 360/(40000000/@cellSizeInMeters)
		set @lng = case
			when @lng < 0 then @lng+360*(round(-@lng/360,0)+1)
			when @lng >= 360 then @lng-360
			else @lng
		end
		declare @lngCell int = round(@lng/@lngStep,0)
		return @lngCell
	end
	'

end