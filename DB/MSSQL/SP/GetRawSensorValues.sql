﻿IF (OBJECT_ID(N'[dbo].[GetRawSensorValues]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetRawSensorValues]
GO

CREATE PROCEDURE [dbo].[GetRawSensorValues]
(
	@vehicle_id int
)
AS
BEGIN
	SET NOCOUNT ON;

	;WITH INPUT([NUMBER])
	AS
	(
		SELECT DISTINCT
			S.[NUMBER]
		FROM [dbo].[CONTROLLER_SENSOR] S
			JOIN [dbo].[CONTROLLER] C
				ON C.[CONTROLLER_TYPE_ID] = S.[CONTROLLER_TYPE_ID]
		WHERE c.[VEHICLE_ID] = @vehicle_id
	)
	SELECT
		L.[Log_Time],
		L.[Number],
		L.[Value]
	FROM INPUT I
		CROSS APPLY
		(
			SELECT TOP(1)
				V.*
			FROM [dbo].[Controller_Sensor_Log#Lasts] L WITH(NOLOCK)
				INNER JOIN [dbo].[Controller_Sensor_Log] V WITH(NOLOCK)
					ON  V.[Vehicle_ID] = L.[Vehicle_ID]
					AND V.[Number]     = L.[Number]
					AND V.[Log_Time]   = L.[Log_Time#Last]
			WHERE L.[Vehicle_ID] = @vehicle_id
			AND   L.[Number]     = I.[NUMBER]
		) L
END