set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

ALTER  PROCEDURE [dbo].[GET_SYSTEM_RIGHTS]
	(
		@login nvarchar (30)
	)
AS
	set nocount on
	set transaction isolation level REPEATABLE READ

	select o.* from OPERATOR o where  o.[LOGIN]=@login
	select ogo.* from OPERATORGROUP_OPERATOR ogo 
	inner join OPERATOR o on ogo.OPERATOR_ID = o.OPERATOR_ID 
	where o.[LOGIN]=@login
	
	select ro.* from RIGHT_OPERATOR ro 
	inner join OPERATOR o on ro.OPERATOR_ID = o.OPERATOR_ID 
	where  o.[LOGIN]=@login
	
	select og.* from OPERATORGROUP og 
	inner join OPERATORGROUP_OPERATOR ogo on og.OPERATORGROUP_ID = ogo.OPERATORGROUP_ID 
	inner join OPERATOR o on ogo.OPERATOR_ID = o.OPERATOR_ID 
	where  o.[LOGIN]=@login
	
	select rog.* from OPERATOR o  
	inner join OPERATORGROUP_OPERATOR ogo on ogo.OPERATOR_ID=o.OPERATOR_ID   
	inner join OPERATORGROUP og on ogo.OPERATORGROUP_ID = og.OPERATORGROUP_ID 
	inner join RIGHT_OPERATORGROUP rog on rog.OPERATORGROUP_ID = og.OPERATORGROUP_ID 
	where     o.[LOGIN]=@login
	
	select * from [RIGHT] r where r.[SYSTEM]=1 or r.right_id in (select distinct right_id from RIGHT_OPERATOR)












