/****** Object:  StoredProcedure [dbo].[MissedTripsDetailGet]    Script Date: 07/14/2010 18:03:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------

-- =Ёў¤Ёвї¤°ї:
-- ж■√║вї¤°ї ЇЁ¤¤v┐ Ї√а  ■╕ки■ї¤°а ■квїкЁ ■  и■ ║Хї¤¤v┐ ° ўЁёиЁ·■ЄЁ¤¤v┐ иї∙╕Ё┐, ¤Ё ║·ўЁ¤¤║ж ЇЁк║
-- LЄк■и: -Ёи°к■¤■Є

ALTER PROCEDURE [dbo].[MissedTripsDetailGet]
	(
	@for_Date DateTime,      -- -ЁкЁ
	@whd datetime,		-- Єиї№а ¤ЁвЁ√Ё Ї¤а Є utc
	@operator_id int = null
	) 
AS

--------------------------------------------------------------------------------
/*
-- Test
declare @for_Date datetime
set @for_Date = 
	--'2006-09-20 19:00:00'
	--'2006-09-25 19:00:00'
	'2006-11-06 20:59:00'
declare @whd datetime
set @whd = 
	--'2006-09-25 20:00:00'
	'2006-11-05 21:00:00'
*/
/*
select * from waybill_header where EXT_NUMBER = '86554'
select * from WB_TRIP where WAYBILL_HEADER_ID = '157343'
update WB_TRIP set STATUS = 0x02 where WB_TRIP_ID = '2741930' or WB_TRIP_ID = '2741931' or WB_TRIP_ID = '2741933'
*/
--------------------------------------------------------------------------------

--===============================================
-- ж■√║вї¤°ї Є╕ї┐ ўЁ·иvкv┐ иї∙╕■Є #closed_tids
--===============================================

-- °Її¤к°п°·Ёк■иv Є╕ї┐ ¤║Ў¤v┐  ║кїЄv┐ √°╕к■Є
select distinct	waybill_header_id
into #whids
from waybill_header wh
left join v_operator_vehicle_right rr on rr.vehicle_id = wh.VEHICLE_ID
where 
	--WAYBILL_DATE between dateadd(hour, -1, @whd) and dateadd(hour, 1, @whd)
	WAYBILL_DATE = @whd
	and cancelled = 0
	and (@operator_id is null or rr.operator_id = @operator_id)

-- °Її¤к°п°·Ёк■иv ўЁ·иvкv┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а
select distinct 
	trip_id
into 
	#closed_tids
from 
	wb_trip
where 
	waybill_header_id in (select * from #whids)
	and waybill_id is not null
	and trip_id is not null

drop table #whids


--===============================================
-- ж■√║вї¤°ї Є╕ї┐ Ё·к║Ё√╣¤v┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а #filtered_tids
--===============================================

-- ¤Ё░Ё ЇЁкЁ Є ёїў■к¤■╕°кї√╣¤■№ Є°Її
declare @dt datetime
set @dt = dateadd(hh, 24 - datepart(hh, @whd), @whd)
--set @dt = @for_Date--@whd

-- кЁё√°бЁ ╕■■кЄїк╕кЄ°а №Ёи░и║к■Є ° Є°Ї■Є Ї¤а
select distinct
	r.route_id, 
	r.ext_number as route, 
	dbo.DayKind(@dt, r.route_id) as day_kind_id
into #r_dk
from route r
left join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
where (@operator_id is null or rr.operator_id = @operator_id)

-- °Її¤к°п°·Ёк■иv Є╕ї┐ Ё·к║Ё√╣¤v┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а
select distinct
	rd.route,
	s.schedule_id, 
	s.ext_number as schedule,
	sd.schedule_detail_id,
	t.trip_id
into 
	#filtered_tids
from 
	schedule as s
	inner join #r_dk as rd on s.route_id = rd.route_id 
		and s.day_kind_id = rd.day_kind_id
	inner join schedule_detail as sd on s.schedule_id = sd.schedule_id
	inner join trip as t on sd.schedule_detail_id = t.schedule_detail_id

drop table #r_dk


--===============================================
-- ж■√║вї¤°ї Є╕ї┐ ¤їўЁ·иvкv┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а #unclosed_t
--===============================================

-- ¤їўЁ·иvкvї иї∙╕v °ў иЁ╕ °╕Ё¤°а
select 
	TRIP_ID, 
	TRIP_KIND_ID, 
	BEGIN_TIME, 
	END_TIME, 
	SCHEDULE_DETAIL_ID, 
	TRIP, 
	ROUTE_TRIP_ID
	-- ї╕√° ї╕к╣ WB_TRIP_ID (Ї√а  ■√║вї¤°а ■ їиЁк■иЁ Є ЇЁ√╣¤ї∙░ї№)
	,WB_TRIP_ID = (
		select top 1 WBT.WB_TRIP_ID from WB_TRIP as WBT  
		where WBT.TRIP_ID = t.TRIP_ID
		order by WBT.WB_TRIP_ID DESC
		)
into 
	#unclosed_t
from 
	trip t
where 
	trip_id in (select distinct trip_id from #filtered_tids)
		and trip_id not in (select * from #closed_tids)

--select ut.*, ft.*, dateadd(s, ut.BEGIN_TIME, @dt), dateadd(s, ut.END_TIME, @dt) from #unclosed_t as ut left join #filtered_tids as ft on ft.TRIP_ID = ut.TRIP_ID order by ft.schedule
--select * from #unclosed_t

drop table #closed_tids
drop table #filtered_tids


--===============================================
-- жїў║√╣кЁк
--===============================================

-- жи■ ║Хї¤¤vї ° ўЁёиЁ·■ЄЁ¤¤vї
select distinct
	--TR.TRIP_ID,
	SC.EXT_NUMBER    	SC_EXTNUM,	-- ¤■№їи Єv┐■ЇЁ
	RO.EXT_NUMBER    	RO_NUM,         -- ¤■№їи №Ёи░и║кЁ
	WBT.BEGIN_TIME    	WBT_BEGINTIME,  -- Tиї№а ¤ЁвЁ√Ё иїЁ√╣¤■є■ иї∙╕Ё  
	WBT.STATUS              WBT_STATUS,     -- TкЁк║╕ ( и■ ║Хї¤, ёиЁ·) 
	OP.NAME                 OP_NAME,        -- LL+ ■ їиЁк■иЁ 
	PO.NAME                 PO_NAME,        -- ж■¤ки■√╣¤Ёа к■в·Ё
	-- L¤кїиЄЁ√ = иЁў¤°бЁ №їЎЇ║ ЇЁ¤¤v№ ° ╕√їЇ║жХ°№  ■ ЇЁ¤¤■№║ №Ёи░и║к║   
	isnull(	
		(select break_interval = 
			(select
				min(TR1.BEGIN_TIME) 
			from 
				TRIP as TR1 
				-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ Єv┐■Ї■Є
				inner join dbo.SCHEDULE_DETAIL as SCD1 on SCD1.[SCHEDULE_DETAIL_ID] = TR1.[SCHEDULE_DETAIL_ID]	
				inner join dbo.SCHEDULE as SC1 on SC1.[SCHEDULE_ID] = SCD1.[SCHEDULE_ID]
				-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ №Ёи░и║к■Є
				inner join dbo.ROUTE as RO1 on RO1.[ROUTE_ID] = SC1.[ROUTE_ID]
			where 
				TR1.BEGIN_TIME > TR.BEGIN_TIME 
				and TR1.TRIP_KIND_ID in(5,6)
				and RO1.EXT_NUMBER = RO.EXT_NUMBER 
				and SC1.DAY_KIND_ID = SC.DAY_KIND_ID 
			) 
		- TR.BEGIN_TIME) 
		, 0) as INTERVAL 
from 
	dbo.TRIP as TR
	inner join dbo.SCHEDULE_DETAIL as SCD on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]	
	inner join dbo.SCHEDULE as SC on SCD.[SCHEDULE_ID] = SC.[SCHEDULE_ID]
	-- TЄаў╣ ╕ кЁё√°бї∙ №Ёи░и║к■Є
	inner join dbo.ROUTE as RO on SC.[ROUTE_ID] = RO.[ROUTE_ID]
	-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ ·■¤ки■√╣¤v┐ к■вї·
	inner join dbo.ROUTE_TRIP as RTR on RTR.[ROUTE_TRIP_ID] = TR.[ROUTE_TRIP_ID]	
	inner join dbo.GEO_TRIP as GTR on GTR.[GEO_TRIP_ID] = RTR.[GEO_TRIP_ID]	
	inner join dbo.POINT as PO on PO.[POINT_ID] = GTR.[POINTA]	
	-- TЄаў╣ ╕ WB_TRIP
	left join dbo.WB_TRIP as WBT on TR.[TRIP_ID] = WBT.[TRIP_ID]
	-- TЄаў╣ ╕ кЁё√°бї∙ жT
	inner join dbo.WAYBILL_HEADER as WBH on WBT.[WAYBILL_HEADER_ID] = WBH.[WAYBILL_HEADER_ID]
	-- TЄаў╣ ╕ кЁё√°бї∙ ■ їиЁк■и■Є
	left join dbo.OPERATOR as OP on WBT.[OPERATOR_ID] = OP.[OPERATOR_ID]
	left join v_operator_route_right rr on rr.route_id = RO.ROUTE_ID
where 
	WBH.[WAYBILL_DATE] > @for_Date - 1
	and WBH.[CANCELLED] != 1
	and WBT.BEGIN_TIME <= @for_Date 
	and WBT.TRIP_KIND_ID in (5, 6)			
	and (@operator_id is null or rr.operator_id = @operator_id)
	--TкЁк║╕ ( 0x02=ёиЁ·, 0x04=Єv ■√¤ї¤ ¤■ ╕ ё■√╣░°№° ■к·√■¤ї¤°а№°, 0x08= и■ ║Хї¤, 0x10= и■ ║Хї¤ °ў-ўЁ ўЁк■иЁ ) 
	and ((WBT.STATUS & 0x02) = 0x02 or (WBT.STATUS & 0x04) = 0x04 
		or (WBT.STATUS & 0x08) = 0x08 or (WBT.STATUS & 0x10) = 0x10)
--order by 
	--RO.EXT_NUMBER, cast(SC.EXT_NUMBER as int), WBT.BEGIN_TIME 

union

-- =їўЁ·иvкvї
select 
	SC.EXT_NUMBER    			SC_EXTNUM,	-- ¤■№їи Єv┐■ЇЁ
	RO.EXT_NUMBER    			RO_NUM,         -- ¤■№їи №Ёи░и║кЁ
	dateadd(ss, TR.BEGIN_TIME, @whd)    	WBT_BEGINTIME,  -- Tиї№а ¤ЁвЁ√Ё иїЁ√╣¤■є■ иї∙╕Ё  
	0             				WBT_STATUS,     -- TкЁк║╕ ( и■ ║Хї¤, ёиЁ·) 
	--''                 			OP_NAME,        -- LL+ ■ їиЁк■иЁ 
	OP.NAME                			OP_NAME,        -- LL+ ■ їиЁк■иЁ 
	PO.NAME                 		PO_NAME,        -- ж■¤ки■√╣¤Ёа к■в·Ё
	-- L¤кїиЄЁ√ = иЁў¤°бЁ №їЎЇ║ ЇЁ¤¤v№ ° ╕√їЇ║жХ°№  ■ ЇЁ¤¤■№║ №Ёи░и║к║   
	isnull(	
		(select break_interval = 
			(select 
				min(TR1.BEGIN_TIME) 
			from 
				TRIP as TR1 
				-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ Єv┐■Ї■Є
				inner join dbo.SCHEDULE_DETAIL as SCD1 on SCD1.[SCHEDULE_DETAIL_ID] = TR1.[SCHEDULE_DETAIL_ID]	
				inner join dbo.SCHEDULE as SC1 on SC1.[SCHEDULE_ID] = SCD1.[SCHEDULE_ID]
				-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ №Ёи░и║к■Є
				inner join dbo.ROUTE as RO1 on RO1.[ROUTE_ID] = SC1.[ROUTE_ID]
			where 
				TR1.BEGIN_TIME > TR.BEGIN_TIME 
				and TR1.TRIP_KIND_ID in(5,6)
				and RO1.EXT_NUMBER = RO.EXT_NUMBER 
				and SC1.DAY_KIND_ID = SC.DAY_KIND_ID 
			)       	
		- TR.BEGIN_TIME) 
	, 0) as INTERVAL 
from 
	#unclosed_t as TR
	inner join dbo.SCHEDULE_DETAIL as SCD on TR.[SCHEDULE_DETAIL_ID] = SCD.[SCHEDULE_DETAIL_ID]	
	inner join dbo.SCHEDULE as SC on SCD.[SCHEDULE_ID] = SC.[SCHEDULE_ID]
	-- TЄаў╣ ╕ кЁё√°бї∙ №Ёи░и║к■Є
	inner join dbo.ROUTE as RO on SC.[ROUTE_ID] = RO.[ROUTE_ID]
	-- TЄаўvЄЁї№╕а ╕ кЁё√°бї∙ ·■¤ки■√╣¤v┐ к■вї·
	inner join dbo.ROUTE_TRIP as RTR on RTR.[ROUTE_TRIP_ID] = TR.[ROUTE_TRIP_ID]	
	inner join dbo.GEO_TRIP as GTR on GTR.[GEO_TRIP_ID] = RTR.[GEO_TRIP_ID]	
	inner join dbo.POINT as PO on PO.[POINT_ID] = GTR.[POINTA]	
	-- + їиЁк■и
	left join dbo.WB_TRIP as WBT on WBT.WB_TRIP_ID = TR.WB_TRIP_ID
	left join dbo.OPERATOR as OP on OP.OPERATOR_ID = WBT.OPERATOR_ID 
where
	-- ¤ї ║в°кvЄЁк╣ ┐.и., ■ёїЇv, ■к╕к■°, ўЁ иЁЄ·°;
	--TR.TRIP_KIND_ID in (5, 6)	
	TR.TRIP_KIND_ID not in (2, 3, 4, 7)			
--order by 
	--RO.EXT_NUMBER, cast(SC.EXT_NUMBER as int), TR.BEGIN_TIME

order by 
	1, 2, 3 

--select * from #unclosed_t


drop table #unclosed_t



