/**
AS: �������� �������� ������� ����������� �� ������� vehicle_profile

���������� getVehicleProperties ������ ��� getVehicleProfile ��� ����
� ������� ���������� ���� �� ������� - ����������� (�����) ������,
�������� ��������� � �.�.
**/

IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'getVehicleProperties' AND type = 'P')
    DROP PROCEDURE getVehicleProperties
GO


CREATE PROCEDURE [dbo].[getVehicleProperties]  
	@vehicle_id int
AS
begin
	select * from [dbo].[vehicle_profile]
	where vehicle_id = @vehicle_id
end
