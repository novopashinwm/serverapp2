﻿IF (OBJECT_ID(N'[dbo].[CloseSessionsByOperator]') IS NOT NULL)
	DROP PROCEDURE [dbo].[CloseSessionsByOperator];
GO
--EXEC [dbo].[CloseSessionsByOperator] 28557
CREATE PROCEDURE [dbo].[CloseSessionsByOperator]
(
	@OPERATOR_ID int,
	@SESSION_END datetime = NULL
)
AS
BEGIN
	IF (@SESSION_END IS NULL)
		SET @SESSION_END = GETUTCDATE();
	UPDATE [dbo].[SESSION]
		SET [SESSION_END] = @SESSION_END
	WHERE [OPERATOR_ID] = @OPERATOR_ID
	AND   [SESSION_END] IS NULL;
END
GO