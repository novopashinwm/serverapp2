set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if not exists (select * from sys.table_types where name = 'Vehicle_Zone_Position_Param')
	create type Vehicle_Zone_Position_Param as table 
	(
		vehicle_id int, 
		zone_id int,
		log_time int,
		lat float,
		lng float
	)
go
if (exists (select 1 from sys.objects where name = 'GetGeoZonesForVehicles' and type='p'))
	drop procedure dbo.GetGeoZonesForVehicles;
go

create procedure dbo.GetGeoZonesForVehicles
	@vehicle		dbo.Id_Param readonly,
	@zone			dbo.Id_Param readonly,
	@log_time_from  int,
	@log_time_to	int,
	@debug bit = NULL
as
begin
	
	if isnull(@debug, 0) = 0
		set nocount on;

	create table #zone (vehicle_id int, zone_id int, type_name varchar(10), isFull bit, log_time int, Lat float, Lng float)

	if @debug = 1
		print '#zone is filling by zone_matrix'

	insert into #zone
	select t.Vehicle_ID, t.ZONE_ID, pt.Name, t.isFull, t.Log_Time, gl.Lat, gl.Lng
	from 
	(
		select	v.id Vehicle_ID,	
			gl.log_time,
			zm.Zone_ID,
			isnull(max(convert(int, zm.isFull)), 0) isFull
		from @vehicle v
		join dbo.Geo_Log gl (nolock) on gl.Vehicle_ID = v.id 
									and gl.Log_Time between @log_time_from and @log_time_to
		join dbo.Scale scale on 1=1
		join dbo.Zone_Matrix zm on zm.Node = dbo.GetMatrixNode(gl.Lng, gl.Lat, scale.Value)
								and zm.Zone_ID in (select id from @zone)
		group by v.id, gl.Log_Time, zm.Zone_ID
	) t
		join Geo_Log gl (nolock) on gl.Vehicle_ID = t.Vehicle_ID and gl.Log_Time = t.Log_Time
		join GEO_ZONE_PRIMITIVE gzp on t.ZONE_ID = gzp.ZONE_ID
		join ZONE_PRIMITIVE p on p.PRIMITIVE_ID = gzp.PRIMITIVE_ID
		join ZONE_PRIMITIVE_TYPE pt on p.PRIMITIVE_TYPE = pt.Id

	if @debug = 1
		print 'result set'

	create clustered index IX_Zone_Type_Name on #zone(isFull, type_name)

	create table #result(zone_id int, vehicle_id int, log_time int)
	declare @circleIntersections Vehicle_Zone_Position_Param
	declare @polygonIntersections Vehicle_Zone_Position_Param

	insert into #result
	select z.zone_id, z.vehicle_id, z.log_time from #zone z where z.isFull = 1

	insert into @circleIntersections(vehicle_id, zone_id, log_time, lat, lng)
	select z.vehicle_id, z.zone_id, z.log_time, z.Lat, z.Lng from #zone z where z.isFull = 0 and z.type_name = 'Circle'
	if @@rowcount > 0
		insert into #result
		exec GetGeoZonesForCircleType @circleIntersections
	
	insert into @polygonIntersections(vehicle_id, zone_id, log_time, lat, lng)
	select z.vehicle_id, z.zone_id, z.log_time, z.Lat, z.Lng from #zone z where z.isFull = 0 and z.type_name = 'Polygon'
	if @@rowcount > 0
		insert into #result
		exec GetGeoZonesForPolygonType @polygonIntersections

	select * from #result

	drop table #zone;
	drop table #result
end
