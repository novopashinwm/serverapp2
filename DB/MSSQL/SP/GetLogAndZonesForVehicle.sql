set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



-----------------------------------------------------------------------------------
--═рчэрўхэшх: ─ы  яюёЄЁюхэш  юЄўхЄр ю яЁюїюцфхэшш ухючюэ
--└тЄюЁ: ├Ёхсхээшъют ╠.┬.
-----------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetLogAndZonesForVehicle]
	@dtFrom			int, 
	@dtTo			int, 
	@vehicle char(40) 
AS

-----------------------------------------------------------------------------------
/*
-- Test
Declare
	@dtFrom			int, 
	@dtTo			int, 
	@vehicle_id		int

set @vehicle_id = 360
set @dtFrom = 1218000000
set @dtTo = 1218522710

set @vehicle_id = 360
set @dtFrom = 1219219440 --1219219200
set @dtTo = 1219219560--1219221000

--select * from vehicle where PUBLIC_NUMBER like '%599%'
--select * from geo_zone where [NAME] like '%599%'
--select Monitoree_ID, max(log_time), dateadd(s, max(log_time), '1970') from Monitoree_log group by Monitoree_ID order by Monitoree_ID
--exec DBO.GETLOGANDZONESFORVEHICLE @dtFrom=1219219200,@dtTo=1219221000,@vehicle_id=360
*/
-----------------------------------------------------------------------------------
declare @vehicle_id int
Set @vehicle_id = (select Top 1 Vehicle_ID from vehicle where Garage_Number = @vehicle)

declare @maxPosCount int
set @maxPosCount = 10000


-- ┬Ёхьхээр  ЄрсышЎр яю Єюўърь
create table #MonitoreeLog(	
	id int identity primary key, 
	Log_Time		int,
	X				float,
	Y				float,
	Speed			int
	)

-- ╬ЄсшЁрхь тёх яючшЎшш ╥╤ чр шэЄхЁтры
insert into #MonitoreeLog
select /*top (@maxPosCount)*/ 	
	Log_Time, 
	X,
	Y,
	Speed
from Monitoree_log (nolock)
Where 	
	Monitoree_ID = @vehicle_id
	and Log_Time between @dtFrom and @dtTo 
	and X < 180 and Y < 180
order by 
	Log_Time

if((select count(*) from #MonitoreeLog) < 1 
	or (select count(*) from ZONE_VEHICLE where vehicle_id = @vehicle_id) < 1)
begin
	delete from #MonitoreeLog
	set @vehicle_id = -1
end


select * 
	,dateadd(s, log_time, '1970') as Log_DateTime
from #MonitoreeLog
order by log_time


-- ╟юэ√ фы  ╥╤
select --* 
	gz.*,
	gzp.PRIMITIVE_ID,
	gzp.[ORDER] as PrimitiveOrder,
	zpv.VERTEX_ID,
	zpv.[ORDER] as VertexOrder,
	mv.X,
	mv.Y
from GEO_ZONE as gz
	inner join GEO_ZONE_PRIMITIVE as gzp on gzp.ZONE_ID = gz.ZONE_ID
	--inner join ZONE_PRIMITIVE as zp on zp.PRIMITIVE_ID = gzp.PRIMITIVE_ID
	inner join ZONE_PRIMITIVE_VERTEX as zpv on zpv.PRIMITIVE_ID = gzp.PRIMITIVE_ID
	inner join MAP_VERTEX as mv on mv.VERTEX_ID = zpv.VERTEX_ID
	inner join ZONE_VEHICLE as zv on gz.ZONE_ID = zv.ZONE_ID
where 
	zv.vehicle_id = @vehicle_id
order by 
	gz.ZONE_ID,
	gzp.[ORDER],
	zpv.[ORDER]

select @vehicle_id as vehicle_id

drop table #MonitoreeLog



