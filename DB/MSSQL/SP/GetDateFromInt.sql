set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF EXISTS ( SELECT * FROM sysobjects WHERE name = N'GetDateFromInt' AND type = 'FN')
    DROP FUNCTION GetDateFromInt
GO

Create    FUNCTION 
	[dbo].[GetDateFromInt](
		@IntDate int
		)  RETURNS datetime

AS  
BEGIN 
	RETURN DateAdd(s, @IntDate, cast('1970-01-01' as datetime));
END

GO