IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetOperatorVehicles' 
)
   DROP PROCEDURE [dbo].[GetOperatorVehicles]
GO

CREATE  PROCEDURE [dbo].[GetOperatorVehicles]
	@operatorId int 
AS
begin
	select op.Operator_ID, 
		   op.Name, 
		   v.Vehicle_ID, 
		   v.Garage_Number, 
		   1 as 'Allow', 
		   v.fuel_tank, 
		   c.phone
	from dbo.v_operator_vehicle_right ovr
	join dbo.Operator op on op.Operator_ID=ovr.Operator_ID
	join dbo.Vehicle v on v.Vehicle_ID=ovr.Vehicle_ID
	join dbo.controller c on c.vehicle_id = v.vehicle_id
	where ovr.OPERATOR_ID = @operatorID
	and ovr.right_id = 102
end





