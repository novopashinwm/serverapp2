﻿IF (OBJECT_ID(N'[dbo].[AddAddress]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[AddAddress];
GO

CREATE PROCEDURE [dbo].[AddAddress]
(
	@Lat      numeric(8,5),
	@Lng      numeric(8,5),
	@Language varchar(2),
	@Value    nvarchar(500),
	@MapId    uniqueidentifier,
	@Radius   int
)
AS
BEGIN
	SET NOCOUNT           ON;
	SET QUOTED_IDENTIFIER ON;

	IF (NOT EXISTS
	(
		SELECT
			*
		FROM [dbo].[Address]
		WHERE [LatCell]  = [dbo].[GetLatCellTolerance](@Lat, 500)
		AND   [LngCell]  = [dbo].[GetLngCellTolerance](@Lng, 500)
		AND   [MapId]    = @MapId
		AND   [Language] = @Language
		AND   [dbo].[GetTwoGeoPointsDistance]([Lng], [Lat], @Lng, @Lat) <= @radius
	))
	BEGIN
		INSERT INTO [dbo].[Address]
			([Lat], [Lng], [Language], [Value], [Time],     [MapId])
		VALUES
			(@Lat,  @Lng,  @Language,  @Value, GETUTCDATE(), @MapId)
	END
END