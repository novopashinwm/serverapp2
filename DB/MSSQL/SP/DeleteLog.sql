if exists (
	select * 
		from sys.objects where name = 'DeleteLog'
) 
	drop procedure DeleteLog
go

create procedure DeleteLog
(
	@vehicle_id int
)
as
begin
	set nocount on
	
	delete from dbo.geo_log
	where vehicle_id = @vehicle_id

	delete from dbo.statistic_log
	where vehicle_id = @vehicle_id

	delete from dbo.gps_log
	where vehicle_id = @vehicle_id

	delete from dbo.Position_Radius
	where vehicle_id = @vehicle_id

	delete from dbo.Controller_Sensor_Log
	where vehicle_id = @vehicle_id

	delete from dbo.can_info
	where vehicle_id = @vehicle_id

	delete from dbo.log_time
	where vehicle_id = @vehicle_id

	delete from dbo.monitoree_log_ignored
	where monitoree_id = @vehicle_id

end