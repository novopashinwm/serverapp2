set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoadIPValues]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LoadIPValues]
GO

CREATE PROCEDURE [dbo].[LoadIPValues]
	@xml XML
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.IPValues
	
	INSERT INTO dbo.IPValues (CountryCode, CityId, RangeFrom, RangeTo, RangeFromInt, RangeToInt)
	SELECT  
		pv.ID.value('@Code', 'CHAR(2)'),
		pv.ID.value('@CityId', 'INT'),
		CAST(pv.ID.value('@IntFrom', 'BIGINT') as binary(4)),
		CAST(pv.ID.value('@IntTo', 'BIGINT') as binary(4)),
		pv.ID.value('@IntFrom', 'BIGINT'),
		pv.ID.value('@IntTo', 'BIGINT')
	FROM    @xml.nodes('/Values/Value') AS pv ( ID )
	
END






  