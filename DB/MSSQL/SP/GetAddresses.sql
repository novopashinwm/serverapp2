﻿IF (OBJECT_ID(N'[dbo].[GetAddresses]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetAddresses];
GO

CREATE PROCEDURE [dbo].[GetAddresses]
(
	@point    [dbo].[LatLngPoint] readonly,
	@language varchar(2),
	@mapid    uniqueidentifier,
	@radius   int
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @earthRadius float = 6371000
	SELECT
		[id],
		[Lat],
		[Lng],
		[Lat0] = [dbo].[GetLatCellTolerance]([Lat], 500) - 1,
		[Lng0] = [dbo].[GetLngCellTolerance]([Lng], 500) - 1,
		[Lat1] = [dbo].[GetLatCellTolerance]([Lat], 500) + 0,
		[Lng1] = [dbo].[GetLngCellTolerance]([Lng], 500) + 0,
		[Lat2] = [dbo].[GetLatCellTolerance]([Lat], 500) + 1,
		[Lng2] = [dbo].[GetLngCellTolerance]([Lng], 500) + 1
	INTO #search
	FROM @point

	;WITH
	addresses
	AS
	(
		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat1]  = a.[LatCell] AND p.[Lng1]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat1]  = a.[LatCell] AND p.[Lng2]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat1]  = a.[LatCell] AND p.[Lng0]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat2]  = a.[LatCell] AND p.[Lng1]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat2]  = a.[LatCell] AND p.[Lng2]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat2]  = a.[LatCell] AND p.[Lng0]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat0]  = a.[LatCell] AND p.[Lng1]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat0]  = a.[LatCell] AND p.[Lng2]     = a.[LngCell])

		UNION ALL

		SELECT
			p.[Id], a.[Value], [latFrom] = p.[Lat], [lngFrom] = p.[Lng], [latTo] = a.[Lat], [lngTo] = a.[Lng]
		FROM #search p, [dbo].[Address] a
		WHERE (a.[MapId] = @mapid      AND a.[Language] = @language)
		AND   (p.[Lat0]  = a.[LatCell] AND p.[Lng0]     = a.[LngCell])
	),
	distances
	AS
	(
		SELECT
			[Id],
			[lat] = [latTo],
			[lng] = [lngTo],
			[Value],
			[Distance] = [dbo].[GetTwoGeoPointsDistance]([lngFrom], [latFrom], [lngTo], [latTo])
		FROM addresses
	),
	sort
	AS
	(
		SELECT
			[Id],
			[lat],
			[lng],
			[Value],
			[Distance],
			[Num] = ROW_NUMBER() OVER(PARTITION BY [Id] ORDER BY [Distance])
		FROM distances
		WHERE [Distance] <= @radius
	)
	SELECT
		[Id],
		[lat],
		[lng],
		[Value],
		[Distance]
	FROM sort
	WHERE [Num] = 1
	ORDER BY Id

	DROP TABLE #search
END
GO