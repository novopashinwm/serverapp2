IF (OBJECT_ID(N'[dbo].[AddOrUpdateControllerSensor]') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[AddOrUpdateControllerSensor]
END
GO

CREATE PROCEDURE [dbo].[AddOrUpdateControllerSensor]
(
	@controllerTypeName nvarchar(50),
	@number             int,
	@name               nvarchar(512),
	@digital            bit,
	@legend             nvarchar(50) = null,
	@mandatory          bit = 0,
	@defaultMultiplier  real = 1,
	@defaultConstant    real = 0
)
AS
BEGIN
	DECLARE @controller_type_id        int = (select ct.controller_type_id from Controller_Type ct where ct.TYPE_NAME = @controllerTypeName)
	DECLARE @controller_sensor_type_id int = case @digital when 1 then 2 else 1 end

	print 'Adding ' + @controllerTypeName + '.' + @name

	insert into controller_sensor (CONTROLLER_TYPE_ID, Number, CONTROLLER_SENSOR_TYPE_ID)
		select
			 @controller_type_id
			,@number
			,@controller_sensor_type_id
			where not exists (select 1 from controller_sensor e where e.CONTROLLER_TYPE_ID = @controller_type_id and e.NUMBER = @number)

	update e
		set CONTROLLER_SENSOR_TYPE_ID = @controller_sensor_type_id 
		  , Descript = @name
		from controller_sensor e
		where e.CONTROLLER_TYPE_ID = @controller_type_id 
		  and e.NUMBER = @number
		  and (CONTROLLER_SENSOR_TYPE_ID <> @controller_sensor_type_id 
		   or  Descript <> @name
		   or  Descript is null)

	if @legend is not null
		update e
			set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
			  , Default_Constant = @defaultConstant
			  , Default_Multiplier = @defaultMultiplier
			  , Mandatory = isnull(@mandatory, 0)
			from controller_sensor e
			join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = @legend
			where e.CONTROLLER_TYPE_ID = @controller_type_id 
			  and e.NUMBER = @number
			  and (e.Default_Sensor_Legend_ID is null or 
			       e.Default_Sensor_Legend_ID <> legend.CONTROLLER_SENSOR_LEGEND_ID or
				   e.Default_Multiplier <> @defaultMultiplier or
				   e.Default_Constant <> @defaultConstant)
	
	if (@mandatory = 1)
	begin

		insert into CONTROLLER_SENSOR_MAP 
					(CONTROLLER_ID,    CONTROLLER_SENSOR_ID, CONTROLLER_SENSOR_LEGEND_ID,            MULTIPLIER,            CONSTANT)
			select c.Controller_ID, cs.Controller_Sensor_ID, cs.Default_Sensor_Legend_ID, cs.Default_Multiplier, cs.Default_Constant
				from Controller c
				join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID and cs.NUMBER = @number
				where c.CONTROLLER_TYPE_ID = @controller_type_id
				  and not exists (select 1 from CONTROLLER_SENSOR_MAP e where e.CONTROLLER_ID = c.CONTROLLER_ID and e.CONTROLLER_SENSOR_LEGEND_ID = cs.Default_Sensor_Legend_ID)

	end
END
GO