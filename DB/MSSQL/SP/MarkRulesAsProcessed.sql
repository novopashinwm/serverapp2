set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'MarkRulesAsProcessed' and type='p'))
	drop procedure dbo.MarkRulesAsProcessed;
go

create procedure dbo.MarkRulesAsProcessed
	@debug bit = NULL
as
begin

	declare @currentUtcDate datetime
	set @currentUtcDate = getutcdate()

	insert into dbo.Vehicle_Rule_Processing (Vehicle_ID, CurrentTime, LastTime)
		select v.Vehicle_ID, @currentUtcDate, @currentUtcDate
			from dbo.Vehicle v
			where not exists (select 1 from Vehicle_Rule_Processing e where e.Vehicle_ID = v.Vehicle_ID)

	update dbo.Vehicle_Rule_Processing
		set LastTime = CurrentTime
		where LastTime <> CurrentTime

end;