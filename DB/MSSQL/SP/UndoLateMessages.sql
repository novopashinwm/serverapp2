if exists (select * from sys.procedures where name='UndoLateMessages')
	drop procedure UndoLateMessages
go

create procedure UndoLateMessages
(
	@destinationId int,
	@seconds int
)
as

declare @datetime datetime = dateadd(ss, -@seconds, getutcdate())
update MESSAGE set STATUS = 3, ProcessingResult = case when ProcessingResult < 3 then 12 else ProcessingResult end
output INSERTED.MESSAGE_ID Id
where DestinationType_ID = @destinationId and [TIME] < @datetime and [STATUS] <> 3

go