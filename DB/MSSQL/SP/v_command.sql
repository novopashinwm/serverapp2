if exists (
	select *
		from sys.views 
		where name = 'v_command'
)
	drop view v_command
go

create view v_command as
	select 
	    c.ID
	 , c.Created
	 ,  [Type] = isnull(t.code, CONVERT(varchar, c.Type_ID))
	 ,  c.Target_ID
	 ,  c.Sender_ID
	 ,  Status = case c.Status
			when 0 then 'Received'
			when 1 then 'Processing'
			when 2 then 'Completed'
			when 3 then 'PartialComplete'
			else 'Unknown (' + CONVERT(varchar(10), c.Status) + ')'
		end
	 ,	CommandReceivedDateMsk = DATEADD(HOUR, 3, c.Date_Received)
	 , 	ResultReceivedDateMsk = DATEADD(HOUR, 3, c.Result_Date_Received)
	 , 	c.Result_Type_ID
	 ,  CmdResult = case c.Result_Type_ID
						when 0 then 'Unknown'
						when 1 then 'Processing'
						when 2 then 'Completed'                                              
						when 3 then 'Failed'                                               
						when 4 then 'Cancelled'
						when 5 then 'VehicleAccessDisallowed'
						when 6 then 'MLPTemporaryDisallowed'
						when 7 then 'AbsentSubscriber'
						when 8 then 'TimeoutExceeded'
						when 9 then 'NoTargetLBSService'
						when 10 then 'TargetLBSServiceIsBlocked'
						when 11 then 'NoCallerLBSService'
						when 12 then 'CallerLBSServiceIsBlocked'
						when 13 then 'MLPRequestCountPerTimespanExceeded'
						when 14 then 'CapacityLimitReached'
						when 15 then 'DeviceNotFoundInReceversList'
						when 16 then 'TerminalManagerIsNull'
						when 17 then 'SendCommandDeviceException'
						when 18 then 'ExecutionTimePassed'
						when 19 then 'InsufficientFunds'
						when 20 then 'NoService'
						when 21 then 'ServiceIsBlocked'
						when 22 then 'NoTarifficationService'
						when 23 then 'PhoneNumberIsUnknown'
						when 24 then 'NetworkIsOverloaded'
						when 25 then 'UnknownCell'
						when 26 then 'LbsDisallowedByLocalRegulations'
						when 27 then 'Inaccessible'
						when 28 then 'NotSupported'
						when 29 then 'OtherCommandIsPending'
						when 30 then 'Throttled'
			else CONVERT(varchar, c.Result_Type_ID)
		end
		, ErrorCount
		, c.Log_Time
		, Parameters = (select cp.[Key] + ': ' + isnull(cp.Value, 'NULL') + ' '
						from Command_Parameter cp
						where cp.Command_ID = c.ID
						for xml path (''))
	from Command c
	left outer join CommandTypes t on t.id = c.Type_ID
go


