set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetGeoZonesForVehicleLogTime' and type='p'))
	drop procedure dbo.GetGeoZonesForVehicleLogTime;
go

create procedure dbo.GetGeoZonesForVehicleLogTime
	@vehicle_zone		dbo.Vehicle_Zone_Param readonly,
	@vehicle_log_time	dbo.Vehicle_Log_Time_Param readonly,
	@debug bit = NULL
as
begin
	
	if isnull(@debug, 0) = 0
		set nocount on;

	declare @vehicle_position dbo.Vehicle_Position_Param

	insert into @vehicle_position (Vehicle_ID, Log_Time, Lat, Lng)
		select Vehicle_ID, Log_Time, geo.Lat, geo.Lng
			from @vehicle_log_time lt
			cross apply (
				select top(1) geo.Lat, geo.Lng
					from Geo_Log geo with (nolock)
					where geo.Vehicle_ID = lt.Vehicle_ID
					  and geo.Log_Time between lt.Log_Time - 10*60 and lt.Log_Time
					order by geo.Log_Time desc
			) geo

	exec dbo.GetGeoZonesForPositions @vehicle_zone, @vehicle_position, @debug	

end