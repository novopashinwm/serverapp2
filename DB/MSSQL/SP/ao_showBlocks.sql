USE [MED_TSS]
GO

/****** Object:  StoredProcedure [dbo].[ao_showBlocks]    Script Date: 25.10.2019 16:27:25 ******/
DROP PROCEDURE [dbo].[ao_showBlocks]
GO

/****** Object:  StoredProcedure [dbo].[ao_showBlocks]    Script Date: 25.10.2019 16:27:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[ao_showBlocks]  --- 1995/11/28 15:48
       @loginame varchar(85) = 'ACTIVE', --or 'active',
	   @dbname varchar(85) = 'MED_TSS' --or 'active'
as

declare	 @spidlow	int,
		 @spidhigh	int,
		 @spid		int,
		 @sid		varbinary(85)

select	 @spidlow	=     0
		,@spidhigh	= 32767




	SELECT  'BLOCKING PROCESS' process_role
			, c.session_id
			, s.status
			, s.last_request_start_time
            , s.last_request_end_time
			, t.text
			, QUOTENAME(OBJECT_SCHEMA_NAME(t.objectid, t.dbid)) + '.'
            + QUOTENAME(OBJECT_NAME(t.objectid, t.dbid)) proc_name
			, c.connect_time
	INTO #T1
    FROM    sys.dm_exec_connections c
    JOIN    sys.dm_exec_sessions s
            ON c.session_id = s.session_id
    CROSS APPLY sys.dm_exec_sql_text(c.most_recent_sql_handle) t
    WHERE   c.session_id in 
		(select distinct blocked
		from  master.dbo.sysprocesses
		where upper(cmd) <> 'AWAITING COMMAND'
		  and blocked <> '0')

    IF NOT EXISTS (SELECT * FROM #t1)
	begin
		select 'NO BLOCKS FOUND' RESULT
		return (0)
	end

	SELECT  c.session_id
			, 'BLOCKING PROCESS' process_role
			, s.status
			, s.last_request_start_time
            , s.last_request_end_time
			, t.text
			, QUOTENAME(OBJECT_SCHEMA_NAME(t.objectid, t.dbid)) + '.'
            + QUOTENAME(OBJECT_NAME(t.objectid, t.dbid)) proc_name
			, c.connect_time
    FROM    sys.dm_exec_connections c
    JOIN    sys.dm_exec_sessions s
            ON c.session_id = s.session_id
    CROSS APPLY sys.dm_exec_sql_text(c.most_recent_sql_handle) t
    WHERE   c.session_id in 
		(select distinct blocked
		from  master.dbo.sysprocesses
		where upper(cmd) <> 'AWAITING COMMAND'
		  and blocked <> '0')

	select  spid session_id
			  , ecid sub_process_id
			  , 'BLOCKED BY'
			  , blocked 'BLOCKING SP ID'
			  , status
			  , loginame=rtrim(loginame)
			  , hostname 
			  , dbname = @dbname
			  , cmd
			  , request_id
		from  master.dbo.sysprocesses
		where spid >= @spidlow and spid <= @spidhigh AND
			  upper(cmd) <> 'AWAITING COMMAND'
			  and blocked <> '0'






return (0) -- ao_who

/*
SELECT  c.session_id, t.text,
            QUOTENAME(OBJECT_SCHEMA_NAME(t.objectid, t.dbid)) + '.'
            + QUOTENAME(OBJECT_NAME(t.objectid, t.dbid)) proc_name,
            c.connect_time,
            s.last_request_start_time,
            s.last_request_end_time,
            s.status
    FROM    sys.dm_exec_connections c
    JOIN    sys.dm_exec_sessions s
            ON c.session_id = s.session_id
    CROSS APPLY sys.dm_exec_sql_text(c.most_recent_sql_handle) t
    WHERE   c.session_id = 134


	select spid,
	   ecid,
	   status,
       loginame=rtrim(loginame),
	   hostname,
	   blk=convert(char(10),blocked),
	   dbname = 'MED_TSS'
	   ,cmd
	   ,request_id
from  master.dbo.sysprocesses
where blocked <> '0'
*/
GO


