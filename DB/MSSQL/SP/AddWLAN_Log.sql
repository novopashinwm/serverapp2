if exists (select * from sys.objects where object_id = object_id('dbo.AddWLAN_Log') and type = 'p')
	drop procedure dbo.AddWLAN_Log
go

create procedure dbo.AddWLAN_Log
(
	@Vehicle_ID 		int,
	@Log_Time   		int,
	@Number	   			tinyint,
	@WLAN_MAC_Address	varchar(12),
	@SignalStrength		tinyint,
	@WLAN_SSID			varchar(32),
	@Channel_Number		tinyint = NULL
)
as

set nocount on;

declare @rows_affected int = 0;

--����������� @WLAN_MAC_Address_ID
declare @WLAN_MAC_Address_ID int

set @WLAN_MAC_Address_ID = (select ID from dbo.WLAN_MAC_Address (nolock) where Value = @WLAN_MAC_Address)

if (@WLAN_MAC_Address_ID is null)
begin
	insert into dbo.WLAN_MAC_Address (Value, IsMobileHotSpot)
		select	@WLAN_MAC_Address,
			case when 
				  @WLAN_SSID like 'mts-%' and len(@WLAN_SSID) = 7 --��� ����� ������ � ������
			   or @WLAN_SSID like '%Metro%'
			   or @WLAN_SSID like '%megafon%'
			   or @WLAN_SSID like '%beeline%'
			   or @WLAN_SSID like '%mts%'
			   or @WLAN_SSID like '%yota%'
			   or @WLAN_SSID like '%portable%'
			   or @WLAN_SSID like '%android%'
			   or @WLAN_SSID like '%iphone%'
			   or @WLAN_SSID like '%modem%'
			   or @WLAN_MAC_Address = '0015628D5AC0'
		   	   or ltrim(rtrim(@WLAN_SSID)) = ''
				then 1 
				else 0 
				end			
			where not exists (select 1 from dbo.WLAN_MAC_Address with (XLOCK, SERIALIZABLE) where Value = @WLAN_MAC_Address)

	set @rows_affected = @rows_affected + @@ROWCOUNT

	set @WLAN_MAC_Address_ID = (select ID from dbo.WLAN_MAC_Address (nolock) where Value = @WLAN_MAC_Address)
end;

--����������� @WLAN_SSID_ID
declare @WLAN_SSID_ID int

set @WLAN_SSID_ID = (select ID from dbo.WLAN_SSID (nolock) where Value = @WLAN_SSID)

if (@WLAN_SSID_ID is null)
begin
	insert into dbo.WLAN_SSID (Value)
		select	@WLAN_SSID
			where not exists (select 1 from dbo.WLAN_SSID with (XLOCK, SERIALIZABLE) where Value = @WLAN_SSID)

	set @rows_affected = @rows_affected + @@ROWCOUNT

	set @WLAN_SSID_ID = (select ID from dbo.WLAN_SSID (nolock) where Value = @WLAN_SSID)
end;

--������� ������
insert into dbo.WLAN_Log (Vehicle_ID, Log_Time, Number, WLAN_MAC_Address_ID, SignalStrength, WLAN_SSID_ID, Channel_Number)
	select @Vehicle_ID, @Log_Time, @Number, @WLAN_MAC_Address_ID, @SignalStrength, @WLAN_SSID_ID, @Channel_Number
		where not exists (select 1 from dbo.WLAN_Log with (XLOCK, SERIALIZABLE)
							where Vehicle_ID = @Vehicle_ID and Log_Time = @Log_TIme and Number = @Number)

set @rows_affected = @rows_affected + @@ROWCOUNT

--���������� ��� ������������ ������ �� ���� �� �����
if @@ROWCOUNT = 0
begin
	update dbo.WLAN_Log
		set WLAN_MAC_Address_ID = @WLAN_MAC_Address_ID
		   ,SignalStrength		= @SignalStrength
		where 
			Vehicle_ID = @Vehicle_ID
		and Log_Time   = @Log_Time
		and Number     = @Number
		and ( WLAN_MAC_Address_ID	<> @WLAN_MAC_Address_ID
		   or SignalStrength		<> @SignalStrength
		   or WLAN_SSID_ID          <> @WLAN_SSID_ID
		   or Channel_Number		<> @Channel_Number
		   or Channel_Number is null and @Channel_Number is not null
		   or Channel_Number is not null and @Channel_Number is null)

	set @rows_affected = @rows_affected + @@ROWCOUNT

end

select Rows_Affected = @rows_affected