﻿IF (OBJECT_ID(N'[dbo].[GetFullLog]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetFullLog]
GO

CREATE PROCEDURE [dbo].[GetFullLog]
(
	@vehicle_id          int,
	@from                int,
	@to                  int,
	@noDataPeriod        int = null,
	@maxTotalPointsCount int = null,
	@debug               bit = null
)
AS
BEGIN
	IF (0 = ISNULL(@debug, 0))
		SET NOCOUNT ON;

	CREATE TABLE #used_sensor_number ([number] int PRIMARY KEY CLUSTERED)
	INSERT INTO #used_sensor_number
		SELECT DISTINCT [Sensor_Number] FROM [dbo].[v_controller_sensor_map] WHERE [Vehicle_ID] = @vehicle_id

	IF (@maxTotalPointsCount IS NOT NULL)
	BEGIN
		IF (@debug = 1)
			PRINT 'Check whether @to requires correction'

		IF (@maxTotalPointsCount <
			(
				SELECT COUNT(1)
				FROM
				(
					SELECT TOP(@maxTotalPointsCount+1) [Log_Time]
					FROM [dbo].[Log_Time] lt WITH(NOLOCK)
					WHERE lt.[Vehicle_ID] = @vehicle_id
					AND   lt.[Log_Time]   BETWEEN @from AND @to
					ORDER BY lt.Log_Time ASC
				) t
			)
		)
		BEGIN
			--Корректируем @to таким образом, чтобы гарантировать, что на выходе будет не более @maxTotalPointsCount точек
			CREATE TABLE #t ([Log_Time] int PRIMARY KEY CLUSTERED)

			IF (@debug = 1)
				PRINT 'Insert into #t'

			INSERT INTO #t
			SELECT TOP(@maxTotalPointsCount)
				*
			FROM
			(
				      SELECT [Log_Time] FROM [dbo].[Geo_Log]               WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
				UNION SELECT [Log_Time] FROM [dbo].[GPS_Log]               WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
				UNION SELECT [Log_Time] FROM [dbo].[CAN_Info]              WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
				UNION SELECT [Log_Time] FROM [dbo].[Position_Radius]       WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
				UNION SELECT [Log_Time] FROM [dbo].[Picture_Log]           WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
				UNION SELECT [Log_Time] FROM [dbo].[Controller_Sensor_Log] WITH (NOLOCK) WHERE [Vehicle_ID] = @vehicle_ID AND [Log_Time] BETWEEN @from AND @to
					AND [Number] IN (SELECT [Number] FROM #used_sensor_number)
			) t
			ORDER BY t.[LOG_TIME]

			DECLARE @max_log_time int = (SELECT MAX([Log_Time]) FROM #t)

			IF (@max_log_time IS NOT NULL)
				SET @to = @max_log_time

			DROP TABLE #t
		END
	END

	IF (@noDataPeriod IS NULL)
		SET @noDataPeriod = 30 * 60

	CREATE TABLE #geo_log               ([log_time] int NOT NULL PRIMARY KEY CLUSTERED, [lng] numeric(8,5), [lat] numeric(8,5))
	CREATE TABLE #gps_log               ([log_time] int NOT NULL PRIMARY KEY CLUSTERED, [speed] tinyint, [course] tinyint, [satellites] tinyint, [altitude] int)
	CREATE TABLE #position_radius       ([log_time] int, [radius] int, [Type] tinyint)
	CREATE TABLE #controller_sensor_log ([log_time] int, [Number] int, [Value] bigint, PRIMARY KEY ([Number], [log_time]))
	CREATE TABLE #can_info              ([log_time] int, [Fuel_Level1] smallint, [Total_Run] int)
	CREATE TABLE #picture_log           ([log_time] int, [url] varchar(255))
	CREATE TABLE #run_log               ([log_time] int, [odometer] bigint)

	-- #geo_log данные интервала
	INSERT INTO #geo_log
	SELECT
		[LOG_TIME], [Lng], [Lat]
	FROM [dbo].[Geo_Log] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [LOG_TIME]   BETWEEN @from AND @to

	-- #geo_log ближайшие данные до искомого интервала
	INSERT INTO #geo_log
	SELECT TOP(1)
		[LOG_TIME], [Lng], [Lat]
	FROM [dbo].[Geo_Log] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [LOG_TIME]   BETWEEN @from - @noDataPeriod AND @from - 1
	ORDER BY [LOG_TIME] DESC
	IF (@@ROWCOUNT = 1)
		INSERT INTO #position_radius
		SELECT
			 [Log_Time]
			,[Radius]
			,[Type]
		FROM [dbo].[Position_Radius] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [Log_Time]   = (SELECT MIN([log_time]) FROM #geo_log)

	-- #geo_log ближайшие данные после искомого интервала
	INSERT INTO #geo_log
	SELECT TOP(1)
		[LOG_TIME], [Lng], [Lat]
	FROM [dbo].[Geo_Log] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [LOG_TIME]   BETWEEN @to + 1 AND @from + @noDataPeriod
	ORDER BY [LOG_TIME] ASC
	IF (@@ROWCOUNT = 1)
		INSERT INTO #position_radius
		SELECT
			 [Log_Time]
			,[Radius]
			,[Type]
		FROM [dbo].[Position_Radius] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [Log_Time]   = (SELECT MAX([log_time]) FROM #geo_log)

	-- #geo_log ближайшие данные до искомого интервала, если предыдущие действия не заполнили ничего
	IF (NOT EXISTS (SELECT 1 FROM #geo_log))
		INSERT INTO #geo_log
		SELECT TOP(1)
			[LOG_TIME], [Lng], [Lat]
		FROM [dbo].[Geo_Log] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [LOG_TIME]   < @from
		ORDER BY [LOG_TIME] DESC

	-- #gps_log данные интервала
	INSERT INTO #gps_log
	SELECT
		[Log_Time], [Speed], [Course], [Satellites], [Altitude]
	FROM [dbo].[GPS_Log] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [Log_Time]   BETWEEN @from AND @to

	-- #gps_log ближайшие данные до искомого интервала
	INSERT INTO #gps_log
	SELECT TOP(1)
		[Log_Time], [Speed], [Course], [Satellites], [Altitude]
	FROM [dbo].[GPS_Log] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [Log_Time]   BETWEEN @from - @noDataPeriod AND @from-1
	ORDER BY [Log_Time] DESC

	-- #gps_log ближайшие данные после искомого интервала
	INSERT INTO #gps_log
	SELECT TOP(1)
		log_time, Speed, Course, Satellites, Altitude
	FROM [dbo].[gps_log] WITH(nolock)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [Log_Time]   BETWEEN @to + 1 AND @from + @noDataPeriod
	ORDER BY [Log_Time] ASC

	-- #gps_log удалаяем данные из #gps_log, для которых по времени нет данных в #geo_log
	DELETE #gps_log
	WHERE [log_time] NOT IN (SELECT [log_time] FROM #geo_log)

	-- #position_radius данные интервала
	INSERT INTO #position_radius
	SELECT
		[Log_Time], [Radius], [Type]
	FROM [dbo].[Position_Radius] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [Log_Time]   BETWEEN @from AND @to

	-- #controller_sensor_log данные интервала
	INSERT INTO #controller_sensor_log
	SELECT
		 csl.log_time
		,used.Number
		,Value = csl.Value-- + ISNULL(sbl.Value, 0)
		from #used_sensor_number used
		cross apply (
			select	csl.Log_Time,
					csl.Value
			from controller_sensor_log csl (nolock)
			where csl.Vehicle_ID = @vehicle_id
			  and csl.Number = used.Number
			  and csl.Log_Time between @from and @to
		union all
			select	top(1)
					csl.Log_Time,
					csl.Value				
			from controller_sensor_log csl (nolock)
			where csl.Vehicle_ID = @vehicle_id
			  and csl.Number = used.Number
			  and csl.Log_Time < @from
			order by csl.Log_Time desc
		union all
			select	top(1)
					csl.Log_Time,
					csl.Value
			from controller_sensor_log csl (nolock)
			where csl.Vehicle_ID = @vehicle_id
			  and csl.Number = used.Number
			  and csl.Log_Time > @to
			order by csl.Log_Time asc
		) csl
/*
		outer apply (
			select top(1) 
				Value = sbl.Multiplier * isnull(
					sbl.Value,
					(select top(1) base_csl.Value 
						from Controller_Sensor_Log base_csl with (nolock)
						where base_csl.Vehicle_ID = sbl.Vehicle_ID
						  and base_csl.Number = sbl.Number
						  and base_csl.Log_Time <= sbl.Log_Time
						order by base_csl.Log_Time desc))
				from Sensor_Base_Log sbl with (nolock)
				where sbl.Vehicle_ID = @vehicle_id
				  and sbl.Number = used.Number
				  and sbl.Log_Time <= csl.Log_Time
				order by sbl.Log_Time desc) sbl
*/
	declare @controller_id int
	set @controller_id = (select controller_id from dbo.Controller c where c.Vehicle_ID = @vehicle_id)  

	INSERT INTO #can_info
	SELECT
		[LOG_TIME], [FUEL_LEVEL1], [TOTAL_RUN]
	FROM [dbo].[CAN_INFO] WITH(NOLOCK)
	WHERE [Vehicle_ID] = @vehicle_id
	AND   [LOG_TIME]   BETWEEN @from - @noDataPeriod AND @to + @noDataPeriod

	DECLARE @statistic_log_recalc_to int = (SELECT [Log_Time] FROM [dbo].[Statistic_Log_Recalc] WHERE [Vehicle_ID] = @vehicle_id)
	INSERT INTO #run_log
/* Убрана точка вне пределов запроса (пердыдущая точка пробега, из-за ошибки если эта точка далеко)
	SELECT
		*
	FROM
	(
		SELECT TOP(1)
			[Log_Time],
			[Odometer]
		FROM [dbo].[Statistic_Log] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [Log_Time]   < @from
		ORDER BY [Log_Time] DESC
	) t1
	UNION ALL
*/
	SELECT
		*
	FROM
	(
		SELECT TOP(100) PERCENT
			[Log_Time],
			[Odometer]
		FROM [dbo].[Statistic_Log] WITH(NOLOCK)
		WHERE [Vehicle_ID] = @vehicle_id
		AND   [Log_Time]  >= @from
		AND   [Log_Time]  <= @to
		ORDER BY [Log_Time] ASC
	) t2
	WHERE t2.[Log_Time] < ISNULL(@statistic_log_recalc_to, 2147483647)

	SELECT COUNT(*) FROM #geo_log
	SELECT *        FROM #geo_log         ORDER BY [log_time]
	SELECT COUNT(*) FROM #gps_log
	SELECT *        FROM #gps_log         ORDER BY [log_time]
	SELECT COUNT(*) FROM #position_radius
	SELECT *        FROM #position_radius ORDER BY [log_time]

	select
		SensorLegend              = csl.Number,
		SensorNumber              = cs.Number,
		Multiplier                = csm.Multiplier,
		Constant                  = csm.Constant,
		Min_Value                 = csm.Min_Value,
		Max_Value                 = csm.Max_Value,
		Value_Expired             = isnull(cs.Value_Expired, csl.Value_Expired),
		Error                     = csm.Error,
		Controller_Sensor_Type_Id = csl.CONTROLLER_SENSOR_TYPE_ID
	from dbo.Controller_Sensor_Map csm
	join dbo.Controller_Sensor cs on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
	join dbo.Controller_Sensor_Legend csl on csl.Controller_Sensor_Legend_ID = csm.Controller_Sensor_Legend_ID
	where csm.Controller_ID = @controller_id

	select Fuel_Tank, Vehicle_Kind_ID from dbo.Vehicle where Vehicle_ID = @vehicle_id

	select csl.Number, count(1) [Count]
	from #controller_sensor_log csl
	group by csl.Number

	select * from #controller_sensor_log order by number, log_time
	select * from #can_info order by log_time

	insert into #picture_log
	 select Log_Time, Url
	  from dbo.Picture_Log (nolock)
	  where Vehicle_ID = @vehicle_id
		and Log_Time between @from and @to
	insert into #picture_log
	 select top(1) Log_Time, Url
	  from dbo.Picture_Log (nolock)
	  where Vehicle_ID = @vehicle_id
		and Log_Time < @from
	  order by Log_Time desc
	insert into #picture_log
	 select top(1) Log_Time, Url
	  from dbo.Picture_Log (nolock)
	  where Vehicle_ID = @vehicle_id
		and Log_Time > @to
	  order by Log_Time asc

	select count(1) from #picture_log
	select Log_Time, Url from #picture_log order by log_time

	--Пробег
	SELECT COUNT(*) FROM #run_log
	SELECT *        FROM #run_log ORDER BY [log_time]

	--Статистика
	select [Run] = CONVERT(numeric(18,2), [dbo].[CalculateDistanceFN](@from, @to, @vehicle_id))

	DROP TABLE #geo_log
	DROP TABLE #gps_log
	DROP TABLE #position_radius
	DROP TABLE #controller_sensor_log
	DROP TABLE #can_info
	DROP TABLE #run_log
END