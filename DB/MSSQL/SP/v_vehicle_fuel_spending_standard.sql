if exists (
	select * from sys.views where name = 'v_vehicle_fuel_spending_standard'
)
	drop view v_vehicle_fuel_spending_standard
go

create view v_vehicle_fuel_spending_standard
as
	select v.Vehicle_ID
		, RunStandardId = isnull(u.Id, 2)
		, RunStandardName = isnull (u.Name, 'LitrePer100Km')
		, RunStandard   = 
			case isnull (u.Name, 'LitrePer100Km')
				when 'LitrePer100Km'	then v.FuelSpendStandardLiter
				when 'KmPerLitre'		then v.FuelSpendStandardKilometer
				when 'KmPerM3'			then v.FuelSpendStandardKilometer
			end
		, RunStandardMultiplier = 
			case isnull (u.Name, 'LitrePer100Km')
				when 'LitrePer100Km'	then FuelSpendStandardLiter / 100
				when 'KmPerLitre'		then case when 0 < FuelSpendStandardKilometer then 1 / FuelSpendStandardKilometer else NULL end
				when 'KmPerM3'			then case when 0 < FuelSpendStandardKilometer then 1 / FuelSpendStandardKilometer else NULL end
			end
		from Vehicle v
		left outer join Fuel_Type ft on ft.ID = v.Fuel_Type_ID
		left outer join Unit u on u.Id = ft.Unit_Id

--select * from v_vehicle_fuel_spending_standard where vehicle_id = 821