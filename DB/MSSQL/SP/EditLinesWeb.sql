
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF EXISTS ( SELECT * FROM sysobjects WHERE name = N'EditLinesWeb' AND type = 'P')
    DROP PROCEDURE EditLinesWeb
GO

-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Edit Lines for Web
-- =============================================

CREATE PROCEDURE [dbo].[EditLinesWeb]  
	@line_id		int,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@description	nvarchar(500)
AS

------------------------------------------------
/*
-- Test
declare
	@line_id		int,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@description	nvarchar(500)
set @line_id = 1
*/
------------------------------------------------
	
--
set @vertex = replace(@vertex, ',', '.')

	
-- MAP_ID
declare @map_id	int
set @map_id = (
	select top 1 map_id from MAP_VERTEX as MV 
		inner join WEB_LINE_VERTEX as WLV on MV.VERTEX_ID = WLV.VERTEX_ID 
	where WLV.LINE_ID = @line_id
	)
if (@map_id is null)
	return


-- удаляем точки преждней линии
create table #Vertex(	
	--id int identity primary key, 
	Vertex_ID	int
	)

insert into #Vertex
select distinct	VERTEX_ID from WEB_LINE_VERTEX where LINE_ID = @line_id

delete from dbo.WEB_LINE_VERTEX where LINE_ID = @line_id
delete from dbo.MAP_VERTEX where VERTEX_ID in (select VERTEX_ID from #Vertex)


--
update WEB_LINE set [NAME] = @name, [DESCRIPTION] = @description where LINE_ID = @line_id


-- выделение в строке VERTEX подстроки с координатами для каждой точки линии
declare 
	@vertex1	varchar(100),
 	@x1			float,
 	@y1			float,
	@order		int,
	@vertex_id	int
set @order = 0

while (len(@vertex) > 1 and charindex(';', @vertex) > 1) 
begin
	set @order = @order + 1
	
	-- выделение координат каждой точки и ее направления
	set @vertex1 = substring(@vertex, 1, (charindex(';', @vertex) - 1))
	set @x1 = convert(float, substring(@vertex1, 1, (charindex(':', @vertex1) - 1))) 
	set @vertex1 = substring(@vertex1, (charindex(':', @vertex1) + 1), len(@vertex1))
	set @y1 = convert(float, @vertex1) 

	--запись полученных значений	
	insert dbo.MAP_VERTEX
		([MAP_ID], [X], [Y], [VISIBLE], [ENABLE]) 
	values
		(@map_id, @x1, @y1, 'true', 'true')

	set @vertex_id = (select @@identity)

	insert dbo.WEB_LINE_VERTEX
		([LINE_ID], [VERTEX_ID], [ORDER]) 
	values
		(@line_id, @vertex_id, @order) 
 
	set @vertex = substring(@vertex, (charindex(';', @vertex) + 1), len(@vertex))
end


drop table #Vertex
