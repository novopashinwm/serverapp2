IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetOperatorInfoByID' 
)
   DROP PROCEDURE [dbo].GetOperatorInfoByID
GO

CREATE PROCEDURE [dbo].[GetOperatorInfoByID]
	@operator_id int
AS
BEGIN

	set nocount on
	
	select 
		OPERATOR_ID,
		LOGIN,
		NAME
		from dbo.Operator o
		where o.OPERATOR_ID = @operator_id
	
END  
  
