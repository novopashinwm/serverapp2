﻿if exists (select * from sys.procedures where name = 'SendMsisdnOrPasswordAbsenceReport')
	drop procedure SendMsisdnOrPasswordAbsenceReport
go

create procedure SendMsisdnOrPasswordAbsenceReport
	@debug bit = 0
as
begin

	declare @tableHTML nvarchar(max) = 
		N'<H1>Терминалы без телефона или пароля</H1>' +
		N'<H2>Сводка</H2>' +
		N'<table border="1">' +
		N'<tr><th>Всего терминалов</th><th>Без телефона</th>' +
		N'<th>Без пароля</th></tr>' +
		N'<tr>' + 
		N'<td>' + convert(nvarchar(12), (select count(1) from v_msisdn_password_status)) + '</td>' +
		N'<td>' + convert(nvarchar(12), (select count(1) from v_msisdn_password_status where "Наличие телефона" = 0)) + '</td>' +
		N'<td>' + convert(nvarchar(12), (select count(1) from v_msisdn_password_status where "Наличие пароля" = 0)) + '</td>' +
		N'</tr></table>'
		

	SET @tableHTML = @tableHTML +
		N'<hr /><H2>Сводка по абонентам</H2>' +
		N'<table border="1">' +
		N'<tr><th>Абонент</th><th>Всего терминалов</th><th>Без телефона</th><th>Без пароля</th></tr></tr>' +
		CAST ( ( SELECT 
						td = [Абонент],       '',
						td = convert(nvarchar(12), sum(1)), '',
						td = convert(nvarchar(12), sum(case when [Наличие телефона] = 0 then 1 else 0 end)), '',
						td = convert(nvarchar(12), sum(case when [Наличие пароля]   = 0 then 1 else 0 end)), ''
					  FROM v_msisdn_password_status
					  group by [Абонент]
					  FOR XML PATH('tr')
			) AS NVARCHAR(MAX) ) +
		N'</table>'

	SET @tableHTML = @tableHTML +
		N'<hr /><H2>Детализация</H2><table border="1">' +
		N'<tr><th>Тип</th><th>IMEI / номер</th>' +
		N'<th>Название машины</th><th>Абонент</th><th>Наличие телефона</th>' +
		N'<th>Наличие пароля</th></tr>' +
		CAST ( ( SELECT td = "Тип",       '',
						td = "IMEI / номер", '',
						td = "Vehicle name", '',
						td = "Абонент", '',
						td = case "Наличие телефона" when 1 then 'Задан' else 'Отсутствует' end, '',
						td = case "Наличие пароля" 
								when 1 then 'Задан'
								when 0 then 'Отсутствует'
								else 'Не применимо' 
							 end
				  FROM v_msisdn_password_status
				  where [Наличие телефона] = 0 or [Наличие пароля] = 0
				  FOR XML PATH('tr')
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;

	declare @subject nvarchar(max) = 'Отчёт об отсутствии телефона или пароля для терминалов на ' + convert(nvarchar(64), getdate())

	if (isnull(@debug, 0) = 0)	
	begin
		exec msdb.dbo.sp_send_dbmail @recipients = 'AOsipov@nvg.ru;SChulskiy@nvg.ru', 
		@subject = @subject, 
		@body = @tableHTML, 
		@body_format = 'HTML'
	end
	else
	begin
		select @subject, @tableHTML		
	end
		
end