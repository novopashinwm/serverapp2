
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ������� ��� ������ �� WEB

-- EXEC ReportCommercialServices @operator_id = 28549, @from = '2019-10-01 00:00:000', @to = '2019-10-31 00:00:000'
-- SELECT * from operator where login like '%admin%'

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.ReportCommercialServices'))
BEGIN
	DROP PROCEDURE [dbo].[ReportCommercialServices]
END

GO

CREATE PROCEDURE [dbo].[ReportCommercialServices]
(
	@operator_id int,
	@from        datetime = null,
	@to          datetime = null,
	@country     nvarchar(255) = NULL
)
AS
BEGIN


			select 
			Country = co.Name,
			MacroRegion = ISNULL(oodr.name, '������') ,
			CustomerName = d.name,
			ContractNumber = isnull(d.ExtID,'N/A'),
			Terminal_Device_Number = v.GARAGE_NUMBER, -- ci.Device_ID + ISNULL(' (' + ci.Phone + ')', ''),
			ServiceTypeCategory = 'GNSS', /*���� ��� ����������� �����, �� ���������� 'GNSS'*/
			ServiceName = 'GEOLOCATION',

			StartDate = min (sl.date_from), /*���� ������ �� ������� @day*/
			EndDate = max (sl.date_to), /*���� ����� �� ������� @day*/
			RenderedCount =  count(*),  /*���������� ��� �������� ����������*/
			LbsPositionCount = 0,
			LbsPositionRequested = 0,
			RenderedLastTime = max (sl.date_to)
				
		FROM DEPARTMENT d
			inner join vehicle v on d.DEPARTMENT_ID = v.DEPARTMENT
			inner join Country co on co.Country_ID = d.Country_ID
			left join REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS sl on sl.VEHICLE_ID = v.VEHICLE_ID

			OUTER APPLY
			(
				SELECT distinct o.name
				FROM v_operator_department_right odr 
					inner join OPERATOR o on o.OPERATOR_ID = odr.operator_id
				WHERE odr.department_id = d.DEPARTMENT_ID
					AND odr.right_id in (2, 110) /*ServiceManagement*/
					and o.operator_id != 28549
			) as oodr
		WHERE 1 = 1
			AND sl.DATE_FROM between @from and @to
		GROUP BY co.Name, v.GARAGE_NUMBER, d.name, d.ExtID, oodr.name
		ORDER BY Country, MacroRegion, CustomerName, v.GARAGE_NUMBER, StartDate
--			
END
GO

-- ��� JOB / ��������� ����������� ��������� ������ �� ��������� �������� ���� 
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.REPORT_COMMERCIAL_SERVICES_RECALC'))
BEGIN
	DROP PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]
	
END
GO

CREATE PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_RECALC]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @to datetime
	DECLARE @from datetime

	set @to = dateadd(day, datediff(day, 0, getdate()), 0);
	set @from = DATEADD (DAY, -30, @to)

	PRINT 'Start @from ' + (CONVERT( VARCHAR(24), @from, 121))
	PRINT 'Start @to ' + (CONVERT( VARCHAR(24), @to, 121))

	DECLARE @current datetime = @from
	WHILE @current < @to
	BEGIN
		
		PRINT 'START ' + (CONVERT( VARCHAR(24), @from, 121)) + ' at ' + (CONVERT( VARCHAR(24), getDate(), 121))

		exec dbo.REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS
			@from = @current
	
		PRINT 'END ' +(CONVERT( VARCHAR(24), @from, 121)) + ' at ' + (CONVERT( VARCHAR(24), getDate(), 121))


		SET @current = DATEADD(day, 1, @current)

		

		WAITFOR DELAY '00:01'
	END

END
GO

-- ��� JOB / �������� �� ���� ����
-- exec REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS @from = '2019-10-21 00:00.000'
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS'))
BEGIN
	DROP PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]
	
END
GO

create PROCEDURE [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_1DAY_HITS]
(
	@from        datetime = nul
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @to datetime,  @to_ETC datetime,  @from_ETC datetime, @from_int int, @to_int int

	--set @from = '2019-10-21 00:00.000'
	set @to = DATEADD (DAY, 1, @from)


	CREATE TABLE #firstinsert_sl  
		([vehicle_id] int NOT NULL,
		[from2] datetime NOT NULL, 
		[to2] datetime NOT NULL,
		[hits] int)


	select @to_ETC = dateadd(MINUTE, -DATEPART(TZoffset, SYSDATETIMEOFFSET()), @to)
	select @from_ETC = dateadd(MINUTE, -DATEPART(TZoffset, SYSDATETIMEOFFSET()), @from)


	select @to_int = CONVERT(INT, DATEDIFF(SECOND, '19700101', @to_ETC))
	select @from_int = CONVERT(INT, DATEDIFF(SECOND, '19700101', @from_ETC))

	--INSERT INTO #firstinsert_sl
	--select distinct vehicle_id, @from, @to, count(*)
	--from  Statistic_Log sl
	--where 1 = 1 --sl.Vehicle_ID = c.VEHICLE_ID 
	--	and dateadd(MINUTE, DATEPART(TZoffset, SYSDATETIMEOFFSET()), dateadd(s, sl.Log_Time, '1970-01-01')) between @from and @to  /*�������� ����� �� UTC � ������� �� ���������, ���� ������� ���� � ���*/  
	--group by vehicle_id
	
	INSERT INTO #firstinsert_sl
	select distinct vehicle_id, @from, @to, count(*)
	from  Statistic_Log sl
	where 1 = 1  
		and sl.Log_Time between @from_int and @to_int  /*�������� ����� �� UTC � ������� �� ���������, ���� ������� ���� � ���*/  
	group by vehicle_id

	DELETE 
	FROM REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS
	WHERE DATE_FROM = @from and DATE_TO = @TO

	INSERT REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS
	SELECT vehicle_id, @from, @to, HITS
	FROM #firstinsert_sl

END
GO