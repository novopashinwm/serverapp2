exec CreateProcedure 'RemoveControllerTypeCommand'
go

alter procedure RemoveControllerTypeCommand
(
	@controllerTypeName nvarchar(50),
	@commandTypeCode    varchar(200)
)
as

	delete e
		from CONTROLLER_TYPE ct, CommandTypes cmd, Controller_Type_CommandTypes e
		where ct.TYPE_NAME = @controllerTypeName
			and cmd.code = @commandTypeCode
			and e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID 
			and e.CommandTypes_ID = cmd.id