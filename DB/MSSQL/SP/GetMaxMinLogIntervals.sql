SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[GetMaxMinLogIntervals]    Script Date: 08/14/2009 11:57:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMaxMinLogIntervals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMaxMinLogIntervals]
GO

CREATE procedure [dbo].[GetMaxMinLogIntervals]
	(
	@startDateUTC		datetime, 
	@endDateUTC	datetime,
	@vehicleGroupID int = null
	)
as
begin
set nocount on;
/*

exec dbo.GetMaxMinLogIntervals '2009-08-13', '2009-08-15'
*/
declare @startDate int;
declare @endDate int;

set @startDate = datediff(s, cast('1970' as datetime), @startDateUTC);
set @endDate = datediff(s, cast('1970' as datetime), @endDateUTC);

declare @t table (VEHICLE_ID int, Garage_number nvarchar(512), [min_int] int, [max_int] int, [avg_int] int);

with mintime (monitoree_id, fr, t) as
(
	select mg.monitoree_id, mg.log_time as fr, 
			(
			select min(mg2.log_time) from monitoree_log mg2 (nolock) 
			where mg2.monitoree_id = mg.monitoree_id 
			and mg2.log_time > (mg.log_time + 4)
			and mg2.log_time < @endDate
			) as t

	from monitoree_log mg (nolock)
	where 1=1 
	---and mg.monitoree_id = vg.VEHICLE_ID 
	and mg.log_time > @startDate
	and mg.log_time < @endDate
	--group by mg.monitoree_id
)
insert into @t
select v.VEHICLE_ID, v.Garage_number,
min(isnull(mintime.t-mintime.fr, 20000000)) 'min_int',
max(isnull(mintime.t-mintime.fr, 0)) 'max_int',
avg(isnull(mintime.t-mintime.fr, 0)) 'avg_int'
from 
---dbo.VEHICLEGROUP_VEHICLE  vg (nolock) 
VEHICLE v ---on v.VEHICLE_ID = vg.VEHICLE_ID
left join minTime on v.VEHICLE_ID = minTime.monitoree_id
where 1=1 
--and vg.VEHICLEGROUP_id = @VEHICLEGROUP_id
group by v.VEHICLE_ID, v.Garage_number

update @t
set min_int = null where min_int = 20000000;

if (@vehicleGroupID is not null)
	select t.* from @t t
	inner join vehicleGroup_vehicle vgv on vgv.vehicle_id = t.VEHICLE_ID
	where vgv.vehicleGroup_id = @vehicleGroupID
	order by isNull(min_int, 0), isnull(avg_int, 0), isnull(max_int, 0);
else
	select * from @t
	order by isNull(min_int, 0), isnull(avg_int, 0), isnull(max_int, 0);

end