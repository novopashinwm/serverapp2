if exists (select * from sys.procedures where name = 'ProcessFreeMsisdns')
	drop procedure ProcessFreeMsisdns
go

create procedure ProcessFreeMsisdns
as

insert into billing_service (Billing_Service_Type_ID, Asid_ID, startdate, EndDate)
	select bst.ID, a.ID, getutcdate(), isnull(fm.EndDate, dateadd(year, 10, getutcdate()))
		from FreeMsisdn fm
		join v_phone p on p.msisdn = fm.value
		join asid a on a.contact_id = p.asid_id
		join billing_service_type bst on bst.service_type = 'FRNIKA.MLP.ChargingNolimits.Trial'
		where not exists (select * from billing_service bs where bs.asid_id = a.id and bs.Billing_Service_Type_ID = bst.ID)

update bs set EndDate = isnull(p.endDate, '2019.01.01'), MaxQuantity = 100 * 30
	from FreeMsisdn p
	join Contact pc on pc.Value = p.value
	join Contact ac on ac.Demasked_ID = pc.ID
	join Asid a on a.Contact_ID = ac.ID
	join Billing_Service bs on bs.Asid_ID = a.ID 
	                       and bs.Billing_Service_Type_ID in (select bst.ID from Billing_Service_Type bst where bst.Service_Type in ('FRNIKA.MLP.ChargingNolimits.Trial'))
	where bs.EndDate < '2019.01.01'
