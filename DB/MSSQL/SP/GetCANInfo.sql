set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

/****** Object:  StoredProcedure [dbo].[GetCANInfo]    Script Date: 09/02/2009 09:45:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCANInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCANInfo]
GO

CREATE PROCEDURE [dbo].[GetCANInfo]
	@log_time	int,				-- UTC time (attached monitoree_log time)
	@vehicle_id	int
AS
BEGIN

	set nocount on
	
	---����� ���������� �� CAN ������ � �������� 3 ���� �� ������������� �������

	select top(1) ci.VEHICLE_ID MONITOREE_ID,
		ci.[LOG_TIME], [CAN_TIME], [SPEED], [CRUISE_CONTROL],[BRAKE],[PARKING_BRAKE],[CLUTCH],[ACCELERATOR],
		[FUEL_RATE],[FUEL_LEVEL1],	[FUEL_LEVEL2],[FUEL_LEVEL3],[FUEL_LEVEL4],[FUEL_LEVEL5],
		[FUEL_LEVEL6],[REVS],[RUN_TO_MNTNC],[ENG_HOURS],[COOLANT_T],[ENG_OIL_T],[FUEL_T],
		[TOTAL_RUN],[DAY_RUN],[AXLE_LOAD1],	[AXLE_LOAD2],[AXLE_LOAD3],[AXLE_LOAD4],
		[AXLE_LOAD5],[AXLE_LOAD6]
	from CAN_INFO ci (nolock)
	where   ci.Vehicle_ID = @vehicle_id 
		and ci.LOG_TIME > (@log_time - 259200)
		and ci.LOG_TIME <= @log_time
	order by ci.Log_Time desc;

	if @@rowcount = 0
		return -1;

	return 1;


END

