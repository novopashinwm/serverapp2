﻿-- Operator -> Department

if exists (Select * from sys.views where name = 'v_operator_department_right_operator_department')
	drop view v_operator_department_right_operator_department
go
create view v_operator_department_right_operator_department
with schemabinding
as
	select operator_id, DEPARTMENT_ID, right_id
		from dbo.OPERATOR_DEPARTMENT
	where ALLOWED = 1
go
create unique clustered index PK_v_operator_department_right_operator_department
	on v_operator_department_right_operator_department(operator_id, department_id, right_id)
go
create nonclustered index IX_v_operator_department_right_operator_department_vid_rid_oid
	on v_operator_department_right_operator_department(department_id, right_id, operator_id)
go

-- OperatorGroup -> Department

if exists (Select * from sys.views where name = 'v_operator_department_right_operatorgroup_department')
	drop view v_operator_department_right_operatorgroup_department
go
create view v_operator_department_right_operatorgroup_department
with schemabinding
as
	select og_o.operator_id, og_v.DEPARTMENT_ID, og_v.right_id, count_big(*) c
		from dbo.OPERATORGROUP_DEPARTMENT og_v
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
	where og_v.ALLOWED = 1
	group by og_o.operator_id, og_v.DEPARTMENT_ID, og_v.right_id
go
create unique clustered index PK_v_operator_department_right_operatorgroup_department
	on v_operator_department_right_operatorgroup_department(operator_id, department_id, right_id)
go
create nonclustered index IX_v_operator_department_right_operatorgroup_department_vid_rid_oid
	on v_operator_department_right_operatorgroup_department(department_id, right_id, operator_id)
go


-- Country -> OperatorGroup -> Department

if exists (Select * from sys.views where name = 'v_operator_department_right_country_operatorgroup_department')
	drop view v_operator_department_right_country_operatorgroup_department
go
create view v_operator_department_right_country_operatorgroup_department
with schemabinding
as
	select og_o.operator_id, d.DEPARTMENT_ID, r.right_id, count_big(*) c
		from dbo.Department d 
		join dbo.OperatorGroup_Country ogc on ogc.Country_ID = d.Country_ID
		join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = ogc.operatorgroup_id
		join dbo.[right] r on r.right_id  in (2, 104, 110)
	group by og_o.operator_id, d.DEPARTMENT_ID, r.right_id
go
create unique clustered index PK_v_operator_department_right_country_operatorgroup_department
	on v_operator_department_right_country_operatorgroup_department(operator_id, department_id, right_id)
go
create nonclustered index IX_v_operator_department_right_country_operatorgroup_department
	on v_operator_department_right_country_operatorgroup_department(department_id, right_id, operator_id)
go

-- Summary

if exists (Select * from sys.views where name = 'v_operator_department_right')
	drop view v_operator_department_right
go
create view v_operator_department_right 
as
	select operator_id, department_id, right_id, priority = min(priority)
	from
	(
		select operator_id, department_id, right_id, priority = 1 from v_operator_department_right_operator_department with (noexpand) 
		union
		select operator_id, department_id, right_id, priority = 2 from v_operator_department_right_operatorgroup_department with (noexpand) 
		union
		select operator_id, department_id, right_id, priority = 3 from v_operator_department_right_country_operatorgroup_department with (noexpand)
	) t
	group by operator_id, department_id, right_id
go