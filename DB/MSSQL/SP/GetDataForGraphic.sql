/****** Object:  StoredProcedure [dbo].[GetDataForGraphic]    Script Date: 07/08/2010 12:03:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetDataForGraphic] 
	@operator_id int = null
AS
begin
	select distinct G.* 
	from GRAPHIC G 
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = G.DEPARTMENT_ID
	where @operator_id is null 
	or (dr.operator_id is not null and dr.operator_id = @operator_id)
	
	select distinct gs.* 
	from GRAPHIC_SHIFT gs
	join GRAPHIC G on g.GRAPHIC_ID = gs.GRAPHIC 
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = G.DEPARTMENT_ID
	where @operator_id is null 
	or (dr.operator_id is not null and dr.operator_id = @operator_id)
	
	select distinct d.* 
	from DEPARTMENT d
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = d.DEPARTMENT_ID
	where @operator_id is null 
	or (dr.operator_id is not null and dr.operator_id = @operator_id)
end








