if exists (select * from sys.views where name = 'v_operatorgroup_department_right')
	drop view v_operatorgroup_department_right
go

create view v_operatorgroup_department_right
as
select og.OperatorGroup_ID
	, OperatorGroup_Name = og.NAME
	, d.DEPARTMENT_ID
	, Department_Name = d.NAME
	, Department_ExtID = d.ExtID
	, Country_Name = c.Name
	, Right_Name = r.NAME
	, ogd.ALLOWED
	from OPERATORGROUP og
	join OPERATORGROUP_DEPARTMENT ogd on ogd.OPERATORGROUP_ID = og.OPERATORGROUP_ID
	join [RIGHT] r on r.RIGHT_ID = ogd.right_id
	join DEPARTMENT d on d.DEPARTMENT_ID = ogd.department_id
	left outer join Country c on c.Country_ID = d.Country_ID
