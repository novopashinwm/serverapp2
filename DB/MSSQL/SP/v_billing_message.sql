if exists (select * from sys.views where name = 'v_billing_message')
	drop view v_billing_message
go

create view v_billing_message
as
select m.MESSAGE_ID
	, m.Created
	, m.TIME
	, Action = actionParam.CONTENT
	, ContractNumber = contractNumberParam.CONTENT
	, TerminalDeviceNumber = terminalDeviceNumberParam.CONTENT
	, ServiceCode = serviceCodeParam.CONTENT
	, Response = responseParam.CONTENT
	, ResponseText = isnull(responseTextParam.LargeContent, responseTextParam.CONTENT)
	from message m
	left outer join v_message_field actionParam on 
			actionParam.message_id = m.MESSAGE_ID 
		and actionParam.NAME = 'Action'
	left outer join v_message_field contractNumberParam on 
			contractNumberParam.message_id = m.MESSAGE_ID 
		and contractNumberParam.NAME = 'ContractNumber'
	left outer join v_message_field terminalDeviceNumberParam on 
			terminalDeviceNumberParam.message_id = m.MESSAGE_ID 
		and terminalDeviceNumberParam.NAME = 'TerminalDeviceNumber'
	left outer join v_message_field serviceCodeParam on 
			serviceCodeParam.message_id = m.MESSAGE_ID 
		and serviceCodeParam.NAME = 'ServiceCode'
	left outer join v_message_field responseParam on 
			responseParam.message_id = m.MESSAGE_ID 
		and responseParam.NAME = 'Response'
	left outer join v_message_field responseTextParam on 
			responseTextParam.message_id = m.MESSAGE_ID 
		and responseTextParam.NAME = 'ResponseText'
	where m.DestinationType_ID = 8
