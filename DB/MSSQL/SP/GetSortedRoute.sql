set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


ALTER  procedure [dbo].[GetSortedRoute]
	@operatorID int = null
as
begin
	if (@operatorID is null)
		begin
			select	*
			from [ROUTE] R
			order by
			case
				when isnumeric(RTRIM(R.EXT_NUMBER))=1
					then convert( numeric(9), RTRIM(R.EXT_NUMBER))
				when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
					then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
				else 
					999999
			end
			,R.EXT_NUMBER
		end
	else
		begin
			select	*
			from [ROUTE] R
			join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
			where rr.OPERATOR_ID = @operatorID
			order by
			case
				when isnumeric(RTRIM(R.EXT_NUMBER))=1
					then convert( numeric(9), RTRIM(R.EXT_NUMBER))
				when isnumeric(LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))=1
					then convert( numeric(9), LEFT(RTRIM(R.EXT_NUMBER),LEN(RTRIM(R.EXT_NUMBER))-1))
				else 
					999999
			end
			,R.EXT_NUMBER
		end


end













