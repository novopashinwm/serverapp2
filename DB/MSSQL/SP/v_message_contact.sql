if exists (
	select *
		from sys.views 
		where name = 'v_message_contact'
)
	drop view v_message_contact
go


create view v_message_contact
as
select mc.message_id
     , mc.contact_id
     , ContactTypeName = ct.Name
     , c.value
     , type_name = mct.Name
	 from Message_Contact mc
	 join Contact c on c.ID = mc.Contact_ID
	 join MessageContactType mct on mct.Id = mc.Type
	 join ContactType ct on ct.Id = c.Type
	 