SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetPosAbsence]
	@from	datetime	= null,	-- begin of check period
	@to	datetime	= null,	-- end of check period
	@tint	int		= 120,	-- max tolerable data absence in secs
	-- 0 bit: 0 ╓ ыЎ°Єvїм TT, ■к ·■к■иv┐ ёv√Ё °¤п■и№Ёб°а ўЁ  ■╕√їЇ¤°∙ Ї■ ║╕к°№v∙ °¤кїиЄЁ√ @tint, 1 ╓ ° ыЎ°Єvїм, ° ы№микЄvїм; 
	-- 1 bit: 0 ╓ иЁё■кЁжХ°ї TT, °Ї║Х°ї  ■ ║вмк¤v№ иї∙╕Ё№, 1 ╓ Є╕ї TT, ¤їўЁЄ°╕°№■ ■к к° Ё иї∙╕Ё
	@status	int		= 3,	-- TкЁк║╕-№Ё╕·Ё (3 - ыЎ°Єvїм, ы№микЄvїм,  ■ иї∙╕Ё№ ° ¤ї  ■ иї∙╕Ё№, к■ ї╕к╣ Є╕ї) 
	@vid	int		= null
AS

--Не используется
return 0;
------------------------------------------------------------------------------
/*
-- Test
declare
	@from		datetime,	--╕
	@to		datetime,	-- ■
	@tint		int,		--°¤кїиЄЁ√		
	--0 bit: 0 ╓ ыЎ°Єvїм TT, ■к ·■к■иv┐ ёv√Ё °¤п■и№Ёб°а ўЁ  ■╕√їЇ¤°∙ Ї■ ║╕к°№v∙ °¤кїиЄЁ√, 1 ╓ ° ыЎ°Єvїм ° ы№микЄvїм; 
    	--1 bit: 0 ╓ иЁё■кЁжХ°ї TT, °Ї║Х°ї  ■ ║вмк¤v№ иї∙╕Ё№, 1 ╓ Є╕ї TT, ¤їўЁЄ°╕°№■ ■к к° Ё иї∙╕Ё
	@status		int, 
	@vid		int			

set @from = '2007-08-16 00:00:00'
set @to = '2007-08-17 12:00:00'
set @tint = 120
set @status = 3
set @vid = null
*/
------------------------------------------------------------------------------

set transaction isolation level read uncommitted

-- ensure bounds
if @to is null set @to = getutcdate()
if @from is null set @from = dateadd(n, -15, @to)
--ж■вї№║-к■ ўЁ иЁ░°ЄЁ√╕а к■√╣·■  ■╕√їЇ¤°∙ вЁ╕, ўЁ·■№№ї¤к°и■ЄЁ√
--if datediff(n, @from, @to) > 60 set @from = dateadd(n, -60, @to)

print 'checking for absence from ' + convert(varchar, @from) + ' to ' + convert(varchar, @to)

--  їи°■Ї Є ╕ї·║¤ЇЁ┐ Ї√а ўЁ и■╕Ё °ў MONITOREE_LOG
declare 
	@start	int, 
	@finish int
set @start = datediff(s, '1970', @from) 
set @finish = datediff(s, '1970', @to)

-- T°  ·■¤ки■√√їиЁ SGGB (¤ї а╕¤■  ■вї№║ к■√╣·■ SGGB, Є°Ї°№■ °ў-ўЁ  ■ЇЇїиЎ·° ЁЄк■Ї■╕v√Ё ??? )
declare @contr_type int
set @contr_type = 2


-- иїў║√╣к°и║жХЁа кЁё√°бЁ
create table #t (
	id			int identity primary key clustered, 
	mid			int, 
	log_time	int, 
	diff		int
	)

-- fill with work pts. only live or all
insert #t (mid, log_time)
select t.* from (
	select vehicle_id, LOG_TIME from log_time where LOG_TIME between @start and @finish
	union
	select v.VEHICLE_ID, @start from VEHICLE as v
	union
	select v.VEHICLE_ID, @finish from VEHICLE as v
	) as t 
where 
	--0 bit: 1 ╓ ыЎ°Єvїм ° ы№микЄvїм °√° 0 ╓ ыЎ°Єvїм TT, ■к ·■к■иv┐ ёv√Ё °¤п■и№Ёб°а ўЁ  ■╕√їЇ¤°∙ Ї■ ║╕к°№v∙ °¤кїиЄЁ√ @tint; 
	@status & 1 = 1 
	or exists(select 0 from log_time i where i.vehicle_id = t.vehicle_id and i.log_time >= datediff(s, '1970', getutcdate()) - @tint) 
ORDER by 
	vehicle_id, LOG_TIME


-- set diff between each consequent pair - °¤кїиЄЁ√ №їЎЇ║  ■ў°б°а№°
update t set t.diff = t.log_time - d.log_time from #t as t join #t as d on d.mid = t.mid and d.id = t.id - 1

--select t.*, dateadd(s, log_time, '1970') as log_date_time from #t as t


-- without regard of work time and trips
--1 bit: 1 ╓ Є╕ї TT, ¤їўЁЄ°╕°№■ ■к к° Ё иї∙╕Ё
if @status & 2 = 2 
begin
	select 
		t.mid						VehicleID, 
		c.NUMBER					UnitNo, 
		c.Phone, 
		dateadd(s, t.log_time - t.diff, '1970')		FromTime, 
		dateadd(s, t.log_time, '1970')			ToTime 
	from #t as t
		join controller as c on t.mid = c.vehicle_id 
	where 
		t.diff >= @tint 
		and c.controller_type_id = @contr_type 
		and (@vid is null or t.mid = @vid)
	order by 
		c.VehicleID, FromTime
	return 0
end


-- only on trips with line_time = 1
--1 bit: 0 ╓ иЁё■кЁжХ°ї TT, °Ї║Х°ї  ■ ║вмк¤v№ иї∙╕Ё№
select distinct 
	t.mid						VehicleID, 
	c.NUMBER					UnitNo, 
	c.Phone, 
	dateadd(s, t.log_time - t.diff, '1970')		FromTime, 
	dateadd(s, t.log_time, '1970')			ToTime
from #t as t
	join controller as c on t.mid = c.vehicle_id 
	join waybill_header w on w.vehicle_id = c.vehicle_id 
	join wb_trip as wt on w.waybill_header_id = wt.waybill_header_id 
	join trip_kind as tk on wt.trip_kind_id = tk.trip_kind_id 
where 
	t.diff >= @tint 
	and c.controller_type_id = @contr_type 
	and wt.begin_time <= @to and wt.end_time >= @from 
	and line_time = 1
	and (@vid is null or t.mid = @vid)
order by 
	VehicleID, FromTime
	

drop table #t

return 0


