if exists (
	select *
		from sys.objects
		where name = 'utc2msk'
)
	drop function utc2msk
go

create function dbo.utc2msk
(
	@datetime datetime
)
returns datetime
as
begin

	return dateadd(hour, 3, @datetime)

end