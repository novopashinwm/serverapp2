if exists (
	select *
		from sys.procedures 
		where name = 'CompressControllerSensorLog'
)
	drop procedure CompressControllerSensorLog
go

create procedure CompressControllerSensorLog
(
	@vehicle_id int,
	@number int,
	@from int,
	@to int,
	@debug bit = null
)
as

if @debug is null
	set @debug = 0
	
if @debug = 0
	set nocount on

declare @change table (id int identity(1,1) primary key clustered, Prev_Log_Time int, Prev_Value int, Log_Time int, Value int)

--��������� ��������� �� ������ ��������������� �������
if @debug = 1
	print 'Last change before @from'
insert into @change (Prev_Log_Time, Prev_Value, Log_Time, Value)
	select top(1) Prev_Log_Time, Prev_Value, Log_Time, Value
		from v_controller_sensor_value_change c
		where     c.vehicle_id = @vehicle_id
			  and c.number = @number
			  and c.log_time < @from
		order by c.Log_Time desc

--���� �� ������ ��������������� ������� ������ �� ��������,
--� ��� ����� null->value, 
--������ ��� �������� ���� ����������� ��� �� �� ���� ������
if @@ROWCOUNT = 0
	return;

--��������� ���������
if @debug = 1
	print 'All changes within interval'
	
insert into @change (Prev_Log_Time, Prev_Value, Log_Time, Value)
	select Prev_Log_Time, Prev_Value, Log_Time, Value
		from v_controller_sensor_value_change c
		where     c.vehicle_id = @vehicle_id
			  and c.number = @number
			  and c.log_time between @from and @to
		order by c.Log_Time

--������������ ��������, �� ������� ����� ������� �������� �������, ����� ��-�������� ���������, ��� ��������� ������� �� �������
declare @interval int =
	(select MIN(Value_Expired) from v_controller_sensor_map where Vehicle_ID = @vehicle_id and Sensor_Number = @number)
	
if (@interval is null or 10 * 60 < @interval)
	set @interval = 10 * 60

if @debug = 1
	print '@interval = ' + convert(varchar(10), @interval)

--� ���� ������� �������� ���� ��������� �������, ������� ����� ��������
declare @leave table (log_time int primary key clustered)

if @debug = 1
	print 'insert into @leave'

--TODO: ��������� ������, ��� �����, ���� � ��������� ������ ��������� �� ���� ��� ���� ����� ���� ���������?
insert into @leave
	select t.Log_Time
	from (
		select Log_Time = MIN(l.Log_Time)
			from @change c
			join @change pc on pc.id = c.id - 1
			join Controller_Sensor_Log l with (nolock) on l.Vehicle_ID = @vehicle_id
													  and l.Number = @number
													  and l.Log_Time between pc.Log_Time + 1 and c.Prev_Log_Time - 1
			where     l.Log_Time >= pc.Log_Time + @interval 
			group by (l.Log_Time - pc.Log_Time) / @interval
		union all 
			select c.Log_Time 
				from @change c
		union all 
			select c.Prev_Log_Time 
				from @change c
				where c.Prev_Log_Time is not null 
				  and c.Prev_Log_Time not in (select c.Log_Time from @change c)
	) t
	order by t.Log_Time
	
declare @delete_from int, @delete_to int

select @delete_from = min(Log_Time)
     , @delete_to   = MAX(Log_Time)
	from @change
	
if @debug = 1
	print 'delete'

delete l
	from Controller_Sensor_Log l
	where l.Vehicle_ID = @vehicle_id
	  and l.Number = @number
	  and l.Log_Time between @delete_from and @delete_to
	  and l.Log_Time not in (select leave.Log_Time from @leave leave)