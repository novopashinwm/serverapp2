/****** Object:  StoredProcedure [dbo].[GetSortedController]    Script Date: 07/09/2010 09:49:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GetSortedController]
@operator_id int = null
as
begin
	select distinct C.* 
	from dbo.CONTROLLER C
	join v_operator_vehicle_right vr on vr.vehicle_id = c.VEHICLE_ID
	where @operator_id is null or (vr.operator_id = @operator_id and vr.right_id = 102) --VehicleAccess
	
	select * from dbo.CONTROLLER_TYPE
end




