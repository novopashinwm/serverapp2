set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if (exists (select 1 from sys.objects where name = 'GetSensorsForPositions' and type='p'))
	drop procedure dbo.GetSensorsForPositions;
go

create procedure dbo.GetSensorsForPositions
	@vehicle_sensor		dbo.Vehicle_Sensor_Param readonly,
	@vehicle_log_time	dbo.Vehicle_Log_Time_Param readonly,
	@include_neighbour	bit = 0,
	@debug bit = NULL
as
begin

	if isnull(@debug, 0) = 0
		set nocount on;

	create table #vehicle_log_time (Vehicle_ID int, Log_Time int, primary key clustered (Vehicle_ID, Log_Time))
	
	if @debug = 1
		print 'Fill #vehicle_log_time'
	
	insert into #vehicle_log_time
		select Vehicle_ID, Log_Time
			from @vehicle_log_time

	create table #vehicle_sensor 
	(
		Vehicle_ID		int not null, 
		Number	int not null, 
		Min_Log_Time	int,
		Max_Log_Time	int,
		Value_Expired	int not null,
		primary key clustered (Vehicle_ID, Number)
	);
	
	if @debug = 1
		print 'Fill #vehicle_sensor'

	insert into #vehicle_sensor (Vehicle_ID, Number, Min_Log_Time, Max_Log_Time, Value_Expired)
		select 
			 vs.Vehicle_ID
			,vs.Sensor_Number
			,(select top(1) Log_Time from #vehicle_log_time vlt where vlt.Vehicle_ID = vs.Vehicle_ID order by vlt.Log_Time asc)
			,(select top(1) Log_Time from #vehicle_log_time vlt where vlt.Vehicle_ID = vs.Vehicle_ID order by vlt.Log_Time desc)
			,isnull(max(csm.Value_Expired), 24*60*60)
			from @vehicle_sensor vs
			join dbo.v_controller_sensor_map csm on csm.Vehicle_ID = vs.Vehicle_ID 
												and csm.Sensor_Number = vs.Sensor_Number
			group by vs.Vehicle_ID, vs.Sensor_Number

	create table #neighbour_log_time
	(
		Vehicle_ID int not null, 
		Log_Time   int not null, 
		Number     int not null,
		primary key clustered (Vehicle_ID, Log_Time, Number)
	)

	if @include_neighbour = 1
	begin
	
		if @debug = 1
			print 'Fill neighboars from the past'

		--��� ������� ������� ��������� ��������� �������� �� ��������		
		insert into #neighbour_log_time (Vehicle_ID, Log_Time, Number)
			select 
				 vs.Vehicle_ID
				,(select top(1) csl.Log_Time 
					from dbo.Controller_Sensor_Log csl (nolock) 
					where csl.Vehicle_ID = vs.Vehicle_ID 
					  and csl.Log_Time between vs.Min_Log_Time - vs.Value_Expired and vs.Min_Log_Time - 1 
					  and csl.Number = vs.Number
					order by csl.Log_Time desc)
				,vs.Number
				from #vehicle_sensor vs
				where exists (
					select 1
					from dbo.Controller_Sensor_Log csl (nolock) 
					where csl.Vehicle_ID = vs.Vehicle_ID 
					  and csl.Log_Time between vs.Min_Log_Time - vs.Value_Expired and vs.Min_Log_Time - 1
					  and csl.Number = vs.Number )
		
		if @debug = 1
			print 'Fill neighboars from the future'

		--��� ������� ������� ������ ��������� �������� �� ��������
		insert into #neighbour_log_time (Vehicle_ID, Log_Time, Number)
			select 
				 vs.Vehicle_ID
				,(select top(1) csl.Log_Time 
					from dbo.Controller_Sensor_Log csl (nolock) 
					where csl.Vehicle_ID = vs.Vehicle_ID 
					  and csl.Log_Time between vs.Max_Log_Time + 1 and vs.Min_Log_Time + vs.Value_Expired 
					  and csl.Number = vs.Number
					order by csl.Log_Time asc)
				,vs.Number
				from #vehicle_sensor vs
				where exists (
					select 1
						from dbo.Controller_Sensor_Log csl (nolock) 
						where csl.Vehicle_ID = vs.Vehicle_ID 
						  and csl.Log_Time between vs.Max_Log_Time + 1 and vs.Min_Log_Time + vs.Value_Expired 
						  and csl.Number = vs.Number )
						   
	end
	
	if @debug = 1
		print 'Fill results'
	
	select * from (
	select vlt.Vehicle_ID, vlt.Log_Time, csl.Number, csl.Value
			from #vehicle_log_time vlt
			join dbo.Controller_Sensor_Log csl (nolock) on csl.Vehicle_ID = vlt.Vehicle_ID and csl.Log_Time = vlt.Log_Time
			where csl.Number in (select vs.Number from #vehicle_sensor vs where vs.Vehicle_ID = vlt.Vehicle_ID)	
	union all
		select  nlt.Vehicle_ID
			   ,nlt.Log_Time
			   ,nlt.Number
			   ,csl.Value
			from #neighbour_log_time nlt
			join dbo.Controller_Sensor_Log csl (nolock) on 
					csl.Vehicle_ID = nlt.Vehicle_Id
				and csl.Log_Time   = nlt.Log_Time
				and csl.Number     = nlt.Number
	) t
	order by t.Vehicle_ID, t.Log_Time
	
	drop table #neighbour_log_time	
	drop table #vehicle_sensor
	drop table #vehicle_log_time
end