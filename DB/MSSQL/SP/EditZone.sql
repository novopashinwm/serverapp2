

IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'EditZone' AND type = 'P')
    DROP PROCEDURE EditZone
GO


CREATE PROCEDURE [dbo].[EditZone]  
	@zone_id		int,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@color			int,
	@description	nvarchar(500),
	@type_id		int,
	@radius			float,
	@operator_id	int,
	@debug			bit = 0

AS


---------------------------------------------------
/*
--test
declare 
	@zone_id		int,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@color			int,
	@description	nvarchar(500),
	@type_id		int,
	@radius			float,
	@operator_id	int

set	@zone_id		= 20 
set @vertex			= '37,65:55,74;'
set @name			= '���������'
set @description	= '���������'
set @color			= 1
set @type_id		= 1
set @radius			= 255
set @operator_id	= 137
*/
/*
select * from GEO_ZONE as Z 
	inner join GEO_ZONE_PRIMITIVE as ZP on Z.zone_id = ZP.zone_id
	inner join ZONE_PRIMITIVE as P on ZP.PRIMITIVE_ID = P.PRIMITIVE_ID
	inner join ZONE_PRIMITIVE_VERTEX as PV on P.PRIMITIVE_ID = PV.PRIMITIVE_ID
	inner join MAP_VERTEX as V on PV.VERTEX_ID = V.VERTEX_ID
where Z.zone_id = (select max(zone_id) from GEO_ZONE)
	or Z.zone_id = (select max(zone_id) from GEO_ZONE) - 1
*/

---------------------------------------------------

if isnull(@debug, 0) = 0
	set nocount on
	
	declare 
		@primitive_id	int,
		@vertex_id		int,
		@map_id			int

	set @primitive_id = (select top 1 PRIMITIVE_ID from GEO_ZONE_PRIMITIVE where ZONE_ID = @zone_id)	
	if(@primitive_id is null)
	begin
		if @debug = 1
			print '@primitive_id is null'
		return
	end

	create table #Vertex(	
		--id int identity primary key, 
		Vertex_ID	int
		)
	insert into #Vertex
	select distinct	VERTEX_ID from ZONE_PRIMITIVE_VERTEX where PRIMITIVE_ID = @primitive_id

	set @map_id = (select top 1 MAP_ID from MAP_VERTEX where VERTEX_ID = (select top 1 Vertex_ID from #Vertex) )


	update dbo.GEO_ZONE set [NAME] = @name, [DESCRIPTION] = @description, [COLOR] = @color where ZONE_ID = @zone_id
	update dbo.ZONE_PRIMITIVE set [RADIUS] = @radius

	delete from ZONE_PRIMITIVE_VERTEX where PRIMITIVE_ID = @primitive_id 
	delete from MAP_VERTEX where VERTEX_ID in (select Vertex_ID from #Vertex) 


	declare 
		@order		int,
		@vertex1	varchar(100),
 		@x1			float,
 		@y1			float

	--
	set @vertex = replace(@vertex, ',', '.')

	set @order = 0
	while (len(@vertex) > 1 and charindex(';', @vertex) > 1) 
	begin
		set @order = @order + 1
		
		-- ��������� ��������� ������ ����� � �� �����������
		set @vertex1 = substring(@vertex, 1, (charindex(';', @vertex) - 1))
		set @x1 = convert(float, substring(@vertex1, 1, (charindex(':', @vertex1) - 1))) 
		set @vertex1 = substring(@vertex1, (charindex(':', @vertex1) + 1), len(@vertex1))
		set @y1 = convert(float, @vertex1) 

		--������ ���������� ��������	
		insert dbo.MAP_VERTEX ([MAP_ID], [X], [Y], [VISIBLE], [ENABLE]) values (@map_id, @x1, @y1, 'true', 'true') 
		set @vertex_id = (select @@identity)


		insert dbo.ZONE_PRIMITIVE_VERTEX ([PRIMITIVE_ID], [VERTEX_ID], [ORDER]) values (@primitive_id, @vertex_id, @order) 
		--set @primitive_vertex_id = (select @@identity)

	 
		set @vertex = substring(@vertex, (charindex(';', @vertex) + 1), len(@vertex))
	end


	drop table #Vertex

