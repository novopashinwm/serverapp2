if not exists (select * from sys.table_types where name = 'Vehicle_Zone_Position_Param')
	create type Vehicle_Zone_Position_Param as table 
	(
		vehicle_id int, 
		zone_id int,
		log_time int,
		lat float,
		lng float
	)
go
if exists (select * from sys.procedures where name = 'GetGeoZonesForCircleType')
	drop procedure GetGeoZonesForCircleType
go
create procedure GetGeoZonesForCircleType
(
	@zoneIntersection Vehicle_Zone_Position_Param readonly
) 
as
begin
	select z.zone_id, z.vehicle_id, z.log_time
	from @zoneIntersection z
	where exists 
		(
			select *
			from GEO_ZONE_PRIMITIVE zp 
				join ZONE_PRIMITIVE p on zp.PRIMITIVE_ID = p.PRIMITIVE_ID
				join ZONE_PRIMITIVE_VERTEX v on v.PRIMITIVE_ID = p.PRIMITIVE_ID
				join MAP_VERTEX mv on mv.VERTEX_ID = v.VERTEX_ID
			where p.RADIUS >= dbo.GetTwoGeoPointsDistance(z.Lng, z.Lat, mv.X, mv.Y)
				and z.ZONE_ID = zp.ZONE_ID
		)
end
go