IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DelZoneMatrix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DelZoneMatrix]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DelZoneMatrix]  
	@zone_id			int
AS
begin
	delete from dbo.ZONE_MATRIX where zone_id = @zone_id;
end


