﻿IF (OBJECT_ID(N'[dbo].[DeleteVehicle]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[DeleteVehicle];
GO

CREATE PROCEDURE [dbo].[DeleteVehicle] --30599
(
	@vehicle_id int
)
AS
BEGIN
	IF (@vehicle_id IS NULL)
		RETURN;

	SET NOCOUNT           ON;
	SET QUOTED_IDENTIFIER ON;
	
	BEGIN TRY
		BEGIN TRAN
		------------------------------------------------------------------
		DELETE FROM [dbo].VEHICLE_PROFILE        WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].OPERATORGROUP_VEHICLE  WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[VEHICLEGROUP_VEHICLE] WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[VEHICLE_PICTURE]      WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[ZONE_VEHICLE]         WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[VEHICLE_RULE]         WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[MLP_Controller]       WHERE [Controller_ID] IN
		(
			SELECT [CONTROLLER_ID]
			FROM [dbo].[CONTROLLER]
			WHERE @vehicle_id = [VEHICLE_ID]
		)
		DELETE FROM csm
		FROM [dbo].[CONTROLLER_SENSOR_MAP] csm
			JOIN [dbo].[CONTROLLER] c
				ON c.[CONTROLLER_ID] = csm.[CONTROLLER_ID]
		WHERE @vehicle_id = c.[VEHICLE_ID]

		DELETE FROM [dbo].[CONTROLLER]           WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[BRIGADE_DRIVER]       WHERE BRIGADE IN (SELECT [BRIGADE_ID] FROM [dbo].[BRIGADE] WHERE @vehicle_id = vehicle)
		DELETE FROM [dbo].[BRIGADE]              WHERE @vehicle_id = [VEHICLE]
		DELETE FROM [dbo].[OPERATOR_VEHICLE]     WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[VEHICLE_OWNER]        WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[WB_TRIP]              WHERE WAYBILL_HEADER_ID IN (SELECT [WAYBILL_HEADER_ID] FROM [dbo].[WAYBILL_HEADER] WHERE @vehicle_id = vehicle_id)
		DELETE FROM [dbo].[WAYBILL_HEADER]       WHERE @vehicle_id = [VEHICLE_ID]
		DELETE FROM [dbo].[Vehicle_Log_Compress] WHERE @vehicle_id = [Vehicle_ID]

		DELETE FROM cp
		FROM [dbo].[Command] c
			JOIN [dbo].[Command_Parameter] cp
				ON cp.[Command_ID] = c.[ID]
			WHERE @vehicle_id = c.[Target_ID]
		DELETE FROM rsi
		FROM [dbo].[Command] c
			JOIN [dbo].[RenderedServiceItem] rsi
				ON rsi.[Command_ID] = c.[ID]
			WHERE @vehicle_id = c.[Target_ID]
		DELETE FROM c
		FROM [dbo].[Command] c
		WHERE @vehicle_id = c.[Target_ID]

		DELETE FROM [dbo].[RenderedServiceItem]     WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM [dbo].[CompoundRule_Vehicle]    WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM [dbo].[Vehicle_Rule_Processing] WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM [dbo].[SensorValueChange]       WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM [dbo].[VehicleControlDate]      WHERE @vehicle_id = [Vehicle_ID]
		DECLARE @bssq [dbo].Id_Param
		INSERT INTO @bssq
		SELECT
			bssq.[SchedulerQueue_ID]
		FROM [dbo].[Billing_Service] bs
			JOIN [dbo].[Billing_Service_SchedulerQueue] bssq
				ON bssq.[Billing_Service_ID] = bs.[ID]
		WHERE @vehicle_id = bs.[Vehicle_ID]
		DELETE FROM bssq
		FROM @bssq id
			JOIN [dbo].[Billing_Service_SchedulerQueue] bssq
				ON bssq.[SchedulerQueue_ID] = id.[Id]
		DELETE FROM sq
		FROM @bssq id
			JOIN [dbo].[SCHEDULERQUEUE] sq
				ON sq.[SCHEDULERQUEUE_ID] = id.[Id]
		DELETE FROM [dbo].[Billing_Service] WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM rs
		FROM [dbo].[Rendered_Service] rs
			JOIN [dbo].[Billing_Service] bs
				ON bs.[id] = rs.[Billing_Service_ID]
		WHERE @vehicle_id = bs.Vehicle_ID
		DELETE FROM [dbo].[Billing_Service]    WHERE @vehicle_id = [Vehicle_ID]
		DELETE FROM [dbo].[VEHICLE_SHARE_LINK] WHERE @vehicle_id = [VEHICLE_SHARE_LINK_VEHICLE_ID]
		DELETE FROM [dbo].[VEHICLE]            WHERE @vehicle_id = [VEHICLE_ID]
		------------------------------------------------------------------
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
	END CATCH
END