/****** Object:  StoredProcedure [dbo].[TripsStatisticsByRoutesGet]    Script Date: 07/14/2010 18:45:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[TripsStatisticsByRoutesGet]
	@ReportDateBegin	datetime,
	@ReportDateEnd		datetime,
	@operator_id int = null
AS
-- test
--declare @ReportDateBegin	datetime
--declare @ReportDateEnd		datetime
--set @ReportDateBegin = '2006-10-25 20:00:00'
--set @ReportDateEnd = '2006-10-28 20:00:00'

	-- °Її¤к°п°·Ёк■иv Є╕ї┐ ¤║Ў¤v┐  ║кїЄv┐ √°╕к■Є
	select distinct waybill_header_id
	into #whids
	from waybill_header
	left join v_operator_vehicle_right rr on rr.vehicle_id = waybill_header.VEHICLE_ID
	where WAYBILL_DATE between dateadd(hour, -1, @ReportDateBegin) and dateadd(hour, 1, @ReportDateEnd)
		and cancelled = 0
		and (@operator_id is null or rr.operator_id = @operator_id)

	select *
	into #closed_wts
	from wb_trip
	where waybill_header_id in (select * from #whids)
		and waybill_id is not null

	-- °Її¤к°п°·Ёк■иv ўЁ·иvкv┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а
	select distinct w.waybill_date as day_date, t.*
	into #closed_ts
	from #closed_wts wt
		join trip t
			on wt.trip_id = t.trip_id
		join waybill w
			on wt.waybill_id = w.waybill_id

	drop table #whids

	-- ¤Ё░Ё ЇЁкЁ Є ёїў■к¤■╕°кї√╣¤■№ Є°Її
	declare @dtBegin datetime
	declare @dtEnd datetime
	set @dtBegin = dateadd(hh, 24-datepart(hh, @ReportDateBegin), @ReportDateBegin)
	set @dtEnd = dateadd(hh, 24-datepart(hh, @ReportDateEnd), @ReportDateEnd)

	-- кЁё√°бЁ ╕■■кЄїк╕кЄ°а №Ёи░и║к■Є ° Є°Ї■Є Ї¤а
	declare @dt datetime
	set @dt = @dtBegin

	select @dt as day_date, route_id, route_id as day_kind_id
	into #r_dk
	from route
	where 0 = 1

	while @dt <= @dtEnd
	begin
		insert into #r_dk
		select distinct @dt, r.route_id, isnull(dbo.DayKind(@dt, r.route_id), 0)
		from route r
		left join v_operator_route_right rr on rr.route_id = r.ROUTE_ID
		where (@operator_id is null or rr.operator_id = @operator_id)

		set @dt = @dt+1
	end

	-- °Її¤к°п°·Ёк■иv Є╕ї┐ Ё·к║Ё√╣¤v┐ Єv┐■Ї■Є
	select rd.day_date, s.schedule_id
	into #filtered_sids
	from schedule s
		join #r_dk rd
			on s.route_id = rd.route_id
				and s.day_kind_id = rd.day_kind_id

	drop table #r_dk

	-- °Її¤к°п°·Ёк■иv Є╕ї┐ Ё·к║Ё√╣¤v┐ ╕№ї¤ °ў иЁ╕ °╕Ё¤°а
	select fs.day_date, sd.schedule_detail_id
	into #filtered_sdids
	from schedule_detail sd 
		join #filtered_sids fs
			on sd.schedule_id = fs.schedule_id

	drop table #filtered_sids

	-- °Її¤к°п°·Ёк■иv Є╕ї┐ Ё·к║Ё√╣¤v┐ иї∙╕■Є °ў иЁ╕ °╕Ё¤°а
	select fsd.day_date, t.trip_id
	into #filtered_tids
	from trip t
		join #filtered_sdids fsd
			on t.schedule_detail_id = fsd.schedule_detail_id

	drop table #filtered_sdids

	-- ¤їўЁ·иvкvї иї∙╕v °ў иЁ╕ °╕Ё¤°а
	select ft.day_date, t.*
	into #unclosed_ts
	from trip t
		join #filtered_tids ft
			on t.trip_id = ft.trip_id

	delete #unclosed_ts
	from #unclosed_ts ut
		join #closed_ts ct
			on ut.day_date = ct.day_date
				and ut.trip_id = ct.trip_id

	drop table #filtered_tids

	select t.*, s.route_id
	into #target_ts
	from #unclosed_ts t
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id

	insert into #target_ts
	select t.*, s.route_id
	from #closed_ts t
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id

	drop table #closed_ts

	select r.ext_number as route_number, count(*) as trips_plan, 0 as trips_fact, 0 as trips_undone, 0 as trips_defect, 0 as trips_skip, 0e+0 as percent_fact
	into #result_table
	from #target_ts tt
		join route r
			on tt.route_id = r.route_id
	where tt.trip_kind_id in (5, 6)
	group by tt.route_id, r.ext_number
	order by r.ext_number

	drop table #target_ts

	select r.ext_number as route_number, count(*) as trips_fact
	into #result_fact
	from #closed_wts wt
		join trip t
			on wt.trip_id = t.trip_id
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id
		join route r
			on s.route_id = r.route_id
	where wt.trip_kind_id in (5, 6)
		and (wt.status = 0		-- ўЁ √Ё¤°и■ЄЁ¤
		or (wt.status&0x01) = 0x01	-- Єv ■√¤ї¤
		or (wt.status&0x02) = 0x02	-- Єv ■√¤ї¤ ╕ ёиЁ·■№
		or (wt.status&0x04) = 0x04)	-- ёиЁ·
	group by r.ext_number

	update #result_table
	set trips_fact = rf.trips_fact
	from #result_table rt, #result_fact rf
	where rt.route_number = rf.route_number

	drop table #result_fact

	select r.ext_number as route_number, count(*) as trips_undone
	into #result_undone_closed
	from #closed_wts wt
		join trip t
			on wt.trip_id = t.trip_id
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id
		join route r
			on s.route_id = r.route_id
	where wt.trip_kind_id in (5, 6)
		and ((wt.status&0x08) = 0x08	-- ¤ї Єv ■√¤ї¤
		or (wt.status&0x10) = 0x10)	--  и■ ║Хї¤ °ў-ўЁ ўЁк■иЁ
	group by r.ext_number

	select r.ext_number as route_number, count(*) as trips_undone
	into #result_undone_unclosed
	from #unclosed_ts t
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id
		join route r
			on s.route_id = r.route_id
	where t.trip_kind_id in (5, 6)
	group by r.ext_number

	drop table #unclosed_ts

	update #result_table
	set trips_undone = ruc.trips_undone
	from #result_table rt, #result_undone_closed ruc
	where rt.route_number = ruc.route_number

	drop table #result_undone_closed

	update #result_table
	set trips_undone = rt.trips_undone+ruu.trips_undone
	from #result_table rt, #result_undone_unclosed ruu
	where rt.route_number = ruu.route_number

	drop table #result_undone_unclosed

	select r.ext_number as route_number, count(*) as trips_defect
	into #result_defect
	from #closed_wts wt
		join trip t
			on wt.trip_id = t.trip_id
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id
		join route r
			on s.route_id = r.route_id
	where wt.trip_kind_id in (5, 6)
		and ((wt.status&0x02) = 0x02	-- Єv ■√¤ї¤ ╕ ёиЁ·■№
		or (wt.status&0x04) = 0x04)	-- ёиЁ·
	group by r.ext_number

	update #result_table
	set trips_defect = rd.trips_defect
	from #result_table rt, #result_defect rd
	where rt.route_number = rd.route_number

	drop table #result_defect

	select r.ext_number as route_number, count(*) as trips_skip
	into #result_skip
	from #closed_wts wt
		join trip t
			on wt.trip_id = t.trip_id
		join schedule_detail sd
			on t.schedule_detail_id = sd.schedule_detail_id
		join schedule s
			on sd.schedule_id = s.schedule_id
		join route r
			on s.route_id = r.route_id
	where wt.trip_kind_id in (5, 6)
		and (wt.status&0x10) = 0x10	--  и■ ║Хї¤ °ў-ўЁ ўЁк■иЁ
	group by r.ext_number

	drop table #closed_wts

	update #result_table
	set trips_skip = rs.trips_skip
	from #result_table rt, #result_skip rs
	where rt.route_number = rs.route_number

	drop table #result_skip

	update #result_table
	set percent_fact = trips_fact*1e+2/trips_plan
	where trips_plan > 0

	select *
	from #result_table

	drop table #result_table

