IF EXISTS (
  SELECT * 
    FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE SPECIFIC_SCHEMA = N'dbo'
     AND SPECIFIC_NAME = N'GetSubscribers' 
)
   DROP PROCEDURE [dbo].[GetSubscribers]
GO

CREATE PROCEDURE [dbo].[GetSubscribers] 
	@schedulerQueueID int
AS
begin
	
	declare @c int
	set @c = (select count(*) from dbo.EMAIL_SCHEDULERQUEUE where SCHEDULERQUEUE_ID = @schedulerQueueID)

	if @c = 0
		begin
			--Не знаю зачем это сделано, но при редактировании email он видимо удаляется из EMAIL_SCHEDULERQUEUE и всем рассылается отчет ненужный им
			select EMAIL, [NAME] FROM EMAIL where 0 = 1;
		end
	else
		begin
			select e.EMAIL, e.[NAME] FROM EMAIL e
				join dbo.EMAIL_SCHEDULERQUEUE es on es.EMAIL_ID = e.EMAIL_ID
				where es.SCHEDULERQUEUE_ID = @schedulerQueueID
		end
end









