CreateProcedure 'GetMoveHistoryDistance'
go

-----------------------------------------------------------------------------------
--����������: ������� ������� ��� �������� �� �� �������� ��������� ��������
--�����: ����������� �.�.
-----------------------------------------------------------------------------------

alter PROCEDURE dbo.GetMoveHistoryDistance
	@time_from DateTime, 
	@time_to DateTime,  
	@vehicle char(40) 
AS

-----------------------------------------------------------------------------------
/*
-- Test
declare 
	@time_from DateTime, 
	@time_to DateTime,  
	@vehicle char(40) 

set @vehicle = '6625'  
set @time_from = '2006-09-05 12:00:00'
set @time_to = '2006-09-06 12:00:00'

set @vehicle = '2088' 
set @time_from = '2008-10-21 04:00:00:000'
set @time_to = '2008-10-21 05:30:00:000'

--�������� ������ �� ������� � ������� ��������
--�� 6619 �������� 21:00 28.11.2006 - 06:00 29.11.2006
--exec DBO.GETMOVEHISTORYDISTANCE @time_from = '10/19/2006 14:00:00', @time_to = '10/19/2006 16:00:00', @vehicle = '4400'
*/
-----------------------------------------------------------------------------------

Declare 
	@vehicle_ID int,
	@time_from_int int,
	@time_to_int int

Set @vehicle_ID = (select Top 1 Vehicle_ID from vehicle where Garage_Number = @vehicle)
Set @time_from_int = datediff(s, '1970', @time_from)
Set @time_to_int = datediff(s, '1970', @time_to)


-- ��������� ������� �� ������
create table #MonitoreeLog(	
	id int identity primary key, 
	Monitoree_ID 	int,
	Log_Time		int,
	X				float,
	Y				float,
	Speed 			int,
	Dist			float,
	GroupNum 		int,
	Data 			int
	,Satellites		int
	)

-- �������� ��� ������� �� �� ��������
insert into #MonitoreeLog
select 	
	Monitoree_ID, 
	Log_Time, 
	X,
	Y,
	Speed, 
	Convert(float, 0), 
	0, 
	1
	,Satellites
from Monitoree_log (nolock)
Where 	
	Monitoree_ID = @vehicle_ID
	and Log_Time between @time_from_int and @time_to_int 
	-- ������ ������� �� ��������
	--and X < 180 and Y < 180
order by 
	Log_Time

update #MonitoreeLog set X = 181, Y = 181 where Satellites < 3 or (X = 0 or Y = 0)


-- ������������ ���������� ������ (������� ������ � ���������� �� ��������)
Declare 
	@max_id		int,
	@i			int,
	@i_			int,
	@log_time	int,
	@log_time_	int,
	@x			float,
	@x_			float,
	@y			float,
	@y_			float,
	@dist		float,
	@data		int,
	@data_		int,
	@GroupNum	int,
	--
	@speed			int,
	@speed_			int,
	@timeBetween	int			
 
set	@max_id = (select max(id) from #MonitoreeLog)
set	@i = 1
set	@GroupNum = 0

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @log_time = (select Log_Time from #MonitoreeLog where id = @i)
	set	@x = (select X from #MonitoreeLog where id = @i) 			
	set	@y = (select Y from #MonitoreeLog where id = @i)			
	set	@data = (select Data from #MonitoreeLog where id = @i)	
	set	@log_time_ = (select Log_Time from #MonitoreeLog where id = @i_)
	set	@x_	= (select X from #MonitoreeLog where id = @i_)		
	set	@y_	= (select Y from #MonitoreeLog where id = @i_)	
	set	@data_	= (select Data from #MonitoreeLog where id = @i_)

	set @speed = (select Speed from #MonitoreeLog where id = @i)
	set @speed_ = (select Speed from #MonitoreeLog where id = @i_)
	set @timeBetween = @log_time - @log_time_
		
	--�������� ��������� ��� ������ ������� (Data=1 - ���� ���������� �������; Data=2 - ���� ������ ���������� GPS)
	if(@i_ = 1 and (@x_ > 180 or @y_ > 180))
	begin
		Set @data_ = 2
		update #MonitoreeLog Set Data = @data_ where id = @i_
	end

	-- ������ ����� �������. �� �������, ���� ���������� ������ --� ���� �������� ������� (������ �����)
	if (@x > 180 or @y > 180 or @x_ > 180 or @y_ > 180)
		Set @dist = 0 
	else
		Set @dist = SQRT(SQUARE(63166*(@x_-38)-63166*(@x-38)) + SQUARE(111411*(@y_-55.665)-111411*(@y-55.665))) / 1000

		-- 
		if(@speed_ < 1)
		begin
			if(@speed < 1)
				if(@timeBetween < 0.02 and @dist < 0.02)
					Set @dist = 0
		end

	--���� ������� ������ � ������� 60 ���
	if(@log_time - @log_time_ <= 60)
	begin
		--������� "�������" ������� (Data=1 - ���� ���������� �������; Data=2 - ���� ������ ���������� GPS)
		set @data = 1
		if (@x > 180 or @y > 180) 
			set @data = 2
	end
	else
	begin
		--������� ���������� ������� (Data=0 - ���������� GSM)
		set @data = 0
	end

	--�������� ������
	if(@data <> @data_)
		Set @GroupNum = @GroupNum + 1

	--��������� ������
    update #MonitoreeLog Set Dist = @dist, GroupNum = @GroupNum, Data = @data where id = @i
end

--test
--select *, ) from #MonitoreeLog
--select sum(Dist) from #MonitoreeLog


--������������ ��������
select 
	'From' = Convert(char(10), Min(m_log.Log_Time)), 
	'To' = Convert(char(10), Max(m_log.Log_Time))
	--,dateadd(s, Min(m_log.Log_Time), '1970') 
	--,dateadd(s, Max(m_log.Log_Time), '1970')
from 
	#MonitoreeLog m_log


--�������� � �������� �������
select distinct 
	WBT.BEGIN_TIME,
	WBT.END_TIME,
	WBH.EXT_NUMBER as WBHNUM,
	V.VEHICLE_ID,
	V.GARAGE_NUMBER,
	V.PUBLIC_NUMBER,
	W.WAYBILL_ID,
	DR.SHORT_NAME,
	DR.EXT_NUMBER
into 
	#wb
from 
	WB_TRIP as WBT
	left join WAYBILL_HEADER as WBH on WBT.WAYBILL_HEADER_ID = WBH.WAYBILL_HEADER_ID
	left join VEHICLE as V on WBH.VEHICLE_ID = V.VEHICLE_ID
	left join WAYBILL as W on WBT.WAYBILL_ID = W.WAYBILL_ID
	left join DRIVER as DR on W.DRIVER_ID = DR.DRIVER_ID
where 
	V.VEHICLE_ID = @vehicle_ID
	and WBT.BEGIN_TIME <= @time_to and WBT.END_TIME >= @time_from

select Distinct 
	m_log.Log_Time, 
	wb.Waybill_ID, 
	wb.Garage_Number, 
	wb.Public_Number, 
	wb.Short_Name, 
	wb.Ext_number, 
	wb.WBHNUM as WaybillExtNumber 
into 
	#Drivers
from 
	#MonitoreeLog m_log
	inner join #wb as wb on wb.vehicle_ID = m_log.Monitoree_ID 
		--and dateadd(s, m_log.Log_Time, '1970') between wb.Begin_Time and wb.End_Time
		and m_log.Log_Time between datediff(s, '1970', wb.Begin_Time) and datediff(s, '1970', wb.End_Time)

--������ ��������� �������� �� ������ �� � �������� ��������
select Distinct 
	temptb.Garage_Number, 
	temptb.Public_Number, 
	temptb.Short_Name, 
	temptb.Ext_number
from 
	#Drivers temptb


--������� ������� � ��������� � ����������
create table #Positions(	
	id int identity primary key, 
	Monitoree_ID 		int,
	Log_Time			int,
	Speed 				int,
	Dist				float,
	GroupNum 			int,
	Data 				int,
	Garage_Number		varchar(50), 
	Public_Number		varchar(100), 
	Waybill_ID			int,  
	Short_Name			varchar(100), 
	Ext_number			varchar(100), 
	[Group]				int,
	WaybillExtNumber	varchar(100),
	TEMPERATURE			int
	)

insert into #Positions
select	
	m_log.Monitoree_ID, 
	m_log.Log_Time, 
	m_log.Speed, 
	m_log.Dist, 
	m_log.GroupNum, 
	m_log.Data, 
	v.Garage_Number, 
	v.Public_Number, 
	temptb.Waybill_ID,  
	temptb.Short_Name, 
	temptb.Ext_number, 
	0,
	temptb.WaybillExtNumber,
	null 
from 
	#MonitoreeLog m_log
	Inner Join vehicle v on v.vehicle_ID = m_log.Monitoree_ID
	Left Join #Drivers temptb on temptb.Log_Time = m_log.Log_Time
Order by 
	m_log.Log_Time

update #Positions set TEMPERATURE = (select top 1 T.V1 from #Temper as T where T.LOG_TIME = #Positions.Log_Time)

-- ����������
Declare 
	@waybillID		int,
	@waybillID_		int

set	@max_id = (select max(id) from #Positions)
set	@i = 1
set	@GroupNum = 0

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @waybillID = (select Waybill_ID from #Positions where id = @i)
	set	@waybillID_ = (select Waybill_ID from #Positions where id = @i_)

	if (@waybillID <> @waybillID_ or (@waybillID is null and @waybillID_ is not null) or (@waybillID is not null and @waybillID_ is null))
		Set @GroupNum = @GroupNum + 1
	
    update #Positions Set [Group] = @GroupNum where id = @i
end


--�������������� ����� ������
select	
	v.Garage_Number, 
	v.Public_Number, 
	temptb.Waybill_ID, 
	temptb.WaybillExtNumber,
	temptb.Short_Name, 
	temptb.Ext_number, 
	'From' = Convert(char(10), Min(m_log.Log_Time)), dateadd(s, Min(m_log.Log_Time), '1970') dtFrom, 
	'To' = Convert(char(10), Max(m_log.Log_Time)), dateadd(s, Max(m_log.Log_Time), '1970') dtTo, 
	'Speed' = Avg(cast(m_log.Speed as decimal(38, 2))), 
	'Dist' = Sum(m_log.Dist), 
	'TEMPERATURE' = Avg(TEMPERATURE),
	m_log.GroupNum Flag, 
	m_log.Data, 
	m_log.[Group]
into
	#Result
from 
	#Positions m_log
	Inner Join vehicle v on v.vehicle_ID = m_log.Monitoree_ID
	Left Join #Drivers temptb on temptb.Log_Time = m_log.Log_Time
Group by 
	v.Garage_Number, 
	v.Public_Number, 
	temptb.Waybill_ID, 
	temptb.WaybillExtNumber,
	temptb.Short_Name, 
	temptb.Ext_number,  
	m_log.GroupNum, 
	m_log.Data, 
	m_log.[Group] 
Order by 
	Min(m_log.Log_Time), Max(m_log.Log_Time)

select --* 
	Waybill_ID, 
	Garage_Number, 
	Public_Number, 
	Short_Name, 
	Ext_number, 
	[From],
	[To],
	Speed, 
	Dist, 
	Flag, 
	Data, 
	[Group],
	WaybillExtNumber
	,TEMPERATURE
from #Result


drop table #MonitoreeLog
drop table #wb
drop table #Drivers
drop table #Temper
drop table #Positions
drop table #Result


GO
