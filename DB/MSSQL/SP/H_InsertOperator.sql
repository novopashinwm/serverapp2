IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_InsertOperator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[H_InsertOperator]

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[H_InsertOperator]
    @TRAIL_ID int, 
    @NAME nvarchar (500), 
    @LOGIN nvarchar (60), 
    @PASSWORD nvarchar (100), 
    @PHONE nvarchar (40), 
    @EMAIL nvarchar (100), 
    @GUID uniqueidentifier
AS
BEGIN
    insert into [dbo].[OPERATOR] ([NAME], 
        [LOGIN], 
        [PASSWORD], 
        --[PHONE], 
        [EMAIL], 
        [GUID]
    ) VALUES (@NAME,@LOGIN,@PASSWORD, /*@PHONE, */ @EMAIL,@GUID)
    declare @OPERATOR_ID int
    SELECT @OPERATOR_ID=SCOPE_IDENTITY()
    SELECT @OPERATOR_ID as 'OPERATOR_ID'
    
    insert into [dbo].[H_OPERATOR] (TRAIL_ID, ACTION, ACTUAL_TIME, [OPERATOR_ID], 
        [NAME], 
        [LOGIN], 
        [PASSWORD], 
        --[PHONE], 
        [EMAIL], 
        [GUID]
    ) VALUES (@TRAIL_ID, 'INSERT', GETUTCDATE(), @OPERATOR_ID, 
        @NAME, 
        @LOGIN, 
        @PASSWORD, 
        --@PHONE, 
        @EMAIL, 
        @GUID
    )
END



GO


