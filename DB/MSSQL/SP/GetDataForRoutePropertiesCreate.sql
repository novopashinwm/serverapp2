/****** Object:  StoredProcedure [dbo].[GetDataForRoutePropertiesCreate]    Script Date: 07/07/2010 16:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[GetDataForRoutePropertiesCreate]
	@operatorID int = null
AS
	select distinct p.* 
	from point p 
	join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
	join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where @operatorID is null
	or gt.CREATOR_OPERATOR_ID = @operatorID
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operatorId)

	select distinct dk.* 
	from [DAY_KIND] dk
	join [DK_SET] dks on dks.DK_SET_ID = dk.DK_SET
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = dks.DEPARTMENT_ID
	where @operatorID is null or dr.operator_id = @operatorID

	
	select * from [POINT_KIND]
	
	select distinct g.* 
	from [DK_SET] g
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = g.DEPARTMENT_ID
	where @operatorID is null or dr.operator_id = @operatorID

	select * from [TRIP_KIND]
	
	select distinct g.* 
	from [GRAPHIC] g
	left join v_operator_department_right dr on dr.DEPARTMENT_ID = g.DEPARTMENT_ID
	where @operatorID is null or dr.operator_id = @operatorID









