﻿IF (OBJECT_ID(N'[dbo].[State_Log#Lasts_Update]') IS NOT NULL)
	DROP PROCEDURE [dbo].[State_Log#Lasts_Update];
GO

CREATE PROCEDURE [dbo].[State_Log#Lasts_Update]
(
	@vehicle_id int,
	@type       tinyint,
	@log_time   int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@rows   int = 0;

	UPDATE [dbo].[State_Log#Lasts]
	SET [Log_Time#Last] =
		CASE
			WHEN [Log_Time#Last] < @log_time
			THEN @log_time
			ELSE [Log_Time#Last]
		END
	WHERE [VEHICLE_ID] = @vehicle_id
	AND   [Type]       = @type
	SELECT @rows = @@ROWCOUNT

	IF (0 = @rows)
	BEGIN
		INSERT INTO [dbo].[State_Log#Lasts] ([Vehicle_ID], [Type], [Log_Time#Last])
		VALUES                              (@vehicle_id,  @type,  @log_time)
	END
END
GO