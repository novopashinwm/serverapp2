if exists (
	select *
		from sys.views 
		where name = 'v_controller_type_command'
)
	drop view v_controller_type_command
go

create view v_controller_type_command
as
select ct.CONTROLLER_TYPE_ID, ct.Type_Name, cmd.id, cmd.code
	from CONTROLLER_TYPE ct
	join Controller_Type_CommandTypes ct_ct on ct_ct.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join CommandTypes cmd on cmd.id = ct_ct.CommandTypes_ID
