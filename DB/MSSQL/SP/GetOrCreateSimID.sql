if exists (select * from sys.objects where name = 'GetOrCreateSimID')
	drop procedure GetOrCreateSimID;
go

set QUOTED_IDENTIFIER on
go

create procedure GetOrCreateSimID
(
	@asid varchar(255)
)
as
begin

	set nocount on

	declare @contactId table (value int)
	insert into @contactId
		exec dbo.GetContactID 3, @asid

	declare @asidId int = (select a.ID from Asid a where a.Contact_ID = (select Value from @contactId))

	if (@asidId is null)
	begin
		insert into Asid (Contact_ID, AllowMlpRequest, AllowSecurityAdministration, WarnAboutLocation, SimID)
			select contactId.value, 1, 1, 0, replace(convert(varchar(36), NEWID()), '-', '')
				from @contactId contactId
				where not exists (select * from Asid a with (XLOCK, SERIALIZABLE) where a.Contact_ID = contactId.value)
				
		if (@@ROWCOUNT <> 0)
			set @asidId = @@IDENTITY
		else
			set @asidId = (select a.ID from Asid a where a.Contact_ID = (select Value from @contactId))
	end	

	exec dbo.GetOrCreateSimIDByAsidId @asidId
	  
end