/****** Object:  StoredProcedure [dbo].[ReportCommercialServices]    Script Date: 03.08.2021 16:26:32 ******/
IF (OBJECT_ID(N'[dbo].[ReportCommercialServices]') IS NOT NULL)
  DROP PROCEDURE [dbo].[ReportCommercialServices];
GO
-- EXEC ReportCommercialServices @operator_id = 28549, @from = '2021-08-01 00:00:000', @to = '2021-08-31 23:59:000'
/****** Object:  StoredProcedure [dbo].[ReportCommercialServices]    Script Date: 03.08.2021 16:26:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ReportCommercialServices]
(
	@operator_id int,
	@from        datetime = null,
	@to          datetime = null,
	@country     nvarchar(255) = NULL
)
AS
BEGIN


			select 
			Country = co.Name,
			MacroRegion = ISNULL(oodr.name, '������') ,
			CustomerName = d.name,
			ContractNumber = isnull(d.ExtID,'N/A'),
			Terminal_Device_Number = v.GARAGE_NUMBER, -- ci.Device_ID + ISNULL(' (' + ci.Phone + ')', ''),
			ServiceTypeCategory = 'GNSS', /*���� ��� ����������� �����, �� ���������� 'GNSS'*/
			ServiceName = 'GEOLOCATION',

			StartDate = min (sl.date_from), /*���� ������ �� ������� @day*/
			EndDate = max (sl.date_to), /*���� ����� �� ������� @day*/
			RenderedCount =  count(*),  /*���������� ��� �������� ����������*/
			LbsPositionCount = 0,
			LbsPositionRequested = 0,
			RenderedLastTime = max (sl.date_to)
			, totalLogTimes = SUM (sl.hits)
				
		FROM DEPARTMENT d
			inner join vehicle v on d.DEPARTMENT_ID = v.DEPARTMENT
			inner join Country co on co.Country_ID = d.Country_ID
			left join REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS sl on sl.VEHICLE_ID = v.VEHICLE_ID

			OUTER APPLY
			(
				SELECT distinct o.name
				FROM v_operator_department_right odr 
					inner join OPERATOR o on o.OPERATOR_ID = odr.operator_id
				WHERE odr.department_id = d.DEPARTMENT_ID
					AND odr.right_id in (2, 110) /*ServiceManagement*/
					and o.operator_id != 28549
			) as oodr
		WHERE 1 = 1
			AND sl.DATE_FROM between @from and @to
		GROUP BY co.Name, v.GARAGE_NUMBER, d.name, d.ExtID, oodr.name
		ORDER BY Country, MacroRegion, CustomerName, v.GARAGE_NUMBER, StartDate
--			
END
GO


