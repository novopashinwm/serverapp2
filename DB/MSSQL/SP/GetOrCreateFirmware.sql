if exists (
	select * from sys.objects where name = 'GetOrCreateFirmware'
)
	drop procedure GetOrCreateFirmware;
go

create procedure GetOrCreateFirmware
(
	@name varchar(32)
)
as
begin

	declare @id int = (select id from Firmware where name = @name)
	
	if @id is null
	begin
	
		insert into Firmware (Name)
			select	@name
				where not exists (select 1 from Firmware with (XLOCK, SERIALIZABLE) where Name = @name)
				
		set @id = @@IDENTITY
	
	end
	
	if @id is null
		set @id = (select id from Firmware where name = @name)
		
	return @id
	
end