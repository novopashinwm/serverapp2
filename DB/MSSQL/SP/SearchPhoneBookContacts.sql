if exists (select * from sys.procedures where name='SearchPhoneBookContacts')
	drop procedure SearchPhoneBookContacts
go
create procedure SearchPhoneBookContacts(
	@operatorId int,
	@msisdns dbo.Msisdn readonly
) as
	declare @phoneBook xml = 
	(
		select PhoneBook
		from Operator_PhoneBook b
		where b.OperatorId = @operatorId
	)

	select 
		coalesce(t.c.value('Name[1]', 'varchar(80)'),'') Name, 
		coalesce(t.c.value('Value[1]', 'varchar(80)'),'') Value
	from @phoneBook.nodes('//contact') t(c)
	where not exists (select * from @msisdns)
		or t.c.value('Value[1]', 'varchar(80)') in (select value from @msisdns)
go