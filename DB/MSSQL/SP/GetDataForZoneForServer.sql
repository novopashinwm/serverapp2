IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDataForZoneForServer]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetDataForZoneForServer] 

GO

CREATE PROCEDURE dbo.GetDataForZoneForServer
AS

select * from MAPS
select * from MAP_VERTEX
select * from ZONE_PRIMITIVE_VERTEX 
select * from ZONE_PRIMITIVE
select * from GEO_ZONE_PRIMITIVE
select * from GEO_ZONE
select * from ZONE_VEHICLE 
select * from ZONE_VEHICLEGROUP

GO

