IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetZoneDistanceStandarts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetZoneDistanceStandarts]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetZoneDistanceStandarts]  
	@operatorID int
AS
BEGIN
	CREATE TABLE #Temp(ZONE_ID int primary key, [NAME] nvarchar(100), ZONE_TYPE_ID int, 
	TYPE_NAME nvarchar(100), COLOR int, DESCRIPTION nvarchar(100), 	Editable bit )

	insert into #Temp
	EXEC [dbo].[GetZone]
			@operator_id = @operatorID,
			@vertexGet = NULL,
			@zone_id = NULL

	select  zone_from 'zone_id_from', zone_to 'zone_id_to', t1.[name] 'zone_name_from', t2.[name] 'zone_name_to', distance
	from dbo.ZONE_DISTANCE_STANDART st
	join #Temp t1 on t1.zone_id = st.zone_from
	join #Temp t2 on t2.zone_id = st.zone_to


	drop table #Temp

	
END



/*
select top 100 * 
from dbo.SESSION
order by session_id desc

select * from operator


select top 100 * from dbo.H_ROUTE
*/