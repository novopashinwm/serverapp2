/****** Object:  StoredProcedure [dbo].[GetDataForRoster]    Script Date: 07/09/2010 10:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetDataForRoster]
	@DATE datetime,
	@operator_id int = null
/* 	
	@DATE  їиїЇЁї№ Є Є°Її ёїў■к¤■╕°кї√╣¤■є■ Єиї№ї¤°
	ж║кїЄvї √°╕кv Єvё°иЁї№ °ў +-12 вЁ╕■Є ■к @DATE.
*/
as
begin
	declare @DATE_TOMORROW datetime
	set @DATE_TOMORROW = dateadd(dd, 0, @DATE)
	
-- ж║кїЄvї √°╕кv
	select distinct wh.*
	from WAYBILL_HEADER wh
	left join v_operator_vehicle_right vr on vr.vehicle_id = wh.VEHICLE_ID
	where 
		(
		WAYBILL_DATE between dateadd(hh, -12, @DATE) and dateadd(hh, +12, @DATE) or 
		WAYBILL_DATE between dateadd(hh, -12, @DATE_TOMORROW) and dateadd(hh, +12, @DATE_TOMORROW)
		) and
		CANCELLED = 0 -- L¤¤║√°и■ЄЁ¤¤vї жT ¤ї ║вЁ╕кЄ║жк Є ж=
		and (@operator_id is null or vr.operator_id = @operator_id)
		

	
-- +к№їк·°  ║кїЄv┐ √°╕к■Є
	select distinct WAYBILLHEADER_WAYBILLMARK.*
	from WAYBILLHEADER_WAYBILLMARK
		join WAYBILL_HEADER on WAYBILL_HEADER.WAYBILL_HEADER_ID = WAYBILLHEADER_WAYBILLMARK.WAYBILLHEADER_ID
		left join v_operator_vehicle_right vr on vr.vehicle_id = WAYBILL_HEADER.VEHICLE_ID
	where
		WAYBILL_DATE between dateadd(hh, -12, @DATE) and dateadd(hh, +12, @DATE) and
		CANCELLED = 0 -- L¤¤║√°и■ЄЁ¤¤vї жT ¤ї ║вЁ╕кЄ║жк Є ж=
		and (@operator_id is null or vr.operator_id = @operator_id)
		
-- T№ї¤v  ║кїЄv┐ √°╕к■Є
	select distinct wb.* 
	from WAYBILL wb
	left join v_operator_driver_right dr on dr.DRIVER_ID = wb.DRIVER_ID
	where 
		WAYBILL_DATE = @DATE or
		WAYBILL_DATE = @DATE_TOMORROW
		and (@operator_id is null or dr.operator_id = @operator_id)

-- жїЁ√╣¤vї иї∙╕v ╕№ї¤  ║кїЄv┐ √°╕к■Є
	select distinct WB_TRIP.*
	from WB_TRIP 
		left JOIN WAYBILL on WAYBILL.WAYBILL_ID = WB_TRIP.WAYBILL_ID
		left JOIN WAYBILL_HEADER on WAYBILL_HEADER.WAYBILL_HEADER_ID = WB_TRIP.WAYBILL_HEADER_ID
		left join v_operator_vehicle_right vr on vr.vehicle_id = WAYBILL_HEADER.VEHICLE_ID
		left join v_operator_driver_right dr on dr.DRIVER_ID = WAYBILL.DRIVER_ID
	where
		WB_TRIP.BEGIN_TIME between dateadd(hh,-12,@DATE) and dateadd( hh,+36, @DATE_TOMORROW) and
		(
			WAYBILL_HEADER.WAYBILL_DATE between dateadd(hh, -12, @DATE) and dateadd(hh, +12, @DATE) or 
			WAYBILL_HEADER.WAYBILL_DATE between dateadd(hh, -12, @DATE_TOMORROW) and dateadd(hh, +12, @DATE_TOMORROW) or
			WAYBILL.WAYBILL_DATE = @DATE or
			WAYBILL.WAYBILL_DATE = @DATE_TOMORROW
		) and 
		( WAYBILL_HEADER.CANCELLED = 0 or WB_TRIP.WAYBILL_HEADER_ID is NULL )
		and (@operator_id is null 
			or dr.operator_id = @operator_id 
			or vr.operator_id = @operator_id
			)

-- T■в·° №Ёи░и║кЁ
	select distinct p.* 
	from point p 
	join GEO_SEGMENT gs on (gs.POINT = p.POINT_ID or gs.POINTTO = p.POINT_ID)
	join geo_trip gt on gt.GEO_TRIP_ID = gs.GEO_TRIP_ID
	left join dbo.ROUTE_TRIP rt on rt.GEO_TRIP_ID = gt.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = rt.ROUTE_ID
	where gt.CREATOR_OPERATOR_ID = @operator_id
	or (rr.OPERATOR_ID is not null and rr.OPERATOR_ID = @operator_id)

-- +╕кЁ¤■Є·°
	select distinct b.* 
	from busstop b
	join v_operator_department_right dr on dr.DEPARTMENT_ID = b.DEPARTMENT_ID
	where dr.operator_id= @operator_id 
	and dr.right_id=104

	/* DAY */
	select *
	from [DAY]
	where [DATE] = @DATE
end

/******************************************************************************
**		File: 
**		Name: GetDataForRoster
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------		--------				-------------------------------------------
**    
*******************************************************************************/




