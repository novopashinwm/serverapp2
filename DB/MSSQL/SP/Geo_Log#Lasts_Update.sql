﻿IF (OBJECT_ID(N'[dbo].[Geo_Log#Lasts_Update]') IS NOT NULL)
	DROP PROCEDURE [dbo].[Geo_Log#Lasts_Update];
GO

CREATE PROCEDURE [dbo].[Geo_Log#Lasts_Update]
(
	@vehicle_id int,
	@log_time   int,
	@lng        real, -- longitude
	@lat        real  -- latitude
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@rows   int = 0;

	UPDATE [dbo].[Geo_Log#Lasts]
	SET
		[LOG_TIME#Prev] = CASE WHEN [LOG_TIME#Last] < @log_time THEN [LOG_TIME#Last] ELSE [LOG_TIME#Prev] END,
		[Lng#Prev]      = CASE WHEN [LOG_TIME#Last] < @log_time THEN [Lng#Last]      ELSE [Lng#Prev]      END,
		[Lat#Prev]      = CASE WHEN [LOG_TIME#Last] < @log_time THEN [Lat#Last]      ELSE [Lat#Prev]      END,
		[LOG_TIME#Last] = CASE WHEN [LOG_TIME#Last] < @log_time THEN @log_time       ELSE [LOG_TIME#Last] END,
		[Lng#Last]      = CASE WHEN [LOG_TIME#Last] < @log_time THEN @lng            ELSE [Lng#Last]      END,
		[Lat#Last]      = CASE WHEN [LOG_TIME#Last] < @log_time THEN @lat            ELSE [Lat#Last]      END
	WHERE [VEHICLE_ID] = @vehicle_id

	SELECT @rows = @@ROWCOUNT

	IF (0 = @rows)
	BEGIN
		INSERT INTO [dbo].[Geo_Log#Lasts] ([VEHICLE_ID], [LOG_TIME#Last], [Lng#Last], [Lat#Last])
		VALUES                            (@vehicle_id,  @log_time,       @lng,       @lat)
	END
END
GO