if (exists (select * from sys.objects where name = 'SetNewPassword' and type='p'))
	drop procedure dbo.SetNewPassword
go
CREATE PROCEDURE dbo.SetNewPassword  
 @operator_id int,  
 @new_password nvarchar(50)  
AS  
 UPDATE dbo.OPERATOR   
 SET PASSWORD = @new_password,
	OneTimePassword = 0
 WHERE OPERATOR_ID = @operator_id  
  
 SELECT * FROM OPERATOR  
 WHERE OPERATOR_ID = @operator_id  
  