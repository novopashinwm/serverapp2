if exists (
	select * from sys.objects where name = 'describeMessageStatus'
)
	drop function describeMessageStatus;
go

create function describeMessageStatus (@status int) returns varchar(255)
as
begin
	return case @status when 1 then 'Wait' when 2 then 'Pending' when 3 then 'Done' else 'Other (' + convert(varchar(10), @status) + ')' end
end