if exists (select * from sys.views where name = 'v_operator_rights')
	drop view v_operator_rights
go

create view [dbo].[v_operator_rights]   
as  
 WITH allRights(operator_id, right_id, allowed, priority) AS   
 (  
  select operator_id, right_id, allowed, 2 'priority'  
   from dbo.RIGHT_OPERATOR   
  union all  
  select og_o.operator_id, og_v.right_id, allowed, 1 'priority'  
   from dbo.RIGHT_OPERATORGROUP og_v  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
 )  
 select distinct operator_id, right_id  
 from allRights r1  
 where allowed = 1  
 and not exists (select * from allRights r2   
     where r2.operator_id = r1.operator_id   
     and r2.right_id = r1.right_id  
     and r2.allowed = 0  
     and r2.priority > r1.priority)  