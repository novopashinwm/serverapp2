/****** Object:  StoredProcedure [dbo].[GetGeoZonesByGroup]    Script Date: 02/06/2012 21:38:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGeoZonesByGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetGeoZonesByGroup]
GO

/****** Object:  StoredProcedure [dbo].[GetGeoZonesByGroup]    Script Date: 02/06/2012 21:38:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[GetGeoZonesByGroup]
	@OperatorID int,
	@GeoZoneGroupID int
AS
	select DISTINCT gz.Zone_ID, gz.NAME, gz.[Description] from v_operator_zone_groups_right ozgr
	join zonegroup_zone zgz on zgz.ZONEGROUP_ID = ozgr.zonegroup_id
	join geo_zone gz on gz.ZONE_ID = zgz.ZONE_ID
	where
		ozgr.operator_id = @operatorID
		and ozgr.zonegroup_id = @GeoZoneGroupID
		and ozgr.right_id = 105		

GO


