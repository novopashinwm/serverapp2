﻿IF (OBJECT_ID(N'[dbo].[UpdateStateLog]', N'P') IS NOT NULL)
	DROP PROCEDURE [dbo].[UpdateStateLog];
GO

CREATE PROCEDURE [dbo].[UpdateStateLog]
(
	@vehicleID int,
	@type      tinyint,
	@Log_Time  int,
	@value     int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@prev_Log_Time int,
		@next_Log_Time int,
		@prev_Value    bit,
		@next_Value    bit

	SELECT TOP(1)
		@prev_Value    = [Value],
		@prev_Log_Time = [Log_Time]
	FROM [dbo].[State_Log] WITH (NOLOCK)
	WHERE [Vehicle_ID] = @vehicleID
	AND   [Type]       = @type
	AND   [Log_Time]   < @Log_Time
	ORDER BY [Log_Time] DESC

	SELECT TOP(1)
		@next_Value    = [Value],
		@next_Log_Time = [Log_Time]
	FROM [dbo].[State_Log] WITH (NOLOCK)
	WHERE [Vehicle_ID] = @vehicleID
	AND   [Type]       = @type
	AND   [Log_Time]   > @Log_Time
	ORDER BY [Log_Time]

	IF (@prev_Value IS NULL OR @prev_Value <> @value)
	BEGIN
		INSERT INTO [dbo].[State_Log]([Vehicle_ID], [Log_Time], [Type], [Value])
		SELECT                        @vehicleID,   @Log_Time,  @type,  @value
		WHERE NOT EXISTS
		(
			SELECT 1
			FROM [dbo].[State_Log] WITH (XLOCK, SERIALIZABLE)
			WHERE [Vehicle_ID] = @vehicleID
			AND   [Type]       = @type
			AND   [Log_Time]   = @Log_Time
		)
	END

	IF (@next_Value = @value)
	BEGIN
		DELETE [dbo].[State_Log] WITH (XLOCK, SERIALIZABLE)
		WHERE [Vehicle_ID] = @vehicleID
		AND   [Type]       = @type
		AND   [Log_Time]   = @next_Log_Time
	END
END
GO