
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDataForReportToSchedulerQueue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetDataForReportToSchedulerQueue]
GO
  
CREATE PROCEDURE [dbo].[GetDataForReportToSchedulerQueue]  
 @operatorID int,  
 @guid uniqueidentifier  
AS  
 declare @schedulereventID int  
   
 set @schedulereventID =   
 (  
  select   
   case @guid   
    -- отчёт о гео-зонах  
    when '6e2de7ad-2656-4812-bc1c-0eea32613662' then 55  
    -- история движения  
    when 'f824b9a6-f435-4940-9788-1c46495bb73f' then 59  
    -- суточный пробег  
    when 'b4c4e567-3ca3-4252-bc9e-e9949fa9c7a7' then 58  
    -- общий отчет по пробегу  
    when '7CF57572-0F9A-4f82-9136-E191DF144C9A' then 61  
    -- отчет по топливу, общий отчет по топливу  
    when '7FA68680-9660-4b9a-9DF0-986F6F9A4F56' then 62  
    -- отчет о превышении скорости  
    when 'B7CB4C01-50DB-4bf4-82AB-F71E5D595BCC' then 63  
    -- Запросы в сообществе  
    when 'D2299B07-03BA-4c17-99BB-7C51F0CDEC3B' then 64  
    -- Кого запрашивал я  
    when 'B939736A-7B95-4d0b-B59C-6A126D310E70' then 65  
    -- Кто запрашивал меня  
    when '873109F4-D52A-40c5-B095-6CE759A17469' then 66  
    -- Расчет стоимости ГСМ  
    when '5159616A-0D75-4174-BAFD-4F6467A96400' then 67  
    -- история движения с интервалами 
    when 'E7E8996D-80C0-4F31-8E1E-E8859019983B' then 68
    -- Отчет по геозонам по входу и выходу   
    when 'C1387D78-34EF-4F91-9709-2FC8515A3E11' then 69  
    -- Отчет по дискретным датчикам   
    when '78288D6E-6AD2-40B5-BE4C-ABDCDC6579A9' then 70  
    -- Отчет по контролю доступа
    when 'F4ED91AC-115F-4E46-9931-1206DBAB05D6' then 71  
   end  
 )  
  
 SELECT   
      OPERATOR_ID  
  ,NAME  
  ,LOGIN  
  ,PASSWORD  
  ,PHONE = isnull((select top(1) p.Phone from dbo.Phone p where p.OPERATOR_ID = @operatorID and p.confirmed = 1 order by p.Phone_id desc), '')
  ,EMAIL = isnull((select top(1) e.EMAIL from dbo.Email e where e.OPERATOR_ID = @operatorID order by e.EMAIL_ID desc), o.EMAIL)  
  ,GUID  
  ,TimeZoneInfo  
  ,IsTimeZoneInfoManual  
  ,OneTimePassword  
  FROM OPERATOR o WHERE OPERATOR_ID = @operatorID  
  
 SELECT e.* FROM EMAIL e WHERE OPERATOR_ID = @operatorID  
  
 SELECT * FROM dbo.SCHEDULEREVENT WHERE SCHEDULEREVENT_ID = @schedulereventID  
  
 SELECT * FROM dbo.SCHEDULERQUEUE WHERE 1=0  
  
 SELECT * FROM dbo.EMAIL_SCHEDULERQUEUE WHERE 1=0  
  
    
--exec DBO.GETDATAFORREPORTTOSCHEDULERQUEUE @operatorID=407,@guid='F824B9A6-F435-4940-9788-1C46495BB73F'  