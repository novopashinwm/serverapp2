---From MGDB
/****** Object:  StoredProcedure [dbo].[GetDefectiveTrips]    Script Date: 07/15/2010 18:20:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------

ALTER procedure [dbo].[GetDefectiveTrips]
	@date datetime,
	@vehiclegroup_id int,
	@operator_id int = null
as

--------------------------------------------------------------------------------
/*
-- Тест
--select * from WB_TRIP where (status & 0x04) > 0 order by BEGIN_TIME DESC

declare 
	@date datetime, 
	@vehiclegroup_id int

set @date = '09/11/2006 20:00' 
set @vehiclegroup_id = 8
*/
--------------------------------------------------------------------------------

select distinct 
	wt.wb_trip_id,
	wt.begin_time							as begin_wb, 
	wt.end_time 							as end_wb, 
	--wt.status & 0x02 						as defect, 	-- забракован
	wt.status & 0x04 						as defect, 	-- выполнен с браком
	wt.waybill_header_id,
	wt.trip_id,
	r.ext_number							as route,
	s.ext_number,
	v.garage_number,
	d.ext_number							as driver_num,
	d.short_name							as driver_surname,		
	d.[name]							as driver_name,		
	dateadd(ss, min(sp.time_out), @date) 				as departure,
	dateadd(ss, max(sp.time_in), @date)				as arrival
	--,datediff(second, max(spg.time_out), max(spg.log_time_out))  	as departure_deviation
	--,datediff(second, min(spg.time_in), min(spg.log_time_in))	as arrival_deviation
into
	#trip
from
	wb_trip as wt	
	--Расписание и маршрут
	inner join trip as t on wt.trip_id = t.trip_id
	inner join schedule_detail as sd on t.schedule_detail_id = sd.schedule_detail_id
	inner join schedule as s on sd.schedule_id = s.schedule_id
	inner join route as r on s.route_id = r.route_id
	--ТС
	inner join waybill_header as wh on wt.waybill_header_id = wh.waybill_header_id
	inner join vehicle as v on wh.vehicle_id = v.vehicle_id
	inner join vehiclegroup_vehicle as vgv on v.vehicle_id = vgv.vehicle_id
	--Водитель
	inner join waybill as w on wt.waybill_id = w.waybill_id
	inner join driver as d on w.driver_id = d.driver_id
	--время прохождения через точки и отклонения
	inner join schedule_point as sp on sp.trip_id = t.trip_id
	--left join schedule_passage as spg on spg.wh_id = wt.waybill_header_id and spg.sp_id = sp.schedule_point_id
	left join v_operator_vehicle_right rr on rr.vehicle_id = v.VEHICLE_ID
where	
	wh.waybill_date between dateadd(hour, -1, @date) and dateadd(hour, 1, @date)
	and vgv.vehiclegroup_id = @vehiclegroup_id 
	and wt.[TRIP_KIND_ID] in (5, 6) 
	and (wt.status & 0x04) > 0
	and (@operator_id is null or rr.operator_id = @operator_id)
group by
	wt.wb_trip_id,
	wt.begin_time, 
	wt.end_time, 
	wt.status, 	
	wt.waybill_header_id,
	wt.trip_id,
	r.ext_number,
	s.ext_number,
	v.garage_number,
	d.ext_number,
	d.short_name,		
	d.[name]	
order by 
	v.garage_number, wt.begin_time, wt.end_time
	

select 
	t.*,
	/*spg_d.time_in, 
	spg_d.time_out, 
	spg_d.log_time_in, 
	spg_d.log_time_out,
	spg_a.time_in, 
	spg_a.time_out, 
	spg_a.log_time_in, 
	spg_a.log_time_out,*/
	isnull(datediff(second, spg_d.time_out, spg_d.log_time_out),0)  	as departure_deviation,
	isnull(datediff(second, spg_a.time_in, spg_a.log_time_in),0)	as arrival_deviation
from 
	#trip as t
	--время отклонения отправления
	inner join schedule_point as sp_d on sp_d.trip_id = t.trip_id
		and dateadd(ss, sp_d.time_out, @date) = t.departure 
	left join schedule_passage as spg_d on spg_d.wh_id = t.waybill_header_id
		and spg_d.sp_id = sp_d.schedule_point_id
	--время отклонения прибытия
	inner join schedule_point as sp_a on sp_a.trip_id = t.trip_id
		and dateadd(ss, sp_a.time_in, @date) = t.arrival 
	left join schedule_passage as spg_a on spg_a.wh_id = t.waybill_header_id
		and spg_a.sp_id = sp_a.schedule_point_id


drop table #trip

