if exists (
	select *
		from sys.procedures
		where name = 'ClearOldAppLogs'
)
	drop procedure ClearOldAppLogs
go


--clearoldapplogs 2, 1000
create procedure ClearOldAppLogs
(
	@debug int = null,
	@row_count int = 1000
)
as

if @debug is null
	set @debug = 0

if 2 <= @debug
	set nocount on

declare @template_id int = (
	select top(1) Message_Template_ID
		from Message_Template mt
		where mt.Name = 'AppLog')

declare @time datetime = GETUTCDATE()-7

declare @id dbo.Id_Param

declare @oldest datetime
declare @selected_rowcount int

while 1=1
begin

	delete @id

	if @debug = 2
		print 'Fill @id'
	insert into @id
		select top(@row_count) m.message_id
			from MESSAGE m (nolock)
			where m.Template_ID = @template_id
			  and m.Time < @time
			order by m.Time

	set @selected_rowcount = @@rowcount

	if @selected_rowcount = 0
		break;
		
	if 1 <= @debug
		print 'Deleting ' + convert(varchar(12), @selected_rowcount) + ' records'

	if @debug = 2
		print 'delete Message_Contact'
	delete d
		from @id id
		join Message_Contact d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete Message_Field'
	delete d
		from @id id
		join Message_Field d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete Message_Attachment'
	delete d
		from @id id
		join Message_Attachment d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete Message_Operator'
	delete d
		from @id id
		join Message_Operator d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete RenderedServiceItem'
	delete d
		from @id id
		join RenderedServiceItem d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete H_Message_Field'
	delete d
		from @id id
		join H_Message_Field d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete H_Message_Attachment'
	delete d
		from @id id
		join H_Message_Attachment d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete H_Message_Operator'
	delete d
		from @id id
		join H_Message_Operator d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete H_Message'
	delete d
		from @id id
		join H_Message d on d.Message_ID = id.Id

	if @debug = 2
		print 'delete Message'
	delete d
		from @id id
		join Message d on d.Message_ID = id.Id

	if @debug = 1
	begin
	
		set @oldest = (
			select top(1) m.TIME
				from MESSAGE m (nolock)
				where m.Template_ID = @template_id
				  and m.Time < @time
				order by m.Time)
		
		print convert(varchar(255), getdate()) + ' oldest time: ' + isnull(convert(varchar(255), @oldest), 'NULL')
	
	end
	
	if 2 <= @debug
		break

end

