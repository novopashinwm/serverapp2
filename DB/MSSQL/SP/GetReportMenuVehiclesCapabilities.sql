﻿IF (OBJECT_ID(N'[dbo].[GetReportMenuVehiclesCapabilities]') IS NOT NULL)
	DROP PROCEDURE [dbo].[GetReportMenuVehiclesCapabilities]
GO
CREATE PROCEDURE [dbo].[GetReportMenuVehiclesCapabilities]
(
	@operatorID   int,
	@departmentId int = null
)
AS
BEGIN
	declare @askPositionCommandId int = (select id from CommandTypes where code='AskPositionOverMLP')
	select ovr.Vehicle_ID, v.vehicle_kind_id, c.controller_id, ct.controller_type_id, convert(bit, case when exists (select * from Controller_Type_CommandTypes ctct where ctct.Controller_Type_ID = c.CONTROLLER_TYPE_ID and ctct.CommandTypes_ID=@askPositionCommandId) then 1 else 0 end) supportspositionasking
		into #vehicles_for_report
	from dbo.v_operator_vehicle_right ovr
		join vehicle v on v.vehicle_id = ovr.vehicle_id
		join controller c on c.vehicle_id=v.vehicle_id
		join controller_type ct on c.controller_type_id=ct.controller_type_id
	where ovr.Right_ID = 102	--VehicleAccess
		and ovr.Operator_ID =	@operatorID
		and (@departmentId is null or v.department = @departmentId)

	select 
		case when exists
		(
			select * from #vehicles_for_report where vehicle_kind_id not in (7,5,9,10)
		) then 1 else 0 end IsIginitionMonitoringAvailable
		,case when exists
		(
			select * from #vehicles_for_report where vehicle_kind_id not in (7,9,10)
		) then 1 else 0 end HasSpeedMonitoring
		,case when exists
		(
			select *
			from #vehicles_for_report v
				join v_operator_vehicle_right ovr on ovr.vehicle_id=v.vehicle_id and ovr.operator_id=@operatorID
				left outer join mlp_controller mlpc on mlpc.controller_id = v.controller_id
				left outer join asid a on a.ID = mlpc.asid_id
			where ovr.right_id = 6
			  and (v.SupportsPositionAsking = 1 --Можно определить местоположение, отправив запрос на устройство
				   or
				   a.Contact_ID is not null) --Можно определить местоположение, отправив запрос на платформу MPX
		) then 1 else 0 end IsAskPositionAvailable
		,case when exists
		(
			select *
			from #vehicles_for_report vfr
				join controller_sensor cs on cs.controller_type_id=vfr.controller_type_id
			where cs.controller_sensor_type_id=2
		) then 1 else 0 end HasDigitalSensors
		,case when exists
		(
			select *
			from asid a
				join mlp_controller mc on a.id=mc.asid_id
			where a.operator_id=@operatorId
		) then 1 else 0 end IsWhoAskedMeAvailable
		,case when exists
		(
			select 1
			from v_operator_rights o_r
			where o_r.operator_id = @operatorID
			  and o_r.right_id = 110 /*ServiceManagement*/
		) then 1 else 0 end IsSalesAvailable
	drop table #vehicles_for_report
END