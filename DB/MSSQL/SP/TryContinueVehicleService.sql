CreateProcedure 'TryContinueVehicleService'
go

--��������� �������� �������� �������� ������ �� ������� ���������� �� ��������� ����
--� ������ ����������� � �������������, ���������� �������� ������
--����������� = ������� ���������� ������� � ��������� �������
--������������� = ������ ����������� ��� ���� �������� ������ �����
--�������� ����, �� ������� ��������� ������
--TryContinueVehicleService 821, '2016.03.15 1:00'
alter procedure TryContinueVehicleService
(
	@vehicle_id int,
	@paidTill datetime,
	@debug bit = 0
)
as

if (@debug is null)
	set @debug = 0

declare @now datetime = GETUTCDATE()

declare @result table(PaidTill datetime)

insert into @result
	select @paidTill
		from Vehicle v
		join Department d on d.Department_ID = v.Department
		where v.Vehicle_ID = @vehicle_id
		  and isnull(d.Type,0) = 0

if @@ROWCOUNT <> 0
begin
	select * from @result
	return
end

--������ ������ ������������ ���������� �� ������� ������� ������ - ��� ���������� ��� ��������� ��������
update Vehicle
	set PaidTill = @paidTill 
	output inserted.PaidTill
	into @result
	where Vehicle_ID = @vehicle_id 
	  and PaidTill is null

if exists(select 1 from @result)
begin
	if @debug = 1
		print 'First free day'
	select * from @result
	return;
end	
else 
begin
	--���������, ����� ����, ������ ��� ��������
	if @@ROWCOUNT = 0
	begin
		insert into @result
			select PaidTill 
				from vehicle 
				where vehicle_id = @vehicle_id
				  and @now < PaidTill

		if @@ROWCOUNT <> 0
		begin
			if @debug = 1
				print 'Service is already payed'
			select * from @result
			return;
		end
	end
end

if @debug = 1
	print 'Searching for appropriate service'

--�����:
--1. ����� ���������� ������ � ������� � ��� 1 ��
--2. �������� ������ � Rendered_Service_Item
--������� ��� � ����� ����������
--���� ������ �� ��������, ����� ����������, ����� ��� ��������� �� ���������� �������� � ������� ���������� �������

declare @type varchar(32) = 'ServiceDays'
declare @type_ID int = (select ID from Rendered_Service_Type where Name = @type)

--TODO: �������� ��������� ������������� ��������

declare @bs table (ID int not null, ServiceDays int not null)
insert into @bs
	select bs.ID, bst.ServiceDays
		from operator_vehicle ovr 
		join Billing_Service bs on bs.operator_id = ovr.operator_id
		join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
		outer apply (select top(1) rs.Quantity from Rendered_Service rs where rs.Billing_Service_ID = bs.ID order by rs.ID desc) rs
		where ovr.vehicle_id = @vehicle_id
		  and ovr.RIGHT_ID = 2 --TODO: ������ ����� ����� - ���������� ��� ��������?
		  and ovr.ALLOWED = 1		  
		  and isnull(rs.Quantity, 0) < bst.ServiceDays
		  

if @@ROWCOUNT = 0
	return;


declare @bsid int = 0
declare @serviceDays int
declare @t table (RenderedQuantity int)

--��������� ������ �������, ����� ����������� �������� ��������

set transaction isolation level repeatable read
while 1=1
begin
	set @bsid = (select top(1) bs.ID from @bs bs where bs.ID > @bsid order by ID desc)
	if (@bsid is null)
		break;
	set @serviceDays = (select bs.ServiceDays from @bs bs where bs.ID = @bsid)

	begin tran

		update vehicle 
			set PaidTill = @paidTill
			output inserted.PaidTill
			into @result
			where Vehicle_ID = @vehicle_id 
			  and (PaidTill is null or PaidTill < @now and PaidTill < @paidTill) 
			  

		if @@ROWCOUNT = 0
		begin
			insert into @result
				select PaidTill from vehicle where vehicle_id = @vehicle_id
			commit
			break
		end

		insert into @t
			exec IncreaseRenderedQuantity @bsid, @type, @serviceDays

		if @@ROWCOUNT = 0
		begin
			rollback
			continue
		end

	commit
	break
end
set transaction isolation level read committed

declare @renderedQuantity int = (select RenderedQuantity from @t)

if @renderedQuantity is not null
	insert into RenderedServiceItem (RenderDate, Billing_Service_ID, Rendered_Service_Type_ID, Status, StatusDate, Vehicle_ID, RenderedQuantity)
		select @now, @bsid, @type_ID, 1 /*Payed*/, @now, @vehicle_id, @renderedQuantity
		
select * from @result

