/****** Object:  StoredProcedure [dbo].[GetTraffic]    Script Date: 11/28/2008 13:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------------------------------------------
--═рчэрўхэшх: ╧юфёўхЄ яЁюсхур фы  чрфрээющ ╥╤ чр чрфрээ√щ тЁхьхээющ шэЄхЁтры
--└тЄюЁ: ├Ёхсхээшъют ╠.┬.
-----------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[GetTraffic]
	@dtfrom DateTime, 
	@dtto DateTime,  
	@vehicle int 
AS

-----------------------------------------------------------------------------------
/*
-- Test
Declare
	@dtfrom DateTime, 
	@dtto DateTime,  
	@vehicle int 

set @dtfrom = '2007-12-31 21:00:00'
set @dtTo = '2008-01-31 20:59:00'
set @vehicle = 241

set @dtfrom = '2008-02-05 11:00:00:000'
set @dtTo = '2008-02-06 10:52:00:000'
set @vehicle = 491 

set @dtfrom = '2008-07-01 00:00:00:000'
set @dtTo = '2008-07-03 20:00:00:000'
set @vehicle = 532 

set @vehicle = 864 
set @dtfrom = '2008-10-21 04:00:00:000'
set @dtTo = '2008-10-21 05:30:00:000'


--select * from VEHICLE where PUBLIC_NUMBER like '%725%'
*/
-----------------------------------------------------------------------------------

Declare 
	@vehicle_id		int,
	@time_from_int	int,
	@time_to_int	int,
	@time_diffUTC	int,
	--фюяєёЄшь√щ шэЄхЁтры юЄёєЄёЄтш  фрээ√ї шч ЄрсышЎ√ яЁртшы фы  ╥╤, хёыш эх чрфрэ яЁшэшьрхь 60 ёхъ.
	@rule_id		int,
	@time_allow		int
	
	
Set @vehicle_id = @vehicle 
Set @time_from_int = datediff(s, '1970', @dtfrom)
Set @time_to_int = datediff(s, '1970', @dtTo)
Set @time_diffUTC = datediff(s, @dtfrom, cast(floor(cast((@dtfrom + 1) as float)) as datetime))
Set @rule_id = 2
Set @time_allow = isnull((select Top 1 Value from VEHICLE_RULE where vehicle_id = @vehicle_id and rule_id = @rule_id), 60)

-- ┬Ёхьхээр  ЄрсышЎр яю Єюўърь
create table #MonitoreeLog(	
	id int identity primary key, 
	Log_Time		int,
	Log_Date		DateTime, 
	X				float,
	Y				float,
	Speed			int,
	Dist			float,
	Data 			int,
	GroupData 		int
	,Satellites		int
	)

-- ╬ЄсшЁрхь тёх яючшЎшш ╥╤ чр шэЄхЁтры
insert into #MonitoreeLog
select 	
	Log_Time, 
	--cast(floor(cast(dateadd(s, Log_Time, '1970') as float)) as datetime),
	--dateadd(hour, -@time_diffUTC / 3600, cast(floor(cast(dateadd(s, Log_Time + @time_diffUTC, '1970') as float)) as datetime) ),
	dbo.GetDateFromInt(Log_Time),
	X,
	Y,
	Speed,
	Convert(float, 0), 
	1,
	0
	,Satellites
from Monitoree_log (nolock)
Where 	
	Monitoree_ID = @vehicle_ID
	and Log_Time between @time_from_int and @time_to_int 
	--and X < 180 and Y < 180
order by 
	Log_Time

-- ╧ыюїшх яючшЎшш сюы№°х эх яюЁЄ Єё , эєцэю яЁютхЁ Є№ DS шч CONTROLLER_STAT
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.CONTROLLER_STAT'))  
	update #MonitoreeLog set X = 181, Y = 181 where  
		((select count(CS.[TIME]) from dbo.CONTROLLER_STAT as CS inner join CONTROLLER as C on CS.CONTROLLER_ID = C.CONTROLLER_ID where C.VEHICLE_ID = @vehicle_ID and CS.[TIME] = dateadd(s, #MonitoreeLog.Log_Time, '1970') and (CS.DS & 0x80000000) = 0 ) > 0)
update #MonitoreeLog set X = 181, Y = 181 where Satellites < 3 or (X = 0 or Y = 0)


-- ╬сЁрсрЄ√трхь яюыєўхээ√х фрээ√х (ёўшЄрхь яЁюсху ш уЁєяяшЁєхь яю яхЁшюфрь)
Declare 
	@max_id			int,
	@i				int,
	@i_				int,
	@log_time		int,
	@log_time_		int,
	@log_time_last	int,
	@x				float,
	@x_				float,
	@x_last			float,
	@y				float,
	@y_				float,
	@y_last			float,
	@dist			float,
	@data			int,
	@data_			int,
	@GroupData		int,
	--
	@speed			int,
	@speed_			int,
	@speed_last		int,
	@timeBetween	int			

 
set	@max_id = (select max(id) from #MonitoreeLog)
set	@i = 1
set	@GroupData = 0

set @x_last			= 181 
set @y_last			= 181
set @speed_last		= 0
set @log_time_last	= 0

while @i < @max_id
begin
	set @i_ = @i
	set @i = @i + 1

	set @log_time = (select Log_Time from #MonitoreeLog where id = @i)
	set	@x = (select X from #MonitoreeLog where id = @i) 			
	set	@y = (select Y from #MonitoreeLog where id = @i)			
	set	@data = (select Data from #MonitoreeLog where id = @i)	
	set	@log_time_ = (select Log_Time from #MonitoreeLog where id = @i_)
	set	@x_	= (select X from #MonitoreeLog where id = @i_)		
	set	@y_	= (select Y from #MonitoreeLog where id = @i_)	
	set	@data_	= (select Data from #MonitoreeLog where id = @i_)
	
	set @speed = (select Speed from #MonitoreeLog where id = @i)
	set @speed_ = (select Speed from #MonitoreeLog where id = @i_)
	set @timeBetween = @log_time - @log_time_

		
	--ърўхёЄтю ъююЁфшэрЄ фы  яхЁтющ яючшЎшш (Data=1 - хёыш ъююЁфшэрЄ√ їюЁю°шх; Data=2 - хёыш яыюїшх юЄёєЄёЄтшх GPS)
	if(@i_ = 1 and (@x_ > 180 or @y_ > 180))
	begin
		Set @data_ = 2
		update #MonitoreeLog Set Data = @data_ where id = @i_
	end

	-- ╧Ёюсху ьхцфє Єюўърьш. ═х ёўшЄрхь, хёыш ъююЁфшэрЄ√ яыюїшх --ш хёыш ёъюЁюёЄ№ эєыхтр  (ьр°шэр ёЄюшЄ)
	if(@x_ < 180 or @y_ < 180)
	begin
		set @x_last = @x_
		set @y_last = @y_
		set @speed_last = @speed_
		set @log_time_last = @log_time_
		set @timeBetween = @log_time - @log_time_last 
	end	

	if(@x > 180 or @y > 180 or @x_last > 180 or @x_last > 180)
		Set @dist = 0 
	else 
	begin
		Set @dist = SQRT(SQUARE(63166*(@x_last-38)-63166*(@x-38)) + SQUARE(111411*(@y_last-55.665)-111411*(@y-55.665))) / 1000
		--
		if(@speed_last < 1)
			begin
				if(@speed < 1)
					if(@timeBetween < 60) --0.02 and @dist < 0.02)
						Set @dist = 0
			end
	end

/*
	--┼ёыш яючшЎшш яЁш°ыш т Єхўхэшш 60 ёхъ
	if(@log_time - @log_time_ <= @time_allow)
	begin
		--яЁшчэръ "їюЁю°шї" яючшЎшщ (Data=1 - хёыш ъююЁфшэрЄ√ їюЁю°шх; Data=2 - хёыш яыюїшх юЄёєЄёЄтшх GPS)
		set @data = 1
		if (@x > 180 or @y > 180) 
			set @data = 2
	end
	else
	begin
		--яЁшчэръ юЄёєЄёЄтш  яючшЎшщ (Data=0 - юЄёєЄёЄтшх GSM)
		set @data = 0
		--тючьюцэю эєцэю тёЄртшЄ№ ярЁє ёЄЁюъ фы  юсючэрўхэш  шэЄхЁтрыр юЄёєЄётш  
	end

	--Єръцх уЁєяяшЁєхь яю ърўхёЄтє яючшЎшщ
	if( @data <> @data_ )
		Set @GroupData = @GroupData + 1
*/
	--┼ёыш яючшЎшш яЁш°ыш т Єхўхэшш 60 ёхъ
	if(@log_time - @log_time_ > @time_allow)
		Set @GroupData = @GroupData + 1

	--юсэюты хь фрээ√х
    update #MonitoreeLog Set Dist = @dist, Data = @data, GroupData = @GroupData where id = @i
end

declare @pl int

set @pl = (select ct.PACKET_LENGTH from CONTROLLER_TYPE ct
left join CONTROLLER c on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
where c.VEHICLE_ID = @vehicle_id)


select 
	Log_Date,
	GroupData,
	Max(Log_Time) - Min(Log_Time) as TimeReceipt,
	Sum(Dist) as Dist,
	Convert(float, Count(Log_Time) * @pl) / 1048576 as Size  --/ 1024 as Size
	,Convert(char(10), Min(Log_Time)) as sFrom, dateadd(s, Min(Log_Time), '1970') as dtFrom 
	,Convert(char(10), Max(Log_Time)) as sTo, dateadd(s, Max(Log_Time), '1970') as dtTo
	,Data
into #Intervals 
from #MonitoreeLog
Group by 
	Log_Date,  
	GroupData
	,Data
order by Log_Date

select --* 
	Log_Date,
	Sum(TimeReceipt) as TimeReceipt,
	Sum(Dist) as Dist,
	Sum(Size) as Size
into #Result 
from #Intervals
Group by Log_Date  
order by Log_Date

--test
--select * ,dateadd(s, Log_Time, '1970') as [DateTime] from #MonitoreeLog
--select * from #Intervals
--select * from #Result


--┬ юЄўхЄх эхюсїюфшью юЄюсЁрцрЄ№ Єръцх фэш юЄёєЄёЄтш  фрээ√ї
create table #Days(	
	--id int identity primary key, 
	[Date]		DateTime 
	)
-- 
declare 
	@dFrom	datetime,
	@dTo	datetime
set @dFrom = dbo.getdatefromint(@time_from_int)--CONVERT(datetime, CONVERT(varchar, @dtfrom, 1), 1)
set @dTo = dbo.getdatefromint(@time_to_int) --CONVERT(datetime, CONVERT(varchar, @dtTo, 1), 1)
--
while @dFrom <= @dTo
begin
	insert into #Days values (@dFrom)  
	set @dFrom = @dFrom + 1
end
 
select --* 
	#Days.[Date] as Log_Date,
	isnull(TimeReceipt, 0) as TimeReceipt,
	isnull(Dist, 0) as Dist,
	isnull(Size, 0) as Size
	,(select count(TimeReceipt) from #Result where TimeReceipt > 0) as CountDaysReceipt
from #Days 
	left join #Result on #Result.Log_Date = #Days.[Date]
order by #Days.[Date]


drop table #MonitoreeLog
drop table #Intervals
drop table #Result
drop table #Days



