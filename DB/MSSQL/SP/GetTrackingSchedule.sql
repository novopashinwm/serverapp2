CreateProcedure 'GetTrackingSchedule'
go

--GetTrackingSchedule 821
alter procedure GetTrackingSchedule
(
	@vehicle_id int,
	@debug bit = null
)
as

if (@debug is null)
	set @debug = 0

if (@debug = 0)
	set nocount on

select ts.id
	 , ts.config
	 from TrackingSchedule ts
	 where 1=1
	   and ts.Vehicle_Id = @vehicle_id
	   and ts.Enabled = 1
	   and exists (select 1 from v_operator_vehicle_right ovr where ovr.vehicle_id = ts.vehicle_id and ovr.operator_id = ts.operator_id and ovr.right_id = 102)
	   and (
			exists (
				select 1 
					from Billing_Service bs 
					join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
					where bs.Operator_ID = ts.Operator_ID
					  and bst.TrackingSchedule = 1
			)	   
			or  ts.Operator_ID in (
				select a.Operator_ID
					from Asid a
					join MLP_Controller mc on mc.Asid_ID = a.ID
					join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
					where c.VEHICLE_ID = @vehicle_id
				)
			)