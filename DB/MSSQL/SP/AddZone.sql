set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



ALTER PROCEDURE [dbo].[AddZone]  
	--@map_id			int,
	@map_guid		uniqueidentifier,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@color			int,
	@description	nvarchar(500),
	@type_id		int,
	@radius			float,
	@operator_id	int,
	@all_operators	bit = 1
AS
---------------------------------------------------
/*
--test
declare
	@map_guid		uniqueidentifier,
	@vertex			nvarchar(3000),
	@name			nvarchar(250),
	@description	nvarchar(500),
	@color			int,
	@type_id		int,
	@radius			float,
	@operator_id	int

set @map_guid		= '{86aa79b0-461f-41fc-8b87-b7879ca3781a}'
set @vertex			= '37,653335571289062:55,747257232666016;'
set @name			= 'кругляшка'
set @description	= 'кругляшка'
set @color			= 0
set @type_id		= 1
set @radius			= 250
set @operator_id	= 137
*/
/*
select * from GEO_ZONE as Z 
	inner join GEO_ZONE_PRIMITIVE as ZP on Z.zone_id = ZP.zone_id
	inner join ZONE_PRIMITIVE as P on ZP.PRIMITIVE_ID = P.PRIMITIVE_ID
	inner join ZONE_PRIMITIVE_VERTEX as PV on P.PRIMITIVE_ID = PV.PRIMITIVE_ID
	inner join MAP_VERTEX as V on PV.VERTEX_ID = V.VERTEX_ID
where Z.zone_id = (select max(zone_id) from GEO_ZONE)
*/

---------------------------------------------------

BEGIN
	
	declare 
		@map_id					int,
		@zone_id				int,
		@geozone_primitive_id	int,
		@primitive_id			int,
		@primitive_vertex_id	int,
		@vertex_id				int,
		@order					int,
		-- 
		@vertex1				varchar(100),
 		@x1						float,
 		@y1						float


	set @map_id = (select MAP_ID from MAPS where GUID = @map_guid)
	/*if(@map_id is null)
		return
	*/
	--
	set @vertex = replace(@vertex, ',', '.')

	
	-- Собственно добавление зоны и ее вершин
	insert dbo.GEO_ZONE ([NAME], [DESCRIPTION], [COLOR]) values (@name, @description, @color) 
	set @zone_id = (select @@identity)

	insert dbo.ZONE_PRIMITIVE ([PRIMITIVE_TYPE], [NAME], [DESCRIPTION], [RADIUS]) values (@type_id, '', '', @radius) 
	set @primitive_id = (select @@identity)

	set @order = 1
	insert dbo.GEO_ZONE_PRIMITIVE ([ZONE_ID], [PRIMITIVE_ID], [ORDER]) values (@zone_id, @primitive_id, @order) 
	set @geozone_primitive_id = (select @@identity)


	set @order = 0
	while (len(@vertex) > 1 and charindex(';', @vertex) > 1) 
	begin
		set @order = @order + 1
		
		-- выделение координат каждой точки и ее направления
		set @vertex1 = substring(@vertex, 1, (charindex(';', @vertex) - 1))
		set @x1 = convert(float, substring(@vertex1, 1, (charindex(':', @vertex1) - 1))) 
		set @vertex1 = substring(@vertex1, (charindex(':', @vertex1) + 1), len(@vertex1))
		set @y1 = convert(float, @vertex1) 

		--запись полученных значений	
		insert dbo.MAP_VERTEX ([MAP_ID], [X], [Y], [VISIBLE], [ENABLE]) values (@map_id, @x1, @y1, 'true', 'true') 
		set @vertex_id = (select @@identity)


		insert dbo.ZONE_PRIMITIVE_VERTEX ([PRIMITIVE_ID], [VERTEX_ID], [ORDER]) values (@primitive_id, @vertex_id, @order) 
		--set @primitive_vertex_id = (select @@identity)

	 
		set @vertex = substring(@vertex, (charindex(';', @vertex) + 1), len(@vertex))
	end

	-- Также добавляем запись в таблицу прав для данного оператора
	--insert dbo.OPERATOR_ZONE ([OPERATOR_ID], [ZONE_ID], [RIGHT_ID], [ALLOWED]) values (@operator_id, @zone_id, 105, 1)

	insert into dbo.OPERATOR_ZONE
	([OPERATOR_ID], [ZONE_ID], [RIGHT_ID], [ALLOWED]) values (@operator_id, @zone_id, 105, 1)

	if @all_operators = 1
	begin
		create table #oper_zone_groups(zonegroup_id int);
		
		insert into #oper_zone_groups(zonegroup_id)
		select ZONEGROUP_ID
		from dbo.OPERATOR_ZONEGROUP where OPERATOR_ID = @operator_id
		union
		select ZONEGROUP_ID
		from dbo.OPERATORGROUP_ZONEGROUP
		where OPERATORGROUP_ID in (select OPERATORGROUP_ID from dbo.OPERATORGROUP_OPERATOR
									where OPERATOR_ID = @operator_id);

		if ((select count(*) from #oper_zone_groups)> 0)
			begin
				insert into dbo.ZONEGROUP_ZONE (ZONEGROUP_ID, ZONE_ID)
				select ZONEGROUP_ID, @zone_id from #oper_zone_groups;
			end
		else	
			begin
				insert into dbo.OPERATORGROUP_ZONE (OPERATORGROUP_ID, ZONE_ID, RIGHT_ID, ALLOWED)
				select OPERATORGROUP_ID,  @zone_id, 105, 1
				from dbo.OPERATORGROUP_OPERATOR
				where OPERATOR_ID = @operator_id

				/*
				create table #ops(operator_id int)

				insert into #ops(operator_id)
				exec GetOperatorsFromGroups @operator_id = @operator_id
			
				insert into dbo.OPERATOR_ZONE
				([OPERATOR_ID], [ZONE_ID], [RIGHT_ID], [ALLOWED])
				select distinct operator_id, @zone_id, 105, 1
				from #ops
				
				drop table #ops
				*/
			end
	end

	-- AS: добавление связи зоны с объектами мониторинга, доступными для данного оператора
	create table #operatorVehicles(
		vehicle_id int primary key clustered
	)
	insert into #operatorVehicles 
		select vehicle_id
			from dbo.v_operator_vehicle_right ovr
			where operator_id = @operator_id
			  and right_id = 102

	insert into dbo.zone_vehicle([ZONE_ID], [VEHICLE_ID])
		select @zone_id, vehicle_id
		from #operatorVehicles;
	
	insert into dbo.ZONE_VEHICLEGROUP (ZONE_ID, VEHICLEGROUP_ID)
			select @zone_id, VEHICLEGROUP_ID
			from dbo.OPERATORGROUP_VEHICLEGROUP 
			where OPERATORGROUP_ID in (select OPERATORGROUP_ID from dbo.OPERATORGROUP_OPERATOR where OPERATOR_ID = @operator_id)
			and ALLOWED = 1
		union 
			select @zone_id, VEHICLEGROUP_ID
			from dbo.OPERATOR_VEHICLEGROUP
			where OPERATOR_ID = @operator_id;

	select @zone_id;
END



