exec CreateProcedure 'AcceptRequestWithLimit'
go
alter procedure AcceptRequestWithLimit
(
	@service varchar(50),
	@ip varchar(50)
) 
as
	declare @accept bit = 1
	declare @acceptDate int = null

	declare @maxCount int
	declare @seconds int
	declare @limitId int 
	select @limitId = l.Id, @seconds=l.Seconds, @maxCount=l.MaxCount from RequestFreqLimit l where l.Name = @service
	if @limitId is not null
	begin
		begin tran
		declare @ipId int = (select Id from Ip i where i.Value = @ip)
		if @ipId is null
		begin
			insert into Ip(Value)values(@ip)
			set @ipId = @@identity
		end
		commit tran

		begin tran
		if not exists (select * from RequestCounter c where c.FreqLimitId = @limitId and c.IpId = @ipId)
			insert into RequestCounter(FreqLimitId, IpId) values (@limitId, @ipId)
		commit tran

		begin tran
		declare @logTime int = datediff(ss, '1970', getutcdate()) - @seconds
		declare @counterValue int
		select @counterValue = (Value + 1), @acceptDate = LogTime + @seconds from RequestCounter where FreqLimitId = @limitId and IpId = @ipId and LogTime >= @logTime
		if @counterValue is null
		begin
			insert into RequestCounterLog(FreqLimitId, IpId, RequestCount, LogTime)
			select @limitId, @ipId, Value, LogTime from RequestCounter where FreqLimitId = @limitId and IpId = @ipId
			update RequestCounter set Value = 1, LogTime = datediff(ss, '1970', getutcdate()) where FreqLimitId = @limitId and IpId = @ipId
		end
		else 
		begin
			update RequestCounter set Value = @counterValue where FreqLimitId = @limitId and IpId = @ipId
			if @counterValue > @maxCount
				set @accept = 0
		end
		commit tran

	end

	select 
		case when @accept = 0 then @acceptDate else null end AcceptDate

	declare @percentage int = 100
	if @accept = 0 and 100 < @counterValue and (@counterValue-1 = @maxCount or (round(log(@counterValue, 1 + @percentage/100.0), 0) > round(log(@counterValue - 1, 1 + @percentage/100.0), 0)))
	begin
		declare @subject nvarchar(max) = N'Ban to ' + @ip + ' ' + convert(nvarchar(100), getutcdate(), 120) + ' on ' + @service
		declare @body nvarchar(max) = N'Count: ' + convert(nvarchar(max), @counterValue)
		declare @email nvarchar(max) = (select VALUE from CONSTANTS where NAME = 'TechnicalSupportEmail')
		exec SendEmail @subject=@subject, @body=@body, @destination_email = @email
	end
go

