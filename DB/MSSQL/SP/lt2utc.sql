﻿if exists (
	select *
		from sys.objects
		where name = 'lt2utc'
)
	drop function lt2utc
go

create function dbo.lt2utc
(
	@log_time int
)
returns datetime
as
begin

	return dbo.GetDateFromInt(@log_Time)

end