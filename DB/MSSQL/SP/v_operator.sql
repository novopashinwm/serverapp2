if exists (
	select *
		from sys.views 
		where name = 'v_operator')
	drop view v_operator
go

create view v_operator
as
	select 
			o.operator_id
	    ,   o.name
		, 	o.login
		,   o.password
		,   Msisdn = oc.Value 
		,   a.SimID
		,   AsidValue = ac.Value
		,   AsidValueValid = ac.Valid
		,   OwnVehicleID = c.VEHICLE_ID
		,   Email = ec.Value
		from Operator o
		LEFT OUTER join v_operator_contact oc on oc.Operator_ID = o.OPERATOR_ID and oc.Type = 2 and oc.Confirmed = 1
		LEFT OUTER join v_operator_contact ec on ec.Operator_ID = o.OPERATOR_ID and ec.Type = 1 and ec.Confirmed = 1
		left outer join Asid a on a.Operator_ID = o.OPERATOR_ID
		left outer join contact ac on ac.id = a.contact_id
		left outer join mlp_controller mc on mc.asid_id = a.ID
		left outer join controller c on c.controller_id = mc.controller_id