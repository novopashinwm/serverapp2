if exists (select * from sys.procedures where name='GetMlpRequestsByOperator')
	drop procedure GetMlpRequestsByOperator
go
create procedure GetMlpRequestsByOperator
(
	@dateFrom datetime,
	@dateTo datetime,
	@departmentId int = null,
	@operatorId int
)
as
declare @validPeriod int = (select cast(value as int) from CONSTANTS where NAME = 'ValidPeriod')

	select 
		ID = c.ID,
		TargetID = c.Target_ID,
		RequestDate = c.Date_Received,
		ResponseDate = c.Result_Date_Received,
		PositionTime = c.Log_Time,
		Sender_Name = o.NAME,
		Sender_Login = o.[LOGIN],
		Sender_Id = o.OPERATOR_ID,
		Longitude = r.Lng,
		Latitude = r.Lat,
		Radius = r.Radius,
		Result = c.Result_Type_ID,
		IsValid = r.IsValid
	from Command c with(nolock)
	join Vehicle v on v.VEHICLE_ID = c.Target_ID
	left join OPERATOR o with(nolock) on o.OPERATOR_ID = c.Sender_ID
	outer apply 
	(
		select 
			geo.Lat, 
			geo.Lng, 
			pr.Radius,
			IsValid = cast(case when geo.LOG_TIME >= c.Log_Time - @validPeriod then 1 else 0 end as bit)
		from (
				select top 1 gl.Lat, gl.Lng, gl.LOG_TIME
				from Geo_Log gl with(nolock) 
				where gl.Vehicle_ID = c.Target_ID and gl.LOG_TIME <= c.Log_Time
				order by gl.LOG_TIME desc
			) geo
			left join Position_Radius pr with(nolock) 
				on pr.Vehicle_ID = c.Target_ID and pr.Log_Time = geo.Log_Time
	) r
	where c.[Type_ID] = 20 and c.[Status] = 2 and c.Sender_ID = @operatorId
		and (v.DEPARTMENT = @departmentId or @departmentId is null)
		and c.Date_Received >= @dateFrom and c.Date_Received <= @dateTo
		and exists (select 1 from v_operator_vehicle_right r where r.vehicle_id = c.Target_ID and r.operator_id = @operatorId and r.right_id in (102))
