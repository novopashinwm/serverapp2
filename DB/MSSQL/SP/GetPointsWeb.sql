
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M. Grebennikov 
-- Create date: <Create Date,,>
-- Description:	Get Points for Web
-- =============================================
alter PROCEDURE [dbo].[GetPointsWeb]  
	--@map_id			int,
	@map_guid		uniqueidentifier,
	@operator_id	int
AS
BEGIN
	
	declare @map_id	int
	--if(@map_id is null)
		set @map_id = (select MAP_ID from MAPS where GUID = @map_guid)
	
	select --* 
		WP.[POINT_ID],
		WP.[NAME],
		WP.[DESCRIPTION],
		WPT.[TYPE_ID],
		WPT.[NAME]			as [TYPE],
		WPT.[DESCRIPTION]	as TYPE_DESCRIPTION,
		V.X,
		V.Y,
		Editable = convert(bit,
			case 
				when wp.Operator_ID is not null 
						or 
					 exists (
						select 1 
							from v_operator_rights [or] 
							where [or].operator_id = @operator_id 
							  and [or].right_id = 22) 
			then 1 
			else 0 
			end)
	from [dbo].[WEB_POINT] as WP
		inner join MAP_VERTEX as V on WP.VERTEX_ID = V.VERTEX_ID 
		left join MAPS as M on V.MAP_ID = M.MAP_ID
		left join WEB_POINT_TYPE as WPT on WPT.TYPE_ID = WP.TYPE_ID
	where 
		(@map_id is not null and M.MAP_ID = @map_id or @map_id is null and m.map_id is null)
		and (WP.OPERATOR_ID = @operator_id or WP.Operator_ID is null)
		
END
GO
