IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'GetVehicleProfile'
        AND 	  type = 'P')
    DROP PROCEDURE GetVehicleProfile
GO


CREATE PROCEDURE [dbo].[GetVehicleProfile]
	@vehicle_id int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1 PICTURE from [VEHICLE_PICTURE] where VEHICLE_ID = @vehicle_id

	declare @fuel_spent_standart decimal(18,4);
	select @fuel_spent_standart = [dbo].[GetRuleValue](@vehicle_id, 10);

	declare @fuel_spent_standard_mh decimal(18,4);
	select @fuel_spent_standard_mh = [dbo].[GetRuleValue](@vehicle_id, 12);

	SELECT v.[PUBLIC_NUMBER],
		   v.[GARAGE_NUMBER],
		   v.[VEHICLE_TYPE], 
		   isnull(@fuel_spent_standart, 0) 'FUEL_SPENT_STANDART',
		   isnull(@fuel_spent_standard_mh, 0) 'FUEL_SPENT_STANDARD_MH',
		   [VEHICLE_KIND_ID],
		   v.Fuel_Type_ID 'Fuel_Type_ID',
		   t.Unit_Id 'Fuel_Unit_Id',
		   t.Tank_Unit_id 'Fuel_Tank_Unit_Id',
		   v.Fuel_Tank,
		   v.FuelSpendStandardKilometer,
		   v.FuelSpendStandardLiter,
		   c.Name as departmentCountryName,
		   v.MaxAllowedSpeed
		   
	from VEHICLE v
		LEFT JOIN dbo.DEPARTMENT d ON ISNULL(v.DEPARTMENT,0) = d.DEPARTMENT_ID
		LEFT JOIN dbo.Country c ON c.Country_ID = ISNULL(d.Country_ID,0)
		LEFT JOIN dbo.Fuel_Type t on t.ID = v.Fuel_Type_ID
	where v.VEHICLE_ID = @vehicle_id

	SELECT o.[FIRST_NAME],o.[SECOND_NAME], o.[LAST_NAME], o.[PHONE_NUMBER1], o.[PHONE_NUMBER2], o.[PHONE_NUMBER3]
		 FROM [OWNER] o join VEHICLE_OWNER v on o.[OWNER_ID] = v.[OWNER_ID] 
			where v.VEHICLE_ID = @vehicle_id and v.OWNER_NUMBER = 1

	SELECT PHONE = controllerPhone.Value
	    , PhoneIsEditable = convert(bit, isnull(controllerPhone.Editable, 1))
		, c.[NUMBER]
		, CAST(ci.DEVICE_ID AS varchar(32)) as deviceId
		, ci.[PASSWORD]
		, ci.IP
		, c.CONTROLLER_TYPE_ID
		, ct.[TYPE_NAME]
		, HasInputs = CONVERT(bit, case when exists (select * from Controller_Sensor cs where cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID) then 1 else 0 end)
		, ct.SupportsPassword
		, ct.DeviceIdIsRequiredForSetup
		, ci.IMSI
		FROM CONTROLLER c 
		join CONTROLLER_TYPE ct ON c.[CONTROLLER_TYPE_ID] = ct.[CONTROLLER_TYPE_ID]
		join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
		outer apply (
			select top(1) *
				from (
					select [Order] = 2
						, phoneContact.Value
						, Editable = CONVERT(bit, 1)
						from MLP_Controller mc
						join Asid a on a.ID = mc.Asid_ID
						join Contact asidContact on asidContact.ID = a.Contact_ID
						join Contact phoneContact on phoneContact.ID = asidContact.Demasked_ID
						where mc.Controller_ID = c.CONTROLLER_ID
					union all
					select [Order] = 1, Value = c.PHONE, Editable = 1 
				) controllerPhone
			order by controllerPhone.[Order] asc
		) controllerPhone
		
		left outer join CONTROLLER_INFO ci ON c.CONTROLLER_ID = ci.CONTROLLER_ID
			where c.[VEHICLE_ID] = @vehicle_id

	-- AS: дополнение - вытащим все свойства объекта мониторинга
	exec [dbo].getVehicleProperties @vehicle_id
END
