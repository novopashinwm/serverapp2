/****** Object:  StoredProcedure [dbo].[GetSortedDriver]    Script Date: 07/08/2010 18:20:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSortedDriver]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetSortedDriver]
GO

/****** Object:  StoredProcedure [dbo].[GetSortedDriver]    Script Date: 07/08/2010 18:20:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetSortedDriver]
	@operator_id int = null
as
begin
	select d.* 
	from DRIVER d
	join v_operator_driver_right dr on dr.driver_id = d.DRIVER_ID
	where @operator_id is null or (dr.operator_id = @operator_id and dr.right_id = 100) --DriverAccess
	order by EXT_NUMBER
end












GO


