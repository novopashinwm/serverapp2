if exists (select * from sys.views where name = 'v_message_field')
	drop view v_message_field
go

create view v_message_field
as
	select mf.MESSAGE_ID, mtf.NAME, mf.CONTENT, mf.LargeContent, mf.MESSAGE_FIELD_ID
		from message_field mf
		join MESSAGE_TEMPLATE_FIELD mtf on mtf.MESSAGE_TEMPLATE_FIELD_ID = mf.MESSAGE_Template_FIELD_ID
