﻿if exists (select * from sys.objects where name='GetMessagesByAppId' and type='P')
	drop procedure GetMessagesByAppId
go
create procedure GetMessagesByAppId
(
	@operatorId int,
	@appId varchar(255),
	@lastMessageId int = null,
	@from datetime = null,
	@to datetime = null,
	@skip int = null,
	@count int = null
) as
	set nocount on
	if @skip is null set @skip = 0

	-- Для GenericNotification
	declare @template_field_date int = 
	(
		select MESSAGE_TEMPLATE_FIELD_ID
		from MESSAGE_TEMPLATE_FIELD mtf
			join MESSAGE_TEMPLATE mt on mtf.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID
		where mt.NAME = 'GenericNotification' and mtf.NAME = 'date'
	)
	-- Для PositionRequest
	declare @position_request_id int = 
	(
		select MESSAGE_TEMPLATE_ID
		from MESSAGE_TEMPLATE
		where NAME = 'PositionRequest'
	)

	;with cte as 
	(
		select m.MESSAGE_ID, convert(datetime, mf.CONTENT) RegistrationDate
		from [MESSAGE] m with (nolock)
			join Message_Contact mc on m.MESSAGE_ID = mc.Message_ID
			join AppClient ac on ac.Contact_ID = mc.Contact_ID
			join MESSAGE_FIELD mf on mf.MESSAGE_ID = m.MESSAGE_ID
		where mc.[Type] = 1 and ac.AppId = @appId and mf.MESSAGE_TEMPLATE_FIELD_ID = @template_field_date
			and (m.MESSAGE_ID > @lastMessageId or @lastMessageId is null)
			and (convert(datetime, mf.CONTENT) > @from or @from is null) 
			and (convert(datetime, mf.CONTENT) <= @to or @to is null)
			and [Status] = 3

		union all

		select m.MESSAGE_ID, m.[TIME] RegistrationDate
		from [MESSAGE] m with (nolock)
			join Message_Contact mc on m.MESSAGE_ID = mc.Message_ID
			join AppClient ac on ac.Contact_ID = mc.Contact_ID
		where mc.[Type] = 1 and ac.AppId = @appId and m.TEMPLATE_ID = @position_request_id
			and (m.MESSAGE_ID > @lastMessageId or @lastMessageId is null)
			and (m.[TIME] > @from or @from is null) 
			and (m.[TIME] <= @to or @to is null)
			and [Status] = 3
	)
	select Message_ID, ROW_NUMBER() over (order by RegistrationDate desc) number into #ids from cte
	create clustered index IX_Ids on #ids(number)

	select count(*) from #ids

	select m.*, i.number SORT_KEY
	from #ids i 
		join [MESSAGE] m on m.MESSAGE_ID=i.MESSAGE_ID
	where (i.number > @skip and i.number <= @skip + @count) or @count is null
	order by i.number

	select count(mf.MESSAGE_FIELD_ID)
	from MESSAGE_FIELD mf
		join #ids i on i.MESSAGE_ID = mf.MESSAGE_ID
	where (i.number > @skip and i.number <= @skip + @count) or @count is null

	select mf.*, i.number SORT_KEY
	from MESSAGE_FIELD mf
		join #ids i on i.MESSAGE_ID = mf.MESSAGE_ID
	where (i.number > @skip and i.number <= @skip + @count) or @count is null

	drop table #ids
go