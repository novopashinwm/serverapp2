if exists (
	select * from sys.procedures where name = 'GetRandomString'
)
	drop procedure GetRandomString
go

/*
declare @result varchar(255)
exec GetRandomString 10, @result out, 'abc'
select @result
*/
create procedure GetRandomString (
	@length int, 
	@result varchar(255) out,
	@alphabet varchar(255) = 'qwertyuiopasdfghjklzxcvbnm1234567890'
)

as
begin

	if (@length < 0 or 255 < @length)
		raiserror (0, -1, -1, 'Parameter is out of range: @length');
	if (len(@alphabet) < 2)
		raiserror (0, -1, -1, 'Parameter is out of range: @alphabet');

	set @result = ''
	declare @i int = 0
	while @i <> @length
	begin
		set @result = @result + SUBSTRING(@alphabet, 1 + convert(int, rand()*len(@alphabet)), 1)
		set @i = @i + 1
	end

end