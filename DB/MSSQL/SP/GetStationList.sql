/****** Object:  StoredProcedure [dbo].[GetStationList]    Script Date: 07/15/2010 10:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetStationList]
	@Date 		datetime, 	-- -ЁкЁ, ¤Ё ·■к■и║ж киїё║їк╕а  ■√║в°к╣ ЇЁ¤¤vї
	@RouteID 	int,		-- route id
	@operator_id int = null
AS

--------------------------------------------------------------------------------
/*-- Tї╕к
declare @Date datetime, @RouteID int, @BusstopID int		
set @Date = '08/30/2006 20:00' --'01/26/2006 21:00' --'01/16/2006 21:00'--'08/09/2006 05:00'
set @RouteID = 31--9--7--8*/
--------------------------------------------------------------------------------

-- ЇЁкЁ Є √Ё·Ё√╣¤■№ Є°Її (Ї√а п-б°° DayKind())
declare @LocalDate datetime
set @LocalDate = dateadd(hh, 24-datepart(hh, @Date), @Date)


-- L¤п■и№Ёб°а  ■ Єv┐■ЇЁ№ 
select distinct 
	RT.ROUTE_ID,
	RT.EXT_NUMBER 		RouteNumber, 	-- ¤■№їи №Ёи░и║кЁ
	SC.EXT_NUMBER,				-- Єv┐■Ї
	--SC.SCHEDULE_ID,			-- ID иЁ╕ °╕Ё¤°а
	SC.BEGIN_TIME 		SCHEDULE_BEGIN,	-- Єиї№а ¤ЁвЁ√Ё Єv┐■ЇЁ
	SC.END_TIME 		SCHEDULE_END,	-- Єиї№а ■·■¤вЁ¤°а Єv┐■ЇЁ
	SCD.SHIFT_ID,				-- ╕№ї¤Ё 
	SCD.BEGIN_TIME		SHIFT_BEGIN,	-- Єиї№а ¤ЁвЁ√Ё ╕№ї¤v 
	SCD.END_TIME		SHIFT_END,	-- Єиї№а ■·■¤вЁ¤°а ╕№ї¤v 
	--,DR.[SHORT_NAME]
	count(*)		TRIPS_COUNT
from 
	dbo.ROUTE as RT
	inner join dbo.SCHEDULE as SC on RT.ROUTE_ID = SC.ROUTE_ID
	inner join dbo.SCHEDULE_DETAIL as SCD on SC.SCHEDULE_ID = SCD.SCHEDULE_ID
	join trip t on scd.schedule_detail_id = t.schedule_detail_id and t.trip_kind_id in (5, 6) --(2, 3, 5, 6, 7)
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where	
	(@RouteID is null or RT.ROUTE_ID = @RouteID)
	and SC.DAY_KIND_ID = dbo.DayKind(@LocalDate, RT.ROUTE_ID)
	and (@operator_id is null or rr.operator_id = @operator_id)
	--and WBH.WAYBILL_DATE = @Date 
group by RT.ROUTE_ID, RT.EXT_NUMBER, SC.EXT_NUMBER, SC.BEGIN_TIME, SC.END_TIME, SCD.SHIFT_ID, SCD.BEGIN_TIME, SCD.END_TIME
order by 
	RT.EXT_NUMBER, SC.EXT_NUMBER, SCD.SHIFT_ID

-- °¤пЁи№Ёб°а  ■ иї∙╕Ё№ ° Єиї№ї¤°  и■┐■ЎЇї¤°а вїиїў к■в·║ (¤Ёё°иЁї№ Є╕ї иї∙╕v ¤ї ўЁЄ°╕°№■ ■к к■в·°, вк■ёv  ■√║в°к╣ ■ёїЇv)
select distinct 
	RT.ROUTE_ID,
	RT.EXT_NUMBER 		RouteNumber, 	-- ¤■№їи №Ёи░и║кЁ
	SC.EXT_NUMBER,				-- Єv┐■Ї
	TR.TRIP,				-- иї∙╕
	TR.BEGIN_TIME		TRIP_BEGIN,	-- Єиї№а ¤ЁвЁ√Ё иї∙╕Ё
	TR.END_TIME		TRIP_END,	-- Єиї№а ·■¤бЁ иї∙╕Ё
	TR.[TRIP_KIND_ID],			-- к°  иї∙╕Ё
	SCP.TIME_IN,				-- Єиї№а Є┐■ЇЁ Є к■в·║
	SCP.TIME_OUT, 				-- Єиї№а Єv┐■ЇЁ °ў к■в·°
	--,SCP.[STATUS]
	BS.BUSSTOP_ID		BUSSTOP		-- ■╕кЁ¤■Є·Ё
	,PT.[NAME]
	,PT.[POINT_ID]
	,GT.[POINTA]
	,GT.[POINTB]
	--,GS.[STATUS]		StatusDirect
	,(GS.[STATUS] & 1) | (GS.[STATUS] & 4)	StatusDirect

from 
	dbo.SCHEDULE_POINT as SCP
	-- ╕Єаў╣ ╕ кЁё√°бї∙ иЁ╕ °╕Ё¤°∙ ° №Ёи░и║к■Є
	inner join dbo.TRIP as TR on SCP.TRIP_ID = TR.TRIP_ID
	inner join dbo.SCHEDULE_DETAIL as SCD on TR.SCHEDULE_DETAIL_ID = SCD.SCHEDULE_DETAIL_ID
	inner join dbo.SCHEDULE as SC on SCD.SCHEDULE_ID = SC.SCHEDULE_ID
	inner join dbo.ROUTE as RT on RT.ROUTE_ID = SC.ROUTE_ID
	-- ╕Єаў╣ ╕ кЁё√°бї∙ к■вї·
	inner join dbo.GEO_SEGMENT as GS on SCP.GEO_SEGMENT_ID = GS.GEO_SEGMENT_ID
	inner join dbo.POINT as PT on GS.POINT = PT.POINT_ID
	left join dbo.BUSSTOP as BS on PT.BUSSTOP_ID = BS.BUSSTOP_ID
	-- ╕Єаў╣ ╕ кЁё√°бї∙ єї■иї∙╕■Є
	inner join dbo.GEO_TRIP as GT on GT.GEO_TRIP_ID = GS.GEO_TRIP_ID
	left join v_operator_route_right rr on rr.route_id = RT.ROUTE_ID
where	
	(@RouteID is null or RT.ROUTE_ID = @RouteID)
	and TR.[TRIP_KIND_ID] in (2, 3, 5, 6) --(3, 4, 5, 6) -- ■ёїЇ, ┐■√■╕к■∙, ·■и■к·°∙, иї∙╕
	and SC.DAY_KIND_ID = dbo.DayKind(@LocalDate, RT.ROUTE_ID)
	and (@operator_id is null or rr.operator_id = @operator_id)
order by 
	--RT.EXT_NUMBER, SCP.TIME_IN, SCP.TIME_OUT  --isnull(SCP.TIME_IN, SCP.TIME_OUT)
	SC.EXT_NUMBER, TRIP

