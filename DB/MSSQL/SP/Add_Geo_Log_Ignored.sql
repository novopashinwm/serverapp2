﻿IF (OBJECT_ID(N'[dbo].[Add_Geo_Log_Ignored]') IS NOT NULL)
	DROP PROCEDURE [dbo].[Add_Geo_Log_Ignored];
GO

CREATE PROCEDURE [dbo].[Add_Geo_Log_Ignored]
(
	@Vehicle_ID int,
	@Log_Time   int,
	@Reason     tinyint,
	@Lat        numeric(8,5),
	@Lng        numeric(8,5),
	@Speed      int          = NULL,
	@Course     int          = NULL,
	@Height     int          = NULL,
	@HDOP       numeric(4,2) = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	IF (
			@Lat IS NULL OR @Lng IS NULL
			OR
			@Speed < 0 OR 32000 < @Speed
			OR
			@Course < -180 OR 180 < @Course
			OR
			@Height < -32000 OR 32000 < @Height
		)
			RETURN;

	INSERT [dbo].[Geo_Log_Ignored]
		(Vehicle_ID,  Log_Time,  Reason,  Lng,  Lat,  Speed,  Course,  Height,  HDOP)
	SELECT
		@Vehicle_ID, @Log_Time, @Reason, @Lng, @Lat, @Speed, @Course, @Height, @HDOP
	WHERE NOT EXISTS
	(
		SELECT 1
		FROM [dbo].[Geo_Log_Ignored] WITH (XLOCK, SERIALIZABLE)
		WHERE [Vehicle_ID] = @Vehicle_ID
		AND   [Log_Time]   = @Log_Time
		AND   [Reason]     = @Reason
	)
END