IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVehicleGroups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetVehicleGroups]
GO

/*
exec GetVehicleGroups 4
exec GetVehicleGroups -1
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetVehicleGroups]
	@operator_id int
as
	set nocount on;
	
	if (@operator_id > 0)
		select gr.vehiclegroup_id, [name], [comment]
		from v_operator_vehicle_groups_right gr
		join vehiclegroup g on g.vehiclegroup_id = gr.vehiclegroup_id
		where gr.operator_id = @operator_id 
		and gr.right_id = 102
		order by [name];
	else
		select vehiclegroup_id, [name], [comment]
		from vehiclegroup
		order by [name];


