IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRunLogForVehicle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRunLogForVehicle]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetRunLogForVehicle]  
(  
 @vehicle_id  int,  
 @from   int,  
 @to    int  
)  
as  

select * 
	from (
		select top(1) sl.log_Time, sl.odometer
			from statistic_log sl with (nolock)
			where sl.Vehicle_ID = @vehicle_id
			  and sl.Log_Time < @from
			order by sl.Log_Time desc
	) t
union all
select
	  sl.log_time
	, sl.odometer
	from statistic_log sl with (nolock) 
	where sl.vehicle_id = @vehicle_id
	  and sl.Log_Time between @from and @to

GO
