if exists (
	select *
		from sys.procedures
		where name = 'GetPhoneID'
)
	drop procedure GetPhoneID;
go

create procedure GetPhoneID
(
	@msisdn varchar(20)
)
as

set nocount on

declare @invariant varchar(20) = dbo.GetPhoneInvariant(@msisdn)

if @invariant is not null
	set @msisdn = @invariant

declare @phone_id int

if @msisdn is not null 
begin
	set @phone_id = (select top(1) p.phone_id from Phone p where p.Phone = @msisdn and p.Operator_ID is null order by p.Phone_ID desc)

	if @phone_id is null 
	begin

		insert into Phone (Phone)
			select @msisdn 
				where not exists (select p.phone_id from Phone p where p.Phone = @msisdn and p.Operator_ID is null)
				
		set @phone_id = 
			case 
				when @@ROWCOUNT <> 0 then @@IDENTITY 
				else (select top(1) p.phone_id from Phone p where p.Phone = @msisdn and p.Operator_ID is null order by p.Phone_ID desc)
			end
	end
end

select Phone_ID = @phone_id