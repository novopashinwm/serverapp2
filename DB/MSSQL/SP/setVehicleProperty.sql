/**
AS: �������� �������� ������� ����������� �� ������� vehicle_profile

���������� getVehicleProperties ������ ��� getVehicleProfile ��� ����
� ������� ���������� ���� �� ������� - ����������� (�����) ������,
�������� ��������� � �.�.
**/

IF EXISTS (SELECT name 
		FROM	sysobjects
        WHERE	name = N'setVehicleProperty' AND type = 'P')
    DROP PROCEDURE setVehicleProperty
GO


CREATE PROCEDURE [dbo].[setVehicleProperty]
	@vehicle_id int,
	@property_name varchar(50),
	@property_value nvarchar(max) = null
AS
begin
	if(@property_value is null)
	begin
		delete from [dbo].vehicle_profile
		where vehicle_id = @vehicle_id
		and property_name = @property_name
	end
	else
	begin
		if(
			not exists (select * from [dbo].vehicle_profile
			where vehicle_id = @vehicle_id
			and property_name = @property_name)
		)
		begin
			insert into [dbo].vehicle_profile(vehicle_id, property_name, value)
			values(@vehicle_id, @property_name, @property_value)
		end
		else
		begin
			update [dbo].vehicle_profile set value = @property_value
			where vehicle_id = @vehicle_id
			and property_name = @property_name
		end
	end
end
