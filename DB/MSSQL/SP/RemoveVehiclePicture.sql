-- AS: удаление изображения транспортного средства

IF EXISTS (SELECT name
        FROM   sysobjects
        WHERE  name = N'RemoveVehiclePicture'
        AND 	  type = 'P')
    DROP PROCEDURE RemoveVehiclePicture
GO
CREATE PROCEDURE dbo.RemoveVehiclePicture
    @VEHICLE_ID int
	--,@PICTURE image
AS
BEGIN
	delete from dbo.[vehicle_picture] where vehicle_id = @VEHICLE_ID
END

GO
