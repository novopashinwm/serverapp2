if exists (select * from sys.objects where object_id = object_id('dbo.GetLastRuleCheckDate') and type = 'p')
	drop procedure dbo.GetLastRuleCheckDate;
go

create procedure dbo.GetLastRuleCheckDate
as
set nocount on
	
select MIN(LastTime) 
	from Vehicle_Rule_Processing vrp
