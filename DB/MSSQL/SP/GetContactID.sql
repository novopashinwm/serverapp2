if exists (
	select * from sys.procedures where name = 'GetContactID'
)
	drop procedure GetContactID
go

create procedure GetContactID
(
	@type int,
	@value nvarchar(255)
)
as

set nocount on

if (@type = 2) --Phone
begin
	declare @invariant varchar(20) = dbo.GetPhoneInvariant(@value)

	if @invariant is not null
		set @value = @invariant
end

if (@type is null or @value is null)
	return;

declare @result int = (select c.ID from Contact c where c.Type = @type and c.Value = @value)

if @result is null
begin		  
	insert into Contact (Type, Value)
		select @type, @value 
			where not exists (select * from Contact c with (xlock, serializable) where c.Type = @type and c.Value = @value)
	
	if @@ROWCOUNT <> 0
	begin
		set @result = @@IDENTITY
		
		if @type = 3 --asid
		begin
			exec dbo.CreateDemaskingMessage @result
		end
	end
	else
	begin
		set @result = (select c.ID from Contact c where c.Type = @type and c.Value = @value)
	end
end

select @result