if exists (select * from sys.objects where object_id = object_id('[dbo].[GetEmulators]'))
	drop procedure [dbo].[GetEmulators]
go

create procedure [dbo].[GetEmulators]
as

set nocount on;


select
	  e.RealVehicleID
	, e.FakeVehicleID
	, e.DateFrom
	, e.DateTo
	, e.FakeLogTimeFrom
	, LastLogTime = (select top(1) lt.Log_Time 
						from dbo.Log_Time lt with (nolock) 
						where lt.Vehicle_ID = e.FakeVehicleID 
						order by lt.Log_Time desc)
	, e.FakeLat
	, e.FakeLng
from dbo.Emulator e
where e.DateFrom is not null 
  and e.DateTo is not null 
  and e.FakeLogTimeFrom is not null
  and e.Enabled = 1
  and exists (select 1 from Emulator_Source src where src.Vehicle_ID = e.RealVehicleID and src.Log_Time between e.DateFrom and e.DateTo)