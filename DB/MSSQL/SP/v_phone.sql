if exists (
	select *
		from sys.views 
		where name = 'v_phone'
)
	drop view v_phone
go

create view v_phone
as
select Phone_ID = pc.ID
     , Msisdn = pc.Value
     , Asid_ID = ac.ID
     , FMsisdn = ac.Value
	 , AsidValid = ac.Valid
	 , AsidCreated = ac.Created
from Contact pc
left outer join Contact ac on ac.Demasked_ID = pc.ID and ac.Type = 3
where pc.Type = 2
union all
select Phone_ID = null
     , Msisdn = null
     , Asid_ID = ac.ID
     , FMsisdn = ac.Value
	 , AsidValid = ac.Valid
	 , AsidCreated = ac.Created
from Contact ac
where ac.Type = 3 and ac.Demasked_ID is null
