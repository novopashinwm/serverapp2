if exists (
	select *
		from sys.objects
		where name = 'msk2utc'
)
	drop function msk2utc
go

create function dbo.msk2utc
(
	@datetime datetime
)
returns datetime
as
begin

	return dateadd(hour, -3, @datetime)

end