exec AddOrUpdateControllerSensor 'Navtelecom', 8, 'EventIndex', 0

exec AddSensorLegend 251, 'WeakBlow', 1
exec AddSensorLegend 252, 'StrongBlow', 1
exec AddSensorLegend 253, 'Inclination', 1

exec AddOrUpdateControllerSensor 'Navtelecom', 1020, 'SH1_WeakBlow',	1, 'WeakBlow',		1, 1, 0
exec AddOrUpdateControllerSensor 'Navtelecom', 1021, 'SH2_StrongBlow',	1, 'StrongBlow',	1, 1, 0
exec AddOrUpdateControllerSensor 'Navtelecom', 1022, 'SH3_Move',		1, 'Motion',		1, 1, 0
exec AddOrUpdateControllerSensor 'Navtelecom', 1023, 'SH4_Inclination',	1, 'Inclination',	1, 1, 0

exec AddOrUpdateControllerSensor 'Navtelecom', 21, 'Ain1', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 22, 'Ain2', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 23, 'Ain3', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 24, 'Ain4', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 25, 'Ain5', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 26, 'Ain6', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 27, 'Ain7', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 28, 'Ain8', 0

exec AddOrUpdateControllerSensor 'Navtelecom', 35, 'FuelSensorFreq1', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 36, 'FuelSensorFreq2', 0

exec AddOrUpdateControllerSensor 'Navtelecom', 38, 'FuelSensor1Level_RS485', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 39, 'FuelSensor2Level_RS485', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 40, 'FuelSensor3Level_RS485', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 41, 'FuelSensor4Level_RS485', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 42, 'FuelSensor5Level_RS485', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 43, 'FuelSensor6Level_RS485', 0

exec AddOrUpdateControllerSensor 'Navtelecom', 44, 'FuelSensor6Level_RS232', 0

exec AddOrUpdateControllerSensor 'Navtelecom', 45, 'TemperatureSensor1', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 46, 'TemperatureSensor2', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 47, 'TemperatureSensor3', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 48, 'TemperatureSensor4', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 49, 'TemperatureSensor5', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 50, 'TemperatureSensor6', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 51, 'TemperatureSensor7', 0
exec AddOrUpdateControllerSensor 'Navtelecom', 52, 'TemperatureSensor8', 0
