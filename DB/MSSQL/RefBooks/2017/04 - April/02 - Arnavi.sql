exec AddControllerType	@type_name = 'Arnavi', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = null

exec AddOrUpdateControllerSensor 'Arnavi', 151, 'HDOP', 0, 'HDOP', 1, 0.01, 0

exec AddOrUpdateControllerSensor 'Arnavi', 1000, 'InputIN0'		, 1
exec AddOrUpdateControllerSensor 'Arnavi', 1001, 'InputIN1'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1002, 'InputIN2'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1003, 'InputIN3'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1004, 'InputIN4'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1005, 'InputIN5'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1006, 'InputIN6'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1007, 'InputIN7'     , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1010, 'OutputOUT0'   , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1011, 'OutputOUT1'   , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1012, 'OutputOUT2'   , 1
exec AddOrUpdateControllerSensor 'Arnavi', 1013, 'OutputOUT3'   , 1
exec AddOrUpdateControllerSensor 'Arnavi', 10	, 'IMPS_0'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 11	, 'IMPS_1'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 12	, 'IMPS_2'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 13	, 'IMPS_3'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 14	, 'IMPS_4'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 15	, 'IMPS_5'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 16	, 'IMPS_6'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 17	, 'IMPS_7'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 20	, 'FREQ_0'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 21	, 'FREQ_1'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 22	, 'FREQ_2'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 23	, 'FREQ_3'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 24	, 'FREQ_4'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 25	, 'FREQ_5'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 26	, 'FREQ_6'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 27	, 'FREQ_7'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 30	, 'VOLT_0'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 31	, 'VOLT_1'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 32	, 'VOLT_2'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 33	, 'VOLT_3'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 34	, 'VOLT_4'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 35	, 'VOLT_5'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 36	, 'VOLT_6'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 37	, 'VOLT_7'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 40	, 'TEMP_0'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 41	, 'TEMP_1'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 42	, 'TEMP_2'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 43	, 'TEMP_3'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 44	, 'TEMP_4'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 45	, 'TEMP_5'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 46	, 'TEMP_6'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 47	, 'TEMP_7'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 48	, 'TEMP_8'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 49	, 'TEMP_9'      , 0
exec AddOrUpdateControllerSensor 'Arnavi', 70	, 'LLS_0'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 71	, 'LLS_1'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 72	, 'LLS_2'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 73	, 'LLS_3'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 74	, 'LLS_4'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 75	, 'LLS_5'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 76	, 'LLS_6'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 77	, 'LLS_7'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 78	, 'LLS_8'       , 0
exec AddOrUpdateControllerSensor 'Arnavi', 79	, 'LLS_9'       , 0

