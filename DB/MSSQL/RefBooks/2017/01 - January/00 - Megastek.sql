﻿declare @t table (i int identity, name varchar(50), photo varchar(255))

insert into @t (name, photo)
	values
		('Megastek MT60', 'Megastek_MT60.png'),
		('Megastek MT70', 'Megastek_MT70.jpg'),
		('Megastek MT90', 'Megastek_MT90.jpg')

declare @i int = 1

while @i <= (select count(1) from @t)
begin

	declare @name varchar(50)
	declare @photo varchar(255)

	select @name = name
		, @photo = case when len(photo) <> 0 then '/about/includes/sources/images/devices/Megastek/' + photo else null end
		from @t
		where i = @i
		
	exec AddControllerType	@type_name = @name, @DeviceIdIsImei = 1, @SupportsPassword = 1, @AllowedToAddByCustomer = 1, @ImagePath = @photo
	
	exec AddControllerTypeCommand 	@controllerTypeName = @name,	@commandTypeCode = 'Setup'

	exec AddOrUpdateControllerSensor @name, 6, 'Sos', 1, 'Тревога', 1
	exec AddOrUpdateControllerSensor @name, 11, 'BatteryLevel', 0, 'BatteryLevel', 1
	exec AddOrUpdateControllerSensor @name, 15, 'IsPlugged', 1, 'IsPlugged', 1
	
	update controller_type set DeviceIdStartIndex = 5, DeviceIdLength = 10 where type_name = @name	

	set @i += 1

end

