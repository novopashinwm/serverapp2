-- ImagePath ��� ��������� ��������
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			 (N'TK STAR XE208',  N'~/img/devices/TKSTAR/TKSTARXE208.png')
			,(N'Vjoy Car T580',  N'~/img/devices/VjoyCar/VjoyCarT580.png')
	) AS SRC ([TYPE_NAME], [ImagePath])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED AND DST.[ImagePath] <> SRC.[ImagePath] THEN
		UPDATE
			SET
				DST.[ImagePath] = SRC.[ImagePath]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO