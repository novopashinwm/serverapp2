﻿DECLARE @CONTROLLER_TYPE_NAME nvarchar(50) = 'SinoTrack ST-901'
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	-- Удалить команды у контроллера
	--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
	-- Добавить команды к контроллеру
	--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Setup'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Immobilize'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Deimmobilize'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Status'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetSos'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'DeleteSOS'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskPositionOverMLP'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ReloadDevice'
	------------------------------------------------------------------
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'PowerAlarmOn'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'PowerAlarmOff'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ShakeAlarmOn'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ShakeAlarmOff'
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO