DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;

-- <summary> Включить режим смс оповещение при прекращении подачи внешнего питания </summary>
-- PowerAlarmOn       = 58,
SELECT
	@id                 = 58,
	@code               = N'PowerAlarmOn',
	@description        = N'Turn on alert when external power is cut off',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Выключить режим смс оповещение при прекращении подачи внешнего питания </summary>
-- PowerAlarmOff      = 59,
SELECT
	@id                 = 59,
	@code               = N'PowerAlarmOff',
	@description        = N'Turn off alert when external power is cut off',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Включить режим смс оповещение при срабатывании датчика вибрации </summary>
-- ShakeAlarmOn       = 60,
SELECT
	@id                 = 60,
	@code               = N'ShakeAlarmOn',
	@description        = N'Turn on alert when shake sensor triggers',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Выключить режим смс оповещение при  срабатывании датчика вибрации </summary>
-- ShakeAlarmOff      = 61,
SELECT
	@id                 = 61,
	@code               = N'ShakeAlarmOff',
	@description        = N'Turn off alert when shake sensor triggers',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds