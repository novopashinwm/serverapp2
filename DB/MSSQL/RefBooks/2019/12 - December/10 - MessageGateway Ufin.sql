-- ��������� ������ ��������� ��� �������� Ufin
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[MessageGateway]
	------------------------------------------------------------------
	MERGE [dbo].[MessageGateway] AS DST
	USING
	(
		VALUES
			 (01, N'RedHatSmsGateway', 1, 2, '^91\d{8,13}$') -- �����
			,(02, N'Smpp',             1, 2, '^79\d{9}$' )   -- ������
	) AS SRC ([ID], [Name], [Enabled], [ContactType_Id], [ContactRegex])
		ON  DST.[ID] = SRC.[ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[Name]           = SRC.[Name]
				,DST.[Enabled]        = SRC.[Enabled]
				,DST.[ContactType_Id] = SRC.[ContactType_Id]
				,DST.[ContactRegex]   = SRC.[ContactRegex]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			 [ID]
			,[Name]
			,[Enabled]
			,[ContactType_Id]
			,[ContactRegex]
		)
		VALUES
		(
			 SRC.[ID]
			,SRC.[Name]
			,SRC.[Enabled]
			,SRC.[ContactType_Id]
			,SRC.[ContactRegex]
		)
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[MessageGateway]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO