BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [CONTROLLER_TYPE_ID]-- [TYPE_NAME]
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			 (N'Anywhere TK106', N'', N'~/img/devices/Anywhere/AnywhereTK106.jpg', N'~/img/devices/Anywhere/AnywhereTK106.png')
			,(N'Anywhere TK108', N'', N'~/img/devices/Anywhere/AnywhereTK108.jpg', N'~/img/devices/Anywhere/AnywhereTK108.png')
			,(N'Arnavi', N'', N'', N'~/img/devices/Arnavi/Arnavi3.png')
			,(N'ASC6 GLONASS/GPS', N'ASC 6 (GLONASS/GPS)', N'', N'~/img/devices/ASC6/ASC6.png')
			,(N'ATOL', N'', N'', N'~/img/devices/ATOL/ATOLDriveSmart.png')
			,(N'AVL', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'Avtofon SE', N'AvtoFon SE', N'~/img/devices/Avtofon/Avtofon.gif', N'~/img/devices/AvtoFon/AvtoFonSE.png')
			,(N'AvtoGraf', N'', N'~/img/devices/avtograf.png', N'~/img/devices/AvtoGraf/AvtoGraf.png')
			,(N'AvtoGrafCan', N'', N'~/img/devices/avtograf.png', N'~/img/devices/AvtoGraf/AvtoGraf.png')
			,(N'AzimuthGSM', N'Azimuth GSM', N'', N'~/img/devices/AzimuthGSM/AzimuthGSM.png')
			,(N'Coban GPS304B', N'', N'~/img/devices/Coban/CobanGPS304B.jpg', N'~/img/devices/Coban/CobanGPS304B.png')
			,(N'Coban GPS305', N'', N'~/img/devices/Coban/CobanGPS305.jpg', N'~/img/devices/Coban/CobanGPS305.png')
			,(N'Coban GPS306A', N'', N'~/img/devices/Coban/CobanGPS306A.jpg', N'~/img/devices/Coban/CobanGPS306A.png')
			,(N'Coban TK102', N'', N'~/img/devices/Coban/CobanTK102.jpg', N'~/img/devices/Coban/CobanTK102.png')
			,(N'Coban TK1022', N'Coban TK102-2', N'~/img/devices/Coban/CobanTK1022.jpg', N'~/img/devices/Coban/CobanTK1022.png')
			,(N'Coban TK103', N'', N'~/img/devices/Coban/CobanTK103.jpg', N'~/img/devices/Coban/CobanTK103.png')
			,(N'Coban TK1042', N'Coban TK104-2', N'~/img/devices/Coban/CobanTK1042.jpg', N'~/img/devices/Coban/CobanTK1042.png')
			,(N'Coban TK104SD', N'Coban TK104 SD', N'~/img/devices/Coban/CobanTK104SD.jpg', N'~/img/devices/Coban/CobanTK104SD.png')
			,(N'Coban TK106', N'', N'~/img/devices/Coban/CobanTK106.jpg', N'~/img/devices/Coban/CobanTK106.png')
			,(N'Coban TK1062', N'Coban TK106-2', N'~/img/devices/Coban/CobanTK1062.jpg', N'~/img/devices/Coban/CobanTK1062.png')
			,(N'CyberGLX', N'M2M-Cyber GLX', N'', N'~/img/devices/M2M/M2MCyberGLX.png')
			,(N'DeerCollar', N'Ultracom Collar', N'', N'~/img/devices/Ultracom/UltracomCollar.png')
			,(N'DIMTS', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'E-Fortune Deest 69 (������ �� �������)', N'MonkeyG Deest 69', N'~/img/devices/Q50/E-Fortune_Deest_69.jpg', N'~/img/devices/MonkeyG/MonkeyGDeest69.png')
			,(N'E-Fortune DS99', N'MonkeyG DS99', N'~/img/devices/Q50/E-Fortune_Deest_69.jpg', N'~/img/devices/MonkeyG/MonkeyGDS99.png')
			,(N'EGTS', N'', N'~/img/devices/EGTS.gif', N'~/img/devices/unknownBox.png')
			,(N'FM3101', N'Teltonika FM3101', N'', N'~/img/devices/Teltonika/TeltonikaFM3101.png')
			,(N'FtpWebCam', N'FTP Webcam', N'', N'~/img/devices/webcam.png')
			,(N'Futureway PetTracker FP03', N'', N'~/img/devices/Futureway/FuturewayPetTrackerFP03.jpg', N'~/img/devices/Futureway/FuturewayPetTrackerFP03.png')
			,(N'Galileo', N'Galileosky v 5.0', N'', N'~/img/devices/Galileosky/GalileoskyV5.png')
			,(N'GenericTracker', N'Unspecified', N'', N'')
			,(N'GlobalOrient', N'', N'', N'~/img/devices/GlobalOrient/GlobalOrient.png')
			,(N'GlobalSat GTR-128', N'', N'~/img/devices/Globalsat/GlobalSatGTR128.jpg', N'~/img/devices/GlobalSat/GlobalSatGTR128.png')
			,(N'GlobalSat TR-102', N'', N'', N'~/img/devices/GlobalSat/GlobalSatTR102.png')
			,(N'GlobalSat TR-203', N'', N'~/img/devices/Globalsat/GlobalSatTR203.jpg', N'~/img/devices/GlobalSat/GlobalSatTR203.png')
			,(N'GlobalSat TR-600', N'', N'~/img/devices/Globalsat/GlobalSatTR600.jpg', N'~/img/devices/GlobalSat/GlobalSatTR600.png')
			,(N'GRANIT_AUTO', N'GRANIT (Vehicle)', N'~/img/devices/granit_auto.jpg', N'~/img/devices/GRANIT/GRANIT618.png')
			,(N'GRANIT_PERSONAL', N'GRANIT (Personal)', N'~/img/devices/granit_personal.jpg', N'~/img/devices/GRANIT/GRANIT613.png')
			,(N'GV100', N'Queclink GV100', N'', N'~/img/devices/Queclink/QueclinkGV100.png')
			,(N'IntelliTrac', N'', N'', N'~/img/devices/IntelliTrac/IntelliTracA1.png')
			,(N'JB100', N'STAR TRACKER JB-100', N'', N'~/img/devices/STARTRACKER/STARTRACKERJB100.png')
			,(N'JB101', N'SkyTrack JB101', N'', N'~/img/devices/SkyTrack/SkyTrackJB101.png')
			,(N'Keelin GPT18', N'', N'~/img/devices/Keelin/KeelinGPT18.jpg', N'~/img/devices/Keelin/KeelinGPT18.png')
			,(N'Keelin K20', N'', N'~/img/devices/Keelin/KeelinK20.jpg', N'~/img/devices/Keelin/KeelinK20.png')
			,(N'Kid Tracker plus Q50 (G36), Q50S', N'Sentar Q50/Q50S', N'~/img/devices/Q50/Kid Tracker plus Q50 (G36).png', N'~/img/devices/Sentar/SentarQ50.png')
			,(N'Kid Tracker Q40, Q40S', N'Sentar Q40/Q40S', N'~/img/devices/Q50/Kid Tracker Q40, Q40S.png', N'~/img/devices/Sentar/SentarQ40.png')
			,(N'Kid Tracker V80', N'Sentar V80', N'~/img/devices/Q50/Kid Tracker V80.png', N'~/img/devices/Sentar/SentarV80.png')
			,(N'Kid Tracker V81', N'Sentar V81', N'~/img/devices/Q50/Kid Tracker V81.png', N'~/img/devices/Sentar/SentarV81.png')
			,(N'Mayak Sled SOBR Chip', N'SOBR Chip', N'', N'~/img/devices/SOBR/SOBRChip.png')
			,(N'Megastek MT60', N'', N'~/img/devices/Megastek/Megastek_MT60.png', N'~/img/devices/Megastek/MegastekMT60.png')
			,(N'Megastek MT70', N'', N'~/img/devices/Megastek/Megastek_MT70.jpg', N'~/img/devices/Megastek/MegastekMT70.png')
			,(N'Megastek MT90', N'', N'~/img/devices/Megastek/Megastek_MT90.jpg', N'~/img/devices/Megastek/MegastekMT90.png')
			,(N'MeiligaoMVT380', N'Meiligao MVT380', N'', N'~/img/devices/Meiligao/MeiligaoMVT380.png')
			,(N'MeiligaoVT300', N'Meiligao VT300', N'', N'~/img/devices/Meiligao/MeiligaoVT300.png')
			,(N'Meitrack MT90', N'', N'~/img/devices/Meitrack/MeitrackMT90.jpg', N'~/img/devices/Meitrack/MeitrackMT90.png')
			,(N'Meitrack MT90S', N'', N'~/img/devices/Meitrack/MeitrackMT90S.jpg', N'~/img/devices/Meitrack/MeitrackMT90S.png')
			,(N'Meitrack MVT100', N'', N'~/img/devices/Meitrack/MeitrackMVT100.jpg', N'~/img/devices/Meitrack/MeitrackMVT100.png')
			,(N'Meitrack MVT380', N'', N'~/img/devices/Meitrack/MeitrackMVT380.jpg', N'~/img/devices/Meitrack/MeitrackMVT380.png')
			,(N'Meitrack T1', N'', N'~/img/devices/Meitrack/MeitrackT1.jpg', N'~/img/devices/Meitrack/MeitrackT1.png')
			,(N'Meitrack T355', N'', N'~/img/devices/Meitrack/MeitrackMT355.jpg', N'~/img/devices/Meitrack/MeitrackT355.png')
			,(N'Meitrack T622', N'', N'~/img/devices/Meitrack/MeitrackT622.jpg', N'~/img/devices/Meitrack/MeitrackT622.png')
			,(N'Meitrack TC68', N'', N'~/img/devices/Meitrack/MeitrackTC68.jpg', N'~/img/devices/Meitrack/MeitrackTC68.png')
			,(N'Meitrack TC68S', N'', N'~/img/devices/Meitrack/MeitrackTC68S.jpg', N'~/img/devices/Meitrack/MeitrackTC68S.png')
			,(N'Meitrack Treckids Bear', N'Meitrack Trackids Bear', N'~/img/devices/Meitrack/MeitrackTreckidsBear.jpg', N'~/img/devices/Meitrack/MeitrackTrackidsBear.png')
			,(N'Meitrack Treckids Turtle', N'Meitrack Trackids Turtle', N'~/img/devices/Meitrack/MeitrackTreckidsTurtle.jpg', N'~/img/devices/Meitrack/MeitrackTrackidsTurtle.png')
			,(N'Mercury', N'', N'', N'~/img/devices/Mercury/MercuryTA001.png')
			,(N'MiniFinder Atto', N'MiniFinder Atto (VG30)', N'~/img/devices/MiniFinder/MiniFinderAtto.jpg', N'~/img/devices/MiniFinder/MiniFinderAttoVG30.png')
			,(N'MiniFinder Pico VG10', N'MiniFinder Pico (VG10)', N'~/img/devices/MiniFinder/MiniFinderPicoVG10.jpg', N'~/img/devices/MiniFinder/MiniFinderPicoVG10.png')
			,(N'MiniFinder Pico VG20', N'MiniFinder Pico (VG20)', N'~/img/devices/MiniFinder/MiniFinderPicoVG20.jpg', N'~/img/devices/MiniFinder/MiniFinderPicoVG20.png')
			,(N'Mirep', N'MiREP', N'', N'~/img/devices/MiREP/MiREP.png')
			,(N'MJ', N'MegaJet', N'', N'~/img/devices/MegaJet/MegaJet.png')
			,(N'NaviTechDevice', N'Navitech', N'', N'~/img/devices/Navitech/Navitech.png')
			,(N'Navtelecom', N'Navtelecom SIGNAL S-2551', N'~/img/devices/Navtelecom/2551.png', N'~/img/devices/Navtelecom/NavtelecomSIGNALS2551.png')
			,(N'Ruptela', N'', N'', N'~/img/devices/Ruptela/RuptelaFMPro4.png')
			,(N'SADSP FOX-IN', N'', N'', N'~/img/devices/SADSP/SADSPFOXIN.png')
			,(N'SANAV', N'', N'', N'~/img/devices/SANAV/SANAV.png')
			,(N'Sentar Q60, Q60S (�����)', N'Sentar Q60/Q60S', N'~/img/devices/Q50/Sentar_Q60.jpg', N'~/img/devices/Sentar/SentarQ60.png')
			,(N'Sentar Q80', N'', N'~/img/devices/Q50/SentarQ80.jpg', N'~/img/devices/Sentar/SentarQ80.png')
			,(N'Sentar T58 (V82)', N'', N'~/img/devices/Q50/sentar_v82.jpg', N'~/img/devices/Sentar/SentarV82.png')
			,(N'SGGB', N'SureGuard SGGB', N'', N'~/img/devices/SGGB/SGGB.png')
			,(N'SGGB2', N'SureGuard SGGB2', N'', N'~/img/devices/SGGB/SGGB2.png')
			,(N'SinoTrack ST-901', N'', N'~/img/devices/SinoTrack/SinoTrackST901.jpg', N'~/img/devices/SinoTrack/SinoTrackST901.png')
			,(N'SinoTrack ST-901A', N'', N'~/img/devices/SinoTrack/SinoTrackST901A.jpg', N'~/img/devices/SinoTrack/SinoTrackST901A.png')
			,(N'Smart GT02B', N'', N'~/img/devices/Smart/SmartGT02B.jpg', N'~/img/devices/Smart/SmartGT02B.png')
			,(N'Smart GT03', N'', N'~/img/devices/Smart/SmartGT03.jpg', N'~/img/devices/Smart/SmartGT03.png')
			,(N'Smart GT03A', N'', N'~/img/devices/Smart/SmartGT03A.jpg', N'~/img/devices/Smart/SmartGT03A.png')
			,(N'Smart GT05', N'', N'~/img/devices/Smart/SmartGT05.jpg', N'~/img/devices/Smart/SmartGT05.png')
			,(N'Smart GT07', N'', N'~/img/devices/Smart/SmartGT07.jpg', N'~/img/devices/Smart/SmartGT07.png')
			,(N'Smart GT09B', N'', N'~/img/devices/Smart/SmartGT09B.jpg', N'~/img/devices/Smart/SmartGT09B.png')
			,(N'Smart TR02', N'', N'~/img/devices/Smart/SmartTR02.jpg', N'~/img/devices/Smart/SmartTR02.png')
			,(N'Smart TR06', N'', N'~/img/devices/Smart/SmartTR06.jpg', N'~/img/devices/Smart/SmartTR06.png')
			,(N'Smart TR06A', N'', N'~/img/devices/Smart/SmartTR06A.jpg', N'~/img/devices/Smart/SmartTR06A.png')
			,(N'Smarts Baby Bear', N'BB-mobile Baby Bear', N'~/img/devices/Smart/SmartBBMobile.jpg', N'~/img/devices/BBMobile/BBMobileBabyBear.png')
			,(N'Smarts GT 300', N'Smart GT300', N'', N'~/img/devices/Smart/SmartGT300.png')
			,(N'Smarts GT02', N'Smart GT02', N'~/img/devices/Smart/SmartGT02A.jpg', N'~/img/devices/Smart/SmartGT02.png')
			,(N'Smarts GT02A', N'Smart GT02A', N'~/img/devices/Smart/SmartGT02A.jpg', N'~/img/devices/Smart/SmartGT02A.png')
			,(N'Smarts GT06', N'Smart GT06', N'~/img/devices/Smart/SmartGT06.jpg', N'~/img/devices/Smart/SmartGT06.png')
			,(N'Smarts GT06N', N'Smart GT06N', N'~/img/devices/Smart/SmartGT06N.jpg', N'~/img/devices/Smart/SmartGT06N.png')
			,(N'SoftTracker', N'', N'', N'~/img/devices/smartphone.png')
			,(N'Sonim', N'', N'', N'~/img/devices/Sonim/Sonim.png')
			,(N'ST101V2', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'StarLineM10', N'StarLine M10', N'', N'~/img/devices/StarLine/StarLineM10.png')
			,(N'STEPP', N'Falcom STEPP', N'', N'~/img/devices/Falcom/FalcomSTEPP.png')
			,(N'Sygic', N'', N'', N'~/img/devices/Sygic/Sygic.png')
			,(N'Teltonika FM1100', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM1100.png')
			,(N'Teltonika FM1110', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM1110.png')
			,(N'Teltonika FM1125', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM1125.png')
			,(N'Teltonika FM1200', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM1200.png')
			,(N'Teltonika FM2200', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM2200.png')
			,(N'Teltonika FM5300', N'', N'', N'~/img/devices/Teltonika/TeltonikaFM5300.png')
			,(N'Teltonika GH3000', N'', N'', N'~/img/devices/Teltonika/TeltonikaGH3000.png')
			,(N'Tetrika', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'TK STAR LK106', N'', N'~/img/devices/TKSTAR/TKSTARLK106.jpg', N'~/img/devices/TKSTAR/TKSTARLK106.png')
			,(N'TK STAR TK909', N'', N'~/img/devices/TKSTAR/TKSTARTK909.jpg', N'~/img/devices/TKSTAR/TKSTARTK909.png')
			,(N'TK STAR TK911', N'', N'~/img/devices/TKSTAR/TKSTARTK911.jpg', N'~/img/devices/TKSTAR/TKSTARTK911.png')
			,(N'TL2000', N'Autocop TL-2000', N'', N'~/img/devices/Autocop/AutocopTL2000.png')
			,(N'TM140', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'TM4', N'', N'', N'~/img/devices/TM4/TM4.png')
			,(N'TS GLONASS', N'', N'', N'~/img/devices/TSGLONASS/TSGLONASS.png')
			,(N'Vjoy �ar TK10SE', N'Vjoy Car TK10SE', N'~/img/devices/Vjoy/vjoycartk10se.png', N'~/img/devices/VjoyCar/VjoyCarTK10SE.png')
			,(N'Watch-WT8', N'WT8 Watch', N'', N'~/img/devices/WT8/WT8.png')
			,(N'WatchDA690', N'GlobalSat DA-690', N'~/img/devices/da-690.jpg', N'~/img/devices/GlobalSat/GlobalSatDA690.png')
			,(N'Wialon', N'', N'', N'~/img/devices/unknownBox.png')
			,(N'Xexun TK101', N'', N'~/img/devices/Xexun/XexunTK101.jpg', N'~/img/devices/Xexun/XexunTK101.png')
			,(N'Xexun TK102', N'', N'~/img/devices/Xexun/XexunTK102.jpg', N'~/img/devices/Xexun/XexunTK102.png')
			,(N'Xexun TK1022', N'Xexun TK102-2', N'~/img/devices/Xexun/XexunTK1022.jpg', N'~/img/devices/Xexun/XexunTK1022.png')
			,(N'Xexun TK103', N'', N'~/img/devices/Xexun/XexunTK103.jpg', N'~/img/devices/Xexun/XexunTK103.png')
			,(N'Xexun TK1032', N'Xexun TK103-2', N'~/img/devices/Xexun/XexunTK1032.jpg', N'~/img/devices/Xexun/XexunTK1032.png')
			,(N'Xexun TK1042', N'Xexun TK104-2', N'~/img/devices/Xexun/XexunTK1042.jpg', N'~/img/devices/Xexun/XexunTK1042.png')
			,(N'Xexun TK104SD', N'Xexun TK104 SD', N'~/img/devices/Xexun/XexunTK104SD.jpg', N'~/img/devices/Xexun/XexunTK104SD.png')
			,(N'Xexun TK201', N'', N'~/img/devices/Xexun/XexunTK201.jpg', N'~/img/devices/Xexun/XexunTK201.png')
			,(N'Xexun TK2012', N'Xexun TK201-2', N'~/img/devices/Xexun/XexunTK2012.jpg', N'~/img/devices/Xexun/XexunTK2012.png')
			,(N'Xexun TK2013', N'Xexun TK201-3', N'~/img/devices/Xexun/XexunTK2013.jpg', N'~/img/devices/Xexun/XexunTK2013.png')
			,(N'Xexun XT008', N'', N'~/img/devices/Xexun/XexunXT008.jpg', N'~/img/devices/Xexun/XexunXT008.png')
			,(N'Xexun XT009', N'', N'~/img/devices/Xexun/XexunXT009.jpg', N'~/img/devices/Xexun/XexunXT009.png')
			,(N'Xexun XT107', N'', N'~/img/devices/Xexun/XexunXT107.jpg', N'~/img/devices/Xexun/XexunXT107.png')
			,(N'Zenith RS1102', N'RS-1102 "Zenith"', N'', N'~/img/devices/RS1102/RS1102.png')
	) AS SRC ([TYPE_NAME_OLD], [TYPE_NAME_NEW], [ImagePath_Old], [ImagePath_New])
		ON  DST.[TYPE_NAME] = SRC.[TYPE_NAME_OLD]
	WHEN MATCHED AND (DST.[TYPE_NAME] <> SRC.[TYPE_NAME_NEW] OR DST.[ImagePath] <> SRC.[ImagePath_New]) THEN
		UPDATE
			SET
				 DST.[TYPE_NAME] = CASE WHEN 0 < LEN(SRC.[TYPE_NAME_NEW]) THEN SRC.[TYPE_NAME_NEW] ELSE SRC.[TYPE_NAME_OLD] END
				,DST.[ImagePath] = CASE WHEN 0 < LEN(SRC.[ImagePath_New]) THEN SRC.[ImagePath_New] ELSE SRC.[ImagePath_Old] END
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [CONTROLLER_TYPE_ID]-- [TYPE_NAME]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO