BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		SELECT
			[TYPE_NAME],
			[SortOrder] =
			CASE [TYPE_NAME]
				WHEN N'Unspecified' THEN 1
				WHEN N'SoftTracker' THEN 2
				ELSE ROW_NUMBER() OVER (ORDER BY [TYPE_NAME]) + 2
			END
		FROM [dbo].[CONTROLLER_TYPE]
	) AS SRC ([TYPE_NAME], [SortOrder])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[SortOrder] = SRC.[SortOrder]
	OUTPUT $action, INSERTED.[TYPE_NAME], INSERTED.[SortOrder], DELETED.[SortOrder]
	;
	------------------------------------------------------------------
	SELECT [SortOrder], [TYPE_NAME] FROM [dbo].[CONTROLLER_TYPE] ORDER BY [SortOrder]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO