BEGIN TRY
	BEGIN TRAN

	-- ��������� ��������� ���� ��������
	DECLARE @typesNames dbo.Msisdn
	INSERT INTO @typesNames
	OUTPUT INSERTED.*
	SELECT
		[value]
	FROM
	(
		VALUES
			 (N'Emulator')
			,(N'GeneratingEmulator')
			,(N'MLP')
			,(N'Order285')
			,(N'Q50 Baby Watch')
			,(N'S60V3FP')
			,(N'Smart GT06')
			,(N'Smart GT06N')
			,(N'TrackerSOS')
			,(N'Xexun TK 201-3')
			,(N'TS GLONASS')
	) T([value])
	;

	-- ��������� �������������� �������� ���������� ��������
	DECLARE @vehicleIds dbo.Id_Param
	INSERT INTO @vehicleIds
	OUTPUT INSERTED.*
	SELECT
		C.[VEHICLE_ID]
	FROM @typesNames N
		INNER JOIN [dbo].[CONTROLLER_TYPE] T
			ON T.[TYPE_NAME] = N.[value]
		INNER JOIN [dbo].[CONTROLLER] C
			ON C.[CONTROLLER_TYPE_ID] = T.[CONTROLLER_TYPE_ID]
	;
	-- ������� ������� � ������ �� ���������������
	DECLARE @vehicleId int = 0
	WHILE (1 = 1)
	BEGIN
		SELECT TOP(1) @vehicleId = [Id]
		FROM @vehicleIds
		WHERE [Id] > @vehicleId
		ORDER BY [Id]
		-- ��������� ����, ���� ��� ������
		IF (0 = @@ROWCOUNT)
			BREAK;
		-- ������� ����
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Geo_Log_Ignored]             /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Geo_Log]                     /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Geo_Log#Lasts]               /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Statistic_Log]               /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Statistic_Log#Lasts]         /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;

		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[GPS_Log]                     /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[GPS_Log#Lasts]               /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;

		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Position_Radius]             /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Controller_Sensor_Log]       /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Controller_Sensor_Log#Lasts] /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[CAN_INFO]                    /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;

		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Log_Time]                    /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[Log_Time#Lasts]              /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		WHILE (1 = 1) BEGIN WAITFOR DELAY '00:00:01'; DELETE TOP(50000) FROM [dbo].[RenderedServiceItem]         /*OUTPUT DELETED.* */ WHERE [Vehicle_ID] = @vehicleId; IF (0 = @@ROWCOUNT) BREAK; END;
		-- ������� ��� ������
		EXEC [dbo].[DeleteVehicle] @vehicle_id = @vehicleId
	END
	-- ������� �������, ��������� ����� ��������
	DELETE C OUTPUT DELETED.*
	FROM @typesNames N
		INNER JOIN [dbo].[CONTROLLER_TYPE] T
			ON T.[TYPE_NAME] = N.[value]
				INNER JOIN [dbo].[Controller_Type_CommandTypes] C
					ON C.[Controller_Type_ID] = T.[CONTROLLER_TYPE_ID]
	;
	-- ������� �������, ��������� ����� ��������
	DELETE S OUTPUT DELETED.*
	FROM @typesNames N
		INNER JOIN [dbo].[CONTROLLER_TYPE] T
			ON T.[TYPE_NAME] = N.[value]
				INNER JOIN [dbo].[CONTROLLER_SENSOR] S
			ON S.[Controller_Type_ID] = T.[CONTROLLER_TYPE_ID]
	;
	-- ������� ��������� ���� ��������
	DELETE T OUTPUT DELETED.*
	FROM @typesNames N
		INNER JOIN [dbo].[CONTROLLER_TYPE] T
			ON T.[TYPE_NAME] = N.[value]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO