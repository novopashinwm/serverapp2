DECLARE @baseControllerType nvarchar(50) = N'Teltonika GH3000'

BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [TYPE_NAME]
	------------------------------------------------------------------
	INSERT INTO [dbo].[CONTROLLER_TYPE]
	(
		 [TYPE_NAME]
		,[ImagePath]
		,[PACKET_LENGTH]
		,[POS_ABSENCE_SUPPORT]
		,[POS_SMOOTH_SUPPORT]
		,[AllowedToAddByCustomer]
		,[DeviceIdIsImei]
		,[SupportsPassword]
		,[Default_Vehicle_Kind_ID]
		,[DeviceIdIsRequiredForSetup]
		,[LocatorGsmTimeout]
		,[SortOrder]
		,[DefaultPassword]
		,[PasswordAlphabet]
		,[PasswordLength]
		,[SmsTemplateForPasswordChange]
		,[DeviceIdStartIndex]
		,[DeviceIdLength]
	)
	OUTPUT INSERTED.*
	SELECT
		 L.[TYPE_NAME]
		,L.[ImagePath]
		,R.[PACKET_LENGTH]
		,R.[POS_ABSENCE_SUPPORT]
		,R.[POS_SMOOTH_SUPPORT]
		,R.[AllowedToAddByCustomer]
		,R.[DeviceIdIsImei]
		,R.[SupportsPassword]
		,R.[Default_Vehicle_Kind_ID]
		,R.[DeviceIdIsRequiredForSetup]
		,R.[LocatorGsmTimeout]
		,R.[SortOrder]
		,R.[DefaultPassword]
		,R.[PasswordAlphabet]
		,R.[PasswordLength]
		,R.[SmsTemplateForPasswordChange]
		,R.[DeviceIdStartIndex]
		,R.[DeviceIdLength]
	FROM
	(
		VALUES
			 (N'Teltonika FMA202', N'~/img/devices/Teltonika/TeltonikaFMA202.png')
			,(N'Teltonika FMB001', N'~/img/devices/Teltonika/TeltonikaFMB001.png')
			,(N'Teltonika FMB010', N'~/img/devices/Teltonika/TeltonikaFMB010.png')
			,(N'Teltonika FMB110', N'~/img/devices/Teltonika/TeltonikaFMB110.png')
			,(N'Teltonika FMB120', N'~/img/devices/Teltonika/TeltonikaFMB120.png')
			,(N'Teltonika FMB125', N'~/img/devices/Teltonika/TeltonikaFMB125.png')
			,(N'Teltonika FMB202', N'~/img/devices/Teltonika/TeltonikaFMB202.png')
			,(N'Teltonika FMB630', N'~/img/devices/Teltonika/TeltonikaFMB630.png')
			,(N'Teltonika FMB900', N'~/img/devices/Teltonika/TeltonikaFMB900.png')
			,(N'Teltonika FMB920', N'~/img/devices/Teltonika/TeltonikaFMB920.png')
			,(N'Teltonika ���250', N'~/img/devices/Teltonika/TeltonikaTMT250.png')
	) L ([TYPE_NAME], [ImagePath])
		OUTER APPLY
		(
			SELECT * FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @baseControllerType
		) R
	WHERE NOT EXISTS
	(
		SELECT * FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = L.[TYPE_NAME]
	)
	;
	INSERT INTO [dbo].[CONTROLLER_SENSOR]
	OUTPUT INSERTED.*
	SELECT
		 R.[CONTROLLER_TYPE_ID]
		,S.[CONTROLLER_SENSOR_TYPE_ID]
		,S.[NUMBER]
		,S.[MAX_VALUE]
		,S.[MIN_VALUE]
		,S.[DEFAULT_VALUE]
		,S.[BITS]
		,S.[Descript]
		,S.[VALUE_EXPIRED]
		,S.[Default_Sensor_Legend_ID]
		,S.[Default_Multiplier]
		,S.[Default_Constant]
		,S.[Mandatory]
	FROM
	(
		VALUES
			 (N'Teltonika FMA202', N'~/img/devices/Teltonika/TeltonikaFMA202.png')
			,(N'Teltonika FMB001', N'~/img/devices/Teltonika/TeltonikaFMB001.png')
			,(N'Teltonika FMB010', N'~/img/devices/Teltonika/TeltonikaFMB010.png')
			,(N'Teltonika FMB110', N'~/img/devices/Teltonika/TeltonikaFMB110.png')
			,(N'Teltonika FMB120', N'~/img/devices/Teltonika/TeltonikaFMB120.png')
			,(N'Teltonika FMB125', N'~/img/devices/Teltonika/TeltonikaFMB125.png')
			,(N'Teltonika FMB202', N'~/img/devices/Teltonika/TeltonikaFMB202.png')
			,(N'Teltonika FMB630', N'~/img/devices/Teltonika/TeltonikaFMB630.png')
			,(N'Teltonika FMB900', N'~/img/devices/Teltonika/TeltonikaFMB900.png')
			,(N'Teltonika FMB920', N'~/img/devices/Teltonika/TeltonikaFMB920.png')
			,(N'Teltonika ���250', N'~/img/devices/Teltonika/TeltonikaTMT250.png')
	) L ([TYPE_NAME], [ImagePath])
		OUTER APPLY
		(
			SELECT [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE]
			WHERE [TYPE_NAME] = L.[TYPE_NAME]
		) R
		OUTER APPLY
		(
			SELECT * FROM [dbo].[CONTROLLER_SENSOR]
			WHERE [CONTROLLER_TYPE_ID] = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @baseControllerType)
		) S
	WHERE NOT EXISTS
	(
		SELECT * FROM [dbo].[CONTROLLER_SENSOR] WHERE [CONTROLLER_TYPE_ID] = R.[CONTROLLER_TYPE_ID]
	)
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [TYPE_NAME]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO