BEGIN TRY
	BEGIN TRAN

	DECLARE @old_controller_type_id int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = N'Smart GT06N')
	DECLARE @new_controller_type_id int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = N'Smarts GT06N')

	-- ��������� �������������� �������� ��� ��������� ����
	DECLARE @vehicleIds dbo.Id_Param
	INSERT INTO @vehicleIds
	OUTPUT INSERTED.*
	SELECT
		[VEHICLE_ID]
	FROM [dbo].[CONTROLLER]
	WHERE [CONTROLLER_TYPE_ID] = @old_controller_type_id
	;
	-- ������� ������� � ������ �� ���������������
	DECLARE @vehicleId int = 0
	WHILE (1 = 1)
	BEGIN
		SELECT TOP(1) @vehicleId = [Id]
		FROM @vehicleIds
		WHERE [Id] > @vehicleId
		ORDER BY [Id]
		-- ��������� ����, ���� ��� ������
		IF (0 = @@ROWCOUNT)
			BREAK;
		-- ��������� ��� �������
		UPDATE c
			SET [CONTROLLER_TYPE_ID] = @new_controller_type_id
		OUTPUT INSERTED.*, DELETED.*
		FROM [dbo].[CONTROLLER] C
		WHERE C.[VEHICLE_ID]          = @vehicleId
		AND   C.[CONTROLLER_TYPE_ID] <> @new_controller_type_id
		-- ��������� �������
		UPDATE M
			SET [CONTROLLER_SENSOR_ID] = N.[CONTROLLER_SENSOR_ID]
		OUTPUT INSERTED.*, DELETED.*
		FROM [dbo].[CONTROLLER_SENSOR_MAP] M
			INNER JOIN [dbo].[CONTROLLER] C
				ON C.[CONTROLLER_ID] = M.[CONTROLLER_ID]
			INNER JOIN [dbo].[CONTROLLER_SENSOR] O
				ON O.[CONTROLLER_SENSOR_ID] = M.[CONTROLLER_SENSOR_ID]
			INNER JOIN [dbo].[CONTROLLER_SENSOR] N
				ON  N.[NUMBER]    = O.[NUMBER]
				AND N.[Descript]  = O.descript
				AND N.[Descript] <> ''
		WHERE C.[VEHICLE_ID] = @vehicleId

		DELETE M
		OUTPUT DELETED.*
		FROM CONTROLLER_SENSOR_MAP M
			INNER JOIN [dbo].[CONTROLLER] C
				ON C.[CONTROLLER_ID] = M.[CONTROLLER_ID]
			INNER JOIN [dbo].[CONTROLLER_SENSOR] S
			 ON S.[CONTROLLER_SENSOR_ID] = M.[CONTROLLER_SENSOR_ID]
		WHERE S.[CONTROLLER_TYPE_ID] <> C.[CONTROLLER_TYPE_ID]
		AND   C.[VEHICLE_ID] = @vehicleId
	END
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO