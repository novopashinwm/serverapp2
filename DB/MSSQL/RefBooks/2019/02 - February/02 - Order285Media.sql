﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @Order285Code sysname = N'Order285';
	------------------------------------------------------------------
	DECLARE @Order285MediaTypeId int = (SELECT TOP(1) [ID] FROM  [dbo].[MEDIA_TYPE] WHERE [Name] = N'RemoteTerminalServer')
	------------------------------------------------------------------
	-- Обновление [dbo].[MEDIA]
	MERGE [dbo].[MEDIA] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			(@Order285Code, @Order285MediaTypeId)
	) AS SRC ([NAME], [TYPE])
		ON DST.[NAME] = SRC.[NAME]
	WHEN MATCHED AND DST.[TYPE] <> SRC.[TYPE] THEN
		UPDATE
			SET
				DST.[TYPE] = SRC.[TYPE]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([NAME], [TYPE]) VALUES (SRC.[NAME], SRC.[TYPE])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	DECLARE @Order285MediaId int = (SELECT TOP(1) [MEDIA_ID] FROM  [dbo].[MEDIA] WHERE [NAME] = @Order285Code)
	------------------------------------------------------------------
	-- Обновление [dbo].[MEDIA_ACCEPTORS]
	MERGE [dbo].[MEDIA_ACCEPTORS] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			(@Order285MediaId, @Order285Code, N'', N'')
	) AS SRC ([MEDIA_ID], [NAME], [DESC], [INIT])
		ON DST.[Name] = SRC.[NAME]
	WHEN MATCHED AND DST.[MEDIA_ID] <> SRC.[MEDIA_ID] THEN
		UPDATE
			SET
				 DST.[MEDIA_ID]       = SRC.[MEDIA_ID]
				,DST.[DESCRIPTION]    = SRC.[DESC]
				,DST.[INITIALIZATION] = SRC.[INIT]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([MEDIA_ID], [Name], [DESCRIPTION], [INITIALIZATION]) VALUES (SRC.[MEDIA_ID], SRC.[NAME], SRC.[DESC], SRC.[INIT])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH