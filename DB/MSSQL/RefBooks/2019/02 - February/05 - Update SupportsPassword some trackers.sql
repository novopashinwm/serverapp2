﻿-- SupportsPassword для некоторых трекеров
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			 (N'Anywhere TK106', 1)
			,(N'Anywhere TK108', 1)
			,(N'Coban GPS304B',  1)
			,(N'Coban GPS305',   1)
			,(N'Coban GPS306A',  1)
			,(N'Coban TK102',    1)
			,(N'Coban TK1022',   1)
			,(N'Coban TK103',    1)
			,(N'Coban TK1042',   1)
			,(N'Coban TK104SD',  1)
			,(N'Coban TK106',    1)
			,(N'Coban TK1062',   1)
			,(N'Xexun TK101',    1)
			,(N'Xexun TK102',    1)
			,(N'Xexun TK1022',   1)
			,(N'Xexun TK103',    1)
			,(N'Xexun TK1032',   1)
			,(N'Xexun TK1042',   1)
			,(N'Xexun TK104SD',  1)
			,(N'Xexun TK201',    1)
			,(N'Xexun TK2012',   1)
			,(N'Xexun TK2013',   1)
			,(N'Xexun XT008',    1)
			,(N'Xexun XT009',    1)
			,(N'Xexun XT107',    1)
	) AS SRC ([TYPE_NAME], [SupportsPassword])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED AND DST.[SupportsPassword] <> SRC.[SupportsPassword] THEN
		UPDATE
			SET
				DST.[SupportsPassword] = SRC.[SupportsPassword]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO