﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	-- Исходный идентификатор объекта
	DECLARE @SrcRealVehicleId  int = 66327;
	DECLARE @DstFakeVehicleIds [dbo].[Id_Param];
	INSERT  @DstFakeVehicleIds
	---------------------------------------
	-- Идентификаторы объектов для эмуляции
	VALUES
		 (82795)
		,(83072)
	------------------------------------------------------------------
	-- Выставляем параметры эмуляторов
	MERGE [dbo].[EMULATOR] WITH (HOLDLOCK) AS DST
	USING
	(
		SELECT
			 [RealVehicleID]   = @SrcRealVehicleId
			,[FakeVehicleID]   = F.[id]
			,[DateFrom]        = D.[DateFrom]
			,[DateTo]          = D.[DateTo]
			,[FakeLogTimeFrom] = [dbo].[utc2lt](DATEADD(day, DATEDIFF(day, 0, GETUTCDATE()), 0))
			,[Enabled]         = 1
		FROM @DstFakeVehicleIds F
			CROSS JOIN
			(
				SELECT
					 [DateFrom] = (SELECT MIN([Log_Time]) FROM [dbo].[Emulator_Source] WHERE [Vehicle_ID] = @SrcRealVehicleId)
					,[DateTo]   = (SELECT MAX([Log_Time]) FROM [dbo].[Emulator_Source] WHERE [Vehicle_ID] = @SrcRealVehicleId)
			) D
	) AS SRC ([RealVehicleID], [FakeVehicleID], [DateFrom], [DateTo], [FakeLogTimeFrom], [Enabled])
		ON  DST.[RealVehicleID] = SRC.[RealVehicleID]
		AND DST.[FakeVehicleID] = SRC.[FakeVehicleID]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[DateFrom]        = SRC.[DateFrom],
				DST.[DateTo]          = SRC.[DateTo],
				DST.[FakeLogTimeFrom] = SRC.[FakeLogTimeFrom],
				DST.[Enabled]         = SRC.[Enabled]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [RealVehicleID],     [FakeVehicleID],     [DateFrom],     [DateTo],     [FakeLogTimeFrom],     [Enabled])
		VALUES (SRC.[RealVehicleID], SRC.[FakeVehicleID], SRC.[DateFrom], SRC.[DateTo], SRC.[FakeLogTimeFrom], SRC.[Enabled])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH