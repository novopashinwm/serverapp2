﻿-- Обновление таблицы типов команд
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CommandTypes] AS DST
	USING
	(
		VALUES
			 (N'Setup',         00300)
			,(N'SetModeOnline', 00060)
	) AS SRC ([code], [ExecutionTimeout])
		ON DST.[code] = SRC.[code]
	WHEN MATCHED AND DST.[ExecutionTimeout] <> SRC.[ExecutionTimeout] THEN
		UPDATE
			SET
				DST.[ExecutionTimeout] = SRC.[ExecutionTimeout]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO