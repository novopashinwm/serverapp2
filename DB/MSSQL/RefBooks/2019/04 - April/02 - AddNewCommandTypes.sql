DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;

-- <summary> Включить световой маяк </summary>
-- <remarks> Включить световой маяк на корпусе трекера </remarks>
-- TurnLightOn        = 56,
SELECT
	@id                 = 56,
	@code               = N'TurnLightOn',
	@description        = N'Turn on tracker light beacon',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Отключить световой маяк </summary>
-- <remarks> Отключить световой маяк на корпусе трекера </remarks>
-- TurnLightOff       = 57,
SELECT
	@id                 = 57,
	@code               = N'TurnLightOff',
	@description        = N'Turn off tracker light beacon',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds