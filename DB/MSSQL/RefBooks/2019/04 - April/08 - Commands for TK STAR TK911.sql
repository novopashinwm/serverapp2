﻿DECLARE @CONTROLLER_TYPE_NAME nvarchar(50) = 'TK STAR TK911'
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	-- Удалить команды у контроллера
	--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
	-- Добавить команды к контроллеру
	--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Setup'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetInterval'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskPositionOverMLP'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetModeOnline'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetModeWaiting'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'MonitorModeOn'
	EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'MonitorModeOff'
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO