﻿-- Обновление таблицы типов команд, с точки зрения прав на команды
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CommandTypes] AS DST
	USING
	(
		VALUES
			 (012, N'SendText',              112, N'Immobilization')
			,(017, N'SetSettings',           116, N'ChangeDeviceConfigBySMS')
			,(018, N'Restart',               112, N'Immobilization')
			,(020, N'AskPosition',           102, N'VehicleAccess')
			,(021, N'CutOffElectricity',     112, N'Immobilization')
			,(022, N'ReopenElectricity',     112, N'Immobilization')
			,(023, N'CutOffFuel',            112, N'Immobilization')
			,(024, N'ReopenFuel',            112, N'Immobilization')
			,(025, N'CancelAlarm',           112, N'Immobilization')
			,(026, N'ChangeMode',            116, N'ChangeDeviceConfigBySMS')
			,(029, N'ReloadDevice',          112, N'Immobilization')
			,(030, N'VibrateRequest',        112, N'Immobilization')
			,(031, N'Callback',              112, N'Immobilization')
			,(032, N'ShutdownDevice',        112, N'Immobilization')
			,(033, N'CapturePicture',        112, N'Immobilization')
			,(035, N'AskPositionOverMLP',    102, N'VehicleAccess')
			,(036, N'Setup',                 116, N'ChangeDeviceConfigBySMS')
			,(037, N'Immobilize',            112, N'Immobilization')
			,(038, N'Deimmobilize',          112, N'Immobilization')
			,(039, N'SetModeOnline',         112, N'Immobilization')
			,(040, N'SetModeWaiting',        112, N'Immobilization')
			,(042, N'EnableMotionDetector',  112, N'Immobilization')
			,(043, N'DisableMotionDetector', 112, N'Immobilization')
			,(044, N'QuickImmobilizeON',     112, N'Immobilization')
			,(045, N'QuickImmobilizeOFF',    112, N'Immobilization')
			,(047, N'SetInterval',           112, N'Immobilization')
			,(048, N'Status',                112, N'Immobilization')
			,(049, N'SetSos',                112, N'Immobilization')
			,(050, N'DeleteSOS',             112, N'Immobilization')
			,(051, N'MonitorModeOn',         112, N'Immobilization')
			,(052, N'MonitorModeOff',        112, N'Immobilization')
			,(053, N'FindDevice',            112, N'Immobilization')
			,(054, N'AddContact',            112, N'Immobilization')
			,(055, N'DeleteContact',         112, N'Immobilization')
			,(056, N'TurnLightOn',           112, N'Immobilization')
			,(057, N'TurnLightOff',          112, N'Immobilization')
	) AS SRC ([id], [code], [Right_ID], [Right_Code])
		ON DST.[code] = SRC.[code]
	WHEN MATCHED AND ISNULL(DST.[Right_ID], 0) <> ISNULL(SRC.[Right_ID], 0) THEN
		UPDATE
			SET
				DST.[Right_ID] = SRC.[Right_ID]
	OUTPUT $action, INSERTED.[id], INSERTED.[code], INSERTED.[Right_ID], DELETED.[Right_ID]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO