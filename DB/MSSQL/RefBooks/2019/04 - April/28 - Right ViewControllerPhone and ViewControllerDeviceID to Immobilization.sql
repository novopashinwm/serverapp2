DECLARE @replaceWithRight int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'Immobilization');
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_DEPARTMENT]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID]   = V.[OPERATOR_ID],
			[DEPARTMENT_ID] = V.[DEPARTMENT_ID],
			[RIGHT_ID]      = @replaceWithRight,
			[ALLOWED]       = V.[ALLOWED]
		FROM [dbo].[OPERATOR_DEPARTMENT] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME] IN (N'ViewControllerPhone', N'ViewControllerDeviceID')
		AND   V.[ALLOWED] = 1
	) AS SRC ([OPERATOR_ID], [DEPARTMENT_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
		AND DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
		AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATOR_ID],     [DEPARTMENT_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[DEPARTMENT_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATORGROUP_DEPARTMENT]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATORGROUP_DEPARTMENT] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATORGROUP_ID] = V.[OPERATORGROUP_ID],
			[DEPARTMENT_ID]    = V.[DEPARTMENT_ID],
			[RIGHT_ID]         = @replaceWithRight,
			[ALLOWED]          = V.[ALLOWED]
		FROM [dbo].[OPERATORGROUP_DEPARTMENT] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME] IN (N'ViewControllerPhone', N'ViewControllerDeviceID')
		AND   V.[ALLOWED] = 1
	) AS SRC ([OPERATORGROUP_ID], [DEPARTMENT_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATORGROUP_ID] = SRC.[OPERATORGROUP_ID]
		AND DST.[DEPARTMENT_ID]    = SRC.[DEPARTMENT_ID]
		AND DST.[RIGHT_ID]         = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATORGROUP_ID],     [DEPARTMENT_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATORGROUP_ID], SRC.[DEPARTMENT_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLE]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLE] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID] = V.[OPERATOR_ID],
			[VEHICLE_ID]  = V.[VEHICLE_ID],
			[RIGHT_ID]    = @replaceWithRight,
			[ALLOWED]     = V.[ALLOWED]
		FROM [dbo].[OPERATOR_VEHICLE] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME] IN (N'ViewControllerPhone', N'ViewControllerDeviceID')
		AND   V.[ALLOWED] = 1
	) AS SRC ([OPERATOR_ID], [VEHICLE_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
		AND DST.[VEHICLE_ID]  = SRC.[VEHICLE_ID]
		AND DST.[RIGHT_ID]    = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATOR_ID],     [VEHICLE_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLE_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLEGROUP]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLEGROUP] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID]     = V.[OPERATOR_ID],
			[VEHICLEGROUP_ID] = V.[VEHICLEGROUP_ID],
			[RIGHT_ID]        = @replaceWithRight,
			[ALLOWED]         = V.[ALLOWED]
		FROM [dbo].[OPERATOR_VEHICLEGROUP] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME] IN (N'ViewControllerPhone', N'ViewControllerDeviceID')
		AND   V.[ALLOWED] = 1
	) AS SRC ([OPERATOR_ID], [VEHICLEGROUP_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID]     = SRC.[OPERATOR_ID]
		AND DST.[VEHICLEGROUP_ID] = SRC.[VEHICLEGROUP_ID]
		AND DST.[RIGHT_ID]        = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATOR_ID],     [VEHICLEGROUP_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLEGROUP_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO