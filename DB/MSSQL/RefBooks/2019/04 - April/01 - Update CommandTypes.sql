﻿-- Обновление таблицы типов команд
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CommandTypes] AS DST
	USING
	(
		VALUES
			 (31, N'Callback', N'Tracker will call to the phone number and your will be able to hear what is happening nearby')
	) AS SRC ([id], [code], [description])
		ON DST.[id] = SRC.[id]
	WHEN MATCHED AND (DST.[code] <> SRC.[code] OR DST.[description] <> SRC.[description]) THEN
		UPDATE
			SET
				DST.[code]        = SRC.[code],
				DST.[description] = SRC.[description]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO