﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND]
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_SENSOR_LEGEND] AS DST
	USING
	(
		VALUES
		 (001, N'PowerVoltage')
		,(002, N'Fuel')
		,(003, N'TemperatureInCabin')
		,(004, N'Ignition')
		,(005, N'DigitalTemperatureSensor')
		,(006, N'Alarm')
		,(007, N'CANFuel')
		,(008, N'CANTotalRun')
		,(009, N'MainBattery')
		,(010, N'AirConditioner')
		,(011, N'BatteryLevel')
		,(012, N'CarAlarm')
		,(013, N'AccumulatorVoltage')
		,(014, N'GpsEnabled')
		,(015, N'IsPlugged')
		,(016, N'RadioScannerEnabled')
		,(017, N'DataSenderEnabled')
		,(018, N'IsMoving')
		,(021, N'AnalogSensor1')
		,(022, N'AnalogSensor2')
		,(023, N'AnalogSensor3')
		,(024, N'AnalogSensor4')
		,(025, N'AnalogSensor5')
		,(026, N'AnalogSensor6')
		,(027, N'AnalogSensor7')
		,(028, N'AnalogSensor8')
		,(031, N'DigitalSensor1')
		,(032, N'DigitalSensor2')
		,(033, N'DigitalSensor3')
		,(034, N'DigitalSensor4')
		,(035, N'DigitalSensor5')
		,(036, N'DigitalSensor6')
		,(037, N'DigitalSensor7')
		,(038, N'DigitalSensor8')
		,(039, N'VirtualOdometer')
		,(040, N'ConsumedPower')
		,(041, N'Power')
		,(042, N'Current')
		,(043, N'RelayStatus')
		,(044, N'CoverOpeningTimes')
		,(045, N'HeartBeatInterval')
		,(046, N'Door')
		,(047, N'HeadLights')
		,(048, N'Accident')
		,(049, N'Up')
		,(050, N'Down')
		,(051, N'Mark')
		,(052, N'MarkNumber')
		,(053, N'ConcreteMixing')
		,(054, N'ConcreteDumping')
		,(055, N'MotionAlarm')
		,(056, N'LineCrossed')
		,(057, N'FaceDetected')
		,(058, N'AbandonedAlarm')
		,(059, N'LoiteringAlarm')
		,(060, N'PassengersInput')
		,(061, N'PassengersOutput')
		,(062, N'PassengersCount')
		,(063, N'Crane')
		,(064, N'PositionTracking')
		,(065, N'NetworkConnected')
		,(066, N'GPSLocationProvider')
		,(067, N'NetworkLocationProvider')
		,(068, N'WiFi')
		,(069, N'AirplaneMode')
		,(070, N'TruckBody')
		,(071, N'UsedFuelType')
		,(081, N'Door1')
		,(082, N'Door2')
		,(083, N'Door3')
		,(084, N'Door4')
		,(100, N'LastPublishingDurationMilliseconds')
		,(101, N'ErrorSource')
		,(102, N'ErrorCode')
		,(103, N'TimeToFirstGPSFix')
		,(200, N'CAN_Speed')
		,(201, N'CAN_CruiseControl')
		,(202, N'CAN_Brake')
		,(203, N'CAN_ParkingBrake')
		,(204, N'CAN_Clutch')
		,(205, N'CAN_Accelerator')
		,(206, N'CAN_FuelRate')
		,(207, N'CAN_FuelLevel1')
		,(208, N'CAN_FuelLevel2')
		,(209, N'CAN_FuelLevel3')
		,(210, N'CAN_FuelLevel4')
		,(211, N'CAN_FuelLevel5')
		,(212, N'CAN_FuelLevel6')
		,(213, N'CAN_Revs')
		,(214, N'CAN_RunToCarMaintenance')
		,(215, N'CAN_EngHours')
		,(216, N'CAN_CoolantT')
		,(217, N'CAN_EngOilT')
		,(218, N'CAN_FuelT')
		,(219, N'CAN_TotalRun')
		,(220, N'CAN_DayRun')
		,(221, N'CAN_AxleLoad1')
		,(222, N'CAN_AxleLoad2')
		,(223, N'CAN_AxleLoad3')
		,(224, N'CAN_AxleLoad4')
		,(225, N'CAN_AxleLoad5')
		,(226, N'CAN_AxleLoad6')
		,(227, N'CAN_TotalFuelSpend')
		,(228, N'CAN_BrakeSpecificFuelConsumption')
		,(229, N'ControllerMode')
		,(230, N'HDOP')
		,(231, N'Tower')
		,(232, N'FallDown')
		,(233, N'Motion')
		,(234, N'Movement')
		,(235, N'Immobilized')
		,(236, N'GsmSignalLevel')
		,(237, N'LocationService')
		,(238, N'AdditionalEquipment')
		,(239, N'EngineMode')
		,(241, N'DigitalOutput1')
		,(242, N'DigitalOutput2')
		,(243, N'DigitalOutput3')
		,(250, N'InvisibleMode')
		,(251, N'WeakBlow')
		,(252, N'StrongBlow')
		,(253, N'Inclination')
	) AS SRC ([NUMBER], [NAME])
		ON  DST.[Number] = SRC.[NUMBER]
	WHEN MATCHED AND (DST.[NAME] <> SRC.[NAME]) THEN
		UPDATE
			SET
				 DST.[NAME] = SRC.[NAME]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			 [NAME]
			,[Number]
			,[VALUE_EXPIRED]
		)
		VALUES
		(
			 SRC.[NAME]
			,SRC.[Number]
			,86400
		)
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO