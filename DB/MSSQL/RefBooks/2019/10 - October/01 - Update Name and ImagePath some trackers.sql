﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [CONTROLLER_TYPE_ID]-- [TYPE_NAME]
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			-- Первая выглядит как надо, но написана на русском
			(N'Teltonika ТМТ250', N'Teltonika TMT250', N'~/img/devices/Teltonika/TeltonikaTMT250.png', N'~/img/devices/Teltonika/TeltonikaTMT250.png')
	) AS SRC ([TYPE_NAME_OLD], [TYPE_NAME_NEW], [ImagePath_Old], [ImagePath_New])
		ON  DST.[TYPE_NAME] = SRC.[TYPE_NAME_OLD]
	WHEN MATCHED AND (DST.[TYPE_NAME] <> SRC.[TYPE_NAME_NEW] OR DST.[ImagePath] <> SRC.[ImagePath_New]) THEN
		UPDATE
			SET
				 DST.[TYPE_NAME] = CASE WHEN 0 < LEN(SRC.[TYPE_NAME_NEW]) THEN SRC.[TYPE_NAME_NEW] ELSE SRC.[TYPE_NAME_OLD] END
				,DST.[ImagePath] = CASE WHEN 0 < LEN(SRC.[ImagePath_New]) THEN SRC.[ImagePath_New] ELSE SRC.[ImagePath_Old] END
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [CONTROLLER_TYPE_ID]-- [TYPE_NAME]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO