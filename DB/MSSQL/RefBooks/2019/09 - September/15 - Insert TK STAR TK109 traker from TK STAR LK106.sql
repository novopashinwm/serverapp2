﻿DECLARE @baseControllerType nvarchar(50) = N'TK STAR LK106'

BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [TYPE_NAME]
	------------------------------------------------------------------
	INSERT INTO [dbo].[CONTROLLER_TYPE]
	(
		 [TYPE_NAME]
		,[ImagePath]
		,[PACKET_LENGTH]
		,[POS_ABSENCE_SUPPORT]
		,[POS_SMOOTH_SUPPORT]
		,[AllowedToAddByCustomer]
		,[DeviceIdIsImei]
		,[SupportsPassword]
		,[Default_Vehicle_Kind_ID]
		,[DeviceIdIsRequiredForSetup]
		,[LocatorGsmTimeout]
		,[SortOrder]
		,[DefaultPassword]
		,[PasswordAlphabet]
		,[PasswordLength]
		,[SmsTemplateForPasswordChange]
		,[DeviceIdStartIndex]
		,[DeviceIdLength]
	)
	OUTPUT INSERTED.*
	SELECT
		 L.[TYPE_NAME]
		,L.[ImagePath]
		,R.[PACKET_LENGTH]
		,R.[POS_ABSENCE_SUPPORT]
		,R.[POS_SMOOTH_SUPPORT]
		,R.[AllowedToAddByCustomer]
		,R.[DeviceIdIsImei]
		,R.[SupportsPassword]
		,R.[Default_Vehicle_Kind_ID]
		,R.[DeviceIdIsRequiredForSetup]
		,R.[LocatorGsmTimeout]
		,R.[SortOrder]
		,R.[DefaultPassword]
		,R.[PasswordAlphabet]
		,R.[PasswordLength]
		,R.[SmsTemplateForPasswordChange]
		,R.[DeviceIdStartIndex]
		,R.[DeviceIdLength]
	FROM
	(
		VALUES
			 (N'TK STAR TK109', N'~/img/devices/TKSTAR/TKSTARTK109.png')
	) L ([TYPE_NAME], [ImagePath])
		OUTER APPLY
		(
			SELECT * FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @baseControllerType
		) R
	WHERE NOT EXISTS
	(
		SELECT * FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = L.[TYPE_NAME]
	)
	;
	--------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		SELECT
			[TYPE_NAME],
			[SortOrder] =
			CASE [TYPE_NAME]
				WHEN N'Unspecified' THEN 1
				WHEN N'SoftTracker' THEN 2
				ELSE ROW_NUMBER() OVER (ORDER BY [TYPE_NAME]) + 2
			END
		FROM [dbo].[CONTROLLER_TYPE]
	) AS SRC ([TYPE_NAME], [SortOrder])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[SortOrder] = SRC.[SortOrder]
	OUTPUT $action, INSERTED.[TYPE_NAME], INSERTED.[SortOrder], DELETED.[SortOrder]
	;
	--------------------------------------------------------------------
	--SELECT * FROM [dbo].[CONTROLLER_TYPE] ORDER BY [TYPE_NAME]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO