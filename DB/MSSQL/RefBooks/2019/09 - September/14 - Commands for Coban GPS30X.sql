﻿-- Заполняем типы объектов
DECLARE @TYPE_NAMES dbo.Msisdn
INSERT INTO @TYPE_NAMES
OUTPUT INSERTED.*
SELECT
	[value]
FROM
(
	VALUES
		 (N'Coban GPS304B')
		,(N'Coban GPS305')
) T([value])
;
DECLARE @CONTROLLER_TYPE_NAME nvarchar(50)
DECLARE TYPE_NAMES CURSOR LOCAL
READ_ONLY
FOR
	SELECT
		[value]
	FROM @TYPE_NAMES
OPEN TYPE_NAMES
FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	IF (@@FETCH_STATUS <> -2)
	BEGIN
		--/////////////////////////////////////////////////
		UPDATE [dbo].[CONTROLLER_TYPE]
			SET [SupportsPassword] = 1
		WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME
		--/////////////////////////////////////////////////
		BEGIN TRY
			BEGIN TRAN
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			-- Удалить команды у контроллера
			--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			-- Добавить команды к контроллеру
			--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Setup'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetInterval'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskPositionOverMLP'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ReloadDevice'
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			COMMIT TRAN
			--ROLLBACK TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT
				ERROR_NUMBER()    AS ErrorNumber,
				ERROR_SEVERITY()  AS ErrorSeverity,
				ERROR_STATE()     AS ErrorState,
				ERROR_PROCEDURE() AS ErrorProcedure,
				ERROR_LINE()      AS ErrorLine,
				ERROR_MESSAGE()   AS ErrorMessage;
		END CATCH
		--/////////////////////////////////////////////////
	END
	FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
END
GO