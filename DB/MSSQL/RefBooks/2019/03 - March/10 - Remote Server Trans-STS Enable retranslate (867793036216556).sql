﻿DECLARE @serverName nvarchar(255) = N'Trans-STS GalileoSky'
DECLARE @departName nvarchar(255) = N'Trans-STS'
DECLARE @departExt  nvarchar(255) = NULL

BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @RemoteTerminalServerId int = (SELECT TOP(1) [RemoteTerminalServer_ID] FROM [dbo].[RemoteTerminalServer] WHERE [Name] = @serverName COLLATE Cyrillic_General_CI_AS)
	
	IF (@RemoteTerminalServerId IS NOT NULL)
	BEGIN
		--Добавление машин для отправки на удаленный сервер терминала
		MERGE [dbo].[Vehicle_RemoteTerminalServer] AS DST
		USING
		(
			-- Записываем [DEVICE_ID] в обратном порядке
			SELECT
				V.[VEHICLE_ID], @RemoteTerminalServerId, REVERSE(I.[DEVICE_ID])
			FROM [dbo].[VEHICLE] V
				INNER JOIN [dbo].[DEPARTMENT] D
					ON D.[DEPARTMENT_ID] = V.[DEPARTMENT]
				INNER JOIN [dbo].[CONTROLLER] C
					ON C.[VEHICLE_ID] = V.[VEHICLE_ID]
						INNER JOIN [dbo].[CONTROLLER_INFO] I
							ON I.[CONTROLLER_ID] = C.[CONTROLLER_ID]
			WHERE 1 = 1
			AND
			(
				D.[ExtID] IS NULL OR D.[ExtID] = @departExt
			)
			AND D.[NAME]      = @departName
			-- Для конкретного IMEI
			AND CONVERT(varchar(32), I.[DEVICE_ID]) = N'867793036216556'
		) AS SRC ([Vehicle_ID], [RemoteTerminalServer_ID], [DeviceID])
			ON  DST.[Vehicle_ID]              = SRC.[Vehicle_ID]
			AND DST.[RemoteTerminalServer_ID] = SRC.[RemoteTerminalServer_ID]
		WHEN MATCHED THEN
			UPDATE
				SET
					DST.[DeviceID] = SRC.[DeviceID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ([Vehicle_ID], [RemoteTerminalServer_ID], [DeviceID]) VALUES (SRC.[Vehicle_ID], SRC.[RemoteTerminalServer_ID], SRC.[DeviceID])
		OUTPUT $action, INSERTED.*, DELETED.*;
	END
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
END CATCH