DECLARE @serverName  nvarchar(255) = N'Trans-STS GalileoSky'
DECLARE @serverUri   varchar(1024) = N'galileo://193.19.171.139:6364'
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @RemoteTerminalServerId int = (SELECT ISNULL(MAX([RemoteTerminalServer_ID]), 0) + 1 FROM [dbo].[RemoteTerminalServer])
	UPDATE [dbo].[RemoteTerminalServer]
		SET
			@RemoteTerminalServerId = [RemoteTerminalServer_ID],
			[Url]                   = @serverUri
	OUTPUT INSERTED.*, DELETED.*
	WHERE [Name] = @serverName COLLATE Cyrillic_General_CI_AS

	IF (0 = @@ROWCOUNT OR @RemoteTerminalServerId IS NULL)
	BEGIN
		SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] ON;
		INSERT INTO [dbo].[RemoteTerminalServer]
		([RemoteTerminalServer_ID], [Name], [Url])
		OUTPUT INSERTED.*
		VALUES
		(
			@RemoteTerminalServerId,
			@serverName,
			@serverUri
		)
		SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
	END
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH