DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;

-- <summary> Добавить контакт </summary>
-- <remarks> Добавить контакт в телефонную книгу трекера </remarks>
--	AddContact         = 54,
SELECT
	@id                 = 54,
	@code               = N'AddContact',
	@description        = N'Add contact to tracker phone book',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Удалить контакт </summary>
-- <remarks> Удалить контакт из телефонной книги трекера </remarks>
--	DeleteContact      = 55,
SELECT
	@id                 = 55,
	@code               = N'DeleteContact',
	@description        = N'Delete contact from tracker phone book',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds