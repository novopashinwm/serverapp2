DECLARE @IndiaServiceTN  nvarchar(050) = N'INDIA.SMS.Unlim'
DECLARE @IndiaServiceDN  nvarchar(100) = N'Unlimited SMS for India'
DECLARE @IndiaCountryId  int          = (SELECT TOP(1) [Country_ID]  FROM [dbo].[Country]  WHERE [Name]  = N'India')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	-- Услуга отправки SMS без ограничений
	MERGE [dbo].[Billing_Service_Type] AS DST
	USING
	(
		VALUES
			 (@IndiaServiceTN, N'SMS', @IndiaServiceDN, 1)
	) AS SRC ([Service_Type], [Service_Type_Category], [Name], [SMS])
		ON DST.[Service_Type] = SRC.[Service_Type] COLLATE Cyrillic_General_CI_AI
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[Service_Type_Category] = SRC.[Service_Type_Category]
				,DST.[Controller_Type_ID]    = DEFAULT
				,DST.[Vehicle_Kind_ID]       = DEFAULT
				,DST.[MaxQuantity]           = DEFAULT
				,DST.[MinIntervalSeconds]    = DEFAULT
				,DST.[LimitedQuantity]       = DEFAULT
				,DST.[ResetCounter]          = DEFAULT
				,DST.[Singleton]             = DEFAULT
				,DST.[Name]                  = SRC.[Name]
				,DST.[SMS]                   = SRC.[SMS]
				,DST.[LBS]                   = DEFAULT
				,DST.[PeriodDays]            = DEFAULT
				,DST.[MinPrice]              = DEFAULT
				,DST.[MaxPrice]              = DEFAULT
				,DST.[Shared]                = DEFAULT
				,DST.[Removable]             = DEFAULT
				,DST.[DepartmentType]        = DEFAULT
				,DST.[IsManual]              = DEFAULT
				,DST.[AllowCdr]              = DEFAULT
				,DST.[AllowLoginOperator]    = DEFAULT
				,DST.[TrackingSchedule]      = DEFAULT
				,DST.[ServiceDays]           = DEFAULT
				,DST.[MaxLogDepthHours]      = DEFAULT
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Service_Type],     [Service_Type_Category],     [Name],     [SMS])
		VALUES (SRC.[Service_Type], SRC.[Service_Type_Category], SRC.[Name], SRC.[SMS])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	-- Для Индии разрешаем отправлять SMS без ограничений
	INSERT INTO [dbo].[Billing_Service]
		([Billing_Service_Type_ID], [Department_ID], [StartDate])
	OUTPUT INSERTED.*
	SELECT
		[Billing_Service_Type_ID] = B.[ID],
		[Department_ID]           = D.[Department_ID],
		[StartDate]               = GETUTCDATE()
	FROM [dbo].[Billing_Service_Type] B, [dbo].[DEPARTMENT] D
	WHERE D.[Country_ID]   = @IndiaCountryId
	AND   B.[Service_Type] = @IndiaServiceTN
	AND NOT EXISTS
	(
		SELECT
			*
		FROM [dbo].[Billing_Service]
		WHERE [Department_ID]           = D.[DEPARTMENT_ID]
		AND   [Billing_Service_Type_ID] = B.[ID]
	)
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH