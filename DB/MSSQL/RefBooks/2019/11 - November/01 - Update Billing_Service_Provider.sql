﻿DECLARE @IndiaCountryId  int = (SELECT TOP(1) [Country_ID]  FROM [dbo].[Country]  WHERE [Name]  = N'India')
DECLARE @RussiaCountryId int = (SELECT TOP(1) [Country_ID]  FROM [dbo].[Country]  WHERE [Name]  = N'Russia')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DELETE FROM [dbo].[Billing_Service_Provider_Host]
	OUTPUT DELETED.*
	------------------------------------------------------------------
	SET IDENTITY_INSERT [dbo].[Billing_Service_Provider] ON;
	MERGE [dbo].[Billing_Service_Provider] AS DST
	USING
	(
		VALUES
			 (001, N'Ufin.Russia',    @RussiaCountryId)
			,(002, N'Nika-gps.India', @IndiaCountryId)
	) AS SRC ([Billing_Service_Provider_ID], [Name], [Country_ID])
		ON  DST.[Billing_Service_Provider_ID] = SRC.[Billing_Service_Provider_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[Name]       = SRC.[Name]
				,DST.[Country_ID] = SRC.[Country_ID]
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [Billing_Service_Provider_ID],     [Name],     [Country_ID])
		VALUES (SRC.[Billing_Service_Provider_ID], SRC.[Name], SRC.[Country_ID])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	SET IDENTITY_INSERT [dbo].[Billing_Service_Provider] OFF;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
	SET IDENTITY_INSERT [dbo].[Billing_Service_Provider] OFF;
END CATCH
GO