﻿DECLARE @IndiaOperatorId int = (SELECT TOP(1) [OPERATOR_ID] FROM [dbo].[OPERATOR] WHERE [LOGIN] = N'NIKA_India')
BEGIN TRY
	BEGIN TRAN
		MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
		USING
		(
			SELECT
				@IndiaOperatorId, D.[department_id], R.[RIGHT_ID], 1
			FROM [dbo].[RIGHT] R,
			(
				SELECT DISTINCT
					[department_id]
				FROM [dbo].[v_operator_department_right]
				WHERE 1=1
				AND [operator_id] = @IndiaOperatorId
				AND [right_id]    = 002 --SecurityAdministration
			) D
			WHERE R.[RIGHT_ID] IN
			(
				 002 --SecurityAdministration
				,006 --ViewVehicleCurrentStatus
				,007 --EditVehicles
				,025 --ControllerPasswordAccess
				,102 --VehicleAccess
				,104 --DepartmentsAccess
				,110 --ServiceManagement
				,112 --Immobilization
				,115 --PathAccess
				,116 --ChangeDeviceConfigBySMS
				,117 --ManageSensors
			)
		) AS SRC ([OPERATOR_ID], [DEPARTMENT_ID], [RIGHT_ID], [ALLOWED])
			ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
			AND DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
			AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
		WHEN MATCHED AND DST.[ALLOWED] = 0 THEN
			UPDATE
				SET
					DST.[ALLOWED] = 1
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (    [OPERATOR_ID],     [DEPARTMENT_ID],     [RIGHT_ID],     [ALLOWED])
			VALUES (SRC.[OPERATOR_ID], SRC.[DEPARTMENT_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
		WHEN NOT MATCHED BY SOURCE AND DST.[OPERATOR_ID] = @IndiaOperatorId THEN
			DELETE
		OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO