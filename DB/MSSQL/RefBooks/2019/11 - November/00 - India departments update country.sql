﻿DECLARE @IndiaCountryId  int = (SELECT TOP(1) [Country_ID]  FROM [dbo].[Country]  WHERE [Name]  = N'India')
DECLARE @IndiaOperatorId int = (SELECT TOP(1) [OPERATOR_ID] FROM [dbo].[OPERATOR] WHERE [LOGIN] = N'NIKA_India')
BEGIN TRY
	BEGIN TRAN
		UPDATE [dbo].[DEPARTMENT]
			SET [Country_ID] = @IndiaCountryId
		OUTPUT INSERTED.*, DELETED.*
		WHERE [DEPARTMENT_ID] IN
		(
			SELECT DISTINCT
				[department_id]
			FROM [dbo].[v_operator_department_right]
			WHERE 1=1
			AND [operator_id] = @IndiaOperatorId
			AND [right_id]    = 2
		)
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO