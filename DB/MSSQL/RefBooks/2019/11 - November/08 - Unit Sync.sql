﻿-- Обновление таблицы единиц измерения
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[Unit] AS DST
	USING
	(
		VALUES
			 (000, N'None')
			,(001, N'KmPerLitre')
			,(002, N'LitrePer100Km')
			,(003, N'KmPerM3')
			,(004, N'LitrePerKm')
			,(005, N'M3PerMotohour')
			,(006, N'LitrePerMotohour')
			,(011, N'Litre')
			,(012, N'Cbm')
			,(021, N'Volt')
			,(022, N'Watt')
			,(023, N'Ampere')
			,(031, N'Celsius')
			,(041, N'Millisecond')
			,(042, N'Seconds')
			,(043, N'Hours')
			,(051, N'Meters')
			,(052, N'Kilometers')
			,(053, N'TenKilometers')
			,(054, N'Kilometers100')
			,(061, N'Percentage')
			,(062, N'Thing')
			,(063, N'Quantity')
			,(071, N'Kmph')
			,(072, N'Mps')
			,(073, N'RpmMinute')
			,(081, N'KiloWattPerHours')
			,(082, N'Kilogram')
	) AS SRC ([Id], [Name])
		ON DST.[Id] = SRC.[Id]
	WHEN MATCHED AND DST.[Name] <> SRC.[Name] THEN
		UPDATE
			SET
				DST.[Name] = SRC.[Name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Id], [Name]) VALUES (SRC.[Id], SRC.[Name])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO