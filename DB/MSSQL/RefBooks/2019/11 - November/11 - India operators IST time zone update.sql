﻿DECLARE @IndiaCountryId  int = (SELECT TOP(1) [Country_ID]  FROM [dbo].[Country]  WHERE [Name]  = N'India')
DECLARE @IndiaOperatorId int = (SELECT TOP(1) [OPERATOR_ID] FROM [dbo].[OPERATOR] WHERE [LOGIN] = N'NIKA_India')
DECLARE @IndiaTimeZoneId int = (SELECT TOP(1) [TimeZone_ID] FROM [dbo].[TimeZone] WHERE [Code]  = N'India Standard Time')
DECLARE @IndiaCultureId  int = (SELECT TOP(1) [Culture_ID]  FROM [dbo].[Culture]  WHERE [Code]  = N'en-US')
BEGIN TRY
	BEGIN TRAN
		UPDATE [dbo].[OPERATOR]
			SET
				[TimeZone_ID] = @IndiaTimeZoneId,
				[Culture_ID]  = @IndiaCultureId
		OUTPUT INSERTED.*, DELETED.*
		WHERE [OPERATOR_ID] IN
		(
			SELECT DISTINCT
				[operator_id]
			FROM [dbo].[v_operator_department_right]
			WHERE 1=1
			AND [department_id] IN
			(
				SELECT DISTINCT
					[department_id]
				FROM [dbo].[v_operator_department_right]
				WHERE 1=1
				AND [operator_id] = @IndiaOperatorId
				AND [right_id]    = 2 -- SecurityAdministration
			)
			AND [right_id]    = 104 -- DepartmentsAccess
		)
		AND [LOGIN] NOT IN (N'admin')
		AND
		(
				[TimeZone_ID] <> @IndiaTimeZoneId
				OR
				[Culture_ID]  <> @IndiaCultureId
		)
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO