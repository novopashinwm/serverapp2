﻿-- Обновление таблицы типов точек на карте
------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[WEB_POINT_TYPE] ON
------------------------------------------------------------------
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[WEB_POINT_TYPE] AS DST
	USING
	(
		VALUES
			 (1, N'UserPoint',     N'Web points that are of interest users')
			,(2, N'EmergencyCall', N'Web points that are emergency calls')
	) AS SRC ([Id], [Name], [Desc])
		ON DST.[TYPE_ID] = SRC.[Id]
	WHEN MATCHED AND DST.[Name] <> SRC.[Name] THEN
		UPDATE
			SET
				DST.[NAME]        = SRC.[Name],
				DST.[DESCRIPTION] = SRC.[Desc]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([TYPE_ID], [NAME], [DESCRIPTION]) VALUES (SRC.[Id], SRC.[Name], SRC.[Desc])
	OUTPUT $action, INSERTED.*, DELETED.*
	;

	INSERT INTO [dbo].[TRAIL] ([TRAIL_TIME]) SELECT GETUTCDATE() DECLARE @TRAIL_ID int = SCOPE_IDENTITY();
	INSERT INTO [dbo].[H_WEB_POINT_TYPE]
		([TYPE_ID],[NAME],[DESCRIPTION], [ACTION], [ACTUAL_TIME], [TRAIL_ID])
	OUTPUT INSERTED.*
	SELECT [TYPE_ID],[NAME],[DESCRIPTION], 'UPLOAD', GETUTCDATE(), @TRAIL_ID
	FROM [dbo].[WEB_POINT_TYPE] T
	WHERE NOT EXISTS (SELECT * FROM [dbo].[H_WEB_POINT_TYPE] WHERE [TYPE_ID] = T.[TYPE_ID])

	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
------------------------------------------------------------------
SET IDENTITY_INSERT [dbo].[WEB_POINT_TYPE] OFF
------------------------------------------------------------------
GO