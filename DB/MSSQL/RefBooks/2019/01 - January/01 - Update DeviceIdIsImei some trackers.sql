﻿-- DeviceIdIsImei для некоторых трекеров
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			 (N'Sentar Q60, Q60S (кулон)', 0)
			,(N'Sentar Q80',               0)
			,(N'Sentar T58 (V82)',         0)
			,(N'Vjoy Сar TK10SE',          1)
	) AS SRC ([TYPE_NAME], [DeviceIdIsImei])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED AND DST.[DeviceIdIsImei] <> SRC.[DeviceIdIsImei] THEN
		UPDATE
			SET
				DST.[DeviceIdIsImei] = SRC.[DeviceIdIsImei]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO