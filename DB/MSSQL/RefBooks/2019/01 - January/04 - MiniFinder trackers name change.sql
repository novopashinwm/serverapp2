﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	UPDATE T
		SET
			T.[TYPE_NAME] = REPLACE(T.[TYPE_NAME], N'Minifinder', N'MiniFinder'),
			T.[ImagePath] = REPLACE(T.[ImagePath], N'Minifinder', N'MiniFinder')
	OUTPUT INSERTED.*, DELETED.*
	FROM CONTROLLER_TYPE T
	WHERE UPPER(T.[TYPE_NAME]) LIKE UPPER(N'%MiniFinder%')
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH