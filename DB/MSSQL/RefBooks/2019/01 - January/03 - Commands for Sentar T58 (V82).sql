﻿DECLARE @CONTROLLER_TYPE_NAME nvarchar(50) = N'Sentar T58 (V82)'
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	-- Удалить команды у контроллера
	EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ShutdownDevice'
	------------------------------------------------------------------
	-- Добавить команды к контроллеру
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO