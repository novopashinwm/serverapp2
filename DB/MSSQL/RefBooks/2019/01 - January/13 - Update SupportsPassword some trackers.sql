﻿-- SupportsPassword для некоторых трекеров
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		VALUES
			 (N'Coban TK1042',              1)
			,(N'Coban TK104SD',             1)
			,(N'TK STAR LK106',             1)
			,(N'Sentar T58 (V82)',          1)
			,(N'Futureway PetTracker FP03', 1)
	) AS SRC ([TYPE_NAME], [SupportsPassword])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED AND DST.[SupportsPassword] <> SRC.[SupportsPassword] THEN
		UPDATE
			SET
				DST.[SupportsPassword] = SRC.[SupportsPassword]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO