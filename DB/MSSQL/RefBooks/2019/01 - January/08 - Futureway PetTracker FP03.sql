BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME      nvarchar(50)  = N'Futureway PetTracker FP03'
	--------------------------------------------------------------------
	--Добавляем контроллер
	EXEC [dbo].[AddControllerType]
		@type_name              = @CONTROLLER_TYPE_NAME,
		@DeviceIdIsImei         = 1,
		@SupportsPassword       = 0,
		@AllowedToAddByCustomer = 1,
		@ImagePath              = '~/img/devices/Futureway/FuturewayPetTrackerFP03.jpg'
	--------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO