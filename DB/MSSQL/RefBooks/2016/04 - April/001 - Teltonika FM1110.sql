exec AddControllerType 'Teltonika FM1110', 1, 1

exec AddControllerTypeCommand 'Teltonika FM1110', 'Setup'
exec AddControllerTypeCommand 'Teltonika FM1110', 'Immobilize'
exec AddControllerTypeCommand 'Teltonika FM1110', 'Deimmobilize'

exec AddOrUpdateControllerSensor 'Teltonika FM1110', 26, 'Ain fuel leak', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 9, 'Analog Input 1', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 10, 'Analog Input 2', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 11, 'Analog Input 3', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 19, 'Analog Input 4', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 145, 'CAN 01', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 146, 'CAN 02', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 147, 'CAN 03', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 148, 'CAN 04', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 149, 'CAN 05', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 150, 'CAN 06', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 151, 'CAN 07', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 152, 'CAN 08', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 153, 'CAN 09', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 154, 'CAN 10', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 4, 'Digital Input 4', 1, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 1, 'Digital Input Status 1', 1, 'Зажигание'
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 2, 'Digital Input Status 2', 1, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 3, 'Digital Input Status 3', 1, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 179, 'Digital output 1 state', 1, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 180, 'Digital output 2 state', 1, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 27, 'Fuel leak', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 200, 'Fuel level meter', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 203, 'Fuel level meter 2', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 201, 'Fuel temperature', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 204, 'Fuel temperature 2', 0, NULL
exec AddOrUpdateControllerSensor 'Teltonika FM1110', 199, 'Virtual Odometer', 0, NULL
