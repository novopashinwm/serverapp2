update vp
	set value = replace(replace(replace(value, ' ', '_'), '-', '_'), '%20', '_')
	from VEHICLE_PROFILE vp
	where PROPERTY_NAME = 'icon'
	  and value <> replace(replace(replace(value, ' ', '_'), '-', '_'), '%20', '_')
