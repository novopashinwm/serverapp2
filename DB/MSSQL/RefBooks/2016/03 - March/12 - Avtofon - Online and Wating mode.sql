exec AddCommandType 39, 'SetModeOnline', 'Enables online sending of location', 86400, 'SecurityAdministration', null
exec AddCommandType 40, 'SetModeWaiting', 'Enables waiting mode',                 60, 'SecurityAdministration', null

exec AddControllerTypeCommand 'Avtofon', 'SetModeOnline'
exec AddControllerTypeCommand 'Avtofon', 'SetModeWaiting'