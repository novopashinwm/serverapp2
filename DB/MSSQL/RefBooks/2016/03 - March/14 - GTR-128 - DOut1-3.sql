exec AddSensorLegend 241, 'DigitalOutput1', 1
exec AddSensorLegend 242, 'DigitalOutput2', 1
exec AddSensorLegend 243, 'DigitalOutput3', 1

exec AddOrUpdateControllerSensor 'GlobalSat GTR-128', 31, 'DOut1', 1, 'DigitalOutput1', 0

exec RemoveControllerSensor 'GlobalSat GTR-128', 'DOut2'
exec RemoveControllerSensor 'GlobalSat GTR-128', 'DOut3'

exec AddControllerTypeCommand 'GlobalSat GTR-128', 'Immobilize'
exec AddControllerTypeCommand 'GlobalSat GTR-128', 'Deimmobilize'

exec RemoveControllerTypeCommand 'GlobalSat GTR-128', 'CutOffElectricity'
exec RemoveControllerTypeCommand 'GlobalSat GTR-128', 'ReopenElectricity'
exec RemoveControllerTypeCommand 'GlobalSat GTR-128', 'CutOffFuel'
exec RemoveControllerTypeCommand 'GlobalSat GTR-128', 'ReopenFuel'
