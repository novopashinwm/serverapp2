declare @media_type int = 9

insert into MEDIA_TYPE 
	select *
		from (select ID = @media_type, Name = 'Emulator') t
		where not exists (select * from MEDIA_TYPE e where e.ID = t.ID)

insert into MEDIA (NAME, TYPE)
	select * 
		from (select name = 'GeneratingEmulator', type = @media_type) t
		where not exists (select * from MEDIA e where e.TYPE = t.type)

insert into MEDIA_ACCEPTORS (MEDIA_ID, INITIALIZATION, DESCRIPTION, Name)
	select m.MEDIA_ID, t.Initialization, t.Desciption, m.NAME
		from (select Initialization = N'', Desciption = N'') t, MEDIA m
		where m.TYPE = @media_type
		  and not exists (select * from MEDIA_ACCEPTORS e where e.MEDIA_ID = m.media_id)

update ma	
	set Name = m.NAME
	from MEDIA m 
	join MEDIA_ACCEPTORS ma on ma.MEDIA_ID = m.MEDIA_ID
	where m.NAME = 'GeneratingEmulator' 
	  and ma.Name is null

exec AddControllerType 'GeneratingEmulator', 0, 0, 0

exec AddOrUpdateControllerSensor 'GeneratingEmulator', 1, 'DI1', 1, '�������� ������ 1', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 2, 'DI2', 1, '�������� ������ 2', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 3, 'DI3', 1, '�������� ������ 3', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 4, 'DI4', 1, '�������� ������ 4', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 5, 'DI5', 1, '�������� ������ 5', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 6, 'DI6', 1, '�������� ������ 6', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 7, 'DI7', 1, '�������� ������ 7', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 8, 'DI8', 1, '�������� ������ 8', 1

exec AddOrUpdateControllerSensor 'GeneratingEmulator', 11, 'AI1', 0, '���������� ������ 1', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 12, 'AI2', 0, '���������� ������ 2', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 13, 'AI3', 0, '���������� ������ 3', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 14, 'AI4', 0, '���������� ������ 4', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 15, 'AI5', 0, '���������� ������ 5', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 16, 'AI6', 0, '���������� ������ 6', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 17, 'AI7', 0, '���������� ������ 7', 1
exec AddOrUpdateControllerSensor 'GeneratingEmulator', 18, 'AI8', 0, '���������� ������ 8', 1

