exec AddOrUpdateControllerSensor 'GlobalSat GTR-128', 42, 'BatteryVoltage', 0, 'Напряжение питания', 1, 0.001, 2

update map
	set CONSTANT = 2
	from CONTROLLER_TYPE ct
	join CONTROLLER c on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID	
	join CONTROLLER_SENSOR_MAP map on map.CONTROLLER_ID = c.CONTROLLER_ID
	join CONTROLLER_SENSOR_LEGEND legend on legend.CONTROLLER_SENSOR_LEGEND_ID = map.CONTROLLER_SENSOR_LEGEND_ID
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_SENSOR_ID = map.CONTROLLER_SENSOR_ID
	where ct.TYPE_NAME = 'GlobalSat GTR-128'
	  and legend.NAME = 'Напряжение питания'
	  and cs.DESCRIPT = 'BatteryVoltage'
	  and map.CONSTANT = 0
