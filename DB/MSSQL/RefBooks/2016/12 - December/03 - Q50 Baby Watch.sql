

select ct.controller_type_id
	into #obsolete
	from controller_type ct
	where 	ct.ImagePath like '%sentar_q60.jpg' and ct.type_name not like '%кулон%'
	or	ct.ImagePath like '%e-fortune_deest_69.jpg' and ct.type_name not like '%трекер%'

delete ctcmd from #obsolete o
	join controller_Type_CommandTypes ctcmd on ctcmd.controller_type_id = o.controller_type_id

delete ct from #obsolete o
	join controller_type ct on ct.controller_type_id = o.controller_type_id

declare @t table (i int identity, name varchar(50), photo varchar(255))

insert into @t (name, photo)
	values
		('Kid Tracker plus Q50 (G36), Q50S', 'Kid Tracker plus Q50 (G36).png'),
		('Kid Tracker Q40, Q40S', 'Kid Tracker Q40, Q40S.png'),
		('Kid Tracker V80', 'Kid Tracker V80.png'),
		('Kid Tracker V81', 'Kid Tracker V81.png'),
		--('Q50 Baby Watch', 'BabyWatch.png'),
		('Sentar T58 (V82)', 'sentar_v82.jpg'),
		('Sentar Q60, Q60S (кулон)', 'Sentar_Q60.jpg'),
		('Sentar Q80', 'SentarQ80.jpg'),
		('E-Fortune DS99', 'E-Fortune_Deest_69.jpg'),
		('E-Fortune Deest 69 (трекер на ошейник)', 'E-Fortune_Deest_69.jpg')

select * from CONTROLLER_TYPE order by controller_type_id desc

declare @i int = 1

while @i <= (select count(1) from @t)
begin

	declare @name varchar(50)
	declare @photo varchar(255)

	select @name = name
		, @photo = case when len(photo) <> 0 then '/about/includes/sources/images/devices/Q50/' + photo else null end
		from @t
		where i = @i
		
	--'Q50 Baby Watch'
	exec AddControllerType	@type_name = @name, @DeviceIdIsImei = 1, @SupportsPassword = 1, @AllowedToAddByCustomer = 1, @ImagePath = @photo
	
	exec AddControllerTypeCommand 	@controllerTypeName = @name,	@commandTypeCode = 'Setup'
	
	update controller_type set DeviceIdStartIndex = 5, DeviceIdLength = 10 where type_name = @name	

	set @i += 1

end

