exec AddControllerType 'Keelin K20', 1, 0

exec AddOrUpdateControllerSensor 'Keelin K20', 1, 'Alarm Type', 0
exec AddOrUpdateControllerSensor 'Keelin K20', 2, 'BatteryVoltage', 0, 'AccumulatorVoltage'
exec AddOrUpdateControllerSensor 'Keelin K20', 5, 'ChargerPlugged', 1, 'IsPlugged', 1

update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/KeelinK20.jpg' where TYPE_NAME='Keelin K20'

exec AddControllerTypeCommand 'Keelin K20', 'Setup'