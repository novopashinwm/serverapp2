--�������� � ������� ������ ���� � ������ � �������� ��������
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatTR203.jpg' where CONTROLLER_TYPE_ID = 19
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatTR600.jpg' where CONTROLLER_TYPE_ID = 29
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatGTR128.jpg' where CONTROLLER_TYPE_ID = 49
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMT90.jpg' where CONTROLLER_TYPE_ID = 71
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06N.jpg' where CONTROLLER_TYPE_ID = 64
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06.jpg' where CONTROLLER_TYPE_ID = 65
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartBBMobile.jpg' where CONTROLLER_TYPE_ID = 66
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT02A.jpg' where CONTROLLER_TYPE_ID = 69
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK103.jpg', type_name = 'Xexun TK103' where CONTROLLER_TYPE_ID = 31
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK1022.jpg', type_name = 'Xexun TK102' where CONTROLLER_TYPE_ID = 74
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartTR06A.jpg', type_name = 'Smart TR06A' where CONTROLLER_TYPE_ID = 76
go
update controller_type set ImagePath = '/about/includes/sources/images/devices/Avtofon/Avtofon.gif', type_name = 'Avtofon SE' where CONTROLLER_TYPE_ID = 53
go


--'Meitrack MT90', 'Meitrack MT90S', 'Meitrack MVT100', 'Meitrack MVT380', 'Meitrack MVT800', 'Meitrack T1', 'Meitrack T622', 'Meitrack TC68', 'Meitrack TC68S', 'Meitrack Treckids Bear', 'Meitrack Treckids Turtle' 
exec AddControllerType	@type_name = 'Meitrack MT90', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMT90.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack MT90',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack MT90S', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMT90S.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack MT90S',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack MVT100', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMVT100.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack MVT100',	@commandTypeCode = 'Setup'
go


exec AddControllerType	@type_name = 'Meitrack MVT380', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMVT380.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack MVT380',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack T1', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackT1.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack T1',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack T622', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackT622.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack T622',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack TC68', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackTC68.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack TC68',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack TC68S', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackTC68S.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack TC68S',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack Treckids Bear', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackTreckidsBear.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack Treckids Bear',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Meitrack Treckids Turtle', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackTreckidsTurtle.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Meitrack Treckids Turtle',	@commandTypeCode = 'Setup'
go

--'Xexun TK101', 'Xexun TK102', 'Xexun TK103', 'Xexun TK104SD', 'Xexun TK201', 'Xexun TK1022', 'Xexun TK1032', 'Xexun TK1042', 'Xexun TK2012', 'Xexun TK2013', 'Xexun XT008', 'Xexun XT009', 'Xexun XT107'
exec AddControllerType	@type_name = 'Xexun TK101', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK101.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK101',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK102', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK102.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK102',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK103', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK103.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK103',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK104SD', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK104SD.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK104SD',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK201', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK201.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK201',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK1022', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK1022.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK1022',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK1032', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK1032.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK1032',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK1042', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK1042.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK1042',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK2012', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK2012.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK2012',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun TK2013', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK2013.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun TK2013',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun XT008', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunXT008.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun XT008',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun XT009', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunXT009.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun XT009',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Xexun XT107', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Xexun/XexunXT107.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Xexun XT107',	@commandTypeCode = 'Setup'
go



--'Coban GPS304B', 'Coban GPS305', 'Coban GPS306A', 'Coban TK102', 'Coban TK103', 'Coban TK104SD', 'Coban TK106', 'Coban TK1022', 'Coban TK1042', 'Coban TK1062'
exec AddControllerType	@type_name = 'Coban GPS304B', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanGPS304B.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban GPS304B',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban GPS305', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanGPS305.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban GPS305',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban GPS306A', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanGPS306A.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban GPS306A',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK102', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK102.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK102',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK103', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK103.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK103',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK104SD', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK104SD.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK104SD',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK106', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK106.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK106',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK1022', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK1022.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK1022',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK1042', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK1042.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK1042',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Coban TK1062', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Coban/CobanTK1062.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Coban TK1062',	@commandTypeCode = 'Setup'
go


--'Smarts Baby Bear', 'Smarts GT02A', 'Smart GT02B', 'Smart GT03', 'Smart GT03A', 'Smart GT05', 'Smart GT06', 'Smart GT06N', 'Smart GT07', 'Smart GT09B', 'Smart TR02', 'Smart TR06', 'Smart TR06A',
exec AddControllerType	@type_name = 'Smarts Baby Bear', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartBBMobile.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts Baby Bear',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smarts GT02A', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT02A.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT02A',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT02B', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT02B.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT02B',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT03', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT03.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT03',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT03A', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT03A.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT03A',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT05', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT05.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT05',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT06', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT06',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT06N', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06N.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT06N',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT07', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT07.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT07',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart GT09B', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT09B.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts GT09B',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart TR02', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartTR02.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts TR02',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart TR06', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartTR06.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts TR06',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Smart TR06A', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Smart/SmartTR06A.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Smarts TR06A',	@commandTypeCode = 'Setup'
go


--'Minifinder Pico VG10'
exec AddControllerType	@type_name = 'Minifinder Pico VG10', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Minifinder/MinifinderPicoVG10.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Minifinder Pico VG10',	@commandTypeCode = 'Setup'
go

--'Anywhere TK106', 'Anywhere TK108'
exec AddControllerType	@type_name = 'Anywhere TK106', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Anywhere/AnywhereTK106.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Anywhere TK106',	@commandTypeCode = 'Setup'
go

exec AddControllerType	@type_name = 'Anywhere TK108', @DeviceIdIsImei = 1, @SupportsPassword = 0, @AllowedToAddByCustomer = 1, @ImagePath = '/about/includes/sources/images/devices/Anywhere/AnywhereTK108.jpg'
go
exec AddControllerTypeCommand 	@controllerTypeName = 'Anywhere TK108',	@commandTypeCode = 'Setup'
go


/*
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatTR203.jpg' where CONTROLLER_TYPE_ID = 19
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatTR600.jpg' where CONTROLLER_TYPE_ID = 29
update controller_type set ImagePath = '/about/includes/sources/images/devices/Globalsat/GlobalSatGTR128.jpg' where CONTROLLER_TYPE_ID = 49
update controller_type set ImagePath = '/about/includes/sources/images/devices/Meitrack/MeitrackMT90.jpg' where CONTROLLER_TYPE_ID = 71
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06N.jpg' where CONTROLLER_TYPE_ID = 64
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT06.jpg' where CONTROLLER_TYPE_ID = 65
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartBBMobile.jpg' where CONTROLLER_TYPE_ID = 66
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartGT02A.jpg' where CONTROLLER_TYPE_ID = 69
update controller_type set ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK103.jpg', type_name = 'Xexun TK103' where CONTROLLER_TYPE_ID = 31
update controller_type set ImagePath = '/about/includes/sources/images/devices/Xexun/XexunTK1022.jpg', type_name = 'Xexun TK102' where CONTROLLER_TYPE_ID = 74
update controller_type set ImagePath = '/about/includes/sources/images/devices/Smart/SmartTR06A.jpg', type_name = 'Smart TR06A' where CONTROLLER_TYPE_ID = 76
update controller_type set ImagePath = '/about/includes/sources/images/devices/Avtofon/Avtofon.gif', type_name = 'Avtofon SE' where CONTROLLER_TYPE_ID = 53
*/
/*
sp_helptext 'AddControllerTypeCommand'
sp_helptext 'AddControllerTypeCommand' '�������� �������', 'Setup'

create procedure AddControllerTypeCommand
(
	@controllerTypeName nvarchar(50),
	@commandTypeCode    varchar(200)
)
as
insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.id
		from CONTROLLER_TYPE ct, CommandTypes cmd
		where ct.TYPE_NAME = @controllerTypeName
			and cmd.code = @commandTypeCode
			and not exists (select 1 from Controller_Type_CommandTypes e where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID and e.CommandTypes_ID = cmd.id)
*/