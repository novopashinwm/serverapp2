﻿-- <summary> Связь с FOTA </summary>
-- <remarks> Команда подключает трекер к сервису настройки FOTA </remarks>
-- Teltonika_ConnectFOTA             = 130
EXEC [dbo].[AddCommandType]
 @id                 = 130,
 @code               = N'Teltonika_ConnectFOTA',
 @description        = N'The command connects the device with FOTA service',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Отключить аналоговый вход </summary>
-- <remarks> Отключение контроля аналогового входа </remarks>
-- Teltonika_SetAnInOff              = 131
EXEC [dbo].[AddCommandType]
 @id                 = 131,
 @code               = N'Teltonika_SetAnInOff',
 @description        = N'Turn off analogue input control',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Отключить передачу напряжения питания </summary>
-- <remarks> Отключить передачу напряжения питания на сервер </remarks>
-- Teltonika_SetVoltageControlOff    = 132
EXEC [dbo].[AddCommandType]
 @id                 = 132,
 @code               = N'Teltonika_SetVoltageControlOff',
 @description        = N'Turn off power supply voltage control',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Запись входа Analog Input 1 - раз в 5 минут </summary>
-- <remarks> Записывать значения на аналоговом входе 1 с частотой 1 раз в 5 минут </remarks>
-- Teltonika_SetAnInpRec1To1Per5Min  = 133
EXEC [dbo].[AddCommandType]
 @id                 = 133,
 @code               = N'Teltonika_SetAnInpRec1To1Per5Min',
 @description        = N'Set Analog Input 1 record frequency — 1 time per 5 mins',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Запись External Voltage - раз в 5 минут </summary>
-- <remarks> Записывать значения внешнего питания 1 с частотой 1 раз в 5 минут </remarks>
-- Teltonika_SetExtVltRecTo1Per5Min  = 134
EXEC [dbo].[AddCommandType]
 @id                 = 134,
 @code               = N'Teltonika_SetExtVltRecTo1Per5Min',
 @description        = N'Set  External Voltage value record frequency — 1 time per 5 mins',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO