﻿-- Заполняем типы объектов
DECLARE @TYPE_NAMES dbo.Msisdn
INSERT INTO @TYPE_NAMES
OUTPUT INSERTED.*
SELECT
	[value]
FROM
(
	VALUES
		 (N'Teltonika FM1100')
		,(N'Teltonika FM1110')
		,(N'Teltonika FM1125')
		,(N'Teltonika FM1200')
		,(N'Teltonika FM2200')
		,(N'Teltonika FM3101')
		,(N'Teltonika FM5300')
		,(N'Teltonika FMA202')
		,(N'Teltonika FMB001')
		,(N'Teltonika FMB010')
		,(N'Teltonika FMB110')
		,(N'Teltonika FMB120')
		,(N'Teltonika FMB125')
		,(N'Teltonika FMB140')
		,(N'Teltonika FMB202')
		,(N'Teltonika FMB630')
		,(N'Teltonika FMB640')
		,(N'Teltonika FMB900')
		,(N'Teltonika FMB920')
) T([value])
;
DECLARE @CONTROLLER_TYPE_NAME nvarchar(50)
DECLARE TYPE_NAMES CURSOR LOCAL
READ_ONLY
FOR
	SELECT
		[value]
	FROM @TYPE_NAMES
OPEN TYPE_NAMES
FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	IF (@@FETCH_STATUS <> -2)
	BEGIN
		--/////////////////////////////////////////////////
		UPDATE [dbo].[CONTROLLER_TYPE]
			SET [SupportsPassword] = 1
		WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME
		--/////////////////////////////////////////////////
		BEGIN TRY
			BEGIN TRAN
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			-- Удалить команды у контроллера
			--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			-- Добавить команды к контроллеру
			--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Teltonika_ConnectFOTA'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Teltonika_SetAnInOff'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Teltonika_SetVoltageControlOff'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Teltonika_SetAnInpRec1To1Per5Min'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Teltonika_SetExtVltRecTo1Per5Min'
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			COMMIT TRAN
			--ROLLBACK TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT
				ERROR_NUMBER()    AS ErrorNumber,
				ERROR_SEVERITY()  AS ErrorSeverity,
				ERROR_STATE()     AS ErrorState,
				ERROR_PROCEDURE() AS ErrorProcedure,
				ERROR_LINE()      AS ErrorLine,
				ERROR_MESSAGE()   AS ErrorMessage;
		END CATCH
		--/////////////////////////////////////////////////
	END
	FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
END
GO