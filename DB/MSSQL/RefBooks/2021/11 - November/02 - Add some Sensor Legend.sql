﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	EXEC AddSensorLegend @number = 460, /*|*/ @name = N'Ecodriving',                 /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 461, /*|*/ @name = N'RefrigerationUnitOperation', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 462, /*|*/ @name = N'AxisXAcceleration',          /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 463, /*|*/ @name = N'AxisYAcceleration',          /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 464, /*|*/ @name = N'AxisZAcceleration',          /*|*/ @digital = 0
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH