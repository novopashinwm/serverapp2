-- <summary> Очистить черный ящик </summary>
-- <remarks> Команда очищает историю накопленную в трекере </remarks>
-- BlackBoxClear            = 80
EXEC [dbo].[AddCommandType]
 @id                 = 80,
 @code               = N'BlackBoxClear',
 @description        = N'The command clears the history accumulated in the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> CAN: Активировать CAN-скрипт 1 </summary>
-- <remarks> Команда выполняет CAN-скрипт 1 на трекере </remarks>
-- RunCanScript01           = 81
EXEC [dbo].[AddCommandType]
 @id                 = 81,
 @code               = N'RunCanScript01',
 @description        = N'The command executes CAN script 1 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> CAN: Активировать CAN-скрипт 2 </summary>
-- <remarks> Команда выполняет CAN-скрипт 2 на трекере </remarks>
-- RunCanScript03           = 82
EXEC [dbo].[AddCommandType]
 @id                 = 82,
 @code               = N'RunCanScript03',
 @description        = N'The command executes CAN script 2 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> CAN: Активировать CAN-скрипт 3 </summary>
-- <remarks> Команда выполняет CAN-скрипт 3 на трекере </remarks>
-- RunCanScript02           = 83
EXEC [dbo].[AddCommandType]
 @id                 = 83,
 @code               = N'RunCanScript02',
 @description        = N'The command executes CAN script 3 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Выход 1: включить </summary>
-- <remarks> Команда включает Выход 1 на трекере </remarks>
-- SetOut01On               = 90
EXEC [dbo].[AddCommandType]
 @id                 = 90,
 @code               = N'SetOut01On',
 @description        = N'The command turns on Output 1 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Выход 1: выключить </summary>
-- <remarks> Команда выключает Выход 1 на трекере </remarks>
-- SetOut01Off              = 91
EXEC [dbo].[AddCommandType]
 @id                 = 91,
 @code               = N'SetOut01Off',
 @description        = N'The command turns off Output 1 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Выход 2: включить </summary>
-- <remarks> Команда включает Выход 2 на трекере </remarks>
-- SetOut02On               = 92
EXEC [dbo].[AddCommandType]
 @id                 = 92,
 @code               = N'SetOut02On',
 @description        = N'The command turns on Output 2 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Выход 2: выключить </summary>
-- <remarks> Команда выключает Выход 2 на трекере </remarks>
-- SetOut02Off              = 93
EXEC [dbo].[AddCommandType]
 @id                 = 93,
 @code               = N'SetOut02Off',
 @description        = N'The command turns off Output 2 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 1: включить </summary>
-- <remarks> Команда включает Дополнительный выход 1 на трекере </remarks>
-- SetExtOut01On            = 100
EXEC [dbo].[AddCommandType]
 @id                 = 100,
 @code               = N'SetExtOut01On',
 @description        = N'The command turns on Extra output 1 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 1: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 1 на трекере </remarks>
-- SetExtOut01Off           = 101
EXEC [dbo].[AddCommandType]
 @id                 = 101,
 @code               = N'SetExtOut01Off',
 @description        = N'The command turns off Extra output 1 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 2: включить </summary>
-- <remarks> Команда включает Дополнительный выход 2 на трекере </remarks>
-- SetExtOut02On            = 102
EXEC [dbo].[AddCommandType]
 @id                 = 102,
 @code               = N'SetExtOut02On',
 @description        = N'The command turns on Extra output 2 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 2: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 2 на трекере </remarks>
-- SetExtOut02Off           = 103
EXEC [dbo].[AddCommandType]
 @id                 = 103,
 @code               = N'SetExtOut02Off',
 @description        = N'The command turns off Extra output 2 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 3: включить </summary>
-- <remarks> Команда включает Дополнительный выход 3 на трекере </remarks>
-- SetExtOut03On            = 104
EXEC [dbo].[AddCommandType]
 @id                 = 104,
 @code               = N'SetExtOut03On',
 @description        = N'The command turns on Extra output 3 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 3: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 3 на трекере </remarks>
-- SetExtOut03Off           = 105
EXEC [dbo].[AddCommandType]
 @id                 = 105,
 @code               = N'SetExtOut03Off',
 @description        = N'The command turns off Extra output 3 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 4: включить </summary>
-- <remarks> Команда включает Дополнительный выход 4 на трекере </remarks>
-- SetExtOut04On            = 106
EXEC [dbo].[AddCommandType]
 @id                 = 106,
 @code               = N'SetExtOut04On',
 @description        = N'The command turns on Extra output 4 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 4: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 4 на трекере </remarks>
-- SetExtOut04Off           = 107
EXEC [dbo].[AddCommandType]
 @id                 = 107,
 @code               = N'SetExtOut04Off',
 @description        = N'The command turns off Extra output 4 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 5: включить </summary>
-- <remarks> Команда включает Дополнительный выход 5 на трекере </remarks>
-- SetExtOut05On            = 108
EXEC [dbo].[AddCommandType]
 @id                 = 108,
 @code               = N'SetExtOut05On',
 @description        = N'The command turns on Extra output 5 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 5: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 5 на трекере </remarks>
-- SetExtOut05Off           = 109
EXEC [dbo].[AddCommandType]
 @id                 = 109,
 @code               = N'SetExtOut05Off',
 @description        = N'The command turns off Extra output 5 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 6: включить </summary>
-- <remarks> Команда включает Дополнительный выход 6 на трекере </remarks>
-- SetExtOut06On            = 110
EXEC [dbo].[AddCommandType]
 @id                 = 110,
 @code               = N'SetExtOut06On',
 @description        = N'The command turns on Extra output 6 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 6: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 6 на трекере </remarks>
-- SetExtOut06Off           = 111
EXEC [dbo].[AddCommandType]
 @id                 = 111,
 @code               = N'SetExtOut06Off',
 @description        = N'The command turns off Extra output 6 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 7: включить </summary>
-- <remarks> Команда включает Дополнительный выход 7 на трекере </remarks>
-- SetExtOut07On            = 112
EXEC [dbo].[AddCommandType]
 @id                 = 112,
 @code               = N'SetExtOut07On',
 @description        = N'The command turns on Extra output 7 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 7: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 7 на трекере </remarks>
-- SetExtOut07Off           = 113
EXEC [dbo].[AddCommandType]
 @id                 = 113,
 @code               = N'SetExtOut07Off',
 @description        = N'The command turns off Extra output 7 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 8: включить </summary>
-- <remarks> Команда включает Дополнительный выход 8 на трекере </remarks>
-- SetExtOut08On            = 114
EXEC [dbo].[AddCommandType]
 @id                 = 114,
 @code               = N'SetExtOut08On',
 @description        = N'The command turns on Extra output 8 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 8: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 8 на трекере </remarks>
-- SetExtOut08Off           = 115
EXEC [dbo].[AddCommandType]
 @id                 = 115,
 @code               = N'SetExtOut08Off',
 @description        = N'The command turns off Extra output 8 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 9: включить </summary>
-- <remarks> Команда включает Дополнительный выход 9 на трекере </remarks>
-- SetExtOut09On            = 116
EXEC [dbo].[AddCommandType]
 @id                 = 116,
 @code               = N'SetExtOut09On',
 @description        = N'The command turns on Extra output 9 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 9: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 9 на трекере </remarks>
-- SetExtOut09Off           = 117
EXEC [dbo].[AddCommandType]
 @id                 = 117,
 @code               = N'SetExtOut09Off',
 @description        = N'The command turns off Extra output 9 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 10: включить </summary>
-- <remarks> Команда включает Дополнительный выход 10 на трекере </remarks>
-- SetExtOut10On            = 118
EXEC [dbo].[AddCommandType]
 @id                 = 118,
 @code               = N'SetExtOut10On',
 @description        = N'The command turns on Extra output 10 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 10: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 10 на трекере </remarks>
-- SetExtOut10Off           = 119
EXEC [dbo].[AddCommandType]
 @id                 = 119,
 @code               = N'SetExtOut10Off',
 @description        = N'The command turns off Extra output 10 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 11: включить </summary>
-- <remarks> Команда включает Дополнительный выход 11 на трекере </remarks>
-- SetExtOut11On            = 120
EXEC [dbo].[AddCommandType]
 @id                 = 120,
 @code               = N'SetExtOut11On',
 @description        = N'The command turns on Extra output 11 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 11: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 11 на трекере </remarks>
-- SetExtOut11Off           = 121
EXEC [dbo].[AddCommandType]
 @id                 = 121,
 @code               = N'SetExtOut11Off',
 @description        = N'The command turns off Extra output 11 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 12: включить </summary>
-- <remarks> Команда включает Дополнительный выход 12 на трекере </remarks>
-- SetExtOut12On            = 122
EXEC [dbo].[AddCommandType]
 @id                 = 122,
 @code               = N'SetExtOut12On',
 @description        = N'The command turns on Extra output 12 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 12: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 12 на трекере </remarks>
-- SetExtOut12Off           = 123
EXEC [dbo].[AddCommandType]
 @id                 = 123,
 @code               = N'SetExtOut12Off',
 @description        = N'The command turns off Extra output 12 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 13: включить </summary>
-- <remarks> Команда включает Дополнительный выход 13 на трекере </remarks>
-- SetExtOut13On            = 124
EXEC [dbo].[AddCommandType]
 @id                 = 124,
 @code               = N'SetExtOut13On',
 @description        = N'The command turns on Extra output 13 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 13: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 13 на трекере </remarks>
-- SetExtOut13Off           = 125
EXEC [dbo].[AddCommandType]
 @id                 = 125,
 @code               = N'SetExtOut13Off',
 @description        = N'The command turns off Extra output 13 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 14: включить </summary>
-- <remarks> Команда включает Дополнительный выход 14 на трекере </remarks>
-- SetExtOut14On            = 126
EXEC [dbo].[AddCommandType]
 @id                 = 126,
 @code               = N'SetExtOut14On',
 @description        = N'The command turns on Extra output 14 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 14: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 14 на трекере </remarks>
-- SetExtOut14Off           = 127
EXEC [dbo].[AddCommandType]
 @id                 = 127,
 @code               = N'SetExtOut14Off',
 @description        = N'The command turns off Extra output 14 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 15: включить </summary>
-- <remarks> Команда включает Дополнительный выход 15 на трекере </remarks>
-- SetExtOut15On            = 128
EXEC [dbo].[AddCommandType]
 @id                 = 128,
 @code               = N'SetExtOut15On',
 @description        = N'The command turns on Extra output 15 on the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO
-- <summary> Дополнительный выход 15: выключить </summary>
-- <remarks> Команда выключает Дополнительный выход 15 на трекере </remarks>
-- SetExtOut15Off           = 129
EXEC [dbo].[AddCommandType]
 @id                 = 129,
 @code               = N'SetExtOut15Off',
 @description        = N'The command turns off Extra output 15 off the tracker',
 @executionTimeout   = 60,
 @right_name         = N'SecurityAdministration',
 @minIntervalSeconds = NULL
GO