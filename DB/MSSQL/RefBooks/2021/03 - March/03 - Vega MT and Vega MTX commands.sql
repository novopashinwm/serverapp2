﻿-- Заполняем типы объектов
DECLARE @TYPE_NAMES dbo.Msisdn
INSERT INTO @TYPE_NAMES
OUTPUT INSERTED.*
SELECT
	[value]
FROM
(
	VALUES
		 (N'Вега МТ-21')
		,(N'Вега МТ-22')
		,(N'Вега МТ-23')
		,(N'Вега МТ-24')
		,(N'Вега МТ-25')
		,(N'Вега MT X Int')
		,(N'Вега MT X Ext')
		,(N'Вега MT X LTE')
) T([value])
;
DECLARE @CONTROLLER_TYPE_NAME nvarchar(50)
DECLARE TYPE_NAMES CURSOR LOCAL
READ_ONLY
FOR
	SELECT
		[value]
	FROM @TYPE_NAMES
OPEN TYPE_NAMES
FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	IF (@@FETCH_STATUS <> -2)
	BEGIN
		--/////////////////////////////////////////////////
		UPDATE [dbo].[CONTROLLER_TYPE]
			SET [SupportsPassword] = 1
		WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME
		--/////////////////////////////////////////////////
		BEGIN TRY
			BEGIN TRAN
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			-- Удалить команды у контроллера
			--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			-- Добавить команды к контроллеру
			--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'BlackBoxClear'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'RunCanScript01'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'RunCanScript03'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'RunCanScript02'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetOut01On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetOut01Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetOut02On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetOut02Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut01On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut01Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut02On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut02Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut03On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut03Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut04On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut04Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut05On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut05Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut06On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut06Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut07On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut07Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut08On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut08Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut09On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut09Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut10On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut10Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut11On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut11Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut12On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut12Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut13On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut13Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut14On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut14Off'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut15On'
			EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetExtOut15Off'
			------------------------------------------------------------------
			SELECT
				CT.*, TT.*
			FROM [dbo].[CONTROLLER_TYPE] CT
				INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
					ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
			WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
			------------------------------------------------------------------
			COMMIT TRAN
			--ROLLBACK TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT
				ERROR_NUMBER()    AS ErrorNumber,
				ERROR_SEVERITY()  AS ErrorSeverity,
				ERROR_STATE()     AS ErrorState,
				ERROR_PROCEDURE() AS ErrorProcedure,
				ERROR_LINE()      AS ErrorLine,
				ERROR_MESSAGE()   AS ErrorMessage;
		END CATCH
		--/////////////////////////////////////////////////
	END
	FETCH NEXT FROM TYPE_NAMES INTO @CONTROLLER_TYPE_NAME
END
GO