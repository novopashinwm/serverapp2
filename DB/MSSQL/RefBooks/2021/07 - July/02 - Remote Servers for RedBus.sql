BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[RemoteTerminalServer] AS DST
	USING
	(
		VALUES
			 (N'RedBus for RedBus', N'redbus://fleetdata.yourbus.in/api/gpsdata?scheme=http&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRla29uaWthIiwidmVuZG9ybmFtZSI6IlRFS09OSUtBIiwiaWF0IjoxNjI1NDY1MzIzfQ._vR-8w6zOXtoavmThEiyslfLk2_3s0-57YYrBCz6WA0')
	) AS SRC ([Name], [Url])
		ON  DST.[Name] = SRC.[Name] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[Url] = SRC.[Url]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Name], [Url]) VALUES (SRC.[Name], SRC.[Url])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH