﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME   nvarchar(50)  = NULL
	DECLARE @DeviceIdIsImei         bit           = NULL
	DECLARE @SupportsPassword       bit           = NULL
	DECLARE @AllowedToAddByCustomer bit           = NULL
	DECLARE @ImagePath              nvarchar(250) = NULL
	--------------------------------------------------------------------
	DECLARE CONTROLLER_TYPES CURSOR
	FORWARD_ONLY READ_ONLY STATIC LOCAL
	FOR
		SELECT DISTINCT
			[CONTROLLER_TYPE_NAME], [DeviceIdIsImei], [SupportsPassword], [AllowedToAddByCustomer], [ImagePath]
		FROM
		(
			VALUES
				 (N'Movireg BPX4-EHO', 0, 0, 1, N'~/img/devices/Movireg/BPX4-EHO.jpg')
		) T([CONTROLLER_TYPE_NAME], [DeviceIdIsImei], [SupportsPassword], [AllowedToAddByCustomer], [ImagePath])
	OPEN CONTROLLER_TYPES;
	FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME, @DeviceIdIsImei, @SupportsPassword, @AllowedToAddByCustomer, @ImagePath;
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		------------------------------------------------------------------
		-- Controller
		------------------------------------------------------------------
		EXEC [dbo].[AddControllerType]
			@type_name              = @CONTROLLER_TYPE_NAME,
			@DeviceIdIsImei         = @DeviceIdIsImei,
			@SupportsPassword       = @SupportsPassword,
			@AllowedToAddByCustomer = @AllowedToAddByCustomer,
			@ImagePath              = @ImagePath
		------------------------------------------------------------------
		FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME, @DeviceIdIsImei, @SupportsPassword, @AllowedToAddByCustomer, @ImagePath;
	END
	CLOSE      CONTROLLER_TYPES;
	DEALLOCATE CONTROLLER_TYPES;
	------------------------------------------------------------------
	-- Sort controllers
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		SELECT
			[TYPE_NAME],
			[SortOrder] =
			CASE [TYPE_NAME]
				WHEN N'Unspecified' THEN 1
				WHEN N'SoftTracker' THEN 2
				ELSE ROW_NUMBER() OVER (ORDER BY [TYPE_NAME]) + 2
			END
		FROM [dbo].[CONTROLLER_TYPE]
	) AS SRC ([TYPE_NAME], [SortOrder])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[SortOrder] = SRC.[SortOrder]
	OUTPUT $action, INSERTED.[TYPE_NAME], INSERTED.[SortOrder], DELETED.[SortOrder]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO