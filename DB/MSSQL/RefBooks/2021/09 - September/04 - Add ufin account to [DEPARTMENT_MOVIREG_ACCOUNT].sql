﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @dep_id INT
	SELECT @dep_id = [DEPARTMENT_ID]
	FROM [dbo].[DEPARTMENT] with(nolock)
	where [NAME] = 'TestCompany'
	------------------------------------------------------------------
	MERGE [dbo].[DEPARTMENT_MOVIREG_ACCOUNT] AS DST
	USING
	(
		VALUES
			 (N'http://movireg.online', 'ufin', '000000', @dep_id, 180)
	) AS SRC ([MOVIREG_SERVER_URL], [MOVIREG_ACCOUNT], [MOVIREG_PASSWORD], [DEPARTMENT_ID], [MOVIREG_SERVER_TIMEOFFSET])
		ON  DST.[MOVIREG_ACCOUNT] = SRC.[MOVIREG_ACCOUNT] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[MOVIREG_SERVER_URL] = SRC.[MOVIREG_SERVER_URL],
				DST.[MOVIREG_PASSWORD] = SRC.[MOVIREG_PASSWORD],
				DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID],
				DST.[MOVIREG_SERVER_TIMEOFFSET] = SRC.[MOVIREG_SERVER_TIMEOFFSET]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([MOVIREG_SERVER_URL], [MOVIREG_ACCOUNT], [MOVIREG_PASSWORD], [DEPARTMENT_ID], [MOVIREG_SERVER_TIMEOFFSET]) 
			VALUES (SRC.[MOVIREG_SERVER_URL], SRC.[MOVIREG_ACCOUNT], SRC.[MOVIREG_PASSWORD], SRC.[DEPARTMENT_ID], SRC.[MOVIREG_SERVER_TIMEOFFSET])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH