﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @MoviregCode sysname = N'Movireg';
	------------------------------------------------------------------
	-- Синхронизация [dbo].[MEDIA_TYPE]
	MERGE [dbo].[MEDIA_TYPE] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			 (1, N'GPRS')
			,(2, N'GSM')
			,(3, N'SMS')
			,(4, N'LBS')
			,(5, N'RemoteTerminalServer')
			,(6, N'RemoteVideoServer')
			,(7, N'Web')
			,(8, N'TCP')
			,(9, N'Emulator')
			,(10, @MoviregCode)
	) AS SRC ([Id], [Name])
		ON DST.[Id] = SRC.[Id]
	WHEN MATCHED AND DST.[Name] <> SRC.[Name] THEN
		UPDATE
			SET
				DST.[Name] = SRC.[Name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Id], [Name]) VALUES (SRC.[Id], SRC.[Name])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	DECLARE @MoviregMediaTypeId int = (SELECT TOP(1) [ID] FROM  [dbo].[MEDIA_TYPE] WHERE [Name] = @MoviregCode)
	------------------------------------------------------------------
	-- Обновление [dbo].[MEDIA]
	MERGE [dbo].[MEDIA] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			(@MoviregCode, @MoviregMediaTypeId)
	) AS SRC ([NAME], [TYPE])
		ON DST.[NAME] = SRC.[NAME]
	WHEN MATCHED AND DST.[TYPE] <> SRC.[TYPE] THEN
		UPDATE
			SET
				DST.[TYPE] = SRC.[TYPE]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([NAME], [TYPE]) VALUES (SRC.[NAME], SRC.[TYPE])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	DECLARE @MoviregMediaId int = (SELECT TOP(1) [MEDIA_ID] FROM  [dbo].[MEDIA] WHERE [NAME] = @MoviregCode)
	------------------------------------------------------------------
	-- Обновление [dbo].[MEDIA_ACCEPTORS]
	MERGE [dbo].[MEDIA_ACCEPTORS] WITH (HOLDLOCK) AS DST
	USING
	(
		VALUES
			(@MoviregMediaId, @MoviregCode, N'', N'')
	) AS SRC ([MEDIA_ID], [NAME], [DESC], [INIT])
		ON DST.[Name] = SRC.[NAME]
	WHEN MATCHED AND DST.[MEDIA_ID] <> SRC.[MEDIA_ID] THEN
		UPDATE
			SET
				 DST.[MEDIA_ID]       = SRC.[MEDIA_ID]
				,DST.[DESCRIPTION]    = SRC.[DESC]
				,DST.[INITIALIZATION] = SRC.[INIT]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([MEDIA_ID], [Name], [DESCRIPTION], [INITIALIZATION]) VALUES (SRC.[MEDIA_ID], SRC.[NAME], SRC.[DESC], SRC.[INIT])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH