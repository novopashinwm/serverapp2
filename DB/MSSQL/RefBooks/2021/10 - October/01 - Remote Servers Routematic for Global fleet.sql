BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[RemoteTerminalServer] AS DST
	USING
	(
		VALUES
			 (N'Routematic for Global fleet', N'routematic://vru.verayu.io/lib/process_location_update_tp.php?scheme=http&authkey=e9b5b88f-02b0-5d8f-9f22-4ce2b005ec2a&zoneid=4007&vendor=Tekonika')
	) AS SRC ([Name], [Url])
		ON  DST.[Name] = SRC.[Name] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[Url] = SRC.[Url]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Name], [Url]) VALUES (SRC.[Name], SRC.[Url])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH