﻿DECLARE @template_name nvarchar(50) = N'EventViolationOfDriving'
BEGIN TRY
	BEGIN TRAN
	---------------------------------------------------------------------------------------------------
	--SELECT TOP (1) [Culture_ID] FROM [dbo].[Culture] WHERE [Code] = N'en-US'
	--SELECT TOP (1) [Culture_ID] FROM [dbo].[Culture] WHERE [Code] = N'ru-RU'
	---------------------------------------------------------------------------------------------------
	-- Шаблон сообщения
	MERGE [dbo].[MESSAGE_TEMPLATE] AS DST
	USING
	(
		VALUES
			 (@template_name, N'Violation: $$DriverName$$', N'Violation: $$DeviceId$$,$$DriverName$$,$$EventTimeUtc$$,$$EventType$$,$$EventVideoUrl$$', (SELECT TOP (1) [Culture_ID] FROM [dbo].[Culture] WHERE [Code] = N'en-US'))
			,(@template_name, N'Нарушение: $$DriverName$$', N'Нарушение: $$DeviceId$$,$$DriverName$$,$$EventTimeUtc$$,$$EventType$$,$$EventVideoUrl$$', (SELECT TOP (1) [Culture_ID] FROM [dbo].[Culture] WHERE [Code] = N'ru-RU'))
	) AS SRC ([NAME], [HEADER], [BODY], [Culture_ID])
		ON  DST.[NAME]       = SRC.[NAME] COLLATE Cyrillic_General_CI_AS
		AND DST.[Culture_ID] = SRC.[Culture_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[HEADER]     = SRC.[HEADER]
				,DST.[BODY]       = SRC.[BODY]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([NAME], [HEADER], [BODY], [Culture_ID]) VALUES (SRC.[NAME], SRC.[HEADER], SRC.[BODY], SRC.[Culture_ID])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	MERGE [dbo].[MESSAGE_TEMPLATE_FIELD] AS DST
	USING
	(
		SELECT
			T2.[MESSAGE_TEMPLATE_ID], T2.[DOTNET_TYPE_ID], T1.[FIELD_NAME]
		FROM
		(
			VALUES
				 (@template_name, N'DeviceId',      N'System.String')
				,(@template_name, N'DriverId',      N'System.String')
				,(@template_name, N'DriverName',    N'System.String')
				,(@template_name, N'EventTimeUtc',  N'System.DateTime')
				,(@template_name, N'EventType',     N'System.String')
				,(@template_name, N'EventVideoUrl', N'System.String')
				,(@template_name, N'VehicleID',     N'System.Int32') -- Обязательно 'VehicleID', иначе процедура dbo.GetMessagesByOperatorId перестает работать в истории
				,(@template_name, N'VehicleName',   N'System.String')
		) T1 ([TEMPLATE_NAME], [FIELD_NAME], [TYPE_NAME])
			CROSS APPLY
			(
				SELECT
					 [MESSAGE_TEMPLATE_ID] = (SELECT TOP(1) [MESSAGE_TEMPLATE_ID] FROM [dbo].[MESSAGE_TEMPLATE] WHERE [NAME]      = T1.[TEMPLATE_NAME])
					,[DOTNET_TYPE_ID]      = (SELECT TOP(1) [DOTNET_TYPE_ID]      FROM [dbo].[DOTNET_TYPE]      WHERE [TYPE_NAME] = T1.[TYPE_NAME])
			) T2
	) AS SRC ([MESSAGE_TEMPLATE_ID], [DOTNET_TYPE_ID], [NAME])
		ON  DST.[MESSAGE_TEMPLATE_ID] = SRC.[MESSAGE_TEMPLATE_ID]
		AND DST.[NAME]                = SRC.[NAME] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[DOTNET_TYPE_ID] = SRC.[DOTNET_TYPE_ID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([MESSAGE_TEMPLATE_ID], [DOTNET_TYPE_ID], [NAME]) VALUES (SRC.[MESSAGE_TEMPLATE_ID], SRC.[DOTNET_TYPE_ID], SRC.[NAME])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH