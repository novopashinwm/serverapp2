BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[RemoteTerminalServer] AS DST
	USING
	(
		VALUES
			 (N'MoveInSync for Global Fleet', N'moveinsync://tracking.moveinsync.com:8080/gps-tracking/devices/Tekonika/packets?scheme=http')
	) AS SRC ([Name], [Url])
		ON  DST.[Url] = SRC.[Url] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[Name] = SRC.[Name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Name], [Url]) VALUES (SRC.[Name], SRC.[Url])
	OUTPUT $action, INSERTED.*, DELETED.*;

	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH