BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME      nvarchar(50)  = N'Smart RP01'
	DECLARE @CONTROLLER_TYPE_COPY      nvarchar(50)  = N'Smart GT06N'
	DECLARE @LEGEND                    nvarchar(50)  = NULL
	DECLARE @NUMBER                    int           = NULL
	DECLARE @DESCRIPTION               nvarchar(512) = NULL
	DECLARE @CONTROLLER_SENSOR_digital bit           = NULL
	DECLARE @MANDATORY                 bit           = 0
	DECLARE @DEFAULTMULTIPLIER         real          = 1.0
	DECLARE @DEFAULTCONSTANT           real          = 0.0
	DECLARE @CONTROLLER_SENSOR_TYPE_ID int           = NULL
	DECLARE @DEFAULT_VALUE             bigint        = NULL
	DECLARE @MIN_VALUE                 bigint        = NULL
	DECLARE @MAX_VALUE                 bigint        = NULL
	DECLARE @BITS                      int           = NULL
	--------------------------------------------------------------------
	--Добавляем контроллер
	EXEC [dbo].[AddControllerType]
		@type_name              = @CONTROLLER_TYPE_NAME,
		@DeviceIdIsImei         = 0,
		@SupportsPassword       = 1,
		@AllowedToAddByCustomer = 1
	--------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_ID int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME);
	------------------------------------------------------------------
	SELECT
		CS.DESCRIPT, CS.NUMBER, CS.*
	FROM [dbo].[CONTROLLER_SENSOR] CS
		JOIN [dbo].[CONTROLLER_TYPE] CT
			ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_SENSOR] AS DST
	USING
	(
		SELECT
			 [CONTROLLER_TYPE_ID]        = @CONTROLLER_TYPE_ID
			,[CONTROLLER_SENSOR_TYPE_ID] = CS.[CONTROLLER_SENSOR_TYPE_ID]
			,[NUMBER]                    = CS.[NUMBER]
			,[MAX_VALUE]                 = CS.[MAX_VALUE]
			,[MIN_VALUE]                 = CS.[MIN_VALUE]
			,[DEFAULT_VALUE]             = CS.[DEFAULT_VALUE]
			,[BITS]                      = CS.[BITS]
			,[DESCRIPT]                  = CS.[DESCRIPT]
			,[VALUE_EXPIRED]             = CS.[VALUE_EXPIRED]
			,[Default_Sensor_Legend_ID]  = CS.[Default_Sensor_Legend_ID]
			,[Default_Multiplier]        = CS.[Default_Multiplier]
			,[Default_Constant]          = CS.[Default_Constant]
			,[Mandatory]                 = CS.[Mandatory]
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		-- Копируем датчики из @CONTROLLER_TYPE_COPY
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_COPY
	) AS SRC
		ON  DST.[CONTROLLER_TYPE_ID] = SRC.[CONTROLLER_TYPE_ID]
		AND DST.[NUMBER]             = SRC.[NUMBER]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[CONTROLLER_TYPE_ID]        = SRC.[CONTROLLER_TYPE_ID]
				,DST.[CONTROLLER_SENSOR_TYPE_ID] = SRC.[CONTROLLER_SENSOR_TYPE_ID]
				,DST.[NUMBER]                    = SRC.[NUMBER]
				,DST.[MAX_VALUE]                 = SRC.[MAX_VALUE]
				,DST.[MIN_VALUE]                 = SRC.[MIN_VALUE]
				,DST.[DEFAULT_VALUE]             = SRC.[DEFAULT_VALUE]
				,DST.[BITS]                      = SRC.[BITS]
				,DST.[DESCRIPT]                  = SRC.[DESCRIPT]
				,DST.[VALUE_EXPIRED]             = SRC.[VALUE_EXPIRED]
				,DST.[Default_Sensor_Legend_ID]  = SRC.[Default_Sensor_Legend_ID]
				,DST.[Default_Multiplier]        = SRC.[Default_Multiplier]
				,DST.[Default_Constant]          = SRC.[Default_Constant]
				,DST.[Mandatory]                 = SRC.[Mandatory]
	WHEN NOT MATCHED BY SOURCE AND DST.[CONTROLLER_TYPE_ID] = @CONTROLLER_TYPE_ID THEN
		DELETE
	WHEN NOT MATCHED BY TARGET THEN
		INSERT
		(
			 [CONTROLLER_TYPE_ID]
			,[CONTROLLER_SENSOR_TYPE_ID]
			,[NUMBER]
			,[MAX_VALUE]
			,[MIN_VALUE]
			,[DEFAULT_VALUE]
			,[BITS]
			,[DESCRIPT]
			,[VALUE_EXPIRED]
			,[Default_Sensor_Legend_ID]
			,[Default_Multiplier]
			,[Default_Constant]
			,[Mandatory]
		)
		VALUES
		(
			 SRC.[CONTROLLER_TYPE_ID]
			,SRC.[CONTROLLER_SENSOR_TYPE_ID]
			,SRC.[NUMBER]
			,SRC.[MAX_VALUE]
			,SRC.[MIN_VALUE]
			,SRC.[DEFAULT_VALUE]
			,SRC.[BITS]
			,SRC.[DESCRIPT]
			,SRC.[VALUE_EXPIRED]
			,SRC.[Default_Sensor_Legend_ID]
			,SRC.[Default_Multiplier]
			,SRC.[Default_Constant]
			,SRC.[Mandatory]
		)
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	SELECT
		CS.DESCRIPT, CS.NUMBER, CS.*
	FROM [dbo].[CONTROLLER_SENSOR] CS
		JOIN [dbo].[CONTROLLER_TYPE] CT
			ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO