﻿BEGIN TRY
	DECLARE @CONTROLLER_TYPE_NAME nvarchar(50) = N'Smart GT06N'
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	-- Добавить команду 'CutOffFuel' к контроллеру
	EXEC AddControllerTypeCommand @CONTROLLER_TYPE_NAME, N'CutOffFuel'
	------------------------------------------------------------------
	-- Добавить команду 'ReopenFuel' к контроллеру
	EXEC AddControllerTypeCommand @CONTROLLER_TYPE_NAME, N'ReopenFuel'
	------------------------------------------------------------------
	SELECT
		CT.*, TT.*
	FROM [dbo].[CONTROLLER_TYPE] CT
		INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
			ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO