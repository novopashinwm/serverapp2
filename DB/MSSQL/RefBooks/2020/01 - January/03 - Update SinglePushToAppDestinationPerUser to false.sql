BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[CONSTANTS] AS DST
	USING
	(
		VALUES
			 (N'SinglePushToAppDestinationPerUser', N'false')
	) AS SRC ([NAME], [VALUE])
		ON DST.[NAME] = SRC.[NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[VALUE] = SRC.[VALUE]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [NAME],     [VALUE])
		VALUES (SRC.[NAME], SRC.[VALUE])
	OUTPUT $action, INSERTED.[NAME], INSERTED.[VALUE] AS [NEW], DELETED.[VALUE] AS [OLD]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO