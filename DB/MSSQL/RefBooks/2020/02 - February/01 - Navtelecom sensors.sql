BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME      nvarchar(50)  = NULL
	DECLARE @LEGEND                    nvarchar(50)  = NULL
	DECLARE @NUMBER                    int           = NULL
	DECLARE @DESCRIPTION               nvarchar(512) = NULL
	DECLARE @CONTROLLER_SENSOR_digital bit           = NULL
	DECLARE @MANDATORY                 bit           = 0
	DECLARE @DEFAULTMULTIPLIER         real          = 1.0
	DECLARE @DEFAULTCONSTANT           real          = 0.0
	DECLARE @CONTROLLER_SENSOR_TYPE_ID int           = NULL
	DECLARE @DEFAULT_VALUE             bigint        = NULL
	DECLARE @MIN_VALUE                 bigint        = NULL
	DECLARE @MAX_VALUE                 bigint        = NULL
	DECLARE @BITS                      int           = NULL
	--------------------------------------------------------------------
	DECLARE CONTROLLER_TYPES CURSOR
	FORWARD_ONLY READ_ONLY STATIC LOCAL
	FOR
		SELECT DISTINCT
			[CONTROLLER_TYPE_NAME]
		FROM
		(
			VALUES
				 (N'Navtelecom SIGNAL S-2551')
				,(N'Navtelecom SMART S-2333A')
				,(N'Navtelecom SMART S-2430')
				,(N'Navtelecom SMART S-2435')
		) T([CONTROLLER_TYPE_NAME])
	OPEN CONTROLLER_TYPES;
	FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME;
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		--------------------------------------------------------------------
		--DELETE [dbo].[CONTROLLER_SENSOR]
		--	OUTPUT DELETED.*
		--FROM [dbo].[CONTROLLER_SENSOR] CS
		--	JOIN [dbo].[CONTROLLER_TYPE] CT
		--		ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		--WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		SELECT
			CS.DESCRIPT, CS.NUMBER, CS.*
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		--------------------------------------------------------------------
		DECLARE SENSORS CURSOR
		FORWARD_ONLY READ_ONLY STATIC LOCAL
		FOR
			SELECT
				 [NUMBER]            = CAST([NUMBER]            AS int)           -- COL01 - Номер в протоколе
				,[DESCRIPTION]       = CAST([DESCRIPTION]       AS nvarchar(512)) -- COL02 - Описание датчика
				,[LEGEND]            = CAST([LEGEND]            AS nvarchar(50))  -- COL03 - Легенда
				,[DIGITAL]           = CAST([DIGITAL]           AS bit)           -- COL04 - Цифровой? (Да - 1, Нет - 0)
				,[MANDATORY]         = CAST([MANDATORY]         AS bit)           -- COL05 - Обязательный? (Да - 1, Нет - 0) создается сразу для устройства этого типа
				,[DEFAULTMULTIPLIER] = CAST([DEFAULTMULTIPLIER] AS real)          -- COL06 - Множитель по умолчанию для вычисленного значения
				,[DEFAULTCONSTANT]   = CAST([DEFAULTCONSTANT]   AS real)          -- COL07 - Константа по умолчанию для вычисленного значения
				,[DEFAULT_VALUE]     = CAST([DEFAULT_VALUE]     AS bigint)        -- COL08 - Значение по умолчанию
				,[MIN_VALUE]         = CAST([MIN_VALUE]         AS bigint)        -- COL09 - Минимальное значение
				,[MAX_VALUE]         = CAST([MAX_VALUE]         AS bigint)        -- COL10 - Максимальное значение
				,[BITS]              = CAST([BITS]              AS int)           -- COL11 - Размер данных в битах
			FROM
			(
				VALUES
				-- COL01  | COL02                                        | COL03                         | COL04  | COL05  | COL06      | COL07  | COL08  | COL09    | COL10                    | COL11
				 (0001, /*|*/ N'Alarm',                                /*|*/ N'Alarm',                 /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0002, /*|*/ N'Engine',                               /*|*/ N'Ignition',              /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0003, /*|*/ N'Navigation',                           /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0004, /*|*/ N'Gsm',                                  /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0005, /*|*/ N'MainAccVoltage',                       /*|*/ N'BatteryLevel',          /*|*/ 0, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0006, /*|*/ N'SecondAccVoltage',                     /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0007, /*|*/ N'MotoHours',                            /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(0008, /*|*/ N'EventIndex',                           /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(0010, /*|*/ N'DigitalInput1',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0011, /*|*/ N'DigitalInput2',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0012, /*|*/ N'DigitalInput3',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0013, /*|*/ N'DigitalInput4',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0014, /*|*/ N'DigitalInput5',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0015, /*|*/ N'DigitalInput6',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0016, /*|*/ N'DigitalInput7',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0017, /*|*/ N'DigitalInput8',                        /*|*/ NULL,                     /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(0021, /*|*/ N'Ain1Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0022, /*|*/ N'Ain2Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0023, /*|*/ N'Ain3Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0024, /*|*/ N'Ain4Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0025, /*|*/ N'Ain5Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0026, /*|*/ N'Ain6Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0027, /*|*/ N'Ain7Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0028, /*|*/ N'Ain8Voltage',                          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(0035, /*|*/ N'FuelSensorFreq1',                      /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               20000, /*|*/ 16)
				,(0036, /*|*/ N'FuelSensorFreq2',                      /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               20000, /*|*/ 16)
				,(0038, /*|*/ N'FuelSensor1Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0039, /*|*/ N'FuelSensor2Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0040, /*|*/ N'FuelSensor3Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0041, /*|*/ N'FuelSensor4Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0042, /*|*/ N'FuelSensor5Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0043, /*|*/ N'FuelSensor6Level_RS485',               /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0044, /*|*/ N'FuelSensorLevel_RS232',                /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65499, /*|*/ 16)
				,(0045, /*|*/ N'TemperatureSensor1',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0046, /*|*/ N'TemperatureSensor2',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0047, /*|*/ N'TemperatureSensor3',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0048, /*|*/ N'TemperatureSensor4',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0049, /*|*/ N'TemperatureSensor5',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0050, /*|*/ N'TemperatureSensor6',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0051, /*|*/ N'TemperatureSensor7',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0052, /*|*/ N'TemperatureSensor8',                   /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/  -55, /*|*/                 125, /*|*/  8)
				,(0053, /*|*/ N'CanFuelLevel',                         /*|*/ N'CAN_FuelLevel',         /*|*/ 0, /*|*/ 1, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32766, /*|*/ 16)
				,(0054, /*|*/ N'CanFuelConsumptionTotal',              /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/ 9223372036854775807, /*|*/ 64)
				,(0055, /*|*/ N'CanEngineRpm',                         /*|*/ N'CAN_Revs',              /*|*/ 0, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0056, /*|*/ N'CanEngineCoolantTemperature',          /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -127, /*|*/                 127, /*|*/  8)
				,(0057, /*|*/ N'CanMileageTotal',                      /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/ 9223372036854775807, /*|*/ 64)
				,(0058, /*|*/ N'CanAxleLoad1',                         /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0059, /*|*/ N'CanAxleLoad2',                         /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0060, /*|*/ N'CanAxleLoad3',                         /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0061, /*|*/ N'CanAxleLoad4',                         /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0062, /*|*/ N'CanAxleLoad5',                         /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65534, /*|*/ 16)
				,(0063, /*|*/ N'CanGasPedalPosition',                  /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(0064, /*|*/ N'CanBrakePedalPosition',                /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(0065, /*|*/ N'CanEngineLoad',                        /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(0066, /*|*/ N'CanLiquidLevelDieselExhaustFilter',    /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32766, /*|*/ 16)
				,(0067, /*|*/ N'CanEngineRuntimeTotal',                /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(0068, /*|*/ N'CanDistanceBeforeTechnicalInspection', /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 5.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32767, /*|*/ 16)
				,(0069, /*|*/ N'CanSpeed',                             /*|*/ NULL,                     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 254, /*|*/  8)
				,(1020, /*|*/ N'SH1_WeakBlow',                         /*|*/ N'WeakBlow',              /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(1021, /*|*/ N'SH2_StrongBlow',                       /*|*/ N'StrongBlow',            /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(1022, /*|*/ N'SH3_Move',                             /*|*/ N'Motion',                /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(1023, /*|*/ N'SH4_Inclination',                      /*|*/ N'Inclination',           /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				-- Счетчик пассажиропотока 1
				,(11001, /*|*/ N'Passengers1Input',                    /*|*/ N'Door1Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11002, /*|*/ N'Passengers1Output',                   /*|*/ N'Door1Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11003, /*|*/ N'Passengers1Error',                    /*|*/ N'Door1Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11004, /*|*/ N'Passengers1Sabotage',                 /*|*/ N'Door1Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11005, /*|*/ N'Passengers1InputToday',               /*|*/ N'Door1EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11006, /*|*/ N'Passengers1OutputToday',              /*|*/ N'Door1LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 2
				,(11101, /*|*/ N'Passengers2Input',                    /*|*/ N'Door2Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11102, /*|*/ N'Passengers2Output',                   /*|*/ N'Door2Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11103, /*|*/ N'Passengers2Error',                    /*|*/ N'Door2Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11104, /*|*/ N'Passengers2Sabotage',                 /*|*/ N'Door2Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11105, /*|*/ N'Passengers2InputToday',               /*|*/ N'Door2EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11106, /*|*/ N'Passengers2OutputToday',              /*|*/ N'Door2LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 3
				,(11201, /*|*/ N'Passengers3Input',                    /*|*/ N'Door3Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11202, /*|*/ N'Passengers3Output',                   /*|*/ N'Door3Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11203, /*|*/ N'Passengers3Error',                    /*|*/ N'Door3Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11204, /*|*/ N'Passengers3Sabotage',                 /*|*/ N'Door3Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11205, /*|*/ N'Passengers3InputToday',               /*|*/ N'Door3EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11206, /*|*/ N'Passengers3OutputToday',              /*|*/ N'Door3LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 4
				,(11301, /*|*/ N'Passengers4Input',                    /*|*/ N'Door4Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11302, /*|*/ N'Passengers4Output',                   /*|*/ N'Door4Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11303, /*|*/ N'Passengers4Error',                    /*|*/ N'Door4Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11304, /*|*/ N'Passengers4Sabotage',                 /*|*/ N'Door4Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11305, /*|*/ N'Passengers4InputToday',               /*|*/ N'Door4EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11306, /*|*/ N'Passengers4OutputToday',              /*|*/ N'Door4LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 5
				,(11401, /*|*/ N'Passengers5Input',                    /*|*/ N'Door5Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11402, /*|*/ N'Passengers5Output',                   /*|*/ N'Door5Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11403, /*|*/ N'Passengers5Error',                    /*|*/ N'Door5Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11404, /*|*/ N'Passengers5Sabotage',                 /*|*/ N'Door5Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11405, /*|*/ N'Passengers5InputToday',               /*|*/ N'Door5EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11406, /*|*/ N'Passengers5OutputToday',              /*|*/ N'Door5LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 6
				,(11501, /*|*/ N'Passengers6Input',                    /*|*/ N'Door6Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11502, /*|*/ N'Passengers6Output',                   /*|*/ N'Door6Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11503, /*|*/ N'Passengers6Error',                    /*|*/ N'Door6Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11504, /*|*/ N'Passengers6Sabotage',                 /*|*/ N'Door6Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11505, /*|*/ N'Passengers6InputToday',               /*|*/ N'Door6EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11506, /*|*/ N'Passengers6OutputToday',              /*|*/ N'Door6LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				-- Счетчик пассажиропотока 7
				,(11601, /*|*/ N'Passengers7Input',                    /*|*/ N'Door7Entered',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11602, /*|*/ N'Passengers7Output',                   /*|*/ N'Door7Leaved',           /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(11603, /*|*/ N'Passengers7Error',                    /*|*/ N'Door7Offline',          /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11604, /*|*/ N'Passengers7Sabotage',                 /*|*/ N'Door7Sabotage',         /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  1)
				,(11605, /*|*/ N'Passengers7InputToday',               /*|*/ N'Door7EnteredToday',     /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(11606, /*|*/ N'Passengers7OutputToday',              /*|*/ N'Door7LeavedToday',      /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
			) S([NUMBER], [DESCRIPTION], [LEGEND], [DIGITAL], [MANDATORY], [DEFAULTMULTIPLIER], [DEFAULTCONSTANT], [DEFAULT_VALUE], [MIN_VALUE], [MAX_VALUE], [BITS])
		OPEN SENSORS
		FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		WHILE (@@FETCH_STATUS <> -1)
		BEGIN
			IF (@@FETCH_STATUS <> -2)
			BEGIN
				--/////////////////////////////////////////////////
				EXEC [dbo].[AddOrUpdateControllerSensor] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_digital, @LEGEND, 0/*@MANDATORY*/, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT
				SELECT
					@CONTROLLER_SENSOR_TYPE_ID = CASE @CONTROLLER_SENSOR_digital WHEN 1 THEN 2 ELSE 1 END,
					@MAX_VALUE                 = COALESCE(@MAX_VALUE, POWER(CAST(2 AS bigint), COALESCE(@BITS, 1) - 1))
				EXEC [dbo].[SetControllerSensorNumber] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_TYPE_ID, @DEFAULT_VALUE, @MAX_VALUE, @MIN_VALUE, @BITS
				--/////////////////////////////////////////////////
				DECLARE @CONTROLLER_TYPE_ID int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME);
				UPDATE [dbo].[CONTROLLER_SENSOR]
					SET
						 [Default_Multiplier] = @DEFAULTMULTIPLIER
						,[Default_Constant]   = @DEFAULTCONSTANT
						,[Mandatory]          = @MANDATORY
				--OUTPUT DELETED.*, INSERTED.*
				WHERE [CONTROLLER_TYPE_ID] = @CONTROLLER_TYPE_ID
				AND   [NUMBER]             = @NUMBER
				--/////////////////////////////////////////////////
			END
			FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		END
		CLOSE      SENSORS;
		DEALLOCATE SENSORS;
		SELECT
			CS.DESCRIPT, CS.NUMBER, CS.*
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME;
	END
	CLOSE      CONTROLLER_TYPES;
	DEALLOCATE CONTROLLER_TYPES;
	------------------------------------------------------------------
	-- Sort controllers
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		SELECT
			[TYPE_NAME],
			[SortOrder] =
			CASE [TYPE_NAME]
				WHEN N'Unspecified' THEN 1
				WHEN N'SoftTracker' THEN 2
				ELSE ROW_NUMBER() OVER (ORDER BY [TYPE_NAME]) + 2
			END
		FROM [dbo].[CONTROLLER_TYPE]
	) AS SRC ([TYPE_NAME], [SortOrder])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[SortOrder] = SRC.[SortOrder]
	OUTPUT $action, INSERTED.[TYPE_NAME], INSERTED.[SortOrder], DELETED.[SortOrder]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO