BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME   nvarchar(50)  = NULL
	DECLARE @DeviceIdIsImei         bit           = NULL
	DECLARE @SupportsPassword       bit           = NULL
	DECLARE @AllowedToAddByCustomer bit           = NULL
	DECLARE @ImagePath              nvarchar(250) = NULL
	--------------------------------------------------------------------
	DECLARE CONTROLLER_TYPES CURSOR
	FORWARD_ONLY READ_ONLY STATIC LOCAL
	FOR
		SELECT DISTINCT
			[CONTROLLER_TYPE_NAME], [DeviceIdIsImei], [SupportsPassword], [AllowedToAddByCustomer], [ImagePath]
		FROM
		(
			VALUES
				 (N'Teltonika FMB125', 1, 1, 1, N'~/img/devices/Teltonika/TeltonikaFMB125.png')
		) T([CONTROLLER_TYPE_NAME], [DeviceIdIsImei], [SupportsPassword], [AllowedToAddByCustomer], [ImagePath])
	OPEN CONTROLLER_TYPES;
	FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME, @DeviceIdIsImei, @SupportsPassword, @AllowedToAddByCustomer, @ImagePath;
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		------------------------------------------------------------------
		SELECT
			CS.DESCRIPT, CS.NUMBER, CS.*
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		-- Controller
		------------------------------------------------------------------
		EXEC [dbo].[AddControllerType]
			@type_name              = @CONTROLLER_TYPE_NAME,
			@DeviceIdIsImei         = @DeviceIdIsImei,
			@SupportsPassword       = @SupportsPassword,
			@AllowedToAddByCustomer = @AllowedToAddByCustomer,
			@ImagePath              = @ImagePath
		------------------------------------------------------------------
		-- Sensors
		------------------------------------------------------------------
		DECLARE @LEGEND                    nvarchar(50)  = NULL
		DECLARE @NUMBER                    int           = NULL
		DECLARE @DESCRIPTION               nvarchar(512) = NULL
		DECLARE @CONTROLLER_SENSOR_digital bit           = NULL
		DECLARE @MANDATORY                 bit           = 0
		DECLARE @DEFAULTMULTIPLIER         real          = 1.0
		DECLARE @DEFAULTCONSTANT           real          = 0.0
		DECLARE @CONTROLLER_SENSOR_TYPE_ID int           = NULL
		DECLARE @DEFAULT_VALUE             bigint        = NULL
		DECLARE @MIN_VALUE                 bigint        = NULL
		DECLARE @MAX_VALUE                 bigint        = NULL
		DECLARE @BITS                      int           = NULL
		DECLARE SENSORS CURSOR
		FORWARD_ONLY READ_ONLY STATIC LOCAL
		FOR
			SELECT
				 [NUMBER]            = CAST([NUMBER]            AS int)           -- COL01 - Номер в протоколе
				,[DESCRIPTION]       = CAST([DESCRIPTION]       AS nvarchar(512)) -- COL02 - Описание датчика
				,[LEGEND]            = CAST([LEGEND]            AS nvarchar(50))  -- COL03 - Легенда
				,[DIGITAL]           = CAST([DIGITAL]           AS bit)           -- COL04 - Цифровой? (Да - 1, Нет - 0)
				,[MANDATORY]         = CAST([MANDATORY]         AS bit)           -- COL05 - Обязательный? (Да - 1, Нет - 0) создается сразу для устройства этого типа
				,[DEFAULTMULTIPLIER] = CAST([DEFAULTMULTIPLIER] AS real)          -- COL06 - Множитель по умолчанию для вычисленного значения
				,[DEFAULTCONSTANT]   = CAST([DEFAULTCONSTANT]   AS real)          -- COL07 - Константа по умолчанию для вычисленного значения
				,[DEFAULT_VALUE]     = CAST([DEFAULT_VALUE]     AS bigint)        -- COL08 - Значение по умолчанию
				,[MIN_VALUE]         = CAST([MIN_VALUE]         AS bigint)        -- COL09 - Минимальное значение
				,[MAX_VALUE]         = CAST([MAX_VALUE]         AS bigint)        -- COL10 - Максимальное значение
				,[BITS]              = CAST([BITS]              AS int)           -- COL11 - Размер данных в битах
			FROM
			(
				VALUES
					-- COL01   | COL02                                   | COL03                            | COL04  | COL05  | COL06      | COL07  | COL08  | COL09       | COL10           | COL11
					 (00001, /*|*/ N'Digital Input 1',                 /*|*/ N'DigitalSensor1',           /*|*/ 1, /*|*/ 1, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   8)
					,(00002, /*|*/ N'Digital Input 2',                 /*|*/ N'DigitalSensor2',           /*|*/ 1, /*|*/ 1, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   8)
					,(00009, /*|*/ N'Analog Input 1',                  /*|*/ N'AnalogSensor1',            /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00015, /*|*/ N'Eco Score',                       /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00021, /*|*/ N'GSM Signal',                      /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          5, /*|*/   8)
					,(00024, /*|*/ N'Speed',                           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        350, /*|*/  16)
					,(00030, /*|*/ N'OBD Number of DTC',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00031, /*|*/ N'OBD Engine Load',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00032, /*|*/ N'OBD Coolant Temperature',         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00033, /*|*/ N'OBD Short Fuel Trim',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -100, /*|*/         99, /*|*/   8)
					,(00034, /*|*/ N'OBD Fuel pressure',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        765, /*|*/  16)
					,(00035, /*|*/ N'OBD Intake MAP',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00036, /*|*/ N'OBD Engine RPM',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      16384, /*|*/  16)
					,(00037, /*|*/ N'OBD Vehicle Speed',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00038, /*|*/ N'OBD Timing Advance',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/    -64, /*|*/         64, /*|*/   8)
					,(00039, /*|*/ N'OBD Intake Air Temperature',      /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00040, /*|*/ N'OBD MAF',                         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00041, /*|*/ N'OBD Throttle Position',           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00042, /*|*/ N'OBD Runtime since engine start',  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00043, /*|*/ N'OBD Distance Traveled MIL On',    /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00044, /*|*/ N'OBD Relative Fuel Rail Pressure', /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/       5178, /*|*/  16)
					,(00045, /*|*/ N'OBD Direct Fuel Rail Pressure',   /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/    10, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00046, /*|*/ N'OBD Commanded EGR',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00047, /*|*/ N'OBD EGR Error',                   /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -100, /*|*/        100, /*|*/   8)
					,(00048, /*|*/ N'OBD Fuel Level',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00049, /*|*/ N'OBD Distance Since Codes Clear',  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00050, /*|*/ N'OBD Barometic Pressure',          /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00051, /*|*/ N'OBD Control Module Voltage',      /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00052, /*|*/ N'OBD Absolute Load Value',         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      25700, /*|*/  16)
					,(00053, /*|*/ N'OBD Ambient Air Temperature',     /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00054, /*|*/ N'OBD Time Run With MIL On',        /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00055, /*|*/ N'OBD Time Since Codes Cleared',    /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00056, /*|*/ N'OBD Absolute Fuel Rail Pressure', /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00057, /*|*/ N'OBD Hybrid battery pack life',    /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00058, /*|*/ N'OBD Engine Oil Temperature',      /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        215, /*|*/   8)
					,(00059, /*|*/ N'OBD Fuel injection timing',       /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/  0.01, /*|*/ 0, /*|*/ 0, /*|*/ -21000, /*|*/      30200, /*|*/  16)
					,(00060, /*|*/ N'OBD Fuel Rate',                   /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/  0.01, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32767, /*|*/  16)
					,(00066, /*|*/ N'External Voltage',                /*|*/ N'PowerVoltage',             /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00067, /*|*/ N'Battery Voltage',                 /*|*/ N'AccumulatorVoltage',       /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00068, /*|*/ N'BatteryCurrent',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00072, /*|*/ N'Dallas Temperature 1',            /*|*/ N'DallasTemperatureSensor1', /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -550, /*|*/       1150, /*|*/  32)
					,(00073, /*|*/ N'Dallas Temperature 2',            /*|*/ N'DallasTemperatureSensor2', /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -550, /*|*/       1150, /*|*/  32)
					,(00074, /*|*/ N'Dallas Temperature 3',            /*|*/ N'DallasTemperatureSensor3', /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -550, /*|*/       1150, /*|*/  32)
					,(00075, /*|*/ N'Dallas Temperature 4',            /*|*/ N'DallasTemperatureSensor4', /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -550, /*|*/       1150, /*|*/  32)
					,(00016, /*|*/ N'Total Odometer',                  /*|*/ N'VirtualOdometer',          /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 2147483647, /*|*/  32)
					,(00081, /*|*/ N'CAN Vehicle Speed',               /*|*/ N'CAN_Speed',                /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00082, /*|*/ N'CAN Accelerator Pedal Position',  /*|*/ N'CAN_Accelerator',          /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        102, /*|*/   8)
					,(00083, /*|*/ N'CAN Fuel Consumed',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 2147483647, /*|*/  32)
					,(00084, /*|*/ N'CAN Fuel level',                  /*|*/ N'CAN_FuelLevel1',           /*|*/ 0, /*|*/ 1, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00085, /*|*/ N'CAN Engine RPM',                  /*|*/ N'CAN_Revs',                 /*|*/ 0, /*|*/ 1, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      16384, /*|*/  16)
					,(00087, /*|*/ N'CAN Total Mileage',               /*|*/ N'CANTotalRun',              /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00089, /*|*/ N'CAN Fuel level',                  /*|*/ N'CANFuel',                  /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00090, /*|*/ N'CAN Door Status',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      16128, /*|*/  16)
					,(00102, /*|*/ N'CAN Engine Worktime',             /*|*/ N'CAN_DayRun',               /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/    1677215, /*|*/  32)
					,(00103, /*|*/ N'CAN Total engine work time',      /*|*/ N'CAN_EngHours',             /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/    1677215, /*|*/  32)
					,(00105, /*|*/ N'CAN Total Vehicle Mileage',       /*|*/ N'CAN_TotalRun',             /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00107, /*|*/ N'CAN Total Fuel Consumed',         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 2147483647, /*|*/  32)
					,(00110, /*|*/ N'CAN Fuel Rate',                   /*|*/ N'CAN_FuelRate',             /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00111, /*|*/ N'CAN AdBlue Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00112, /*|*/ N'CAN AdBlue Level Liters',         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00113, /*|*/ N'Battery Level',                   /*|*/ N'BatteryLevel',             /*|*/ 0, /*|*/ 1, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00114, /*|*/ N'CAN Engine Load',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        130, /*|*/   8)
					,(00115, /*|*/ N'CAN Engine Temperature',          /*|*/ N'CAN_CoolantT',             /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -600, /*|*/       1270, /*|*/  16)
					,(00118, /*|*/ N'CAN Axle 1 Load',                 /*|*/ N'CAN_AxleLoad1',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00119, /*|*/ N'CAN Axle 2 Load',                 /*|*/ N'CAN_AxleLoad2',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00120, /*|*/ N'CAN Axle 3 Load',                 /*|*/ N'CAN_AxleLoad3',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00121, /*|*/ N'CAN Axle 4 Load',                 /*|*/ N'CAN_AxleLoad4',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00122, /*|*/ N'CAN Axle 5 Load',                 /*|*/ N'CAN_AxleLoad5',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/  16)
					,(00123, /*|*/ N'Control State Flags',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00125, /*|*/ N'Harvesting Time',                 /*|*/ N'HarvestingTime',           /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/   16777215, /*|*/  32)
					,(00126, /*|*/ N'Area of Harvest',                 /*|*/ N'AreaofHarvest',            /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00127, /*|*/ N'Mowing Efficiency',               /*|*/ N'MowingEfficiency',         /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00128, /*|*/ N'Grain Mown Volume',               /*|*/ N'GrainMownVolume',          /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/ 4294967295, /*|*/  32)
					,(00129, /*|*/ N'Grain Moisture',                  /*|*/ N'GrainMoisture',            /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00130, /*|*/ N'Harvesting Drum RPM',             /*|*/ N'HarvestingDrumRPM',        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00131, /*|*/ N'Gap Under Harvesting Drum',       /*|*/ N'GapUnderHarvestingDrum',   /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        255, /*|*/   8)
					,(00025, /*|*/ N'BLE Temperature #1',              /*|*/ N'TemperatureSensor1',       /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -400, /*|*/      12050, /*|*/  16)
					,(00026, /*|*/ N'BLE Temperature #2',              /*|*/ N'TemperatureSensor2',       /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -400, /*|*/      12050, /*|*/  16)
					,(00027, /*|*/ N'BLE Temperature #3',              /*|*/ N'TemperatureSensor3',       /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -400, /*|*/      12050, /*|*/  16)
					,(00028, /*|*/ N'BLE Temperature #4',              /*|*/ N'TemperatureSensor4',       /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/   -400, /*|*/      12050, /*|*/  16)
					,(00029, /*|*/ N'BLE Battery #1',                  /*|*/ N'BatteryExternalSensor1',   /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00020, /*|*/ N'BLE Battery #2',                  /*|*/ N'BatteryExternalSensor2',   /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00022, /*|*/ N'BLE Battery #3',                  /*|*/ N'BatteryExternalSensor3',   /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00023, /*|*/ N'BLE Battery #4',                  /*|*/ N'BatteryExternalSensor4',   /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        100, /*|*/   8)
					,(00086, /*|*/ N'BLE Humidity #1',                 /*|*/ N'HumiditySensor1',          /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/       1000, /*|*/  16)
					,(00104, /*|*/ N'BLE Humidity #2',                 /*|*/ N'HumiditySensor2',          /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/       1000, /*|*/  16)
					,(00106, /*|*/ N'BLE Humidity #3',                 /*|*/ N'HumiditySensor3',          /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/       1000, /*|*/  16)
					,(00108, /*|*/ N'BLE Humidity #4',                 /*|*/ N'HumiditySensor4',          /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/       1000, /*|*/  16)
					,(00335, /*|*/ N'BLE Luminosity #1',               /*|*/ N'LuminositSensor1',         /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00336, /*|*/ N'BLE Luminosity #2',               /*|*/ N'LuminositSensor2',         /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00337, /*|*/ N'BLE Luminosity #3',               /*|*/ N'LuminositSensor3',         /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00338, /*|*/ N'BLE Luminosity #4',               /*|*/ N'LuminositSensor4',         /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00270, /*|*/ N'BLE Fuel Level #1',               /*|*/ N'CAN_FuelLevel1',           /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00273, /*|*/ N'BLE Fuel Level #2',               /*|*/ N'CAN_FuelLevel2',           /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00276, /*|*/ N'BLE Fuel Level #3',               /*|*/ N'CAN_FuelLevel3',           /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00279, /*|*/ N'BLE Fuel Level #4',               /*|*/ N'CAN_FuelLevel4',           /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      65535, /*|*/  16)
					,(00182, /*|*/ N'GNSS HDOP',                       /*|*/ N'HDOP',                     /*|*/ 0, /*|*/ 0, /*|*/   0.1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/        500, /*|*/  16)
					,(00201, /*|*/ N'LLS 1 Fuel Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/     -4, /*|*/      32767, /*|*/  16)
					,(00202, /*|*/ N'LLS 1 Temperature',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00203, /*|*/ N'LLS 2 Fuel Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/     -4, /*|*/      32767, /*|*/  16)
					,(00204, /*|*/ N'LLS 2 Temperature',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00210, /*|*/ N'LLS 3 Fuel Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/     -4, /*|*/      32767, /*|*/  16)
					,(00211, /*|*/ N'LLS 3 Temperature',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00212, /*|*/ N'LLS 4 Fuel Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/     -4, /*|*/      32767, /*|*/  16)
					,(00213, /*|*/ N'LLS 4 Temperature',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00214, /*|*/ N'LLS 5 Fuel Level',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/     -4, /*|*/      32767, /*|*/  16)
					,(00215, /*|*/ N'LLS 5 Temperature',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/   -128, /*|*/        127, /*|*/   8)
					,(00252, /*|*/ N'Main Battery',                    /*|*/ N'MainBattery',              /*|*/ 1, /*|*/ 1, /*|*/    -1, /*|*/ 1, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   1)
					,(00239, /*|*/ N'Ignition',                        /*|*/ N'Ignition',                 /*|*/ 1, /*|*/ 1, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   8)
					,(00240, /*|*/ N'Movement',                        /*|*/ N'Movement',                 /*|*/ 1, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   8)
					,(00246, /*|*/ N'Towing',                          /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          2, /*|*/   8)
					,(00247, /*|*/ N'Crash detection',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          5, /*|*/   8)
					,(00249, /*|*/ N'Jamming',                         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          1, /*|*/   8)
					,(00253, /*|*/ N'Green driving type',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/          3, /*|*/   8)
					,(00255, /*|*/ N'Over Speeding',                   /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/     1, /*|*/ 0, /*|*/ 0, /*|*/      0, /*|*/      32768, /*|*/   8)			
			) S([NUMBER], [DESCRIPTION], [LEGEND], [DIGITAL], [MANDATORY], [DEFAULTMULTIPLIER], [DEFAULTCONSTANT], [DEFAULT_VALUE], [MIN_VALUE], [MAX_VALUE], [BITS])
		OPEN SENSORS
		FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		WHILE (@@FETCH_STATUS <> -1)
		BEGIN
			IF (@@FETCH_STATUS <> -2)
			BEGIN
				--/////////////////////////////////////////////////
				EXEC [dbo].[AddOrUpdateControllerSensor] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_digital, @LEGEND, 0/*@MANDATORY*/, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT
				SELECT
					@CONTROLLER_SENSOR_TYPE_ID = CASE @CONTROLLER_SENSOR_digital WHEN 1 THEN 2 ELSE 1 END,
					@MAX_VALUE                 = COALESCE(@MAX_VALUE, POWER(CAST(2 AS bigint), COALESCE(@BITS, 1) - 1))
				EXEC [dbo].[SetControllerSensorNumber] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_TYPE_ID, @DEFAULT_VALUE, @MAX_VALUE, @MIN_VALUE, @BITS
				--/////////////////////////////////////////////////
				DECLARE @CONTROLLER_TYPE_ID int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME);
				UPDATE [dbo].[CONTROLLER_SENSOR]
					SET
						 [Default_Multiplier] = @DEFAULTMULTIPLIER
						,[Default_Constant]   = @DEFAULTCONSTANT
						,[Mandatory]          = @MANDATORY
				WHERE [CONTROLLER_TYPE_ID] = @CONTROLLER_TYPE_ID
				AND   [NUMBER]             = @NUMBER
				--/////////////////////////////////////////////////
			END
			FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		END
		CLOSE      SENSORS;
		DEALLOCATE SENSORS;
		------------------------------------------------------------------
		DECLARE    DELETES CURSOR
		FORWARD_ONLY READ_ONLY STATIC LOCAL
		FOR
			SELECT [NUMBER]
			FROM
			(
				VALUES
					(3),(6)
			) S([NUMBER])
		OPEN DELETES
		FETCH NEXT FROM DELETES INTO @NUMBER
		WHILE (@@FETCH_STATUS <> -1)
		BEGIN
			IF (@@FETCH_STATUS <> -2)
			BEGIN
				------------------------------------------------------------------
				DECLARE @OLD_CONTROLLER_SENSOR_ID int =
				(
					SELECT TOP(1) [CONTROLLER_SENSOR_ID] FROM [dbo].[CONTROLLER_SENSOR] CS
						JOIN [dbo].[CONTROLLER_TYPE] CT
							ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
					WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
					AND   CS.[NUMBER]    = @NUMBER
				)
				DELETE [dbo].[CONTROLLER_SENSOR_MAP]
				OUTPUT DELETED.*
				WHERE [CONTROLLER_SENSOR_ID] = @OLD_CONTROLLER_SENSOR_ID
				------------------------------------------------------------------
				DELETE [dbo].[CONTROLLER_SENSOR]
					OUTPUT DELETED.*
				WHERE [CONTROLLER_SENSOR_ID] = @OLD_CONTROLLER_SENSOR_ID
				------------------------------------------------------------------
			END
			FETCH NEXT FROM DELETES INTO @NUMBER
		END
		CLOSE      DELETES;
		DEALLOCATE DELETES;
		------------------------------------------------------------------
		SELECT
			CS.DESCRIPT, CS.NUMBER, CS.*
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		-- Commands
		------------------------------------------------------------------
		SELECT
			CT.[TYPE_NAME], CC.*
		FROM [dbo].[CONTROLLER_TYPE] CT
			INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
				ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
					INNER JOIN [dbo].[CommandTypes] CC
						ON CC.[id] = TT.[CommandTypes_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		-- Удалить команды у контроллера
		--EXEC [dbo].[RemoveControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
		-- Добавить команды к контроллеру
		--EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'...'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Setup'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Immobilize'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Deimmobilize'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetInterval'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskGPSPosition'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskGoogleLink'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'AskBattery'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'BluetoothScan'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'BluetoothDiscoveredList'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'BluetoothConnectedList'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'SetOdometer'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'GetOdometer'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'GetOBDInfo'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'GetFaultCodes'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'GetVin'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'Status'
		EXEC [dbo].[AddControllerTypeCommand] @CONTROLLER_TYPE_NAME, N'ReloadDevice'
		------------------------------------------------------------------
		SELECT
			CT.[TYPE_NAME], CC.*
		FROM [dbo].[CONTROLLER_TYPE] CT
			INNER JOIN [dbo].[Controller_Type_CommandTypes] TT
				ON TT.[Controller_Type_ID] = CT.[CONTROLLER_TYPE_ID]
					INNER JOIN [dbo].[CommandTypes] CC
						ON CC.[id] = TT.[CommandTypes_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME, @DeviceIdIsImei, @SupportsPassword, @AllowedToAddByCustomer, @ImagePath;
	END
	CLOSE      CONTROLLER_TYPES;
	DEALLOCATE CONTROLLER_TYPES;
	------------------------------------------------------------------
	-- Sort controllers
	------------------------------------------------------------------
	MERGE [dbo].[CONTROLLER_TYPE] AS DST
	USING
	(
		SELECT
			[TYPE_NAME],
			[SortOrder] =
			CASE [TYPE_NAME]
				WHEN N'Unspecified' THEN 1
				WHEN N'SoftTracker' THEN 2
				ELSE ROW_NUMBER() OVER (ORDER BY [TYPE_NAME]) + 2
			END
		FROM [dbo].[CONTROLLER_TYPE]
	) AS SRC ([TYPE_NAME], [SortOrder])
		ON DST.[TYPE_NAME] = SRC.[TYPE_NAME]
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[SortOrder] = SRC.[SortOrder]
	OUTPUT $action, INSERTED.[TYPE_NAME], INSERTED.[SortOrder], DELETED.[SortOrder]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO