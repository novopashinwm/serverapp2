BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	EXEC AddSensorLegend @number = 230, /*|*/ @name = N'HDOP', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 254, /*|*/ @name = N'VDOP', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 255, /*|*/ @name = N'PDOP', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 256, /*|*/ @name = N'TDOP', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 257, /*|*/ @name = N'GDOP', /*|*/ @digital = 0
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH