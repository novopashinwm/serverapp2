DECLARE @DATE datetime = N'2020-01-01'
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT DISTINCT
		H.*
	FROM [dbo].[H_OPERATOR] H
	WHERE 1 = 1
	AND 0 < LEN([LOGIN])
	AND [ACTUAL_TIME] > @DATE AND [ACTION] = N'INSERT'
	AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_department_right] WHERE [operator_id] = H.[OPERATOR_ID])
	AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_rights]           WHERE [operator_id] = H.[OPERATOR_ID] AND [right_id] = 2)
	------------------------------------------------------------------
	--���������� ���� ��� ���������� ���� ��� ����, �� ���������
	MERGE [dbo].[RIGHT_OPERATOR] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID], 2, 1
		FROM [dbo].[H_OPERATOR] H
		WHERE 1 = 1
		AND 0 < LEN([LOGIN])
		AND [ACTUAL_TIME] > @DATE AND [ACTION] = N'INSERT'
		AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_department_right] WHERE [operator_id] = H.[OPERATOR_ID])
		AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_rights]           WHERE [operator_id] = H.[OPERATOR_ID] AND [right_id] = 2)
	) AS SRC ([OPERATOR_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
		AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
	WHEN MATCHED AND DST.[ALLOWED] = 0 THEN
		UPDATE
			SET
				DST.[ALLOWED] = 1
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([OPERATOR_ID], [RIGHT_ID], [ALLOWED]) VALUES (SRC.[OPERATOR_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	SELECT DISTINCT
		H.*
	FROM [dbo].[H_OPERATOR] H
	WHERE 1 = 1
	AND 0 < LEN([LOGIN])
	AND [ACTUAL_TIME] > @DATE AND [ACTION] = N'INSERT'
	AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_department_right] WHERE [operator_id] = H.[OPERATOR_ID])
	AND NOT EXISTS(SELECT * FROM [dbo].[v_operator_rights]           WHERE [operator_id] = H.[OPERATOR_ID] AND [right_id] = 2)
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH