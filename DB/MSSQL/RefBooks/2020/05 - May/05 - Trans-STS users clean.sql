﻿DECLARE @deptId        int = (SELECT TOP(1) [DEPARTMENT_ID] FROM [dbo].[DEPARTMENT] WHERE [NAME]  = 'Trans-STS')
DECLARE @operId        int = (SELECT TOP(1) [OPERATOR_ID]   FROM [dbo].[OPERATOR]   WHERE [LOGIN] = '79136861407')
DECLARE @rightSecAdmId int = (SELECT TOP(1) [RIGHT_ID]      FROM [dbo].[RIGHT]      WHERE [NAME] = N'SecurityAdministration')
DECLARE @rightDepAccId int = (SELECT TOP(1) [RIGHT_ID]      FROM [dbo].[RIGHT]      WHERE [NAME] = N'DepartmentsAccess')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[v_operator_department_right] WHERE [operator_id] = @operId AND [department_id] = @deptId
	SELECT * FROM [dbo].[v_operator_vehicle_right]    WHERE [operator_id] = @operId AND [priority]  = 1
	SELECT * FROM [dbo].[v_operator_vehicle_right]    WHERE [operator_id] = @operId AND [priority] <> 1
	------------------------------------------------------------------
	-- [dbo].[OPERATORGROUP_OPERATOR] Удаляем вхождение в группу департамента
	------------------------------------------------------------------
	MERGE [dbo].[OPERATORGROUP_OPERATOR] AS DST
	USING
	(
		SELECT
			*
		FROM [dbo].[OPERATORGROUP_OPERATOR]
		WHERE 1 = 1
		AND [OPERATOR_ID]      = @operId
		AND [OPERATORGROUP_ID] IN
		(
			SELECT
				G.[OPERATORGROUP_ID]
			FROM [dbo].[OPERATORGROUP] G
			WHERE 1 = 1
			AND     EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightDepAccId AND [OPERATORGROUP_ID] = G.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = @deptId)
			AND NOT EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightSecAdmId AND [OPERATORGROUP_ID] = G.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = @deptId)
		)
	) AS SRC ([OPERATORGROUP_ID], [OPERATOR_ID])
		ON  DST.[OPERATORGROUP_ID] = SRC.[OPERATORGROUP_ID]
		AND DST.[OPERATOR_ID]      = SRC.[OPERATOR_ID]
	WHEN MATCHED THEN
		DELETE
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_DEPARTMENT] Удаляем прямые права на департамент
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
	USING
	(
		VALUES
			(@operId, @deptId)
	) AS SRC ([OPERATOR_ID], [DEPARTMENT_ID])
		ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
		AND DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
	WHEN MATCHED THEN
		DELETE
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	SELECT * FROM [dbo].[v_operator_department_right] WHERE [operator_id] = @operId AND [department_id] = @deptId
	SELECT * FROM [dbo].[v_operator_vehicle_right]    WHERE [operator_id] = @operId AND [priority]  = 1
	SELECT * FROM [dbo].[v_operator_vehicle_right]    WHERE [operator_id] = @operId AND [priority] <> 1
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO