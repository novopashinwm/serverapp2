﻿DECLARE @rightSecAdmId int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'SecurityAdministration')
DECLARE @rightDepAccId int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'DepartmentsAccess')
DECLARE @rightVehAccId int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'VehicleAccess')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	-- Объявляем таблицу пар оператор - департамент
	DECLARE @OperatorDepartment TABLE
	(
		[DEPARTMENT_ID] int NOT NULL,
		[OPERATOR_ID]   int NOT NULL
	);
	-- Заполняем таблицу пар оператор - департамент
	INSERT INTO @OperatorDepartment
	(
		[DEPARTMENT_ID], [OPERATOR_ID]
	)
	SELECT
		V.[DEPARTMENT_ID],
		V.[OPERATOR_ID]
	FROM [dbo].[OPERATOR_DEPARTMENT] V
		INNER JOIN
		(
			SELECT DISTINCT
				D.[DEPARTMENT_ID],
				M.[OPERATOR_ID]
			FROM [dbo].[DEPARTMENT] D
			CROSS APPLY
			(
				SELECT
					X.*
				FROM [dbo].[OPERATORGROUP] X
				WHERE 1 = 1
				AND     EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightDepAccId AND [OPERATORGROUP_ID] = X.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = D.[DEPARTMENT_ID])
				AND NOT EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightSecAdmId AND [OPERATORGROUP_ID] = X.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = D.[DEPARTMENT_ID])
			) G
				INNER JOIN [dbo].[OPERATORGROUP_OPERATOR] M
					ON M.[OPERATORGROUP_ID] = G.[OPERATORGROUP_ID]
						INNER JOIN [dbo].[OPERATOR] O
							ON O.[OPERATOR_ID] = M.[OPERATOR_ID]
		) X
			ON  X.[DEPARTMENT_ID] = V.[DEPARTMENT_ID]
			AND X.[OPERATOR_ID]   = V.[OPERATOR_ID]
	WHERE V.[RIGHT_ID] = @rightDepAccId
	--AND   V.[DEPARTMENT_ID] = ... -- Можно отфильтровать по одному депаратаменту
	-- Объявляем таблицу пар оператор - департамент для RU
	DECLARE @OperatorDepartmentRU TABLE
	(
		[DEPARTMENT_ID] int NOT NULL,
		[OPERATOR_ID]   int NOT NULL
	);
	-- Заполняем таблицу пар оператор - департамент для RU
	INSERT INTO @OperatorDepartmentRU
	(
		[DEPARTMENT_ID], [OPERATOR_ID]
	)
	SELECT DISTINCT
		[DEPARTMENT_ID], [OPERATOR_ID]
	FROM @OperatorDepartment
	WHERE [DEPARTMENT_ID] NOT IN
	(
		SELECT
			[department_id]
		FROM [dbo].[v_operator_department_right]
		WHERE [operator_id] IN
		(
			SELECT
				[OPERATOR_ID]
			FROM [dbo].[OPERATOR]
			WHERE [LOGIN] = N'NIKA_India'
		)
		AND [right_id] IN (@rightSecAdmId, @rightDepAccId)
	)
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLE]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLE] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID] = V.[OPERATOR_ID],
			[VEHICLE_ID]  = V.[VEHICLE_ID],
			[RIGHT_ID]    = N.[RIGHT_ID],
			[ALLOWED]     = N.[ALLOWED]
		FROM [dbo].[v_operator_vehicle_right] V
			INNER JOIN (SELECT DISTINCT [OPERATOR_ID] FROM @OperatorDepartmentRU) R
				ON  R.[OPERATOR_ID] = V.[OPERATOR_ID]
			CROSS APPLY
			(
				SELECT
					R.[RIGHT_ID], X.[ALLOWED]
				FROM [dbo].[RIGHT] R
					INNER JOIN
					(
						VALUES
							 (N'',                         0)
							,(N'VehicleAccess',            1)
							,(N'PathAccess',               1)
							,(N'EditVehicles',             1)
							,(N'Immobilization',           1)
							,(N'ChangeDeviceConfigBySMS',  1)
							,(N'EditGroup',                1)
							,(N'ViewingTrackerAttributes', 1)
							,(N'EditingTrackerAttributes', 1)
							,(N'CommandAccess',            1)
							,(N'ShareLinkAccess',          1)
							,(N'ManageSensors',            1)
							,(N'SecurityAdministration',   1)
					) X([RIGHT_NAME], [ALLOWED])
						ON X.[RIGHT_NAME] = R.[NAME]
			) N
		WHERE V.[RIGHT_ID] = @rightVehAccId
	) AS SRC ([OPERATOR_ID], [VEHICLE_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
		AND DST.[VEHICLE_ID]  = SRC.[VEHICLE_ID]
		AND DST.[RIGHT_ID]    = SRC.[RIGHT_ID]
	WHEN MATCHED AND SRC.[ALLOWED] = 0 THEN
		DELETE
	WHEN MATCHED AND SRC.[ALLOWED] = 1 THEN
		UPDATE
			SET
				 DST.[ALLOWED] = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET AND SRC.[ALLOWED] = 1 THEN
		INSERT (    [OPERATOR_ID],     [VEHICLE_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLE_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	-- Объявляем таблицу пар оператор - департамент для IN
	DECLARE @OperatorDepartmentIN TABLE
	(
		[DEPARTMENT_ID] int NOT NULL,
		[OPERATOR_ID]   int NOT NULL
	);
	-- Заполняем таблицу пар оператор - департамент для IN
	INSERT INTO @OperatorDepartmentIN
	(
		[DEPARTMENT_ID], [OPERATOR_ID]
	)
	SELECT DISTINCT
		[DEPARTMENT_ID], [OPERATOR_ID]
	FROM @OperatorDepartment
	WHERE [DEPARTMENT_ID]     IN
	(
		SELECT
			[department_id]
		FROM [dbo].[v_operator_department_right]
		WHERE [operator_id] IN
		(
			SELECT
				[OPERATOR_ID]
			FROM [dbo].[OPERATOR]
			WHERE [LOGIN] = N'NIKA_India'
		)
		AND [right_id] IN (@rightSecAdmId, @rightDepAccId)
	)
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLE]
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLE] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID] = V.[OPERATOR_ID],
			[VEHICLE_ID]  = V.[VEHICLE_ID],
			[RIGHT_ID]    = N.[RIGHT_ID],
			[ALLOWED]     = N.[ALLOWED]
		FROM [dbo].[v_operator_vehicle_right] V
			INNER JOIN (SELECT DISTINCT [OPERATOR_ID] FROM @OperatorDepartmentIN) R
				ON  R.[OPERATOR_ID] = V.[OPERATOR_ID]
			CROSS APPLY
			(
				SELECT
					R.[RIGHT_ID], X.[ALLOWED]
				FROM [dbo].[RIGHT] R
					INNER JOIN
					(
						VALUES
							 (N'',                         0)
							,(N'ShareLinkAccess',          1)
							,(N'CommandAccess',            1)
							,(N'SecurityAdministration',   1)
							,(N'EditVehicles',             0)
							,(N'ViewingTrackerAttributes', 0)
							,(N'EditingTrackerAttributes', 0)
					) X([RIGHT_NAME], [ALLOWED])
						ON X.[RIGHT_NAME] = R.[NAME]
			) N
		WHERE V.[RIGHT_ID] = @rightVehAccId
	) AS SRC ([OPERATOR_ID], [VEHICLE_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
		AND DST.[VEHICLE_ID]  = SRC.[VEHICLE_ID]
		AND DST.[RIGHT_ID]    = SRC.[RIGHT_ID]
	WHEN MATCHED AND SRC.[ALLOWED] = 0 THEN
		DELETE
	WHEN MATCHED AND SRC.[ALLOWED] = 1 THEN
		UPDATE
			SET
				 DST.[ALLOWED] = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET AND SRC.[ALLOWED] = 1 THEN
		INSERT (    [OPERATOR_ID],     [VEHICLE_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLE_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH