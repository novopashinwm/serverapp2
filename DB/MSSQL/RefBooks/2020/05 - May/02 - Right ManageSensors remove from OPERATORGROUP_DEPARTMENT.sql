﻿/*
Все права уровней приоритетов 7(OPERATOR_DEPARTMENT) и 8 (OPERATORGROUP_DEPARTMENT) переносим на уровень 1 (OPERATOR_VEHICLE)
*/
-- Получаем идентификатор группы суперадмина
DECLARE @superAdminGroupId int = CONVERT(int, (SELECT [VALUE] FROM [dbo].[CONSTANTS] WHERE [NAME] = 'SalesOperatorGroupID'))
-- Получаем идентификаторы операторов группы суперадмина
DECLARE @superAdminIds [dbo].[Id_Param]
INSERT INTO @superAdminIds SELECT [OPERATOR_ID] FROM [dbo].[OPERATORGROUP_OPERATOR] WHERE [OPERATORGROUP_ID] = @superAdminGroupId

BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT
		V.[priority], [cnt] = COUNT(*)
	FROM [dbo].[v_operator_vehicle_right] V
		INNER JOIN [dbo].[RIGHT] R
			ON R.[RIGHT_ID] = V.[RIGHT_ID]
	WHERE R.[NAME] IN (N'ManageSensors')
	AND   V.[priority] IN (1, 7, 8)
	AND   V.[operator_id] NOT IN (SELECT [Id] FROM @superAdminIds)
	GROUP BY V.[priority]
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLE] переносим все права с 7 и 8 уровня на уровень 1 (непосредственно на объект)
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLE] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID] = V.[OPERATOR_ID],
			[VEHICLE_ID]  = V.[VEHICLE_ID],
			[RIGHT_ID]    = V.[RIGHT_ID],
			[ALLOWED]     = 1
		FROM [dbo].[v_operator_vehicle_right] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME] IN (N'ManageSensors')
		AND   V.[priority] IN (7, 8)
		AND   V.[operator_id] NOT IN (SELECT [Id] FROM @superAdminIds)
	) AS SRC ([OPERATOR_ID], [VEHICLE_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
		AND DST.[VEHICLE_ID]  = SRC.[VEHICLE_ID]
		AND DST.[RIGHT_ID]    = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATOR_ID],     [VEHICLE_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLE_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATORGROUP_DEPARTMENT] Удаляем права с уровня 8
	------------------------------------------------------------------
	MERGE [dbo].[OPERATORGROUP_DEPARTMENT] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATORGROUP_ID] = V.[OPERATORGROUP_ID],
			[DEPARTMENT_ID]    = V.[DEPARTMENT_ID],
			[RIGHT_ID]         = V.[RIGHT_ID]
		FROM [dbo].[OPERATORGROUP_DEPARTMENT] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME]             IN (N'ManageSensors')
		AND   V.[OPERATORGROUP_ID] <> @superAdminGroupId
	) AS SRC ([OPERATORGROUP_ID], [DEPARTMENT_ID], [RIGHT_ID])
		ON  DST.[OPERATORGROUP_ID] = SRC.[OPERATORGROUP_ID]
		AND DST.[DEPARTMENT_ID]    = SRC.[DEPARTMENT_ID]
		AND DST.[RIGHT_ID]         = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		DELETE
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_DEPARTMENT] Удаляем права с уровня 7
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID]   = V.[OPERATOR_ID],
			[DEPARTMENT_ID] = V.[DEPARTMENT_ID],
			[RIGHT_ID]      = V.[RIGHT_ID]
		FROM [dbo].[OPERATOR_DEPARTMENT] V
			INNER JOIN [dbo].[RIGHT] R
				ON R.[RIGHT_ID] = V.[RIGHT_ID]
		WHERE R.[NAME]             IN (N'ManageSensors')
	) AS SRC ([OPERATOR_ID], [DEPARTMENT_ID], [RIGHT_ID])
		ON  DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
		AND DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
		AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		DELETE
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	SELECT
		V.[priority], [cnt] = COUNT(*)
	FROM [dbo].[v_operator_vehicle_right] V
		INNER JOIN [dbo].[RIGHT] R
			ON R.[RIGHT_ID] = V.[RIGHT_ID]
	WHERE R.[NAME] IN (N'ManageSensors')
	AND   V.[priority] IN (1, 7, 8)
	AND   V.[operator_id] NOT IN (SELECT [Id] FROM @superAdminIds)
	GROUP BY V.[priority]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO