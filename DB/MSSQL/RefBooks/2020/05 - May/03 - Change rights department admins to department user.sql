﻿DECLARE @rightSecAdmId int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'SecurityAdministration')
DECLARE @rightDepAccId int = (SELECT TOP(1) [RIGHT_ID] FROM [dbo].[RIGHT] WHERE [NAME] = N'DepartmentsAccess')
BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	-- Объявляем таблицу прав, которые нужно перенсти с уровня департамента на объект
	DECLARE @OperatorDepartmentRights TABLE
	(
		[DEPARTMENT_ID] int NOT NULL,
		[OPERATOR_ID]   int NOT NULL,
		[RIGHT_ID]      int NOT NULL
	);
	-- Заполняем таблицу прав, которые нужно перенсти с уровня департамента на объект
	INSERT INTO @OperatorDepartmentRights
	(
		[DEPARTMENT_ID], [OPERATOR_ID], [RIGHT_ID]
	)
	SELECT
		V.[DEPARTMENT_ID],
		V.[OPERATOR_ID],
		V.[RIGHT_ID]
	FROM [dbo].[OPERATOR_DEPARTMENT] V
		INNER JOIN
		(
			SELECT DISTINCT
				D.[DEPARTMENT_ID],
				M.[OPERATOR_ID]
			FROM [dbo].[DEPARTMENT] D
			CROSS APPLY
			(
				SELECT
					X.*
				FROM [dbo].[OPERATORGROUP] X
				WHERE 1 = 1
				AND     EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightDepAccId AND [OPERATORGROUP_ID] = X.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = D.[DEPARTMENT_ID])
				AND NOT EXISTS(SELECT * FROM [dbo].[OPERATORGROUP_DEPARTMENT] WHERE [RIGHT_ID] = @rightSecAdmId AND [OPERATORGROUP_ID] = X.[OPERATORGROUP_ID] AND [DEPARTMENT_ID] = D.[DEPARTMENT_ID])
			) G
				INNER JOIN [dbo].[OPERATORGROUP_OPERATOR] M
					ON M.[OPERATORGROUP_ID] = G.[OPERATORGROUP_ID]
						INNER JOIN [dbo].[OPERATOR] O
							ON O.[OPERATOR_ID] = M.[OPERATOR_ID]
		) X
			ON  X.[DEPARTMENT_ID] = V.[DEPARTMENT_ID]
			AND X.[OPERATOR_ID]   = V.[OPERATOR_ID]
	WHERE V.[RIGHT_ID] <> @rightDepAccId
	------------------------------------------------------------------
	-- Получение прав до обновления
	SELECT V.* FROM [dbo].[v_operator_vehicle_right] V
		INNER JOIN (SELECT DISTINCT [OPERATOR_ID], [RIGHT_ID] FROM @OperatorDepartmentRights) R
			ON  R.[OPERATOR_ID] = V.[operator_id]
			AND R.[RIGHT_ID]    = V.[right_id]
	WHERE V.[right_id] IN (SELECT DISTINCT [RIGHT_ID] FROM @OperatorDepartmentRights)
	SELECT V.* FROM [dbo].[v_operator_department_right] V
		INNER JOIN @OperatorDepartmentRights R
			ON  R.[OPERATOR_ID]   = V.[operator_id]
			AND R.[DEPARTMENT_ID] = V.[department_id]
	------------------------------------------------------------------
	-- [dbo].[OPERATOR_VEHICLE] переносим все права с 7 и 4 уровня на уровень 1 (непосредственно на объект)
	------------------------------------------------------------------
	MERGE [dbo].[OPERATOR_VEHICLE] AS DST
	USING
	(
		SELECT DISTINCT
			[OPERATOR_ID] = V.[OPERATOR_ID],
			[VEHICLE_ID]  = V.[VEHICLE_ID],
			[RIGHT_ID]    = V.[RIGHT_ID],
			[ALLOWED]     = 1
		FROM [dbo].[v_operator_vehicle_right] V
			INNER JOIN (SELECT DISTINCT [OPERATOR_ID], [RIGHT_ID] FROM @OperatorDepartmentRights) R
				ON  R.[OPERATOR_ID] = V.[OPERATOR_ID]
				AND R.[RIGHT_ID]    = V.[RIGHT_ID]
		WHERE 1 = 1
		AND   V.[priority] IN (4, 7)
	) AS SRC ([OPERATOR_ID], [VEHICLE_ID], [RIGHT_ID], [ALLOWED])
		ON  DST.[OPERATOR_ID] = SRC.[OPERATOR_ID]
		AND DST.[VEHICLE_ID]  = SRC.[VEHICLE_ID]
		AND DST.[RIGHT_ID]    = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[ALLOWED]      = SRC.[ALLOWED]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [OPERATOR_ID],     [VEHICLE_ID],     [RIGHT_ID],     [ALLOWED])
		VALUES (SRC.[OPERATOR_ID], SRC.[VEHICLE_ID], SRC.[RIGHT_ID], SRC.[ALLOWED])
	OUTPUT $action, INSERTED.*, DELETED.*;

	-- Удаление прав на уровне 7 (при удалении отсюда права SecurityAdministration, это еще и удаление прав уровня 4)
	MERGE [dbo].[OPERATOR_DEPARTMENT] AS DST
	USING
	(
		SELECT
			[DEPARTMENT_ID],
			[OPERATOR_ID],
			[RIGHT_ID]
		FROM @OperatorDepartmentRights
	) AS SRC ([DEPARTMENT_ID], [OPERATOR_ID], [RIGHT_ID])
		ON  DST.[DEPARTMENT_ID] = SRC.[DEPARTMENT_ID]
		AND DST.[OPERATOR_ID]   = SRC.[OPERATOR_ID]
		AND DST.[RIGHT_ID]      = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		DELETE
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	-- Получение прав до после
	SELECT V.* FROM [dbo].[v_operator_vehicle_right] V
		INNER JOIN (SELECT DISTINCT [OPERATOR_ID], [RIGHT_ID] FROM @OperatorDepartmentRights) R
			ON  R.[OPERATOR_ID] = V.[operator_id]
			AND R.[RIGHT_ID]    = V.[right_id]
	WHERE V.[right_id] IN (SELECT DISTINCT [RIGHT_ID] FROM @OperatorDepartmentRights)
	SELECT V.* FROM [dbo].[v_operator_department_right] V
		INNER JOIN @OperatorDepartmentRights R
			ON  R.[OPERATOR_ID]   = V.[operator_id]
			AND R.[DEPARTMENT_ID] = V.[department_id]
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH