BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	MERGE [dbo].[RemoteTerminalServer] AS DST
	USING
	(
		VALUES
			 (N'ДМ-Транс EGTS',   N'egts://62.113.122.184:39001?authasobject=true&onlyposition=true||true')
			,(N'ДМ-Транс Wialon', N'wialon://62.113.122.184:39002/')
	) AS SRC ([Name], [Url])
		ON  DST.[Name] = SRC.[Name] COLLATE Cyrillic_General_CI_AS
	WHEN MATCHED THEN
		UPDATE
			SET
				DST.[Url] = SRC.[Url]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Name], [Url]) VALUES (SRC.[Name], SRC.[Url])
	OUTPUT $action, INSERTED.*, DELETED.*;
	---------------------------------------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
	SET IDENTITY_INSERT [dbo].[RemoteTerminalServer] OFF;
END CATCH