BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	EXEC AddSensorLegend @number = 601, /*|*/ @name = N'ToEgtsDigitalSensor1', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 602, /*|*/ @name = N'ToEgtsDigitalSensor2', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 603, /*|*/ @name = N'ToEgtsDigitalSensor3', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 604, /*|*/ @name = N'ToEgtsDigitalSensor4', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 605, /*|*/ @name = N'ToEgtsDigitalSensor5', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 606, /*|*/ @name = N'ToEgtsDigitalSensor6', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 607, /*|*/ @name = N'ToEgtsDigitalSensor7', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 608, /*|*/ @name = N'ToEgtsDigitalSensor8', /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 609, /*|*/ @name = N'ToEgtsIgnition',       /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 610, /*|*/ @name = N'ToEgtsHDOP',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 611, /*|*/ @name = N'ToEgtsVDOP',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 612, /*|*/ @name = N'ToEgtsPDOP',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 613, /*|*/ @name = N'ToEgtsAnalogSensor1',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 614, /*|*/ @name = N'ToEgtsAnalogSensor2',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 615, /*|*/ @name = N'ToEgtsAnalogSensor3',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 616, /*|*/ @name = N'ToEgtsAnalogSensor4',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 617, /*|*/ @name = N'ToEgtsAnalogSensor5',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 618, /*|*/ @name = N'ToEgtsAnalogSensor6',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 619, /*|*/ @name = N'ToEgtsAnalogSensor7',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 620, /*|*/ @name = N'ToEgtsAnalogSensor8',  /*|*/ @digital = 0
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH