BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	EXEC AddSensorLegend @number = 261, /*|*/ @name = N'TemperatureSensor1',     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 262, /*|*/ @name = N'TemperatureSensor2',     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 263, /*|*/ @name = N'TemperatureSensor3',     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 264, /*|*/ @name = N'TemperatureSensor4',     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 271, /*|*/ @name = N'BatteryExternalSensor1', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 272, /*|*/ @name = N'BatteryExternalSensor2', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 273, /*|*/ @name = N'BatteryExternalSensor3', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 274, /*|*/ @name = N'BatteryExternalSensor4', /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 281, /*|*/ @name = N'HumiditySensor1',        /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 282, /*|*/ @name = N'HumiditySensor2',        /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 283, /*|*/ @name = N'HumiditySensor3',        /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 284, /*|*/ @name = N'HumiditySensor4',        /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 291, /*|*/ @name = N'LuminositSensor1',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 292, /*|*/ @name = N'LuminositSensor2',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 293, /*|*/ @name = N'LuminositSensor3',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 294, /*|*/ @name = N'LuminositSensor4',       /*|*/ @digital = 0
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH