DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;

--/// <summary> Получить координаты и время </summary>
--AskGPSPosition     = 62,
SELECT
	@id                 = 62,
	@code               = N'AskGPSPosition',
	@description        = N'Current GPS data, date and time.',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Запросить ссылку на Google карты </summary>
--AskGoogleLink      = 63,
SELECT
	@id                 = 63,
	@code               = N'AskGoogleLink',
	@description        = N'Location information with Google maps link.',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Состояние батареи </summary>
--AskBattery         = 64,
SELECT
	@id                 = 64,
	@code               = N'AskBattery',
	@description        = N'Battery charge level',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Поиск Bluetooth устройств </summary>
--BluetoothScan = 65,
SELECT
	@id                 = 65,
	@code               = N'BluetoothScan',
	@description        = N'Start Bluetooth scan',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--	/// <summary> Вывести список видимых Bluetooth устройств </summary>
--BluetoothDiscoveredList   = 66,
SELECT
	@id                 = 66,
	@code               = N'BluetoothDiscoveredList',
	@description        = N'Get discovered Bluetooth list',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Вывести список присоединенных Bluetooth устройств </summary>
--BluetoothConnectedList    = 67,
SELECT
	@id                 = 67,
	@code               = N'BluetoothConnectedList',
	@description        = N'Get connected Bluetooth list',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Установить значение одометра трекера </summary>
--SetOdometer        = 68,
SELECT
	@id                 = 68,
	@code               = N'SetOdometer',
	@description        = N'Set odometer value',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Получить значение одометра трекера </summary>
--GetOdometer        = 69,
SELECT
	@id                 = 69,
	@code               = N'GetOdometer',
	@description        = N'Get odometer value',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Компьютерная диагностика (OBD) </summary>
--GetOBDInfo         = 70,
SELECT
	@id                 = 70,
	@code               = N'GetOBDInfo',
	@description        = N'Get onboard diagnostics ',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Получить коды ошибок (OBD) </summary>
--GetFaultCodes      = 71,
SELECT
	@id                 = 71,
	@code               = N'GetFaultCodes',
	@description        = N'Get all visible fault code',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
--/// <summary> Получить VIN код </summary>
--GetVin             = 72,
SELECT
	@id                 = 72,
	@code               = N'GetVin',
	@description        = N'Get vehicle VIN code',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
