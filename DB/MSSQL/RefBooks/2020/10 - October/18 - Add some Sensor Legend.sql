BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	EXEC AddSensorLegend @number = 207, /*|*/ @name = N'CAN_FuelLevel1',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 208, /*|*/ @name = N'CAN_FuelLevel2',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 209, /*|*/ @name = N'CAN_FuelLevel3',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 210, /*|*/ @name = N'CAN_FuelLevel4',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 211, /*|*/ @name = N'CAN_FuelLevel5',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 212, /*|*/ @name = N'CAN_FuelLevel6',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 228, /*|*/ @name = N'CAN_BrakeSpecificFuelConsumption',   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 295, /*|*/ @name = N'DallasTemperatureSensor1',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 296, /*|*/ @name = N'DallasTemperatureSensor2',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 297, /*|*/ @name = N'DallasTemperatureSensor3',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 298, /*|*/ @name = N'DallasTemperatureSensor4',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 299, /*|*/ @name = N'DallasTemperatureSensor5',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 300, /*|*/ @name = N'DallasTemperatureSensor6',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 301, /*|*/ @name = N'LLS1FuelLevel',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 302, /*|*/ @name = N'LLS1Temperature',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 303, /*|*/ @name = N'LLS2FuelLevel',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 304, /*|*/ @name = N'LLS2Temperature',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 305, /*|*/ @name = N'LLS3FuelLevel',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 306, /*|*/ @name = N'LLS3Temperature',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 307, /*|*/ @name = N'LLS4FuelLevel',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 308, /*|*/ @name = N'LLS4Temperature',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 309, /*|*/ @name = N'LLS5FuelLevel',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 310, /*|*/ @name = N'LLS5Temperature',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 311, /*|*/ @name = N'FuelCounter',                        /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 312, /*|*/ @name = N'FuelLevel1',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 313, /*|*/ @name = N'FuelLevel2',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 314, /*|*/ @name = N'FuelLevel3',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 315, /*|*/ @name = N'FuelLevel4',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 316, /*|*/ @name = N'FuelLevel5',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 317, /*|*/ @name = N'FuelLevel6',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 318, /*|*/ @name = N'FuelLevel7',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 319, /*|*/ @name = N'DataMode',                           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 320, /*|*/ @name = N'SleepMode',                          /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 321, /*|*/ @name = N'ActiveGSMOperator',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 322, /*|*/ @name = N'DigitalOutput4',                     /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 323, /*|*/ @name = N'UltrasonicFuelLevel1',               /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 324, /*|*/ @name = N'UltrasonicFuelLevel2',               /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 325, /*|*/ @name = N'TemperatureSensor0',                 /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 326, /*|*/ @name = N'TemperatureSensor5',                 /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 327, /*|*/ @name = N'BreakPedal',                         /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 328, /*|*/ @name = N'EngineLoad',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 329, /*|*/ @name = N'CAN_AxleLoad7',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 330, /*|*/ @name = N'CAN_AxleLoad8',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 331, /*|*/ @name = N'CAN_AxleLoad9',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 332, /*|*/ @name = N'CAN_AxleLoad10',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 333, /*|*/ @name = N'CAN_AxleLoad11',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 334, /*|*/ @name = N'CAN_AxleLoad12',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 335, /*|*/ @name = N'CAN_AxleLoad13',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 336, /*|*/ @name = N'CAN_AxleLoad14',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 337, /*|*/ @name = N'CAN_AxleLoad15',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 338, /*|*/ @name = N'InstantaneousFuelEconomy',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 339, /*|*/ @name = N'HighResolutionEngineTotalFuelUsed',  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 340, /*|*/ @name = N'GrossCombinationVehicleWeight',      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 341, /*|*/ @name = N'Tire1pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 342, /*|*/ @name = N'Tire2pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 343, /*|*/ @name = N'Tire3pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 344, /*|*/ @name = N'Tire4pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 345, /*|*/ @name = N'Tire5pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 346, /*|*/ @name = N'Tire6pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 347, /*|*/ @name = N'Tire7pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 348, /*|*/ @name = N'Tire8pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 349, /*|*/ @name = N'Tire9pressure',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 350, /*|*/ @name = N'Tire10pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 351, /*|*/ @name = N'Tire11pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 352, /*|*/ @name = N'Tire12pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 353, /*|*/ @name = N'Tire13pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 354, /*|*/ @name = N'Tire14pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 355, /*|*/ @name = N'Tire15pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 356, /*|*/ @name = N'Tire16pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 357, /*|*/ @name = N'Tire17pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 358, /*|*/ @name = N'Tire18pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 359, /*|*/ @name = N'Tire19pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 360, /*|*/ @name = N'Tire20pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 361, /*|*/ @name = N'Tire21pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 362, /*|*/ @name = N'Tire22pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 363, /*|*/ @name = N'Tire23pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 364, /*|*/ @name = N'Tire24pressure',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 365, /*|*/ @name = N'Tire1temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 366, /*|*/ @name = N'Tire2temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 367, /*|*/ @name = N'Tire3temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 368, /*|*/ @name = N'Tire4temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 369, /*|*/ @name = N'Tire5temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 370, /*|*/ @name = N'Tire6temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 371, /*|*/ @name = N'Tire7temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 372, /*|*/ @name = N'Tire8temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 373, /*|*/ @name = N'Tire9temperature',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 374, /*|*/ @name = N'Tire10temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 375, /*|*/ @name = N'Tire11temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 376, /*|*/ @name = N'Tire12temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 377, /*|*/ @name = N'Tire13temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 378, /*|*/ @name = N'Tire14temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 379, /*|*/ @name = N'Tire15temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 380, /*|*/ @name = N'Tire16temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 381, /*|*/ @name = N'Tire17temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 382, /*|*/ @name = N'Tire18temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 383, /*|*/ @name = N'Tire19temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 384, /*|*/ @name = N'Tire20temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 385, /*|*/ @name = N'Tire21temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 386, /*|*/ @name = N'Tire22temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 387, /*|*/ @name = N'Tire23temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 388, /*|*/ @name = N'Tire24temperature',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 389, /*|*/ @name = N'Tire1warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 390, /*|*/ @name = N'Tire2warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 391, /*|*/ @name = N'Tire3warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 392, /*|*/ @name = N'Tire4warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 393, /*|*/ @name = N'Tire5warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 394, /*|*/ @name = N'Tire6warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 395, /*|*/ @name = N'Tire7warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 396, /*|*/ @name = N'Tire8warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 397, /*|*/ @name = N'Tire9warning',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 398, /*|*/ @name = N'Tire10warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 399, /*|*/ @name = N'Tire11warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 400, /*|*/ @name = N'Tire12warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 401, /*|*/ @name = N'Tire13warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 402, /*|*/ @name = N'Tire14warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 403, /*|*/ @name = N'Tire15warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 404, /*|*/ @name = N'Tire16warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 405, /*|*/ @name = N'Tire17warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 406, /*|*/ @name = N'Tire18warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 407, /*|*/ @name = N'Tire19warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 408, /*|*/ @name = N'Tire20warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 409, /*|*/ @name = N'Tire21warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 410, /*|*/ @name = N'Tire22warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 411, /*|*/ @name = N'Tire23warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 412, /*|*/ @name = N'Tire24warning',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 413, /*|*/ @name = N'CAN_EngHours_CNTD',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 414, /*|*/ @name = N'CAN_TotalFuelSpend_CNTD',            /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 415, /*|*/ @name = N'CAN_TotalRun_CNTD',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 416, /*|*/ @name = N'AdBlueLevelPercent',                 /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 417, /*|*/ @name = N'AdBlueLevelLiters',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 418, /*|*/ @name = N'HarvestingTime',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 419, /*|*/ @name = N'AreaofHarvest',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 420, /*|*/ @name = N'MowingEfficiency',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 421, /*|*/ @name = N'GrainMownVolume',                    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 422, /*|*/ @name = N'GrainMoisture',                      /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 423, /*|*/ @name = N'HarvestingDrumRPM',                  /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 424, /*|*/ @name = N'GapUnderHarvestingDrum',             /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 425, /*|*/ @name = N'BatteryTemperature',                 /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 426, /*|*/ @name = N'DTCErrors',                          /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 427, /*|*/ @name = N'CNGStatus',                          /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 428, /*|*/ @name = N'CNGUsed',                            /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 429, /*|*/ @name = N'CNGLevel',                           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 430, /*|*/ @name = N'OverSpeeding',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 431, /*|*/ @name = N'Greendrivingtype',                   /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 432, /*|*/ @name = N'Towing',                             /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 433, /*|*/ @name = N'Crashdetection',                     /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 434, /*|*/ @name = N'Jamming',                            /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 435, /*|*/ @name = N'TotalTires',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 436, /*|*/ @name = N'TotalAxles',                         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 437, /*|*/ @name = N'DriveRecognize',                     /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 438, /*|*/ @name = N'Driver1WorkingState',                /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 439, /*|*/ @name = N'Driver2WorkingState',                /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 440, /*|*/ @name = N'TachographOverSpeed',                /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 441, /*|*/ @name = N'Driver1CardPresence',                /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 442, /*|*/ @name = N'Driver2CardPresence',                /*|*/ @digital = 1
	EXEC AddSensorLegend @number = 443, /*|*/ @name = N'Driver1TimeRelatedStates',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 444, /*|*/ @name = N'Driver2TimeRelatedStates',           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 445, /*|*/ @name = N'VehicleSpeed',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 446, /*|*/ @name = N'Odometer',                           /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 447, /*|*/ @name = N'TripDistance',                       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 448, /*|*/ @name = N'Timestamp',                          /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 449, /*|*/ @name = N'Card1IssuingMemberState',            /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 450, /*|*/ @name = N'Card2IssuingMemberState',            /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 451, /*|*/ @name = N'Driver1ContinuousDrivingTime',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 452, /*|*/ @name = N'Driver2ContinuousDrivingTime',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 453, /*|*/ @name = N'Driver1CumulativeBreakTime',         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 454, /*|*/ @name = N'Driver2CumulativeBreakTime',         /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 455, /*|*/ @name = N'Driver1SelectedActivityDuration',    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 456, /*|*/ @name = N'Driver2SelectedActivityDuration',    /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 457, /*|*/ @name = N'Driver1CumulativeDrivingTime',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 458, /*|*/ @name = N'Driver2CumulativeDrivingTime',       /*|*/ @digital = 0
	EXEC AddSensorLegend @number = 459, /*|*/ @name = N'TachoDataSource',                    /*|*/ @digital = 0
	------------------------------------------------------------------
	SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND] ORDER BY [Number] DESC
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH