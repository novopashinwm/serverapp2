﻿-- Обновление таблицы единиц измерения
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[Unit] AS DST
	USING
	(
		VALUES
			 (006, N'LitrePerHour')
			,(085, N'Millimeter')
			,(086, N'Kilopascal')
			,(087, N'Minute')
			,(088, N'SquareMeter')
			,(089, N'SquareMetersPerHour')
	) AS SRC ([Id], [Name])
		ON DST.[Id] = SRC.[Id]
	WHEN MATCHED AND DST.[Name] <> SRC.[Name] THEN
		UPDATE
			SET
				DST.[Name] = SRC.[Name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Id], [Name]) VALUES (SRC.[Id], SRC.[Name])
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO