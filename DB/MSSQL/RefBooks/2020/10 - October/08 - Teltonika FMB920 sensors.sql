BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME      nvarchar(50)  = NULL
	DECLARE @LEGEND                    nvarchar(50)  = NULL
	DECLARE @NUMBER                    int           = NULL
	DECLARE @DESCRIPTION               nvarchar(512) = NULL
	DECLARE @CONTROLLER_SENSOR_digital bit           = NULL
	DECLARE @MANDATORY                 bit           = 0
	DECLARE @DEFAULTMULTIPLIER         real          = 1.0
	DECLARE @DEFAULTCONSTANT           real          = 0.0
	DECLARE @CONTROLLER_SENSOR_TYPE_ID int           = NULL
	DECLARE @DEFAULT_VALUE             bigint        = NULL
	DECLARE @MIN_VALUE                 bigint        = NULL
	DECLARE @MAX_VALUE                 bigint        = NULL
	DECLARE @BITS                      int           = NULL
	--------------------------------------------------------------------
	DECLARE CONTROLLER_TYPES CURSOR
	FORWARD_ONLY READ_ONLY STATIC LOCAL
	FOR
		SELECT DISTINCT
			[CONTROLLER_TYPE_NAME]
		FROM
		(
			VALUES
				 (N'Teltonika FMB920')
		) T([CONTROLLER_TYPE_NAME])
	OPEN CONTROLLER_TYPES;
	FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME;
	WHILE (0 = @@FETCH_STATUS)
	BEGIN
		DECLARE SENSORS CURSOR
		FORWARD_ONLY READ_ONLY STATIC LOCAL
		FOR
			SELECT
				 [NUMBER]            = CAST([NUMBER]            AS int)           -- COL01 - Номер в протоколе
				,[DESCRIPTION]       = CAST([DESCRIPTION]       AS nvarchar(512)) -- COL02 - Описание датчика
				,[LEGEND]            = CAST([LEGEND]            AS nvarchar(50))  -- COL03 - Легенда
				,[DIGITAL]           = CAST([DIGITAL]           AS bit)           -- COL04 - Цифровой? (Да - 1, Нет - 0)
				,[MANDATORY]         = CAST([MANDATORY]         AS bit)           -- COL05 - Обязательный? (Да - 1, Нет - 0) создается сразу для устройства этого типа
				,[DEFAULTMULTIPLIER] = CAST([DEFAULTMULTIPLIER] AS real)          -- COL06 - Множитель по умолчанию для вычисленного значения
				,[DEFAULTCONSTANT]   = CAST([DEFAULTCONSTANT]   AS real)          -- COL07 - Константа по умолчанию для вычисленного значения
				,[DEFAULT_VALUE]     = CAST([DEFAULT_VALUE]     AS bigint)        -- COL08 - Значение по умолчанию
				,[MIN_VALUE]         = CAST([MIN_VALUE]         AS bigint)        -- COL09 - Минимальное значение
				,[MAX_VALUE]         = CAST([MAX_VALUE]         AS bigint)        -- COL10 - Максимальное значение
				,[BITS]              = CAST([BITS]              AS int)           -- COL11 - Размер данных в битах
			FROM
			(
				VALUES
				-- COL01 | COL02                                  | COL03                            | COL04  | COL05  | COL06      | COL07  | COL08  | COL09     | COL10                    | COL11
				 (239, /*|*/ N'Ignition',                       /*|*/ N'Ignition',                 /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(240, /*|*/ N'Movement',                       /*|*/ N'Movement',                 /*|*/ 1, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(021, /*|*/ N'GSM Signal',                     /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   5, /*|*/  8)
				,(078, /*|*/ N'iButton',                        /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/ 9223372036854770000, /*|*/ 64)
				,(207, /*|*/ N'RFID',                           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/ 9223372036854770000, /*|*/ 64)
				,(182, /*|*/ N'GNSS HDOP',                      /*|*/ N'HDOP',                     /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 500, /*|*/ 16)
				,(066, /*|*/ N'External Voltage',               /*|*/ N'MainBattery',              /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(024, /*|*/ N'Speed',                          /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 350, /*|*/ 16)
				,(067, /*|*/ N'Battery Voltage',                /*|*/ N'PowerVoltage',             /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(001, /*|*/ N'Digital Input 1',                /*|*/ N'DigitalSensor1',           /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(002, /*|*/ N'Digital Input 2',                /*|*/ N'DigitalSensor2',           /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(003, /*|*/ N'Digital Input 3',                /*|*/ N'DigitalSensor3',           /*|*/ 1, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(009, /*|*/ N'Analog Input 1',                 /*|*/ N'AnalogSensor1',            /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(006, /*|*/ N'Analog Input 2',                 /*|*/ N'AnalogSensor2',            /*|*/ 0, /*|*/ 1, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(072, /*|*/ N'Dallas Temperature 1',           /*|*/ N'DigitalTemperatureSensor', /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/ -550, /*|*/                1150, /*|*/ 32)
				,(073, /*|*/ N'Dallas Temperature 2',           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/ -550, /*|*/                1150, /*|*/ 32)
				,(074, /*|*/ N'Dallas Temperature 3',           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/ -550, /*|*/                1150, /*|*/ 32)
				,(075, /*|*/ N'Dallas Temperature 4',           /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/ -550, /*|*/                1150, /*|*/ 32)
				,(201, /*|*/ N'LLS 1 Fuel Level',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/   -4, /*|*/               32767, /*|*/ 16)
				,(202, /*|*/ N'LLS 1 Temperature',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(203, /*|*/ N'LLS 2 Fuel Level',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/   -4, /*|*/               32767, /*|*/ 16)
				,(204, /*|*/ N'LLS 2 Temperature',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(210, /*|*/ N'LLS 3 Fuel Level',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/   -4, /*|*/               32767, /*|*/ 16)
				,(211, /*|*/ N'LLS 3 Temperature',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(212, /*|*/ N'LLS 4 Fuel Level',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/   -4, /*|*/               32767, /*|*/ 16)
				,(213, /*|*/ N'LLS 4 Temperature',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(214, /*|*/ N'LLS 5 Fuel Level',               /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/   -4, /*|*/               32767, /*|*/ 16)
				,(215, /*|*/ N'LLS 5 Temperature',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(015, /*|*/ N'Eco Score',                      /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.001, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(113, /*|*/ N'Battery Level',                  /*|*/ N'BatteryLevel',             /*|*/ 0, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(081, /*|*/ N'CAN Vehicle Speed',              /*|*/ N'CAN_Speed',                /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(082, /*|*/ N'CAN Accelerator Pedal Position', /*|*/ N'CAN_Accelerator',          /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 102, /*|*/  8)
				,(083, /*|*/ N'CAN Fuel Consumed',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          2147483647, /*|*/ 32)
				,(084, /*|*/ N'CAN Fuel level',                 /*|*/ N'CAN_FuelLevel1',           /*|*/ 0, /*|*/ 1, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(085, /*|*/ N'CAN Engine RPM',                 /*|*/ N'CAN_Revs',                 /*|*/ 0, /*|*/ 1, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               16384, /*|*/ 16)
				,(087, /*|*/ N'CAN Total Mileage',              /*|*/ N'CANTotalRun',              /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(089, /*|*/ N'CAN Fuel level',                 /*|*/ N'CANFuel',                  /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(102, /*|*/ N'CAN Engine Worktime',            /*|*/ N'CAN_DayRun',               /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/             1677215, /*|*/ 32)
				,(103, /*|*/ N'CAN Total engine work time',     /*|*/ N'CAN_EngHours',             /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/             1677215, /*|*/ 32)
				,(105, /*|*/ N'CAN Total Vehicle Mileage',      /*|*/ N'CAN_TotalRun',             /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          4294967295, /*|*/ 32)
				,(107, /*|*/ N'CAN Total Fuel Consumed',        /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/          2147483647, /*|*/ 32)
				,(110, /*|*/ N'CAN Fuel Rate',                  /*|*/ N'CAN_FuelRate',             /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(114, /*|*/ N'CAN Engine Load',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 130, /*|*/  8)
				,(115, /*|*/ N'CAN Engine Temperature',         /*|*/ N'CAN_CoolantT',             /*|*/ 0, /*|*/ 0, /*|*/ 0.100, /*|*/ 0, /*|*/ 0, /*|*/ -600, /*|*/                1270, /*|*/ 16)
				,(118, /*|*/ N'CAN Axle 1 Load',                /*|*/ N'CAN_AxleLoad1',            /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(119, /*|*/ N'CAN Axle 2 Load',                /*|*/ N'CAN_AxleLoad2',            /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(120, /*|*/ N'CAN Axle 3 Load',                /*|*/ N'CAN_AxleLoad3',            /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(121, /*|*/ N'CAN Axle 4 Load',                /*|*/ N'CAN_AxleLoad4',            /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(122, /*|*/ N'CAN Axle 5 Load',                /*|*/ N'CAN_AxleLoad5',            /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/ 16)
				,(255, /*|*/ N'Over Speeding',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32768, /*|*/  8)
				,(253, /*|*/ N'Green driving type',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   3, /*|*/  8)
				,(246, /*|*/ N'Towing',                         /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   2, /*|*/  8)
				,(247, /*|*/ N'Crash detection',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   5, /*|*/  8)
				,(249, /*|*/ N'Jamming',                        /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                   1, /*|*/  8)
				,(030, /*|*/ N'OBD Number of DTC',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(031, /*|*/ N'OBD Engine Load',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(032, /*|*/ N'OBD Coolant Temperature',        /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(034, /*|*/ N'OBD Fuel pressure',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 765, /*|*/ 16)
				,(036, /*|*/ N'OBD Engine RPM',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               16384, /*|*/ 16)
				,(037, /*|*/ N'OBD Vehicle Speed',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 255, /*|*/  8)
				,(042, /*|*/ N'OBD Runtime since engine start', /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(048, /*|*/ N'OBD Fuel Level',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(053, /*|*/ N'OBD Ambient Air Temperature',    /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/ -128, /*|*/                 127, /*|*/  8)
				,(060, /*|*/ N'OBD Fuel Rate',                  /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 0.010, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               32767, /*|*/ 16)
				-- Bluetooth sensors
				,(025, /*|*/ N'BLE Temperature #1',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               12050, /*|*/ 16)
				,(026, /*|*/ N'BLE Temperature #2',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               12050, /*|*/ 16)
				,(027, /*|*/ N'BLE Temperature #3',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               12050, /*|*/ 16)
				,(028, /*|*/ N'BLE Temperature #4',             /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               12050, /*|*/ 16)
				,(029, /*|*/ N'BLE Battery #1',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(020, /*|*/ N'BLE Battery #2',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(022, /*|*/ N'BLE Battery #3',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(023, /*|*/ N'BLE Battery #4',                 /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                 100, /*|*/  8)
				,(086, /*|*/ N'BLE Humidity #1',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                1000, /*|*/ 16)
				,(104, /*|*/ N'BLE Humidity #2',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                1000, /*|*/ 16)
				,(106, /*|*/ N'BLE Humidity #3',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                1000, /*|*/ 16)
				,(108, /*|*/ N'BLE Humidity #4',                /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/                1000, /*|*/ 16)
				,(335, /*|*/ N'BLE Luminosity #1',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(336, /*|*/ N'BLE Luminosity #2',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(337, /*|*/ N'BLE Luminosity #3',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(338, /*|*/ N'BLE Luminosity #4',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(270, /*|*/ N'BLE Fuel Level #1',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(273, /*|*/ N'BLE Fuel Level #2',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(276, /*|*/ N'BLE Fuel Level #3',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
				,(279, /*|*/ N'BLE Fuel Level #4',              /*|*/ NULL,                        /*|*/ 0, /*|*/ 0, /*|*/ 1.000, /*|*/ 0, /*|*/ 0, /*|*/    0, /*|*/               65535, /*|*/ 16)
			) S([NUMBER], [DESCRIPTION], [LEGEND], [DIGITAL], [MANDATORY], [DEFAULTMULTIPLIER], [DEFAULTCONSTANT], [DEFAULT_VALUE], [MIN_VALUE], [MAX_VALUE], [BITS])
		OPEN SENSORS
		FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		WHILE (@@FETCH_STATUS <> -1)
		BEGIN
			IF (@@FETCH_STATUS <> -2)
			BEGIN
				--/////////////////////////////////////////////////
				EXEC [dbo].[AddOrUpdateControllerSensor] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_digital, @LEGEND, 0/*@MANDATORY*/, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT
				SELECT
					@CONTROLLER_SENSOR_TYPE_ID = CASE @CONTROLLER_SENSOR_digital WHEN 1 THEN 2 ELSE 1 END,
					@MAX_VALUE                 = COALESCE(@MAX_VALUE, POWER(CAST(2 AS bigint), COALESCE(@BITS, 1) - 1))
				EXEC [dbo].[SetControllerSensorNumber] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_TYPE_ID, @DEFAULT_VALUE, @MAX_VALUE, @MIN_VALUE, @BITS
				--/////////////////////////////////////////////////
				DECLARE @CONTROLLER_TYPE_ID int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME);
				UPDATE [dbo].[CONTROLLER_SENSOR]
					SET
						 [Default_Multiplier] = @DEFAULTMULTIPLIER
						,[Default_Constant]   = @DEFAULTCONSTANT
						,[Mandatory]          = @MANDATORY
					--OUTPUT DELETED.*, INSERTED.*
				WHERE [CONTROLLER_TYPE_ID] = @CONTROLLER_TYPE_ID
				AND   [NUMBER]             = @NUMBER
				--/////////////////////////////////////////////////
			END
			FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
		END
		CLOSE      SENSORS;
		DEALLOCATE SENSORS;
		------------------------------------------------------------------
		SELECT
			CS.DESCRIPT, CS.NUMBER, CS.*
		FROM [dbo].[CONTROLLER_SENSOR] CS
			JOIN [dbo].[CONTROLLER_TYPE] CT
				ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
		WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
		------------------------------------------------------------------
		FETCH NEXT FROM CONTROLLER_TYPES INTO @CONTROLLER_TYPE_NAME;
	END
	CLOSE      CONTROLLER_TYPES;
	DEALLOCATE CONTROLLER_TYPES;
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO