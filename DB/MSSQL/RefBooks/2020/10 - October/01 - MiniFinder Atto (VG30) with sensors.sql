BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DECLARE @CONTROLLER_TYPE_NAME      nvarchar(50)  = N'MiniFinder Atto (VG30)'
	DECLARE @LEGEND                    nvarchar(50)  = NULL
	DECLARE @NUMBER                    int           = NULL
	DECLARE @DESCRIPTION               nvarchar(512) = NULL
	DECLARE @CONTROLLER_SENSOR_digital bit           = NULL
	DECLARE @MANDATORY                 bit           = 0
	DECLARE @DEFAULTMULTIPLIER         real          = 1.0
	DECLARE @DEFAULTCONSTANT           real          = 0.0
	DECLARE @CONTROLLER_SENSOR_TYPE_ID int           = NULL
	DECLARE @DEFAULT_VALUE             bigint        = NULL
	DECLARE @MIN_VALUE                 bigint        = NULL
	DECLARE @MAX_VALUE                 bigint        = NULL
	DECLARE @BITS                      int           = NULL
	--------------------------------------------------------------------
	--Добавляем контроллер
	EXEC [dbo].[AddControllerType]
		@type_name              = @CONTROLLER_TYPE_NAME,
		@DeviceIdIsImei         = 1,
		@SupportsPassword       = 0,
		@AllowedToAddByCustomer = 1,
		@ImagePath              = '~/img/devices/MiniFinder/MiniFinderAttoVG30.png'
	--------------------------------------------------------------------
/*
	DELETE [dbo].[CONTROLLER_SENSOR]
		OUTPUT DELETED.*
	FROM [dbo].[CONTROLLER_SENSOR] CS
		JOIN [dbo].[CONTROLLER_TYPE] CT
			ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
*/
	DECLARE SENSORS CURSOR LOCAL
	READ_ONLY
	FOR
		SELECT
			 [NUMBER]            = CAST([NUMBER]            AS int)           -- COL01 - Номер в протоколе
			,[DESCRIPTION]       = CAST([DESCRIPTION]       AS nvarchar(512)) -- COL02 - Описание датчика
			,[LEGEND]            = CAST([LEGEND]            AS nvarchar(50))  -- COL03 - Легенда
			,[DIGITAL]           = CAST([DIGITAL]           AS bit)           -- COL04 - Цифровой? (Да - 1, Нет - 0)
			,[MANDATORY]         = CAST([MANDATORY]         AS bit)           -- COL05 - Обязательный? (Да - 1, Нет - 0) создается сразу для устройства этого типа
			,[DEFAULTMULTIPLIER] = CAST([DEFAULTMULTIPLIER] AS real)          -- COL06 - Множитель по умолчанию для вычисленного значения
			,[DEFAULTCONSTANT]   = CAST([DEFAULTCONSTANT]   AS real)          -- COL07 - Константа по умолчанию для вычисленного значения
			,[DEFAULT_VALUE]     = CAST([DEFAULT_VALUE]     AS bigint)        -- COL08 - Значение по умолчанию
			,[MIN_VALUE]         = CAST([MIN_VALUE]         AS bigint)        -- COL09 - Минимальное значение
			,[MAX_VALUE]         = CAST([MAX_VALUE]         AS bigint)        -- COL10 - Максимальное значение
			,[BITS]              = CAST([BITS]              AS int)           -- COL11 - Размер данных в битах
		FROM
		(
			VALUES
				-- COL01 | COL02            | COL03                | COL04  | COL05  | COL06   | COL07    | COL08  | COL09            | COL10           | COL11
				 ( 01, /*|*/ N'Battery',  /*|*/ N'BatteryLevel', /*|*/ 0, /*|*/ 1, /*|*/ 1,  /*|*/ 0,   /*|*/ 0, /*|*/ -2147483648, /*|*/ 2147483647, /*|*/ 32)
				,( 02, /*|*/ N'SOS',      /*|*/ N'Alarm',        /*|*/ 1, /*|*/ 1, /*|*/ 1,  /*|*/ 0,   /*|*/ 0, /*|*/           0, /*|*/          1, /*|*/  1)
				,( 03, /*|*/ N'FallDown', /*|*/ N'FallDown',     /*|*/ 1, /*|*/ 1, /*|*/ 1,  /*|*/ 0,   /*|*/ 0, /*|*/           0, /*|*/          1, /*|*/  1)
				,( 04, /*|*/ N'Motion',   /*|*/ N'Motion',       /*|*/ 1, /*|*/ 1, /*|*/ 1,  /*|*/ 0,   /*|*/ 0, /*|*/           0, /*|*/          1, /*|*/  1)
				,( 05, /*|*/ N'Movement', /*|*/ N'Movement',     /*|*/ 1, /*|*/ 1, /*|*/ 1,  /*|*/ 0,   /*|*/ 0, /*|*/           0, /*|*/          1, /*|*/  1)
		) S([NUMBER], [DESCRIPTION], [LEGEND], [DIGITAL], [MANDATORY], [DEFAULTMULTIPLIER], [DEFAULTCONSTANT], [DEFAULT_VALUE], [MIN_VALUE], [MAX_VALUE], [BITS])
	OPEN SENSORS
	FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
	WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		IF (@@FETCH_STATUS <> -2)
		BEGIN
			--/////////////////////////////////////////////////
			EXEC [dbo].[AddOrUpdateControllerSensor] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_digital, @LEGEND, 0/*@MANDATORY*/, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT
			SELECT
				@CONTROLLER_SENSOR_TYPE_ID = CASE @CONTROLLER_SENSOR_digital WHEN 1 THEN 2 ELSE 1 END,
				@MAX_VALUE                 = COALESCE(@MAX_VALUE, POWER(CAST(2 AS bigint), COALESCE(@BITS, 1) - 1))
			EXEC [dbo].[SetControllerSensorNumber] @CONTROLLER_TYPE_NAME, @NUMBER, @DESCRIPTION, @CONTROLLER_SENSOR_TYPE_ID, @DEFAULT_VALUE, @MAX_VALUE, @MIN_VALUE, @BITS
			--/////////////////////////////////////////////////
			DECLARE @CONTROLLER_TYPE_ID int = (SELECT TOP(1) [CONTROLLER_TYPE_ID] FROM [dbo].[CONTROLLER_TYPE] WHERE [TYPE_NAME] = @CONTROLLER_TYPE_NAME);
			UPDATE [dbo].[CONTROLLER_SENSOR]
				SET
					 [Default_Multiplier] = @DEFAULTMULTIPLIER
					,[Default_Constant]   = @DEFAULTCONSTANT
					,[Mandatory]          = @MANDATORY
			WHERE [CONTROLLER_TYPE_ID] = @CONTROLLER_TYPE_ID
			AND   [NUMBER]             = @NUMBER
			--/////////////////////////////////////////////////
		END
		FETCH NEXT FROM SENSORS INTO @NUMBER, @DESCRIPTION, @LEGEND, @CONTROLLER_SENSOR_digital, @MANDATORY, @DEFAULTMULTIPLIER, @DEFAULTCONSTANT, @DEFAULT_VALUE, @MIN_VALUE, @MAX_VALUE, @BITS
	END
	------------------------------------------------------------------
	SELECT
		CS.DESCRIPT, CS.NUMBER, CS.*
	FROM [dbo].[CONTROLLER_SENSOR] CS
		JOIN [dbo].[CONTROLLER_TYPE] CT
			ON CT.[CONTROLLER_TYPE_ID] = CS.[CONTROLLER_TYPE_ID]
	WHERE CT.[TYPE_NAME] = @CONTROLLER_TYPE_NAME
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO