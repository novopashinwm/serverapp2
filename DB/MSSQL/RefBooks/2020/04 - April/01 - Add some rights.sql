BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	--Добавление прав или обновление прав пользователей
	MERGE [dbo].[RIGHT] AS DST
	USING
	(
		VALUES
		 (030, 1, N'AddingTracker',            N'Разрешение на добавление трекеров',                                                              NULL, NULL, NULL)
		,(031, 1, N'ViewingTrackerAttributes', N'Просмотр атрибутов трекера, разрешение редактировать атрибуты объекта на закладке Трекер',       NULL, NULL, NULL)
		,(032, 1, N'EditingTrackerAttributes', N'Редактирование атрибутов трекера, разрешение редактировать атрибуты объекта на закладке Трекер', NULL, NULL, NULL)
		,(114, 1, N'CommandAccess',            N'Право на выполнение команд помеченных данным правом',                                            NULL, NULL, NULL)

	) AS SRC ([RIGHT_ID], [SYSTEM], [NAME], [DESCRIPTION], [HELP_URL], [URL], [forOperator])
		ON  DST.[RIGHT_ID] = SRC.[RIGHT_ID]
	WHEN MATCHED THEN
		UPDATE
			SET
				 DST.[SYSTEM]      = SRC.[SYSTEM]
				,DST.[NAME]        = SRC.[NAME]
				,DST.[DESCRIPTION] = SRC.[DESCRIPTION]
				,DST.[HELP_URL]    = SRC.[HELP_URL]
				,DST.[URL]         = SRC.[URL]
				,DST.[forOperator] = SRC.[forOperator]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (    [RIGHT_ID],     [SYSTEM],     [NAME],     [DESCRIPTION],     [HELP_URL],     [URL],     [forOperator])
		VALUES (SRC.[RIGHT_ID], SRC.[SYSTEM], SRC.[NAME], SRC.[DESCRIPTION], SRC.[HELP_URL], SRC.[URL], SRC.[forOperator])
	OUTPUT $action, INSERTED.*, DELETED.*;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH