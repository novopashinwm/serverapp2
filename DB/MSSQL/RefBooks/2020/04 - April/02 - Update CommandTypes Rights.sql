﻿-- Обновление таблицы типов команд, с точки зрения прав на команды
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CommandTypes] AS DST
	USING
	(
		VALUES
			 (N'UDP',                   N'ChangeDeviceConfigBySMS')
			,(N'GSM',                   N'ChangeDeviceConfigBySMS')
			,(N'SMS',                   N'ChangeDeviceConfigBySMS')
			,(N'Monitor',               N'ChangeDeviceConfigBySMS')
			,(N'Trace',                 N'ChangeDeviceConfigBySMS')
			,(N'GetLog',                N'ChangeDeviceConfigBySMS')
			,(N'GetLogFromDB',          N'ChangeDeviceConfigBySMS')
			,(N'Control',               N'ChangeDeviceConfigBySMS')
			,(N'GetState',              N'ChangeDeviceConfigBySMS')
			,(N'SetState',              N'ChangeDeviceConfigBySMS')
			,(N'SendText',              N'ChangeDeviceConfigBySMS')
			,(N'Confirm',               N'ChangeDeviceConfigBySMS')
			,(N'SetFirmware',           N'ChangeDeviceConfigBySMS')
			,(N'SendFirmware',          N'ChangeDeviceConfigBySMS')
			,(N'GetSettings',           N'ChangeDeviceConfigBySMS')
			,(N'SetSettings',           N'ChangeDeviceConfigBySMS')
			,(N'Restart',               N'CommandAccess')
			,(N'RegisterMobileUnit',    N'ChangeDeviceConfigBySMS')
			,(N'AskPositionOverMLP',    N'CommandAccess')
			,(N'CutOffElectricity',     N'Immobilization')
			,(N'ReopenElectricity',     N'Immobilization')
			,(N'CutOffFuel',            N'Immobilization')
			,(N'ReopenFuel',            N'Immobilization')
			,(N'CancelAlarm',           N'CommandAccess')
			,(N'ChangeConfig',          N'ChangeDeviceConfigBySMS')
			,(N'ReloadDevice',          N'CommandAccess')
			,(N'VibrateRequest',        N'CommandAccess')
			,(N'Callback',              N'CommandAccess')
			,(N'ShutdownDevice',        N'CommandAccess')
			,(N'CapturePicture',        N'CommandAccess')
			,(N'Setup',                 N'ChangeDeviceConfigBySMS')
			,(N'Immobilize',            N'Immobilization')
			,(N'Deimmobilize',          N'Immobilization')
			,(N'SetModeOnline',         N'CommandAccess')
			,(N'SetModeWaiting',        N'CommandAccess')
			,(N'SetModeBeacon',         N'ChangeDeviceConfigBySMS')
			,(N'EnableMotionDetector',  N'CommandAccess')
			,(N'DisableMotionDetector', N'CommandAccess')
			,(N'QuickImmobilizeON',     N'Immobilization')
			,(N'QuickImmobilizeOFF',    N'Immobilization')
			,(N'SetAPN',                N'ChangeDeviceConfigBySMS')
			,(N'SetInterval',           N'CommandAccess')
			,(N'Status',                N'CommandAccess')
			,(N'SetSos',                N'CommandAccess')
			,(N'DeleteSOS',             N'CommandAccess')
			,(N'MonitorModeOn',         N'CommandAccess')
			,(N'MonitorModeOff',        N'CommandAccess')
			,(N'FindDevice',            N'CommandAccess')
			,(N'AddContact',            N'CommandAccess')
			,(N'DeleteContact',         N'CommandAccess')
			,(N'TurnLightOn',           N'CommandAccess')
			,(N'TurnLightOff',          N'CommandAccess')
			,(N'PowerAlarmOn',          N'CommandAccess')
			,(N'PowerAlarmOff',         N'CommandAccess')
			,(N'ShakeAlarmOn',          N'CommandAccess')
			,(N'ShakeAlarmOff',         N'CommandAccess')
	) AS SRC ([Cmd_Code], [Rht_Code])
		ON DST.[code] = SRC.[Cmd_Code]
	WHEN MATCHED AND ISNULL(DST.[Right_ID], 0) <> ISNULL((SELECT TOP(1) [Right_ID] FROM [dbo].[RIGHT] WHERE [NAME] = SRC.[Rht_Code]), 0) THEN
		UPDATE
			SET
				DST.[Right_ID] = (SELECT TOP(1) [Right_ID] FROM [dbo].[RIGHT] WHERE [NAME] = SRC.[Rht_Code])
	OUTPUT $action, INSERTED.[id], INSERTED.[code], INSERTED.[Right_ID], DELETED.[Right_ID]
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO