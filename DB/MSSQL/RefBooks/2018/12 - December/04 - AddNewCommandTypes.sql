DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;
-- <summary> Режим мгновенной блокировки </summary>
-- <remarks> Двигатель будет блокироваться мгновенно после подачи команды </remarks>
SELECT
	@id                 = 44,
	@code               = N'QuickImmobilizeON',
	@description        = N'Engine will be blocked immediately after command activation',
	@executionTimeout   = 60,
	@right_name         = N'Immobilization',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Режим отложенной блокировки </summary>
-- <remarks> Двигатель будет блокироваться после понижения скорости ТС </remarks>
SELECT
	@id                 = 45,
	@code               = N'QuickImmobilizeOFF',
	@description        = N'Engine will be blocked after speed decreasing',
	@executionTimeout   = 60,
	@right_name         = N'Immobilization',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Настроить APN </summary>
-- <remarks> Настраивает APN контроллера </remarks>
SELECT
	@id                 = 46,
	@code               = N'SetAPN',
	@description        = N'Set controller APN',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Настроить интервал передачи данных </summary>
-- <remarks> Устанавливает интервал передачи данных в секундах. Значение параметра секунд содержится в тексте команды </remarks>
SELECT
	@id                 = 47,
	@code               = N'SetInterval',
	@description        = N'Set data transmission interval',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Статус </summary>
-- <remarks> Получить статус контроллера </remarks>
SELECT
	@id                 = 48,
	@code               = N'Status',
	@description        = N'Get controller status',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds