DECLARE
	@id                 int,
	@code               nvarchar(200),
	@description        nvarchar(500),
	@executionTimeout   int,
	@right_name         nvarchar(255),
	@minIntervalSeconds int;
-- <summary> Установить номер SOS </summary>
-- <remarks> Установить номер для получения вызова SOS от контроллера </remarks>
SELECT
	@id                 = 49,
	@code               = N'SetSos',
	@description        = N'Set phone number to receive SOS call from controller',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Удалить номер SOS </summary>
-- <remarks> Удалить SOS номер контроллера </remarks>
SELECT
	@id                 = 50,
	@code               = N'DeleteSOS',
	@description        = N'Delete controller SOS number',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Включить режим мониторинга </summary>
-- <remarks> В режиме мониторинга появляется возможность позвонить на конроллер и услышать, что происходит вокруг </remarks>
SELECT
	@id                 = 51,
	@code               = N'MonitorModeOn',
	@description        = N'In monitoring mode you can call the controller and hear what is happening nearby',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Выключить режим мониторинга </summary>
-- <remarks> Выключает режим мониторинга </remarks>
SELECT
	@id                 = 52,
	@code               = N'MonitorModeOff',
	@description        = N'Switches off monitoring mode',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds
-- <summary> Найти контроллер </summary>
-- <remarks> Найти устройство </remarks>
SELECT
	@id                 = 53,
	@code               = N'FindDevice',
	@description        = N'Find the device',
	@executionTimeout   = 60,
	@right_name         = N'SecurityAdministration',
	@minIntervalSeconds = NULL
EXEC [dbo].[AddCommandType] @id, @code, @description, @executionTimeout, @right_name, @minIntervalSeconds