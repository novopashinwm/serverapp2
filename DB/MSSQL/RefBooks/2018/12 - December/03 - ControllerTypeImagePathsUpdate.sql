BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	UPDATE [dbo].[CONTROLLER_TYPE]
		SET [ImagePath] =
			CASE LEN([ImagePath])
				WHEN 0 THEN NULL
				ELSE REPLACE([ImagePath], '/about/includes/sources/images/', '~/img/')
				--ELSE REPLACE([ImagePath], '~/img/', '/about/includes/sources/images/')
			END
	OUTPUT
		DELETED.[CONTROLLER_TYPE_ID],
		DELETED.[TYPE_NAME],
		DELETED.[ImagePath],
		INSERTED.[CONTROLLER_TYPE_ID],
		INSERTED.[TYPE_NAME],
		INSERTED.[ImagePath]
	WHERE [ImagePath] IS NOT NULL
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH