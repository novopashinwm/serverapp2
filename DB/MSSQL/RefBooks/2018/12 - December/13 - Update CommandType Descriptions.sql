﻿-- Обновление таблицы типов команд
BEGIN TRY
	BEGIN TRAN
	MERGE [dbo].[CommandTypes] AS DST
	USING
	(
		VALUES
			 (N'Setup',              N'Sets basic controller settings ')
			,(N'Immobilize',         N'Lock the vehicle engine')
			,(N'Deimmobilize',       N'Unlock the vehicle  engine')
			,(N'QuickImmobilizeON',  N'Engine will be blocked immediately after command activation')
			,(N'QuickImmobilizeOFF', N'Engine will be blocked after speed decreasing')
			,(N'SetAPN',             N'Set controller APN')
			,(N'SetInterval',        N'Sets data transmission interval in seconds')
			,(N'Status',             N'Get controller status')
			,(N'SetSos',             N'Set phone number to receive SOS call from controller')
			,(N'DeleteSOS',          N'Delete controller SOS number')
			,(N'AskPosition',        N'One-time position determining via cellular network data and GPS data')
			,(N'ReloadDevice',       N'Controller reloading')
			,(N'ShutdownDevice',     N'Completely turns of the controller')
			,(N'SetModeOnline',      N'Enables online data transmission')
			,(N'SetModeWaiting',     N'Enables power saving mode')
			,(N'CallPhone',          N'Controller will call to the phone number and your will be able to hear what is happening nearby')
			,(N'CancelAlarm',        N'Cancel alarm status on the controller')
			,(N'MonitorModeOn',      N'In monitoring mode you can call the controller and hear what is happening nearby')
			,(N'MonitorModeOff',     N'Switches off monitoring mode')
			,(N'FindDevice',         N'Find the device')
	) AS SRC ([code], [description])
		ON DST.[code] = SRC.[code]
	WHEN MATCHED AND DST.[description] <> SRC.[description] THEN
		UPDATE
			SET
				DST.[description] = SRC.[description]
	OUTPUT $action, INSERTED.*, DELETED.*
	;
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH
GO