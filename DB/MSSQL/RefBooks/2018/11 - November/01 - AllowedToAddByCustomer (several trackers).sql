﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	UPDATE T
		SET T.[AllowedToAddByCustomer] = 1
	OUTPUT INSERTED.*, DELETED.*
	FROM CONTROLLER_TYPE T
	WHERE T.[TYPE_NAME] IN
	(
		N'AvtoGraf',
		N'AvtoGrafCan',
		N'Teltonika FM1100',
		N'Teltonika FM1110',
		N'Teltonika FM1125',
		N'Teltonika FM1200',
		N'Teltonika FM2200',
		N'Teltonika FM5300',
		N'Teltonika GH3000'
	)
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SELECT
		ERROR_NUMBER()    AS ErrorNumber,
		ERROR_SEVERITY()  AS ErrorSeverity,
		ERROR_STATE()     AS ErrorState,
		ERROR_PROCEDURE() AS ErrorProcedure,
		ERROR_LINE()      AS ErrorLine,
		ERROR_MESSAGE()   AS ErrorMessage;
END CATCH