﻿exec AddControllerType 'Teltonika FM1125', 1, 0, 1

exec AddOrUpdateControllerSensor  'Teltonika FM1125',   1, 'Digital Input Status 1'					, 1
exec AddOrUpdateControllerSensor  'Teltonika FM1125',   9, 'Analog Input 1'                         , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  21, 'GSM level'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  24, 'Speed'                                  , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  66, 'External Power Voltage'                 , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  67, 'Battery Voltage'                        , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  68, 'Battery Charging Current'               , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  69, 'GPS Status'                             , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  72, 'Dallas Temperature'                     , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  78, 'iButton ID'                             , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  80, 'Data Mode'                              , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 179, 'Digital Output 1 state'                 , 1, 'DigitalOutput1', 1, 1, 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 181, 'PDOP'                                   , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 182, 'HDOP'                                   , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 199, 'Odometer Value (Virtual Odometer)'      , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 200, 'Deep Sleep'                             , 1
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 205, 'Cell ID'                                , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 206, 'Area Code'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 239, 'Ignition'                               , 1, 'Зажигание', 1, 1, 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 240, 'Movement Sensor'                        , 1, 'IsMoving', 1, 1, 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 241, 'GSM Operator Code'                      , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 207, 'RFID'                                   , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 201, 'LLS Fuel1'                              , 0, 'Датчик топлива', 1, 1, 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 202, 'LLS Temp1'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 203, 'LLS Fuel2'                              , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 204, 'LLS Temp2'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 210, 'LLS Fuel3'                              , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 211, 'LLS Temp3'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 212, 'LLS Fuel4'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 213, 'LLS Temp4'                              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 214, 'LLS Fuel5'                              , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 215, 'LLS Temp5'                              , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  81, 'LVCAN Speed'                            , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  82, 'LVCAN Accelerator Pedal Position'       , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  83, 'LVCAN Total Fuel Used'                  , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125',  84, 'LVCAN Fuel Level (liters)'              , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  85, 'LVCAN Engine RPM'                       , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  87, 'LVCAN Vehicle Distance'                 , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125',  89, 'LVCAN Fuel Level (percentage)'          , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 100, 'LVCAN Program Number'                   , 0
exec AddOrUpdateControllerSensor  'Teltonika FM1125', 253, 'Green driving type'                     , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 254, 'Green driving value'                    , 0exec AddOrUpdateControllerSensor  'Teltonika FM1125', 255, 'Over Speeding'                          , 0