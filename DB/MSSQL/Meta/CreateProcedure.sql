if exists (select * from sys.procedures where name = 'CreateProcedure')
	drop procedure CreateProcedure
go

create procedure CreateProcedure
(
	@name varchar(255)
)
as

declare @sql nvarchar(255) = 'create procedure ' + @name + '
(
	@dummy int
)
as

print ''Procedure ' + @name + ' has not been defined yet''

';

if not exists (select * from sys.procedures where name = @name)
	exec sp_executesql @sql