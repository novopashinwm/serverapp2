if exists (select * from sys.procedures where name = 'CreateFunction')
	drop procedure CreateFunction
go

create procedure CreateFunction
(
	@name varchar(255),
	@returns varchar(255)
)
as

declare @sql nvarchar(255) = 'create function ' + @name + '
(
	@dummy int
)
returns ' + @returns + '
as
begin
return null
end

';

if not exists (select * from sys.objects where name = @name and type = 'FN')
	exec sp_executesql @sql