if exists (select * from sys.procedures where name = 'CreateView')
	drop procedure CreateView
go

create procedure CreateView
(
	@name varchar(255)
)
as

if exists (select * from sys.views where name = @name)
	return;

declare @sql nvarchar(max) = 
	'create view ' + @name + ' as select [null] = null '

exec sp_executesql @sql