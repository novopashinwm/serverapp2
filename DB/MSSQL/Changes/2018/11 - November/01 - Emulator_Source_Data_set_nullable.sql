﻿IF EXISTS (SELECT * FROM sys.columns WHERE [object_id] = OBJECT_ID('[dbo].[Emulator_Source]') AND [name] = 'Data' AND [is_nullable] = 0)
	ALTER TABLE [dbo].[Emulator_Source] ALTER COLUMN [Data] xml NULL;
GO