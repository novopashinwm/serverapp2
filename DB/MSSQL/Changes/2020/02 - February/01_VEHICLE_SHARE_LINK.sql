﻿IF (OBJECT_ID(N'[dbo].[VEHICLE_SHARE_LINK]') IS NULL)
BEGIN
	PRINT N'Создание таблицы [dbo].[VEHICLE_SHARE_LINK]';
	CREATE TABLE [dbo].[VEHICLE_SHARE_LINK]
	(
		[VEHICLE_SHARE_LINK_ID]         int              NOT NULL IDENTITY (1, 1),
		[VEHICLE_SHARE_LINK_NAME]       nvarchar(255)        NULL,
		[VEHICLE_SHARE_LINK_DESC]       nvarchar(500)        NULL,
		[VEHICLE_SHARE_LINK_GUID]       uniqueidentifier NOT NULL CONSTRAINT [DF_VEHICLE_SHARE_LINK_GUID] DEFAULT (NEWID()),
		[VEHICLE_SHARE_LINK_BEG]        datetime         NOT NULL CONSTRAINT [DF_VEHICLE_SHARE_LINK_BEG]  DEFAULT (GETUTCDATE()),
		[VEHICLE_SHARE_LINK_END]        datetime         NOT NULL CONSTRAINT [DF_VEHICLE_SHARE_LINK_END]  DEFAULT (CAST('2999-12-31' AS datetime)),
		[VEHICLE_SHARE_LINK_CREATOR_ID] int              NOT NULL,
		[VEHICLE_SHARE_LINK_VEHICLE_ID] int              NOT NULL,
		CONSTRAINT [PK_VEHICLE_SHARE_LINK]  PRIMARY KEY CLUSTERED ([VEHICLE_SHARE_LINK_ID]),
		CONSTRAINT [AK_VEHICLE_SHARE_LINK1] UNIQUE   NONCLUSTERED ([VEHICLE_SHARE_LINK_GUID]),
		CONSTRAINT [AK_VEHICLE_SHARE_LINK2] UNIQUE   NONCLUSTERED ([VEHICLE_SHARE_LINK_CREATOR_ID], [VEHICLE_SHARE_LINK_VEHICLE_ID], [VEHICLE_SHARE_LINK_BEG], [VEHICLE_SHARE_LINK_END], [VEHICLE_SHARE_LINK_NAME]),
		CONSTRAINT [FK_VEHICLE_SHARE_LINK_VEHICLE]  FOREIGN KEY   ([VEHICLE_SHARE_LINK_VEHICLE_ID])
			REFERENCES [dbo].[VEHICLE] ([VEHICLE_ID]),
		CONSTRAINT [FK_VEHICLE_SHARE_LINK_OPERATOR] FOREIGN KEY   ([VEHICLE_SHARE_LINK_CREATOR_ID])
			REFERENCES [dbo].[OPERATOR] ([OPERATOR_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица общих ссылок на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ общей ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Наименование объекта отображаемого по общей ссылке на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_NAME';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Описание объекта отображаемого по общей ссылке на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_DESC';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Уникальный идентификатор общей ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_GUID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'UTC момент времени начала действия общей ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_BEG';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'UTC момент времени окончания действия общей ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_END';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу операторов общей ссылки на объект, создателя ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_CREATOR_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу объектов общей ссылки на объект',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'VEHICLE_SHARE_LINK',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_SHARE_LINK_VEHICLE_ID';
END
GO
IF (OBJECT_ID(N'[dbo].[H_VEHICLE_SHARE_LINK]') IS NULL)
BEGIN
	PRINT N'Создание исторической таблицы для [dbo].[VEHICLE_SHARE_LINK]';
	CREATE TABLE [dbo].[H_VEHICLE_SHARE_LINK]
	(
		[VEHICLE_SHARE_LINK_ID]         int               NOT NULL,
		[VEHICLE_SHARE_LINK_NAME]       nvarchar(255)         NULL,
		[VEHICLE_SHARE_LINK_DESC]       nvarchar(500)         NULL,
		[VEHICLE_SHARE_LINK_GUID]       uniqueidentifier  NOT NULL,
		[VEHICLE_SHARE_LINK_BEG]        datetime          NOT NULL,
		[VEHICLE_SHARE_LINK_END]        datetime          NOT NULL,
		[VEHICLE_SHARE_LINK_CREATOR_ID] int               NOT NULL,
		[VEHICLE_SHARE_LINK_VEHICLE_ID] int               NOT NULL,
		[TRAIL_ID]                      int               NOT NULL,
		[ACTION]                        nvarchar(6)       NOT NULL,
		[ACTUAL_TIME]                   datetime          NOT NULL,
		[ID]                            int IDENTITY(1,1) NOT NULL,
		CONSTRAINT [PK_H_VEHICLE_SHARE_LINK]
			PRIMARY KEY CLUSTERED ([ID] ASC),
		CONSTRAINT [FK_H_VEHICLE_SHARE_LINK_TRAIL_ID]
			FOREIGN KEY ([TRAIL_ID]) REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
	);
END
GO