﻿IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_AppClient_Contact_ID_AppId')
	CREATE INDEX [IX_AppClient_Contact_ID_AppId] ON [dbo].[AppClient]
		([Contact_ID],[AppId])
GO