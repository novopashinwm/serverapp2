if not exists (
	select * from sys.columns where object_id = object_id('Vehicle_RemoteTerminalServer') and name = 'Enabled'
)
begin

	alter table Vehicle_RemoteTerminalServer 
		add Enabled bit not null
			constraint DF_RemoteTerminalServer_Enabled
				default (1)

	alter table H_Vehicle_RemoteTerminalServer
		add Enabled bit not null
			constraint DF_H_RemoteTerminalServer_Enabled
				default (1)

end