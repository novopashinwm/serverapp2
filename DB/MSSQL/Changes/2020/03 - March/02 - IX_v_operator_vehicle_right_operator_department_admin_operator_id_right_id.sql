﻿IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_department_admin_operator_id_right_id')
	CREATE INDEX [IX_v_operator_vehicle_right_operator_department_admin_operator_id_right_id] ON [dbo].[v_operator_vehicle_right_operator_department_admin]
	([operator_id], [right_id])
GO