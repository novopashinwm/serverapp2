﻿IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_Statistic_Log_Recalc_Log_Time')
	CREATE INDEX [IX_Statistic_Log_Recalc_Log_Time] ON [dbo].[Statistic_Log_Recalc]
	([Log_Time])
	INCLUDE
	([Pending], [Pending_date])
GO