IF (EXISTS(SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'[dbo].[Log_Time]', N'U') AND [name] = N'IX_Log_Time_InsertTime'))
	EXEC sp_rename 'Log_Time.IX_Log_Time_InsertTime', 'IX_Vehicle_ID_InsertTime', 'INDEX';
GO