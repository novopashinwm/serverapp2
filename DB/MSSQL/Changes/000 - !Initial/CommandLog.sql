/*	��������� � ����� � ������ ��� ����������� ������ � �������� ������� � �� ����������.*/

if (not exists( select 1 from sys.objects where name = 'Command' and type = 'u'))
begin
	create table dbo.Command (
		ID				int identity constraint PK_Command primary key,
		Sender_ID		int constraint FK_Command_Sender_ID foreign key references dbo.operator(operator_id),
		Target_ID   	int constraint FK_Command_Target_ID foreign key references dbo.vehicle(vehicle_id),
		Type_ID			int,
		Date_Received	datetime
	);

	create table dbo.Command_Parameter (
		ID				int identity constraint PK_Command_Parameter primary key,
		Command_ID		int constraint FK_Command_Parameter_Command_ID foreign key references dbo.Command(ID),
		[Key]			nvarchar(30),
		[Value]			nvarchar(255)
	);

	create table dbo.Command_Result (
		ID				int identity constraint PK_Command_Result primary key,
		Command_ID		int constraint FK_Command_Result_Command_ID foreign key references dbo.Command(ID),
		Date_Received	datetime,
		Result_Type_ID  int,
		Log_Time_ID		bigint,
		Processed		bit
	);
	
	/*
		drop table dbo.H_Command_Result
		drop table dbo.Command_Result
		drop table dbo.Command_Parameter
		drop table dbo.Command
	*/
end;