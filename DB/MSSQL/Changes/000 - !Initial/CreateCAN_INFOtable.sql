/****** Object:  Table [dbo].[CAN_INFO]    Script Date: 09/02/2009 08:43:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CAN_INFO]') AND type in (N'U'))
DROP TABLE [dbo].[CAN_INFO]

/****** Object:  Table [dbo].[CAN_INFO]    Script Date: 09/02/2009 08:43:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAN_INFO](
	[MONITOREE_ID] int NOT NULL,
	[LOG_TIME] int NOT NULL,
	[CAN_TIME] int NOT NULL,
    [SPEED] tinyint NULL,
	[CRUISE_CONTROL] bit NULL,
	[BRAKE] bit NULL,
	[PARKING_BRAKE] bit NULL,
	[CLUTCH] bit NULL,
	[ACCELERATOR] tinyint NULL,
	[FUEL_RATE] int NULL,
	[FUEL_LEVEL1] smallint NULL,
	[FUEL_LEVEL2] smallint NULL,
	[FUEL_LEVEL3] smallint NULL,
	[FUEL_LEVEL4] smallint NULL,
	[FUEL_LEVEL5] smallint NULL,
	[FUEL_LEVEL6] smallint NULL,
	[REVS] tinyint NULL,
	[RUN_TO_MNTNC] smallint NULL,
	[ENG_HOURS] int NULL,
	[COOLANT_T] smallint NULL,
	[ENG_OIL_T] smallint NULL,
	[FUEL_T] smallint NULL,
	[TOTAL_RUN] int NULL,
	[DAY_RUN] smallint NULL,
	[AXLE_LOAD1] int NULL,
	[AXLE_LOAD2] int NULL,
	[AXLE_LOAD3] int NULL,
	[AXLE_LOAD4] int NULL,
	[AXLE_LOAD5] int NULL,
	[AXLE_LOAD6] int NULL,
	CONSTRAINT [PK_CAN_INFO] PRIMARY KEY CLUSTERED 
	(	[LOG_TIME] ASC,
		[MONITOREE_ID] ASC
	)WITH (IGNORE_DUP_KEY = OFF) ON [TSS_DATA2]
) ON [TSS_DATA2]
GO