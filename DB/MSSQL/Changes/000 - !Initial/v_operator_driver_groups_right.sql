IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_driver_groups_right]'))
DROP VIEW [dbo].[v_operator_driver_groups_right]
GO

/*
select * from [v_operator_driver_groups_right]
where operator_id = 186
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_operator_driver_groups_right] 
as
	WITH allRights(operator_id, drivergroup_id, right_id, allowed, priority) AS 
	(
		select o_vg.operator_id, o_vg.drivergroup_id, o_vg.right_id, allowed, 2 'priority'
			from dbo.operator_drivergroup o_vg
		union all
		select og_o.operator_id, og_vg.drivergroup_id, og_vg.right_id, allowed, 1 'priority'
			from dbo.OPERATORGROUP_driverGROUP og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
	)
	select distinct operator_id, drivergroup_id, right_id
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.drivergroup_id = r1.drivergroup_id
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority > r1.priority)