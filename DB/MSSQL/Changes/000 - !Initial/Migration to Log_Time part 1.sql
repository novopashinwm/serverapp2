--1. ������� ������� Log_time (ID (bigint identity, vehicle_id, log_time, media, insertTime timeStamp)) cluster index (vehicle_id, log_time)
/*
alter table dbo.Monitoree_Log
	drop constraint FK_Geo_Log_Log_Time_ID
drop table dbo.Log_Time
alter table dbo.Monitoree_Log
	drop column Log_Time_ID
drop table dbo.GPS_Log
*/
create table dbo.Log_Time (
	ID				bigint identity constraint PK_Log_Time primary key nonclustered on TSS_Data2,
	Vehicle_ID		int,
	Log_Time		int, 
	Media			tinyint constraint FK_Log_Time_Media foreign key (Media) references dbo.Media_Type(ID),
	InsertTime		datetime
) on TSS_Data2;

create clustered index IX_Log_Time_VID_LT on dbo.Log_Time (vehicle_id, log_time) on TSS_Data2;

alter table dbo.Monitoree_Log 
	add 
		Log_Time_ID		bigint,
		Lng				numeric(8,5),
		Lat				numeric(8,5);
	
--��������� ������ ��� ������ ��������� [MigrateLogData]
create nonclustered index IX_Monitoree_Log_LTID_LT on dbo.Monitoree_Log (Log_Time_ID, Log_Time) on TSS_Data2; --��������������� �����: 00:05:47

--1.1 ������� ������� gps_log. Log_time_id, satellites, firmware, altitude, speed
create table dbo.GPS_Log (
	Log_Time_ID		bigint not null constraint PK_GPS_Log primary key nonclustered on TSS_Data2,
	Vehicle_ID		int,
	Log_Time		int, 
	Satellites		tinyint, 
	Firmware		smallint, 
	Altitude		int, 
	Speed			tinyint) on TSS_Data2;

create clustered index IX_GPS_Log_VID_LT on dbo.GPS_Log(Vehicle_ID, Log_Time) on TSS_Data2;

if exists (select 1 from sys.objects where name = 'position_radius' and type = 'u') 
	drop table dbo.position_radius;
	
create table dbo.Position_Radius 
(
    Log_Time_ID	bigint not null constraint PK_Position_Radius primary key nonclustered on TSS_Data2,
	monitoree_ID	int,
	Log_Time		int, 
	Radius			int
) on TSS_DATA2;

--create clustered index IX_Position_Radius_VID_LT on dbo.Position_Radius(Vehicle_ID, Log_Time) on TSS_Data2;

create table dbo.Asid (
	ID int identity constraint PK_Asid primary key clustered,
	Value varchar (32),
	AllowMlpRequest bit,
	AllowSecurityAdministration bit,
	WarnAboutLocation bit
);

create nonclustered index IX_Asid_Value on dbo.Asid(Value);

if exists (select 1 from sys.objects where name = 'mlp_controller' and type = 'u') 
	drop table dbo.mlp_controller
	
create table dbo.MLP_Controller 
(
	  ID			int identity primary key nonclustered
	, Controller_ID	int constraint PK_MLP_Controller foreign key references dbo.Controller(controller_id) 
	, Asid_ID		int constraint FK_MLP_Controller_Asid_ID foreign key references dbo.Asid(ID)
);

alter table dbo.Controller_Stat 
	add Log_Time_ID		bigint,
		Vehicle_ID		int,
		Log_Time		int, 
		Odometer		int;

alter table dbo.Can_Info
	add Log_Time_ID bigint;

create nonclustered index IX_Can_Info_Log_Time_ID on dbo.Can_Info (Log_Time_ID) on TSS_Data2;
/*
create table dbo.Statistic_Log 
(
	Log_Time_ID		bigint null, --not null constraint PK_Statistic_Log primary key nonclustered on TSS_Data2,
	Vehicle_ID		int,
	Log_Time		int,
	Odometer		bigint
) on TSS_Data2;

create clustered index IX_Statistic_Log_VID_LT on dbo.Statistic_Log(Vehicle_ID, Log_Time) on TSS_Data2;
*/
create index IX_Statistic_Log_LT_ID on dbo.Statistic_Log(Log_Time_ID) on TSS_Data2;

insert into dbo.Monitoree_Log_Ignored (MONITOREE_ID,LOG_TIME,X,Y,SPEED,MEDIA,V,V1,V2,V3,V4,SATELLITES,FIRMWARE,ODOMETER,HEIGHT)
	select ml.MONITOREE_ID, ml.LOG_TIME, ml.X, ml.Y, ml.SPEED, ml.MEDIA, ml.V, ml.V1, ml.V2, ml.V3, ml.V4, ml.SATELLITES, ml.FIRMWARE, ml.ODOMETER, ml.HEIGHT
		from dbo.Monitoree_Log ml
		left outer join dbo.Monitoree_Log_Ignored mli on mli.Monitoree_ID = ml.Monitoree_ID
													 and mli.Log_Time = ml.Log_Time
		where 
			(     ml.X not between -999.99999 and 999.99999
			   or ml.Y not between -999.99999 and 999.99999)
			and not exists (select 1 from dbo.Monitoree_Log_Ignored mli 
							where mli.Monitoree_ID = ml.Monitoree_ID
							  and mli.Log_Time = ml.Log_Time)
--�������� 3:01

delete from Monitoree_Log 
	where X not between -999.99999 and 999.99999
	   or Y not between -999.99999 and 999.99999
--�������� 10:51


--������� ��������� ������� �� monitoree_log
select log_time, Monitoree_ID, count(*) as num
into #dups
from monitoree_log ml (nolock)
group by log_time, Monitoree_ID
having count(*) > 1
---order by count(*) desc

select ml.MONITOREE_ID, ml.LOG_TIME, 
min(ml.MEDIA) MEDIA, min(X) X, min(Y) Y, min(SPEED) SPEED ,min(V) V, min(V1) V1, min(V2) V2, min(V3) V3,
min(V4) V4, min(SATELLITES) SATELLITES, min(FIRMWARE) FIRMWARE, min(ODOMETER) ODOMETER, min(HEIGHT) HEIGHT
into #nodups
from monitoree_log ml
join #dups d on d.log_time = ml.log_time 
and d.Monitoree_ID = ml.monitoree_id
group by ml.MONITOREE_ID, ml.LOG_TIME

delete from monitoree_log
from monitoree_log ml
join #dups d on d.log_time = ml.log_time 
and d.Monitoree_ID = ml.monitoree_id

insert into monitoree_log (MONITOREE_ID, LOG_TIME, X, Y, SPEED, MEDIA, V, V1, V2, V3, V4, SATELLITES, FIRMWARE, ODOMETER, HEIGHT)
select MONITOREE_ID, LOG_TIME, X, Y, SPEED, MEDIA, V, V1, V2, V3, V4, SATELLITES, FIRMWARE, ODOMETER, HEIGHT
from #nodups

drop table #nodups
drop table #dups