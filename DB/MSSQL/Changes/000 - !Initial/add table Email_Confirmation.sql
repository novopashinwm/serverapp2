if (not exists (select 1 from sys.objects where name='Email_Confirmation' and type='u'))
begin
	create table dbo.Email_Confirmation 
	(
		ID				int identity constraint PK_Email_Confirmation primary key clustered,
		Email_ID		int constraint FK_Email foreign key references dbo.Email(Email_ID),
		Creation_Date	datetime,
		[Key]			varchar(255)
	);
	
	create nonclustered index IX_Email_Confirmation_Creation_Date on dbo.Email_Confirmation (Creation_Date)

	create nonclustered index IX_Email_Confirmation_Key on dbo.Email_Confirmation ([Key])

end;

--������ ����� ���� ������: � ����� ������ �������� �� ������ ����� � ���� ������� �� ���� �����/������
--������ ��� ���� �������� ��� ��� ����� ����� ��� ����� ������� �������, ��������� ������� �������������� ������ �� email
alter table dbo.Operator
	alter column Password nvarchar(50) null

alter table dbo.H_Operator
	alter column Password nvarchar(50) null

alter table dbo.Controller
	alter column Phone varchar(20) null

alter table dbo.H_Controller
	alter column Phone varchar(20) null

alter table dbo.Email
	add Operator_ID int constraint FK_Email_Operator_ID foreign key references dbo.Operator(Operator_ID)

alter table dbo.H_Email
	add Operator_ID int

alter table dbo.Email
	add constraint UQ_Email_Email unique (Email)

insert into dbo.Email (Email, Name, Comment, Operator_ID)
	select o.Email, o.Name, '��������� �������������', o.Operator_ID
		from dbo.Operator o
		where not exists (select 1 from dbo.Email e where e.Email = o.Email)
		  and o.Email is not null
		  and o.Email <> ''
		  and o.Operator_ID = (select max(o1.Operator_ID) from dbo.Operator o1 where o1.Email = o.Email)
	
