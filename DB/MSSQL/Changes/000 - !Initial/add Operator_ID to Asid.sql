/*	���������� ������� Operator_ID � ������� Asid � �������� ������ */

alter table dbo.Asid
	add Operator_ID int constraint FK_Operator_ID foreign key references dbo.Operator(Operator_ID);
	
go
	
	
declare @rightID int;
set @rightID = 106; --(select right_id from dbo.[Right] where [name] = 'AsidSecurityAdministration');
--��-�� ���������������� SystemRight � dbo.Right �������� ����������� � right_id = 106 � �������� asidSecurityAdministration


update a
	set Operator_ID = ov.Operator_ID
	from dbo.Asid a
	join dbo.MLP_Controller mlpc on mlpc.Asid_ID = a.ID
	join dbo.Controller c on c.Controller_ID = mlpc.Controller_ID
	join dbo.Operator_Vehicle ov on ov.Vehicle_id = c.Vehicle_id								
	where ov.Right_ID = @rightID;
		
delete from dbo.Operator_Vehicle
	where Right_ID = @rightID;

delete from dbo.[Right]
	where [Name] = 'AsidSecurityAdministration';