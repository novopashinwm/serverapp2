if (not exists (select 1 from sys.columns where name = 'InsertTime' and object_id = object_id('monitoree_log_ignored')))
begin
	alter table dbo.Monitoree_Log_Ignored
		add InsertTime datetime;
end;