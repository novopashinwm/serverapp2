/* �������, � ������� ���������� ���������� �� ������� �������� � ����������� ����� */

if not exists (select 1 from sys.objects where name = 'Tracker' and type = 'u') 
begin
	create table dbo.Tracker 
	(
		/*ROW_ID*/
		ID int identity constraint PK_Tracker primary key clustered,
		/*��� �����������*/
		Controller_Type_ID int constraint FK_Tracker_Controller_Type_ID foreign key references dbo.Controller_Type(Controller_Type_ID),
		/*����� �����������, ���������� � ������ �������������.
		����������� ���� ������������ ����������� Controller_Info.Device_ID*/
		Number varchar(32),
		/*����������� ��� ��� ����������� ����������� � �������*/
		Control_Code varchar(10)
	);

	create nonclustered index IX_Tracker_Number
		on dbo.Tracker (Number);
	
end;