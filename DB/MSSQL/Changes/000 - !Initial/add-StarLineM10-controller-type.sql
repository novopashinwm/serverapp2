/*��������� ��� ����������� "����" StarLine M10/M11*/

insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
	select 'StarLineM10', 200, 0, 0
	where not exists (select 1 from dbo.Controller_Type where Type_Name = 'StarLineM10')
	
declare @controller_type_id int

set @controller_type_id = (select controller_type_id from dbo.Controller_Type where Type_Name = 'StarLineM10')

create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '��������'   CSTName, 0 Number, Descript = 'Alarm'	
		union select '����������' CSTName, 1 Number, Descript = 'BatteryLevel'
		union select '��������'   CSTName, 2 Number, Descript = 'ChannelEnabled'						    
		union select '����������' CSTName, 3 Number, Descript = 'ChannelWorkTime'					
		union select '��������'   CSTName, 4 Number, Descript = 'ChannelWorkTimeInfinite'							
		union select '����������' CSTName, 5 Number, Descript = 'GprsPacketTransmissionInterval'						
		union select '��������'	  CSTName, 6 Number, Descript = 'OuterPower'							
		union select '����������' CSTName, 7 Number, Descript = 'Temperature'	
		union select '����������' CSTName, 8 Number, Descript = 'WakeUpInterval'	
		union select '����������' CSTName, 9 Number, Descript = 'WorkMode'	
		) t
	where ct.Type_Name = 'StarLineM10'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)


drop table #Controller_Sensor