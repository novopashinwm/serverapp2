alter table dbo.Vehicle
	drop constraint CK_Fuel
GO

alter table dbo.Vehicle
	add constraint CK_Fuel CHECK (Fuel_Tank is null or Fuel_Tank >= 0);
go	