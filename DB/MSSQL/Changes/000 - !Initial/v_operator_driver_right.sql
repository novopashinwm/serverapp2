/****** Object:  View [dbo].[v_operator_vehicle_right]    Script Date: 07/08/2010 18:12:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_driver_right]'))
DROP VIEW [dbo].[v_operator_driver_right]
GO



/****** Object:  View [dbo].[v_operator_driver_right]    Script Date: 07/08/2010 18:11:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[v_operator_driver_right] 
as
	WITH allRights(operator_id, driver_id, right_id, allowed, priority) AS 
	(
		select operator_id, driver_id, right_id, allowed, 4 'priority'
			from dbo.operator_driver 
		union all
		select o_vg.operator_id, vg_v.driver_id, o_vg.right_id, allowed, 3 'priority'
			from dbo.operator_drivergroup o_vg
			join dbo.driverGROUP_driver vg_v on vg_v.drivergroup_id = o_vg.drivergroup_id
		union all
		select og_o.operator_id, og_v.driver_id, og_v.right_id, allowed, 2 'priority'
			from dbo.operatorgroup_driver og_v
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
		union all
		select og_o.operator_id, vg_v.driver_id, og_vg.right_id, allowed, 1 'priority'
			from dbo.OPERATORGROUP_driverGROUP og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
			join dbo.driverGROUP_driver vg_v on vg_v.drivergroup_id = og_vg.drivergroup_id
	)
	select distinct operator_id, driver_id, right_id
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.driver_id = r1.driver_id
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority > r1.priority)
GO


