IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DRIVER]') AND type in (N'U'))
	ALTER TABLE dbo.DRIVER 
		add [PHONE] varchar(15)
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_DRIVER]') AND type in (N'U'))
	ALTER TABLE dbo.H_DRIVER 
		add [PHONE] varchar(15)