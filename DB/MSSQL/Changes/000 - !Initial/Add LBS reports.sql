insert into dbo.Report(Report_Guid, Report_Filename)
	select 'B939736A-7B95-4d0b-B59C-6A126D310E70', 'Запросы местоположения, инициированные оператором'
	where not exists (select 1 from dbo.Report where Report_Guid = 'B939736A-7B95-4d0b-B59C-6A126D310E70');

insert into dbo.Report(Report_Guid, Report_Filename)
	select '873109F4-D52A-40c5-B095-6CE759A17469', 'Запросы местоположения других операторов'
	where not exists (select 1 from dbo.Report where Report_Guid = '873109F4-D52A-40c5-B095-6CE759A17469');