/* ��������� ��� ����������� MLP (Mobile Location Protocol) */

if not exists (select 1 from dbo.controller_type where type_name = 'MLP')
	insert into dbo.controller_type (type_name)
	values ('MLP')

/* ��������� ������� � �������� "�����������" ����������� ���������� monitoree_log */
if not exists (select 1 from sys.objects where name = 'position_radius' and type = 'u') 
begin
	create table dbo.position_radius 
	(
		  monitoree_id	int
		, log_time		int
		, radius		int
		, primary key clustered (log_time desc, monitoree_id asc)
	) on TSS_DATA2;
end;

/* ��������� ������� � �������������� ��������� - ������������� MSISDN - ������������ ���� MLP */
if not exists (select 1 from sys.objects where name = 'mlp_controller' and type = 'u') 
begin
	create table dbo.mlp_controller 
	(
		  controller_id	int foreign key references dbo.controller(controller_id) primary key clustered 
		, asid			varchar (32)
	);
end;

/* ��������� ������� � ������, �������� �� ����������� ���������� ��� ������� ���������� (�������� ������� AskPosition)
if not exists (select 1 from sys.columns where name = 'allow_ask_position' and object_id = object_id('controller_type'))
begin
	alter table dbo.controller_type 
		drop column allow_ask_position bit null
	update controller_type
		set allow_ask_position = 1
		where type_name = 'mlp'
end;
*/
