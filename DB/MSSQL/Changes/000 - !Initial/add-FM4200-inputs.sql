/*��������� ����� CAN ��� Teltonika FM4200 (FM3101)*/

declare @controller_type_id int

set @controller_type_id = (select controller_type_id from dbo.Controller_Type where Type_Name = 'FM3101')

create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' CSTName, 145 Number, Descript = 'CAN 01'	
		union select '����������' CSTName, 146 Number, Descript = 'CAN 02'
		union select '����������' CSTName, 147 Number, Descript = 'CAN 03'						    
		union select '����������' CSTName, 148 Number, Descript = 'CAN 04'					
		union select '����������' CSTName, 149 Number, Descript = 'CAN 05'							
		union select '����������' CSTName, 150 Number, Descript = 'CAN 06'						
		union select '����������' CSTName, 151 Number, Descript = 'CAN 07'							
		union select '����������' CSTName, 152 Number, Descript = 'CAN 08'	
		union select '����������' CSTName, 153 Number, Descript = 'CAN 09'	
		union select '����������' CSTName, 154 Number, Descript = 'CAN 10'	
		union select '����������' CSTName, 199 Number, Descript = 'Virtual Odometer'	
		) t
	where ct.Type_Name = 'FM3101'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

create table #Controller_Sensor_Legend
(
	Controller_Sensor_Type_ID int,
	Name nvarchar(50),
	Number int
)

insert into #Controller_Sensor_Legend
	select cst.Controller_Sensor_Type_ID, t.Name, t.Number
		from dbo.Controller_Sensor_Type cst, (
			select '����������' CSTName,  39 Number, Name = 'VirtualOdometer' 
	  union select '����������' CSTName, 227 Number, Name = 'CAN_TotalFuelSpend'						  
	  union select '����������' CSTName, 228 Number, Name = 'CAN_BrakeSpecificFuelConsumption'
	  ) t	
		where t.CSTName = cst.Name

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number)
	select * 
		from #Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)

drop table #Controller_Sensor
drop table #Controller_Sensor_Legend
