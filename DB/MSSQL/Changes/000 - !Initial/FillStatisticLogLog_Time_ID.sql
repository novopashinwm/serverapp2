/*
exec FillStatisticLogLog_Time_ID
*/

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FillStatisticLogLog_Time_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FillStatisticLogLog_Time_ID]
GO

CREATE PROCEDURE [dbo].[FillStatisticLogLog_Time_ID]
as 

begin
	--set nocount on;

	declare @logTimeFrom int, @logTimeTo int;
	
	select top 1 @logTimeFrom = log_time
	from Statistic_Log (nolock) 
	where log_time_id is null
	order by log_time asc;

	print dbo.getdatefromint(@logTimeFrom);

	while (@logTimeFrom < datediff(s, cast('1970-01-01' as datetime), getutcdate()))
	begin
		declare @interval int;
		set @interval = 2555; 
		
		select @logTimeTo = (@logTimeFrom + @interval);

		declare @sql nvarchar(max);
			
		set @sql = N'	
		update s
		set Log_Time_ID = lt.ID
		from dbo.Statistic_Log s
		join dbo.Log_Time lt (nolock) on s.Vehicle_ID = lt.Vehicle_ID
							and s.Log_Time = lt.Log_Time
		where s.log_time >= @logTimeFrom 
		and s.log_time < @logTimeTo
		and s.Log_Time_ID is null
		;	'

		exec sp_executesql @sql, N'@logTimeFrom int, @logTimeTo int', @logTimeFrom = @logTimeFrom, @logTimeTo = @logTimeTo;

		select @logTimeFrom = @logTimeFrom + @interval;

		print dbo.getdatefromint(@logTimeFrom)
	end


end;
