/*
10	AvfoGraf
11	TM4
*/

---AvfoGraf
declare @controller_type_id int;

select @controller_type_id = controller_type_id from dbo.CONTROLLER_TYPE
where type_name = 'AvfoGraf';

if (@controller_type_id is null)
	begin
		insert into dbo.CONTROLLER_TYPE (type_name, packet_length)
		values ('AvfoGraf', 1000);

		select @controller_type_id = @@IDENTITY;
	end
else
	begin
		update dbo.CONTROLLER_TYPE 
		set packet_length = 1000
		where controller_type_id = @controller_type_id;
	end


---select * from dbo.CONTROLLER_SENSOR
---�������� ������ �����������, �� ��������� ��� ����������
if (not exists(select * from dbo.CONTROLLER_SENSOR 
where CONTROLLER_TYPE_ID = @controller_type_id and CONTROLLER_SENSOR_TYPE_ID = 1 and NUMBER = 2))
begin
	insert into dbo.CONTROLLER_SENSOR (CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, 
	NUMBER, MAX_VALUE, MIN_VALUE, DEFAULT_VALUE, BITS)
	values (@controller_type_id, 1,
			2, 65535, 0, 0, 16);
end

---�������� ������ ���������
if (not exists(select * from dbo.CONTROLLER_SENSOR 
where CONTROLLER_TYPE_ID = @controller_type_id and CONTROLLER_SENSOR_TYPE_ID = 2 and NUMBER = 1))
begin
	insert into dbo.CONTROLLER_SENSOR (CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, 
	NUMBER, MAX_VALUE, MIN_VALUE, DEFAULT_VALUE, BITS)
	values (@controller_type_id, 2,
			1, 1, 0, 0, 1);
end


---select * from dbo.CONTROLLER_SENSOR_LEGEND
---�������� ���������� ������ �����������
if (not exists(select * from dbo.CONTROLLER_SENSOR_LEGEND
where CONTROLLER_SENSOR_TYPE_ID = 1 and [name] = '�������� ������ �����������'))
begin
	insert into dbo.CONTROLLER_SENSOR_LEGEND(CONTROLLER_SENSOR_TYPE_ID, NAME)
	values (1, '�������� ������ �����������');
end

---select * from dbo.CONTROLLER_SENSOR_MAP