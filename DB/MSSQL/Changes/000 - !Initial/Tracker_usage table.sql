SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tracker_usage]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[Tracker_usage](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Controller_Type_ID] [int] NULL,
		[Number] [varchar](32) NULL,
		[Control_Code] [varchar](10) NULL,
		Controller_id int not null,
		Activation_Date datetime not null
	 CONSTRAINT [PK_Tracker_usage] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

SET ANSI_PADDING OFF
GO