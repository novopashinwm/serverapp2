if not exists (select * from sys.objects where object_id = object_id('dbo.Picture_Log'))
begin

	create table dbo.Picture_Log
	(
		--������������� ������� ���������� Vehicle(Vehicle_ID)
		Vehicle_ID int not null,
		--����� �������� �����������, ������� �� 1970.01.01 UTC
		Log_Time   int not null,
		--�����������
		Picture    varbinary(max) not null,
		--���������� ������
		constraint PK_Picture_Log
			primary key clustered (Vehicle_ID, Log_Time)
	) ON [TSS_DATA2]

end
go
