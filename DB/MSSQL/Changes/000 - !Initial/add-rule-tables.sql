/* ��������� ����� ������ ��� ���������� ����������� �� ����������� */

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter table dbo.Vehicle_Rule add XML_Condition nvarchar(max);
alter table dbo.Vehicle_Rule add XML_Action nvarchar(max);
alter table dbo.Vehicle_Rule add Enabled bit not null default 0;
alter table dbo.Vehicle_Rule drop constraint IX_VEHICLE_RULE;
create nonclustered index IX_Vehicle_Rule on dbo.Vehicle_Rule (Vehicle_ID, Rule_ID);

alter table dbo.H_Vehicle_Rule add XML_Condition nvarchar(max);
alter table dbo.H_Vehicle_Rule add XML_Action nvarchar(max);
alter table dbo.H_Vehicle_Rule add Enabled bit;

alter table dbo.VehicleGroup_Rule add XML_Condition nvarchar(max);
alter table dbo.VehicleGroup_Rule add XML_Action nvarchar(max);
alter table dbo.VehicleGroup_Rule add Enabled bit not null default 0;
alter table dbo.VehicleGroup_Rule drop constraint IX_VEHICLEGROUP_RULE;
create nonclustered index IX_VehicleGroup_Rule on dbo.VehicleGroup_Rule (VehicleGroup_ID, Rule_ID);

alter table dbo.H_VehicleGroup_Rule add XML_Condition nvarchar(max);
alter table dbo.H_VehicleGroup_Rule add XML_Action nvarchar(max);
alter table dbo.H_VehicleGroup_Rule add Enabled bit;

alter table dbo.[Rule] add Interval int;
alter table dbo.[Rule] add AggregationType varchar(32);	--'Union', 'Intersection'

alter table dbo.Zone_Matrix add IsFull bit;

create index IX_Log_Time_InsertTime on dbo.Log_Time(Vehicle_ID, InsertTime);

if (not exists (select 1 from sys.objects where name = 'Event_Log' and type = 'u'))
begin	
	create table dbo.Event_Log 
	(
		--Dummy row id - for ORM
		ID						int identity(1,1) constraint PK_Event_Log primary key nonclustered,
		--������ �� ������, ��� �������� ���� ������������� ���������� �������
		Vehicle_ID				int,
		--����� ������������� �������
		Log_Time_From			int,
		--����� ��������� �������
		Log_Time_To				int,
		--������ �� ��������� ������� (������������ � ���������� �������), ���������� ������ �������
		Vehicle_Rule_ID			int,
		--������ �� ��������� ������� (������������ � ������ ��������), ���������� ������ �������
		VehicleGroup_Rule_ID	int,
		--�����, �� �������� ������-������ ������ ������������ ����� � �������. 
		--��������, ��� ������� "���� � �������" ����� �������� ������������� �������.		
		Reason					int
	);

	create clustered index IX_Event_Log on dbo.Event_Log(Vehicle_ID, Log_Time_From) ON TSS_Data2;		
end;

if (not exists (select 1 from sys.objects where name = 'Vehicle_Rule_Processing_State' and type = 'u'))
begin
	create table dbo.Vehicle_Rule_Processing 
	(
		--Dummy row id - for ORM
		ID						int identity(1,1) constraint PK_Event primary key nonclustered,
		--������ �� ������, ��� �������� ���� ������������� ���������� �������
		Vehicle_ID				int constraint FK_Vehicle_Rule_Processing_Vehicle_ID foreign key references dbo.Vehicle(Vehicle_ID),
		--������ �� ��������� ������� (������������ � ���������� �������), ���������� ������ �������
		Vehicle_Rule_ID			int constraint FK_Vehicle_Rule_Processing_Vehicle_Rule_ID foreign key references dbo.Vehicle_Rule(Vehicle_Rule_ID),
		--������ �� ��������� ������� (������������ � ������ ��������), ���������� ������ �������
		VehicleGroup_Rule_ID	int constraint FK_Vehicle_Rule_Processing_VehicleGroup_Rule_ID foreign key references dbo.VehicleGroup_Rule(VehicleGroup_Rule_ID),
		--����� Log_Time.InsertTime ������, ������ �� ��������� ��� �������� ������� ��������. ����� ��������� �������� ���������� � LastTime
		CurrentTime				DateTime,
		--������������ ����� Log_Time.InsertTime ������, ������������ ��������.
		LastTime				DateTime not null
	);

	--TODO: ���������� ���������� ������ �, ��������, ������ �������
end;
 

update dbo.[Rule]
	set Interval = 0,
		AggregationType = 'Intersection'
	where Number = 7
	

if (exists (select 1 from sys.objects where name = 'v_zone_primitive_edge' and type = 'v'))
	drop view dbo.v_zone_primitive_edge;

create view dbo.v_zone_primitive_edge
	as 
		select gzp.Zone_ID, gzp.Primitive_ID, zpv1.[Order], mv1.X Lng1, mv1.Y Lat1, mv2.X Lng2, mv2.Y Lat2
			from dbo.Geo_Zone_Primitive gzp
			join dbo.Zone_Primitive_Vertex zpv1 on zpv1.Primitive_ID = gzp.Primitive_ID
			join dbo.Map_Vertex mv1 on mv1.Vertex_ID = zpv1.Vertex_ID
			join dbo.Zone_Primitive_Vertex zpv2 on zpv2.Primitive_ID = gzp.Primitive_ID 
												and zpv2.[Order] = zpv1.[Order] + 1
			join dbo.Map_Vertex mv2 on mv2.Vertex_ID = zpv2.Vertex_ID
				
if (not exists (select 1 from sys.objects where name = 'Scale' and type = 'u'))
begin
	create table dbo.Scale (Value int primary key clustered);
	
	insert into dbo.Scale
				select 0
	union all	select 1
	union all	select 2
	union all	select 3
	union all	select 4
	union all	select 5

	alter table dbo.Zone_Matrix
		add constraint FK_Zone_Matrix_Scale foreign key (Scale) references dbo.Scale(Value)

end
