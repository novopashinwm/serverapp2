/*��������� ��� ����������� S60 - NaviTechDevice*/

if (not exists (select 1 from dbo.Controller_Type where Type_Name = 'NaviTechDevice'))
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
		values ('NaviTechDevice', 200, 0, 0)
		
create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' 	CSTName, 0 Number, Descript = 'PowerVoltage'	
		union select '����������' 	CSTName, 1 Number, Descript = 'AccumulatorVoltage'							
		union select '����������' 	CSTName, 2 Number, Descript = 'Temperature'						    
		union select '����������' 	CSTName, 3 Number, Descript = 'Acceleration0'					
		union select '����������' 	CSTName, 4 Number, Descript = 'Acceleration1'							
		union select '����������' 	CSTName, 5 Number, Descript = 'Acceleration2'						
		union select '����������' 	CSTName,30 Number, Descript = 'AN0'		
		union select '����������' 	CSTName,31 Number, Descript = 'AN1'
		union select '����������' 	CSTName,32 Number, Descript = 'AN2'						
		union select '����������' 	CSTName,33 Number, Descript = 'AN3'						
		union select '����������' 	CSTName,34 Number, Descript = 'AN4'						
		union select '����������' 	CSTName,35 Number, Descript = 'AN5'						
		union select '����������' 	CSTName,36 Number, Descript = 'AN6'						
		union select '����������' 	CSTName,37 Number, Descript = 'AN7'
		union select '��������' 	CSTName,38 Number, Descript = 'D0'		
		union select '��������' 	CSTName,39 Number, Descript = 'D1'
		union select '��������' 	CSTName,40 Number, Descript = 'D2'						
		union select '��������' 	CSTName,41 Number, Descript = 'D3'						
		union select '��������' 	CSTName,42 Number, Descript = 'D4'						
		union select '��������' 	CSTName,43 Number, Descript = 'D5'						
		union select '��������' 	CSTName,44 Number, Descript = 'D6'						
		union select '��������' 	CSTName,45 Number, Descript = 'D7'		) t
	where ct.Type_Name = 'NaviTechDevice'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)
				
drop table #Controller_Sensor