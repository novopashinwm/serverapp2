/* ��������� � ������� GPS_Log ���������� � ����������� ��������, 
	���������� � 1/256 ����������, 
	������������� �� ������ ������ ������� ������� */

if (not exists (select * from sys.columns where object_id = object_id('dbo.GPS_Log') and name = 'Course'))
begin
	alter table dbo.GPS_Log 
		add Course tinyint
end
GO

if (not exists (select * from sys.columns where object_id = object_id('dbo.GPS_Log') and name = 'CourseDegrees'))
begin
	alter table dbo.GPS_Log
		add CourseDegrees as convert(numeric(3), Course * 256 / 360)

end
go