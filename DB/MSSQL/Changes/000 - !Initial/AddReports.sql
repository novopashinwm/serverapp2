

--========================================================================
--������ ���������� ����� ������� � ������� REPORT � SCHEDULEREVENT (��� ��������)
--��� ���������� ��������� ������
--========================================================================

BEGIN TRANSACTION

-- ������ ������� ��� ���������� � SCHEDULEREVENT (��� ���������� ������ ������ �������� �������� � ������) 
create table #ReportsList(	
	--id int identity primary key, 
	[name] 	varchar(1000), 		--�������� �������			
	class	varchar(1000),		--�������� ������ ������
	guid 	uniqueidentifier, 	--GUID ������
	dll 	varchar(1000)		--�������� ������ ������
	)

insert into #ReportsList values ('����� � ������ ��������', 'AnalyticsTssReport', '{256B9CD5-C7E8-4821-BF80-CB639B440EB0}', 'FORIS.TSS.TransportDispatcher.ReportAnalytics')
insert into #ReportsList values ('��������� �������� �� ����� ����������� �����', 'CheckpointIntervalsTssReport', 'F02DAF74-E1D6-4cb9-996F-F07D057C083E', 'FORIS.TSS.TransportDispatcher.ReportCheckpointIntervals')
insert into #ReportsList values ('����� � ������������� ������', 'DefectiveTripsTSSReport', '36FE9F80-44AE-4b42-9C86-BDE947D0B627', 'FORIS.TSS.TransportDispatcher.ReportDefectiveTrips')
insert into #ReportsList values ('�������� ������ ��������', 'DriverProtokolTSSReport', 'B89D5B42-2DDA-4397-A5FE-B45DC7635069', 'FORIS.TSS.TransportDispatcher.ReportDriverProtokol')
insert into #ReportsList values ('���������������� ���������� �� ��������', 'ExploitationTSSReport', '12D915FA-8E6F-4e56-BA2E-143F4CA2F095', 'FORIS.TSS.TransportDispatcher.ReportExploitation')
insert into #ReportsList values ('����� � ����������� GPRS', 'GPRSAccessibilityTssReport', 'E551E65A-FF61-4612-8E07-68042FA9893E', 'FORIS.TSS.TransportDispatcher.ReportGPRSAccessibility')
insert into #ReportsList values ('����� �� ���������� ���������� ������ GPRS �� ��', 'GPRSInaccessibilityTSSReport', '32B443E2-0D60-41f6-973B-84E968DA2243', 'FORIS.TSS.TransportDispatcher.ReportGPRSInaccessibility')
insert into #ReportsList values ('����������� ���������', 'LinearListTssReport', '5AE65843-CC4A-4e87-BAB2-7EAB1AC54727', 'FORIS.TSS.TransportDispatcher.ReportLinearList')
insert into #ReportsList values ('������ ���������', 'ListDriversTssReport', '70AE4F14-444C-4486-B661-148E69323D29', 'FORIS.TSS.TransportDispatcher.ReportListDrivers')
insert into #ReportsList values ('������ ��', 'ListVehiclesTssReport', '36E62835-6B65-45e7-A650-1F41165CB7F9', 'FORIS.TSS.TransportDispatcher.ReportListVehicles')
insert into #ReportsList values ('������������� ������', 'MissedTripsTssReport', '1A5EAF1D-CA46-4480-8C55-E1803A611FD1', 'FORIS.TSS.TransportDispatcher.ReportMissedTrips')
insert into #ReportsList values ('����� � ����������� � ������������� ������', 'MissedTripsDetailTssReport', '7C10794F-0951-46e3-972B-A51A1A0827F0', 'FORIS.TSS.TransportDispatcher.ReportMissedTripsDetail')
insert into #ReportsList values ('�������� � ������� ��������� �� �����', 'MissedTripsResultsTssReport', '51059295-9D4D-4b13-AAA8-DF62906DFBB7', 'FORIS.TSS.TransportDispatcher.ReportMissedTripsResults')
insert into #ReportsList values ('������� ��������', 'MoveTssReport', 'E87D142D-50F1-410c-B125-F5D38760AE35', 'FORIS.TSS.TransportDispatcher.ReportMove')
insert into #ReportsList values ('����� �� �������', 'MoveDistanceTssReport', 'D403FAC9-65A1-4747-B18A-A2A53062E220', 'FORIS.TSS.TransportDispatcher.ReportMoveDistance')
insert into #ReportsList values ('��������� �������� �� �� ���������', 'MovementIntervalsTssReport', '1EEDF92B-3C9D-4686-8B69-7ED32CC053B0', 'FORIS.TSS.TransportDispatcher.ReportMovementIntervals')
insert into #ReportsList values ('����� � ������������ �������� ��������� �� ��������', 'MovementRegularityTssReport', '58CEC1AD-F9BB-4a30-9CB6-D44A6BC245D0', 'FORIS.TSS.TransportDispatcher.ReportMovementRegularity')
insert into #ReportsList values ('�����', 'RosterTssReport', '11EF2A02-C407-49a0-80C6-962AE5733371', 'FORIS.TSS.TransportDispatcher.ReportRoster')
insert into #ReportsList values ('���������� �������� �����', 'SeparateWaybillScheduleTssReport', '6054C10E-4780-43e5-A0E9-206A13FA3231', 'FORIS.TSS.TransportDispatcher.ReportSeparateWaybillSchedule')
insert into #ReportsList values ('����� � ��������� ����������� ������', 'SpeedViolationTSSReport', 'B7CB4C01-50DB-4bf4-82AB-F71E5D595BCC', 'FORIS.TSS.TransportDispatcher.ReportSpeedViolation')
insert into #ReportsList values ('����� � ���������� �������������� ������', 'TemperatureTSSReport', 'E5949AE9-2770-40f9-BA3D-FD03A766D126', 'FORIS.TSS.TransportDispatcher.ReportTemperature')
insert into #ReportsList values ('����� � ������������ ������', 'TransportTSSReport', '69106E5E-D825-47da-A51D-D372A2A1526B', 'FORIS.TSS.TransportDispatcher.ReportTransport')
insert into #ReportsList values ('����� �� ������������� ��������', 'UseFuelTSSReport', 'C467DA3C-A488-4509-A51A-A1F085E0661E', 'FORIS.TSS.TransportDispatcher.ReportUseFuel')
insert into #ReportsList values ('�������� ������ ��', 'VehicleProtokolTSSReport', 'E55580D7-8BBB-4b87-AA70-B43636FB4A2D', 'FORIS.TSS.TransportDispatcher.ReportVehicleProtokol')
insert into #ReportsList values ('����� � ���������� ������������ ������� �� ���������', 'VehiclesByRoutesTssReport', 'F5D4F9BD-14D8-4fa2-ACBE-979E38DBF440', 'FORIS.TSS.TransportDispatcher.ReportVehiclesByRoutes')
insert into #ReportsList values ('������� �� ������', 'VehiclesOutsTssReport', 'DF504E68-27D0-4259-824B-B21888527BAD', 'FORIS.TSS.TransportDispatcher.ReportVehiclesOuts')
--insert into #ReportsList values ('������� �������� �����', 'WaybillHistoryTssReport', '46039364-E424-4393-BEFE-8F677F27F55B', 'FORIS.TSS.TransportDispatcher.ReportWaybillHistory')
insert into #ReportsList values ('������� ����', 'WaybillNewTssReport', '73157D33-19AC-4d05-A9E4-10E48F32E4AB', 'FORIS.TSS.TransportDispatcher.ReportWaybillNew')
insert into #ReportsList values ('�������� �������� ������� ������', 'WaybillsTransferTssReport', '6D4AE7A2-ECAE-4ed9-889A-C8DFF0120826', 'FORIS.TSS.TransportDispatcher.ReportWaybillsTransfer')
insert into #ReportsList values ('����� �� ������� ������� �� ������', 'FuelCostTssReport', '7FA68680-9660-4b9a-9DF0-986F6F9A4F56', 'FORIS.TSS.TransportDispatcher.ReportFuelCost')
--insert into #ReportsList values ('', '', '', '')
--insert into #ReportsList values ('', '', '', '')

--select * from #ReportsList


-- ��������� � ���������� ��� ������ � ������� SCHEDULEREVENT
declare 
	@plugin 		varchar(1000),
	@comment 		varchar(1000),
	@configXML_namespace	varchar(1000),
	@configXML_class	varchar(1000)

set @plugin = 'FORIS.TSS.ServerApplication.Mail.GenericReport_SchedulerPlugin, FORIS.TSS.ServerApplication'
set @comment = '' 	
set @configXML_namespace = 'FORIS.TSS.TransportDispatcher.Reports' 	
set @configXML_class = ''	
	

--������ �� ������� #ReportsList - ���������� ����� ������� � ������� REPORT � SCHEDULEREVENT(��� ��������)
declare 
	@report_name 	varchar(1000),
	@report_class	varchar(1000),
	@report_guid 	uniqueidentifier, 
	@report_dll 	varchar(1000)

DECLARE cursor_ReportsList CURSOR FOR SELECT * FROM #ReportsList 
OPEN cursor_ReportsList 
FETCH NEXT FROM cursor_ReportsList into @report_name, @report_class, @report_guid, @report_dll
WHILE @@FETCH_STATUS = 0
BEGIN
	-- ���������� � ������� REPORT
	if((select REPORT_GUID from REPORT where REPORT_GUID = @report_guid) is null)
		insert into REPORT 
			(REPORT_FILENAME, REPORT_GUID)
		select 
			@report_name, @report_guid

	-- ���������� � ������� SCHEDULEREVENT (CONFIG_XML ��� ntext, � ��� ������ �������� � �������)
	--1)
	if not exists (select 1 from SCHEDULEREVENT where CONFIG_XML LIKE '%' + @report_class + '%') 
		insert into SCHEDULEREVENT 
			(PLUGIN_NAME, COMMENT, CONFIG_XML)
		select 
			--@plugin, @comment, @configXML_namespace + '.' + @report_class + ', ' + @report_dll
			@plugin, @comment, @configXML_namespace + '.' + @report_class + ', ' + @configXML_namespace
	/*--2)
	if((select cast(CONFIG_XML as varchar(100)) from SCHEDULEREVENT where CONFIG_XML LIKE '%' + @report_class + '%') is null)
		insert into SCHEDULEREVENT 
			(PLUGIN_NAME, COMMENT, CONFIG_XML)
		select 
			--@plugin, @comment, @configXML_namespace + '.' + @report_class + ', ' + @report_dll
			@plugin, @comment, @configXML_namespace + '.' + @report_class + ', ' + @configXML_namespace */

	FETCH NEXT FROM cursor_ReportsList into @report_name, @report_class, @report_guid, @report_dll
END
CLOSE cursor_ReportsList
DEALLOCATE cursor_ReportsList


drop table #ReportsList

COMMIT TRANSACTION
--ROLLBACK TRANSACTION

--========================================================================
--������ ���������� ����� ������� � ������� REPORT � SCHEDULEREVENT (��� ��������)
--��� ���������� ��������� ������
--========================================================================
