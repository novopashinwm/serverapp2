alter table dbo.h_day_kind
	alter column Weekdays int;

alter table dbo.H_JOURNAL
	alter column Status int;

alter table dbo.H_Point
	alter column Point_Kind_ID int;

alter table dbo.H_Point_Kind
	alter column Point_Kind_ID int;

alter table dbo.H_ROUTE_POINT
	alter column ORD int;

alter table dbo.H_SCHEDULE_DETAIL
	alter column SHIFT_ID int;

alter table dbo.H_RS_STEP
	alter column RS_VARIANT int null

alter table dbo.H_Controller_Sensor_Map
	add Multiplier numeric(18, 9)

alter table dbo.H_Controller_Sensor_Map
	add Constant numeric(18, 9)

alter table dbo.H_Controller_Sensor_Legend
	add Number int

alter table dbo.H_Controller_Sensor
	add Descript nvarchar(512)