create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' CSTName, 100 Number, 'CAN_Speed'				Descript
		union select '��������'	  CSTName, 101 Number, 'CAN_CruiseControl'      Descript
		union select '��������'	  CSTName, 102 Number, 'CAN_Brake'              Descript
		union select '��������'	  CSTName, 103 Number, 'CAN_ParkingBrake'       Descript
		union select '��������'	  CSTName, 104 Number, 'CAN_Clutch'             Descript
		union select '����������' CSTName, 105 Number, 'CAN_Accelerator'        Descript
		union select '����������' CSTName, 106 Number, 'CAN_FuelRate'           Descript
		union select '����������' CSTName, 107 Number, 'CAN_FuelLevel1'         Descript
		union select '����������' CSTName, 108 Number, 'CAN_FuelLevel2'         Descript
		union select '����������' CSTName, 109 Number, 'CAN_FuelLevel3'         Descript
		union select '����������' CSTName, 110 Number, 'CAN_FuelLevel4'         Descript
		union select '����������' CSTName, 111 Number, 'CAN_FuelLevel5'         Descript
		union select '����������' CSTName, 112 Number, 'CAN_FuelLevel6'         Descript
		union select '����������' CSTName, 113 Number, 'CAN_Revs'               Descript
		union select '����������' CSTName, 114 Number, 'CAN_RunToCarMaintenance'Descript
		union select '����������' CSTName, 115 Number, 'CAN_EngHours'           Descript
		union select '����������' CSTName, 116 Number, 'CAN_CoolantT'           Descript
		union select '����������' CSTName, 117 Number, 'CAN_EngOilT'            Descript
		union select '����������' CSTName, 118 Number, 'CAN_FuelT'              Descript
		union select '����������' CSTName, 119 Number, 'CAN_TotalRun'           Descript
		union select '����������' CSTName, 120 Number, 'CAN_DayRun'             Descript
		union select '����������' CSTName, 121 Number, 'CAN_AxleLoad1'          Descript
		union select '����������' CSTName, 122 Number, 'CAN_AxleLoad2'          Descript
		union select '����������' CSTName, 123 Number, 'CAN_AxleLoad3'          Descript
		union select '����������' CSTName, 124 Number, 'CAN_AxleLoad4'          Descript
		union select '����������' CSTName, 125 Number, 'CAN_AxleLoad5'          Descript
		union select '����������' CSTName, 126 Number, 'CAN_AxleLoad6'          Descript) t
	where ct.Type_Name = 'AvtoGRAF'			  
	  and cst.Name = t.CSTName
											  
insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

create table #Controller_Sensor_Legend
(
	Controller_Sensor_Type_ID int,
	Name nvarchar(50) COLLATE Cyrillic_General_CI_AI ,
	Number int
)

insert into #Controller_Sensor_Legend
	select cst.Controller_Sensor_Type_ID, t.Name, t.Number
		from dbo.Controller_Sensor_Type cst, (
		      select '����������' CSTName, 200 Number, 'CAN_Speed'				Name
		union select '��������'	  CSTName, 201 Number, 'CAN_CruiseControl'      Name
		union select '��������'	  CSTName, 202 Number, 'CAN_Brake'              Name
		union select '��������'	  CSTName, 203 Number, 'CAN_ParkingBrake'       Name
		union select '��������'	  CSTName, 204 Number, 'CAN_Clutch'             Name
		union select '����������' CSTName, 205 Number, 'CAN_Accelerator'        Name
		union select '����������' CSTName, 206 Number, 'CAN_FuelRate'           Name
		union select '����������' CSTName, 207 Number, 'CAN_FuelLevel1'         Name
		union select '����������' CSTName, 208 Number, 'CAN_FuelLevel2'         Name
		union select '����������' CSTName, 209 Number, 'CAN_FuelLevel3'         Name
		union select '����������' CSTName, 210 Number, 'CAN_FuelLevel4'         Name
		union select '����������' CSTName, 211 Number, 'CAN_FuelLevel5'         Name
		union select '����������' CSTName, 212 Number, 'CAN_FuelLevel6'         Name
		union select '����������' CSTName, 213 Number, 'CAN_Revs'               Name
		union select '����������' CSTName, 214 Number, 'CAN_RunToCarMaintenance'Name
		union select '����������' CSTName, 215 Number, 'CAN_EngHours'           Name
		union select '����������' CSTName, 216 Number, 'CAN_CoolantT'           Name
		union select '����������' CSTName, 217 Number, 'CAN_EngOilT'            Name
		union select '����������' CSTName, 218 Number, 'CAN_FuelT'              Name
		union select '����������' CSTName, 219 Number, 'CAN_TotalRun'           Name
		union select '����������' CSTName, 220 Number, 'CAN_DayRun'             Name
		union select '����������' CSTName, 221 Number, 'CAN_AxleLoad1'          Name
		union select '����������' CSTName, 222 Number, 'CAN_AxleLoad2'          Name
		union select '����������' CSTName, 223 Number, 'CAN_AxleLoad3'          Name
		union select '����������' CSTName, 224 Number, 'CAN_AxleLoad4'          Name
		union select '����������' CSTName, 225 Number, 'CAN_AxleLoad5'          Name
		union select '����������' CSTName, 226 Number, 'CAN_AxleLoad6'          Name) t	
		where t.CSTName = cst.Name

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number)
	select * 
		from #Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)

drop table #Controller_Sensor
drop table #Controller_Sensor_Legend