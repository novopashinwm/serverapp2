insert into dbo.Media (Name, Type)
	select 'FTP', 1
	where not exists (select m.Media_ID from dbo.Media m where m.Name = 'FTP' and Type = 1)
	
insert into dbo.Media_Acceptors (Media_ID, Initialization, Description)
	select	t.*
		from (
			select	m.Media_ID,
					'127.0.0.1' Initialization,
					'������ IP-�����, �� ������� ������� ftp-������' Description
				from dbo.Media m
				where m.Name = 'FTP' and Type = 1) t
		where not exists (select 1 from dbo.Media_Acceptors ma where ma.Media_ID = t.Media_ID)
					
			