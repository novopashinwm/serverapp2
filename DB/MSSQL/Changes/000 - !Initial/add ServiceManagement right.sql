/*	��������� � ���������� ���� ����� ����� ServiceManagement - 
��� ������ ������������ ����������/��������������� ��� - 
��� ����������� ������� � ����������� � ���������� ����� */

/*1. ����������� ��������������� ����� � ����������*/
if not exists (select * from dbo.[Right] where Right_ID=110)
	insert into dbo.[Right] (Right_ID, System, Name, Description)
	values (110, 1, 'ServiceManagement', '���������� ��������: ������� ���������� � ������������/���������� �����, ����� ������� ��������������� etc');

/*2. ������������ MPX-��������� ������������ � ������ ��� ������� ���������� �������*/
if not exists (select * from dbo.OperatorGroup where Name='MPXOperators')
	insert into dbo.OperatorGroup(Name, Comment)
	values ('MPXOperators', '��� ���������, ������������������ ����� MPX');

insert into dbo.OperatorGroup_Operator (OperatorGroup_ID, Operator_ID)
	select og.OperatorGroup_ID, o.Operator_ID
		from dbo.OperatorGroup og
		join dbo.Operator o on o.Operator_ID in (select a.Operator_ID from dbo.Asid a)
		left outer join dbo.OperatorGroup_Operator og_o on og_o.OperatorGroup_ID = og.OperatorGroup_ID
													   and og_o.Operator_ID = o.Operator_ID
		where 1=1
		  and og_o.Operator_ID is null
		  and og.Name = 'MPXOperators'
		  and not exists(select * from OperatorGroup_Operator ogo where ogo.OPERATOR_ID = og.OperatorGroup_ID 
						and ogo.OPERATORGROUP_ID = o.Operator_ID)

/*3. ������������ ������� ������������� ����������� ����� ������� � �����, �������� � �������*/
insert into dbo.Right_OperatorGroup (Right_ID, OperatorGroup_ID, Allowed)
	select r.Right_ID, og.OperatorGroup_ID, 1
		from dbo.[Right] r
		join dbo.OperatorGroup og on 1=1
		left outer join dbo.Right_OperatorGroup rog on rog.Right_ID = r.Right_ID 
												   and rog.OperatorGroup_ID = og.OperatorGroup_ID
		where 1=1
			and rog.OperatorGroup_ID is null
			and r.Name in ('Map_GIS', 'VehicleAccess', 'ReportsAccess')
			and not exists (select * from Right_OperatorGroup ro 
							where ro.OPERATORGROUP_ID= og.OperatorGroup_ID 
							and ro.RIGHT_ID = r.Right_ID
							)

/*4. ������������ ������������� ����������� ����� ������� � �����, �������� � �������*/
insert into dbo.Right_Operator (Right_ID, Operator_ID, Allowed)
	select r.Right_ID, o.Operator_ID, 1
		from dbo.[Right] r
		join dbo.Operator o on 1=1
			left outer join v_operator_rights vor on vor.Right_ID = r.Right_ID
												 and vor.Operator_ID = o.Operator_ID
		where vor.Operator_ID is null
		  and r.Name in ('Map_GIS', 'VehicleAccess', 'ReportsAccess')
		and not exists (select * from Right_Operator ro where ro.OPERATOR_ID = o.Operator_ID
						and ro.RIGHT_ID = r.Right_ID) 
		
