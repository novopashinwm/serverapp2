if not exists (select 1 from sys.columns where object_id = object_id('dbo.Tracker') and name = 'Pattern_Vehicle_ID')
begin
	alter table dbo.Tracker
		add Pattern_Vehicle_ID int constraint FK_Tracker_Pattern_Vehicle_ID foreign key references dbo.Vehicle(Vehicle_ID);
end;

if not exists (select 1 from sys.columns where object_id = object_id('dbo.Tracker') and name = 'Vehicle_ID')
begin
	alter table dbo.Tracker
		add Vehicle_ID int constraint FK_Tracker_Vehicle_ID foreign key references dbo.Vehicle(Vehicle_ID);
end;

if not exists (select 1 from sys.columns where object_id = object_id('dbo.Tracker') and name = 'Activated')
begin
	alter table dbo.Tracker
		add Activated bit constraint FK_Tracker_Activated not null default (0);
end;

if exists (select 1 from sys.columns where object_id = object_id('dbo.Tracker') and name = 'Controller_Type_ID')
begin
	alter table dbo.Tracker
		drop FK_Tracker_Controller_Type_ID;
	alter table dbo.Tracker
		drop column Controller_Type_ID;
end;

if (exists (select 1 from sys.objects where object_id = object_id('dbo.Tracker_usage')))
begin
	drop table dbo.Tracker_Usage;
end;

if (not exists (select 1 from sys.objects where object_id = object_id('dbo.H_Tracker')))
begin
	create table dbo.H_Tracker
	(
		H_Billing_Service_ID		int identity(1,1) constraint PK_H_Tracker primary key nonclustered,
		ID							int,
		Number						varchar(32),
		Control_Code				varchar(10),
		Pattern_Vehicle_ID			int,
		Vehicle_ID					int,
		Activated					bit not null,
		TRAIL_ID					int constraint FK_H_Tracker_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	);

end;