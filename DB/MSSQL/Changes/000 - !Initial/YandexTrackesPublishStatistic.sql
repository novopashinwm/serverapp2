IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[YandexTrackesPublishStatistic]') AND type in (N'U'))
DROP TABLE [dbo].[YandexTrackesPublishStatistic]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[YandexTrackesPublishStatistic](
	vehicle_id int,
	log_time int,
	amount int
	 
	 CONSTRAINT [PK_YandexTrackesPublishStatistic] PRIMARY KEY CLUSTERED 
	(
		log_time, vehicle_id
	)
) ON [PRIMARY]

GO


