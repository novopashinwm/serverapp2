/****** Object:  Table [dbo].[H_BUSSTOP]    Script Date: 01/11/2010 16:25:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[H_BUSSTOP](
	[BUSSTOP_ID] [int] NOT NULL,
	[NAME] [nvarchar](50)  NOT NULL,
	[SHORTNAME] [nvarchar](50)  NULL,
	[ADDRESS] [nvarchar](255)  NULL,
	[TRAIL_ID] [int] NOT NULL,
	[ACTION] [nvarchar](6) NOT NULL,
	[ACTUAL_TIME] [datetime] NOT NULL CONSTRAINT [DF__H_BUSSTOP__ACTUA__08B7AF6E]  DEFAULT (getutcdate())
) ON [PRIMARY]
