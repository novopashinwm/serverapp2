alter table dbo.Asid 
	add Contract_Number varchar(255);

alter table dbo.Asid
	add Customer_ID int

alter table dbo.Asid
	add Customer_Type_ID int

alter table dbo.Asid
	add TDC_ID int

create nonclustered index IX_Asid_Contract_Number on dbo.Asid(Contract_Number);
create nonclustered index IX_Asid_Customer_ID on dbo.Asid(Customer_ID);

create table dbo.Billing_Service_Type 
(
	ID int constraint PK_Billing_Service_Type primary key clustered,
	Name varchar(255) constraint UN_Billing_Service_Type_Name unique
);

create nonclustered index IX_Billing_Service_Type_Name on dbo.Billing_Service_Type(Name);

insert into dbo.Billing_Service_Type (ID, Name) values (1, 'LBS');
insert into dbo.Billing_Service_Type (ID, Name) values (2, 'GNSS');
insert into dbo.Billing_Service_Type (ID, Name) values (3, 'NikaTracker');
insert into dbo.Billing_Service_Type (ID, Name) values (4, 'Sygic');

create table dbo.Billing_Service (
	ID int identity(1,1) constraint PK_Billing_Service primary key nonclustered,
	Asid_ID int constraint FK_Billing_Service_Asid_ID foreign key references dbo.Asid(ID),
	Billing_Service_Type_ID int constraint FK_Billing_Service_Billing_Service_Type_ID foreign key references dbo.Billing_Service_Type(ID)
);

create clustered index IX_Billing_Service on dbo.Billing_Service(Asid_ID, Billing_Service_Type_ID);

create table dbo.H_Billing_Service
(
	H_Billing_Service_ID		int identity(1,1) constraint PK_H_Billing_Service primary key nonclustered,
	ID							int,
	Asid_ID						int,
	Billing_Service_Type_ID		int,
	TRAIL_ID					int constraint FK_H_Billing_Service_Trail_ID foreign key references dbo.Trail(Trail_ID),
	[ACTION]					nvarchar(6),
	ACTUAL_TIME					datetime not null
);

create table dbo.Billing_Blocking_Type (
	ID int constraint PK_Billing_Blocking_Type primary key clustered,
	Name varchar(255) constraint UN_Billing_Blocking_Type_Name unique);

create nonclustered index IX_Billing_Blocking_Type_Name on dbo.Billing_Blocking_Type(Name);

insert into dbo.Billing_Blocking_Type (ID, Name) values (1, 'Any')

create table dbo.Billing_Blocking (
	ID							int identity(1,1) constraint PK_Billing_Blocking primary key nonclustered,
	Asid_ID						int constraint FK_Billing_Blocking_Asid_ID foreign key references dbo.Asid(ID),
	Billing_Blocking_Type_ID	int constraint FK_Billing_Blocking_Billing_Blocking_Type_ID foreign key references dbo.Billing_Blocking_Type(ID)
);

create clustered index IX_Billing_Blocking on dbo.Billing_Blocking(Asid_ID, Billing_Blocking_Type_ID);

create table dbo.H_Billing_Blocking
(
	H_Billing_Blocking_ID		int identity(1,1) constraint PK_H_Billing_Blocking primary key nonclustered,
	ID							int,
	Asid_ID						int,
	Billing_Blocking_Type_ID	int,
	TRAIL_ID					int constraint FK_H_Billing_Blocking_Trail_ID foreign key references dbo.Trail(Trail_ID),
	[ACTION]					nvarchar(6),
	ACTUAL_TIME					datetime not null
);

insert into dbo.[Right] (Right_ID, [System], Name, Description)
	select 111, 1, 'PayForCaller', '������, ��������� ���������, ������������ �� ����� �������� ����������'
	where not exists (select 1 from dbo.[Right] where Right_ID = 111)