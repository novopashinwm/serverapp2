CREATE NONCLUSTERED INDEX IX_Vehicle_Rule_Processing_VID_VGRID
ON [dbo].[Vehicle_Rule_Processing] ([Vehicle_ID],[VehicleGroup_Rule_ID])
INCLUDE ([LastTime])
GO