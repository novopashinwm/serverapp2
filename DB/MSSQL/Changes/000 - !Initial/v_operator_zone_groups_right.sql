create view [dbo].[v_operator_zone_groups_right]   
as  
 WITH allRights(operator_id, zonegroup_id, right_id, allowed, priority) AS   
 (  
  select o_vg.operator_id, o_vg.zonegroup_id, o_vg.right_id, allowed, 2 'priority'  
   from dbo.operator_zonegroup o_vg  
  union all  
  select og_o.operator_id, og_vg.zonegroup_id, og_vg.right_id, allowed, 1 'priority'  
   from dbo.OPERATORGROUP_zoneGROUP og_vg  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id  
 )  
 select distinct operator_id, zonegroup_id, right_id  
 from allRights r1  
 where allowed = 1  
 and not exists (select * from allRights r2   
     where r2.operator_id = r1.operator_id   
     and r2.zonegroup_id = r1.zonegroup_id  
     and r2.right_id = r1.right_id  
     and r2.allowed = 0  
     and r2.priority > r1.priority)