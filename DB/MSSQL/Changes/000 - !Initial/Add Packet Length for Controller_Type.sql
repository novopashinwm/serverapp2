
ALTER table CONTROLLER_TYPE
add PACKET_LENGTH int not null default (56)
go
ALTER table H_CONTROLLER_TYPE
add PACKET_LENGTH int not null default (56)
go
update controller_type
set PACKET_LENGTH = 56
where TYPE_NAME = 'SGGB2'
go
update controller_type
set PACKET_LENGTH = 57
where TYPE_NAME = 'SGGB'
go
update controller_type
set PACKET_LENGTH = 47
where TYPE_NAME = 'MJ'
go
update controller_type
set PACKET_LENGTH = 56
where TYPE_NAME = 'SANAV'
go
update controller_type
set PACKET_LENGTH = 170
where TYPE_NAME = 'STEPP'
go
update controller_type
set PACKET_LENGTH = 200
where TYPE_NAME = 'TM140'
go
update controller_type
set PACKET_LENGTH = 70
where TYPE_NAME = 'FM3101'
go
update controller_type
set PACKET_LENGTH = 70
where TYPE_NAME = 'TR-102'
 
