if not exists (select * from sys.objects where object_id = object_id('dbo.WLAN_Log') and type = 'u')
begin

	create table dbo.WLAN_Log
	(
		Vehicle_ID		int not null,
		Log_Time		int not null,
		/*� ���� � ��� �� ������ �������� ����� ������ ��������� WiFi �����, ���� Number ��������� �� ��������� �� ������ PK*/
		Number			tinyint not null,
		/*������ �� �������� �� ����������� MAC-������� WLAN (WLAN_MAC_Address)*/
		WLAN_MAC_Address_ID int not null,
		/*������� �������*/
		SignalStrength	tinyint not null,
		/*������ �� �������� �� ����������� SSID WLAN (WLAN_SSID)*/
		WLAN_SSID_ID    int not null,
		/*����� ������, ����� ���� �� �����*/
		Channel_Number	tinyint,
		constraint PK_WLAN_Log primary key clustered (Vehicle_ID, Log_Time, Number)
	);

end;
go
if not exists (select * from sys.objects where object_id = object_id('dbo.WLAN_MAC_Address') and type = 'u')
begin

	create table dbo.WLAN_MAC_Address
	(
		ID		int identity(1,1)	constraint PK_WLAN_MAC_Address primary key clustered,
		Value	varchar(12)			constraint UQ_WLAN_MAC_Address_Value unique
	);

end;
go

if not exists (select * from sys.objects where object_id = object_id('dbo.WLAN_SSID') and type = 'u')
begin

	create table dbo.WLAN_SSID
	(
		ID		int identity(1,1)	constraint PK_WLAN_SSID primary key clustered,
		Value	varchar(32)			constraint UQ_WLAN_SSID_Value unique
	);

end;
go