IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_zone_vehicle]'))
DROP VIEW [dbo].[v_zone_vehicle]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_zone_vehicle] 
as
	select	zv.zone_id,	zv.vehicle_id
	from dbo.zone_vehicle zv
	union 
	select zv.zone_id, vv.vehicle_id
	from ZONE_VEHICLEGROUP zv
	inner join dbo.VEHICLEGROUP_VEHICLE vv on vv.VEHICLEGROUP_ID = zv.VEHICLEGROUP_ID
