/*	����� 2 ��������� ����� ������ ������ �������� �����. 
����������� ����� ���������� ��������� ������ ������ ���������� MigrateLogData 
������� �������� �������, 
��������������� ������� �������� ����� �����������, 
����������� ���������� ������� � 
������� ����� ��� �������� �������������*/

--���������� ���� ���� ������ ���� ���������
while exists (select 1 from dbo.Monitoree_Log where Log_Time_ID is null)
	exec dbo.[MigrateLogData] @interval = 5555, @debug = 1

--�������������� ����������
exec FillStatisticLog;

--����������� Log_Time_ID ��� ������� Statistic_Log
exec FillStatisticLogLog_Time_ID;

--��������� ���������� �� Monitoree_Log ������ � ������� Can_Info
insert into dbo.Log_Time (Vehicle_ID, Log_Time, Media, InsertTime)
select	ci.Monitoree_ID,
					ci.Log_Time,
					1,
					dbo.GetDateFromInt(ci.Log_Time)
from dbo.Can_Info ci (nolock)
where ci.log_time_id is null

update ci
set Log_Time_ID = lt.ID
from dbo.Can_Info ci
join dbo.Log_Time lt on ci.Monitoree_ID = lt.Vehicle_ID
					and ci.Log_Time = lt.Log_Time
where ci.log_time_id is null

--�� ������ ���� ���� ��������� Can_Info � Controller_Stat ����������
update cs
set
	Log_Time_ID = lt.ID,
	Vehicle_ID = lt.Vehicle_ID,
	Log_Time = lt.Log_Time,
	Odometer    = ml.Odometer
from dbo.Controller_Stat cs
join dbo.Controller c		on 	c.Controller_ID = cs.Controller_ID
join dbo.Log_Time lt        on  lt.Vehicle_ID   = c.Vehicle_ID
							and	lt.Log_Time = datediff(s, cast('1970-01-01' as datetime), cs.[time])
left join dbo.Monitoree_Log ml	on 	ml.Log_Time_ID  = lt.ID									
where cs.log_time_id is null

--��������� ���������� �� Monitoree_Log ������ � ������� Controller_Stat
insert into dbo.Log_Time (Vehicle_ID, Log_Time, Media, InsertTime)
select	c.vehicle_id,
datediff(s, cast('1970-01-01' as datetime), cs.[time]),
1,
cs.[time]
from dbo.Controller_Stat cs
join dbo.Controller c on c.Controller_ID = cs.Controller_ID
where cs.log_time_id is null

update cs
set
	Log_Time_ID = lt.ID,
	Vehicle_ID = lt.Vehicle_ID,
	Log_Time = lt.Log_Time,
	Odometer    = ml.Odometer
from dbo.Controller_Stat cs
join dbo.Controller c		on 	c.Controller_ID = cs.Controller_ID
join dbo.Log_Time lt        on  lt.Vehicle_ID   = c.Vehicle_ID
							and	lt.Log_Time = datediff(s, cast('1970-01-01' as datetime), cs.[time])
left join dbo.Monitoree_Log ml	on 	ml.Log_Time_ID  = lt.ID									
where cs.log_time_id is null



-------------�������� ���������
exec sp_rename 'dbo.Position_Radius.Monitoree_ID', 'Vehicle_ID', 'COLUMN';
go

create clustered index IX_Position_Radius_VID_LT on dbo.Position_Radius(Vehicle_ID, Log_Time) on TSS_Data2;
go

if exists (select 1 from sys.stats where name = 'Statistic_X' and [object_id] = object_id('Monitoree_Log'))
	drop statistics dbo.Monitoree_Log.Statistic_X
go

if exists (select 1 from sys.stats where name = 'Statistic_Y' and [object_id] = object_id('Monitoree_Log'))
	drop statistics dbo.Monitoree_Log.Statistic_Y;
go

alter table dbo.Monitoree_Log
	drop column X, Y;	--�������� 0:00
go

drop index dbo.Monitoree_Log.IX_Monitoree_Log_LTID_LT;	
drop index dbo.Controller_Stat.IX_TIME;					 
drop index dbo.Controller_Stat.PK_ID;					 
drop index dbo.Controller_Stat.PK_TIME;					--�������� 5:19
go

alter table dbo.Controller_Stat
	drop constraint FK_CONTROLLER_STAT_CONTROLLER;
go

alter table dbo.Controller_Stat
	drop column Controller_ID,
		 [Time];	--�������� 0:00
go

exec sp_rename 'dbo.Controller_Stat', 'Sensors_Log', 'OBJECT';
go

create clustered index IX_Sensors_Log_VID_LT on dbo.Sensors_Log(Vehicle_ID, Log_Time) on TSS_Data2;
go

alter table dbo.Sensors_Log 
alter column Log_Time_ID bigint not null;
go

ALTER TABLE dbo.Sensors_Log ADD  CONSTRAINT [PK_Sensors_Log] PRIMARY KEY NONCLUSTERED 
(
	Log_Time_ID
) ON TSS_Data2
go

drop index dbo.Monitoree_Log.IX_LT						
drop index dbo.Monitoree_Log.IX_MID						
drop index dbo.Monitoree_Log.IX_MID_LT_MEDIA
go

alter table dbo.Monitoree_Log
	drop constraint MonitoreeLog_MediaType;
		
go

alter table dbo.Monitoree_Log --future table Geo_Log
	drop column
		Speed,
		Satellites,
		Media,
		Firmware,
		Height,		
		v,
		v1,
		v2,
		v3,
		v4,
		Odometer; --�������� 0:00
go

exec sp_rename 'dbo.Monitoree_Log.Monitoree_ID', 'Vehicle_ID', 'COLUMN';
go

exec sp_rename 'dbo.Monitoree_Log',	'Geo_Log',	'OBJECT';
go

create clustered index IX_Geo_Log_VID_LT on dbo.Geo_Log(Vehicle_ID, Log_Time) on TSS_Data2;
go

alter table dbo.Geo_Log 
alter column Log_Time_ID bigint not null;
go

ALTER TABLE dbo.Geo_Log ADD  CONSTRAINT [PK_Geo_Log] PRIMARY KEY NONCLUSTERED 
(
	Log_Time_ID
) ON TSS_Data2
go

alter table dbo.Can_Info
	drop constraint PK_CAN_INFO;
go

EXEC sp_rename 'dbo.Can_Info.Monitoree_ID', 'Vehicle_ID', 'COLUMN';
GO

drop index dbo.Can_Info.IX_Can_Info_Log_Time_ID;
go

create clustered index IX_Can_Info_VID_LT on dbo.Can_Info(Vehicle_ID, Log_Time) on TSS_Data2;
go

alter table dbo.Can_Info 
alter column Log_Time_ID bigint not null;
go

ALTER TABLE dbo.Can_Info ADD  CONSTRAINT [PK_Can_Info] PRIMARY KEY NONCLUSTERED 
(
	Log_Time_ID
) ON TSS_Data2
go


delete from dbo.Statistic_Log where Log_Time_ID is null;

drop index dbo.Statistic_Log.IX_Statistic_Log_LT_ID;
drop index dbo.Statistic_Log.IX_Statistic_Log_LT_ID_time;
drop index dbo.Statistic_Log.IX_Statistic_time;
go

alter table dbo.Statistic_Log 
alter column Log_Time_ID bigint not null;
go

ALTER TABLE dbo.Statistic_Log ADD  CONSTRAINT [PK_Statistic_Log] PRIMARY KEY NONCLUSTERED 
(
	Log_Time_ID
) ON TSS_Data2
go

alter table dbo.Monitoree_Log_Ignored alter column V	int;
alter table dbo.Monitoree_Log_Ignored alter column V1	int;
alter table dbo.Monitoree_Log_Ignored alter column V2	int;
alter table dbo.Monitoree_Log_Ignored alter column V3	int;
alter table dbo.Monitoree_Log_Ignored alter column V4	int; --02:03
go

--3. ������� ����� monitoree_log ()
create view dbo.Monitoree_Log
	as	select 
			lt.Vehicle_ID	MONITOREE_ID,
			lt.Log_Time		LOG_TIME,
			geol.Lng		X,
			geol.lat		Y,
			gpsl.Speed		SPEED,
			lt.Media		MEDIA,
			sl.V			V,
			sl.V1			V1,
			sl.V2			V2,
			sl.V3			V3,
			sl.V4			V4,
			gpsl.Satellites SATELLITES,
			gpsl.Firmware	FIRMWARE,
			sl.Odometer	    ODOMETER,
			gpsl.Altitude	HEIGHT,
			lt.ID			LOG_TIME_ID
		from dbo.Log_Time lt 
		left outer join dbo.Geo_Log 	geol 	on geol.Log_Time_ID = lt.ID
		left outer join dbo.GPS_Log 	gpsl 	on gpsl.Log_Time_ID = lt.ID
		left outer join dbo.Sensors_Log	sl		on sl.Log_Time_ID = lt.ID;

--5. ������� ����� Controller_stat.
GO
create view dbo.Controller_Stat
	as	select
			c.Controller_ID					CONTROLLER_ID,
			dbo.GetDateFromInt(lt.Log_Time)	[TIME],
			sl.V							V,
			sl.V1							V1,
			sl.V2							V2,
			sl.V3							V3,
			sl.V4							V4,
			sl.DS							DS
		from dbo.Log_Time lt
		inner join dbo.Controller		c	on c.Vehicle_ID = lt.Vehicle_ID
		inner join dbo.Sensors_Log		sl	on sl.Log_Time_ID = lt.ID;
go