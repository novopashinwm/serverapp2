/*��������� ����������-���������� MLP-��������� ���������������� ����� �� ��� MLP-���������� ��� ������������� � ������-������� Sygic*/

declare @securityAdministration int, @vehicleAccess int

set @securityAdministration = (select Right_ID from dbo.[Right] where [Name] = 'SecurityAdministration')
set @vehicleAccess = (select Right_ID from dbo.[Right] where [Name] = 'VehicleAccess')

insert into dbo.Operator_Vehicle (operator_id, vehicle_id, right_id, allowed)
	select a.Operator_ID, c.Vehicle_ID, @securityAdministration, 1
		from dbo.Asid a
		join dbo.MLP_Controller				mlpc on mlpc.Asid_ID = a.ID
		join dbo.Controller					   c on c.Controller_ID = mlpc.Controller_ID
		where 1=1
			and exists (
				select 1 
					from v_operator_vehicle_right ovr 
					where ovr.Operator_ID = a.Operator_ID 
					  and ovr.Vehicle_ID = c.Vehicle_ID
					  and ovr.Right_ID = @vehicleAccess)
			and not exists (
				select 1 
					from v_operator_vehicle_right ovr 
					where ovr.Operator_ID = a.Operator_ID 
					  and ovr.Vehicle_ID = c.Vehicle_ID
					  and ovr.Right_ID = @securityAdministration)
			and a.AllowSecurityAdministration = 1
