if not exists (select * from sys.objects where name = 'Tracker_Attempt' and type = 'u')
begin

	create table dbo.Tracker_Attempt 
	(
		ID			int identity constraint PK_Tracker_Attempt primary key nonclustered,
		Number		varchar(32) not null,
		IP			varchar(39),	--��������� IPv6
		[Count]		int not null,
		Last_Time	datetime not null
	)

	create clustered index IX_Tracker_Attempt_Tracker_ID_IP on dbo.Tracker_Attempt(Number, IP)		

end