alter table dbo.H_CONTROLLER_SENSOR
	alter column Max_Value bigint

alter table dbo.H_CONTROLLER_SENSOR
	alter column Min_Value bigint

alter table dbo.H_CONTROLLER_SENSOR
	alter column Default_Value bigint

alter table dbo.Vehicle_Rule
	add constraint FK_Vehicle_Rule_Vehicle foreign key (Vehicle_ID) references dbo.Vehicle(Vehicle_ID)

alter table dbo.Vehicle_Rule
	add constraint FK_Vehicle_Rule_Rule foreign key (Rule_ID) references dbo.[Rule](Rule_ID)