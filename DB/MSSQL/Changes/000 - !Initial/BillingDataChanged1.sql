SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


--ASID
if (exists (select * from sys.columns where object_id = object_id('dbo.Asid') and name = 'Customer_ID'))
begin
	if (exists (select * from sys.indexes where object_id = object_id('dbo.Asid') and name = 'IX_Asid_Customer_ID'))
	begin
		DROP INDEX IX_Asid_Customer_ID ON dbo.Asid;
	end
	
	ALTER TABLE dbo.Asid
		DROP COLUMN Customer_ID, Customer_Type_ID, TDC_ID;
end
GO

if (not exists (select * from sys.columns where object_id = object_id('dbo.Asid') and name = 'Terminal_Device_Number'))
begin
	ALTER TABLE dbo.Asid ADD
		Terminal_Device_Number bigint NULL;
end

GO

if (not exists (select * from sys.indexes where object_id = object_id('dbo.Asid') and name = 'IX_Asid_Terminal_Device_Number'))
begin
	CREATE NONCLUSTERED INDEX [IX_Asid_Terminal_Device_Number] ON [dbo].[Asid] 
	(
		[Terminal_Device_Number] ASC
	)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
end
GO

--=========================================================
--H_ASID
--=========================================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if not exists (select * from sys.objects where object_id = object_id('dbo.H_Asid') and type = 'u')
begin
	CREATE TABLE [dbo].[H_Asid](
		[H_Asid_ID] [int] IDENTITY(1,1) NOT NULL,
		[ID] [int] NOT NULL,
		[Value] [varchar](255) NULL,
		[AllowMlpRequest] [bit] NOT NULL,
		[AllowSecurityAdministration] [bit] NOT NULL,
		[WarnAboutLocation] [bit] NOT NULL,
		[Operator_ID] [int] NULL,
		[Contract_Number] [varchar](255) NULL,
		[Terminal_Device_Number] [bigint] NULL,
		[TRAIL_ID] [int] NULL,
		[ACTION] [nvarchar](6) NULL,
		[ACTUAL_TIME] [datetime] NOT NULL,
	 CONSTRAINT [PK_H_Asid] PRIMARY KEY NONCLUSTERED 
	(
		[H_Asid_ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];


	ALTER TABLE [dbo].[H_Asid]  WITH CHECK ADD  CONSTRAINT [FK_H_Asid_Trail_ID] FOREIGN KEY([TRAIL_ID])
	REFERENCES [dbo].[TRAIL] ([TRAIL_ID]);

	ALTER TABLE [dbo].[H_Asid] CHECK CONSTRAINT [FK_H_Asid_Trail_ID];

end
GO


--=========================================================
--[Billing_Service_Type]
--=========================================================
if (not exists (select * from sys.columns where object_id = OBJECT_ID('dbo.Billing_Service_Type') and name = 'Service_Type_Category'))
begin 
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Billing_Service_Type]') AND type in (N'U'))
	begin
		if (exists (select * from sys.objects where name = 'FK_Billing_Service_Billing_Service_Type_ID'))
			alter table dbo.Billing_Service
				drop constraint FK_Billing_Service_Billing_Service_Type_ID
	
		DROP TABLE [dbo].[Billing_Service_Type];
	end

	CREATE TABLE [dbo].[Billing_Service_Type](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Service_Type] [varchar](50) NOT NULL,
		[Service_Type_Category] [varchar](50) NOT NULL
	 CONSTRAINT [PK_Billing_Service_Type] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];
end

GO

if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.MLP')
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.MLP', 'LBS');
if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.MLP.Charging')
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.MLP.Charging', 'MlpCharging');
if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.GPS.Modem')	
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.GPS.Modem', 'GNSS');
if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.GPS.Phone')	
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.GPS.Phone', 'NikaTracker');
if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.GPS.Server')	
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.GPS.Server', 'Sygic');
if not exists(select * from [Billing_Service_Type] where [Service_Type]= 'FRNIKA.Admin')	
	insert into [Billing_Service_Type]([Service_Type], [Service_Type_Category])
	values ('FRNIKA.Admin', 'NikaAdmin');

--=========================================================
----------[Billing_Service]
--=========================================================
if (not exists (select * from sys.columns where object_id = OBJECT_ID('dbo.Billing_Service') and name = 'StartDate'))
begin
	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Billing_Service_Asid_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Billing_Service]'))
		ALTER TABLE [dbo].[Billing_Service] DROP CONSTRAINT [FK_Billing_Service_Asid_ID];

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Billing_Service]') AND type in (N'U'))
		DROP TABLE [dbo].[Billing_Service];

	CREATE TABLE [dbo].[Billing_Service](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Asid_ID] [int] not null,
		[Billing_Service_Type_ID] [int] not null,
		[StartDate] datetime not null,
	 CONSTRAINT [PK_Billing_Service] PRIMARY KEY NONCLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [TSS_DATA2];


	CREATE UNIQUE CLUSTERED INDEX IX_Billing_Service_Asid_ID ON dbo.Billing_Service
		(
		[Asid_ID],
		[Billing_Service_Type_ID]
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

	ALTER TABLE [dbo].[Billing_Service]  WITH CHECK ADD  CONSTRAINT [FK_Billing_Service_Asid_ID] FOREIGN KEY([Asid_ID])
	REFERENCES [dbo].[Asid] ([ID]);

	ALTER TABLE [dbo].[Billing_Service] CHECK CONSTRAINT [FK_Billing_Service_Asid_ID];

	ALTER TABLE dbo.Billing_Service ADD CONSTRAINT
		FK_Billing_Service_Billing_Service_Service_Type_ID FOREIGN KEY
		(
		Billing_Service_Type_ID
		) REFERENCES dbo.Billing_Service_Type
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION ;
end
GO

--=========================================================
---H_Billing_service
--=========================================================
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[H_Billing_Service]') AND name = 'StartDate')
begin
	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_Billing_Service_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_Billing_Service]'))
	ALTER TABLE [dbo].[H_Billing_Service] DROP CONSTRAINT [FK_H_Billing_Service_Trail_ID];

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Billing_Service]') AND type in (N'U'))
	DROP TABLE [dbo].[H_Billing_Service];

	CREATE TABLE [dbo].[H_Billing_Service](
		[H_Billing_Service_ID] [int] IDENTITY(1,1) NOT NULL,
		[ID] [int] NOT NULL,
		[Asid_ID] [int] NOT NULL,
		[Billing_Service_Type_ID] [int] NOT NULL,
		[StartDate] [datetime] NOT NULL, 
		[TRAIL_ID] [int] NULL,
		[ACTION] [nvarchar](6) NULL,
		[ACTUAL_TIME] [datetime] NOT NULL,
	 CONSTRAINT [PK_H_Billing_Service] PRIMARY KEY NONCLUSTERED 
	(
		[H_Billing_Service_ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];


	ALTER TABLE [dbo].[H_Billing_Service]  WITH CHECK ADD  CONSTRAINT [FK_H_Billing_Service_Trail_ID] FOREIGN KEY([TRAIL_ID])
	REFERENCES [dbo].[TRAIL] ([TRAIL_ID]);

	ALTER TABLE [dbo].[H_Billing_Service] CHECK CONSTRAINT [FK_H_Billing_Service_Trail_ID];

end

--=========================================================
---Billing_Blocking
--=========================================================
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[Billing_Blocking]') AND name = 'BlockingDate')
begin

	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Billing_Blocking_Asid_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Billing_Blocking]'))
	ALTER TABLE [dbo].[Billing_Blocking] DROP CONSTRAINT [FK_Billing_Blocking_Asid_ID];

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Billing_Blocking]') AND type in (N'U'))
	DROP TABLE [dbo].[Billing_Blocking];

	CREATE TABLE [dbo].[Billing_Blocking](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Asid_ID] [int] not null,
		[BlockingDate] [datetime] not null,
		CONSTRAINT [PK_Billing_Blocking] PRIMARY KEY NONCLUSTERED 
		(
		[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];


	CREATE UNIQUE CLUSTERED INDEX IX_Billing_Blocking_Asid_ID ON dbo.Billing_Blocking
		(
		[Asid_ID]
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

	ALTER TABLE [dbo].[Billing_Blocking]  WITH CHECK ADD  CONSTRAINT [FK_Billing_Blocking_Asid_ID] FOREIGN KEY([Asid_ID])
	REFERENCES [dbo].[Asid] ([ID]);


	ALTER TABLE [dbo].[Billing_Blocking] CHECK CONSTRAINT [FK_Billing_Blocking_Asid_ID];

end

--=========================================================
---H_Billing_Blocking
--=========================================================
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[H_Billing_Blocking]') AND name = 'BlockingDate')
begin
	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_Billing_Blocking_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_Billing_Blocking]'))
	ALTER TABLE [dbo].[H_Billing_Blocking] DROP CONSTRAINT [FK_H_Billing_Blocking_Trail_ID];

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Billing_Blocking]') AND type in (N'U'))
	DROP TABLE [dbo].[H_Billing_Blocking];

	CREATE TABLE [dbo].[H_Billing_Blocking](
		[H_Billing_Blocking_ID] [int] IDENTITY(1,1) NOT NULL,
		[ID] [int],
		[Asid_ID] [int],
		[BlockingDate] [datetime],
		[TRAIL_ID] [int] NULL,
		[ACTION] [nvarchar](6) NULL,
		[ACTUAL_TIME] [datetime] NOT NULL,
	 CONSTRAINT [PK_H_Billing_Blocking] PRIMARY KEY NONCLUSTERED 
	(
		[H_Billing_Blocking_ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];


	ALTER TABLE [dbo].[H_Billing_Blocking]  WITH CHECK ADD  CONSTRAINT [FK_H_Billing_Blocking_Trail_ID] FOREIGN KEY([TRAIL_ID])
	REFERENCES [dbo].[TRAIL] ([TRAIL_ID]);


	ALTER TABLE [dbo].[H_Billing_Blocking] CHECK CONSTRAINT [FK_H_Billing_Blocking_Trail_ID];
end
GO

--=========================================================
--[Billing_Blocking_Type]
--=========================================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Billing_Blocking_Type]') AND type in (N'U'))
	DROP TABLE [dbo].[Billing_Blocking_Type]
GO


--=========================================================
---H_MLP_Controller
--=========================================================
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_MLP_Controller]') AND type in (N'U'))
begin
	CREATE TABLE [dbo].[H_MLP_Controller](
		[H_MLP_Controller_ID] [int] IDENTITY(1,1) NOT NULL,
		[ID] [int] NULL,
		[Controller_ID] [int] NULL,
		[Asid_ID] [int] NULL,
		[TRAIL_ID] [int] NULL,
		[ACTION] [nvarchar](6) NULL,
		[ACTUAL_TIME] [datetime] NOT NULL,
	 CONSTRAINT [PK_H_MLP_Controller] PRIMARY KEY NONCLUSTERED 
	(
		[H_MLP_Controller_ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY];

	ALTER TABLE [dbo].[H_MLP_Controller]  WITH CHECK ADD  CONSTRAINT [FK_H_MLP_Controller_Trail_ID] FOREIGN KEY([TRAIL_ID])
	REFERENCES [dbo].[TRAIL] ([TRAIL_ID]);


	ALTER TABLE [dbo].[H_MLP_Controller] CHECK CONSTRAINT [FK_H_MLP_Controller_Trail_ID];
end

GO