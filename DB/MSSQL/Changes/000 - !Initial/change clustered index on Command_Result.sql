/* ���������� ������ ������� Command_Result ������ Command_ID, � �� PK*/

alter table dbo.Command_Result
	drop constraint PK_Command_Result

alter table dbo.Command_Result
	drop constraint FK_Command_Result_Command_ID;

create clustered index IX_Command_Result_Command_ID on dbo.Command_Result (Command_ID)
	
alter table dbo.Command_Result
	add constraint FK_Command_Result_Command foreign key (Command_ID) references dbo.Command(ID)

alter table dbo.Command_Result
	add constraint PK_Command_Result primary key nonclustered (ID);
