IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_zone_right]'))
DROP VIEW [dbo].[v_operator_zone_right]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
dbo.GEO_ZONE ZONE_ID, NAME, COLOR, DESCRIPTION
dbo.OPERATOR_ZONE OPERATOR_ID, ZONE_ID, RIGHT_ID, ALLOWED
dbo.dbo.OPERATOR_ZONEGROUP OPERATOR_ID, ZONEGROUP_ID, RIGHT_ID, ALLOWED
dbo.OPERATORGROUP_ZONE OPERATORGROUP_ID, ZONE_ID, RIGHT_ID, ALLOWED
dbo.OPERATORGROUP_ZONEGROUP OPERATORGROUP_ID, ZONEGROUP_ID, RIGHT_ID, ALLOWED
dbo.ZONEGROUP_ZONE ZONEGROUP_ZONE_ID, ZONEGROUP_ID, ZONE_ID
*/

create view [dbo].[v_operator_zone_right] 
as
	WITH allRights(operator_id, ZONE_ID, right_id, allowed, priority) AS 
	(
		select operator_id, ZONE_ID, right_id, allowed, 4 'priority'
			from dbo.OPERATOR_ZONE 
		union all
		select o_vg.operator_id, vg_v.ZONE_ID, o_vg.right_id, allowed, 3 'priority'
			from dbo.OPERATOR_ZONEGROUP o_vg
			join dbo.ZONEGROUP_ZONE vg_v on vg_v.ZONEGROUP_ID = o_vg.ZONEGROUP_ID
		union all
		select og_o.operator_id, og_v.ZONE_ID, og_v.right_id, allowed, 2 'priority'
			from dbo.OPERATORGROUP_ZONE og_v
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
		union all
		select og_o.operator_id, vg_v.ZONE_ID, og_vg.right_id, allowed, 1 'priority'
			from dbo.OPERATORGROUP_ZONEGROUP og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
			join dbo.ZONEGROUP_ZONE vg_v on vg_v.ZONEGROUP_ID = og_vg.ZONEGROUP_ID
	)
	select distinct operator_id, ZONE_ID, right_id
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.ZONE_ID = r1.ZONE_ID
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority > r1.priority)

/*
select * from [v_operator_zone_right]
where operator_id = 2
*/