alter table dbo.Operator_Department
	add constraint FK_Operator_Department_Department foreign key (Department_ID) references dbo.Department(Department_ID);

alter table dbo.OperatorGroup_Department
	add constraint FK_OperatorGroup_Department_Department foreign key (Department_ID) references dbo.Department(Department_ID);