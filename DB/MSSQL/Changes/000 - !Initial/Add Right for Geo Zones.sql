
-- Add right for with Zone

if not exists (select * from [RIGHT] where RIGHT_ID = 19)
begin
	insert [RIGHT]
	(RIGHT_ID, [SYSTEM], [NAME], [DESCRIPTION], HELP_URL, URL)
	values
	(19, 1, 'EditZone', 'Allows to edit geo-zones', null, null) 
end
go

if not exists (select * from [RIGHT] where RIGHT_ID = 105)
begin
	insert into [RIGHT] values (105, 0, 'ZoneAccess', 'Grants access for zones', null, null)             
end
go
