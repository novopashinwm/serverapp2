if not exists (select * from sys.objects where object_id = object_id('dbo.Cell_Network_Log') and type = 'u')
begin
	
	--���������� � ������� ���� ������ ���������
	create table dbo.Cell_Network_Log
	(
		Vehicle_ID 		int not null,
		Log_Time   		int not null,
		/*� ���� � ��� �� ������ �������� ����� ������ ��������� ������� �����, ���� Number ��������� �� ��������� �� ������ PK*/
		Number	   		tinyint not null,
		/*������ �� �������� �� ����������� ����� �����*/
		Country_Code_ID smallint not null,
		/*������ �� �������� �� ����������� ����� �����*/
		Network_Code_ID smallint not null,
		/*������������� ����*/
		Cell_ID	   		int not null,
		/*Local Area Code*/
		LAC		   		int not null,
		/*������� �������*/
		SignalStrength	smallint not null,
		constraint PK_Cell_Network_Log primary key clustered (Vehicle_ID, Log_Time, Number)
	);

end;
go
if not exists (select * from sys.objects where object_id = object_id('dbo.Country_Code'))
begin
	
	--���������� ����� �����
	create table dbo.Country_Code
	(
		ID smallint identity(1,1) constraint PK_Country_Code primary key clustered,
		Value varchar(4) constraint UQ_Country_Code_Value unique
	);

end;
go
if not exists (select * from sys.objects where object_id = object_id('dbo.Network_Code'))
begin
	
	--���������� ����� �����
	create table dbo.Network_Code
	(
		ID smallint identity(1,1) constraint PK_Network_Code primary key clustered,
		Value varchar(4) constraint UQ_Network_Code_Value unique
	);

end;
	