/*
	��������� � ���������� ���� ����� AsidSecurityAdministration �� ���������� ���� �� ������� asid'�.
	��������, ����� �� ��������� ��������� �� ��������� asid'�.
*/

if not exists (select 1 from dbo.[right] where [name] = 'AsidSecurityAdministration') 
begin
	insert into dbo.[right] (right_id, system, [name], description)
		values (107, 0, 'AsidSecurityAdministration', 'Allows to add and remove permissions associated with current asid')
end;
