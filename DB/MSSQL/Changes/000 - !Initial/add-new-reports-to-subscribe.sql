create table #NewReportToSubscribe (SchedulerEvent_ID int, Comment nvarchar(512), Config_XML ntext)
insert into #NewReportToSubscribe values (61, '����� ����� �� �������',						'FORIS.TSS.TransportDispatcher.Reports.VehicleGroupMove, FORIS.TSS.TransportDispatcher.ReportVehicleGroupMove')
insert into #NewReportToSubscribe values (62, '����� �� �������',							'FORIS.TSS.TransportDispatcher.Reports.FuelCostTSSReport, FORIS.TSS.TransportDispatcher.ReportFuelCost')
insert into #NewReportToSubscribe values (63, '����� � ���������� ��������',				'FORIS.TSS.TransportDispatcher.Reports.SpeedViolationTSSReport, FORIS.TSS.TransportDispatcher.ReportSpeedViolation')
insert into #NewReportToSubscribe values (64, '������� � ����������',						'FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests.CommunityRequestsTssReport, FORIS.TSS.TransportDispatcher.ReportsLBS')
insert into #NewReportToSubscribe values (65, '���� ���������� �',							'FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests.OperatorRequestsTssReport, FORIS.TSS.TransportDispatcher.ReportsLBS')
insert into #NewReportToSubscribe values (66, '��� ���������� ����',						'FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests.AnothersRequestsTssReport, FORIS.TSS.TransportDispatcher.ReportsLBS')
insert into #NewReportToSubscribe values (67, '������ ��������� ���',						'FORIS.TSS.TransportDispatcher.FuelCostCalculation.FuelCostCalculationReport, FORIS.TSS.TransportDispatcher.FuelCostCalculation')

set identity_insert SchedulerEvent on

insert into dbo.SchedulerEvent (SchedulerEvent_ID, Plugin_Name, Comment, Config_Xml)
	select 
		SchedulerEvent_ID,
		'FORIS.TSS.ServerApplication.Mail.GenericReport_SchedulerPlugin, FORIS.TSS.ServerApplication',
		Comment,
		Config_XML
		from #NewReportToSubscribe
		where SchedulerEvent_ID not in (select SchedulerEvent_ID from dbo.SchedulerEvent)

set identity_insert SchedulerEvent off

drop table #NewReportToSubscribe