/*	��������� PK � FK(Trail_ID) � ������� H_* ��� ���� Entity Framework*/

create table #tt_history_table (hname varchar(255), name varchar(255), id int, i int);

insert into #tt_history_table
	select hso.name,
		   so.name,
		   hso.object_id,
		   row_number() over (order by hso.name)
		from sys.objects hso
		join sys.objects so on so.type = 'u' 
							and 'h_' + so.name = hso.name
		where hso.type = 'u'
		  and exists (select 1 
						from sys.columns sc
						where 
							sc.object_id = hso.object_id
						and sc.name = 'trail_id');

declare @i int, @max_i int;

select @i = min(i), @max_i = max(i) from #tt_history_table;

select @i, @max_i;

select * from #tt_history_table;

declare @sql nvarchar(max)

while @i <= @max_i
begin

	declare @hname varchar(255), @name varchar(255), @id int

	select @hname = hname, @name = name, @id = id
		from #tt_history_table
		where i = @i;
	
	print '<' + @hname + '>'

	if (not exists (select 1 from sys.objects where type = 'pk' and parent_object_id = @id) 
	and not exists (select 1 from sys.columns where object_id = @id and (name='id' or is_identity = 1)))
	begin
		set @sql = ' alter table ' + @hname + ' add ID int identity constraint PK_' + @hname + ' primary key'
		
		exec sp_executesql @sql
	end;

	if (not exists (select 1 
						from sys.foreign_key_columns fkc 
						join sys.columns sc  on  sc.object_id = fkc.referenced_object_id 
											  and sc.column_id = fkc.referenced_column_id
						where fkc.parent_object_id = @id
						  and sc.name = 'trail_id'))
	begin

		
		set @sql = ' 
delete h
	from ' + @hname + ' h
	left outer join dbo.Trail t on t.Trail_ID = h.Trail_ID
	where t.Trail_ID is null;

alter table ' + @hname + ' add constraint FK_' + @hname + '_Trail_ID foreign key (Trail_ID) references dbo.Trail(Trail_ID)'
		
		exec sp_executesql @sql

	end;

	print '</' + @hname + '>'
	set @i = @i + 1
end;

drop table #tt_history_table;
