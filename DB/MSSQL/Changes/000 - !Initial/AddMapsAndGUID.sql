
--================================================================
-- ���������� ����� ���� � ������� MAPS � GUID-�� ��� ����
-- ��� ���������� ����� ����� ��������� ������
--================================================================

BEGIN TRANSACTION

-- ������ ���� ��� ���������� � (��� ���������� ����� ����� �������� � ������) 
create table #MapsGuid(	
	[num]			int identity primary key, 
	[name] 			varchar(1000), 					
	[description]	varchar(1000),			
	[file_name]		varchar(1000),	
	[version]		varchar(10), --DEFAULT '2.0',
	[check_sum]		int,
	[guid] 			uniqueidentifier 		
	)

insert into #MapsGuid values ('������', '������', 'Kazan_2007.mt', '2.0', '', '32AC582F-CE3A-4d72-9036-B72A5D00FB70')
insert into #MapsGuid values ('������������� ����', '������������� ����', 'Krasnodar kray.mt', '2.0', '', '9C54ABA9-1C93-478a-B304-2A8CD547E112')
insert into #MapsGuid values ('���������', '���������', 'Krasnodar.mt', '2.0', '', '685D5BF0-D977-424f-8AD7-4C986D07FD5A')
insert into #MapsGuid values ('������', '������ � ���������� �������', 'Moscow.mt', '2.0', '', '86AA79B0-461F-41fc-8B87-B7879CA3781A')
insert into #MapsGuid values ('������', '������ � ���������� �������', 'Mosregion_7_3.mt', '2.0', '', '0CF388E0-6596-4ae4-88F1-7B24D9B00204')
insert into #MapsGuid values ('������', '������', 'Russia_2007.mt', '2.0', '', 'D4589457-A382-446f-9698-9F9D3680E0B8')
insert into #MapsGuid values ('�����-���������', '�����-���������', 'Snt-Peterburg_2007.mt', '2.0', '', 'A0DC5796-6614-4881-AA64-8A2396EFE74E')
insert into #MapsGuid values ('���������', '���������', 'Tatarstan_2007.mt', '2.0', '', '9408903B-DFD8-4196-96BE-658C1A5CC67C')
insert into #MapsGuid values ('����������', '���������� ��', 'Zelenograd.mt', '2.0', '', 'F2E4B855-B7DA-4368-8927-68E16E337265')
--insert into #MapsGuid values ('', '', '', '2.0', '', '')
--insert into #MapsGuid values ('', '', '', '2.0', '', '')
--insert into #MapsGuid values ('', '', '', '2.0', '', '')

--select * from #MapsGuid

-- ���������� 
declare 
	@num		int,
	@count		int,
	@FileName 	varchar(1000),
	@guid 		uniqueidentifier

set	@num = 0
set	@count = (select count(*) from #MapsGuid)

while @num < @count
begin
	set	@num = @num + 1

	set @FileName = (select [FILE_NAME] from #MapsGuid where num = @num)
	set	@guid = (select GUID from #MapsGuid where num = @num) 			
	
	if((select [FILE_NAME] from MAPS where [FILE_NAME] = @FileName) is null)
		insert into MAPS 
			([NAME], [DESCRIPTION], [FILE_NAME], [VERSION], [CHECK_SUM], [GUID]) 
		select [NAME], [DESCRIPTION], [FILE_NAME], [VERSION], [CHECK_SUM], [GUID] from #MapsGuid where num = @num
	
	if((select GUID from MAPS where GUID = @guid) is null)
		update MAPS set GUID = @guid where [FILE_NAME] = @FileName

end

drop table #MapsGuid

COMMIT TRANSACTION
--ROLLBACK TRANSACTION

--================================================================
-- ���������� ����� ���� � ������� MAPS � GUID-�� ��� ����
--================================================================
