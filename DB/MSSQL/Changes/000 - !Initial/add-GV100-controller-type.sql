/*��������� ��� ����������� GV100
	����������� ������� ����������� (��������� ������, ����������� etc)
*/

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'GV100')

if (@controller_type_id is null)
begin
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
		values ('GV100', 100, 0, 0)
	set @controller_type_id = @@identity;
end;

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));


insert into @controller_sensor values (@controller_type_id, 1, 1,    0, 65535, 0,    64, 	'AN1');
insert into @controller_sensor values (@controller_type_id, 1, 70,	 0, 65535, 0,    64, 	'BackupBatteryVCC');
insert into @controller_sensor values (@controller_type_id, 2, 11,    0,   1,   0, 	1, 		'DS1');
insert into @controller_sensor values (@controller_type_id, 2, 12,    0,   1,   0, 	1, 		'DS2');
insert into @controller_sensor values (@controller_type_id, 2, 13,    0,   1,   0, 	1, 		'DS3');
insert into @controller_sensor values (@controller_type_id, 2, 14,    0,   1,   0, 	1, 		'DS4');
insert into @controller_sensor values (@controller_type_id, 2, 15,    0,   1,   0, 	1, 		'DS5');
insert into @controller_sensor values (@controller_type_id, 2, 20,    0,   1,   0, 	1, 		'D_OUT1');
insert into @controller_sensor values (@controller_type_id, 2, 21,    0,   1,   0, 	1, 		'D_OUT2');
insert into @controller_sensor values (@controller_type_id, 2, 22,    0,   1,   0, 	1, 		'D_OUT3');
insert into @controller_sensor values (@controller_type_id, 2, 23,    0,   1,   0, 	1, 		'D_OUT4');
insert into @controller_sensor values (@controller_type_id, 2, 40,    0,   1,   0, 	1, 		'TOW');
insert into @controller_sensor values (@controller_type_id, 2, 50,    0,   1,   0, 	1, 		'MoveSensor');
insert into @controller_sensor values (@controller_type_id, 2, 60,    0,   1,   0, 	1, 		'MainSupply');


update cs
set MAX_VALUE = t.MAX_VALUE,
MIN_VALUE = t.MIN_VALUE,
CONTROLLER_SENSOR_TYPE_ID = t.CONTROLLER_SENSOR_TYPE_ID,
DEFAULT_VALUE = t.DEFAULT_VALUE,
BITS = t.BITS,
Descript = t.Descript
from dbo.Controller_Sensor cs
join @controller_sensor t on t.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID and t.NUMBER = cs.NUMBER


insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)
						  
/*
select * from Controller_Type where controller_type_id = @controller_type_id

select * from Controller_Sensor where controller_type_id = @controller_type_id
*/