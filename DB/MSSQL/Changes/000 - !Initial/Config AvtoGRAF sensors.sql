exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 1,
	@DESCRIPTION = 'AN1',
	@CONTROLLER_SENSOR_TYPE_ID = 1,
	@DEFAULT_VALUE = 0,
	@MAX_VALUE = 1023,
	@MIN_VALUE = 0,
	@BITS = 10;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 2,
	@DESCRIPTION = 'AN2',
	@CONTROLLER_SENSOR_TYPE_ID = 1,
	@DEFAULT_VALUE = 0,
	@MAX_VALUE = 1023,
	@MIN_VALUE = 0,
	@BITS = 10;


--==========================================================
---    D ��������
--==========================================================	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 11, @DESCRIPTION = 'D1',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 12, @DESCRIPTION = 'D2',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 13, @DESCRIPTION = 'D3',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 14, @DESCRIPTION = 'D4',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 15, @DESCRIPTION = 'D5',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 16, @DESCRIPTION = 'D6',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 17, @DESCRIPTION = 'D7',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 18, @DESCRIPTION = 'D8',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 1,	@MIN_VALUE = 0,	@BITS = 1;
	

--==========================================================
---    D_SUM ����������� �������� ��������� ��������
--==========================================================
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 21, @DESCRIPTION = 'D_SUM1 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 22, @DESCRIPTION = 'D_SUM2 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 23, @DESCRIPTION = 'D_SUM3 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 24, @DESCRIPTION = 'D_SUM4 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 25, @DESCRIPTION = 'D_SUM5 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 26, @DESCRIPTION = 'D_SUM6 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 27, @DESCRIPTION = 'D_SUM7 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;
	
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 28, @DESCRIPTION = 'D_SUM8 ����������� �������� ��������� ��������',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 16777216,	@MIN_VALUE = 0,	@BITS = 24;

--==========================================================
---                 LLS
--==========================================================
exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 31, @DESCRIPTION = 'LLS1',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 32, @DESCRIPTION = 'LLS2',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 33, @DESCRIPTION = 'LLS3',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 34, @DESCRIPTION = 'LLS4',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 35, @DESCRIPTION = 'LLS5',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 36, @DESCRIPTION = 'LLS6',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 37, @DESCRIPTION = 'LLS7',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;

exec SetControllerSensorNumber @CONTROLLER_TYPE_NAME = 'AvtoGRAF',
	@NUMBER = 38, @DESCRIPTION = 'LLS8',
	@CONTROLLER_SENSOR_TYPE_ID = 2,	@DEFAULT_VALUE = 0,	@MAX_VALUE = 4096,	@MIN_VALUE = 0,	@BITS = 12;


--==========================================================
--==========================================================
select cs.descript, cs.number, cs.* 
from controller_sensor cs
join controller_type ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
where [TYPE_NAME] = 'AvtoGRAF'

/*
delete from controller_sensor
where controller_sensor_id in (23,24)
*/