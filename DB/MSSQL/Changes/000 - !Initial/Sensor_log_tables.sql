SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
---==============================================================
----Controller_Sensor_Log
---==============================================================
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Controller_Sensor_Log]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Controller_Sensor_Log](
		Vehicle_ID	int not null,
		Log_Time	int not null,
		Number	int not null,		--����� �����, �� ������� Controller_Sensor
		Value	bigint not null	--"�����" �������� �������
			 CONSTRAINT [PK_Controller_Sensor_Log] PRIMARY KEY CLUSTERED 
			(
				VEHICLE_ID ASC,
				LOG_TIME ASC,
				NUMBER asc
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

END ----Controller_Sensor_Log



alter table Controller_Sensor add DESCRIPT nvarchar(512);
go

--������ int �� bigInt ������� DEFAULT_VALUE,Min_Value � Max_Value 
ALTER TABLE [dbo].[CONTROLLER_SENSOR] DROP CONSTRAINT [DF_CONTROLLER_SENSOR_MIN_VALUE];
go
alter table Controller_Sensor alter column Min_Value bigint  not null;
go
ALTER TABLE [dbo].[CONTROLLER_SENSOR] ADD  CONSTRAINT [DF_CONTROLLER_SENSOR_MIN_VALUE]  DEFAULT ((0)) FOR [MIN_VALUE];

ALTER TABLE [dbo].[CONTROLLER_SENSOR] DROP CONSTRAINT [DF_CONTROLLER_SENSOR_MAX_VALUE];
go
alter table Controller_Sensor alter column Max_Value bigint not null;
go
ALTER TABLE [dbo].[CONTROLLER_SENSOR] ADD  CONSTRAINT [DF_CONTROLLER_SENSOR_MAX_VALUE]  DEFAULT ((1)) FOR [MAX_VALUE];

ALTER TABLE [dbo].[CONTROLLER_SENSOR] DROP CONSTRAINT [DF_CONTROLLER_SENSOR_DEFAULT_VALUE];
go
alter table Controller_Sensor alter column DEFAULT_VALUE bigint not null;
GO
ALTER TABLE [dbo].[CONTROLLER_SENSOR] ADD  CONSTRAINT [DF_CONTROLLER_SENSOR_DEFAULT_VALUE]  DEFAULT ((0)) FOR [DEFAULT_VALUE];
GO

---	����������� ��������� ��������� �������������� �� �������� ������� � �������� 
alter table Controller_Sensor_Map add MULTIPLIER numeric(18, 9);  --��������� � �������� �������������� Multiplier*Value+Constant, ��� Value - ��������, �������� � �������
alter table Controller_Sensor_Map add CONSTANT   numeric(18, 9); --��������� � �������� �������������� Multiplier*Value+Constant, ��� Value - ��������, �������� � �������

go