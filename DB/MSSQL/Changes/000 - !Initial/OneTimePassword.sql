if not exists (select * from sys.columns where name = 'OneTimePassword' and object_id = object_id('Operator'))
begin

	alter table dbo.Operator
		add OneTimePassword bit not null default 0

	alter table dbo.H_Operator
		add OneTimePassword bit
end