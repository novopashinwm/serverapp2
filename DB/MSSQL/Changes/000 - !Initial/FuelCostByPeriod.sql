/*1. ���������� "��� �������"
  2. �������� ����������� "��� �������"
  3. ���� "��� �������" ��� �������� "������"*/

if (not exists (select * from sys.objects where name = 'Fuel_Type' and type = 'u'))
begin

	create table dbo.Fuel_Type
	(
		ID		int constraint PK_Fuel_Type primary key clustered,
		Name	nvarchar(30)
	);		

end;

insert into dbo.Fuel_Type(ID, Name)
	select t.ID, t.Name
		from (
					select 1 ID, '��'		Name
			union	select 2 ID, '������'	Name
			union	select 3 ID, '��-80'	Name
			union	select 4 ID, '��-92'	Name
			union	select 5 ID, '��-95'	Name
			union	select 6 ID, '��-98'	Name) t
		where not exists (select * from dbo.Fuel_Type ft where ft.ID = t.ID)

if (not exists (select * from sys.columns where object_id = object_id('dbo.Vehicle') and name = 'Fuel_Type_ID'))
begin	
	alter table dbo.Vehicle
		add Fuel_Type_ID int constraint FK_Vehicle_Fuel_Type_ID foreign key references dbo.Fuel_Type(ID)
end;
