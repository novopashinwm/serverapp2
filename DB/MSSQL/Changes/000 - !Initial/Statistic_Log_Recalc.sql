SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statistic_Log_Recalc](
	[Vehicle_ID] [int] CONSTRAINT [PK_Statistic_Log_Recalc] PRIMARY KEY CLUSTERED,
	[Log_Time] [int] NULL
)
GO