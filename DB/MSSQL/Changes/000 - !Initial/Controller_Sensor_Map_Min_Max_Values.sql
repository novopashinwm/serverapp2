/*��������� ���� Min_Value � Max_Value � ������� Controller_Sensor_Map*/

if not exists (select * from sys.columns where object_id = object_id('dbo.Controller_Sensor_Map') and name = 'Min_Value')
begin
	alter table dbo.Controller_Sensor_Map 
		add Min_Value bigint;
end;

if not exists (select * from sys.columns where object_id = object_id('dbo.Controller_Sensor_Map') and name = 'Max_Value')
begin
	alter table dbo.Controller_Sensor_Map 
		add Max_Value bigint;
end;

/*��������� ���� Min_Value � Max_Value � ������� Controller_Sensor_Map*/

if not exists (select * from sys.columns where object_id = object_id('dbo.H_Controller_Sensor_Map') and name = 'Min_Value')
begin
	alter table dbo.H_Controller_Sensor_Map 
		add Min_Value bigint;
end;

if not exists (select * from sys.columns where object_id = object_id('dbo.H_Controller_Sensor_Map') and name = 'Max_Value')
begin
	alter table dbo.H_Controller_Sensor_Map 
		add Max_Value bigint;
end;
