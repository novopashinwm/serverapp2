if not exists (select * from sys.columns where object_id = object_id('dbo.Billing_Service_Type') and name = 'Pattern_Vehicle_ID')
begin

	alter table dbo.Billing_Service_Type
		add Pattern_Vehicle_ID int constraint FK_Billing_Service_Type_Pattern_Vehicle_ID foreign key references dbo.Vehicle(Vehicle_ID)

end
go