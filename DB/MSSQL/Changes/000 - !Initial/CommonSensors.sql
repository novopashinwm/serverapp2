create table #Controller_Sensor_Legend
(
	Controller_Sensor_Type_ID int,
	Name nvarchar(50),
	Number int
)

insert into #Controller_Sensor_Legend
	select cst.Controller_Sensor_Type_ID, t.Name, t.Number
		from dbo.Controller_Sensor_Type cst, (
			select '����������' CSTName, 7 Number, 'CAN.Fuel'		Name
	  union select '����������' CSTName, 8 Number, 'CAN.Total_Run'	Name) t	
		where t.CSTName = cst.Name

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number)
	select * 
		from #Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)

drop table #Controller_Sensor_Legend
