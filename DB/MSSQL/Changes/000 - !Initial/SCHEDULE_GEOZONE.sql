
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE]') AND parent_object_id = OBJECT_ID(N'[dbo].[SCHEDULE_GEOZONE]'))
ALTER TABLE [dbo].[SCHEDULE_GEOZONE] DROP CONSTRAINT [FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCHEDULE_GEOZONE_TRIP]') AND parent_object_id = OBJECT_ID(N'[dbo].[SCHEDULE_GEOZONE]'))
ALTER TABLE [dbo].[SCHEDULE_GEOZONE] DROP CONSTRAINT [FK_SCHEDULE_GEOZONE_TRIP]
GO
/****** Object:  Table [dbo].[SCHEDULE_GEOZONE]    Script Date: 01/11/2010 10:56:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCHEDULE_GEOZONE]') AND type in (N'U'))
DROP TABLE [dbo].[SCHEDULE_GEOZONE]


/****** Object:  Table [dbo].[SCHEDULE_GEOZONE]    Script Date: 01/11/2010 10:56:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCHEDULE_GEOZONE](
	[SCHEDULE_GEOZONE_ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_GEOZONE_ID] [int] NULL,
	[TIME_IN] [int] NULL,
	[TIME_OUT] [int] NULL,
	[TIME_DEVIATION] [int] NULL,
	[STATUS] [int] NULL,
	[TRIP_ID] [int] NULL,
 CONSTRAINT [PK_SCHEDULE_GEOZONE] PRIMARY KEY CLUSTERED 
(
	[SCHEDULE_GEOZONE_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[SCHEDULE_GEOZONE]  WITH CHECK ADD  CONSTRAINT [FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE] FOREIGN KEY([ROUTE_GEOZONE_ID])
REFERENCES [dbo].[ROUTE_GEOZONE] ([ROUTE_GEOZONE_ID])
GO
ALTER TABLE [dbo].[SCHEDULE_GEOZONE]  WITH CHECK ADD  CONSTRAINT [FK_SCHEDULE_GEOZONE_TRIP] FOREIGN KEY([TRIP_ID])
REFERENCES [dbo].[TRIP] ([TRIP_ID])