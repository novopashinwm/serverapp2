/****** Object:  View [dbo].[v_operator_route_right]    Script Date: 07/02/2010 12:45:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_route_right]'))
DROP VIEW [dbo].[v_operator_route_right]
GO

/****** Object:  View [dbo].[v_operator_route_right]    Script Date: 07/02/2010 12:45:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[v_operator_route_right] 
as
	WITH allRights(operator_id, route_id, right_id, allowed, priority) AS 
	(
		select operator_id, route_id, right_id, allowed, 4 'priority'
			from dbo.operator_route 
		union all
		select o_vg.operator_id, vg_v.route_id, o_vg.right_id, allowed, 3 'priority'
			from dbo.operator_routegroup o_vg
			join dbo.ROUTEGROUP_ROUTE vg_v on vg_v.ROUTEGROUP_ID = o_vg.ROUTEGROUP_ID
		union all
		select og_o.operator_id, og_v.route_id, og_v.right_id, allowed, 2 'priority'
			from dbo.operatorgroup_route og_v
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
		union all
		select og_o.operator_id, vg_v.route_id, og_vg.right_id, allowed, 1 'priority'
			from dbo.OPERATORGROUP_routegroup og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
			join dbo.ROUTEGROUP_ROUTE vg_v on vg_v.ROUTEGROUP_ID = og_vg.ROUTEGROUP_ID
	)
	select distinct operator_id, route_id, right_id
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.route_id = r1.route_id
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority > r1.priority)
GO


