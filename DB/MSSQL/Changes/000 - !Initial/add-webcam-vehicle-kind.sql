set identity_insert dbo.Vehicle_Kind on
go

declare @vehicle_kind_id int
declare @name nvarchar(255)

set @vehicle_kind_id = 9
set @name = '���-������'

insert into dbo.Vehicle_Kind (Vehicle_Kind_ID, Name)
	select @vehicle_kind_id, @name
		where not exists (select 1 from dbo.Vehicle_Kind where Vehicle_Kind_ID = @vehicle_kind_id)

set identity_insert dbo.Vehicle_Kind off