if not exists (
	select * 
		from sys.objects 
		where name = 'DF_Phone_Confirmed'
		  and type_desc = 'DEFAULT_CONSTRAINT'
)
begin

	alter table Phone
		add constraint DF_Phone_Confirmed default 0 for Confirmed

end
