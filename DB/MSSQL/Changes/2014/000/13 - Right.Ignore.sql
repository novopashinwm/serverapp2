insert into [Right] (right_id, system, name, description)
	select t.right_id, 1, 'Ignore', 'Hides object from users''s ones'
		from (select right_id = 120) t
		where not exists (select * from [right] r where r.right_id = t.right_id)
go

