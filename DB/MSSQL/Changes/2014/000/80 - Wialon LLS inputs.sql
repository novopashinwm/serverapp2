declare @type_name varchar(100) = 'Wialon'
declare @controller_type_id int;
set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)
declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 1, 400 + 0, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'FuelLevel0');
insert into @controller_sensor values (@controller_type_id, 1, 400 + 1, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'FuelLevel1');

insert into @controller_sensor values (@controller_type_id, 1, 410 + 0, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'FuelTemperature0');
insert into @controller_sensor values (@controller_type_id, 1, 410 + 1, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'FuelTemperature1');

set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

update controller_sensor
	set Descript = 'FuelLevel0' 
	where CONTROLLER_TYPE_ID = @controller_type_id and DESCRIPT = 'LLS0'

update controller_sensor
	set Descript = 'FuelLevel1' 
	where CONTROLLER_TYPE_ID = @controller_type_id and DESCRIPT = 'LLS1'
	
