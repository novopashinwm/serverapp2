if not exists (
	select *
		from sys.indexes where name = 'IX_Message_SourceType_ID_Source_ID'
)
begin

	create nonclustered index IX_Message_SourceType_ID_Source_ID
		on Message(SourceType_ID, Source_ID)
		
end
go

if not exists (
	select *
		from sys.indexes where name = 'IX_Message_DestinationType_ID_Destination_ID'
)
begin

	create nonclustered index IX_Message_DestinationType_ID_Destination_ID
		on Message(DestinationType_ID, Destination_ID)
		
end