if not exists (
	select *
		from sys.tables
		where name = 'Attachment'
)
begin

	create table Attachment
	(
		ID int identity constraint PK_Attachment primary key clustered,
		Name nvarchar(255),
		Data varbinary(max)
	)
	
	create table Message_Attachment
	(
		Message_ID int not null	
			constraint FK_Message_Attachment_Message_ID
				foreign key references Message(Message_ID),
		Attachment_ID int not null
			constraint FK_Message_Attachment_Attachment_ID
				foreign key references Attachment(ID),
		constraint PK_Message_Attachment
			primary key clustered (Message_ID, Attachment_ID)
	)
	
	create nonclustered index IX_Message_Attachment_Attachment_ID on Message_Attachment(Attachment_ID)
	
	create table H_Message_Attachment
	(
		Message_ID int not null,
		Attachment_ID int not null,
		H_Message_Attachment_ID int identity constraint PK_H_Message_Attachment primary key clustered,
		
        TRAIL_ID        int 
			constraint FK_H_Message_Attachment_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

end