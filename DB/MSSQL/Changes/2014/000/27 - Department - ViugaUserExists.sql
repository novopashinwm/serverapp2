if not exists (
	select *
		from sys.columns 
		where object_id = object_id('Department')
		  and name = 'ViugaUserExists'
)
begin

	alter table Department 
		add ViugaUserExists bit not null
			constraint DF_Department_ViugaUserExists
				default (0)

	alter table H_Department 
		add ViugaUserExists bit not null
			constraint DF_H_Department_ViugaUserExists
				default (0)

end