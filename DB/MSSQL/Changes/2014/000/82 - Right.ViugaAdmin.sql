insert into [RIGHT] (right_id, NAME, DESCRIPTION)
	select t.*
		from (
			select Right_ID = 121
			     , Name = 'ViugaAdmin'
			     , Description = '����� ��������� ������� ������������� � ������ ������� �������') t
		where not exists (select * from [RIGHT] e where e.RIGHT_ID = t.Right_ID)

insert into Operator_Department
	select *
		from (
			select o.operator_id, d.department_id, right_id = 121, allowed = 1
				from Department d
				cross apply (
					select top(1) od.Operator_ID
						from Operator_Department od 
						where od.Department_ID = d.Department_ID
						  and od.Right_ID = 2
						order by od.Operator_ID asc
				) o
				where d.ViugaUserExists = 1
		) t
		where not exists (
			select * 
				from Operator_Department e 
				where e.Operator_ID = t.Operator_ID 
				  and e.Department_ID = t.Department_ID 
				  and e.Right_ID = t.right_id)