if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Command_Sender_ID_Date_Received_Type_ID'
)
begin

	create nonclustered index IX_Command_Sender_ID_Date_Received_Type_ID
		on Command (Sender_ID, Date_Received, Type_ID)	

end