if not exists (
	select * 
		from sys.indexes
		where name = 'IX_RenderedServiceItem_Asid_ID_Billing_Service_ID_RenderDate'
)
begin

	create nonclustered index IX_RenderedServiceItem_Asid_ID_Billing_Service_ID_RenderDate
		on RenderedServiceItem (Asid_ID, Billing_Service_ID, RenderDate)
		include (Rendered_Service_Type_ID)


end