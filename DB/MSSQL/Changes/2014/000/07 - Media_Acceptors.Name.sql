if not exists (
	select * 
		from sys.columns
		where object_id = object_id('media_acceptors')
		  and name = 'name'
)
begin

	alter table Media_Acceptors 
		add Name varchar(255)

end
go

update ma
	set Name = m.NAME
	from MEDIA_ACCEPTORS ma
	join Media m on m.media_id = ma.media_id
	where ma.Name is null
