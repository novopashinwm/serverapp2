--��� ���� �������������-��������� ����� ���� ������� � ���� ������ ����� �� �������������� ����������

insert into operator_vehicle (operator_id, VEHICLE_ID, right_id, ALLOWED)
	select o.operator_id, c.vehicle_id, r.right_id, 1
		from Asid a
		join Operator o on o.Operator_ID = a.Operator_ID
		join mlp_controller mc on mc.asid_id = a.id
		join controller c on c.controller_id = mc.controller_id
		join [right] r on r.name in (	'SecurityAdministration',
										'VehicleAccess',
										'ViewVehicleCurrentStatus',
										'EditVehicles',
										'EditControllerDeviceID',
										'EditControllerPhone',
										'ControllerPasswordAccess', 
										'ViewControllerDeviceID', 
										'ViewControllerPhone')
		where 
				exists (
					select * 
						from Billing_Service bs 
						join billing_service_type bst on bst.id = bs.billing_Service_type_Id
						where bs.Asid_ID = a.ID							
						  and bst.Service_Type_Category in ('NikaTracker', 'GNSS')		)
			and
			not exists (
			select * 
				from operator_vehicle e
				where e.operator_Id = o.operator_id
				  and e.VEHICLE_ID = c.VEHICLE_ID
				  and e.right_id = r.right_id)
		
