if not exists (
	select * from sys.indexes where name = 'IX_H_Operator_Operator_ID'
)

begin

	CREATE NONCLUSTERED INDEX [IX_H_Operator_Operator_ID] ON [dbo].[H_OPERATOR] 
	(
		[OPERATOR_ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

end

