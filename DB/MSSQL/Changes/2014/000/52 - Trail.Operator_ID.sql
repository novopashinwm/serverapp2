if not exists (
	select * 
		from sys.columns 
		where OBJECT_ID = OBJECT_ID('Trail') 
		  and name = 'Operator_ID'
)
begin

	alter table Trail
		add Operator_ID int
			constraint FK_Trail_Operator_ID
				foreign key references Operator(Operator_ID)

end