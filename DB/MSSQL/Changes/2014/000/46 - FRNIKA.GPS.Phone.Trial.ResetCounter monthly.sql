update bst 
	set ResetCounter = 'monthly'
	  , LimitedQuantity = 1
	  , MaxQuantity = 40
	  , MinIntervalSeconds = null
	  , SMS = 1
	  , LBS = 1
	from Billing_Service_Type bst
	where Service_Type = 'FRNIKA.GPS.Phone.Trial' 
	  and (	1=0
	   or   ResetCounter is null or ResetCounter <> 'monthly'
	   or   LimitedQuantity <> 1
	   or	MaxQuantity is null 
	   or   MaxQuantity <> 40
	   or	MinIntervalSeconds is not null
	   or   SMS <> 1
	   or	LBS <> 1)