if not exists (
	select *
		from sys.columns
		where object_id = object_id('Emulator')
		  and name = 'Enabled'
)
begin 

	alter table Emulator
		add Enabled bit not null
			constraint DF_Emulator default(1)
			
end