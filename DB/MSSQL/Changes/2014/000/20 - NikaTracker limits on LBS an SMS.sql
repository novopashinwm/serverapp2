update bst
	set SMS = 1
	  , LBS = 1
	  , LimitedQuantity = 1
	  , MinIntervalSeconds = null
	  , MaxQuantity = 40
	  , ResetCounter = 'monthly'
	from Billing_Service_Type bst
	where service_type_category = 'NikaTracker'
	  and SMS = 0

