update bst
	set LimitedQuantity = 0
	from Billing_Service_Type bst
	where Service_Type in ('FRNIKA.MLP')
	  and LimitedQuantity = 1

update bst 
	set Shared = 1
	from Billing_Service_Type bst
	where bst.Shared = 0
	  and bst.Service_Type in ('FRNIKA.GPS.Phone', 'FRNIKA.MLP.ChargingEconom', 'FRNIKA.GPS.Phone.Trial')

update bst 
	set ResetCounter = null
	  , LimitedQuantity = 1
	  , MaxQuantity = 40
	  , MinIntervalSeconds = null
	  , SMS = 1
	  , LBS = 1
	from Billing_Service_Type bst
	where Service_Type = 'FRNIKA.GPS.Phone.Trial' 
	  and (	1=0
	   or   ResetCounter is not null
	   or   LimitedQuantity <> 1
	   or	MaxQuantity is null 
	   or   MaxQuantity <> 40
	   or	MinIntervalSeconds is not null
	   or   SMS <> 1
	   or	LBS <> 1)