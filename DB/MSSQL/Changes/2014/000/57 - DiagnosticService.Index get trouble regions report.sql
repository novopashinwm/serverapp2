--������������ ������ ������ ��, �� ������� �� ��������� N ���� �� ��������� ������� ������� �� ���������� ��������

if not exists (
	select *
		from sys.indexes where name = 'IX_H_Billing_Service_Asid_ID_Action_Actual_Time')
begin

	create nonclustered index IX_H_Billing_Service_Asid_ID_Action_Actual_Time
		on H_Billing_Service(Asid_ID, Action, Actual_Time)

end
go

if not exists (
	select *
		from sys.indexes where name = 'IX_H_Billing_Service_ID_Action_Actual_Time')
begin

	create nonclustered index IX_H_Billing_Service_ID_Action_Actual_Time
		on H_Billing_Service(ID, Action, Actual_Time)

end
go

if not exists (
	select *
		from sys.indexes where name = 'IX_H_Billing_Blocking_Asid_ID_Action_Actual_Time')
begin

	create nonclustered index IX_H_Billing_Blocking_Asid_ID_Action_Actual_Time
		on H_Billing_Blocking(Asid_ID, Action, Actual_Time)

end
go

if not exists (
	select *
		from sys.indexes where name = 'IX_H_Billing_Blocking_ID_Action_Actual_Time')
begin

	create nonclustered index IX_H_Billing_Blocking_ID_Action_Actual_Time
		on H_Billing_Blocking(ID, Action, Actual_Time)

end
go
