if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Message_Template_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_Message_Template_CompoundRule_ID
		on CompoundRule_Message_Template (CompoundRule_ID)

end