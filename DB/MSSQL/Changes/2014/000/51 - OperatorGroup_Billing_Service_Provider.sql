if not exists (
	select * 
		from sys.objects
		where name = 'OperatorGroup_Billing_Service_Provider'
)
begin
	
	create table OperatorGroup_Billing_Service_Provider
	(
		ID int identity(1,1)
			constraint PK_OperatorGroup_Billing_Service_Provider
				primary key nonclustered,	
		OperatorGroup_ID int not null
			constraint FK_OperatorGroup_Billing_Service_Provider_OperatorGroup_ID
				foreign key references OperatorGroup(OperatorGroup_ID),
		Billing_Service_Provider_ID int not null 
			constraint FK_OperatorGroup_Billing_Service_Provider_Billing_Service_Provider_ID
				foreign key references Billing_Service_Provider(Billing_Service_Provider_ID),
		Right_ID int not null
			constraint FK_OperatorGroup_Billing_Service_Provider
				foreign key references [Right](Right_ID),
		constraint UQ_OperatorGroup_Billing_Service_Provider
			unique (OperatorGroup_ID, Billing_Service_Provider_ID, Right_ID),
	)
	
	create clustered index IX_OperatorGroup_Billing_Service_Provider_OperatorGroup_ID_Billing_Service_Provider_ID_Right_ID
		on OperatorGroup_Billing_Service_Provider (OperatorGroup_ID, Billing_Service_Provider_ID, Right_ID)

	create table H_OperatorGroup_Billing_Service_Provider
	(
		ID int not null,
		OperatorGroup_ID int not null,
		Billing_Service_Provider_ID int not null,
		Right_ID int not null,
		
		H_OperatorGroup_Billing_Service_Provider_ID int identity (1,1)
			constraint PK_H_OperatorGroup_Billing_Service_Provider_ID 
				primary key clustered,
				
        TRAIL_ID        int 
			constraint FK_H_OperatorGroup_Billing_Service_Provider_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)		
		
end
go

if not exists (
	select *
		from sys.columns 
		where object_id = object_id('Billing_Service_Provider')
		  and name = 'Country_ID'
)
begin

	alter table Billing_Service_Provider
		add Country_ID int
			constraint FK_Billing_Service_Provider_Country_ID
				foreign key references Country(Country_ID)
			
end
go

if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Department')
		  and name = 'Billing_Service_Provider_ID'
)
begin

	alter table Department
		add Billing_Service_Provider_ID int
			constraint FK_Department_Billing_Service_Provider_ID
				foreign key references Billing_Service_Provider(Billing_Service_Provider_ID)

	alter table H_Department
		add Billing_Service_Provider_ID int
			
end
go

update bsp
	set Country_ID = c.Country_ID
	from Billing_Service_Provider bsp
	join Country c on c.Name = 'Russia'
	where bsp.Country_ID is null
	  and bsp.Name like '��� ��%'
	  
update d
	set Billing_Service_Provider_ID = a.Billing_Service_Provider_ID
	from Department d
	cross apply (
		select top(1) Billing_Service_Provider_ID 
			from Asid a 
			where a.Department_ID = d.Department_ID 
			  and a.Billing_Service_Provider_ID is not null) a
	where d.Billing_Service_Provider_ID is null
	
insert into Billing_Service_Provider (Name, Country_ID)
	select t.Name, c.Country_ID
		from (select Name = 'Sitronics.India') t, Country c
		where c.Name = 'India'
		  and not exists (select * from Billing_Service_Provider e where e.Name = t.Name)
		  
update d 
	set Billing_Service_Provider_ID = bsp.Billing_Service_Provider_ID
	from Department d
	join Billing_Service_Provider bsp on bsp.Country_ID = d.Country_ID
	where d.Billing_Service_Provider_ID is null