insert into RIGHT_OPERATORGROUP
	select r.Right_ID, og.OperatorGroup_ID, 1
		from [RIGHT] r, OPERATORGROUP og
		where r.NAME = 'SecurityAdministration'
		  and og.NAME = 'MPXOperators'
		  and not exists (
			select * 
				from RIGHT_OPERATORGROUP e
				where e.RIGHT_ID = r.RIGHT_ID
				  and e.OPERATORGROUP_ID = og.OPERATORGROUP_ID)