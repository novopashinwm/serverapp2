if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_PositionAbsence_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_PositionAbsence_CompoundRule_ID
		on CompoundRule_PositionAbsence (CompoundRule_ID)

end