begin tran

declare @billing_service_id int
declare @schedulerqueue_id int
declare @day int

while 1=1
begin

	set @billing_service_id = (
		select top(1) bs.ID
				from Billing_Service_Type bst
				join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
				where bst.ResetCounter = 'monthly'
				  and not exists (
					select * from Billing_Service_SchedulerQueue e where e.Billing_Service_ID = bs.ID))

	if @billing_service_id is null
		break

	set @day = datepart(day, (select StartDate from Billing_Service where ID = @billing_service_id))

	insert into SCHEDULERQUEUE (SCHEDULEREVENT_ID, NEAREST_TIME, REPETITION_XML, Enabled)
		select top(1) 
			se.SCHEDULEREVENT_ID
			, GETUTCDATE()
			, '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:clr="http://schemas.microsoft.com/soap/encoding/clr/1.0" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">  <SOAP-ENV:Body>  <a1:RepeatOnNthDayOfMonth id="ref-1" xmlns:a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces">  <n>' 
			+ CONVERT(varchar(2), @day) 
			+ '</n>  <month>0</month>  <comment xsi:null="1"/>  </a1:RepeatOnNthDayOfMonth>  </SOAP-ENV:Body>  </SOAP-ENV:Envelope>'
			, 1
			from SCHEDULEREVENT se 
			where se.COMMENT = 'Reset billing service counter value'

	set @schedulerqueue_id = @@IDENTITY
	
	insert into Billing_Service_SchedulerQueue values(@billing_service_id, @schedulerqueue_id)

end
 
commit
