insert into Billing_Service_Type(
	Service_Type, Name, LBS, Service_Type_Category, Controller_Type_ID, Vehicle_Kind_ID, MaxQuantity, MinIntervalSeconds, LimitedQuantity, ResetCounter, Singleton, SMS, PeriodDays, MinPrice, MaxPrice, Shared, Removable)

	select * 
		from (
			select 
				 Service_Type = 'APN:nika.msk'
			   , Name = '�� �������������'
			   , LBS = 0
			   , bst.Service_Type_Category, bst.Controller_Type_ID, bst.Vehicle_Kind_ID, bst.MaxQuantity, bst.MinIntervalSeconds, bst.LimitedQuantity, bst.ResetCounter, bst.Singleton, bst.SMS, bst.PeriodDays, bst.MinPrice, bst.MaxPrice, bst.Shared, bst.Removable
				from Billing_Service_Type bst
				where bst.Service_Type = 'FRNIKA.GPS.Modem'
			) t
			where not exists (
				select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)