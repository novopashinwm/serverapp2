--������� ���������������� ����� �� ����������� � ���� ���������

begin tran

;with Department_Admin (Department_ID, Operator_ID, ServicePresent)
as (
select d.department_id
	 , a.Operator_ID
	 , ServicePresent = convert(bit, case when exists (
							select *
								from Billing_Service bs
								join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
								where bs.Asid_ID = a.ID
								  and bst.Service_Type_Category = 'NikaAdmin'
						) then 1 else 0 end)
								  
	from Department d
	join Asid a on a.Department_ID = d.Department_ID
	join v_operator_department_right odr on odr.operator_id = a.operator_id
										and odr.right_id = 2
										and odr.department_id = d.department_id	
),
DepartmentToFix as (
select da.Department_ID
	 , FirstAdminID = (
		select top(1) fda.Operator_ID 
			from Department_Admin fda 
			where fda.Department_ID = da.Department_ID 
			order by fda.ServicePresent desc, fda.Operator_ID asc)
	from Department_Admin da
	group by da.Department_ID
	having count(1) > 1
)
delete od 
	from DepartmentToFix dtf 
	join Department_Admin da on da.Department_ID = dtf.Department_ID 
							and da.Operator_ID <> dtf.FirstAdminID
	left outer join Operator_Department od on od.Department_ID = da.Department_ID
	                                      and od.Operator_ID = da.Operator_ID
	                                      and od.Right_ID <> 104 /*DepartmentsAccess*/

commit