if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_ZoneGroup_ZoneGroup_ID'
)
begin

	create nonclustered index IX_CompoundRule_ZoneGroup_ZoneGroup_ID
		on CompoundRule_ZoneGroup (ZoneGroup_ID) include (CompoundRule_ID)

end