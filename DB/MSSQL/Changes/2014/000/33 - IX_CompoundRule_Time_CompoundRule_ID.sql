if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Time_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_Time_CompoundRule_ID
		on CompoundRule_Time (CompoundRule_ID)

end