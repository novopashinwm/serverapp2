if not exists (
	select *
		from sys.objects
		where name = 'RenderedServiceItem'
)
begin

	create table RenderedServiceItem
	(
		ID bigint identity
			constraint PK_RenderedServiceItem
				primary key clustered,
		RenderDate datetime not null,
		Asid_ID int not null
			constraint FK_RenderedServiceItem_Asid_ID 
				foreign key references Asid(ID),
		--��� foreign key, ����� � ������ �������� ������ �� ������� CDR �� ��
		Billing_Service_ID int,
		Rendered_Service_Type_ID int not null
			constraint FK_RenderedServiceItem_Rendered_Service_Type_ID
				foreign key references Rendered_Service_Type(ID),
		Command_ID int
			constraint FK_RenderedServiceItem_Command_ID
				foreign key references Command(ID),
		Message_ID int
			constraint FK_RenderedServiceItem_Message_ID
				foreign key references Message(Message_ID),
		Status int not null
			constraint DF_RenderedServiceItem_Status
				default(0),
		StatusDate datetime not null	
			constraint DF_RenderedServiceItem_StatusDate
				default(getutcdate()),
		ErrorCount int not null
			constraint DF_RenderedServiceItem_ErrorCount
				default(0)
	)
	
end