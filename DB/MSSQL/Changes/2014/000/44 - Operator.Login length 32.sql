if (exists (
	select *
		from sys.columns 
		where object_id = object_id('Operator')
		  and name = 'LOGIN'
		  and max_length = 60
))
begin
	alter table Operator
		alter column Login nvarchar(32)
	alter table H_Operator
		alter column Login nvarchar(32)
end