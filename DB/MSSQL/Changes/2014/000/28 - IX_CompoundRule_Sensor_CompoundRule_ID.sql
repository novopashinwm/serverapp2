if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Sensor_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_Sensor_CompoundRule_ID
		on CompoundRule_Sensor (CompoundRule_ID) include (Controller_Sensor_Legend_ID)

end