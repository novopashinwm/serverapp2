insert into dbo.Message_Template(Name)
	select t.Name 
		from (
			  select Name = 'QuestionToSupport'		union all 
			  select        'CalculationRequest'
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)