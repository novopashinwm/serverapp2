begin tran

declare @access int = 102
declare @lbs    int = (select r.Right_ID from [RIGHT] r where r.name = 'ViewVehicleCurrentStatus')

insert into OPERATOR_VEHICLE (OPERATOR_ID, VEHICLE_ID, RIGHT_ID, ALLOWED)
	select ov.OPERATOR_ID, ov.VEHICLE_ID, @lbs, ov.ALLOWED
		from OPERATOR_VEHICLE ov
		where not exists (
			select *
				from OPERATOR_VEHICLE e
				where e.OPERATOR_ID = ov.OPERATOR_ID
				  and e.VEHICLE_ID = ov.Vehicle_ID
				  and e.RIGHT_ID = @lbs)
		and ov.RIGHT_ID = @access
		
update lbs
	set Allowed = access.Allowed
	from OPERATOR_VEHICLE access
	join OPERATOR_VEHICLE lbs on lbs.Operator_ID = access.OPERATOR_ID
							 and lbs.Vehicle_ID = access.VEHICLE_ID
							 and lbs.RIGHT_ID = @lbs						 
	where lbs.Allowed <> access.ALLOWED
	  and access.RIGHT_ID = @access

insert into OPERATOR_VehicleGroup (OPERATOR_ID, VehicleGroup_ID, RIGHT_ID, ALLOWED)
	select ov.OPERATOR_ID, ov.VehicleGroup_ID, @lbs, ov.ALLOWED
		from OPERATOR_VehicleGroup ov
		where not exists (
			select *
				from OPERATOR_VehicleGroup e
				where e.OPERATOR_ID = ov.OPERATOR_ID
				  and e.VehicleGroup_ID = ov.VehicleGroup_ID
				  and e.RIGHT_ID = @lbs)
		and ov.RIGHT_ID = @access
		
update lbs
	set Allowed = access.Allowed
	from OPERATOR_VehicleGroup access
	join OPERATOR_VehicleGroup lbs on lbs.Operator_ID = access.OPERATOR_ID
							 and lbs.VehicleGroup_ID = access.VehicleGroup_ID
							 and lbs.RIGHT_ID = @lbs						 
	where lbs.Allowed <> access.ALLOWED
	  and access.RIGHT_ID = @access

commit