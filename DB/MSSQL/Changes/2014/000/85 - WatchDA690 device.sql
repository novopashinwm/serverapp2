insert into CONTROLLER_TYPE (TYPE_NAME, AllowedToAddByCustomer)
	select t.Name, 1
		from (select Name = 'WatchDA690') t
		where not exists (select * from CONTROLLER_TYPE e where e.TYPE_NAME = t.Name)
	