if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_Field_Message_Template_Field_ID_Message_ID'
)
begin

	create nonclustered index IX_Message_Field_Message_Template_Field_ID_Message_ID
		on Message_Field (Message_Template_Field_ID, Message_ID) include (Content)
		
end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_Field_Message_ID_Message_Template_Field_ID'
)
begin

	create nonclustered index IX_Message_Field_Message_ID_Message_Template_Field_ID
		on Message_Field (Message_ID, Message_Template_Field_ID) include (Content)
		
end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_Template_ID_DestinationType_ID_Destination_ID'
)
begin

	create nonclustered index IX_Message_Template_ID_DestinationType_ID_Destination_ID
		on Message(Template_ID, DestinationType_ID, Destination_ID)

end