if exists (
	select * 
		from sys.views
		where name = 'v_operator_department_right'
)
	drop view v_operator_department_right
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[v_operator_department_right] 
as
	WITH allRights(operator_id, DEPARTMENT_ID, right_id, allowed, priority) AS 
	(
		select operator_id, DEPARTMENT_ID, right_id, allowed, 4 'priority'
			from dbo.OPERATOR_DEPARTMENT
		union all
		select og_o.operator_id, og_v.DEPARTMENT_ID, og_v.right_id, allowed, 2 'priority'
			from dbo.OPERATORGROUP_DEPARTMENT og_v
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
	)
	select operator_id, DEPARTMENT_ID, right_id, Priority = MIN(priority)
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.DEPARTMENT_ID = r1.DEPARTMENT_ID
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority > r1.priority)

	group by operator_id, DEPARTMENT_ID, right_id