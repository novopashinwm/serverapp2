update bs
	set StartDate = lt.InsertTime	
	from Billing_Service_Type bst
	join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
	join Asid a on a.ID = bs.Asid_ID
	join MLP_Controller mc on mc.Asid_ID = a.ID
	join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
	cross apply (
		select top(1) lt.InsertTime
			from Log_Time lt
			where lt.Vehicle_ID = c.VEHICLE_ID
			  and lt.InsertTime is not null
			order by lt.InsertTime asc) lt
	where bst.Service_Type = 'APN:nika.msk'
	  and lt.InsertTime < bs.StartDate 
	  and not exists (
		select * 
			from H_Billing_Service h
			where h.Asid_ID = a.ID
			  and h.ID < bs.ID)