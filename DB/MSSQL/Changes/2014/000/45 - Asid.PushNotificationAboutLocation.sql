if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Asid')
		  and name = 'PushNotificationAboutLocation'
)
begin

	alter table Asid
		add PushNotificationAboutLocation bit not null
			constraint DF_Asid_PushNotificationAboutLocation
				default(0)

	alter table H_Asid
		add PushNotificationAboutLocation bit not null
			constraint DF_H_Asid_PushNotificationAboutLocation
				default(0)

end