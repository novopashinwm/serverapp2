/*	��������� ��� ������� "�������� ��-001" � �������� �� ������� � Reliz. 0030:
	����������� ������� �������
*/

begin tran

declare @type_name varchar(100) = 'ASC6 GLONASS/GPS'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table 
(
	Descript	nvarchar(512),
	Number		int not null,
	IsDigital	bit not null
);

set nocount on

insert into @controller_sensor (IsDigital, Descript, Number)
          select 1, 'Reboot', 0
union all select 0, 'Sim', 1
union all select 1, 'Connection', 2
union all select 1, 'Guard', 3
union all select 1, 'LowVoltage', 4
union all select 1, 'Motion', 6
union all select 1, 'OuterPower', 7
union all select 1, 'Alarm', 8
union all select 1, 'GpsAntennaCut', 9
union all select 1, 'GpsAntennaShortCircuit', 10
union all select 1, 'GlonassSatellites', 11
union all select 0, 'Acc', 16
union all select 0, 'Hdop', 17
union all select 0, 'PowerVoltage', 18
union all select 0, 'BatteryVoltage', 19
union all select 0, 'Vibration', 20
union all select 0, 'VibrationCount', 21
union all select 1, 'Out0', 30
union all select 1, 'Out1', 31
union all select 1, 'Out2', 32
union all select 1, 'Out3', 33
union all select 1, 'InputAlarm0', 40
union all select 1, 'InputAlarm1', 41
union all select 1, 'InputAlarm2', 42
union all select 1, 'InputAlarm3', 43
union all select 1, 'InputAlarm4', 44
union all select 1, 'InputAlarm5', 45
union all select 1, 'InputAlarm6', 46
union all select 1, 'InputAlarm7', 47
union all select 0, 'In0', 50
union all select 0, 'In1', 51
union all select 0, 'In2', 52
union all select 0, 'In3', 53
union all select 0, 'In4', 54
union all select 0, 'In5', 55
union all select 1, 'Counter0', 60
union all select 1, 'Counter1', 61
union all select 1, 'FuelLevel0', 70
union all select 1, 'FuelLevel1', 71
union all select 1, 'FuelLevel2', 72
union all select 1, 'Temperature0', 80
union all select 1, 'Temperature1', 81
union all select 1, 'Temperature2', 82

declare @canI int = 0
while @canI < 64
begin
	insert into @controller_sensor (
			                             Descript,      Number, IsDigital) 
		select 'CAN' + CONVERT(varchar(2), @canI), 100 + @canI,         0
	set @canI = @canI + 1
end

set nocount off

print 'insert into dbo.Controller_Sensor'

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	Descript)
select 
	@controller_type_id,
	case IsDigital when 1 then 2 else 1 end,
	NUMBER,
	Descript
	from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = @controller_type_id
						  and cs.Number = t.Number)

commit