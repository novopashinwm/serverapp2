if exists (
	select * from sys.objects where name = 'v_command'
)
	drop view v_command;
go

create view v_command as
	select 
	    c.ID
	 ,  [Type] = isnull(t.code, CONVERT(varchar, c.Type_ID))
	 ,  c.Target_ID
	 ,	CommandReceivedDateMsk = DATEADD(HOUR, 4, c.Date_Received)
	 , 	ResultReceivedDateMsk = DATEADD(HOUR, 4, cr.Date_Received)
	 , 	cr.Result_Type_ID
	 ,  CmdResult = case cr.Result_Type_ID
						when 0 then 'Received'
						when 1 then 'Processing'
						when 2 then 'Completed'                                              
						when 3 then 'Failed'                                               
						when 4 then 'Cancelled'
						when 5 then 'VehicleAccessDisallowed'
						when 6 then 'MLPTemporaryDisallowed'
						when 7 then 'AbsentSubscriber'
						when 8 then 'TimeoutExceeded'
						when 9 then 'NoTargetLBSService'
						when 10 then 'TargetLBSServiceIsBlocked'
						when 11 then 'NoCallerLBSService'
						when 12 then 'CallerLBSServiceIsBlocked'
						when 13 then 'MLPRequestCountPerTimespanExceeded'
						when 14 then 'CapacityLimitReached'
						when 15 then 'DeviceNotFoundInReceversList'
						when 16 then 'TerminalManagerIsNull'
						when 17 then 'SendCommandDeviceException'
						when 18 then 'ExecutionTimePassed'
						when 19 then 'InsufficientFunds'
						when 20 then 'NoService'
						when 21 then 'ServiceIsBlocked'
						when 22 then 'NoTarifficationService'
						when 23 then 'PhoneNumberIsUnknown'
						when 24 then 'NetworkIsOverloaded'
						when 25 then 'UnknownCell'
						when 26 then 'LbsDisallowedByLocalRegulations'
						when 27 then 'Inaccessible'
						when 28 then 'NotSupported'
						when 29 then 'OtherCommandIsPending'
						when 30 then 'Throttled'
			else CONVERT(varchar, cr.Result_Type_ID)
		end
		, c.Sender_ID
		, Sender_Login = o.Login
		, Sender_Name  = o.Name
		, Target_Name = v.GARAGE_NUMBER
	from Command c
	left outer join CommandTypes t on t.id = c.Type_ID
	left outer join Command_Result cr on cr.Command_ID = c.ID
	left outer join Vehicle v on v.VEHICLE_ID = c.Target_ID
	left outer join Operator o on o.OPERATOR_ID = c.Sender_ID