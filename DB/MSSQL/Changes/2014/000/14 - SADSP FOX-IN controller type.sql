/*��������� ��� ������� Avtofon
	����������� ������� �������
*/

begin tran

declare @type_name varchar(100) = 'SADSP FOX-IN'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 1, 1, 40000,   0, 0, 32, 'MainPowerVoltage'		);
insert into @controller_sensor values (@controller_type_id, 1, 2, 40000,   0, 0, 32, 'WaterLevelSensorVoltage'		);

insert into @controller_sensor values (@controller_type_id, 2, 3, 1, 0, 0, 1, 'Ignition');
insert into @controller_sensor values (@controller_type_id, 2, 4, 1, 0, 0, 1, 'Input1TripStartKey');
insert into @controller_sensor values (@controller_type_id, 2, 5, 1, 0, 0, 1, 'Input2WaterDispenseKey');
insert into @controller_sensor values (@controller_type_id, 2, 6, 1, 0, 0, 1, 'Input3SOSKey');
insert into @controller_sensor values (@controller_type_id, 2, 7, 1, 0, 0, 1, 'Output1');
insert into @controller_sensor values (@controller_type_id, 2, 8, 1, 0, 0, 1, 'Output2');
insert into @controller_sensor values (@controller_type_id, 2, 9, 1, 0, 0, 1, 'Trip');
insert into @controller_sensor values (@controller_type_id, 2,10, 1, 0, 0, 1, 'Panic');


set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

print 'Assign default sensors'

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID,
		Default_Multiplier = case cs.Descript when 'MainPowerVoltage' then 0.001 else 1 end,
		Default_Constant = 0,
		Mandatory = 1
	from CONTROLLER_SENSOR cs
	join CONTROLLER_SENSOR_LEGEND legend on 
		legend.Number = case cs.Descript 
							when 'Ignition' then 4
							when 'Input3SOSKey' then 6
							when 'MainPowerVoltage' then 1
						end
	where cs.CONTROLLER_TYPE_ID = @controller_type_id
	  and cs.Default_Sensor_Legend_ID is null
	  
update cs
	set Default_Multiplier = 0.001,
		Default_Constant = 0,
		Mandatory = 0
	from CONTROLLER_SENSOR cs
	where cs.CONTROLLER_TYPE_ID = @controller_type_id
	  and cs.Descript = 'WaterLevelSensorVoltage'
	  and (cs.Default_Multiplier is null or cs.Default_Multiplier <> 0.001)

commit