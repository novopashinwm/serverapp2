if exists (
	select *
		from sys.columns
		where object_id = object_id('Phone_Confirmation')
		  and name = 'Creation_Date'
		  and is_nullable = 1
)
begin

	alter table Phone_Confirmation alter column Creation_Date datetime not null

end
go

if exists (
	select *
		from sys.columns
		where object_id = object_id('Phone_Confirmation')
		  and name = 'Phone_ID'
		  and is_nullable = 1
)
begin

	alter table Phone_Confirmation alter column Phone_ID int not null

end
go

if not exists (
	select *
		from sys.columns
		where object_id = object_id('Phone_Confirmation')
		  and name = 'ExpirationDate'
)
begin

	alter table Phone_Confirmation
		add ExpirationDate datetime
				
	exec sp_sqlexec N'update Phone_Confirmation set ExpirationDate = dateadd(minute, 30, Creation_Date)'
	
	alter table Phone_Confirmation
		alter column ExpirationDate datetime not null	

end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Phone_Confirmation_Phone_ID_ExpirationDate'
)
begin
	create nonclustered index IX_Phone_Confirmation_Phone_ID_ExpirationDate
		on Phone_Confirmation(Phone_ID, ExpirationDate);
end
go

if exists (
	select *
		from sys.columns
		where object_id = object_id('Email_Confirmation')
		  and name = 'Email_ID'
		  and is_nullable = 1
)
begin

	alter table Email_Confirmation alter column Email_ID int not null

end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Email_Confirmation_Email_ID'
)
begin
	create nonclustered index IX_Email_Confirmation_Email_ID
		on Email_Confirmation(Email_ID);
end
go
