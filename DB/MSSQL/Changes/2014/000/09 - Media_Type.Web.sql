insert into MEDIA_TYPE 
	select *
		from (	  select ID = 7, Name = 'Web'
		union all select ID = 8, Name = 'TCP') t
		where not exists (select * from MEDIA_TYPE e where e.ID = t.ID)

update MEDIA
	set TYPE = 8
	where NAME = 'TCP'
	  and TYPE <> 8