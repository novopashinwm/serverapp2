if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Controller_Sensor_Controller_Type_ID'
)
begin

	create nonclustered index IX_Controller_Sensor_Controller_Type_ID
		on Controller_Sensor(Controller_Type_ID)

end
