if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_ZoneGroup_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_ZoneGroup_CompoundRule_ID
		on CompoundRule_ZoneGroup (CompoundRule_ID)

end