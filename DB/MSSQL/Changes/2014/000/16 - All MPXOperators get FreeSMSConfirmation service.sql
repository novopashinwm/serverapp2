insert into billing_service (Billing_Service_Type_ID, StartDate, Operator_ID)
	select bst.ID, getutcdate(), a.Operator_ID
		from billing_service_type bst,
		     Asid a
		where bst.Service_Type = 'Free.SMS.Confirmation'
		  and a.Operator_ID is not null
		  and not exists (select * from billing_service e where e.Operator_ID = a.Operator_ID
		                                                and e.Billing_Service_Type_ID = bst.ID)
    		      
