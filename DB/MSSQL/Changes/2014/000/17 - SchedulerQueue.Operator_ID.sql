if not exists (
	select *
		from sys.columns
		where object_id = object_id('SchedulerQueue')
		  and name = 'Operator_ID')
begin

	alter table SchedulerQueue
		add Operator_ID int 
			constraint FK_SchedulerQueue_Operator_ID 
				foreign key references Operator(Operator_ID)
	
	alter table H_SchedulerQueue
		add Operator_ID int
		
	create nonclustered index IX_SchedulerQueue_Operator_ID_Vehicle_ID on SchedulerQueue(Operator_ID, Vehicle_ID)
	
	create nonclustered index IX_SchedulerQueue_Operator_ID_VehicleGroup_ID on SchedulerQueue(Operator_ID, VehicleGroup_ID)
	
end

go

;with body as (
		select 
			sq.SCHEDULERQUEUE_ID
			, x = CONVERT(xml, sq.CONFIG_XML).query('
								declare namespace SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/";
								declare namespace a2="http://schemas.microsoft.com/clr/ns/System.Collections";
								/SOAP-ENV:Envelope/SOAP-ENV:Body/*')
			from SCHEDULERQUEUE sq
			join SCHEDULEREVENT se on se.SCHEDULEREVENT_ID = sq.SCHEDULEREVENT_ID
			where se.COMMENT = '������ ��������� ����� MLP'
)
, hashtable as (
	select body.SCHEDULERQUEUE_ID
		 , paramsHashtable.keysHref
		 , paramsHashtable.valuesHref
		from body
		cross apply (
			select Href = body.x.query('
				declare namespace a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.Common/FORIS.TSS.Common.Utils";
				a1:PARAMS/DictionaryBase_x002B_hashtable')
				.value('(DictionaryBase_x002B_hashtable/@href)[1]', 'nvarchar(max)')) paramsHashtableHref
		cross apply (
			select id = body.x.value('declare namespace a2="http://schemas.microsoft.com/clr/ns/System.Collections";
									  (a2:Hashtable/@id)[1]', 'nvarchar(max)')
				 , keysHref = body.x.value('declare namespace a2="http://schemas.microsoft.com/clr/ns/System.Collections";
									  (a2:Hashtable/Keys/@href)[1]', 'nvarchar(max)')
				 , valuesHref = body.x.value('declare namespace a2="http://schemas.microsoft.com/clr/ns/System.Collections";
									  (a2:Hashtable/Values/@href)[1]', 'nvarchar(max)')
		) paramsHashtable
		where paramsHashtable.id = SUBSTRING(paramsHashtableHref.Href, 2, LEN(paramsHashtableHref.Href)-1)
)
, array as (
		select body.SCHEDULERQUEUE_ID
			 , arrayId = array.col.value('@id', 'nvarchar(max)')
			 , item.*
			from body
			cross apply body.x.nodes('declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";
							   SOAP-ENC:Array') as array(col)
			cross apply (
				select   n     = item.col.value('for $i in . return count(../*[. << $i]) + 1', 'int')
				       , value = item.col.value('.', 'nvarchar(max)')
					from array.col.nodes('item') as item(col)
			) item
)
update sq set Operator_ID = CONVERT(int, v.value)
	from hashtable ht 
	join array k on '#' + k.arrayId = ht.keysHref	and k.SCHEDULERQUEUE_ID = ht.SCHEDULERQUEUE_ID
	join array v on '#' + v.arrayId = ht.valuesHref	and v.SCHEDULERQUEUE_ID = ht.SCHEDULERQUEUE_ID
	join SCHEDULERQUEUE sq on sq.SCHEDULERQUEUE_ID = ht.SCHEDULERQUEUE_ID
	where k.n = v.n
	  and k.value = 'OPERATORID'
	  and sq.Operator_ID is null
