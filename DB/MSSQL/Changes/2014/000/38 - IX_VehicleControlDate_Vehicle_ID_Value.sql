if not exists (
	select *
		from sys.indexes 
		where name = 'IX_VehicleControlDate_Vehicle_ID_Value')
begin

	create nonclustered index IX_VehicleControlDate_Vehicle_ID_Value
		on VehicleControlDate(Vehicle_ID, Value) include ([Type_ID])
		
end