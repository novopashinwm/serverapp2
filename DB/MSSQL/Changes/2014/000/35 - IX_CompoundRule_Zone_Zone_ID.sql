if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Zone_Zone_ID'
)
begin

	create nonclustered index IX_CompoundRule_Zone_Zone_ID
		on CompoundRule_Zone (Zone_ID) include (CompoundRule_ID)

end