insert into Billing_Service_Type (Service_Type, Service_Type_Category, MaxQuantity, LimitedQuantity, Singleton, Name, Removable)
	select * from (
		select 
			  Service_Type = 'FRNIKAVideo' + convert(varchar(max), q.Value)
			, Service_Type_Category = 'Video'
			, MaxQuantity = q.Value
			, LimitedQuantity = 0
			, Singleton = 0
			, Name = '���� ����� ' + convert(varchar(max), q.Value),
			  Removable = 1
		FROM (select Value = 1 union all select 7 union all select 30) q
	) t
	where not exists (
		select * 
			from Billing_Service_Type e
			where e.Service_Type = t.Service_Type
	)
