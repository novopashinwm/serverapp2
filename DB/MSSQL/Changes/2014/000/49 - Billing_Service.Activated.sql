if not exists (
	select *
		from sys.columns
		where object_id = object_id('Billing_Service')
		  and name = 'Activated')
begin

	alter table Billing_Service
		add Activated bit not null
			constraint FK_Billing_Service_Activated 
				default (1)

	alter table H_Billing_Service
		add Activated bit not null
			constraint FK_H_Billing_Service_Activated 
				default (1)
		
end