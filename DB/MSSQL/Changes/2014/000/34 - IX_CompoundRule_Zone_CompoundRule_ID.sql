if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Zone_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_Zone_CompoundRule_ID
		on CompoundRule_Zone (CompoundRule_ID)

end