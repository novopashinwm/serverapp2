	update a
		set Department_ID = v.DEPARTMENT
	from Asid a
	join MLP_Controller mc on mc.Asid_ID = a.ID
	join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	where v.DEPARTMENT is not null
	  and a.Department_ID is null
	  