if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Command_Sender_ID_Type_ID_Date_Received'
)
begin

	create nonclustered index IX_Command_Sender_ID_Type_ID_Date_Received
		on Command (Sender_ID, Type_ID, Date_Received)	

end