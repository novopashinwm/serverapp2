/*	��������� ��� ������� "�������� ��-001" � �������� �� ������� � Reliz. 0030:
	����������� ������� �������
*/

begin tran

declare @type_name varchar(100) = 'Mercury'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	NUMBER						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 32,  1,'SpeedTaho'		);
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 32,  2,'SpeedPulse'		);
																				  
insert into @controller_sensor values (@controller_type_id, 2,	   1,   0, 0,  1,  3,'Ignition');
insert into @controller_sensor values (@controller_type_id, 2,	   1,   0, 0,  1,  4,'Panic');
insert into @controller_sensor values (@controller_type_id, 2,     1,   0, 0,  1,  5,'Tamper');
insert into @controller_sensor values (@controller_type_id, 1,    -1,   0, 0, 64,  6,'FuelFromCAN');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 11,'ADC1');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 12,'ADC2');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 13,'ADC3');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 14,'ADC4');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 15,'ADC5');
insert into @controller_sensor values (@controller_type_id, 1, 65535,   0, 0, 16, 16,'ADC6');


set nocount off

print 'insert into dbo.Controller_Sensor'

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript
	from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

print 'Assign default sensors'

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID,
		Default_Multiplier = 1,
		Default_Constant = 0,
		Mandatory = 1
	from CONTROLLER_SENSOR cs
	join CONTROLLER_SENSOR_LEGEND legend on 
		legend.Number = case cs.Descript 
							when 'Ignition' then 4
							when 'Panic' then 6							
						end
	where cs.CONTROLLER_TYPE_ID = @controller_type_id
	  and cs.Default_Sensor_Legend_ID is null
  

commit