delete dup
	from Billing_Service_Type bst
	join Asid a on a.ID in (select bs.Asid_ID from Billing_Service bs where bs.Billing_Service_Type_ID = bst.ID)
	cross apply (
		select top(1) *
			from Billing_Service bs
			where bs.Asid_ID = a.ID
			  and bs.Billing_Service_Type_ID = bst.ID
			order by bs.ID asc) bs
	join Billing_Service dup on dup.Asid_ID = a.ID
	                        and dup.Billing_Service_Type_ID = bst.ID
	                        and dup.ID <> bs.ID
	where bst.Service_Type_Category = 'Video'

delete h
from H_Billing_Service h 
where h.ID in (
	select adding.ID
		from Billing_Service_Type bst
		join H_Billing_Service adding on adding.Billing_Service_Type_ID = bst.ID and adding.ACTION = 'INSERT'
		where bst.Service_Type_Category = 'Video'
		  and not exists (select * from H_Billing_Service deleting where deleting.ID = adding.ID and deleting.ACTION = 'DELETE')
		  and not exists (select * from Billing_Service bs where bs.ID = adding.ID)
)

update bst set Singleton = 1, Removable = 1 
from Billing_Service_Type bst
where Service_Type_Category = 'Video' and Singleton = 0