insert into OPERATORGROUP_OPERATOR
	select og.OperatorGroup_ID, a.Operator_ID
		from OPERATORGROUP og, Asid a
		where og.NAME = 'MPXOperators' 
		  and a.Value is not null
		  and a.Operator_ID is not null
		  and not exists (
			select * 
				from OPERATORGROUP_OPERATOR e
				where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID
				  and e.OPERATOR_ID = a.Operator_ID)