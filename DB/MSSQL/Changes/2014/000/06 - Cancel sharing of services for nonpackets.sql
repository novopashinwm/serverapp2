update bst
	set Shared = 0
	from billing_service_type bst
	where Shared = 1
	  and (MaxQuantity is null or MaxQuantity = 40)
	