declare @templateName nvarchar(50) = 'DemaskAsidRequest'

begin tran

insert into dbo.Message_Template(Name)
	select t.Name 
		from (
			  select Name = @templateName
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
		
insert into MESSAGE_TEMPLATE_FIELD (DOTNET_TYPE_ID, NAME, MESSAGE_TEMPLATE_ID)
	select t.*, mt.Message_Template_ID
		from (
			select DOTNET_TYPE_ID, Name = 'RequestUid'
				from DOTNET_TYPE 
				where TYPE_NAME = 'System.String') t
		join Message_Template mt on mt.Name = @templateName
		where not exists (	
			select * 
				from MESSAGE_TEMPLATE_FIELD e 
				where e.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID 
				  and e.NAME = t.Name)
		
		
commit