if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_Speed_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_Speed_CompoundRule_ID
		on CompoundRule_Speed (CompoundRule_ID)

end