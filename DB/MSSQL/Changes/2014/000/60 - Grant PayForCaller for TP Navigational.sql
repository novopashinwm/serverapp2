insert into OPERATORGROUP_DEPARTMENT
select og.OPERATORGROUP_ID, d.DEPARTMENT_ID, r.RIGHT_ID, 1
	from Department d
	join OperatorGroup og on 
		exists (
			select * 
				from OPERATORGROUP_DEPARTMENT ogd 
				where ogd.DEPARTMENT_ID = d.DEPARTMENT_ID 
				  and ogd.OPERATORGROUP_ID = og.OPERATORGROUP_ID 
				  and ogd.RIGHT_ID = 104 /*DepartmentsAccess*/
				  and ogd.ALLOWED = 1)
		and not exists (
			select * 
				from OPERATORGROUP_DEPARTMENT ogd 
				where ogd.DEPARTMENT_ID = d.DEPARTMENT_ID 
				  and ogd.OPERATORGROUP_ID = og.OPERATORGROUP_ID 
				  and ogd.RIGHT_ID = 2 /*SecurityAdministration*/
				  and ogd.ALLOWED = 1)					  
	join [RIGHT] r on r.NAME = 'PayForCaller'
	where (d.Type is null or d.Type = 0)
	  and not exists (
		select *
			from OPERATORGROUP_DEPARTMENT e
			where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID
			  and e.DEPARTMENT_ID = d.DEPARTMENT_ID
			  and e.RIGHT_ID = r.RIGHT_ID)