declare @type_name nvarchar(50) = 'Teltonika FM5500'

insert into dbo.Controller_Type (type_name, packet_length, pos_absence_support, pos_smooth_support, allowedToAddByCustomer)
select type_name, 1000, null, 0, 1
	from (
		select Type_Name = @type_name
	) t
	where not exists (select 1 from dbo.Controller_Type ct where ct.Type_Name = t.Type_Name)
	
	
	insert into Controller_Sensor (
					  CONTROLLER_TYPE_ID
					, CONTROLLER_SENSOR_TYPE_ID
					, NUMBER
					, MAX_VALUE
					, MIN_VALUE
					, DEFAULT_VALUE
					, BITS
					, DESCRIPT
					, VALUE_EXPIRED
					, Default_Sensor_Legend_ID
					, Default_Multiplier
					, Default_Constant
					, Mandatory)
		select 
		  newCt.CONTROLLER_TYPE_ID
		, cs.CONTROLLER_SENSOR_TYPE_ID
		, cs.NUMBER
		, cs.MAX_VALUE
		, cs.MIN_VALUE
		, cs.DEFAULT_VALUE
		, cs.BITS
		, cs.DESCRIPT
		, cs.VALUE_EXPIRED
		, cs.Default_Sensor_Legend_ID
		, cs.Default_Multiplier
		, cs.Default_Constant
		, cs.Mandatory	
		from controller_type ct 
		join controller_sensor cs on cs.controller_type_id = ct.controller_type_id
		join Controller_Type newCt on newCt.Type_Name = @type_name
		where ct.type_name = 'Teltonika FM5300'
		  and not exists (select * from controller_sensor e where e.Controller_Type_ID = newCt.CONTROLLER_TYPE_ID and e.Number = cs.Number)
		
		
		