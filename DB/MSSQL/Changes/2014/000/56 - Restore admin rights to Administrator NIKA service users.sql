insert into OPERATOR_DEPARTMENT 
	select o.OPERATOR_ID, d.DEPARTMENT_ID, r.RIGHT_ID, 1
	from Billing_Service bs
	join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
	join Asid a on a.ID = bs.Asid_ID
	join DEPARTMENT d on d.DEPARTMENT_ID = a.Department_ID
	join [RIGHT] r on r.NAME = 'SecurityAdministration'
			       or r.NAME = 'ManageSensors' and d.Type = 0
	join OPERATOR o on o.OPERATOR_ID = a.Operator_ID
	where bst.Service_Type_Category = 'NikaAdmin'
	  and not exists (
		select * 
			from OPERATOR_DEPARTMENT e 
			where e.OPERATOR_ID = o.OPERATOR_ID 
			  and e.DEPARTMENT_ID = d.Department_ID
			  and e.RIGHT_ID = r.RIGHT_ID)