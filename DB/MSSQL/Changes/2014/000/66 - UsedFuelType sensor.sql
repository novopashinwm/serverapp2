insert into CONTROLLER_SENSOR_LEGEND (CONTROLLER_SENSOR_TYPE_ID, NAME, Number)
	select 2, t.Name, t.Number
		from (
			select Name = 'UsedFuelType',		Number = 71
		) t
		where not exists (select * from CONTROLLER_SENSOR_LEGEND e where e.Number = t.Number)
