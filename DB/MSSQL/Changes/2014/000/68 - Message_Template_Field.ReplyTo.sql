insert into MESSAGE_TEMPLATE_FIELD (DOTNET_TYPE_ID, NAME)
	select *
		from (
			select DOTNET_TYPE_ID, Name = 'ReplyTo' 
				from DOTNET_TYPE 
				where TYPE_NAME = 'System.String') t
		where not exists (select * from MESSAGE_TEMPLATE_FIELD e where e.MESSAGE_TEMPLATE_ID is null and e.NAME = t.Name)