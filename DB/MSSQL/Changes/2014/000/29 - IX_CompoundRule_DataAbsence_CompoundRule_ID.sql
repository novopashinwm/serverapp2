if not exists (
	select *
		from sys.indexes where name = 'IX_CompoundRule_DataAbsence_CompoundRule_ID'
)
begin

	create nonclustered index IX_CompoundRule_DataAbsence_CompoundRule_ID
		on CompoundRule_DataAbsence (CompoundRule_ID)

end