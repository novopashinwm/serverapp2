if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller_Type')
		  and name = 'DeviceIdIsRequiredForSetup'
)
begin

	alter table Controller_Type
		add DeviceIdIsRequiredForSetup bit not null
			constraint DF_Controller_Type_DeviceIdIsRequiredForSetup default(0)
	
	alter table H_Controller_Type
		add DeviceIdIsRequiredForSetup bit not null
			constraint DF_H_Controller_Type_DeviceIdIsRequiredForSetup default(0)
	
end
go

update Controller_Type
	set DeviceIdIsRequiredForSetup = 1
	where TYPE_NAME in ('GlobalSat GTR-128', 'GlobalSat TR-102', 'GlobalSat TR-203', 'GlobalSat TR-600')
	  and DeviceIdIsRequiredForSetup <> 1
