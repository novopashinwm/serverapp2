declare @file_prefix nvarchar(max) = '/common/images/'
declare @property_name nvarchar(max) = 'icon'

update i
	set value = 
	SUBSTRING(
		i.value,  
		charINDEX(@file_prefix, i.value, 0) + LEN(@file_prefix),
		LEN(i.value) - LEN(@file_prefix) - charINDEX(@file_prefix, i.value) + 1)  
from VEHICLE_PROFILE i
where i.PROPERTY_NAME = @property_name
  and charINDEX(@file_prefix, i.value, 0) <> 0