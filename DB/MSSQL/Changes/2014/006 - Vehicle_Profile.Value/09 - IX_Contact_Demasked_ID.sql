if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Contact_Demasked_ID'
)
begin

	create nonclustered index IX_Contact_Demasked_ID on Contact(Demasked_ID)

end		
