if 1 = (
	select is_nullable
		from sys.columns 
		where object_id = object_id('Command')
		  and name = 'Target_ID')
begin

	drop index IX_Command_Target_ID_Type_ID_Result_Date on Command
	drop index IX_Command_Target_Date on Command
	

	alter table Command
		alter column Target_ID int not null

	create nonclustered index IX_Command_Target_ID_Type_ID_Result_Date on dbo.Command(Target_ID, Type_ID, Date_Received)

end
	  