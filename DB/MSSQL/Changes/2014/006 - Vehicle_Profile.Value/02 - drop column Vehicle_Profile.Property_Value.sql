if exists (
	select *
		from sys.columns
		where object_id = object_id('Vehicle_Profile')
		  and name = 'Property_Value'
)
begin

	alter table Vehicle_Profile
		drop column Property_Value

end