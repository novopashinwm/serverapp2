update bst
	set MaxQuantity = 60
from Billing_Service_Type bst
where Service_Type in ('FRNIKA.GPS.Phone', 'FRNIKA.GPS.Phone.Trial', 'FRNIKA.MLP.ChargingEconom')
  and MaxQuantity = 40
  
update bs
	set MaxQuantity = bst.MaxQuantity
	from Billing_Service bs
	join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
	where bst.Service_Type in ('FRNIKA.GPS.Phone', 'FRNIKA.GPS.Phone.Trial', 'FRNIKA.MLP.ChargingEconom')
	  and bs.MaxQuantity < bst.MaxQuantity

update bst
	set MinIntervalSeconds = 15*60
from Billing_Service_Type bst
where Service_Type in ('FRNIKA.MLP.ChargingNolimits', 'FRNIKA.GPS.Modem')
  and MinIntervalSeconds = 20*60
  