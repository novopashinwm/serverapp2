insert into controller_type (Type_Name, AllowedToAddByCustomer, Default_Vehicle_Kind_ID, DeviceIdIsImei, DeviceIdIsRequiredForSetup)
	select t.name, 1, vk.Vehicle_Kind_ID, 1, 1
		from (select name = 'ATOL', Vehicle_Kind_Name = N'грузовой'
              ) t
        join VEHICLE_KIND vk on vk.NAME = t.Vehicle_Kind_Name
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
