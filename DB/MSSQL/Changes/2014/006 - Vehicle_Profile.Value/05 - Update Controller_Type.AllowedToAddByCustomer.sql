update ct
	set AllowedToAddByCustomer = 0
	from CONTROLLER_TYPE ct
	where ct.TYPE_NAME in ('Emulator', 'Sygic', 'S60V3FP' , 'RemoteServer', 'MLP')
	  and ct.AllowedToAddByCustomer = 1