if exists (
	select c.name, t.*
		from sys.columns c
		join sys.types t on t.system_type_id = c.system_type_id
		where c.object_id = object_id('Vehicle_Profile')
		  and c.name = 'Property_Value'
		  and t.name = 'image'
)
begin

	alter table Vehicle_Profile
		add Value nvarchar(max)

	exec sp_executesql N'		
	update VEHICLE_PROFILE 
		set Value = CONVERT(nvarchar(max),
						convert(varchar(max),
							convert(varbinary(max),
								Property_Value)))
		where Value is null
		  and PROPERTY_VALUE is not null
	'
	
	alter table Vehicle_Profile
		drop column Property_Value
	
end
