if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller_Type')
		  and name = 'LocatorGsmTimeout'
)
begin

	alter table Controller_Type
		add LocatorGsmTimeout int

end

go

update Controller_Type
	set LocatorGsmTimeout = case when Type_Name = 'Smarts Baby Bear' then 1 else 5 * 60 end
	where (Type_Name like 'Smarts %' or Type_Name = 'Nika.Tracker')
	  and LocatorGsmTimeout is null