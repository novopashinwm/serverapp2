update ct 
	set SupportsPositionAsking = 1
	from CONTROLLER_TYPE ct
	where ct.SupportsPositionAsking = 0
	  and (ct.TYPE_NAME like 'Smarts %'
	    or ct.TYPE_NAME = 'Nika.Tracker'
	    or ct.TYPE_NAME = 'MLP'
	    or ct.TYPE_NAME like 'GlobalSat%'
	    or ct.TYPE_NAME like 'AvtoGraf%')
	    