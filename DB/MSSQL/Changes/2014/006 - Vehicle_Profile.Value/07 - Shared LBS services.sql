update bst 
	set Shared = 1
	from Billing_Service_Type bst
	where (
	      service_type = 'FRNIKA.MLP.ChargingNolimits'
	   or service_type = 'FRNIKA.MLP.ChargingNolimits.Trial')
		and shared = 0
		
update bst 
	set Shared = 0
	from Billing_Service_Type bst
	where (
	      service_type = 'FRNIKA.MLP.ChargingNolimits'
	   or service_type = 'FRNIKA.MLP.ChargingNolimits.Trial')
		and shared = 1