if not exists (
	select *	
		from sys.columns
		where object_id = object_id('Billing_Service_Type')
		  and name = 'DepartmentType'
)
begin

	alter table Billing_Service_Type
		add DepartmentType int

end
go

update Billing_Service_Type
	set DepartmentType = 0
	where Service_Type in ('FRNIKA.Admin', 'FRNIKA.MLP', 'FRNIKA.MLP.Charging', 'FRNIKA.GPS.Modem', 'FRNIKA.GPS.Phone', 'FRNIKAVideo1', 'FRNIKAVideo7', 'FRNIKAVideo30')
	  and (DepartmentType is null or DepartmentType <> 0)
	
update Billing_Service_Type
	set DepartmentType = 1
	where Service_Type in ('FRNIKA.GPS.Modem.2014', 'FRNIKA.GPS.Phone.2014')
	  and (DepartmentType is null or DepartmentType <> 1)	
	