if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('WLAN_MAC_Address')
		  and name = 'IsMobileHotSpot'
)
begin

	alter table WLAN_MAC_Address
		add IsMobileHotSpot bit not null
			constraint DF_WLAN_MAC_Address_IsMobileHotSpot default (0)

end
go

update mac
	set IsMobileHotSpot = 1
	from WLAN_SSID ssid
	join WLAN_MAC_Address mac on mac.ID in (select l.WLAN_MAC_Address_ID from WLAN_Log l where l.WLAN_SSID_ID = ssid.ID)
	where (
	      ssid.Value like 'mts-%' and len(ssid.Value) = 7 --��� ����� ������ � ������
	   or ssid.Value like '%Metro%'
	   or ssid.Value like '%megafon%'
	   or ssid.Value like '%beeline%'
	   or ssid.Value like '%mts%'
	   or ssid.Value like '%yota%'
	   or ssid.Value like '%portable%'
	   or ssid.Value like '%android%'
	   or ssid.Value like '%iphone%'
	   or ssid.Value like '%modem%'
	   or ltrim(rtrim(ssid.Value)) = ''
	   or mac.Value = '0015628D5AC0'
	) and mac.IsMobileHotSpot = 0
	  
go

if exists (
	select * from sys.tables where name = 'Wrong_Wlan_For_Locator'
)
begin

	drop table Wrong_Wlan_For_Locator

end