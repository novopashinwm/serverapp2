declare @type_name nvarchar(255) = 'TM140'

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	Descript,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Default_Constant,
	Mandatory
)
	select 
		  ct.Controller_Type_ID
		, cst.Controller_Sensor_Type_ID
		, t.Number
		, t.Descript
		, legend.Controller_Sensor_Legend_ID
		, 1
		, 0
		, t.Mandatory
		from (
			select      Type = '��������',	Number = 1,  Descript = 'Ignition',	Legend_Name = '���������', Mandatory = 1
			union all select   '��������',			 2,                'Alarm',				    '�������',			   0
			union all select '����������',	         3,         'PowerVoltage',      '���������� �������',	           0
			union all select '����������',	         4,                 'Fuel',          '������ �������',			   0
		) t
		join Controller_Type ct on ct.Type_Name = @type_name
		join Controller_Sensor_Type cst on cst.Name = t.Type
		join Controller_Sensor_Legend legend on legend.Name = t.Legend_Name
		where not exists (
			select *
				from Controller_Sensor e
				where e.Controller_Type_ID = ct.Controller_Type_ID
				  and e.Number = t.Number)		

