create table #t(vehicle_id int, IgnitionOff int, IgnitionOn int, IgnitionCount int)

declare @from  int = DATEDIFF(ss, '1970', GETUTCDATE()-3*12*30)
declare @to    int = DATEDIFF(ss, '1970', GETUTCDATE()-1)

declare @sensor_legend_id int = (
	select Controller_Sensor_Legend_ID 
		from CONTROLLER_SENSOR_LEGEND ignition 
		where ignition.NAME = '���������')

declare @vehicle_id int

while 1=1
begin

	set @vehicle_id = (
		select TOP(1) v.vehicle_id
			from Vehicle v
			join Controller c on c.VEHICLE_ID = v.VEHICLE_ID
			where v.vehicle_id not in (select t.vehicle_id from #t t)
			  and not exists (
				select *
					from CONTROLLER_SENSOR_MAP  m
					where m.CONTROLLER_ID = c.CONTROLLER_ID
					  and m.CONTROLLER_SENSOR_LEGEND_ID = @sensor_legend_id)
			  and exists (
				select *
					from Controller_Sensor cs 
					where cs.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
					  and cs.Default_Sensor_Legend_ID = @sensor_legend_id)
			order by v.vehicle_id asc
			)

	if @vehicle_id is null
		break

	insert into #t
		select top(1) c.VEHICLE_ID, sl.IgnitionOff, sl.IgnitionOn, sl.IgnitionCount
			from Controller c			
			outer apply (
				select IgnitionOn  = SUM(case when Ignition = 1 then 1 else 0 end)
					 , IgnitionOff = SUM(case when Ignition = 1 then 0 else 1 end)
					 , IgnitionCount=COUNT(1)
					from (
						select top(3600*24*7) 
							Ignition = case when (sl.DS & 32) = 0 then 1 else 0 end 
							from Sensors_Log sl (nolock)
							where sl.VEHICLE_ID = c.VEHICLE_ID
							  and sl.Log_Time between @from and @to 
							  and sl.DS is not null
							order by Log_Time desc
					) t
			) sl	
			where c.Vehicle_ID = @vehicle_id

end

declare @v dbo.Id_Param
insert into @v 
	select t.Vehicle_ID
		from #t t
		join Controller c on c.Vehicle_ID = t.Vehicle_ID
		join Controller_Info ci on ci.Controller_ID = c.Controller_ID
		where t.IgnitionCount <> 0
		  and t.IgnitionOff <> t.IgnitionCount and t.IgnitionOn <> t.IgnitionCount
		  and len(ci.Device_ID) > 0
		  and not exists (
			select *
				from CONTROLLER_SENSOR_MAP  m
				where m.CONTROLLER_ID = c.CONTROLLER_ID
				  and m.CONTROLLER_SENSOR_LEGEND_ID = @sensor_legend_id)

select * from @v

while 1=1
begin

	set @vehicle_id = (select top(1) id from @v v)
	if (@vehicle_id is null) break
	delete @v where id = @vehicle_id
	
	declare @number int
	set @number = (
		select cs.Number 
			from Controller c
			join Controller_Sensor cs on cs.Controller_Type_ID = c.Controller_Type_ID
			where cs.Default_Sensor_Legend_ID = @sensor_legend_id
			  and c.Vehicle_ID = @vehicle_id)
	
	if @number is null
		continue;

	--��� ������������� ����������������, ����� �� ������������ �������� ���������� �������	
	insert into Controller_Sensor_Log
		select @vehicle_id, l.Log_Time, @number, case when (l.DS & 32) = 0 then 1 else 0 end
			from Sensors_Log l
			where l.Vehicle_ID = @vehicle_id
			  and not exists (
				select * 
					from Controller_Sensor_Log e
					where e.Vehicle_ID = @vehicle_id
					  and e.Log_Time = l.Log_Time
					  and e.Number = @number)
	
	insert into Controller_Sensor_Map (Controller_ID, Controller_Sensor_Legend_ID, Controller_Sensor_ID, Multiplier, Constant)
		select c.Controller_ID
			 , cs.Default_Sensor_Legend_ID
			 , cs.Controller_Sensor_ID
			 , cs.Default_Multiplier
			 , cs.Default_Constant
			 from Controller c
			 join Controller_Type ct on ct.Controller_Type_ID = c.Controller_Type_ID
			 join Controller_Sensor cs on cs.Controller_Type_ID = ct.Controller_Type_ID
			 join Controller_Info ci on ci.Controller_ID = c.Controller_ID
			 where c.Vehicle_ID = @vehicle_id
			   and cs.Default_Sensor_Legend_ID = @sensor_legend_id
			   and not exists (
				select * 
					from Controller_Sensor_Map e 
					where e.Controller_ID = c.Controller_ID 
					  and e.Controller_Sensor_Legend_ID = cs.Default_Sensor_Legend_ID)
				  


end
			  
drop table #t