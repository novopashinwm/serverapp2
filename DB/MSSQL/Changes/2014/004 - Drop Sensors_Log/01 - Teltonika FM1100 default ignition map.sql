update cs
	set Default_Sensor_Legend_ID = legend.CONTROLLER_SENSOR_LEGEND_ID,
	    Default_Multiplier = 1,
	    Default_Constant = 0,
	    Mandatory = 1
	from CONTROLLER_TYPE ct
	join Controller_Sensor cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = 'Зажигание'
	where ct.TYPE_NAME = 'Teltonika FM1100'
	  and cs.NUMBER = 1
	  and cs.Default_Sensor_Legend_ID is null