if exists (
	select *
		from sys.views 
		where name = 'v_geo_log_change'
)
	drop view v_geo_log_change
go
  
create view v_geo_log_change  
as  
select 
	l.Vehicle_ID,  
    Prev_Log_Time = p.Log_Time,   
    Prev_Lat = p.Lat,  
    Prev_Lng = p.Lng,
    l.Log_Time,   
    l.Lat,
    l.Lng
 from Geo_Log l with (nolock)  
 cross apply (  
  select top(1) p.Log_Time, p.Lat, p.Lng
   from Geo_Log p with (nolock)  
   where p.Vehicle_ID = l.Vehicle_ID  
     and p.Log_Time < l.Log_Time  
   order by p.Log_Time desc) p  
 where (p.Lat <> l.Lat or p.Lng <> l.Lng)
union all  
 select v.VEHICLE_ID  
      , Prev_Log_Time = null  
      , Prev_Lat = null  
      , Prev_Lng = null
      , l.Log_Time  
      , l.Lat
      , l.Lng
 from Vehicle v   
 cross apply (  
  select top(1) l.Log_Time, l.Lat, l.Lng
   from Geo_Log l with (nolock)  
   where l.Vehicle_ID = v.VEHICLE_ID  
   order by l.Log_Time asc) l  