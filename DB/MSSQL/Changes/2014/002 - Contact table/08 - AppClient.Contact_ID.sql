if not exists (
	select *
		from sys.objects o 
		join sys.columns c on c.object_id = o.object_id
		where o.name = 'AppClient'
		  and c.name = 'Contact_ID')
begin

	alter table AppClient
		add Contact_ID int 
			constraint FK_AppClient_Contact_ID foreign key references Contact(ID)
			
	alter table H_AppClient
		add Contact_ID int

exec sp_executesql N'
	declare @android_contact_type		int = 6 --AppClient.GcmRegistrationId
	declare @apple_contact_type			int = 7 --AppClient.ApnToken

	update ac
		set Contact_ID = c.ID 
		from AppClient ac
		join Contact c on c.Value = ac.GcmRegistrationId and c.Type = @android_contact_type
		where ac.Contact_ID is null

	update ac
		set Contact_ID = c.ID 
		from AppClient ac
		join Contact c on c.Value = ac.ApnToken and c.Type = @apple_contact_type
		where ac.Contact_ID is null
'

end