if not exists (
	select *
		from sys.tables
		where name = 'Contact'
)
begin

	create table Contact 
	(
		ID int identity (1,1) 
			constraint PK_Contact
				primary key clustered,
		Type int not null,
		--������������ ������ - Google Cloud Message Registration Id
		Value nvarchar(255) not null,
		LockedCount int not null
			constraint DF_Contact_LockedCount default (0),
		LockedUntil datetime,
		
	)
	
	create unique nonclustered index IX_Contact on Contact (Type, Value)

	create table Operator_Contact
	(
		Operator_ID int not null
			constraint FK_Operator_Contact_Operator_ID
				foreign key references Operator(Operator_ID),
		Contact_ID int not null
			constraint FK_Operator_Contact_Contact_ID
				foreign key references Contact(ID),
		Confirmed bit not null
			constraint DF_Operator_Contact_Confirmed
				default (0),
		--��, ��� ������������ ���� ������ �������
		Value nvarchar(255),
		Name nvarchar(100),
		Description nvarchar(100),
		constraint PK_Operator_Contact primary key clustered (Operator_ID, Contact_ID)		
	)
			
	create nonclustered index IX_Operator_Contact_Contact_ID  on Operator_Contact (Contact_ID) 
	
	create table H_Operator_Contact
	(
		Operator_ID int not null,
		Contact_ID int not null,
		Confirmed bit not null,
		Value nvarchar(255),
		Name nvarchar(100),
		Description nvarchar(100),

		H_Operator_Contact_ID int identity(1,1)
			constraint PK_H_Operator_Contact
				primary key clustered,
		
        TRAIL_ID        int 
			constraint FK_H_Operator_Contact_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

end

