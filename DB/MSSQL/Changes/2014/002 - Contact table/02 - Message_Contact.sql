if not exists (
	select *
		from sys.objects
		where name = 'Message_Contact'
)
begin

	create table Message_Contact
	(
		Message_ID int not null constraint FK_Message_Contact_Message_ID foreign key references Message(Message_ID),
		--��� ��������: ����������, �����������, �����, ������� ����� etc
		Type int not null,
		Contact_ID int not null constraint FK_Message_Contact_Contact_ID foreign key references Contact(ID),
		constraint PK_Message_Contact primary key clustered (Message_ID, Type, Contact_ID)
	)
	
	create nonclustered index IX_Message_Contact_Contact_ID on Message_Contact (Contact_ID)
	create nonclustered index IX_Message_Contact_Type_Contact_ID on Message_Contact (Type, Contact_ID)

end	