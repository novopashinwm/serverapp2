begin tran

declare @email_contact_type			int = 1
declare @phone_contact_type			int = 2
declare @asid_contact_type			int = 3 --MTS MPX F(MSISDN) - Anonymous Subscriber IDentity
declare @controller_contact_type	int = 4 --Controller.Controller_ID
declare @operator_contact_type		int = 5 --Operator.Operator_ID
declare @android_contact_type		int = 6 --AppClient.GcmRegistrationId
declare @apple_contact_type			int = 7 --AppClient.ApnToken

insert into Contact (Type, Value)
	select t.* 
		from (
			select Type = 0, Value = N'' where 1=0
			union all select distinct @email_contact_type,		e.Email from Email e
			union all select distinct @phone_contact_type,		p.Phone from Phone p
			union all select distinct @asid_contact_type,		a.Value from Asid a
			union all select distinct @controller_contact_type,	convert(nvarchar(255), c.Controller_ID) from Controller c
			union all select distinct @operator_contact_type,   convert(nvarchar(255), o.Operator_ID) from Operator o
			union all select distinct @android_contact_type,	ac.GcmRegistrationId from AppClient ac
			union all select distinct @apple_contact_type,		ac.ApnToken from AppClient ac
		) t
		where t.Value is not null
		  and LEN(ltrim(rtrim(t.Value))) > 0
		  and not exists (select * from Contact e where e.Type = t.Type and e.Value = t.Value)
	
insert into Operator_Contact (Operator_ID, Contact_ID, Confirmed, Value, Name, Description)
	select e.Operator_ID, c.ID, e.Confirmed, e.EMAIL, e.Name, e.Comment
		from Email e
		join Contact c on c.Value = e.Email and c.Type = @email_contact_type
		where e.Operator_ID is not null		
		  and not exists (select * from Operator_Contact ex where ex.Operator_ID = e.Operator_ID and ex.Contact_ID = c.ID)

insert into Operator_Contact (Operator_ID, Contact_ID, Confirmed, Value, Name, Description)
	select p.Operator_ID, c.ID, p.Confirmed, p.Phone, p.Name, p.Comment
		from Phone p
		join Contact c on c.Value = p.Phone and c.Type = @phone_contact_type
		where p.Operator_ID is not null
		  and not exists (select * from Operator_Contact ex where ex.Operator_ID = p.Operator_ID and ex.Contact_ID = c.ID)

insert into Operator_Contact (Operator_ID, Contact_ID, Confirmed)
	select a.Operator_ID, c.ID, 1
		from Asid a
		join Contact c on c.Value = a.Value and c.Type = @asid_contact_type
		where a.Operator_ID is not null
		  and not exists (select * from Operator_Contact ex where ex.Operator_ID = a.Operator_ID and ex.Contact_ID = c.ID)
	
commit