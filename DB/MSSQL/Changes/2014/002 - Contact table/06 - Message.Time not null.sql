if (1 = (
	select c.is_nullable
		from sys.columns c
		join sys.objects o on o.object_id = c.object_id
		where o.name = 'Message'
		  and c.name = 'Time'))
begin

	update m
		set m.Time = isnull(h.Actual_Time, GETUTCDATE())
		from Message m
		outer apply (
			select top(1) h.Actual_Time
				from H_MESSAGE h
				where h.MESSAGE_ID = m.MESSAGE_ID
				order by h.ID desc) h			
		where m.Time is null
	
	update H_Message
		set Time = Actual_Time
		where Time is null

	drop index IX_Message_DestinationType_ID_Time on Message
	
	alter table Message
		alter column Time datetime not null

	create index IX_Message_DestinationType_ID_Time on Message(DestinationType_ID, TIME)

	alter table H_Message
		alter column Time datetime not null
		
end