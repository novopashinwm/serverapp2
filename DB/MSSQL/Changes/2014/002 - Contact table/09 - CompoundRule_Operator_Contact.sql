if not exists (
	select *
		from sys.objects o
		where o.name = 'CompoundRule_Operator_Contact'
)
begin

	create table CompoundRule_Operator_Contact
	(
		CompoundRule_ID int not null
			constraint FK_CompoundRule_Contact_CompoundRule_ID 
				foreign key references CompoundRule(CompoundRule_ID),
		Operator_ID int not null
			constraint FK_CompoundRule_Contact_Operator_ID
				foreign key references Operator(Operator_ID),
		Contact_ID int not null
			constraint FK_CompoundRule_Operator_Contact_ID
				foreign key references Contact(ID),
		constraint PK_CompoundRule_Operator_Contact
			primary key clustered (CompoundRule_ID, Contact_ID),
		constraint FK_CompoundRule_Operator_Contact_Operator_Contact
			foreign key (Operator_ID, Contact_ID) references Operator_Contact(Operator_ID, Contact_ID)
	)
	
	create nonclustered index IX_CompoundRule_Operator_Contact_Operator_ID_Contact_ID
		on CompoundRule_Operator_Contact (Operator_ID, Contact_ID)
	
	create table H_CompoundRule_Contact
	(
		CompoundRule_ID int not null,
		Operator_ID int not null,
		Contact_ID int not null,
		
		H_CompoundRule_Contact_ID int identity(1,1)
			constraint PK_H_CompoundRule_Contact
				primary key clustered,
		
        TRAIL_ID        int 
			constraint FK_H_CompoundRule_Contact_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

exec sp_executesql N'
	insert into CompoundRule_Operator_Contact
		select * from (
			select cr_se.CompoundRule_ID, oc.Operator_ID, oc.Contact_ID
				from CompoundRule_SubscriptionEmail cr_se
				join Email e on e.EMAIL_ID = cr_se.Email_ID
				join Contact c on c.Type = 1 and c.Value = e.EMAIL
				join Operator_Contact oc on oc.Operator_ID = e.Operator_ID and oc.Contact_ID = c.ID
			union all
			select cr_sp.CompoundRule_ID, oc.Operator_ID, oc.Contact_ID
				from CompoundRule_SubscriptionPhone cr_sp
				join Phone p on p.Phone_ID = cr_sp.Phone_ID
				join Contact c on c.Type = 2 and c.Value = dbo.GetPhoneInvariant(p.Phone)
				join Operator_Contact oc on oc.Operator_ID = p.Operator_ID and oc.Contact_ID = c.ID
		) t
		where not exists (
			select *
				from CompoundRule_Operator_Contact e
				where e.CompoundRule_ID = t.CompoundRule_ID
				  and e.Operator_ID = t.Operator_ID
				  and e.Contact_ID = t.Contact_ID)
'

end