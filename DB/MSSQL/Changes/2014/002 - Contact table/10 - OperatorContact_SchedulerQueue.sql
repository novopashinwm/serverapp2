if not exists (
	select *
		from sys.objects 
		where name = 'OperatorContact_SchedulerQueue'
)
begin

	create table OperatorContact_SchedulerQueue
	(
		Operator_ID int not null,
		Contact_ID int not null,
		SchedulerQueue_ID int not null
			constraint FK_OperatorContact_SchedulerQueue_SchedulerQueue_ID
				foreign key references SchedulerQueue(SchedulerQueue_ID),
		constraint FK_OperatorContact_SchedulerQueue_OperatorContact
			foreign key (Operator_ID, Contact_ID) references Operator_Contact(Operator_ID, Contact_ID),
		constraint PK_OperatorContact_SchedulerQueue
			primary key clustered (Operator_ID, Contact_ID, SchedulerQueue_ID)
	)
	
	create nonclustered index IX_OperatorContact_SchedulerQueue_SchedulerQueue_ID
		on OperatorContact_SchedulerQueue (SchedulerQueue_ID)


	create table H_OperatorContact_SchedulerQueue
	(
		Operator_ID int not null,
		Contact_ID int not null,
		SchedulerQueue_ID int not null,
		
		ID int identity (1,1) 
			constraint PK_H_OperatorContact_SchedulerQueue
				primary key clustered,
				
        TRAIL_ID        int 
			constraint FK_H_OperatorContact_SchedulerQueue_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

	exec sp_executesql N'	
		insert into OperatorContact_SchedulerQueue
			select distinct oc.Operator_ID, oc.Contact_ID, esq.SchedulerQueue_ID
				from EMAIL_SCHEDULERQUEUE esq
				join EMAIL e on e.EMAIL_ID = esq.EMAIL_ID
				join Contact c on c.Type = 1 and c.Value = e.EMAIL
				join Operator_Contact oc on oc.Contact_ID = c.ID and oc.Operator_ID = e.Operator_ID
	'

end