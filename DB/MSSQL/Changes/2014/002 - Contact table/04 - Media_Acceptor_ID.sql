if not exists (
	select *
		from sys.columns sc
		join sys.objects so on so.object_id = sc.object_id
		where so.name = 'Message'
		  and sc.name = 'MA_ID'
)
begin

	alter table Message
		add MA_ID int 
			constraint FK_Message_MA_ID 
				foreign key references Media_Acceptors(MA_ID)
		
		
	alter table H_Message
		add MA_ID int

end
go
