set IDENTITY_INSERT DOTNET_TYPE on
insert into DOTNET_TYPE (DOTNET_TYPE_ID, TYPE_NAME)
	select t.ID, t.Name
		from (select ID = 5, Name = 'System.Boolean') t
		where not exists (select * from DOTNET_TYPE e where e.DOTNET_TYPE_ID = t.ID)
set IDENTITY_INSERT DOTNET_TYPE off

declare @templateName nvarchar(50) = 'GenericConfirmation'

begin tran

insert into dbo.Message_Template(Name)
	select t.Name 
		from (
			  select Name = @templateName
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
		
insert into MESSAGE_TEMPLATE_FIELD (DOTNET_TYPE_ID, NAME, MESSAGE_TEMPLATE_ID)
	select t.*, mt.Message_Template_ID
		from (
			select DOTNET_TYPE_ID, Name = 'ConfirmationCode'
				from DOTNET_TYPE 
				where TYPE_NAME = 'System.String'
			union all
			select DOTNET_TYPE_ID, Name = 'ConfirmationIsUsed'
				from DOTNET_TYPE 
				where TYPE_NAME = 'System.Boolean'
		) t
		join Message_Template mt on mt.Name = @templateName
		where not exists (	
			select * 
				from MESSAGE_TEMPLATE_FIELD e 
				where e.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID 
				  and e.NAME = t.Name)
		
		
commit