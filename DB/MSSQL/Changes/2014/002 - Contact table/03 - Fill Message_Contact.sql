declare @destination_contact_type int = 1
declare @source_contact_type      int = 2

declare @email_contact_type			int = 1
declare @phone_contact_type			int = 2
declare @asid_contact_type			int = 3 --MTS MPX F(MSISDN) - Anonymous Subscriber IDentity
declare @controller_contact_type	int = 4 --Controller.Controller_ID
declare @operator_contact_type		int = 5 --Operator.Operator_ID
declare @android_contact_type		int = 6 --AppClient.GcmRegistrationId
declare @apple_contact_type			int = 7 --AppClient.ApnToken

insert into Message_Contact
	select t.*
		from (
			select Message_ID = 0, Type = 0, Contact_ID = 0 where 1=0

			--���������� - Email
			union all 
			select m.Message_ID, @destination_contact_type, c.ID
				from MESSAGE m
				join Email e on e.EMAIL_ID = m.DESTINATION_ID
				join Contact c on c.Value = e.EMAIL and c.Type = @email_contact_type
				where m.DestinationType_ID = 6 /*Email*/
				
			--���������� - MSISDN
			union all
			select m.Message_ID, @destination_contact_type, c.ID
				from MESSAGE m
				join Phone p on p.Phone_ID = m.DESTINATION_ID
				join Contact c on c.Value = p.Phone and c.Type = @phone_contact_type
				where m.DestinationType_ID in (1 /*RedHat HTTP SMS Gateway*/, 2 /*SMPP*/)
				
			--���������� - F(MSISDN)
			union all
			select m.Message_ID, @destination_contact_type, c.ID
				from MESSAGE m
				join Asid a on a.ID = m.DESTINATION_ID
				join Contact c on c.Value = a.Value and c.Type = @asid_contact_type
				where m.DestinationType_ID = 3 /*Asid*/			
				
			--���������� - ���������� � Google Cloud Message
			union all
			select m.Message_ID, @destination_contact_type, c.ID
				from MESSAGE m
				join AppClient ac on ac.ID = m.DESTINATION_ID
				join Contact c on c.Value = ac.GcmRegistrationId and c.Type = @android_contact_type
				where m.DestinationType_ID = 9 /*Android*/
				
			--���������� - ���������� � Apple iCloud
			union all
			select m.Message_ID, @destination_contact_type, c.ID
				from MESSAGE m
				join AppClient ac on ac.ID = m.DESTINATION_ID
				join Contact c on c.Value = ac.ApnToken and c.Type = @apple_contact_type
				where m.DestinationType_ID = 10 /*AppleiOS*/
				
			--���������� - Controller.Controller_ID - ��� �������� ��������� �������������� �� ����������
			union all 
			select m.Message_ID, @destination_contact_type, c.ID
				from Message m
				join Contact c on c.Value = CONVERT(nvarchar(255), m.Destination_ID) and c.Type = @controller_contact_type
				where (m.TYPE & CONVERT(int, 0x0100)) <> 0
				
			--���������� - Operator.Operator_ID - ��� �������� ��������� ������������
			union all 
			select m.Message_ID, @destination_contact_type, c.ID
				from Message m
				join Contact c on c.Value = CONVERT(nvarchar(255), m.Destination_ID) and c.Type = @operator_contact_type
				where (m.TYPE & CONVERT(int, 0x2000)) <> 0
				
			--����������� - Email
			union all 
			select m.Message_ID, @source_contact_type, c.ID
				from MESSAGE m
				join Email e on e.EMAIL_ID = m.SOURCE_ID
				join Contact c on c.Value = e.EMAIL and c.Type = @email_contact_type
				where m.SourceType_ID = CONVERT(int, 0x8000)
			--����������� - MSISDN
			union all
			select m.Message_ID, @source_contact_type, c.ID
				from MESSAGE m
				join Phone p on p.Phone_ID = m.SOURCE_ID
				join Contact c on c.Value = p.Phone and c.Type = @phone_contact_type
				where m.SourceType_ID = CONVERT(int, 0x4000)
			union all
			--����������� - F(MSISDN)
			select m.Message_ID, @source_contact_type, c.ID
				from MESSAGE m
				join Asid a on a.ID = m.SOURCE_ID
				join Contact c on c.Value = a.Value and c.Type = @asid_contact_type
				where m.SourceType_ID = CONVERT(int, 0x2000)
				
			--����������� - Controller.Controller_ID - ��� �������� ��������� ��������������� �� ����������
			union all 
			select m.Message_ID, @source_contact_type, c.ID
				from Message m
				join Contact c on c.Value = CONVERT(nvarchar(255), m.Destination_ID) and c.Type = @controller_contact_type
				where (m.TYPE & CONVERT(int, 0x200)) <> 0
				
			--����������� - Operator.Operator_ID - ��� �������� ��������� ������������
			union all 
			select m.Message_ID, @source_contact_type, c.ID
				from Message m
				join Contact c on c.Value = CONVERT(nvarchar(255), m.Destination_ID) and c.Type = @operator_contact_type
				where (m.TYPE & CONVERT(int, 0x400)) <> 0
		) t
		