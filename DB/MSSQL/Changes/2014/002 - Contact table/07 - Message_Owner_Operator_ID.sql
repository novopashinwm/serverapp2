if not exists (
	select *
		from sys.columns c
		join sys.objects o on o.object_id = c.object_id
		where o.name = 'Message'
		  and c.name = 'Owner_Operator_ID'
)
begin

	alter table Message
		add Owner_Operator_ID int 
			constraint FK_Message_Owner_Operator_ID
				foreign key references Operator(Operator_ID)

	alter table H_Message
		add Owner_Operator_ID int 

exec sp_executesql N'		
	update m
		set m.Owner_Operator_ID = p.Operator_ID
		from Message m
		join Phone p on p.Phone_ID = m.DESTINATION_ID and p.Operator_ID is not null
		where m.DestinationType_ID in (1, 2)
		  and m.Owner_Operator_ID is null
		  
	update m
		set m.Owner_Operator_ID = e.Operator_ID
		from Message m
		join Email e on e.EMAIL_ID = m.DESTINATION_ID and e.Operator_ID is not null
		where m.DestinationType_ID = 6
		  and m.Owner_Operator_ID is null
		  
	update m
		set m.Owner_Operator_ID = a.Operator_ID
		from Message m
		join Asid a on a.ID = m.DESTINATION_ID and a.Operator_ID is not null
		where m.DestinationType_ID = 3
		  and m.Owner_Operator_ID is null	
		
	update m
		set m.Owner_Operator_ID = ac.Operator_ID
		from Message m
		join AppClient ac on ac.ID = m.DESTINATION_ID
		where m.DestinationType_ID in (9, 10)
		  and m.Owner_Operator_ID is null	
'

end