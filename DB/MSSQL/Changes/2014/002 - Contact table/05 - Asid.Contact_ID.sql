if not exists (
	select *
		from sys.columns c
		join sys.objects o on o.object_id = c.object_id
		where o.name = 'Asid'
		  and c.name = 'Contact_ID'
)
begin

	alter table Asid
		add Contact_ID int
			constraint FK_Asid_Contact_ID 
				foreign key references Contact(ID)
				
	alter table H_Asid
		add Contact_ID int			

end
go

update a
	set Contact_ID = c.ID
	from Asid a
	join Contact c on c.Type = 3 /*Asid*/ and c.Value = a.Value
	where a.Contact_ID is null
       or a.Contact_ID <> c.ID
