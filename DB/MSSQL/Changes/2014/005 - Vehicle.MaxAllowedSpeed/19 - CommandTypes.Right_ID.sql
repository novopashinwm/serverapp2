if not exists (
	select *
		from sys.columns
		where object_id = object_id('CommandTypes')
		  and name = 'Right_ID'
)
begin

	alter table CommandTypes
		add Right_ID int 
			constraint FK_CommandTypes_Right_ID 
				foreign key references [Right](Right_ID)

	create nonclustered index IX_CommandTypes_Right_ID on CommandTypes (Right_ID)
				
end