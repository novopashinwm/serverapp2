if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_Template_ID'
)
begin

	create nonclustered index IX_Message_Template_ID on Message(Template_ID, Time)

end