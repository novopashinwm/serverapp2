insert into controller_type (Type_Name, AllowedToAddByCustomer, Default_Vehicle_Kind_ID, DeviceIdIsImei)
	select t.name, 1, vk.Vehicle_Kind_ID, 1
		from (select name = 'Smarts GT02', Vehicle_Kind_Name = N'��������'
              ) t
        join VEHICLE_KIND vk on vk.NAME = t.Vehicle_Kind_Name
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)

update CONTROLLER_TYPE set SupportsPassword = 1 where TYPE_NAME = 'Smarts GT02'