if exists (
	select *
		from sys.columns c
		where c.object_id = object_id('Asid')
		  and c.name = 'Value'
)
begin

	drop index IX_Asid_Value on Asid

	alter table Asid
		drop column Value
		
	alter table H_Asid
		drop column Value

end