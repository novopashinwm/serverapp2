declare @templateName nvarchar(50) = 'AppLog'

insert into dbo.Message_Template(Name)
	select t.Name 
		from (
			  select Name = @templateName
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
