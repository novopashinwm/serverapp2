if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller')
		  and name = 'PhoneContactID'
)
begin

	alter table Controller
		add PhoneContactID int
			constraint FK_Controller_PhoneContactID
				foreign key references Contact(ID)

	create nonclustered index IX_Controller_PhoneContactID on Controller(PhoneContactID)
	
	alter table H_Controller
		add PhoneContactID int
		
end
go

declare @controller_id int = -1
declare @contact_id dbo.Id_Param
declare @phone varchar(20)

while 1=1
begin

	set @controller_id = (select MIN(controller_id) from Controller where CONTROLLER_ID > @controller_id)
	if @controller_id is null
		break
		
	set @phone = (select phone from CONTROLLER where CONTROLLER_ID = @controller_id)
	if dbo.GetPhoneInvariant(@phone) is null
		continue	
	delete from @contact_id
	
	insert into @contact_id
		exec dbo.GetContactID 2, @phone
		
	if @@ROWCOUNT = 0
		continue
		
	update CONTROLLER 
		set PhoneContactID = (select id from @contact_id)
		where CONTROLLER_ID = @controller_id

end

