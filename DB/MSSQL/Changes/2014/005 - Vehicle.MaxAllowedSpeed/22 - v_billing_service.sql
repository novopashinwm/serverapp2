if exists (
	select *
		from sys.views
		where name = 'v_billing_service'
)
	drop view v_billing_service;
go

create view v_billing_service
as
	select Phone = pc.Value, bst.Name, o.OPERATOR_ID, o.LOGIN, o.PASSWORD, Department_Name = d.NAME
		from Billing_Service bs
		join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
		left outer join Asid a on a.ID = bs.Asid_ID 
		left outer join Contact ac on ac.ID = a.Contact_ID
		left outer join Contact pc on pc.ID = ac.Demasked_ID
		left outer join Operator o on o.OPERATOR_ID = a.Operator_ID
		left outer join Department d on d.DEPARTMENT_ID = a.Department_ID