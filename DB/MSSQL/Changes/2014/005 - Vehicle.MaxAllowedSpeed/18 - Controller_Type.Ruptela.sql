declare @type_name varchar(255) = 'Ruptela'

insert into CONTROLLER_TYPE (TYPE_NAME, DeviceIdIsImei, AllowedToAddByCustomer)
	select @type_name, 1, 1
	where not exists (select * from CONTROLLER_TYPE e where e.TYPE_NAME = @type_name)

declare @sensor table(digital bit, name varchar(255), number int)

set nocount on

insert into @sensor (number, name)
          select 2	, 'Digital Input 1'
union all select 3	, 'Digital Input 2'
union all select 4	, 'Digital Input 3'
union all select 5	, 'Digital Input 4'
union all select 6	, 'Modem Temperature'
union all select 7	, 'Time record'
union all select 8	, 'Dsitance record'
union all select 9	, 'Course record'
union all select 10	, 'CANBUS request supported'
union all select 11	, 'CANBUS diagnostics supported'
union all select 12	, 'CANBUS FMS software'
union all select 13	, 'CANBUS vehicle motion'
union all select 14	, 'CANBUS driver 1 time'
union all select 15	, 'CANBUS driver 1 card'
union all select 16	, 'CANBUS driver 2 time'
union all select 17	, 'CANBUS driver 2 card'
union all select 18	, 'fuel counter 1'
union all select 19	, 'fuel counter 2'
union all select 20	, 'fuel counter 3'
union all select 21	, 'fuel counter 4'
union all select 22	, 'Analog Input 1'
union all select 23	, 'Analog Input 2'
union all select 24	, 'Tacho_ddd_available'
union all select 25	, 'Security_info'
union all select 26	, 'Analog Input Delta 1'
union all select 27	, 'GSM Signal Strength'
union all select 28	, 'Current Profile'
union all select 29	, 'Power Supply Voltage'
union all select 30	, 'Battery Voltage'
union all select 31	, 'Reserved'
union all select 32	, 'PCB Temperature'
union all select 33	, 'Analog Input Delta 2'
union all select 34	, 'iButton ID'
union all select 35	, 'CANBUS ClutchS'
union all select 36	, 'CANBUS BreakS'
union all select 37	, 'CANBUS CruiseCS'
union all select 38	, 'CANBUS PTOstate'
union all select 39	, 'CANBUS Engine PLCS'
union all select 40	, 'CANBUS Tire location'
union all select 41	, 'CANBUS Axle weight_1'
union all select 42	, 'CANBUS system event'
union all select 43	, 'CANBUS handling information'
union all select 44	, 'CANBUS direction indicator'
union all select 45	, 'Din1_hours'
union all select 46	, 'Din2_hours'
union all select 47	, 'Din3_hours'
union all select 48	, 'Din4_hours'
union all select 49	, 'Accelerometer_x'
union all select 50	, 'Accelerometer_y'
union all select 51	, 'Accelerometer_z'
union all select 52	, 'Canbus_AxleW_2'
union all select 53	, 'Canbus_AxleW_3'
union all select 54	, 'Canbus_AxleW_4'
union all select 55	, 'Canbus_AxleW_5'
union all select 56	, 'Digital fuel sensorA1 temperature'
union all select 57	, 'Digital fuel sensorA2 temperature'
union all select 58	, 'Digital fuel sensorA3 temperature'
union all select 59	, 'Digital fuel sensorA4 temperature'
union all select 60	, 'Digital fuel sensorA5 temperature'
union all select 61	, 'Digital fuel sensorA6 temperature'
union all select 62	, 'Digital fuel sensorA7 temperature'
union all select 63	, 'Digital fuel sensorA8 temperature'
union all select 64	, 'Digital fuel sensorA9 temperature'
union all select 65	, 'Virtual Odometer'
union all select 66	, 'Digital fuel sensorA1'
union all select 67	, 'Digital fuel sensor B1'
union all select 68	, 'Digital fuel sensorA2'
union all select 69	, 'Digital fuel sensorA3'
union all select 70	, 'Temp sensor ID 0'
union all select 71	, 'Temp sensor ID 1'
union all select 72	, 'Temp sensor ID 2'
union all select 75	, 'Digital fuel sensorA10 temperature'
union all select 76	, 'Digital fuel sensorB1 temperature'
union all select 77	, 'odometer_delta'
union all select 78	, 'Temperature Sensor 0'
union all select 79	, 'Temperature Sensor 1'
union all select 80	, 'Temperature Sensor 2'
union all select 81	, 'Digital fuel sensorA4'
union all select 82	, 'Digital fuel sensorA5'
union all select 83	, 'Digital fuel sensorA6'
union all select 84	, 'Digital fuel sensorA7'
union all select 85	, 'Digital fuel sensorA8'
union all select 86	, 'Digital fuel sensorA9'
union all select 87	, 'Digital fuel sensorA10'
union all select 89	, 'CANBUS ambient air temperature'
union all select 90	, 'CANBUS fuel economy'
union all select 91	, 'CANBUS PTO'
union all select 92	, 'CANBUS HRFC'
union all select 93	, 'DTC & MIL'
union all select 94	, 'RPM'
union all select 95	, 'Vehicle speed sensor ( Km/h )'
union all select 96	, 'Engine coolant temperature ( C )'
union all select 97	, 'Ambient air temperature ( C )'
union all select 98	, 'fuel level input (%)'
union all select 99	, 'Type of fuel'
union all select 100	, 'Engine fuel rate ( l/h )'
union all select 101	, 'Actual engine � percent torgue (%)'
union all select 102	, 'Distance traveled while MIL is activated (Km)'
union all select 103	, 'Relative Accelerator Pedal position (%)'
union all select 104	, 'VIN'
union all select 105	, 'VIN'
union all select 106	, 'VIN'
union all select 107	, 'Time since engine start'
union all select 108	, 'DTC counted'
union all select 114	, 'CANBUS Distance'
union all select 115	, 'CANBUS coolant temperature'
union all select 116	, 'CANBUS fuel rate'
union all select 117	, 'TCO_CAN totaldriving time'
union all select 118	, 'TCO_CAN current activity duration'
union all select 119	, 'TCO_CAN continious driving time'
union all select 120	, 'TCO_CAN break time'
union all select 121	, 'TCO_CAN driver time states'
union all select 123	, 'CANBUS Vehicle Id 1'
union all select 124	, 'CANBUS Vehicle Id 2'
union all select 125	, 'CANBUS Vehicle Id 3'
union all select 126	, 'CANBUS First Driver Id1'
union all select 127	, 'CANBUS First Driver Id2'
union all select 128	, 'CANBUS Second Driver Id 1'
union all select 129	, 'CANBUS Second Driver Id 2'
union all select 130	, 'ECO_max_speed'
union all select 131	, 'ECO_overspeed'
union all select 132	, 'ECO_rpm_red'
union all select 133	, 'ECO_max_rpm'
union all select 134	, 'ECO_braking events'
union all select 135	, 'ECO_ext_hrsh_braking'
union all select 136	, 'ECO_harsh_acceleration'
union all select 137	, 'ECO_idling_time'
union all select 138	, 'ECO_cruise_control'
union all select 139	, 'ECO_engine_on'
union all select 145	, 'ME_TamperAlert'
union all select 146	, 'ME_TSRenabled'
union all select 147	, 'ME_TSRwarningLevel'
union all select 148	, 'ME_TrafficSignRecognition1'
union all select 149	, 'ME_TrafficSignRecognition2'
union all select 150	, 'GSM Operator'
union all select 152	, 'TCO Vehicle ID 1'
union all select 153	, 'TCO Vehicle ID 2'
union all select 154	, 'TCO Vehicle ID 3'
union all select 155	, 'TCO first Driver ID 1'
union all select 156	, 'TCO first Driver ID 2'
union all select 157	, 'TCO second Driver ID 1'
union all select 158	, 'TCO second Driver ID 2'
union all select 159	, 'TCO first driver state'
union all select 160	, 'TCO second driver state'
union all select 161	, 'TCO first driver card'
union all select 162	, 'TCO second driver card'
union all select 163	, 'TCO Dsitance'
union all select 164	, 'TCO trip'
union all select 165	, 'TCO vehicle speed'
union all select 166	, 'TCO RPM'
union all select 167	, 'TCO registration number 1'
union all select 168	, 'TCO registration number 2'
union all select 170	, 'Battery Current'
union all select 171	, 'RFID A'
union all select 172	, 'RFID B'
union all select 173	, 'Movement_sensor'
union all select 174	, 'Sleep_timer'
union all select 176	, 'GPS speed'
union all select 177	, 'TK fuel level'
union all select 178	, 'TK battery voltage'
union all select 179	, 'TK total electric hours'
union all select 180	, 'TK total vehicle hours'
union all select 181	, 'TK total engine hours'
union all select 182	, 'TK alarm type'
union all select 183	, 'TK alarm code'
union all select 184	, 'TK return air temperature'
union all select 185	, 'TK discharge temperature'
union all select 186	, 'TK temperature setpoint'
union all select 187	, 'TK evaporator coil temperature'
union all select 188	, 'TK operating mode'
union all select 189	, 'TK cycle mode'
union all select 190	, 'TK high speed status'
union all select 191	, 'TK door status'
union all select 192	, 'TK diesel electric status'
union all select 197	, 'CANBUS RPM'
union all select 203	, 'CANBUS Engine Hours'
union all select 204	, 'CANBUS service distance'
union all select 205	, 'CANBUS Fuel level (ltr)'
union all select 206	, 'CANBUS AccPedal position'
union all select 207	, 'Fuel level %'
union all select 208	, 'CANBUS Fuel used'
union all select 209	, 'CANBUS  Secondary fuel level %'
union all select 210	, 'CANBUS WBSpeed'
union all select 213	, 'CANBUS tachogr. Vehicle speed'
union all select 215	, 'CANBUS driver 1 state'
union all select 216	, 'CANBUS driver 2 state'
union all select 217	, 'CANBUS tachograph performance'
union all select 218	, 'CANBUS over speed'
union all select 219	, 'CANBUS Axle location'
union all select 220	, 'ME Sound type'
union all select 221	, 'ME Hi/Low beam'
union all select 222	, 'ME Pedestrians in DZ'
union all select 223	, 'ME Pedestrians FCW'
union all select 224	, 'ME Night time indicator'
union all select 225	, 'ME Dusk time indicator'
union all select 226	, 'ME Error code'
union all select 227	, 'ME Error valid'
union all select 228	, 'ME Zero speed'
union all select 229	, 'ME Headway valid'
union all select 230	, 'ME Headway measurement'
union all select 231	, 'ME LDW off'
union all select 232	, 'ME Right LDW on'
union all select 233	, 'ME Left LDW on'
union all select 234	, 'ME Maintenance'
union all select 235	, 'ME Failsafe'
union all select 236	, 'ME FCW on'
union all select 237	, 'ME Headway warning level'
union all select 238	, 'ME Hi beam'
union all select 239	, 'ME Low beam'
union all select 240	, 'ME Wipers'
union all select 241	, 'ME Right signal'
union all select 242	, 'ME Left signal'
union all select 243	, 'ME Brake signal'
union all select 244	, 'ME Wipers available'
union all select 245	, 'ME Low beam available'
union all select 246	, 'ME Hi beam available'
union all select 247	, 'ME Speed available'
union all select 248	, 'ME Speed'
union all select 250	, 'Extension1'
union all select 251	, 'Extension2'
union all select 252	, 'Extension3'
union all select 253	, 'Extension4'
union all select 254	, 'Extension5'
union all select 255	, 'Extension6'

update @sensor
	set digital = 
		case 
			when number in (2,3,4,5,13,15,17,24,35,36,37,42,43,44,145,146,161,162,173,174,189,190,191,192,217,218,220,221,222,223,224,225,227,229,231,232,233,236,238,239,240,241,242,243,244,245,246,247)
				then 1
			else 0
		end

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.Type_Name = @type_name
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
