if not exists (
	select *
		from sys.tables 
		where name = 'Controller_Type_Supported_CommandTypes'
)
begin

	create table Controller_Type_Supported_CommandTypes
	(
		Controller_Type_ID int not null 
			constraint FK_Controller_Type_Supported_CommandTypes_Controller_Type_ID
				foreign key references Controller_Type(Controller_Type_ID),
		CommandType_ID int not null
			constraint FK_Controller_Type_Supported_CommandTypes_CommandTypes_ID
				foreign key references CommandTypes(ID),
		constraint PK_Controller_Type_Supported_CommandTypes primary key clustered (Controller_Type_ID),
	)
	
	create nonclustered index IX_Controller_Type_Supported_CommandTypes_CommandType_ID 
		on Controller_Type_Supported_CommandTypes(CommandType_ID)

end