if not exists (
	select *
		from sys.indexes 
		where name = 'IX_H_Message_Field_Message_ID'
)
begin

	create nonclustered index IX_H_Message_Field_Message_ID on H_Message_Field(Message_ID)

end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_H_Message_Attachment_Message_ID'
)
begin

	create nonclustered index IX_H_Message_Attachment_Message_ID on H_Message_Attachment(Message_ID)

end
go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_H_Message_Operator_Message_ID'
)
begin

	create nonclustered index IX_H_Message_Operator_Message_ID on H_Message_Operator(Message_ID)

end
go
