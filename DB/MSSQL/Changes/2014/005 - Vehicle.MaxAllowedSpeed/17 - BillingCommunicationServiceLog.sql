if not exists (
	select *
		from sys.tables 
		where name = 'BillingCommunicationServiceLog'
)
begin

	create table BillingCommunicationServiceLog
	(
		Id bigint identity(1,1)
			constraint PK_BillingCommunicationServiceLog 
				primary key clustered,
		RequestTime datetime not null,
		ResponseTime datetime,
		IP varchar(40),
		Request nvarchar(max) not null,
		Response nvarchar(max),
		Error nvarchar(max)
	)

	create nonclustered index IX_BillingCommunicationServiceLog_RequestTime on BillingCommunicationServiceLog(RequestTime)
	create nonclustered index IX_BillingCommunicationServiceLog_RequestValidXml_RequestTime on BillingCommunicationServiceLog(RequestValidXml, RequestTime)

end
go

if not exists (
	select *
		from sys.columns
		where object_id = object_id('BillingCommunicationServiceLog')
		  and name = 'RequestValidXml'
)
begin	  
		  
	alter table BillingCommunicationServiceLog add RequestValidXml bit
	
end
go

if exists (
	select *
		from sys.views 
		where name = 'v_BillingCommunicationServiceLog'
)
	drop view v_BillingCommunicationServiceLog;
go

create view v_BillingCommunicationServiceLog as
	select 
	      Id
	    , IP
	    , Billing_Service_Provider_Name = bsp.Name
		, RequestTime
		, ActionName = CONVERT(xml, l.Request).value('(action/@name)[1]', 'nvarchar(max)')
		, TerminalDeviceNumber = CONVERT(xml, l.Request).value('(action/params/param[@name="TerminalDeviceNumber"])[1]', 'nvarchar(max)')
		, ContractNumber = CONVERT(xml, l.Request).value('(action/params/param[@name="ContractNumber"])[1]', 'nvarchar(max)') 
		, FMSISDN = CONVERT(xml, l.Request).value('(action/params/param[@name="FMSISDN"])[1]', 'nvarchar(max)') 
		, ServiceCode = CONVERT(xml, l.Request).value('(action/params/param[@name="ServiceCode"])[1]', 'nvarchar(max)') 
		, Quantity = CONVERT(xml, l.Request).value('(action/params/param[@name="Quantity"])[1]', 'nvarchar(max)') 
		, ResponseTime
		, Response		
		, Error
		, Request
		from BillingCommunicationServiceLog l
		outer apply (
			select bsp.Name
				from Billing_Service_Provider_Host bsph
				join Billing_Service_Provider bsp on bsp.Billing_Service_Provider_ID = bsph.Billing_Service_Provider_ID
				where bsph.IP = l.IP) bsp
		where l.RequestValidXml = 1