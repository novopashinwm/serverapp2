if exists (
	select *
		from sys.columns
		where object_id = object_id('Session')
		  and name = 'Host_IP'
		  and max_length < 80
)
begin

	alter table Session
		alter column Host_IP nvarchar(40) not null

end