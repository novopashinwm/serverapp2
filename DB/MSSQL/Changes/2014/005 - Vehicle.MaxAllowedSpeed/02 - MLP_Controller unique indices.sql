if exists (
	select *
		from sys.indexes 
		where name = 'UQ_MLP_Controller_Asid_ID_Controller_ID'
)
begin	
	delete MLP_Controller where Asid_ID is null or Controller_ID is null
	delete H_MLP_Controller where Asid_ID is null or Controller_ID is null

	alter table MLP_Controller
		drop constraint UQ_MLP_Controller_Asid_ID_Controller_ID
	
	alter table MLP_Controller
		alter column Asid_ID int not null
	alter table MLP_Controller
		alter column Controller_ID int not null
		
	alter table H_MLP_Controller
		alter column Asid_ID int not null
	alter table H_MLP_Controller
		alter column Controller_ID int not null

	create unique nonclustered index UQ_MLP_Controller_Asid_ID on MLP_Controller(Asid_ID) include (Controller_ID)
	create unique nonclustered index UQ_MLP_Controller_Controller_ID on MLP_Controller(Controller_ID) include (Asid_ID)

end