update CommandTypes
	set code = 'ShutdownDevice'
	where id = 32 and code <> 'ShutdownDevice'

insert into CommandTypes (id, code)
	select t.id, t.code
		from (select id = 36, code = 'Setup') t
		where not exists (
			select *
				from CommandTypes e
				where e.id = t.id)