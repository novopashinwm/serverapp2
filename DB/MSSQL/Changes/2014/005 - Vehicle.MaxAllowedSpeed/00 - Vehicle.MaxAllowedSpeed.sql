if not exists (
	select *
		from sys.columns
		where object_id = object_id('Vehicle')
		  and name = 'MaxAllowedSpeed'
)
begin

	alter table Vehicle
		add MaxAllowedSpeed int
		
	alter table H_Vehicle
		add MaxAllowedSpeed int

end