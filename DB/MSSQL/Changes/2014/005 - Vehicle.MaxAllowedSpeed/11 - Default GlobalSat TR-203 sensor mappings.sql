/*
select cs.Descript, legend.NAME
	from CONTROLLER_TYPE ct
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	left outer join CONTROLLER_SENSOR_LEGEND legend on legend.CONTROLLER_SENSOR_LEGEND_ID = cs.Default_Sensor_Legend_ID
	where TYPE_NAME like 'GlobalSat TR-203'
	   or TYPE_NAME = 'TR203'
*/

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from CONTROLLER_TYPE ct
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join Controller_Sensor_Legend legend on legend.Name = 'BatteryLevel'
	where ct.Type_Name = 'GlobalSat TR-203'
	  and cs.Descript = 'Battery capacity, %'
	  and (cs.Default_Sensor_Legend_ID is null or Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from CONTROLLER_TYPE ct
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join Controller_Sensor_Legend legend on legend.Name = '�������'
	where ct.Type_Name = 'GlobalSat TR-203'
	  and cs.Descript = 'SOS alarm'
	  and (cs.Default_Sensor_Legend_ID is null or Mandatory = 0)
