if not exists (
	select *
		from sys.tables
		where name = 'TestMtsSim'
)
begin

	create table TestMtsSim (		
		msisdn varchar(11) not null
			constraint PK_TestMtsSim_
				primary key clustered
	)

end