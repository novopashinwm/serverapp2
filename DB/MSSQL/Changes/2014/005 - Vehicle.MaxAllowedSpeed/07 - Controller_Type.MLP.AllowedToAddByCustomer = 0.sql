update CONTROLLER_TYPE
	set AllowedToAddByCustomer = 0
	from CONTROLLER_TYPE ct 
	where ct.TYPE_NAME = 'mlp'
	  and AllowedToAddByCustomer = 1