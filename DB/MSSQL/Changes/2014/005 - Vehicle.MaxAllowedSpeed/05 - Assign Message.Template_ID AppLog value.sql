set nocount on

create table #message (id int primary key clustered)
insert into #message 
	select messageContact.Message_ID
		from Controller c 
		join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
		join Asid a on a.ID = mc.Asid_ID
		join Contact contact (nolock) on contact.Type = 4 
				                     and contact.Value = CONVERT(varchar(12), c.vehicle_id)
		join Message_Contact messageContact (nolock) on messageContact.Contact_ID = contact.ID		
		join MESSAGE m (nolock) on m.MESSAGE_ID = messageContact.Message_ID
		where a.SimID is not null
		  and m.Template_ID is null

declare @template_id int = (
	select top(1) Message_Template_ID
		from Message_Template mt
		where mt.Name = 'AppLog')

declare @update table (id int primary key clustered)

declare @messages_left int

while 1=1
begin

	set @messages_left = (select count(1) from #message)
	
	print convert(varchar(12), @messages_left) + ' messages left'

	insert into @update 
		select top(10000) m.id
			from #message m
	
	if @@rowcount = 0
		break
	
	update m 
		set template_id = @template_id
		from @update u
		join message m on m.message_id = u.id
		
	delete m 
		from @update u
		join #message m on m.id = u.id

	delete @update

end

drop table #message