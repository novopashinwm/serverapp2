if not exists (
	select *
		from sys.objects 
		where name = 'Vehicle_ZoneGroup'
)
begin

	create table Vehicle_ZoneGroup
	(
		Vehicle_ID int not null
			constraint FK_ZoneGroup_Vehicle_Vehicle_ID 
				foreign key references Vehicle(Vehicle_ID),
		ZoneGroup_ID int not null
			constraint FK_ZoneGroup_Vehicle_ZoneGroup_ID 
				foreign key references ZoneGroup(ZoneGroup_ID),
		constraint PK_Vehicle_ZoneGroup 
			primary key clustered (Vehicle_ID, ZoneGroup_ID)		
	)
	
	create nonclustered index IX_Vehicle_ZoneGroup_ZoneGroup_ID 
		on Vehicle_ZoneGroup(ZoneGroup_ID)
	
	create table H_Vehicle_ZoneGroup
	(
		Vehicle_ID int not null,
		ZoneGroup_ID int not null,
		
		H_Vehicle_ZoneGroup_ID int identity(1,1)
			constraint PK_H_Vehicle_ZoneGroup
				primary key clustered,
		
        TRAIL_ID        int 
			constraint FK_H_Vehicle_ZoneGroup_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null		
	)


end