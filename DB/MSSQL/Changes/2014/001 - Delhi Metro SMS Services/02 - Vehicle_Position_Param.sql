IF NOT EXISTS(SELECT * FROM sys.table_types WHERE name = 'Vehicle_Position_Param')
BEGIN
	CREATE TYPE Vehicle_Position_Param AS TABLE
	(
		Vehicle_ID int not null,
		Log_Time int not null,
		Lat numeric(8,5) not null,
		Lng numeric(8,5) not null
	)
END
