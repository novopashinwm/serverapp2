if not exists (
	select *
		from sys.indexes 
		where name = 'UQ_ZoneGroup_Zone_ZoneGroup_ID_Zone_ID'
)
begin

	delete zgz
		from (
			select ZONEGROUP_ID, ZONE_ID, ZONEGROUP_ZONE_ID = MIN(ZONEGROUP_ZONE_ID)
				from ZoneGroup_Zone
				group by ZONEGROUP_ID, ZONE_ID
				having COUNT(1) > 1
		) dup
		join ZONEGROUP_ZONE zgz on zgz.ZONEGROUP_ID = dup.ZONEGROUP_ID 
		                       and zgz.ZONE_ID = dup.ZONE_ID 
		                       and zgz.ZONEGROUP_ZONE_ID <> dup.ZONEGROUP_ZONE_ID
	
	create unique nonclustered index UQ_ZoneGroup_Zone_ZoneGroup_ID_Zone_ID
		on ZoneGroup_Zone(ZoneGroup_ID, Zone_ID)

end
go
if not exists (
	select *
		from sys.indexes 
		where name = 'IX_ZoneGroup_Zone_Zone_ID'
)
begin

	create nonclustered index IX_ZoneGroup_Zone_Zone_ID
		on ZoneGroup_Zone(Zone_ID) include (ZoneGroup_ID)

end
