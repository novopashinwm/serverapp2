if not exists (
	select *
		from sys.indexes
		where name = 'IX_SchedulerQueue_SCHEDULEREVENT_ID'
)
begin

	create nonclustered index IX_SchedulerQueue_SCHEDULEREVENT_ID on SchedulerQueue(SCHEDULEREVENT_ID)

end