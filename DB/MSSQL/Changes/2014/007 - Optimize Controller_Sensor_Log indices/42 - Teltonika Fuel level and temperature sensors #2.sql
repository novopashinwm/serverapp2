insert into Controller_Sensor (Controller_Type_ID, Number, Descript, Controller_Sensor_Type_ID)
select 
	ct.Controller_Type_ID
	, t.Number
	, t.Descript
	, cst.Controller_Sensor_Type_ID
	from (
		select Number = 203, Descript = 'Fuel level meter 2'
		union
		select Number = 204, Descript = 'Fuel temperature 2'
	) t
	join Controller_Type ct on ct.Type_Name in ('Teltonika FM1100', 'Teltonika FM5300', 'Teltonika GH3000', 'Teltonika FM2200', 'Teltonika FM1200')
	join Controller_Sensor_Type cst on cst.Name = '����������'
	where not exists (
		select *
			from Controller_Sensor e
			where e.Controller_Type_ID = ct.Controller_Type_ID
			  and e.Number = t.Number)