insert into OPERATOR_VEHICLE(OPERATOR_ID, VEHICLE_ID, ALLOWED, RIGHT_ID)
select od.OPERATOR_ID, v.VEHICLE_ID, 1, 115
from DEPARTMENT d
	join v_operator_department_right od on od.DEPARTMENT_ID = d.DEPARTMENT_ID and od.RIGHT_ID = 104
	join vehicle v on v.DEPARTMENT = d.DEPARTMENT_ID
where not exists
(
	select *
	from OPERATOR_VEHICLE ov 
	where ov.vehicle_id = v.VEHICLE_ID and ov.operator_id = od.operator_id and ov.right_id = 115
) 

insert into OPERATOR_VEHICLEGROUP(OPERATOR_ID, VEHICLEGROUP_ID, ALLOWED, RIGHT_ID)
select distinct od.OPERATOR_ID, vgv.VEHICLEGROUP_ID, 1, 115
from DEPARTMENT d
	join v_operator_department_right od on od.DEPARTMENT_ID = d.DEPARTMENT_ID and od.RIGHT_ID = 104
	join VEHICLE v on v.DEPARTMENT = od.DEPARTMENT_ID
	join VEHICLEGROUP_VEHICLE vgv on vgv.VEHICLE_ID = v.VEHICLE_ID
where not exists
(
	select *
	from OPERATOR_VEHICLEGROUP ovg
	where ovg.VEHICLEGROUP_ID = vgv.VEHICLEGROUP_ID and ovg.operator_id = od.operator_id and ovg.right_id = 115
) 