DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'TL2000')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('TL2000', 1500, 1, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 1, 'ADC', 11
union all select 1, 'ExternalBatteryVoltage', 12
union all select 1, 'InternalBatteryVoltage', 13
union all select 1, 'PCBTemperature', 14
union all select 1, 'ExternalTemperature', 15
union all select 1, 'Moving', 16
union all select 1, 'Accel', 17
union all select 1, 'AccelCorneringValue', 18
union all select 1, 'Trip', 19
union all select 1, 'SignalStrength', 20
union all select 1, 'DigitalInput1', 101
union all select 1, 'DigitalInput2', 102
union all select 1, 'DigitalInput3', 103
union all select 1, 'DigitalOutput1', 104
union all select 1, 'DigitalOutput2', 105
union all select 1, 'ACStatus', 202
union all select 1, 'DrivingStatus', 206
union all select 1, 'ModelStatus', 207
union all select 1, 'ExternalBatteryRemoved', 209
union all select 1, 'GpsError', 210
union all select 1, 'TowedAway', 211
union all select 1, 'CouldntConnectToServer', 212
union all select 1, 'Mode', 213
union all select 1, 'SmsCount', 215

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
select ct.Controller_Type_ID
	, case s.digital when 1 then 2 else 1 end
	, s.number
	, s.name
from @sensor s
	join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
where not exists (
	select * 
	from Controller_Sensor e
	where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
		and e.NUMBER = s.number)

declare @legend table(controller_sensor_name varchar(100), legend_name nvarchar(100))
insert into @legend
	(controller_sensor_name, legend_name)
values 
	('ExternalBatteryVoltage', '���������� �������'),
	('ExternalTemperature', '�������� ������ �����������')


update cs
set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	, Default_Multiplier = 1
	, Default_Constant = 0
	, Mandatory = 1
from Controller_Sensor cs
	join @legend l on cs.Descript = l.controller_sensor_name
	join Controller_Sensor_Legend legend on legend.Name = l.legend_name
where cs.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	and (cs.Default_Sensor_Legend_ID is null or cs.Mandatory = 0)