/*��������� ������� Setup ��� ������� TS GLONASS */

declare @type_name varchar(100) = 'TS GLONASS'

insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.id
		from Controller_Type ct, CommandTypes cmd
		where ct.TYPE_NAME = @type_name
		  and cmd.code in ('Setup')
		  and not exists (select * from Controller_Type_CommandTypes e where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID and e.CommandTypes_ID = cmd.id)
