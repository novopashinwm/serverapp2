insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.ID
		from CONTROLLER_TYPE ct, CommandTypes cmd
		where ct.TYPE_NAME = 'Avtofon'
		  and cmd.code in ('Setup', 'CallPhone')
		  and not exists (
			select * 
				from Controller_Type_CommandTypes e
				where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID
				  and e.CommandTypes_ID = cmd.id)