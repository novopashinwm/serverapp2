declare @type_name varchar(255) = 'Meitrack MT90'

insert into CONTROLLER_TYPE
(
	TYPE_NAME,
	AllowedToAddByCustomer,
	Default_Vehicle_Kind_ID,
	DeviceIdIsImei,
	SupportsPassword
)
select *
	from (
		select 
			 TYPE_NAME = 'Meitrack MT90',
			 AllowedToAddByCustomer = 1,
			 Default_Vehicle_Kind_ID = vk.Vehicle_Kind_ID,
			 DeviceIdIsImei = 1,
			 SupportsPassword = 1
		from VEHICLE_KIND vk
		where vk.NAME = '������'
	) t		
	where not exists (
		select *
			from CONTROLLER_TYPE ct
			where ct.TYPE_NAME = t.TYPE_NAME)

insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.ID
	from CONTROLLER_TYPE ct, CommandTypes cmd
	where ct.TYPE_NAME = @type_name
	  and cmd.code = 'Setup'
	  and not exists (
		select * 
			from Controller_Type_CommandTypes e
			where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID
			  and e.CommandTypes_ID = cmd.id)