select oc.Contact_ID, null AsidId, oc.Operator_ID, hoc.ACTUAL_TIME [Date], 2 [Order]
	into #contacts
from Operator_Contact oc
	join Operator o on oc.Operator_ID = o.OPERATOR_ID
	join Contact c on oc.Contact_ID = c.ID
	outer apply 
	(
		select top 1 *
		from H_Operator_Contact
		where Contact_ID=oc.Contact_ID and Operator_ID=oc.Operator_ID and ACTION='INSERT'
		order by ACTUAL_TIME desc
	) hoc
where oc.Confirmed = 1
union all 
-- ��� ��������� �� ���������� ������������ - ���� ���������� ������������, ��� ��� � ��� ������������ ���������.
select c.Demasked_ID, a.ID AsidId, a.Operator_ID, DATEADD(dd, 1, getdate()) [Date], 1 [Order]
from Asid a
	join Contact c on c.ID = a.Contact_ID
	join OPERATOR o on a.Operator_ID = o.OPERATOR_ID
where c.Demasked_ID is not null

-- ������� ��� ��������������� ��������
delete cc
from #contacts cc
	join #contacts dc on dc.Contact_ID = cc.Contact_ID and dc.Operator_ID = cc.Operator_ID
where cc.AsidId is null and dc.AsidId is not null

-- ��� ��������� ������� �������, ��� � ���������� ���
select *, ROW_NUMBER() over(partition by c.Contact_ID order by c.[Order] asc, c.[Date] desc) Number
	into #dublicates
from #contacts c
where Contact_ID in ( 
	select c.Contact_ID
	from #contacts c
	group by c.Contact_ID
	having count(c.Operator_ID) > 1
)

drop table #contacts

if exists (select * from #dublicates where AsidId is not null and Number > 1)
begin
	print 'there are at least 2 asids with same contactId'
	select * 
	from #dublicates d1
		cross apply (
			select * 
			from #dublicates d2
			where d1.Contact_ID = d2.Contact_ID and d1.Operator_ID = d2.Operator_ID
				and d2.AsidId is not null
		) d2
	where d1.AsidId is not null and d1.Number > 1

	drop table #dublicates
	return
end

update oc
set Confirmed = 0
from Operator_Contact oc
	join #dublicates d on oc.Contact_ID = d.Contact_ID and oc.Operator_ID = d.Operator_ID
where d.Number > 1

drop table #dublicates