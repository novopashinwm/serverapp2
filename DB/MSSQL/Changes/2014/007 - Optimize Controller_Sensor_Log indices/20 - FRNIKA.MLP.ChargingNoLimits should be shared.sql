update bst
	set Shared = 1
	from Billing_Service_Type bst
	where bst.Shared = 0
	  and bst.LBS = 1
	  and bst.Service_Type in ('FRNIKA.MLP.ChargingNolimits', 'FRNIKA.MLP.ChargingNolimits.Trial')
	  