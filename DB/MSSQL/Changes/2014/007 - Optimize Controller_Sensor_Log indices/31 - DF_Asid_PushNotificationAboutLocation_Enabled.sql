if exists (
	select *
		from sys.objects 
		where name = 'DF_Asid_PushNotificationAboutLocation')
begin

	alter table Asid drop constraint DF_Asid_PushNotificationAboutLocation

end

go

if not exists (
	select *
		from sys.objects 
		where name = 'DF_Asid_PushNotificationAboutLocation_Enabled')
begin

	alter table Asid 
		add constraint DF_Asid_PushNotificationAboutLocation_Enabled 
			default (1) for PushNotificationAboutLocation

end
go

	update a 
		set PushNotificationAboutLocation = 1
	from Asid a
	join Contact ac on ac.ID = a.Contact_ID
	where a.PushNotificationAboutLocation = 0
	  and not exists (
		select *
			from Message_Contact mc
			join MESSAGE m on m.MESSAGE_ID = mc.Message_ID
			where mc.Contact_ID = a.Contact_ID
			  and mc.Type = 2
			  and m.BODY = '���������')
			  