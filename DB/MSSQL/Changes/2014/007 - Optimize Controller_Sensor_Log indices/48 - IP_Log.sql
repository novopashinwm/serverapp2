/* ������� ��������� ������ ����������� ������� ���������� �� ���������� IP � ��������� ���� ������ �� ����� IP */

if not exists (
	select *
		from sys.tables 
		where name = 'IP_Log'
)
begin

	create table IP_Log
	(
		Vehicle_ID int not null
			constraint PK_IP_Log
				primary key clustered,
		IP varchar(40) not null,
		FirstTime datetime not null,
		LastTime datetime not null
	)

end