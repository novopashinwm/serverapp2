insert into Billing_Service_Type(Service_Type, Service_Type_Category, MaxQuantity, LimitedQuantity, Singleton, Name, SMS, LBS, Shared, Removable, Network, DepartmentType)
	select *
		from (
			select 
				  Service_Type = 'Demo.Video1'
				, Service_Type_Category = 'Video'
				, MaxQuantity = 1
				, LimitedQuantity = 0
				, Singleton = 1
				, Name = '����� ���� 1'
				, SMS = 0
				, LBS = 0
				, Shared = 0
				, Removable = 1
				, Network = 0
				, DepartmentType = 0
			) t 
			where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)