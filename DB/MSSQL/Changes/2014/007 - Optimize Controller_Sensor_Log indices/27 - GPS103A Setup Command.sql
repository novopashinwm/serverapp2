insert into CommandTypes(id, code, ExecutionTimeout)
select id, code, ExecutionTimeout
from (
	select 37, 'Immobilize', 60
	union all
	select 38, 'Deimmobilize', 60
) t(id, code, ExecutionTimeout)
where not exists (select * from CommandTypes ct where ct.id = t.id)
go
insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.id
		from Controller_Type ct, CommandTypes cmd
		where ct.TYPE_NAME = 'GPS103A'
		  and cmd.code in ('Setup', 'Immobilize', 'Deimmobilize')
		  and not exists (select * from Controller_Type_CommandTypes e where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID and e.CommandTypes_ID = cmd.id)
go
update Controller_Type set SupportsPositionAsking = 1 where TYPE_NAME='GPS103A' and SupportsPositionAsking = 0
go