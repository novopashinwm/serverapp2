if exists (select * from sys.indexes where name='IX_AppClient_AppId' and object_id=object_id('AppClient'))
	drop index IX_AppClient_AppId on AppClient
go
create nonclustered index IX_AppClient_AppId on AppClient(AppId) include(Contact_ID)
go
-- можно удалить если ПЕРЕНЕСТИ поле ДАТА регистрации сообщения для мобильных приложений из MESSAGE_FIELD куда-нить еще
if exists (select * from sys.indexes where name='IX_MESSAGE_FIELD_MESSAGE_ID' and object_id=object_id('MESSAGE_FIELD'))
	drop index IX_MESSAGE_FIELD_MESSAGE_ID on MESSAGE_FIELD
go
create nonclustered index IX_MESSAGE_FIELD_MESSAGE_ID on MESSAGE_FIELD(MESSAGE_ID, MESSAGE_TEMPLATE_FIELD_ID) include (CONTENT)
go
update statistics [MESSAGE]
go
update statistics [Message_Contact]
go
update statistics AppClient
go
update statistics MESSAGE_TEMPLATE
go 
update statistics MESSAGE_TEMPLATE_FIELD
go