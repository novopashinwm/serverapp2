insert into TimeZone(Code)
select code 
from 
(
	select 'Russia Time Zone 3'
	union all
	select 'Russia Time Zone 10' 
	union all
	select 'Russia Time Zone 11'
) t(code)
where not exists 
(
	select *
	from TimeZone
	where Code = t.code
)