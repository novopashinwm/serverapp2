update ct
	set POS_SMOOTH_SUPPORT = 1
	from CONTROLLER_TYPE ct
	where TYPE_NAME = 'Meitrack MT90'
	  and ISNULL(POS_SMOOTH_SUPPORT, 0) = 0