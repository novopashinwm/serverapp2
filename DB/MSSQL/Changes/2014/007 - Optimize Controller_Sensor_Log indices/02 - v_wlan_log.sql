if exists (select * from sys.views where name = 'v_wlan_log')
	drop view v_wlan_log;
go

create view v_wlan_log 
	as
	select 
		Vehicle_ID,
		Log_Time,
		Number,
		SSID = (select value from wlan_ssid where id = wlan.wlan_ssid_id),
		Mac_Address = mac.Value,
		mac.IsMobileHotSpot,
		Channel_Number,
		SignalStrength
	from Wlan_Log wlan (nolock)
	join wlan_mac_address mac on id = wlan.wlan_mac_address_id
