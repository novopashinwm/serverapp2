declare @messageTemplate nvarchar(255) = 'FORIS.SPA.Request'

insert into MESSAGE_TEMPLATE (Name)
	select @messageTemplate
		where not exists (select * from Message_Template e where e.Name = @messageTemplate)
		
insert into MESSAGE_TEMPLATE_FIELD (Name, DOTNET_TYPE_ID, Message_Template_ID)
	select t.Name, dnt.DOTNET_TYPE_ID, mt.Message_Template_ID
		from (
			select Name =	 'ServiceCode'
			union all select 'ContractNumber'
			union all select 'TerminalDeviceNumber'
			union all select 'FMSISDN'
			union all select 'NewFMSISDN'
			union all select 'Quantity'
			union all select 'SimID'
			union all select 'SendSmsNotification'
			union all select 'CallingHost'
			union all select 'Action'
		) t
		join Message_Template mt on mt.Name = @messageTemplate
		join DotNet_Type dnt on dnt.TYPE_NAME = 'System.String'
		where not exists (
			select *
				from Message_Template_Field e
				where e.Message_Template_ID = mt.Message_Template_ID
				  and e.Name = t.Name)