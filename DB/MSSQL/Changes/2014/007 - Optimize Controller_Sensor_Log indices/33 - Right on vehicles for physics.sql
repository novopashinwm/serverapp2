/****** Script for SelectTopNRows command from SSMS  ******/
insert into [RIGHT](RIGHT_ID, SYSTEM, NAME, DESCRIPTION)
select RIGHT_ID, SYSTEM, NAME, DESCRIPTION
from 
(
	select 300, 1, 'VehicleView', 'Право используется для группировки прав на просмотр ТС для физ.лиц'
	union all
	select 301, 1, 'VehicleManagement', 'Право используется для группировки прав на управление ТС для физ.лиц'
) t(RIGHT_ID, SYSTEM, NAME, DESCRIPTION)
where not exists 
(
	select *
	from [RIGHT] r
	where r.RIGHT_ID = t.RIGHT_ID
)
