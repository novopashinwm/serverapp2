if exists (
	select *
		from sys.columns 
		where object_id = object_id('Controller')
		  and name = 'CloudContactID'
)
begin

	alter table Controller
		drop constraint FK_Controller_CloudContactID

	alter table Controller
		drop column CloudContactID
		
	alter table H_Controller
		drop column CloudContactID
		
end