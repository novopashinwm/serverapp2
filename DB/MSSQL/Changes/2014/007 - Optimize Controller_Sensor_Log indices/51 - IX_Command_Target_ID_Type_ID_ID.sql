if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Command_Target_ID_Type_ID_ID')
begin
	create nonclustered index IX_Command_Target_ID_Type_ID_ID
	on Command(Target_ID, Type_ID, ID)
end