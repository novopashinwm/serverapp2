if not exists (
	select *
		from sys.tables 
		where name = 'FreeMsisdn'
)
begin

	create table FreeMsisdn
	(
		name nvarchar(255),
		value varchar(32)
	)
	
end
go

insert into FreeMsisdn (value, name)
	select * from (
	  select value = '79161458528' , name = '�������� ����� '
	union all select '79162065059' , '������ ����� '
	union all select '79152000707' , '�������� ������ '
	union all select '79166388808' , '����� ����� '
	union all select '79161264412' , '������������� ������� '
	union all select '79851698929' , '��������� ��������� '
	union all select '79166661211' , '������� ����� '
	union all select '79858009875' , '������� ����� '
	union all select '79153580003' , '����� ����� '
	union all select '79175659755' , '�������� ���� '
	union all select '79162229449' , '������������ ���� '
	union all select '79851989070' , '������ ������ '
	union all select '79167503259' , '������ ����� '
	union all select '79152343819' , '����� ������� '
	union all select '79150160848' , '�������� ������� '
	union all select '79160305212' , '������ �������� '
	union all select '79112226143' , '����� �.'
	union all select '79852207796' , ' ��������� �.'
	union all select '79852580246' , ' ���������� �.'
	union all select '79154715535' , ' ���������� �.'
	union all select '79199949759' , ' ���������� �.'
	union all select '79175759619' , '������� �.'
	union all select '79859937168' , '������� �.'
	union all select '79161274855' , '������ �.'
	union all select '79165027527' , '������ �.'
	union all select '79857644834' , '������ �.'
	union all select '79166951092' , '������� �.'
	union all select '79167142216' , '��������� �.'
	union all select '79166096969' , '������� �.'
	union all select '79167082588' , '������� �.'
	union all select '79152929132' , '������ �.'
	union all select '79161604376' , '�������� �.'
	union all select '79152852280' , '�������� �.'
	union all select '79175028851' , '������� �. ���.'
	union all select '79152852302' , '������� �. ����.'
	union all select '79852579565' , 'Koscheev Yuri'
	union all select '79158019888' , '��������� ���������, MTS Bryansk'
	union all select '79147742802' , 'Artem, Khabarovsk'
	union all select '79110003636'	 , '�� ��  / ��������� ����  /  8 911 000 3636'
	union all select '79126440700' , '�� ����  /  ��������� ������  / 8 912 644 0700'
	union all select '79146534210' , '�� �� /  �������� �������  /  8 914 653 4210'
	union all select '73832993351' , '�� ������  /  ������� �����  /  8 383 299 3351'
	union all select '79103990210' , '�� ��������  /  ������� �����  /  8 910 399 0210'
	union all select '79858009481' , '�� �����  /  �������� �����  /  8 985 800 9481'
	union all select '79882480038' , '�� ��  /  �������� ���������  /  8 988 248 0038'
	union all select '79160585895' , '�� ������  /  ������ �������  /  9 916 058 5895'
	union all select '79166166437', '�� ������  /  ��������� �����  /  79166166437'
	union all select '79852117812', '�� ������  /  ��������� �����  /  79852117812'
	union all select '79104642250', '�� ������  /  ��������� �����  /  79104642250'
	union all select '79166023344', '�� ������  /  ��������� �����  /  79166023344'
	union all select '79154214591', '�� ������  /  ��������� �����  /  79154214591'
) t
where not exists (
	select * from FreeMsisdn e where e.value = t.value)