-- ���������� ������ ������ ��� ���� �������������
select o.OPERATOR_ID, null OPERATORGROUP_ID
	into #ops
from OPERATOR o
where not exists 
(
	select *
	from Operator_OperatorGroup oog
	where oog.Operator_ID = o.OPERATOR_ID and oog.Right_ID=118
)

declare @lastId int = (select max(OPERATORGROUP_ID) from OPERATORGROUP)

insert into OPERATORGROUP(NAME) 
select convert(nvarchar(250), #ops.OPERATOR_ID) from #ops

update #ops 
set OPERATORGROUP_ID = og.OPERATORGROUP_ID
from OPERATORGROUP og
where og.OPERATORGROUP_ID > @lastId and #ops.OPERATOR_ID = convert(int, og.NAME)

insert into Operator_OperatorGroup(Operator_ID, OperatorGroup_ID, Right_ID, Allowed)
select OPERATOR_ID, OPERATORGROUP_ID, 118, 1
from #ops

drop table #ops


-- �������� ���������� �����
select oog.Operator_ID, oog.OperatorGroup_ID, ROW_NUMBER() over(partition by Operator_ID order by OperatorGroup_ID) Number
	into #dbl
from Operator_OperatorGroup oog
where oog.Operator_ID in 
(
	select Operator_ID
	from Operator_OperatorGroup
	group by Operator_ID, Right_ID
	having Right_ID=118 and count(*) > 1
) and oog.Right_ID=118

insert into OPERATORGROUP_OPERATOR(OPERATOR_ID, OPERATORGROUP_ID)
select distinct d1.OPERATOR_ID, (select OPERATORGROUP_ID from #dbl d2 where d2.Number=1 and d2.Operator_ID=d1.Operator_ID)
from #dbl d1
where d1.Number > 1 and not exists
(
	select *
	from OPERATORGROUP_OPERATOR ogo
	where ogo.OPERATOR_ID = d1.Operator_ID and ogo.OPERATORGROUP_ID = (select OPERATORGROUP_ID from #dbl d3 where d3.Number=1 and d3.Operator_ID=d1.Operator_ID)
)

delete from OPERATORGROUP_OPERATOR where OPERATORGROUP_ID in (select OPERATORGROUP_ID from #dbl where Number > 1)
delete from Operator_OperatorGroup where ID in 
(
	select oog.ID 
	from #dbl d 
		join Operator_OperatorGroup oog on d.OperatorGroup_ID=oog.OperatorGroup_ID and d.Operator_ID=oog.Operator_ID
	where d.Number > 1 and oog.Right_ID = 118
)

drop table #dbl