--����� PayForCaller ������ ������������� ������������� ��������

insert into OPERATORGROUP_DEPARTMENT
	select ogd.OPERATORGROUP_ID, d.DEPARTMENT_ID, 111, 1
	from DEPARTMENT d
	join OPERATORGROUP_DEPARTMENT ogd on ogd.DEPARTMENT_ID = d.DEPARTMENT_ID
	                                 and ogd.ALLOWED = 1
	                                 and ogd.RIGHT_ID = 104 --DepartmentAccess
									 and not exists (
										select *
											from OPERATORGROUP_DEPARTMENT e
											where e.DEPARTMENT_ID = d.DEPARTMENT_ID
											  and e.ALLOWED = 1
											  and e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID
											  and e.RIGHT_ID = 2 --SecurityAdministrator
									 )
	join OPERATORGROUP og on og.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID
	where d.Type = 0
	 and not exists (
		select *
			from OPERATORGROUP_DEPARTMENT e
			where e.DEPARTMENT_ID = d.DEPARTMENT_ID
			  and e.ALLOWED = 1
			  and e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID
			  and e.RIGHT_ID = 111 --PayForCaller
	 )
	  
--��������� ������������ � ������ ������������� �� �������
insert into OPERATORGROUP_OPERATOR 
	select ogd.operatorgroup_id, o.OPERATOR_ID
	from OPERATOR o
	join Asid a on a.Operator_ID = o.OPERATOR_ID
	join DEPARTMENT d on d.DEPARTMENT_ID = a.Department_ID
	join OPERATORGROUP_DEPARTMENT ogd on ogd.DEPARTMENT_ID = d.DEPARTMENT_ID
										and ogd.ALLOWED = 1
										and ogd.RIGHT_ID = 104 --DepartmentsAccess
										and not exists (
										select *
											from OPERATORGROUP_DEPARTMENT e
											where e.DEPARTMENT_ID = d.DEPARTMENT_ID
											  and e.ALLOWED = 1
											  and e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID
											  and e.RIGHT_ID = 2 --SecurityAdministrator
									 )	
	where 1=1
	 and not exists (
		select *
			from OPERATORGROUP_OPERATOR e 
			where e.OPERATORGROUP_ID = ogd.OperatorGroup_ID 
			  and e.OPERATOR_ID = o.OPERATOR_ID)