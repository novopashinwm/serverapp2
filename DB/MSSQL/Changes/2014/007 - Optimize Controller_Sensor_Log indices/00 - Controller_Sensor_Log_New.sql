if not exists (
	select *
		from sys.tables
		where name = 'Controller_Sensor_Log_New'
)
begin

	create table Controller_Sensor_Log_New
	(
		Vehicle_ID int not null,
		Number smallint not null,
		Log_Time int not null,
		Value bigint not null,
		constraint PK_Controller_Sensor_Log_New primary key clustered (Vehicle_ID, Number, Log_Time)
	) on TSS_DATA2


end