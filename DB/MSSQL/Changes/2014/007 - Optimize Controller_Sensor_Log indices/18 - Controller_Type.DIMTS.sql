DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'DIMTS')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('DIMTS', 1000, 1, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
          select 1, 'DigitalInput1', 1
union all select 1, 'DigitalInput2', 2
union all select 1, 'Ignition', 100
union all select 1, 'SimTamper', 201
union all select 1, 'BoxTamper', 202
union all select 1, 'BatteryTamper', 203
union all select 1, 'BatteryLevel', 300

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Зажигание'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Ignition'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
	    
	    
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'BatteryLevel'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'BatteryLevel'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)