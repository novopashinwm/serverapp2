-- Удаляем вообще все некорректные подписки
;with cte as
(
	select SCHEDULERQUEUE_ID, SCHEDULEREVENT_ID, CONVERT(xml, CONFIG_XML) cfg
	from SCHEDULERQUEUE
)
select q.SCHEDULERQUEUE_ID
	into #ids
from cte q 
	join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID=e.SCHEDULEREVENT_ID 
where e.Report_Id is not null and (cfg.exist('//_operatorId') <> 1 or cfg.exist('//_allParametersNames') = 1)

delete from Billing_Service_SchedulerQueue where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from OperatorContact_SchedulerQueue where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from EMAIL_SCHEDULERQUEUE where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from SCHEDULERQUEUE where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
drop table #ids
go
-- Удаляем некорректные подписки именно для этого отчета
;with cte as
(
	select SCHEDULERQUEUE_ID, SCHEDULEREVENT_ID, CONVERT(xml, CONFIG_XML) cfg
	from SCHEDULERQUEUE
)
select q.SCHEDULERQUEUE_ID
	into #ids
from cte q 
	join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID=e.SCHEDULEREVENT_ID 
where e.Report_Id is not null and e.Report_Id is not null and (
cfg.exist('
	declare namespace a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportSpeedViolation";
	//a1:SpeedViolationCorporateReportParameters
') = 1
or
cfg.exist('
	declare namespace a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportSpeedViolation";
	//a1:SpeedViolationPhysicReportParameters
') = 1
)
delete from Billing_Service_SchedulerQueue where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from OperatorContact_SchedulerQueue where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from EMAIL_SCHEDULERQUEUE where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)
delete from SCHEDULERQUEUE where SchedulerQueue_ID in (select SCHEDULERQUEUE_ID from #ids)

drop table #ids
go
-- Удаляем подписки с VehicleId=0
delete from SCHEDULERQUEUE where convert(xml,CONFIG_XML).exist('//vehicleID[text()=0]') = 1
go