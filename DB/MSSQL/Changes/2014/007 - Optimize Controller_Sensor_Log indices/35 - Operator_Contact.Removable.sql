if not exists (
	select *
		from sys.columns 
		where object_id = object_id('Operator_Contact')
		  and name = 'Removable'
)
begin

	alter table Operator_Contact
		add Removable bit not null
			constraint DF_Operator_Contact_Removable default (1)
			
	alter table H_Operator_Contact
		add Removable bit not null
			constraint DF_H_Operator_Contact_Removable default (1)

end
go

insert into Operator_Contact (Operator_ID, Contact_ID, Removable)
	select a.Operator_ID, ac.Demasked_ID, 0
		from Asid a
		join Contact ac on ac.ID = a.Contact_ID
		where a.Operator_ID is not null
		  and ac.Demasked_ID is not null
		  and not exists (
			select *
				from Operator_Contact e
				where e.Operator_ID = a.Operator_ID
				  and e.Contact_ID = ac.Demasked_ID)
			
update oc
	set Removable = 0
	from Operator_Contact oc
	join Contact ac on ac.Demasked_ID = oc.Contact_ID
	join Asid a on a.Contact_ID = ac.ID and a.Operator_ID = oc.Operator_ID
	where oc.Removable <> 0
	
	