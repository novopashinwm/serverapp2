select v.VEHICLE_ID
 	,phone = 
		case 
			when coalesce(phoneContact.Value,'') != '' then phoneContact.Value 
			when coalesce(c.phone,'') != '' then c.phone
			else null
		end
	,controller_number = 
		case
			when ci.Device_ID is not null 
				and ci.Device_ID <> 0x00 
				and len(rtrim(ltrim(convert(varchar(32), ci.Device_ID)))) <> 0
			then rtrim(ltrim(convert(varchar(32), ci.Device_ID)))
			else null
		end
	,ct.TYPE_NAME
	into #v
from VEHICLE v
	join dbo.controller c on c.vehicle_id = v.vehicle_id
	join dbo.controller_type ct on ct.controller_type_id = c.controller_type_id		
	left outer join dbo.MLP_Controller mlpc on mlpc.Controller_ID = c.Controller_ID
	left outer join dbo.Asid a on a.ID = mlpc.Asid_ID
	left outer join Contact asidContact on asidContact.ID = a.Contact_ID
	left outer join Contact phoneContact on phoneContact.ID = asidContact.Demasked_ID
	left outer join dbo.Controller_Info ci on ci.Controller_ID = c.Controller_ID
where coalesce(GARAGE_NUMBER,'') = ''

select * from #v

update VEHICLE 
set GARAGE_NUMBER = COALESCE(#v.phone, #v.controller_number) 
from #v 
where #v.VEHICLE_ID = VEHICLE.VEHICLE_ID

drop table #v