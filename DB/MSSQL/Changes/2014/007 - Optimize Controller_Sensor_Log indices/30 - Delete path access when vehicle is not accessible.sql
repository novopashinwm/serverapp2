delete ov
	from OPERATOR_VEHICLE ov
	where ov.RIGHT_ID = 115 
	  and not exists (
		select *
			from v_operator_vehicle_right ovr
			where ovr.right_id = 102
			  and ovr.operator_id = ov.OPERATOR_ID
			  and ovr.vehicle_id = ov.VEHICLE_ID
	)
