if not exists (select * from sys.columns where object_id=object_id('Command') and name='Status')
	alter table Command add [Status] int null
go
if exists (select * from Command where Status is null)
	update Command
	set [Status] = 
		case
			-- Received
			when cr.Result_Type_ID = 0 then 0
			-- Processing
			when cr.Result_Type_ID = 1 then 1
			-- Completed
			else 2
		end
	from Command c
		left join Command_Result cr on c.Id = cr.Command_ID
go
if exists (select * from sys.columns c where object_id=object_id('Command') and name='Status' and c.is_nullable = 1)
	alter table Command alter column [Status] int not null
go
if exists (select * from Command_Result where Result_Type_ID = 1)
	update Command_Result set Result_Type_ID = 0 where Result_Type_ID = 1
go
if exists (select * from sys.columns where object_id=object_id('Command_Result') and name='Processed')
	alter table Command_Result drop column Processed
go
-- Закрываем все выполненные команды.
update cr 
	set cr.Result_Type_ID = 
		case when cr.Log_Time is not null then 2 else 18 end
	from Command c
	join Command_Result cr on cr.Command_ID = c.ID
	where c.[Status] = 2
	  and cr.Result_Type_ID = 0
go