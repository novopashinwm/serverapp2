if not exists (
	select *
		from sys.objects
		where name = 'Controller_Type_CommandTypes'
)
begin

	create table Controller_Type_CommandTypes
	(
		Controller_Type_ID int not null
			constraint FK_Controller_Type_CommandTypes_Controller_Type_ID
				foreign key references Controller_Type(Controller_Type_ID),
		CommandTypes_ID int not null
			constraint FK_Controller_Type_CommandTypes_CommandTypes_ID
				foreign key references CommandTypes(ID),
		constraint PK_Controller_Type_CommandTypes
			primary key clustered (Controller_Type_ID, CommandTypes_ID)
	)
	
	create nonclustered index IX_Controller_Type_CommandTypes_CommandTypes_ID on Controller_Type_CommandTypes(CommandTypes_ID)

end
go

insert into Controller_Type_CommandTypes
	select ct.Controller_Type_ID, cmd.ID
		from Controller_Type ct
		join CommandTypes cmd on 
			ct.TYPE_NAME = 'JB101' and 
				cmd.code in (
					'CutOffElectricity', 
					'ReopenElectricity',
					'CutOffFuel',
					'ReopenFuel',
					'CapturePicture'
					)
			or
			ct.TYPE_NAME = 'Sonim' and
				cmd.code in (
					'CancelAlarm',
					'ReloadDevice',
					'ShutdownDevice',
					'VibrateRequest',
					'CallPhone'
				)
			or
			ct.TYPE_NAME in ('Smarts Baby Bear', 'Smarts GT06', 'Smarts GT06N', 'Smarts GT02') and
				cmd.code = 'Setup'
			or
			ct.TYPE_NAME = 'Smarts Baby Bear' and
				cmd.code = 'CallPhone'
			or
			ct.TYPE_NAME in ('AvtoGraf', 'AvtoGrafCan') and 
				cmd.code = 'Setup'
			or
			ct.TYPE_NAME in ('GlobalSat TR-203', 'GlobalSat TR-600', 'GlobalSat GTR-128') and 
				cmd.code = 'Setup'
		where not exists (
			select * 
				from Controller_Type_CommandTypes e
				where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID
				  and e.CommandTypes_ID = cmd.id)