update c
	set PHONE = pc.Value
	  , PhoneContactID = pc.ID
	from Controller c
	join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
	join Asid a on a.ID = mc.Asid_ID
	join Contact ac on ac.ID = a.Contact_ID
	join Contact pc on pc.ID = ac.Demasked_ID
	where c.PHONE is null
	  and c.PhoneContactID is null
	  and not exists (
		select *
			from CONTROLLER e
			where e.PhoneContactID = pc.ID)