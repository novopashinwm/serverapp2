select SCHEDULERQUEUE_ID, convert(xml, q.CONFIG_XML) CONFIG
	into #SCHEDULERQUEUE
from SCHEDULERQUEUE q
	join SCHEDULEREVENT e on e.SCHEDULEREVENT_ID=q.SCHEDULEREVENT_ID
	join REPORT r on r.REPORT_ID = e.Report_Id
where REPORT_GUID in ('F824B9A6-F435-4940-9788-1C46495BB73F', 'E7E8996D-80C0-4F31-8E1E-E8859019983B'
	,'6E2DE7AD-2656-4812-BC1C-0EEA32613662', 'B4C4E567-3CA3-4252-BC9E-E9949FA9C7A7', '7CF57572-0F9A-4f82-9136-E191DF144C9A')

update #SCHEDULERQUEUE SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataTable
')
update #SCHEDULERQUEUE set CONFIG.modify('
	declare namespace a2="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data%2C%20Version%3D2.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3Db77a5c561934e089";
	delete //a2:DataTable
')
update #SCHEDULERQUEUE SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataSet
')
update #SCHEDULERQUEUE SET CONFIG.modify('
	delete //dsParamsForEditor
')

update SCHEDULERQUEUE
set CONFIG_XML = convert(nvarchar(max),q.CONFIG)
from #SCHEDULERQUEUE q
where q.SCHEDULERQUEUE_ID = SCHEDULERQUEUE.SCHEDULERQUEUE_ID

drop table #SCHEDULERQUEUE

select ID, convert(xml, q.CONFIG_XML) CONFIG
	into #H_SCHEDULERQUEUE
from H_SCHEDULERQUEUE q
	join SCHEDULEREVENT e on e.SCHEDULEREVENT_ID=q.SCHEDULEREVENT_ID
	join REPORT r on r.REPORT_ID = e.Report_Id
where REPORT_GUID in ('F824B9A6-F435-4940-9788-1C46495BB73F', 'E7E8996D-80C0-4F31-8E1E-E8859019983B'
	,'6E2DE7AD-2656-4812-BC1C-0EEA32613662', 'B4C4E567-3CA3-4252-BC9E-E9949FA9C7A7', '7CF57572-0F9A-4f82-9136-E191DF144C9A')

update #H_SCHEDULERQUEUE SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataTable
')
update #H_SCHEDULERQUEUE set CONFIG.modify('
	declare namespace a2="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data%2C%20Version%3D2.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3Db77a5c561934e089";
	delete //a2:DataTable
')
update #H_SCHEDULERQUEUE SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataSet
')
update #H_SCHEDULERQUEUE SET CONFIG.modify('
	delete //dsParamsForEditor
')

update H_SCHEDULERQUEUE
set CONFIG_XML = convert(nvarchar(max),q.CONFIG)
from #H_SCHEDULERQUEUE q
where q.ID = H_SCHEDULERQUEUE.ID

drop table #H_SCHEDULERQUEUE