if not exists (
	select *
		from sys.indexes
		where name = 'IX_Message_Status_DestinationType_ID_Time')
begin

	create nonclustered index IX_Message_Status_DestinationType_ID_Time on Message(Status, DestinationType_ID, Time)

end