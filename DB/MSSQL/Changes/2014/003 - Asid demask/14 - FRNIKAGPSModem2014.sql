insert into billing_Service_type (
	 Name
	, Service_Type
	, MaxQuantity
	, MinPrice
	, MaxPrice
	, LimitedQuantity
	, MinIntervalSeconds
	, ResetCounter
	, PeriodDays
	, Singleton
	, Service_Type_Category
	, Controller_Type_ID
	, Vehicle_Kind_ID
	, Shared
	, Removable)
	select * 
		from (
			select
			  Name = '����-�������'
			, Service_Type = 'FRNIKA.GPS.Modem.2014'
			, baseService.MaxQuantity
			, MinPrice = 350/2
			, MaxPrice = 350
			, baseService.LimitedQuantity
			, baseService.MinIntervalSeconds
			, baseService.ResetCounter
			, baseService.PeriodDays
			, baseService.Singleton
			, baseService.Service_Type_Category
			, baseService.Controller_Type_ID
			, baseService.Vehicle_Kind_ID
			, baseService.Shared
			, baseService.Removable
			from Billing_Service_Type baseService
			where baseService.Service_Type = 'FRNIKA.GPS.Modem'
		) t
		where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)
		