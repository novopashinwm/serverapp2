declare @templateName nvarchar(50) = 'PositionRequest'

insert into dbo.Message_Template(Name)
	select t.Name 
		from (
			  select Name = @templateName
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
		
insert into MESSAGE_TEMPLATE_FIELD (DOTNET_TYPE_ID, MESSAGE_TEMPLATE_ID, NAME)
	select dt.DotNet_Type_ID, mt.Message_Template_ID, t.Name
		from (select Name = 'VehicleID') t
		join DOTNET_TYPE dt on dt.TYPE_NAME = 'System.Int32'
		join MESSAGE_TEMPLATE mt on mt.NAME = @templateName
		where not exists ( 
			select * 
				from MESSAGE_TEMPLATE_FIELD mtf 
				where mtf.NAME = t.Name 
				  and mtf.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID)
				  
update mtf
	set DotNet_Type_ID = dt.DOTNET_TYPE_ID
	from MESSAGE_TEMPLATE_FIELD mtf
	join DOTNET_TYPE dt on dt.TYPE_NAME = 'System.Int32'
	where mtf.NAME = 'VehicleID'
	  and mtf.DOTNET_TYPE_ID <> dt.DOTNET_TYPE_ID