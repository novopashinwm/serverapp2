if not exists (
	select *
		from sys.columns c
		where c.object_id = OBJECT_ID('Controller_Type')
		  and c.name = 'SupportsPositionAsking'
)
begin

	alter table Controller_Type
		add SupportsPositionAsking bit not null
			constraint DF_Controller_Type_SupportsPositionAsking default(0)

	alter table H_Controller_Type
		add SupportsPositionAsking bit not null
			constraint DF_H_Controller_Type_SupportsPositionAsking default(0)

end