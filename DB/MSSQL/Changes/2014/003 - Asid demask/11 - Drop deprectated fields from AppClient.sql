if exists (
	select * 
		from sys.columns 
		where object_id = object_id('AppClient')
		  and name = 'GcmRegistrationId'
)
begin

	drop index IX_AppClient_GcmRegistrationId on AppClient

	alter table AppClient
		drop column GcmRegistrationId
		
	alter table AppClient
		drop column ApnToken
		
	alter table H_AppClient
		drop column GcmRegistrationId
		
	alter table H_AppClient
		drop column ApnToken

end
