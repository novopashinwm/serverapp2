if not exists (
	select *
		from sys.indexes i
		where i.object_id = object_id('Command_Result')
		  and name = 'IX_Command_Result_Result_Type_ID_Date_Received'
)
begin

	create nonclustered index IX_Command_Result_Result_Type_ID_Date_Received on Command_Result(Result_Type_ID, Date_Received)

end