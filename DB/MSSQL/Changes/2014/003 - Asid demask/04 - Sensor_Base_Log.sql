if not exists (
	select *
		from sys.tables t
		where t.name = 'Sensor_Base_Log'
)
begin

	create table Sensor_Base_Log
	(
		Vehicle_ID int not null,
		Number int not null,
		Log_Time int not null,
		Multiplier int not null,
		Value bigint,
		constraint PK_Sensor_Base_Log primary key clustered (Vehicle_ID, Number, Log_Time)
	) on TSS_DATA2

end