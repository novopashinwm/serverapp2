if not exists (
	select c.name
		from sys.columns c
		join sys.tables t on t.object_id = c.object_id
		where t.name = 'Contact'
		  and c.name = 'Demasked_ID'
)
begin

	alter table Contact
		add Demasked_ID int
			constraint FK_Contact_Demasked_ID
				foreign key references Contact(ID)

end