insert into billing_Service_type (
	 Name
	, Service_Type
	, MaxQuantity
	, MinPrice
	, MaxPrice
	, LimitedQuantity
	, MinIntervalSeconds
	, ResetCounter
	, PeriodDays
	, Singleton
	, Service_Type_Category
	, Controller_Type_ID
	, Vehicle_Kind_ID
	, Shared
	, Removable)
	select * 
		from (
			select
			  Name = '����-������'
			, Service_Type = 'FRNIKA.GPS.Phone.2014'
			, MaxQuantity = 100
			, MinPrice = 60
			, MaxPrice = 120
			, baseService.LimitedQuantity
			, baseService.MinIntervalSeconds
			, baseService.ResetCounter
			, baseService.PeriodDays
			, baseService.Singleton
			, baseService.Service_Type_Category
			, baseService.Controller_Type_ID
			, baseService.Vehicle_Kind_ID
			, baseService.Shared
			, baseService.Removable
			from Billing_Service_Type baseService
			where baseService.Service_Type = 'FRNIKA.GPS.Phone'
		) t
		where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)
		