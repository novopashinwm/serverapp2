if not exists (
	select *
		from sys.columns 
		where OBJECT_ID = OBJECT_ID('Controller')
		  and name = 'CloudContactID'
)
begin

	alter table Controller
		add CloudContactID int
			constraint FK_Controller_CloudContactID references Contact(ID)

	alter table H_Controller
		add CloudContactID int

end