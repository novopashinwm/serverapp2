/*		����������� ������� ������� WatchDA690*/

begin tran

declare @type_name varchar(100) = 'WatchDA690'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table 
(
	Descript			nvarchar(512),
	Number				int not null,
	IsDigital			bit not null,
	Default_Legend_Name	nvarchar(512)
);

set nocount on

insert into @controller_sensor (IsDigital, Descript, Number, Default_Legend_Name)
          select 0, 'BatteryCapacity', 1, 'BatteryLevel'
union all select 1, 'SOS',             2, '�������'

set nocount off

print 'insert into dbo.Controller_Sensor'

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	Descript,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Default_Constant,
	Mandatory)
select 
	@controller_type_id,
	case t.IsDigital when 1 then 2 else 1 end,
	t.NUMBER,
	t.Descript,
	l.CONTROLLER_SENSOR_LEGEND_ID,
	1,
	0,
	case when l.CONTROLLER_SENSOR_LEGEND_ID is not null then 1 else 0 end
	from @controller_sensor t
	outer apply (
		select l.CONTROLLER_SENSOR_LEGEND_ID
			from CONTROLLER_SENSOR_LEGEND l
			where l.NAME = t.Default_Legend_Name) l			
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = @controller_type_id
						  and cs.Number = t.Number)

update controller_sensor set mandatory = 1 where controller_type_id = @controller_type_id and isnull(mandatory, 0) = 0

commit