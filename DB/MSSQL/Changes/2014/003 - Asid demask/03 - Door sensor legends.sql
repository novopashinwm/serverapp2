create table #Controller_Sensor_Legend
(
	Controller_Sensor_Type_ID int,
	Name nvarchar(50),
	Number int
)

insert into #Controller_Sensor_Legend
	select cst.Controller_Sensor_Type_ID, t.Name, t.Number
		from dbo.Controller_Sensor_Type cst, (
			select '��������' CSTName, 81 Number, 'Door1'		Name
	  union select '��������' CSTName, 82 Number, 'Door2'		Name
	  union select '��������' CSTName, 83 Number, 'Door3'		Name
	  union select '��������' CSTName, 84 Number, 'Door4'		Name
	  ) t	
		where t.CSTName = cst.Name

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number)
	select * 
		from #Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)

drop table #Controller_Sensor_Legend
