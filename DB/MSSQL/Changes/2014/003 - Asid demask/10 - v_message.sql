if exists (
	select *
		from sys.views 
		where name = 'v_message'
)
	drop view v_message
go

create view v_message as
	select m.MESSAGE_ID
	    , UtcTime = m.Time
		, MskTime = dateadd(hour, 4, m.TIME)
		, mt.NAME, m.Owner_Operator_ID, m.BODY
		, Contacts = (select 
				case mc.type 
					when 1 then 'Destination' 
					when 2 then 'Source' 
					when 6 then 'Reference'
					else 'Other' 
				end + 
				' ' +
				case c.Type		
					when 1 then 'Email' 
					when 2 then 'Phone' 
					when 3 then 'Asid' 
					when 4 then 'Vehicle' 
					when 6 then 'Android' 
					when 7 then 'Apple' 
					else 'Other (' + CONVERT(varchar(10), c.Type) + ')' 
				end +
				': ' + c.Value + ' '
			from Message_Contact mc
			join Contact c on c.ID = mc.Contact_ID
			where mc.Message_ID = m.MESSAGE_ID
			for xml path (''))
		, Fields = (select mtf.Name + ': ' + mf.Content + ' '
						from MESSAGE_TEMPLATE_FIELD mtf
						join MESSAGE_FIELD mf on mf.MESSAGE_TEMPLATE_FIELD_ID = mtf.MESSAGE_TEMPLATE_FIELD_ID
						where mf.MESSAGE_ID = m.MESSAGE_ID
						for xml path (''))
		, State = case m.Status when 1 then 'Wait' when 2 then 'Pending' when 3 then 'Done' else 'Other (' + convert(varchar(10), m.Status) + ')' end
		, ProcessingResult = case ProcessingResult   
							  when 0 then 'Unknown'  
							  when 1 then 'Received'  
							  when 2 then 'Processing'  
							  when 3 then 'Sent'  
							  when 4 then 'ContactDoesNotExist'  
							  when 5 then 'UnsuccessfulBillingResult'  
							  when 6 then 'Throttled'  
							  when 7 then 'MessageQueueFull'  
							  when 8 then 'InvalidMessageLength'  
							  when 9 then 'InvalidDestinationAddress'  
							  when 10 then 'NoConnectionWithGateway'  
							  when 11 then 'HttpError'   
							  when 12 then 'Timeout'      
							  when 13 then 'ServiceTurnedOff'  
							  when 14 then 'Error'     
							  when 15 then 'DestinationIsNotSpecified'  
							  when 16 then 'Queued'  
							  else CONVERT(varchar, ProcessingResult)  
							 end  
		from MESSAGE m 
		left outer join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = m.TEMPLATE_ID
