if not exists (
	select *
		from sys.columns
		where object_id = object_id('CompoundRule')
		  and name = 'MotionStart'
)
begin

	alter table CompoundRule
		add MotionStart bit not null
			constraint DF_CompoundRule_MotionStart default (0)

	alter table H_CompoundRule
		add MotionStart bit not null
			constraint DF_H_CompoundRule_MotionStart default (0)

end