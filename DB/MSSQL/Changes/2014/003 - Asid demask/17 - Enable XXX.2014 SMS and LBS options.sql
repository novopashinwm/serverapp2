update Billing_Service_Type
	set SMS = 1, LBS = 1
	where Service_Type in ('FRNIKA.GPS.Phone.2014', 'FRNIKA.GPS.Modem.2014')
	  and (SMS = 0 or LBS = 0)
