create table Side10
(
	i int not null,
	primary key clustered (i)
)

declare @size int = 10
declare @i int = 0

set nocount on
while @i <= @size
begin
	insert into Side10 values(@i)
	set @i = @i + 1
end
set nocount off
go



create table Side180
(
	i int not null,
	primary key clustered (i)
)

declare @size int = 180
declare @i int = 0

set nocount on
while @i <= @size
begin
	insert into Side180 values(@i)
	set @i = @i + 1
end
set nocount off
go

create table Side360
(
	i int not null,
	primary key clustered (i)
)

declare @size int = 360
declare @i int = 0

set nocount on
while @i <= @size
begin
	insert into Side360 values(@i)
	set @i = @i + 1
end
set nocount off
go
