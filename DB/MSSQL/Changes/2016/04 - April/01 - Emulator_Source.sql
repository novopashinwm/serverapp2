--�������� ������ ��� ��������� �������� � ��������� �������, ����� ��� ������ �� �� ������ ���������� ��� ����������
--����� �������, ��� ��������� ������ ���������� ��������� ����� ������ �������� ��� ���� ������
--����� Log_Time ����������� ���������� �� ����

if not exists (select * from sys.tables where name = 'Emulator_Source')
begin

	create table Emulator_Source
	(
		Vehicle_ID int not null,
		Log_Time   int not null,
		constraint PK_Emulator_Source primary key clustered (Vehicle_ID, Log_Time),
		Data xml
	)

end
go

if not exists (select * from sys.columns where object_id = object_id('Emulator_Source') and name = 'Sensors')
begin
	exec sp_executesql N'alter table Emulator_Source add Sensors xml'
end

go

if exists (select * from sys.columns where object_id = object_id('Emulator_Source') and name = 'Data' and is_nullable = 1)
begin
	alter table Emulator_Source alter column Data xml not null
end