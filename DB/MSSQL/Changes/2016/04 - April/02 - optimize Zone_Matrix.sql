if exists (
	select * from sys.columns where object_id = object_id('ZONE_MATRIX') and name = 'ZONE_ID' and is_nullable = 1
)
begin

	create table Scale_2
	(
		Value tinyint not null 
			constraint PK_Scale 
				primary key clustered
	)
	insert into Scale_2 
		select Value from Scale

	create table Zone_Matrix_2
	(
		ZONE_ID int not null 
			constraint FK_ZONE_MATRIX_2_ZONE_ID 
				foreign key references Geo_Zone(Zone_ID),
		NODE bigint not null,			
		SCALE tinyint not null
			constraint FK_Zone_Matrix_2_Scale 
				foreign key references Scale_2(Value),
		IsFull bit not null,
		constraint PK_Zone_Matrix_2
			primary key clustered (Node, Zone_ID)
	)
	
	create nonclustered index IX_Zone_Matrix_Zone_ID_Node on Zone_Matrix_2(Zone_ID, Node) include (Scale, IsFull)

	insert into Zone_Matrix_2
		select Zone_ID, Node, Scale, IsFull
			from Zone_Matrix
			where Zone_ID is not null
			  and Node is not null
			  and Scale is not null
			  and IsFull is not null

	drop table Zone_Matrix
	drop table Scale
	
	exec sp_rename 'Zone_Matrix_2', 'Zone_Matrix'
	exec sp_rename 'Scale_2', 'Scale'
end