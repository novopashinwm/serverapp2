if not exists (
	select *
		from sys.columns
		where object_id = object_id('Message')
		  and name = 'Sent'
)
begin

	alter table Message 
		add [Sent] datetime,
			[Delivered] datetime

	alter table H_Message 
		add [Sent] datetime,
			[Delivered] datetime

end

