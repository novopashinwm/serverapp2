-- ��������� �������� ������ "���������� ������-����"

if not exists (
	select * from sys.columns where object_id = object_id('billing_service_type') and name = 'ServiceDays'
)
begin

	alter table billing_service_type
		add ServiceDays int

end

insert into Rendered_Service_Type
	select 3, 'ServiceDays'
		where not exists (select 1 from Rendered_Service_Type e where e.ID = 3)
