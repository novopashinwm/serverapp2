if not exists (select * from sys.indexes where name = 'H_Vehicle_Vehicle_ID_Action_Actual_Time')
begin

	create nonclustered index H_Vehicle_Vehicle_ID_Action_Actual_Time
		on H_Vehicle (Vehicle_ID, [Action], Actual_Time)

end