if not exists (select * from sys.indexes where name = 'IX_RenderedServiceItem_Billing_Service_ID_RenderDate')
begin

	create nonclustered index IX_RenderedServiceItem_Billing_Service_ID_RenderDate
		on RenderedServiceItem(Billing_Service_ID, RenderDate)

end