update f
	set DOTNET_TYPE_ID = newt.DOTNET_TYPE_ID	
	from message_template_field f
	join DOTNET_TYPE t on t.DOTNET_TYPE_ID = f.DOTNET_TYPE_ID
	join DOTNET_TYPE newt on newt.TYPE_NAME = 'System.DateTime'
	where f.name = 'date'
	  and f.DOTNET_TYPE_ID <> newt.DOTNET_TYPE_ID
