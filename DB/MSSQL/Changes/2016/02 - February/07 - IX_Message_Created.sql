if not exists (select * from sys.indexes where name = 'IX_Message_Created')
begin

	create nonclustered index IX_Message_Created
		on Message(Created) include (DestinationType_ID)

end