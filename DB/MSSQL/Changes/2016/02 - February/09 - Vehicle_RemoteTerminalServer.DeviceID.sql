--��������� ������������� ���������� �� ��������� �������
if not exists (
	select * from sys.columns where object_id = object_id('Vehicle_RemoteTerminalServer') and name = 'DeviceID'
)
begin

	alter table Vehicle_RemoteTerminalServer
		add DeviceID varchar(32)

	alter table H_Vehicle_RemoteTerminalServer
		add DeviceID varchar(32)

end