if not exists (
	select * from sys.columns where object_id = object_id('Trail') and name = 'SchedulerQueue_ID'
)
begin

	alter table Trail
		add SchedulerQueue_ID int

end