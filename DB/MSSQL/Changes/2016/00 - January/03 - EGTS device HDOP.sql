if exists (select * from sys.procedures where name = 'AddOrUpdateControllerSensor')
begin

	exec AddOrUpdateControllerSensor 'GRANIT_PERSONAL', 11, 'HDOP', 0, 'HDOP', 0
	exec AddOrUpdateControllerSensor 'GRANIT_AUTO',		11, 'HDOP', 0, 'HDOP', 0
	exec AddOrUpdateControllerSensor 'EGTS',			11, 'HDOP', 0, 'HDOP', 0

	exec AddOrUpdateControllerSensor 'GRANIT_PERSONAL', 12, 'VDOP', 0
	exec AddOrUpdateControllerSensor 'GRANIT_AUTO',		12, 'VDOP', 0
	exec AddOrUpdateControllerSensor 'EGTS',			12, 'VDOP', 0

	exec AddOrUpdateControllerSensor 'GRANIT_PERSONAL', 13, 'PDOP', 0
	exec AddOrUpdateControllerSensor 'GRANIT_AUTO',		13, 'PDOP', 0
	exec AddOrUpdateControllerSensor 'EGTS',			13, 'PDOP', 0

	exec AddOrUpdateControllerSensor 'GRANIT_PERSONAL', 14, 'AnalogueInput1', 0
end