if not exists (
	select * from sys.columns where object_id = object_id('Billing_Service') and name = 'PurchaseToken'
)
begin

	alter table Billing_Service 
		add PurchaseToken varchar(900)

	alter table H_Billing_Service
		add PurchaseToken varchar(900)

end
go

if not exists (
	select * from sys.indexes where name = 'IX_Billing_Service_PurchaseToken'
)
begin
	create unique nonclustered index IX_Billing_Service_PurchaseToken
		on Billing_Service(PurchaseToken)
		where PurchaseToken is not null
end