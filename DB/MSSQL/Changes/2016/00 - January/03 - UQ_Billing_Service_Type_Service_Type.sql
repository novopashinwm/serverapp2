select Service_Type, MaxQuantity
	from Billing_Service_Type 
	where Service_Type in (
		select Service_Type from Billing_Service_Type group by Service_Type having count(1) > 1)
	order by Service_Type

update bst
	set Service_Type = Service_Type + convert(varchar(12), MaxQuantity)
	from Billing_Service_Type bst
	where bst.Service_Type in ('FRNIKA.MLP.ChargingEconom', 'FRNIKA.SMS.LBS', 'FRNIKA.SMS.M2M')
	  and bst.MaxQuantity in (500, 10000, 50000)
go

create unique nonclustered index UQ_Billing_Service_Type_Service_Type
	on Billing_Service_Type(Service_Type)
	