if not exists (select * from sys.types where name = 'Calibrate_Param')
	create type Calibrate_Param as table (RawValue bigint, ActualValue numeric(18, 9))
