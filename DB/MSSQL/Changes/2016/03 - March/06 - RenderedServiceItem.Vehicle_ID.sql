if not exists (
	select * from sys.columns where object_id = object_id('RenderedServiceItem') and name = 'Vehicle_ID'
)
begin

	alter table RenderedServiceItem
		add Vehicle_ID int 
			constraint FK_RenderedServiceItem_Vehicle_ID foreign key references Vehicle(Vehicle_ID)

end
go

if ((select is_nullable from sys.columns where object_id = object_id('RenderedServiceItem') and name = 'Asid_ID') <> 1)
begin

	drop index IX_RenderedServiceItem_Asid_ID_Billing_Service_ID_RenderDate 
		on RenderedServiceItem

	alter table RenderedServiceItem
		alter column Asid_ID int

	create nonclustered index IX_RenderedServiceItem_Asid_ID_Billing_Service_ID_RenderDate on 
		RenderedServiceItem(Asid_ID, Billing_Service_ID, RenderDate)

end
