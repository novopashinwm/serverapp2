if not exists (
	select * from sys.columns where object_id = object_id('Vehicle') and name = 'PaidTill'
)
begin

	alter table Vehicle 
		add PaidTill datetime
		
	alter table H_Vehicle
		add PaidTill datetime

end