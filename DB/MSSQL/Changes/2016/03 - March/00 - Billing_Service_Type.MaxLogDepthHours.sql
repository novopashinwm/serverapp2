if not exists (
	select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'MaxLogDepthHours'
)
begin

	alter table Billing_Service_Type
		add MaxLogDepthHours int

end
go

if exists (
	select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'MaxLogDepthDays'
)
begin

	exec sp_executesql N'
	update Billing_Service_Type
		set MaxLogDepthHours = MaxLogDepthDays * 24
		where MaxLogDepthDays is not null
		  and MaxLogDepthHours is null'

	alter table Billing_Service_Type
		drop column MaxLogDepthDays

end