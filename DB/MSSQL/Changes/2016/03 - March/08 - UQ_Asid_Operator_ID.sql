if not exists (select * from sys.indexes where name = 'UQ_Asid_Operator_ID')
begin

	update ua
		set Operator_ID = null
		from (
			select a.Operator_ID, Left_Asid_ID = max(a.ID)
				from Asid a
				where a.Operator_ID is not null
				group by a.Operator_ID 
				having count(1) > 1
		) a
		join Asid ua on ua.Operator_ID = a.Operator_ID and a.Operator_ID <> Left_Asid_ID

	create unique nonclustered index UQ_Asid_Operator_ID
		on Asid(Operator_ID) where Operator_ID is not null

end