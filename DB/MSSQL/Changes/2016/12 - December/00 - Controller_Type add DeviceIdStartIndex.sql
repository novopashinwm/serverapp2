if not exists (
	select *
		from sys.columns 
		where object_id = object_id('Controller_Type')
		  and name = 'DeviceIdStartIndex'
)
begin

	alter table Controller_Type add DeviceIdStartIndex int null

	alter table Controller_Type add DeviceIdLength int null

	alter table H_Controller_Type add DeviceIdStartIndex int null
	alter table H_Controller_Type add DeviceIdLength int null

end