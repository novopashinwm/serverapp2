if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Command')
		  and name = 'Created'
)
begin

	alter table Command
		add Created datetime not null
			constraint DF_Command_Created default (getutcdate())

end