if not exists (select * from sys.columns where object_id=object_id('CONTROLLER_INFO') and name='IMSI')
	alter table CONTROLLER_INFO add IMSI varchar(15) null
go
if not exists (select * from sys.columns where object_id=object_id('H_CONTROLLER_INFO') and name='IMSI')
	alter table H_CONTROLLER_INFO add IMSI varchar(15) null
go