DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'Nika.Tracker')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, Default_Vehicle_Kind_ID, SupportsPositionAsking)
      VALUES
            ('Nika.Tracker', 200, 0, 5, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on
declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
select 1, 'Alarm', 38
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '�������'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Alarm'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

select * 
from CONTROLLER_SENSOR cs
	join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID