if not exists (
	select *
		from sys.objects 
		where name like 'DF_Session_LastActivity'
)
begin

	alter table Session
		add constraint DF_Session_LastActivity default (getutcdate()) for LastActivity 

end