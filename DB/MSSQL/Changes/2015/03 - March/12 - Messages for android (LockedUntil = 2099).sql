update m set m.STATUS = 3
from [MESSAGE] m
	join Message_Contact mc on mc.Message_ID = m.MESSAGE_ID
	join Contact c on mc.Contact_ID = c.ID
where DestinationType_ID = 9 and c.LockedUntil = '2099' and m.STATUS != 3
