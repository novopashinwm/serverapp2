if exists (
	select *
		from sys.indexes 
		where name = 'IX_CONTROLLER_SENSOR_MAP_Controller_ID'
)
	drop index IX_CONTROLLER_SENSOR_MAP_Controller_ID on Controller_Sensor_Map
go

if exists (
	select *
		from sys.indexes 
		where name = 'PK_CONTROLLER_SENSOR_MAP'
)
begin
	alter table Controller_Sensor_Map 
		drop PK_CONTROLLER_SENSOR_MAP

end

go

if exists (
	select *
		from sys.indexes 
		where name = 'IX_Controller_Sensor_Map_CONTROLLER_SENSOR_ID'
)
	drop index IX_Controller_Sensor_Map_CONTROLLER_SENSOR_ID on Controller_Sensor_Map
go

if not exists (
	select *
		from sys.indexes 
		where name = 'PK_Controller_Sensor_Map_Controller_ID'
)
begin

	alter table Controller_Sensor_Map
		add constraint PK_Controller_Sensor_Map
			primary key nonclustered (Controller_Sensor_Map_ID)
			
	create clustered index IX_Controller_Sensor_Map_Controller_ID
		on Controller_Sensor_Map(Controller_ID, Controller_Sensor_Legend_ID, Controller_Sensor_ID, Min_Value)	

end