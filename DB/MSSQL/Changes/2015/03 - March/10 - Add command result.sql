if object_id('tempdb..#ids') is not null drop table #ids

select c.ID
	into #ids
from Command c
	left join Command_Result r on r.Command_ID = c.ID
where r.ID is null

insert into Command_Result (Command_ID, Date_Received, Result_Type_ID)
select ID, GETUTCDATE(), 3
from #ids

update Command set Status = 2
where Id in (select Id from #ids)

drop table #ids
