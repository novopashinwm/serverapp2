if exists (
	select *
		from sys.objects
		where name = 'FK_TRAIL_Message_ID'
)
begin

	alter table Trail
		drop constraint FK_Trail_Message_ID

end