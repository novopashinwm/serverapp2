if not exists (select * from sys.columns where object_id = object_id('Contact') and name = 'Created')
	alter table Contact add Created datetime null constraint DF_Contact_Created default(getutcdate())
go

declare @template_id int = (select MESSAGE_TEMPLATE_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'DemaskAsidRequest')
update c set Created = 
(
	select top(1) m.TIME
	from [Message] m with (nolock) 
		join Message_Contact mc with (nolock) on m.MESSAGE_ID = mc.Message_ID
	where mc.Contact_ID = c.ID and mc.Type = 6 and m.TEMPLATE_ID = @template_id
	order by m.TIME desc
) 
from Contact c
where c.[Type] = 3 and Created is null

update d set Created = c.Created
from Contact c
	join Contact d on c.Demasked_ID = d.ID
where c.Created is not null and d.Created is null


