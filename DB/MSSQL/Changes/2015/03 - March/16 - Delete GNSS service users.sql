select a.Operator_ID
	into #t
	from Billing_Service_Type bst
	join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
	join Asid a on a.ID = bs.Asid_ID
	outer apply (
		select top(1) s.SESSION_ID, s.LastActivity
			from SESSION s with (nolock)
			where s.OPERATOR_ID = a.OPERATOR_ID
			order by s.LastActivity desc) s
	where bst.Service_Type_Category = 'GNSS'
	  and a.Operator_ID is not null
	  and s.SESSION_ID is null
	  and a.SimID is null

while exists (select 1 from #t)
begin

	declare @operator_id int
	set @operator_id = (select top(1) operator_id from #t)
	delete #t where operator_id = @operator_id
	
	delete x from Operator_Department x where x.Operator_ID = @operator_id
	delete x from OperatorGroup_Operator x where x.Operator_ID = @operator_id
	delete x from Operator_Contact x where x.Operator_ID = @operator_id
	delete x from Operator_Vehicle x where x.Operator_ID = @operator_id

	update x set login = ''  from Operator x where x.Operator_ID = @operator_id
	
	update x set Operator_ID = null from Asid x where Operator_ID = @operator_id

end

drop table #t