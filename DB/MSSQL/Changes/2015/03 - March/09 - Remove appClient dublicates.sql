delete ac
from ( 
	select Contact_ID, Max_ID = MAX(ID)
	from AppClient 
	where Contact_ID is not null
	group by Contact_ID
	having COUNT(1)>1
) t
join AppClient ac on ac.Contact_ID = t.Contact_ID
join Contact c on ac.Contact_ID = c.ID
where ac.ID <> t.Max_ID