if not exists (select * from sys.columns where name = 'Id' and object_id = object_id('VEHICLE_PROFILE'))
	alter table VEHICLE_PROFILE add ID int identity(1,1) constraint PK_VEHICLE_PROFILE primary key(ID)
go
if not exists (select * from sys.tables where name = 'H_VEHICLE_PROFILE')
	CREATE TABLE H_VEHICLE_PROFILE (
		ID int IDENTITY(1,1) NOT NULL,
		TRAIL_ID int NOT NULL,
		ACTION nvarchar(6) NOT NULL,
		ACTUAL_TIME datetime NOT NULL,
		VEHICLE_PROFILE_ID int,
		VEHICLE_ID int NOT NULL,
		PROPERTY_NAME varchar(50) NOT NULL,
		comment ntext NULL,
		Value nvarchar(max) NULL,
		constraint PK_H_VEHICLE_PROFILE primary key(ID),
		constraint FK_H_VEHICLE_PROFILE_TRAIL_ID foreign key(TRAIL_ID) references TRAIL(TRAIL_ID),
	)
GO
if not exists (select * from sys.indexes where name='IX_H_VEHICLE_PROFILE_Vehicle_ID_Property_Name' and object_id = object_id('H_VEHICLE_PROFILE'))
	create nonclustered index IX_H_VEHICLE_PROFILE_Vehicle_ID_Property_Name on H_VEHICLE_PROFILE(VEHICLE_ID, PROPERTY_NAME) include(Value, ACTUAL_TIME)
go