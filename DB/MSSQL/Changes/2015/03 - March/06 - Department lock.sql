if not exists (select * from sys.columns where object_id=object_id('DEPARTMENT') and name='LockDate')
	alter table DEPARTMENT add LockDate datetime null
go
if not exists (select * from sys.columns where object_id=object_id('H_DEPARTMENT') and name='LockDate')
	alter table H_DEPARTMENT add LockDate datetime null
go