if not exists (
	select *
		from sys.objects
		where name = 'DF_Message_Created'
)
begin

	alter table Message
		add constraint DF_Message_Created default (getutcdate()) for Created

end