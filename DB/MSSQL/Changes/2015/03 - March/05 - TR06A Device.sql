DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'TR06A')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('TR06A', 1000, 0, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END
else
begin
	update CONTROLLER_TYPE set DeviceIdIsImei = 0 where Controller_Type_Id = @CONTROLLER_TYPE_ID and DeviceIdIsImei = 1
end

set nocount on
declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
select 1, 'DigitalInputs', 100
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)

insert into Controller_Type_CommandTypes(CommandTypes_ID, Controller_Type_ID)
select t.id, @CONTROLLER_TYPE_ID
from CommandTypes t
where t.code = 'Setup' and not exists (
	select top 1 1
	from Controller_Type_CommandTypes
	where CommandTypes_ID = t.id and Controller_Type_ID = @CONTROLLER_TYPE_ID
)
