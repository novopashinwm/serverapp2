if not exists (select * from sys.tables where name = 'Session_Info')
	create table Session_Info (
		SessionId int not null 
			constraint FK_Session_Info_SessionId foreign key references [SESSION](SESSION_ID) 
			constraint PK_Session_Info primary key,
		Reason nvarchar(255) null,
		ParentSessionId int not null 
			constraint FK_Session_Info_ParentSessionId foreign key references [SESSION](SESSION_ID),
	)
go