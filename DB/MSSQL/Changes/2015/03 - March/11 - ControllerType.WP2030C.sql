DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'WP2030C')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('WP2030C', 65535, 1, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
          select 1, 'Ignition', 1
union all select 1, 'Door', 2
union all select 1, 'Sos', 3
union all select 1, 'DigitalInput4', 4
union all select 1, 'DigitalInput5', 5
union all select 1, 'DigitalInput6', 6
union all select 1, 'DigitalInput7', 7
union all select 1, 'DigitalInput8', 8
union all select 1, 'DigitalInput9', 9
union all select 1, 'DigitalInput10', 10
union all select 1, 'DigitalInput11', 11
union all select 1, 'DigitalInput12', 12
union all select 1, 'DigitalInput13', 13
union all select 1, 'DigitalInput14', 14

union all select 1, 'Voltage', 20
union all select 1, 'Fuel', 21
union all select 1, 'Tank', 22
union all select 1, 'Temperature', 23
union all select 1, 'InternalBattery', 24

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Зажигание'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Ignition'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Тревога'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Sos'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Температура в салоне'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Temperature'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Напряжение питания'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Voltage'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Датчик топлива'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Fuel'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
