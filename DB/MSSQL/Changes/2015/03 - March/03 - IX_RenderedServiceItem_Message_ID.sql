if not exists (
	select *
		from sys.indexes 
		where name = 'IX_RenderedServiceItem_Message_ID'
)
begin

	create nonclustered index IX_RenderedServiceItem_Message_ID
		on RenderedServiceItem(Message_ID)

end
