if exists (
	select *
		from sys.indexes 
		where name = 'IX_Vehicle_Rule_Processing_Vehicle_ID'
		  and type_desc = 'NONCLUSTERED'
)
begin

	drop index IX_Vehicle_Rule_Processing_Vehicle_ID on Vehicle_Rule_Processing

end
go

if exists (
	select *
		from sys.indexes 
		where name = 'PK_Vehicle_Rule_Processing'
)
begin

	alter table Vehicle_Rule_Processing
		drop constraint PK_Vehicle_Rule_Processing

	alter table Vehicle_Rule_Processing
		add constraint PK_Vehicle_Rule_Processing
			primary key nonclustered (ID)

end

go

if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Vehicle_Rule_Processing_Vehicle_ID'
		  and type_desc = 'CLUSTERED'
)
begin

	create clustered index IX_Vehicle_Rule_Processing_Vehicle_ID
		on Vehicle_Rule_Processing(Vehicle_ID)

end

go

if exists (
	select *
		from sys.indexes 
		where name = 'IX_Vehicle_Rule_Processing_Host_ID_LastTime'
)
begin
	
	drop index IX_Vehicle_Rule_Processing_Host_ID_LastTime on Vehicle_Rule_Processing
	
	create nonclustered index IX_Vehicle_Rule_Processing_Host_ID_LastTime_Vehicle_ID_CurrentTime
		on Vehicle_Rule_Processing(Host_ID, LastTime) include (Vehicle_ID, CurrentTime)
	
end
