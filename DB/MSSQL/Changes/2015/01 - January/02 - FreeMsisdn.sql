if not exists (
	select *
		from sys.columns
		where object_id = object_id('FreeMsisdn')
		  and name = 'endDate'
)
begin

	alter table FreeMsisdn
		add endDate datetime

end