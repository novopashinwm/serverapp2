update c set PHONE = pc.Value, PhoneContactID = pc.ID
	from Contact pc 
	join Contact ac on ac.Demasked_ID = pc.ID
	join Asid a on a.Contact_ID = ac.ID
	join MLP_Controller mc on mc.Asid_ID = a.ID
	join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
	join VEHICLE v on v.VEHICLE_ID = c.VEHICLE_ID
	where c.PhoneContactID is null
	  and not exists (
		select *
			from Controller e
			where e.PhoneContactID = pc.ID)
	  
