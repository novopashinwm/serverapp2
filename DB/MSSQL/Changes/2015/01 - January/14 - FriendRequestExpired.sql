declare @name varchar(100) = 'FriendRequestExpired'
insert into CONSTANTS (NAME, VALUE)
select Name, Value
from (
	select @name, '1200'
) t(Name, Value)
where not exists (select * from CONSTANTS where name = @name)