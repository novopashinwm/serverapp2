declare @serviceType varchar(255) = 'NIKA.Manual.LBS.Unlimited'

insert into Billing_Service_Type (Service_Type, Service_Type_Category, LimitedQuantity, MinIntervalSeconds, Name, SMS, LBS, IsManual)
	select t.*
		from (
			select 
			  Service_Type = @serviceType
			, Service_Type_Category = 'LBS'
			, LimitedQuantity = 0
			, MinIntervalSeconds = 15*60
			, Name = '��� LBS ������ ����� ��������������� ��'
			, SMS = 1
			, LBS = 1
			, IsManual = 1
		) t
		where not exists (
			select *
				from Billing_Service_Type e
				where e.Service_Type = t.Service_Type)
				
update Billing_Service_Type set IsManual = 1 where Service_Type = @serviceType and IsManual <> 1