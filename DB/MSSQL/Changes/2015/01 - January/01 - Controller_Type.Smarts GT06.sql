DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'Smarts GT06')

declare @sensor table(digital bit, name varchar(255), number int)

set nocount on
insert into @sensor
		  select 1, 'VoltageLevel', 2
union all select 1, 'Charged', 1
union all select 1, 'GasOilElectricityConnected', 3
union all select 1, 'GpsTracking', 4
union all select 1, 'Alarm', 5
union all select 1, 'Acc', 6
union all select 1, 'Status', 7
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
select ct.Controller_Type_ID
	, case s.digital when 1 then 2 else 1 end
	, s.number
	, s.name
from @sensor s
	join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
where not exists (
	select * 
	from Controller_Sensor e
	where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
		and e.NUMBER = s.number)

set nocount on
declare @legend table(controller_sensor_name varchar(100), legend_name nvarchar(100))
insert into @legend
	(controller_sensor_name, legend_name)
values ('VoltageLevel', 'Напряжение питания')
	  ,('Acc', 'Зажигание')
set nocount off

delete csm
from CONTROLLER_SENSOR_MAP csm
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_SENSOR_ID = csm.CONTROLLER_SENSOR_ID
	join CONTROLLER_SENSOR_LEGEND csl on cs.Default_Sensor_Legend_ID = csl.CONTROLLER_SENSOR_LEGEND_ID
where csl.NAME = 'GpsEnabled' and cs.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID

update cs 
set Default_Sensor_Legend_ID = null
from CONTROLLER_SENSOR cs
	join CONTROLLER_SENSOR_LEGEND csl on cs.Default_Sensor_Legend_ID = csl.CONTROLLER_SENSOR_LEGEND_ID
where csl.NAME = 'GpsEnabled' and CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID 

update cs
set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	, Default_Multiplier = 1
	, Default_Constant = 0
	, Mandatory = 1
from Controller_Sensor cs
	join @legend l on cs.Descript = l.controller_sensor_name
	join Controller_Sensor_Legend legend on legend.Name = l.legend_name
where cs.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	and (cs.Default_Sensor_Legend_ID is null or cs.Mandatory = 0)

