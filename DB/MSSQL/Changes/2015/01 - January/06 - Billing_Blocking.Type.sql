if not exists(
	select *
		from sys.columns
		where object_id = object_id('Billing_Blocking')
		  and name = 'Type'
)
begin

	alter table Billing_Blocking
		add Type int not null
			constraint DF_Billing_Blocking_Type default(0)	

	alter table H_Billing_Blocking
		add Type int not null
			constraint DF_H_Billing_Blocking_Type default(0)	

end