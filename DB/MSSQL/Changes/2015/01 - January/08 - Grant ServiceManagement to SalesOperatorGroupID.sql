declare @right_id int = 110 --ServiceManagement
declare @superAdminGroup int = convert(int, (select value from CONSTANTS where NAME = 'SalesOperatorGroupID'))

insert into OPERATORGROUP_DEPARTMENT (OPERATORGROUP_ID, DEPARTMENT_ID, RIGHT_ID, ALLOWED)
	select ogd.OPERATORGROUP_ID, ogd.DEPARTMENT_ID, @right_id, 1
		from OPERATORGROUP_DEPARTMENT ogd
		where ogd.OPERATORGROUP_ID = @superAdminGroup
		  and ogd.ALLOWED = 1
		  and ogd.RIGHT_ID = 2 --SecurityAdministration
		  and not exists (
			select *
				from OPERATORGROUP_DEPARTMENT e
				where e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID 
				  and e.DEPARTMENT_ID = ogd.DEPARTMENT_ID
				  and e.RIGHT_ID = @right_id)