if not exists (
	select *
		from sys.columns
		where object_id = object_id('Billing_Service_Type')
		  and name = 'IsManual'
)
begin

	alter table Billing_Service_Type
		add IsManual bit not null
			constraint DF_Billing_Service_Type_IsManual default(0)

end