declare @serviceType varchar(255) = 'INDIA.SMS.Limited'

insert into Billing_Service_Type (Service_Type, Service_Type_Category, MaxQuantity, LimitedQuantity, ResetCounter, Name, SMS)
	select t.*
		from (
			select 
			  Service_Type = @serviceType
			, Service_Type_Category = 'SMS'
			, MaxQuantity = 50
			, LimitedQuantity = 1
			, ResetCounter = 'monthly'
			, Name = 'SMS pack'
			, SMS = 1
		) t
		where not exists (
			select *
				from Billing_Service_Type e
				where e.Service_Type = t.Service_Type)
				
--����� �������� ������ �� ������������ ������� � �����
insert into Billing_Service (Vehicle_ID, Billing_Service_Type_ID, StartDate)
	select v.VEHICLE_ID, bst.ID, GETUTCDATE()
		from Country c
		join Department d on d.Country_ID = c.Country_ID
		join Vehicle v on v.DEPARTMENT = d.DEPARTMENT_ID
		join Billing_Service_Type bst on bst.Service_Type = @serviceType
		where c.Name = 'India'
		  and not exists (
			select *
				from Billing_Service e 
				where e.Vehicle_ID = v.VEHICLE_ID
				  and e.Billing_Service_Type_ID = bst.ID)

while 1=1
begin
	declare @bs_id int
	
	set @bs_id = (
		select top(1) bs.ID
			from Billing_Service_Type bst
			join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
			where bst.ResetCounter = 'monthly'
			  and not exists (
				select *
					from Billing_Service_SchedulerQueue e
					where e.Billing_Service_ID = bs.ID))

	if @bs_id is null
		break
	
	declare @sq_id int

	begin tran
		
	insert into schedulerqueue (SCHEDULEREVENT_ID, NEAREST_TIME, REPETITION_XML, Enabled)
		select 
			se.SchedulerEvent_ID
			, dateadd(month, 1, GETUTCDATE())
			, '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:clr="http://schemas.microsoft.com/soap/encoding/clr/1.0" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">  <SOAP-ENV:Body>  <a1:RepeatOnNthDayOfMonth id="ref-1" xmlns:a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces">  <n>1</n>  <month>0</month>  <comment xsi:null="1"/>  </a1:RepeatOnNthDayOfMonth>  </SOAP-ENV:Body>  </SOAP-ENV:Envelope>'
			, 1
		from SCHEDULEREVENT se
		where se.COMMENT = 'Reset billing service counter value'

	set @sq_id = @@identity

	insert into Billing_Service_SchedulerQueue
		select @bs_id, @sq_id
	
	commit
	
end

--�������� �������� ���� �������������� SMS ������ ����
update m
	set STATUS = 3
	from Message m
	where m.STATUS <> 3
	  and m.DestinationType_ID = 1 --Red Hat SMS Gateway
	  and m.TIME < DATEADD(hour, -1, getutcdate())
	  
--������� ������ ����������� SMS � �����
update bs
	set EndDate = GETUTCDATE()
	from Billing_Service_Type bst
	join Billing_Service bs on bs.Billing_Service_Type_ID = bst.ID
	where bst.Service_Type = 'INDIA.SMS.Unlim'
	  and bs.EndDate is null