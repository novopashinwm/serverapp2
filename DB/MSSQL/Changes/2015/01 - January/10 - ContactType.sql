if not exists (
	select *
		from sys.tables 
		where name = 'ContactType'
)
begin

	create table ContactType
	(
		Id int not null
			constraint PK_ContactType
				primary key clustered,
		Name varchar(255) not null
	)

end
go

insert into ContactType (Id, Name)
	select t.*
		from (
			select Id=1, Name='Email' 
			union all select 2, 'Phone' 
			union all select 3, 'Asid' 
			union all select 4, 'Vehicle' 
			union all select 5, 'Operator'
			union all select 6, 'Android' 
			union all select 7, 'Apple' 
		) t
		where not exists (
			select *
				from ContactType e
				where e.Id = t.Id)
				
go

if not exists (
	select *
		from sys.objects 
		where name = 'FK_Contact_Type'
)
begin

	alter table Contact
		add constraint FK_Contact_Type
			foreign key (Type) references ContactType(Id)

end