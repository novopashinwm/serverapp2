if not exists (select * from sys.tables where name = 'State_Log')
	create table State_Log
	(
		Vehicle_ID int not null, 
		Log_Time int not null, 
		[Type] tinyint not null, 
		Value int not null

		constraint PK_State_Log primary key clustered (Vehicle_ID, [Type], Log_Time)
	) ON [TSS_DATA2]
go

set nocount on
insert into CONSTANTS(NAME, VALUE)
select Name, Value
from 
(
	select 'StopPeriod', '60'
) t(Name, Value)
where not exists (select * from CONSTANTS where NAME = t.Name)
set nocount off