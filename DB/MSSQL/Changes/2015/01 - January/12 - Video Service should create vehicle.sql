update video
	set Controller_Type_ID = lbsMarker.Controller_Type_ID
	  , Vehicle_Kind_ID = lbsMarker.Vehicle_Kind_ID
	  from billing_service_type video
	  join billing_service_type lbsMarker on lbsMarker.Service_Type = 'FRNIKA.MLP'
	  where video.Service_Type_Category = 'Video'
	    and (video.Controller_Type_ID is null
	      or video.Vehicle_Kind_ID is null)