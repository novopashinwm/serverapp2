if not exists (
	select *
		from sys.tables
		where name = 'MessageContactType'
)
begin

	create table MessageContactType
	(
		Id int not null 
			constraint PK_MessageContactType 
				primary key clustered,
		Name varchar(255)
	)

end
go

insert into MessageContactType (Id, Name)
	select *
		from (
			select Id = 1, Name = 'Destination' 
			union all select 2, 'Source' 
			union all select 3, 'Copy'
			union all select 4, 'HiddenCopy'
			union all select 5, 'ReplyTo'
			union all select 6, 'Reference'
		) t
		where not exists (
			select *
				from MessageContactType e
				where e.Id = t.Id)
				
go

if not exists (
	select *
		from sys.objects 
		where name = 'FK_Message_Contact_Type')
begin

	alter table Message_Contact
		add constraint FK_Message_Contact_Type
			foreign key (Type) references MessageContactType(Id)

end