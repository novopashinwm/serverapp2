if not exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_Owner_Operator_ID_Template_ID_Time')
begin

	--�� ������ ���� ���������� 7 �����
	create nonclustered index IX_Message_Owner_Operator_ID_Template_ID_Time
		on Message(Owner_Operator_ID, Template_ID, Time)

end