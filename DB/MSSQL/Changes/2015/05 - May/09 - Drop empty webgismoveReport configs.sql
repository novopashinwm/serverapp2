if object_id('tempdb..#configs') is not null 
	drop table #configs
go

;with configs as 
(
	select q.SCHEDULERQUEUE_ID, cast(q.CONFIG_XML as xml).value
('
	declare namespace a2 = "http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher/FORIS.TSS.Commands";
	(//a2:TagListBoxItem/mobjTagElement)[1]
', 'int') value
	from SCHEDULERQUEUE q
		join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID = e.SCHEDULEREVENT_ID
		join REPORT r on r.REPORT_ID = e.Report_Id
	where r.REPORT_GUID = 'F824B9A6-F435-4940-9788-1C46495BB73F' and cast(q.CONFIG_XML as xml).exist
	('
		declare namespace a2 = "http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher/FORIS.TSS.Commands";
		//a2:TagListBoxItem/mobjTagElement
	') = 1
)
select *
	into #configs
from configs
where value = 0

delete q 
from EMAIL_SCHEDULERQUEUE q
	join #configs c on q.SCHEDULERQUEUE_ID = c.SCHEDULERQUEUE_ID
delete q 
from OperatorContact_SchedulerQueue q
	join #configs c on q.SCHEDULERQUEUE_ID = c.SCHEDULERQUEUE_ID
delete q 
from SCHEDULERQUEUE q
	join #configs c on q.SCHEDULERQUEUE_ID = c.SCHEDULERQUEUE_ID

drop table #configs