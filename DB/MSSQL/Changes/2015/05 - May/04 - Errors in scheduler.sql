update q set q.CONFIG_XML = 
replace(
	replace(
		cast(q.CONFIG_XML as nvarchar(max)), 
		'http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests/FORIS.TSS.TransportDispatcher.ReportsLBS', 
		'http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportsLBS'),
	'http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.ReportsLBS.OperatorRequests/FORIS.TSS.TransportDispatcher.ReportsLBS',
	'http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportsLBS')
from SCHEDULERQUEUE q
	join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID = e.SCHEDULEREVENT_ID
	join REPORT r on r.REPORT_ID = e.Report_Id
where r.REPORT_GUID in ('D2299B07-03BA-4C17-99BB-7C51F0CDEC3B','B939736A-7B95-4D0B-B59C-6A126D310E70','873109F4-D52A-40C5-B095-6CE759A17469')
go