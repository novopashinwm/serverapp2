if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'VALUE' and object_id = object_id('VEHICLE_RULE') and t.name = 'decimal'
)
	alter table VEHICLE_RULE alter column VALUE decimal(18,4) not null
go
if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'VALUE' and object_id = object_id('H_VEHICLE_RULE') and t.name = 'decimal'
)
	alter table H_VEHICLE_RULE alter column VALUE decimal(18,4) not null
go
if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardKilometer' and object_id = object_id('VEHICLE') and t.name = 'decimal'
)
	alter table VEHICLE alter column FuelSpendStandardKilometer decimal(18,4)
go
if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardKilometer' and object_id = object_id('H_VEHICLE') and t.name = 'decimal'
)
	alter table H_VEHICLE alter column FuelSpendStandardKilometer decimal(18,4)
go

if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardLiter' and object_id = object_id('VEHICLE') and t.name = 'decimal'
)
	alter table VEHICLE alter column FuelSpendStandardLiter decimal(18,4)
go
if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardLiter' and object_id = object_id('H_VEHICLE') and t.name = 'decimal'
)
	alter table H_VEHICLE alter column FuelSpendStandardLiter decimal(18,4)
go

if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardGas' and object_id = object_id('VEHICLE') and t.name = 'decimal'
)
	alter table VEHICLE alter column FuelSpendStandardGas decimal(18,4)
go
if not exists 
(
	select * from sys.columns c join sys.systypes t on c.user_type_id = t.xusertype 
	where c.name = 'FuelSpendStandardGas' and object_id = object_id('H_VEHICLE') and t.name = 'decimal'
)
	alter table H_VEHICLE alter column FuelSpendStandardGas decimal(18,4)
go