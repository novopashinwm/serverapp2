if not exists (select * from sys.columns where object_id=object_id('Operator_PhoneBook') and name='IsNotified')
	alter table Operator_PhoneBook add IsNotified bit null 
go
if exists (select * from sys.columns where object_id=object_id('Operator_PhoneBook') and name='IsNotified' and is_nullable=1)
	update Operator_PhoneBook set IsNotified = 1 where IsNotified is null
go
if exists (select * from sys.columns where object_id=object_id('Operator_PhoneBook') and name='IsNotified' and is_nullable=1)
	alter table Operator_PhoneBook alter column IsNotified bit not null
go
if not exists (select * from sys.default_constraints where name = 'DF_IsNotified')
	alter table Operator_PhoneBook add constraint DF_IsNotified default(0) for IsNotified
go