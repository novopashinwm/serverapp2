if exists (select * from sys.check_constraints where name='CK_Fuel')
	alter table Vehicle drop constraint CK_Fuel
go
alter table Vehicle alter column Fuel_Tank decimal(18,4) null
go
alter table H_Vehicle alter column Fuel_Tank decimal(18,4) null
go

if not exists (select * from sys.tables where name='Unit')
	create table Unit
	(
		Id smallint not null,
		Name varchar(50) not null
		constraint PK_Units_Id primary key (Id)
	)
go

insert into Unit(Id, Name)
select *
from 
( 
	select 1, 'KmPerLitre'
	union all
	select 2, 'LitrePer100Km'
	union all
	select 3, 'KmPerM3'
	union all
	select 11, 'Litre'
	union all 
	select 12, 'Cbm' 
) t(Id, Name)
where not exists (select * from Unit where Id = t.Id)
go

if not exists (select * from sys.columns where name='Unit_Id' and object_id=object_id('Fuel_Type'))
	alter table Fuel_Type add Unit_Id smallint not null 
		constraint DF_Fuel_Type_Unit_Id default(1)
		constraint FK_Fuel_Type_Unit_Id foreign key references Unit(Id)
go

if not exists (select * from sys.columns where name='Tank_Unit_Id' and object_id=object_id('Fuel_Type'))
	alter table Fuel_Type add Tank_Unit_Id smallint not null 
		constraint DF_Fuel_Type_Tank_Unit_Id default(11)
		constraint FK_Fuel_Type_Tank_Unit_Id foreign key references Unit(Id)
go

if exists (select * from sys.default_constraints where name='DF_Fuel_Type_Tank_Unit_Id')
	alter table Fuel_Type drop constraint DF_Fuel_Type_Tank_Unit_Id
go

if not exists (select * from sys.default_constraints where name='DF_Fuel_Type_Tank_Unit_Id')
	alter table Fuel_Type add constraint DF_Fuel_Type_Tank_Unit_Id default(11) for Tank_Unit_Id
go
update Fuel_Type set Tank_Unit_Id = 11 where Tank_Unit_Id = 1
go


update t set 
	t.Unit_Id = (select id from Unit where Name = 'KmPerLitre')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Litre')
from Fuel_Type t
where t.Country_ID = 1

update t set 
	t.Unit_Id = (select id from Unit where Name = 'LitrePer100Km')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Litre')
from Fuel_Type t
where t.Country_ID = 2

update t set 
	t.Unit_Id = (select id from Unit where Name = 'KmPerM3')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Cbm')
from Fuel_Type t
where t.Name in ('Пропан','Метан')

go

if exists (select * from sys.columns where name='FuelSpendStandardGas' and object_id=object_id('Vehicle'))
	EXECUTE('update v set v.FuelSpendStandardLiter = 1000, v.FuelSpendStandardKilometer = v.FuelSpendStandardGas
	from VEHICLE v
		join Fuel_Type t on t.ID = v.Fuel_Type_ID
		join Unit u on u.Id = t.Unit_Id
	where u.Name = ''KmPerM3''')
go
if exists (select * from sys.columns where name='FUEL_TANK_GAS' and object_id=object_id('Vehicle'))
	EXECUTE('update v set v.FUEL_TANK = v.FUEL_TANK_GAS * 1000
	from VEHICLE v
		join Fuel_Type t on t.ID = v.Fuel_Type_ID
		join Unit u on u.Id = t.Tank_Unit_Id
	where u.Name = ''Cbm''')
go

if exists (select * from sys.columns where name='FuelSpendStandardGas' and object_id=object_id('Vehicle'))
	alter table Vehicle drop column FuelSpendStandardGas
go
if exists (select * from sys.columns where name='FUEL_TANK_GAS' and object_id=object_id('Vehicle'))
	alter table Vehicle drop column FUEL_TANK_GAS
go
if exists (select * from sys.columns where name='FuelSpendStandardGas' and object_id=object_id('H_Vehicle'))
	alter table H_Vehicle drop column FuelSpendStandardGas
go
if exists (select * from sys.columns where name='FUEL_TANK_GAS' and object_id=object_id('H_Vehicle'))
	alter table H_Vehicle drop column FUEL_TANK_GAS
go

select *
from Fuel_Type