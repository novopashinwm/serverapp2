declare @message_template_id int = (select MESSAGE_TEMPLATE_ID from MESSAGE_TEMPLATE where NAME = 'LossRightsToObject')
if @message_template_id is null
begin
	insert into MESSAGE_TEMPLATE (NAME, HEADER, BODY)
	values ('LossRightsToObject', '�������� �� "$scheduler_name$" ��������������', '�� �������� �����($rights_names$) �� $object_type$ "$object_name$", ����� �� ����� ���� ��������') 
	
	set @message_template_id = @@identity
end

insert into MESSAGE_TEMPLATE_FIELD (NAME, MESSAGE_TEMPLATE_ID, DOTNET_TYPE_ID)
select *
from
(
	select  'scheduler_name', @message_template_id, (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all
	select 'rights_names', @message_template_id, (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all 
	select 'object_type', @message_template_id, (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all 
	select 'object_name', @message_template_id, (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
) t(NAME, MESSAGE_TEMPLATE_ID, DOTNET_TYPE_ID)
where not exists (select * from MESSAGE_TEMPLATE_FIELD where NAME = t.NAME and MESSAGE_TEMPLATE_ID = t.MESSAGE_TEMPLATE_ID)