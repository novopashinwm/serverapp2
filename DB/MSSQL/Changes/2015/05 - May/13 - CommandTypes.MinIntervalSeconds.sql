if not exists (
	select *
		from sys.columns
		where object_id = object_id('CommandTypes')
		  and name = 'MinIntervalSeconds'
)
begin

	alter table CommandTypes
		add MinIntervalSeconds int
		
end

go

update CommandTypes set MinIntervalSeconds = 60 where id = 20 and MinIntervalSeconds is null