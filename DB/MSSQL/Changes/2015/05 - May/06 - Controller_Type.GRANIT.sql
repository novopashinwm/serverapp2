DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'GRANIT_PERSONAL')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer, ImagePath)
      VALUES
            ('GRANIT_PERSONAL', 1000, 1, 1, '/about/includes/sources/images/devices/granit_personal.jpg')
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 1, 'Alarm', 0
union all select 1, 'Sos', 1
union all select 1, 'FromBattery', 2
union all select 1, 'BatteryVoltage', 3

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)	    
	    
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '�������'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Alarm'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
	    
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '���������� �������'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'BatteryVoltage'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
go

DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'GRANIT_AUTO')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer, ImagePath)
      VALUES
            ('GRANIT_AUTO', 1000, 1, 1, '/about/includes/sources/images/devices/granit_auto.jpg')
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 1, 'Alarm', 0
union all select 1, 'Sos', 1
union all select 1, 'FromBattery', 2
union all select 1, 'BatteryVoltage', 3
union all select 0, 'AnalogueInput0', 11
union all select 0, 'AnalogueInput1', 12
union all select 0, 'AnalogueInput2', 13
union all select 0, 'AnalogueInput3', 14
union all select 1, 'DigitalInput', 20
union all select 1, 'DigitalInput0', 21
union all select 1, 'DigitalInput1', 22
union all select 1, 'DigitalInput2', 23
union all select 1, 'DigitalInput3', 24
union all select 1, 'DigitalOutput', 25
union all select 1, 'Odometer', 31
union all select 1, 'SignalStrength', 41
union all select 1, 'GprsSignal', 42
union all select 1, 'AccelerometerEnergy', 43
union all select 1, 'AccelerometerAccel', 44
union all select 1, 'FuelStatus0', 51
union all select 1, 'FuelHeight0', 52
union all select 1, 'FuelLevel0', 53
union all select 1, 'FuelTemperature0', 54
union all select 1, 'FuelStatus1', 61
union all select 1, 'FuelHeight1', 62
union all select 1, 'FuelLevel1', 63
union all select 1, 'FuelTemperature1', 64

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)	    
	    
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '�������'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Alarm'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
	    
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '���������� �������'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'BatteryVoltage'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)