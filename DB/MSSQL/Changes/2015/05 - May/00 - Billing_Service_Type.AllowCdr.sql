if not exists (
	select *
		from sys.columns
		where object_id = object_id('Billing_Service_Type')
		  and name = 'AllowCdr'
)
begin

	alter table Billing_Service_Type
		add AllowCdr bit not null
			constraint DF_Billing_Service_Type_AllowCdr default (0)

end
go

update Billing_Service_Type
	set AllowCdr = 1
	where Service_Type = 'FRNIKA.GPS.Phone.2014'
	  and AllowCdr = 0