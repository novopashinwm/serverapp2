if exists (
	select *
		from sys.columns
		where object_id = object_id('OperatorContact_SchedulerQueue')
		  and name = 'Operator_ID'
)
begin

	alter table OperatorContact_SchedulerQueue
		drop constraint PK_OperatorContact_SchedulerQueue

	alter table OperatorContact_SchedulerQueue
		drop constraint FK_OperatorContact_SchedulerQueue_OperatorContact

	alter table OperatorContact_SchedulerQueue
		drop column Operator_ID

	alter table OperatorContact_SchedulerQueue
		add constraint PK_OperatorContact_SchedulerQueue
			primary key clustered (SchedulerQueue_ID, Contact_ID)

	drop index IX_OperatorContact_SchedulerQueue_SchedulerQueue_ID on OperatorContact_SchedulerQueue

	create nonclustered index IX_OperatorContact_SchedulerQueue_Contact_ID on OperatorContact_SchedulerQueue(Contact_ID)

	alter table OperatorContact_SchedulerQueue
		add constraint FK_OperatorContact_SchedulerQueue_Contact_ID 
			foreign key (Contact_ID) references Contact(ID)

	alter table H_OperatorContact_SchedulerQueue
		drop column Operator_ID

end