if not exists (
	select *
		from sys.indexes 
		where name = 'IX_SchedulerQueue_Enabled_ErrorCount_Nearest_Time'
)
begin

	create nonclustered index IX_SchedulerQueue_Enabled_ErrorCount_Nearest_Time
		on SchedulerQueue(Enabled, ErrorCount, Nearest_Time) include (LastSchedulerStartDate)

end