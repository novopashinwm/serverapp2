if not exists (select * from sys.types where name='Msisdn')
	create type Msisdn as table 
	(
		value varchar(80) not null
	)
go