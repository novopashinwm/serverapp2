if not exists (select * from sys.columns where name='ECI' and object_id=object_id('Cell_Network_Log'))
	alter table Cell_Network_Log add ECI int null
go
if not exists (select * from sys.columns where name='SAC' and object_id=object_id('Cell_Network_Log'))
	alter table Cell_Network_Log add SAC int null
go