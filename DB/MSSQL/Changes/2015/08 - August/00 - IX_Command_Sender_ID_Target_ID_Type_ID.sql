if not exists (
	select * from sys.indexes where name = 'IX_Command_Sender_ID_Target_ID_Type_ID'
)
begin

	create nonclustered index IX_Command_Sender_ID_Target_ID_Type_ID
		on Command(Sender_ID, Target_ID, Type_ID)
		include (Date_Received, Result_Type_ID)

end