update bst 
	set Shared = 0
	from Billing_Service_Type bst
	where LimitedQuantity = 0 
	  and MinIntervalSeconds is not null
	  and Shared = 1