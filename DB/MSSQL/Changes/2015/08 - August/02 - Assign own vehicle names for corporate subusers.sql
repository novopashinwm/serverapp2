update v
	set garage_number = 
		case
			when len(o.NAME) > 0 and len(o.login) > 0 then o.NAME + ' [' + o.LOGIN + ']'
			when len(o.NAME) > 0 and len(o.login) = 0 then o.NAME
			when len(o.NAME) = 0 and len(o.login) > 0 then '[' + o.LOGIN + ']'
		end

	from vehicle v
	join controller c on c.VEHICLE_ID = v.VEHICLE_ID
	join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
	join asid a on a.id = mc.Asid_ID
	join operator o on o.OPERATOR_ID = a.Operator_ID	
	where len(v.garage_number) = 0
	  and (len(o.login) > 0 or len(o.name) > 0)
	  and not exists (
		select 1 
			from Operator_Contact oc 
			join contact c on c.id = oc.Contact_ID
			where oc.Operator_ID = o.OPERATOR_ID 
			  and oc.Confirmed = 1)