if exists (select * from sys.indexes where name = 'PK_Command_Parameter')
begin
	alter table Command_Parameter
		drop constraint PK_Command_Parameter
	alter table Command_Parameter
		add constraint PK_Command_Parameter_Nonclustered 
			primary key nonclustered (ID)
end
go

if exists (select * from sys.indexes where name = 'PK_Command_Parameter')
begin
	alter table Command_Parameter
		drop constraint PK_Command_Parameter
end
go

if exists (
	select * from sys.columns where object_id = object_id('Command_Parameter') and name = 'Command_ID' and is_nullable = 1
)
begin
	delete Command_Parameter where Command_ID is null or [Key] is null
	alter table Command_Parameter
		alter column Command_ID int not null
	alter table Command_Parameter 
		alter column [Key] nvarchar(30) not null
end
go

if not exists (
	select * from sys.indexes where name = 'IX_Command_Parameter_Command_ID_Key'
)
begin
	create clustered index IX_Command_Parameter_Command_ID_Key
		on Command_Parameter(Command_ID, [Key])
end
go

