set nocount on

declare @sensor table(digital bit, name varchar(255), number int)

insert into @sensor
		  select 1, 'Digital output 1 state', 179
union all select 1, 'Digital output 2 state', 180

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.TYPE_NAME in ('Teltonika FM1100')
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)