if not exists (
	select * from sys.indexes where name = 'IX_Asid_Contact_ID'
)
begin

	create nonclustered index IX_Asid_Contact_ID
		on Asid(Contact_ID)

end