declare @russia_id int = (select Country_ID from Country where Name = 'Russia')

while 1=1
begin

	insert into Fuel_Type (ID, Name, Country_ID, Unit_Id, Tank_Unit_Id)
		select top(1)
			 (select max(ID) from Fuel_Type ft_id)+1
			, ft.Name
			, @russia_id
			, ft.Unit_Id
			, ft.Tank_Unit_Id
			from Fuel_Type ft
			where ft.Country_ID is null
			  and not exists (select * from Fuel_Type e where e.Country_ID = @russia_id and e.Name = ft.Name)
			order by ft.ID asc

	if @@ROWCOUNT = 0
		break
end
