if not exists (
	select *
		from sys.columns
		where object_id = object_id('H_Controller_Type')
		  and name = 'DefaultPassword'
)
begin

	alter table H_Controller_Type
		add DefaultPassword varchar(32),
		PasswordAlphabet varchar(255),
		PasswordLength int,
		SmsTemplateForPasswordChange varchar(255)

end
