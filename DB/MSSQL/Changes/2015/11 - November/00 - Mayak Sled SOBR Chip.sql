declare @type_name nvarchar(255) = 'Mayak Sled SOBR Chip'
declare @deviceIdIsImei bit = 0

DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = @type_name)
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            (@type_name, 1000, @deviceIdIsImei, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END
else
begin
	update CONTROLLER_TYPE set DeviceIdIsImei = @deviceIdIsImei where Controller_Type_Id = @CONTROLLER_TYPE_ID and DeviceIdIsImei <> @deviceIdIsImei
end

set nocount on
declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 0, 'EventCode',		1
union all select 0, 'ChipTemperature',	2
union all select 0, 'SentSmsCount',		3
union all select 0, 'SentGprsCount',	4
union all select 1, 'VoltageIsOk',		5
union all select 0, 'VoltageMV',		6
union all select 1, 'Search',			7
union all select 0, 'SimBalance',		8

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
