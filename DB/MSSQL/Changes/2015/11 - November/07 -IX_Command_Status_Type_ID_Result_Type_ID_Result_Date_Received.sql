if not exists (
	select * from sys.indexes where name = 'IX_Command_Status_Type_ID_Result_Type_ID_Result_Date_Received'
)
begin

	create nonclustered index IX_Command_Status_Type_ID_Result_Type_ID_Result_Date_Received
		on Command(Status, Type_ID, Result_Type_ID, Result_Date_Received)

end