delete vp
	from VEHICLE_PROFILE vp
	where vp.PROPERTY_NAME = 'icon'
	  and len(vp.Value) > 0
	  and substring(vp.value, len(vp.value)-4+1, 4) <> '.png'