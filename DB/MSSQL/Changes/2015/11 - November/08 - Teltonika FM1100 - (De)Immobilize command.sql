insert into Controller_Type_CommandTypes (Controller_Type_ID, CommandTypes_ID)
	select ct.CONTROLLER_TYPE_ID, cmd.id
		from CONTROLLER_TYPE ct
		join CommandTypes cmd on cmd.code in ('Immobilize', 'Deimmobilize')
		where ct.TYPE_NAME = 'Teltonika FM1100'
		  and not exists (select 1 from Controller_Type_CommandTypes e where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID and e.CommandTypes_ID = cmd.id)

update cmd set right_id = r.right_id
	from CommandTypes cmd
	join [Right] r on r.NAME = 'Immobilization'
	where cmd.code in ('Immobilize', 'Deimmobilize')
	  and cmd.Right_ID is null

update ct set SupportsPassword = 1
	from CONTROLLER_TYPE ct
	where ct.TYPE_NAME = 'Teltonika FM1100'
	  and SupportsPassword = 0