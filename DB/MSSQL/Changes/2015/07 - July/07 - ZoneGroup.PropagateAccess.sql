if not exists (
	select *
		from sys.columns
		where object_id = object_id('ZoneGroup')
		  and name = 'PropagateAccess'
)
begin

	alter table ZoneGroup
		add PropagateAccess bit not null
			constraint DF_ZoneGroup_PropagateAccess
				default(0)

	alter table H_ZoneGroup
		add PropagateAccess bit not null
			constraint DF_H_ZoneGroup_PropagateAccess
				default(0)

end