if exists (select * from sys.columns where object_id=object_id('RequestCounterLog') and name='Id')
	drop table RequestCounterLog
go
if exists (select * from sys.columns where object_id=object_id('RequestCounter') and name='StartDate')
	drop table RequestCounter
go
if not exists (select * from sys.tables where name='RequestCounter')
begin
	create table RequestCounter
	(
		FreqLimitId int not null,
		IpId int not null,
		Value int not null constraint DF_RequestCounter_Value default(0),
		LogTime int not null constraint DF_RequestCounter_StartDate default(datediff(ss, '1970', getutcdate())),
		 
		constraint PK_RequestCounter primary key(FreqLimitId, IpId),
		constraint FK_RequestCounter_FreqLimitId foreign key (FreqLimitId) references RequestFreqLimit(Id),
		constraint FK_RequestCounter_Ip foreign key (IpId) references Ip(Id)
	)
	execute sp_addextendedproperty 'MS_Description', '���������� �������� ��������� �� �������� ��������� � RequestFreqLimit', 'user', 'dbo', 'table', 'RequestCounter', 'column', 'Value'
end
go
if not exists (select * from sys.tables where name='RequestCounterLog')
begin
	create table RequestCounterLog 
	(
		FreqLimitId int not null,
		IpId int not null,
		LogTime int not null constraint DF_RequestCounterLog_ResetTime default datediff(ss, '1970', getutcdate()),
		RequestCount int not null,

		constraint PK_RequestCounterLog primary key(FreqLimitId, IpId, LogTime),
		constraint FK_RequestCounterLog_RequestCounterId foreign key (FreqLimitId, IpId) references RequestCounter(FreqLimitId, IpId)
	) 
	execute sp_addextendedproperty 'MS_Description', '���������� �������� ��������� �� �������� �� ������ ��������', 'user', 'dbo', 'table', 'RequestCounterLog', 'column', 'RequestCount'
	execute sp_addextendedproperty 'MS_Description', '���� ������ ��������', 'user', 'dbo', 'table', 'RequestCounterLog', 'column', 'LogTime'
end
go
