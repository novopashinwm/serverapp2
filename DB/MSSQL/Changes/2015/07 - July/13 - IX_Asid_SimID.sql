if not exists (select * from sys.indexes where object_id = object_id('Asid') and name = 'IX_Asid_SimID')
begin

	create nonclustered index IX_Asid_SimID on Asid(SimID)

end