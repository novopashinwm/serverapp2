if not exists (select * from sys.columns where object_id=object_id('Billing_Service_Type') and name='AllowLoginOperator')
	alter table Billing_Service_Type add AllowLoginOperator bit not null constraint DF_Billing_Service_Type_AllowCreateOperator default(1)
go
exec sp_executesql N'update Billing_Service_Type set AllowLoginOperator = 0 where Service_Type = ''FRNIKA.GPS.Modem'''
go
