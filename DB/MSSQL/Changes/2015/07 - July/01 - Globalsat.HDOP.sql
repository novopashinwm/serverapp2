declare @hdopId int = (select l.CONTROLLER_SENSOR_LEGEND_ID from CONTROLLER_SENSOR_LEGEND l where l.NAME = 'HDOP')
insert into Controller_Sensor(Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript, Default_Sensor_Legend_ID, Mandatory, Default_Multiplier, Default_Constant)
select t.Controller_Type_ID, 1, 43, 'HDOP', @hdopId, 0, 0.1, 0
from CONTROLLER_TYPE t
where not exists 
(
	select * from Controller_Sensor s
	where s.CONTROLLER_TYPE_ID = t.CONTROLLER_TYPE_ID and s.NUMBER = 43
) and t.TYPE_NAME in ('GlobalSat TR-203', 'GlobalSat TR-600', 'GlobalSat GTR-128')
