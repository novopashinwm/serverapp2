/*��������� ��� ����������� Nika.Tracker, ����������� ������� */
begin tran

declare @controller_type_id int;
declare @controller_vehicle_kind int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'Tetrika')
set @controller_vehicle_kind = (select Vehicle_Kind_ID from VEHICLE_KIND where NAME = 'SmartPhone');
if (@controller_type_id is null)
begin
	print 'Inserting new controller type: '
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT, Default_Vehicle_Kind_ID)
		values ('Tetrika', 200, 0, 0, @controller_vehicle_kind)
	set @controller_type_id = @@identity;
end;

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	Default_Legend				nvarchar(100),
	Default_Multiplier			numeric(3,1),
	Descript					nvarchar(512),
	ValueExpired				int);

set nocount on
insert into @controller_sensor values (@controller_type_id, 1, 40, '���������� ������ 1', 1, 'AnalogInput1', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 41, '���������� ������ 2', 1, 'AnalogInput2', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 42, '���������� ������ 3', 1, 'AnalogInput3', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 43, '���������� ������ 4', 1, 'AnalogInput4', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 44, '���������� ������ 5', 1, 'AnalogInput5', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 45, '���������� ������ 6', 1, 'AnalogInput6', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 46, '���������� ������ 7', 1, 'AnalogInput7', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 47, '���������� ������ 8', 1, 'AnalogInput8', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 48, '���������� ������ 9', 1, 'AnalogInput9', 84600);
insert into @controller_sensor values (@controller_type_id, 1, 49, '���������� ������ 10', 1, 'AnalogInput10', 84600);

set nocount off

print 'Updating existing sensors: '
update cs
set CONTROLLER_SENSOR_TYPE_ID = t.CONTROLLER_SENSOR_TYPE_ID,
	Descript = t.Descript,
	Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID,
	Default_Multiplier = t.Default_Multiplier,
	Mandatory = case when legend.Controller_Sensor_Legend_ID is not null then 1 else 0 end,
	VALUE_EXPIRED = t.ValueExpired
from dbo.Controller_Sensor cs
	join @controller_sensor t on t.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID and t.NUMBER = cs.NUMBER
	left outer join controller_Sensor_legend legend on legend.name = t.Default_Legend

print 'Remove not existing sensors'
delete m
from CONTROLLER_SENSOR_MAP m
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_SENSOR_ID = m.CONTROLLER_SENSOR_ID
	left join @controller_sensor t on t.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID and t.NUMBER = cs.NUMBER
where cs.CONTROLLER_TYPE_ID = @controller_type_id and t.NUMBER is null
delete cs
from CONTROLLER_SENSOR cs
	left join @controller_sensor t on t.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID and t.NUMBER = cs.NUMBER
where cs.CONTROLLER_TYPE_ID = @controller_type_id and t.NUMBER is null

print 'Inserting new sensors: '
insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Mandatory,
	Descript,
	VALUE_EXPIRED)
select 
	t.CONTROLLER_TYPE_ID,
	t.CONTROLLER_SENSOR_TYPE_ID,
	t.Number,
	legend.Controller_Sensor_Legend_ID,
	t.Default_Multiplier,
	Mandatory = case when legend.Controller_Sensor_Legend_ID is not null then 1 else 0 end,
	t.Descript,
	t.ValueExpired
from @controller_sensor t
left outer join controller_Sensor_legend legend on legend.name = t.Default_Legend
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

commit

select * from CONTROLLER_SENSOR where CONTROLLER_TYPE_ID = @controller_type_id