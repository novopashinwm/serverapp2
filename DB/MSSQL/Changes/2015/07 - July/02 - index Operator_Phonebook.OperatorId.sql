if not exists (select * from sys.indexes where name='IX_Operator_PhoneBook')
	create unique nonclustered index IX_Operator_PhoneBook on Operator_PhoneBook(OperatorId)
go
