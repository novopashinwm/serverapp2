if not exists (
	select *
		from sys.indexes
		where name = 'IX_Contact_Value'
)
begin

	create nonclustered index IX_Contact_Value on Contact(Value) include (Type)

end