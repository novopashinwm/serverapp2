if not exists (select * from sys.objects where name = 'Department_Rule')
begin

	create table Department_Rule
	(
		Department_Rule_ID int identity constraint PK_Department_Rule primary key clustered,
		Department_ID int not null constraint FK_Department_Rule_Department_ID foreign key references Department(Department_ID),
		Rule_ID int not null constraint FK_Department_Rule_Rule_ID foreign key references [Rule](Rule_ID),
		Value int not null
	)

end