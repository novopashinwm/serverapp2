--declare @order285server int = (select RemoteTerminalServer_ID from RemoteTerminalServer where Name = 'Служба терминалов')
--declare @vehicleId int = 821
--if @order285server is null
--begin
--	insert into RemoteTerminalServer(Name, Url)
--	values ('Служба терминалов','http://127.0.0.1/Terminal/Order285')

--	set @order285server = @@identity
--end
--select * from RemoteTerminalServer where RemoteTerminalServer_ID = @order285server

--if not exists (select * from Vehicle_RemoteTerminalServer where Vehicle_ID = @vehicleId and RemoteTerminalServer_ID = @order285server)
--	insert into Vehicle_RemoteTerminalServer(RemoteTerminalServer_ID, Vehicle_ID)
--	select @order285server, @vehicleId
--select * from RemoteTerminalServer s join Vehicle_RemoteTerminalServer v on s.RemoteTerminalServer_ID = v.RemoteTerminalServer_ID where v.Vehicle_ID = @vehicleId

insert into CONTROLLER_TYPE(TYPE_NAME, AllowedToAddByCustomer, DeviceIdIsImei)
select 'Order285', 0, 1
where not exists (select * from CONTROLLER_TYPE where TYPE_NAME = 'Order285')
