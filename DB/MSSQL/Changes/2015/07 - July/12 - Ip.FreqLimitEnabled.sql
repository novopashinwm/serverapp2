if exists (select * from sys.default_constraints where name='DF_IP_IsLoopback')
	alter table Ip drop constraint DF_IP_IsLoopback
go
if exists (select * from sys.columns where object_id=object_id('Ip') and name='IsLoopback')
	alter table Ip drop column IsLoopback
go
if not exists (select * from sys.columns where object_id=object_id('Ip') and name='FreqLimitEnabled')
	alter table Ip add FreqLimitEnabled bit not null constraint DF_IP_FreqLimitEnabled default(1)
go
if not exists (select * from sys.indexes where name='IX_Ip_FreqLimitEnabled')
	create nonclustered index IX_Ip_FreqLimitEnabled on Ip(FreqLimitEnabled) include(Value) where FreqLimitEnabled = 0
go
