select o.OPERATOR_ID, t1.LOGIN, t2.LastActivity, t2.SESSION_ID
	into #sessions
from (
		select LOGIN, count(OPERATOR_ID) Count
		from OPERATOR
		where LOGIN is not null and LOGIN <> ''
		group by LOGIN
		having count(OPERATOR_ID) > 1	
	) t1
	join OPERATOR o on o.LOGIN = t1.LOGIN
	outer apply 
	(
		select top 1 s.SESSION_ID, s.LastActivity
		from SESSION s
		where s.OPERATOR_ID = o.OPERATOR_ID
		order by s.LastActivity desc
	) t2

create nonclustered index IX on #sessions(LOGIN, OPERATOR_ID, LastActivity)

;with cte as
(
	select s.LOGIN, s.OPERATOR_ID, s.LastActivity
	from(
			select LOGIN, max(LastActivity) LastActivity, max(OPERATOR_ID) OPERATOR_ID
			from #sessions
			group by LOGIN	
		) t1
		left join #sessions s on  t1.LOGIN = s.LOGIN
	where (t1.LastActivity is not null and t1.LastActivity = s.LastActivity) 
		or (t1.LastActivity is null and t1.OPERATOR_ID = s.OPERATOR_ID) 
)

update OPERATOR 
	set LOGIN = null
where OPERATOR_ID in
(
	select OPERATOR_ID
	from #sessions s
	except
	select OPERATOR_ID
	from cte
)

drop table #sessions

if not exists (select * from sys.indexes where name='IX_OPERATOR_UNIQUELOGIN')
	create unique nonclustered index IX_OPERATOR_UNIQUELOGIN on OPERATOR(LOGIN) where LOGIN is not null and LOGIN <> ''
go