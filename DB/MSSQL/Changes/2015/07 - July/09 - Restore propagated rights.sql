insert into operator_vehicle (OPERATOR_ID, vehicle_id, ALLOWED, RIGHT_ID)
	select distinct t.OPERATOR_ID, t.vehicle_id, 1, t.RIGHT_ID
		from (
			select ovg.OPERATOR_ID, vgv.vehicle_id, ovg.RIGHT_ID
				from operator_vehiclegroup ovg
				join vehiclegroup_vehicle vgv on vgv.vehiclegroup_Id = ovg.VEHICLEGROUP_ID
				where ovg.ALLOWED = 1
			union all
			select o.OPERATOR_ID, vgv.vehicle_id, ogvg.RIGHT_ID
				FROM OPERATOR O
				join OPERATORGROUP_OPERATOR ogo on ogo.OPERATOR_ID = o.OPERATOR_ID
				join operatorgroup og on og.OPERATORGROUP_ID = ogo.OPERATORGROUP_ID
				join OPERATORGROUP_VEHICLEGROUP ogvg on ogvg.OPERATORGROUP_ID = ogo.OPERATORGROUP_ID
				join VEHICLEGROUP vg on vg.VEHICLEGROUP_ID = ogvg.VEHICLEGROUP_ID
				join vehiclegroup_vehicle vgv on vgv.VEHICLEGROUP_ID = vg.VEHICLEGROUP_ID
				where ogvg.ALLOWED = 1
		) t
		where not exists (
			select 1
				from operator_vehicle e
				where e.OPERATOR_ID = t.OPERATOR_ID
					and e.RIGHT_ID = t.RIGHT_ID
					and e.VEHICLE_ID = t.VEHICLE_ID
			)
			and not exists (
				select 1
					from v_operator_vehicle_right e
					where   e.OPERATOR_ID = t.OPERATOR_ID
						and e.RIGHT_ID = t.RIGHT_ID
						and e.VEHICLE_ID = t.VEHICLE_ID
				)

insert into OPERATOR_ZONE (OPERATOR_ID, ZONE_ID, RIGHT_ID, ALLOWED)
	select distinct ozg.OPERATOR_ID, zgz.ZONE_ID, ozg.RIGHT_ID, 1
		from OPERATOR_ZONEGROUP ozg
		join zonegroup_zone zgz on zgz.zonegroup_Id = ozg.zonEGROUP_ID
		where ozg.ALLOWED = 1
		  and not exists (
			select 1
				from OPERATOR_ZONE e
				where e.OPERATOR_ID = ozg.OPERATOR_ID
				  and e.RIGHT_ID = ozg.RIGHT_ID
				  and e.ZONE_ID = zgz.ZONE_ID
			)