insert into RequestFreqLimit(Name, MaxCount, Seconds)
select 'SmsConfirmation', 1, 15 * 60
where not exists 
	(select * from RequestFreqLimit where Name = 'SmsConfirmation')
go
insert into RequestFreqLimit(Name, MaxCount, Seconds)
select 'MsisdnVerification', 5, 15 * 60
where not exists 
	(select * from RequestFreqLimit where Name = 'MsisdnVerification')
go
insert into RequestFreqLimit(Name, MaxCount, Seconds)
select 'TryLogin', 10, 2 * 60
where not exists 
	(select * from RequestFreqLimit where Name = 'TryLogin')
go