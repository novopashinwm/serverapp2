if not exists (
	select *
		from sys.columns
		where object_id = object_id('VehicleGroup')
		  and name = 'PropagateAccess'
)
begin

	alter table VehicleGroup
		add PropagateAccess bit not null
			constraint DF_VehicleGroup_PropagateAccess
				default(0)

	alter table H_VehicleGroup
		add PropagateAccess bit not null
			constraint DF_H_VehicleGroup_PropagateAccess
				default(0)

end