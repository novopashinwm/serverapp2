if not exists (
	select *
		from sys.indexes where name = 'IX_H_AppClient_ID'
)
begin

	create nonclustered index IX_H_AppClient_ID on H_AppClient (ID)

end