if exists (select * from sys.procedures where name='UpdateMessagesStatus')
	drop procedure UpdateMessagesStatus
go
if exists (select * from sys.columns where object_id=object_id('MESSAGE') and name='DeviceStatus')
begin
exec sp_executesql N'
	update [MESSAGE] set ProcessingResult = 24 where DeviceStatus = 1
	update [MESSAGE] set ProcessingResult = 25 where DeviceStatus = 2
'
end
go
if exists (select * from sys.columns where object_id=object_id('MESSAGE') and name='DeviceStatus')
	alter table [MESSAGE] drop column DeviceStatus
go
if exists (select * from sys.columns where object_id=object_id('H_MESSAGE') and name='DeviceStatus')
	alter table H_MESSAGE drop column DeviceStatus
go
