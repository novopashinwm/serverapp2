if not exists (
select * from sys.indexes where name = 'IX_Address_Language_MapId_LatCell_LngCell'
)
begin

	CREATE NONCLUSTERED INDEX [IX_Address_Language_MapId_LatCell_LngCell]
	ON [dbo].[Address] ([Language],[MapId],[LatCell],[LngCell])
	INCLUDE ([Value],[Lat],[Lng])

end
go

if exists (
	select * from sys.indexes where name = 'IX_Address_LatLngCells'
)
begin

	drop INDEX [IX_Address_Language_MapId_LatCell_LngCell] on [dbo].[Address]

end
