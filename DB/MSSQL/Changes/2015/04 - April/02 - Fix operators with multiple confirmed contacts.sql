-- ��������� �������������� ��������� ����������� �������

if object_id('tempdb..#ids') is not null drop table #ids

select oc.operator_id, convert(int, null) Contact_ID
	into #ids
from operator_contact oc
	join contact c on c.id = oc.Contact_ID
where oc.confirmed = 1
	and c.type = 2
group by oc.Operator_ID
having count(1) > 1

update #ids set Contact_ID = 
(
	select c.Demasked_ID
	from Asid a
		join Contact c on a.Contact_ID = c.ID
	where a.Operator_ID = #ids.Operator_ID
)
where #ids.Contact_ID is null

update #ids set Contact_ID = 
(
	select top 1 c.ID
	from Operator_Contact oc 
		join Contact c on oc.Contact_ID = c.ID
	where oc.Operator_ID = #ids.Operator_ID and c.[Type] = 2 and oc.Confirmed = 1
	order by c.Created desc
)
where #ids.Contact_ID is null

update #ids set Contact_ID = 
(
	select top 1 c.ID
	from Operator_Contact oc 
		join Contact c on oc.Contact_ID = c.ID
	where oc.Operator_ID = #ids.Operator_ID and c.[Type] = 2 and oc.Confirmed = 1
	order by c.Created desc
)
where #ids.Contact_ID is null

update oc set oc.Confirmed = 0, oc.Removable = 1
from Operator_Contact oc
	join #ids on #ids.Operator_ID = oc.Operator_ID 
	join Contact c on c.ID = oc.Contact_ID
where #ids.Contact_ID != oc.Contact_ID and c.[Type] = 2 and oc.Confirmed = 1