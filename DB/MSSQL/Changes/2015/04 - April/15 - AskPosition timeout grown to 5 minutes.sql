declare @newExecutionTimeout int = 5*60

update ct 
	set ct.ExecutionTimeout = @newExecutionTimeout
	from CommandTypes ct
	where ct.id = 20 --AskPosition
	  and isnull(ct.ExecutionTimeout, -1) <> @newExecutionTimeout
