if not exists (select * from sys.columns where object_id=object_id('Vehicle') and name='FuelSpendStandardGas')
	alter table Vehicle add FuelSpendStandardGas int null
go
if not exists (select * from sys.columns where object_id=object_id('Vehicle') and name='FUEL_TANK_GAS')
	alter table Vehicle add FUEL_TANK_GAS smallint null
go
if not exists (select * from sys.columns where object_id=object_id('H_Vehicle') and name='FuelSpendStandardGas')
	alter table H_Vehicle add FuelSpendStandardGas int null
go
if not exists (select * from sys.columns where object_id=object_id('H_Vehicle') and name='FUEL_TANK_GAS')
	alter table H_Vehicle add FUEL_TANK_GAS smallint null
go
