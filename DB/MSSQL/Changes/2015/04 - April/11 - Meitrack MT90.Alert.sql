declare @controller_type_id int = 
(
	select t.CONTROLLER_TYPE_ID
	from CONTROLLER_TYPE t
	where TYPE_NAME = 'Meitrack MT90'
)

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
          select 1, 'D9', 19

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Зажигание'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'D9'
	  and (cs.Default_Sensor_Legend_ID is null or cs.Mandatory = 0)