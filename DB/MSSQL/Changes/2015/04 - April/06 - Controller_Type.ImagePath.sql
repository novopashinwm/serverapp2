if not exists (select * from sys.columns where object_id=object_id('CONTROLLER_TYPE') and name='ImagePath')
	alter table CONTROLLER_TYPE add ImagePath varchar(250) null
go
if not exists (select * from sys.columns where object_id=object_id('H_CONTROLLER_TYPE') and name='ImagePath')
	alter table H_CONTROLLER_TYPE add ImagePath varchar(250) null
go
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/TR203.jpg' where TYPE_NAME='GlobalSat TR-203'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/xexsun.jpg' where TYPE_NAME='TK-102'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/meitrack.jpg' where TYPE_NAME='Meitrack MT90'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/xexun-p.jpg' where TYPE_NAME=''
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/bb-mobile.jpg' where TYPE_NAME='Smarts Baby Bear'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/da-690.jpg' where TYPE_NAME='WatchDA690'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/c-devices/mayak-sm.jpg' where TYPE_NAME=''
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/gs_128.jpg' where TYPE_NAME='GlobalSat GTR-128'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/TR600.jpg' where TYPE_NAME='GlobalSat TR-600'
update CONTROLLER_TYPE set ImagePath='/about/includes/sources/images/devices/autograf.jpg' where TYPE_NAME='AvtoGraf'
go
