update vehicle set storageId = null where StorageId = 0
go

if exists (
	select *
		from sys.default_constraints
		where name = 'DF_VEHICLE_StorageId'
)
begin

	alter table VEHICLE
		drop constraint DF_VEHICLE_StorageId

end