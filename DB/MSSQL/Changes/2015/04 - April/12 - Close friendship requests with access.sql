declare @templateId int = (select mt.MESSAGE_TEMPLATE_ID from MESSAGE_TEMPLATE mt where mt.NAME = 'ProposeFriendship')
;with cte as
(
	select m.MESSAGE_ID, m.Owner_Operator_ID, mc.Contact_ID
	from [MESSAGE] m
		join Message_Contact mc on mc.Message_ID = m.MESSAGE_ID and mc.Type = 1
	where m.TEMPLATE_ID = @templateId and m.ProcessingResult <> 18 and m.[GROUP] is null and DestinationType_ID = 0
)

update [MESSAGE] set ProcessingResult = 18
from cte
	join Contact ac on ac.Demasked_ID = cte.Contact_ID
	join Asid a on a.Contact_ID = ac.ID
	join MLP_Controller mc on mc.Asid_ID = a.ID
	join CONTROLLER c on c.CONTROLLER_ID = mc.Controller_ID
where cte.MESSAGE_ID = [MESSAGE].MESSAGE_ID and exists 
(
	select * 
	from v_operator_vehicle_right r
	where r.right_id = 102 and r.vehicle_id = c.VEHICLE_ID and r.operator_id = cte.Owner_Operator_ID 
)