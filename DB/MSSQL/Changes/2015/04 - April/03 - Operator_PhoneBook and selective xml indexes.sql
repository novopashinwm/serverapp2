execute sys.sp_db_selective_xml_index null, 'true'
go

if not exists (select * from sys.tables where name='Operator_PhoneBook')
	create table Operator_PhoneBook (
		Id int identity(1,1) constraint PK_Operator_PhoneBook primary key,
		OperatorId int not null constraint FK_Operator_PhoneBook foreign key references OPERATOR(OPERATOR_ID),
		PhoneBook xml not null
	)
go

if exists (select * from sys.indexes where name='SXI_Operator_PhoneBook_PhoneBook')
	drop index SXI_Operator_PhoneBook_PhoneBook on Operator_PhoneBook
go
create selective xml index SXI_Operator_PhoneBook_PhoneBook on Operator_PhoneBook(PhoneBook) 
for 
(
	node_path1 = '/contacts/contact' as xquery 'node()',
	value_path = '/contacts/contact/Value' as xquery 'xs:string' maxlength(15) singleton 
)
go

if exists (select * from sys.indexes where name='SXI_Operator_PhoneBook_PhoneBook_Secondary')
	drop index SXI_Operator_PhoneBook_PhoneBook_Secondary on Operator_PhoneBook
create xml index SXI_Operator_PhoneBook_PhoneBook_Secondary on Operator_PhoneBook(PhoneBook) 
using xml index SXI_Operator_PhoneBook_PhoneBook for (value_path)
go
