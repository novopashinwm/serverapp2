if not exists (select * from sys.columns where object_id=object_id('VEHICLE') and name='StorageId')
	alter table VEHICLE add StorageId smallint
go
if not exists (select * from sys.columns where object_id=object_id('H_VEHICLE') and name='StorageId')
	alter table H_VEHICLE add StorageId smallint
go
if exists (select * from sys.columns where object_id=object_id('VEHICLE') and name='StorageId' and is_nullable = 0)
	alter table VEHICLE alter column StorageId smallint
go
update VEHICLE set StorageId = null where StorageId = 0
