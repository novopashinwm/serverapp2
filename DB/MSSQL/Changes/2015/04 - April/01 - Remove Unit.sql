if exists (select * from sys.columns where object_id=object_id('CONTROLLER_SENSOR_LEGEND') and name='Unit')
	alter table CONTROLLER_SENSOR_LEGEND drop column Unit
go
if exists (select * from sys.columns where object_id=object_id('CONTROLLER_SENSOR_MAP') and name='Unit')
	alter table CONTROLLER_SENSOR_MAP drop column Unit
go
if exists (select * from sys.columns where object_id=object_id('H_CONTROLLER_SENSOR_LEGEND') and name='Unit')
	alter table H_CONTROLLER_SENSOR_LEGEND drop column Unit
go
if exists (select * from sys.columns where object_id=object_id('H_CONTROLLER_SENSOR_MAP') and name='Unit')
	alter table H_CONTROLLER_SENSOR_MAP drop column Unit
go