if exists (
	select *
		from sys.tables
		where name = 'Monitoree_Log_Ignored'
)
	drop table Monitoree_Log_Ignored