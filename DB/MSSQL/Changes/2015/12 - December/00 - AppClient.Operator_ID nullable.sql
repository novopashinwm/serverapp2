if 0 = (select is_nullable from sys.columns where object_id = object_id('AppClient') and name = 'Operator_ID')
begin

	alter table AppClient 
		alter column Operator_ID int null

	alter table H_AppClient 
		alter column Operator_ID int null
end