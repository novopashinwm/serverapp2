if exists (
	select *
		from sys.indexes 
		where name = 'IX_Message_DestinationType_ID_Destination_ID'
)
begin

	drop index IX_Message_DestinationType_ID_Destination_ID on Message
	
end