declare @type_name varchar(100) = 'TS GLONASS'
declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

if exists (
	select * from CONTROLLER_SENSOR cs where cs.CONTROLLER_TYPE_ID = @controller_type_id and cs.DESCRIPT in ('CAN00', 'CAN0', 'CAN 0')
)
begin
	delete csm
		from CONTROLLER_SENSOR cs 
		join CONTROLLER_SENSOR_MAP csm on csm.CONTROLLER_SENSOR_ID = cs.CONTROLLER_SENSOR_ID
		where cs.CONTROLLER_TYPE_ID = @controller_type_id
		  and cs.DESCRIPT like 'CAN%'

	delete cs
		from CONTROLLER_SENSOR cs 
		where cs.CONTROLLER_TYPE_ID = @controller_type_id
		  and cs.DESCRIPT like 'CAN%'
end

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

declare @i int = 0, @j int, @n int
while @i < 8
begin

	set @j = 0
	while @j < 4
	begin
		set @n = @i * 10 + @j

		insert into @controller_sensor 
			select @controller_type_id, 1, 500 + @n, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'CAN' + (case when @n < 10 then '0' else '' end) + convert(varchar(2), @n)

		set @j = @j + 1
	end

	set @i = @i + 1
end

set nocount off

select * from @controller_sensor

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)
