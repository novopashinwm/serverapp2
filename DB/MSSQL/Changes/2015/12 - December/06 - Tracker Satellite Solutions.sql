declare @type_name nvarchar(255) = 'SatLite'
declare @deviceIdIsImei bit = 0

DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = @type_name)
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            (@type_name, 1000, @deviceIdIsImei, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END
else
begin
	update CONTROLLER_TYPE set DeviceIdIsImei = @deviceIdIsImei where Controller_Type_Id = @CONTROLLER_TYPE_ID and DeviceIdIsImei <> @deviceIdIsImei
end

set nocount on
declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 1, 'Ignition',		   2
union all select 1, 'Alarm',           3
union all select 1, 'DiscreteInput0', 10
union all select 1, 'DiscreteInput1', 11
union all select 1, 'DiscreteInput2', 12
union all select 1, 'DiscreteInput3', 13
union all select 1, 'DiscreteInput4', 14
union all select 1, 'DiscreteInput5', 15
union all select 1, 'DiscreteInput6', 16
union all select 1, 'DiscreteInput7', 17
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from (
		select Input = '', Sensor = '' where 1=0
		union all select 'Ignition', 'Зажигание'
		union all select 'Alarm', 'Тревога'
	) t
	join Controller_Sensor cs on cs.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID and cs.DESCRIPT = t.Input
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = t.Sensor
	where 1=1
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)