if not exists (
	select * from sys.indexes where name = 'IX_Message_Time'
)
begin

	create nonclustered index IX_Message_Time on Message(Time)
	
end