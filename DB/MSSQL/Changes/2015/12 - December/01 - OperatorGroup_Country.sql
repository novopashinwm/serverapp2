if not exists (
	select * from sys.tables where name = 'OperatorGroup_Country'
)
begin

	create table OperatorGroup_Country
	(
		ID int identity(1,1)
			constraint PK_OperatorGroup_Country
				primary key nonclustered,
		OperatorGroup_ID int not null 
			constraint FK_OperatorGroup_Country_OperatorGroup_ID
				foreign key references OperatorGroup(OperatorGroup_ID),
		Country_ID int not null 
			constraint FK_OperatorGroup_Country_Country_ID 
				foreign key references Country(Country_ID)
	)

	create unique clustered index IX_OperatorGroup_Country_OperatorGroup_ID_Country_ID
		on OperatorGroup_Country(OperatorGroup_ID, Country_ID)

	create nonclustered index IX_OperatorGroup_Country_Country_ID_OperatorGroup_ID
		on OperatorGroup_Country(Country_ID, OperatorGroup_ID)

end