declare @type_name nvarchar(255) = 'TrackerSOS'
declare @deviceIdIsImei bit = 1

DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = @type_name)
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            (@type_name, 1000, @deviceIdIsImei, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END
else
begin
	update CONTROLLER_TYPE set DeviceIdIsImei = @deviceIdIsImei where Controller_Type_Id = @CONTROLLER_TYPE_ID and DeviceIdIsImei <> @deviceIdIsImei
end

set nocount on
declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
		  select 0, 'Battery',	1
union all select 1, 'SOS',		2
union all select 1, 'FallDown',	3
union all select 1, 'Motion',	4
union all select 1, 'Movement',	5
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)

insert into CONTROLLER_SENSOR_LEGEND (Number, NAME, CONTROLLER_SENSOR_TYPE_ID)
	select t.Number, t.Name, cst.CONTROLLER_SENSOR_TYPE_ID
		from (
			select		Number = 232, Name = 'FallDown'
			union all select	 233,        'Motion'
			union all select	 234,        'Movement'
		) t
		join CONTROLLER_SENSOR_TYPE cst on cst.NAME = '��������'
		where not exists (select * from CONTROLLER_SENSOR_LEGEND e where e.Number = t.Number)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Sensor_Legend legend on legend.NAME = 
		case cs.Descript 
			when 'Battery' then 'BatteryLevel'
			when 'SOS' then '�������'
			when 'FallDown' then 'FallDown'
			when 'Motion' then 'Motion'
			when 'Movement' then 'Movement'
			else null
		 end
	where cs.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Default_Sensor_Legend_ID is null