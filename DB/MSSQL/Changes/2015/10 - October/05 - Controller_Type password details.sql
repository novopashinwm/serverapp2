if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller_Type')
		  and name = 'DefaultPassword'
)
begin

	alter table Controller_Type
		add DefaultPassword varchar(32),
		PasswordAlphabet varchar(255),
		PasswordLength int

end

go

if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller_Type')
		  and name = 'SmsTemplateForPasswordChange'
)
begin

	alter table Controller_Type
		add SmsTemplateForPasswordChange varchar(255)

end

go

update ct
	set DefaultPassword = t.DefaultPassword
		, PasswordAlphabet = t.PasswordAlphabet
		, PasswordLength = len(t.DefaultPassword)
		, SmsTemplateForPasswordChange = t.Template
	from CONTROLLER_TYPE ct
	join (
		select Type_Name = '', DefaultPassword = '', PasswordAlphabet = '', Template = '' where 1=0
		union all select 'TR06A',       '6666',     '1234567890',							'#<%-old%>#CP#<%-new%>#'
		union all select 'GPS103A',     '123456',   '1234567890',							'password<%-old%> <%-new%>'
		union all select 'Avtograf',    'testtest', 'abcdefghijklmnopqrstvwxyz0123456789',	null
		union all select 'AvtografCan', 'testtest', 'abcdefghijklmnopqrstvwxyz0123456789',  null
	) t on t.Type_Name = ct.TYPE_NAME
	where ct.DefaultPassword is null
	   or t.Template is not null and (ct.SmsTemplateForPasswordChange is null or ct.SmsTemplateForPasswordChange <> t.Template)
	   or t.DefaultPassword <> ct.DefaultPassword
