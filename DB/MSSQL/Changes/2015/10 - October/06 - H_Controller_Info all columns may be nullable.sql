if exists (
	select * from sys.columns where object_id = object_id('H_Controller_Info') and name = 'Time'
)
begin
	alter table H_Controller_Info
		alter column Time smalldatetime null
end

go

if exists (
	select * from sys.columns where object_id = object_id('H_Controller_Info') and name = 'Trail_ID'
)
begin
	alter table H_Controller_Info
		alter column Trail_ID int null
end

go


if exists (
	select * from sys.columns where object_id = object_id('H_Controller_Info') and name = 'FUEL_SENSOR_DIRECTION'
)
begin
	alter table H_Controller_Info
		alter column FUEL_SENSOR_DIRECTION bit null
end

go

