update controller_type
	set SupportsPassword = 1
	where TYPE_NAME in ('TR06A', 'GPS103A')
	  and SupportsPassword = 0