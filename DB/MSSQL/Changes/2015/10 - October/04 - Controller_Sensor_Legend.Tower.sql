declare @sensor table(name varchar(255), number int)

declare @legendNumber int = 231
declare @legendName varchar(255) = 'Tower'

insert into CONTROLLER_SENSOR_LEGEND (Number, NAME, CONTROLLER_SENSOR_TYPE_ID)
	select t.Number, t.Name, cst.CONTROLLER_SENSOR_TYPE_ID
		from (
			select Number = @legendNumber, Name = @legendName
		) t
		join CONTROLLER_SENSOR_TYPE cst on cst.NAME = '��������'
		where not exists (select * from CONTROLLER_SENSOR_LEGEND e where e.Number = t.Number)
