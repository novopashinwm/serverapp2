create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 set nocount on
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' CSTName, 4 Number, Descript = 'HDOP'

		) t
	where ct.Type_Name in ('AvtoGraf', 'AvtoGrafCan')
	  and cst.Name = t.CSTName
set nocount off
insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

drop table #Controller_Sensor

update cs
	set 
	   Default_Multiplier = 0.01
     , Default_Constant = 0
     , Default_Sensor_Legend_ID = legend.CONTROLLER_SENSOR_LEGEND_ID
	from Controller_Type ct 
	join CONTROLLER_SENSOR cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID 
	join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = 'HDOP'
	where ct.TYPE_NAME in ('AvtoGraf', 'AvtoGrafCan')
	  and cs.Descript = 'HDOP'
	  and cs.Default_Sensor_Legend_ID is null