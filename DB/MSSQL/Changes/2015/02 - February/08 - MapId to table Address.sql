if object_id('GetLatCell') is null
	exec sp_sqlexec '
create function dbo.GetLatCell(
	@lat float
) returns int with schemabinding
as
begin
	declare @cellSizeInMeters float = 15
	declare @latStep float = 180/((40000000/2)/@cellSizeInMeters)
    set @lat = case	
		when @lat < 0 then @lat+180*(round(-@lat/180,0)+1)
		when @lat >= 180 then @lat-180
		else @lat
	end
	declare @latCell int = round(@lat/@latStep,0)
	return @latCell
end'
go
if object_id('GetLngCell') is null
	exec sp_sqlexec '
create function dbo.GetLngCell(
	@lng float
) returns int with schemabinding
as
begin
	declare @cellSizeInMeters float = 15
	declare @lngStep float = 360/(40000000/@cellSizeInMeters)
	set @lng = case
		when @lng < 0 then @lng+360*(round(-@lng/360,0)+1)
		when @lng >= 360 then @lng-360
		else @lng
	end
	declare @lngCell int = round(@lng/@lngStep,0)
	return @lngCell
end'
go
if not exists (select * from sys.columns where object_id=object_id('Address') and name='Lat')
	alter table Address add Lat float null
go
if not exists (select * from sys.columns where object_id=object_id('Address') and name='Lng')
	alter table Address add Lng float null
go
if not exists (select * from sys.columns where object_id=object_id('Address') and (name='Lat' or name='Lng') and is_nullable=0)
begin
	exec sp_sqlexec 'update Address set Lat=geo.Lat, Lng=geo.Long'
	alter table Address alter column Lat float not null
	alter table Address alter column Lng float not null
end
go
if exists (select * from sys.indexes where name = 'IX_Address_Geo')
	drop index IX_Address_Geo on Address
go
if exists (select * from sys.columns where object_id=object_id('Address') and name='Geo')
	alter table Address drop column Geo
go
if not exists (select * from sys.columns where object_id=object_id('Address') and name='MapId')
	alter table Address add MapId uniqueidentifier constraint DF_Address_MapId default('00000000-0000-0000-0000-000000000000') not null
go
if not exists (select * from sys.columns where object_id=object_id('Address') and name='LatCell')
	alter table Address add LatCell as dbo.GetLatCell(Lat) persisted not null 
go
if not exists (select * from sys.columns where object_id=object_id('Address') and name='LngCell')
	alter table Address add LngCell as dbo.GetLngCell(Lng) persisted not null 
go
if not exists (select * from sys.indexes where object_id=object_id('Address') and name='IX_Address_LatLngCells')
	create nonclustered index IX_Address_LatLngCells on Address(LatCell, LngCell, MapId, Language) include (Value)
go
