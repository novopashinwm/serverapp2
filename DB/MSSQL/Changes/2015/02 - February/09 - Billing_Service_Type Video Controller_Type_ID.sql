update bst set Controller_Type_ID = ct.Controller_Type_ID
	from Billing_Service_Type bst
	join Controller_Type ct on ct.Type_Name = 'MLP'
	where bst.Service_Type_Category = 'Video'
	  and bst.Controller_Type_ID is null