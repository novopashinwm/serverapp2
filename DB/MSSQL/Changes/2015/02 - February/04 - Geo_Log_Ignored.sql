if not exists (
	select *
		from sys.tables 
		where name = 'Geo_Log_Ignored'
)
begin

	create table Geo_Log_Ignored
	(
		Vehicle_ID int not null,
		Log_Time int not null,
		Reason tinyint not null,
		Lng numeric(8,5) not null, 
		Lat numeric(8,5) not null,
		Speed smallint,
		Course smallint,
		Height smallint,
		HDOP numeric(4,2),
		InsertTime datetime not null constraint DF_Geo_Log_Ignored default (getutcdate())
		constraint PK_Geo_Log_Ignored primary key clustered (Vehicle_ID, Log_Time, Reason)
	) on TSS_Data2
		

end