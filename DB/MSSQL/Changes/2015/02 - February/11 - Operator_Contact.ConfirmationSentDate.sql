if not exists (select * from sys.columns where object_id=object_id('Operator_Contact') and name='ConfirmationSentDate')
	alter table Operator_Contact add ConfirmationSentDate datetime
go
if not exists (select * from sys.columns where object_id=object_id('H_Operator_Contact') and name='ConfirmationSentDate')
	alter table H_Operator_Contact add ConfirmationSentDate datetime
go
update oc set ConfirmationSentDate = m.Created
from Operator_Contact oc
	join Message_Contact mc on mc.Contact_ID = oc.Contact_ID
	join MESSAGE m on m.MESSAGE_ID = mc.Message_ID
	join MESSAGE_TEMPLATE mt on mt.MESSAGE_TEMPLATE_ID = m.TEMPLATE_ID
where m.Owner_Operator_ID = oc.Operator_ID and mt.NAME = 'GenericConfirmation' and oc.ConfirmationSentDate is null
go
