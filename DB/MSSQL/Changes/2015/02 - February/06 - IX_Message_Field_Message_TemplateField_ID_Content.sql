if not exists (
	select *
		from sys.columns
		where object_id = object_id('Message_Field')
		  and name = 'LargeContent'
)
begin

	alter table Message_Field
		add LargeContent nvarchar(MAX)

	alter table H_Message_Field
		add LargeContent nvarchar(MAX)

end
go

update Message_Field
	set LargeContent = Content
	  , Content = substring(Content, 1, 255)
	where len(Content) > 255

update H_Message_Field
	set LargeContent = Content
	  , Content = substring(Content, 1, 255)
	where len(Content) > 255

go

if exists (
	select *
		from sys.columns
		where object_id = object_id('Message_Field')
		  and name = 'Content'
		  and max_length = 4096
)
begin
	
	if exists (select * from sys.indexes where name = 'IX_Message_Field_Message_Template_Field_ID_Message_ID')
		drop index IX_Message_Field_Message_Template_Field_ID_Message_ID on Message_Field
		
	if exists (select * from sys.indexes where name = 'IX_Message_Field_Message_ID_Message_Template_Field_ID')
		drop index IX_Message_Field_Message_ID_Message_Template_Field_ID on Message_Field
		
	if exists (select * from sys.indexes where name = 'IX_MESSAGE_FIELD_MESSAGE_ID')
		drop index IX_MESSAGE_FIELD_MESSAGE_ID on Message_Field	

	exec sp_executesql N'	alter table Message_Field alter column Content nvarchar(255)'
	exec sp_executesql N'	alter table H_Message_Field alter column Content nvarchar(255)'

end

go

if not exists (
	select *
		from sys.indexes
		where name = 'IX_Message_Field_Message_Template_Field_ID_Content')
begin

	create nonclustered index IX_Message_Field_Message_Template_Field_ID_Content
		on Message_Field (Message_Template_Field_ID, Content) include (Message_ID)

end

go

if not exists (
	select *
		from sys.indexes
		where name = 'IX_Message_Field_Message_ID_Message_Template_Field_ID'
)
begin

	create nonclustered index IX_Message_Field_Message_ID_Message_Template_Field_ID
		on Message_Field(Message_ID, Message_Template_Field_ID) include (Content)

end