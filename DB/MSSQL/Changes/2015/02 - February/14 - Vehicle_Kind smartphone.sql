declare @smartphone int = 12

set identity_insert Vehicle_Kind on

insert into Vehicle_Kind (VEHICLE_KIND_ID, NAME)
	select t.*
		from (
			select ID = @smartphone, Name = 'Smartphone') t
		where not exists (Select * from VEHICLE_KIND vk where vk.VEHICLE_KIND_ID = t.ID)

set identity_insert Vehicle_Kind off

update bst 
	set Vehicle_Kind_ID = @smartphone
	from billing_service_type bst
	where bst.Service_Type_Category = 'NikaTracker'
	  and bst.Vehicle_Kind_ID <> @smartphone
	  
--���������� ������������ ��������
	  
update ct
	set Default_Vehicle_Kind_ID = vk.Vehicle_Kind_ID
	from CONTROLLER_Type ct
	join Vehicle_Kind vk on vk.Name = 'SmartPhone'
	where ct.Type_Name in ('Nika.Tracker', 'S60V3FP')
	  and (ct.Default_Vehicle_Kind_ID is null or 
	       ct.Default_Vehicle_Kind_ID <> vk.Vehicle_Kind_ID)

update v 
	set Vehicle_Kind_ID = ct.Default_Vehicle_Kind_ID
	from Vehicle v
	join Controller c on c.VEHICLE_ID = v.VEHICLE_ID
	join Controller_Type ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
	where ct.Type_Name in ('Nika.Tracker', 'S60V3FP')
	  and v.Vehicle_Kind_ID <> ct.Default_Vehicle_Kind_ID