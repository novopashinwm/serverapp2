declare @sensor table(name varchar(255), number int)

set nocount on
insert into @sensor select 'Temperature', 421
set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript, Default_Sensor_Legend_ID, Default_Multiplier)
	select ct.Controller_Type_ID
		, 1
		, s.number
		, s.name
		, legend.CONTROLLER_SENSOR_LEGEND_ID
		, 0.001
	from @sensor s
		join Controller_Type ct on ct.Type_Name in ('TS GLONASS', 'Wialon')
		join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = '�������� ������ �����������'
	where not exists (
		select * 
		from Controller_Sensor e
		where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
			and e.NUMBER = s.number)
