if not exists (select * from sys.columns where object_id = object_id('H_Controller_Type') and name = 'LocatorGsmTimeout')
	alter table H_Controller_Type add LocatorGsmTimeout int
go
if not exists (select * from sys.columns where name='Message_ID' and object_id=object_id('TRAIL'))
	alter table TRAIL add Message_ID int null constraint FK_TRAIL_Message_ID foreign key(Message_ID) references [MESSAGE](Message_ID)
go
-- ���������� ��� ��������
insert into CONSTANTS
select Name, Value from
(
	select 'InterworkingContentProviderId', '37'
	union all
	select 'InterworkingMegafonMaskedPhone', '000'
	union all 
	select 'InterworkingRequestQuantity', '100'
	union all
	select 'InterworkingContactsQuantity', '10'
	union all
	select 'InterworkingNikaId', '36'
) t(Name, Value)
where not exists 
(select * from CONSTANTS where NAME = t.Name)

-- ���� ���������
if (not exists (select * from sys.tables where name = 'CounterType'))
	create table CounterType (
		Id int not null identity(1,1),
		Type tinyint not null,
		Value varchar(100) not null,

		constraint PK_CounterType primary key (Id)
	)
go

-- ������� ������������� �����
if not exists (select * from sys.tables where name = 'ContactCounter')
	create table ContactCounter
	(
		From_Id int not null,								-- ������������� ������������� ��������
		To_Id int not null,									-- ������������� ������������� ��������
		TypeId int not null,								-- ��� ��������
		Value int not null 
			constraint DF_ContactCounter_Value default(0),	-- �������� ��������

		constraint PK_ContactCounter primary key (From_Id, To_Id, TypeId),
		constraint FK_ContactCounter_From foreign key(From_Id) references Contact(ID),
		constraint FK_ContactCounter_To foreign key(To_Id) references Contact(ID),
		constraint FK_ContactCounter_TypeId foreign key(TypeId) references CounterType(Id),
	)
go
