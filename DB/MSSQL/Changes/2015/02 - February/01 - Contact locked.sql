if not exists (select * from sys.columns where object_id = object_id('Contact') and name = 'Valid')
	alter table Contact add Valid bit not null constraint DF_Contact_Valid default(1)
go
