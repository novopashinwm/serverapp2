if not exists (
	select *
		from sys.columns c
		where c.object_id = object_id('Message')
		  and c.name = 'Created'
)
begin

	alter table Message
		add Created datetime 
		
	alter table H_Message
		add Created datetime

end
go

if exists (
	select *
		from sys.columns c
		where c.object_id = object_id('Message')
		  and c.name = 'Created'
		  and c.is_nullable = 0
)
begin

	alter table Message
		alter column Created datetime

	alter table H_Message
		alter column Created datetime	

end