if not exists (select * from sys.tables where name = 'ZONE_PRIMITIVE_TYPE')
	create table ZONE_PRIMITIVE_TYPE
	(
		Id int not null,
		Name varchar(10) not null,
		constraint PK_ZONE_PRIMITIVE_TYPE primary key (Id)
	)
go
insert into ZONE_PRIMITIVE_TYPE(Id, Name)
select *
from 
(
	select 1, 'Circle'
	union all
	select 3, 'Polygon'
) t(Id, Name)
where not exists (select * from ZONE_PRIMITIVE_TYPE where Id = t.Id)
go
if not exists (select * from sys.indexes where name = 'IX_GEO_ZONE_PRIMITIVE_ZONE_ID')
	create nonclustered index IX_GEO_ZONE_PRIMITIVE_ZONE_ID on GEO_ZONE_PRIMITIVE(ZONE_ID) INCLUDE(PRIMITIVE_ID)
go