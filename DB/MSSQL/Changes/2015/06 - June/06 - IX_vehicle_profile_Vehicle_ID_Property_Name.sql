if not exists (
	select *
		from sys.indexes 
		where name = 'IX_vehicle_profile_Vehicle_ID_Property_Name'
)
begin

	create nonclustered index IX_vehicle_profile_Vehicle_ID_Property_Name
		on vehicle_profile(Vehicle_ID, Property_Name) include (Value)

end