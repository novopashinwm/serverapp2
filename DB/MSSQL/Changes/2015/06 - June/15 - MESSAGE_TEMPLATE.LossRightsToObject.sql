update MESSAGE_TEMPLATE set 
	HEADER = '�������� �$scheduler_id$ �� "$scheduler_name$" �� ����� ���� ���������', 
	BODY = '�� �������� ����� ($rights_names$) �� $object_type$ "$object_name$", ����� �� ����� ���� ��������.' 
where name = 'LossRightsToObject' and Culture_ID is null
update t set 
	t.HEADER = 'Subscription �$scheduler_id$ to "$scheduler_name$" cannot be done',
	t.BODY = 'You loss rights ($rights_names$) on $object_type$ "$object_name$", report can not be built.' 
from MESSAGE_TEMPLATE t 
	join Culture c on t.Culture_ID = c.Culture_ID
where name = 'LossRightsToObject' and c.Code = 'en-US'
go

insert into MESSAGE_TEMPLATE_FIELD (NAME, MESSAGE_TEMPLATE_ID, DOTNET_TYPE_ID)
select f.NAME, t.id, f.DOTNET_TYPE_ID
from 
(
	select  'scheduler_name', (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all
	select 'rights_names', (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all 
	select 'object_type', (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all 
	select 'object_name', (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.String')
	union all 
	select 'scheduler_id', (select DOTNET_TYPE_ID from DOTNET_TYPE where TYPE_NAME = 'System.Int32')
) f(NAME, DOTNET_TYPE_ID)
	cross apply 
	(
		select MESSAGE_TEMPLATE_ID
		from MESSAGE_TEMPLATE
		where name = 'LossRightsToObject'
	) t(id)
where not exists (select * from MESSAGE_TEMPLATE_FIELD where NAME = f.NAME and MESSAGE_TEMPLATE_ID = t.id)

