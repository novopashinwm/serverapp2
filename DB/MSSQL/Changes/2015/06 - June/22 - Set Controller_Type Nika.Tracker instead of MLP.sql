update c 
	set CONTROLLER_TYPE_ID = ct_new.CONTROLLER_TYPE_ID
	from controller_type ct
	join Controller c on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join MLP_Controller mc on mc.Controller_ID = c.CONTROLLER_ID
	join Asid a on a.ID = mc.Asid_ID
	join CONTROLLER_TYPE ct_new on ct_new.TYPE_NAME = 'Nika.Tracker'
	where ct.TYPE_NAME = 'MLP'
	  and a.SimID is not null