if not exists (select * from sys.columns where name='SortOrder' and object_id=object_id('CONTROLLER_TYPE'))
	alter table CONTROLLER_TYPE add SortOrder int 
go
if not exists (select * from sys.columns where name='SortOrder' and object_id=object_id('H_CONTROLLER_TYPE'))
	alter table H_CONTROLLER_TYPE add SortOrder int 
go

update CONTROLLER_TYPE set SortOrder = 1 where TYPE_NAME='GenericTracker'
update CONTROLLER_TYPE set SortOrder = 2 where TYPE_NAME='Nika.Tracker'