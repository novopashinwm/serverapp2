declare @enCultureId int = (select Culture_ID from Culture where Code='en-US')
declare @templateId int = (select MESSAGE_TEMPLATE_ID from MESSAGE_TEMPLATE where NAME = 'LossRightsToObject' and Culture_ID is null)
declare @enTemplateId int = (select MESSAGE_TEMPLATE_ID from MESSAGE_TEMPLATE where NAME = 'LossRightsToObject' and Culture_ID = @enCultureId)
if @enTemplateId is null
begin
	insert into MESSAGE_TEMPLATE(NAME, HEADER, BODY, Culture_ID)
	values ('LossRightsToObject', 'Subscription to "$scheduler_name$" is stopped', 'Your access on $rights_names$ to $object_type$ "$object_name$" is denied, report cannot be built', @enCultureId)
	set @enTemplateId = @@identity
end

insert into MESSAGE_TEMPLATE_FIELD(MESSAGE_TEMPLATE_ID, NAME, DOTNET_TYPE_ID)
select @enTemplateId, f.NAME, f.DOTNET_TYPE_ID
from MESSAGE_TEMPLATE_FIELD f
where f.MESSAGE_TEMPLATE_ID = @templateId and not exists 
(
	select *
	from MESSAGE_TEMPLATE_FIELD tf
	where tf.MESSAGE_TEMPLATE_ID = @enTemplateId and tf.NAME = f.NAME
)