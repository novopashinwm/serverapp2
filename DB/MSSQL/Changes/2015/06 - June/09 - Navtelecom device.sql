DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'Navtelecom')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('Navtelecom', 65551, 1, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
          select 1, 'Alarm', 1
union all select 1, 'Engine', 2
union all select 1, 'Navigation', 3
union all select 1, 'Gsm', 4
union all select 1, 'MainAccVoltage', 5
union all select 1, 'SecondAccVoltage', 6
union all select 1, 'MotoHours', 7
union all select 1, 'DigitalInput1', 10
union all select 1, 'DigitalInput2', 11
union all select 1, 'DigitalInput3', 12
union all select 1, 'DigitalInput4', 13
union all select 1, 'DigitalInput5', 14
union all select 1, 'DigitalInput6', 15
union all select 1, 'DigitalInput7', 16
union all select 1, 'DigitalInput8', 17

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Тревога'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Alarm'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Зажигание'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Engine'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
	        	        
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'BatteryLevel'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'MainAccVoltage'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
