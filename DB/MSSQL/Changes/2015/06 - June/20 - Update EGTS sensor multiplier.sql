update s set s.Default_Multiplier = 0.01
from CONTROLLER_SENSOR s
where s.CONTROLLER_TYPE_ID = (select t.CONTROLLER_TYPE_ID from CONTROLLER_TYPE t where t.TYPE_NAME = 'Egts')
	and s.Descript in ('Hdop', 'Vdop', 'Pdop') and s.Default_Multiplier <> 0.01

update s set s.Mandatory = 0
from CONTROLLER_TYPE t
	join CONTROLLER_SENSOR s on s.CONTROLLER_TYPE_ID = t.CONTROLLER_TYPE_ID
where t.TYPE_NAME = 'Egts' and s.Descript = 'Hdop' and s.Mandatory = 1

delete m
from CONTROLLER_TYPE t
	join CONTROLLER_SENSOR s on s.CONTROLLER_TYPE_ID = t.CONTROLLER_TYPE_ID
	join CONTROLLER_SENSOR_MAP m on m.CONTROLLER_SENSOR_ID = s.CONTROLLER_SENSOR_ID
where t.TYPE_NAME = 'Egts' and s.Descript = 'Hdop'