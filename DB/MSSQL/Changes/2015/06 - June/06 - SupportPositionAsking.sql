if exists (select * from sys.default_constraints where name='DF_Controller_Type_SupportsPositionAsking')
	exec('alter table CONTROLLER_TYPE drop constraint DF_Controller_Type_SupportsPositionAsking')
go
if exists (select * from sys.columns where object_id=object_id('CONTROLLER_TYPE') and name='SupportsPositionAsking')
	exec('alter table CONTROLLER_TYPE drop column SupportsPositionAsking')
go
if exists (select * from sys.default_constraints where name='DF_H_Controller_Type_SupportsPositionAsking')
	exec('alter table H_CONTROLLER_TYPE drop constraint DF_H_Controller_Type_SupportsPositionAsking')
go
if exists (select * from sys.columns where object_id=object_id('H_CONTROLLER_TYPE') and name='SupportsPositionAsking')
	exec('alter table H_CONTROLLER_TYPE drop column SupportsPositionAsking')
go