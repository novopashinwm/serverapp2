if object_id('tempdb..#configs') is not null drop table #configs

select q.SCHEDULERQUEUE_ID id, convert(xml, q.config_xml) config
	into #configs
from SCHEDULERQUEUE q 
	join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID = e.SCHEDULEREVENT_ID
	join REPORT r on r.REPORT_ID = e.Report_Id
where r.REPORT_GUID = '6E2DE7AD-2656-4812-BC1C-0EEA32613662'
order by q.SCHEDULERQUEUE_ID desc

update #configs SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataTable
')
update #configs SET CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data";
	delete //a3:DataSet
')
update #configs set CONFIG.modify('
	declare namespace a2="http://schemas.microsoft.com/clr/nsassem/System.Data/System.Data%2C%20Version%3D2.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3Db77a5c561934e089";
	delete //a2:DataTable
')
update #configs set CONFIG.modify('
	declare namespace a3="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Editors/FORIS.TSS.Commands";
	delete //a3:PropValue
')
update #configs SET CONFIG.modify('
	delete //pvZonesList
')
update #configs SET CONFIG.modify('
	delete //dsParamsForEditor
')
update #configs SET CONFIG.modify('
	declare namespace a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.TransportDispatcher.Reports/FORIS.TSS.TransportDispatcher.ReportZoneOver";
	insert <_zoneIds xsi:null="1" />
	into (//a1:GeoZoneOverTssReportParameters)[1]
')
where config.exist('//_zoneIds') = 0

update SCHEDULERQUEUE
set CONFIG_XML = convert(nvarchar(max), c.config)
from #configs c
where c.id = SCHEDULERQUEUE_ID

drop table #configs
