if exists (select * from sys.tables where name = 'Command_Result')
begin
	create table Commands
	(
		ID int IDENTITY(1,1) NOT NULL,
		Sender_ID int NULL,
		Target_ID int NOT NULL,
		Type_ID int NOT NULL,
		Date_Received datetime NOT NULL,
		Payer_Vehicle_ID int NULL,
		Status int NOT NULL,
		Result_Date_Received datetime NOT NULL,
		Result_Type_ID int NOT NULL,
		Log_Time int NULL,
		ErrorCount int NOT NULL constraint DF_Command_ErrorCount default(0)
	)

	set identity_insert Commands on

	declare @count int = 1000
	declare @startId int = 0
	declare @inserted int = -1
	while @inserted <> 0
	begin
		insert into Commands(ID, Sender_ID, Target_ID, Type_ID, Date_Received, Payer_Vehicle_ID, Status, Result_Date_Received, Result_Type_ID, Log_Time, ErrorCount)
		select top(@count) c.ID, c.Sender_ID, c.Target_ID, c.Type_ID, c.Date_Received, c.Payer_Vehicle_ID, c.Status, r.Date_Received, r.Result_Type_ID, r.Log_Time, r.ErrorCount
		from Command c
			join Command_Result r on c.Id = r.Command_ID
		where c.ID > @startId
		order by c.ID asc

		set @inserted = @@rowcount
		set @startId = scope_identity()
	end

	set identity_insert Commands off

	alter table Commands add constraint PK_Commands primary key clustered(ID)
	alter table Commands add constraint FK_Commands_Payer_Vehicle_ID foreign key(Payer_Vehicle_ID) references VEHICLE(VEHICLE_ID)
	alter table Commands add constraint FK_Commands_Sender_ID foreign key(Sender_ID) references OPERATOR(OPERATOR_ID)
	alter table Commands add constraint FK_Commands_Target_ID foreign key(Target_ID) references VEHICLE(VEHICLE_ID)
	alter table Commands add constraint FK_Commands_Type_ID foreign key(Type_ID) references CommandTypes(id)

	-- для отчетов
	create nonclustered index IX_Commands_Type_ID_Status_Date_Received on Commands(Type_ID, Status, Date_Received) include(Target_Id, Sender_Id)
	-- для запроса ТС
	create nonclustered index IX_Commands_Target_Id_Type_ID_Date_Received on Commands(Target_Id, Type_ID, Date_Received)
	-- для хранимки GetLogNew
	create nonclustered index IX_Commands_Target_ID_Log_Time on Commands(Target_ID, Log_Time) 
	-- для выборки из очереди
	create nonclustered index IX_Commands_Status_Date_Received on Commands(Status, Date_Received) include(ErrorCount)
	
	alter table Command_Parameter drop constraint FK_Command_Parameter_Command_ID
	alter table RenderedServiceItem drop constraint FK_RenderedServiceItem_Command_ID

	drop table Command_Result
	drop table Command
	exec sp_rename 'Commands', 'Command'
	exec sp_rename 'PK_Commands', 'PK_Command'
	exec sp_rename 'FK_Commands_Payer_Vehicle_ID', 'FK_Command_Payer_Vehicle_ID'
	exec sp_rename 'FK_Commands_Sender_ID', 'FK_Command_Sender_ID'
	exec sp_rename 'FK_Commands_Target_ID', 'FK_Command_Target_ID'
	exec sp_rename 'FK_Commands_Type_ID', 'FK_Command_Type_ID'

	exec sp_rename 'Command.IX_Commands_Type_ID_Status_Date_Received', 'IX_Command_Type_ID_Status_Date_Received'
	exec sp_rename 'Command.IX_Commands_Target_Id_Type_ID_Date_Received', 'IX_Command_Target_Id_Type_ID_Date_Received'
	exec sp_rename 'Command.IX_Commands_Target_ID_Log_Time', 'IX_Command_Target_ID_Log_Time'
	exec sp_rename 'Command.IX_Commands_Status_Date_Received', 'IX_Command_Status_Date_Received'

	alter table Command_Parameter with check add constraint FK_Command_Parameter_Command_ID foreign key(Command_ID) references Command(ID) 
	alter table RenderedServiceItem with check add constraint FK_RenderedServiceItem_Command_ID foreign key(Command_ID) references Command(ID) 
end