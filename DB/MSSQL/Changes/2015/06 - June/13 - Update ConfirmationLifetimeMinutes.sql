if not exists (select * from CONSTANTS where NAME='ConfirmationLifetimeMinutes')
	insert into CONSTANTS(NAME, VALUE) values ('ConfirmationLifetimeMinutes', '15')
else
	update CONSTANTS set VALUE = 15 where NAME = 'ConfirmationLifetimeMinutes' and VALUE <> '15'
go

delete r
from Billing_Service_Type t
	join Billing_Service s on s.Billing_Service_Type_ID = t.ID
	join Rendered_Service r on r.Billing_Service_ID = s.ID
where Service_Type = 'Free.SMS.Confirmation'
go

delete s 
from Billing_Service_Type t
	join Billing_Service s on s.Billing_Service_Type_ID = t.ID
where Service_Type = 'Free.SMS.Confirmation'
go

delete t
from Billing_Service_Type t 
where Service_Type = 'Free.SMS.Confirmation'
go

if not exists (select * from sys.tables where name='Ip')
	create table Ip
	(
		Id int identity(1,1) not null,
		Value varchar(50)

		constraint PK_Ip primary key(Id)
	)
go

if not exists (select * from sys.tables where name='RequestFreqLimit')
begin
	create table RequestFreqLimit
	(
		Id int identity(1,1),
		Name varchar(50) not null,
		MaxCount int not null,
		Seconds int not null,

		constraint PK_RequestLimit primary key (Id)
	)

	execute sp_addextendedproperty 'MS_Description', '������������ ���������� �������� � 1 ip-������ �� �����', 'user', 'dbo', 'table', 'RequestFreqLimit', 'column', 'MaxCount'
	execute sp_addextendedproperty 'MS_Description', '�������� ��������� �������� ���������� �������� (���)', 'user', 'dbo', 'table', 'RequestFreqLimit', 'column', 'Seconds'
end
go

if not exists (select * from sys.tables where name='RequestCounter')
begin
	create table RequestCounter
	(
		Id int identity(1,1),
		FreqLimitId int not null,
		IpId int not null,
		Value int not null constraint DF_RequestCounter_Value default(0),
		StartDate datetime not null constraint DF_RequestCounter_StartDate default(getutcdate()),
		 
		constraint PK_RequestCounter primary key(Id),
		constraint FK_RequestCounter_FreqLimitId foreign key (FreqLimitId) references RequestFreqLimit(Id),
		constraint FK_RequestCounter_Ip foreign key (IpId) references Ip(Id)
	)

	execute sp_addextendedproperty 'MS_Description', '���������� �������� ��������� �� �������� ��������� � RequestFreqLimit', 'user', 'dbo', 'table', 'RequestCounter', 'column', 'Value'
end
go
if not exists (select * from sys.tables where name='RequestCounterLog')
begin
	create table RequestCounterLog 
	(
		Id int identity(1,1),
		RequestCounterId int not null,
		RequestCount int not null,
		ResetTime datetime not null constraint DF_RequestCounterLog_ResetTime default getutcdate(),

		constraint PK_RequestCounterLog primary key(Id),
		constraint FK_RequestCounterLog_RequestCounterId foreign key (RequestCounterId) references RequestCounter(Id)
	) 

	execute sp_addextendedproperty 'MS_Description', '���������� �������� ��������� �� �������� �� ������ ��������', 'user', 'dbo', 'table', 'RequestCounterLog', 'column', 'RequestCount'
	execute sp_addextendedproperty 'MS_Description', '���� ������ ��������', 'user', 'dbo', 'table', 'RequestCounterLog', 'column', 'ResetTime'
end

if exists (select * from sys.indexes where name = 'IX_Ip_Value')
	drop index IX_Ip_Value on Ip
go
create nonclustered index IX_Ip_Value on Ip(Value)
go
if exists (select * from sys.indexes where name = 'IX_RequestFreqLimit_Name')
	drop index IX_RequestFreqLimit_Name on RequestFreqLimit
go
create nonclustered index IX_RequestFreqLimit_Name on RequestFreqLimit(Name)
go
if exists (select * from sys.indexes where name = 'IX_RequestCounter_Ip_RequestLimit')
	drop index IX_RequestCounter_Ip_RequestLimit on RequestCounter
go
create nonclustered index IX_RequestCounter_Ip_RequestLimit on RequestCounter(IpId, FreqLimitId)
go

insert into RequestFreqLimit(Name, MaxCount, Seconds)
select 'SmsConfirmation', 1, 15 * 60
where not exists 
(
	select * from RequestFreqLimit where Name = 'SmsConfirmation'
)

insert into RequestFreqLimit(Name, MaxCount, Seconds)
select 'MsisdnVerification', 5, 15 * 60
where not exists 
(
	select * from RequestFreqLimit where Name = 'MsisdnVerification'
)
