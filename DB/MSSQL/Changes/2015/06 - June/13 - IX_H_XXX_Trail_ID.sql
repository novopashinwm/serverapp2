declare @sql nvarchar(max)

while 1=1
begin

	set @sql = (
		select top(1) 'create nonclustered index IX_' + o.name + '_Trail_ID on ' + o.name + '(Trail_ID)'
			from sys.objects o
			outer apply (
				select i.name, column_name = c.name
					from sys.indexes i 
					join sys.index_columns ic on ic.index_id = i.index_id and ic.object_id = o.object_id
					join sys.columns c on c.column_id = ic.column_id and c.object_id = o.object_id
					where i.object_id = o.object_id
					  and c.name = 'Trail_ID'
			) i
			where substring(o.name, 1, 2) = 'H_'
			  and o.type = 'u'
			  and i.column_name is null
			order by o.name, i.name
	)

	if @sql is null
		break

	exec sp_executesql @sql

end


