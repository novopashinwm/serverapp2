if not exists (
	select *
		from sys.indexes 
		where name = 'IX_OperatorGroup_Department_Department_ID'
)
begin

	create nonclustered index IX_OperatorGroup_Department_Department_ID
		on OperatorGroup_Department(Department_ID)

end