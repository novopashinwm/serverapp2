declare @askPositionId int = (select id from CommandTypes where code = 'AskPositionOverMLP')

set nocount on

declare @supporting_type table (name varchar(255))
insert into @supporting_type
select 'AvtoGraf' 
union all select 'AvtoGrafCan' 
union all select 'Smarts GT02' 
union all select 'Smarts Baby Bear' 
union all select 'Smarts GT06' 
union all select 'Smarts GT06N' 
union all select 'MLP' 
union all select 'Nika.Tracker'

set nocount off

delete ctct
	from Controller_Type_CommandTypes ctct
	join controller_type ct on ct.CONTROLLER_TYPE_ID = ctct.Controller_Type_ID	
	where ctct.CommandTypes_ID = @askPositionId
	  and ct.TYPE_NAME not in ( select st.name from @supporting_type st )

insert into Controller_Type_CommandTypes(Controller_Type_ID, CommandTypes_ID)
	select CONTROLLER_TYPE_ID, @askPositionId
		from CONTROLLER_TYPE ct
		where TYPE_NAME in ( select st.name from @supporting_type st )
		and not exists (select * from Controller_Type_CommandTypes e where e.Controller_Type_ID = ct.CONTROLLER_TYPE_ID and e.CommandTypes_ID = @askPositionId)