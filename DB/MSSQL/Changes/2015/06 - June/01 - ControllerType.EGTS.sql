DECLARE @CONTROLLER_TYPE_ID INT = (SELECT TOP 1 CONTROLLER_TYPE_ID FROM CONTROLLER_TYPE WHERE TYPE_NAME = 'EGTS')
IF @CONTROLLER_TYPE_ID IS NULL
BEGIN
      INSERT INTO CONTROLLER_TYPE
            (TYPE_NAME, PACKET_LENGTH, DeviceIdIsImei, AllowedToAddByCustomer)
      VALUES
            ('EGTS', 65535, 1, 1)
            
      SET @CONTROLLER_TYPE_ID = @@IDENTITY
END

update CONTROLLER_TYPE set ImagePath = '/about/includes/sources/images/devices/112_3g.jpg' where CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID

set nocount on

declare @sensor table(digital bit, name varchar(255), number int)
insert into @sensor
          select 1, 'DigitalInput1', 1
union all select 1, 'DigitalInput2', 2
union all select 1, 'DigitalInput3', 3
union all select 1, 'DigitalInput4', 4
union all select 1, 'DigitalInput5', 5
union all select 1, 'DigitalInput6', 6
union all select 1, 'DigitalInput7', 7
union all select 1, 'DigitalInput8', 8
union all select 1, 'Ignition', 10
union all select 1, 'Hdop', 11
union all select 1, 'Vdop', 12
union all select 1, 'Pdop', 13
union all select 1, 'AnalogueInput1', 14
union all select 1, 'AnalogueInput2', 15
union all select 1, 'AnalogueInput3', 16
union all select 1, 'AnalogueInput4', 17
union all select 1, 'AnalogueInput5', 18
union all select 1, 'AnalogueInput6', 19
union all select 1, 'AnalogueInput7', 20
union all select 1, 'AnalogueInput8', 21
union all select 1, 'LiquidLevelLiters', 22
union all select 1, 'LiquidLevelPercentage', 23
union all select 1, 'LiquidLevelNoUnit', 24

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Зажигание'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript = 'Ignition'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'HDOP'
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript like 'Hdop'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'Аналоговый датчик ' + REPLACE(cs.Descript, 'AnalogueInput', '')
	where ct.CONTROLLER_TYPE_ID = @CONTROLLER_TYPE_ID
	  and cs.Descript like 'AnalogueInput%'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)
