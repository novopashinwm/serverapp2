if not exists (select * from sys.columns where object_id=object_id('CompoundRule_Message_Template') and name='ToOwnEmail')
	alter table CompoundRule_Message_Template add ToOwnEmail bit not null constraint DF_CompoundRule_Message_Template_ToOwnEmail default(0)
go
if not exists (select * from sys.columns where object_id=object_id('CompoundRule_Message_Template') and name='ToOwnPhone')
begin
	exec sp_rename 'CompoundRule_Message_Template.ToMaskedPhone', 'ToOwnPhone'
	exec sp_rename 'DF_CompoundRule_Message_Template_ToMaskedPhone', 'DF_CompoundRule_Message_Template_ToOwnPhone'
end
go

if not exists (select * from sys.columns where object_id=object_id('H_CompoundRule_Message_Template') and name='ToOwnEmail')
	alter table H_CompoundRule_Message_Template add ToOwnEmail bit not null constraint DF_H_CompoundRule_Message_Template_ToOwnEmail default(0)
go
if not exists (select * from sys.columns where object_id=object_id('H_CompoundRule_Message_Template') and name='ToOwnPhone')
begin
	exec sp_rename 'H_CompoundRule_Message_Template.ToMaskedPhone', 'ToOwnPhone'
	exec sp_rename 'DF_H_CompoundRule_Message_Template_ToMaskedPhone', 'DF_H_CompoundRule_Message_Template_ToOwnPhone'
end
go

-- ������� ��� �������, ������� �� �������� ������ �� ���������
delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_DataAbsence d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d 
from CompoundRule r left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Log d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d 
from CompoundRule r left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Operator_Contact d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_PositionAbsence d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Sensor d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Sensor d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Speed d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_SubscriptionEmail d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_SubscriptionPhone d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Time d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Vehicle d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_VehicleGroup d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_Zone d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null

delete d
from CompoundRule r	left join CompoundRule_Message_Template t on r.CompoundRule_ID = t.CompoundRule_ID
	join CompoundRule_ZoneGroup d on d.CompoundRule_ID = r.CompoundRule_ID
where t.CompoundRule_ID is null
