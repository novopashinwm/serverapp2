if not exists (select * from sys.indexes where name = 'IX_OPERATOR_NAME')
	create nonclustered index IX_OPERATOR_NAME on operator(NAME)
go