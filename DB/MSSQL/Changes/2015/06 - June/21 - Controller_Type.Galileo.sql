declare @type_name varchar(255) = 'Galileo'

insert into CONTROLLER_TYPE (TYPE_NAME, DeviceIdIsImei, AllowedToAddByCustomer)
	select @type_name, 1, 1
	where not exists (select * from CONTROLLER_TYPE e where e.TYPE_NAME = @type_name)

declare @sensor table(digital bit, name varchar(255), number int)

set nocount on

insert into @sensor
          select 1, 'VibrationMotion', 0
union all select 1, 'AngulationTooHigh', 1
union all select 1, 'TrustedIButtonConnected', 2
union all select 1, 'SimIsAttached', 3
union all select 1, 'InZone', 4
union all select 1, 'LowIntervalPowerSourceVoltage', 5
union all select 1, 'GpsAntennaDisconnected', 6
union all select 1, 'AbnormalInternalPowerVoltage', 7
union all select 1, 'AbnormalOuterPowerVoltage', 8
union all select 1, 'Engine', 9
union all select 1, 'Impact', 10
union all select 1, 'Glonass', 11
union all select 1, 'SignalLevelQuality', 12
union all select 1, 'SignallingMode', 14
union all select 1, 'Alarm', 15
union all select 0, 'PowerVoltage', 16
union all select 0, 'BatteryVoltage', 17
union all select 0, 'DeviceTemperature', 18
union all select 0, 'AccelerationX', 19
union all select 0, 'AccelerationY', 20
union all select 0, 'AccelerationZ', 21
union all select 0, 'OutputStatus0', 30
union all select 0, 'OutputStatus1', 31
union all select 0, 'OutputStatus2', 32
union all select 0, 'OutputStatus3', 33
union all select 0, 'OutputStatus4', 34
union all select 0, 'OutputStatus5', 35
union all select 0, 'OutputStatus6', 36
union all select 0, 'OutputStatus7', 37
union all select 0, 'OutputStatus8', 38
union all select 0, 'OutputStatus9', 39
union all select 0, 'OutputStatus10', 40
union all select 0, 'OutputStatus11', 41
union all select 0, 'OutputStatus12', 42
union all select 0, 'OutputStatus13', 43
union all select 0, 'OutputStatus14', 44
union all select 0, 'OutputStatus15', 45
union all select 0, 'InputStatus0', 50
union all select 0, 'InputStatus1', 51
union all select 0, 'InputStatus2', 52
union all select 0, 'InputStatus3', 53
union all select 0, 'InputStatus4', 54
union all select 0, 'InputStatus5', 55
union all select 0, 'InputStatus6', 56
union all select 0, 'InputStatus7', 57
union all select 0, 'InputStatus8', 58
union all select 0, 'InputStatus9', 59
union all select 0, 'InputStatus10', 60
union all select 0, 'InputStatus11', 61
union all select 0, 'InputStatus12', 62
union all select 0, 'InputStatus13', 63
union all select 0, 'InputStatus14', 64
union all select 0, 'InputStatus15', 65
union all select 0, 'AnalogInput0', 70
union all select 0, 'AnalogInput1', 71
union all select 0, 'AnalogInput2', 72
union all select 0, 'AnalogInput3', 73
union all select 0, 'AnalogInput4', 74
union all select 0, 'AnalogInput5', 75
union all select 0, 'AnalogInput6', 76
union all select 0, 'AnalogInput7', 77
union all select 0, 'Temperature0', 80
union all select 0, 'Temperature1', 81
union all select 0, 'Temperature2', 82
union all select 0, 'Temperature3', 83
union all select 0, 'Temperature4', 84
union all select 0, 'Temperature5', 85
union all select 0, 'Temperature6', 86
union all select 0, 'Temperature7', 87
union all select 0, 'IButton1ID', 91
union all select 0, 'IButton2ID', 92
union all select 0, 'TotalFuelConsumption', 93
union all select 0, 'FuelLevel', 94
union all select 0, 'CoolantT', 95
union all select 0, 'Revs', 96
union all select 0, 'CanRunMeters', 97
union all select 0, 'GpsRunMeters', 98
union all select 0, 'Can16Bitr0', 100
union all select 0, 'Can16Bitr1', 101
union all select 0, 'Can16Bitr2', 102
union all select 0, 'Can16Bitr3', 103
union all select 0, 'Can16Bitr4', 104
union all select 0, 'Can32Bitr0', 110
union all select 0, 'Can32Bitr1', 111
union all select 0, 'Can32Bitr2', 112
union all select 0, 'Can32Bitr3', 113
union all select 0, 'RS485LLS0', 120
union all select 0, 'RS485LLS1', 121
union all select 0, 'RS485LLS2', 122
union all select 0, 'RS485LLS3', 123
union all select 0, 'RS485LLS4', 124
union all select 0, 'RS485LLS5', 125
union all select 0, 'RS485LLS6', 126
union all select 0, 'RS485LLS7', 127
union all select 0, 'RS485LLS8', 128
union all select 0, 'RS485LLS9', 129
union all select 0, 'RS485LLS10', 130
union all select 0, 'RS485LLS11', 131
union all select 0, 'RS485LLS12', 132
union all select 0, 'RS485LLS13', 133
union all select 0, 'RS485LLS14', 134
union all select 0, 'RS485LLS15', 135    
union all select 0, 'RS485LTS0', 140
union all select 0, 'RS485LTS1', 141
union all select 0, 'RS485LTS2', 142
union all select 0, 'RS485LTS3', 143
union all select 0, 'RS485LTS4', 144
union all select 0, 'RS485LTS5', 145
union all select 0, 'RS485LTS6', 146
union all select 0, 'RS485LTS7', 147
union all select 0, 'RS485LTS8', 148
union all select 0, 'RS485LTS9', 149
union all select 0, 'RS485LTS10', 150
union all select 0, 'RS485LTS11', 151
union all select 0, 'RS485LTS12', 152
union all select 0, 'RS485LTS13', 153
union all select 0, 'RS485LTS14', 154
union all select 0, 'RS485LTS15', 155
union all select 0, 'EcoDriveAcceleration', 160
union all select 0, 'EcoDriveBreaking', 161
union all select 0, 'EcoDriveTurningAcceleration', 162
union all select 0, 'EcoDriveBumpImpact', 163
union all select 0, 'HDOP', 164

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.Type_Name = @type_name
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
	     
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '�������'
	where ct.Type_Name = @type_name
	  and cs.Descript = 'Alarm'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 0.1
	  , Default_Constant = 0
	  , Mandatory = 0
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'HDOP'
	where ct.Type_Name = @type_name
	  and cs.Descript = 'HDOP'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 0)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = '���������� �������'
	where ct.Type_Name = @type_name
	  and cs.Descript = 'PowerVoltage'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 1)
		
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'CAN.Fuel'
	where ct.Type_Name = @type_name
	  and cs.Descript = 'FuelLevel'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 1)

update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID
	  , Default_Multiplier = 1
	  , Default_Constant = 0
	  , Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Sensor_Legend legend on legend.Name = 'CAN_Revs'
	where ct.Type_Name = @type_name
	  and cs.Descript = 'Revs'
	  and (cs.Default_Sensor_Legend_ID is null 
	    or cs.Mandatory = 1)

select *
from CONTROLLER_SENSOR s
where s.CONTROLLER_TYPE_ID = (select CONTROLLER_TYPE_ID from CONTROLLER_TYPE t where t.TYPE_NAME = 'Galileo') 

select *
from Controller_sensor_legend
order by NAME