if not exists (select * from sys.indexes where name = 'IX_DEPARTMENT_ExtID')
	create nonclustered index IX_DEPARTMENT_EXTID on department(extId)
go
if not exists (select * from sys.indexes where name = 'IX_Asid_Department_ID')
	create nonclustered index IX_Asid_Department_ID on asid(department_id) include(operator_id, contact_id)
go