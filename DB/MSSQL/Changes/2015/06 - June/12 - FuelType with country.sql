update Fuel_Type set Tank_Unit_Id = (select id from Unit where Name = 'Litre'), Unit_Id = (select id from Unit where Name = 'LitrePer100Km')

update t set 
	t.Unit_Id = (select id from Unit where Name = 'KmPerLitre')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Litre')
from Fuel_Type t
	join Country c on t.Country_ID = c.Country_ID
where c.Name = 'India'

update t set 
	t.Unit_Id = (select id from Unit where Name = 'LitrePer100Km')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Litre')
from Fuel_Type t
	join Country c on t.Country_ID = c.Country_ID
where c.Name = 'Russia'

update t set 
	t.Unit_Id = (select id from Unit where Name = 'KmPerM3')
	,t.Tank_Unit_id = (select id from Unit where Name = 'Cbm')
from Fuel_Type t
where t.Name in ('Пропан','Метан')


select *
from Fuel_Type t
	join Unit u1 on u1.Id = t.Unit_Id
	join Unit u2 on u2.Id = t.Tank_Unit_Id
	left join Country c on t.Country_ID = c.Country_ID

update Vehicle set FuelSpendStandardKilometer = 100, 
				FuelSpendStandardLiter = FuelSpendStandardLiter / 100.0 
where FuelSpendStandardKilometer = 10000