if not exists (
	select *
		from sys.indexes 
		where name = 'IX_OperatorGroup_Operator_Operator_ID'
)
begin

	create nonclustered index IX_OperatorGroup_Operator_Operator_ID
		on OperatorGroup_Operator(Operator_ID)

end