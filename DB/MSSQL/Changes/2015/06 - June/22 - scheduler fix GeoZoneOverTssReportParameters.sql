declare cur cursor for
select queueId, config
from 
(
	select q.SCHEDULERQUEUE_ID queueId, convert(xml, q.CONFIG_XML) config
	from SCHEDULERQUEUE q 
		join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID = e.SCHEDULEREVENT_ID
		join REPORT r on r.REPORT_ID = e.Report_Id
	where r.REPORT_GUID = '6E2DE7AD-2656-4812-BC1C-0EEA32613662'
) cte
where config.exist('
	declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
	//_zoneIds[@xsi:null=1]
') = 1 and config.exist('
	declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";
	declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
	//SOAP-ENC:Array[contains(@SOAP-ENC:arrayType,"xsd:anyType")]/item[@xsi:type="xsd:int"]
') = 1

declare @queueId int
declare @config xml

open cur
fetch next from cur
into @queueId, @config
while @@fetch_status = 0
begin
	print 'modified'

	declare @id nvarchar(50)
	declare @refId nvarchar(50)
	declare @arrType nvarchar(50)

	select 
		@id = a.n.value('declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"; 
			(//SOAP-ENC:Array/@id)[1]', 'nvarchar(50)'),
		@arrType = a.n.value('declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"; 
			(//SOAP-ENC:Array/@SOAP-ENC:arrayType)[1]', 'nvarchar(50)')
	from
	(
		select t.n.query('.') n
		from @config.nodes('
			declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";
			declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
			//SOAP-ENC:Array[contains(@SOAP-ENC:arrayType,"xsd:anyType")]
		') t(n)
	) a(n)
	where a.n.exist('
		declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";
		declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
		//SOAP-ENC:Array[contains(@SOAP-ENC:arrayType,"xsd:anyType")]/item[@xsi:type="xsd:int"]
	') = 1

	set @refId = '#' + @id
	set @arrType = replace(@arrType, 'anyType', 'int')
	set @config.modify('declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";delete //_zoneIds/@xsi:null')
	set @config.modify('declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";insert attribute href {sql:variable("@refId")} into (//_zoneIds)[1]')
	set @config.modify('declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";replace value of (//SOAP-ENC:Array/@SOAP-ENC:arrayType)[1] with sql:variable("@arrType")')
	set @config.modify('declare namespace a9="http://schemas.microsoft.com/clr/ns/System.Collections";delete //a9:ArrayList')
	set @config.modify('declare namespace SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/";	delete //SOAP-ENC:Array[@id != sql:variable("@id")]')

	update SCHEDULERQUEUE set CONFIG_XML = convert(nvarchar(max), @config) where SCHEDULERQUEUE_ID = @queueId

	fetch next from cur
	into @queueId, @config
end

CLOSE cur;
DEALLOCATE cur;