insert into ContactType (Id, Name)
	select t.Id, t.Name
		from (select Id = 9, Name = 'Text') t
		where not exists (Select * from ContactType e where e.Id = t.Id)