if not exists (
	select * 
		from sys.columns
		where object_id = object_id('Asid')
		  and name = 'AllowsCdrPayment'
)
begin

	alter table Asid
		add AllowsCdrPayment bit not null 
			constraint DF_Asid_AllowsCdrPayment
				default(1)

	alter table H_Asid
		add AllowsCdrPayment bit not null 
			constraint DF_H_Asid_AllowsCdrPayment
				default(1)

end
