declare @template_name nvarchar(50) = 'DataUpdate'
declare @dataType nvarchar(255) = 'DataType'
declare @visible nvarchar(255) = 'visible'

insert into MESSAGE_TEMPLATE (Name)
	select @template_name
		where not exists (select 1 from Message_Template e where e.Name = @template_name)

insert into MESSAGE_TEMPLATE_FIELD (MESSAGE_TEMPLATE_ID, DOTNET_TYPE_ID, NAME)
	select mt.MESSAGE_TEMPLATE_ID, dt.DotNet_Type_ID, @dataType
		from MESSAGE_TEMPLATE mt, DOTNET_TYPE dt
		where mt.NAME = @template_name
		  and dt.TYPE_NAME = 'System.String'
		  and not exists (select 1 from Message_Template_Field e where e.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID and e.NAME = @dataType)

insert into MESSAGE_TEMPLATE_FIELD (MESSAGE_TEMPLATE_ID, DOTNET_TYPE_ID, NAME)
	select mt.MESSAGE_TEMPLATE_ID, dt.DotNet_Type_ID, @visible
		from MESSAGE_TEMPLATE mt, DOTNET_TYPE dt
		where mt.NAME = @template_name
		  and dt.TYPE_NAME = 'System.Boolean'
		  and not exists (select 1 from Message_Template_Field e where e.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID and e.NAME = @visible)
