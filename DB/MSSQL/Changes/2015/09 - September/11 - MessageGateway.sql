if not exists (
	select *
		from sys.tables 
		where name = 'MessageGateway'
)
begin

	create table MessageGateway
	(
		ID int not null 
			constraint PK_MessageGateway primary key clustered,
		Name varchar(255) not null 
			constraint UQ_MessageGateway_Name unique,
		Enabled bit not null
			constraint DF_MessageGateway_Enabled default(0),
		ContactType_Id int not null
			constraint FK_MessageGateway_ContactType_Id foreign key references ContactType(Id),
		ContactRegex varchar(255)
	)

	create table MessageGateway_Template
	(
		Gateway_ID int not null
			constraint FK_MessageGateway_Template_Gateway_ID
				foreign key references MessageGateway(ID),
		Template_ID int not null
			constraint FK_MessageGateway_Template_Template_ID
				foreign key references Message_Template(Message_Template_ID),
		constraint PK_MessageGateway_Template primary key clustered (Gateway_ID, Template_ID)
	)

end
