if not exists (
	select *
		from sys.tables 
		where name = 'TrackingSchedule'
)
begin

	create table TrackingSchedule
	(
		ID int not null identity(1,1)
			constraint PK_TrackingSchedule
				primary key clustered,
		Enabled bit not null 
			constraint DF_TrackingSchedule_Enabled
				default(0),
		Operator_ID int not null
			constraint FK_TrackingSchedule_Operator_ID
				foreign key references Operator(Operator_ID),
		Vehicle_ID int not null
			constraint FK_TrackingSchedule_Vehicle_ID
				foreign key references Vehicle(Vehicle_ID),
		Config xml not null
	)

	create nonclustered index IX_TrackingSchedule_Operator_ID 
		on TrackingSchedule(Operator_ID)

	create nonclustered index IX_TrackingSchedule_Vehicle_ID_Enabled
		on TrackingSchedule(Vehicle_ID, Enabled)

	create table H_TrackingSchedule
	(
		ID int not null,
		Enabled bit not null,
		Operator_ID int not null,
		Vehicle_ID int not null,
		Config xml not null,

		H_TrackingSchedule_ID int identity(1,1)
			constraint PK_H_TrackingSchedule
				primary key clustered,
		
        TRAIL_ID        int 
			constraint FK_H_TrackingSchedule_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

end
