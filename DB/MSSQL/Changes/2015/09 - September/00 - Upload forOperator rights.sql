
insert into RIGHT_OPERATOR (RIGHT_ID, OPERATOR_ID, ALLOWED)	
	select r.RIGHT_ID, o.OPERATOR_ID, 1
	from [right] r, operator o
	where 1=1
		and r.forOperator = 1
		and not exists (
			select 1
				from RIGHT_OPERATOR ro
				where ro.OPERATOR_ID = o.OPERATOR_ID
			      and ro.RIGHT_ID = r.RIGHT_ID)
		and not exists (
			select 1
				from v_operator_rights vor
				where vor.OPERATOR_ID = o.OPERATOR_ID
			      and vor.RIGHT_ID = r.RIGHT_ID)
