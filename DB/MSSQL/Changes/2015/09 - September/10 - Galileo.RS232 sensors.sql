declare @type_name varchar(255) = 'Galileo'

insert into CONTROLLER_TYPE (TYPE_NAME, DeviceIdIsImei, AllowedToAddByCustomer)
	select @type_name, 1, 1
	where not exists (select * from CONTROLLER_TYPE e where e.TYPE_NAME = @type_name)

declare @sensor table(digital bit, name varchar(255), number int)

set nocount on

insert into @sensor
		  select 0, 'RS232_0', 165
union all select 0, 'RS232_1', 166

set nocount off

insert into Controller_Sensor (Controller_Type_ID, Controller_Sensor_Type_ID, Number, Descript)
	select ct.Controller_Type_ID
	     , case s.digital when 1 then 2 else 1 end
	     , s.number
	     , s.name
		from @sensor s
	    join Controller_Type ct on ct.Type_Name = @type_name
	    where not exists (
			select * 
				from Controller_Sensor e
				where e.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
				  and e.NUMBER = s.number)
