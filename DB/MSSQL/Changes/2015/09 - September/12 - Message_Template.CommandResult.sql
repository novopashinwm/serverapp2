declare @template_name nvarchar(50) = 'CommandResult'

insert into MESSAGE_TEMPLATE (Name)
	select @template_name
		where not exists (select 1 from Message_Template e where e.Name = @template_name)

go

declare @template_name nvarchar(50) = 'UserAccount'

insert into MESSAGE_TEMPLATE (Name)
	select @template_name
		where not exists (select 1 from Message_Template e where e.Name = @template_name)
