update oc
	set Removable = 1
	from Operator_Contact oc
	join v_operator_department_right odr on odr.operator_id = oc.Operator_ID 
										and odr.right_id = 104 /*DepartmentAccess*/
	join DEPARTMENT d on d.DEPARTMENT_ID = odr.department_id
	where oc.Confirmed = 1
	  and oc.Removable = 0
	  and isnull(d.Type, 0) = 0 /*Corporate*/
