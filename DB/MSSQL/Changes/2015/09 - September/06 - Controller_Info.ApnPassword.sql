if not exists (
	select *
		from sys.columns
		where object_id = object_id('Controller_Info')
		  and name = 'ApnPassword'
)
begin

	alter table Controller_Info
		add ApnPassword nvarchar(16)

	alter table H_Controller_Info
		add ApnPassword nvarchar(16)

end