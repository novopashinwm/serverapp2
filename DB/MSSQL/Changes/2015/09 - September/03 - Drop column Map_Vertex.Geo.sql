if exists (select * from sys.columns where object_id = object_id('map_vertex') and name = 'geo')
begin
	drop index IX_Map_Vertex_Geo on Map_Vertex
	alter table map_vertex drop column geo
	alter table h_map_vertex drop column geo
end