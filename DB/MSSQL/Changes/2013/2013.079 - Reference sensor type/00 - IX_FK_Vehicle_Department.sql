if not exists (select * from sys.indexes where name = 'IX_FK_Vehicle_Department')
begin

	create nonclustered index IX_FK_Vehicle_Department on Vehicle (Department)

end