if not exists (
	select * from sys.columns where object_id = object_id('Asid') and name = 'SimID'
)
begin

	alter table Asid
		add SimID varchar(32)
		
	alter table H_Asid
		add SimID varchar(32)
	
end