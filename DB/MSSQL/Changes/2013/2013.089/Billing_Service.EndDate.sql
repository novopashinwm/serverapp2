if not exists (
	select * from sys.columns where object_id = object_id('Billing_Service') and name = 'EndDate'
)
begin

	alter table Billing_Service
		add EndDate datetime
		
	alter table H_Billing_Service
		add EndDate datetime


end