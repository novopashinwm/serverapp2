if not exists (
	select * from sys.indexes where name = 'IX_Vehicle_Rule_Processing_Host_ID_LastTime'
)
begin

	create nonclustered index IX_Vehicle_Rule_Processing_Host_ID_LastTime on Vehicle_Rule_Processing(Host_ID, LastTime)
		
end