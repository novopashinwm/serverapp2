if not exists (
	select * from sys.indexes where name = 'IX_CompoundRule_Vehicle_Vehicle_ID'
)
begin

	create nonclustered index IX_CompoundRule_Vehicle_Vehicle_ID on CompoundRule_Vehicle(Vehicle_ID) include (CompoundRule_ID)

end