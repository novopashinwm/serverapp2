if not exists (
	select * from sys.indexes where name = 'IX_CompoundRule_VehicleGroup_VehicleGroup_ID'
)
begin

	create nonclustered index IX_CompoundRule_VehicleGroup_VehicleGroup_ID 
		on CompoundRule_VehicleGroup(VehicleGroup_ID) 
		include (CompoundRule_ID)
		
end