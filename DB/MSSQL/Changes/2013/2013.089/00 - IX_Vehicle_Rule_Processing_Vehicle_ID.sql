if not exists (
	select * from sys.indexes where name = 'IX_Vehicle_Rule_Processing_Vehicle_ID'
)
begin

	create nonclustered index IX_Vehicle_Rule_Processing_Vehicle_ID on Vehicle_Rule_Processing(Vehicle_ID)
		
end