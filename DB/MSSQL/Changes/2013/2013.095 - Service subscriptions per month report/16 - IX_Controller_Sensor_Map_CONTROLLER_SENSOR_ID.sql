if not exists (
	select * 
		from sys.indexes 
		where name = 'IX_Controller_Sensor_Map_CONTROLLER_SENSOR_ID'
)
begin

	CREATE NONCLUSTERED INDEX IX_Controller_Sensor_Map_CONTROLLER_SENSOR_ID
		ON [dbo].[CONTROLLER_SENSOR_MAP] ([CONTROLLER_SENSOR_ID])
		INCLUDE ([CONTROLLER_ID])

end