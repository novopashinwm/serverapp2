/*��������� ��� ������� Avtofon
	����������� ������� �������
*/

declare @type_name varchar(100) = 'Avtofon'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 2, 1, 1, 0, 0, 1, 'ExternalPower');
insert into @controller_sensor values (@controller_type_id, 2, 2, 1, 0, 0, 1, 'AlarmInput');
insert into @controller_sensor values (@controller_type_id, 2, 3, 1, 0, 0, 1, 'AlarmButton');
insert into @controller_sensor values (@controller_type_id, 2, 4, 1, 0, 0, 1, 'Motion');

insert into @controller_sensor values (@controller_type_id, 1, 5, 12750,   0, 0, 32, 'PowerVoltage'		);
insert into @controller_sensor values (@controller_type_id, 1, 6,   127, -99, 0, 32, 'Temperature'		);


set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

