if not exists (
	select * 
		from sys.columns 
		where OBJECT_ID = OBJECT_ID('department')
	      and Name = 'IsCommercial'
)
begin 

	alter table Department
		add IsCommercial bit not null
			constraint DF_Department_IsCommercial default(1)
		
	alter table H_Department 
		add IsCommercial bit not null
			constraint DF_H_Department_IsCommercial default(1)
		
end