if not exists (
	select * 
		from sys.objects
		where name = 'Operator_OperatorGroup'
)
begin

	create table Operator_OperatorGroup
	(
		ID int identity(1,1)
			constraint PK_Operator_OperatorGroup
				primary key nonclustered,
		Operator_ID int not null
			constraint FK_Operator_OperatorGroup_Operator_ID
				foreign key references Operator(Operator_ID),
		OperatorGroup_ID int not null
			constraint FK_Operator_OperatorGroup_OperatorGroup_ID
				foreign key references OperatorGroup(OperatorGroup_ID),
		Right_ID int not null
			constraint FK_Operator_OperatorGroup_Right_ID
				foreign key references [Right](Right_ID),
		Allowed bit not null
	)
	
	create clustered index IX_Operator_OperatorGroup
		on Operator_OperatorGroup(Operator_ID, Right_ID, OperatorGroup_ID)
		
	alter table Operator_OperatorGroup
		add constraint UQ_Operator_OperatorGroup unique(Operator_ID, Right_ID, OperatorGroup_ID)

	create table H_Operator_OperatorGroup
	(
		ID int not null,
		Operator_ID int not null,
		OperatorGroup_ID int not null,
		Right_ID int not null,
		Allowed bit not null,
		
		H_Operator_OperatorGroup_ID int identity(1,1)
			constraint PK_H_Operator_OperatorGroup
				primary key clustered,	
        TRAIL_ID        int 
			constraint FK_H_Operator_OperatorGroup_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)		
			
end