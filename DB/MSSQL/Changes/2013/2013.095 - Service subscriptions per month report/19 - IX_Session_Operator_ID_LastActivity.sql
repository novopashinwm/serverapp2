if not exists (
	select * 
		from sys.indexes where name = 'IX_Session_Operator_ID_LastActivity'
)
begin

	create nonclustered index IX_Session_Operator_ID_LastActivity
		on Session(Operator_ID, LastActivity)

end