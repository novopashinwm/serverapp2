if exists (
	select * from sys.views where name = 'v_controller_sensor_value_change'
)
	drop view v_controller_sensor_value_change
go

create view v_controller_sensor_value_change
as
select l.Vehicle_ID,
	   l.Number,
	   Prev_Log_Time = p.Log_Time, 
	   Prev_Value = p.Value,
	   l.Log_Time, 
	   l.Value	   
	from Controller_Sensor_Log l with (nolock)
	cross apply (
		select top(1) p.Log_Time, p.Value
			from Controller_Sensor_Log p with (nolock)
			where p.Vehicle_ID = l.Vehicle_ID
			  and p.Log_Time < l.Log_Time
			  and p.Number = l.Number
			order by p.Log_Time desc) p
	where p.Value <> l.Value
union all
	select cs.VEHICLE_ID
	     , cs.NUMBER
	     , Prev_Log_Time = null
	     , Prev_Value = null
	     , l.Log_Time
	     , l.Value
	from (
		select distinct csm.Vehicle_ID, Number = csm.Sensor_Number
			from v_controller_sensor_map csm with (noexpand)
	) cs
	cross apply (
		select top(1) l.Log_Time, l.Value
			from Controller_Sensor_Log l with (nolock)
			where l.Vehicle_ID = cs.VEHICLE_ID
			  and l.Number = cs.NUMBER
			order by l.Log_Time asc) l
