/*��������� ��� ����������� DeerCollar
	����������� ������� ����������� (������� ������� � ����� ���������������)
*/
begin tran

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'DeerCollar')

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	Default_Legend				nvarchar(100),
	Default_Multiplier			numeric(3,1),
	Descript					nvarchar(512));

set nocount on
insert into @controller_sensor values (@controller_type_id, 1,  4, '�������� ������ �����������',  1, 'Temperature');             
set nocount off

print 'Inserting new sensors: '
insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Mandatory,
	Descript)
select 
	t.CONTROLLER_TYPE_ID,
	t.CONTROLLER_SENSOR_TYPE_ID,
	t.Number,
	legend.Controller_Sensor_Legend_ID,
	t.Default_Multiplier,
	Mandatory = case when legend.Controller_Sensor_Legend_ID is not null then 1 else 0 end,
	t.Descript
from @controller_sensor t
left outer join controller_Sensor_legend legend on legend.name = t.Default_Legend
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

commit