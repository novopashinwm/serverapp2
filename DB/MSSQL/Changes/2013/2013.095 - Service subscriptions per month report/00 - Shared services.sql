if not exists (
	select *
		from sys.columns  
		where object_id = object_id('Billing_Service_Type')
		  and name = 'Shared'
)
begin

	alter table Billing_Service_Type
		add Shared bit not null
			constraint DF_Billing_Service_Type_Shared 
				default(0)

end

go

update bst
	set Shared = 1
	from Billing_Service_Type bst
	where bst.Service_Type in ('FRNIKA.SMS.M2M', 
							   'FRNIKA.SMS.LBS', 
							   'FRNIKA.MLP.ChargingEconom',
							   'FRNIKA.MLP.ChargingNolimits',
							   'FRNIKA.MLP.ChargingNolimits.Trial',
							   'FRNIKA.SMS.Trial')
	   and Shared = 0
