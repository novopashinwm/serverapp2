update Billing_Service_Type
	set LBS = 1,
	    MinIntervalSeconds = 1200	    
	where service_type_category = 'NikaTracker'
	  and (LBS = 0 or MinIntervalSeconds is null)