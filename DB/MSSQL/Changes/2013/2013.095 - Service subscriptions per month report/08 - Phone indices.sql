if not exists (
	select * from sys.indexes where name = 'IX_Phone_Phone'
)
begin

	create nonclustered index IX_Phone_Phone on Phone (Phone, Confirmed, Operator_ID)
	
end
go

if not exists (
	select * from sys.indexes where name = 'IX_Phone_Operator_ID'
)
begin

	create nonclustered index IX_Phone_Operator_ID on Phone (Operator_ID, Confirmed, Phone)
	
end
