IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_vehicle_groups_right]'))
DROP VIEW [dbo].[v_operator_vehicle_groups_right]
GO

/*
select * from [v_operator_vehicle_groups_right]
where operator_id = 4
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_operator_vehicle_groups_right] 
as
	WITH allRights(operator_id, vehiclegroup_id, right_id, allowed, priority) AS 
	(
		select o_vg.operator_id, o_vg.vehiclegroup_id, o_vg.right_id, allowed, 1 'priority'
			from dbo.operator_vehiclegroup o_vg
		union all
		select og_o.operator_id, og_vg.vehiclegroup_id, og_vg.right_id, allowed, 2 'priority'
			from dbo.OPERATORGROUP_VEHICLEGROUP og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
		union all
		select o_d.operator_id, vg.VEHICLEGROUP_ID, r.right_id, 1, 3 'priority'
			from dbo.OPERATOR_DEPARTMENT o_d
			join dbo.VEHICLEGROUP vg on vg.Department_ID = o_d.department_id
			join dbo.[Right] r on r.Right_ID in (102, 109, 119)
			where o_d.right_id = 2 /*SecurityAdministrator*/
			  and o_d.allowed = 1
		union all
		select og_o.operator_id, vg.vehiclegroup_id, r.right_id, 1, 4 'priority'
			from OperatorGroup_Operator og_o
			join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
			join dbo.VEHICLEGROUP vg on vg.Department_ID = og_d.department_id
			join dbo.[Right] r on r.Right_ID in (102, 109, 119)
			where og_d.right_id = 2 /*SecurityAdministrator*/
			  and og_d.allowed = 1		
	)
	select distinct operator_id, vehiclegroup_id, right_id, r1.priority
	from allRights r1
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.vehiclegroup_id = r1.vehiclegroup_id
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority < r1.priority)