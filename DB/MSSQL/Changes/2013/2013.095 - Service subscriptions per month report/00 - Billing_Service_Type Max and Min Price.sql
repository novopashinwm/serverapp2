if not exists (
	select * 
		from sys.columns
		where object_id = OBJECT_ID('Billing_Service_Type')
		  and name = 'MinPrice'
)
begin

	alter table Billing_Service_Type
		add MinPrice numeric(12,2)

end
go

if not exists (
	select * 
		from sys.columns
		where object_id = OBJECT_ID('Billing_Service_Type')
		  and name = 'MaxPrice'
)
begin

	alter table Billing_Service_Type
		add MaxPrice numeric(12,2)

end
go

insert into Billing_Service_Type (
			 Service_Type,   Service_Type_Category,     Controller_Type_ID,     Vehicle_Kind_ID, LimitedQuantity,   MaxQuantity, Singleton,   Name, SMS, PeriodDays, Shared)
	select t.Service_Type, t.Service_Type_Category, bst.Controller_Type_ID, bst.Vehicle_Kind_ID,               1, t.MaxQuantity,		 0, t.Name,   1,         30,      1
	from (
	select Service_Type = '', Service_Type_Category = '', LimitedQuantity = 1, MaxQuantity = 500, Name = N'', SMS = 1, PeriodDays = 30 where 1=0
	union all	select 'FRNIKA.SMS.M2M', 'SMS', 1, 500,		'SMS ����-������� 500',		1, 30
	union all	select 'FRNIKA.SMS.M2M', 'SMS', 1, 10000,	'SMS ����-������� 10000',	1, 30
	union all	select 'FRNIKA.SMS.M2M', 'SMS', 1, 50000,	'SMS ����-������� 50000',	1, 30
	union all	select 'FRNIKA.SMS.LBS', 'SMS', 1, 500,		'SMS ���� LBS 500',			1, 30
	union all	select 'FRNIKA.SMS.LBS', 'SMS', 1, 10000,	'SMS ���� LBS 10000',		1, 30
	union all	select 'FRNIKA.SMS.LBS', 'SMS', 1, 50000,	'SMS ���� LBS 50000',		1, 30
	) t
	join Billing_Service_Type bst on bst.Service_Type = 'FRNIKA.MLP'
	where not exists (
		select * from Billing_Service_Type e where e.Service_Type = t.Service_Type and e.MaxQuantity = t.MaxQuantity)

update bs
	set bs.Billing_Service_Type_ID = bstnew.ID
	from Billing_Service bs
	join Billing_Service_Type bstold on bstold.ID = bs.Billing_Service_Type_ID
	join Billing_Service_Type bstnew on bstnew.Service_Type = bstold.Service_Type and bstnew.MaxQuantity = bs.MaxQuantity
	where bstnew.ID <> bstold.ID

update bs
	set bs.Billing_Service_Type_ID = bstnew.ID
	from H_Billing_Service bs
	join Billing_Service_Type bstold on bstold.ID = bs.Billing_Service_Type_ID
	join Billing_Service_Type bstnew on bstnew.Service_Type = bstold.Service_Type and bstnew.MaxQuantity = bs.MaxQuantity
	where bstnew.ID <> bstold.ID

--TODO: �������� ����� ��������� ���
update bst
	set MinPrice = t.MinPrice,
	    MaxPrice = t.MaxPrice
	from (
		select Name = N'', Quantity = CONVERT(int, null), MinPrice = CONVERT(numeric(12,2), 0), MaxPrice = CONVERT(numeric(12,2), 0) where 1=0
		union all select '���� LBS ������'			,   40, 	60		, 120
		union all select '���� LBS ��������'		, null, 	195		, 390
		union all select '����-������'				, null, 	60		, 195
		union all select '����-�������'				, null, 	350		, 700
		union all select '���� LBS ������ 500'		, 500, 		1250	, 1250
		union all select '���� LBS ������ 10000'	, 10000, 	13000	, 13000
		union all select '���� LBS ������ 50000'	, 50000, 	55000	, 55000
		union all select 'SMS ���� LBS 500'			, 500, 		1250	, 1250
		union all select 'SMS ���� LBS 10000'		, 10000, 	13000	, 13000
		union all select 'SMS ���� LBS 50000'		, 50000, 	55000	, 55000
		union all select 'SMS ����-������� 500'		, 500, 		1250	, 1250
		union all select 'SMS ����-������� 10000'	, 10000, 	13000	, 13000
		union all select 'SMS ����-������� 50000'	, 50000, 	55000	, 55000
	) t
	left outer join Billing_Service_Type bst on bst.Name = t.Name and (bst.MaxQuantity is null and t.Quantity is null or bst.MaxQuantity = t.Quantity)
	where bst.MinPrice is null or bst.MaxPrice is null
	