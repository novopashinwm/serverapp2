insert into [Right] (right_id, system, name, description)
	select t.right_id, 1, 'Friendship', 'Relation from operator/group to department. Department admin may grant access to object but cannot change any attribute of the operator or delete him.'
		from (select right_id = 118) t
		where not exists (select * from [right] r where r.right_id = t.right_id)
go

insert into Operator_Department
	select o.Operator_ID, d.Department_ID, friendship.right_id, 1
		from Operator o 
		join Department d on d.Department_ID in (
			select v.Department
				from Vehicle v
				join v_operator_vehicle_right ovr on ovr.right_id = 102
												 and ovr.priority <= 2
												 and ovr.operator_id = o.operator_id
												 and ovr.vehicle_id = v.vehicle_id
												 and not exists (
													select * 
														from Asid a 
														join MLP_Controller mlpc on mlpc.Asid_ID = a.ID
														join Controller c on c.Controller_ID = mlpc.Controller_ID
														where a.Operator_ID = o.Operator_ID
														  and c.Vehicle_ID = v.Vehicle_ID))
		join [Right] friendship on friendship.Name = 'Friendship'
		where 1=1
		and not exists (
			select * from v_operator_department_right odr
				where odr.operator_id = o.operator_id
				  and odr.department_id = d.department_id
				  and odr.right_id in (104 /*DepartmentsAccess*/, friendship.right_id)
		)
		and not exists (
			select *
				from Operator_Department e
				where e.Operator_ID = o.Operator_ID 
				  and e.Department_ID = d.Department_ID
				  and e.Right_ID = friendship.right_id)
