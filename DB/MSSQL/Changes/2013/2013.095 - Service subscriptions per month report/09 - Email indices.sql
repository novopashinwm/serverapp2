if not exists (
	select * from sys.indexes where name = 'IX_Email_Email'
)
begin

	create nonclustered index IX_Email_Email on Email (Email, Confirmed, Operator_ID)
	
end
go

if not exists (
	select * from sys.indexes where name = 'IX_Email_Operator_ID'
)
begin

	create nonclustered index IX_Email_Operator_ID on Email (Operator_ID, Confirmed, Email)
	
end
