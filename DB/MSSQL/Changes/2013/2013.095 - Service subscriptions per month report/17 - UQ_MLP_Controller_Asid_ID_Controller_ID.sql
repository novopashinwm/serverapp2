delete victim
from 
(
	select c.Asid_ID
		from MLP_Controller c
		group by c.Asid_ID
		having COUNT(1) > 1
) dup
cross apply (
	select mlpc.Controller_ID, N = ROW_NUMBER() over (partition by mlpc.Asid_ID order by mlpc.ID desc)
		from MLP_Controller mlpc
		where mlpc.Asid_ID = dup.Asid_ID		
) v
join MLP_Controller victim on victim.Asid_ID = dup.Asid_ID and victim.Controller_ID = v.Controller_ID
where N > 1

go

if not exists (
	select * 
		from sys.objects 
		where name = 'UQ_MLP_Controller_Asid_ID_Controller_ID'
)
begin

	alter table MLP_Controller
		add constraint UQ_MLP_Controller_Asid_ID_Controller_ID unique (Asid_ID, Controller_ID)
		
end