if not exists (
	select *
		from sys.tables
		where name = 'Vehicle_Log_Compress'
)
begin

	create table Vehicle_Log_Compress
	(
		ID int identity(1,1)
			constraint PK_Vehicle_Log
				primary key nonclustered,
		Vehicle_ID int 
			constraint FK_Vehicle_Log_Compress_Vehicle_ID
				foreign key references Vehicle(Vehicle_ID),
		Log_Time_To int not null
	)
	
	create clustered index IX_Vehicle_Log_Compress_Vehicle_ID on Vehicle_Log_Compress(Vehicle_ID)

	create nonclustered index IX_Vehicle_Log_Compress_Log_Time on Vehicle_Log_Compress(Log_Time_To) include (Vehicle_ID)
	
end