update bst
	set PeriodDays = 14
	from Billing_Service_Type bst
	where bst.Service_Type = 'FRNIKA.MLP.ChargingNolimits.Trial'
	  and (bst.PeriodDays is null
	    or bst.PeriodDays <> 14)
