if not exists (
	select *
		from sys.columns
		where OBJECT_ID = OBJECT_ID('VEHICLEGROUP')
		  and name = 'Department_ID'
)
begin

	alter table VehicleGroup
		add Department_ID int 
			constraint FK_VehicleGroup_Department foreign key references Department(Department_ID)
			
	alter table H_VehicleGroup
		add Department_ID int

	create nonclustered index IX_VehicleGroup_Department_ID on VehicleGroup(Department_ID)

end
go

print 'Set Department_ID for VehicleGroup'
update vg
	set Department_ID = (
		select distinct Department_ID = isnull(v.Department, 0)
			from Vehicle v
			join VehicleGroup_Vehicle vgv on vgv.Vehicle_ID = v.Vehicle_ID
			where vgv.VehicleGroup_ID = vg.VehicleGroup_ID
			group by v.Department)
	from VehicleGroup vg
	where 1=1
	  and vg.Department_ID is null
	  and 1 = (select count(distinct isnull(v.Department, 0))
			from Vehicle v
			join VehicleGroup_Vehicle vgv on vgv.Vehicle_ID = v.Vehicle_ID
			where vgv.VehicleGroup_ID = vg.VehicleGroup_ID)
	  and 0 <> (select distinct Department_ID = isnull(v.Department, 0)
			from Vehicle v
			join VehicleGroup_Vehicle vgv on vgv.Vehicle_ID = v.Vehicle_ID
			where vgv.VehicleGroup_ID = vg.VehicleGroup_ID
			group by v.Department)
go

if not exists (
	select *
		from sys.columns
		where OBJECT_ID = OBJECT_ID('ZoneGroup')
		  and name = 'Department_ID'
)
begin

	alter table ZoneGroup
		add Department_ID int 
			constraint FK_ZoneGroup_Department foreign key references Department(Department_ID)
			
	alter table H_ZoneGroup
		add Department_ID int

	create nonclustered index IX_ZoneGroup_Department_ID on ZoneGroup(Department_ID)
end
go

print 'Set Department_ID for ZoneGroup'
;with ZoneGroup_Department 
as (
select zg.ZoneGroup_ID, d.Department_ID 
	from ZoneGroup zg
	cross apply (
		select distinct odr.DEPARTMENT_ID
			from v_operator_zone_groups_right ozr
			join v_operator_department_right odr on odr.operator_id = ozr.operator_id 
			where ozr.ZoneGroup_ID = zg.ZoneGroup_ID
			  and ozr.operator_id in (		
				select odr.Operator_ID 
					from v_operator_department_right odr
					where odr.right_id = 104
					group by odr.Operator_ID
					having COUNT(1) = 1)) d
)
update zg
	set 
		Department_ID = (select zgd.DEPARTMENT_ID from ZoneGroup_Department zgd where zgd.ZoneGroup_ID = zg.ZoneGroup_ID)
	from ZoneGroup zg
	where 1 = (select COUNT(1) from ZoneGroup_Department zgd where zgd.ZoneGroup_ID = zg.ZoneGroup_ID)
	  and zg.Department_ID is null

go

if not exists (
	select *
		from sys.columns
		where OBJECT_ID = OBJECT_ID('Geo_Zone')
		  and name = 'Department_ID'
)
begin

	alter table Geo_Zone
		add Department_ID int 
			constraint FK_Geo_Zone_Department foreign key references Department(Department_ID)
			
	alter table H_Geo_Zone
		add Department_ID int

	create nonclustered index IX_Geo_Zone_Department_ID on Geo_Zone(Department_ID)
end

go

print 'Set Department_ID for GEO_ZONE'
;with Zone_Department 
as (
select z.Zone_ID, d.Department_ID 
	from GEO_ZONE z
	cross apply (
		select distinct odr.DEPARTMENT_ID
			from v_operator_zone_right ozr
			join v_operator_department_right odr on odr.operator_id = ozr.operator_id 
			where ozr.ZONE_ID = z.ZONE_ID
			  and ozr.operator_id in (		
				select odr.Operator_ID 
					from v_operator_department_right odr
					where odr.right_id = 104
					group by odr.Operator_ID
					having COUNT(1) = 1)) d
)
update z
	set Department_ID = (select zd.DEPARTMENT_ID from Zone_Department zd where zd.ZONE_ID = z.ZONE_ID)
	from GEO_ZONE z
	where 1 = (select COUNT(1) from Zone_Department zd where zd.ZONE_ID = z.ZONE_ID)
	  and z.Department_ID is null
