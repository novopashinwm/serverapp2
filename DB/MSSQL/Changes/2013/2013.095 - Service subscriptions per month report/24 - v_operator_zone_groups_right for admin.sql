if exists (
	select *
		from sys.views 
		where name = 'v_operator_zone_groups_right'
)
	drop view v_operator_zone_groups_right
go

create view [dbo].[v_operator_zone_groups_right]   
as  
 WITH allRights(operator_id, zonegroup_id, right_id, allowed, priority) AS   
 (  
  select o_vg.operator_id, o_vg.zonegroup_id, o_vg.right_id, allowed, 1 'priority'  
   from dbo.operator_zonegroup o_vg  
  union all  
  select og_o.operator_id, og_vg.zonegroup_id, og_vg.right_id, allowed, 2 'priority'  
   from dbo.OPERATORGROUP_zoneGROUP og_vg  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id  
	union all
	select o_d.operator_id, zg.ZONEGROUP_ID, r.right_id, 1, 3 'priority'
		from dbo.OPERATOR_DEPARTMENT o_d
		join dbo.ZONEGROUP zg on zg.Department_ID = o_d.department_id
		join dbo.[Right] r on r.Right_ID in (105, 19, 113, 109, 119)
		where o_d.right_id = 2 /*SecurityAdministrator*/
		  and o_d.allowed = 1
	union all
	select og_o.operator_id, zg.zonegroup_id, r.right_id, 1, 4 'priority'
		from OperatorGroup_Operator og_o
		join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
		join dbo.ZONEGROUP zg on zg.Department_ID = og_d.department_id
		join dbo.[Right] r on r.Right_ID in (105, 19, 113, 109, 119)
		where og_d.right_id = 2 /*SecurityAdministrator*/
		  and og_d.allowed = 1		
)  
 select distinct operator_id, zonegroup_id, right_id  
 from allRights r1  
 where allowed = 1  
 and not exists (select * from allRights r2   
     where r2.operator_id = r1.operator_id   
     and r2.zonegroup_id = r1.zonegroup_id  
     and r2.right_id = r1.right_id  
     and r2.allowed = 0  
     and r2.priority < r1.priority)