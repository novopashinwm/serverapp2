insert into CONTROLLER_SENSOR_LEGEND (CONTROLLER_SENSOR_TYPE_ID, NAME, Number)
	select 2, t.Name, t.Number
		from (
			select Name = 'NetworkConnected',		Number = 65
		union all  select 'GPSLocationProvider',	Number = 66
		union all  select 'NetworkLocationProvider',Number = 67
		union all  select 'WiFi',					Number = 68
		union all  select 'AirplaneMode',			Number = 69
		) t
		where not exists (select * from CONTROLLER_SENSOR_LEGEND e where e.Number = t.Number)

insert into Controller_Sensor (CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, NUMBER, MAX_VALUE, MIN_VALUE, DEFAULT_VALUE, BITS, Descript, VALUE_EXPIRED, 
	Default_Sensor_Legend_ID, Default_Multiplier, Default_Constant, Mandatory)
	select ct.Controller_Type_ID, 2, t.Number, 1, 0, 0, 1, t.Name, 84600,
		legend.CONTROLLER_SENSOR_LEGEND_ID, 1, 0, 1
		from (
			select				Number = 32, Name = 'NetworkConnected'
			union all select			 33,        'GPSLocationProvider'		
			union all select			 34,        'NetworkLocationProvider'
			union all select			 35,        'WiFi'
			union all select			 36,        'AirplaneMode'
		) t
		join CONTROLLER_TYPE ct on ct.Type_Name in ('s60v3fp', 'nika.tracker')
		join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = t.Name
		where not exists (
			select * 
				from CONTROLLER_SENSOR cs 
				where cs.Controller_Type_ID = ct.CONTROLLER_TYPE_ID 
				  and cs.NUMBER = t.Number)
				
