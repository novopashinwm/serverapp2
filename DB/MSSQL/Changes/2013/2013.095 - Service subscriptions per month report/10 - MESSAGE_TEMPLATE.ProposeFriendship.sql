insert into dbo.Message_Template(Name)
	select t.Name 
		from (
					  select Name = 'ProposeFriendship'
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
