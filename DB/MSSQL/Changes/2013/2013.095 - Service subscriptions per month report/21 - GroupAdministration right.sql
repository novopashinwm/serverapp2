insert into [Right] (right_id, system, name, description)
	select t.right_id, 1, 'GroupAdministration', 'Allows to manage rights of users on group.'
		from (select right_id = 119) t
		where not exists (select * from [right] r where r.right_id = t.right_id)
go

