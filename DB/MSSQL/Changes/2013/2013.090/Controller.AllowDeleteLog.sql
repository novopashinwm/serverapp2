if not exists (
	select * from sys.columns where object_id = object_id('Controller') and name = 'AllowDeleteLog'
)
begin

	alter table Controller
		add AllowDeleteLog bit not null
			constraint DF_Controller_AllowDeleteLog default (0)
			
	alter table H_Controller
		add AllowDeleteLog bit not null
			constraint DF_H_Controller_AllowDeleteLog default (0)

end