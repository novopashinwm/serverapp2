/*��������� ��� ������� Zenith RS1102
	����������� ������� �������
*/

declare @type_name varchar(100) = 'Zenith RS1102'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 2, 1, 1, 0, 0, 1, 'Positive1');
insert into @controller_sensor values (@controller_type_id, 2, 2, 1, 0, 0, 1, 'Positive2');
insert into @controller_sensor values (@controller_type_id, 2, 3, 1, 0, 0, 1, 'Negative3');
insert into @controller_sensor values (@controller_type_id, 2, 4, 1, 0, 0, 1, 'Negative4');
insert into @controller_sensor values (@controller_type_id, 2, 5, 1, 0, 0, 1, 'Positive5');
insert into @controller_sensor values (@controller_type_id, 2, 6, 1, 0, 0, 1, 'Positive6');
insert into @controller_sensor values (@controller_type_id, 2, 7, 1, 0, 0, 1, 'Positive7');
insert into @controller_sensor values (@controller_type_id, 2, 8, 1, 0, 0, 1, 'Positive8');
insert into @controller_sensor values (@controller_type_id, 2, 9, 1, 0, 0, 1, 'Alarm'    );
insert into @controller_sensor values (@controller_type_id, 2, 10, 1, 0, 0, 1, 'Reserved' );
insert into @controller_sensor values (@controller_type_id, 2, 11, 1, 0, 0, 1, 'Negative1');
insert into @controller_sensor values (@controller_type_id, 2, 12, 1, 0, 0, 1, 'Negative2');

insert into @controller_sensor values (@controller_type_id, 1, 21, 4000, 0, 0, 32, 'Analogue1'		);
insert into @controller_sensor values (@controller_type_id, 1, 22, 4000, 0, 0, 32, 'Analogue2'		);
insert into @controller_sensor values (@controller_type_id, 1, 23, 4000, 0, 0, 32, 'Analogue3'		);
insert into @controller_sensor values (@controller_type_id, 1, 24, 4000, 0, 0, 32, 'Analogue4'		);
insert into @controller_sensor values (@controller_type_id, 1, 25, 4000, 0, 0, 32, 'Analogue5'		);
insert into @controller_sensor values (@controller_type_id, 1, 26, 4000, 0, 0, 32, 'Analogue6'		);
insert into @controller_sensor values (@controller_type_id, 1, 27, 4000, 0, 0, 32, 'ReservePower'	);
insert into @controller_sensor values (@controller_type_id, 1, 28, 4000, 0, 0, 32, 'OuterPower'	);

set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

