if not exists (
	select * 
		from sys.columns where OBJECT_ID = OBJECT_ID('AppClient')
		and name = 'ApnToken'
) 
begin

	alter table AppClient
		add ApnToken varbinary(32)	

	alter table H_AppClient
		add ApnToken varbinary(32)	

end