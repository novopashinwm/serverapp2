declare @type_name varchar(100) = 'Wialon'

insert into controller_type (Type_Name, AllowedToAddByCustomer)
	select name, 1	
		from (select name = @type_name) t
		where not exists (select * from Controller_Type e where e.Type_Name = t.Name)
	

/*��������� ��� ������� Wialon
	����������� ������� �������
*/

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = @type_name)

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on

insert into @controller_sensor values (@controller_type_id, 2, 0, 1, 0, 0, 1, 'DI0');
insert into @controller_sensor values (@controller_type_id, 2, 1, 1, 0, 0, 1, 'DI1');
insert into @controller_sensor values (@controller_type_id, 2, 2, 1, 0, 0, 1, 'DI2');
insert into @controller_sensor values (@controller_type_id, 2, 3, 1, 0, 0, 1, 'DI3');
insert into @controller_sensor values (@controller_type_id, 2, 4, 1, 0, 0, 1, 'DI4');
insert into @controller_sensor values (@controller_type_id, 2, 5, 1, 0, 0, 1, 'DI5');
insert into @controller_sensor values (@controller_type_id, 2, 6, 1, 0, 0, 1, 'DI6');
insert into @controller_sensor values (@controller_type_id, 2, 7, 1, 0, 0, 1, 'DI7');

insert into @controller_sensor values (@controller_type_id, 1, 100 + 0, 100000, -100000, 0, 32, 'AI0');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 1, 100000, -100000, 0, 32, 'AI1');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 2, 100000, -100000, 0, 32, 'AI2');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 3, 100000, -100000, 0, 32, 'AI3');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 4, 100000, -100000, 0, 32, 'AI4');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 5, 100000, -100000, 0, 32, 'AI5');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 6, 100000, -100000, 0, 32, 'AI6');
insert into @controller_sensor values (@controller_type_id, 1, 100 + 7, 100000, -100000, 0, 32, 'AI7');

insert into @controller_sensor values (@controller_type_id, 1, 200 + 0, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN0');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 1, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN1');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 2, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN2');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 3, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN3');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 4, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN4');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 5, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN5');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 6, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN6');
insert into @controller_sensor values (@controller_type_id, 1, 200 + 7, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPIN7');

insert into @controller_sensor values (@controller_type_id, 1, 300 + 0, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT0');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 1, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT1');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 2, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT2');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 3, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT3');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 4, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT4');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 5, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT5');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 6, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT6');
insert into @controller_sensor values (@controller_type_id, 1, 300 + 7, CAST(0x7FFFFFFFFFFFFFFF AS bigint), 0, 0, 64, 'PPOUT7');

set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

