if not exists (select * from sys.indexes where name = 'IX_AppClient_GcmRegistrationId')
begin
	create nonclustered index IX_AppClient_GcmRegistrationId on AppClient(GcmRegistrationId)
end

if not exists (select * from sys.indexes where name = 'IX_AppClient_AppId')
begin
	create nonclustered index IX_AppClient_AppId on AppClient(AppId)
end

if not exists (select * from sys.indexes where name = 'IX_AppClient_Operator_ID')
begin
	create nonclustered index IX_AppClient_Operator_ID on AppClient(Operator_ID)
end