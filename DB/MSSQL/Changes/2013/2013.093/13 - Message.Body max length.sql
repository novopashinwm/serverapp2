if exists (
	select * from sys.columns where object_id = object_id('MESSAGE') and name = 'Body' and max_length <> -1
)
begin

	alter table Message
		alter column Body nvarchar(max)

	alter table H_Message
		alter column Body nvarchar(max)

end