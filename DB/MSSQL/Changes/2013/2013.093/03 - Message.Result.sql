if not exists (
	select * from sys.columns where OBJECT_ID = OBJECT_ID('Message') and name = 'ProcessingResult'
)
begin

	alter table Message
		add ProcessingResult int not null
			constraint DF_Message_ProcessingResult default (0)

	alter table H_Message
		add ProcessingResult int not null
			constraint DF_H_Message_ProcessingResult default (0)

end
