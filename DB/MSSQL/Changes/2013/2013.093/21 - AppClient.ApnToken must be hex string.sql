if exists (
	select * 
		from sys.columns sc
		join sys.types t on t.system_type_id = sc.system_type_id
		where OBJECT_ID = OBJECT_ID('AppClient') 
		  and sc.name = 'ApnToken'
		  and t.name = 'varbinary'
)
begin

	alter table AppClient
		alter column ApnToken varchar(64)

	alter table H_AppClient
		alter column ApnToken varchar(64)

end