if exists (select * from sys.views where name = 'v_message')
	drop view v_message
go

create view v_message
	as
	select
	 m.message_id
	, Time = isnull(m.Time, (select top(1) Actual_Time from H_MESSAGE h where h.MESSAGE_ID = m.Message_ID order by h.ID asc))
	, m.subject
	, m.body
	, Template = (select mt.Name from MESSAGE_TEMPLATE mt where mt.MESSAGE_TEMPLATE_ID = m.TEMPLATE_ID)
	, m.SourceType_ID
	, Source_Type = case m.SourceType_ID 
			when 0x0100 then 'Controller'
			when 0x0200 then 'Terminal	'
			when 0x0400 then 'Server'
			when 0x0800 then 'Client'
			when 0x1000 then 'Operator'
			when 0x2000 then 'Asid'
			when 0x4000 then 'Phone'
			when 0x8000 then 'Email'
			else 'Unknown'
		end			
	, m.source_id
	, m.destination_id
	, "Contact Type" = 
		case destinationType_ID
			when 1 then 'SMS / HTTP'
			when 2 then 'SMS / SMPP'
			when 3 then 'SMS / MPX'
			when 6 then 'Email'
			when 7 then 'Operator'
			when 8 then 'Server'
			when 9 then 'Android'
			when 10 then 'iOS'
			else 'Unknown'
		end	
	, "Contact" = 
		case destinationType_ID 
			when 1 then p.phone
			when 2 then p.phone
			when 3 then a.value
			when 6 then e.email
			when 9 then (select GcmRegistrationId from AppClient where ID = m.DESTINATION_ID)
			when 10 then (select ApnToken from AppClient where ID = m.DESTINATION_ID)
			else null
		end
	, Confirmed = isnull(p.Confirmed, e.Confirmed)
	, "Owner Login" = o.login
	, ProcessingResult = case ProcessingResult	
		when 0 then 'Unknown'
		when 1 then 'Received'
		when 2 then 'Processing'
		when 3 then 'Sent'
		when 4 then 'ContactDoesNotExist'
		when 5 then 'UnsuccessfulBillingResult'
		when 6 then 'Throttled'
		when 7 then 'MessageQueueFull'
		when 8 then 'InvalidMessageLength'
		when 9 then 'InvalidDestinationAddress'
		when 10 then 'NoConnectionWithGateway'
		when 11 then 'HttpError'	
		when 12 then 'Timeout'				
		when 13 then 'ServiceTurnedOff'
		when 14 then 'Error'			
		when 15 then 'DestinationIsNotSpecified'
		else CONVERT(varchar, ProcessingResult)
	end
	, Accepted  = case m.TYPE & 0x08 when 0 then 'no' else 'yes' end
	, Delivered = case m.TYPE & 0x04 when 0 then 'no' else 'yes' end
	, Failed    = case m.TYPE & 0x80 when 0 then 'no' else 'yes' end
	, Pending   = case m.TYPE & 0x10 when 0 then 'no' else 'yes' end
	, Throttled = case m.TYPE & 0x40 when 0 then 'no' else 'yes' end
	, MQF       = case m.TYPE & 0x20 when 0 then 'no' else 'yes' end
	, m.DestinationType_ID
	, m.ErrorCount
	--update m
	--	set m.TYPE = m.TYPE &  (~(convert(int, 0x08)))
	 from message m
	 outer apply (
		select top(1) h.Phone, h.Operator_ID, h.Confirmed
			from H_Phone h
			where h.Phone_ID = m.DESTINATION_ID
			  and m.DestinationType_ID in (1,2)
			  and h.ACTION <> 'DELETE'
			order by h.ID desc) p
	 left outer join email e on e.email_id = m.destination_id and destinationType_ID in (6)
	 left outer join asid a  on a.id       = m.destination_id and destinationType_ID in (3)
	 left outer join operator o on o.operator_id = 
		(case destinationType_ID 
			when 1 then p.operator_id
			when 2 then p.operator_id
			when 3 then a.operator_id
			when 6 then e.operator_id
			else null
		 end)
