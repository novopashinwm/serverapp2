if not exists (
	select * from sys.objects where name like 'AppClient'
)
begin

	create table AppClient
	(
		ID int identity(1,1)
			constraint PK_AppClient
				primary key clustered,
		Operator_ID int not null
			constraint FK_AppClient_Operator
				foreign key references Operator(Operator_ID),
		AppId varchar(255) COLLATE Latin1_General_CS_AS not null,
		GcmRegistrationId varchar(255)
	)
	
	create table H_AppClient
	(
		ID int not null,
		Operator_ID int not null,
		AppId varchar(255) COLLATE Latin1_General_CS_AS not null,
		GcmRegistrationId varchar(255),
		
		H_AppClient_ID int identity(1,1)
			constraint PK_H_AppClient
				primary key clustered,	
        TRAIL_ID        int 
			constraint FK_H_AppClient_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)


end