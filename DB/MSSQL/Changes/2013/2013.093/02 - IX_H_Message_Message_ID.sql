if not exists (
	select * from sys.indexes where name = 'IX_H_Message_Message_ID'
)
begin

	create nonclustered index IX_H_Message_Message_ID on H_Message(Message_ID)
	
end