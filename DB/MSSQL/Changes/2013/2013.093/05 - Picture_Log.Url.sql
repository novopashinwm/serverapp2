if not exists (
	select * from sys.columns where OBJECT_ID = OBJECT_ID('Picture_Log') and name = 'Url'
) 
begin
	alter table Picture_Log
		add Url varchar(255)
		
	alter table Picture_log
		alter column Picture varbinary(max) 
end