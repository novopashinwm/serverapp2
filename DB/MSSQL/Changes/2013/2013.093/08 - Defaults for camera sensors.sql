update cs
	set Default_Sensor_Legend_ID = legend.CONTROLLER_SENSOR_LEGEND_ID,
		Default_Multiplier = 1,
		Default_Constant = 0
from CONTROLLER_SENSOR cs
join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = cs.Descript
where ct.TYPE_NAME = 'FtpWebCam'
  and cs.Default_Sensor_Legend_ID is null