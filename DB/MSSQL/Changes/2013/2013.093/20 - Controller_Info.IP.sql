if not exists (
	select * 
		from sys.columns 
		where object_id = OBJECT_ID('Controller_Info')
		  and name = 'IP'
) 
begin

	alter table Controller_Info
		add IP varchar(39)

	alter table H_Controller_Info
		add IP varchar(39)

end