insert into MESSAGE_TEMPLATE_FIELD (DOTNET_TYPE_ID, NAME)
	select dt.DOTNET_TYPE_ID, t.Name
		from DOTNET_TYPE dt
		join (		select Type = 'System.String', Name = 'Picture.Name'
		union all	select        'System.Int32',         'Picture.VehicleID'
		union all	select        'System.Int32',         'Picture.LogTime'
		) t on t.Type = dt.TYPE_NAME
		where not exists (
			select * from MESSAGE_TEMPLATE_FIELD e where e.DOTNET_TYPE_ID = dt.DOTNET_TYPE_ID and e.NAME = t.Name and e.MESSAGE_TEMPLATE_ID is null)