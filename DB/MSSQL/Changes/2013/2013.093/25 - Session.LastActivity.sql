if not exists (
	select * from sys.columns where object_id = object_id('Session') and name = 'LastActivity'
)
begin

	alter table Session
		add LastActivity datetime

end