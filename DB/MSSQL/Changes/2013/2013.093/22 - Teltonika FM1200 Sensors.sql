insert into Controller_Sensor
(
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	DESCRIPT,
	VALUE_EXPIRED,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Default_Constant,
	Mandatory
)
	select	ct_target.Controller_Type_ID,
			cs.CONTROLLER_SENSOR_TYPE_ID,
			cs.NUMBER,
			cs.MAX_VALUE,
			cs.MIN_VALUE,
			cs.DEFAULT_VALUE,
			cs.BITS,
			cs.DESCRIPT,
			cs.VALUE_EXPIRED,
			cs.Default_Sensor_Legend_ID,
			cs.Default_Multiplier,
			cs.Default_Constant,
			cs.Mandatory
	from Controller_Sensor cs
	join Controller_Type ct_source on ct_source.Controller_Type_ID = cs.Controller_Type_ID
	join Controller_Type ct_target on ct_target.Type_Name = 'Teltonika FM1200'
	where ct_source.Type_Name = 'FM3101'
	and not exists (
		select *
			from Controller_Sensor e
			where e.Controller_Type_ID = ct_target.Controller_Type_ID
			  and e.Number = cs.Number)