update CONTROLLER_TYPE
	set SupportsPassword = 1
	where TYPE_NAME = 'FtpWebCam'
	  and SupportsPassword = 0