if not exists (
	select * from sys.objects where name = 'UserAgent'
)
begin

	create table UserAgent
	(
		ID int identity (1,1) 
			constraint PK_UserAgent
				primary key clustered,
		Name varchar(255)
	)
	
	create nonclustered index IX_UserAgent_Name on UserAgent (Name)
	
end