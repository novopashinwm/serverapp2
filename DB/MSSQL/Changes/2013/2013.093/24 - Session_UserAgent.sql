if not exists (
	select * from sys.objects where name = 'Session_UserAgent'
)
begin

	create table Session_UserAgent
	(
		ID int identity(1,1)
			constraint PK_Session_UserAgent
				primary key clustered,
		Session_ID int not null
			constraint FK_Session_UserAgent_Session_ID
				foreign key references Session(Session_ID),
		UserAgent_ID int not null 
			constraint FK_Session_UserAgent_UserAgent_ID
				foreign key references UserAgent(ID),
		[Date] datetime not null
	)

end