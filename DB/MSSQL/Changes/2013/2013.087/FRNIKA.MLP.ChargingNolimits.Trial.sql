insert into billing_service_type (Service_Type, Service_Type_Category, Controller_Type_ID, Vehicle_Kind_ID, MinIntervalSeconds, LimitedQuantity, Singleton, Name)
	select * from (
		select Service_Type = 'FRNIKA.MLP.ChargingNolimits.Trial'
			  , bst.Service_Type_Category
			  , bst.Controller_Type_ID
			  , bst.Vehicle_Kind_ID
			  , MinIntervalSeconds
			  , LimitedQuantity
			  , Singleton
			  , Name = '���� LBS �������� �����'
			from billing_service_type bst
			where bst.Service_Type = 'FRNIKA.MLP.ChargingNolimits'
	) t where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)