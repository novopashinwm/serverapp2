insert into OPERATORGROUP_DEPARTMENT
select og.OPERATORGROUP_ID, d.DEPARTMENT_ID, r.RIGHT_ID, 1
from [right] r, DEPARTMENT d, OPERATORGROUP og
where r.name in (
'SecurityAdministration'
,'ViewVehicleCurrentStatus'
,'EditVehicles'
,'ViewControllerPhone'
,'ViewControllerDeviceID'
,'EditControllerPhone'
,'EditControllerDeviceId'
,'ControllerPasswordAccess'
,'VehicleAccess'
,'DepartmentsAccess'
,'ManageSensors')
and og.NAME = 'sales'
and not exists (
	select * 
		from OPERATORGROUP_DEPARTMENT ogd
		where ogd.RIGHT_ID = r.RIGHT_ID
		  and ogd.DEPARTMENT_ID = d.DEPARTMENT_ID
		  and ogd.OPERATORGROUP_ID = og.OPERATORGROUP_ID)
	