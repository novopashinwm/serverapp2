if not exists (
	select * from sys.columns where object_id = object_id('Emulator') and name = 'FakeLat')
begin

	alter table Emulator
		add FakeLat numeric(8,5)
		
	alter table Emulator
		add FakeLng numeric(8,5)
		
end