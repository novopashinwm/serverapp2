if exists (
	select * from sys.columns where object_id = object_id('Command_Result') and name = 'Date_Received' and is_nullable = 1
)
begin

	alter table Command_Result
		alter column Date_Received datetime not null

end