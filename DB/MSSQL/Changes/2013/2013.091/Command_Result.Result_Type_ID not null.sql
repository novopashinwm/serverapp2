if exists (
	select * from sys.columns where object_id = object_id('Command_Result') and name = 'Result_Type_ID' and is_nullable = 1
)
begin

	if exists (select * from sys.indexes where name = 'IX_LogTime')
	begin
		drop index IX_LogTime on Command_Result;
	end

	alter table Command_Result
		alter column Result_Type_ID int not null

	create nonclustered index IX_LogTime on Command_Result(Log_Time, Result_Type_ID)
	

end