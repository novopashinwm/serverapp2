if not exists (
	select * from sys.columns where object_id = object_id('Controller_Type') and name = 'Default_Vehicle_Kind_ID'
)
begin

	alter table Controller_Type
		add Default_Vehicle_Kind_ID int
			constraint FK_Controller_Type_Default_Vehicle_Kind_ID
				foreign key references Vehicle_Kind(Vehicle_Kind_ID)

	alter table H_Controller_Type
		add Default_Vehicle_Kind_ID int

end

go

update ct
	set Default_Vehicle_Kind_ID = vk.Vehicle_Kind_ID
	from Controller_Type ct, VEHICLE_KIND vk
	where ct.Type_Name = 'TR203'
	  and vk.NAME = '������'
	  and ct.Default_Vehicle_Kind_ID is null
	