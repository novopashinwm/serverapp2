create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select  Number = 0 , Descript = '' where 1=0
			  union all select 70100, 'SENSOR_ACCELEROMETER_X'
			  union all select 70101, 'SENSOR_ACCELEROMETER_Y'
			  union all select 70102, 'SENSOR_ACCELEROMETER_Z'
			  union all select 70900, 'SENSOR_GRAVITY_X'
			  union all select 70901, 'SENSOR_GRAVITY_Y'
			  union all select 70902, 'SENSOR_GRAVITY_Z'
			  union all select 70400, 'SENSOR_GYROSCOPE_X'
			  union all select 70401, 'SENSOR_GYROSCOPE_Y'
			  union all select 70402, 'SENSOR_GYROSCOPE_Z'
			  union all select 70200, 'SENSOR_MAGNETIC_FIELD_X'
			  union all select 70201, 'SENSOR_MAGNETIC_FIELD_Y'
			  union all select 70202, 'SENSOR_MAGNETIC_FIELD_Z'
		) t
	where ct.Type_Name = 'S60V3FP'
	  and cst.Name = '����������'

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

drop table #Controller_Sensor
