if not exists (select * from sys.objects where name = 'Billing_Service_Provider')
begin

	create table Billing_Service_Provider
	(
		Billing_Service_Provider_ID int identity
			constraint PK_Billing_Service_Provider primary key clustered,
		Name nvarchar(255)	
	)

end;

go

insert into Billing_Service_Provider (Name)
	select t.Name
	from (
		select '��� �� ������� ������' as Name union all
		select '��� �� ������' union all
		select '��� �� ��������/����' union all		
		select '��� �� ������-�����' union all
		select '��� �� ��' union all
		select '��� �� ������/�����' ) t
		where not exists (
			select * 
				from Billing_Service_Provider e
				where e.Name = t.Name)

go

if not exists (select * from sys.objects where name = 'Billing_Service_Provider_Host')
begin

	create table Billing_Service_Provider_Host
	(
		Billing_Service_Provider_Host int identity
			constraint PK_Billing_Service_Provider_Host
				primary key clustered,
		Billing_Service_Provider_ID int not null
			constraint FK_Billing_Service_Provider_Host_Billing_Service_Provider_ID
				foreign key references Billing_Service_Provider(Billing_Service_Provider_ID),
		IP varchar(39) not null
	)
	
end

go

insert into Billing_Service_Provider_Host(Billing_Service_Provider_ID, IP)
	select bsp.Billing_Service_Provider_ID, src.IP
	from (
		select Name = N'', IP = '' where 1=0
		union all 
			select * from 
				(select Name = '��� �� ������� ������') bsp,
				(	select IP = '10.147.8.97' union all
					select IP = '10.147.9.97' union all
					select IP = '10.147.8.98' union all
					select IP = '10.147.9.98' union all
					select IP = '10.147.8.99' union all
					select IP = '10.147.9.99') h
		union all 
			select * from 
				(select Name = '��� �� ������') bsp,
				(	select IP = '10.112.170.97' union all
					select IP = '10.112.171.97' union all
					select IP = '10.112.170.98' union all
					select IP = '10.112.171.98' union all
					select IP = '10.112.170.99' union all
					select IP = '10.112.171.99' union all
					select IP = '10.112.170.100' union all
					select IP = '10.112.171.100' union all
					select IP = '10.112.170.101' union all
					select IP = '10.112.171.101') h
		union all 
			select * from 
				(select Name = '��� �� ��������/����') bsp,
				(	select IP = '10.35.50.97' union all
					select IP = '10.35.50.98' union all
					select IP = '10.35.50.99' union all
					select IP = '10.35.51.97' union all
					select IP = '10.35.51.98' union all
					select IP = '10.35.51.99') h
		union all 
			select * from 
				(select Name = '��� �� ������-�����') bsp,
				(	select IP = '10.16.232.100' union all
					select IP = '10.16.233.100' union all
					select IP = '10.16.232.101' union all
					select IP = '10.16.233.101' union all
					select IP = '10.16.232.102' union all
					select IP = '10.16.233.102' union all
					select IP = '10.16.232.103' union all
					select IP = '10.16.233.103') h
		union all 
			select * from 
				(select Name = '��� �� ��') bsp,
				(	select IP = '10.40.180.97' union all
					select IP = '10.40.181.97' union all
					select IP = '10.40.180.233' union all
					select IP = '10.40.181.233' union all
					select IP = '10.40.180.235' union all
					select IP = '10.40.181.235' union all
					select IP = '10.40.180.234' union all
					select IP = '10.40.181.234') h
		union all 
			select * from 
				(select Name = '��� �� ������/�����') bsp,
				(	select IP = '10.0.240.105' union all
					select IP = '10.0.240.106' union all
					select IP = '10.0.240.107' union all
					select IP = '10.0.241.105' union all
					select IP = '10.0.241.106' union all
					select IP = '10.0.241.107') h
	) src
	join Billing_Service_Provider bsp on bsp.Name = src.Name
	where not exists (
		select *
			from Billing_Service_Provider_Host e
			where e.Billing_Service_Provider_ID = bsp.Billing_Service_Provider_ID
			  and e.IP = src.IP)