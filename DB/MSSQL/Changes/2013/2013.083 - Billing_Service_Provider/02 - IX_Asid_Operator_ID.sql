if not exists (select * from sys.indexes where name = 'IX_Asid_Operator_ID')
begin

	create nonclustered index IX_Asid_Operator_ID on Asid (Operator_ID)
	
end