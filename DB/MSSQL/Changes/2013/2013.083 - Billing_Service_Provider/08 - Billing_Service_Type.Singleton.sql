if not exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'Singleton')
begin

	alter table Billing_Service_Type
		add Singleton bit not null constraint DF_Billing_Service_Type_Singleton default(1)

end

go

update bst
	set Singleton = 0
	from Billing_Service_Type bst
	where Singleton = 1
	  and (Service_Type = 'FRNIKA.MLP.ChargingEconom' and MaxQuantity in (500, 10000, 50000)
	   or  Service_Type = 'FRNIKA.SMS.M2M'
	   or  Service_Type = 'FRNIKA.SMS.LBS')
		