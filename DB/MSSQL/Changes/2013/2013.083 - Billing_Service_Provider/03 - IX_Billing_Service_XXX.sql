if exists (select * from sys.indexes where name = 'IX_Billing_Service_ID')
	drop index IX_Billing_Service_ID on Billing_Service
go

if not exists (select * from sys.indexes where name = 'IX_Billing_Service_Asid_ID')
begin

	create clustered index IX_Billing_Service_Asid_ID on Billing_Service(Asid_ID)

end
go

if not exists (select * from sys.indexes where name = 'IX_Billing_Service_Department_ID')
begin

	create nonclustered index IX_Billing_Service_Department_ID on Billing_Service(Department_ID)

end
go

if not exists (select * from sys.indexes where name = 'IX_Billing_Service_Operator_ID')
begin

	create nonclustered index IX_Billing_Service_Operator_ID on Billing_Service(Operator_ID)

end
go

if not exists (select * from sys.indexes where name = 'IX_Billing_Service_Vehicle_ID')
begin

	create nonclustered index IX_Billing_Service_Vehicle_ID on Billing_Service(Vehicle_ID)

end
go
