if not exists (select * from sys.columns where object_id = object_id('Asid') and name = 'Billing_Service_Provider_ID')
begin

	alter table Asid
		add Billing_Service_Provider_ID int
			constraint FK_Billing_Service_Provider 
				foreign key references Billing_Service_Provider(Billing_Service_Provider_ID)

	alter table H_Asid
		add Billing_Service_Provider_ID int

end