if not exists (select * from sys.columns where object_id = object_id('Controller_Type') and name = 'DeviceIdIsImei')
begin

	alter table Controller_Type 
		add DeviceIdIsImei bit not null constraint DF_Controller_Type_DeviceIdIsImei default(0)
		
	alter table H_Controller_Type 
		add DeviceIdIsImei bit not null constraint DF_H_Controller_Type_DeviceIdIsImei default(0)

end

go

update CONTROLLER_TYPE
	set DeviceIdIsImei = 1
	where TYPE_NAME in ('TR600', 'TR203', 'Teltonika FM1100', 'Teltonika FM5300', 'Teltonika GH3000', 'Teltonika FM2200', 'GlobalSat GTR-128')
	  and DeviceIdIsImei = 0
	