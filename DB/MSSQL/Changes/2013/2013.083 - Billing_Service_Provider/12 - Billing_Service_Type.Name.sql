if not exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'Name')
begin

	alter table Billing_Service_Type
		add Name nvarchar(100)

end

go

update bst
	set Name = src.Name
	from Billing_Service_Type bst
	join (select Code = '', MaxQuantity = 0, Name = N'' where 1=0
			union all select 'FRNIKA.MLP'					, null	, '���� LBS (������)'					
			union all select 'FRNIKA.MLP.Charging'			, null	, '���� LBS (�����������)'			
			union all select 'FRNIKA.GPS.Modem'				, null	, '����-�������'						
			union all select 'FRNIKA.GPS.Phone'				, null	, '����-������'						
			union all select 'FRNIKA.GPS.Server'			, null	, '��������� ���������� ���������'	
			union all select 'FRNIKA.Admin'					, null	, '������������� ����'				
			union all select 'FRNIKA.MLP.ChargingEconom'	, 40	, '���� LBS ������'					
			union all select 'FRNIKA.SMS.ChargingEconom'	, null	, '���� SMS ������'					
			union all select 'FRNIKA.MLP.ChargingNolimits'	, null	, '���� LBS ��������'								
			union all select 'FRNIKA.SMS.M2M'				, null	, 'SMS ����-�������'					
			union all select 'FRNIKA.SMS.LBS'				, null	, 'SMS ���� LBS'						
			union all select 'INDIA.SMS.Unlim'				, null	, 'Unlimited SMS for India'			
			union all select 'Free.SMS.Confirmation'		, 3		, 'Free confirmation SMS'				
			union all select 'FRNIKA.MLP.ChargingEconom'	, 500	, '���� LBS ������ 500'				
			union all select 'FRNIKA.MLP.ChargingEconom'	, 10000	, '���� LBS ������ 10000'				
			union all select 'FRNIKA.MLP.ChargingEconom'	, 50000	, '���� LBS ������ 50000'				
	) src on src.Code = bst.Service_Type
	     and (src.MaxQuantity = bst.MaxQuantity or src.MaxQuantity is null and bst.MaxQuantity is null)
	where bst.Name is null
	