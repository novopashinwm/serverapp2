if exists (select * from sys.objects where name = 'UQ_Billing_Service')
begin

	alter table Billing_Service
		drop constraint UQ_Billing_Service;

end
