﻿
	declare @trail_id int
	insert into trail(trail_time) select getutcdate()
	set @trail_id = @@identity
	
		print ' H_MAPS'
		
		insert into H_MAPS ( [MAP_ID],[NAME],[DESCRIPTION],[FILE_NAME],[VERSION],[CHECK_SUM],[GUID], action, actual_time, trail_id)
		select o.[MAP_ID],[NAME],[DESCRIPTION],[FILE_NAME],[VERSION],[CHECK_SUM],[GUID], 'upload', getutcdate(), @trail_id
			from [MAPS] o where not exists (select * from [H_MAPS] h where h.MAP_ID = o.MAP_ID)
		
		print ' H_MAP_VERTEX'
		
		insert into H_MAP_VERTEX ( [VERTEX_ID],[MAP_ID],[X],[Y],[VISIBLE],[ENABLE],[Geo], action, actual_time, trail_id)
		select o.[VERTEX_ID],[MAP_ID],[X],[Y],[VISIBLE],[ENABLE],[Geo], 'upload', getutcdate(), @trail_id
			from [MAP_VERTEX] o where not exists (select * from [H_MAP_VERTEX] h where h.VERTEX_ID = o.VERTEX_ID)
		
		print ' H_DEPARTMENT'
		
		insert into H_DEPARTMENT ( [DEPARTMENT_ID],[NAME],[ExtID],[Description],[Country_ID], action, actual_time, trail_id)
		select o.[DEPARTMENT_ID],[NAME],[ExtID],[Description],[Country_ID], 'upload', getutcdate(), @trail_id
			from [DEPARTMENT] o where not exists (select * from [H_DEPARTMENT] h where h.DEPARTMENT_ID = o.DEPARTMENT_ID)
		
		print ' H_ROUTE_POINT'
		
		insert into H_ROUTE_POINT ( [ROUTE_POINT_ID],[ROUTE_ID],[POINT_ID],[ORD],[STATUS], action, actual_time, trail_id)
		select o.[ROUTE_POINT_ID],[ROUTE_ID],[POINT_ID],[ORD],[STATUS], 'upload', getutcdate(), @trail_id
			from [ROUTE_POINT] o where not exists (select * from [H_ROUTE_POINT] h where h.ROUTE_POINT_ID = o.ROUTE_POINT_ID)
		
		print ' H_GEO_SEGMENT_VERTEX'
		
		insert into H_GEO_SEGMENT_VERTEX ( [GEO_SEGMENT_ID],[VERTEX_ID],[ORDER_GS],[DIRECT],[GEO_TRIP_ID],[ORDER_GT],[GEO_SEGMENT_VERTEX_ID], action, actual_time, trail_id)
		select o.[GEO_SEGMENT_ID],[VERTEX_ID],[ORDER_GS],[DIRECT],[GEO_TRIP_ID],[ORDER_GT],[GEO_SEGMENT_VERTEX_ID], 'upload', getutcdate(), @trail_id
			from [GEO_SEGMENT_VERTEX] o where not exists (select * from [H_GEO_SEGMENT_VERTEX] h where h.GEO_SEGMENT_VERTEX_ID = o.GEO_SEGMENT_VERTEX_ID)
		
		print ' H_ROUTEGROUP'
		
		insert into H_ROUTEGROUP ( [ROUTEGROUP_ID],[NAME],[COMMENT], action, actual_time, trail_id)
		select o.[ROUTEGROUP_ID],[NAME],[COMMENT], 'upload', getutcdate(), @trail_id
			from [ROUTEGROUP] o where not exists (select * from [H_ROUTEGROUP] h where h.ROUTEGROUP_ID = o.ROUTEGROUP_ID)
		
		print ' H_WORKSTATION'
		
		insert into H_WORKSTATION ( [WORKSTATION_ID],[PLACE_NUM],[NAME],[CELLPHONE],[FIO],[STATUS],[MACHINE_NAME], action, actual_time, trail_id)
		select o.[WORKSTATION_ID],[PLACE_NUM],[NAME],[CELLPHONE],[FIO],[STATUS],[MACHINE_NAME], 'upload', getutcdate(), @trail_id
			from [WORKSTATION] o where not exists (select * from [H_WORKSTATION] h where h.WORKSTATION_ID = o.WORKSTATION_ID)
		
		print ' H_DECADE'
		
		insert into H_DECADE ( [DECADE_ID],[DEPARTMENT],[BEGIN],[END], action, actual_time, trail_id)
		select o.[DECADE_ID],[DEPARTMENT],[BEGIN],[END], 'upload', getutcdate(), @trail_id
			from [DECADE] o where not exists (select * from [H_DECADE] h where h.DECADE_ID = o.DECADE_ID)
		
		print ' H_Tracker'
		
		insert into H_Tracker ( [ID],[Number],[Control_Code],[Pattern_Vehicle_ID],[Vehicle_ID],[Activated], action, actual_time, trail_id)
		select o.[ID],[Number],[Control_Code],[Pattern_Vehicle_ID],[Vehicle_ID],[Activated], 'upload', getutcdate(), @trail_id
			from [Tracker] o where not exists (select * from [H_Tracker] h where h.ID = o.ID)
		
		print ' H_BRIGADE'
		
		insert into H_BRIGADE ( [BRIGADE_ID],[DECADE],[VEHICLE],[GRAPHIC],[GRAPHIC_BEGIN],[WAYOUT], action, actual_time, trail_id)
		select o.[BRIGADE_ID],[DECADE],[VEHICLE],[GRAPHIC],[GRAPHIC_BEGIN],[WAYOUT], 'upload', getutcdate(), @trail_id
			from [BRIGADE] o where not exists (select * from [H_BRIGADE] h where h.BRIGADE_ID = o.BRIGADE_ID)
		
		print ' H_MESSAGE_OPERATOR'
		
		insert into H_MESSAGE_OPERATOR ( [MESSAGE_OPERATOR_ID],[MESSAGE_ID],[OPERATOR_ID],[MESSAGE_STATUS_ID], action, actual_time, trail_id)
		select o.[MESSAGE_OPERATOR_ID],[MESSAGE_ID],[OPERATOR_ID],[MESSAGE_STATUS_ID], 'upload', getutcdate(), @trail_id
			from [MESSAGE_OPERATOR] o where not exists (select * from [H_MESSAGE_OPERATOR] h where h.MESSAGE_OPERATOR_ID = o.MESSAGE_OPERATOR_ID)
		
		print ' H_BRIGADE_DRIVER'
		
		insert into H_BRIGADE_DRIVER ( [BRIGADE_DRIVER_ID],[BRIGADE],[DRIVER],[DRIVER_NUMBER], action, actual_time, trail_id)
		select o.[BRIGADE_DRIVER_ID],[BRIGADE],[DRIVER],[DRIVER_NUMBER], 'upload', getutcdate(), @trail_id
			from [BRIGADE_DRIVER] o where not exists (select * from [H_BRIGADE_DRIVER] h where h.BRIGADE_DRIVER_ID = o.BRIGADE_DRIVER_ID)
		
		print ' H_GEO_TRIP'
		
		insert into H_GEO_TRIP ( [GEO_TRIP_ID],[LENGTH],[POINTA],[POINTB],[COMMENTS],[ROUTE_ID],[RUNTIME],[CREATOR_OPERATOR_ID], action, actual_time, trail_id)
		select o.[GEO_TRIP_ID],[LENGTH],[POINTA],[POINTB],[COMMENTS],[ROUTE_ID],[RUNTIME],[CREATOR_OPERATOR_ID], 'upload', getutcdate(), @trail_id
			from [GEO_TRIP] o where not exists (select * from [H_GEO_TRIP] h where h.GEO_TRIP_ID = o.GEO_TRIP_ID)
		
		print ' H_JOURNAL_DAY'
		
		insert into H_JOURNAL_DAY ( [JOURNAL_DAY_ID],[BRIGADE_DRIVER],[DATE_DAY],[VEHICLE],[WAYOUT],[SHIFT],[TIME_BEGIN],[TIME_END], action, actual_time, trail_id)
		select o.[JOURNAL_DAY_ID],[BRIGADE_DRIVER],[DATE_DAY],[VEHICLE],[WAYOUT],[SHIFT],[TIME_BEGIN],[TIME_END], 'upload', getutcdate(), @trail_id
			from [JOURNAL_DAY] o where not exists (select * from [H_JOURNAL_DAY] h where h.JOURNAL_DAY_ID = o.JOURNAL_DAY_ID)
		
		print ' H_WEB_LINE_VERTEX'
		
		insert into H_WEB_LINE_VERTEX ( [LINE_VERTEX_ID],[LINE_ID],[VERTEX_ID],[ORDER], action, actual_time, trail_id)
		select o.[LINE_VERTEX_ID],[LINE_ID],[VERTEX_ID],[ORDER], 'upload', getutcdate(), @trail_id
			from [WEB_LINE_VERTEX] o where not exists (select * from [H_WEB_LINE_VERTEX] h where h.LINE_VERTEX_ID = o.LINE_VERTEX_ID)
		
		print ' H_RS'
		
		insert into H_RS ( [RS_ID],[ROUTE_ID],[DAY_TYPE_ID],[NAME],[DESCRIPTION], action, actual_time, trail_id)
		select o.[RS_ID],[ROUTE_ID],[DAY_TYPE_ID],[NAME],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [RS] o where not exists (select * from [H_RS] h where h.RS_ID = o.RS_ID)
		
		print ' H_PhoneMQF'
		
		insert into H_PhoneMQF ( [PhoneMQF_ID],[PhoneID],[NextCheck],[Count], action, actual_time, trail_id)
		select o.[PhoneMQF_ID],[PhoneID],[NextCheck],[Count], 'upload', getutcdate(), @trail_id
			from [PhoneMQF] o where not exists (select * from [H_PhoneMQF] h where h.PhoneMQF_ID = o.PhoneMQF_ID)
		
		print ' H_WEB_POINT'
		
		insert into H_WEB_POINT ( [POINT_ID],[VERTEX_ID],[NAME],[DESCRIPTION],[TYPE_ID],[OPERATOR_ID], action, actual_time, trail_id)
		select o.[POINT_ID],[VERTEX_ID],[NAME],[DESCRIPTION],[TYPE_ID],[OPERATOR_ID], 'upload', getutcdate(), @trail_id
			from [WEB_POINT] o where not exists (select * from [H_WEB_POINT] h where h.POINT_ID = o.POINT_ID)
		
		print ' H_GRAPHIC'
		
		insert into H_GRAPHIC ( [GRAPHIC_ID],[NAME],[BEGIN],[DAY_COUNT],[DRIVER_COUNT],[SHIFT_COUNT],[DEPARTMENT_ID], action, actual_time, trail_id)
		select o.[GRAPHIC_ID],[NAME],[BEGIN],[DAY_COUNT],[DRIVER_COUNT],[SHIFT_COUNT],[DEPARTMENT_ID], 'upload', getutcdate(), @trail_id
			from [GRAPHIC] o where not exists (select * from [H_GRAPHIC] h where h.GRAPHIC_ID = o.GRAPHIC_ID)
		
		print ' H_RS_OPERATIONSTYPE'
		
		insert into H_RS_OPERATIONSTYPE ( [RS_OPERATIONSTYPE_ID],[ALIAS],[LETTER],[DESCRIPTION], action, actual_time, trail_id)
		select o.[RS_OPERATIONSTYPE_ID],[ALIAS],[LETTER],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [RS_OPERATIONSTYPE] o where not exists (select * from [H_RS_OPERATIONSTYPE] h where h.RS_OPERATIONSTYPE_ID = o.RS_OPERATIONSTYPE_ID)
		
		print ' H_RS_OPERATIONTYPE'
		
		insert into H_RS_OPERATIONTYPE ( [RS_OPERATIONTYPE_ID],[RS_VARIANT_ID],[RS_OPERATIONSTYPE_ID],[TRIP_OUT],[TRIP_OUT_0],[TRIP_IN_0],[TRIP_IN],[CODE],[OPERATION_TIME],[NAME], action, actual_time, trail_id)
		select o.[RS_OPERATIONTYPE_ID],[RS_VARIANT_ID],[RS_OPERATIONSTYPE_ID],[TRIP_OUT],[TRIP_OUT_0],[TRIP_IN_0],[TRIP_IN],[CODE],[OPERATION_TIME],[NAME], 'upload', getutcdate(), @trail_id
			from [RS_OPERATIONTYPE] o where not exists (select * from [H_RS_OPERATIONTYPE] h where h.RS_OPERATIONTYPE_ID = o.RS_OPERATIONTYPE_ID)
		
		print ' H_GRAPHIC_SHIFT'
		
		insert into H_GRAPHIC_SHIFT ( [ID],[GRAPHIC],[DAY_NUMBER],[DRIVER_NUMBER],[SHIFT],[BEGIN_TIME],[END_TIME], action, actual_time, trail_id)
		select o.[ID],[GRAPHIC],[DAY_NUMBER],[DRIVER_NUMBER],[SHIFT],[BEGIN_TIME],[END_TIME], 'upload', getutcdate(), @trail_id
			from [GRAPHIC_SHIFT] o where not exists (select * from [H_GRAPHIC_SHIFT] h where h.ID = o.ID)
		
		print ' H_RS_POINT'
		
		insert into H_RS_POINT ( [RS_POINT_ID],[RS_ROUND_TRIP_ID],[RS_WAYOUT_ID],[RS_TRIPTYPE_ID],[RS_OPERATIONTYPE_ID],[RS_SHIFT_ID],[TIME_IN],[TIME_OUT],[STOP_TIME],[OPERATION_TIME], action, actual_time, trail_id)
		select o.[RS_POINT_ID],[RS_ROUND_TRIP_ID],[RS_WAYOUT_ID],[RS_TRIPTYPE_ID],[RS_OPERATIONTYPE_ID],[RS_SHIFT_ID],[TIME_IN],[TIME_OUT],[STOP_TIME],[OPERATION_TIME], 'upload', getutcdate(), @trail_id
			from [RS_POINT] o where not exists (select * from [H_RS_POINT] h where h.RS_POINT_ID = o.RS_POINT_ID)
		
		print ' H_RS_ROUND_TRIP'
		
		insert into H_RS_ROUND_TRIP ( [RS_ROUND_TRIP_ID],[RS_VARIANT_ID],[INDEX],[A_B], action, actual_time, trail_id)
		select o.[RS_ROUND_TRIP_ID],[RS_VARIANT_ID],[INDEX],[A_B], 'upload', getutcdate(), @trail_id
			from [RS_ROUND_TRIP] o where not exists (select * from [H_RS_ROUND_TRIP] h where h.RS_ROUND_TRIP_ID = o.RS_ROUND_TRIP_ID)
		
		print ' H_WEB_POINT_TYPE'
		
		insert into H_WEB_POINT_TYPE ( [TYPE_ID],[NAME],[DESCRIPTION], action, actual_time, trail_id)
		select o.[TYPE_ID],[NAME],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [WEB_POINT_TYPE] o where not exists (select * from [H_WEB_POINT_TYPE] h where h.TYPE_ID = o.TYPE_ID)
		
		print ' H_WAYOUT'
		
		insert into H_WAYOUT ( [ROUTE],[EXT_NUMBER],[WAYOUT_ID],[graphic], action, actual_time, trail_id)
		select o.[ROUTE],[EXT_NUMBER],[WAYOUT_ID],[graphic], 'upload', getutcdate(), @trail_id
			from [WAYOUT] o where not exists (select * from [H_WAYOUT] h where h.WAYOUT_ID = o.WAYOUT_ID)
		
		print ' H_RS_TRIP'
		
		insert into H_RS_TRIP ( [RS_TRIP_ID],[RS_VARIANT_ID],[ROUTE_TRIP_ID],[STATUS],[NAME], action, actual_time, trail_id)
		select o.[RS_TRIP_ID],[RS_VARIANT_ID],[ROUTE_TRIP_ID],[STATUS],[NAME], 'upload', getutcdate(), @trail_id
			from [RS_TRIP] o where not exists (select * from [H_RS_TRIP] h where h.RS_TRIP_ID = o.RS_TRIP_ID)
		
		print ' H_VEHICLEGROUP'
		
		insert into H_VEHICLEGROUP ( [VEHICLEGROUP_ID],[NAME],[COMMENT],[Type], action, actual_time, trail_id)
		select o.[VEHICLEGROUP_ID],[NAME],[COMMENT],[Type], 'upload', getutcdate(), @trail_id
			from [VEHICLEGROUP] o where not exists (select * from [H_VEHICLEGROUP] h where h.VEHICLEGROUP_ID = o.VEHICLEGROUP_ID)
		
		print ' H_SCHEDULERQUEUE'
		
		insert into H_SCHEDULERQUEUE ( [SCHEDULERQUEUE_ID],[SCHEDULEREVENT_ID],[NEAREST_TIME],[CONFIG_XML],[REPETITION_XML],[Vehicle_ID],[VehicleGroup_ID],[Enabled],[ErrorCount],[LastSchedulerStartDate], action, actual_time, trail_id)
		select o.[SCHEDULERQUEUE_ID],[SCHEDULEREVENT_ID],[NEAREST_TIME],[CONFIG_XML],[REPETITION_XML],[Vehicle_ID],[VehicleGroup_ID],[Enabled],[ErrorCount],[LastSchedulerStartDate], 'upload', getutcdate(), @trail_id
			from [SCHEDULERQUEUE] o where not exists (select * from [H_SCHEDULERQUEUE] h where h.SCHEDULERQUEUE_ID = o.SCHEDULERQUEUE_ID)
		
		print ' H_RS_TRIPSTYPE'
		
		insert into H_RS_TRIPSTYPE ( [RS_TRIPSTYPE_ID],[ALIAS],[LETTER],[DESCRIPTION], action, actual_time, trail_id)
		select o.[RS_TRIPSTYPE_ID],[ALIAS],[LETTER],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [RS_TRIPSTYPE] o where not exists (select * from [H_RS_TRIPSTYPE] h where h.RS_TRIPSTYPE_ID = o.RS_TRIPSTYPE_ID)
		
		print ' H_JOURNAL_VEHICLE'
		
		insert into H_JOURNAL_VEHICLE ( [JOURNAL_VEHICLE_ID],[BEGIN],[END],[VEHICLE],[STATUS], action, actual_time, trail_id)
		select o.[JOURNAL_VEHICLE_ID],[BEGIN],[END],[VEHICLE],[STATUS], 'upload', getutcdate(), @trail_id
			from [JOURNAL_VEHICLE] o where not exists (select * from [H_JOURNAL_VEHICLE] h where h.JOURNAL_VEHICLE_ID = o.JOURNAL_VEHICLE_ID)
		
		print ' H_WEB_LINE'
		
		insert into H_WEB_LINE ( [LINE_ID],[NAME],[DESCRIPTION],[OPERATOR_ID], action, actual_time, trail_id)
		select o.[LINE_ID],[NAME],[DESCRIPTION],[OPERATOR_ID], 'upload', getutcdate(), @trail_id
			from [WEB_LINE] o where not exists (select * from [H_WEB_LINE] h where h.LINE_ID = o.LINE_ID)
		
		print ' H_RS_TRIPTYPE'
		
		insert into H_RS_TRIPTYPE ( [RS_TRIPTYPE_ID],[RS_VARIANT_ID],[RS_TRIPSTYPE_ID],[TRIP_A],[TRIP_B],[CODE],[STOP_TIME],[NAME], action, actual_time, trail_id)
		select o.[RS_TRIPTYPE_ID],[RS_VARIANT_ID],[RS_TRIPSTYPE_ID],[TRIP_A],[TRIP_B],[CODE],[STOP_TIME],[NAME], 'upload', getutcdate(), @trail_id
			from [RS_TRIPTYPE] o where not exists (select * from [H_RS_TRIPTYPE] h where h.RS_TRIPTYPE_ID = o.RS_TRIPTYPE_ID)
		
		print ' H_RS_TYPE'
		
		insert into H_RS_TYPE ( [RS_TYPE_ID],[NAME], action, actual_time, trail_id)
		select o.[RS_TYPE_ID],[NAME], 'upload', getutcdate(), @trail_id
			from [RS_TYPE] o where not exists (select * from [H_RS_TYPE] h where h.RS_TYPE_ID = o.RS_TYPE_ID)
		
		print ' H_CompoundRule'
		
		insert into H_CompoundRule ( [CompoundRule_ID],[Active],[Description], action, actual_time, trail_id)
		select o.[CompoundRule_ID],[Active],[Description], 'upload', getutcdate(), @trail_id
			from [CompoundRule] o where not exists (select * from [H_CompoundRule] h where h.CompoundRule_ID = o.CompoundRule_ID)
		
		print ' H_RS_VARIANT'
		
		insert into H_RS_VARIANT ( [RS_VARIANT_ID],[RS_ID],[RS_TYPE_ID],[NAME],[STATUS],[DESCRIPTION],[GUID],[BEGIN_TIME],[START_POINT],[STOP_TIME], action, actual_time, trail_id)
		select o.[RS_VARIANT_ID],[RS_ID],[RS_TYPE_ID],[NAME],[STATUS],[DESCRIPTION],[GUID],[BEGIN_TIME],[START_POINT],[STOP_TIME], 'upload', getutcdate(), @trail_id
			from [RS_VARIANT] o where not exists (select * from [H_RS_VARIANT] h where h.RS_VARIANT_ID = o.RS_VARIANT_ID)
		
		print ' H_AsidMQF'
		
		insert into H_AsidMQF ( [AsidMQF_ID],[AsidID],[NextCheck],[Count], action, actual_time, trail_id)
		select o.[AsidMQF_ID],[AsidID],[NextCheck],[Count], 'upload', getutcdate(), @trail_id
			from [AsidMQF] o where not exists (select * from [H_AsidMQF] h where h.AsidMQF_ID = o.AsidMQF_ID)
		
		print ' H_JOURNAL_DRIVER'
		
		insert into H_JOURNAL_DRIVER ( [JOURNAL_DRIVER_ID],[BEGIN],[END],[DRIVER],[STATUS], action, actual_time, trail_id)
		select o.[JOURNAL_DRIVER_ID],[BEGIN],[END],[DRIVER],[STATUS], 'upload', getutcdate(), @trail_id
			from [JOURNAL_DRIVER] o where not exists (select * from [H_JOURNAL_DRIVER] h where h.JOURNAL_DRIVER_ID = o.JOURNAL_DRIVER_ID)
		
		print ' H_RS_WAYOUT'
		
		insert into H_RS_WAYOUT ( [RS_WAYOUT_ID],[RS_VARIANT_ID],[GRAPHIC_ID],[EXT_NUMBER],[BEGIN_TIME],[END_TIME],[INDEX],[TRIP_OUT_0],[TRIP_OUT],[TRIP_IN],[TRIP_IN_0],[WAYOUT_ID],[START_POINT], action, actual_time, trail_id)
		select o.[RS_WAYOUT_ID],[RS_VARIANT_ID],[GRAPHIC_ID],[EXT_NUMBER],[BEGIN_TIME],[END_TIME],[INDEX],[TRIP_OUT_0],[TRIP_OUT],[TRIP_IN],[TRIP_IN_0],[WAYOUT_ID],[START_POINT], 'upload', getutcdate(), @trail_id
			from [RS_WAYOUT] o where not exists (select * from [H_RS_WAYOUT] h where h.RS_WAYOUT_ID = o.RS_WAYOUT_ID)
		
		print ' H_ZONE_DISTANCE_STANDART'
		
		insert into H_ZONE_DISTANCE_STANDART ( [ZONE_DISTANCE_STANDART_ID],[ZONE_NAME_FROM],[ZONE_NAME_TO],[DISTANCE],[ZONE_FROM],[ZONE_TO], action, actual_time, trail_id)
		select o.[ZONE_DISTANCE_STANDART_ID],[ZONE_NAME_FROM],[ZONE_NAME_TO],[DISTANCE],[ZONE_FROM],[ZONE_TO], 'upload', getutcdate(), @trail_id
			from [ZONE_DISTANCE_STANDART] o where not exists (select * from [H_ZONE_DISTANCE_STANDART] h where h.ZONE_DISTANCE_STANDART_ID = o.ZONE_DISTANCE_STANDART_ID)
		
		print ' H_RS_PERIOD'
		
		insert into H_RS_PERIOD ( [PERIOD_ID],[BEGIN],[END],[RS_VARIANT_ID], action, actual_time, trail_id)
		select o.[PERIOD_ID],[BEGIN],[END],[RS_VARIANT_ID], 'upload', getutcdate(), @trail_id
			from [RS_PERIOD] o where not exists (select * from [H_RS_PERIOD] h where h.PERIOD_ID = o.PERIOD_ID)
		
		print ' H_RS_RUNTIME'
		
		insert into H_RS_RUNTIME ( [RS_RUNTIME_ID],[RS_TRIP_ID],[PERIOD_ID],[RUNTIME], action, actual_time, trail_id)
		select o.[RS_RUNTIME_ID],[RS_TRIP_ID],[PERIOD_ID],[RUNTIME], 'upload', getutcdate(), @trail_id
			from [RS_RUNTIME] o where not exists (select * from [H_RS_RUNTIME] h where h.RS_RUNTIME_ID = o.RS_RUNTIME_ID)
		
		print ' H_ROUTE_GEOZONE'
		
		insert into H_ROUTE_GEOZONE ( [ROUTE_GEOZONE_ID],[ROUTE_ID],[GEOZONE_ID],[STATUS], action, actual_time, trail_id)
		select o.[ROUTE_GEOZONE_ID],[ROUTE_ID],[GEOZONE_ID],[STATUS], 'upload', getutcdate(), @trail_id
			from [ROUTE_GEOZONE] o where not exists (select * from [H_ROUTE_GEOZONE] h where h.ROUTE_GEOZONE_ID = o.ROUTE_GEOZONE_ID)
		
		print ' H_CompoundRule_Message_Template'
		
		insert into H_CompoundRule_Message_Template ( [CompoundRule_Message_Template_ID],[CompoundRule_ID],[Message_Template_ID],[Operator_ID], action, actual_time, trail_id)
		select o.[CompoundRule_Message_Template_ID],[CompoundRule_ID],[Message_Template_ID],[Operator_ID], 'upload', getutcdate(), @trail_id
			from [CompoundRule_Message_Template] o where not exists (select * from [H_CompoundRule_Message_Template] h where h.CompoundRule_Message_Template_ID = o.CompoundRule_Message_Template_ID)
		
		print ' H_RS_STEP'
		
		insert into H_RS_STEP ( [RS_STEP_ID],[BEGIN],[END],[RS_VARIANT], action, actual_time, trail_id)
		select o.[RS_STEP_ID],[BEGIN],[END],[RS_VARIANT], 'upload', getutcdate(), @trail_id
			from [RS_STEP] o where not exists (select * from [H_RS_STEP] h where h.RS_STEP_ID = o.RS_STEP_ID)
		
		print ' H_RS_NUMBER'
		
		insert into H_RS_NUMBER ( [RS_NUMBER_ID],[RS_STEP],[NUMBER],[INTERVAL], action, actual_time, trail_id)
		select o.[RS_NUMBER_ID],[RS_STEP],[NUMBER],[INTERVAL], 'upload', getutcdate(), @trail_id
			from [RS_NUMBER] o where not exists (select * from [H_RS_NUMBER] h where h.RS_NUMBER_ID = o.RS_NUMBER_ID)
		
		print ' H_CompoundRule_ZoneGroup'
		
		insert into H_CompoundRule_ZoneGroup ( [CompoundRule_ZoneGroup_ID],[CompoundRule_ID],[ZoneGroup_ID],[Type], action, actual_time, trail_id)
		select o.[CompoundRule_ZoneGroup_ID],[CompoundRule_ID],[ZoneGroup_ID],[Type], 'upload', getutcdate(), @trail_id
			from [CompoundRule_ZoneGroup] o where not exists (select * from [H_CompoundRule_ZoneGroup] h where h.CompoundRule_ZoneGroup_ID = o.CompoundRule_ZoneGroup_ID)
		
		print ' H_BUSSTOP'
		
		insert into H_BUSSTOP ( [BUSSTOP_ID],[NAME],[SHORTNAME],[ADDRESS],[DEPARTMENT_ID], action, actual_time, trail_id)
		select o.[BUSSTOP_ID],[NAME],[SHORTNAME],[ADDRESS],[DEPARTMENT_ID], 'upload', getutcdate(), @trail_id
			from [BUSSTOP] o where not exists (select * from [H_BUSSTOP] h where h.BUSSTOP_ID = o.BUSSTOP_ID)
		
		print ' H_WAYBILLMARK'
		
		insert into H_WAYBILLMARK ( [WAYBILLMARK_ID],[WAYBILLMARK_NAME], action, actual_time, trail_id)
		select o.[WAYBILLMARK_ID],[WAYBILLMARK_NAME], 'upload', getutcdate(), @trail_id
			from [WAYBILLMARK] o where not exists (select * from [H_WAYBILLMARK] h where h.WAYBILLMARK_ID = o.WAYBILLMARK_ID)
		
		print ' H_RS_SHIFT'
		
		insert into H_RS_SHIFT ( [RS_SHIFT_ID],[RS_WAYOUT_ID],[SHIFT],[PREPARE_TIME],[BEGIN_SHIFT],[CLOSE_TIME],[BEGIN_TIME],[END_TIME],[TRIP_OUT],[TRIP_OUT_0],[TRIP_IN_0],[TRIP_IN], action, actual_time, trail_id)
		select o.[RS_SHIFT_ID],[RS_WAYOUT_ID],[SHIFT],[PREPARE_TIME],[BEGIN_SHIFT],[CLOSE_TIME],[BEGIN_TIME],[END_TIME],[TRIP_OUT],[TRIP_OUT_0],[TRIP_IN_0],[TRIP_IN], 'upload', getutcdate(), @trail_id
			from [RS_SHIFT] o where not exists (select * from [H_RS_SHIFT] h where h.RS_SHIFT_ID = o.RS_SHIFT_ID)
		
		print ' H_CompoundRule_Sensor'
		
		insert into H_CompoundRule_Sensor ( [CompoundRule_Sensor_ID],[CompoundRule_ID],[Controller_Sensor_Legend_ID],[Value],[Comparison_Type], action, actual_time, trail_id)
		select o.[CompoundRule_Sensor_ID],[CompoundRule_ID],[Controller_Sensor_Legend_ID],[Value],[Comparison_Type], 'upload', getutcdate(), @trail_id
			from [CompoundRule_Sensor] o where not exists (select * from [H_CompoundRule_Sensor] h where h.CompoundRule_Sensor_ID = o.CompoundRule_Sensor_ID)
		
		print ' H_GOODS_TYPE'
		
		insert into H_GOODS_TYPE ( [GOODS_TYPE_ID],[TYPE_NAME],[WEIGHT],[VOLUME],[MEASUREMENT_UNIT], action, actual_time, trail_id)
		select o.[GOODS_TYPE_ID],[TYPE_NAME],[WEIGHT],[VOLUME],[MEASUREMENT_UNIT], 'upload', getutcdate(), @trail_id
			from [GOODS_TYPE] o where not exists (select * from [H_GOODS_TYPE] h where h.GOODS_TYPE_ID = o.GOODS_TYPE_ID)
		
		print ' H_CompoundRule_Speed'
		
		insert into H_CompoundRule_Speed ( [CompoundRule_Speed_ID],[CompoundRule_ID],[Type],[Value], action, actual_time, trail_id)
		select o.[CompoundRule_Speed_ID],[CompoundRule_ID],[Type],[Value], 'upload', getutcdate(), @trail_id
			from [CompoundRule_Speed] o where not exists (select * from [H_CompoundRule_Speed] h where h.CompoundRule_Speed_ID = o.CompoundRule_Speed_ID)
		
		print ' H_SEATTYPE'
		
		insert into H_SEATTYPE ( [SEATTYPE_ID],[SEATTYPE_NAME], action, actual_time, trail_id)
		select o.[SEATTYPE_ID],[SEATTYPE_NAME], 'upload', getutcdate(), @trail_id
			from [SEATTYPE] o where not exists (select * from [H_SEATTYPE] h where h.SEATTYPE_ID = o.SEATTYPE_ID)
		
		print ' H_OWNER'
		
		insert into H_OWNER ( [OWNER_ID],[FIRST_NAME],[LAST_NAME],[SECOND_NAME],[PHONE_NUMBER1],[PHONE_NUMBER2],[PHONE_NUMBER3], action, actual_time, trail_id)
		select o.[OWNER_ID],[FIRST_NAME],[LAST_NAME],[SECOND_NAME],[PHONE_NUMBER1],[PHONE_NUMBER2],[PHONE_NUMBER3], 'upload', getutcdate(), @trail_id
			from [OWNER] o where not exists (select * from [H_OWNER] h where h.OWNER_ID = o.OWNER_ID)
		
		print ' H_CompoundRule_DataAbsence'
		
		insert into H_CompoundRule_DataAbsence ( [CompoundRule_DataAbsence_ID],[CompoundRule_ID],[Minutes], action, actual_time, trail_id)
		select o.[CompoundRule_DataAbsence_ID],[CompoundRule_ID],[Minutes], 'upload', getutcdate(), @trail_id
			from [CompoundRule_DataAbsence] o where not exists (select * from [H_CompoundRule_DataAbsence] h where h.CompoundRule_DataAbsence_ID = o.CompoundRule_DataAbsence_ID)
		
		print ' H_VEHICLEGROUP_RULE'
		
		insert into H_VEHICLEGROUP_RULE ( [VEHICLEGROUP_RULE_ID],[VEHICLEGROUP_ID],[RULE_ID],[VALUE],[XML_Condition],[XML_Action],[Enabled], action, actual_time, trail_id)
		select o.[VEHICLEGROUP_RULE_ID],[VEHICLEGROUP_ID],[RULE_ID],[VALUE],[XML_Condition],[XML_Action],[Enabled], 'upload', getutcdate(), @trail_id
			from [VEHICLEGROUP_RULE] o where not exists (select * from [H_VEHICLEGROUP_RULE] h where h.VEHICLEGROUP_RULE_ID = o.VEHICLEGROUP_RULE_ID)
		
		print ' H_VEHICLE_OWNER'
		
		insert into H_VEHICLE_OWNER ( [VEHICLE_OWNER_ID],[OWNER_ID],[VEHICLE_ID],[OWNER_NUMBER], action, actual_time, trail_id)
		select o.[VEHICLE_OWNER_ID],[OWNER_ID],[VEHICLE_ID],[OWNER_NUMBER], 'upload', getutcdate(), @trail_id
			from [VEHICLE_OWNER] o where not exists (select * from [H_VEHICLE_OWNER] h where h.VEHICLE_OWNER_ID = o.VEHICLE_OWNER_ID)
		
		print ' H_SensorValueChange'
		
		insert into H_SensorValueChange ( [SensorValueChange_ID],[Controller_Sensor_Legend_ID],[Log_Time],[Value],[Vehicle_ID], action, actual_time, trail_id)
		select o.[SensorValueChange_ID],[Controller_Sensor_Legend_ID],[Log_Time],[Value],[Vehicle_ID], 'upload', getutcdate(), @trail_id
			from [SensorValueChange] o where not exists (select * from [H_SensorValueChange] h where h.SensorValueChange_ID = o.SensorValueChange_ID)
		
		print ' H_LOGISTIC_ADDRESS'
		
		insert into H_LOGISTIC_ADDRESS ( [LOGISTIC_ADDRESS_ID],[CONTRACTOR_ID],[ADDRESS],[GEOZONE_ID],[CONTRACTOR_CALENDAR_ID], action, actual_time, trail_id)
		select o.[LOGISTIC_ADDRESS_ID],[CONTRACTOR_ID],[ADDRESS],[GEOZONE_ID],[CONTRACTOR_CALENDAR_ID], 'upload', getutcdate(), @trail_id
			from [LOGISTIC_ADDRESS] o where not exists (select * from [H_LOGISTIC_ADDRESS] h where h.LOGISTIC_ADDRESS_ID = o.LOGISTIC_ADDRESS_ID)
		
		print ' H_OPERATOR_PROFILE'
		
		insert into H_OPERATOR_PROFILE ( [OPERATOR_ID],[PROFILE],[OPERATOR_PROFILE_ID], action, actual_time, trail_id)
		select o.[OPERATOR_ID],[PROFILE],[OPERATOR_PROFILE_ID], 'upload', getutcdate(), @trail_id
			from [OPERATOR_PROFILE] o where not exists (select * from [H_OPERATOR_PROFILE] h where h.OPERATOR_PROFILE_ID = o.OPERATOR_PROFILE_ID)
		
		print ' H_Phone'
		
		insert into H_Phone ( [Phone_ID],[Phone],[Name],[Comment],[Operator_ID],[Confirmed],[InsertDate],[ConfirmDate], action, actual_time, trail_id)
		select o.[Phone_ID],[Phone],[Name],[Comment],[Operator_ID],[Confirmed],[InsertDate],[ConfirmDate], 'upload', getutcdate(), @trail_id
			from [Phone] o where not exists (select * from [H_Phone] h where h.Phone_ID = o.Phone_ID)
		
		print ' H_CompoundRule_Zone'
		
		insert into H_CompoundRule_Zone ( [CompoundRule_Zone_ID],[CompoundRule_ID],[Zone_ID],[Type], action, actual_time, trail_id)
		select o.[CompoundRule_Zone_ID],[CompoundRule_ID],[Zone_ID],[Type], 'upload', getutcdate(), @trail_id
			from [CompoundRule_Zone] o where not exists (select * from [H_CompoundRule_Zone] h where h.CompoundRule_Zone_ID = o.CompoundRule_Zone_ID)
		
		print ' H_EMAIL'
		
		insert into H_EMAIL ( [EMAIL_ID],[EMAIL],[NAME],[COMMENT],[Operator_ID],[Confirmed],[InsertDate],[ConfirmDate],[IsForPasswordRestore], action, actual_time, trail_id)
		select o.[EMAIL_ID],[EMAIL],[NAME],[COMMENT],[Operator_ID],[Confirmed],[InsertDate],[ConfirmDate],[IsForPasswordRestore], 'upload', getutcdate(), @trail_id
			from [EMAIL] o where not exists (select * from [H_EMAIL] h where h.EMAIL_ID = o.EMAIL_ID)
		
		print ' H_ZONE_VEHICLE'
		
		insert into H_ZONE_VEHICLE ( [ZONE_VEHICLE_ID],[ZONE_ID],[VEHICLE_ID], action, actual_time, trail_id)
		select o.[ZONE_VEHICLE_ID],[ZONE_ID],[VEHICLE_ID], 'upload', getutcdate(), @trail_id
			from [ZONE_VEHICLE] o where not exists (select * from [H_ZONE_VEHICLE] h where h.ZONE_VEHICLE_ID = o.ZONE_VEHICLE_ID)
		
		print ' H_GEO_SEGMENT_RUNTIME'
		
		insert into H_GEO_SEGMENT_RUNTIME ( [GEO_SEGMENT_RUNTIME_ID],[GEO_SEGMENT_ID],[DAY_TIME_ID],[RUNTIME], action, actual_time, trail_id)
		select o.[GEO_SEGMENT_RUNTIME_ID],[GEO_SEGMENT_ID],[DAY_TIME_ID],[RUNTIME], 'upload', getutcdate(), @trail_id
			from [GEO_SEGMENT_RUNTIME] o where not exists (select * from [H_GEO_SEGMENT_RUNTIME] h where h.GEO_SEGMENT_RUNTIME_ID = o.GEO_SEGMENT_RUNTIME_ID)
		
		print ' H_ZONE_VEHICLEGROUP'
		
		insert into H_ZONE_VEHICLEGROUP ( [ZONE_VEHICLEGROUP_ID],[ZONE_ID],[VEHICLEGROUP_ID], action, actual_time, trail_id)
		select o.[ZONE_VEHICLEGROUP_ID],[ZONE_ID],[VEHICLEGROUP_ID], 'upload', getutcdate(), @trail_id
			from [ZONE_VEHICLEGROUP] o where not exists (select * from [H_ZONE_VEHICLEGROUP] h where h.ZONE_VEHICLEGROUP_ID = o.ZONE_VEHICLEGROUP_ID)
		
		print ' H_VehicleControlDate'
		
		insert into H_VehicleControlDate ( [VehicleControlDate_ID],[Type_ID],[Value],[Vehicle_ID], action, actual_time, trail_id)
		select o.[VehicleControlDate_ID],[Type_ID],[Value],[Vehicle_ID], 'upload', getutcdate(), @trail_id
			from [VehicleControlDate] o where not exists (select * from [H_VehicleControlDate] h where h.VehicleControlDate_ID = o.VehicleControlDate_ID)
		
		print ' H_LOGISTIC_ORDER'
		
		insert into H_LOGISTIC_ORDER ( [LOGISTIC_ORDER_ID],[NUMBER],[CREATE_DATE],[LOGISTIC_ADDRESS_ID],[CONTRACTOR_ID],[ORDER_TIME_FROM],[ORDER_TIME_TO],[BLADING_TYPE_ID],[PLANNING_SHIPMENT_TIME], action, actual_time, trail_id)
		select o.[LOGISTIC_ORDER_ID],[NUMBER],[CREATE_DATE],[LOGISTIC_ADDRESS_ID],[CONTRACTOR_ID],[ORDER_TIME_FROM],[ORDER_TIME_TO],[BLADING_TYPE_ID],[PLANNING_SHIPMENT_TIME], 'upload', getutcdate(), @trail_id
			from [LOGISTIC_ORDER] o where not exists (select * from [H_LOGISTIC_ORDER] h where h.LOGISTIC_ORDER_ID = o.LOGISTIC_ORDER_ID)
		
		print ' H_GEO_SEGMENT'
		
		insert into H_GEO_SEGMENT ( [GEO_SEGMENT_ID],[GEO_TRIP_ID],[LENGTH],[RUNTIME],[STOP_TIME],[ORDER],[POINT],[VERTEX],[FACTOR_ID],[STATUS],[POINTTO], action, actual_time, trail_id)
		select o.[GEO_SEGMENT_ID],[GEO_TRIP_ID],[LENGTH],[RUNTIME],[STOP_TIME],[ORDER],[POINT],[VERTEX],[FACTOR_ID],[STATUS],[POINTTO], 'upload', getutcdate(), @trail_id
			from [GEO_SEGMENT] o where not exists (select * from [H_GEO_SEGMENT] h where h.GEO_SEGMENT_ID = o.GEO_SEGMENT_ID)
		
		print ' H_JOURNAL'
		
		insert into H_JOURNAL ( [JOURNAL_ID],[BEGIN],[END],[DRIVER],[VEHICLE],[STATUS],[WAYOUT],[REPLACED_DRIVER], action, actual_time, trail_id)
		select o.[JOURNAL_ID],[BEGIN],[END],[DRIVER],[VEHICLE],[STATUS],[WAYOUT],[REPLACED_DRIVER], 'upload', getutcdate(), @trail_id
			from [JOURNAL] o where not exists (select * from [H_JOURNAL] h where h.JOURNAL_ID = o.JOURNAL_ID)
		
		print ' H_GOODS_LOGISTIC_ORDER'
		
		insert into H_GOODS_LOGISTIC_ORDER ( [GOODS_LOGISTIC_ORDER_ID],[LOGISTIC_ORDER_ID],[GOODS_ID],[NUMBER], action, actual_time, trail_id)
		select o.[GOODS_LOGISTIC_ORDER_ID],[LOGISTIC_ORDER_ID],[GOODS_ID],[NUMBER], 'upload', getutcdate(), @trail_id
			from [GOODS_LOGISTIC_ORDER] o where not exists (select * from [H_GOODS_LOGISTIC_ORDER] h where h.GOODS_LOGISTIC_ORDER_ID = o.GOODS_LOGISTIC_ORDER_ID)
		
		print ' H_CompoundRule_PositionAbsence'
		
		insert into H_CompoundRule_PositionAbsence ( [CompoundRule_PositionAbsence_ID],[CompoundRule_ID],[Minutes], action, actual_time, trail_id)
		select o.[CompoundRule_PositionAbsence_ID],[CompoundRule_ID],[Minutes], 'upload', getutcdate(), @trail_id
			from [CompoundRule_PositionAbsence] o where not exists (select * from [H_CompoundRule_PositionAbsence] h where h.CompoundRule_PositionAbsence_ID = o.CompoundRule_PositionAbsence_ID)
		
		print ' H_FACTORS'
		
		insert into H_FACTORS ( [FACTOR_ID],[COMMENTS], action, actual_time, trail_id)
		select o.[FACTOR_ID],[COMMENTS], 'upload', getutcdate(), @trail_id
			from [FACTORS] o where not exists (select * from [H_FACTORS] h where h.FACTOR_ID = o.FACTOR_ID)
		
		print ' H_CompoundRule_Time'
		
		insert into H_CompoundRule_Time ( [CompoundRule_Time_ID],[CompoundRule_ID],[TimeZone_ID],[Hours_From],[Minutes_From],[Hours_To],[Minutes_To], action, actual_time, trail_id)
		select o.[CompoundRule_Time_ID],[CompoundRule_ID],[TimeZone_ID],[Hours_From],[Minutes_From],[Hours_To],[Minutes_To], 'upload', getutcdate(), @trail_id
			from [CompoundRule_Time] o where not exists (select * from [H_CompoundRule_Time] h where h.CompoundRule_Time_ID = o.CompoundRule_Time_ID)
		
		print ' H_SEASON'
		
		insert into H_SEASON ( [SEASON_ID],[NAME],[TIME_BEGIN],[TIME_END], action, actual_time, trail_id)
		select o.[SEASON_ID],[NAME],[TIME_BEGIN],[TIME_END], 'upload', getutcdate(), @trail_id
			from [SEASON] o where not exists (select * from [H_SEASON] h where h.SEASON_ID = o.SEASON_ID)
		
		print ' H_Asid'
		
		insert into H_Asid ( [ID],[Value],[AllowMlpRequest],[AllowSecurityAdministration],[WarnAboutLocation],[Operator_ID],[Contract_Number],[Terminal_Device_Number],[Billing_Service_Provider_ID], action, actual_time, trail_id)
		select o.[ID],[Value],[AllowMlpRequest],[AllowSecurityAdministration],[WarnAboutLocation],[Operator_ID],[Contract_Number],[Terminal_Device_Number],[Billing_Service_Provider_ID], 'upload', getutcdate(), @trail_id
			from [Asid] o where not exists (select * from [H_Asid] h where h.ID = o.ID)
		
		print ' H_VEHICLE_RULE'
		
		insert into H_VEHICLE_RULE ( [VEHICLE_RULE_ID],[VEHICLE_ID],[RULE_ID],[VALUE],[XML_Condition],[XML_Action],[Enabled], action, actual_time, trail_id)
		select o.[VEHICLE_RULE_ID],[VEHICLE_ID],[RULE_ID],[VALUE],[XML_Condition],[XML_Action],[Enabled], 'upload', getutcdate(), @trail_id
			from [VEHICLE_RULE] o where not exists (select * from [H_VEHICLE_RULE] h where h.VEHICLE_RULE_ID = o.VEHICLE_RULE_ID)
		
		print ' H_PERIOD'
		
		insert into H_PERIOD ( [PERIOD_ID],[NAME],[ROUTE],[DAY_KIND],[DK_SET],[BEGIN],[END],[COLOR],[HOLIDAY],[SEASON_ID],[COMMENT], action, actual_time, trail_id)
		select o.[PERIOD_ID],[NAME],[ROUTE],[DAY_KIND],[DK_SET],[BEGIN],[END],[COLOR],[HOLIDAY],[SEASON_ID],[COMMENT], 'upload', getutcdate(), @trail_id
			from [PERIOD] o where not exists (select * from [H_PERIOD] h where h.PERIOD_ID = o.PERIOD_ID)
		
		print ' H_DAY_TYPE'
		
		insert into H_DAY_TYPE ( [DAY_TYPE_ID],[NAME],[WEEKDAYS], action, actual_time, trail_id)
		select o.[DAY_TYPE_ID],[NAME],[WEEKDAYS], 'upload', getutcdate(), @trail_id
			from [DAY_TYPE] o where not exists (select * from [H_DAY_TYPE] h where h.DAY_TYPE_ID = o.DAY_TYPE_ID)
		
		print ' H_ROUTE'
		
		insert into H_ROUTE ( [ROUTE_ID],[EXT_NUMBER],[LENGTH],[DESCRIPTION],[COLOR],[VERTEX],[PREFIX],[DIGIT_COUNT],[TIME_BEGIN],[TIME_END],[TIME_DEVIATION],[DEVIATION], action, actual_time, trail_id)
		select o.[ROUTE_ID],[EXT_NUMBER],[LENGTH],[DESCRIPTION],[COLOR],[VERTEX],[PREFIX],[DIGIT_COUNT],[TIME_BEGIN],[TIME_END],[TIME_DEVIATION],[DEVIATION], 'upload', getutcdate(), @trail_id
			from [ROUTE] o where not exists (select * from [H_ROUTE] h where h.ROUTE_ID = o.ROUTE_ID)
		
		print ' H_DAY_TIME'
		
		insert into H_DAY_TIME ( [DAY_TIME_ID],[TIME_BEGIN],[TIME_END],[DAY_TYPE_ID], action, actual_time, trail_id)
		select o.[DAY_TIME_ID],[TIME_BEGIN],[TIME_END],[DAY_TYPE_ID], 'upload', getutcdate(), @trail_id
			from [DAY_TIME] o where not exists (select * from [H_DAY_TIME] h where h.DAY_TIME_ID = o.DAY_TIME_ID)
		
		print ' H_SCHEDULE'
		
		insert into H_SCHEDULE ( [SCHEDULE_ID],[ROUTE_ID],[DAY_KIND_ID],[EXT_NUMBER],[COMMENT],[BEGIN_TIME],[END_TIME],[WAYOUT], action, actual_time, trail_id)
		select o.[SCHEDULE_ID],[ROUTE_ID],[DAY_KIND_ID],[EXT_NUMBER],[COMMENT],[BEGIN_TIME],[END_TIME],[WAYOUT], 'upload', getutcdate(), @trail_id
			from [SCHEDULE] o where not exists (select * from [H_SCHEDULE] h where h.SCHEDULE_ID = o.SCHEDULE_ID)
		
		print ' H_CONTROLLER_SENSOR'
		
		insert into H_CONTROLLER_SENSOR ( [CONTROLLER_SENSOR_ID],[CONTROLLER_TYPE_ID],[CONTROLLER_SENSOR_TYPE_ID],[NUMBER],[MAX_VALUE],[MIN_VALUE],[DEFAULT_VALUE],[BITS],[Descript],[VALUE_EXPIRED],[Default_Sensor_Legend_ID],[Default_Multiplier],[Default_Constant],[Mandatory], action, actual_time, trail_id)
		select o.[CONTROLLER_SENSOR_ID],[CONTROLLER_TYPE_ID],[CONTROLLER_SENSOR_TYPE_ID],[NUMBER],[MAX_VALUE],[MIN_VALUE],[DEFAULT_VALUE],[BITS],[Descript],[VALUE_EXPIRED],[Default_Sensor_Legend_ID],[Default_Multiplier],[Default_Constant],[Mandatory], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_SENSOR] o where not exists (select * from [H_CONTROLLER_SENSOR] h where h.CONTROLLER_SENSOR_ID = o.CONTROLLER_SENSOR_ID)
		
		print ' H_CONTROLLER_TYPE'
		
		insert into H_CONTROLLER_TYPE ( [CONTROLLER_TYPE_ID],[TYPE_NAME],[PACKET_LENGTH],[POS_ABSENCE_SUPPORT],[POS_SMOOTH_SUPPORT],[AllowedToAddByCustomer], action, actual_time, trail_id)
		select o.[CONTROLLER_TYPE_ID],[TYPE_NAME],[PACKET_LENGTH],[POS_ABSENCE_SUPPORT],[POS_SMOOTH_SUPPORT],[AllowedToAddByCustomer], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_TYPE] o where not exists (select * from [H_CONTROLLER_TYPE] h where h.CONTROLLER_TYPE_ID = o.CONTROLLER_TYPE_ID)
		
		print ' H_SCHEDULE_DETAIL'
		
		insert into H_SCHEDULE_DETAIL ( [SCHEDULE_DETAIL_ID],[SCHEDULE_ID],[SHIFT_ID],[RUN_FULL],[RUN_EMPTY],[BEGIN_TIME],[END_TIME], action, actual_time, trail_id)
		select o.[SCHEDULE_DETAIL_ID],[SCHEDULE_ID],[SHIFT_ID],[RUN_FULL],[RUN_EMPTY],[BEGIN_TIME],[END_TIME], 'upload', getutcdate(), @trail_id
			from [SCHEDULE_DETAIL] o where not exists (select * from [H_SCHEDULE_DETAIL] h where h.SCHEDULE_DETAIL_ID = o.SCHEDULE_DETAIL_ID)
		
		print ' H_CONTROLLER_SENSOR_LEGEND'
		
		insert into H_CONTROLLER_SENSOR_LEGEND ( [CONTROLLER_SENSOR_LEGEND_ID],[CONTROLLER_SENSOR_TYPE_ID],[NAME],[Number],[VALUE_EXPIRED], action, actual_time, trail_id)
		select o.[CONTROLLER_SENSOR_LEGEND_ID],[CONTROLLER_SENSOR_TYPE_ID],[NAME],[Number],[VALUE_EXPIRED], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_SENSOR_LEGEND] o where not exists (select * from [H_CONTROLLER_SENSOR_LEGEND] h where h.CONTROLLER_SENSOR_LEGEND_ID = o.CONTROLLER_SENSOR_LEGEND_ID)
		
		print ' H_SCHEDULEREVENT'
		
		insert into H_SCHEDULEREVENT ( [SCHEDULEREVENT_ID],[PLUGIN_NAME],[COMMENT],[CONFIG_XML],[Report_Id], action, actual_time, trail_id)
		select o.[SCHEDULEREVENT_ID],[PLUGIN_NAME],[COMMENT],[CONFIG_XML],[Report_Id], 'upload', getutcdate(), @trail_id
			from [SCHEDULEREVENT] o where not exists (select * from [H_SCHEDULEREVENT] h where h.SCHEDULEREVENT_ID = o.SCHEDULEREVENT_ID)
		
		print ' H_CONTROLLER_SENSOR_MAP'
		
		insert into H_CONTROLLER_SENSOR_MAP ( [CONTROLLER_SENSOR_MAP_ID],[CONTROLLER_ID],[CONTROLLER_SENSOR_ID],[CONTROLLER_SENSOR_LEGEND_ID],[DESCRIPTION],[Multiplier],[Constant],[Min_Value],[Max_Value],[Error], action, actual_time, trail_id)
		select o.[CONTROLLER_SENSOR_MAP_ID],[CONTROLLER_ID],[CONTROLLER_SENSOR_ID],[CONTROLLER_SENSOR_LEGEND_ID],[DESCRIPTION],[Multiplier],[Constant],[Min_Value],[Max_Value],[Error], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_SENSOR_MAP] o where not exists (select * from [H_CONTROLLER_SENSOR_MAP] h where h.CONTROLLER_SENSOR_MAP_ID = o.CONTROLLER_SENSOR_MAP_ID)
		
		print ' H_CONTROLLER_SENSOR_TYPE'
		
		insert into H_CONTROLLER_SENSOR_TYPE ( [CONTROLLER_SENSOR_TYPE_ID],[NAME], action, actual_time, trail_id)
		select o.[CONTROLLER_SENSOR_TYPE_ID],[NAME], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_SENSOR_TYPE] o where not exists (select * from [H_CONTROLLER_SENSOR_TYPE] h where h.CONTROLLER_SENSOR_TYPE_ID = o.CONTROLLER_SENSOR_TYPE_ID)
		
		print ' H_DK_SET'
		
		insert into H_DK_SET ( [DK_SET_ID],[NAME],[COMMENT],[DEPARTMENT_ID], action, actual_time, trail_id)
		select o.[DK_SET_ID],[NAME],[COMMENT],[DEPARTMENT_ID], 'upload', getutcdate(), @trail_id
			from [DK_SET] o where not exists (select * from [H_DK_SET] h where h.DK_SET_ID = o.DK_SET_ID)
		
		print ' H_DAY_KIND'
		
		insert into H_DAY_KIND ( [DAY_KIND_ID],[NAME],[DK_SET],[WEEKDAYS], action, actual_time, trail_id)
		select o.[DAY_KIND_ID],[NAME],[DK_SET],[WEEKDAYS], 'upload', getutcdate(), @trail_id
			from [DAY_KIND] o where not exists (select * from [H_DAY_KIND] h where h.DAY_KIND_ID = o.DAY_KIND_ID)
		
		print ' H_FACTOR_VALUES'
		
		insert into H_FACTOR_VALUES ( [FACTOR_VALUES_ID],[FACTOR],[RUNTIME],[SEASON_ID],[DAY_TYPE_ID],[DAY_TIME_ID],[FACTOR_ID], action, actual_time, trail_id)
		select o.[FACTOR_VALUES_ID],[FACTOR],[RUNTIME],[SEASON_ID],[DAY_TYPE_ID],[DAY_TIME_ID],[FACTOR_ID], 'upload', getutcdate(), @trail_id
			from [FACTOR_VALUES] o where not exists (select * from [H_FACTOR_VALUES] h where h.FACTOR_VALUES_ID = o.FACTOR_VALUES_ID)
		
		print ' H_DOTNET_TYPE'
		
		insert into H_DOTNET_TYPE ( [DOTNET_TYPE_ID],[TYPE_NAME], action, actual_time, trail_id)
		select o.[DOTNET_TYPE_ID],[TYPE_NAME], 'upload', getutcdate(), @trail_id
			from [DOTNET_TYPE] o where not exists (select * from [H_DOTNET_TYPE] h where h.DOTNET_TYPE_ID = o.DOTNET_TYPE_ID)
		
		print ' H_WAYBILL_HEADER'
		
		insert into H_WAYBILL_HEADER ( [WAYBILL_HEADER_ID],[SCHEDULE_ID],[WAYBILL_DATE],[VEHICLE_ID],[CANCELLED],[COMMENT],[EXT_NUMBER],[PRINTED],[PRINTREASON],[INITIAL_TRIP_KIND],[TRAILOR1],[TRAILOR2], action, actual_time, trail_id)
		select o.[WAYBILL_HEADER_ID],[SCHEDULE_ID],[WAYBILL_DATE],[VEHICLE_ID],[CANCELLED],[COMMENT],[EXT_NUMBER],[PRINTED],[PRINTREASON],[INITIAL_TRIP_KIND],[TRAILOR1],[TRAILOR2], 'upload', getutcdate(), @trail_id
			from [WAYBILL_HEADER] o where not exists (select * from [H_WAYBILL_HEADER] h where h.WAYBILL_HEADER_ID = o.WAYBILL_HEADER_ID)
		
		print ' H_CONTROLLER'
		
		insert into H_CONTROLLER ( [CONTROLLER_ID],[CONTROLLER_TYPE_ID],[VEHICLE_ID],[PHONE],[NUMBER],[FIRMWARE], action, actual_time, trail_id)
		select o.[CONTROLLER_ID],[CONTROLLER_TYPE_ID],[VEHICLE_ID],[PHONE],[NUMBER],[FIRMWARE], 'upload', getutcdate(), @trail_id
			from [CONTROLLER] o where not exists (select * from [H_CONTROLLER] h where h.CONTROLLER_ID = o.CONTROLLER_ID)
		
		print ' H_Billing_Service'
		
		insert into H_Billing_Service ( [ID],[Asid_ID],[Billing_Service_Type_ID],[StartDate],[Department_ID],[Operator_ID],[Vehicle_ID],[MaxQuantity],[RenderedQuantity],[RenderedLastTime],[MinIntervalSeconds], action, actual_time, trail_id)
		select o.[ID],[Asid_ID],[Billing_Service_Type_ID],[StartDate],[Department_ID],[Operator_ID],[Vehicle_ID],[MaxQuantity],[RenderedQuantity],[RenderedLastTime],[MinIntervalSeconds], 'upload', getutcdate(), @trail_id
			from [Billing_Service] o where not exists (select * from [H_Billing_Service] h where h.ID = o.ID)
		
		print ' H_DRIVER_BONUS'
		
		insert into H_DRIVER_BONUS ( [driver_bonus_id],[time_delta],[bonus_percentage], action, actual_time, trail_id)
		select o.[driver_bonus_id],[time_delta],[bonus_percentage], 'upload', getutcdate(), @trail_id
			from [DRIVER_BONUS] o where not exists (select * from [H_DRIVER_BONUS] h where h.driver_bonus_id = o.driver_bonus_id)
		
		print ' H_DRIVERGROUP'
		
		insert into H_DRIVERGROUP ( [DRIVERGROUP_ID],[NAME],[COMMENT], action, actual_time, trail_id)
		select o.[DRIVERGROUP_ID],[NAME],[COMMENT], 'upload', getutcdate(), @trail_id
			from [DRIVERGROUP] o where not exists (select * from [H_DRIVERGROUP] h where h.DRIVERGROUP_ID = o.DRIVERGROUP_ID)
		
		print ' H_TRIP'
		
		insert into H_TRIP ( [TRIP_ID],[TRIP_KIND_ID],[BEGIN_TIME],[END_TIME],[SCHEDULE_DETAIL_ID],[TRIP],[ROUTE_TRIP_ID], action, actual_time, trail_id)
		select o.[TRIP_ID],[TRIP_KIND_ID],[BEGIN_TIME],[END_TIME],[SCHEDULE_DETAIL_ID],[TRIP],[ROUTE_TRIP_ID], 'upload', getutcdate(), @trail_id
			from [TRIP] o where not exists (select * from [H_TRIP] h where h.TRIP_ID = o.TRIP_ID)
		
		print ' H_Billing_Blocking'
		
		insert into H_Billing_Blocking ( [ID],[Asid_ID],[BlockingDate], action, actual_time, trail_id)
		select o.[ID],[Asid_ID],[BlockingDate], 'upload', getutcdate(), @trail_id
			from [Billing_Blocking] o where not exists (select * from [H_Billing_Blocking] h where h.ID = o.ID)
		
		print ' H_ZONEGROUP'
		
		insert into H_ZONEGROUP ( [ZONEGROUP_ID],[NAME],[DESCRIPTION], action, actual_time, trail_id)
		select o.[ZONEGROUP_ID],[NAME],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [ZONEGROUP] o where not exists (select * from [H_ZONEGROUP] h where h.ZONEGROUP_ID = o.ZONEGROUP_ID)
		
		print ' H_WAYBILL'
		
		insert into H_WAYBILL ( [WAYBILL_ID],[DRIVER_ID],[COMMENT],[BEGIN_TIME],[END_TIME],[SHIFT_ID],[RELEASED],[ACTIVE],[DESTINATION],[RESERVE],[WAYBILL_DATE],[END_TIME_PLANNED],[TOTAL_PENALTY],[TRAINEE], action, actual_time, trail_id)
		select o.[WAYBILL_ID],[DRIVER_ID],[COMMENT],[BEGIN_TIME],[END_TIME],[SHIFT_ID],[RELEASED],[ACTIVE],[DESTINATION],[RESERVE],[WAYBILL_DATE],[END_TIME_PLANNED],[TOTAL_PENALTY],[TRAINEE], 'upload', getutcdate(), @trail_id
			from [WAYBILL] o where not exists (select * from [H_WAYBILL] h where h.WAYBILL_ID = o.WAYBILL_ID)
		
		print ' H_ZONEGROUP_ZONE'
		
		insert into H_ZONEGROUP_ZONE ( [ZONEGROUP_ZONE_ID],[ZONEGROUP_ID],[ZONE_ID], action, actual_time, trail_id)
		select o.[ZONEGROUP_ZONE_ID],[ZONEGROUP_ID],[ZONE_ID], 'upload', getutcdate(), @trail_id
			from [ZONEGROUP_ZONE] o where not exists (select * from [H_ZONEGROUP_ZONE] h where h.ZONEGROUP_ZONE_ID = o.ZONEGROUP_ZONE_ID)
		
		print ' H_GEO_ZONE'
		
		insert into H_GEO_ZONE ( [ZONE_ID],[NAME],[COLOR],[DESCRIPTION], action, actual_time, trail_id)
		select o.[ZONE_ID],[NAME],[COLOR],[DESCRIPTION], 'upload', getutcdate(), @trail_id
			from [GEO_ZONE] o where not exists (select * from [H_GEO_ZONE] h where h.ZONE_ID = o.ZONE_ID)
		
		print ' H_MESSAGE_TEMPLATE'
		
		insert into H_MESSAGE_TEMPLATE ( [MESSAGE_TEMPLATE_ID],[NAME],[HEADER],[BODY], action, actual_time, trail_id)
		select o.[MESSAGE_TEMPLATE_ID],[NAME],[HEADER],[BODY], 'upload', getutcdate(), @trail_id
			from [MESSAGE_TEMPLATE] o where not exists (select * from [H_MESSAGE_TEMPLATE] h where h.MESSAGE_TEMPLATE_ID = o.MESSAGE_TEMPLATE_ID)
		
		print ' H_DRIVER'
		
		insert into H_DRIVER ( [DRIVER_ID],[EXT_NUMBER],[NAME],[SHORT_NAME],[DATE_OF_BIRTH],[COMMENT],[CITY_ONLY],[DRIVER_STATUS_ID],[FIRED_DATE],[DEPARTMENT],[PHONE], action, actual_time, trail_id)
		select o.[DRIVER_ID],[EXT_NUMBER],[NAME],[SHORT_NAME],[DATE_OF_BIRTH],[COMMENT],[CITY_ONLY],[DRIVER_STATUS_ID],[FIRED_DATE],[DEPARTMENT],[PHONE], 'upload', getutcdate(), @trail_id
			from [DRIVER] o where not exists (select * from [H_DRIVER] h where h.DRIVER_ID = o.DRIVER_ID)
		
		print ' H_MLP_Controller'
		
		insert into H_MLP_Controller ( [ID],[Controller_ID],[Asid_ID], action, actual_time, trail_id)
		select o.[ID],[Controller_ID],[Asid_ID], 'upload', getutcdate(), @trail_id
			from [MLP_Controller] o where not exists (select * from [H_MLP_Controller] h where h.ID = o.ID)
		
		print ' H_MESSAGE'
		
		insert into H_MESSAGE ( [MESSAGE_ID],[TEMPLATE_ID],[SOURCE_ID],[SEVERITY_ID],[POSITION_ID],[SUBJECT],[BODY],[STATUS],[TIME],[DESTINATION_ID],[TYPE],[GROUP],[SystemId],[SourceType_ID],[DestinationType_ID],[ErrorCount], action, actual_time, trail_id)
		select o.[MESSAGE_ID],[TEMPLATE_ID],[SOURCE_ID],[SEVERITY_ID],[POSITION_ID],[SUBJECT],[BODY],[STATUS],[TIME],[DESTINATION_ID],[TYPE],[GROUP],[SystemId],[SourceType_ID],[DestinationType_ID],[ErrorCount], 'upload', getutcdate(), @trail_id
			from [MESSAGE] o where not exists (select * from [H_MESSAGE] h where h.MESSAGE_ID = o.MESSAGE_ID)
		
		print ' H_MESSAGE_TEMPLATE_FIELD'
		
		insert into H_MESSAGE_TEMPLATE_FIELD ( [MESSAGE_TEMPLATE_FIELD_ID],[MESSAGE_TEMPLATE_ID],[DOTNET_TYPE_ID],[NAME], action, actual_time, trail_id)
		select o.[MESSAGE_TEMPLATE_FIELD_ID],[MESSAGE_TEMPLATE_ID],[DOTNET_TYPE_ID],[NAME], 'upload', getutcdate(), @trail_id
			from [MESSAGE_TEMPLATE_FIELD] o where not exists (select * from [H_MESSAGE_TEMPLATE_FIELD] h where h.MESSAGE_TEMPLATE_FIELD_ID = o.MESSAGE_TEMPLATE_FIELD_ID)
		
		print ' H_VEHICLE'
		
		insert into H_VEHICLE ( [VEHICLE_ID],[PUBLIC_NUMBER],[GARAGE_NUMBER],[VEHICLE_TYPE],[COMMENT],[VEHICLE_STATUS_ID],[OWNER_ID],[VALIDATOR_PRESENT],[SEATTYPE_ID],[SITTING_BERTHS_COUNT],[STANDING_BERTHS_COUNT],[GUARD],[FUEL_TANK],[GRAPHIC],[GRAPHIC_BEGIN],[TIME_PREPARATION],[VEHICLE_KIND_ID],[VEHICLE_REMOVED_DATE],[DEPARTMENT],[VIN],[Fuel_Type_ID],[FuelSpendStandardKilometer],[FuelSpendStandardLiter], action, actual_time, trail_id)
		select o.[VEHICLE_ID],[PUBLIC_NUMBER],[GARAGE_NUMBER],[VEHICLE_TYPE],[COMMENT],[VEHICLE_STATUS_ID],[OWNER_ID],[VALIDATOR_PRESENT],[SEATTYPE_ID],[SITTING_BERTHS_COUNT],[STANDING_BERTHS_COUNT],[GUARD],[FUEL_TANK],[GRAPHIC],[GRAPHIC_BEGIN],[TIME_PREPARATION],[VEHICLE_KIND_ID],[VEHICLE_REMOVED_DATE],[DEPARTMENT],[VIN],[Fuel_Type_ID],[FuelSpendStandardKilometer],[FuelSpendStandardLiter], 'upload', getutcdate(), @trail_id
			from [VEHICLE] o where not exists (select * from [H_VEHICLE] h where h.VEHICLE_ID = o.VEHICLE_ID)
		
		print ' H_MESSAGE_FIELD'
		
		insert into H_MESSAGE_FIELD ( [MESSAGE_FIELD_ID],[MESSAGE_ID],[MESSAGE_TEMPLATE_FIELD_ID],[CONTENT], action, actual_time, trail_id)
		select o.[MESSAGE_FIELD_ID],[MESSAGE_ID],[MESSAGE_TEMPLATE_FIELD_ID],[CONTENT], 'upload', getutcdate(), @trail_id
			from [MESSAGE_FIELD] o where not exists (select * from [H_MESSAGE_FIELD] h where h.MESSAGE_FIELD_ID = o.MESSAGE_FIELD_ID)
		
		print ' H_ZONE_PRIMITIVE'
		
		insert into H_ZONE_PRIMITIVE ( [PRIMITIVE_ID],[PRIMITIVE_TYPE],[NAME],[DESCRIPTION],[RADIUS], action, actual_time, trail_id)
		select o.[PRIMITIVE_ID],[PRIMITIVE_TYPE],[NAME],[DESCRIPTION],[RADIUS], 'upload', getutcdate(), @trail_id
			from [ZONE_PRIMITIVE] o where not exists (select * from [H_ZONE_PRIMITIVE] h where h.PRIMITIVE_ID = o.PRIMITIVE_ID)
		
		print ' H_OPERATOR'
		
		insert into H_OPERATOR ( [OPERATOR_ID],[NAME],[LOGIN],[PASSWORD],[GUID],[TimeZoneInfo],[IsTimeZoneInfoManual],[OneTimePassword],[TimeZone_ID],[Culture_ID],[EMAIL],[MandatoryEmail],[MandatoryPhone], action, actual_time, trail_id)
		select o.[OPERATOR_ID],[NAME],[LOGIN],[PASSWORD],[GUID],[TimeZoneInfo],[IsTimeZoneInfoManual],[OneTimePassword],[TimeZone_ID],[Culture_ID],[EMAIL],[MandatoryEmail],[MandatoryPhone], 'upload', getutcdate(), @trail_id
			from [OPERATOR] o where not exists (select * from [H_OPERATOR] h where h.OPERATOR_ID = o.OPERATOR_ID)
		
		print ' H_DAY'
		
		insert into H_DAY ( [DAY_ID],[DATE],[TEMPERATURE], action, actual_time, trail_id)
		select o.[DAY_ID],[DATE],[TEMPERATURE], 'upload', getutcdate(), @trail_id
			from [DAY] o where not exists (select * from [H_DAY] h where h.DAY_ID = o.DAY_ID)
		
		print ' H_OPERATORGROUP'
		
		insert into H_OPERATORGROUP ( [OPERATORGROUP_ID],[NAME],[COMMENT], action, actual_time, trail_id)
		select o.[OPERATORGROUP_ID],[NAME],[COMMENT], 'upload', getutcdate(), @trail_id
			from [OPERATORGROUP] o where not exists (select * from [H_OPERATORGROUP] h where h.OPERATORGROUP_ID = o.OPERATORGROUP_ID)
		
		print ' H_ROUTE_TRIP'
		
		insert into H_ROUTE_TRIP ( [ROUTE_TRIP_ID],[GEO_TRIP_ID],[ROUTE_ID], action, actual_time, trail_id)
		select o.[ROUTE_TRIP_ID],[GEO_TRIP_ID],[ROUTE_ID], 'upload', getutcdate(), @trail_id
			from [ROUTE_TRIP] o where not exists (select * from [H_ROUTE_TRIP] h where h.ROUTE_TRIP_ID = o.ROUTE_TRIP_ID)
		
		print ' H_RESERV'
		
		insert into H_RESERV ( [RESERV_ID],[RESERV_DATE],[DRIVER],[BEGIN_TIME],[END_TIME],[RELEASED], action, actual_time, trail_id)
		select o.[RESERV_ID],[RESERV_DATE],[DRIVER],[BEGIN_TIME],[END_TIME],[RELEASED], 'upload', getutcdate(), @trail_id
			from [RESERV] o where not exists (select * from [H_RESERV] h where h.RESERV_ID = o.RESERV_ID)
		
		print ' H_ORDER_TRIP'
		
		insert into H_ORDER_TRIP ( [ORDER_TRIP_ID],[ORDER],[JOURNAL_DAY], action, actual_time, trail_id)
		select o.[ORDER_TRIP_ID],[ORDER],[JOURNAL_DAY], 'upload', getutcdate(), @trail_id
			from [ORDER_TRIP] o where not exists (select * from [H_ORDER_TRIP] h where h.ORDER_TRIP_ID = o.ORDER_TRIP_ID)
		
		print ' H_CUSTOMER'
		
		insert into H_CUSTOMER ( [CUSTOMER_ID],[EXT_NUMBER],[NAME],[SHORT_NAME],[DEPARTMENT_ID], action, actual_time, trail_id)
		select o.[CUSTOMER_ID],[EXT_NUMBER],[NAME],[SHORT_NAME],[DEPARTMENT_ID], 'upload', getutcdate(), @trail_id
			from [CUSTOMER] o where not exists (select * from [H_CUSTOMER] h where h.CUSTOMER_ID = o.CUSTOMER_ID)
		
		print ' H_GEO_ZONE_PRIMITIVE'
		
		insert into H_GEO_ZONE_PRIMITIVE ( [GEO_ZONE_PRIMITIVE_ID],[ZONE_ID],[PRIMITIVE_ID],[ORDER], action, actual_time, trail_id)
		select o.[GEO_ZONE_PRIMITIVE_ID],[ZONE_ID],[PRIMITIVE_ID],[ORDER], 'upload', getutcdate(), @trail_id
			from [GEO_ZONE_PRIMITIVE] o where not exists (select * from [H_GEO_ZONE_PRIMITIVE] h where h.GEO_ZONE_PRIMITIVE_ID = o.GEO_ZONE_PRIMITIVE_ID)
		
		print ' H_POINT'
		
		insert into H_POINT ( [POINT_ID],[POINT_KIND_ID],[NAME],[X],[Y],[RADIUS],[ADDRESS],[COMMENT],[BUSSTOP_ID],[VERTEX_ID], action, actual_time, trail_id)
		select o.[POINT_ID],[POINT_KIND_ID],[NAME],[X],[Y],[RADIUS],[ADDRESS],[COMMENT],[BUSSTOP_ID],[VERTEX_ID], 'upload', getutcdate(), @trail_id
			from [POINT] o where not exists (select * from [H_POINT] h where h.POINT_ID = o.POINT_ID)
		
		print ' H_CONTROLLER_TIME'
		
		insert into H_CONTROLLER_TIME ( [CONTROLLER_ID],[TIME_BEGIN],[TIME_END],[CONTROLLER_TIME_ID], action, actual_time, trail_id)
		select o.[CONTROLLER_ID],[TIME_BEGIN],[TIME_END],[CONTROLLER_TIME_ID], 'upload', getutcdate(), @trail_id
			from [CONTROLLER_TIME] o where not exists (select * from [H_CONTROLLER_TIME] h where h.CONTROLLER_TIME_ID = o.CONTROLLER_TIME_ID)
		
		print ' H_ORDER_TYPE'
		
		insert into H_ORDER_TYPE ( [ORDER_TYPE_ID],[NAME], action, actual_time, trail_id)
		select o.[ORDER_TYPE_ID],[NAME], 'upload', getutcdate(), @trail_id
			from [ORDER_TYPE] o where not exists (select * from [H_ORDER_TYPE] h where h.ORDER_TYPE_ID = o.ORDER_TYPE_ID)
		
		print ' H_ZONE_PRIMITIVE_VERTEX'
		
		insert into H_ZONE_PRIMITIVE_VERTEX ( [PRIMITIVE_VERTEX_ID],[PRIMITIVE_ID],[VERTEX_ID],[ORDER], action, actual_time, trail_id)
		select o.[PRIMITIVE_VERTEX_ID],[PRIMITIVE_ID],[VERTEX_ID],[ORDER], 'upload', getutcdate(), @trail_id
			from [ZONE_PRIMITIVE_VERTEX] o where not exists (select * from [H_ZONE_PRIMITIVE_VERTEX] h where h.PRIMITIVE_VERTEX_ID = o.PRIMITIVE_VERTEX_ID)
		
		print ' H_ORDER'
		
		insert into H_ORDER ( [ORDER_ID],[DATE],[DEPARTMENT],[ORDER_TYPE],[CUSTOMER],[RECIPIENT],[RESPONDENT],[TIME_BEGIN],[TIME_END],[ORIGIN],[DESTINATION],[LENGTH], action, actual_time, trail_id)
		select o.[ORDER_ID],[DATE],[DEPARTMENT],[ORDER_TYPE],[CUSTOMER],[RECIPIENT],[RESPONDENT],[TIME_BEGIN],[TIME_END],[ORIGIN],[DESTINATION],[LENGTH], 'upload', getutcdate(), @trail_id
			from [ORDER] o where not exists (select * from [H_ORDER] h where h.ORDER_ID = o.ORDER_ID)
		