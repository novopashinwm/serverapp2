if not exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'ResetCounter')
begin

	alter table Billing_Service_Type
		add ResetCounter varchar(10)

end