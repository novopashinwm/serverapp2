update Billing_Service_Type
	set MaxQuantity = 40
	where Service_Type = 'FRNIKA.MLP.ChargingEconom'
	  and MaxQuantity is null

insert into Billing_Service_Type (Service_Type, Service_Type_Category, MaxQuantity, LimitedQuantity)
	select 
		t.*, LimitedQuantity = 1
		from (
			select           Service_Type = '',           	Service_Type_Category = '', MaxQuantity = 0 where 1=0
			union all select 'FRNIKA.MLP.ChargingEconom', 	'LBS',						500
			union all select 'FRNIKA.MLP.ChargingEconom', 	'LBS',						10000
			union all select 'FRNIKA.MLP.ChargingEconom', 	'LBS',						50000
		) t
		where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type and e.MaxQuantity = t.MaxQuantity)


