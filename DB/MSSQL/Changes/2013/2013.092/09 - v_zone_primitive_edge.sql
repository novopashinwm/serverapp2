if (exists (select 1 from sys.objects where name = 'v_zone_primitive_edge' and type = 'v'))
	drop view dbo.v_zone_primitive_edge;
go

create view dbo.v_zone_primitive_edge
	as 
		select zpv1.Zone_ID
		     , zpv1.Primitive_ID
		     , [Order] = zpv1.[Vertex_Order]
		     , Lng1 = zpv1.Lng
		     , Lat1 = zpv1.Lat
		     , Lng2 = zpv2.Lng
		     , Lat2 = zpv2.Lat
			from dbo.v_zone_vertex zpv1 with (noexpand)
			join dbo.v_zone_vertex zpv2 with (noexpand)
			                             on zpv2.Zone_ID = zpv1.Zone_ID
									    and zpv2.[Vertex_Order] = zpv1.[Vertex_Order] + 1
		union all --���� ������� �� ��������, ��������� ���������� ����� - ��������� ��������� ����� � ������
		select zvFirst.Zone_ID, zvFirst.Primitive_ID, zvLast.Vertex_Order, Lng1 = zvLast.Lng, Lat1 = zvLast.Lat, Lng2 = zvFirst.Lng, Lat2 = zvFirst.Lat
			from v_zone_vertex zvFirst
			cross apply (
				select top(1) * 
					from v_zone_vertex zv with (noexpand)
					where zv.Zone_ID = zvFirst.Zone_ID
					order by zv.Vertex_Order desc
			) zvLast
			where zvFirst.Vertex_Order = 1
			  and (zvFirst.Lat <> zvLast.Lat or zvFirst.Lng <> zvLast.Lng)
