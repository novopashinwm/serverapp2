insert into Billing_Service_Type (Service_Type, Service_Type_Category, MaxQuantity, LimitedQuantity, ResetCounter, Singleton, Name)
	select * from (
		select 
			  Service_Type = 'FRNIKA.SMS.Trial'
			, Service_Type_Category = 'SMS'
			, MaxQuantity = 40
			, LimitedQuantity = 1
			, ResetCounter = 'monthly'
			, Singleton = 0
			, Name = '���� SMS �����'
	) t
	where not exists (
		select * 
			from Billing_Service_Type e
			where e.Service_Type = t.Service_Type
	)
