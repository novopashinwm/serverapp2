insert into MESSAGE_TEMPLATE (NAME)
	select t.Name
		from (
			select Name = 'GenericNotificationSmsOverMpx'
		) t
		where not exists (
			select * from MESSAGE_TEMPLATE e where e.NAME = t.Name)