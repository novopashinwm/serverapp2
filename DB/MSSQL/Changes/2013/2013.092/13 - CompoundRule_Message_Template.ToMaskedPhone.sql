if not exists (
	select * 
		from sys.columns where object_id = object_id('CompoundRule_Message_Template') and name = 'ToMaskedPhone'
)
begin

	alter table CompoundRule_Message_Template
		add ToMaskedPhone bit not null
			constraint DF_CompoundRule_Message_Template_ToMaskedPhone default (0)

	alter table H_CompoundRule_Message_Template
		add ToMaskedPhone bit not null
			constraint DF_H_CompoundRule_Message_Template_ToMaskedPhone default (0)

end