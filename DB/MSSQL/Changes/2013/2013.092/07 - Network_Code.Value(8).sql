if (
	select max_length from sys.columns where object_id = object_id('Network_Code') and name = 'Value'
) < 8
begin

	alter table Network_Code 
		alter column Value varchar(8)
		
end