update Billing_Service_Type 
	set MinIntervalSeconds = 20*60
	where Service_Type_Category = 'GNSS'
	  and MinIntervalSeconds is null