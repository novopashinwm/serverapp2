if 0 = (
	select is_nullable from sys.columns where object_id = OBJECT_ID('Monitoree_Log_Ignored') and name = 'Speed'
)
begin

	alter table Monitoree_Log_Ignored
		alter column Speed tinyint null

end