if exists (
	select * from sys.views where name = 'v_zone_vertex'
)
	drop view v_zone_vertex;
go

create view v_zone_vertex 
WITH SCHEMABINDING
as
	select zp.ZONE_ID
	     , zp.Primitive_ID
		 , Primitive_Order = zp.[ORDER]
		 , Vertex_Order = pv.[ORDER]
		 , pv.VERTEX_ID
		 , Lat = v.Y
		 , Lng = v.X
	from dbo.GEO_ZONE_PRIMITIVE zp
	join dbo.ZONE_PRIMITIVE_VERTEX pv on pv.PRIMITIVE_ID = zp.PRIMITIVE_ID
	join dbo.MAP_VERTEX v on v.VERTEX_ID = pv.VERTEX_ID

go
create unique clustered index IX_v_zone_vertex on v_zone_vertex (ZONE_ID, Vertex_Order)