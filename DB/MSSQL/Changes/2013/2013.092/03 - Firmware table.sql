if not exists (
	select * from sys.objects where name = 'Firmware'
)
begin

	create table Firmware
	(
		ID smallint identity(1,1)	
			constraint PK_Firmware
				primary key nonclustered,
		Name varchar(32)
	)
	
end