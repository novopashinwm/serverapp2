if not exists (
	select * from sys.objects where name = 'DF_Asid_AllowMlpRequest'
)
begin
	alter table Asid
		add constraint DF_Asid_AllowMlpRequest default(1) for AllowMlpRequest
end

go

if not exists (
	select * from sys.objects where name = 'DF_Asid_AllowSecurityAdministration'
)
begin
	alter table Asid
		add constraint DF_Asid_AllowSecurityAdministration default(1) for AllowSecurityAdministration
end

go

if not exists (
	select * from sys.objects where name = 'DF_Asid_WarnAboutLocation'
)
begin
	alter table Asid
		add constraint DF_Asid_WarnAboutLocation default(0) for WarnAboutLocation
end

