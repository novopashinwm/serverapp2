delete from CompoundRule_Message_Template
	where CompoundRule_Message_Template_ID in (
		select CompoundRule_Message_Template_ID
		from compoundRule cr
		outer apply (
			select cr_mt.CompoundRule_Message_Template_ID
				from CompoundRule_Message_Template cr_mt
				where cr_mt.CompoundRule_ID = cr.CompoundRule_ID
				  and cr_mt.Operator_ID not in (
						select distinct e.Operator_ID
							from CompoundRule_SubscriptionEmail cr_se
							join Email e on e.Email_ID = cr_se.Email_ID
							where cr_se.CompoundRule_ID = cr.CompoundRule_ID
						union
							select distinct p.Operator_ID
							from CompoundRule_SubscriptionPhone cr_sp
							join Phone p on p.Phone_ID = cr_sp.Phone_ID
							where cr_sp.CompoundRule_ID = cr.CompoundRule_ID)		  
					) tmt
		where tmt.CompoundRule_Message_Template_ID is not null)
