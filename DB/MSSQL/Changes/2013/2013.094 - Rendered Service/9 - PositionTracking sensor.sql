insert into CONTROLLER_SENSOR_LEGEND (CONTROLLER_SENSOR_TYPE_ID, NAME, Number)
	select 2, t.Name, t.Number
		from (select Name = 'PositionTracking', Number = 64) t
		where not exists (select * from CONTROLLER_SENSOR_LEGEND e where e.Number = t.Number)
go

insert into Controller_Sensor (CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, NUMBER, MAX_VALUE, MIN_VALUE, DEFAULT_VALUE, BITS, Descript, VALUE_EXPIRED, 
	Default_Sensor_Legend_ID, Default_Multiplier, Default_Constant, Mandatory)
	select t.Controller_Type_ID, 2, t.Number, 1, 0, 0, 1, 'PositionTracking', 84600,
		legend.CONTROLLER_SENSOR_LEGEND_ID, 1, 0, 1
		from (
			select ct.Controller_Type_ID, Number = 31
				from CONTROLLER_TYPE ct
				where ct.TYPE_NAME in ('s60v3fp', 'nika.tracker')
		) t
		join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = 'PositionTracking'
		where not exists (select * from CONTROLLER_SENSOR cs where cs.Controller_Type_ID = t.CONTROLLER_TYPE_ID and cs.NUMBER = t.Number)
				