if not exists (
	select * from sys.objects where name = 'Rendered_Service_Type'
)
begin
	create table Rendered_Service_Type
	(
		ID int not null
			constraint PK_Rendered_Service_Type
				primary key clustered,
		Name varchar(32)
	)
end

go

insert into Rendered_Service_Type (ID, Name)
	select * from (
			select ID = 0, Name = 'LBS' 
	   union all select 1,        'SMS'
	   union all select 2,		  'SMSMPX'
	) t
	where not exists (select * from Rendered_Service_Type e where e.ID = t.ID)