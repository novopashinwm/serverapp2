if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Asid')
		  and name = 'Department_ID'
)
begin

	alter table Asid
		add Department_ID int 
			constraint FK_Asid_Department_ID
				foreign key references Department(Department_ID)
				
	alter table H_Asid
		add Department_ID int 			

end
go

if exists (
	select * 
		from sys.columns 
		where object_id = object_id('Asid')
		  and name = 'Contract_Number'
)
begin

	exec sp_executesql N'

	update a
		set Department_ID = d.Department_ID
		from Asid a
		join Department d on d.ExtID = a.Contract_Number
		where a.Contract_Number is not null
		  and (a.Department_ID is null 
			or a.Department_ID <> d.Department_ID)
'
	
	drop index IX_Asid_Contract_Number on Asid

	alter table Asid
		drop column Contract_Number
		
	alter table H_Asid
		drop column Contract_Number
	
end