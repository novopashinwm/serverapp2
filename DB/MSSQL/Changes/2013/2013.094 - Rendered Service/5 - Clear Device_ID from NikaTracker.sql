update ci
	set device_id = null
from CONTROLLER c
join CONTROLLER_INFO ci on ci.CONTROLLER_ID = c.CONTROLLER_ID
join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = c.CONTROLLER_TYPE_ID
where ct.TYPE_NAME = 'Nika.Tracker'
  and ci.DEVICE_ID is not null
  