update ci
	set DEVICE_ID = hci.DEVICE_ID
from Asid a
join MLP_Controller mlpc on mlpc.Asid_ID = a.ID
join CONTROLLER_INFO ci on ci.CONTROLLER_ID = mlpc.Controller_ID
outer apply (
	select top(1) * 
		from H_CONTROLLER_INFO ci
		where ci.CONTROLLER_ID = mlpc.controller_id
		order by ACTUAL_TIME desc) hci 
where a.SimID is null
  and hci.DEVICE_ID is not null
  and ci.device_id is null
