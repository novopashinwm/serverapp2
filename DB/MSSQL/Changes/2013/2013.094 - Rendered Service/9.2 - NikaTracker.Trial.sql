insert into billing_Service_type (
	Name, Service_Type, Service_Type_Category, Controller_Type_ID, Vehicle_Kind_ID, Singleton, PeriodDays)
	select * 
		from (
			select
			  Name = '���� ������ �����'
			, Service_Type = 'FRNIKA.GPS.Phone.Trial'
			, nikaTracker.Service_Type_Category
			, nikaTracker.Controller_Type_ID
			, nikaTracker.Vehicle_Kind_ID
			, Singleton = 1
			, PeriodDays = 60 /*��� ������*/
			from Billing_Service_Type nikaTracker
			where nikaTracker.Service_Type = 'FRNIKA.GPS.Phone'
		) t
		where not exists (select * from Billing_Service_Type e where e.Service_Type = t.Service_Type)
		