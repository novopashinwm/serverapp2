update Billing_Service_Type
	set PeriodDays = 30
	where Service_Type in (
		  'FRNIKA.SMS.ChargingEconom'
		, 'FRNIKA.SMS.M2M'
		, 'FRNIKA.SMS.LBS'
		, 'FRNIKA.MLP.ChargingEconom')
	and ResetCounter is null
	and PeriodDays is null