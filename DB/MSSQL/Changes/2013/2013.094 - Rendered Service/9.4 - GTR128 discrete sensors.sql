/*��������� ��� ����������� Globalsat - TR203
	����������� ������� ����������� (��������� ������, ����������� etc)
*/
begin tran
declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'GlobalSat GTR-128')

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512),
	DefaultSensorLegendNumber   int
	);
set nocount on
insert into @controller_sensor values (@controller_type_id, 1, 24,    1, 0, 0,    1, 	'ACC',			 4 /*���������*/);
insert into @controller_sensor values (@controller_type_id, 1, 21,    1, 0, 0,    1, 	'DS1 (Input 1)', null);
insert into @controller_sensor values (@controller_type_id, 1, 22,    1, 0, 0,    1, 	'DS2 (Input 2)', null);
insert into @controller_sensor values (@controller_type_id, 1, 23,    1, 0, 0,    1, 	'DS3 (Input 3)', null);
set nocount off

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript,
	Default_Sensor_Legend_ID,
	Default_Multiplier,
	Default_Constant,
	Mandatory
	)
select
 CONTROLLER_TYPE_ID		
,CONTROLLER_SENSOR_TYPE_ID
,NUMBER					
,MAX_VALUE				
,MIN_VALUE				
,DEFAULT_VALUE			
,BITS					
,Descript				
,(select legend.Controller_Sensor_Legend_ID from CONTROLLER_SENSOR_LEGEND legend where legend.Number = t.DefaultSensorLegendNumber)
,1
,0
,case when t.DefaultSensorLegendNumber is not null then 1 else 0 end
from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)
						  
commit