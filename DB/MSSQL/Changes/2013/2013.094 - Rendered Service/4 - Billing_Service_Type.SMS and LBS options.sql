if not exists (
	select * from sys.columns where OBJECT_ID = OBJECT_ID('Billing_Service_Type') and name = 'SMS'
)
begin

	alter table Billing_Service_Type
		add SMS bit not null
			constraint DF_Billing_Service_Type_SMS default (0)

	alter table Billing_Service_Type
		add LBS bit not null
			constraint DF_Billing_Service_Type_LBS default (0)

end
go

update Billing_Service_Type
	set SMS = 1 
	where SMS = 0
	  and (Service_Type_Category = 'SMS' or 
	       Service_Type = 'FRNIKA.MLP.ChargingEconom' and MaxQuantity = 40)
	  
update Billing_Service_Type
	set LBS = 1 
	where LBS = 0
	  and (Service_Type_Category = 'LBS' or 
	       Service_Type = 'FRNIKA.GPS.Modem')
	  