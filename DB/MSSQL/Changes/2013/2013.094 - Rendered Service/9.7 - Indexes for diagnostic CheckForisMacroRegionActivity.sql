if not exists (
	select * 
		from sys.indexes
		where name = 'IX_H_Billing_Service_Action__ID_Actual_Time'
)
begin

	create nonclustered index IX_H_Billing_Service_Action__ID_Actual_Time
		on H_Billing_Service(Action) include (ID, Actual_Time)

end

if not exists (
	select * 
		from sys.indexes
		where name = 'IX_H_Billing_Service_ID_Action__Actual_Time'
)
begin

	create nonclustered index IX_H_Billing_Service_ID_Action__Actual_Time
		on H_Billing_Service(ID, Action) include (Actual_Time)

end

