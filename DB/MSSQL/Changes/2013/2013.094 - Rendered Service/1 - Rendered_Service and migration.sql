if not exists (
	select * from sys.objects where name = 'Rendered_Service'
)
begin
	create table Rendered_Service
	(
		ID int identity (1,1)
			constraint PK_Rendered_Service
				primary key nonclustered,
		Billing_Service_ID int not null
			constraint FK_Rendered_Service_Billing_Service_ID
				foreign key references Billing_Service(ID),
		Rendered_Service_Type_ID int not null
			constraint FK_Rendered_Service_Rendered_Service_Type_ID
				foreign key references Rendered_Service_Type(ID),
		Quantity int not null,
		LastTime datetime not null
	)
	
	create clustered index IX_Rendered_Service_Billing_Service_ID_Rendered_Service_Type_ID 
		on Rendered_Service(Billing_Service_ID, Rendered_Service_Type_ID)
	
	create table H_Rendered_Service
	(
		H_Rendered_Service_ID	bigint identity constraint PK_H_Rendered_Service primary key nonclustered,

		ID int not null,
		Billing_Service_ID int not null,
		Rendered_Service_Type_ID int not null,
		Quantity int not null,
		LastTime datetime not null,
		
		TRAIL_ID					int constraint FK_H_Rendered_Service_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end
go

if exists (
	select * from sys.columns where object_id = object_id('Billing_Service') and name = 'RenderedQuantity'
)
begin

	begin tran

	--Миграция 
	
	print 'Migrating to Rendered_Service'
	exec sp_executesql N'
	insert into Rendered_Service (Billing_Service_ID, Rendered_Service_Type_ID, Quantity, LastTime)
		select bs.ID, rst.ID, bs.RenderedQuantity, bs.RenderedLastTime
			from Billing_Service bs
			join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
			join Rendered_Service_Type rst on rst.Name = bst.Service_Type_Category
			where bs.RenderedLastTime is not null and
			      bs.RenderedQuantity >= 0
'
	print 'Migrating to H_Rendered_Service'
	exec sp_executesql N'
	insert into H_Rendered_Service (ID, Billing_Service_ID, Rendered_Service_Type_ID, Quantity, LastTime, TRAIL_ID, [ACTION], ACTUAL_TIME)
		select rs.ID, bs.ID, rs.Rendered_Service_Type_ID, bs.RenderedQuantity, bs.RenderedLastTime, bs.TRAIL_ID, bs.[ACTION], bs.ACTUAL_TIME
			from H_Billing_Service bs
			join Billing_Service_Type bst on bst.ID = bs.Billing_Service_Type_ID
			join Rendered_Service_Type rst on rst.Name = bst.Service_Type_Category
			join Rendered_Service rs on rs.Billing_Service_ID = bs.ID and rs.Rendered_Service_Type_ID = rst.ID
			where bs.RenderedLastTime is not null and
			      bs.RenderedQuantity >= 0
'

	alter table Billing_Service
		drop column RenderedQuantity
		
	alter table Billing_Service
		drop column RenderedLastTime

	alter table H_Billing_Service
		drop column RenderedQuantity
		
	alter table H_Billing_Service
		drop column RenderedLastTime

	commit

end
