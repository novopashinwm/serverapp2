--������ ������� ��� �������� ���������� � ���������� �� � Billing_Service_Type

declare @pattern table (
	Service_Type_Category varchar(50), 
	Vehicle_Kind_Name nvarchar(32), 
	Controller_Type_Name nvarchar(50)
)

set nocount on
insert into @pattern values 
	('NikaAdmin',			'��������� �������','MLP')
set nocount off

update bst
	set 
		bst.Vehicle_Kind_ID = vk.Vehicle_Kind_ID,
		bst.Controller_Type_ID = ct.Controller_Type_ID
	from Billing_Service_Type bst
	join @pattern p on p.Service_Type_Category = bst.Service_Type_Category
	join Vehicle_Kind vk on vk.Name = p.Vehicle_Kind_Name
	join Controller_Type ct on ct.Type_Name = p.Controller_Type_Name
	where (bst.Vehicle_Kind_ID is null or  bst.Vehicle_Kind_ID <> vk.Vehicle_Kind_ID)
	   or (bst.Controller_Type_ID is null  or bst.Controller_Type_ID <> ct.Controller_Type_ID)
	  
	