if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Asid')
		  and name = 'DeviceId'
)
begin

	alter table Asid 
		add DeviceId varchar(255)
		
	alter table H_Asid
		add DeviceId varchar(255)

	create nonclustered index IX_Asid_DeviceId on Asid(DeviceId)

end