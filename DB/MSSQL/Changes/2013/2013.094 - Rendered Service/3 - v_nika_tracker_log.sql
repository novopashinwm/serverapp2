if exists (
	select * from sys.views where name = 'v_nika_tracker_log'
)
	drop view v_nika_tracker_log;
go

create view v_nika_tracker_log
	as
	select UtcTime = m.Time, MskTime =  DATEADD(hour, 4, m.Time), m.source_id, m.body
		from v_message m
		where m.Source_Type = 'Controller'
