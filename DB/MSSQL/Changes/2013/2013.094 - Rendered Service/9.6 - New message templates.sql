insert into dbo.Message_Template(Name)
	select t.Name 
		from (
					  select Name = 'ProposeViewVehicle'
			union all select 'AppClientNotification'
		) t
		where not exists (select * from dbo.Message_Template e where e.Name = t.Name)
