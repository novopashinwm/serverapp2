alter view v_msisdn_password_status
as 
select   "���"           	= ct.type_name
	   , "IMEI / �����"  	= convert(varchar(32), ci.device_id)
	   , "Vehicle name"  	= v.garage_number
	   , "������� "      	= isnull(d.name, '')
	   , "������� ��������" = case when len(c.phone) >= 10 then 1 else 0 end
	   , "������� ������"   = case when ct.SupportsPassword = 0 then null
								   when len(ci.password) > 0 then 1 else 0 end
	from controller c
	join controller_info ci on ci.controller_id = c.controller_id
	join vehicle v on v.vehicle_id = c.vehicle_id
	join controller_type ct on ct.controller_type_id = c.controller_type_id
	left outer join department d on d.department_id = v.department
	outer apply (  
	  select top(1) lt.InsertTime   
	   from Log_Time lt (nolock)   
	   where lt.Vehicle_ID = c.Vehicle_ID   
	   order by lt.InsertTime desc) last_lt  
	where 1=1
	  and ci.device_id is not null
	  and ci.device_id <> 0x00
	  and c.vehicle_id not in (select FakeVehicleID from emulator)
	  and c.vehicle_id not in (select t.Vehicle_ID from Tracker t where t.Vehicle_ID is not null)
	  and (d.name is null or d.name <> 'SitronicsIndia')
	  and (last_lt.InsertTime > getutcdate() - 90)
	  