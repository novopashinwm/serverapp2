if not exists (
	select * from sys.columns where object_id = object_id('Command') and name = 'Payer_Vehicle_ID'
)
begin

	alter table Command
		add Payer_Vehicle_ID int
			constraint FK_Command_Payer_Vehicle_ID
				foreign key references Vehicle(Vehicle_ID)

end
