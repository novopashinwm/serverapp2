if not exists (
	select * from sys.columns where object_id = object_id('Controller_Type') and name = 'SupportsPassword'
)
begin

	alter table Controller_Type 
		add SupportsPassword bit not null
			constraint DF_Controller_Type_SupportsPassword default(0)
			
	alter table H_Controller_Type 
		add SupportsPassword bit not null
			constraint DF_H_Controller_Type_SupportsPassword default(0)

end
go

update CONTROLLER_TYPE
	set SupportsPassword = 1
	where TYPE_NAME in ('AvtoGraf', 'AvtoGrafCan') and SupportsPassword = 0
	