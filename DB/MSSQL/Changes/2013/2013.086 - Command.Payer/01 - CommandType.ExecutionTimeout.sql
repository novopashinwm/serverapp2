if not exists (
	select * from sys.columns where object_id = object_id('CommandTypes') and name = 'ExecutionTimeout'
) 
begin
	
	alter table CommandTypes 
		add ExecutionTimeout int
		
end
go

if not exists (
	select * from sys.objects where name = 'FK_Command_Type_ID')
begin
	alter table Command
		add constraint FK_Command_Type_ID 
			foreign key ([Type_ID]) references CommandTypes(ID)
end		
go

update CommandTypes
	set ExecutionTimeout = 
		case code
			when 'AskPositionOverMLP' then 60
			when 'Restart' then 15 
			when 'RegisterMobileUnit' then 15
			when 'AskPositionOverMLP' then 15
			when 'CutOffElectricity'  then 15
			when 'ReopenElectricity'  then 15
			when 'CutOffFuel'         then 15
			when 'ReopenFuel'         then 15
			when 'CancelAlarm'        then 15
			when 'ChangeConfig'       then 15
			when 'ReloadDevice'       then 15
			when 'VibrateRequest'     then 15
			when 'CallPhone'          then 15
			when 'ShotdownDevice'     then 15
			when 'CapturePicture'     then 15
			else null
		end
