set nocount on

select 
  Name = N'SwitchToCorporativeRequest'
, Header = N'������� $Department.Name$ $Department.Region$ ������� ������ �� ����������� ���������������� ������������� ��������'
, Body = N'
<p>
������� $Department.Name$ $Department.Region$ ������� ������ �� ����������� ���������������� ������������� ��������.
</p>

<p>
������� �������� �� ������ $AppSettings.UrlWebGis$/department.aspx?a=switchToAndGo&departmentId=$Department.Department_ID$&backUrl=$AppSettings.UrlWebGis$/department.aspx?a=approveCorporativeForm
</p>
'
into #t

set nocount off

update mt
	set HEADER = t.Header
	  , BODY = t.Body
	from #t t 
	join Message_Template mt on mt.Name = t.NAME and mt.Culture_ID is null
	where mt.Header <> t.Header
	   or mt.Body <> t.Body

insert into Message_Template (NAME, HEADER, BODY)
	select t.Name, t.Header, Body
		from #t t
		where not exists (select * from MESSAGE_TEMPLATE e where e.NAME = t.Name and e.Culture_ID is null)

;with t as (select Name = N'', Type = N'' where 1=0
union all select N'Department.Name', 'System.String'
union all select N'Department.Region', 'System.String'
union all select N'AppSettings.UrlWebGis', 'System.String'
union all select N'Department.Department_ID', 'System.Int32'
) 
insert into MESSAGE_TEMPLATE_FIELD (MESSAGE_TEMPLATE_ID, NAME, DOTNET_TYPE_ID)
	select mt.Message_Template_ID, t.Name, dnt.DotNet_Type_ID
		from t 
		join DOTNET_TYPE dnt on dnt.TYPE_NAME = t.Type
		join MESSAGE_TEMPLATE mt on mt.NAME = 'SwitchToCorporativeRequest' and mt.Culture_ID is null
		where not exists (select * from MESSAGE_TEMPLATE_FIELD e where e.MESSAGE_TEMPLATE_ID = mt.MESSAGE_TEMPLATE_ID and e.NAME = t.Name)
		
drop table #t