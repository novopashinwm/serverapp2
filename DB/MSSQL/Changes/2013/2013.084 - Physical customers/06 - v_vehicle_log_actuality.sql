if (exists (select * from sys.views where name = 'v_vehicle_log_actuality'))
	drop view v_vehicle_log_actuality
go

create view v_vehicle_log_actuality
as
--������, �� ������� �������� �����
with [v] (vehicle_id) as (
	select v.VEHICLE_ID
		from DEPARTMENT d
		join Vehicle v on v.DEPARTMENT = d.DEPARTMENT_ID
		join Vehicle_Kind vk on vk.Vehicle_Kind_ID = v.Vehicle_Kind_ID
		where (d.Type = 0 
			or d.Type is null) --������������� �������
			and vk.Name in ('�������', '��������', '��������', '������')
			and d.Name not in ('SitronicsIndia', '�����')
)
--���������� � ��������� ������
select v.VEHICLE_ID
	, Type = 'Insert'
	, Time = [log].Time
	from [v] v
	cross apply (
		select top(1) [Time] = InsertTime
			from Log_Time l (nolock)
			where l.Vehicle_ID = v.VEHICLE_ID
			order by l.InsertTime desc) [log]
union all
select v.VEHICLE_ID
	, Type = 'AnyLog'
	, Time = [log].Time
	from [v] v
	cross apply (
		select top(1) Time = dbo.GetDateFromInt(Log_Time)
			from Log_Time l (nolock)
			where l.Vehicle_ID = v.VEHICLE_ID
			order by l.LOG_TIME desc) [log]
union all
select v.VEHICLE_ID
	, Type = 'Geo'
	, Time = [log].Time
	from [v] v
	cross apply (
		select top(1) Time = dbo.GetDateFromInt(Log_Time)
			from Geo_Log l (nolock)
			where l.Vehicle_ID = v.VEHICLE_ID
			order by l.LOG_TIME desc) [log]
union all
select v.VEHICLE_ID
	, Type = 'CAN'
	, Time = [log].Time
	from [v] v
	cross apply (
		select top(1) Time = dbo.GetDateFromInt(Log_Time)
			from CAN_INFO l (nolock)
			where l.Vehicle_ID = v.VEHICLE_ID
			order by l.LOG_TIME desc) [log]
union all
select v.VEHICLE_ID
	, [Type] = 'CAN'
	, [Time] = [log].[Time]
	from [v] v
	cross apply (
		select top(1) [Time] = dbo.GetDateFromInt(Log_Time)
			from v_controller_sensor_log l (nolock)
			where l.Vehicle_ID = v.VEHICLE_ID
			  and l.SensorLegend = (select Number from CONTROLLER_SENSOR_LEGEND where NAME = '������ �������')
			order by l.LOG_TIME desc) [log]
go
