update cs
	set Mandatory = 0
	from CONTROLLER_SENSOR cs 
	join CONTROLLER_SENSOR_LEGEND l on l.CONTROLLER_SENSOR_LEGEND_ID = cs.Default_Sensor_Legend_ID
	join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
	where ct.TYPE_NAME in ('AvtoGrafCan', 'AvtoGraf')
	  and  l.NAME = '�������'
	  and  cs.Mandatory = 1
	