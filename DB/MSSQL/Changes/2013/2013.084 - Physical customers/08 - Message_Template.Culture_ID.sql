if not exists (select * from sys.columns where object_id = object_id('Message_Template') and name = 'Culture_ID')
begin

	alter table Message_Template
		add Culture_ID int 
			constraint FK_Message_Template_Culture_ID
				foreign key references Culture(Culture_ID)

	alter table H_Message_Template
		add Culture_ID int

end