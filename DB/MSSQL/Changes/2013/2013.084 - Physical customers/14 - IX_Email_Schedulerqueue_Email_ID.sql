if not exists (select * from sys.indexes where object_id = object_id('email_schedulerqueue') and name = 'IX_Email_Schedulerqueue_Email_ID')
begin

	create nonclustered index IX_Email_Schedulerqueue_Email_ID on Email_Schedulerqueue(Email_ID)

end