insert into [Right] (right_id, system, name, description)
	select t.right_id, 1, 'ManageSensors', 'Manage vehicle sensors'
		from (select right_id = 117) t
		where not exists (select * from [right] r where r.right_id = t.right_id)
go

if exists (select * from sys.objects where name = 'CK_OPERATORGROUP_DEPARTMENT_RIGHT')
begin

	alter table OPERATORGROUP_DEPARTMENT
		drop CK_OPERATORGROUP_DEPARTMENT_RIGHT

end
go

if exists (select * from sys.objects where name = 'CK_OPERATOR_DEPARTMENT_RIGHT')
begin

	alter table OPERATOR_DEPARTMENT
		drop CK_OPERATOR_DEPARTMENT_RIGHT

end

go

insert into OPERATORGROUP_DEPARTMENT
	select ogd.OperatorGroup_ID, ogd.Department_ID, r.Right_ID, 1
		from OPERATORGROUP_DEPARTMENT ogd
		join [RIGHT] r on r.NAME = 'ManageSensors'
		join Department d on d.DEPARTMENT_ID = ogd.DEPARTMENT_ID
		where 1=1
			and isnull(d.Type, 0) = 0 --Corporate (default)
			and ogd.ALLOWED = 1 
			and ogd.RIGHT_ID = 104 /*DepartmentAccess*/ 		
			and not exists (
				select * 
					from OPERATORGROUP_DEPARTMENT e 
					where e.OPERATORGROUP_ID = ogd.OPERATORGROUP_ID 
					  and e.DEPARTMENT_ID = ogd.DEPARTMENT_ID
					  and e.RIGHT_ID = r.RIGHT_ID)

go

insert into OPERATOR_DEPARTMENT
	select od.Operator_ID, od.Department_ID, r.Right_ID, 1
		from OPERATOR_DEPARTMENT od
		join [RIGHT] r on r.NAME = 'ManageSensors'
		join Department d on d.DEPARTMENT_ID = od.DEPARTMENT_ID
		where 1=1
			and isnull(d.Type, 0) = 0 --Corporate (default)
			and od.ALLOWED = 1 
			and od.RIGHT_ID = 104 /*DepartmentAccess*/ 		
			and not exists (
				select * 
					from OPERATOR_DEPARTMENT e 
					where e.OPERATOR_ID = od.OPERATOR_ID 
					  and e.DEPARTMENT_ID = od.DEPARTMENT_ID
					  and e.RIGHT_ID = r.RIGHT_ID)
