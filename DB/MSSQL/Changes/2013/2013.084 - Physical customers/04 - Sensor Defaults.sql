update cs
	set Default_Sensor_Legend_ID = isnull(cs.Default_Sensor_Legend_ID, legend.CONTROLLER_SENSOR_LEGEND_ID),
		Default_Multiplier = isnull(cs.Default_Multiplier, t.Multiplier),
		Default_Constant = isnull(cs.Default_Constant, t.Constant)
from (
	select Controller_Type_Name = '', Sensor_Name = '', Multiplier = 1.0, Constant = 0.0 where 1=0
		union all select 'JB101', 'Accident', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'LastPublishingDurationMilliseconds', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'ErrorSource', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'ErrorCode', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'TimeToFirstGPSFix', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_FuelLevel2', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_FuelLevel3', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_FuelLevel4', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_FuelLevel5', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_FuelLevel6', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_AxleLoad2', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_AxleLoad3', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_AxleLoad4', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_AxleLoad5', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_AxleLoad6', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_CruiseControl', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_Brake', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_ParkingBrake', 1.000000000, 0.000000000
		union all select 'AvtoGrafCan', 'CAN_Clutch', 1.000000000, 0.000000000
		union all select 'NaviTechDevice', 'AccumulatorVoltage', 1.000000000, 0.000000000
		union all select 'StarLineM10', 'BatteryLevel', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'GpsEnabled', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'RadioScannerEnabled', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'DataSenderEnabled', 1.000000000, 0.000000000
		union all select 'Neva', 'Power', 1.000000000, 0.000000000
		union all select 'Neva', 'Current', 0.010000000, 0.000000000
		union all select 'Neva', 'CoverOpeningTimes', 1.000000000, 0.000000000
		union all select 'Neva', 'HeartBeatInterval', 1.000000000, 0.000000000
		union all select 'Neva', 'RelayStatus', 1.000000000, 0.000000000
		union all select 'S60V3FP', 'IsMoving', 1.000000000, 0.000000000
) t
join CONTROLLER_TYPE ct on ct.TYPE_NAME = t.Controller_Type_Name
join Controller_Sensor cs on cs.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID and cs.Descript = t.Sensor_Name
join CONTROLLER_SENSOR_LEGEND legend on legend.NAME = t.Sensor_Name
where cs.Default_Sensor_Legend_ID is null or cs.Default_Multiplier is null or cs.Default_Constant is null