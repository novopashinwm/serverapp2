IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_operator_vehicle_right]'))
DROP VIEW [dbo].[v_operator_vehicle_right]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_operator_vehicle_right] 
as
	WITH allRights(operator_id, vehicle_id, right_id, allowed, priority) AS 
	(
		select operator_id, vehicle_id, right_id, allowed, 1 'priority'
			from dbo.operator_vehicle 
		union all
		select o_vg.operator_id, vg_v.vehicle_id, o_vg.right_id, allowed, 2 'priority'
			from dbo.operator_vehiclegroup o_vg
			join dbo.VEHICLEGROUP_VEHICLE vg_v on vg_v.vehiclegroup_id = o_vg.vehiclegroup_id
		union all
		select og_o.operator_id, og_v.vehicle_id, og_v.right_id, allowed, 3 'priority'
			from dbo.operatorgroup_vehicle og_v
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
		union all
		select og_o.operator_id, vg_v.vehicle_id, og_vg.right_id, allowed, 4 'priority'
			from dbo.OPERATORGROUP_VEHICLEGROUP og_vg
			join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_vg.operatorgroup_id
			join dbo.VEHICLEGROUP_VEHICLE vg_v on vg_v.vehiclegroup_id = og_vg.vehiclegroup_id
		union all
		select o_d.operator_id, v.vehicle_id, o_d.right_id, allowed, 5 'priority'
			from dbo.OPERATOR_DEPARTMENT o_d
			join dbo.VEHICLE v on v.department = o_d.department_id
		union all
		select og_o.operator_id, v.vehicle_id, og_d.right_id, allowed, 6 'priority'
			from dbo.OPERATORGROUP_DEPARTMENT og_d
			join dbo.VEHICLE v on v.department = og_d.department_id
			join dbo.OperatorGroup_Operator og_o on og_o.OperatorGroup_ID = og_d.OperatorGroup_ID
		union all
		select o_d.operator_id, v.vehicle_id, r.right_id, 1, 7 'priority'
			from dbo.OPERATOR_DEPARTMENT o_d
			join dbo.VEHICLE v on v.department = o_d.department_id
			join dbo.[Right] r on r.Right_ID in (6, 7, 20, 21, 102, 112, 116)
			where o_d.right_id = 2 /*SecurityAdministrator*/
			  and o_d.allowed = 1
		union all
		select og_o.operator_id, v.vehicle_id, r.right_id, 1, 8 'priority'
			from OperatorGroup_Operator og_o
			join dbo.OPERATORGROUP_DEPARTMENT og_d on og_d.OperatorGroup_ID = og_o.OperatorGroup_ID
			join dbo.VEHICLE v on v.department = og_d.department_id
			join dbo.[Right] r on r.Right_ID in (6, 7, 20, 21, 102, 112, 116)
			where og_d.right_id = 2 /*SecurityAdministrator*/
			  and og_d.allowed = 1
	)
	select distinct operator_id, vehicle_id, r1.right_id, right_name = r.NAME
	from allRights r1
	join [RIGHT] r on r.RIGHT_ID = r1.right_id
	where allowed = 1
	and not exists (select * from allRights r2 
					where r2.operator_id = r1.operator_id 
					and r2.vehicle_id = r1.vehicle_id
					and r2.right_id = r1.right_id
					and r2.allowed = 0
					and r2.priority < r1.priority)