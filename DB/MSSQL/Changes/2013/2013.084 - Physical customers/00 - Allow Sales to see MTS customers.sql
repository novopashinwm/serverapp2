insert into OPERATORGROUP_DEPARTMENT
	select og.OPERATORGROUP_ID, d.DEPARTMENT_ID, r.RIGHT_ID, 1
	from DEPARTMENT d, OPERATORGROUP og, [RIGHT] r 
	where d.ExtID is not null 
	  and d.Description = '������ �� ������� �� FORIS.SPA'
	  and og.NAME = 'Sales'
	  and r.NAME in (
			'SecurityAdministration',
			'DepartmentsAccess',
			'EditVehicles',
			'EditControllerPhone', 
			'EditControllerDeviceID',
			'ControllerPasswordAccess')
	  and not exists (select * from OPERATORGROUP_DEPARTMENT e where e.OPERATORGROUP_ID = og.OPERATORGROUP_ID and e.DEPARTMENT_ID = d.DEPARTMENT_ID and e.RIGHT_ID = r.RIGHT_ID)