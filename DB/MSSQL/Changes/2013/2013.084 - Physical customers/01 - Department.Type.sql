if not exists (select * from sys.columns where object_id = object_id('Department') and name = 'Type')
begin

	alter table Department
		add [Type] int

	alter table H_Department
		add [Type] int

end
