declare @c int = (select COUNT(1) from (
	select c.* 
	from sys.indexes o 
	join sys.index_columns ic on ic.object_id = o.object_id
	join sys.columns c on c.column_id = ic.column_id and c.object_id = o.object_id
	where o.name = 'PK_Emulator'
) t)

if (0 < @c and @c <> 2) 
begin

	alter table Emulator
		drop constraint PK_Emulator

end

if (@c <> 2)
begin 

	alter table Emulator
		add constraint PK_Emulator primary key clustered (RealVehicleID, FakeVehicleID)

end