if not exists (select * from sys.objects where name = 'Wrong_Wlan_For_Locator')
begin
	create table Wrong_Wlan_For_Locator
	(
		WLAN_MAC_Address_ID int not null
			constraint PK_Wrong_Wlan_For_Locator
				primary key clustered,	
		constraint FK_Wrong_Wlan_For_Locator_WLAN_MAC_Address_ID
			foreign key (WLAN_MAC_Address_ID) references wlan_mac_address(ID)
	)
end

go

insert into Wrong_Wlan_For_Locator
	select distinct mac.ID
		from WLAN_SSID ssid
		join WLAN_MAC_Address mac on mac.ID in (select l.WLAN_MAC_Address_ID from WLAN_Log l where l.WLAN_SSID_ID = ssid.ID)
		where 
			not exists (select * from Wrong_Wlan_For_Locator e where e.WLAN_MAC_Address_ID = mac.ID)		
		   and (
		      ssid.Value like 'mts-%' and len(ssid.Value) = 7 --��� ����� ������ � ������
		   or ssid.Value like '%portable%'
		   or ssid.Value like '%android%'
		   or ssid.Value like '%iphone%'
		   or ssid.Value like '%modem%'
		   or ssid.Value like '%metro%'
		   or mac.Value = '0015628D5AC0')
		