select *
	into #t
	from (
		select Name = N'��-�����������������', Url = 'raw://109.236.250.45:20200/'
	) t


insert into RemoteTerminalServer (Name, Url)
	select * 
		from #t t
		where not exists (select * from RemoteTerminalServer e where e.Name = t.Name)

update rts
	set Url = t.Url
	from RemoteTerminalServer rts
	join #t t on t.Name = rts.Name
	where rts.Url <> t.Url
	
drop table #t