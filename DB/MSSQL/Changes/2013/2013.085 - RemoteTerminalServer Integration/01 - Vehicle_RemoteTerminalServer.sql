if not exists (select * from sys.objects where name = 'Vehicle_RemoteTerminalServer')
begin

	create table Vehicle_RemoteTerminalServer
	(
		Vehicle_ID int not null
			constraint FK_Vehicle_RemoteTerminalServer_Vehicle_ID
				foreign key references Vehicle(Vehicle_ID),
		RemoteTerminalServer_ID int not null
			constraint FK_Vehicle_RemoteTerminalServer_RemoteTerminalServer_ID
				foreign key references RemoteTerminalServer(RemoteTerminalServer_ID),
		constraint PK_Vehicle_RemoteTerminalServer 
			primary key clustered (Vehicle_ID, RemoteTerminalServer_ID)
	)

	create table H_Vehicle_RemoteTerminalServer
	(
		H_Vehicle_RemoteTerminalServer_ID int identity(1,1)
			constraint PK_H_Vehicle_RemoteTerminalServer	
				primary key clustered,
	
		Vehicle_ID int not null,
		RemoteTerminalServer_ID int not null,
		
        TRAIL_ID        int 
			constraint FK_H_Vehicle_RemoteTerminalServer_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

end
