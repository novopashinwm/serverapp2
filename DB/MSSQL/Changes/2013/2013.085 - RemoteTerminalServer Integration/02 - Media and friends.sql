insert into MEDIA_TYPE 
	select *
		from (select ID = 5, Name = 'RemoteTerminalServer') t
		where not exists (select * from MEDIA_TYPE e where e.ID = t.ID)

insert into MEDIA (NAME, TYPE)
	select * 
		from (select name = 'Order285', type = 5) t
		where not exists (select * from MEDIA e where e.TYPE = t.type)

insert into MEDIA_ACCEPTORS (MEDIA_ID, INITIALIZATION, DESCRIPTION)
	select m.MEDIA_ID, t.Initialization, t.Desciption
		from (select Initialization = N'', Desciption = N'') t, MEDIA m
		where m.TYPE = 5
		  and not exists (select * from MEDIA_ACCEPTORS e where e.MEDIA_ID = m.media_id)
