if not exists (
	select * from sys.columns where object_id = object_id('vehicle') and name = 'StoreLog'
)
begin

	alter table Vehicle
		add StoreLog bit not null
			constraint DF_Vehicle_StoreLog default(1)
			
	alter table H_Vehicle	
		add StoreLog bit not null
			constraint DF_H_Vehicle_StoreLog default(1)
		
end