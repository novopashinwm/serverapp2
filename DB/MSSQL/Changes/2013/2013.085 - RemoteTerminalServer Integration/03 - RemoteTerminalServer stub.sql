select *
	into #t
	from (select Name = N'Local stub', Url = 'http://localhost:11233/Terminal/Order285SoapV1.4/'
	union all select N'���� ���� ����������� �� ��������', 'http://91.198.71.237:8010/gate15'
	) t


insert into RemoteTerminalServer (Name, Url)
	select * 
		from #t t
		where not exists (select * from RemoteTerminalServer e where e.Name = t.Name)

update rts
	set Url = t.Url
	from RemoteTerminalServer rts
	join #t t on t.Name = rts.Name
	where rts.Url <> t.Url
	
drop table #t