if not exists (select * from sys.objects where name = 'RemoteTerminalServer')
begin

	create table RemoteTerminalServer
	(
		RemoteTerminalServer_ID int identity(1,1)
			constraint PK_RemoteTerminalServer 
				primary key clustered,
		Name nvarchar(255),
		Url varchar(1024)
	)

end
