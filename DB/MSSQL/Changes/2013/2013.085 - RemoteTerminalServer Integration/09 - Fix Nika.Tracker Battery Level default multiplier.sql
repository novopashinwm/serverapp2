update cs
	set Default_Multiplier = 1
	from CONTROLLER_SENSOR cs
	join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
	where ct.TYPE_NAME = 'nika.tracker' 
	  and cs.Descript = 'BatteryLevel'
	  and cs.Default_Multiplier <> 1

update csm
	set Multiplier = cs.Default_Multiplier
	from CONTROLLER_SENSOR cs
	join CONTROLLER_TYPE ct on ct.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID
	join Controller c on c.CONTROLLER_TYPE_ID = ct.CONTROLLER_TYPE_ID
	join CONTROLLER_SENSOR_MAP csm on csm.CONTROLLER_ID = c.CONTROLLER_ID 
	                              and csm.CONTROLLER_SENSOR_ID = cs.CONTROLLER_SENSOR_ID 
	                              and csm.CONTROLLER_SENSOR_LEGEND_ID = cs.CONTROLLER_SENSOR_ID
	where ct.TYPE_NAME = 'nika.tracker' 
	  and cs.Descript = 'BatteryLevel'
	  and csm.Multiplier <> 1
	