if not exists (select * from sys.indexes where name = 'IX_VehicleGroup_Vehicle_Vehicle_ID')
begin

	create nonclustered index IX_VehicleGroup_Vehicle_Vehicle_ID
		on VehicleGroup_Vehicle (Vehicle_ID) include (VehicleGroup_ID)
		
end