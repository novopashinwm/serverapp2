if (not exists (select * from sys.objects where name = 'Address'))
begin

	create table [Address]
	(
		Address_ID bigint identity (1,1)
			constraint PK_Address
				primary key clustered,
		Geo geography not null,
		Language varchar(2) not null,
		Value nvarchar(500) not null,
		[Time] datetime not null,
	)  on TSS_DATA2;
end

go

if (not exists (select * from sys.indexes where name = 'IX_Address_Geo'))
begin
	CREATE SPATIAL INDEX IX_Address_Geo ON [dbo].[Address] (Geo)
		USING GEOGRAPHY_GRID 
		WITH
		(GRIDS =(
			LEVEL_1 = HIGH,
			LEVEL_2 = HIGH, 
			LEVEL_3 = HIGH,
			LEVEL_4 = HIGH)
		, CELLS_PER_OBJECT=4 /*�������� �������*/) ON TSS_DATA2;
end