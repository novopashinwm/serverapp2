if not exists (select * from sys.columns where object_id = object_id('Position_Radius') and name = 'Type')
begin
	alter table Position_Radius
		add [Type] tinyint not null
			constraint DF_Position_Radius_Type default (0)
end
go

update pr
	set [Type] = 1 --LBS
	from Position_Radius pr
	join Command_Result cr on cr.Log_Time = pr.Log_Time
	join Command c on c.ID = cr.Command_ID and 
	                  c.Target_ID = pr.Vehicle_ID
	where c.[Type_ID] = 20 --AskPositionOverMLP
	  and pr.[Type] = 0
