if not exists (select * from sys.objects where type = 'u' and name = 'VehicleNetmonitoring') 
begin

	create table VehicleNetmonitoring
	(
		Vehicle_ID int not null,
		Last_Log_Time int not null,
		UUID uniqueidentifier not null,
		constraint PK_VehicleNetmonitoring primary key clustered (Vehicle_ID),
		constraint FK_VehicleNetmonitoring_Vehicle_ID foreign key (Vehicle_ID) references Vehicle(Vehicle_ID)		 
	)

end;
