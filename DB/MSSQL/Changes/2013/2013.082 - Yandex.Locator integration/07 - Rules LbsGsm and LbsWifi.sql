insert into [Rule] (Number, Description, Client_Side)
	select r.Number, r.Description, 0
		from (
			select Number = 16, Description = 'LbsGsm' union all
			select Number = 17, Description = 'LbsWifi' 
		) r
		where not exists (select * from [Rule] e where e.Number = r.Number)