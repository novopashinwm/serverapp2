if exists (select * from sys.views where name = 'v_cell_log')
	drop view v_cell_log;
go

create view v_cell_log 
	as
	select 
		Vehicle_ID,
		Log_Time,
		Number,
		Country_Code = (select value from country_code where id = cell.country_code_id),
		Network_Code = (select value from network_code where id = cell.network_code_id),
		cell.LAC,
		cell.Cell_ID,
		SignalStrength
	from Cell_Network_Log cell (nolock)
