if not exists (select * from sys.objects where type = 'u' and name = 'VehicleLocator')
begin

	create table VehicleLocator
	(
		Vehicle_ID int not null,
		Log_Time int not null,
		constraint PK_VehicleLocator primary key clustered (Vehicle_ID, Log_Time)
	) on TSS_DATA2;
		

end