insert into Media_Type (ID, Name)
	select t.ID, t.Name
		from (
			select ID = 4, Name = 'LBS'
		) t
	where not exists (select * from Media_Type mt where mt.ID = t.ID)

insert into dbo.Media (Name, Type)
	select m.Name, m.Type
		from (
			select Name = 'YandexLocator', Type = 4
		) m
		where not exists (select * from dbo.Media e where e.Name = m.Name)
	
insert into dbo.Media_Acceptors (Media_ID, Initialization, Description)
	select	t.*
		from (
			select	m.Media_ID,
					'AF-HjE0BAAAA7NIHZgMA3wF-Z_isw1A5-Ji2L2kc-xtnPAUAAAAAAAAAAAD-vRJsoKdRjBb1XKCRsi7hlNz-Uw==' Initialization,
					'Yandex API Key' Description
				from dbo.Media m
				where m.Name = 'YandexLocator') t
		where not exists (select * from dbo.Media_Acceptors e where e.Media_ID = t.Media_ID)
