if not exists (select * from sys.types where name = 'LatLngPoint')
begin
	create type dbo.LatLngPoint 
	as table
	(
		Id int not null,
		Lat numeric(8,5) not null,
		Lng numeric(8,5) not null
	)
end