if not exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'LimitedQuantity')
begin

	alter table Billing_Service_Type
		add LimitedQuantity bit not null constraint DF_Billing_Service_Type_LimitedQuantity default (0)

end;
go

update Billing_Service_Type
	set LimitedQuantity = 1
	where Service_Type in ('FRNIKA.MLP', 'FRNIKA.MLP.ChargingEconom', 'FRNIKA.SMS.ChargingEconom', 'FRNIKA.SMS.M2M', 'FRNIKA.SMS.LBS', 'Free.SMS.Confirmation')
	and LimitedQuantity <> 1

update Billing_Service_Type
	set LimitedQuantity = 0
	where Service_Type in ('FRNIKA.MLP')
	and LimitedQuantity <> 0