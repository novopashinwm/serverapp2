--�������� ������� ��� �������� ���������� � ������������ � ������, ��� ��������� ������ "���� LBS" ������� ��� �����������

declare @pattern table (
	Service_Type_Category varchar(50), 
	Vehicle_Kind_Name nvarchar(32), 
	Controller_Type_Name nvarchar(50)
)

set nocount on
insert into @pattern values 
	('LBS',	'��������� �������','MLP')
set nocount off

update bst
	set 
		bst.Vehicle_Kind_ID = vk.Vehicle_Kind_ID,
		bst.Controller_Type_ID = ct.Controller_Type_ID
	from @pattern p
	join Billing_Service_Type bst on bst.Service_Type_Category = p.Service_Type_Category
	join Vehicle_Kind vk on vk.Name = p.Vehicle_Kind_Name
	join Controller_Type ct on ct.Type_Name = p.Controller_Type_Name
	where (bst.Vehicle_Kind_ID is null or  bst.Vehicle_Kind_ID <> vk.Vehicle_Kind_ID)
	   or (bst.Controller_Type_ID is null  or bst.Controller_Type_ID <> ct.Controller_Type_ID)

update bst
	set Controller_Type_ID = null,
	    Vehicle_Kind_ID = null
	from Billing_Service_Type bst 
	where Service_Type = 'FRNIKA.MLP' and (
	      Controller_Type_ID is not null
	   or Vehicle_Kind_ID is not null)