if not exists (select * from sys.columns where object_id = object_id('Picture_Log') and name = 'Camera_Number')
begin 
	alter table Picture_Log
		add Camera_Number int not null constraint DF_Picture_Log_Camera_Number default (0)
end
go

if not exists (select * from sys.columns where object_id = object_id('Picture_Log') and name = 'Photo_Number')
begin 
	alter table Picture_Log
		add Photo_Number int not null constraint DF_Picture_Log_Photo_Number default (0)
end
go

if exists (select * from sys.indexes where name = 'PK_Picture_Log')
begin
	alter table Picture_Log
		drop constraint PK_Picture_Log
end
go

if not exists (select * from sys.indexes where name = 'PK_Picture_Log_VID_LT_CN_PN')
begin
	alter table Picture_Log
		add constraint PK_Picture_Log_VID_LT_CN_PN primary key clustered (Vehicle_ID, Log_Time, Camera_Number, Photo_Number)
end