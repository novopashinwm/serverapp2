insert into CommandTypes (id, code)
	select id, code
		from (
			select id = 33, code = 'CapturePicture') t
		where not exists (select * from CommandTypes e where e.id = t.id)