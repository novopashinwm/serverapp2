﻿IF (OBJECT_ID(N'[dbo].[DEPARTMENT_MOVIREG_ACCOUNT]') IS NULL)
BEGIN
	CREATE TABLE [dbo].[DEPARTMENT_MOVIREG_ACCOUNT]
	(
		[DEPARTMENT_MOVIREG_ACCOUNT_ID] int NOT NULL IDENTITY (1, 1),
		[MOVIREG_SERVER_URL]            varchar(1000) NOT NULL,
		[MOVIREG_ACCOUNT]               varchar(100) NOT NULL,
		[MOVIREG_PASSWORD]              varchar(100) NOT NULL,
		[DEPARTMENT_ID]                 int NOT NULL,
		[MOVIREG_SERVER_TIMEOFFSET]     int NOT NULL,
		CONSTRAINT [PK_DEPARTMENT_MOVIREG_ACCOUNT]            PRIMARY KEY CLUSTERED ([DEPARTMENT_MOVIREG_ACCOUNT_ID]),
		CONSTRAINT [FK_DEPARTMENT_MOVIREG_ACCOUNT_DEPARTMENT] FOREIGN KEY           ([DEPARTMENT_ID])
			REFERENCES [dbo].[DEPARTMENT]                                           ([DEPARTMENT_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица для хранения УЗ для сервера MOVIREG',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ в таблице DEPARTMENT_MOVIREG_ACCOUNT',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'DEPARTMENT_MOVIREG_ACCOUNT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'URL сервера MOVIREG',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'MOVIREG_SERVER_URL';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'УЗ сервера MOVIREG',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'MOVIREG_ACCOUNT';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Пароль УЗ сервера MOVIREG',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'MOVIREG_PASSWORD';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Внешний ключ - ссылка на таблицу департаментов',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'DEPARTMENT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Смещение времени для корректировки принятого значения времени',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'DEPARTMENT_MOVIREG_ACCOUNT',
		@level2type = N'COLUMN',
		@level2name = N'MOVIREG_SERVER_TIMEOFFSET';
END