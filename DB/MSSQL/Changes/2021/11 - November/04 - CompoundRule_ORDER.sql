﻿IF (OBJECT_ID(N'[orders].[CompoundRule_ORDER]') IS NULL)
BEGIN
	CREATE TABLE [orders].[CompoundRule_ORDER]
	(
		[CompoundRule_ID] int NOT NULL,
		[ORDER_ID]        int NOT NULL,
		CONSTRAINT [PK_CompoundRule_ORDER]              PRIMARY KEY CLUSTERED ([CompoundRule_ID], [ORDER_ID]),
		CONSTRAINT [FK_CompoundRule_ORDER_CompoundRule] FOREIGN KEY           ([CompoundRule_ID]) REFERENCES [dbo].[CompoundRule] ([CompoundRule_ID]),
		CONSTRAINT [FK_CompoundRule_ORDER_ORDER]        FOREIGN KEY           ([ORDER_ID])        REFERENCES [orders].[ORDER]     ([ORDER_ID])
	);
	CREATE NONCLUSTERED INDEX [IX_CompoundRule_ORDER_ORDER_ID]
		ON [orders].[CompoundRule_ORDER]([ORDER_ID]) INCLUDE([CompoundRule_ID]);

	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица связей правил и заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_ORDER',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу правил',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_ORDER',
		@level2type = N'COLUMN',
		@level2name = N'CompoundRule_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_ORDER',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';

	CREATE TABLE [orders].[H_CompoundRule_ORDER]
	(
		[CompoundRule_ID] int               NOT NULL,
		[ORDER_ID]        int               NOT NULL,
		[TRAIL_ID]        int               NOT NULL,
		[ACTION]          nvarchar(6)       NOT NULL,
		[ACTUAL_TIME]     datetime          NOT NULL,
		[ID]              int IDENTITY(1,1) NOT NULL,
		CONSTRAINT [PK_H_CompoundRule_ORDER]
			PRIMARY KEY CLUSTERED ([ID] ASC),
		CONSTRAINT [FK_H_CompoundRule_ORDER_TRAIL_ID]
			FOREIGN KEY ([TRAIL_ID]) REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
	);
END