﻿IF (OBJECT_ID(N'[orders].[CompoundRule_TimePoint]') IS NULL)
BEGIN

	CREATE TABLE [dbo].[CompoundRule_TimePoint]
	(
		[CompoundRule_TimePoint_ID] int IDENTITY (1, 1) NOT NULL,
		[CompoundRule_ID]           int                 NOT NULL,
		[LogTime]                   int                 NOT NULL,
		CONSTRAINT [PK_CompoundRule_TimePoint]              PRIMARY KEY CLUSTERED ([CompoundRule_TimePoint_ID]),
		CONSTRAINT [FK_CompoundRule_TimePoint_CompoundRule] FOREIGN KEY           ([CompoundRule_ID]) REFERENCES [dbo].[CompoundRule] ([CompoundRule_ID]),
	);

	CREATE NONCLUSTERED INDEX [IX_CompoundRule_TimePoint_CompoundRule_ID]
		ON [dbo].[CompoundRule_TimePoint]([CompoundRule_ID]);

	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица условий меток времени',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_TimePoint',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу правил',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_TimePoint',
		@level2type = N'COLUMN',
		@level2name = N'CompoundRule_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Метка времени, в виде LogTime (секунды от 1970-01-01 UTC)',
		@level0type = N'SCHEMA',
		@level0name = N'dbo',
		@level1type = N'TABLE',
		@level1name = N'CompoundRule_TimePoint',
		@level2type = N'COLUMN',
		@level2name = N'LogTime';

	CREATE TABLE [dbo].[H_CompoundRule_TimePoint]
	(
		[CompoundRule_TimePoint_ID] int               NOT NULL,
		[CompoundRule_ID]           int               NOT NULL,
		[LogTime]                   int               NOT NULL,
		[TRAIL_ID]                  int               NOT NULL,
		[ACTION]                    nvarchar(6)       NOT NULL,
		[ACTUAL_TIME]               datetime          NOT NULL,
		[ID]                        int IDENTITY(1,1) NOT NULL,
		CONSTRAINT [PK_H_CompoundRule_TimePoint]
			PRIMARY KEY CLUSTERED ([ID]),
		CONSTRAINT [FK_H_CompoundRule_TimePoint_TRAIL_ID]
			FOREIGN KEY ([TRAIL_ID]) REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
	);
END