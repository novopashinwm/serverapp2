﻿IF NOT EXISTS (SELECT * FROM sys.columns WHERE [object_id] = OBJECT_ID('CompoundRule') AND [name] = 'ProcessingBegAt')
BEGIN

	ALTER TABLE [dbo].[CompoundRule]
		ADD
			[ProcessingBegAt] int CONSTRAINT [DF_CompoundRule_ProcessingBegAt] DEFAULT (([dbo].[utc2lt](GETUTCDATE()))) NULL,
			[ProcessingEndAt] int                                                                                       NULL;
	ALTER TABLE [dbo].[H_CompoundRule]
		ADD
			[ProcessingBegAt] int NULL,
			[ProcessingEndAt] int NULL;

END