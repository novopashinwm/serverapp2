﻿IF NOT EXISTS (SELECT * FROM sys.columns WHERE [object_id] = OBJECT_ID('CompoundRule_Message_Template_Message_Template') AND [name] = 'ProcessingBegAt')
BEGIN

	ALTER TABLE [dbo].[CompoundRule_Message_Template]
		ADD
			[ToOwnPhoneMaxQty]  int NULL,
			[ToAppClientMaxQty] int NULL,
			[ToOwnEmailMaxQty]  int NULL;
	ALTER TABLE [dbo].[H_CompoundRule_Message_Template]
		ADD
			[ToOwnPhoneMaxQty]  int NULL,
			[ToAppClientMaxQty] int NULL,
			[ToOwnEmailMaxQty]  int NULL;

END