﻿CREATE SCHEMA [orders] AUTHORIZATION [dbo];
GO

EXEC sp_addextendedproperty
	@name       = N'MS_Description',
	@value      = N'Схема управления заказами.',
	@level0type = N'SCHEMA',
	@level0name = N'orders',
	@level1type = NULL,
	@level1name = NULL,
	@level2type = NULL,
	@level2name = NULL
GO