﻿IF (OBJECT_ID(N'[orders].[CLIENT]') IS NULL)
BEGIN
	CREATE TABLE [orders].[CLIENT]
	(
		[CLIENT_ID]     int            NOT NULL IDENTITY (1, 1),
		[CLIENT_EXT_ID] nvarchar( 128) NOT NULL,
		[CUSTOMER_ID]   int            NOT NULL,
		[CLIENT_NAME]   nvarchar(1024) NOT NULL,
		[CLIENT_INN]    nvarchar( 128) NOT NULL,
		[CLIENT_KPP]    nvarchar( 128) NOT NULL,
		CONSTRAINT [PK_CLIENT]            PRIMARY KEY CLUSTERED ([CLIENT_ID]),
		CONSTRAINT [FK_CLIENT_DEPARTMENT] FOREIGN KEY           ([CUSTOMER_ID])
			REFERENCES [dbo].[DEPARTMENT]                       ([DEPARTMENT_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица клиентов заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ клиента заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Внешний идентификатор клиента заказа, по информации клиента системы',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_EXT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Идентификатор клиента системы (департамент/корпорант), наименования для одного клиента могут быть разные у двух клиентов системы',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CUSTOMER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Наименование клиента заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_NAME';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'ИНН клиента заказа (часть естественного ключа)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_INN';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'КПП клиента заказа (часть естественного ключа)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'CLIENT',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_KPP';
END