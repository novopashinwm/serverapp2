﻿IF (OBJECT_ID(N'[orders].[ROUTE_POINT]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ROUTE_POINT]
	(
		[ROUTE_POINT_ID]   int            NOT NULL IDENTITY (1, 1),
		[ORDER_ID]         int            NOT NULL,
		[ARRIVAL_LOG_TIME] int            NOT NULL,
		[LAT]              numeric(8,5)   NOT NULL,
		[LNG]              numeric(8,5)   NOT NULL,
		[NAME]             nvarchar(1024)     NULL,
		CONSTRAINT [PK_ROUTE_POINT]       PRIMARY KEY CLUSTERED ([ROUTE_POINT_ID]),
		CONSTRAINT [FK_ROUTE_POINT_ORDER] FOREIGN KEY ([ORDER_ID])
			REFERENCES [orders].[ORDER] ([ORDER_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица контрольных точек заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный контрольной точки заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'ROUTE_POINT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Время прибытия в контрольную точку (в секундах от 1970-01-01 UTC)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'ARRIVAL_LOG_TIME';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Широта контрольной точки',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'LAT';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Долгота контрольной точки',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'LNG';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Наименование контрольной точки',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT',
		@level2type = N'COLUMN',
		@level2name = N'NAME';
END