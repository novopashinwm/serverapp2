﻿IF (OBJECT_ID(N'[orders].[ROUTE_POINT_ALERT]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ROUTE_POINT_ALERT]
	(
		[ROUTE_POINT_ALERT_ID]                       int NOT NULL IDENTITY (1, 1),
		[ROUTE_POINT_ID]                             int NOT NULL,
		[RADIUS_IN_METERS]                           int NOT NULL,
		[ALERT_MINUTES_BEFORE_ARRIVAL_NOT_IN_RADIUS] int NOT NULL,
		CONSTRAINT [PK_ROUTE_POINT_ALERT]             PRIMARY KEY CLUSTERED ([ROUTE_POINT_ALERT_ID]),
		CONSTRAINT [FK_ROUTE_POINT_ALERT_ROUTE_POINT] FOREIGN KEY           ([ROUTE_POINT_ID])
			REFERENCES [orders].[ROUTE_POINT]                               ([ROUTE_POINT_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица оповещений для контрольных точек',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT_ALERT',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ оповещения для контрольной точки',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT_ALERT',
		@level2type = N'COLUMN',
		@level2name = N'ROUTE_POINT_ALERT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу контрольных точек',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT_ALERT',
		@level2type = N'COLUMN',
		@level2name = N'ROUTE_POINT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Радиус вокруг контрольной точки',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT_ALERT',
		@level2type = N'COLUMN',
		@level2name = N'RADIUS_IN_METERS';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'За сколько минут до времени прибытия в контрольную точку уведомить, если объект не внутри радиуса',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ROUTE_POINT_ALERT',
		@level2type = N'COLUMN',
		@level2name = N'ALERT_MINUTES_BEFORE_ARRIVAL_NOT_IN_RADIUS';
END