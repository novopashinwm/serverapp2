﻿IF (OBJECT_ID(N'[orders].[ORDER_ZONEGROUP]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ORDER_ZONEGROUP]
	(
		[ORDER_ZONEGROUP_ID] int NOT NULL IDENTITY (1, 1),
		[ORDER_ID]           int NOT NULL,
		[ZONEGROUP_ID]       int NOT NULL,
		CONSTRAINT [PK_ORDER_ZONEGROUP]           PRIMARY KEY CLUSTERED ([ORDER_ZONEGROUP_ID]),
		CONSTRAINT [FK_ORDER_ZONEGROUP_ORDER]     FOREIGN KEY           ([ORDER_ID])
			REFERENCES [orders].[ORDER]                                 ([ORDER_ID]),
		CONSTRAINT [FK_ORDER_ZONEGROUP_ZONEGROUP] FOREIGN KEY           ([ZONEGROUP_ID])
			REFERENCES [dbo].[ZONEGROUP]                                ([ZONEGROUP_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица связей заказа с группой геозон',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_ZONEGROUP',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ связи заказа с группой геозон',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_ZONEGROUP',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ZONEGROUP_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_ZONEGROUP',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу групп геозон',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_ZONEGROUP',
		@level2type = N'COLUMN',
		@level2name = N'ZONEGROUP_ID';
END