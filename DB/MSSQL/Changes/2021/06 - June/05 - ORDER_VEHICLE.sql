﻿IF (OBJECT_ID(N'[orders].[ORDER_VEHICLE]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ORDER_VEHICLE]
	(
		[ORDER_VEHICLE_ID] int NOT NULL IDENTITY (1, 1),
		[ORDER_ID]         int NOT NULL,
		[VEHICLE_ID]       int NOT NULL,
		CONSTRAINT [PK_ORDER_VEHICLE]          PRIMARY KEY CLUSTERED ([ORDER_VEHICLE_ID]),
		CONSTRAINT [FK_ORDER_VEHICLE_ORDER]    FOREIGN KEY           ([ORDER_ID])
			REFERENCES [orders].[ORDER]                              ([ORDER_ID]),
		CONSTRAINT [FK_ORDER_VEHICLE_VEHICLE]  FOREIGN KEY           ([VEHICLE_ID])
			REFERENCES [dbo].[VEHICLE]                               ([VEHICLE_ID]),
	);
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица связей заказов и объектов наблюдения',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_VEHICLE',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ связи заказов и объектов наблюдения',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_VEHICLE',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_VEHICLE_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_VEHICLE',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу объектов наблюдения',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_VEHICLE',
		@level2type = N'COLUMN',
		@level2name = N'VEHICLE_ID';
END