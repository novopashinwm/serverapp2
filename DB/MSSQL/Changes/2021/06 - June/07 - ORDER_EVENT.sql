﻿IF (OBJECT_ID(N'[orders].[ORDER_EVENT]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ORDER_EVENT]
	(
		[ORDER_EVENT_ID] int            NOT NULL IDENTITY (1, 1),
		[ORDER_ID]       int            NOT NULL,
		[LOG_TIME]       int            NOT NULL,
		[MESSAGE]        nvarchar(1024) NOT NULL,
		CONSTRAINT [PK_ORDER_EVENT]       PRIMARY KEY CLUSTERED ([ORDER_EVENT_ID]),
		CONSTRAINT [FK_ORDER_EVENT_ORDER] FOREIGN KEY           ([ORDER_ID])
			REFERENCES [orders].[ORDER] ([ORDER_ID]),
	);
	ALTER TABLE [orders].[ORDER_EVENT] ADD CONSTRAINT [DF_ORDER_EVENT_LOG_TIME] DEFAULT (([dbo].[utc2lt](GETUTCDATE()))) FOR [LOG_TIME];
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица событий заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_EVENT',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ события заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_EVENT',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_EVENT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_EVENT',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Метка времени события заказа (в секундах от 1970-01-01 UTC)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_EVENT',
		@level2type = N'COLUMN',
		@level2name = N'LOG_TIME';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Текст события заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER_EVENT',
		@level2type = N'COLUMN',
		@level2name = N'MESSAGE';
END