﻿IF (OBJECT_ID(N'[orders].[ORDER]') IS NULL)
BEGIN
	CREATE TABLE [orders].[ORDER]
	(
		[ORDER_ID]           int           NOT NULL IDENTITY (1, 1),
		[ORDER_EXT_ID]       nvarchar(128) NOT NULL,
		[ORDER_OWNER_ID]     int           NOT NULL,
		[ORDER_WATCHER_ID]   int           NOT NULL,
		[CLIENT_ID]          int           NOT NULL,
		[CREATED_AT]         int           NOT NULL,
		[DELETED_AT]         int               NULL,
		CONSTRAINT [PK_ORDER]            PRIMARY KEY CLUSTERED ([ORDER_ID]),
		CONSTRAINT [FK_ORDER_DEPARTMENT] FOREIGN KEY           ([ORDER_OWNER_ID])
			REFERENCES [dbo].[DEPARTMENT]                      ([DEPARTMENT_ID]),
		CONSTRAINT [FK_ORDER_CLIENT]     FOREIGN KEY           ([CLIENT_ID])
			REFERENCES [orders].[CLIENT]                       ([CLIENT_ID]),
		CONSTRAINT [FK_ORDER_OPERATOR]   FOREIGN KEY           ([ORDER_WATCHER_ID])
			REFERENCES [dbo].[OPERATOR]                        ([OPERATOR_ID]),
	);
	ALTER TABLE [orders].[ORDER] ADD CONSTRAINT [DF_ORDER_CREATED_AT] DEFAULT (([dbo].[utc2lt](GETUTCDATE()))) FOR [CREATED_AT];
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Таблица заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = NULL,
		@level2name = NULL;
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Суррогатный ключ заказа',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Внешний идентификатор заказа, уникальность не проверяем',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_EXT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Идентификатор владельца заказа (департамент/корпорант)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_OWNER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу клиентов заказов',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'CLIENT_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Ссылка на таблицу операторов системы (наблюдающий, ответственный за заказ)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'ORDER_WATCHER_ID';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Дата создания заказа (в секундах от 1970-01-01 UTC)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'CREATED_AT';
	EXEC sp_addextendedproperty @name = N'MS_Description',
		@value      = N'Дата удаления заказа (в секундах от 1970-01-01 UTC)',
		@level0type = N'SCHEMA',
		@level0name = N'orders',
		@level1type = N'TABLE',
		@level1name = N'ORDER',
		@level2type = N'COLUMN',
		@level2name = N'DELETED_AT';
END