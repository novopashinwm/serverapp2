﻿IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operatorgroup_department_right_id')
	CREATE INDEX [IX_v_operator_vehicle_right_operatorgroup_department_right_id]
		ON [dbo].[v_operator_vehicle_right_operatorgroup_department] ([right_id])
GO
IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_vehicle_right_id')
	CREATE INDEX [IX_v_operator_vehicle_right_operator_vehicle_right_id]
		ON [dbo].[v_operator_vehicle_right_operator_vehicle] ([right_id])
GO
IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_vehicle_admin_RIGHT_ID')
	CREATE INDEX [IX_v_operator_vehicle_right_operator_vehicle_admin_RIGHT_ID]
		ON [dbo].[v_operator_vehicle_right_operator_vehicle_admin] ([RIGHT_ID])
GO