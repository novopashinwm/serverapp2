﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_department_admin_RIGHT_ID')
		CREATE INDEX [IX_v_operator_vehicle_right_operator_department_admin_RIGHT_ID]
			ON [dbo].[v_operator_vehicle_right_operator_department_admin] ([RIGHT_ID])

	IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operatorgroup_department_admin_RIGHT_ID')
		CREATE INDEX [IX_v_operator_vehicle_right_operatorgroup_department_admin_RIGHT_ID]
			ON [dbo].[v_operator_vehicle_right_operatorgroup_department_admin] ([RIGHT_ID])

	IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_department_right_id')
		CREATE INDEX [IX_v_operator_vehicle_right_operator_department_right_id]
			ON [dbo].[v_operator_vehicle_right_operator_department] ([right_id])

	IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_operator_vehicle_right_operator_department_admin_OPERATOR_ID_RIGHT_ID')
		CREATE INDEX [IX_v_operator_vehicle_right_operator_department_admin_OPERATOR_ID_RIGHT_ID]
			ON [dbo].[v_operator_vehicle_right_operator_department_admin] ([OPERATOR_ID], [RIGHT_ID])
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO