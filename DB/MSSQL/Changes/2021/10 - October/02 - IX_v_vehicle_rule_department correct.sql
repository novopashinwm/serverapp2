﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	IF EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_vehicle_rule_department' AND N'v_vehicle_rule_vehiclegroup' = OBJECT_NAME([object_id]))
		DROP INDEX [IX_v_vehicle_rule_department] ON [dbo].[v_vehicle_rule_vehiclegroup];

	IF NOT EXISTS (SELECT * FROM SYS.INDEXES WHERE [name] = 'IX_v_vehicle_rule_department')
		CREATE INDEX [IX_v_vehicle_rule_department]
			ON [dbo].[v_vehicle_rule_department] ([Vehicle_id], [Rule_ID]);
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO