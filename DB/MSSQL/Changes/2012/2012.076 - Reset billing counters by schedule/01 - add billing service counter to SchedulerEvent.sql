set identity_insert SchedulerEvent on
go

insert into schedulerevent (SchedulerEvent_ID, Plugin_Name, Comment)
	select * from (
		select 
			  ID = 76
			, DotNetType = 'FORIS.TSS.ServerApplication.Billing.BillingServiceCounterReseter, FORIS.TSS.ServerApplication'
			, UserFriendlyName = 'Reset billing service counter value'
	) t
	where not exists (select * from SchedulerEvent e where SchedulerEvent_ID = ID)

go

set identity_insert SchedulerEvent off