if not exists (select * from sys.objects where name = 'Billing_Service_SchedulerQueue')
begin

	create table Billing_Service_SchedulerQueue
	(
		Billing_Service_ID int not null
			constraint FK_Billing_Service_SchedulerQueue_Billing_Service_ID
				foreign key references Billing_Service(ID),
		SchedulerQueue_ID int not null
			constraint FK_Billing_Service_SchedulerQueue_ID
				foreign key references SchedulerQueue(SchedulerQueue_ID),
		constraint PK_Billing_Service_SchedulerQueue
			primary key clustered (Billing_Service_ID)
	)

	create table H_Billing_Service_SchedulerQueue
	(
        [H_Billing_Service_SchedulerQueue_ID] INT IDENTITY(1,1) NOT NULL
			CONSTRAINT [PK_H_Billing_Service_SchedulerQueue] 
						PRIMARY KEY CLUSTERED,		
	
		Billing_Service_ID int not null,
		SchedulerQueue_ID int not null,

        TRAIL_ID        int 
			constraint FK_H_Billing_Service_SchedulerQueue_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
	)

end