if not exists (select * from sys.indexes where name = 'IX_Zone_Matrix_Zone_ID_Node')
begin

	create nonclustered index IX_Zone_Matrix_Zone_ID_Node on Zone_Matrix(ZONE_ID, NODE, SCALE) include (IsFull)

end

go

exec sp_recompile 'GetGeoZonesForPositions'