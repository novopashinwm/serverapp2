if exists (select * from sys.types where name = 'Vehicle_Log_Time_Span_Param')
	drop type dbo.Vehicle_Log_Time_Span_Param
go	
create type dbo.Vehicle_Log_Time_Span_Param as table
(
	Vehicle_ID int,
	Log_Time_From int,
	Log_Time_To int
)
go