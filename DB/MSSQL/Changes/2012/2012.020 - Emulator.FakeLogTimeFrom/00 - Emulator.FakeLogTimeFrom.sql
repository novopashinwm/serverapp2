if not exists (select * from sys.columns where object_id = object_id('dbo.Emulator') and name = 'FakeLogTimeFrom')
begin
	alter table Emulator
		add FakeLogTimeFrom int;
end;
go

if exists (select * from sys.columns where object_id = object_id('dbo.Emulator') and name = 'DateOffset')
begin
	alter table Emulator
		drop column DateOffset;
end;
go
