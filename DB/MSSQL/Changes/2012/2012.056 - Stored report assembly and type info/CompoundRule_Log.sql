if exists (select * from sys.columns where object_id = object_id('Event_Log') and name = 'ID')
begin
	drop table Event_Log;
end
go

if not exists (select * from sys.objects where name = 'CompoundRule_Log')
begin

	create table CompoundRule_Log
	(
		Vehicle_ID int not null,
		Log_Time int not null,
		CompoundRule_ID int not null,
		constraint PK_CompoundRule_Log
			primary key clustered (Vehicle_ID, Log_Time, CompoundRule_ID)
	) on TSS_DATA2

end