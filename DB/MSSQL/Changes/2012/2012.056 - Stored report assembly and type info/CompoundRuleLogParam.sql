if not exists (select * from sys.types where name = 'CompoundRuleLogParam')
begin

	create type dbo.CompoundRuleLogParam as table
	(
		Vehicle_ID int not null,
		Log_Time int not null,
		CompoundRule_ID int not null,
		primary key clustered (Vehicle_ID, Log_Time, CompoundRule_ID)
	)	

end