/*��������� ���������� �����, 
����������� ������� ������ ��� ������������ � ������ ���������� ����� ������� ��������� �� ������*/

if not exists (select * from sys.objects where object_id = object_id('dbo.Country'))
begin

	create table dbo.Country
	(
		Country_ID int identity
			constraint PK_Country
				primary key clustered,
		Name nvarchar(255)
	)

	insert into dbo.Country (Name)
		select 'India'

end
go

if not exists (select * from sys.columns where object_id = object_id('dbo.Department') and name = 'Country_ID')
begin
	
	alter table dbo.Department
		add Country_ID int constraint FK_Department_Country_ID foreign key references dbo.Country(Country_ID)

	alter table dbo.H_Department
		add Country_ID int
	
end
go

if not exists (select * from sys.columns where object_id = object_id('dbo.Fuel_Type') and name = 'Country_ID')
begin
	alter table dbo.Fuel_Type
		add Country_ID int constraint FK_Fuel_Type_Country_ID foreign key references dbo.Country(Country_ID)
end
go
	insert into dbo.Fuel_Type (ID, Name, Country_ID)
		select t.ID, t.Name, c.Country_ID
			from (
				select	     ID =  7, Name = 'Diesel'
				union select ID =  8, Name = 'Gas'
				union select ID =  9, Name = 'Petrol'
				union select ID = 10, Name = 'Petrol super'
				union select ID = 11, Name = 'Petrol super+') t, dbo.Country c
			where c.Name = 'India'
			and not exists (
				select * 
					from dbo.Fuel_Type e
					where e.Country_ID = c.Country_ID
					  and e.Name = t.Name)

