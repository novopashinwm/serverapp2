if exists (select * from sys.columns where object_id = object_id('dbo.SensorValueChange') and name = 'Controller_ID')
begin
	declare @sql nvarchar(max)
	set @sql = '
	alter table dbo.SensorValueChange
		add Vehicle_ID int'
		
	exec sp_executesql @sql	
	
	set @sql = '
	update svc
		set Vehicle_ID = c.Vehicle_ID
		from dbo.SensorValueChange svc
		join dbo.Controller c on c.Controller_ID = svc.Controller_ID'
		
	exec sp_executesql @sql	
	
	set @sql = '
	delete SensorValueChange where Vehicle_ID is null'
		
	exec sp_executesql @sql	
	
	set @sql = '	
	alter table dbo.SensorValueChange
		alter column Vehicle_ID int not null '
		
	exec sp_executesql @sql	
	
	set @sql = '
	alter table dbo.SensorValueChange
		drop constraint FK_SensorValueChange_Vehicle_ID'
		
	exec sp_executesql @sql	
	
	set @sql = '
	alter table dbo.SensorValueChange
		drop column Controller_ID'
		
	exec sp_executesql @sql	
	
	set @sql = '
	alter table dbo.SensorValueChange	
		add constraint FK_SensorValueChange_Vehicle_ID foreign key (Vehicle_ID) references dbo.Vehicle(Vehicle_ID)'
		
	exec sp_executesql @sql	
	
	set @sql = '
		
	delete  H_SensorValueChange
	
	alter table dbo.H_SensorValueChange
		add Vehicle_ID int not null		'
		
	exec sp_executesql @sql	
	
	set @sql = '
	
	alter table dbo.H_SensorValueChange
		drop column Controller_ID		'
		
	exec sp_executesql @sql	
		
end;