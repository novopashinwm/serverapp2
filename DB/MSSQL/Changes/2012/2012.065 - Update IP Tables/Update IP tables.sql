/*
����� cities.txt � cidr_optim.txt �������� � D:\, ��������� �������
*/

DELETE FROM dbo.IPValues
DELETE FROM dbo.IPRegions

BULK INSERT dbo.IPRegions
   FROM 'd:\cities.txt'
   WITH 
      (
         FIELDTERMINATOR ='%',
         ROWTERMINATOR ='*',
         CODEPAGE = 'ACP'
      )



create table #tmp (intFrom BIGINT, intTo BIGINT, ipLen NVARCHAR(200), countryCode NVARCHAR(5), CountryId INT)

BULK INSERT #tmp
	FROM  'd:\cidr_optim.txt'
	WITH 
      (
         FIELDTERMINATOR ='%',
         ROWTERMINATOR ='*',
         CODEPAGE = 'ACP'
      )
      
INSERT INTO dbo.IPValues (CountryCode, CityId, RangeFrom, RangeTo, RangeFromInt, RangeToInt)
	SELECT countryCode, CountryId, CAST(intFrom as binary(4)), CAST(intTo as binary(4)), intFrom, intTo FROM #tmp

DROP TABLE #tmp

