DECLARE @controllerTypeId INT = (
SELECT CONTROLLER_TYPE_ID FROM dbo.CONTROLLER_TYPE WHERE TYPE_NAME='AzimuthGSM')


IF NOT EXISTS( SELECT * FROM dbo.CONTROLLER_SENSOR
	WHERE CONTROLLER_TYPE_ID=@controllerTypeId AND NUMBER = 7 )
BEGIN
	INSERT INTO dbo.CONTROLLER_SENSOR 
		(CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, NUMBER, MAX_VALUE, MIN_VALUE, 
			DEFAULT_VALUE, BITS, DESCRIPT, VALUE_EXPIRED, Default_Sensor_Legend_ID, Default_Multiplier, Default_Constant, Mandatory)
	select @controllerTypeId, 2, 7, 1, 0, 0, 1, 'Ignition', NULL, controller_sensor_legend_id, NULL, NULL, 1
		from controller_sensor_legend where number = 4
	
END

UPDATE  dbo.CONTROLLER_TYPE SET POS_SMOOTH_SUPPORT = 1 WHERE CONTROLLER_TYPE_ID=@controllerTypeId






