IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Phone]') AND name = N'UQ_Phone_Phone')
ALTER TABLE [dbo].[Phone] DROP CONSTRAINT [UQ_Phone_Phone]
GO
