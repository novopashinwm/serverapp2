/* Consolidated poi subscription. */
alter table schedulerevent
	alter column config_xml nvarchar(max)

go

/* Consolidated poi subscription. */
if (not exists(select 1 from schedulerevent
				where config_xml = 'FORIS.TSS.TransportDispatcher.Reports.RoutingSheetSummary, FORIS.TSS.TransportDispatcher.ReportGeoPoints'))
	insert into schedulerevent (plugin_name, comment, config_xml)
		select
			'FORIS.TSS.ServerApplication.Mail.GenericReport_SchedulerPlugin, FORIS.TSS.ServerApplication',
			'POI Report. Consolidated',
			'FORIS.TSS.TransportDispatcher.Reports.RoutingSheetSummary, FORIS.TSS.TransportDispatcher.ReportGeoPoints'

/* Itinerary subscription. */
if (not exists(select 1 from schedulerevent
				where config_xml = 'FORIS.TSS.TransportDispatcher.Reports.RoutingSheet, FORIS.TSS.TransportDispatcher.ReportGeoPoints'))
	insert into schedulerevent (plugin_name, comment, config_xml)
		select
			'FORIS.TSS.ServerApplication.Mail.GenericReport_SchedulerPlugin, FORIS.TSS.ServerApplication',
			'Itinerary report',
			'FORIS.TSS.TransportDispatcher.Reports.RoutingSheet, FORIS.TSS.TransportDispatcher.ReportGeoPoints'