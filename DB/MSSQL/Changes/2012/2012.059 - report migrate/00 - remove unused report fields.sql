if exists(select * from sys.columns where Name = N'BackLinkReportId' and Object_ID = Object_ID(N'report')) 
	alter table dbo.report
		drop column BackLinkReportId

if exists (select * from sys.objects where name = 'Report_SchedulerEvent_Id_FK' and type = 'F')
	alter table dbo.report
		drop constraint Report_SchedulerEvent_Id_FK

if exists(select * from sys.columns where Name = N'SchedulerEvent_Id' and Object_ID = Object_ID(N'report')) 
	alter table dbo.report
		drop column SchedulerEvent_Id
		