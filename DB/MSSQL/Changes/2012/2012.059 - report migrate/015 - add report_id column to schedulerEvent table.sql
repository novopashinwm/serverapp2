if not exists(
	select syscolumns.name
		from sysobjects 
		join syscolumns on sysobjects.id = syscolumns.id
	where sysobjects.xtype='U' AND sysobjects.name = 'SchedulerEvent' AND syscolumns.name = 'Report_Id'
) 
begin
	alter table dbo.SchedulerEvent
		add Report_Id int null
	alter table dbo.SchedulerEvent
		add constraint FK_SchedulerEvent_Report_Id
		foreign key (Report_Id)
		references REPORT(REPORT_ID)
end

if not exists(
	select syscolumns.name
		from sysobjects 
		join syscolumns on sysobjects.id = syscolumns.id
	where sysobjects.xtype='U' AND sysobjects.name = 'H_SchedulerEvent' AND syscolumns.name = 'Report_Id'
) 
begin
	alter table dbo.H_SchedulerEvent
		add Report_Id int null
end
