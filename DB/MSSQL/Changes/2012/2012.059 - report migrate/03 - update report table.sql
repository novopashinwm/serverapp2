update report set
	Name = 'speedViolationReport',
	GroupName = 'gpsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportSpeedViolation',
	ClassParametersName = 'SpeedViolationReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'B7CB4C01-50DB-4BF4-82AB-F71E5D595BCC'

update report set
	Name = 'reportTraffic',
	GroupName = 'commonReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportWebGisMoveHistory',
	ClassParametersName = 'MoveReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'F824B9A6-F435-4940-9788-1C46495BB73F'

update report set
	Name = 'reportGroupFuelCost',
	GroupName = 'gpsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportFuelCost',
	ClassParametersName = 'FuelCostReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = '7FA68680-9660-4B9A-9DF0-986F6F9A4F56'

update report set
	Name = 'reportGeoZoneOver',
	GroupName = 'commonReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportZoneOver',
	ClassParametersName = 'GeoZoneOverTssReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = '6E2DE7AD-2656-4812-BC1C-0EEA32613662'

update report set
	Name = 'reportGroupedMovingHistory',
	GroupName = 'gpsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportWebMoveGroupDay',
	ClassParametersName = 'MoveGroupDayReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'B4C4E567-3CA3-4252-BC9E-E9949FA9C7A7'

update report set
	Name = 'reportGroupMove',
	GroupName = 'gpsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportVehicleGroupMove',
	ClassParametersName = 'VehicleGroupMoveParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = '7CF57572-0F9A-4F82-9136-E191DF144C9A'

update report set
	Name = 'reportPositionRequestsOperator',
	GroupName = 'mlpReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportsLBS',
	ClassParametersName = 'OperatorRequestsReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'B939736A-7B95-4D0B-B59C-6A126D310E70'

update report set
	Name = 'reportPositionRequestsOther',
	GroupName = 'mlpReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportsLBS',
	ClassParametersName = 'OperatorRequestsReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = '873109F4-D52A-40C5-B095-6CE759A17469'

update report set
	Name = 'reportPositionRequestsCommunity',
	GroupName = 'mlpReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportsLBS',
	ClassParametersName = 'OperatorRequestsReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'D2299B07-03BA-4C17-99BB-7C51F0CDEC3B'

update report set
	Name = 'fuelCostCalculation',
	GroupName = 'gpsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.FuelCostCalculation',
	ClassParametersName = 'FuelCostCalculationParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.FuelCostCalculation.Strings'
where report_guid = '5159616A-0D75-4174-BAFD-4F6467A96400'

update report set
	Name = 'itinerary',
	GroupName = 'geoPointReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportGeoPoints',
	ClassParametersName = 'RoutingSheetParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'C0F0D3BE-33CD-4AAA-9043-C7578884982D'

update report set
	Name = 'byDays',
	GroupName = 'geoPointReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportGeoPoints',
	ClassParametersName = 'RoutingSheetSummaryParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'A2E44AB0-8678-4B5E-B80D-91B866F3867A'

update report set
	Name = 'reportSensorsSingle',
	GroupName = 'sensorsReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportSensors',
	ClassParametersName = 'ReportSensorsParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.ReportSensorsResources'
where report_guid = '78288D6E-6AD2-40B5-BE4C-ABDCDC6579A9'

update report set
	Name = 'byTwoGeozone',
	GroupName = 'geoPointReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportGeoPoints',
	ClassParametersName = 'ZoneLeaveEnterParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'C1387D78-34EF-4F91-9709-2FC8515A3E11'

update report set
	Name = 'reportTrafficIntervals',
	GroupName = 'commonReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportWebGisMoveHistory',
	ClassParametersName = 'MoveIntervalReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'E7E8996D-80C0-4F31-8E1E-E8859019983B'

update report set
	Name = 'accessControl',
	GroupName = 'geoPointReports',
	AssemblyName = 'FORIS.TSS.TransportDispatcher.ReportGeoPoints',
	ClassParametersName = 'AccessControlReportParameters',
	ClassResourcesName = 'FORIS.TSS.TransportDispatcher.Reports.Strings'
where report_guid = 'F4ED91AC-115F-4E46-9931-1206DBAB05D6'
