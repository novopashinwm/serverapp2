update cs
	set Descript = case Number 
						when 0 then 'Voltage1' 
						when 1 then 'Voltage2' 
						when 2 then 'Voltage3' 
						when 3 then 'Voltage4' 
						when 4 then 'Spare1' 
					end
from controller_type ct
join controller_sensor cs on cs.controller_type_id = ct.controller_type_id
where ct.type_name = 'mj'
and number between 0 and 4

