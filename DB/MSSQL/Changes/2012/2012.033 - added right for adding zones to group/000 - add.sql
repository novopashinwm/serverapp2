insert into [right] (right_id, [system], name, [description])
select 113, 1, 'AddZoneToGroup', 'Allows to add zones to selected zonegroup.'
where not exists (select * from [right] where right_id = 113)
