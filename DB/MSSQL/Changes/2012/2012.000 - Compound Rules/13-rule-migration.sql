/* �������� ������ �� ����� ����� �� SOAP-XML � CompoundRuleXXX */

if not exists (select * from sys.columns where name = 'XML_Condition' and object_id = object_id('dbo.H_Vehicle_Rule'))
	alter table dbo.H_Vehicle_Rule
		add XML_Condition nvarchar(max)
go
if not exists (select * from sys.columns where name = 'XML_Action' and object_id = object_id('dbo.H_Vehicle_Rule'))
	alter table dbo.H_Vehicle_Rule
		add XML_Action nvarchar(max)
go
if not exists (select * from sys.columns where name = 'Enabled' and object_id = object_id('dbo.H_Vehicle_Rule'))
	alter table dbo.H_Vehicle_Rule
		add [Enabled] bit
go

declare @trail_id int

insert into dbo.Trail(Trail_Time)
	select getutcdate()
	
set @trail_id = @@identity

while (exists (select * from vehicle_rule where xml_action is not null)
	or exists (select * from vehiclegroup_rule where xml_action is not null))
begin
	declare @vehicle_rule_id int
	declare @enabled bit
	declare @condition_xml xml
	declare @action_xml xml
	declare @vehicle_id int
	
	if exists (select 1 from vehicle_rule where xml_action is not null)
		select top(1) 
			  @vehicle_rule_id = vehicle_rule_id 
			, @enabled = [enabled]
			, @condition_xml = XML_Condition
			, @action_xml = XML_Action
			, @vehicle_id = vehicle_id
			from vehicle_rule 
			where xml_action is not null 
			order by vehicle_rule_id
	else 
		select top(1) 
			  @vehicle_rule_id = vehiclegroup_rule_id 
			, @enabled = [enabled]
			, @condition_xml = XML_Condition
			, @action_xml = XML_Action
			, @vehicle_id = vehiclegroup_id
			from vehiclegroup_rule 
			where xml_action is not null 
			order by vehiclegroup_rule_id
		
	insert into dbo.CompoundRule(Active) values (@enabled)

	declare @compound_rule_id int
	set @compound_rule_id = @@identity
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @condition_xml, '<root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:clr="http://schemas.microsoft.com/soap/encoding/clr/1.0" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:a1="http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.DTO.Rules/FORIS.TSS.Interfaces" />'
	
	insert into dbo.CompoundRule_Sensor (CompoundRule_ID, Controller_Sensor_Legend_ID, Value, Comparison_Type)
		select @compound_rule_id, legend.Controller_Sensor_Legend_ID, 1, 1
			from dbo.Controller_Sensor_Legend legend
			where legend.Name = '�������'
			  and exists (select 1 from openxml(@idoc, '/SOAP-ENV:Envelope/SOAP-ENV:Body/a1:DigitalSensorCondition', 2))
			  
	declare @href varchar(255)
	declare @zoneConditionType int
	set @href = (select ZoneIdsHRef 
					from openxml(@idoc, '/SOAP-ENV:Envelope/SOAP-ENV:Body/a1:ZoneCondition', 2) 
						 with (ZoneIdsHRef			varchar(255)	'./ZoneIds/@href'))
	set @zoneConditionType = 
		case (select ConditionType
				from openxml(@idoc, '/SOAP-ENV:Envelope/SOAP-ENV:Body/a1:ZoneCondition', 2) 
					 with (ConditionType		varchar(255)	'./ConditionType'))
			when 'Incoming' then 0
			when 'Outgoing' then 1
			when 'InZone'   then 2
			when 'OutOfZone'then 3
			else null
		end

	declare @xpath varchar(255)
	set @xpath = '/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENC:Array[@id="' + substring(@href, 2, len(@href)-1) + '"]/item'

	if (@href is not null and @zoneConditionType is not null)
		insert into dbo.CompoundRule_Zone (CompoundRule_ID, Zone_ID, Type)
			select distinct @compound_rule_id, z.Zone_ID, @zoneConditionType
				from openxml(@idoc, @xpath, 2) with (ZoneId int '.') x
				join dbo.Geo_Zone z on z.Zone_ID = x.ZoneId

	set @href = (select ZoneGroupIdsHRef
					from openxml(@idoc, '/SOAP-ENV:Envelope/SOAP-ENV:Body/a1:ZoneCondition', 2) 
						 with (ZoneGroupIdsHRef	varchar(255)	'./ZoneGroupIds/@href'))

	if (@href is not null and @zoneConditionType is not null)
		insert into dbo.CompoundRule_ZoneGroup (CompoundRule_ID, ZoneGroup_ID, Type)
			select distinct @compound_rule_id, zg.ZoneGroup_ID, @zoneConditionType
				from openxml(@idoc, @xpath, 2) with (ZoneGroupId int '.') x
				join dbo.ZoneGroup zg on zg.ZoneGroup_ID = x.ZoneGroupId
	
	insert into dbo.CompoundRule_Message_Template (CompoundRule_ID, Message_Template_ID, Operator_ID, Sms, Email)
		select @compound_rule_id, mt.Message_Template_ID, o.Operator_ID, 0, 1
			from openxml(@idoc, '/SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENC:Array[@id=substring(/SOAP-ENV:Envelope/SOAP-ENV:Body/a1:SendEmailAction[ActionType="SendEmail"]/OperatorIDs/@href, 2)]/item', 2) 
				 with (Operator_ID int '.') x
			join dbo.Message_Template mt on mt.Name = 'GenericNotification'
			join dbo.Operator o on o.Operator_ID = x.Operator_ID

	if exists (select 1 from vehicle_rule where xml_action is not null)
		insert into dbo.CompoundRule_Vehicle (CompoundRule_ID, Vehicle_ID)
			select @compound_rule_id, @vehicle_id
	else
		insert into dbo.CompoundRule_VehicleGroup (CompoundRule_ID, VehicleGroup_ID)
			select @compound_rule_id, @vehicle_id
	
	if exists (select 1 from vehicle_rule where xml_action is not null)
	begin
		insert into dbo.H_Vehicle_Rule 
		(
			 VEHICLE_RULE_ID
			,VEHICLE_ID
			,RULE_ID
			,VALUE
			,TRAIL_ID
			,ACTION
			,ACTUAL_TIME
			,XML_Condition
			,XML_Action
			,Enabled
		)
			select 
				 VEHICLE_RULE_ID
				,VEHICLE_ID
				,RULE_ID
				,VALUE
				,TRAIL_ID = @trail_id
				,ACTION = 'DELETE'
				,ACTUAL_TIME = getutcdate()
				,XML_Condition
				,XML_Action
				,Enabled
			from dbo.Vehicle_Rule where vehicle_rule_id = @vehicle_rule_id
			
		delete from vehicle_rule where vehicle_rule_id = @vehicle_rule_id
	end
	else
	begin
		insert into dbo.H_VehicleGroup_Rule
		(
			 VEHICLEGROUP_RULE_ID
			,VEHICLEGROUP_ID
			,RULE_ID
			,VALUE
			,TRAIL_ID
			,ACTION
			,ACTUAL_TIME
			,XML_Condition
			,XML_Action
			,Enabled
		)
		select 
			 VEHICLEGROUP_RULE_ID
			,VEHICLEGROUP_ID
			,RULE_ID
			,VALUE
			,TRAIL_ID = @trail_id
			,ACTION = 'DELETE'
			,ACTUAL_TIME = getutcdate()
			,XML_Condition
			,XML_Action
			,Enabled
		from dbo.VehicleGroup_Rule where vehiclegroup_rule_id = @vehicle_rule_id
		delete from vehiclegroup_rule where vehiclegroup_rule_id = @vehicle_rule_id
	end
		
end