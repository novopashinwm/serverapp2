if Exists(select * from sys.columns where Name = N'CourseDegrees'  
    and Object_ID = Object_ID(N'GPS_Log'))
    begin
    ALTER TABLE GPS_Log
		DROP COLUMN CourseDegrees
    ALTER TABLE GPS_Log
		ADD CourseDegrees AS (CONVERT(int, Course/256.0*360,0))
    end
