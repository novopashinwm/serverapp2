if not exists (select * from sys.objects where object_id = object_id('dbo.Host'))
begin

	create table dbo.[Host]
	(
		[Host_ID] int identity constraint PK_Host primary key clustered,
		Name varchar(64) constraint UQ_Host_Name unique
	);

end;
go

if not exists (select * from sys.columns where object_id = object_id('dbo.Vehicle_Rule_Processing') and name = 'Host_ID')
begin

	alter table dbo.Vehicle_Rule_Processing
		add [Host_ID] int constraint FK_Vehicle_Rule_Processing_Host_ID foreign key references dbo.[Host]([Host_ID])

end
go
