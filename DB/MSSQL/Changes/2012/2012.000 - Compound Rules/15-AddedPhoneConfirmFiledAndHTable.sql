if not exists(select * from sys.columns where Name = N'Confirmed'  
            and object_id = object_id(N'Phone'))
begin

alter table [dbo].[Phone]
    add [Confirmed] bit null
end

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_H_Phone_Trail_ID]') and parent_object_id = object_id(N'[dbo].[H_Phone]'))
alter table [dbo].[H_Phone] drop constraint [FK_H_Phone_Trail_ID]
go

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[H_Phone]') and type in (N'U'))
drop table [dbo].[H_Phone]
go

set ansi_nulls on
go

set quoted_identifier on
go

create table [dbo].[H_Phone](
	[Phone_ID] [int] not null,
	[Phone] [nvarchar](300) not null,
	[Name] [nvarchar](300) null,
	[Comment] [nvarchar](512) null,
	[TRAIL_ID] [int] not null,
	[Confirmed] bit null,
	[ACTION] [nvarchar](6) not null,
	[ACTUAL_TIME] [datetime] not null,
	[ID] [int] identity(1,1) not null,
	[Operator_ID] [int] null,
 constraint [PK_H_Phone] primary key clustered 
(
	[ID] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

go

alter table [dbo].[H_Phone]  with check add  constraint [FK_H_Phone_Trail_ID] foreign key([TRAIL_ID])
references [dbo].[TRAIL] ([TRAIL_ID])
go

alter table [dbo].[H_Phone] check constraint [FK_H_Phone_Trail_ID]
go
