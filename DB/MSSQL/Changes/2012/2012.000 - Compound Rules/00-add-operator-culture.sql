if not exists (select * from sys.columns where object_id = object_id('dbo.Culture'))
begin
	create table dbo.Culture
	(
		Culture_ID int identity (1,1)
			constraint PK_Culture primary key clustered,
		Code varchar(11) --http://stackoverflow.com/questions/4862765/max-length-of-the-cultureinfo-name-property
			constraint UQ_Culture_Code unique 
	)
end;

if not exists (select * from sys.columns where object_id = object_id('dbo.Operator') and name = 'Culture_ID')
begin

	alter table dbo.Operator
		add Culture_ID int constraint FK_Operator_Culture_ID foreign key references dbo.Culture(Culture_ID)

end;

if not exists (select * from sys.columns where object_id = object_id('dbo.H_Operator') and name = 'Culture_ID')
begin

	alter table dbo.H_Operator
		add Culture_ID int;

end
