/* ��������� ����� ������ ��� ��������� ��������� ���������� � ����������/��������� ������ */

/* ��������� �������.
	�������� ��� ����������� ������� � �������� ��� ����������/��������� �������
 */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule'))
begin

	create table dbo.CompoundRule
	(
		CompoundRule_ID int identity constraint PK_CompoundRule primary key clustered,
		/*��������/��������� � ��������� �����*/
		Active bit not null,
		/*�������� ������� (�����������) */
		[Description] nvarchar(255)
	)
	
	create table dbo.H_CompoundRule
	(
		H_CompoundRule_ID int identity constraint PK_H_CompoundRule primary key clustered,
		CompoundRule_ID int not null,
		Active bit not null,
		[Description] nvarchar(255),
		TRAIL_ID					int constraint FK_H_CompoundRule_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)

end;
GO

/* ������ CompoundRule *<->* Vehicle.
	������� CompoundRule ����������� ��� Vehicle
 */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Vehicle'))
begin
	
	create table dbo.CompoundRule_Vehicle
	(
		CompoundRule_ID int not null
			constraint FK_CompoundRule_Vehicle_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		Vehicle_ID int not null
			constraint FK_CompoundRule_Vehicle_Vehicle
				foreign key references dbo.Vehicle(Vehicle_ID),
		constraint PK_CompoundRule_Vehicle
			primary key clustered (CompoundRule_ID, Vehicle_ID)
	)
	
	create table dbo.H_CompoundRule_Vehicle
	(
		H_CompoundRule_Vehicle_ID int identity
			constraint PK_H_CompoundRule_Vehicle primary key clustered,
		CompoundRule_ID int not null,
		Vehicle_ID int not null,
		TRAIL_ID					int constraint FK_H_CompoundRule_Vehicle_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;	
GO

/* ������ CompoundRule *<->* VehicleGroup.
	������� CompoundRule ����������� ��� VehicleGroup
 */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_VehicleGroup'))
begin
	
	create table dbo.CompoundRule_VehicleGroup
	(
		CompoundRule_ID int not null 
			constraint FK_CompoundRule_VehicleGroup_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		VehicleGroup_ID int not null
			constraint FK_CompoundRule_VehicleGroup_VehicleGroup
				foreign key references dbo.VehicleGroup(VehicleGroup_ID),
		constraint PK_CompoundRule_VehicleGroup
			primary key clustered (CompoundRule_ID, VehicleGroup_ID)				
	)
	
	create table dbo.H_CompoundRule_VehicleGroup
	(
		H_CompoundRule_VehicleGroup_ID int identity
			constraint PK_H_CompoundRule_VehicleGroup primary key clustered,
		CompoundRule_ID int not null,
		VehicleGroup_ID int not null,
		TRAIL_ID					int constraint FK_H_CompoundRule_VehicleGroup_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;
GO

/*	������� ��� ������������ ������� ��� ������ */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Zone'))
begin
	
	create table dbo.CompoundRule_Zone
	(
		CompoundRule_Zone_ID int identity
			constraint PK_CompoundRule_Zone 
				primary key clustered,
		CompoundRule_ID int not null 
			constraint FK_CompoundRule_Zone_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		Zone_ID int not null
			constraint FK_CompoundRule_Zone_Zone
				foreign key references dbo.Geo_Zone(Zone_ID),
		--��� �������� ���� (����/�����, � ����/��� ����)
		[Type] int not null		
	)
	
	create table dbo.H_CompoundRule_Zone
	(
		H_CompoundRule_Zone_ID int identity
			constraint PK_H_CompoundRule_Zone primary key clustered,
			
		CompoundRule_Zone_ID int not null,
		CompoundRule_ID int not null,
		Zone_ID int not null,
		[Type] int not null,
		
		TRAIL_ID					int constraint FK_H_CompoundRule_Zone_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;	
GO

/*	������� ��� ������������ ������� ��� ����� ������ */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_ZoneGroup'))
begin
	
	create table dbo.CompoundRule_ZoneGroup
	(
		CompoundRule_ZoneGroup_ID int identity
			constraint PK_CompoundRule_ZoneGroup
				primary key clustered,
		CompoundRule_ID int not null 
			constraint FK_CompoundRule_ZoneGroup_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		ZoneGroup_ID int not null
			constraint FK_CompoundRule_ZoneGroup_ZoneGroup
				foreign key references dbo.ZoneGroup(ZoneGroup_ID),
		--��� �������� ���� (����/�����, � ����/��� ����)
		[Type] int not null		
	)
	
	create table dbo.H_CompoundRule_ZoneGroup
	(
		H_CompoundRule_ZoneGroup_ID int identity
			constraint PK_H_CompoundRule_ZoneGroup primary key clustered,
		CompoundRule_ZoneGroup_ID int not null,
		CompoundRule_ID int not null,
		ZoneGroup_ID int not null,
		[Type] int not null,
		TRAIL_ID					int constraint FK_H_CompoundRule_ZoneGroup_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;
go

/*	������� ��� ������������ ������� ��� �������� �������� */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Sensor'))
begin
	
	create table dbo.CompoundRule_Sensor
	(
		CompoundRule_Sensor_ID int identity
			constraint PK_CompoundRule_Sensor
				primary key clustered,
		CompoundRule_ID int not null 
			constraint FK_CompoundRule_Sensor_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		Controller_Sensor_Legend_ID int not null
			constraint FK_CompoundRule_Sensor_Controller_Sensor_Legend
				foreign key references dbo.Controller_Sensor_Legend(Controller_Sensor_Legend_ID),
		Value numeric(18,9) not null,
		Comparison_Type int not null
	)
	
	create table dbo.H_CompoundRule_Sensor
	(
		H_CompoundRule_Sensor_ID int identity
			constraint PK_H_CompoundRule_Sensor primary key clustered,

		CompoundRule_Sensor_ID int not null,
		CompoundRule_ID int not null,
		Controller_Sensor_Legend_ID int not null,
		Value numeric(18,9) not null,
		Comparison_Type int not null,

		TRAIL_ID					int constraint FK_H_CompoundRule_Sensor_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;	
GO

/*	������� ��� ������������ ������� ��� �������� �������� */
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Speed'))
begin
	
	create table dbo.CompoundRule_Speed
	(
		CompoundRule_Speed_ID int identity
			constraint PK_CompoundRule_Speed
				primary key clustered,
		CompoundRule_ID int not null 
			constraint FK_CompoundRule_Speed_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		--��� ��������: ������/������
		[Type] int not null,
		Value int not null
	)
	
	create table dbo.H_CompoundRule_Speed
	(
		H_CompoundRule_Speed_ID int identity
			constraint PK_H_CompoundRule_Speed primary key clustered,

		CompoundRule_Speed_ID int not null,
		CompoundRule_ID int not null,
		[Type] int not null,
		Value int not null,

		TRAIL_ID					int constraint FK_H_CompoundRule_Speed_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end;	
GO

/*���������� ��������� ��� ��� ����������� ������*/
if not exists (select * from sys.objects where object_id = object_id('dbo.TimeZone'))
begin
	
	create table dbo.TimeZone
	(
		TimeZone_ID int identity
			constraint PK_TimeZone
				primary key clustered,
		Code varchar(32) not null 
			constraint UQ_TimeZone_Code
				unique
	)
	
	insert into dbo.TimeZone(Code) values ('Dateline Standard Time')
	insert into dbo.TimeZone(Code) values ('UTC-11')
	insert into dbo.TimeZone(Code) values ('Samoa Standard Time')
	insert into dbo.TimeZone(Code) values ('Hawaiian Standard Time')
	insert into dbo.TimeZone(Code) values ('Alaskan Standard Time')
	insert into dbo.TimeZone(Code) values ('Pacific Standard Time (Mexico)')
	insert into dbo.TimeZone(Code) values ('Pacific Standard Time')
	insert into dbo.TimeZone(Code) values ('US Mountain Standard Time')
	insert into dbo.TimeZone(Code) values ('Mountain Standard Time')
	insert into dbo.TimeZone(Code) values ('Mountain Standard Time (Mexico)')
	insert into dbo.TimeZone(Code) values ('Mexico Standard Time 2')
	insert into dbo.TimeZone(Code) values ('Central Standard Time (Mexico)')
	insert into dbo.TimeZone(Code) values ('Mexico Standard Time')
	insert into dbo.TimeZone(Code) values ('Canada Central Standard Time')
	insert into dbo.TimeZone(Code) values ('Central America Standard Time')
	insert into dbo.TimeZone(Code) values ('Central Standard Time')
	insert into dbo.TimeZone(Code) values ('SA Pacific Standard Time')
	insert into dbo.TimeZone(Code) values ('Eastern Standard Time')
	insert into dbo.TimeZone(Code) values ('US Eastern Standard Time')
	insert into dbo.TimeZone(Code) values ('Venezuela Standard Time')
	insert into dbo.TimeZone(Code) values ('Paraguay Standard Time')
	insert into dbo.TimeZone(Code) values ('Atlantic Standard Time')
	insert into dbo.TimeZone(Code) values ('SA Western Standard Time')
	insert into dbo.TimeZone(Code) values ('Central Brazilian Standard Time')
	insert into dbo.TimeZone(Code) values ('Pacific SA Standard Time')
	insert into dbo.TimeZone(Code) values ('Newfoundland Standard Time')
	insert into dbo.TimeZone(Code) values ('E. South America Standard Time')
	insert into dbo.TimeZone(Code) values ('Argentina Standard Time')
	insert into dbo.TimeZone(Code) values ('Greenland Standard Time')
	insert into dbo.TimeZone(Code) values ('SA Eastern Standard Time')
	insert into dbo.TimeZone(Code) values ('Montevideo Standard Time')
	insert into dbo.TimeZone(Code) values ('UTC-02')
	insert into dbo.TimeZone(Code) values ('Mid-Atlantic Standard Time')
	insert into dbo.TimeZone(Code) values ('Azores Standard Time')
	insert into dbo.TimeZone(Code) values ('Cape Verde Standard Time')
	insert into dbo.TimeZone(Code) values ('UTC')
	insert into dbo.TimeZone(Code) values ('GMT Standard Time')
	insert into dbo.TimeZone(Code) values ('Morocco Standard Time')
	insert into dbo.TimeZone(Code) values ('Greenwich Standard Time')
	insert into dbo.TimeZone(Code) values ('W. Europe Standard Time')
	insert into dbo.TimeZone(Code) values ('Central Europe Standard Time')
	insert into dbo.TimeZone(Code) values ('Romance Standard Time')
	insert into dbo.TimeZone(Code) values ('Central European Standard Time')
	insert into dbo.TimeZone(Code) values ('Namibia Standard Time')
	insert into dbo.TimeZone(Code) values ('W. Central Africa Standard Time')
	insert into dbo.TimeZone(Code) values ('Jordan Standard Time')
	insert into dbo.TimeZone(Code) values ('GTB Standard Time')
	insert into dbo.TimeZone(Code) values ('Middle East Standard Time')
	insert into dbo.TimeZone(Code) values ('FLE Standard Time')
	insert into dbo.TimeZone(Code) values ('Syria Standard Time')
	insert into dbo.TimeZone(Code) values ('Israel Standard Time')
	insert into dbo.TimeZone(Code) values ('Egypt Standard Time')
	insert into dbo.TimeZone(Code) values ('E. Europe Standard Time')
	insert into dbo.TimeZone(Code) values ('South Africa Standard Time')
	insert into dbo.TimeZone(Code) values ('Arabic Standard Time')
	insert into dbo.TimeZone(Code) values ('Arab Standard Time')
	insert into dbo.TimeZone(Code) values ('Russian Standard Time')
	insert into dbo.TimeZone(Code) values ('E. Africa Standard Time')
	insert into dbo.TimeZone(Code) values ('Iran Standard Time')
	insert into dbo.TimeZone(Code) values ('Arabian Standard Time')
	insert into dbo.TimeZone(Code) values ('Azerbaijan Standard Time')
	insert into dbo.TimeZone(Code) values ('Armenian Standard Time')
	insert into dbo.TimeZone(Code) values ('Caucasus Standard Time')
	insert into dbo.TimeZone(Code) values ('Mauritius Standard Time')
	insert into dbo.TimeZone(Code) values ('Georgian Standard Time')
	insert into dbo.TimeZone(Code) values ('Afghanistan Standard Time')
	insert into dbo.TimeZone(Code) values ('Ekaterinburg Standard Time')
	insert into dbo.TimeZone(Code) values ('Pakistan Standard Time')
	insert into dbo.TimeZone(Code) values ('West Asia Standard Time')
	insert into dbo.TimeZone(Code) values ('India Standard Time')
	insert into dbo.TimeZone(Code) values ('Sri Lanka Standard Time')
	insert into dbo.TimeZone(Code) values ('Nepal Standard Time')
	insert into dbo.TimeZone(Code) values ('Central Asia Standard Time')
	insert into dbo.TimeZone(Code) values ('Bangladesh Standard Time')
	insert into dbo.TimeZone(Code) values ('N. Central Asia Standard Time')
	insert into dbo.TimeZone(Code) values ('Myanmar Standard Time')
	insert into dbo.TimeZone(Code) values ('SE Asia Standard Time')
	insert into dbo.TimeZone(Code) values ('North Asia Standard Time')
	insert into dbo.TimeZone(Code) values ('China Standard Time')
	insert into dbo.TimeZone(Code) values ('North Asia East Standard Time')
	insert into dbo.TimeZone(Code) values ('Singapore Standard Time')
	insert into dbo.TimeZone(Code) values ('W. Australia Standard Time')
	insert into dbo.TimeZone(Code) values ('Taipei Standard Time')
	insert into dbo.TimeZone(Code) values ('Ulaanbaatar Standard Time')
	insert into dbo.TimeZone(Code) values ('Tokyo Standard Time')
	insert into dbo.TimeZone(Code) values ('Korea Standard Time')
	insert into dbo.TimeZone(Code) values ('Yakutsk Standard Time')
	insert into dbo.TimeZone(Code) values ('Cen. Australia Standard Time')
	insert into dbo.TimeZone(Code) values ('AUS Central Standard Time')
	insert into dbo.TimeZone(Code) values ('E. Australia Standard Time')
	insert into dbo.TimeZone(Code) values ('Vladivostok Standard Time')
	insert into dbo.TimeZone(Code) values ('West Pacific Standard Time')
	insert into dbo.TimeZone(Code) values ('AUS Eastern Standard Time')
	insert into dbo.TimeZone(Code) values ('Tasmania Standard Time')
	insert into dbo.TimeZone(Code) values ('Magadan Standard Time')
	insert into dbo.TimeZone(Code) values ('Central Pacific Standard Time')
	insert into dbo.TimeZone(Code) values ('UTC+12')
	insert into dbo.TimeZone(Code) values ('New Zealand Standard Time')
	insert into dbo.TimeZone(Code) values ('Kamchatka Standard Time')
	insert into dbo.TimeZone(Code) values ('Fiji Standard Time')
	insert into dbo.TimeZone(Code) values ('Tonga Standard Time')
end

/*����������� ������ �� ������� �����*/
if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Time'))
begin

	create table dbo.CompoundRule_Time
	(
		CompoundRule_Time_ID int identity
			constraint PK_CompoundRule_Time
				primary key clustered,
				
		CompoundRule_ID int not null
			constraint FK_CompoundRule_Time_CompoundRule 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		TimeZone_ID int not null
			constraint FK_CompoundRule_Time_TimeZone
				foreign key references dbo.TimeZone(TimeZone_ID),
		Hours_From tinyint not null,
		Minutes_From tinyint not null,
		Hours_To tinyint not null,
		Minutes_To tinyint not null
	)

	create table dbo.H_CompoundRule_Time
	(
		H_CompoundRule_Time_ID int identity
			constraint PK_H_CompoundRule_Time
				primary key clustered,

		CompoundRule_Time_ID int not null,
		CompoundRule_ID int not null,
		TimeZone_ID int not null,
		Hours_From tinyint not null,
		Minutes_From tinyint not null,
		Hours_To tinyint not null,
		Minutes_To tinyint not null,
		
		TRAIL_ID					int constraint FK_H_CompoundRule_Time_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)

end
go

/*������ �������, ����������� ��������� �������� ������*/
delete from dbo.Vehicle_Rule_Processing
	
if exists (
	select * from sys.objects 
		where name = 'FK_Vehicle_Rule_Processing_Vehicle_Rule_ID' 
		  and type = 'f')
	alter table dbo.Vehicle_Rule_Processing
		drop constraint FK_Vehicle_Rule_Processing_Vehicle_Rule_ID;
go

if exists (
	select * from sys.columns 
		where name = 'Vehicle_Rule_ID' 
		  and object_id = object_id('dbo.Vehicle_Rule_Processing'))
	alter table dbo.Vehicle_Rule_Processing
		drop column Vehicle_Rule_ID;
go

if exists (
	select * from sys.indexes
		where name = 'IX_Vehicle_Rule_Processing_VID_VGRID')
	drop index IX_Vehicle_Rule_Processing_VID_VGRID on dbo.Vehicle_Rule_Processing
go

if exists (
	select * from sys.objects
		where name = 'FK_Vehicle_Rule_Processing_VehicleGroup_Rule_ID'
		  and type = 'F')
	alter table dbo.Vehicle_Rule_Processing
		drop constraint FK_Vehicle_Rule_Processing_VehicleGroup_Rule_ID;
go

if exists (
	select * from sys.columns
		where name = 'VehicleGroup_Rule_ID'
		  and object_id = object_id('Vehicle_Rule_Processing'))
	alter table dbo.Vehicle_Rule_Processing
		drop column VehicleGroup_Rule_ID;
go

if exists(
	select * from sys.objects
		where name = 'PK_Event'
		  and type = 'PK')
	alter table dbo.Vehicle_Rule_Processing
		drop constraint PK_Event
go
	
alter table dbo.Vehicle_Rule_Processing
	alter column Vehicle_ID int not null
go

if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_Message_Template'))
begin
	create table dbo.CompoundRule_Message_Template
	(
		CompoundRule_Message_Template_ID int identity
			constraint PK_CompoundRule_Message_Template primary key clustered,
		CompoundRule_ID int not null
			constraint FK_CompoundRule_Message_Template_CompoundRule_ID 
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		Message_Template_ID int not null
			constraint FK_CompoundRule_Message_Template_Message_Template_ID
				foreign key references dbo.Message_Template(Message_Template_ID),
		Operator_ID int not null
			constraint FK_CompoundRule_Message_Template_Operator_ID
				foreign key references dbo.Operator(Operator_ID),
		Sms bit not null,
		Email bit not null		
	);
	
	create table dbo.H_CompoundRule_Message_Template
	(
		H_CompoundRule_Message_Template_ID int identity
			constraint PK_H_CompoundRule_Message_Template primary key clustered,

		CompoundRule_Message_Template_ID int not null,
		CompoundRule_ID int not null,
		Message_Template_ID int not null,
		Operator_ID int not null,
		Sms bit not null,
		Email bit not null,
		
		TRAIL_ID					int constraint FK_H_CompoundRule_Message_Template_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	);
end
go