if not exists (select * from sys.types where name = 'Vehicle_Zone_Param')
begin
	create type Vehicle_Zone_Param as table
	(
		Vehicle_ID int,
		Zone_ID int,
		primary key clustered (Vehicle_ID, Zone_ID)
	)
end
go