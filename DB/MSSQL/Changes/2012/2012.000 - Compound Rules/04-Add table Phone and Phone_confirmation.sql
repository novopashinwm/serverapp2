if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Phone]') and type in (N'U'))
begin 
	create table [dbo].[Phone](
		[Phone_ID] [int] identity(1,1) not null,
		[Phone] [nvarchar](300) not null,
		[Name] [nvarchar](300) null,
		[Comment] [nvarchar](512) null,
		[Operator_ID] [int] null,
	 constraint [PK_Phone] primary key clustered 
	(
		[Phone_ID] asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary],
	 constraint [UQ_Phone_Phone] unique nonclustered 
	(
		[Phone] asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	) on [primary]


	alter table [dbo].[Phone]  with check add  constraint [FK_Phone_Operator_ID] foreign key([Operator_ID])
	references [dbo].[OPERATOR] ([OPERATOR_ID])

	alter table [dbo].[Phone] check constraint [FK_Phone_Operator_ID]


	create table [dbo].[Phone_Confirmation](
		[ID] [int] identity(1,1) not null,
		[Phone_ID] [int] null,
		[Creation_Date] [datetime] null,
		[key] [varchar](255) null,
	 constraint [PK_Phone_Confirmation] primary key clustered 
	(
		[ID] asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	) on [primary]

	alter table [dbo].[Phone_Confirmation]  with check add  constraint [FK_Phone] foreign key([Phone_ID])
	references [dbo].[Phone] ([Phone_ID])

	alter table [dbo].[Phone_Confirmation] check constraint [FK_Phone]
end