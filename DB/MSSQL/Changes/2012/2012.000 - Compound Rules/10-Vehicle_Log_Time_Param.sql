if exists (select * from sys.types where name = 'Vehicle_Log_Time_Param')
	drop type dbo.Vehicle_Log_Time_Param
go	
create type Vehicle_Log_Time_Param as table
(
	Vehicle_ID int,
	Log_Time int,
	primary key clustered (Vehicle_ID, Log_Time)
)
go