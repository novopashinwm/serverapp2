if not Exists(select * from sys.columns where Name = N'SystemId'  
            and Object_ID = Object_ID(N'MESSAGE'))
BEGIN

ALTER TABLE [dbo].[MESSAGE]
    ADD [SystemId] NVARCHAR (510) NULL
	
END

if not Exists(select * from sys.columns where Name = N'SystemId'  
            and Object_ID = Object_ID(N'H_MESSAGE'))
BEGIN

ALTER TABLE [dbo].[H_MESSAGE]
    ADD [SystemId] NVARCHAR (510) NULL
	
END