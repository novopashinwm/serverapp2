/* ��������� �������������:
1. v_controller_sensor_log (Vehicle_ID, Log_Time, SensorLegend, Value)
2. v_controller_sensor_map (Vehicle_ID, Sensor_Legend, Sensor_Number, Multiplier, Constant)
3. v_controller_sensor_legend (Vehicle_ID, Sensor_Legend, Sensor_Number, count(Controller_Sensor))  */


--Set the options to support indexed views.
SET NUMERIC_ROUNDABORT OFF;
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT,
    QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
if (exists (select 1 from sys.objects where name = 'v_controller_sensor_log' and type='v'))
	drop view v_controller_sensor_log
go
if (exists (select 1 from sys.objects where name = 'v_controller_sensor_map' and type='v'))
	drop view v_controller_sensor_map
go
if (exists (select 1 from sys.objects where name = 'v_controller_sensor_legend' and type='v'))
	drop view v_controller_sensor_legend
go
alter table dbo.Controller_Sensor_Legend
	alter column Number int not null
go
create view v_controller_sensor_map 
WITH SCHEMABINDING
as
	select	Vehicle_ID = c.Vehicle_ID,
			Sensor_Legend = csl.Number,
			Sensor_Number = cs.Number,
			Multiplier = csm.Multiplier,
			Constant = csm.Constant,
			VALUE_EXPIRED = isnull(cs.VALUE_EXPIRED, csl.VALUE_EXPIRED),
			Min_Value = csm.Min_Value,
			Max_Value = csm.Max_Value
		from dbo.Controller c
		join dbo.Controller_Sensor_Map csm on csm.Controller_ID = c.Controller_ID
		join dbo.Controller_Sensor_Legend csl on csl.Controller_Sensor_Legend_ID = csm.Controller_Sensor_Legend_ID
		join dbo.Controller_Sensor cs on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
go
create unique clustered index IX_V_Controller_sensor_map 
	on v_controller_sensor_map (Vehicle_ID, Sensor_Legend, Sensor_Number, Min_Value, Max_Value)
go
create view v_controller_sensor_legend
WITH SCHEMABINDING
as
	select	Vehicle_ID = c.Vehicle_ID,
			Sensor_Legend = csl.Number,
			Sensor_Count = count_big(*)
		from dbo.Controller c
		join dbo.Controller_Sensor_Map csm on csm.Controller_ID = c.Controller_ID
		join dbo.Controller_Sensor_Legend csl on csl.Controller_Sensor_Legend_ID = csm.Controller_Sensor_Legend_ID
		join dbo.Controller_Sensor cs on cs.Controller_Sensor_ID = csm.Controller_Sensor_ID
		group by c.Vehicle_ID, csl.Number
go
create unique clustered index IX_V_Controller_sensor_legend 
	on v_controller_sensor_legend (Vehicle_ID, Sensor_Legend)
go

create view v_controller_sensor_log as
	select distinct
		Vehicle_ID = csl_outer.Vehicle_ID,
		Log_Time = csl_outer.Log_Time,
		SensorLegend = csm_outer.Sensor_Legend,
		Value = (select 
			sum (csm.Multiplier * csl.Value + csm.Constant)
			from dbo.v_controller_sensor_map csm (noexpand)
			join dbo.Controller_Sensor_Log csl (nolock) on 
				  csl.Vehicle_ID = csl_outer.Vehicle_ID
			  and csl.Log_Time   = csl_outer.Log_Time
			  and csl.Number = csm.Sensor_Number
			  and (csm.Min_Value is null or csm.Min_Value <= csl.Value)
			  and (csm.Max_Value is null or csm.Max_Value >= csl.Value)
			where csm.Vehicle_ID = csl_outer.Vehicle_ID
			  and csm.Sensor_Legend = csm_outer.Sensor_Legend)
		from dbo.Controller_Sensor_Log csl_outer (nolock)
		join dbo.v_controller_sensor_map csm_outer (noexpand) on csm_outer.Vehicle_ID = csl_outer.Vehicle_ID
															 and csm_outer.Sensor_Number = csl_outer.Number
		join dbo.v_controller_sensor_legend legend (noexpand) on legend.Vehicle_ID    = csm_outer.Vehicle_ID
												  and legend.Sensor_Legend = csm_outer.Sensor_Legend
--TODO: ������, ��� ��������� ������ ��������, ����� �������� ��������� ��������, �� �������� �������� �� �� ����
--		where 
--			--���������� ���������� ����� �� ��������
--			(select 
--				count(1)
--				from dbo.v_controller_sensor_map csm (noexpand)
--				join dbo.Controller_Sensor_Log csl (nolock) on 
--					  csl.Vehicle_ID = csl_outer.Vehicle_ID
--				  and csl.Log_Time   = csl_outer.Log_Time
--				  and csl.Number = csm.Sensor_Number
--				where csm.Vehicle_ID = csl_outer.Vehicle_ID
--				  and csm.Sensor_Legend = csm_outer.Sensor_Legend) 
--			= 
--			legend.Sensor_Count --��������� ���������� ����� �� ��������
			
				
