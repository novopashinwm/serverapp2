if exists (select * from sys.types where name = 'Vehicle_Sensor_Param')
	drop type dbo.Vehicle_Sensor_Param
go	
create type Vehicle_Sensor_Param as table
(
	Vehicle_ID int,
	Sensor_Number int,
	primary key clustered (Vehicle_ID, Sensor_Number)
)
go