alter table dbo.H_FACTOR_VALUES
	alter column Factor_ID int null
go

alter table dbo.H_JOURNAL
	alter column [Status] int not null
go

alter table dbo.H_MESSAGE_FIELD
	alter column MESSAGE_TEMPLATE_FIELD_ID int null
go

alter table H_ROUTE
	alter column [LENGTH] real null
go

alter table H_ROUTE
	alter column DIGIT_COUNT int null
go

alter table H_ROUTE_POINT
	alter column ORD int not null
go

alter table H_RS_NUMBER
	alter column RS_STEP int null
go

alter table H_SCHEDULE_DETAIL
	alter column SHIFT_ID int not null
go

alter table H_TRIP
	alter column TRIP int not null
go

if not exists (select * from sys.columns where name = 'Fuel_Type_ID' and object_id = object_id('H_Vehicle'))
	alter table H_Vehicle
		add Fuel_Type_ID int null
go

alter table H_ZONE_DISTANCE_STANDART
	alter column ZONE_FROM int null
go

alter table H_ZONE_DISTANCE_STANDART
	alter column ZONE_TO int null
	
if not exists (select * from sys.columns where name = 'VALUE_EXPIRED' and object_id = object_id('H_CONTROLLER_SENSOR'))
	alter table H_CONTROLLER_SENSOR
		add VALUE_EXPIRED int null
go

if not exists (select * from sys.columns where name = 'VALUE_EXPIRED' and object_id = object_id('H_CONTROLLER_SENSOR_LEGEND'))
	alter table H_CONTROLLER_SENSOR_LEGEND
		add VALUE_EXPIRED int not null
go

alter table H_DAY_KIND
	alter column WEEKDAYS int not null 
go

