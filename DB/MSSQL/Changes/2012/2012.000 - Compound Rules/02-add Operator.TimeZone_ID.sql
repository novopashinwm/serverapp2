if not exists (select * from sys.columns where name = 'TimeZone_ID' and object_id = object_id('dbo.OPERATOR'))
begin

	alter table dbo.Operator
		add TimeZone_ID int constraint FK_Operator_TimeZone foreign key references dbo.TimeZone(TimeZone_ID)
		
	alter table dbo.H_Operator
		add TimeZone_ID int 

end