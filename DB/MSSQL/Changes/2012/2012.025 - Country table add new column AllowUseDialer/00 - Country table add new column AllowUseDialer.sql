  
  IF NOT Exists(select * from sys.columns where Name = N'AllowUseDialer'  
            and Object_ID = Object_ID(N'Country'))
  BEGIN
  
  ALTER TABLE dbo.Country ADD
  AllowUseDialer BIT NOT NULL DEFAULT 0
  
  EXEC ('UPDATE dbo.Country SET AllowUseDialer = 1 WHERE Name = ''Russia''')
  
  END
