  
alter view v_controller_sensor_log as  
 select
  Vehicle_ID   = legend.Vehicle_ID,  
  SensorLegend = legend.Sensor_Legend,  
  Log_Time     = csl_outer.Log_Time,  
  Value = (select   
   sum (csm.Multiplier * csl.Value + csm.Constant)  
   from dbo.v_controller_sensor_map csm (noexpand)  
   join dbo.Controller_Sensor_Log csl with (nolock) on   
      csl.Vehicle_ID = csl_outer.Vehicle_ID  
     and csl.Log_Time   = csl_outer.Log_Time  
     and csl.Number = csm.Sensor_Number  
     and (csm.Min_Value is null or csm.Min_Value <= csl.Value)  
     and (csm.Max_Value is null or csm.Max_Value >= csl.Value)  
   where csm.Vehicle_ID = csl_outer.Vehicle_ID  
     and csm.Sensor_Legend = legend.Sensor_Legend)  
  from dbo.v_controller_sensor_legend legend (noexpand)
  join dbo.Controller_Sensor_Log csl_outer with (nolock)
	   on csl_outer.Vehicle_ID = legend.Vehicle_ID
      and csl_outer.Number in (
			select csm.Sensor_Number 
				from dbo.v_controller_sensor_map csm with (noexpand)
				where csm.Vehicle_ID = legend.Vehicle_ID
				  and csm.Sensor_Legend = legend.Sensor_Legend)
  