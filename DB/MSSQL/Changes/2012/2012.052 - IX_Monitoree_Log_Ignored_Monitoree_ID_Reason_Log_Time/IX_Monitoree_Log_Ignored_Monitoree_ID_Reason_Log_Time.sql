if not exists (select * from sys.indexes where name = 'IX_Monitoree_Log_Ignored_Monitoree_ID_Reason_Log_Time') 
begin

	create nonclustered index IX_Monitoree_Log_Ignored_Monitoree_ID_Reason_Log_Time
		on Monitoree_Log_Ignored (Monitoree_ID, Reason, Log_Time)
			
end
go
sp_recompile 'AddMonitoreeLog' 