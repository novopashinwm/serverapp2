--��������� ������ Sales ����� �� �������������� ���������� ����������

if exists (select * from sys.objects where name like 'CK_OPERATORGROUP_VEHICLEGROUP_RIGHT')
begin
	alter table dbo.OperatorGroup_VehicleGroup
		drop constraint CK_OPERATORGROUP_VEHICLEGROUP_RIGHT
end
go

insert into dbo.OperatorGroup_VehicleGroup
	select ogvg.operatorgroup_id, ogvg.vehiclegroup_id, 1, r.right_id
		from OperatorGroup_VehicleGroup ogvg
		join operatorgroup og on og.operatorgroup_id = ogvg.operatorgroup_id
		join [right] r on r.name in (
			'ViewControllerPhone', 
			'ViewControllerDeviceID',
			'EditControllerPhone', 
			'EditControllerDeviceId', 
			'ControllerPasswordAccess' )
		where og.Name = 'Sales' and ogvg.right_id = 102
		and not exists (
			select 1 
				from operatorgroup_vehiclegroup e 
				where e.operatorgroup_id = ogvg.operatorgroup_id and 
					  e.vehiclegroup_id = ogvg.vehiclegroup_id and 
					  e.right_id = r.right_id)