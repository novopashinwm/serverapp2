if not exists (select * from sys.columns where object_id = object_id('message') and name = 'ErrorCount')
begin

	alter table dbo.Message
		add ErrorCount int not null default 0

	alter table dbo.H_Message
		add ErrorCount int not null default 0		
		
end