if (not exists (select 1 from sys.tables where name = 'CompoundRule_SubscriptionEmail' and type = 'U'))
begin
create table CompoundRule_SubscriptionEmail (
	CompoundRule_ID int not null,
	Email_ID int not null
	constraint [PK_CompoundRule_SubscriptionEmail] primary key clustered (
		CompoundRule_ID asc,
		Email_ID asc
	)
)

alter table CompoundRule_SubscriptionEmail with check add constraint FK_CompoundRule_SubscriptionEmail_CompundRule
foreign key (CompoundRule_ID)
references CompoundRule(CompoundRule_ID)

alter table CompoundRule_SubscriptionEmail with check add constraint FK_CompoundRule_SubscriptionEmail_Email
foreign key (Email_ID)
references EMAIL(Email_Id)

end


if (not exists (select 1 from sys.tables where name = 'H_CompoundRule_SubscriptionEmail' and type = 'U'))
begin
create table H_CompoundRule_SubscriptionEmail (
	H_CompoundRule_SubscriptionEmail_ID int not null identity (1, 1),
	CompoundRule_ID int not null,
	Email_ID int not null,
	TRAIL_ID int null,
	[ACTION] nvarchar(6) null,
	ACTUAL_TIME datetime not null
	constraint [PK_H_CompoundRule_SubscriptionEmail] primary key clustered (
		H_CompoundRule_SubscriptionEmail_ID asc
	)
)

alter table H_CompoundRule_SubscriptionEmail with check add constraint FK_H_CompoundRule_SubscriptionEmail_Trail
foreign key (TRAIL_ID)
references Trail(TRAIL_ID)

end


if (not exists (select 1 from sys.tables where name = 'CompoundRule_SubscriptionPhone' and type = 'U'))
begin
create table CompoundRule_SubscriptionPhone (
	CompoundRule_ID int not null,
	Phone_ID int not null
	constraint [PK_CompoundRule_SubscriptionPhone] primary key clustered (
		CompoundRule_ID asc,
		Phone_ID asc
	)
)

alter table CompoundRule_SubscriptionPhone with check add constraint FK_CompoundRule_SubscriptionPhone_CompundRule
foreign key (CompoundRule_ID)
references CompoundRule(CompoundRule_ID)

alter table CompoundRule_SubscriptionPhone with check add constraint FK_CompoundRule_SubscriptionPhone_Phone
foreign key (Phone_ID)
references Phone(Phone_Id)

end


if (not exists (select 1 from sys.tables where name = 'H_CompoundRule_SubscriptionPhone' and type = 'U'))
begin
create table H_CompoundRule_SubscriptionPhone (
	H_CompoundRule_SubscriptionPhone_ID int not null identity (1, 1),
	CompoundRule_ID int not null,
	Phone_ID int not null,
	TRAIL_ID int null,
	[ACTION] nvarchar(6) null,
	ACTUAL_TIME datetime not null
	constraint [PK_H_CompoundRule_SubscriptionPhone] primary key clustered (
		H_CompoundRule_SubscriptionPhone_ID asc
	)
)

alter table H_CompoundRule_SubscriptionPhone with check add constraint FK_H_CompoundRule_SubscriptionPhone_Trail
foreign key (TRAIL_ID)
references Trail(TRAIL_ID)

end
