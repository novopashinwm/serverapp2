IF  EXISTS (
	SELECT * 
		FROM sys.check_constraints 
		WHERE object_id = OBJECT_ID(N'[dbo].[CK_OPERATOR_DEPARTMENT_RIGHT]') 
		  AND parent_object_id = OBJECT_ID(N'[dbo].[Operator_Department]')
	)
	ALTER TABLE [dbo].[Operator_Department] DROP CONSTRAINT [CK_OPERATOR_DEPARTMENT_RIGHT]
GO

ALTER TABLE [dbo].[Operator_Department]  WITH CHECK ADD  CONSTRAINT [CK_OPERATOR_DEPARTMENT_RIGHT]
	CHECK  ([RIGHT_ID]=(104) OR [RIGHT_ID]=(2) or right_id = 7)
GO

IF  EXISTS (
	SELECT * 
		FROM sys.check_constraints 
		WHERE object_id = OBJECT_ID(N'[dbo].[CK_OperatorGroup_DEPARTMENT_RIGHT]') 
		  AND parent_object_id = OBJECT_ID(N'[dbo].[OperatorGroup_Department]')
	)
	ALTER TABLE [dbo].[OperatorGroup_Department] DROP CONSTRAINT [CK_OperatorGroup_DEPARTMENT_RIGHT]
GO

ALTER TABLE [dbo].[OperatorGroup_Department]  WITH CHECK ADD  CONSTRAINT [CK_OperatorGroup_DEPARTMENT_RIGHT]
	CHECK  ([RIGHT_ID]=(104) OR [RIGHT_ID]=(2) or right_id = 7)
GO

