IF NOT EXISTS (
SELECT c.* FROM sys.tables t 
	JOIN sys.columns c ON t.object_id = c.object_id
WHERE t.NAME = 'IPValues'
	  AND c.name = 'RangeFromInt'
) 
BEGIN
	ALTER TABLE dbo.IPValues ADD  RangeFromInt BIGINT NULL
END

IF NOT EXISTS (
SELECT c.* FROM sys.tables t 
	JOIN sys.columns c ON t.object_id = c.object_id
WHERE t.NAME = 'IPValues'
	  AND c.name = 'RangeToInt'
) 
BEGIN
	ALTER TABLE dbo.IPValues ADD  RangeToInt BIGINT NULL
END

IF NOT EXISTS (
	SELECT i.* FROM sys.indexes i
		JOIN sys.tables t ON t.object_id = i.object_id
	WHERE t.name = 'IPValues' AND i.name='ind_ncl_IPRange'
)
BEGIN
	EXEC ('CREATE NONCLUSTERED INDEX ind_ncl_IPRange ON [dbo].IPValues (RangeFromInt, RangeToInt) INCLUDE (CityId)')	
END

EXEC ('UPDATE dbo.IPValues SET RangeFromInt = CAST(RangeFrom as BIGINT), RangeToInt = CAST(RangeTo as BIGINT)')


