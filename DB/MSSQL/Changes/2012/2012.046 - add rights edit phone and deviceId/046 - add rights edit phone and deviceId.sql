IF NOT EXISTS(SELECT * FROM dbo.[RIGHT] WHERE RIGHT_ID = 21 )
BEGIN
	INSERT INTO dbo.[RIGHT] (RIGHT_ID, SYSTEM, NAME, [DESCRIPTION])
	VALUES (21, 1, 'ViewControllerDeviceId', 'Allows to view contoller EMEI ID')
END

IF (NOT EXISTS(SELECT * FROM dbo.[Right] WHERE RIGHT_ID = 24))
BEGIN
	INSERT INTO dbo.[RIGHT] (RIGHT_ID, SYSTEM, NAME, [DESCRIPTION])
	VALUES (24, 1, 'EditControllerDeviceId', 'Allows to edit contoller EMEI ID')
END 

IF (NOT EXISTS(SELECT * FROM dbo.[Right] WHERE RIGHT_ID = 23))
BEGIN
	INSERT INTO dbo.[RIGHT] (RIGHT_ID, SYSTEM, NAME, [DESCRIPTION])
	VALUES (23, 1, 'EditControllerPhone', 'Allows to edit contoller phone')
END 