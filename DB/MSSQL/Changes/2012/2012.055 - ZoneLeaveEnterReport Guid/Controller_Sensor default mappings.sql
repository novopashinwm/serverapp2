if not exists (select * from sys.columns where object_id = object_id('Controller_Sensor') and name = 'Default_Sensor_Legend_ID')
begin

	alter table Controller_Sensor
		add 
			Default_Sensor_Legend_ID int 
				constraint FK_Controller_Sensor_Controller_Sensor_Legend_ID 
				foreign key references Controller_Sensor_Legend(Controller_Sensor_Legend_ID),
			Default_Multiplier numeric(18, 9),
			Default_Constant   numeric(18, 9),
			Mandatory bit not null default(0)

	alter table H_Controller_Sensor
		add 
			Default_Sensor_Legend_ID int,
			Default_Multiplier numeric(18, 9),
			Default_Constant   numeric(18, 9),
			Mandatory bit not null default(0)

end