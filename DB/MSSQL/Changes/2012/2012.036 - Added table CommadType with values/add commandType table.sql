IF NOT EXISTS(SELECT * FROM sys.tables 
	WHERE name = 'CommandTypes')
BEGIN
	CREATE TABLE dbo.CommandTypes
		(
			id INT PRIMARY KEY,
			code VARCHAR(200) NOT NULL,
			[description] VARCHAR(500)
		)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 1)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (1, 'UDP', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 2)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (2, 'GSM', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 3)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (3, 'SMS', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 4)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (4, 'Monitor', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 5)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (5, 'Trace', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 6)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (6, 'AskPosition', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 7)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (7, 'GetLog', NULL)
END


IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 8)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (8, 'GetLogFromDB', NULL)
END


IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 9)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (9, 'Control', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 10)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (10, 'GetState', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 11)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (11, 'SetState', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 12)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (12, 'SendText', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 13)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (13, 'Confirm', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 14)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (14, 'SetFirmware', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 15)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (15, 'SendFirmware', NULL)
END


IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 16)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (16, 'GetSettings', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 17)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (17, 'SetSettings', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 18)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (18, 'Restart', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 19)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (19, 'RegisterMobileUnit', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 20)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (20, 'AskPositionOverMLP', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 21)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (21, 'CutOffElectricity', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 22)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (22, 'ReopenElectricity', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 23)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (23, 'CutOffFuel', NULL)
END

IF NOT EXISTS(SELECT * FROM dbo.CommandTypes WHERE id = 24)
BEGIN
	INSERT INTO dbo.CommandTypes VALUES (24, 'ReopenFuel', NULL)
END








