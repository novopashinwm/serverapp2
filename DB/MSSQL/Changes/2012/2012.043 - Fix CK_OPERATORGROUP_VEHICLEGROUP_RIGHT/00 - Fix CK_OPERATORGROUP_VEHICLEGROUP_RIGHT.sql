IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_OPERATORGROUP_VEHICLEGROUP_RIGHT]') AND parent_object_id = OBJECT_ID(N'[dbo].[OPERATORGROUP_VEHICLEGROUP]'))
ALTER TABLE [dbo].[OPERATORGROUP_VEHICLEGROUP] DROP CONSTRAINT [CK_OPERATORGROUP_VEHICLEGROUP_RIGHT]
GO

ALTER TABLE [dbo].[OPERATORGROUP_VEHICLEGROUP]  WITH CHECK ADD  CONSTRAINT [CK_OPERATORGROUP_VEHICLEGROUP_RIGHT]
CHECK  ([RIGHT_ID] in (2, 102, 109, 21, 22, 20, 7, 6))
GO

ALTER TABLE [dbo].[OPERATORGROUP_VEHICLEGROUP] CHECK CONSTRAINT [CK_OPERATORGROUP_VEHICLEGROUP_RIGHT]
GO
