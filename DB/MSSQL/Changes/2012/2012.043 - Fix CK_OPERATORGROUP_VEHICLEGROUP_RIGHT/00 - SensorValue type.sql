IF NOT EXISTS(SELECT * FROM sys.table_types WHERE name = 'SensorValue')
BEGIN
	CREATE TYPE SensorValue AS TABLE
	(
		Number INT NOT NULL,
		Value  BIGINT NOT NULL
	)
END