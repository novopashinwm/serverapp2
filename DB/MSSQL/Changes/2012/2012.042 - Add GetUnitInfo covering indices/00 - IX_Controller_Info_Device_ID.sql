if not exists (select * from sys.indexes where name = 'IX_Controller_Info_Device_ID')
begin
	
	create nonclustered index IX_Controller_Info_Device_ID 
		on dbo.Controller_Info (Device_ID)

end;