if not exists (select * from sys.indexes where name = 'IX_Controller_Phone')
begin
	
	create nonclustered index IX_Controller_Phone
		on dbo.Controller (Phone)

end;