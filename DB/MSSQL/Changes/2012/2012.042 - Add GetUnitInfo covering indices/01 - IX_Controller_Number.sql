if not exists (select * from sys.indexes where name = 'IX_Controller_Number')
begin
	
	create nonclustered index IX_Controller_Number
		on dbo.Controller (Number)

end;