DECLARE @controllerTypeId INT
--device
SELECT @controllerTypeId = CONTROLLER_TYPE_ID FROM dbo.CONTROLLER_TYPE
	WHERE TYPE_NAME='Sonim'
IF (@controllerTypeId IS NULL)
BEGIN

	INSERT INTO dbo.CONTROLLER_TYPE
		(TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT, AllowedToAddByCustomer)
	VALUES 
		('Sonim', 1000, NULL, NULL, 1)
		
	SELECT @controllerTypeId = SCOPE_IDENTITY()
END



IF NOT EXISTS(
	SELECT *  FROM dbo.CONTROLLER_SENSOR WHERE CONTROLLER_TYPE_ID = @controllerTypeId)
BEGIN
 INSERT INTO dbo.CONTROLLER_SENSOR 
		(CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, NUMBER, MAX_VALUE, MIN_VALUE, DEFAULT_VALUE, BITS, Descript, Mandatory)
	VALUES
		(@controllerTypeId, 2, 26, 1, 0, 1, 1, 'AllOK', 0),
		(@controllerTypeId, 2, 27, 1, 0, 1, 1, 'Attention Needed But Not Urgent', 0),
		(@controllerTypeId, 2, 28, 1, 0, 1, 1, 'Attention Needed Immediately', 0),
		(@controllerTypeId, 2, 29, 1, 0, 1, 1, 'Full Alert Situation', 0),
		
		(@controllerTypeId, 2, 30, 1, 0, 1, 1, 'Gsm', 0),
		(@controllerTypeId, 2, 31, 1, 0, 1, 1, 'Gps', 0),
		(@controllerTypeId, 2, 32, 1, 0, 1, 1, 'Gprs', 0),
		(@controllerTypeId, 1, 33, 1, 0, 1, 1, 'Batary', 0),
		
		(@controllerTypeId, 1, 34, 1, 0, 1, 1, 'Mode', 0)
END


IF NOT EXISTS (SELECT * FROM dbo.CommandTypes WHERE id  in (25,26,27,28,29,30,31,32))
BEGIN
	INSERT INTO dbo.CommandTypes (id, code, [description])
		VALUES 
			(25, 'CancelAlarm' ,''),
			(26, 'ChangeConfig' ,''),
			(29, 'ReloadDevice' ,''),
			(30, 'VibrateRequest' ,''),
			(31, 'CallPhone' ,''),
			(32, 'ShotdownDevice' ,'')
END


IF NOT EXISTS(SELECT * FROM [dbo].[RIGHT] WHERE NAME='ChangeDeviceConfigBySMS' AND RIGHT_ID=116)
BEGIN
	INSERT INTO [dbo].[RIGHT] (RIGHT_ID, SYSTEM, NAME, DESCRIPTION)
	VALUES (116, 1, 'ChangeDeviceConfigBySMS','���������������� ���������� � ������� ��� ���������')
END 

IF NOT EXISTS(SELECT * FROM [dbo].[CONTROLLER_SENSOR_LEGEND]  WHERE Number=229)
BEGIN
	INSERT INTO [dbo].[CONTROLLER_SENSOR_LEGEND] (CONTROLLER_SENSOR_TYPE_ID, NAME, Number, VALUE_EXPIRED)
		VALUES (1, '����� ������ ����������', 229, 1) 
END





