create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512),
	Default_Multiplier	numeric(18, 9),
	Default_Constant	numeric(18, 9)
);

insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript,
		   t.Default_Multiplier,
		   t.Default_Constant
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' CSTName, 127 Number, Descript = 'CAN_PressurizationAirT, C',	Default_Multiplier = 1,		  Default_Constant = 0
		union select '����������' CSTName, 128 Number, Descript = 'CAN_AirT, 0.00001C',			Default_Multiplier = 0.00001, Default_Constant = 0
		) t
	where ct.Type_Name = 'AvtoGrafCan'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript, Default_Multiplier, Default_Constant)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number
				and target.Default_Multiplier		 = source.Default_Multiplier
				and target.Default_Constant			 = source.Default_Constant)

drop table #Controller_Sensor
