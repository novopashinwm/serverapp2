if not exists (
	select * from sys.objects where name = 'PhoneMQF'
)
begin

	create table PhoneMQF
	(
		PhoneMQF_ID	bigint identity constraint PK_PhoneMQF primary key nonclustered,
		PhoneID		int not null 
			constraint UQ_PhoneMQF_PhoneID unique  
			constraint FK_PhoneMQF_PhoneID foreign key references Phone(Phone_ID),
		NextCheck	datetime not null,
		Count		int not null
	)
	
	create clustered index IX_PhoneMQF_PhoneID on PhoneMQF (PhoneID)
	create nonclustered index IX_PhoneMQF_NextCheck on PhoneMQF (NextCheck) include (PhoneID)

	create table H_PhoneMQF
	(
		H_PhoneMQF_ID	bigint identity constraint PK_H_PhoneMQF primary key nonclustered,
	
		PhoneMQF_ID		bigint not null,
		PhoneID			int not null,
		NextCheck		datetime not null,
		Count			int not null,
		
		TRAIL_ID					int constraint FK_H_PhoneMQF_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end

