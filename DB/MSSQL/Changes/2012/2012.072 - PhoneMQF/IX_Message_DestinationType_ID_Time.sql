if not exists (select * from sys.indexes where name = 'IX_Message_DestinationType_ID_Time')
begin

	create nonclustered index IX_Message_DestinationType_ID_Time
		on Message(DestinationType_ID, Time)
		include (Type)

end