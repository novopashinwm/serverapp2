IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'StopSpeed')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD StopSpeed INT NULL                       --����������� ��������
END


IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'StopSpeedNoIgnition')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD StopSpeedNoIgnition INT NULL				--����������� �������� ��� ���������
END	

IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'SmoothRadiusMotionWithIgnition')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD SmoothRadiusMotionWithIgnition INT NULL   --������ ��� �������� � ����������
END	

IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'SmoothRadius')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD SmoothRadius INT NULL						--������ ����� ����� � �� ��������
END	

IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'SmoothRadiusMotion')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD SmoothRadiusMotion INT NULL				--������ ����� �������� � �� ��������
END
	

IF NOT EXISTS(SELECT * FROM sys.columns c
	JOIN sys.tables t ON t.object_Id = c.object_Id
WHERE t.NAME = 'VEHICLE_KIND' AND c.NAME = 'SmoothRadiusWithIgnition')
BEGIN
	ALTER TABLE dbo.VEHICLE_KIND
		ADD SmoothRadiusWithIgnition INT NULL			--������ ����� ����� � ��������
END


GO 

UPDATE dbo.VEHICLE_KIND
	SET StopSpeed = 10, StopSpeedNoIgnition = 15, 
		SmoothRadiusMotionWithIgnition = 100,
		SmoothRadius = 300,
		SmoothRadiusMotion = 200,
		SmoothRadiusWithIgnition = 200


UPDATE dbo.CONTROLLER_TYPE SET POS_SMOOTH_SUPPORT = 1 WHERE TYPE_NAME='Sonim'

UPDATE dbo.VEHICLE_KIND SET SmoothRadiusMotionWithIgnition = 50, 
	SmoothRadius = 50, SmoothRadiusMotion = 50, SmoothRadiusWithIgnition= 50,
	StopSpeed = 4, StopSpeedNoIgnition = 4
WHERE VEHICLE_KIND_ID IN (5,7)





