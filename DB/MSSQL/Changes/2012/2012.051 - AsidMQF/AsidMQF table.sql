if not exists (
	select * from sys.objects where name = 'AsidMQF'
)
begin

	create table AsidMQF
	(
		AsidMQF_ID	bigint identity constraint PK_AsidMQF primary key nonclustered,
		AsidID		int not null 
			constraint UQ_AsidMQF_AsidID unique  
			constraint FK_AsidMQF_AsidID foreign key references Asid(ID),
		NextCheck	datetime not null,
		Count		int not null
	)
	
	create clustered index IX_AsidMQF_AsidID on AsidMQF (AsidID)
	create nonclustered index IX_AsidMQF_NextCheck on AsidMQF (NextCheck) include (AsidID)

	create table H_AsidMQF
	(
		H_AsidMQF_ID	bigint identity constraint PK_H_AsidMQF primary key nonclustered,
	
		AsidMQF_ID		bigint not null,
		AsidID			int not null,
		NextCheck		datetime not null,
		Count			int not null,
		
		TRAIL_ID					int constraint FK_H_AsidMQF_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)
	
end

