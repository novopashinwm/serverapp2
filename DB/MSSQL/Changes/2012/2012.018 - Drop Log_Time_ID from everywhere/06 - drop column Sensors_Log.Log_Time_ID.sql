if exists (select * from sys.columns where object_id = object_id('dbo.Sensors_Log') and name = 'Log_Time_ID')
begin

	alter table dbo.Sensors_Log
		drop constraint PK_Sensors_Log 

	drop index IX_Sensors_Log_VID_LT on dbo.Sensors_Log

	alter table dbo.Sensors_Log
		alter column Vehicle_ID int not null

	alter table dbo.Sensors_Log
		alter column Log_Time int not null
	
	alter table dbo.Sensors_Log
		add constraint PK_Sensors_Log primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.Sensors_Log
		drop column Log_Time_ID
end
go
