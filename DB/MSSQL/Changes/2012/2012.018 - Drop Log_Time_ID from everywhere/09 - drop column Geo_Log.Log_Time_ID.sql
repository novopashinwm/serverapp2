if exists (select * from sys.columns where object_id = object_id('dbo.Geo_Log') and name = 'Log_Time_ID')
begin
	alter table dbo.Geo_Log
		drop constraint PK_Geo_Log 

	drop index IX_Geo_Log_VID_LT on dbo.Geo_Log

	alter table dbo.Geo_Log
		alter column Vehicle_ID int not null

	alter table dbo.Geo_Log
		alter column Log_Time int not null
	
	alter table dbo.Geo_Log
		add constraint PK_Geo_Log primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.Geo_Log
		drop column Log_Time_ID
end
go
