create nonclustered index IX_GPS_Log_Log_Time on GPS_Log (Log_Time)
go

set nocount on

delete from GPS_Log where vehicle_id is null 
delete from GPS_Log where Log_Time is null 

declare @from int
declare @to int
declare @max_to int
declare @interval int

set @interval = 24*60*60

set @max_to = datediff(ss, '1970', '2012-03-01')
set @from = (select top(1) Log_Time from GPS_Log order by Log_Time)
set @to = @from + @interval

while @to < @max_to
begin
	exec sp_executesql N'
	delete t
		from GPS_Log t
		where t.Log_Time between @from and @to
		and
		exists (
			select 1 
				from dbo.GPS_Log t1
				where t1.Vehicle_ID = t.Vehicle_ID and 
					  t1.Log_Time   = t.Log_Time and 
					  t1.Log_Time_ID > t.Log_Time_ID)
		', 
		@params = N'@from int, @to int', @from = @from, @to = @to

	set @from = (select top(1) Log_Time from GPS_Log where Log_Time >= @to order by Log_Time)		
	set @to = @from +  @interval
	
	print convert(varchar(255), dbo.GetDateFromInt(@from))
end
go

drop index IX_GPS_Log_Log_Time on GPS_Log
go