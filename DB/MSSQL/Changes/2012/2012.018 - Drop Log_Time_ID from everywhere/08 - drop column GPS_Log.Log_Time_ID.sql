if exists (select * from sys.columns where object_id = object_id('dbo.GPS_Log') and name = 'Log_Time_ID')
begin

	alter table dbo.GPS_Log
		drop constraint PK_GPS_Log 

	drop index IX_GPS_Log_VID_LT on dbo.GPS_Log

	alter table dbo.GPS_Log
		alter column Vehicle_ID int not null

	alter table dbo.GPS_Log
		alter column Log_Time int not null
	
	alter table dbo.GPS_Log
		add constraint PK_GPS_Log primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.GPS_Log
		drop column Log_Time_ID
end
go
