create nonclustered index IX_Sensors_Log_Log_Time on Sensors_Log (Log_Time)
go

set nocount on

delete from Sensors_Log where vehicle_id is null 
delete from Sensors_Log where Log_Time is null 

declare @from int
declare @to int
declare @max_log_time int
declare @interval int

set @interval = 24*60*60

set @max_log_time = datediff(ss, '1970', '2012-03-01')
set @from = (select top(1) Log_Time from Sensors_Log order by Log_Time)

while @from < @max_log_time
begin
	set @to = @from + @interval

	exec sp_executesql N'
	delete t
		from Sensors_Log t
		where t.Log_Time between @from and @to
		and
		exists (
			select 1 
				from dbo.Sensors_Log t1
				where t1.Vehicle_ID = t.Vehicle_ID and 
					  t1.Log_Time   = t.Log_Time and 
					  t1.Log_Time_ID > t.Log_Time_ID)
		', 
		@params = N'@from int, @to int', @from = @from, @to = @to

	set @from = (select top(1) Log_Time from Sensors_Log where Log_Time > @to order by Log_Time)		
	
	print convert(varchar(255), dbo.GetDateFromInt(@from))
end
go

drop index IX_Sensors_Log_Log_Time on Sensors_Log
go