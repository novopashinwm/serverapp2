if exists (select * from sys.columns where object_id = object_id('dbo.Statistic_Log') and name = 'Log_Time_ID')
begin

	alter table dbo.Statistic_Log
		drop constraint PK_Statistic_Log 

	drop index IX_Statistic_Log_VID_LT on dbo.Statistic_Log

	alter table dbo.Statistic_Log
		alter column Vehicle_ID int not null

	alter table dbo.Statistic_Log
		alter column Log_Time int not null
	
	alter table dbo.Statistic_Log
		add constraint PK_Statistic_Log primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.Statistic_Log
		drop column Log_Time_ID
end
go
