if exists (select * from sys.columns where object_id = object_id('dbo.Log_Time') and name = 'ID')
begin
	alter table dbo.Log_Time
		drop constraint PK_Log_Time 

	drop index IX_Log_Time_VID_LT on dbo.Log_Time
	drop index IX_Log_Time_InsertTime on dbo.Log_Time
	drop index IX_Log_time_LT on dbo.Log_Time

	alter table dbo.Log_Time
		alter column Vehicle_ID int not null

	alter table dbo.Log_Time
		alter column Log_Time int not null
	
	alter table dbo.Log_Time
		add constraint PK_Log_Time primary key clustered (Vehicle_ID, Log_Time)
		
	create nonclustered index IX_Log_Time_InsertTime on dbo.Log_Time(Vehicle_ID, InsertTime);
	create nonclustered index IX_Log_Time_LT on dbo.Log_Time(Log_Time);

	alter table dbo.Log_Time
		drop column ID
end
go
