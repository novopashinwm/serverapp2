create nonclustered index IX_Position_Radius_Log_Time on Position_Radius (Log_Time)
go

set nocount on

delete from Position_Radius where vehicle_id is null 
delete from Position_Radius where Log_Time is null 

declare @from int
declare @to int
declare @max_to int
declare @interval int

set @interval = 24*60*60

set @max_to = datediff(ss, '1970', '2012-03-01')
set @from = (select top(1) Log_Time from Position_Radius order by Log_Time)
set @to = @from + @interval

while @to < @max_to
begin
	exec sp_executesql N'
	delete t
		from Position_Radius t
		where t.Log_Time between @from and @to
		and
		exists (
			select 1 
				from dbo.Position_Radius t1
				where t1.Vehicle_ID = t.Vehicle_ID and 
					  t1.Log_Time   = t.Log_Time and 
					  t1.Log_Time_ID > t.Log_Time_ID)
		', 
		@params = N'@from int, @to int', @from = @from, @to = @to

	set @from = (select top(1) Log_Time from Position_Radius where Log_Time >= @to order by Log_Time)		
	set @to = @from +  @interval
	
	print convert(varchar(255), dbo.GetDateFromInt(@from))
end
go

drop index IX_Position_Radius_Log_Time on Position_Radius
go