if (exists (select 1 from sys.objects where name = 'Monitoree_Log' and type='v'))
	drop view dbo.Monitoree_Log

--3. ������� ����� monitoree_log ()
go
create view dbo.Monitoree_Log
	as	select 
			lt.Vehicle_ID	MONITOREE_ID,
			lt.Log_Time		LOG_TIME,
			geol.Lng		X,
			geol.lat		Y,
			gpsl.Speed		SPEED,
			lt.Media		MEDIA,
			sl.V			V,
			sl.V1			V1,
			sl.V2			V2,
			sl.V3			V3,
			sl.V4			V4,
			gpsl.Satellites SATELLITES,
			gpsl.Firmware	FIRMWARE,
			sl.Odometer	    ODOMETER,
			gpsl.Altitude	HEIGHT
		from dbo.Log_Time lt				 (nolock)
		left outer join dbo.Geo_Log 	geol (nolock)	on geol.Log_Time = lt.Log_Time and geol.Vehicle_ID = lt.Vehicle_ID
		left outer join dbo.GPS_Log 	gpsl (nolock)	on gpsl.Log_Time = lt.Log_Time and gpsl.Vehicle_ID = lt.Vehicle_ID
		left outer join dbo.Sensors_Log	sl	 (nolock)	on   sl.Log_Time = lt.Log_Time and   sl.Vehicle_ID = lt.Vehicle_ID
go

if (exists (select 1 from sys.objects where name = 'Controller_Stat' and type='v'))
	drop view dbo.Controller_Stat

go
create view dbo.Controller_Stat
	as	select
			c.Controller_ID					CONTROLLER_ID,
			dbo.GetDateFromInt(lt.Log_Time)	[TIME],
			sl.V							V,
			sl.V1							V1,
			sl.V2							V2,
			sl.V3							V3,
			sl.V4							V4,
			sl.DS							DS
		from dbo.Log_Time lt				(nolock)
		inner join dbo.Controller		c	(nolock) on c.Vehicle_ID = lt.Vehicle_ID
		inner join dbo.Sensors_Log		sl	(nolock) on sl.Log_Time = lt.Log_Time and   sl.Vehicle_ID = lt.Vehicle_ID
go