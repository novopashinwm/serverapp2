if exists (select * from sys.columns where object_id = object_id('dbo.Position_Radius') and name = 'Log_Time_ID')
begin
	alter table dbo.Position_Radius
		drop constraint PK_Position_Radius 

	drop index IX_Position_Radius_VID_LT on dbo.Position_Radius

	alter table dbo.Position_Radius
		alter column Vehicle_ID int not null

	alter table dbo.Position_Radius
		alter column Log_Time int not null
	
	alter table dbo.Position_Radius
		add constraint PK_Position_Radius primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.Position_Radius
		drop column Log_Time_ID
end
go
