if exists (select * from sys.columns where object_id = object_id('dbo.CAN_Info') and name = 'Log_Time_ID')
begin

	alter table dbo.CAN_Info
		drop constraint PK_CAN_Info 

	drop index IX_CAN_Info_VID_LT on dbo.CAN_Info

	alter table dbo.CAN_Info
		alter column Vehicle_ID int not null

	alter table dbo.CAN_Info
		alter column Log_Time int not null
	
	alter table dbo.CAN_Info
		add constraint PK_CAN_Info primary key clustered (Vehicle_ID, Log_Time)

	alter table dbo.CAN_Info
		drop column Log_Time_ID
end
go
