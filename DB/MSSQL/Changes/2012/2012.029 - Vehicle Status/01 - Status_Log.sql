if not exists (select * from sys.objects where object_id = object_id('dbo.Status_Log') and type = 'u')
begin
	
	create table dbo.Status_Log 
	(
		Vehicle_ID	int not null,
		Log_Time	int not null,
		[Status]	int not null,
		constraint PK_Status_Log
			primary key clustered (Vehicle_ID, Log_Time)
	) ON TSS_Data2;

end;