/*��������� �������� � ���������� ����� ��� Teltonika FM4200 (FM3101)*/

declare @controller_type_id int

set @controller_type_id = (select controller_type_id from dbo.Controller_Type where Type_Name = 'FM3101')

create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '��������' CSTName,	 1  Number, Descript = 'Digital Input Status 1'	
		union select '��������' CSTName,	 2  Number, Descript = 'Digital Input Status 2'
		union select '��������' CSTName,	 3  Number, Descript = 'Digital Input Status 3'
		union select '��������' CSTName,	 4  Number, Descript = 'Digital Input 4'					
		union select '����������' CSTName,	 9  Number, Descript = 'Analog Input 1'
		union select '����������' CSTName,	 10 Number, Descript = 'Analog Input 2'						
		union select '����������' CSTName,	 11 Number, Descript = 'Analog Input 3'							
		union select '����������' CSTName,	 19 Number, Descript = 'Analog Input 4'	
		) t
	where ct.Type_Name = 'FM3101'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

drop table #Controller_Sensor
