create table #Controller_Sensor 
(
	controller_type_id int, 
	controller_sensor_type_id int, 
	number int, 
	Descript nvarchar(512)
);
 
insert into #Controller_Sensor
	select ct.Controller_Type_ID,
		   cst.Controller_Sensor_Type_ID,
		   t.Number,
		   t.Descript
	from dbo.Controller_Type ct, dbo.Controller_Sensor_Type cst, (
			  select '����������' CSTName, 0 Number, Descript = 'LastPublishingDurationMilliseconds'	
		union select '����������' CSTName, 1 Number, Descript = 'ErrorSource'							
		union select '����������' CSTName, 2 Number, Descript = 'ErrorCode'						    
		union select '����������' CSTName, 3 Number, Descript = 'TimeToFirstGPSFix'					
		union select '����������' CSTName, 5 Number, Descript = 'BatteryLevel'							
		union select '����������' CSTName, 6 Number, Descript = 'SatellitesInView'						
		union select '��������'	  CSTName,11 Number, Descript = 'GpsEnabled'							
		union select '��������'	  CSTName,12 Number, Descript = 'IsPlugged'	
		union select '��������'	  CSTName,13 Number, Descript = 'RadioScannerEnabled'	
		union select '��������'	  CSTName,14 Number, Descript = 'DataSenderEnabled'	
		union select '��������'	  CSTName,15 Number, Descript = 'IsMoving'	
		union select '����������' CSTName,16 Number, Descript = 'DebugMessage'	
		union select '����������' CSTName,17 Number, Descript = 'DebugValue'	
		union select '����������' CSTName,18 Number, Descript = 'SatellitesWithAlmanac'	
		union select '����������' CSTName,19 Number, Descript = 'SatellitesWithEphemeris'
		union select '��������'   CSTName,20 Number, Descript = 'WlanAllowsGps'
		union select '��������'   CSTName,21 Number, Descript = 'ActualSatelliteInfo'
		) t
	where ct.Type_Name = 'S60V3FP'
	  and cst.Name = t.CSTName

insert into dbo.controller_sensor 
	(controller_type_id, controller_sensor_type_id, number, Descript)
	select * 
		from #Controller_Sensor source
		where not exists (
			select 1 
				from dbo.Controller_Sensor target
				where 1=1
				and	target.controller_type_id        = source.controller_type_id
				and	target.controller_sensor_type_id = source.controller_sensor_type_id
				and	target.number					 = source.number)

create table #Controller_Sensor_Legend
(
	Controller_Sensor_Type_ID int,
	Name nvarchar(50),
	Number int
)

insert into #Controller_Sensor_Legend
	select cst.Controller_Sensor_Type_ID, t.Name, t.Number
		from dbo.Controller_Sensor_Type cst, (
			select '����������' CSTName, 100 Number, Name = 'LastPublishingDurationMilliseconds' 
	  union select '����������' CSTName, 101 Number, Name = 'ErrorSource'						  
	  union select '����������' CSTName, 102 Number, Name = 'ErrorCode'						  
	  union select '����������' CSTName, 103 Number, Name = 'TimeToFirstGPSFix'				  
	  union select '����������' CSTName,  11 Number, Name = 'BatteryLevel'						  
	  union select '����������' CSTName,  13 Number, Name = 'SatellitesInView'					  
		union select '��������'	CSTName,  14 Number, Name = 'GpsEnabled'							
		union select '��������'	CSTName,  15 Number, Name = 'IsPlugged'	
		union select '��������'	CSTName,  16 Number, Name = 'RadioScannerEnabled'	
		union select '��������'	CSTName,  17 Number, Name = 'DataSenderEnabled'	
		union select '��������'	CSTName,  18 Number, Name = 'IsMoving'
	  ) t	
		where t.CSTName = cst.Name

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number)
	select * 
		from #Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)

drop table #Controller_Sensor
drop table #Controller_Sensor_Legend