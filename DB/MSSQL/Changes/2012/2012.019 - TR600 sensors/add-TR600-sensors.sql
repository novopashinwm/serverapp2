/*��������� ��� ����������� Globalsat - TR203
	����������� ������� ����������� (��������� ������, ����������� etc)
*/

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'TR600')

if (@controller_type_id is null)
begin
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
		values ('TR600', 200, 0, 0)
	set @controller_type_id = @@identity;
end;

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

insert into @controller_sensor values (@controller_type_id, 1, 0,    0, 100, 0,    64, 	'Battery capacity, %');
insert into @controller_sensor values (@controller_type_id, 1, 2,  200,-100, 0,    64, 	'Temperature, degrees');
insert into @controller_sensor values (@controller_type_id, 2, 3,    0,   1, 0, 	1, 		'SOS alarm');
insert into @controller_sensor values (@controller_type_id, 2, 4,    0,   1, 0, 	1, 		'Parking alarm');
insert into @controller_sensor values (@controller_type_id, 2, 5,    0,   1, 0, 	1, 		'Sleeping alarm');
insert into @controller_sensor values (@controller_type_id, 2, 6,    0,   1, 0, 	1, 		'Geo-fence alarm');
insert into @controller_sensor values (@controller_type_id, 2, 7,    0,   1, 0, 	1, 		'Speed alarm');
insert into @controller_sensor values (@controller_type_id, 2, 8,    0,   1, 0, 	1, 		'Battery low alarm');
insert into @controller_sensor values (@controller_type_id, 2, 9,    0,   1, 0, 	1, 		'Autonomous GeoFence alarm');
insert into @controller_sensor values (@controller_type_id, 2, 10,   0,   1, 0, 	1, 		'Battery Disconnection alarm');

insert into @controller_sensor values (@controller_type_id, 2, 21,   0,   1, 0, 	1, 		'�������� ���� DS1');
insert into @controller_sensor values (@controller_type_id, 2, 22,   0,   1, 0, 	1, 		'�������� ���� DS2');
insert into @controller_sensor values (@controller_type_id, 2, 23,   0,   1, 0, 	1, 		'�������� ���� DS3');
insert into @controller_sensor values (@controller_type_id, 2, 24,   0,   1, 0, 	1, 		'ACC (���������)');

insert into @controller_sensor values (@controller_type_id, 2, 31,   0,   1, 0, 	1, 		'�������� ����� DOut1');
insert into @controller_sensor values (@controller_type_id, 2, 32,   0,   1, 0, 	1, 		'�������� ����� DOut2');
insert into @controller_sensor values (@controller_type_id, 2, 33,   0,   1, 0, 	1, 		'�������� ����� DOut3');

insert into @controller_sensor values (@controller_type_id, 1, 41,    65535, 0, 0,    64, 	'���������� ���� 0 AN0');
insert into @controller_sensor values (@controller_type_id, 1, 42,    65535, 0, 0,    64, 	'���������� �������, mV');

insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)