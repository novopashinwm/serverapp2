if exists (select * from sys.objects where name = 'CK_OPERATOR_DEPARTMENT_RIGHT' and type = 'c')
	alter table OPERATOR_DEPARTMENT drop constraint CK_OPERATOR_DEPARTMENT_RIGHT
go

alter table OPERATOR_DEPARTMENT 
	add constraint CK_OPERATOR_DEPARTMENT_RIGHT
		check ([RIGHT_ID] in (
			   2 /*Security Admin*/
			,  7 /*EditVehicles*/
			, 20 /*ViewControllerPhone		*/
			, 21 /*ViewControllerDeviceID	*/
			, 23 /*EditControllerPhone		*/
			, 24 /*EditControllerDeviceId	*/
			, 25 /*ControllerPasswordAccess	*/
			,102 /*VehicleAccess			*/
			,104 /*DepartmentAccess			*/
			,111 /*PayForCaller				*/
		))
go
