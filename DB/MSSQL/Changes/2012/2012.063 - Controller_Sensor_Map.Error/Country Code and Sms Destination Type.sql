if not exists (select * from sys.columns where object_id = object_id('Country') and name = 'PhonePrefix')
begin

	alter table Country
		add PhonePrefix varchar(10)
end
go
update Country
	set PhonePrefix = 
		case Name 
			when 'Russia'	then '7'
			when 'Ukraine'	then '380'
			when 'India'	then '91'
			else null
		end
		where PhonePrefix is null
go
if not exists (select * from sys.columns where object_id = object_id('Country') and name = 'SmsDestinationType')
begin

	alter table Country
		add SmsDestinationType int

end
go
update Country
	set SmsDestinationType = 
		case Name 
			when 'Russia'	then 2 --MessageDestinationType.Smpp
			when 'Ukraine'	then 0 --unknown transport
			when 'India'	then 1 --MessageDestinationType.Http
			else null
		end			
	where SmsDestinationType is null
