if not exists (select * from sys.columns where object_id = object_id('Controller_Sensor_Map') and name = 'Error')
begin

	alter table Controller_Sensor_Map
		add Error numeric(18, 9)
		
	alter table H_Controller_Sensor_Map
		add Error numeric(18, 9)

end