insert into dbo.[Right] (Right_ID, [System], Name, Description)
	select 21, 1, 'ViewControllerDeviceID', 'Allows to view id of installed device'
	where not exists (select 1 from dbo.[Right] where Right_ID = 21)