/*�� ����� ������ �������� Property ID ��� �������� 28 � 29*/

update cs
	set Number = case Number 
		when 28 then 200 
		when 29 then 201 
	end
	from dbo.Controller_Sensor cs
	join dbo.Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	where ct.Type_Name = 'FM3101'
	and cs.Number in (28, 29)
