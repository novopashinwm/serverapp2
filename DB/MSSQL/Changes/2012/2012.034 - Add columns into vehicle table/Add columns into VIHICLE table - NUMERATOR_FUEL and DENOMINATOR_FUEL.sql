
IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'VEHICLE' AND syscolumns.name = 'NUMERATOR_FUEL'
) 
BEGIN
	ALTER TABLE  dbo.VEHICLE ADD NUMERATOR_FUEL INT NULL
	exec ('UPDATE dbo.VEHICLE SET NUMERATOR_FUEL = [dbo].[GetRuleValue](VEHICLE_ID, 10)')
END


IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'VEHICLE' AND syscolumns.name = 'DENOMINATOR_FUEL'
) 
BEGIN
	ALTER TABLE  dbo.VEHICLE ADD DENOMINATOR_FUEL INT NULL
	exec ('UPDATE dbo.VEHICLE SET DENOMINATOR_FUEL = 10000')
END


IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'H_VEHICLE' AND syscolumns.name = 'NUMERATOR_FUEL'
) 
BEGIN
	ALTER TABLE  dbo.H_VEHICLE ADD NUMERATOR_FUEL INT NULL
END


IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'H_VEHICLE' AND syscolumns.name = 'DENOMINATOR_FUEL'
) 
BEGIN
	ALTER TABLE  dbo.H_VEHICLE ADD DENOMINATOR_FUEL INT NULL
END





