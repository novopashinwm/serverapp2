set IDENTITY_INSERT Web_Point_Type ON

insert into dbo.Web_Point_Type ([Type_ID], [Name], [Description])
	select 2, 'EmergencyCall', 'Web points that are emergency calls'
	where not exists (select 1 from dbo.Web_Point_Type where [Type_ID] = 2)
	
set IDENTITY_INSERT Web_Point_Type OFF