insert into dbo.[Right] (Right_ID, [System], Name, Description)
	select 22, 1, 'EditPublicPoints', 'Allows to add/change/remove publicly accessible points of interest'
	where not exists (select 1 from dbo.[Right] where Right_ID = 22)
	