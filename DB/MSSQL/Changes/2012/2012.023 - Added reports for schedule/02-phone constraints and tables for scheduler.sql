if (not exists(select * from sys.objects where object_id = object_id('phone_schedulerevent') and type = 'u'))
begin

create table phone_schedulerevent (
	schedulerevent_id int not null,
	phone_id int not null,
	constraint pk_phone_schedulerevent primary key clustered (
		schedulerevent_id asc,
		phone_id asc
	)
)

alter table phone_schedulerevent with check
	add constraint fk_phone_schedulerevent_phone foreign key (phone_id)
	references phone(phone_id)

alter table phone_schedulerevent with check
	add constraint fk_phone_schedulerevent_schedulerevent foreign key (schedulerevent_id)
	references schedulerevent(schedulerevent_id)

end

if (not exists(select * from sys.objects where object_id = object_id('phone_schedulerqueue') and type = 'u'))
begin

create table phone_schedulerqueue (
	schedulerqueue_id int not null,
	phone_id int not null,
	constraint pk_phone_schedulerqueue primary key clustered (
		schedulerqueue_id asc,
		phone_id asc
	)
)

alter table phone_schedulerqueue with check
	add constraint fk_phone_schedulerqueue_phone foreign key (phone_id)
	references phone(phone_id)

alter table phone_schedulerqueue with check
	add constraint fk_phone_schedulerqueue_schedulerqueue foreign key (schedulerqueue_id)
	references schedulerqueue(schedulerqueue_id)

end
