if (col_length('Email', 'Confirmed') is null)
begin

alter table Email
	add Confirmed bit not null
	constraint dv_Confirmed default 0

end

if (col_length('Email', 'InsertDate') is null)
begin
alter table Email
	add InsertDate datetime not null
	constraint dv_InsertDate default getutcdate()
end

if (col_length('Email', 'ConfirmDate') is null)
begin
alter table Email
	add ConfirmDate datetime null
end

if (col_length('Phone', 'Confirmed') is null)
begin
alter table Phone
	add Confirmed bit not null
	constraint dv_Phone_Confirmed default 0
end


if (col_length('Phone', 'InsertDate') is null)
begin
alter table Phone
	add InsertDate datetime not null
	constraint dv_Phone_InsertDate default getutcdate()
end

if (col_length('Phone', 'ConfirmDate') is null)
begin
alter table Phone
	add ConfirmDate datetime null
end


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_PHONE_SCHEDULEREVENT_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_PHONE_SCHEDULEREVENT]'))
ALTER TABLE [dbo].[H_PHONE_SCHEDULEREVENT] DROP CONSTRAINT [FK_H_PHONE_SCHEDULEREVENT_Trail_ID]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_PHONE_SCHEDULEREVENT]') AND type in (N'U'))
DROP TABLE [dbo].[H_PHONE_SCHEDULEREVENT]
GO

CREATE TABLE [dbo].[H_PHONE_SCHEDULEREVENT](
	[SCHEDULEREVENT_ID] [int] NOT NULL,
	[PHONE_ID] [int] NOT NULL,
	[TRAIL_ID] [int] NOT NULL,
	[ACTION] [nvarchar](6) NOT NULL,
	[ACTUAL_TIME] [datetime] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_H_PHONE_SCHEDULEREVENT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[H_PHONE_SCHEDULEREVENT]  WITH CHECK ADD  CONSTRAINT [FK_H_PHONE_SCHEDULEREVENT_Trail_ID] FOREIGN KEY([TRAIL_ID])
REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
GO

ALTER TABLE [dbo].[H_PHONE_SCHEDULEREVENT] CHECK CONSTRAINT [FK_H_PHONE_SCHEDULEREVENT_Trail_ID]
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_PHONE_SCHEDULERQUEUE_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_PHONE_SCHEDULERQUEUE]'))
ALTER TABLE [dbo].[H_PHONE_SCHEDULERQUEUE] DROP CONSTRAINT [FK_H_PHONE_SCHEDULERQUEUE_Trail_ID]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_PHONE_SCHEDULERQUEUE]') AND type in (N'U'))
DROP TABLE [dbo].[H_PHONE_SCHEDULERQUEUE]
GO

CREATE TABLE [dbo].[H_PHONE_SCHEDULERQUEUE](
	[SCHEDULERQUEUE_ID] [int] NOT NULL,
	[PHONE_ID] [int] NOT NULL,
	[TRAIL_ID] [int] NOT NULL,
	[ACTION] [nvarchar](6) NOT NULL,
	[ACTUAL_TIME] [datetime] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_H_PHONE_SCHEDULERQUEUE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[H_PHONE_SCHEDULERQUEUE]  WITH CHECK ADD  CONSTRAINT [FK_H_PHONE_SCHEDULERQUEUE_Trail_ID] FOREIGN KEY([TRAIL_ID])
REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
GO

ALTER TABLE [dbo].[H_PHONE_SCHEDULERQUEUE] CHECK CONSTRAINT [FK_H_PHONE_SCHEDULERQUEUE_Trail_ID]
GO


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_Phone_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_Phone]'))
ALTER TABLE [dbo].[H_Phone] DROP CONSTRAINT [FK_H_Phone_Trail_ID]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Phone]') AND type in (N'U'))
DROP TABLE [dbo].[H_Phone]

CREATE TABLE [dbo].[H_Phone](
	[Phone_ID] [int] NOT NULL,
	[Phone] [nvarchar](300) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[Comment] [nvarchar](512) NULL,
	[TRAIL_ID] [int] NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[ConfirmDate] [datetime] NULL,
	[ACTION] [nvarchar](6) NOT NULL,
	[ACTUAL_TIME] [datetime] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Operator_ID] [int] NULL,
 CONSTRAINT [PK_H_Phone] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[H_Phone]  WITH CHECK ADD  CONSTRAINT [FK_H_Phone_Trail_ID] FOREIGN KEY([TRAIL_ID])
REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
GO

ALTER TABLE [dbo].[H_Phone] CHECK CONSTRAINT [FK_H_Phone_Trail_ID]
GO


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_H_EMAIL_Trail_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[H_EMAIL]'))
ALTER TABLE [dbo].[H_EMAIL] DROP CONSTRAINT [FK_H_EMAIL_Trail_ID]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_EMAIL]') AND type in (N'U'))
DROP TABLE [dbo].[H_EMAIL]
GO

CREATE TABLE [dbo].[H_EMAIL](
	[EMAIL_ID] [int] NOT NULL,
	[EMAIL] [nvarchar](300) NOT NULL,
	[NAME] [nvarchar](300) NULL,
	[COMMENT] [nvarchar](512) NULL,
	[TRAIL_ID] [int] NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[ConfirmDate] [datetime] NULL,
	[ACTION] [nvarchar](6) NOT NULL,
	[ACTUAL_TIME] [datetime] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Operator_ID] [int] NULL,
 CONSTRAINT [PK_H_EMAIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[H_EMAIL]  WITH CHECK ADD  CONSTRAINT [FK_H_EMAIL_Trail_ID] FOREIGN KEY([TRAIL_ID])
REFERENCES [dbo].[TRAIL] ([TRAIL_ID])
GO

ALTER TABLE [dbo].[H_EMAIL] CHECK CONSTRAINT [FK_H_EMAIL_Trail_ID]
GO