IF NOT EXISTS (
	SELECT * FROM sys.indexes i 
		JOIN sys.objects o ON o.object_id = o.object_id
	WHERE i.Name = 'IX_LogTime' AND o.name = 'Command_Result'
)

BEGIN
	CREATE NONCLUSTERED INDEX [IX_LogTime] ON [dbo].[Command_Result] 
	(
		[Log_Time], [Result_Type_id]
	)
	INCLUDE ( Command_ID ) 	
END



