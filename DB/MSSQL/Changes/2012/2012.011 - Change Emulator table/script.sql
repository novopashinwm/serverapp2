IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'emulator' 
           AND  COLUMN_NAME = 'DateFrom')
alter table emulator 
  add DateFrom int

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'emulator' 
           AND  COLUMN_NAME = 'DateTo')
alter table emulator 
  add DateTo int