/*��������� ��� ����������� ST101V2
	����������� ������� ����������� (��������� ������, ����������� etc)
*/
begin tran

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'ST101V2')

if (@controller_type_id is null)
begin
	print 'Inserting new controller type: '
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
		values ('ST101V2', 100, 0, 1)
	set @controller_type_id = @@identity;
end;

declare @controller_sensor table (
	CONTROLLER_TYPE_ID			int,
	CONTROLLER_SENSOR_TYPE_ID	int,
	NUMBER						int,
	MAX_VALUE					bigint,
	MIN_VALUE					bigint,
	DEFAULT_VALUE				bigint,
	BITS						int,
	Descript					nvarchar(512));

set nocount on
insert into @controller_sensor values (@controller_type_id, 2,  2,    0,   1,   0, 	1, 		'Pin02AccidentGray');             
insert into @controller_sensor values (@controller_type_id, 2,  3,    0,   1,   0, 	1, 		'Pin03LightsOrange');             
insert into @controller_sensor values (@controller_type_id, 2,  6,    0,   1,   0, 	1, 		'Pin06BlueSOS');                  
insert into @controller_sensor values (@controller_type_id, 1, 10,    0,   1,   0, 	1, 		'Pin10GreenBlackTemperature');    
insert into @controller_sensor values (@controller_type_id, 1, 11,    0,   1,   0, 	1, 		'Pin11YellowBlackFuel');          
insert into @controller_sensor values (@controller_type_id, 2, 13,    0,   1,   0, 	1, 		'Pin13BrownDoorControl');         
insert into @controller_sensor values (@controller_type_id, 2, 14,    0,   1,   0, 	1, 		'Pin14GreenIgnition');            
insert into @controller_sensor values (@controller_type_id, 2, 15,    0,   1,   0, 	1, 		'Pin15PurpleAirConditioner');     
insert into @controller_sensor values (@controller_type_id, 2, 16,    0,   1,   0, 	1, 		'Pin16PowerOn');                  
																					 
insert into @controller_sensor values (@controller_type_id, 2, 55,    0,   1,   0, 	1, 		'OverSpeedAlert');                
insert into @controller_sensor values (@controller_type_id, 2, 56,    0,   1,   0, 	1, 		'OutOfGeoFence');                 
insert into @controller_sensor values (@controller_type_id, 2, 57,    0,   1,   0, 	1, 		'Movement');                      
set nocount off

print 'Updating existing sensors: '
update cs
set MAX_VALUE = t.MAX_VALUE,
MIN_VALUE = t.MIN_VALUE,
CONTROLLER_SENSOR_TYPE_ID = t.CONTROLLER_SENSOR_TYPE_ID,
DEFAULT_VALUE = t.DEFAULT_VALUE,
BITS = t.BITS,
Descript = t.Descript
from dbo.Controller_Sensor cs
join @controller_sensor t on t.CONTROLLER_TYPE_ID = cs.CONTROLLER_TYPE_ID and t.NUMBER = cs.NUMBER


print 'Inserting new sensors: '
insert into dbo.Controller_Sensor (
	CONTROLLER_TYPE_ID,
	CONTROLLER_SENSOR_TYPE_ID,
	NUMBER,
	MAX_VALUE,
	MIN_VALUE,
	DEFAULT_VALUE,
	BITS,
	Descript)
select * from @controller_sensor t
	where not exists (select 1 
						from dbo.Controller_Sensor cs 
						where cs.Controller_Type_ID = t.Controller_Type_ID 
						  and cs.Number = t.Number)

commit