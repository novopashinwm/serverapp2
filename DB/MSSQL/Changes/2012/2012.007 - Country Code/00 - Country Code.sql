if not exists (select * from sys.columns where object_id = object_id('dbo.Country') and name = 'Currency')
begin
	alter table dbo.Country
		add Currency varchar(3) --��� ������ ������
end
go

update dbo.Country
	set Currency = 'INR'
		where Name = 'India'
		and Currency is null
		
insert into dbo.Country (Name, Currency)
	select 'Russia', 'RUR'
		where not exists (select * from Country where Name = 'Russia')