if (exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_DataAbsence'))
and 
not exists (select * from sys.columns where object_id = object_id('dbo.CompoundRule_DataAbsence') and name = 'minutes'))
	drop table CompoundRule_DataAbsence
go

if (exists (select * from sys.objects where object_id = object_id('dbo.H_CompoundRule_DataAbsence'))
and 
not exists (select * from sys.columns where object_id = object_id('dbo.H_CompoundRule_DataAbsence') and name = 'minutes'))
	drop table H_CompoundRule_DataAbsence
go

if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_DataAbsence'))
begin

	create table dbo.CompoundRule_DataAbsence
	(
		CompoundRule_DataAbsence_ID int identity(1,1)
			constraint PK_CompoundRule_DataAbsence
				primary key clustered,
		CompoundRule_ID int not null
			constraint FK_CompoundRule_DataAbsence_CompoundRule
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		[Minutes]	int not null
	)
end
if not exists (select * from sys.objects where object_id = object_id('dbo.H_CompoundRule_DataAbsence'))
begin
	create table dbo.H_CompoundRule_DataAbsence
	(
		H_CompoundRule_DataAbsence_ID int identity constraint PK_H_CompoundRule_DataAbsence primary key clustered,
	
		CompoundRule_DataAbsence_ID int not null,
		CompoundRule_ID int not null,
		[Minutes]	int not null,
		
		TRAIL_ID					int constraint FK_H_CompoundRule_DataAbsence_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)

end