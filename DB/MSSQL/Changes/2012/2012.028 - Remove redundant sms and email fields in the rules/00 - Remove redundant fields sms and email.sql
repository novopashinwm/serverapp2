if Exists(select * from sys.columns where Name = N'Sms'  
            and Object_ID = Object_ID(N'CompoundRule_Message_Template'))
begin
	alter table CompoundRule_Message_Template
	drop column Sms
end

if Exists(select * from sys.columns where Name = N'Email'  
            and Object_ID = Object_ID(N'CompoundRule_Message_Template'))
begin
	alter table CompoundRule_Message_Template
	drop column Email
end

if Exists(select * from sys.columns where Name = N'Sms'  
            and Object_ID = Object_ID(N'H_CompoundRule_Message_Template'))
begin
	alter table H_CompoundRule_Message_Template
	drop column Sms
end

if Exists(select * from sys.columns where Name = N'Email'  
            and Object_ID = Object_ID(N'H_CompoundRule_Message_Template'))
begin
	alter table H_CompoundRule_Message_Template
	drop column Email
end
