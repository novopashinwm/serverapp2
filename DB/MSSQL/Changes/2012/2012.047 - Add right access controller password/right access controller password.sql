IF NOT EXISTS(SELECT * FROM dbo.[RIGHT] WHERE [RIGHT_ID] = 25)
BEGIN
	INSERT INTO dbo.[RIGHT] ([RIGHT_ID], [SYSTEM], NAME, [DESCRIPTION], HELP_URL, URL)
	VALUES (25, 1, 'ControllerPasswordAccess', 'Access to change and view controller password', NULL, NULL)
END

