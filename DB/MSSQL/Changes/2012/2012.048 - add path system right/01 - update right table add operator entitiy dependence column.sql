IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'right' AND syscolumns.name = 'forOperator'
) 
BEGIN
	ALTER TABLE  dbo.[right] ADD forOperator bit NULL
	-- ������������� ��������� ��������:
	-- NULL - �� ��������� � ������ �������� ���� �� �������������� � WEB UI admin.
	-- 1 ������ ���������. ��������� ������� ����� � ��������� ����� �������������� �������� ��-���������.
	-- 0 ������� ���������. ��������� ������� ����� � ��������� ����� �������������� �������� ��-���������.
END
go

IF NOT EXISTS(
	SELECT syscolumns.name
		FROM sysobjects 
		JOIN syscolumns ON sysobjects.id = syscolumns.id
	WHERE sysobjects.xtype='U' AND sysobjects.name = 'h_right' AND syscolumns.name = 'forOperator'
) 
BEGIN
	ALTER TABLE  dbo.[h_right] ADD forOperator bit NULL
	-- ������������� ��������� ��������:
	-- NULL - �� ��������� � ������ �������� ���� �� �������������� � WEB UI admin.
	-- 1 ������ ���������. ��������� ������� ����� � ��������� ����� �������������� �������� ��-���������.
	-- 0 ������� ���������. ��������� ������� ����� � ��������� ����� �������������� �������� ��-���������.
END
go

update [right]
		set forOperator = 1
		where right_id in (103, 115)
		and (forOperator is null or forOperator <> 1)
go

alter view [dbo].[v_operator_rights]   
as  
 WITH allRights(operator_id, right_id, allowed, priority) AS   
 (  
  select operator_id, right_id, allowed, 2 'priority'  
   from dbo.RIGHT_OPERATOR   
  union all  
  select og_o.operator_id, og_v.right_id, allowed, 1 'priority'  
   from dbo.RIGHT_OPERATORGROUP og_v  
   join dbo.operatorgroup_operator og_o on og_o.operatorgroup_id = og_v.operatorgroup_id
  -- ���������������� �����-����������, ������� � ��������� ������ ���� �������������� �������� �����.
  union all
  select o.operator_id, r.right_id, 1, 0 'priority'
    from [right] r
    cross join operator o
    -- ������ ��� ����, ������� ��-��������� (� ��������� ������� � �������� ���� ���������) ���������.
    where r.[forOperator] = 1
 )  
 select distinct operator_id, right_id  
 from allRights r1  
 where allowed = 1  
 and not exists (select * from allRights r2   
     where r2.operator_id = r1.operator_id   
     and r2.right_id = r1.right_id  
     and r2.allowed = 0  
     and r2.priority > r1.priority)  
  