if not exists (select * from sysindexes where name = 'IX_Command_Target_ID_Type_ID_Result_Date')
begin
	create nonclustered index IX_Command_Target_ID_Type_ID_Result_Date on dbo.Command(Target_ID, Type_ID, Date_Received)
end