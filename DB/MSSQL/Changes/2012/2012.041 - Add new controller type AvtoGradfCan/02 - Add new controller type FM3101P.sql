if (not exists(select 1 from controller_type where type_name = 'FM3101P'))
begin
declare @newController int

insert into controller_type (type_name, packet_length, pos_absence_support, pos_smooth_support, allowedToAddByCustomer)
select 'FM3101P', 70, NULL, NULL, 0
set @newController = @@identity

insert into controller_sensor (controller_type_id, controller_sensor_type_id, number, max_value, min_value, default_value, bits, descript, value_expired)
select @newController, controller_sensor_type_id, number, max_value, min_value, default_value, bits, descript, value_expired
from controller_sensor where controller_type_id = 7 and descript not like 'CAN%'

end
