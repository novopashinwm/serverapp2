if (not exists(select 1 from controller_type where type_name = 'AvtoGrafCan'))
begin
declare @newController int

update controller_type set type_name = 'AvtoGrafCan' where controller_type_id = 10

insert into controller_type (type_name, packet_length, pos_absence_support, pos_smooth_support, allowedToAddByCustomer)
select 'AvtoGraf', 1000, NULL, 1, 1
set @newController = @@identity

insert into controller_sensor (controller_type_id, controller_sensor_type_id, number, max_value, min_value, default_value, bits, descript, value_expired)
select @newController, controller_sensor_type_id, number, max_value, min_value, default_value, bits, descript, value_expired
from controller_sensor where controller_type_id = 10 and descript not like 'CAN%'

update c 
	set controller_type_id = (select controller_type_id from controller_type where type_name = 'AvtoGraf')
	from controller c
	where c.controller_type_id = 10 
	  and c.vehicle_id is not null
	  and not exists (
		select 1 
		from can_info ci with (nolock)
		where ci.vehicle_id = c.vehicle_id)

end
