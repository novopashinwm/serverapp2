insert into dbo.Controller_Type (type_name, packet_length, pos_absence_support, pos_smooth_support, allowedToAddByCustomer)
select type_name, 1000, null, 0, 1
	from (
					select 'Teltonika FM1100' Type_Name
		union all	select 'Teltonika FM5300' 
		union all	select 'Teltonika GH3000'
	) t
	where not exists (select 1 from dbo.Controller_Type ct where ct.Type_Name = t.Type_Name)