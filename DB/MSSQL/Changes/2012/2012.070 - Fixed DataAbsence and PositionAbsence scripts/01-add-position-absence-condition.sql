if (not exists (select * from sys.columns where object_id = object_id('dbo.CompoundRule_PositionAbsence') and name = 'Minutes')
    and exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_PositionAbsence')))
	drop table CompoundRule_PositionAbsence
go

if not exists (select * from sys.objects where object_id = object_id('dbo.CompoundRule_PositionAbsence'))
begin

	create table dbo.CompoundRule_PositionAbsence
	(
		CompoundRule_PositionAbsence_ID int identity(1,1)
			constraint PK_CompoundRule_PositionAbsence
				primary key clustered,
		CompoundRule_ID int not null
			constraint FK_CompoundRule_PositionAbsence_CompoundRule
				foreign key references dbo.CompoundRule(CompoundRule_ID),
		[Minutes]	int not null
	)

	create table dbo.H_CompoundRule_PositionAbsence
	(
		H_CompoundRule_PositionAbsence_ID int identity constraint PK_H_CompoundRule_PositionAbsence primary key clustered,
	
		CompoundRule_PositionAbsence_ID int not null,
		CompoundRule_ID int not null,
		[Minutes]	int not null,
		
		TRAIL_ID					int constraint FK_H_CompoundRule_PositionAbsence_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null
	)

end