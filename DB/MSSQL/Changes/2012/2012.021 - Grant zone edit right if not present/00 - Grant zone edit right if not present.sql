insert into dbo.Operator_Zone 
	select oz.Operator_ID, oz.Zone_ID, 19, 1
	from dbo.Operator_Zone oz
	where oz.Right_ID = 105 --ZoneAccess
	  and not exists (
		select 1 from dbo.Operator_Zone e
			where e.Operator_ID = oz.Operator_ID
			  and e.Zone_ID = oz.Zone_ID
			  and e.Right_ID = 19 --EditZone
	)
						