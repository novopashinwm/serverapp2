if not exists (select * from sys.objects where object_id = object_id('dbo.SensorValueChange'))
begin

	--���������� �� ��������� ���������� �������� �������, ��������, ��� ������� ������� - ���������� � ���������, - ��� ��������� �������.
	create table dbo.SensorValueChange
	(
		--��������� ����
		SensorValueChange_ID int identity (1,1)
			constraint PK_SensorValueChange
				primary key clustered,
		--������ �� ��������, ������������� �� ������� ����������
		Controller_ID int not null
			constraint FK_SensorValueChange_Vehicle_ID
				foreign key references dbo.Vehicle(Vehicle_ID),
		--������ �� ������
		Controller_Sensor_Legend_ID int not null
			constraint FK_SensorValueChange_Controller_Sensor_Legend_ID
				foreign key references dbo.Controller_Sensor_Legend(Controller_Sensor_Legend_ID),
		--��������������� ����� ��������� �������� �������, Unix Time UTC
		Log_Time int not null,
		--������� ����� ����� � ������ ��������� �������
		Value numeric(18, 9) not null
	)
		
	create table dbo.H_SensorValueChange
	(	
		--����� ��� ���� ������������ ������ ������� 
		TRAIL_ID					int constraint FK_H_SensorValueChange_Trail_ID foreign key references dbo.Trail(Trail_ID),
		[ACTION]					nvarchar(6),
		ACTUAL_TIME					datetime not null,
		
		--��������� ����
		H_SensorValueChange_ID int identity (1,1)
			constraint PK_H_SensorValueChange
				primary key clustered,
		SensorValueChange_ID int not null,
		--������ �� ��������, ������������� �� ������� ����������
		Controller_ID int not null,
		--������ �� ������
		Controller_Sensor_Legend_ID int not null,
		--��������������� ����� ��������� �������� �������, Unix Time UTC
		Log_Time int not null,
		--������� ����� ����� � ������ ��������� �������
		Value numeric(18, 9) not null
	)

end;