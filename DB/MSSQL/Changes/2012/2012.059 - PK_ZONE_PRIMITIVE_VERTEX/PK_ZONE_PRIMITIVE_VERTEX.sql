if not exists (select * from sys.indexes where name = 'PK_ZONE_PRIMITIVE_VERTEX')
begin

	alter table ZONE_PRIMITIVE_VERTEX
		add constraint PK_ZONE_PRIMITIVE_VERTEX primary key (PRIMITIVE_VERTEX_ID)

end