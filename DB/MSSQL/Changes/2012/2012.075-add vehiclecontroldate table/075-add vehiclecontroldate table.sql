IF NOT EXISTS(SELECT * FROM dbo.sysobjects WHERE NAME='VehicleControlDate' and type='U')
BEGIN
	  CREATE TABLE [dbo].[VehicleControlDate]
	  (
			VehicleControlDate_ID INT IDENTITY(1,1) NOT NULL 
				  CONSTRAINT [PK_VehicleControlDate] 
						PRIMARY KEY CLUSTERED,
			[Type_ID] INT NOT NULL,
			Value datetime NOT NULL,
			Vehicle_ID int NOT NULL 
				  constraint FK_VehicleControlDate_VEHICLE 
						FOREIGN KEY references Vehicle([Vehicle_ID])
	  )

    CREATE TABLE [dbo].[H_VehicleControlDate]
    (
        [H_VehicleControlDate_ID] INT IDENTITY(1,1) NOT NULL
			CONSTRAINT [PK_H_VehicleControlDate] 
						PRIMARY KEY CLUSTERED,
          
        VehicleControlDate_ID INT NOT NULL, 
		[Type_ID] INT NOT NULL,
		Value datetime NOT NULL,
		Vehicle_ID int NOT NULL,

        TRAIL_ID        int 
			constraint FK_H_VehicleControlDate_Trail_ID 
				foreign key references dbo.Trail(Trail_ID),
        [ACTION]        nvarchar(6),
        ACTUAL_TIME     datetime not null
    )

END




