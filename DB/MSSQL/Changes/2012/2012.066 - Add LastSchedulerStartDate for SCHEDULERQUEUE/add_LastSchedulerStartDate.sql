if not exists (select * from sys.columns where object_id = object_id('dbo.SCHEDULERQUEUE') and name = 'LastSchedulerStartDate')
begin
	alter table dbo.SCHEDULERQUEUE
		add LastSchedulerStartDate datetime
	alter table dbo.H_SCHEDULERQUEUE
		add LastSchedulerStartDate datetime
end