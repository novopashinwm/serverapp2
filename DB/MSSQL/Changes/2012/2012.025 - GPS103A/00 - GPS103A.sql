/*��������� ��� ����������� GPS103A - ��� ��������
*/
begin tran

declare @controller_type_id int;

set @controller_type_id = (select Controller_Type_ID from dbo.Controller_Type where Type_Name = 'GPS103A')

if (@controller_type_id is null)
begin
	print 'Inserting new controller type: '
	insert into dbo.Controller_Type (TYPE_NAME, PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT)
		values ('GPS103A', 100, 0, 1)
	set @controller_type_id = @@identity;
end;

commit