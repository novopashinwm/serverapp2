IF NOT EXISTS(SELECT * FROM sys.table_types WHERE name = 'DateTimeRange')
BEGIN
	CREATE TYPE DateTimeRange AS TABLE
	(
		dateFrom INT NOT NULL,
		dateTo INT NOT NULL
	)
END