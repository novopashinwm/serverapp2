insert into dbo.[Rule] (Number, Description, Client_Side)
select t.Number, t.Description, 0
from (
	select Number = 15, Description = 'FuelReportVersion'
) t
where not exists (select 1 from dbo.[Rule] e where e.Number = t.Number)