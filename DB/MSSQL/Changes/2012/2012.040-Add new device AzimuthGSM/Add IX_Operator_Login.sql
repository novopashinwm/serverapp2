if not exists (select * from sys.indexes where name = 'IX_OPERATOR_LOGIN')
begin

	create nonclustered index IX_OPERATOR_LOGIN on dbo.OPERATOR([LOGIN]);
	
end