declare @controller_sensor_legend table (
	Controller_Sensor_Type_ID int,
	Name nvarchar(255),
	Number int,
	Value_Expired int
)

set nocount on

insert into @controller_sensor_legend
		  select Controller_Sensor_Type_ID = 1, Name = '���������� �������', Number = 1, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '������ �������', Number = 2, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '����������� � ������', Number = 3, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '���������', Number = 4, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '�������� ������ �����������', Number = 5, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������', Number = 6, Value_Expired = 120
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN.Fuel', Number = 7, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN.Total_Run', Number = 8, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'LastPublishingDurationMilliseconds', Number = 100, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'ErrorSource', Number = 101, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'ErrorCode', Number = 102, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'TimeToFirstGPSFix', Number = 103, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� �������', Number = 9, Value_Expired = 120
union all select Controller_Sensor_Type_ID = 2, Name = '�����������', Number = 10, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'BatteryLevel', Number = 11, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 1', Number = 21, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 2', Number = 22, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 3', Number = 23, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 4', Number = 24, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 5', Number = 25, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 6', Number = 26, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 7', Number = 27, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = '���������� ������ 8', Number = 28, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 1', Number = 31, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 2', Number = 32, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 3', Number = 33, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 4', Number = 34, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 5', Number = 35, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 6', Number = 36, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 7', Number = 37, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '�������� ������ 8', Number = 38, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = '������������', Number = 12, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_Speed', Number = 200, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_Accelerator', Number = 205, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelRate', Number = 206, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel1', Number = 207, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel2', Number = 208, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel3', Number = 209, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel4', Number = 210, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel5', Number = 211, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelLevel6', Number = 212, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_Revs', Number = 213, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_RunToCarMaintenance', Number = 214, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_EngHours', Number = 215, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_CoolantT', Number = 216, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_EngOilT', Number = 217, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_DayRun', Number = 220, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad1', Number = 221, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad2', Number = 222, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad3', Number = 223, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad4', Number = 224, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad5', Number = 225, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_AxleLoad6', Number = 226, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_FuelT', Number = 218, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_TotalRun', Number = 219, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'CAN_CruiseControl', Number = 201, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'CAN_Brake', Number = 202, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'CAN_ParkingBrake', Number = 203, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'CAN_Clutch', Number = 204, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'AccumulatorVoltage', Number = 13, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'VirtualOdometer', Number = 39, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_TotalFuelSpend', Number = 227, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CAN_BrakeSpecificFuelConsumption', Number = 228, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'GpsEnabled', Number = 14, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'IsPlugged', Number = 15, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'RadioScannerEnabled', Number = 16, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'DataSenderEnabled', Number = 17, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'ConsumedPower', Number = 40, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'Power', Number = 41, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'Current', Number = 42, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'CoverOpeningTimes', Number = 44, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 1, Name = 'HeartBeatInterval', Number = 45, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'RelayStatus', Number = 43, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'IsMoving', Number = 18, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'Door', Number = 46, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'HeadLights', Number = 47, Value_Expired = 86400
union all select Controller_Sensor_Type_ID = 2, Name = 'Accident', Number = 48, Value_Expired = 86400

set nocount off

insert into dbo.Controller_Sensor_Legend (Controller_Sensor_Type_ID, Name, Number, Value_Expired)
	select * 
		from @Controller_Sensor_Legend source
		where not exists (select 1 from dbo.Controller_Sensor_Legend target where target.Number = source.Number)
