if not exists (select * from sys.columns where object_id = object_id('dbo.SchedulerQueue') and name = 'Enabled')
begin

	alter table dbo.SchedulerQueue
		add Enabled bit not null default 1

	alter table dbo.H_SchedulerQueue
		add Enabled bit not null default 1

end;

if not exists (select * from sys.columns where object_id = object_id('dbo.SchedulerQueue') and name = 'ErrorCount')
begin

	alter table dbo.SchedulerQueue
		add ErrorCount int not null default 0

	alter table dbo.H_SchedulerQueue
		add ErrorCount int not null default 0

end;