if exists (select * from sys.types where name = 'Log_Time_Span_Param')
	drop type dbo.Log_Time_Span_Param
go	
create type dbo.Log_Time_Span_Param as table
(
	Log_Time_From int,
	Log_Time_To int
)
go