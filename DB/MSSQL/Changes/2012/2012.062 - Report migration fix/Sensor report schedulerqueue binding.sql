--� ������ ��������� ���������, �������� � ���� ��������� �������� ���������

declare @guid uniqueidentifier = '78288d6e-6ad2-40b5-be4c-abdcdc6579a9'
declare @name nvarchar(50) = '����� �� ���������� ��������'

declare @report_id int = (select report_id from report where report_guid = @guid)

update report	
	set report_filename = @name
	where Report_Id = @report_id
	  and report_filename <> @name
	
update SchedulerEvent 
	set Report_Id = @report_id
	where SchedulerEvent_id = (select SchedulerEvent_id from SchedulerEvent where comment = @name)
	  and Report_Id <> @report_id
