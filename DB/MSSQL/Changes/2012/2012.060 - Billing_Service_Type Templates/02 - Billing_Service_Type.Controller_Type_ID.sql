if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Billing_Service_Type') 
		  and name = 'Controller_Type_ID')
begin

	alter table Billing_Service_Type
		add Controller_Type_ID int 
			constraint FK_Billing_Service_Type_Controller_Type_ID
				foreign key references Controller_Type(Controller_Type_ID)
		
end
