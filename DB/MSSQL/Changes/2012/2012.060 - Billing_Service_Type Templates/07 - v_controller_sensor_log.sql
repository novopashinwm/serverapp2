alter view v_controller_sensor_log as    
 select  
  Vehicle_ID   = legend.Vehicle_ID  
  , SensorLegend = legend.Sensor_Legend
  , Log_Time     = csl_outer.Log_Time
  , Value = (
	  select     
		   sum (csm.Multiplier * input.Value + csm.Constant)    
	  from (
		select	distinct csm.Sensor_Number
			from v_controller_sensor_map csm
			where csm.Vehicle_ID = legend.Vehicle_ID
			  and csm.Sensor_Legend = legend.Sensor_Legend
	  ) used
	  cross apply (
		select top(1) csl.Value 
			from dbo.Controller_Sensor_Log csl with (nolock)
			where csl.Vehicle_ID = csl_outer.Vehicle_ID    
			  and csl.Number = used.Sensor_Number
			  and csl.Log_Time  <= csl_outer.Log_Time    
			order by csl.Log_Time desc
	  ) input
	  join dbo.v_controller_sensor_map csm (noexpand) on
				csm.Vehicle_ID = csl_outer.Vehicle_ID
			and csm.Sensor_Legend = legend.Sensor_Legend
			and csm.Sensor_Number = used.Sensor_Number
			and (csm.Min_Value is null or csm.Min_Value <= input.Value)    
			and (csm.Max_Value is null or csm.Max_Value >= input.Value)    
  )
  from v_controller_sensor_legend legend (noexpand)  
  join dbo.Log_Time csl_outer with (nolock)  
    on csl_outer.Vehicle_ID = legend.Vehicle_ID  
