if exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'Pattern_Vehicle_ID')
begin

	alter table Billing_Service_Type
		drop FK_Billing_Service_Type_Pattern_Vehicle_ID

	alter table Billing_Service_Type
		drop column Pattern_Vehicle_ID

end
go