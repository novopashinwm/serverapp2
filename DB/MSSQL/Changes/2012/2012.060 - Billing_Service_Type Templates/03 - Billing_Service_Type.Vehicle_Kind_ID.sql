if not exists (
	select * 
		from sys.columns 
		where object_id = object_id('Billing_Service_Type') 
		  and name = 'Vehicle_Kind_ID')
begin

	alter table Billing_Service_Type
		add Vehicle_Kind_ID int 
			constraint FK_Billing_Service_Type_Vehicle_Kind_ID 
				foreign key references Vehicle_Kind(Vehicle_Kind_ID)
		
end
