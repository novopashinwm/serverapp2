declare @controller_type_name nvarchar(50) = 'S60V3FP'

declare @default_sensors table
(
	Legend nvarchar(255),
	Input  nvarchar(255)
)

insert into @default_sensors values 
('BatteryLevel', 'BatteryLevel'),
('IsPlugged', 'IsPlugged')

update cs
	set Default_Sensor_Legend_ID = null,
		Default_Multiplier = null,
		Default_Constant = null,
		Mandatory = 0
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	where ct.Type_Name = @controller_type_name
	  and cs.Descript not in (select ds.Input from @default_sensors ds)
	  and (cs.Default_Sensor_Legend_ID is not null or 
	       cs.Default_Multiplier is not null or 
	       cs.Default_Constant is not null)
	  
update cs
	set Default_Sensor_Legend_ID = legend.Controller_Sensor_Legend_ID,
		Default_Multiplier = 1,
		Default_Constant = 0,
		Mandatory = 1
	from Controller_Sensor cs
	join Controller_Type ct on ct.Controller_Type_ID = cs.Controller_Type_ID
	join @default_sensors ds on ds.Input = cs.Descript
	join Controller_Sensor_Legend legend on legend.Name = ds.Legend
	where ct.Type_Name = @controller_type_name
	  and (cs.Default_Sensor_Legend_ID is null or 
		   cs.Default_Sensor_Legend_ID <> legend.Controller_Sensor_Legend_ID or
		   cs.Mandatory is null or cs.Mandatory <> 1)

