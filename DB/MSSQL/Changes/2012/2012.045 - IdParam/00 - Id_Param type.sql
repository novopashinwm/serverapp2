IF NOT EXISTS(SELECT * FROM sys.table_types WHERE name = 'Id_Param')
BEGIN
	CREATE TYPE Id_Param AS TABLE
	(
		Id  int NOT NULL
	)
END