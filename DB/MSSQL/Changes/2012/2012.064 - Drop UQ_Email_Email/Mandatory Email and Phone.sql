if not exists (select * from sys.columns where object_id = object_id('operator') and name = 'MandatoryEmail')
begin

	alter table Operator
		add MandatoryEmail bit not null default (0)

	alter table H_Operator
		add MandatoryEmail bit not null default (0)

end
go
if not exists (select * from sys.columns where object_id = object_id('operator') and name = 'MandatoryPhone')
begin

	alter table Operator
		add MandatoryPhone bit not null default (0)

	alter table H_Operator
		add MandatoryPhone bit not null default (0)

end
