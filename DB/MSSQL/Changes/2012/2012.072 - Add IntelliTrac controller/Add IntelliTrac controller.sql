DECLARE @controllerTypeId INT

IF (NOT EXISTS(SELECT * FROM dbo.CONTROLLER_TYPE WHERE [TYPE_NAME] = 'IntelliTrac'))
BEGIN
	INSERT INTO dbo.CONTROLLER_TYPE ([TYPE_NAME], PACKET_LENGTH, POS_ABSENCE_SUPPORT, POS_SMOOTH_SUPPORT, AllowedToAddByCustomer)
		VALUES('IntelliTrac', 1000, 0, 0, 1)
	SELECT @controllerTypeId = SCOPE_IDENTITY()
END
ELSE SELECT @controllerTypeId = CONTROLLER_TYPE_ID FROM dbo.CONTROLLER_TYPE WHERE [TYPE_NAME] = 'IntelliTrac'

IF NOT EXISTS (SELECT  * FROM CONTROLLER_SENSOR WHERE CONTROLLER_TYPE_ID = @controllerTypeId)
BEGIN
	INSERT INTO dbo.CONTROLLER_SENSOR (CONTROLLER_TYPE_ID, CONTROLLER_SENSOR_TYPE_ID, NUMBER, MAX_VALUE, 
			MIN_VALUE, DEFAULT_VALUE, BITS, Descript, Mandatory)
	VALUES
		(@controllerTypeId, 2, 0, 1, 0, 0, 1, 'Input 1', '0'),
		(@controllerTypeId, 2, 1, 1, 0, 0, 1, 'Input 2', '0'),
		(@controllerTypeId, 2, 2, 1, 0, 0, 1, 'Input 3', '0'),
		(@controllerTypeId, 2, 3, 1, 0, 0, 1, 'Input 4', '0'),
		(@controllerTypeId, 2, 4, 1, 0, 0, 1, 'Input 5', '0'),
		(@controllerTypeId, 2, 5, 1, 0, 0, 1, 'Input 6', '0'),
		(@controllerTypeId, 2, 6, 1, 0, 0, 1, 'Input 7', '0'),
		(@controllerTypeId, 2, 7, 1, 0, 0, 1, 'Input 8', '0'),
		
		(@controllerTypeId, 2, 8, 1, 0, 0, 1, 'Output 1', '0'),
		(@controllerTypeId, 2, 9, 1, 0, 0, 1, 'Output 2', '0'),
		(@controllerTypeId, 2, 10, 1, 0, 0, 1, 'Output 3', '0'),
		(@controllerTypeId, 2, 11, 1, 0, 0, 1, 'Output 4', '0'),
		(@controllerTypeId, 2, 12, 1, 0, 0, 1, 'Output 5', '0'),
		(@controllerTypeId, 2, 13, 1, 0, 0, 1, 'Output 6', '0'),
		(@controllerTypeId, 2, 14, 1, 0, 0, 1, 'Output 7', '0'),
		(@controllerTypeId, 2, 15, 1, 0, 0, 1, 'Output 8', '0'),
		
		(@controllerTypeId, 1, 16, 1000, 0, 0, 16, 'AnalogInput 9', '0'),
		(@controllerTypeId, 1, 17, 1000, 0, 0, 16, 'AnalogInput 10', '0')
END
