insert into dbo.[Right] (Right_ID, [System], Name, Description)
	select 20, 1, 'ViewControllerPhone', 'Allows to view phone number of SIM inserted in device'
	where not exists (select 1 from dbo.[Right] where Right_ID = 20)