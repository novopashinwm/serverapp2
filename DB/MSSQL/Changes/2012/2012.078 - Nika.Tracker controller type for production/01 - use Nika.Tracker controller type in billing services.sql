update bst
	set Controller_Type_ID = (select newct.Controller_Type_ID from Controller_Type newct where newct.Type_Name = 'Nika.Tracker')
	from Billing_Service_Type bst
	left outer join Controller_Type oldct on oldct.Controller_Type_ID = bst.Controller_Type_ID
	where (oldct.Type_Name = 'S60V3FP' or bst.Service_Type_Category = 'NikaTracker' and oldct.CONTROLLER_TYPE_ID is null)