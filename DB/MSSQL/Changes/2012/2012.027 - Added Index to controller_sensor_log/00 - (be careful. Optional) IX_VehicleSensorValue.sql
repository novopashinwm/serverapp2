IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Controller_Sensor_Log]') AND name = N'IX_VehicleSensorValue')
DROP INDEX [IX_VehicleSensorValue] ON [dbo].[Controller_Sensor_Log] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_VehicleSensorValue] ON [dbo].[Controller_Sensor_Log] 
(
	[Vehicle_ID] ASC,
	[Number] ASC,
	[Log_Time] ASC
)
INCLUDE ( [Value]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
ON [TSS_DATA2]
GO