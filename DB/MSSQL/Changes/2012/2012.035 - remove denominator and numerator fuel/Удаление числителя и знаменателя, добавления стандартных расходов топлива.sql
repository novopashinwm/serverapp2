-- add standard fuel consumption (kilometer), standard fuel consumption (liter)
IF NOT EXISTS 
(
	SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'H_VEHICLE'  
			AND syscolumns.name = 'FuelSpendStandardKilometer'
)
BEGIN 
	exec('ALTER TABLE  dbo.H_VEHICLE ADD FuelSpendStandardKilometer INT NULL')
END

IF NOT EXISTS 
(
	SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'H_VEHICLE'  
			AND syscolumns.name = 'FuelSpendStandardLiter'
)
BEGIN 
	exec('ALTER TABLE  dbo.H_VEHICLE ADD FuelSpendStandardLiter INT NULL')
END


IF NOT EXISTS 
(
	SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'VEHICLE'  
			AND syscolumns.name = 'FuelSpendStandardKilometer'
)
BEGIN 
	exec('ALTER TABLE  dbo.VEHICLE ADD FuelSpendStandardKilometer INT NULL')
END

IF NOT EXISTS 
(
	SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'VEHICLE'  
			AND syscolumns.name = 'FuelSpendStandardLiter'
)
BEGIN 
	exec('ALTER TABLE  dbo.VEHICLE ADD FuelSpendStandardLiter INT NULL')
END

IF (
	EXISTS (
		SELECT syscolumns.name
				FROM sysobjects 
				JOIN syscolumns ON sysobjects.id = syscolumns.id
			WHERE sysobjects.xtype='U' 
				AND sysobjects.name = 'VEHICLE'  
				AND syscolumns.name = 'DENOMINATOR_FUEL') AND
	EXISTS (
		SELECT syscolumns.name
				FROM sysobjects 
				JOIN syscolumns ON sysobjects.id = syscolumns.id
			WHERE sysobjects.xtype='U' 
				AND sysobjects.name = 'VEHICLE'  
				AND syscolumns.name = 'DENOMINATOR_FUEL')
	)
BEGIN
	--������ ��� ������
	exec(';with cte as
	(
		SELECT v.*, c.name FROM dbo.VEHICLE v
			LEFT JOIN dbo.DEPARTMENT d ON v.DEPARTMENT = d.DEPARTMENT_ID
			LEFT JOIN dbo.Country c ON c.Country_ID = d.Country_ID
		WHERE c.Name <> ''India'' OR c.Name IS NULL
	)
	UPDATE cte SET FuelSpendStandardLiter = CASE ISNULL(NUMERATOR_FUEL, 0) WHEN 0 THEN NULL ELSE NUMERATOR_FUEL END,
				   FuelSpendStandardKilometer = CASE ISNULL(NUMERATOR_FUEL, 0) WHEN 0 THEN NULL ELSE 10000 END');
	--������ ��� �����
	exec(';with cte as
	(
		SELECT v.* FROM dbo.VEHICLE v
			LEFT JOIN dbo.DEPARTMENT d ON v.DEPARTMENT = d.DEPARTMENT_ID
			LEFT JOIN dbo.Country c ON c.Country_ID = d.Country_ID
		WHERE c.Name = ''India''
	)
	UPDATE cte SET FuelSpendStandardKilometer = CASE ISNULL(NUMERATOR_FUEL, 0) WHEN 0 THEN NULL ELSE NUMERATOR_FUEL END,
				   FuelSpendStandardLiter = CASE ISNULL(NUMERATOR_FUEL,0) WHEN 0 THEN NULL ELSE 1 END')
END
--DENOMINATOR_FUEL
IF EXISTS(
	SELECT OBJECT_NAME(OBJECT_ID) AS NameofConstraint
			FROM sys.objects
				WHERE OBJECT_NAME(OBJECT_ID) = 'DF_vehicle_DENOMINATOR_FUEL')
BEGIN
	exec('ALTER TABLE dbo.VEHICLE
		DROP CONSTRAINT DF_vehicle_DENOMINATOR_FUEL')
END

IF EXISTS(
		SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'VEHICLE'  
			AND syscolumns.name = 'DENOMINATOR_FUEL'
)
BEGIN 
	exec('ALTER TABLE dbo.VEHICLE
		DROP COLUMN DENOMINATOR_FUEL')
END


IF EXISTS(
		SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'H_VEHICLE'  
			AND syscolumns.name = 'DENOMINATOR_FUEL'
)
BEGIN 
	exec ('ALTER TABLE dbo.H_VEHICLE
		DROP COLUMN DENOMINATOR_FUEL')
END


--NUMERATOR_FUEL
IF EXISTS(
		SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'VEHICLE'  
			AND syscolumns.name = 'NUMERATOR_FUEL'
)
BEGIN 
	exec('ALTER TABLE dbo.VEHICLE
		DROP COLUMN NUMERATOR_FUEL')
END


IF EXISTS(
		SELECT syscolumns.name
			FROM sysobjects 
			JOIN syscolumns ON sysobjects.id = syscolumns.id
		WHERE sysobjects.xtype='U' 
			AND sysobjects.name = 'H_VEHICLE'  
			AND syscolumns.name = 'NUMERATOR_FUEL'
)
BEGIN 
	exec ('ALTER TABLE dbo.H_VEHICLE
		DROP COLUMN NUMERATOR_FUEL')
END




	


