if not exists (select * from sys.columns where object_id = object_id('dbo.Controller_Type') and name = 'AllowedToAddByCustomer')
begin
	alter table dbo.Controller_Type
		add AllowedToAddByCustomer bit
	
	alter table dbo.H_Controller_Type
		add AllowedToAddByCustomer bit
end
go
update dbo.Controller_Type
	set AllowedToAddByCustomer = 0
