if exists (select * from sys.objects where name = 'CK_OPERATORGROUP_DEPARTMENT_RIGHT' and type = 'c')
	alter table OPERATORGROUP_DEPARTMENT drop constraint CK_OPERATORGROUP_DEPARTMENT_RIGHT

alter table OPERATORGROUP_DEPARTMENT 
	add constraint CK_OPERATORGROUP_DEPARTMENT_RIGHT
		check ([RIGHT_ID] in (2, 104))