if not exists (select * from sys.columns where object_id = object_id('Billing_Service_Type') and name = 'MaxQuantity')
begin

	alter table Billing_Service_Type
		add 
			MaxQuantity	int,
			MinIntervalSeconds int

end
