insert into Billing_Service_Type (Service_Type, Service_Type_Category, MinIntervalSeconds, MaxQuantity)
	select 
		t.*
		from (
			select           Service_Type = '',           	Service_Type_Category = '', MinIntervalSeconds = 0, MaxQuantity = 0 where 1=0
			union all select 'FRNIKA.MLP.ChargingEconom', 	'LBS',						null,					null
			union all select 'FRNIKA.SMS.ChargingEconom', 	'SMS',						null,					null
			union all select 'FRNIKA.MLP.ChargingNolimits',	'LBS',						20*60,					null
			union all select 'FRNIKA.SMS.M2M',				'SMS',						null,					null
			union all select 'FRNIKA.SMS.LBS',				'SMS',						null,					null
			union all select 'INDIA.SMS.Unlim',				'SMS',						null,					null
			union all select 'Free.SMS.Confirmation',		'SMS',						60*60,					3
		) t
		where not exists (select 1 from Billing_Service_Type e where e.Service_Type = t.Service_Type)

--��� ����� ��������� ���������� SMS ��� �����������	
insert into Billing_Service (Billing_Service_Type_ID, Department_ID, StartDate)
	select bst.ID, d.Department_ID, getutcdate()
		from Billing_Service_Type bst, Department d
		where d.Country_ID = (select c.Country_ID from Country c where Name = 'India')
		  and bst.Service_Type = 'INDIA.SMS.Unlim'
		and not exists (select * from Billing_Service e where e.Department_ID = d.Department_ID and e.Billing_Service_Type_ID = bst.ID)
		
		