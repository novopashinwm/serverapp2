insert into Message_Template_Field (DotNet_Type_ID, Name)
	select 		
		t.DotNet_Type_ID, 'VehicleID'
	from dotnet_type t
	where t.Type_Name = 'System.Int32'
	  and not exists (select * from Message_Template_Field mtf where mtf.Name = 'VehicleID' and Message_Template_ID is null)	