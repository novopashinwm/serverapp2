if exists (select * from sys.indexes where name = 'IX_Billing_Service_Asid_ID')
begin
	drop index IX_Billing_Service_Asid_ID on Billing_Service
end
go
if not exists (select * from sys.indexes where name = 'IX_Billing_Service_ID')
begin
	create clustered index IX_Billing_Service_ID on Billing_Service(ID)
end