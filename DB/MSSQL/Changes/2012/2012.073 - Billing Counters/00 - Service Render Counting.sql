if not exists (select * from sys.columns where object_id = object_id('Billing_Service') and name = 'Department_ID')
begin
				
	alter table Billing_Service
		add 
			Department_ID int
				constraint FK_Billing_Service_Department_ID
					foreign key references Department(Department_ID),				
			Operator_ID int
				constraint FK_Billing_Service_Operator_ID 
					foreign key references Operator(Operator_ID),
			Vehicle_ID int 
				constraint FK_Billing_Service_Vehicle_ID
					foreign key references Vehicle(Vehicle_ID),
			MaxQuantity			int,
			RenderedQuantity	int,
			RenderedLastTime	datetime,
			constraint CK_Billing_Service check (
					Department_ID is not null and Operator_ID is null		and Vehicle_ID is null		and Asid_ID is null
				or  Department_ID is null     and Operator_ID is not null	and Vehicle_ID is null		and Asid_ID is null
				or  Department_ID is null     and Operator_ID is null	    and Vehicle_ID is not null	and Asid_ID is null
				or  Department_ID is null     and Operator_ID is null	    and Vehicle_ID is null		and Asid_ID is not null
			),
			constraint UQ_Billing_Service unique(Billing_Service_Type_ID, Department_ID, Operator_ID, Vehicle_ID, Asid_ID)
	
	
	alter table H_Billing_Service
		add 
			Department_ID int,
			Operator_ID int,
			Vehicle_ID int,
			MaxQuantity int,
			RenderedQuantity	int,
			RenderedLastTime	datetime

	alter table Billing_Service
		alter column Asid_ID int null

	alter table H_Billing_Service
		alter column Asid_ID int null
		
	alter table Billing_Service
		add MinIntervalSeconds int

	alter table H_Billing_Service
		add MinIntervalSeconds int
		
end