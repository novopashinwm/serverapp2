insert into Message_Template (Name)
	select t.name
		from (select Name = 'GenericConfirmationSms') t
		where not exists (
			select * 
				from Message_Template e
				where e.Name = t.Name)
