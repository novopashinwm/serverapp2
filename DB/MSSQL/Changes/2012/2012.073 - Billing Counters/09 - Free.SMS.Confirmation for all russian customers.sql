insert into billing_service (Billing_Service_Type_ID, StartDate, Department_ID)
	select bst.ID, getutcdate(), d.Department_ID
		from billing_service_type bst,
		     Department d
		where bst.Service_Type = 'Free.SMS.Confirmation'
		  and not exists (select * from billing_service e where e.department_id = d.department_id 
		                                                and e.Billing_Service_Type_ID = bst.ID)
		  and (d.country_id is null or 
		       d.country_id = (select c.Country_ID from Country c where c.SmsDestinationType = 2))
    		      
