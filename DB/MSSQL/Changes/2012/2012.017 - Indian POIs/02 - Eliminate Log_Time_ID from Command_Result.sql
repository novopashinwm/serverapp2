if not exists (select * from sys.columns where object_id = object_id('dbo.Command_Result') and name = 'Log_Time')
begin
	alter table dbo.Command_Result
		add Log_Time int
end

if exists (select * from sys.columns where object_id = object_id('dbo.Command_Result') and name = 'Log_Time_ID')
begin
	
	--��� ���������� ������ ���������� ������� ������������ Dynamic SQL
	exec sp_executesql N'
	update cr
		set Log_Time = lt.Log_Time
		from dbo.Command_Result cr
		join dbo.Log_Time lt on lt.ID = cr.Log_Time_ID
	'
	
	alter table dbo.Command_Result
		drop column Log_Time_ID
end

