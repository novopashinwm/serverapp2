if not exists (select * from sys.columns where object_id = object_id('dbo.Map_Vertex') and name = 'Geo')
begin
	alter table dbo.Map_Vertex
		add Geo geography
	alter table dbo.H_Map_Vertex
		add Geo geography
	
	create spatial index IX_Map_Vertex_Geo
		on dbo.Map_Vertex(Geo)
end