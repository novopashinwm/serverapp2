if not exists (select * from sys.columns where object_id = object_id('dbo.Command_Result') and name = 'ErrorCount')
begin
	alter table dbo.Command_Result
		add ErrorCount int not null default (0)
end
