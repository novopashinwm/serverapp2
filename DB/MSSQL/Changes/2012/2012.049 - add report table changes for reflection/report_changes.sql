if (not exists(select 1 from sys.columns where Name = N'Name' and Object_ID = Object_ID(N'report')))
	alter table report
	add Name nvarchar(255)
	
if (not exists(select 1 from sys.columns where Name = N'GroupName' and Object_ID = Object_ID(N'report')))
	alter table report
	add GroupName nvarchar(255)

if (not exists(select 1 from sys.columns where Name = N'AssemblyName' and Object_ID = Object_ID(N'report')))
	alter table report
	add AssemblyName nvarchar(255)
	
if (not exists(select 1 from sys.columns where Name = N'ClassParametersName' and Object_ID = Object_ID(N'report')))
	alter table report
	add ClassParametersName nvarchar(255)

if (not exists(select 1 from sys.columns where Name = N'ClassResourcesName' and Object_ID = Object_ID(N'report')))
	alter table report
	add ClassResourcesName nvarchar(255)
	
if (not exists(select 1 from sys.columns where Name = N'BackLinkReportId' and Object_ID = Object_ID(N'report')))
	alter table report
	add BackLinkReportId int
	

