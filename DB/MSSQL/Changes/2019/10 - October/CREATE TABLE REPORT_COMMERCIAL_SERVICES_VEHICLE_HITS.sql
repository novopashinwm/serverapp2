IF NOT EXISTS(SELECT name FROM sys.sysobjects WHERE Name = N'REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS' AND xtype = N'U')
BEGIN
    CREATE TABLE [dbo].[REPORT_COMMERCIAL_SERVICES_VEHICLE_HITS](
		[RPT_COMMSERVICE_ROW_ID] [int] IDENTITY(1,1) NOT NULL,
		[VEHICLE_ID] [int] NOT NULL,
		[DATE_FROM] [datetime] NOT NULL,
		[DATE_TO] [datetime] NOT NULL,
		[HITS] [int] NOT NULL
	) ON [PRIMARY]
END

