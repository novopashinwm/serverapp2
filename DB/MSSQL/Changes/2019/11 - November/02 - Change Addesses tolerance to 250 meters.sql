﻿BEGIN TRY
	BEGIN TRAN
	------------------------------------------------------------------
	DROP INDEX [IX_Address_LangMapLatLngCells] ON [dbo].[Address];
	------------------------------------------------------------------
	ALTER TABLE [dbo].[Address] DROP COLUMN [LatCell];
	ALTER TABLE [dbo].[Address] ADD         [LatCell] AS ([dbo].[GetLatCellTolerance]([Lat],250)) PERSISTED;
	------------------------------------------------------------------
	ALTER TABLE [dbo].[Address] DROP COLUMN [LngCell];
	ALTER TABLE [dbo].[Address] ADD         [LngCell] AS ([dbo].[GetLngCellTolerance]([Lng],250)) PERSISTED;
	------------------------------------------------------------------
	CREATE NONCLUSTERED INDEX [IX_Address_LangMapLatLngCells] ON [dbo].[Address]
	(
		[Language],
		[MapId],
		[LatCell],
		[LngCell]
	)
	INCLUDE([Value], [Lat], [Lng])
	ON [PRIMARY];
	------------------------------------------------------------------
	COMMIT TRAN
	--ROLLBACK TRAN
END TRY
BEGIN CATCH
	SELECT
		 ERROR_NUMBER()    AS [ErrorNumber]
		,ERROR_SEVERITY()  AS [ErrorSeverity]
		,ERROR_STATE()     AS [ErrorState]
		,ERROR_PROCEDURE() AS [ErrorProcedure]
		,ERROR_LINE()      AS [ErrorLine]
		,ERROR_MESSAGE()   AS [ErrorMessage];
	ROLLBACK TRAN
END CATCH
GO