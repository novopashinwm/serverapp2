﻿IF (OBJECT_ID(N'[dbo].[Log_Time]', N'U') IS NOT NULL            AND OBJECT_ID(N'[dbo].[DF_Log_Time_InsertTime]', 'D') IS NULL)
	ALTER TABLE [dbo].[Log_Time]            ADD CONSTRAINT [DF_Log_Time_InsertTime]            DEFAULT GETUTCDATE() FOR [InsertTime];
IF (OBJECT_ID(N'[dbo].[Log_Time#Stage]', N'U') IS NOT NULL      AND OBJECT_ID(N'[dbo].[DF_Log_Time#Stage_InsertTime]', 'D') IS NULL)
	ALTER TABLE [dbo].[Log_Time#Stage]      ADD CONSTRAINT [DF_Log_Time#Stage_InsertTime]      DEFAULT GETUTCDATE() FOR [InsertTime];
IF (OBJECT_ID(N'[dbo].[Log_Time#Marks]', N'U') IS NOT NULL      AND OBJECT_ID(N'[dbo].[DF_Log_Time#Marks_InsertTime]', 'D') IS NULL)
	ALTER TABLE [dbo].[Log_Time#Marks]      ADD CONSTRAINT [DF_Log_Time#Marks_InsertTime]      DEFAULT GETUTCDATE() FOR [InsertTime];
IF (OBJECT_ID(N'[dbo].[Log_Time#Partitions]', N'U') IS NOT NULL AND OBJECT_ID(N'[dbo].[DF_Log_Time#Partitions_InsertTime]', 'D') IS NULL)
	ALTER TABLE [dbo].[Log_Time#Partitions] ADD CONSTRAINT [DF_Log_Time#Partitions_InsertTime] DEFAULT GETUTCDATE() FOR [InsertTime];