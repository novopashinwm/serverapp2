﻿IF (NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'LogsPartitionFunction'))
BEGIN
	DECLARE
		@date0 datetime = DATEADD(second, -0, DATEADD(year, DATEDIFF(year, 0, GETUTCDATE()) - 1, 0)),
		@date1 datetime = DATEADD(second, -1, DATEADD(year, DATEDIFF(year, 0, GETUTCDATE()) + 1, 0)),
		@stepN int      = 5;
	DECLARE @boundaries nvarchar(max) = N'';
	WITH
		Nbrs_1( n ) AS ( SELECT 1 UNION SELECT 0                      ), --2^1
		Nbrs_2( n ) AS ( SELECT 1 FROM Nbrs_1 n1 CROSS JOIN Nbrs_1 n2 ), --2^2
		Nbrs_3( n ) AS ( SELECT 1 FROM Nbrs_2 n1 CROSS JOIN Nbrs_2 n2 ), --2^4
		Nbrs_4( n ) AS ( SELECT 1 FROM Nbrs_3 n1 CROSS JOIN Nbrs_3 n2 ), --2^8
		Nbrs_5( n ) AS ( SELECT 1 FROM Nbrs_4 n1 CROSS JOIN Nbrs_4 n2 ), --2^16 ~ 18часов в секундах
		Nbrs_6( n ) AS ( SELECT 1 FROM Nbrs_5 n1 CROSS JOIN Nbrs_5 n2 ), --2^32 ~136лет   в секундах
		Nbrs  ( n ) AS ( SELECT ROW_NUMBER() OVER (ORDER BY n) - 1 FROM Nbrs_6 )
	SELECT @boundaries = STUFF((SELECT TOP(DATEDIFF(day, @date0, @date1) / @stepN + 1) ',' + CAST([dbo].[utc2lt](DATEADD(day, n * @stepN, @date0)) AS nvarchar(max)) FROM Nbrs FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 1, N'')
	EXEC(N'CREATE PARTITION FUNCTION [LogsPartitionFunction] (int) AS RANGE RIGHT FOR VALUES (' + @boundaries + ');');
END
GO
IF (NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'LogsPartitionScheme'))
BEGIN
	CREATE PARTITION SCHEME [LogsPartitionScheme]
		AS PARTITION [LogsPartitionFunction]
	ALL TO ([TSS_DATA_LOGS]);
END
GO