﻿-- [dbo].[Controller_Sensor_Log_New]
IF (OBJECT_ID(N'[dbo].[Move_Data_To_Controller_Sensor_Log_New]', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[Move_Data_To_Controller_Sensor_Log_New]
END
GO

IF (OBJECT_ID(N'[dbo].[Controller_Sensor_Log_New]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Controller_Sensor_Log_New]
END
GO

IF (OBJECT_ID(N'[dbo].[test_t]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[test_t]
END
GO

IF (OBJECT_ID(N'[dbo].[GetVehiclesWithPositions_test]', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[GetVehiclesWithPositions_test]
END
GO

IF (OBJECT_ID(N'[dbo].[Test]', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[Test]
END
GO

IF (OBJECT_ID(N'[dbo].[test_view]', 'V') IS NOT NULL)
BEGIN
	DROP VIEW [dbo].[test_view]
END
GO

IF (OBJECT_ID(N'[dbo].[Копия 2014 2Q не подтверждено (2)]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Копия 2014 2Q не подтверждено (2)]
END
GO

IF (OBJECT_ID(N'[dbo].[AddMonitoreeLogIgnored]', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE [dbo].[AddMonitoreeLogIgnored]
END
GO

IF (OBJECT_ID(N'[dbo].[temp_not_exists_vehicle_ids]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[temp_not_exists_vehicle_ids]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_n111ika_201402_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_n111ika_201402_2]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_nika_201402_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_nika_201402_2]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_nika_201403_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_nika_201403_2]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_nika_201404_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_nika_201404_2]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_nika_201405_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_nika_201405_2]
END
GO

IF (OBJECT_ID(N'[dbo].[itg47434_nika_201406_new]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[itg47434_nika_201406_new]
END
GO

IF (OBJECT_ID(N'[dbo].[Service_2014_04_1]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Service_2014_04_1]
END
GO

IF (OBJECT_ID(N'[dbo].[Service_2014_04_2]', 'U') IS NOT NULL)
BEGIN
	DROP TABLE [dbo].[Service_2014_04_2]
END
GO