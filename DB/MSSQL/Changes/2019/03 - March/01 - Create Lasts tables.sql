﻿----------------------------------------------------------------------------------------------------------------
-- Cell_Network_Log#Lasts
----------------------------------------------------------------------------------------------------------------
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Cell_Network_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Cell_Network_Log#Lasts]
	(
		[Vehicle_ID]      int     NOT NULL,
		[Number]          tinyint NOT NULL,
		[Log_Time#Last]   int     NOT NULL,
			CONSTRAINT [PK_Cell_Network_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID],
				[Number]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Picture_Log#Lasts
----------------------------------------------------------------------------------------------------------------
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Picture_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Picture_Log#Lasts]
	(
		[Vehicle_ID]    int NOT NULL,
		[Camera_Number] int NOT NULL
			CONSTRAINT [DF_Picture_Log#Lasts_Camera_Number] DEFAULT (0),
		[Photo_Number]  int NOT NULL
			CONSTRAINT [DF_Picture_Log#Lasts_Photo_Number]  DEFAULT (0),
		[Log_Time#Last] int NOT NULL,
			CONSTRAINT [PK_Picture_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID],
				[Camera_Number],
				[Photo_Number]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Geo_Log_Ignored
----------------------------------------------------------------------------------------------------------------
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Geo_Log_Ignored#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Geo_Log_Ignored#Lasts]
	(
		[Vehicle_ID]      int      NOT NULL,
		[Reason]          tinyint  NOT NULL,
		[Log_Time#Last]   int      NOT NULL,
		[InsertTime#Last] datetime NOT NULL CONSTRAINT [DF_Geo_Log_Ignored#Lasts]  DEFAULT (GETUTCDATE()),
			CONSTRAINT [PK_Geo_Log_Ignored#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID],
				[Reason]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- State_Log
----------------------------------------------------------------------------------------------------------------
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[State_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[State_Log#Lasts]
	(
		[Vehicle_ID]    int     NOT NULL,
		[Type]          tinyint NOT NULL,
		[Log_Time#Last] int     NOT NULL,
			CONSTRAINT [PK_State_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID],
				[Type]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Statistic_Log
----------------------------------------------------------------------------------------------------------------
IF (0 = 1/*Пока не нужна*/ AND OBJECT_ID(N'[dbo].[Statistic_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Statistic_Log#Lasts]
	(
		[Vehicle_ID]          int    NOT NULL,
		[Log_Time#Last]       int    NOT NULL,
		[Odometer#Last]       bigint NOT NULL,
		[IgnitionOnTime#Last] int    NOT NULL,
			CONSTRAINT [PK_Statistic_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Geo_Log
----------------------------------------------------------------------------------------------------------------
IF (OBJECT_ID(N'[dbo].[Geo_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Geo_Log#Lasts]
	(
		[Vehicle_ID]    int           NOT NULL,
		[LOG_TIME#Last] int           NOT NULL,
		[LOG_TIME#Prev] int               NULL,
		[Lng#Last]      numeric(8, 5)     NULL,
		[Lng#Prev]      numeric(8, 5)     NULL,
		[Lat#Last]      numeric(8, 5)     NULL,
		[Lat#Prev]      numeric(8, 5)     NULL,
			CONSTRAINT [PK_Geo_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- GPS_Log
----------------------------------------------------------------------------------------------------------------
IF (OBJECT_ID(N'[dbo].[GPS_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[GPS_Log#Lasts]
	(
		[Vehicle_ID]    int NOT NULL,
		[Log_Time#Last] int NOT NULL,
			CONSTRAINT [PK_GPS_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Log_Time
----------------------------------------------------------------------------------------------------------------
IF (OBJECT_ID(N'[dbo].[Log_Time#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Log_Time#Lasts]
	(
		[Vehicle_ID]      int      NOT NULL,
		[Log_Time#Last]   int      NOT NULL,
		[InsertTime#Last] datetime     NULL,
			CONSTRAINT [PK_Log_Time#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO
----------------------------------------------------------------------------------------------------------------
-- Controller_Sensor_Log
----------------------------------------------------------------------------------------------------------------
IF (OBJECT_ID(N'[dbo].[Controller_Sensor_Log#Lasts]', N'U') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Controller_Sensor_Log#Lasts]
	(
		[Vehicle_ID]    int    NOT NULL,
		[Number]        int    NOT NULL,
		[Log_Time#Last] int    NOT NULL,
			CONSTRAINT [PK_Controller_Sensor_Log#Lasts] PRIMARY KEY CLUSTERED
			(
				[Vehicle_ID],
				[Number]
			) ON [TSS_DATA_LOGS]
	) ON [TSS_DATA_LOGS];
END
GO