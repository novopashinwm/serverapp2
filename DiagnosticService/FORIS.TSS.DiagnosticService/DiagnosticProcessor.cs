﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.Terminal;
using FORIS.TSS.WorkplaceShadow.Terminal;
using FORIS.TSS.WorkplaceSharnier;
using Timer = System.Threading.Timer;

namespace FORIS.TSS.DiagnosticService
{
	public class DiagnosticProcessor
	{
		private readonly string[]     _recipients;
		private DateTime              _lastSmsSendTime = DateTime.Now.AddDays(-1);
		private readonly int          _minSmsIntervalMinutes;
		private readonly string       _dbConnString;
		private readonly string       _webAppUrl;
		private readonly int          _webAppTimeOut;
		private readonly int          _webAppTimeOutThreshold;
		private readonly string       _webAppPageShouldContainsString;
		private readonly string       _messageChannel;
		private readonly bool         _checkSite;
		private readonly string[]     _terminalDeviceMedias;
		private readonly bool         _checkTerminalSaveQueueCount;
		private readonly int          _checkTerminalSaveQueueCountThreshold;
		private readonly bool         _checkDiskFreeSpace;
		private readonly string[]     _diskNames;
		private readonly decimal      _diskFreeSpaceThresholdGb;
		private readonly bool         _checkMessages;
		private readonly string       _terminalServiceUrl;
		private readonly TimeSpan     _checkInterval;
		private TerminalClient        _terminalClient;

		private class Message
		{
			public string[] Recipients;
			public string Subject;
			public string Body;
		}

		private readonly List<Message> _messages = new List<Message>();

		public DiagnosticProcessor()
		{
			Trace.TraceInformation($@"{nameof(DiagnosticProcessor)} constructor started");

			var checkInterval = ConfigurationManager.AppSettings["CheckInterval"];
			try
			{
				_checkInterval = XmlConvert.ToTimeSpan(checkInterval);
			}
			catch (FormatException)
			{
				_checkInterval = TimeSpan.FromMinutes(1);
			}

			if (!int.TryParse(ConfigurationManager.AppSettings["MinSMSIntervalMinutes"], out _minSmsIntervalMinutes))
				_minSmsIntervalMinutes = 30;

			_checkSite          = ConfigurationManager.AppSettings["checkSite"]          == "1";
			_checkDiskFreeSpace = ConfigurationManager.AppSettings["checkDiskFreeSpace"] == "1";
			_checkMessages      = ConfigurationManager.AppSettings["checkMessages"]      == "1";

			var terminalMediaString = ConfigurationManager.AppSettings["TerminalDeviceMedias"];
			if (terminalMediaString != null)
				_terminalDeviceMedias = terminalMediaString.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

			_checkTerminalSaveQueueCount          = ConfigurationManager.AppSettings["checkTerminalSaveQueueCount"]          == "1";
			_checkTerminalSaveQueueCountThreshold = int.TryParse(ConfigurationManager.AppSettings["checkTerminalSaveQueueCountThreshold"], out _checkTerminalSaveQueueCountThreshold)
				? _checkTerminalSaveQueueCountThreshold
				: 5000;

			if (_checkDiskFreeSpace)
			{
				var thresholdString = ConfigurationManager.AppSettings["checkDiskFreeSpaceThresholdGb"];
				if (!decimal.TryParse(thresholdString, out _diskFreeSpaceThresholdGb))
					_diskFreeSpaceThresholdGb = 5;
				_diskNames = (ConfigurationManager.AppSettings["diskName"] ?? "c:\\").Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
			}

			_checkRuleProcessorDelay = ConfigurationManager.AppSettings["checkRuleProcessorDelay"] == "1";

			_terminalServiceUrl = ConfigurationManager.AppSettings["TerminalService"];

			_dbConnString = ConfigurationManager.AppSettings["dbConnString"];
			_webAppUrl = ConfigurationManager.AppSettings["webAppUrl"];
			_webAppPageShouldContainsString = ConfigurationManager.AppSettings["webAppPageShouldContainsString"];
			_messageChannel = ConfigurationManager.AppSettings["MessageChannel"];
			int timeOut;
			_webAppTimeOut = int.TryParse(ConfigurationManager.AppSettings["WebAppTimeOut"], out timeOut) ? timeOut : 50000;
			int timeOutThreshold;
			_webAppTimeOutThreshold = int.TryParse(ConfigurationManager.AppSettings["WebAppTimeOutThreshold"], out timeOutThreshold) ? timeOutThreshold : 10000;

			var schedulerQueueTimeoutMinutesString = ConfigurationManager.AppSettings["SchedulerQueueTimeoutMinutes"];
			int schedulerQueueTimeoutMinutes;
			if (int.TryParse(schedulerQueueTimeoutMinutesString, out schedulerQueueTimeoutMinutes))
				_schedulerQueueTimeoutMinutes = schedulerQueueTimeoutMinutes;

			//Список получателей
			string strRecipients = ConfigurationManager.AppSettings["Recipients"];
			_recipients = strRecipients.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

			Trace.TraceInformation($@"{nameof(DiagnosticProcessor)} constructor finished");
		}

		public void Start()
		{
			Trace.TraceInformation($@"{nameof(DiagnosticProcessor)}.{nameof(DiagnosticProcessor.Start)}");
			try
			{
				SendEmail("Diagnostic server starting", "Diagnostic server starting", _recipients);
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to send email: {0}", ex);
			}
			using (new Stopwatcher($@"{nameof(DiagnosticProcessor)}.{nameof(DiagnosticProcessor.CheckSystem)}", null, TimeSpan.Zero))
				CheckSystem();
			_workTimer = new Timer(workTimer_Elapsed, null, _checkInterval, TimeSpan.FromMilliseconds(-1));
		}
		/// <summary> На завершение работы сервиса </summary>
		public void Stop()
		{
			Trace.TraceInformation($@"{nameof(DiagnosticProcessor)}.{nameof(DiagnosticProcessor.Stop)}");
			((IClient)_terminalClient).Stop();
			_terminalClient.Dispose();
			_terminalClient = null;
		}

		public void CheckSystem()
		{
			var prevTroubles = new HashSet<string>(_lastTroubleSubject.Select(pair => pair.Key));
			_messages.Clear();

			try
			{
				// Проверка наличия соединения к службе терминалов
				if (!string.IsNullOrWhiteSpace(_terminalServiceUrl))
					CheckTerminalServiceRemoting();
				// Проверка наличия очереди записи в БД
				if (_checkTerminalSaveQueueCount)
					CheckTerminalSaveQueue(_checkTerminalSaveQueueCountThreshold);
				// Проверяем регистрируются ли координаты в базе
				if (_terminalDeviceMedias != null)
				{
					foreach (var media in _terminalDeviceMedias)
					{
						if (media == "TCP_internet")
						{
							CheckTerminalService(media, "172.021.___.___", true);
							CheckTerminalService(media, "172.021.___.___", false);
						}
						else
						{
							CheckTerminalService(media, null, false);
						}
					}
				}

				// Проверка свободного места на диске
				if (_checkDiskFreeSpace)
					CheckDiskFreeSpace();

				if (_checkRuleProcessorDelay)
					CheckRuleProcessorDelay();

				// Проверяем работает ли сайт
				if (_checkSite)
					CheckWebSite();

				// Проверяет отправку сообщений
				if (_checkMessages)
					CheckMessages();

				// Проверяет очередь планировщика заданий
				if (_schedulerQueueTimeoutMinutes != null)
					CheckSchedulerQueue(_schedulerQueueTimeoutMinutes.Value);

				// Отправка отчетов о проверках, если что обнаружено
				if (_messages.Count != 0)
				{
					var currentTroubles = new HashSet<string>(_lastTroubleSubject.Select(pair => pair.Key));
					// Шлём только если что-то починилось или сломалось или сломано и не отправляли уже больше 3-х часов
					if ((DateTime.Now - _lastSmsSendTime).TotalMinutes > _minSmsIntervalMinutes || !currentTroubles.Equals(prevTroubles))
						SendMessage();
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("CheckSystem error: {0}\n{1}", ex.Message, ex.StackTrace);
			}
		}

		private void CheckSchedulerQueue(int timeoutMinutes)
		{
			const string key = "Scheduler";

			using (var connection = new SqlConnection(_dbConnString))
			{
				try
				{
					connection.Open();
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);
					ReportTrouble(key, "Unable to connect to DB", ex.ToString());
					return;
				}

				try
				{
					var command = new SqlCommand(@"
select count(1)
	from schedulerqueue
	where enabled = 1
	  and nearest_Time < dateadd(minute, -5, getutcdate())
	  and (LastSchedulerStartDate IS NULL OR (DATEDIFF (second, LastSchedulerStartDate, getutcdate())) > 1)
	  and errorcount = 0
", connection);

					var delayedTaskCount = (int)command.ExecuteScalar();
					if (0 < delayedTaskCount)
					{
						command = new SqlCommand(@"
select q.SCHEDULERQUEUE_ID, e.SCHEDULEREVENT_ID, e.COMMENT
from SCHEDULERQUEUE q
	join SCHEDULEREVENT e on q.SCHEDULEREVENT_ID = e.SCHEDULEREVENT_ID
where q.[Enabled] = 1 
	AND q.NEAREST_TIME < getutcdate() 
	AND (q.LastSchedulerStartDate IS NULL OR (DATEDIFF (second, q.LastSchedulerStartDate, getutcdate())) > 1)
ORDER BY q.ErrorCount ASC, q.NEAREST_TIME ASC
", connection);
						using (var reader = command.ExecuteReader())
						{
							var blockingTaskString = string.Empty;
							var blockedTasks = new List<string>();
							var i = 0;
							while (reader.Read())
							{
								var queueId = (int)reader["SCHEDULERQUEUE_ID"];
								var comment = (string)reader["COMMENT"];
								if (i == 0)
								{
									blockingTaskString = string.Format(CultureInfo.CurrentCulture,
										"Blocking queueId: {0} event: {1};{2}", queueId, comment, Environment.NewLine);
								}
								else
								{
									blockedTasks.Add(string.Format(CultureInfo.CurrentCulture, "   {0}) queueId: {1} event: {2};", i, queueId, comment));
								}

								++i;
							}

							var body = string.Format(CultureInfo.CurrentCulture,
								"{0}Blocked tasks:{1}{2}",
								blockingTaskString,
								Environment.NewLine,
								string.Join(Environment.NewLine, blockedTasks));
							ReportTrouble(key, "Scheduler is slow: " + delayedTaskCount + " are uncompleted for last " + timeoutMinutes + " minutes", body);
						}

						return;
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);
					ReportTrouble(key, "Unable to connect to DB", ex.ToString());
					return;
				}
			}

			ReportSuccess(key);
		}

		private void CheckMessages()
		{
			const string keyPart = "messageSender-";
			int temp;
			var criticalCount = int.TryParse(ConfigurationManager.AppSettings["messagesCount"], out temp) ? temp : 300;
			var messageSenders = new Dictionary<MessageDestinationType, int>();
			using (var connection = new SqlConnection(_dbConnString))
			{
				try
				{
					connection.Open();
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);
					ReportTrouble(keyPart, "Unable to connect to DB", ex.ToString());
					return;
				}

				try
				{
					var command = new SqlCommand(@"
select DestinationType_ID
	,sum(case when status in (1,2) then 1 else 0 end) WaitMessages
from [MESSAGE]
where [STATUS] in (1, 2, 3) 
  and Time < dateadd(minute, 5, getutcdate())
group by DestinationType_ID
", connection);

					using (var reader = command.ExecuteReader())
					{
						var destinationOrdinal = reader.GetOrdinal("DestinationType_ID");
						var countOrdinal = reader.GetOrdinal("WaitMessages");
						while (reader.Read())
						{
							var destination = (MessageDestinationType)reader.GetInt32(destinationOrdinal);
							var count = reader.GetInt32(countOrdinal);

							messageSenders.Add(destination, count);
						}
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);
					ReportTrouble(keyPart, "Unable to connect to DB", ex.ToString());
					return;
				}

				ReportSuccess(keyPart);
			}

			foreach (var messageSender in messageSenders)
			{
				var key = keyPart + messageSender.Key;
				if (messageSender.Value > criticalCount)
				{
					var subject = string.Format(CultureInfo.CurrentCulture, "Too much waiting messages for sender: {0}", messageSender.Key);
					var body = string.Format(CultureInfo.CurrentCulture, "Waiting messages: {0}", messageSender.Value);
					ReportTrouble(key, subject, body);
				}
				else
				{
					ReportSuccess(key);
				}
			}
		}

		private void CheckRuleProcessorDelay()
		{
			using (var conn = new SqlConnection(_dbConnString))
			{
				try
				{
					conn.Open();
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
					ReportTrouble("RuleProcessor", "Rule processor trouble", "Rule processor: no DB connect (" + ex.Message + ")");
					return;
				}

				try
				{
					var cmd =
						new SqlCommand(@"
select AVG(delay) + STDEV(delay)
	from (
		select delay = DATEDIFF(second, currentTime, getutcdate())
			from Vehicle_Rule_Processing vrp with (nolock) 
			join VEHICLE v on v.VEHICLE_ID = vrp.Vehicle_ID
	) t
	", conn);
					var sec = cmd.ExecuteScalar() as int?;

					//больше 1 минуты
					if (sec > 60)
					{
						ReportTrouble("RuleProcessor", "Rule processor trouble",
									  string.Format("Rule processor delay too big: {0} seconds; \n\r ", sec));
						return;
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
					ReportTrouble("RuleProcessor", "Rule processor trouble",
								  string.Format("Rule processor: exception {0}; \n\r", ex));
					return;
				}
				finally
				{
					conn.Close();
				}

				ReportSuccess("RuleProcessor");
			}
		}

		private void CheckDiskFreeSpace()
		{
			if (_diskNames.Length == 0)
				return;

			foreach (var drive in DriveInfo.GetDrives())
			{
				if (!_diskNames.Contains(drive.Name, StringComparer.OrdinalIgnoreCase))
					continue;

				var totalFreeSpaceGb = drive.TotalFreeSpace / (1024m * 1024m * 1024m);
				if (totalFreeSpaceGb < _diskFreeSpaceThresholdGb)
				{
					ReportTrouble("Disk " + drive.Name,
								  "Disk " + drive.Name + " problem",
								  string.Format("Disk {0} free space is low: {1:0.00}; ",
												drive.Name, totalFreeSpaceGb));
				}
				else
				{
					ReportSuccess("Disk " + drive.Name,
								  "Disk " + drive.Name + " OK",
								  string.Format("Disk {0} free space is now: {1:0.00}; ",
													  drive.Name, totalFreeSpaceGb));
				}
			}
		}

		void workTimer_Elapsed(object sender)
		{
			using (new Stopwatcher($@"{nameof(DiagnosticProcessor)}.{nameof(DiagnosticProcessor.CheckSystem)}", null, TimeSpan.Zero))
				CheckSystem();
			_workTimer.Change(_checkInterval, TimeSpan.FromMilliseconds(-1));
		}

		private readonly bool _checkRuleProcessorDelay;
		private string GetWebSiteContent(string url, int timeout)
		{
			string response;
			var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
			http.Headers.Add(HttpRequestHeader.AcceptLanguage, "ru");
			http.AllowAutoRedirect = true;
			http.CookieContainer = new CookieContainer();
			http.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0b; Windows NT 5.0; .NET CLR 1.0.2914)";
			http.ContentType = "application/x-www-form-urlencoded";
			http.KeepAlive = true;
			http.SendChunked = false;
			http.Timeout = timeout;
			http.UseDefaultCredentials = true;
			http.Method = WebRequestMethods.Http.Get;

			//	запрос
			var resp = (HttpWebResponse)http.GetResponse();

			if (resp.StatusCode != HttpStatusCode.OK)
			{
				return "";
			}

			//	копирование ответного потока в массив байт
			using (Stream s = resp.GetResponseStream())
			{
				if (s == null) return null;

				using (var reader = new StreamReader(s))
				{
					response = reader.ReadToEnd();
					reader.Close();
				}

				s.Close();
			}

			return response;
		}
		private void CheckWebSite()
		{
			try
			{
				var sw = new Stopwatch();
				sw.Start();
				string response = GetWebSiteContent(_webAppUrl, _webAppTimeOut);
				sw.Stop();

				if (null == response || !response.Contains(_webAppPageShouldContainsString))
				{
					ReportTrouble("WebSite", "NoWebLogin", "Web request success, but no Guest login");
					return;
				}

				if (_webAppTimeOutThreshold < sw.ElapsedMilliseconds)
				{
					ReportTrouble("WebSite", "Web is slow: guest login has taken " + sw.Elapsed,
								  "Web is slow: guest login has taken " + sw.Elapsed);
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
				ReportTrouble("WebSite", "NoWeb for " + (_webAppTimeOut / 1000) + " seconds",
							  "NoWeb error to request site: failed to get web app timeout in " + (_webAppTimeOut / 1000) + " seconds");
				return;
			}

			ReportSuccess("WebSite");
		}
		private void CheckTerminalService(string mediaName, string ipMask, bool shouldMatchIpMask)
		{
			string channelName = mediaName + " " + (shouldMatchIpMask ? " APN nika.msk" : " Public Internet");

			using (var conn = new SqlConnection(_dbConnString))
			{
				string reportCode = "CheckTerminalService(" + channelName + ")";
				try
				{
					conn.Open();
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
					ReportTrouble(reportCode, "Unable to connect to DB",
								  "Diagnostic service is unable to connect to DB server");
					return;
				}

				string cmdText = string.Empty;
				try
				{
					string ipMaskCondition = string.Empty;
					if (!string.IsNullOrWhiteSpace(ipMask))
					{
						ipMaskCondition = @" and vc.IP " + (shouldMatchIpMask ? "like" : "not like") + @" @ipMask";
					}

					cmdText = @"
	select datediff(minute, max(t.LastTime), GETUTCDATE())
	from (
		select vc.LastTime
			from v_vehicle_connection vc
			where vc.Name = @maName" + ipMaskCondition + @") t";
					var cmd = new SqlCommand(cmdText, conn);

					cmd.Parameters.Add("@maName", SqlDbType.VarChar).Value = mediaName;

					if (!string.IsNullOrWhiteSpace(ipMask))
						cmd.Parameters.Add("@ipMask", SqlDbType.VarChar).Value = ipMask;

					var result = cmd.ExecuteScalar();
					var noDataMinutes = result as int?;
					//больше 4 минут
					if (noDataMinutes == null || 4 < noDataMinutes)
					{
						var subject = new StringBuilder("No data on " + channelName);

						var message = new StringBuilder();
						message.Append("No data registered on network interface ");
						message.Append(channelName);
						if (noDataMinutes == null)
							message.Append(" ever");
						else
							message.AppendFormat(" for last {0} minutes", noDataMinutes.Value);

						ReportTrouble(reportCode, subject.ToString(), message.ToString());
						return;
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
					Trace.TraceInformation("SQL query was: {0}", cmdText);
					ReportTrouble(reportCode, "Unable to connect to DB",
						"Diagnostic service is unable to retrieve data from DB server: " + ex);
					return;
				}
				finally
				{
					conn.Close();
				}

				ReportSuccess(reportCode,
							  channelName + " has been restored",
							  "Network interface " + channelName + " is now OK");
			}
		}
		private void CheckTerminalServiceRemoting()
		{
			if (_terminalClientLastOnline == null)
			{
				ConnectTerminalClient();
				_terminalClientLastOnline = DateTime.UtcNow;
				return;
			}

			var timeSinceLastReceiveEvent = DateTime.UtcNow - _terminalClientLastOnline.Value;
			if (5 < timeSinceLastReceiveEvent.TotalSeconds)
			{
				ReportTrouble(string.Format(
					"Terminal service remoting: no receive event for {0} seconds",
					timeSinceLastReceiveEvent.TotalSeconds));

				bool isAlive;
				try
				{
					isAlive = _terminalClient.Session.IsAlive();
				}
				catch (Exception ex)
				{
					Trace.TraceWarning("{0}", ex);
					isAlive = false;
				}
				if (!isAlive)
					ConnectTerminalClient();
				return;
			}
			ReportSuccess("Terminal service remoting");
		}
		private void CheckTerminalSaveQueue(int saveQueueCountThreshold)
		{
			var perfSaveQueue = Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaveQueue);
			if (null == perfSaveQueue)
			{
				ReportTrouble(
					$"TerminalSaveQueue",
					$"Unable to check queue length for storing terminal data in the database",
					$"Performance counter not found");
				return;
			}
			var perfSaveQueueValue = perfSaveQueue.RawValue;
			if (saveQueueCountThreshold < perfSaveQueueValue)
			{
				ReportTrouble(
					$"TerminalSaveQueue",
					$"The queue length for storing terminal data in the database is more than {saveQueueCountThreshold} items",
					$"Current queue length: {perfSaveQueueValue}");
			}
			else
			{
				ReportSuccess($"TerminalSaveQueue");
			}
		}
		private void ReportTrouble(string key, string subject = null, string body = null, string[] recipients = null)
		{
			if (subject == null)
				subject = key + " trouble";

			Trace.TraceWarning("Trouble report: key={0}, subject={1}, body={2},", key, subject, body);

			string lastTroubleSubject;
			if (_lastTroubleSubject.TryGetValue(key, out lastTroubleSubject) && lastTroubleSubject == subject)
				return;

			_lastTroubleSubject[key] = subject;

			_messages.Add(new Message
			{
				Subject    = subject,
				Body       = body ?? subject,
				Recipients = recipients
			});
		}
		private void ReportSuccess(string key, string subject = null, string body = null, string[] recipients = null)
		{
			string lastTroubleSubject;
			if (!_lastTroubleSubject.TryGetValue(key, out lastTroubleSubject))
				return;

			Trace.TraceInformation("Success report: key={0}, subject={1}, body={2},", key, subject, body);

			_lastTroubleSubject.Remove(key);

			_messages.Add(new Message
			{
				Subject    = subject ?? (key + " fixed"),
				Body       = body    ?? (key + " fixed"),
				Recipients = recipients
			});
		}
		private readonly Dictionary<string, string> _lastTroubleSubject = new Dictionary<string, string>();
		private readonly int? _schedulerQueueTimeoutMinutes;
		private void SendMessage()
		{
			try
			{
				bool res = true;
				if (_messageChannel == "SMS")
				{
					foreach (var message in _messages)
					{
						res = SendSms(message.Subject);
					}
				}
				if (_messageChannel == "Email")
				{
					foreach (var message in _messages)
						res = SendEmail(message.Subject, message.Body, message.Recipients);
				}

				if (res)
				{
					_lastSmsSendTime = DateTime.Now;
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("Error sending message: " + ex);
			}

			_messages.Clear();
		}
		private bool SendEmail(string subject, string text, string[] recipients = null)
		{
			bool res = true;
			try
			{
				recipients = recipients ?? _recipients;
				if (0 == (recipients?.Length ?? 0))
					return false;
				EmailSender.Send(subject, text, recipients, null);
			}
			catch (Exception ex)
			{
				Trace.TraceWarning(ex.ToString());
				res = false;
			}

			return res;
		}
		private bool SendSms(string subject)
		{
			bool res = true;
			if (0 == (_recipients?.Length ?? 0))
				return false;
			foreach (string phoneNum in _recipients)
			{
				try
				{
					throw new NotImplementedException("Sending reports via SMS is not implemented, use email.");
				}
				catch (Exception ex)
				{
					Trace.TraceWarning(ex.ToString());
					res = false;
				}
			}

			return res;
		}
		private void ConnectTerminalClient()
		{
			if (!_remotingConfigured)
			{
				_remotingConfigured = true;
				ConfigureRemoting();
			}

			try
			{
				_terminalClient = new TerminalClient(ConfigurationManager.AppSettings);
				_terminalClient.Connect(_terminalServiceUrl, false);
				_terminalClient.Receive += MobilUnitReceived;
				((IClient)_terminalClient).Start();
			}
			catch (Exception ex)
			{
				Trace.TraceWarning("{0}", ex);
				ReportTrouble("Terminal service remoting", "Unable to connect to " + _terminalServiceUrl, ex.ToString());
			}
		}
		private void ConfigureRemoting()
		{
			RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);
		}

		private DateTime? _terminalClientLastOnline;
		private bool      _remotingConfigured;
		private Timer     _workTimer;

		private void MobilUnitReceived(ReceiveEventArgs args)
		{
			// Без этого преобразования не работает Remoting, т.к. не нигде ссылки на сборку, а объект физически приходит
			var mobiUnits = args?.MobilUnits?.OfType<MobilUnit.Unit.MobilUnit>();

			_terminalClientLastOnline = DateTime.UtcNow;
		}
	}
}