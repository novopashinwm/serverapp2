﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace FORIS.TSS.DiagnosticService
{
	public partial class DiagnosticService : ServiceBase
	{
		public DiagnosticService()
		{
			InitializeComponent();
		}

		private DiagnosticProcessor _processor;
		protected override void OnStart(string[] args)
		{
			try
			{
				_processor = new DiagnosticProcessor();
				_processor.Start();
			}
			catch (Exception ex)
			{
				Trace.TraceError("Service '{0}' start error: {1}\n{2}", ServiceName, ex.Message, ex.StackTrace);
			}
		}
		protected override void OnStop()
		{
			try
			{
				_processor.Stop();
			}
			catch (Exception ex)
			{
				Trace.TraceError("Service '{0}' stop error: {1}\n{2}", ServiceName, ex.Message, ex.StackTrace);
			}
		}
	}
}