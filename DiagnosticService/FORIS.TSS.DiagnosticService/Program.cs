﻿using System;
using System.ServiceProcess;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerLauncher.Application;

namespace FORIS.TSS.DiagnosticService
{
	static class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var servicesToRun = new ServiceBase[]
				{
					new DiagnosticService(),
				};
				if (Environment.UserInteractive)
					TrayIconApplicationContext.RunInteractive("Diagnostics Server", servicesToRun);
				else
					ServiceBase.Run(servicesToRun);
			}
			catch (Exception ex)
			{
				$"Application start"
					.WithException(ex, true)
					.TraceError();
			}
		}
	}
}