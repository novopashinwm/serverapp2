﻿using System.ComponentModel;
using System.Configuration.Install;

namespace FORIS.TSS.DiagnosticService
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}
	}
}