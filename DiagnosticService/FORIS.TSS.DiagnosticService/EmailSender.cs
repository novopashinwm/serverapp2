﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;


namespace FORIS.TSS.DiagnosticService
{
	public class EmailSender
	{
		private static string      _smtpServer;
		private static int         _smtpServerPort;
		private static MailAddress _mailFromDefault;

		private static bool        _useSSL;
		private static string      _smtpServerLogin;
		private static string      _smtpServerPassword;


		private static SmtpClient GetSmtpClient()
		{
			var client = new SmtpClient(_smtpServer, _smtpServerPort);
			client.EnableSsl = _useSSL;

			if (string.IsNullOrEmpty(_smtpServerLogin))
			{
				client.UseDefaultCredentials = true;
				client.Credentials = CredentialCache.DefaultNetworkCredentials;
			}
			else
			{
				client.UseDefaultCredentials = false;
				client.Credentials = new NetworkCredential(_smtpServerLogin, _smtpServerPassword);
			}
			return client;
		}



		static EmailSender()
		{
			string smtpServerPortString = ConfigurationManager.AppSettings["MailSmtpServerPort"];
			_smtpServer = ConfigurationManager.AppSettings["MailServer"];
			_smtpServerPort = (!string.IsNullOrEmpty(smtpServerPortString))
				? int.Parse(smtpServerPortString)
				: 25;

			_useSSL             = ConfigurationManager.AppSettings["MailSmtpServerSSL"] == "1";
			_smtpServerLogin    = ConfigurationManager.AppSettings["MailServerLogin"];
			_smtpServerPassword = ConfigurationManager.AppSettings["MailServerPassword"];

			_mailFromDefault    = new MailAddress(QuoteSquareBraces(ConfigurationManager.AppSettings["MailFrom"]));
		}


		public static void Send(MailMessage mailmsg)
		{
			try
			{
				if (mailmsg.From == null || string.IsNullOrEmpty(mailmsg.From.Address))
				{
					mailmsg.From = _mailFromDefault;
				}

				using (var client = GetSmtpClient())
				{
					client.Send(mailmsg);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Unable to send mail: " + ex.ToString(), "EmailSender");
			}
			finally
			{
				if (mailmsg != null)
				{
					mailmsg.Dispose();
				}
			}
		}

		public static void Send(string subject, string message, string[] recipients, string mailFrom, params Attachment[] attachments)
		{
			var mailmsg = new MailMessage();
			mailmsg.From = string.IsNullOrWhiteSpace(mailFrom)
				? _mailFromDefault
				: new MailAddress(QuoteSquareBraces(mailFrom));

			foreach (var recipient in recipients)
				mailmsg.To.Add(recipient);

			mailmsg.Subject = subject;
			mailmsg.Body    = message;

			Send(mailmsg);
		}

		private static string QuoteSquareBraces(string str)
		{
			return str.Replace("&lt;", "<").Replace("&gt;", ">");
		}
		private static string BuildEmail(string name, string email)
		{
			return QuoteSquareBraces(name) + "<" + QuoteSquareBraces(email) + ">";
		}
	}
}