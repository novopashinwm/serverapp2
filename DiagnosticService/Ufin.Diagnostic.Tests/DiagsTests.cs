﻿using FORIS.TSS.DiagnosticService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Diagnostic.Tests
{
	[TestClass]
	public class DiagsTests
	{
		[TestMethod]
		public void TestService()
		{
			DiagnosticProcessor proc = new DiagnosticProcessor();
			proc.CheckSystem();
		}

		[TestMethod]
		public void TestEmail()
		{
			EmailSender.Send("Модульный тест", "Модульный тест", new string[] { "Я <k@ufin.online>", "Оператор диагностики <k@ufin.online>" }, null);
		}
	}
}