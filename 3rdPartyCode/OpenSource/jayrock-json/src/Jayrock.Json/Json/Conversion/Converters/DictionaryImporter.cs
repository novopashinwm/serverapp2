#region License, Terms and Conditions
//
// Jayrock - JSON and JSON-RPC for Microsoft .NET Framework and Mono
// Written by Atif Aziz (atif.aziz@skybow.com)
// Copyright (c) 2005 Atif Aziz. All rights reserved.
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Lesser General Public License as published by the Free
// Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
//
#endregion

using System;
using System.Collections.Generic;

namespace Jayrock.Json.Conversion.Converters
{
    #region Imports

    using System.Collections;

    #endregion

    public class DictionaryImporter : ImportAwareImporter
    {
        public DictionaryImporter() : 
            base(typeof(IDictionary)) {}

        public DictionaryImporter(Type type) : 
            base(type) {}

        protected override IJsonImportable CreateObject()
        {
            return new JsonObject();
        }

        public override object Import(ImportContext context, JsonReader reader)
        {
            var result = (IDictionary)base.Import(context, reader);

            if (!OutputType.IsInterface && !OutputType.IsAbstract)
            {
                var resultValue = (IDictionary)Activator.CreateInstance(OutputType);

                if (OutputType.IsGenericType &&
                    OutputType.GetGenericTypeDefinition() == typeof (Dictionary<,>) &&
                    OutputType.GetGenericArguments()[0].IsEnum)
                {
                    foreach (var key in result.Keys)
                    {
                        var keyString = key as string;
                        if (keyString != null)
                        {
                            var keyEnum = (Enum) Enum.Parse(OutputType.GetGenericArguments()[0], keyString);
                            resultValue[keyEnum] = result[key];
                            continue;
                        }

                        resultValue[key] = result[key];
                    }
                }
                else
                {
                    foreach (var key in result.Keys)
                    {
                        resultValue[key] = result[key];
                    }
                }
                return resultValue;
            }

            return result;
        }
    }
}