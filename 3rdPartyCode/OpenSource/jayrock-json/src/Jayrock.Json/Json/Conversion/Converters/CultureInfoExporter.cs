using System;
using System.Diagnostics;
using System.Globalization;

namespace Jayrock.Json.Conversion.Converters
{
    public sealed class CultureInfoExporter : ExporterBase
    {
        public CultureInfoExporter() : 
            this(typeof(CultureInfo)) {}

        public CultureInfoExporter(Type type) : 
            base(type) {}

        protected override void ExportValue(ExportContext context, object value, JsonWriter writer)
        {
            Debug.Assert(context != null);
            Debug.Assert(value != null);
            Debug.Assert(writer != null);
            var cultureInfo = (CultureInfo) value;
            writer.WriteString(cultureInfo.ThreeLetterISOLanguageName);
        }
    }
}