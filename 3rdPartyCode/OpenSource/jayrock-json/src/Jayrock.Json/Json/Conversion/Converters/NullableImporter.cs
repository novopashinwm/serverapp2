using System;

namespace Jayrock.Json.Conversion.Converters
{
    public sealed class NullableImporter : IImporter
    {
        private readonly Type _type;
        private readonly Type _underlyingType;

        public NullableImporter(Type type)
        {
            _type = type;
            _underlyingType = type.GetGenericArguments()[0];
        }

        public Type OutputType
        {
            get { return _type; }
        }

        public object Import(ImportContext context, JsonReader reader)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (reader == null) throw new ArgumentNullException("reader");

            if (!reader.MoveToContent())
                throw new JsonException("Unexpected EOF.");

            if (reader.TokenClass == JsonTokenClass.Null ||
                reader.TokenClass == JsonTokenClass.String && reader.Token.Text.Length == 0)
            {
                reader.Read();
                return null;
            }

            return context.Import(_underlyingType, reader);
        }
    }
}