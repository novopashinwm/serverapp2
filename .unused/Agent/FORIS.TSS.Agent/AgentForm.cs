using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Agent;

namespace FORIS.TSS.Agent
{
	public class AgentForm: 
		AgentFormBase
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public AgentForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new Container();
			this.ctrlTabs.SuspendLayout();
			this.SuspendLayout();
			// 
			// AgentForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 559, 399 );
			this.Name = "AgentForm";
			this.ctrlTabs.ResumeLayout( false );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion
	}
}