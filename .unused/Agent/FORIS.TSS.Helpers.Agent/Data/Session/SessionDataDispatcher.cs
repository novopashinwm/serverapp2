﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Agent.Data.Session
{
	public class SessionDataDispatcher :
		DataDispatcher<ISessionData>
	{
		public SessionDataDispatcher()
		{

		}
		public SessionDataDispatcher(IContainer container)
			: this()
		{
			container.Add(this);
		}
		private readonly SessionDataAmbassadorCollection ambassadors =
			new SessionDataAmbassadorCollection();
		protected override AmbassadorCollection<IDataItem<ISessionData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new SessionDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
	}
	public class SessionDataAmbassadorCollection :
		DataAmbassadorCollection<ISessionData>
	{
		public new SessionDataAmbassador this[int index]
		{
			get { return (SessionDataAmbassador)base[index]; }
		}
	}
	public class SessionDataAmbassador :
		DataAmbassador<ISessionData>
	{
	}
}