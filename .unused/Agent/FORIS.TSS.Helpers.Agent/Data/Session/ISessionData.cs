﻿using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Agent.Data.Session;

namespace FORIS.TSS.Helpers.Agent.Data.Session
{
	public interface ISessionData :
		IData<ISessionDataTreater>,
		ISessionDataSupplier
	{
		SessionTable Session { get; }
		ParamTable   Param   { get; }
	}
}