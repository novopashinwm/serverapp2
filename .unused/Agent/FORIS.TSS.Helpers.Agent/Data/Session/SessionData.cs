﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Agent.Data.Session;

namespace FORIS.TSS.Helpers.Agent.Data.Session
{
	public class SessionData :
		Data<ISessionDataTreater>,
		ISessionData
	{
		#region Controls & Components

		private IContainer components;
		private SessionDataDispatcher tablesDataDispatcher;

		private SessionTable session;
		private ParamTable param;

		private SessionDataAmbassador sessionAmbassador;
		private SessionDataAmbassador paramAmbassador;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public SessionData()
		{
			InitializeComponent();
		}
		public SessionData(IContainer container)
			: this()
		{
			container.Add(this);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new Container();
			tablesDataDispatcher = new SessionDataDispatcher();

			session = new SessionTable(components);
			param = new ParamTable(components);

			sessionAmbassador = new SessionDataAmbassador();
			paramAmbassador = new SessionDataAmbassador();

			sessionAmbassador.Item = session;
			paramAmbassador.Item = param;

			tablesDataDispatcher.Ambassadors.Add(sessionAmbassador);
			tablesDataDispatcher.Ambassadors.Add(paramAmbassador);
		}

		#endregion // Component Designer generated code

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			tablesDataDispatcher.Data = null;
		}
		protected override void Build()
		{
			tablesDataDispatcher.Data = this;
		}

		#endregion // Build & Destroy

		#region Tables properties

		[Browsable(false)]
		[Category("Tables")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public SessionTable Session
		{
			get { return session; }
		}

		[Browsable(false)]
		[Category("Tables")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ParamTable Param
		{
			get { return param; }
		}


		#endregion // Tables properties
	}
}