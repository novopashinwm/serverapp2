﻿using System;
using System.ComponentModel;
using System.Data;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Agent.Data.Session
{
	public class SessionTable :
		Table<SessionRow, SessionTable, ISessionData>
	{
		#region Properties

		public override string Name
		{
			get { return "SESSION"; }
		}

		#endregion // Properties

		public SessionTable()
		{
		}
		public SessionTable(IContainer container)
		{
			container.Add(this);
		}

		protected override SessionRow OnCreateRow(DataRow dataRow)
		{
			return new SessionRow(dataRow);
		}
	}

	public class SessionRow :
		Row<SessionRow, SessionTable, ISessionData>
	{
		#region Fields

		private const int SESSION_ID = 0;
		private const int LOGIN = 1;
		private const int LEASE_TIME = 2;
		private const int LEASE_STATE = 3;

		public int SessionId
		{
			get { return (int)DataRow[SESSION_ID, DataRowVersion.Current]; }
			set { DataRow[SESSION_ID] = value; }
		}

		public string Login
		{
			get { return (string)DataRow[LOGIN, DataRowVersion.Current]; }
			set { DataRow[LOGIN] = value; }
		}

		public TimeSpan? LeaseTime
		{
			get
			{
				return !DataRow.IsNull(LEASE_TIME)
					? (TimeSpan)DataRow[LEASE_TIME, DataRowVersion.Current]
					: (TimeSpan?)null;
			}
			set { DataRow[LEASE_TIME] = value ?? (object)DBNull.Value; }
		}

		public LeaseState? LeaseState
		{
			get
			{
				return !DataRow.IsNull(LEASE_STATE)
					? (LeaseState)DataRow[LEASE_STATE, DataRowVersion.Current]
					: (LeaseState?)null;
			}
			set { DataRow[LEASE_STATE] = value ?? (object)DBNull.Value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)DataRow[SESSION_ID];
		}

		#endregion // Id

		public SessionRow(DataRow dataRow)
			: base(dataRow)
		{
		}

		protected override void OnBuildFields()
		{
		}
	}
}