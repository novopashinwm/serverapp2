﻿using System;
using System.Collections.Generic;
using System.Data;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Agent.Data;
using FORIS.TSS.Interfaces.Operator.Data;

namespace FORIS.TSS.Interfaces.Agent
{
	public interface IAgentPersonalServer :
		IPersonalServer,
		ISessionDataSupplierProvider,
		IOperatorDataSupplierProvider
	{
		/// <summary> Сбросить кэш параметров хранимых процедур БД </summary>
		/// <returns> Список имен хранимых процедур для которых был сброшен кэш параметров </returns>
		IList<string> ResetStoredProceduresParametersCache();
		void CloseSession(int sessionId);
		DataSet GetOperatorsSessionByDate(int operator_id, DateTime from, DateTime to);
	}
}