﻿using FORIS.TSS.Interfaces.Agent.Data.Session;

namespace FORIS.TSS.Interfaces.Agent.Data
{
	public interface ISessionDataSupplierProvider
	{
		ISessionDataSupplier GetSessionDataSupplier();
	}
}