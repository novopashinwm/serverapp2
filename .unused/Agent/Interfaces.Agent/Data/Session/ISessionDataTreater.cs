﻿using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Agent.Data.Session
{
	public interface ISessionDataTreater :
		IDataTreater
	{
		void SessionInsert(int sessionId, string login);
		void SessionDelete(int sessionId);
		void UpdateSessionLeaseTimes(IDictionary<int, ILease> sessionLeases);
	}
}