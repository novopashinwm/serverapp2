﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Agent.Data.Session
{
	/// <summary> Интерфейс поставщика данных о сессиях </summary>
	/// <remarks> Содержит информацию о сессиях </remarks>
	public interface ISessionDataSupplier :
		IDataSupplier<ISessionDataTreater>
	{
	}
}