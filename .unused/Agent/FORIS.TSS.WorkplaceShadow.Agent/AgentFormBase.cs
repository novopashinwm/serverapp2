using System;
using System.Collections.Specialized;
using System.Runtime.Remoting;
using System.Threading;
using System.Windows.Forms;
using FORIS.Common.Forms;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Config;
using FORIS.TSS.Helpers.Agent.Data.Session;
using FORIS.TSS.Interfaces.Agent;
using FORIS.TSS.WorkplaceShadow.Agent.Controls.Session;
using FORIS.TSS.WorkplaceShadow.Agent.Data.Session;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Agent
{
	public class AgentFormBase : 
		Form,
		IClientItem<AgentClient>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;
		private SessionTableControl ctrlSessionTable;
		private CommandManager commandManager;
		protected AgentClientDispatcher clientDispatcher;
		private SessionData sessionData;
		private SessionDataSupplier sessionDataSupplier;
		private SessionDataDispatcher dataDispatcher;
		private InvokeBroker invokeBroker;
		private AgentClientAmbassador caSessionDataSupplier;
		protected ToolStrip toolStrip1;
		private ToolStripButton btnResetProceduresCache;
		private Command cmdResetProceduresCache;
		private Command cmdDropSession;
		protected TabControl ctrlTabs;
		private TabPage tpSession;
		private ToolStripButton btnDropSession;
		private CommandInstance ciResetProceduresCache;
		private CommandInstance ciDropSession;
		private SessionFilter sessionFilter;
		private SessionDataAmbassador daSessionXpRowFactory;
		private SessionDataAmbassador daSessionFilter;
		private SessionXpRowFactory sessionXpRowFactory;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public AgentFormBase()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AgentFormBase ) );
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.invokeBroker = new FORIS.TSS.Common.InvokeBroker( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.Agent.Data.Session.SessionDataDispatcher( this.components );
			this.daSessionXpRowFactory = new FORIS.TSS.Helpers.Agent.Data.Session.SessionDataAmbassador();
			this.sessionXpRowFactory = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Session.SessionXpRowFactory();
			this.sessionFilter = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Session.SessionFilter();
			this.ctrlSessionTable = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Session.SessionTableControl();
			this.daSessionFilter = new FORIS.TSS.Helpers.Agent.Data.Session.SessionDataAmbassador();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.btnResetProceduresCache = new System.Windows.Forms.ToolStripButton();
			this.btnDropSession = new System.Windows.Forms.ToolStripButton();
			this.cmdResetProceduresCache = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciResetProceduresCache = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdDropSession = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciDropSession = new FORIS.TSS.Common.Commands.CommandInstance();
			this.ctrlTabs = new System.Windows.Forms.TabControl();
			this.tpSession = new System.Windows.Forms.TabPage();
			this.clientDispatcher = new FORIS.TSS.WorkplaceShadow.Agent.AgentClientDispatcher( this.components );
			this.caSessionDataSupplier = new FORIS.TSS.WorkplaceShadow.Agent.AgentClientAmbassador();
			this.sessionDataSupplier = new FORIS.TSS.WorkplaceShadow.Agent.Data.Session.SessionDataSupplier( this.components );
			this.sessionData = new FORIS.TSS.Helpers.Agent.Data.Session.SessionData( this.components );
			this.toolStrip1.SuspendLayout();
			this.ctrlTabs.SuspendLayout();
			this.tpSession.SuspendLayout();
			this.SuspendLayout();
			// 
			// invokeBroker
			// 
			this.invokeBroker.Invoke += new FORIS.TSS.Common.InvokeEventHandler( this.invokeBroker_Invoke );
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daSessionXpRowFactory );
			this.dataDispatcher.Ambassadors.Add( this.daSessionFilter );
			// 
			// daSessionXpRowFactory
			// 
			this.daSessionXpRowFactory.Item = this.sessionXpRowFactory;
			// 
			// sessionXpRowFactory
			// 
			this.sessionXpRowFactory.Data = null;
			this.sessionXpRowFactory.Filter = this.sessionFilter;
			this.sessionXpRowFactory.ViewControl = this.ctrlSessionTable;
			// 
			// ctrlSessionTable
			// 
			this.ctrlSessionTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctrlSessionTable.Location = new System.Drawing.Point( 3, 3 );
			this.ctrlSessionTable.Name = "ctrlSessionTable";
			this.ctrlSessionTable.Size = new System.Drawing.Size( 545, 578 );
			this.ctrlSessionTable.TabIndex = 0;
			// 
			// daSessionFilter
			// 
			this.daSessionFilter.Item = this.sessionFilter;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.btnResetProceduresCache,
            this.btnDropSession} );
			this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size( 559, 25 );
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// btnResetProceduresCache
			// 
			this.btnResetProceduresCache.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnResetProceduresCache.Image = ( (System.Drawing.Image)( resources.GetObject( "btnResetProceduresCache.Image" ) ) );
			this.btnResetProceduresCache.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnResetProceduresCache.Name = "btnResetProceduresCache";
			this.btnResetProceduresCache.Size = new System.Drawing.Size( 23, 22 );
			this.btnResetProceduresCache.Text = "P";
			this.btnResetProceduresCache.ToolTipText = "Reset procedures parameters cache";
			// 
			// btnDropSession
			// 
			this.btnDropSession.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnDropSession.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDropSession.Name = "btnDropSession";
			this.btnDropSession.Size = new System.Drawing.Size( 72, 22 );
			this.btnDropSession.Text = "Drop session";
			// 
			// cmdResetProceduresCache
			// 
			this.cmdResetProceduresCache.Instances.Add( this.ciResetProceduresCache );
			this.cmdResetProceduresCache.Manager = this.commandManager;
			this.cmdResetProceduresCache.Execute += new System.EventHandler( this.cmdResetProceduresCache_Execute );
			// 
			// ciResetProceduresCache
			// 
			this.ciResetProceduresCache.Item = this.btnResetProceduresCache;
			// 
			// cmdDropSession
			// 
			this.cmdDropSession.Instances.Add( this.ciDropSession );
			this.cmdDropSession.Manager = this.commandManager;
			this.cmdDropSession.Execute += new System.EventHandler( this.cmdDropSession_Execute );
			// 
			// ciDropSession
			// 
			this.ciDropSession.Item = this.btnDropSession;
			// 
			// ctrlTabs
			// 
			this.ctrlTabs.Controls.Add( this.tpSession );
			this.ctrlTabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctrlTabs.Location = new System.Drawing.Point( 0, 25 );
			this.ctrlTabs.Name = "ctrlTabs";
			this.ctrlTabs.SelectedIndex = 0;
			this.ctrlTabs.Size = new System.Drawing.Size( 559, 610 );
			this.ctrlTabs.TabIndex = 2;
			// 
			// tpSession
			// 
			this.tpSession.Controls.Add( this.ctrlSessionTable );
			this.tpSession.Location = new System.Drawing.Point( 4, 22 );
			this.tpSession.Name = "tpSession";
			this.tpSession.Padding = new System.Windows.Forms.Padding( 3 );
			this.tpSession.Size = new System.Drawing.Size( 551, 584 );
			this.tpSession.TabIndex = 0;
			this.tpSession.Text = "Session";
			this.tpSession.UseVisualStyleBackColor = true;
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.Ambassadors.Add( this.caSessionDataSupplier );
			// 
			// caSessionDataSupplier
			// 
			this.caSessionDataSupplier.Item = this.sessionDataSupplier;
			// 
			// sessionData
			// 
			this.sessionData.DataSupplier = this.sessionDataSupplier;
			this.sessionData.InvokeBroker = this.invokeBroker;
			this.sessionData.Loaded += new System.EventHandler( this.serverData_Loaded );
			this.sessionData.Loading += new System.EventHandler( this.serverData_Loading );
			// 
			// AgentFormBase
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 559, 635 );
			this.Controls.Add( this.ctrlTabs );
			this.Controls.Add( this.toolStrip1 );
			this.Name = "AgentFormBase";
			this.Text = "AgentFormBase";
			this.Load += new System.EventHandler( this.AgentFormBase_Load );
			this.toolStrip1.ResumeLayout( false );
			this.toolStrip1.PerformLayout();
			this.ctrlTabs.ResumeLayout( false );
			this.tpSession.ResumeLayout( false );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (AgentClient)value; }
		}

		#endregion

		#region IClientItem<AdminClient> Members

		public virtual AgentClient Client
		{
			get { return this.clientDispatcher.Client; }
			set
			{
				this.clientDispatcher.Client = value;
			}
		}

		#endregion

		#region Handle invokeBroker events

		private object invokeBroker_Invoke( InvokeEventArgs e )
		{
			/* ��� ������ ����� �������� �� ������ ����������
			 * � ������ ����������� ��������, ��� ��� � ������� 
			 * ���� ��� ����� ���������� �������, ������� �������� 
			 * ��������� � ������ ��������� � ������� ����������
			 * ����������, ������� �� ������ ��������
			 */

			return this.Invoke( e.Handler, e.Args );
		}

		#endregion // Handle invokeBroker events

		#region Handle serverData events

		private void serverData_Loading( object sender, EventArgs e )
		{
			this.dataDispatcher.Data = null;
		}

		private void serverData_Loaded( object sender, EventArgs e )
		{
			this.dataDispatcher.Data = this.sessionData;
		}

		#endregion // Handle serverData events

		#region Handle Form events

		private void AgentFormBase_Load( object sender, EventArgs e )
		{
			try
			{
				if ( this.DesignMode ) return;

				if ( Globals.AppSettings["Server"] == null )
				{
					throw new ApplicationException(
						"Server param not found in section appSettings in config file"
						);
				}

				RemotingConfiguration.Configure( AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false );

				string serverName = Globals.AppSettings["Server"];
				string clientType = Globals.AppSettings["Client"];
				Type type = Type.GetType( clientType, true );

				NameValueCollection properties = new NameValueCollection();
				properties.Set( "map", "" );

				AgentClient adminClient = new AgentClient( new NameValueCollection( 0 ) );
				using ( IClient commonClient =
					(IClient)Activator.CreateInstance( type, new object[] { properties } ) )
				{
					commonClient.Connect( serverName );
					Thread.CurrentPrincipal = commonClient.Session.GetPrincipal();

					adminClient.Connect( (IAgentPersonalServer)commonClient.Session.GetAdmin() );

					commonClient.Stop();
				}

				( (IClient)adminClient ).Start();

				this.Client = adminClient;
			}
			catch ( Exception ex )
			{
				ErrorForm.ShowErrorForm( null, ex );
			}
		}

		private void cmdResetProceduresCache_Execute( object sender, EventArgs e )
		{
			this.clientDispatcher.Client.Session.ResetStoredProceduresParametersCache();
		}

		private void cmdDropSession_Execute( object sender, EventArgs e )
		{
			if ( this.ctrlSessionTable.SelectedSession != 0 )
			{
				this.clientDispatcher.Client.Session.CloseSession( this.ctrlSessionTable.SelectedSession );
			}
		}

		#endregion  // Handle Form events
	}
}