﻿using System.Collections.Specialized;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Agent
{
	public class CommonClient :
		ClientBase<IPersonalServer>
	{
		public CommonClient( NameValueCollection parameters ) : 
			base( parameters )
		{
		}

		protected override void Start()
		{
		}

		protected override void Stop()
		{
		}

		protected override void ReConnect()
		{
			throw new System.NotImplementedException();
		}
	}
}