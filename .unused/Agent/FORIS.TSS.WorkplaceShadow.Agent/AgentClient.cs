﻿using System;
using System.Collections.Specialized;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Interfaces.Agent;
using FORIS.TSS.Interfaces.Agent.Data.Session;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Agent.Data.Session;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator;

namespace FORIS.TSS.WorkplaceShadow.Agent
{
	public class AgentClient :
		ClientBase<IAgentPersonalServer>,
		IOperatorClientDataProvider,
		ISecurityProvider
	{
		#region Constructor & Dispose

		public AgentClient( NameValueCollection properties )
			: base( properties )
		{

		}

		#endregion // Constructor & Dispose

		#region Data

		private SessionDataSupplierFactory m_serverDataSupplierFactory;
		private OperatorDataSupplierFactory m_operatorDataSupplierFactory;

		#endregion Data

		protected override void Start()
		{
			m_serverDataSupplierFactory = new SessionDataSupplierFactory( this );
			m_operatorDataSupplierFactory = new OperatorDataSupplierFactory( this );
		}
		protected override void Stop()
		{

		}

		public ISessionDataSupplier GetSessionClientData()
		{
			return m_serverDataSupplierFactory[DataParams.Empty];
		}

		public IOperatorDataSupplier GetOperatorDataSupplier()
		{
			return this.Session.GetOperatorDataSupplier();
		}

		#region IOperatorClientDataProvider Members

		public OperatorClientData GetOperatorClientData()
		{
			return m_operatorDataSupplierFactory[DataParams.Empty];
		}

		#endregion

		#region ISecurityProvider Members

		public ICollectionWithEvents<TssPrincipalInfo> Principals
		{
			get { throw new NotImplementedException(); }
		}

		#endregion

		protected override void ReConnect()
		{
			throw new NotImplementedException();
		}
	}
}