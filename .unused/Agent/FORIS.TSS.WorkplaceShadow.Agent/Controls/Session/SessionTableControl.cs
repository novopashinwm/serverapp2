﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Session
{
	public class SessionTableControl :
		TssUserControl,
		IViewObjectControl<SessionXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private TextColumn columnSessionId;
		private TextColumn columnLogin;
		private TextColumn columnLeaseTime;
		private TextColumn columnLeaseState;
		private SessionTableModel tableModel;
		private Table table;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public SessionTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Session.SessionTableModel( this.components );
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnSessionId = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLogin = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLeaseTime = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLeaseState = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// tableModel
			// 
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnSessionId,
            this.columnLogin,
            this.columnLeaseTime,
            this.columnLeaseState} );
			// 
			// columnSessionId
			// 
			this.columnSessionId.Editable = false;
			this.columnSessionId.Editor = null;
			this.columnSessionId.Text = "Session Id";
			// 
			// columnLogin
			// 
			this.columnLogin.Editable = false;
			this.columnLogin.Editor = null;
			this.columnLogin.Text = "Login";
			// 
			// columnLeaseTime
			// 
			this.columnLeaseTime.Editable = false;
			this.columnLeaseTime.Editor = null;
			this.columnLeaseTime.Text = "Lease time";
			// 
			// columnLeaseState
			// 
			this.columnLeaseState.Editable = false;
			this.columnLeaseState.Editor = null;
			this.columnLeaseState.Text = "Lease state";
			// 
			// table
			// 
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.Dock = System.Windows.Forms.DockStyle.Fill;
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Location = new System.Drawing.Point( 0, 0 );
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.Padding = new System.Windows.Forms.Padding( 5 );
			this.table.Size = new System.Drawing.Size( 320, 200 );
			this.table.TabIndex = 1;
			this.table.TableModel = this.tableModel;
			this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler( this.table_SelectionChanged );
			// 
			// SessionTableControl
			// 
			this.Controls.Add( this.table );
			this.Name = "SessionTableControl";
			this.Size = new System.Drawing.Size( 320, 200 );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );

		}

		#endregion  // Component Designer generated code

		#region IViewObjectControl<OwnersXpRow> Members

		public void RowShow( ViewObjectEventArgs<SessionXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<SessionXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<OwnersXpRow> Members

		#region  Handle controls events

		private void table_SelectionChanged( object sender, SelectionEventArgs e )
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				this.selectedSession = ( (SessionXpRow)this.table.SelectedItems[0] ).GetId();
			}
			else
			{
				this.selectedSession = 0;
			}
		}

		#endregion  // Handle controls events

		#region Properties

		private int selectedSession;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedSession
		{
			get { return this.selectedSession; }
			set
			{
				if ( this.tableModel.ContainsId( value ) )
				{
					this.tableModel.Selections.SelectCell(
						this.tableModel.IndexOf( value ),
						0
						);

					this.selectedSession = value;
				}
				else
				{
					this.tableModel.Selections.Clear();
				}
			}
		}

		#endregion  // Properties
	}
}