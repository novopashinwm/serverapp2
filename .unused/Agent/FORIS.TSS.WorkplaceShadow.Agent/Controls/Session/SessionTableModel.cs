﻿using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Session
{
	public class SessionTableModel :
		ViewObjectTableModel<SessionXpRow>
	{
		#region Constructor & Dispose

		public SessionTableModel( IContainer container )
		{
			container.Add( this );
		}
		public SessionTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}