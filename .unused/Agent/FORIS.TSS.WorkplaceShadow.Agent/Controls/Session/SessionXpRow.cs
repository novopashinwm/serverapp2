﻿using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Session
{
	public class SessionXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellSessionId;
		private readonly Cell cellLogin;
		private readonly Cell cellLeaseTime;
		private readonly Cell cellLeaseState;

		#endregion // Cells

		private readonly int ID;

		public SessionXpRow( 
			int id,
			string sessionId,
			string login,
			string leaseTime,
			string leaseState
			)
		{
			this.ID = id;

			#region Cells

			this.cellSessionId	= new Cell();
			this.cellLogin		= new Cell();
			this.cellLeaseTime	= new Cell();
			this.cellLeaseState	= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellSessionId,
						this.cellLogin,
						this.cellLeaseTime,
						this.cellLeaseState
					}
				);

			#endregion  // Cells

			this.cellSessionId.Text		= sessionId;
			this.cellLogin.Text			= login;
			this.cellLeaseTime.Text		= leaseTime;
			this.cellLeaseState.Text	= leaseState;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}