﻿using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Agent.Data.Session;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Session
{
	public class SessionFilter :
		Filter<SessionRow, ISessionData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Session.Rows.Inserted -= Session_Inserted;
			this.Data.Session.Rows.Removing -= Session_Removing;
		}

		protected override void OnAfterSetData()
		{
			this.Data.Session.Rows.Inserted += Session_Inserted;
			this.Data.Session.Rows.Removing += Session_Removing;
		}

		#endregion  // Data

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}

		protected override void OnBuildView()
		{
			foreach ( SessionRow row in this.Data.Session.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View

		#region Data events

		void Session_Inserted( object sender, CollectionChangeEventArgs<SessionRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}

		void Session_Removing( object sender, CollectionChangeEventArgs<SessionRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion  // Data events
	}
}