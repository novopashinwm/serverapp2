﻿using System;
using FORIS.TSS.Helpers.Agent.Data.Session;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Session
{
	public class SessionXpRowFactory :
		ViewObjectFactory<SessionRow, SessionXpRow, ISessionData>
	{
		public override SessionXpRow CreateViewObject(
			int viewObjectId,
			SessionRow sessionRow )
		{
			#region Preconditions

			if ( this.Data == null ) throw new ApplicationException( "There is no data to create new item" );

			#endregion  // Preconditions

			return
				new SessionXpRow(
					viewObjectId,
					sessionRow.SessionId.ToString(),
					sessionRow.Login,
					sessionRow.LeaseTime.ToString(),
					sessionRow.LeaseState.ToString()
					);
		}
	}
}