﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsFilter :
		Filter<DataRow, IData>
	{
		private int indexer;

		#region Data

		protected override void OnBeforeSetData()
		{
		}

		protected override void OnAfterSetData()
		{
		}

		#endregion  // Data

		#region View

		protected override void OnDestroyView()
		{
			if ( this.dataSet != null ) this.dataObjects.Clear();

			this.indexer = 1;
		}

		protected override void OnBuildView()
		{
			if ( this.dataSet != null )
			{
				foreach ( DataRow row in this.dataSet.Tables["SESSION_STAT"].Rows )
				{
					this.dataObjects.Add( this.indexer++ , row );
				}
			}
		}

		#endregion  // View

		private DataSet dataSet;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataSet DataSet
		{
			get { return this.dataSet; }
			set
			{
				this.OnDestroyView();

				this.dataSet = value;

				this.OnBuildView();
			}
		}
	}
}