﻿using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellHostIP;
		private readonly Cell cellHostName;
		private readonly Cell cellSessionStart;
		private readonly Cell cellSessionEnd;

		#endregion // Cells

		private readonly int ID;

		public StatisticsXpRow( 
			int id,
			string hostIP,
			string hostName,
			string sessionStart,
			string sessionEnd
			)
		{
			this.ID = id;

			#region Cells

			this.cellHostIP			= new Cell();
			this.cellHostName		= new Cell();
			this.cellSessionStart	= new Cell();
			this.cellSessionEnd		= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellHostIP,
						this.cellHostName,
						this.cellSessionStart,
						this.cellSessionEnd
					}
				);

			#endregion  // Cells

			this.cellHostIP.Text		= hostIP;
			this.cellHostName.Text		= hostName;
			this.cellSessionStart.Text	= sessionStart;
			this.cellSessionEnd.Text	= sessionEnd;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}