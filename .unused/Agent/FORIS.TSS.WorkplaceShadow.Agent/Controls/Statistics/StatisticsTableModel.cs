﻿using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsTableModel :
		ViewObjectTableModel<StatisticsXpRow>
	{
		#region Constructor & Dispose

		public StatisticsTableModel( IContainer container )
		{
			container.Add( this );
		}
		public StatisticsTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}