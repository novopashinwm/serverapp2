﻿using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsXpRowFactory :
		ViewObjectFactory<DataRow, StatisticsXpRow, IData>
	{
		public override StatisticsXpRow CreateViewObject( 
			int viewObjectId, 
			DataRow statisticsRow )
		{
			return
				new StatisticsXpRow(
					viewObjectId,
					statisticsRow["HOST_IP"].ToString(),
					statisticsRow["HOST_NAME"].ToString(),
					statisticsRow["SESSION_START"].ToString(),
					statisticsRow["SESSION_END"].ToString()
					);
		}
	}
}