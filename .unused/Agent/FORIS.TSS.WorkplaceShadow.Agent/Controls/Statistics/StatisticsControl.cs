﻿using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsControl : 
		UserControl,
		IClientItem<AgentClient>
	{
		#region Controls & Components

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private IContainer components;
		private TableLayoutPanel tableLayoutPanelMain;
		private TableLayoutPanel tableLayoutPanelOptions;
		private Label lblOperator;
		private Label lblFrom;
		private Label lblTo;
		private OperatorComboBox cbxOperator;
		private DateTimePicker dtpBeginTime;
		private DateTimePicker dtpEndTime;
		private Button btnGet;
		private StatisticsTableControl statisticsTableControl;
		private OperatorDataAmbassador daOperatorComboBox;
		private OperatorDataDispatcher operatorDataDispatcher;
		private StatisticsXpRowFactory statisticsXpRowFactory;
		private StatisticsFilter statisticsFilter;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public StatisticsControl()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.statisticsTableControl = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics.StatisticsTableControl();
			this.tableLayoutPanelOptions = new System.Windows.Forms.TableLayoutPanel();
			this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
			this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
			this.lblOperator = new System.Windows.Forms.Label();
			this.lblFrom = new System.Windows.Forms.Label();
			this.lblTo = new System.Windows.Forms.Label();
			this.cbxOperator = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorComboBox();
			this.btnGet = new System.Windows.Forms.Button();
			this.operatorDataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher( this.components );
			this.daOperatorComboBox = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.statisticsXpRowFactory = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics.StatisticsXpRowFactory();
			this.statisticsFilter = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics.StatisticsFilter();
			this.tableLayoutPanelMain.SuspendLayout();
			this.tableLayoutPanelOptions.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanelMain.ColumnCount = 2;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.Controls.Add( this.statisticsTableControl, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.tableLayoutPanelOptions, 0, 0 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 1;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 522, 375 );
			this.tableLayoutPanelMain.TabIndex = 0;
			// 
			// statisticsTableControl
			// 
			this.statisticsTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.statisticsTableControl.Location = new System.Drawing.Point( 219, 3 );
			this.statisticsTableControl.Name = "statisticsTableControl";
			this.statisticsTableControl.Size = new System.Drawing.Size( 300, 369 );
			this.statisticsTableControl.TabIndex = 0;
			// 
			// tableLayoutPanelOptions
			// 
			this.tableLayoutPanelOptions.AutoSize = true;
			this.tableLayoutPanelOptions.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanelOptions.ColumnCount = 2;
			this.tableLayoutPanelOptions.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanelOptions.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanelOptions.Controls.Add( this.dtpEndTime, 1, 2 );
			this.tableLayoutPanelOptions.Controls.Add( this.dtpBeginTime, 1, 1 );
			this.tableLayoutPanelOptions.Controls.Add( this.lblOperator, 0, 0 );
			this.tableLayoutPanelOptions.Controls.Add( this.lblFrom, 0, 1 );
			this.tableLayoutPanelOptions.Controls.Add( this.lblTo, 0, 2 );
			this.tableLayoutPanelOptions.Controls.Add( this.cbxOperator, 1, 0 );
			this.tableLayoutPanelOptions.Controls.Add( this.btnGet, 1, 3 );
			this.tableLayoutPanelOptions.Location = new System.Drawing.Point( 3, 3 );
			this.tableLayoutPanelOptions.Name = "tableLayoutPanelOptions";
			this.tableLayoutPanelOptions.RowCount = 4;
			this.tableLayoutPanelOptions.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelOptions.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelOptions.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelOptions.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelOptions.Size = new System.Drawing.Size( 210, 108 );
			this.tableLayoutPanelOptions.TabIndex = 1;
			// 
			// dtpEndTime
			// 
			this.dtpEndTime.CustomFormat = "dd.MM.yyyy HH:mm";
			this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpEndTime.Location = new System.Drawing.Point( 57, 56 );
			this.dtpEndTime.Name = "dtpEndTime";
			this.dtpEndTime.ShowUpDown = true;
			this.dtpEndTime.Size = new System.Drawing.Size( 111, 20 );
			this.dtpEndTime.TabIndex = 5;
			// 
			// dtpBeginTime
			// 
			this.dtpBeginTime.CustomFormat = "dd.MM.yyyy HH:mm";
			this.dtpBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpBeginTime.Location = new System.Drawing.Point( 57, 30 );
			this.dtpBeginTime.Name = "dtpBeginTime";
			this.dtpBeginTime.ShowUpDown = true;
			this.dtpBeginTime.Size = new System.Drawing.Size( 111, 20 );
			this.dtpBeginTime.TabIndex = 4;
			// 
			// lblOperator
			// 
			this.lblOperator.AutoSize = true;
			this.lblOperator.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblOperator.Location = new System.Drawing.Point( 3, 0 );
			this.lblOperator.Name = "lblOperator";
			this.lblOperator.Size = new System.Drawing.Size( 48, 27 );
			this.lblOperator.TabIndex = 0;
			this.lblOperator.Text = "Operator";
			this.lblOperator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblFrom
			// 
			this.lblFrom.AutoSize = true;
			this.lblFrom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblFrom.Location = new System.Drawing.Point( 3, 27 );
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.Size = new System.Drawing.Size( 48, 26 );
			this.lblFrom.TabIndex = 1;
			this.lblFrom.Text = "From";
			this.lblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblTo
			// 
			this.lblTo.AutoSize = true;
			this.lblTo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTo.Location = new System.Drawing.Point( 3, 53 );
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size( 48, 26 );
			this.lblTo.TabIndex = 2;
			this.lblTo.Text = "To";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbxOperator
			// 
			this.cbxOperator.Location = new System.Drawing.Point( 57, 3 );
			this.cbxOperator.Name = "cbxOperator";
			this.cbxOperator.Size = new System.Drawing.Size( 150, 21 );
			this.cbxOperator.TabIndex = 3;
			// 
			// btnGet
			// 
			this.btnGet.Location = new System.Drawing.Point( 57, 82 );
			this.btnGet.Name = "btnGet";
			this.btnGet.Size = new System.Drawing.Size( 75, 23 );
			this.btnGet.TabIndex = 6;
			this.btnGet.Text = "Get";
			this.btnGet.UseVisualStyleBackColor = true;
			this.btnGet.Click += new System.EventHandler( this.btnGet_Click );
			// 
			// operatorDataDispatcher
			// 
			this.operatorDataDispatcher.Ambassadors.Add( this.daOperatorComboBox );
			// 
			// daOperatorComboBox
			// 
			this.daOperatorComboBox.Item = this.cbxOperator;
			// 
			// statisticsXpRowFactory
			// 
			this.statisticsXpRowFactory.Data = null;
			this.statisticsXpRowFactory.Filter = this.statisticsFilter;
			this.statisticsXpRowFactory.ViewControl = this.statisticsTableControl;
			// 
			// StatisticsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "StatisticsControl";
			this.Size = new System.Drawing.Size( 522, 375 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.tableLayoutPanelOptions.ResumeLayout( false );
			this.tableLayoutPanelOptions.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (AgentClient)value; }
		}

		#endregion

		#region IClientItem<AdminClient> Members

		private AgentClient adminClient;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public AgentClient Client
		{
			get { return this.adminClient; }
			set
			{
				this.adminClient = value;

				if ( value != null )
				{
					this.operatorDataDispatcher.Data = value.GetOperatorClientData();
				}
			}
		}

		#endregion

		private void btnGet_Click( object sender, System.EventArgs e )
		{
			this.statisticsFilter.DataSet = 
				this.Client.Session.GetOperatorsSessionByDate(
					this.cbxOperator.SelectedId,
					this.dtpBeginTime.Value,
					this.dtpEndTime.Value
					);
		}
	}
}