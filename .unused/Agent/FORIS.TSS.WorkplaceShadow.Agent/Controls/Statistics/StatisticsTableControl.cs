﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics
{
	public class StatisticsTableControl :
		TssUserControl,
		IViewObjectControl<StatisticsXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private TextColumn columnHostIP;
		private TextColumn columnHostName;
		private TextColumn columnSessionBegin;
		private TextColumn columnSessionEnd;
		private StatisticsTableModel tableModel;
		private Table table;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public StatisticsTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.Agent.Controls.Statistics.StatisticsTableModel( this.components );
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnHostIP = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnHostName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnSessionBegin = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnSessionEnd = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// tableModel
			// 
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnHostIP,
            this.columnHostName,
            this.columnSessionBegin,
            this.columnSessionEnd} );
			// 
			// columnHostIP
			// 
			this.columnHostIP.Editable = false;
			this.columnHostIP.Editor = null;
			this.columnHostIP.Text = "Host IP";
			// 
			// columnHostName
			// 
			this.columnHostName.Editable = false;
			this.columnHostName.Editor = null;
			this.columnHostName.Text = "Host name";
			// 
			// columnSessionBegin
			// 
			this.columnSessionBegin.Editable = false;
			this.columnSessionBegin.Editor = null;
			this.columnSessionBegin.Text = "Session begin";
			// 
			// columnSessionEnd
			// 
			this.columnSessionEnd.Editable = false;
			this.columnSessionEnd.Editor = null;
			this.columnSessionEnd.Text = "Session end";
			// 
			// table
			// 
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.Dock = System.Windows.Forms.DockStyle.Fill;
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Location = new System.Drawing.Point( 5, 5 );
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.Padding = new System.Windows.Forms.Padding( 5 );
			this.table.Size = new System.Drawing.Size( 310, 190 );
			this.table.TabIndex = 1;
			this.table.TableModel = this.tableModel;
			// 
			// StatisticsTableControl
			// 
			this.Controls.Add( this.table );
			this.Name = "StatisticsTableControl";
			this.Padding = new System.Windows.Forms.Padding( 5 );
			this.Size = new System.Drawing.Size( 320, 200 );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );

		}

		#endregion  // Component Designer generated code

		#region IViewObjectControl<OwnersXpRow> Members

		public void RowShow( ViewObjectEventArgs<StatisticsXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<StatisticsXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<OwnersXpRow> Members
	}
}