﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Agent
{
	public class AgentClientDispatcher :
		ClientDispatcher<AgentClient>
	{
		public AgentClientDispatcher()
		{

		}

		public AgentClientDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}

		private readonly AgentClientAmbassadorCollection ambassadors =
			new AgentClientAmbassadorCollection();

		protected override AmbassadorCollection<IClientItem<AgentClient>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new AgentClientAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
	}

	public class AgentClientAmbassadorCollection :
		ClientAmbassadorCollection<AgentClient>
	{
		public new AgentClientAmbassador this[int index]
		{
			get { return (AgentClientAmbassador)base[index]; }
		}
	}

	public class AgentClientAmbassador :
		ClientAmbassador<AgentClient>
	{

	}
}