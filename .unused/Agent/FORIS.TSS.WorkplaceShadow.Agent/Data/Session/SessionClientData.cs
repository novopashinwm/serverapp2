﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Agent.Data.Session;
using FORIS.TSS.Interfaces.Agent.Data.Session;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceShadow.Agent.Data.Session
{
	/// <summary>
	/// Поставщик данных стороны клиента
	/// модуля "Профили Бригад"
	/// </summary>
	public class SessionClientData :
		SessionData
	{
		#region Constructor & Dispose

		public SessionClientData(
			AgentClient agentClient
			)
		{
			this.dataSupplier = new SessionClientDataSupplier( agentClient );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion  // Construtor & Dispose

		/// <summary>
		/// Используется для получения ссылки объекта DataTreater
		/// </summary>
		private readonly ISessionDataSupplier dataSupplier;

		public IDataSupplier SecurityDataSupplier
		{
			get { throw new NotSupportedException(); }
		}
	}

	internal class SessionClientDataSupplier :
		ClientDataSupplier<AgentClient, ISessionDataSupplier, ISessionDataTreater>,
		ISessionDataSupplier
	{
		public SessionClientDataSupplier( AgentClient agentClient )
			: base( agentClient, agentClient.Session.GetSessionDataSupplier() )
		{

		}
	}
}