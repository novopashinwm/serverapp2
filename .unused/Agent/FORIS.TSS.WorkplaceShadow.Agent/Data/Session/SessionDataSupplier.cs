﻿using System.ComponentModel;
using FORIS.TSS.Interfaces.Agent.Data.Session;
using FORIS.TSS.WorkplaceShadow.Data;

namespace FORIS.TSS.WorkplaceShadow.Agent.Data.Session
{
	/// <summary>
	/// Поставщик данных стороны клиента
	/// </summary>
	public class SessionDataSupplier :
		ModuleDataSupplier<AgentClient, ISessionDataSupplier>,
		ISessionDataSupplier
	{
		#region Constructor & Dispose

		public SessionDataSupplier( IContainer container )
		{
			container.Add( this );
		}

		public SessionDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

		public ISessionDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override ISessionDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetSessionClientData();
		}
	}
}