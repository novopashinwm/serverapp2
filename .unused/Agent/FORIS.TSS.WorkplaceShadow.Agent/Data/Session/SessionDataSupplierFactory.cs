﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceShadow.Agent.Data.Session;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceShadow.Agent.Data.Session
{
	internal class SessionDataSupplierFactory :
		ClientDataSupplierFactory<AgentClient, SessionClientData, DataParams>
	{
		public SessionDataSupplierFactory( AgentClient client )
			: base( client )
		{

		}

		protected override SessionClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if ( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			SessionClientData Supplier =
				new SessionClientData( this.Client );

			return Supplier;
		}
	}
}