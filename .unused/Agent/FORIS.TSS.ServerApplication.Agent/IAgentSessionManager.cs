﻿using FORIS.TSS.Helpers;
using FORIS.TSS.Interfaces.Operator.Data;
using FORIS.TSS.ServerApplication.Agent.Data;

namespace FORIS.TSS.ServerApplication.Agent
{
	public interface IAgentSessionManager :
		ISessionServerDataProvider,
		IOperatorDataSupplierProvider
	{
		IDatabaseDataSupplier DatabaseDataSupplier { get; }
		void CloseSession(int sessionId);
	}
}