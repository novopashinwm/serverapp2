﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Agent;
using FORIS.TSS.Interfaces.Agent.Data.Session;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Agent.Data.Session;

namespace FORIS.TSS.ServerApplication.Agent
{
	/// <summary> Класс объекта административной сессии </summary>
	/// <remarks> Предоставляет доступ к поставщику данных сервера </remarks>
	public class AgentPersonalServer :
		Component,
		IAgentPersonalServer
	{
		#region Constructor & Dispose

		public AgentPersonalServer(IAgentSessionManager sessionManager, ISessionInfo sessionInfo)
		{
			this.sessionManager = sessionManager;
			this.sessionInfo    = sessionInfo;
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Close();
				RemotingServices.Disconnect(this);
			}
			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Initialize

		private ILease lease;

		public override object InitializeLifetimeService()
		{
			this.lease = (ILease)base.InitializeLifetimeService();
			return this.lease;
		}

		#endregion // Initialize

		private readonly IAgentSessionManager sessionManager;
		private readonly ISessionInfo         sessionInfo;

		#region IAgentPersonalServer Members

		public ISessionDataSupplier GetSessionDataSupplier()
		{
			return new SessionPersonalDataSupplier(sessionManager.SessionData, this);
		}
		public IOperatorDataSupplier GetOperatorDataSupplier()
		{
			return this.sessionManager.GetOperatorDataSupplier();
		}
		public IList<string> ResetStoredProceduresParametersCache()
		{

			IList<string> result =
				this.sessionManager.DatabaseDataSupplier.ParametersCache.Names;

			this.sessionManager.DatabaseDataSupplier.ParametersCache.Reset();

			return result;
		}

		public DataSet GetOperatorsSessionByDate( int operator_id, DateTime from, DateTime to )
		{
			using ( StoredProcedure sp =
					  Globals.TssDatabaseManager.DefaultDataBase.Procedure( "dbo.GetOperatorsSessionByDate" ) )
			{
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				sp["@OperatorID"].Value = operator_id;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(
					dataSet,
					new string[] { "SESSION_STAT" }
				);
				DatasetHelperBase.Dump( dataSet );

				return dataSet;
			}
		}

		#endregion // IAgentPersonalServer Members

		#region IPersonalServer Members

		public string ApplicationName
		{
			get { throw new NotSupportedException(); }
		}

		public ISessionInfo SessionInfo
		{
			get { return this.sessionInfo; }
		}

		private bool closed = false;

		/// <summary> Закрыть сессию </summary>
		public void Close()
		{
			if (!this.closed)
			{
				this.OnClosing();
				this.closed = true;
				this.OnClosed();
				this.Dispose(true);
			}
		}

		TimeSpan IPersonalServer.LeaseRenewOnCallTime
		{
			get
			{
				if (this.lease == null)
				{
					throw new InvalidOperationException("Call this property only through remoting");
				}

				return this.lease.RenewOnCallTime;
			}
		}
		/// <summary> Метод ничего не делает. Нужен только для проверки жизнеспособности </summary>
		/// <returns></returns>
		public bool IsAlive()
		{
			return true;
		}
		public ISecurity Security
		{
			get { return null; }
		}
		public void Update(DataSet dataSet)
		{
			throw new NotImplementedException();
		}
		/// <summary> Возвращает запись из таблицы по условию </summary>
		/// <param name="tablename"></param>
		/// <returns></returns>
		/// <param name="where"></param>
		public DataSet GetRecords(string tablename, string where)
		{
			throw new NotImplementedException();
		}
		/// <summary> Проверяет совпадает ли указанный пароль с паролем оператора сессии </summary>
		/// <param name="password"></param>
		/// <returns></returns>
		public bool CheckPassword(string password)
		{
			throw new NotImplementedException();
		}
		/// <summary> Изменяет/задает собственный пароль оператора </summary>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае
		/// если пользователю не разрешено менять
		/// собственный пароль
		/// </remarks>
		public void SetPassword(string password)
		{
			throw new NotImplementedException();
		}
		/// <summary> Изменяет/задает пароль оператора </summary>
		/// <param name="idOperator"></param>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае,
		/// если пользователю не разрешено менять
		/// пароль указанного пользователя
		/// </remarks>
		public void SetPassword(string password, int idOperator)
		{
			throw new NotImplementedException();
		}
		public SetNewLoginResult SetNewLogin(string newLogin)
		{
			throw new NotImplementedException();
		}
		public IList<TssPrincipalInfo> GetPrincipals()
		{
			throw new NotImplementedException();
		}
		/// <summary> Получение списков контроля доступа объекта </summary>
		/// <param name="info"></param>
		/// <returns></returns>
		public ISecurityModel GetACL(SecureObjectInfo info)
		{
			throw new NotImplementedException();
		}
		/// <summary> Применение списка контроля доступа объекта </summary>
		/// <param name="info"></param>
		/// <param name="security"></param>
		public void SetACL(SecureObjectInfo info, ISecurityModel security)
		{
			throw new NotImplementedException();
		}
		void IPersonalServer.ServerGC()
		{
			GC.Collect();
			GC.Collect();
		}
		public TssPrincipalInfo GetPrincipal()
		{
			return default(TssPrincipalInfo);
		}
		public IPersonalServer GetAdmin()
		{
			return this;
		}
		/// <summary> Returns tables for calculating access from Operators to Vehicles and Routes </summary>
		/// <returns></returns>
		public DataSet GetSecurityDataset()
		{
			return null;
		}

		#endregion  // IPersonalServer Members

		#region ISponsor Members

		///<summary>
		///Requests a sponsoring client to renew the lease for the specified object.
		///</summary>
		///<returns>
		///The additional lease time for the specified object.
		///</returns>
		///<param name="lease">The lifetime lease of the object that requires lease renewal. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public TimeSpan Renewal(ILease lease)
		{
			if (!this.closed)
				return lease.InitialLeaseTime;
			else
				return new TimeSpan(0);
		}

		#endregion

		#region Closing & Closed

		private       EventHandler closingDelegate;
		public  event EventHandler Closing
		{
			add    { closingDelegate += value; }
			remove { closingDelegate -= value; }
		}
		protected virtual void OnClosing()
		{
			closingDelegate?.Invoke(this, EventArgs.Empty);
		}
		private       EventHandler closedDelegate;
		public  event EventHandler Closed
		{
			add    { closedDelegate += value; }
			remove { closedDelegate -= value; }
		}
		protected virtual void OnClosed()
		{
			closedDelegate?.Invoke(this, EventArgs.Empty);
		}
		#endregion // Closing & Closed
		public void CloseSession(int sessionId)
		{
			sessionManager.CloseSession(sessionId);
		}

		#region IAgentPersonalServer Members

		IList<string> IAgentPersonalServer.ResetStoredProceduresParametersCache()
		{
			throw new NotImplementedException();
		}
		void IAgentPersonalServer.CloseSession(int sessionId)
		{
			throw new NotImplementedException();
		}
		DataSet IAgentPersonalServer.GetOperatorsSessionByDate(int operator_id, DateTime from, DateTime to)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IPersonalServer Members

		string IPersonalServer.ApplicationName
		{
			get { throw new NotImplementedException(); }
		}
		ISessionInfo IPersonalServer.SessionInfo
		{
			get { throw new NotImplementedException(); }
		}
		void IPersonalServer.Close()
		{
			throw new NotImplementedException();
		}
		event EventHandler IPersonalServer.Closing
		{
			add    { throw new NotImplementedException(); }
			remove { throw new NotImplementedException(); }
		}

		bool IPersonalServer.IsAlive()
		{
			throw new NotImplementedException();
		}

		ISecurity IPersonalServer.Security
		{
			get { throw new NotImplementedException(); }
		}

		void IPersonalServer.Update(DataSet dataSet)
		{
			throw new NotImplementedException();
		}

		bool IPersonalServer.CheckPassword(string password)
		{
			throw new NotImplementedException();
		}

		SetNewPasswordResult IPersonalServer.SetPassword(string password)
		{
			throw new NotImplementedException();
		}

		void IPersonalServer.SetPassword(string password, int idOperator)
		{
			throw new NotImplementedException();
		}
		ISecurityModel IPersonalServer.GetACL(SecureObjectInfo info)
		{
			throw new NotImplementedException();
		}
		void IPersonalServer.SetACL(SecureObjectInfo info, ISecurityModel security)
		{
			throw new NotImplementedException();
		}
		TssPrincipalInfo IPersonalServer.GetPrincipal()
		{
			throw new NotImplementedException();
		}
		DataSet IPersonalServer.GetSecurityDataset()
		{
			throw new NotImplementedException();
		}

		#endregion

		#region ISponsor Members

		TimeSpan ISponsor.Renewal(ILease lease)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region ISessionDataSupplierProvider Members

		ISessionDataSupplier FORIS.TSS.Interfaces.Agent.Data.ISessionDataSupplierProvider.GetSessionDataSupplier()
		{
			throw new NotImplementedException();
		}

		#endregion

		#region IOperatorDataSupplierProvider Members

		IOperatorDataSupplier FORIS.TSS.Interfaces.Operator.Data.IOperatorDataSupplierProvider.GetOperatorDataSupplier()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}