﻿using System.ComponentModel;
using System.Linq;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Helpers;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Agent.Data.Session;
using FORIS.TSS.ServerApplication.Operator;

namespace FORIS.TSS.ServerApplication.Agent
{
	public abstract class AgentSessionManager<TServer> :
		SessionManager<TServer>,
		IAgentSessionManager
		where TServer : ServerBase<TServer>, IOperatorServerDataProvider
	{
		private IContainer components;
		private SessionServerData sessionData;

		public AgentSessionManager()
		{
			this.components  = new Container();
			this.sessionData = new SessionServerData(this.components);
		}
		protected override void OnCreateSession(IPersonalServer session)
		{
			base.OnCreateSession(session);
			using (SessionDataTreater treater = new SessionDataTreater(this.sessionData, null))
			{
				/* В это время объект сессии создан, но еще ни разу
				 * не был передан через инфраструктуру ремотинга
				 * и поэтому для него еще не было создано объекта
				 * лицензии управления временем жизни
				 */
				treater.SessionInsert(session.SessionInfo.SessionID, session.SessionInfo.OperatorInfo.Login);
			}
		}
		protected override void OnCloseSession(PersonalServerBase<TServer> session)
		{
			base.OnCloseSession(session);
			using (SessionDataTreater treater = new SessionDataTreater(this.sessionData, null))
			{
				treater.SessionDelete(session.SessionInfo.SessionID);
			}
		}
		protected override void OnUpdateSessionLease(System.Collections.Generic.Dictionary<int, System.Runtime.Remoting.Lifetime.ILease> sessionLeases)
		{
			base.OnUpdateSessionLease(sessionLeases);
			using (SessionDataTreater treater = new SessionDataTreater(this.sessionData, null))
			{
				treater.UpdateSessionLeaseTimes(sessionLeases);
			}
		}

		#region IAgentSessionManager Members
		public SessionServerData SessionData
		{
			get { return this.sessionData; }
		}
		public IOperatorDataSupplier GetOperatorDataSupplier()
		{
			return this.Server.OperatorData;
		}
		public void CloseSession(int sessionId)
		{
			lock (this.sessionHash)
			{
				sessionHash.Values
					?.FirstOrDefault(s => s.Target?.SessionID == sessionId)
					?.Target
					?.Close();
			}
		}

		#endregion

		#region IDatabaseProvider Members

		public IDatabaseDataSupplier Database
		{
			get { throw new System.NotImplementedException(); }
		}

		#endregion
	}
}