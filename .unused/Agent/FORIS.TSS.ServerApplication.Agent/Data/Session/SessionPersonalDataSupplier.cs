﻿using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Agent.Data.Session;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Agent.Data.Session
{
	public class SessionPersonalDataSupplier :
		PersonalDataSupplier<SessionServerData, ISessionDataTreater>,
		ISessionDataSupplier
	{
		public SessionPersonalDataSupplier(
			SessionServerData sessionData,
			IPersonalServer personalServer
			)
			: base(sessionData, personalServer)
		{
			this.sessionData = sessionData;
		}

		private readonly SessionServerData sessionData;

		public override ISessionDataTreater GetDataTreater()
		{
			return
				new SessionDataTreater(
					this.sessionData,
					this.PersonalServer.SessionInfo
					);
		}
	}
}