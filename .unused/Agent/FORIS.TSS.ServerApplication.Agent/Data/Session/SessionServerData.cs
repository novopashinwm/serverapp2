﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Agent.Data.Session;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Agent.Data.Session;

namespace FORIS.TSS.ServerApplication.Agent.Data.Session
{
	public class SessionServerData :
		SessionData
	{
		public SessionServerData(IContainer container)
			: this()
		{
			container.Add(this);
		}
		public SessionServerData()
		{
			this.DataSupplier = new SessionCustomDataSupplier();
		}
	}

	public class SessionCustomDataSupplier :
		FinalDataSupplier,
		IDataSupplier<ISessionDataTreater>
	{
		#region IDataSupplier<ISessionDataTreater> Members

		public ISessionDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}

		#endregion IDataSupplier<ISessionDataTreater> Members

		protected override DataInfo GetData()
		{
			return new DataInfo(new SessionDataSet(), Guid.NewGuid());
		}
	}
}