﻿using FORIS.TSS.ServerApplication.Agent.Data.Session;

namespace FORIS.TSS.ServerApplication.Agent.Data
{
	public interface ISessionServerDataProvider :
		IDatabaseProvider
	{
		SessionServerData SessionData { get; }
	}
}