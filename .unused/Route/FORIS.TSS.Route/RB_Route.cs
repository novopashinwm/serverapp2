using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Route.Controls.Route;
using FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup;
using FORIS.TSS.WorkplaceShadow.Route.Data.Route;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Route;

namespace FORIS.TSS.Route
{
    public class RB_Route : 
		PluginForm,
        IClientItem<IRouteClientDataProvider>
	{
		#region Control & Components

		private IContainer components;
        private RouteDataDispatcher routeDataDispatcher;
		private RouteData routeData;
		private RouteTableControl routeTableControl;
        private RouteClientDataProviderDispatcher clientDispatcher;
        private RouteDataSupplier routeDataSupplier;
		private RouteClientDataProviderAmbassador caRouteDataSupplier;
    	private RouteFilterDefault routeFilterDefault;
    	private RouteXpRowFactory routeXpRowFactory;
		private RouteDataAmbassador daRouteFilter;
		private RouteDataAmbassador daRouteEditorProvider;
    	private RouteEditorProvider routeEditorProvider;
		private TableLayoutPanel tableLayoutPanel1;
		private RouteGroupTableControl routeGroupTableControl;
		private RoutePropertiesControl routePropertiesControl;
		private RouteGroupXpRowFactory routeGroupXpRowFactory;
		private RouteGroupFilterDefault routeGroupFilter;
    	private RouteGroupEditorProvider routeGroupEditorProvider;
		private RouteDataAmbassador daRoutePropertiesControl;
		private RouteDataAmbassador daRouteGroupXpRowFactory;
		private RouteDataAmbassador daRouteGroupFilter;
		private RouteDataAmbassador daRouteGroupEditorProvider;
		private TableLayoutPanel tableLayoutPanelMain;

		#endregion  // Control & Components

		#region Constructor & Dispose

		public RB_Route()
		{
			InitializeComponent();

			this.routePropertiesControl.Mode = DataItemControlMode.View;
		}

		#endregion  //Constructor & Dispose

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.routeDataDispatcher = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataDispatcher( this.components );
			this.daRoutePropertiesControl = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.routePropertiesControl = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RoutePropertiesControl();
			this.daRouteFilter = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.routeFilterDefault = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RouteFilterDefault();
			this.daRouteEditorProvider = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.routeEditorProvider = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RouteEditorProvider( this.components );
			this.daRouteGroupXpRowFactory = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.routeGroupXpRowFactory = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupXpRowFactory();
			this.routeGroupFilter = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupFilterDefault();
			this.routeGroupTableControl = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupTableControl();
			this.daRouteGroupFilter = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.routeTableControl = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RouteTableControl();
			this.routeData = new FORIS.TSS.Helpers.Route.Data.Route.RouteData( this.components );
			this.routeDataSupplier = new FORIS.TSS.WorkplaceShadow.Route.Data.Route.RouteDataSupplier( this.components );
			this.clientDispatcher = new FORIS.TSS.Route.RouteClientDataProviderDispatcher( this.components );
			this.caRouteDataSupplier = new FORIS.TSS.Route.RouteClientDataProviderAmbassador();
			this.routeGroupEditorProvider = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupEditorProvider( this.components );
			this.routeXpRowFactory = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RouteXpRowFactory();
			this.daRouteGroupEditorProvider = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.tableLayoutPanelMain.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// routeDataDispatcher
			// 
			this.routeDataDispatcher.Ambassadors.Add( this.daRoutePropertiesControl );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteFilter );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteEditorProvider );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteGroupXpRowFactory );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteGroupFilter );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteGroupEditorProvider );
			// 
			// daRoutePropertiesControl
			// 
			this.daRoutePropertiesControl.Item = this.routePropertiesControl;
			// 
			// routePropertiesControl
			// 
			this.routePropertiesControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.routePropertiesControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.routePropertiesControl.Enabled = false;
			this.routePropertiesControl.Location = new System.Drawing.Point( 488, 3 );
			this.routePropertiesControl.Name = "routePropertiesControl";
			this.routePropertiesControl.Size = new System.Drawing.Size( 202, 343 );
			this.routePropertiesControl.TabIndex = 2;
			// 
			// daRouteFilter
			// 
			this.daRouteFilter.Item = this.routeFilterDefault;
			// 
			// daRouteEditorProvider
			// 
			this.daRouteEditorProvider.Item = this.routeEditorProvider;
			// 
			// daRouteGroupXpRowFactory
			// 
			this.daRouteGroupXpRowFactory.Item = this.routeGroupXpRowFactory;
			// 
			// routeGroupXpRowFactory
			// 
			this.routeGroupXpRowFactory.Filter = this.routeGroupFilter;
			this.routeGroupXpRowFactory.ViewControl = this.routeGroupTableControl;
			// 
			// routeGroupTableControl
			// 
			this.routeGroupTableControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			// 
			// 
			// 
			this.routeGroupTableControl.ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.routeGroupTableControl.ToolBar.Divider = false;
			this.routeGroupTableControl.ToolBar.DropDownArrows = true;
			this.routeGroupTableControl.ToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.routeGroupTableControl.ToolBar.Location = new System.Drawing.Point( 0, 0 );
			this.routeGroupTableControl.ToolBar.Name = "globalToolbar";
			this.routeGroupTableControl.ToolBar.ShowToolTips = true;
			this.routeGroupTableControl.ToolBar.Size = new System.Drawing.Size( 473, 26 );
			this.routeGroupTableControl.ToolBar.TabIndex = 0;
			this.routeGroupTableControl.Controls.Add( this.routeGroupTableControl.ToolBar );
			this.routeGroupTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.routeGroupTableControl.EditorProvider = this.routeGroupEditorProvider;
			this.routeGroupTableControl.Location = new System.Drawing.Point( 3, 174 );
			this.routeGroupTableControl.Name = "routeGroupTableControl";
			this.routeGroupTableControl.Size = new System.Drawing.Size( 473, 166 );
			this.routeGroupTableControl.TabIndex = 1;
			this.routeGroupTableControl.ToolBarVisible = true;
			// 
			// daRouteGroupFilter
			// 
			this.daRouteGroupFilter.Item = this.routeGroupFilter;
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.ColumnCount = 2;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 70F ) );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 30F ) );
			this.tableLayoutPanelMain.Controls.Add( this.tableLayoutPanel1, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.routePropertiesControl, 1, 0 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 1;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 349F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 693, 349 );
			this.tableLayoutPanelMain.TabIndex = 1;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanel1.Controls.Add( this.routeTableControl, 0, 0 );
			this.tableLayoutPanel1.Controls.Add( this.routeGroupTableControl, 0, 1 );
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point( 3, 3 );
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 50F ) );
			this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 50F ) );
			this.tableLayoutPanel1.Size = new System.Drawing.Size( 479, 343 );
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// routeTableControl
			// 
			// 
			// 
			// 
			this.routeTableControl.ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.routeTableControl.ToolBar.Divider = false;
			this.routeTableControl.ToolBar.DropDownArrows = true;
			this.routeTableControl.ToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.routeTableControl.ToolBar.Location = new System.Drawing.Point( 0, 0 );
			this.routeTableControl.ToolBar.Name = "globalToolbar";
			this.routeTableControl.ToolBar.ShowToolTips = true;
			this.routeTableControl.ToolBar.Size = new System.Drawing.Size( 473, 26 );
			this.routeTableControl.ToolBar.TabIndex = 0;
			this.routeTableControl.Controls.Add( this.routeTableControl.ToolBar );
			this.routeTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.routeTableControl.EditorProvider = this.routeEditorProvider;
			this.routeTableControl.Location = new System.Drawing.Point( 3, 3 );
			this.routeTableControl.Name = "routeTableControl";
			this.routeTableControl.Size = new System.Drawing.Size( 473, 165 );
			this.routeTableControl.TabIndex = 0;
			this.routeTableControl.ToolBarVisible = true;
			this.routeTableControl.SelectionChanged += new System.EventHandler( this.routeTableControl_SelectionChanged );
			// 
			// routeData
			// 
			this.routeData.DataSupplier = this.routeDataSupplier;
			this.routeData.InvokeBroker = this.invokeBroker;
			this.routeData.Loaded += new System.EventHandler( this.ownersData_Loaded );
			this.routeData.Loading += new System.EventHandler( this.ownersData_Loading );
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.Ambassadors.Add( this.caRouteDataSupplier );
			// 
			// caRouteDataSupplier
			// 
			this.caRouteDataSupplier.Item = this.routeDataSupplier;
			// 
			// routeXpRowFactory
			// 
			this.routeXpRowFactory.Filter = this.routeFilterDefault;
			this.routeXpRowFactory.ViewControl = this.routeTableControl;
			// 
			// daRouteGroupEditorProvider
			// 
			this.daRouteGroupEditorProvider.Item = this.routeGroupEditorProvider;
			// 
			// RB_Route
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.ClientSize = new System.Drawing.Size( 693, 349 );
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "RB_Route";
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanel1.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion  // Windows Form Designer generated code

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IRouteClientDataProvider)value; }
		}

		#endregion  // IClientItem Members

		#region IClientItem<IRouteClientDataProvider> Members

		public IRouteClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set { this.clientDispatcher.Client = value; }
		}

		#endregion  // IClientItem<IRouteClientDataProvider> Members

		#region Handle routeData events

		void ownersData_Loading(object sender, System.EventArgs e)
        {
            this.routeDataDispatcher.Data = null;
        }

        void ownersData_Loaded(object sender, System.EventArgs e)
        {
            this.routeDataDispatcher.Data = this.routeData;
		}

		#endregion  // Handle routeData events

		private void routeTableControl_SelectionChanged( object sender, System.EventArgs e )
		{
			this.routePropertiesControl.IdRoute = this.routeTableControl.SelectedRoute;
		}
	}
}