using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Route;

namespace FORIS.TSS.Route
{
    class RouteClientDataProviderDispatcher:
		ClientDispatcher<IRouteClientDataProvider>
    {
        public RouteClientDataProviderDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
        public RouteClientDataProviderDispatcher()
		{
		}

        private readonly RouteClientDataProviderAmbassadorCollection ambassadors =
            new RouteClientDataProviderAmbassadorCollection();

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RouteClientDataProviderAmbassadorCollection Ambassadors
        {
            get { return this.ambassadors; }
        }

        protected override AmbassadorCollection<IClientItem<IRouteClientDataProvider>> GetAmbassadorCollection()
        {
            return this.ambassadors;
        }
    }

    internal class RouteClientDataProviderAmbassadorCollection :
        ClientAmbassadorCollection<IRouteClientDataProvider>
    {
        public new RouteClientDataProviderAmbassador this[int index]
        {
            get { return (RouteClientDataProviderAmbassador)base[index]; }
        }
    }

    internal class RouteClientDataProviderAmbassador :
        ClientAmbassador<IRouteClientDataProvider>
    {
    }
}