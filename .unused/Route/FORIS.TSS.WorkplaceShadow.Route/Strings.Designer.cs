﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FORIS.TSS.WorkplaceShadow.Route {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FORIS.TSS.WorkplaceShadow.Route.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data error(s):
        ///{0}.
        /// </summary>
        internal static string Message_DataErrors {
            get {
                return ResourceManager.GetString("Message_DataErrors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No data for edit.
        /// </summary>
        internal static string NoDataForEdit {
            get {
                return ResourceManager.GetString("NoDataForEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not create new Route
        ///Exception: {0}.
        /// </summary>
        internal static string RouteEditorProvider_Exception_CanNotCreateNewRoute {
            get {
                return ResourceManager.GetString("RouteEditorProvider_Exception_CanNotCreateNewRoute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not delete Route
        ///Exception: {0}.
        /// </summary>
        internal static string RouteEditorProvider_Exception_CanNotDeleteRoute {
            get {
                return ResourceManager.GetString("RouteEditorProvider_Exception_CanNotDeleteRoute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not update Route
        ///Exception: {0}.
        /// </summary>
        internal static string RouteEditorProvider_Exception_CanNotUpdateRoute {
            get {
                return ResourceManager.GetString("RouteEditorProvider_Exception_CanNotUpdateRoute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete Route.
        /// </summary>
        internal static string RouteEditorProvider_Warning_AreYouSureToDeleteRoute {
            get {
                return ResourceManager.GetString("RouteEditorProvider_Warning_AreYouSureToDeleteRoute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not create new RouteGroup
        ///Exception: {0}.
        /// </summary>
        internal static string RouteGroupEditorProvider_Exception_CanNotCreateNewRouteGroup {
            get {
                return ResourceManager.GetString("RouteGroupEditorProvider_Exception_CanNotCreateNewRouteGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not delete RouteGroup
        ///Exception: {0}.
        /// </summary>
        internal static string RouteGroupEditorProvider_Exception_CanNotDeleteRouteGroup {
            get {
                return ResourceManager.GetString("RouteGroupEditorProvider_Exception_CanNotDeleteRouteGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not update RouteGroup
        ///Exception: {0}.
        /// </summary>
        internal static string RouteGroupEditorProvider_Exception_CanNotUpdateRouteGroup {
            get {
                return ResourceManager.GetString("RouteGroupEditorProvider_Exception_CanNotUpdateRouteGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete RouteGroup.
        /// </summary>
        internal static string RouteGroupEditorProvider_Warning_AreYouSureToDeleteRouteGroup {
            get {
                return ResourceManager.GetString("RouteGroupEditorProvider_Warning_AreYouSureToDeleteRouteGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name required.
        /// </summary>
        internal static string RouteGroupPropertiesForm_Warning_NameRequired {
            get {
                return ResourceManager.GetString("RouteGroupPropertiesForm_Warning_NameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Length required.
        /// </summary>
        internal static string RoutePropertiesForm_Warning_LengthRequired {
            get {
                return ResourceManager.GetString("RoutePropertiesForm_Warning_LengthRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name required.
        /// </summary>
        internal static string RoutePropertiesForm_Warning_NameRequired {
            get {
                return ResourceManager.GetString("RoutePropertiesForm_Warning_NameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number required.
        /// </summary>
        internal static string RoutePropertiesForm_Warning_NumberRequired {
            get {
                return ResourceManager.GetString("RoutePropertiesForm_Warning_NumberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RouteType required.
        /// </summary>
        internal static string RoutePropertiesForm_Warning_RouteTypeRequired {
            get {
                return ResourceManager.GetString("RoutePropertiesForm_Warning_RouteTypeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        internal static string Warning {
            get {
                return ResourceManager.GetString("Warning", resourceCulture);
            }
        }
    }
}
