﻿using System.ComponentModel;
using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Route;

namespace FORIS.TSS.WorkplaceShadow.Route.Data.Route
{
	/// <summary>
	/// Поставщик данных правил для модулей
	/// </summary>
	public class RouteDataSupplier :
		ModuleDataSupplier<IRouteClientDataProvider, IRouteDataSupplier>,
		IRouteDataSupplier
	{
		#region Constructor & Dispose

		public RouteDataSupplier( IContainer container )
		{
			container.Add( this );
		}

		public RouteDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

		public IRouteDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IRouteDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetRouteClientData();
		}
	}
}