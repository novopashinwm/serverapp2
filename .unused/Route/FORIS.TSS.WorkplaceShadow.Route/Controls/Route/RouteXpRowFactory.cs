using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteXpRowFactory :
		ViewObjectFactory<RouteRow, RouteXpRow, IRouteData>
	{
		public override RouteXpRow CreateViewObject( int viewObjectId, RouteRow routeRow )
		{
			return
				new RouteXpRow(
					viewObjectId,
					routeRow.RowRouteType.Name,
					routeRow.Number,
					routeRow.Name,
					routeRow.Length.ToString(),
					routeRow.Description,
					routeRow.Color
					);
		}
	}
}