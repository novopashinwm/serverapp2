using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	internal class RoutePropertiesForm :
		TssUserForm,
		IDataItem<IRouteData>
	{
		#region Controls & Components

		private new IContainer components;
		private TabControl tcMain;
		private TabPage tpCommon;
		private TabPage tpGroups;
		private RoutePropertiesControl ctrlRouteProperties;
		private RouteGroupTableControl ctrlRouteGroupTable;
		private RouteDataDispatcher routeDataDispatcher;
		private RouteGroupXpRowFactory routeGroupXpRowFactory;
		private RouteDataAmbassador daRouteGroupXpRowFactory;
		private RouteDataAmbassador daRouteGroupFilter;
		private RouteDataAmbassador daRoutePropertiesControl;
		private RouteGroupFilterDefault routeGroupFilter;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RoutePropertiesForm()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tcMain = new System.Windows.Forms.TabControl();
			this.tpCommon = new System.Windows.Forms.TabPage();
			this.ctrlRouteProperties = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RoutePropertiesControl();
			this.tpGroups = new System.Windows.Forms.TabPage();
			this.ctrlRouteGroupTable = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupTableControl();
			this.routeDataDispatcher = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataDispatcher( this.components );
			this.daRouteGroupXpRowFactory = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.routeGroupXpRowFactory = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupXpRowFactory();
			this.routeGroupFilter = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupFilterDefault();
			this.daRouteGroupFilter = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.daRoutePropertiesControl = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.panelFill.SuspendLayout();
			this.tcMain.SuspendLayout();
			this.tpCommon.SuspendLayout();
			this.tpGroups.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add( this.tcMain );
			this.panelFill.Size = new System.Drawing.Size( 294, 195 );
			// 
			// tcMain
			// 
			this.tcMain.Controls.Add( this.tpCommon );
			this.tcMain.Controls.Add( this.tpGroups );
			this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tcMain.Location = new System.Drawing.Point( 0, 0 );
			this.tcMain.Name = "tcMain";
			this.tcMain.SelectedIndex = 0;
			this.tcMain.Size = new System.Drawing.Size( 294, 195 );
			this.tcMain.TabIndex = 0;
			// 
			// tpCommon
			// 
			this.tpCommon.Controls.Add( this.ctrlRouteProperties );
			this.tpCommon.Location = new System.Drawing.Point( 4, 22 );
			this.tpCommon.Name = "tpCommon";
			this.tpCommon.Padding = new System.Windows.Forms.Padding( 3 );
			this.tpCommon.Size = new System.Drawing.Size( 286, 169 );
			this.tpCommon.TabIndex = 0;
			this.tpCommon.Text = "Common";
			this.tpCommon.UseVisualStyleBackColor = true;
			// 
			// ctrlRouteProperties
			// 
			this.ctrlRouteProperties.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ctrlRouteProperties.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctrlRouteProperties.Location = new System.Drawing.Point( 3, 3 );
			this.ctrlRouteProperties.Name = "ctrlRouteProperties";
			this.ctrlRouteProperties.Size = new System.Drawing.Size( 280, 163 );
			this.ctrlRouteProperties.TabIndex = 0;
			// 
			// tpGroups
			// 
			this.tpGroups.Controls.Add( this.ctrlRouteGroupTable );
			this.tpGroups.Location = new System.Drawing.Point( 4, 22 );
			this.tpGroups.Name = "tpGroups";
			this.tpGroups.Padding = new System.Windows.Forms.Padding( 3 );
			this.tpGroups.Size = new System.Drawing.Size( 192, 74 );
			this.tpGroups.TabIndex = 1;
			this.tpGroups.Text = "Groups";
			this.tpGroups.UseVisualStyleBackColor = true;
			// 
			// ctrlRouteGroupTable
			// 
			this.ctrlRouteGroupTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			// 
			// 
			// 
			this.ctrlRouteGroupTable.ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.ctrlRouteGroupTable.ToolBar.Divider = false;
			this.ctrlRouteGroupTable.ToolBar.DropDownArrows = true;
			this.ctrlRouteGroupTable.ToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.ctrlRouteGroupTable.ToolBar.Location = new System.Drawing.Point( 0, 0 );
			this.ctrlRouteGroupTable.ToolBar.Name = "globalToolbar";
			this.ctrlRouteGroupTable.ToolBar.ShowToolTips = true;
			this.ctrlRouteGroupTable.ToolBar.Size = new System.Drawing.Size( 186, 26 );
			this.ctrlRouteGroupTable.ToolBar.TabIndex = 0;
			this.ctrlRouteGroupTable.Controls.Add( this.ctrlRouteGroupTable.ToolBar );
			this.ctrlRouteGroupTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctrlRouteGroupTable.Location = new System.Drawing.Point( 3, 3 );
			this.ctrlRouteGroupTable.Name = "ctrlRouteGroupTable";
			this.ctrlRouteGroupTable.Size = new System.Drawing.Size( 186, 68 );
			this.ctrlRouteGroupTable.TabIndex = 0;
			// 
			// routeDataDispatcher
			// 
			this.routeDataDispatcher.Ambassadors.Add( this.daRoutePropertiesControl );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteGroupXpRowFactory );
			this.routeDataDispatcher.Ambassadors.Add( this.daRouteGroupFilter );
			// 
			// daRouteGroupXpRowFactory
			// 
			this.daRouteGroupXpRowFactory.Item = this.routeGroupXpRowFactory;
			// 
			// routeGroupXpRowFactory
			// 
			this.routeGroupXpRowFactory.Filter = this.routeGroupFilter;
			this.routeGroupXpRowFactory.ViewControl = this.ctrlRouteGroupTable;
			// 
			// daRouteGroupFilter
			// 
			this.daRouteGroupFilter.Item = this.routeGroupFilter;
			// 
			// daRoutePropertiesControl
			// 
			this.daRoutePropertiesControl.Item = this.ctrlRouteProperties;
			// 
			// RoutePropertiesForm
			// 
			this.ClientSize = new System.Drawing.Size( 294, 225 );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MinimumSize = new System.Drawing.Size( 300, 250 );
			this.Name = "RoutePropertiesForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.RoutePropertiesForm_FormClosing );
			this.panelFill.ResumeLayout( false );
			this.tcMain.ResumeLayout( false );
			this.tpCommon.ResumeLayout( false );
			this.tpGroups.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion  // Windows Form Designer generated code

		#region IDataItem<IRouteData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRouteData Data
		{
			get { return this.routeDataDispatcher.Data; }
			set { this.routeDataDispatcher.Data = value; }
		}

		#endregion  // IDataItem<IRouteData> Members

		#region Properties

		[Browsable( false )]
		[DefaultValue( DataItemControlMode.Undefined )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.ctrlRouteProperties.Mode; }
			set { this.ctrlRouteProperties.Mode = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int IdRoute
		{
			get { return this.ctrlRouteProperties.IdRoute; }
			set { this.ctrlRouteProperties.IdRoute = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int RouteType
		{
			get { return this.ctrlRouteProperties.RouteType; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Number
		{
			get { return this.ctrlRouteProperties.Number; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string RouteName
		{
			get { return this.ctrlRouteProperties.RouteName; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int Length
		{
			get { return this.ctrlRouteProperties.Length; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Description
		{
			get { return this.ctrlRouteProperties.Description; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Color
		{
			get { return this.ctrlRouteProperties.Color; }
		}

		#endregion  // Properties

		#region Handle controls' events

		private void RoutePropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = this.ctrlRouteProperties.Check();

				if ( Warnings.Count > 0 )
				{
					e.Cancel = true;
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings )
						);
				}
			}
		}

		#endregion  // Handle controls' events
	}
}