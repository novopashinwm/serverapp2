﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RoutePropertiesControl :
		UserControl,
		IDataItem<IRouteData>
	{
		#region Controls & Components

		private IContainer components = null;
		private RouteDataDispatcher dataDispatcher;
		private TableLayoutPanel tableLayoutPanelMain;
		private Label lblNumber;
		private TextBox edtDescription;
		private TextBox edtLength;
		private Label lblName;
		private TextBox edtName;
		private Label lblDescription;
		private TextBox edtNumber;
		private Label lblLength;
		private Label lblRouteType;
		private RouteTypeComboBox cbxRouteType;
		private RouteTypeXpRowFactory routeTypeXpRowFactory;
		private RouteTypeFilter routeTypeFilter;
		private Label lblColor;
		private RouteDataAmbassador daRouteTypeXpRowFactory;
		private RouteDataAmbassador daRouteTypeFilter;
		private TextBox edtColor;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RoutePropertiesControl()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dataDispatcher = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataDispatcher( this.components );
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblNumber = new System.Windows.Forms.Label();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.edtLength = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtNumber = new System.Windows.Forms.TextBox();
			this.lblLength = new System.Windows.Forms.Label();
			this.lblRouteType = new System.Windows.Forms.Label();
			this.cbxRouteType = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType.RouteTypeComboBox();
			this.lblColor = new System.Windows.Forms.Label();
			this.edtColor = new System.Windows.Forms.TextBox();
			this.routeTypeXpRowFactory = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType.RouteTypeXpRowFactory();
			this.routeTypeFilter = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType.RouteTypeFilter();
			this.daRouteTypeFilter = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.daRouteTypeXpRowFactory = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataAmbassador();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daRouteTypeXpRowFactory );
			this.dataDispatcher.Ambassadors.Add( this.daRouteTypeFilter );
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.AutoSize = true;
			this.tableLayoutPanelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanelMain.ColumnCount = 2;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.Controls.Add( this.lblNumber, 0, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.edtDescription, 1, 5 );
			this.tableLayoutPanelMain.Controls.Add( this.edtLength, 1, 3 );
			this.tableLayoutPanelMain.Controls.Add( this.lblName, 0, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.edtName, 1, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.lblDescription, 0, 5 );
			this.tableLayoutPanelMain.Controls.Add( this.edtNumber, 1, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.lblLength, 0, 3 );
			this.tableLayoutPanelMain.Controls.Add( this.lblRouteType, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.cbxRouteType, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.lblColor, 0, 4 );
			this.tableLayoutPanelMain.Controls.Add( this.edtColor, 1, 4 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 6;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 200, 157 );
			this.tableLayoutPanelMain.TabIndex = 1;
			// 
			// lblNumber
			// 
			this.lblNumber.AutoSize = true;
			this.lblNumber.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblNumber.Location = new System.Drawing.Point( 3, 27 );
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Size = new System.Drawing.Size( 60, 26 );
			this.lblNumber.TabIndex = 7;
			this.lblNumber.Text = "Number";
			this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtDescription
			// 
			this.edtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtDescription.Location = new System.Drawing.Point( 69, 134 );
			this.edtDescription.Multiline = true;
			this.edtDescription.Name = "edtDescription";
			this.edtDescription.Size = new System.Drawing.Size( 128, 20 );
			this.edtDescription.TabIndex = 5;
			// 
			// edtLength
			// 
			this.edtLength.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtLength.Location = new System.Drawing.Point( 69, 82 );
			this.edtLength.Name = "edtLength";
			this.edtLength.Size = new System.Drawing.Size( 128, 20 );
			this.edtLength.TabIndex = 3;
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblName.Location = new System.Drawing.Point( 3, 53 );
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size( 60, 26 );
			this.lblName.TabIndex = 8;
			this.lblName.Text = "Name";
			this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtName
			// 
			this.edtName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtName.Location = new System.Drawing.Point( 69, 56 );
			this.edtName.Name = "edtName";
			this.edtName.Size = new System.Drawing.Size( 128, 20 );
			this.edtName.TabIndex = 2;
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDescription.Location = new System.Drawing.Point( 3, 131 );
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size( 60, 26 );
			this.lblDescription.TabIndex = 11;
			this.lblDescription.Text = "Description";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtNumber
			// 
			this.edtNumber.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtNumber.Location = new System.Drawing.Point( 69, 30 );
			this.edtNumber.Name = "edtNumber";
			this.edtNumber.Size = new System.Drawing.Size( 128, 20 );
			this.edtNumber.TabIndex = 1;
			// 
			// lblLength
			// 
			this.lblLength.AutoSize = true;
			this.lblLength.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblLength.Location = new System.Drawing.Point( 3, 79 );
			this.lblLength.Name = "lblLength";
			this.lblLength.Size = new System.Drawing.Size( 60, 26 );
			this.lblLength.TabIndex = 9;
			this.lblLength.Text = "Length";
			this.lblLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblRouteType
			// 
			this.lblRouteType.AutoSize = true;
			this.lblRouteType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblRouteType.Location = new System.Drawing.Point( 3, 0 );
			this.lblRouteType.Name = "lblRouteType";
			this.lblRouteType.Size = new System.Drawing.Size( 60, 27 );
			this.lblRouteType.TabIndex = 6;
			this.lblRouteType.Text = "Route type";
			this.lblRouteType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbxRouteType
			// 
			this.cbxRouteType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbxRouteType.FormattingEnabled = true;
			this.cbxRouteType.Location = new System.Drawing.Point( 69, 3 );
			this.cbxRouteType.Name = "cbxRouteType";
			this.cbxRouteType.Size = new System.Drawing.Size( 128, 21 );
			this.cbxRouteType.TabIndex = 0;
			// 
			// lblColor
			// 
			this.lblColor.AutoSize = true;
			this.lblColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblColor.Location = new System.Drawing.Point( 3, 105 );
			this.lblColor.Name = "lblColor";
			this.lblColor.Size = new System.Drawing.Size( 60, 26 );
			this.lblColor.TabIndex = 10;
			this.lblColor.Text = "Color";
			this.lblColor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtColor
			// 
			this.edtColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtColor.Location = new System.Drawing.Point( 69, 108 );
			this.edtColor.Name = "edtColor";
			this.edtColor.Size = new System.Drawing.Size( 128, 20 );
			this.edtColor.TabIndex = 4;
			// 
			// routeTypeXpRowFactory
			// 
			this.routeTypeXpRowFactory.Filter = this.routeTypeFilter;
			this.routeTypeXpRowFactory.ViewControl = this.cbxRouteType;
			// 
			// daRouteTypeFilter
			// 
			this.daRouteTypeFilter.Item = this.routeTypeFilter;
			// 
			// daRouteTypeXpRowFactory
			// 
			this.daRouteTypeXpRowFactory.Item = this.routeTypeXpRowFactory;
			// 
			// RoutePropertiesControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "RoutePropertiesControl";
			this.Size = new System.Drawing.Size( 200, 157 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion  // Component Designer generated code

		#region IDataItem<IRouteData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRouteData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion  // IDataItem<IRouteData> Members

		#region Data

		private RouteRow rowRoute;

		protected virtual void OnBeforeSetData()
		{
			if ( this.rowRoute != null )
			{
				this.rowRoute.AfterChange -= rowRoute_AfterChange;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if ( this.rowRoute != null )
			{
				this.rowRoute.AfterChange += rowRoute_AfterChange;
			}
		}

		private void rowRoute_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.Enabled = false;

			#region Clear controls

			this.cbxRouteType.SelectedId	= 0;
			this.edtNumber.Text				= string.Empty;
			this.edtName.Text				= string.Empty;
			this.edtLength.Text				= string.Empty;
			this.edtDescription.Text		= string.Empty;
			this.edtColor.Text				= string.Empty;

			#endregion  // Clear controls

			this.rowRoute = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowRoute = this.Data.Route.FindRow( this.idRoute );
		}

		private void OnUpdateView()
		{
			switch ( this.Mode )
			{
				#region Edit

				case DataItemControlMode.Edit:

					if ( this.rowRoute == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.cbxRouteType.SelectedId	= this.rowRoute.RouteType;
					this.edtNumber.Text				= this.rowRoute.Number;
					this.edtName.Text				= this.rowRoute.Name;
					this.edtLength.Text				= this.rowRoute.Length.ToString();
					this.edtDescription.Text		= this.rowRoute.Description;
					this.edtColor.Text				= this.rowRoute.Color;

					break;

				#endregion  // Edit

				#region New

				case DataItemControlMode.New:

					this.cbxRouteType.SelectedId	= 0;
					this.edtNumber.Text				= string.Empty;
					this.edtName.Text				= string.Empty;
					this.edtLength.Text				= string.Empty;
					this.edtDescription.Text		= string.Empty;
					this.edtColor.Text				= string.Empty;

					break;

				#endregion  // New

				#region View

				case DataItemControlMode.View:

					if ( this.rowRoute != null )
					{
						this.cbxRouteType.SelectedId	= this.rowRoute.RouteType;
						this.edtNumber.Text				= this.rowRoute.Number;
						this.edtName.Text				= this.rowRoute.Name;
						this.edtLength.Text				= this.rowRoute.Length.ToString();
						this.edtDescription.Text		= this.rowRoute.Description;
						this.edtColor.Text				= this.rowRoute.Color;
					}

					break;

				#endregion  // View
			}
		}

		#endregion // View

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.View;
		[Browsable( false )]
		[DefaultValue( DataItemControlMode.View )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idRoute;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int IdRoute
		{
			get { return this.idRoute; }
			set
			{
				if ( this.idRoute != value )
				{
					if ( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idRoute = value;

					if ( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int RouteType
		{
			get { return this.cbxRouteType.SelectedId; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Number
		{
			get { return this.edtNumber.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string RouteName
		{
			get { return this.edtName.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int Length
		{
			get
			{
				int result;

				Int32.TryParse( this.edtLength.Text, out result );

				return result;
			}
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Description
		{
			get { return this.edtDescription.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Color
		{
			get { return this.edtColor.Text; }
		}

		#endregion  // Properties

		public StringCollection Check()
		{
			StringCollection Warnings = new StringCollection();

			if ( this.cbxRouteType.SelectedId == 0 )
			{
				Warnings.Add( Strings.RoutePropertiesForm_Warning_RouteTypeRequired );
			}

			if ( this.Number.Length == 0 )
			{
				Warnings.Add( Strings.RoutePropertiesForm_Warning_NumberRequired );
			}

			if ( this.RouteName.Length == 0 )
			{
				Warnings.Add( Strings.RoutePropertiesForm_Warning_NameRequired );
			}

			int result;
			if ( !Int32.TryParse( this.edtLength.Text, out result ) )
			{
				Warnings.Add( Strings.RoutePropertiesForm_Warning_LengthRequired );
			}

			return Warnings;
		}
	}
}