using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellRouteType;
		private readonly Cell cellNumber;
		private readonly Cell cellName;
		private readonly Cell cellLength;
		private readonly Cell cellDescription;
		private readonly Cell cellColor;

		#endregion  // Cells

		private readonly int ID;

		public RouteXpRow( 
			int id,
			string routeType,
			string number,
			string name,
			string length,
			string description,
			string color
			)
		{
			this.ID = id;

			#region Cells

			this.cellRouteType		= new Cell();
            this.cellNumber			= new Cell();
            this.cellName			= new Cell();
            this.cellLength			= new Cell();
            this.cellDescription	= new Cell();
            this.cellColor			= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellRouteType,
						this.cellNumber,
						this.cellName,
						this.cellLength,
						this.cellDescription,
						this.cellColor
					}
				);

			#endregion  // Cells

			this.cellRouteType.Text		= routeType;
			this.cellNumber.Text		= number;
			this.cellName.Text			= name;
			this.cellLength.Text		= length;
			this.cellDescription.Text	= description;
			this.cellColor.Text			= color;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}