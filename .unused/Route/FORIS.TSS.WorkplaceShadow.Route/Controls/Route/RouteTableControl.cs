using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteTableControl :
		TssUserControl,
		IViewObjectControl<RouteXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private TextColumn columnNumber;
		private TextColumn columnName;
		private TextColumn columnLength;
		private TextColumn columnDescription;
		private TextColumn columnColor;
		private RouteTableModel tableModel;
		private Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton tbbRemove;
		private Command cmdRemove;
		private Command cmdProperties;
		private Command cmdNew;
		private CommandInstance ciRemoveToolBarButton;
		private CommandInstance ciPropertiesToolBarButton;
		private CommandInstance ciNewToolBarButton;
		private ToolBarButton separator2;
		private TextColumn columnRouteType;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RouteTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( RouteTableControl ) );
			this.tableModel = new FORIS.TSS.WorkplaceShadow.Route.Controls.Route.RouteTableModel( this.components );
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnRouteType = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnNumber = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLength = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnColor = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar( this.components );
			this.tbbNew = new System.Windows.Forms.ToolBarButton();
			this.tbbProperties = new System.Windows.Forms.ToolBarButton();
			this.separator2 = new System.Windows.Forms.ToolBarButton();
			this.tbbRemove = new System.Windows.Forms.ToolBarButton();
			this.cmdRemove = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciRemoveToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdProperties = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPropertiesToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdNew = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciNewToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// tableModel
			// 
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnRouteType,
            this.columnNumber,
            this.columnName,
            this.columnLength,
            this.columnDescription,
            this.columnColor} );
			// 
			// columnRouteType
			// 
			this.columnRouteType.Editable = false;
			this.columnRouteType.Editor = null;
			resources.ApplyResources( this.columnRouteType, "columnRouteType" );
			// 
			// columnNumber
			// 
			this.columnNumber.Editable = false;
			this.columnNumber.Editor = null;
			resources.ApplyResources( this.columnNumber, "columnNumber" );
			// 
			// columnName
			// 
			this.columnName.Editable = false;
			this.columnName.Editor = null;
			resources.ApplyResources( this.columnName, "columnName" );
			// 
			// columnLength
			// 
			this.columnLength.Editable = false;
			this.columnLength.Editor = null;
			resources.ApplyResources( this.columnLength, "columnLength" );
			// 
			// columnDescription
			// 
			this.columnDescription.Editable = false;
			this.columnDescription.Editor = null;
			resources.ApplyResources( this.columnDescription, "columnDescription" );
			this.columnDescription.Width = 100;
			// 
			// columnColor
			// 
			this.columnColor.Editable = false;
			this.columnColor.Editor = null;
			resources.ApplyResources( this.columnColor, "columnColor" );
			this.columnColor.Width = 100;
			// 
			// table
			// 
			this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			resources.ApplyResources( this.table, "table" );
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.TableModel = this.tableModel;
			this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler( this.table_SelectionChanged );
			// 
			// globalToolbar
			// 
			resources.ApplyResources( this.globalToolbar, "globalToolbar" );
			this.globalToolbar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove} );
			this.globalToolbar.Divider = false;
			this.globalToolbar.Name = "globalToolbar";
			// 
			// tbbNew
			// 
			resources.ApplyResources( this.tbbNew, "tbbNew" );
			this.tbbNew.Name = "tbbNew";
			// 
			// tbbProperties
			// 
			resources.ApplyResources( this.tbbProperties, "tbbProperties" );
			this.tbbProperties.Name = "tbbProperties";
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbbRemove
			// 
			resources.ApplyResources( this.tbbRemove, "tbbRemove" );
			this.tbbRemove.Name = "tbbRemove";
			// 
			// cmdRemove
			// 
			this.cmdRemove.Instances.Add( this.ciRemoveToolBarButton );
			this.cmdRemove.Manager = this.commandManager;
			this.cmdRemove.Execute += new System.EventHandler( this.cmdRemove_Execute );
			this.cmdRemove.Update += new System.EventHandler( this.cmdRemove_Update );
			// 
			// ciRemoveToolBarButton
			// 
			this.ciRemoveToolBarButton.Item = this.tbbRemove;
			// 
			// cmdProperties
			// 
			this.cmdProperties.Instances.Add( this.ciPropertiesToolBarButton );
			this.cmdProperties.Manager = this.commandManager;
			this.cmdProperties.Execute += new System.EventHandler( this.cmdProperties_Execute );
			this.cmdProperties.Update += new System.EventHandler( this.cmdProperties_Update );
			// 
			// ciPropertiesToolBarButton
			// 
			this.ciPropertiesToolBarButton.Item = this.tbbProperties;
			// 
			// cmdNew
			// 
			this.cmdNew.Instances.Add( this.ciNewToolBarButton );
			this.cmdNew.Manager = this.commandManager;
			this.cmdNew.Execute += new System.EventHandler( this.cmdNew_Execute );
			// 
			// ciNewToolBarButton
			// 
			this.ciNewToolBarButton.Item = this.tbbNew;
			// 
			// RouteTableControl
			// 
			this.Controls.Add( this.table );
			this.Controls.Add( this.globalToolbar );
			this.Name = "RouteTableControl";
			resources.ApplyResources( this, "$this" );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion  // Component Designer generated code

		#region Properties

		private int selectedRoute;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedRoute
		{
			get { return this.selectedRoute; }
			set
			{
				if ( this.tableModel.ContainsId( value ) )
				{
					this.tableModel.Selections.SelectCell(
						this.tableModel.IndexOf( value ),
						0
						);

					this.selectedRoute = value;
				}
				else
				{
					this.tableModel.Selections.Clear();
				}
			}
		}

		private IRouteEditorProvider editorProvider;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public IRouteEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public GlobalToolbar ToolBar
		{
			get { return this.globalToolbar; }
		}

		[Browsable( true )]
		[DefaultValue( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public bool ToolBarVisible
		{
			get { return this.globalToolbar.Visible; }
			set { this.globalToolbar.Visible = value; }
		}

		#endregion  // Properties

		#region Actions

		private void ActionNew()
		{
			if ( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdRoute = ( (RouteXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Properties( IdRoute );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdRoute = ( (RouteXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Remove( IdRoute );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		#endregion  // Actions

		#region Events

		private EventHandler delegateSelectionChanged;

		public event EventHandler SelectionChanged
		{
			add { this.delegateSelectionChanged += value; }
			remove { this.delegateSelectionChanged -= value; }
		}

		protected virtual void OnSelectionChanged()
		{
			if ( this.delegateSelectionChanged != null )
			{
				this.delegateSelectionChanged( this, EventArgs.Empty );
			}
		}

		#endregion  // Events

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void table_SelectionChanged( object sender, SelectionEventArgs e )
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				this.selectedRoute = ( (RouteXpRow)this.table.SelectedItems[0] ).GetId();
			}
			else
			{
				this.selectedRoute = 0;
			}

			this.OnSelectionChanged();
		}

		#endregion  // Handle controls events

		#region IViewObjectControl<RouteXpRow> Members

		public void RowShow( ViewObjectEventArgs<RouteXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<RouteXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<RouteXpRow> Members
	}
}