using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteFilterDefault :
		Filter<RouteRow, IRouteData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Route.Rows.Inserted -= this.RouteRows_Inserted;
			this.Data.Route.Rows.Removing -= this.RouteRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.Route.Rows.Inserted += this.RouteRows_Inserted;
			this.Data.Route.Rows.Removing += this.RouteRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void RouteRows_Inserted( object sender, CollectionChangeEventArgs<RouteRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}
		void RouteRows_Removing( object sender, CollectionChangeEventArgs<RouteRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( RouteRow row in this.Data.Route.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View
	}
}