using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteEditorProvider :
		EditorProvider<IRouteDataTreater, IRouteData>,
		IRouteEditorProvider
	{
		#region Constructor & Dispose

		public RouteEditorProvider()
		{
		}
		public RouteEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		protected override void New()
		{
			using ( RoutePropertiesForm Form = new RoutePropertiesForm() )
			{
				Form.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IRouteDataTreater Treater = this.Data.GetDataTreater();

						using ( Treater )
						{
							Treater.RouteInsert(
								Form.RouteType,
								Form.Number,
								Form.RouteName,
								Form.Length,
								Form.Description,
								Form.Color
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteEditorProvider_Exception_CanNotCreateNewRoute,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Properties( int idRoute )
		{
			using ( RoutePropertiesForm Form = new RoutePropertiesForm() )
			{
				Form.Mode = DataItemControlMode.Edit;
				Form.IdRoute = idRoute;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IRouteDataTreater Treater = this.Data.GetDataTreater();

						using ( Treater )
						{
							Treater.RouteUpdate(
								idRoute,
								Form.RouteType,
								Form.Number,
								Form.RouteName,
								Form.Length,
								Form.Description,
								Form.Color
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteEditorProvider_Exception_CanNotUpdateRoute,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Remove( int idRoute )
		{
			if (
				MessageBox.Show(
					Strings.RouteEditorProvider_Warning_AreYouSureToDeleteRoute,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRouteDataTreater Treater = this.Data.GetDataTreater();

					using ( Treater )
					{
						Treater.RouteDelete( idRoute );
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteEditorProvider_Exception_CanNotDeleteRoute,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idRoute )
		{
			throw new NotImplementedException();
		}
	}
}