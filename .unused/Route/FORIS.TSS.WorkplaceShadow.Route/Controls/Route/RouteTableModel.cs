using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.Route
{
	public class RouteTableModel :
		ViewObjectTableModel<RouteXpRow>
	{
		#region Constructor & Dispose

		public RouteTableModel( IContainer container )
		{
			container.Add( this );
		}
		public RouteTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}