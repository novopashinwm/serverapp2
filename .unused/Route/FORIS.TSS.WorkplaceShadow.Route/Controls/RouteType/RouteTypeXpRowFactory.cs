﻿using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType
{
	public class RouteTypeXpRowFactory :
		ViewObjectFactory<RouteTypeRow, RouteTypeXpRow, IRouteData>
	{
		public override RouteTypeXpRow CreateViewObject( 
			int viewObjectId, 
			RouteTypeRow routeTypeRow 
			)
		{
			return
				new RouteTypeXpRow(
					viewObjectId,
					routeTypeRow.Name
					);
		}
	}
}