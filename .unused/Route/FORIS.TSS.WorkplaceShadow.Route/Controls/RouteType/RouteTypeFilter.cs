﻿using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType
{
	public class RouteTypeFilter :
		Filter<RouteTypeRow, IRouteData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.RouteType.Rows.Inserted -= RouteType_Inserted;
			this.Data.RouteType.Rows.Removing -= RouteType_Removing;
		}

		protected override void OnAfterSetData()
		{
			this.Data.RouteType.Rows.Inserted += RouteType_Inserted;
			this.Data.RouteType.Rows.Removing += RouteType_Removing;
		}

		#endregion  // Data

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}

		protected override void OnBuildView()
		{
			foreach ( RouteTypeRow row in this.Data.RouteType.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View

		#region Data events

		void RouteType_Inserted( object sender, CollectionChangeEventArgs<RouteTypeRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}

		void RouteType_Removing( object sender, CollectionChangeEventArgs<RouteTypeRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion  // Data events
	}
}