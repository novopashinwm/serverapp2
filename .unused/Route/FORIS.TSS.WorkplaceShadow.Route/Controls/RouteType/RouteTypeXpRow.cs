﻿using FORIS.TSS.Common.Collections;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteType
{
	public class RouteTypeXpRow : 
		Row,
		IViewObject,
		ISortable
	{
		#region Cells

		private readonly Cell cellName;

		#endregion  // Cells

		private readonly int ID;

		public RouteTypeXpRow( 
			int id,
			string name
			)
		{
			this.ID = id;

			#region Cells

			this.cellName = new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellName
					}
				);

			#endregion  // Cells

			this.cellName.Text = name;
		}

		#region IViewObject members

		public int GetId()
		{
			return this.ID;
		}

		#endregion  // IViewObject members

		#region ISortable Members

		public object Key
		{
			get { return this.ToString(); }
		}

		#endregion  // ISortable Members

		public override string ToString()
		{
			return this.cellName.Text;
		}
	}
}