using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	internal class RouteGroupPropertiesForm :
		TssUserForm,
		IDataItem<IRouteData>
	{
		#region Controls & Components

		private new IContainer components;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RouteGroupPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				this.components.Dispose();
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.routeDataDispatcher = new FORIS.TSS.Helpers.Route.Data.Route.RouteDataDispatcher( this.components );
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.panelFill.SuspendLayout();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add( this.tableLayoutPanelMain );
			this.panelFill.Size = new System.Drawing.Size( 294, 72 );
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDescription.Location = new System.Drawing.Point( 3, 26 );
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size( 60, 46 );
			this.lblDescription.TabIndex = 10;
			this.lblDescription.Text = "Description";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtName
			// 
			this.edtName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtName.Location = new System.Drawing.Point( 69, 3 );
			this.edtName.Name = "edtName";
			this.edtName.Size = new System.Drawing.Size( 222, 20 );
			this.edtName.TabIndex = 2;
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblName.Location = new System.Drawing.Point( 3, 0 );
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size( 60, 26 );
			this.lblName.TabIndex = 9;
			this.lblName.Text = "Name";
			this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// edtDescription
			// 
			this.edtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.edtDescription.Location = new System.Drawing.Point( 69, 29 );
			this.edtDescription.Multiline = true;
			this.edtDescription.Name = "edtDescription";
			this.edtDescription.Size = new System.Drawing.Size( 222, 40 );
			this.edtDescription.TabIndex = 3;
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.AutoSize = true;
			this.tableLayoutPanelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanelMain.ColumnCount = 2;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.Controls.Add( this.edtDescription, 1, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.lblName, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.edtName, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.lblDescription, 0, 1 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 2;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle() );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 294, 72 );
			this.tableLayoutPanelMain.TabIndex = 0;
			// 
			// RouteGroupPropertiesForm
			// 
			this.ClientSize = new System.Drawing.Size( 294, 102 );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MinimumSize = new System.Drawing.Size( 300, 127 );
			this.Name = "RouteGroupPropertiesForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.RouteGroupPropertiesForm_FormClosing );
			this.panelFill.ResumeLayout( false );
			this.panelFill.PerformLayout();
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion  // Windows Form Designer generated code

		#region IDataItem<IRouteGroupData> Members

		private RouteDataDispatcher routeDataDispatcher;
		private TableLayoutPanel tableLayoutPanelMain;
		private TextBox edtDescription;
		private Label lblName;
		private TextBox edtName;
		private Label lblDescription;
	
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRouteData Data
		{
			get { return this.routeDataDispatcher.Data; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.routeDataDispatcher.Data = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion  // IDataItem<IRouteGroupData> Members

		#region Data

		private RouteGroupRow rowRouteGroup;

		protected virtual void OnBeforeSetData()
		{
			if ( this.rowRouteGroup != null )
			{
				this.rowRouteGroup.AfterChange -= rowRouteGroup_AfterChange;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if ( this.rowRouteGroup != null )
			{
				this.rowRouteGroup.AfterChange += rowRouteGroup_AfterChange;
			}
		}

		private void rowRouteGroup_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.Enabled = false;

			#region Clear controls

			this.edtName.Text			= string.Empty;
			this.edtDescription.Text	= string.Empty;

			#endregion  // Clear controls

			this.rowRouteGroup = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowRouteGroup = this.Data.RouteGroup.FindRow( this.idRouteGroup );
		}

		private void OnUpdateView()
		{
			switch ( this.Mode )
			{
				#region Edit

				case DataItemControlMode.Edit:

					if ( this.rowRouteGroup == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.edtName.Text			= this.rowRouteGroup.Name;
					this.edtDescription.Text	= this.rowRouteGroup.Description;

					break;

				#endregion // Edit

				#region New

				case DataItemControlMode.New:

					this.edtName.Text			= string.Empty;
					this.edtDescription.Text	= string.Empty;

					break;

				#endregion // New
			}
		}

		#endregion // View

		#region Properties

		private DataItemControlMode mode;
		[Browsable( false )]
		[DefaultValue( DataItemControlMode.Undefined )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idRouteGroup;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int IdRouteGroup
		{
			get { return this.idRouteGroup; }
			set
			{
				if ( this.idRouteGroup != value )
				{
					if ( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idRouteGroup = value;

					if ( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string RouteGroupName
		{
			get { return this.edtName.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Description
		{
			get { return this.edtDescription.Text; }
		}

		#endregion  // Properties

		#region Handle controls' events

		private void RouteGroupPropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if ( this.RouteGroupName.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.RouteGroupPropertiesForm_Warning_NameRequired );
				}

				if ( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings )
						);
				}
			}
		}

		#endregion  // Handle controls' events
	}
}