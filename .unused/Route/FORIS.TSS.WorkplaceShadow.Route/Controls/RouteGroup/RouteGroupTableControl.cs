using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupTableControl :
		TssUserControl,
		IViewObjectControl<RouteGroupXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private TextColumn columnName;
		private TextColumn columnDescription;
		private Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton tbbRemove;
		private Command cmdRemove;
		private Command cmdProperties;
		private Command cmdNew;
		private CommandInstance ciRemoveToolBarButton;
		private CommandInstance ciPropertiesToolBarButton;
		private CommandInstance ciNewToolBarButton;
		private ToolBarButton separator2;
		private RouteGroupTableModel tableModel;
		private CheckBoxColumn columnCheck;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RouteGroupTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
			this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar( this.components );
			this.tbbNew = new System.Windows.Forms.ToolBarButton();
			this.tbbProperties = new System.Windows.Forms.ToolBarButton();
			this.separator2 = new System.Windows.Forms.ToolBarButton();
			this.tbbRemove = new System.Windows.Forms.ToolBarButton();
			this.cmdRemove = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciRemoveToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdProperties = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPropertiesToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdNew = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciNewToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup.RouteGroupTableModel( this.components );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnDescription} );
			// 
			// columnCheck
			// 
			this.columnCheck.Editor = null;
			this.columnCheck.Width = 16;
			// 
			// columnName
			// 
			this.columnName.Editable = false;
			this.columnName.Editor = null;
			this.columnName.Text = "Name";
			// 
			// columnDescription
			// 
			this.columnDescription.Editable = false;
			this.columnDescription.Editor = null;
			this.columnDescription.Text = "Description";
			// 
			// table
			// 
			this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.Dock = System.Windows.Forms.DockStyle.Fill;
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Location = new System.Drawing.Point( 5, 31 );
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.Padding = new System.Windows.Forms.Padding( 5 );
			this.table.Size = new System.Drawing.Size( 604, 164 );
			this.table.TabIndex = 1;
			this.table.TableModel = this.tableModel;
			this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler( this.table_SelectionChanged );
			// 
			// globalToolbar
			// 
			this.globalToolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.globalToolbar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove} );
			this.globalToolbar.Divider = false;
			this.globalToolbar.DropDownArrows = true;
			this.globalToolbar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.globalToolbar.Location = new System.Drawing.Point( 5, 5 );
			this.globalToolbar.Name = "globalToolbar";
			this.globalToolbar.ShowToolTips = true;
			this.globalToolbar.Size = new System.Drawing.Size( 604, 26 );
			this.globalToolbar.TabIndex = 0;
			// 
			// tbbNew
			// 
			this.tbbNew.ImageIndex = 3;
			this.tbbNew.Name = "tbbNew";
			this.tbbNew.ToolTipText = "Add new";
			// 
			// tbbProperties
			// 
			this.tbbProperties.ImageIndex = 7;
			this.tbbProperties.Name = "tbbProperties";
			this.tbbProperties.ToolTipText = "Properties";
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbbRemove
			// 
			this.tbbRemove.ImageIndex = 4;
			this.tbbRemove.Name = "tbbRemove";
			this.tbbRemove.ToolTipText = "Remove";
			// 
			// cmdRemove
			// 
			this.cmdRemove.Instances.Add( this.ciRemoveToolBarButton );
			this.cmdRemove.Manager = this.commandManager;
			this.cmdRemove.Execute += new System.EventHandler( this.cmdRemove_Execute );
			this.cmdRemove.Update += new System.EventHandler( this.cmdRemove_Update );
			// 
			// ciRemoveToolBarButton
			// 
			this.ciRemoveToolBarButton.Item = this.tbbRemove;
			// 
			// cmdProperties
			// 
			this.cmdProperties.Instances.Add( this.ciPropertiesToolBarButton );
			this.cmdProperties.Manager = this.commandManager;
			this.cmdProperties.Execute += new System.EventHandler( this.cmdProperties_Execute );
			this.cmdProperties.Update += new System.EventHandler( this.cmdProperties_Update );
			// 
			// ciPropertiesToolBarButton
			// 
			this.ciPropertiesToolBarButton.Item = this.tbbProperties;
			// 
			// cmdNew
			// 
			this.cmdNew.Instances.Add( this.ciNewToolBarButton );
			this.cmdNew.Manager = this.commandManager;
			this.cmdNew.Execute += new System.EventHandler( this.cmdNew_Execute );
			// 
			// ciNewToolBarButton
			// 
			this.ciNewToolBarButton.Item = this.tbbNew;
			// 
			// tableModel
			// 
			this.tableModel.SelectionAdjuster = null;
			// 
			// RouteGroupTableControl
			// 
			this.Controls.Add( this.table );
			this.Controls.Add( this.globalToolbar );
			this.Name = "RouteGroupTableControl";
			this.Padding = new System.Windows.Forms.Padding( 5 );
			this.Size = new System.Drawing.Size( 614, 200 );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion  // Component Designer generated code

		#region Properties

		private int selectedRouteGroup;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedRouteGroup
		{
			get { return this.selectedRouteGroup; }
			set
			{
				if ( this.tableModel.ContainsId( value ) )
				{
					this.tableModel.Selections.SelectCell(
						this.tableModel.IndexOf( value ),
						0
						);

					this.selectedRouteGroup = value;
				}
				else
				{
					this.tableModel.Selections.Clear();
				}
			}
		}

		private IRouteGroupEditorProvider editorProvider;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public IRouteGroupEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public GlobalToolbar ToolBar
		{
			get { return this.globalToolbar; }
		}

		[Browsable( true )]
		[DefaultValue( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public bool ToolBarVisible
		{
			get { return this.globalToolbar.Visible; }
			set { this.globalToolbar.Visible = value; }
		}

		#endregion  // Properties

		#region Actions

		private void ActionNew()
		{
			if ( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdRouteGroup = ( (RouteGroupXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Properties( IdRouteGroup );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdRouteGroup = ( (RouteGroupXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Remove( IdRouteGroup );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		#endregion  // Actions

		#region Events

		private EventHandler delegateSelectionChanged;

		public event EventHandler SelectionChanged
		{
			add { this.delegateSelectionChanged += value; }
			remove { this.delegateSelectionChanged -= value; }
		}

		protected virtual void OnSelectionChanged()
		{
			if ( this.delegateSelectionChanged != null )
			{
				this.delegateSelectionChanged( this, EventArgs.Empty );
			}
		}

		#endregion  // Events

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void table_SelectionChanged( object sender, SelectionEventArgs e )
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				this.selectedRouteGroup = ( (RouteGroupXpRow)this.table.SelectedItems[0] ).GetId();
			}
			else
			{
				this.selectedRouteGroup = 0;
			}

			this.OnSelectionChanged();
		}

		#endregion  // Handle controls events

		#region IViewObjectControl<RouteGroupXpRow> Members

		public void RowShow( ViewObjectEventArgs<RouteGroupXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<RouteGroupXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<RouteGroupXpRow> Members
	}
}