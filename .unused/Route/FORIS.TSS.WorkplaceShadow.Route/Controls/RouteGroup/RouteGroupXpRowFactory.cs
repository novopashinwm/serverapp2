using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupXpRowFactory :
		ViewObjectFactory<RouteGroupRow, RouteGroupXpRow, IRouteData>
	{
		public override RouteGroupXpRow CreateViewObject( 
			int viewObjectId, 
			RouteGroupRow routeGroupRow 
			)
		{
		
			return
				new RouteGroupXpRow(
					viewObjectId,
					routeGroupRow.Name,
					routeGroupRow.Description
					);
		}
	}
}