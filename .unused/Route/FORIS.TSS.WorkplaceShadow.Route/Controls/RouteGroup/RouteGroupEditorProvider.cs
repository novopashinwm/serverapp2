using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupEditorProvider :
		EditorProvider<IRouteDataTreater, IRouteData>,
		IRouteGroupEditorProvider
	{
		#region Constructor & Dispose

		public RouteGroupEditorProvider()
		{
		}
		public RouteGroupEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		protected override void New()
		{
			using ( RouteGroupPropertiesForm Form = new RouteGroupPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IRouteDataTreater Treater = this.Data.GetDataTreater();

						using ( Treater )
						{
							Treater.RouteGroupInsert(
								Form.RouteGroupName,
								Form.Description
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteGroupEditorProvider_Exception_CanNotCreateNewRouteGroup,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Properties( int idRouteGroup )
		{
			using ( RouteGroupPropertiesForm Form = new RouteGroupPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.Edit;
				Form.IdRouteGroup = idRouteGroup;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IRouteDataTreater Treater = this.Data.GetDataTreater();

						using ( Treater )
						{
							Treater.RouteGroupUpdate(
								idRouteGroup,
								Form.RouteGroupName,
								Form.Description 
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteGroupEditorProvider_Exception_CanNotUpdateRouteGroup,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Remove( int idRouteGroup )
		{
			if (
				MessageBox.Show(
					Strings.RouteGroupEditorProvider_Warning_AreYouSureToDeleteRouteGroup,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRouteDataTreater Treater = this.Data.GetDataTreater();

					using ( Treater )
					{
						Treater.RouteGroupDelete( idRouteGroup );
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RouteGroupEditorProvider_Exception_CanNotDeleteRouteGroup,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idRouteGroup )
		{
			throw new NotImplementedException();
		}
	}
}