using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellCheck;
		private readonly Cell cellName;
		private readonly Cell cellDescription;

		#endregion  // Cells

		private readonly int ID;

		public RouteGroupXpRow( 
			int id,
			string name,
			string description
			)
		{
			this.ID = id;

			#region Cells

			this.cellCheck			= new Cell();
            this.cellName			= new Cell();
			this.cellDescription	= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellCheck,
						this.cellName,
						this.cellDescription
					}
				);

			#endregion  // Cells

			this.cellCheck.Checked		= false;
			this.cellName.Text			= name;
			this.cellDescription.Text	= description;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}