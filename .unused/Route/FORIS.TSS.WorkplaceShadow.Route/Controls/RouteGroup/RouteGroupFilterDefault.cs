using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupFilterDefault :
		Filter<RouteGroupRow, IRouteData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.RouteGroup.Rows.Inserted -= this.RouteGroupRows_Inserted;
			this.Data.RouteGroup.Rows.Removing -= this.RouteGroupRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.RouteGroup.Rows.Inserted += this.RouteGroupRows_Inserted;
			this.Data.RouteGroup.Rows.Removing += this.RouteGroupRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void RouteGroupRows_Inserted( object sender, CollectionChangeEventArgs<RouteGroupRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}
		void RouteGroupRows_Removing( object sender, CollectionChangeEventArgs<RouteGroupRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( RouteGroupRow row in this.Data.RouteGroup.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View
	}
}