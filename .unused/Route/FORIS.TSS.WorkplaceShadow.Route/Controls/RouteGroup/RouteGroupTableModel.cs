using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Route.Controls.RouteGroup
{
	public class RouteGroupTableModel :
		ViewObjectTableModel<RouteGroupXpRow>
	{
		#region Constructor & Dispose

		public RouteGroupTableModel( IContainer container )
		{
			container.Add( this );
		}
		public RouteGroupTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}