﻿using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public class RoutePersonalDataSupplier :
		PersonalDataSupplier<RouteServerData, IRouteDataTreater>,
		IRouteDataSupplier
	{
		public RoutePersonalDataSupplier(
			RouteServerData dataSupplier,
			IPersonalServer personalServer
			)
			: base( dataSupplier, personalServer )
		{

		}

		public override IRouteDataTreater GetDataTreater()
		{
			return
				this.DataSupplier.GetTreater( this.PersonalServer.SessionInfo );
		}
	}
}