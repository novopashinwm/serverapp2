﻿using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	/// <summary>
	/// Поставщик данных для модуля "Маршруты"
	/// </summary>
	/// <remarks>
	/// Всегда можно изменить способ получения,
	/// при необходимости - кэширования данных,
	/// выполнения операций и т.д. и т.п.
	/// 
	/// Именно этот самый объект осуществляет
	/// соединение данных непосредственно с БД
	/// </remarks>
	public class RouteServerData :
		RouteData,
		IRouteServerData
	{
		#region Constructor & Dispose

		/// <summary>
		/// Конструктор загружает данные и формирует обертку
		/// </summary>
		public RouteServerData( IRouteServerDataProvider serverDataProvider )
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new RouteDatabaseDataSupplier( this.serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				RemotingServices.Disconnect( this );

				this.Destroy();
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// Передается в объекты DataTreater
		/// для обновления данных в БД
		/// </summary>
		private readonly IRouteServerDataProvider serverDataProvider;

		/// <summary>
		/// Создает новый объект DataTreater
		/// </summary>
		/// <param name="sessionInfo"></param>
		/// <returns>объект RouteDataTreater</returns>
		public IRouteDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new RouteDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		public override IRouteDataTreater GetDataTreater()
		{
			return
				new RouteDataTreater(
					this.serverDataProvider.Database,
					this,
					null
					);
		}
	}

	public interface IRouteServerData :
		IRouteData
	{

	}
}