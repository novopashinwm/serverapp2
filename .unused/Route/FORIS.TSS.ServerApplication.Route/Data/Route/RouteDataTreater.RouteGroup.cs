﻿using System;
using FORIS.TSS.Helpers.Route.Data.Route;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public partial class RouteDataTreater
	{
		public void RouteGroupInsert( 
			string name, 
			string description 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteGroupRow routeRow = this.Data.RouteGroup.NewRow();

					routeRow.Name = name;
					routeRow.Description = description;

					this.Data.RouteGroup.Rows.Add( routeRow );

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RouteGroupUpdate( 
			int routeGroupId, 
			string name, 
			string description 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteGroupRow routeRow = this.Data.RouteGroup.FindRow( routeGroupId );

					routeRow.Name = name;
					routeRow.Description = description;

					this.Data.RouteGroup.Rows.Add( routeRow );

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RouteGroupDelete( 
			int routeGroupId 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteGroupRow routeGroupRow = this.Data.RouteGroup.FindRow( routeGroupId );

					routeGroupRow.Delete();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}