﻿using System;
using FORIS.TSS.Helpers.Route.Data.Route;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public partial class RouteDataTreater
	{
		public void RouteInsert( 
			int routeType, 
			string number, 
			string name, 
			int length, 
			string description, 
			string color 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteRow routeRow = this.Data.Route.NewRow();

					routeRow.RouteType		= routeType;
					routeRow.Number			= number;
					routeRow.Name			= name;
					routeRow.Length			= length;
					routeRow.Description	= description;
					routeRow.Color			= color;

					this.Data.Route.Rows.Add( routeRow );

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RouteUpdate( 
			int routeId, 
			int routeType, 
			string number, 
			string name, 
			int length, 
			string description, 
			string color 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteRow routeRow = this.Data.Route.FindRow( routeId );

					routeRow.BeginEdit();

					routeRow.RouteType		= routeType;
					routeRow.Number			= number;
					routeRow.Name			= name;
					routeRow.Length			= length;
					routeRow.Description	= description;
					routeRow.Color			= color;

					routeRow.EndEdit();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RouteDelete( 
			int routeId 
			)
		{
			lock ( this.Data )
			{
				try
				{
					RouteRow routeRow = this.Data.Route.FindRow( routeId );

					routeRow.Delete();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}