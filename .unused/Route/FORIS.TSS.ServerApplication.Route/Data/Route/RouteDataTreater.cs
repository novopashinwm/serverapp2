﻿using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public partial class RouteDataTreater :
		DataTreater<IRouteData>,
		IRouteDataTreater
	{
		public RouteDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IRouteData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{

		}
	}
}