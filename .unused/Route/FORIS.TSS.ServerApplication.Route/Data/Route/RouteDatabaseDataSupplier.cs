﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public class RouteDatabaseDataSupplier :
		DatabaseDataSupplier<IRouteServerDataProvider>,
		IRouteDataSupplier
	{
		public RouteDatabaseDataSupplier( IRouteServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{

		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerRoute",
					new string[]
						{
							"ROUTE_TYPE", 
							"ROUTEGROUP", 
							"ROUTE", 
							"ROUTEGROUP_ROUTE"
						}
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IRouteDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}