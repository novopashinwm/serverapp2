﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.Route.Data.Route
{
	public class RouteServerDataDispatcher :
		DataDispatcher<IRouteServerData>
	{
		#region Constructor

		public RouteServerDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public RouteServerDataDispatcher()
		{

		}

		#endregion // Constructor

		private readonly RuleServerDataAmbassadorCollection ambassadors =
			new RuleServerDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new RuleServerDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRouteServerData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class RuleServerDataAmbassadorCollection :
		DataAmbassadorCollection<IRouteServerData>
	{
		public new RuleServerDataAmbassador this[int index]
		{
			get { return (RuleServerDataAmbassador)base[index]; }
		}
	}

	public class RuleServerDataAmbassador :
		DataAmbassador<IRouteServerData>
	{

	}
}