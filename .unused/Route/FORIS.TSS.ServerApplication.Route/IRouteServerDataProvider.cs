﻿using FORIS.TSS.ServerApplication.Route.Data.Route;

namespace FORIS.TSS.ServerApplication.Route
{
	public interface IRouteServerDataProvider :
		IDatabaseProvider
	{
		RouteServerData RouteData { get; }
	}
}