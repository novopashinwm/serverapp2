﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Route.Data.Route;
using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.WorkplaceSharnier.Route.Data.Route
{
	/// <summary>
	/// Поставщик данных стороны клиента
	/// </summary>
	public class RouteClientData :
		RouteData
	{
		#region Constructor & Dispose

		public RouteClientData( IRouteClientDataProvider clientProvider )
		{
			this.dataSupplier = new RouteClientDataSupplier( clientProvider );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// Используется для получения ссылки объекта DataTreater
		/// </summary>
		private readonly IRouteDataSupplier dataSupplier;

		public IDataSupplier SecurityDataSupplier
		{
			get { throw new NotSupportedException(); }
		}
	}
}