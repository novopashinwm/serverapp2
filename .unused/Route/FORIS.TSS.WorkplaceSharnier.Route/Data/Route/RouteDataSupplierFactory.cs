﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Route;
using FORIS.TSS.WorkplaceSharnier.Route.Data.Route;

namespace FORIS.TSS.WorkplaceSharnier.Route.Data.Route
{
	public class RouteDataSupplierFactory :
		ClientDataSupplierFactory<IRouteClientDataProvider, RouteClientData, DataParams>
	{
		public RouteDataSupplierFactory( IRouteClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override RouteClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if ( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			RouteClientData Supplier = new RouteClientData( this.Client );

			return Supplier;
		}
	}
}