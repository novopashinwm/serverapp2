﻿using FORIS.TSS.Interfaces.Route.Data.Route;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Route;

namespace FORIS.TSS.WorkplaceSharnier.Route.Data.Route
{
	internal class RouteClientDataSupplier :
		ClientDataSupplier<IRouteClientDataProvider, IRouteDataSupplier, IRouteDataTreater>,
		IRouteDataSupplier
	{
		public RouteClientDataSupplier( IRouteClientDataProvider clientProvider )
			: base( clientProvider, clientProvider.GetRouteDataSupplier() )
		{

		}
	}
}