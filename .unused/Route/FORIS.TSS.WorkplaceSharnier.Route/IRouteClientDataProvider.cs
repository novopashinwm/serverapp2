﻿using FORIS.TSS.Interfaces.Route.Data;
using FORIS.TSS.WorkplaceSharnier.Route.Data.Route;

namespace FORIS.TSS.WorkplaceSharnier.Route
{
	public interface IRouteClientDataProvider :
		IClientDataProvider,
		IRouteDataSupplierProvider
	{
		RouteClientData GetRouteClientData();
	}
}