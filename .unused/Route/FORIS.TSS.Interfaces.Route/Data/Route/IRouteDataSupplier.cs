﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Route.Data.Route
{
	public interface IRouteDataSupplier :
		IDataSupplier<IRouteDataTreater>
	{

	}
}