﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Route.Data.Route
{
	public interface IRouteDataTreater :
		IDataTreater
	{
		#region Route

		void RouteInsert(
			int routeType,
			string number,
			string name,
			int length,
			string description,
			string color
			);

		void RouteUpdate(
			int routeId,
			int routeType,
			string number,
			string name,
			int length,
			string description,
			string color
			);

		void RouteDelete(
			int routeId
			);

		#endregion  // Route

		#region RouteGroup

		void RouteGroupInsert(
			string name,
			string description
			);

		void RouteGroupUpdate(
			int routeGroupId,
			string name,
			string description
			);

		void RouteGroupDelete(
			int routeGroupId
			);

		#endregion  // RouteGroup
	}
}