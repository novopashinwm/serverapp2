﻿using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.Interfaces.Route.Data
{
	public interface IRouteDataSupplierProvider
	{
		IRouteDataSupplier GetRouteDataSupplier();
	}
}