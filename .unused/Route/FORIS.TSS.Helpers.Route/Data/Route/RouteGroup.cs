﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteGroupTable :
		Table<RouteGroupRow, RouteGroupTable, IRouteData>
	{
		#region Propeties

		public override string Name
		{
			get { return "ROUTEGROUP"; }
		}

		#endregion // Properties

		#region Constructor & Dispose

		public RouteGroupTable()
		{

		}
		public RouteGroupTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion // Constructor & Dispose

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int nameIndex;
		internal int NameIndex { get { return this.nameIndex; } }

		private int descriptionIndex;
		internal int DescriptionIndex { get { return this.descriptionIndex; } }

		#endregion // Fields' indexes

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.idIndex			= this.DataTable.Columns.IndexOf( "ROUTEGROUP_ID" );
			this.nameIndex			= this.DataTable.Columns.IndexOf( "NAME" );
			this.descriptionIndex	= this.DataTable.Columns.IndexOf( "DESCRIPTION" );
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex			= -1;
			this.nameIndex			= -1;
			this.descriptionIndex	= -1;
		}

		#endregion // View

		protected override RouteGroupRow OnCreateRow( DataRow dataRow )
		{
			return new RouteGroupRow( dataRow );
		}
	}

	public class RouteGroupRow :
		Row<RouteGroupRow, RouteGroupTable, IRouteData>,
		IManyToManyRow<RouteRow, RouteTable, IRouteData, RouteGroupRouteTable>
	{
		#region IManyToManyRow<RouteRow,RouteTable,IRouteData,RouteGroupRouteTable> Members

		public ICollection<RouteRow> OppositeRows
		{
			get { return this.rowsRoute; }
		}

		private RouteGroupRouteTable routeGroupRouteTable;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public RouteGroupRouteTable Helper
		{
			get { return this.routeGroupRouteTable; }
			set { this.routeGroupRouteTable = value; }
		}

		#endregion  // IManyToManyRow<RouteRow,RouteTable,IRouteData,RouteGroupRouteTable> Members

		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Description
		{
			get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DescriptionIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Child rows

		private readonly ChildRowCollection<RouteRow, RouteTable, IRouteData> rowsRoute;

		public ChildRowCollection<RouteRow, RouteTable, IRouteData> RowsRoute
		{
			get { return this.rowsRoute; }
		}

		#endregion // Child rows

		public RouteGroupRow( DataRow dataRow ) : 
			base( dataRow )
		{
			this.rowsRoute = new ChildRowCollection<RouteRow, RouteTable, IRouteData>();
		}

		protected override void OnBuildFields() { }

		public override string ToString()
		{
			return this.Name;
		}
	}
}