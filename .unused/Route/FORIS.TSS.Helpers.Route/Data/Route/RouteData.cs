﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteData :
		Data<IRouteDataTreater>,
		IRouteData
	{
		#region Controls & Components

		private IContainer components;
		private RouteDataDispatcher tablesDataDispatcher;

		private RouteTypeTable			routeType;
		private RouteGroupTable			routeGroup;
		private RouteTable				route;
		private RouteGroupRouteTable	routeGroupRoute;

		private RouteDataAmbassador daRouteType;
		private RouteDataAmbassador daRouteGroup;
		private RouteDataAmbassador daRoute;
		private RouteDataAmbassador daRouteGroupRoute;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public RouteData( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public RouteData()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new RouteDataDispatcher( this.components );

			this.routeType			= new RouteTypeTable		(this.components );
			this.routeGroup			= new RouteGroupTable		( this.components );
			this.route				= new RouteTable			( this.components );
			this.routeGroupRoute	= new RouteGroupRouteTable	( this.components );

			this.daRouteType		= new RouteDataAmbassador();
			this.daRouteGroup		= new RouteDataAmbassador();
			this.daRoute			= new RouteDataAmbassador();
			this.daRouteGroupRoute	= new RouteDataAmbassador();

			this.daRouteType.Item		= this.routeType;
			this.daRouteGroup.Item		= this.routeGroup;
			this.daRoute.Item			= this.route;
			this.daRouteGroupRoute.Item	= this.routeGroupRoute;

			this.tablesDataDispatcher.Ambassadors.Add( this.daRouteType );
			this.tablesDataDispatcher.Ambassadors.Add( this.daRouteGroup );
			this.tablesDataDispatcher.Ambassadors.Add( this.daRoute );
			this.tablesDataDispatcher.Ambassadors.Add( this.daRouteGroupRoute );
		}

		#endregion  // Componane Designer generated code

		#region Properties

		public new IRouteDataSupplier DataSupplier
		{
			get { return (IRouteDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion  // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion  // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;
		}
		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;
		}

		#endregion  // Build & Destroy

		#region Tables properties

		[Browsable( false )]
		public RouteTypeTable RouteType
		{
			get { return this.routeType; }
		}

		[Browsable( false )]
		public RouteGroupTable RouteGroup
		{
			get { return this.routeGroup; }
		}

		[Browsable( false )]
		public RouteTable Route
		{
			get { return this.route; }
		}

		#endregion // Tables properties
	}
}