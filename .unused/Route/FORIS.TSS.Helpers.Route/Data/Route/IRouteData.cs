﻿using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Route.Data.Route;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public interface IRouteData :
		IData<IRouteDataTreater>,
		IRouteDataSupplier
	{
		RouteTypeTable RouteType { get; }
		RouteGroupTable RouteGroup { get; }
		RouteTable Route { get; }
	}
}