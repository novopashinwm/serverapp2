﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteDataDispatcher :
		DataDispatcher<IRouteData>
	{
		#region Constructor & Dispose

		public RouteDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public RouteDataDispatcher()
		{

		}

		#endregion // Constructor & Dispose

		#region Ambassadors

		private readonly RouteDataAmbassadorCollection ambassadors =
			new RouteDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new RouteDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRouteData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors
	}

	public class RouteDataAmbassadorCollection :
		DataAmbassadorCollection<IRouteData>
	{
		public new RouteDataAmbassador this[int index]
		{
			get { return (RouteDataAmbassador)base[index]; }
		}
	}

	public class RouteDataAmbassador :
		DataAmbassador<IRouteData>
	{

	}
}