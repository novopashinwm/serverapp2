﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteTable :
		Table<RouteRow, RouteTable, IRouteData>
	{
		#region Propeties

		public override string Name
		{
			get { return "ROUTE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public RouteTable()
		{

		}
		public RouteTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion // Constructor & Dispose

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int routeTypeIdIndex;
		internal int RouteTypeIdIndex { get { return this.routeTypeIdIndex; } }

		private int numberIndex;
		internal int NumberIndex { get { return this.numberIndex; } }

		private int lengthIndex;
		internal int LengthIndex { get { return this.lengthIndex; } }

		private int nameIndex;
		internal int NameIndex { get { return this.nameIndex; } }

		private int descriptionIndex;
		internal int DescriptionIndex { get { return this.descriptionIndex; } }

		private int colorIndex;
		internal int ColorIndex { get { return this.colorIndex; } }

		#endregion // Fields' indexes

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex			= this.DataTable.Columns.IndexOf( "ROUTE_ID" );
				this.routeTypeIdIndex	= this.DataTable.Columns.IndexOf( "ROUTE_TYPE_ID" );
				this.numberIndex		= this.DataTable.Columns.IndexOf( "NUMBER" );
				this.lengthIndex		= this.DataTable.Columns.IndexOf( "LENGTH" );
				this.nameIndex			= this.DataTable.Columns.IndexOf( "NAME" );
				this.descriptionIndex	= this.DataTable.Columns.IndexOf( "DESCRIPTION" );
				this.colorIndex			= this.DataTable.Columns.IndexOf( "COLOR" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex			= -1;
			this.routeTypeIdIndex	= -1;
			this.numberIndex		= -1;
			this.lengthIndex		= -1;
			this.nameIndex			= -1;
			this.descriptionIndex	= -1;
			this.colorIndex			= -1;
		}

		#endregion // View

		protected override RouteRow OnCreateRow( DataRow dataRow )
		{
			return new RouteRow( dataRow );
		}
	}

	public class RouteRow :
		Row<RouteRow, RouteTable, IRouteData>,
		IManyToManyRow<RouteGroupRow, RouteGroupTable, IRouteData, RouteGroupRouteTable>
	{
		#region IManyToManyRow<RouteGroupRow,RouteGroupTable,IRouteData,RouteGroupRouteTable> Members

		public ICollection<RouteGroupRow> OppositeRows
		{
			get { return this.rowsRouteGroup; }
		}

		private RouteGroupRouteTable routeGroupRouteTable;
		[Browsable( false )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public RouteGroupRouteTable Helper
		{
			get { return this.routeGroupRouteTable; }
			set { this.routeGroupRouteTable = value; }
		}

		#endregion  // IManyToManyRow<RouteGroupRow,RouteGroupTable,IRouteData,RouteGroupRouteTable> Members

		#region Fields

		public int RouteType
		{
			get { return (int)this.DataRow[this.Table.RouteTypeIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RouteTypeIdIndex] = value; }
		}

		public string Number
		{
			get { return ( string)this.DataRow[this.Table.NumberIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NumberIndex] = value; }
		}

		public int Length
		{
			get { return (int)this.DataRow[this.Table.LengthIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LengthIndex] = value; }
		}

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Description
		{
			get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DescriptionIndex] = value; }
		}

		public string Color
		{
			get { return (string)this.DataRow[this.Table.ColorIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ColorIndex] = value; }
		}

		#endregion  // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		#region Child rows

		private readonly ChildRowCollection<RouteGroupRow, RouteGroupTable, IRouteData> rowsRouteGroup;

		public ChildRowCollection<RouteGroupRow, RouteGroupTable, IRouteData> RowsRouteGroup
		{
			get { return this.rowsRouteGroup; }
		}

		#endregion  // Child rows

		#region Parent row

		private RouteTypeRow rowRouteType;

		public RouteTypeRow RowRouteType
		{
			get { return this.rowRouteType; }
		}

		#endregion  // Parent row

		#region View

		protected override void OnDestroyView()
		{
			this.rowRouteType = null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			this.rowRouteType = this.Data.RouteType.FindRow( this.RouteType );

			base.OnBuildView();
		}

		#endregion  // View

		public bool InGroup( RouteGroupRow routeGroupRow )
		{
			return this.routeGroupRouteTable.Linked( routeGroupRow, this );
		}
		public void ToGroup( RouteGroupRow routeGroupRow )
		{
			this.routeGroupRouteTable.Link( routeGroupRow, this );
		}
		public void FromGroup( RouteGroupRow routeGroupRow )
		{
			this.routeGroupRouteTable.Unlink( routeGroupRow, this );
		}

		public RouteRow( DataRow dataRow ) : 
			base( dataRow )
		{
			this.rowsRouteGroup = new ChildRowCollection<RouteGroupRow, RouteGroupTable, IRouteData>();
		
		}

		protected override void OnBuildFields() { }
	}
}