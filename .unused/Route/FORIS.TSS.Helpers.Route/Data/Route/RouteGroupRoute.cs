﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteGroupRouteTable :
		ManyToManyTable<RouteGroupRouteTable, RouteGroupRow, RouteGroupTable, RouteRow, RouteTable, IRouteData>
	{
		public RouteGroupRouteTable( IContainer container )
		{
			container.Add( this );
		}

		public override string Name
		{
			get { return "ROUTEGROUP_ROUTE"; }
		}

		protected override RouteGroupTable LeftTable
		{
			get { return this.Data.RouteGroup; }
		}

		protected override RouteTable RightTable
		{
			get { return this.Data.Route; }
		}

		public override int IdIndex
		{
			get { return this.idIndex; }
		}

		protected override int LeftKeyIndex
		{
			get { return this.leftKeyIndex; }
		}

		protected override int RightKeyIndex
		{
			get { return this.rightKeyIndex; }
		}

		private int idIndex			= -1;
		private int leftKeyIndex	= -1;
		private int rightKeyIndex	= -1;

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex		= -1;
			this.leftKeyIndex	= -1;
			this.rightKeyIndex	= -1;
		}

		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.idIndex		= this.DataTable.Columns.IndexOf( "ROUTEGROUP_ROUTE_ID" );
			this.leftKeyIndex	= this.DataTable.Columns.IndexOf( "ROUTEGROUP_ID" );
			this.rightKeyIndex	= this.DataTable.Columns.IndexOf( "ROUTE_ID" );
		}
	}
}