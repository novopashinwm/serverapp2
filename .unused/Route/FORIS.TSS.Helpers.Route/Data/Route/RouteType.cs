﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Route.Data.Route
{
	public class RouteTypeTable :
		Table<RouteTypeRow, RouteTypeTable, IRouteData>
	{
		#region Propeties

		public override string Name
		{
			get { return "ROUTE_TYPE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public RouteTypeTable()
		{

		}
		public RouteTypeTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int nameIndex;
		internal int NameIndex { get { return this.nameIndex; } }

		#endregion  // Fields' indexes

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex	= this.DataTable.Columns.IndexOf( "ROUTE_TYPE_ID" );
				this.nameIndex	= this.DataTable.Columns.IndexOf( "NAME" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex	= -1;
			this.nameIndex	= -1;
		}

		#endregion  // View

		protected override RouteTypeRow OnCreateRow( DataRow dataRow )
		{
			return new RouteTypeRow( dataRow );
		}
	}

	public class RouteTypeRow :
		Row<RouteTypeRow, RouteTypeTable, IRouteData>
	{
		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public RouteTypeRow( DataRow dataRow ) : base( dataRow ) { }

		protected override void OnBuildFields() { }

		public override string ToString()
		{
			return this.Name;
		}
	}
}