using System.ServiceProcess;

namespace FORIS.TSS.ServerLauncher.Service.ReportService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;

			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,

            ServicesToRun = new ServiceBase[] { new ReportService() };

			ServiceBase.Run( ServicesToRun );
		}
	}
}