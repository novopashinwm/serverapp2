using System.ComponentModel;
using System.Configuration.Install;

namespace FORIS.TSS.ServerLauncher.Service.ReportService
{
	[RunInstaller( true )]
	public partial class ProjectInstaller: Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}
	}
}