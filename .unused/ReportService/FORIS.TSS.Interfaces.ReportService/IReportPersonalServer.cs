using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;

namespace FORIS.TSS.Interfaces.ReportService
{
	public interface IReportPersonalServer : IPersonalServer
	{
        event AnonymousEventHandler<CreatedReportEventArgs> CreatedReportEvent; 

        void ReportMake(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName);
        ReportParamsDictionary GetReportParams(Guid reportGuid);
        DataSet GetGeneratedReportData(int operatorID);
    }

    /*/// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ReportMakeServerEventArgs : EventArgs
    {
        private Guid guid;
        private PARAMS paramsReport;
        private int formatType;

        public ReportMakeServerEventArgs(Guid guid, PARAMS paramsReport, int formatType)
        {
            this.guid = guid;
            this.paramsReport = paramsReport;
            this.formatType = formatType;
        }
    }
    */


}