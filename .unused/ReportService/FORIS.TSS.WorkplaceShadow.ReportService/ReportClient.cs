using System;
using System.Collections.Specialized;
using System.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.ReportService
{
	public class ReportClient : ClientBase<IReportPersonalServer>
	{
		public ReportClient() : base(new NameValueCollection(0))
		{
		}

		protected override void Start()
		{
			
		}

		protected override void Stop()
		{

        }


        #region IReportPersonalServer Members

        public void ReportMake(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName)
        {
            this.Session.ReportMake(operatorID, guid, paramsReport, formatType, fileName);
        }

        public ReportParamsDictionary GetReportParams(Guid guid)
        {
            return this.Session.GetReportParams(guid);
        }
        
        #endregion IReportPersonalServer Members

	    protected override void ReConnect()
	    {
	        throw new NotImplementedException();
	    }

        public DataSet GetGeneratedReportData(int operatorID)
	    {
            return this.Session.GetGeneratedReportData(operatorID);
        }
	}
}