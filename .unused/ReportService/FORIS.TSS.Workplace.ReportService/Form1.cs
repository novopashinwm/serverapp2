using System;
using System.Windows.Forms;
using FORIS.TSS.WorkplaceShadow.ReportService;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.Workplace.ReportService
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnTest1_Click( object sender, EventArgs e )
		{
            ReportClient client = new ReportClient();

			client.Connect( "tcp://localhost:11123/ReportSessionManager", false );

			( (IClient)client ).Start();

			//client.Session.LoadReports();
		}
	}
}