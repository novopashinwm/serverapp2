using System;
using System.Runtime.Remoting;
using System.Windows.Forms;

namespace FORIS.TSS.Workplace.ReportService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			RemotingConfiguration.Configure( AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, true );

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault( false );
			Application.Run( new Form1() );
		}
	}
}