﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using System.Runtime.InteropServices;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.Interfaces.ReportService.Params;
using FORIS.TSS.Report.Base;
using FORIS.TSS.TransportDispatcher;

//using FORIS.TSS.TransportDispatcher;

namespace ReportTspMoveHistory
{
	/// <summary>
	/// Отчёт "WEB-ГИС: история движения" 
	/// </summary>
    [Guid("E2D96FFD-097B-4e58-8707-28811695E5E7")] 
	public class MoveHistoryTspReport : TssReportBase
	{

		/// <summary>
		/// </summary>
        public MoveHistoryTspReport()
		{
		}

		/// <summary>
		/// Текстовое описание данного отчета.
		/// </summary>
		public override string ReportNameGet()
		{
            return Strings.MoveHistoryTspReport; // "История движения";
        }

        //Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета.
        public override ReportParameters ReportParametersInstanceGet()
        {
            ReportParametersThis reportParameters = new ReportParametersThis();
            ////Создание датасета для хранения списка ТС, полученного из БД.
            //DataSet dsDataFromDB = iDataSupplier.GetDataFromDB();
            //moveReportParameters.dsParamsForEditor = dsDataFromDB.Tables["Vehicle"]; ;
            ////Подключение полученных данных к комбобоксу в параметрах.
            ////FORIS.TSS.TransportDispatcher.ComboBoxEditor.DataSourceTable =dsDataFromDB.Tables["Vehicle"];
            ////FORIS.TSS.TransportDispatcher.ComboBoxEditor.ValueMember ="Garage_Number";
            ////Передача полученных из БД всех номеров ТС в список параметров.
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.DataSourceTable = dsDataFromDB.Tables["Vehicle"];
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberText = "Garage_Number";
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberTag = "Garage_Number";
            ////Возвращение параметров отчета.
            return reportParameters;
        }

        #region //
//        /// <summary>
//        /// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
//        /// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
//        /// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
//        /// </summary>
//        /// <param name="iReportParameters">Набор параметров для создания отчета</param>
//        /// <returns></returns>
//        public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
//        {
//            //Переменная для хранения полученных параметров отчета.
//            MoveReportParameters clsReportParameters = new MoveReportParameters();

//            try
//            {
//                //Проверка был ли передан набор параметров для отчета.
//                if (iReportParameters != null) 
//                {
//                    //[В функцию был передан набор параметров].
//                    //Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
//                    clsReportParameters = (MoveReportParameters)iReportParameters;
//                    //Переменная для хранения даты начала периода, на который формируется отчет.
//                    DateTime dtmReportDateFrom = clsReportParameters.DateFrom;
//                    //Переменная для хранения даты окончания периода, на который формируется отчет.
//                    DateTime dtmReportDateTo = clsReportParameters.DateTo;
					
//                    //Требуется построение отчета на дату, заданную в параметрах, или на текущую дату?
//                    if (clsReportParameters.AlwaysForCurrentDateProp == true)
//                    {
//                        //[Требуется построение отчета на текущую дату].
//                        //Установка значения свойству From параметров значения даты два дня назад. Отчет строится за последние два дня.
//                        clsReportParameters.DateFrom = DateTime.Now.AddDays(-2).Date;
//                        //Установка значения свойству To параметров значения текущей даты.
//                        clsReportParameters.DateTo = DateTime.Now.Date;
//                    }

//                    //Датасет для хранения полученных из БД данных.
//                    DataSet dsDataFromDB = new DataSet();
//                    //Получение из БД данных для заполнения листбокса с номерами ТС.
//                    dsDataFromDB = mclsInstance.GetVehicles();

//                    clsReportParameters.dsParamsForEditor = dsDataFromDB.Tables["Vehicle"];;
 
//                    //Передача полученных из БД всех номеров ТС в список параметров.
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.DataSourceTable = dsDataFromDB.Tables["Vehicle"];
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberText = "Garage_Number";
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberTag = "Garage_Number";

//                    // Проверка вводимого значения
//                    if(dsDataFromDB.Tables.Count>0 && clsReportParameters.VehicleProp.Trim()!="")
//                    {
//                        int find = 0; 
//                        for(int i=0; i<dsDataFromDB.Tables["Vehicle"].Rows.Count; i++)
//                        {
//                            if(clsReportParameters.VehicleProp.Trim() == dsDataFromDB.Tables["Vehicle"].Rows[i]["Garage_Number"].ToString().Trim())
//                            {
//                                find++;
//                            }
//                        }
//                        if(find == 0)
//                        {
//                            MessageBox.Show(Strings.WebGisTSNotRegistered);// "Введенный номер ТС не зарегистрирован!");
//                            //Trace.WriteLine(Strings.TSNotRegistered);
//                            clsReportParameters.VehicleProp = "";
//                        }
//                    }
					
//                    //Возвращаются скорректированные параметры.
//                    return clsReportParameters;
//                }
//                else
//                {
//                    //Если набор параметров не был передан в данный отчет, то создать его заново.
//                    return new MoveReportParameters();
//                }
//            }
//            catch (Exception ex)
//            {
//                //Вывод сообщения об ошибке.
//#if DEBUG
//                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
//#endif
//                //Функция возвращает пустой набор параметров.
//                return clsReportParameters;
//            }		
//        }

//        /// <summary>
//        /// Функция проверяет переданный набор параметров на корректность заполнения.
//        /// </summary>
//        /// <param name="iReportParameters">Заполненный набор параметров отчета</param>
//        /// <returns>В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false</returns>
//        public override bool ReportParametersInstanceCheck(ReportParameters iReportParameters)
//        {
//            try
//            {
//                //Сохранение полученных параметров отчета с предварительным переводом их в параметры именно для этого отчта.
//                MoveReportParameters clsReportParameters = (MoveReportParameters)iReportParameters;
//                //Получение значения дата+время начала периода из параметров путем присоединения параметра "Время" к параметру "Дата".
//                DateTime dtmReportDateFrom = DateTimeMerge(clsReportParameters.DateFrom.Date, clsReportParameters.ReportTimeFromProp);
//                //Получение значения дата+время окончания периода из параметров путем присоединения параметра "Время" к параметру "Дата".
//                DateTime dtmReportDateTo = DateTimeMerge(clsReportParameters.DateTo.Date, clsReportParameters.ReportTimeToProp);
//                //Получение из переданных параметров флага, определяющего отображается ли данный отчет заполненным, или он будет отображаться пустым.
//                bool blnShowBlankReport = clsReportParameters.ShowBlankReportProp;

//                //Требуется заполнение отчета данными?
//                if (blnShowBlankReport == false)
//                {
//                    //[Требуется заполнение отчета данными].
//                    //Анализ корректности заполнения параметров.
//                    bool blnParametersCheckResult = false;
//                    blnParametersCheckResult = (clsReportParameters.DateFrom <= clsReportParameters.DateTo) 
//                        && (clsReportParameters.VehicleProp.Trim() != "")
//                        && (clsReportParameters.ReportTimeFromProp != "") 
//                        && (clsReportParameters.ReportTimeToProp != "")
//                        && (clsReportParameters.Interval > 0)
//                        && (clsReportParameters.PositionsСount > 0)
//                        && (dtmReportDateFrom.Ticks < dtmReportDateTo.Ticks);
//                    //Функция возвращает результат проверки параметров.
//                    return blnParametersCheckResult;
//                }
//                else
//                {
//                    //[Заполнение отчета данными не требуется].
//                    //Функция возвращает положительный результат проверки параметров, потому что для пустого отчета корректность параметров смысла не имеет, а кнопку запуска построения отчета надо делать доступной.
//                    return true;
//                }
//            }
//            catch (Exception ex)
//            {
//                //Вывод сообщения об ошибке.
//#if DEBUG
//                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
//#endif
//                return false;
//            }
//        }
        #endregion //

        /// <summary>
		/// Создает отчет, используя переданные параметры
		/// </summary>
		public override ReportClass Create(ReportParameters iReportParameters)
		{
            MoveHistoryDataSet dsReport = null; 

			try
			{
                dsReport = new MoveHistoryDataSet();

                if (iReportParameters != null && iReportParameters.ShowBlankReportProp == false)
                {
                    ReportParametersThis reportParameters = (ReportParametersThis) iReportParameters;

                    DateTime dtFrom = DateTimeMerge(reportParameters.DateFrom, reportParameters.TimeFrom); 
                    DateTime dtTo = DateTimeMerge(reportParameters.DateTo, reportParameters.TimeTo);

                    // Заполнение заголовка отчета (Формирование строки в таблице ReportHeader)
                    MoveHistoryDataSet.HeaderRow rowHeader = dsReport.Header.NewHeaderRow();
                    rowHeader.mo = reportParameters.Mo;
                    rowHeader.From = dtFrom.ToShortDateString() + " " + dtFrom.ToShortTimeString();
                    rowHeader.To = dtTo.ToShortDateString() + " " + dtTo.ToShortTimeString();
                    dsReport.Header.AddHeaderRow(rowHeader);

                    // Получение данных для отчета из БД.
                    DataSet ds = this.iDataSupplier.GetLogFromDB(
                        dtFrom.ToUniversalTime(),
                        dtTo.ToUniversalTime(),
                        reportParameters.ControllerID,
                        reportParameters.Interval,
                        reportParameters.PositionsСount
                        );

                    
                    if (ds.Tables["History"].Rows.Count > 0)
                    {
                        #region Address

                        ds.Tables["History"].TableName = "MoveHistory";
                        ds.Tables["MoveHistory"].Columns["LONGITUDE"].ColumnName = "X";
                        ds.Tables["MoveHistory"].Columns["LATITUDE"].ColumnName = "Y";
                        ds.Tables["MoveHistory"].Columns.Add(new DataColumn("Address", Type.GetType("System.String")));

                        Guid guid = Guid.Empty;
                        try
                        {
                            if (reportParameters.ShowAddress == true
                                && reportParameters.MapGuid != null && reportParameters.MapGuid != String.Empty)
                            {
                                guid = new Guid(reportParameters.MapGuid);
                                if (guid != Guid.Empty)
                                    ds = this.iDataSupplier.GetMoveHistoryAddresses(guid, (HistoryDataSet) ds);
                            }
                        }
                        catch (Exception exc)
                        {
                            Trace.WriteLine("Invalid GUID for report! " + exc.ToString());
                        }

                        #endregion Address

                        // 
                        double kmSum = 0;
                        for (int i = 0; i < ds.Tables["MoveHistory"].Rows.Count; i++)
                        {
                            DataRow dr = ds.Tables["MoveHistory"].Rows[i];

                            MoveHistoryDataSet.DataRow rowData = dsReport.Data.NewDataRow();

                            if (dr["LOG_TIME"] != DBNull.Value)
                            {
                                DateTime dt = (DateTime) dr["LOG_TIME"];
                                rowData.DateTime = dt.ToLocalTime().ToString();
                            }

                            float sdeed = (float) dr["SPEED"];
                            rowData.Speed = (uint) sdeed;

                            double km = 0;
                            if (i > 1 && dr["VALID"] != DBNull.Value && (bool) dr["VALID"] == true)
                            {
                                DataRow dr_ = ds.Tables["MoveHistory"].Rows[i - 1];
                                if (dr_["VALID"] != DBNull.Value && (bool) dr_["VALID"] == true)
                                {
                                    float xx = 63166*((float) dr_["X"] - 38) - 63166*((float) dr["X"] - 38);
                                    float yy =
                                        (float) (111411*((float) dr_["Y"] - 55.665) - 111411*((float) dr["Y"] - 55.665));
                                    km = Math.Sqrt(xx*xx + yy*yy)/1000;
                                }
                            }
                            rowData.km = km;
                            kmSum += km;
                            rowData.kmSum = kmSum;

                            rowData.Address = dr["Address"].ToString();

                            dsReport.Data.AddDataRow(rowData);
                        }
                    }
                }

			    //Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
                ConstantsListDataset dsEstablishmentAttributes = TssReportBase.ReportEstablishmentAttributesFill(this.iDataSupplier);
				//Добавление в датасет данных атрибутов предприятия.
                dsReport.Merge(dsEstablishmentAttributes);

				//Создание экземпляра отчета.
				ReportClass rptResultReport = new MoveHistoryReport(); 
				//Подключение отчета к данным, собранным в DataSet'е.
                rptResultReport.SetDataSource(dsReport);	
				//Вертикальное положение бланка формы отчета.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
				//Функция возвращает сформированный экземпляр отчета.
				return rptResultReport;
			}
			catch (Exception ex)
			{
				//Вывод сообщения об ошибке.
                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
				Trace.WriteLine(ex.StackTrace);
				return null;
			}
            finally
            {
                if (dsReport != null)
                {
                    dsReport.Dispose();
                }
            }
		}

	    protected override void SetParamsDictionary()
	    {
            reportParamsDictionary.Add("Interval", new ReportParamsCommon(
                0,
                ReportParamsType.Int32,
                ReportParamsCategory.ReportParams,
                "Интервал, с",
                "Минимальный интервал между позициями истории движения.",
                false,
                true,
                0));

            reportParamsDictionary.Add("PositionsСount", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Количество позиций",
                "Максимальное количество позиций истории движения, которое будет запрошено из базы данных.",
                false,
                true,
                1));

            reportParamsDictionary.Add("DateFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Дата начала периода",
                "Дата начала периода, за который формируется отчёт.",
                false,
                true,
                2));

            reportParamsDictionary.Add("TimeFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "Время начала периода",
                "Время начала периода, за который формируется отчёт.",
                false,
                true,
                3));

            reportParamsDictionary.Add("DateTo", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Дата окончания периода",
                "Дата окончания периода, за который формируется отчёт.",
                false,
                true,
                4));

            reportParamsDictionary.Add("TimeTo", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "Время окончания периода",
                "Время окончания периода, за который формируется отчёт.",
                false,
                true,
                5));

            reportParamsDictionary.Add("Mo", new ReportParamsCommon(
                0,
                ReportParamsType.String,
                ReportParamsCategory.ReportParams,
                "Объект слежения",
                "Название (номер) объекта слежения.",
                false,
                true,
                6));

            reportParamsDictionary.Add("ShowAddress", new ReportParamsCommon(
                0,
                ReportParamsType.Boolean,
                ReportParamsCategory.ReportParams,
                "Показывать адрес",
                "Показывать/не показывать адрес в отчёте.",
                false,
                true,
                7));
        }

	    #region Подготовка списка параметров для передачи в функцию вызова хранимой процедуры
        private ParamValue[] ParamsPreparingForStorProc(DateTime dtFrom, DateTime dtTo, string vehicle, int interval, int posСount)
        {
            ParamValue[] paramValues = new ParamValue[5];

            paramValues[0] = new ParamValue("@time_begin", dtFrom);
            paramValues[1] = new ParamValue("@time_end", dtTo);
            paramValues[2] = new ParamValue("@vehicle", vehicle);
            paramValues[3] = new ParamValue("@interval", interval);
            paramValues[4] = new ParamValue("@count", posСount);
                                    
            return paramValues;
        }
        #endregion Подготовка списка параметров для передачи в функцию вызова хранимой процедуры


		/// <summary>
		/// Параметры для данного отчета.
		/// </summary>
		[Serializable]
		public class  ReportParametersThis : ReportParameters
		{


			/// <summary>
			/// Время начала периода, на который формируется отчет.
			/// </summary>
            protected string timeFrom = DateTime.Now.ToShortTimeString();
            /// <summary>
			/// Время окончания периода, на который формируется отчет.
			/// </summary>
            protected string timeTo = DateTime.Now.ToShortTimeString();

            /// <summary>
			/// Объект слежения
			/// </summary>
			protected string mo = "";
            /// <summary>
            /// Объект слежения
            /// </summary>
            protected int controllerID = 0;

            protected int interval = 60;
            protected int count = 1000;

            /// <summary>
            /// Показывать/не показывать адрес в отчёте
            /// </summary>
            protected bool showAddress = false;

            /// <summary>
            /// GUID карты
            /// </summary>
            protected string mapGuid = Guid.Empty.ToString();
            

			/// <summary>
			/// Гаражный номер ТС.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("TSNumberDN", 8, true), Category("ReportParsCat"), Description("TSNumberDesc")]
			[Editor(typeof(FORIS.TSS.Report.Base.Editors.ListBoxEditor), typeof(UITypeEditor))]
				//[Editor(typeof(FORIS.TSS.TransportDispatcher.ComboBoxEditor), typeof(UITypeEditor))]
				//[TypeConverter(typeof(ConvInToClean))]
				//public FORIS.TSS.TransportDispatcher.TagComboBoxItem VehicleProp
            public string Mo
			{
				get
				{
                    return mo;
				}
				set
				{
                    mo = value; 
				}
			}

            /// <summary>
            /// ControllerID
            /// </summary>
            [FORIS.TSS.TransportDispatcher.DisplayName("ControllerDN", 2, true), Category("ReportParsCat"), Description("ControllerDesc")]
            public int ControllerID
            {
                get
                {
                    return controllerID;
                }
                set
                {
                    controllerID = value;
                }
            }

			/// <summary>
			/// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
			[Browsable(true)]
			public override DateTime DateFrom
			{
				get
				{
					return dtDateFrom;
				}
				set
				{
					dtDateFrom = value;
				}
			}

			/// <summary>
			/// Время начала периода, на который формируется доклад.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
			[PropertyFormat(typeof(FormaterTimeExt))] 
			public string TimeFrom
			{
				get
				{
                    return timeFrom; 
				}
				set
				{ 
					//Вызов функции проверки введенного пользователем значения времени.
                    timeFrom = TssReportBase.TimeInputCheck(value);
				}
			}

			/// <summary>
			/// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
			[Browsable(true)]
			public override DateTime DateTo
			{
				get
				{
					return dtDateTo;
				}
				set
				{
					dtDateTo = value;
				}
			}

			/// <summary>
			/// Время окончания периода, на который формируется доклад.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
			[PropertyFormat(typeof(FormaterTimeExt))] 
			public string TimeTo
			{
				get
				{
                    return timeTo; 
				}
				set
				{ 
					//Вызов функции проверки введенного пользователем значения времени.
                    timeTo = TssReportBase.TimeInputCheck(value);
				}
			}

            /// <summary>
            /// Интервал
            /// </summary>
            [FORIS.TSS.TransportDispatcher.DisplayName("IntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
            public int Interval
            {
                get
                {
                    return interval;
                }
                set
                {
                    interval = value;
                }
            }

			/// <summary>
			/// Количество позиций
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
			public int PositionsСount
			{
				get
				{ 
					return count; 
				}
				set
				{ 
					count = value; 
				}
			}

            /// <summary>
            /// Показывать/не показывать адрес в отчёте
            /// </summary>
            [Browsable(false)]
            public bool ShowAddress
            {
                get
                {
                    return showAddress;
                }
                set
                {
                    showAddress = value;
                }
            }

            /// <summary>
            /// GUID карты
            /// </summary>
            [Browsable(false)]
            public string MapGuid
            {
                get { return mapGuid; }
                set { mapGuid = value; }
            }


		}
	}
}