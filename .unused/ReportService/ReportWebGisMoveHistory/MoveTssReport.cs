using System;
using System.ComponentModel;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using System.Drawing.Design;
using System.Runtime.InteropServices;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.Interfaces.ReportService.Params;
using FORIS.TSS.Report.Base;
using FORIS.TSS.TransportDispatcher;

//using FORIS.TSS.TransportDispatcher;

namespace ReportWebGisMoveHistory
{
	/// <summary>
	/// ����� "WEB-���: ������� ��������" 
	/// </summary>
	[Guid("9666515E-77FC-4EFE-875D-C928EE61FFA7")] 
	public class MoveTssReport : TssReportBase
	{

		/// <summary>
		/// </summary>
		public MoveTssReport()
		{
		}

		/// <summary>
		/// ��������� �������� ������� ������.
		/// </summary>
		public override string ReportNameGet()
		{
			return Strings.WebGisMoveRep;// "WEB-���: ������� ��������";
        }

        //������� ���������� ��������� ������, ���������� ����� ���������� ��� ������� ������.
        public override ReportParameters ReportParametersInstanceGet()
        {
            MoveReportParameters moveReportParameters = new MoveReportParameters();
            ////�������� �������� ��� �������� ������ ��, ����������� �� ��.
            //DataSet dsDataFromDB = iDataSupplier.GetDataFromDB();
            //moveReportParameters.dsParamsForEditor = dsDataFromDB.Tables["Vehicle"]; ;
            ////����������� ���������� ������ � ���������� � ����������.
            ////FORIS.TSS.TransportDispatcher.ComboBoxEditor.DataSourceTable =dsDataFromDB.Tables["Vehicle"];
            ////FORIS.TSS.TransportDispatcher.ComboBoxEditor.ValueMember ="Garage_Number";
            ////�������� ���������� �� �� ���� ������� �� � ������ ����������.
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.DataSourceTable = dsDataFromDB.Tables["Vehicle"];
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberText = "Garage_Number";
            //FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberTag = "Garage_Number";
            ////����������� ���������� ������.
            return moveReportParameters;
        }

        #region //
//        /// <summary>
//        /// ������� ���������� ��������� ������, ���������� ����� ����������������� ���������� ��� ������� ������.
//        /// ��������, ���� ���� �������� ������ ������� �� �������, ����������� �������������, �� ���� ����� ������������� ����������� ��������� ��������
//        /// � ��������� �������� �� ������ ������, ��������� ������������� � ������ ��������.
//        /// </summary>
//        /// <param name="iReportParameters">����� ���������� ��� �������� ������</param>
//        /// <returns></returns>
//        public override ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
//        {
//            //���������� ��� �������� ���������� ���������� ������.
//            MoveReportParameters clsReportParameters = new MoveReportParameters();

//            try
//            {
//                //�������� ��� �� ������� ����� ���������� ��� ������.
//                if (iReportParameters != null) 
//                {
//                    //[� ������� ��� ������� ����� ����������].
//                    //���������� ���������� ���������� ������ � ��������������� ��������� �� � ��������� ������ ��� ����� �����.
//                    clsReportParameters = (MoveReportParameters)iReportParameters;
//                    //���������� ��� �������� ���� ������ �������, �� ������� ����������� �����.
//                    DateTime dtmReportDateFrom = clsReportParameters.DateFrom;
//                    //���������� ��� �������� ���� ��������� �������, �� ������� ����������� �����.
//                    DateTime dtmReportDateTo = clsReportParameters.DateTo;
					
//                    //��������� ���������� ������ �� ����, �������� � ����������, ��� �� ������� ����?
//                    if (clsReportParameters.AlwaysForCurrentDateProp == true)
//                    {
//                        //[��������� ���������� ������ �� ������� ����].
//                        //��������� �������� �������� From ���������� �������� ���� ��� ��� �����. ����� �������� �� ��������� ��� ���.
//                        clsReportParameters.DateFrom = DateTime.Now.AddDays(-2).Date;
//                        //��������� �������� �������� To ���������� �������� ������� ����.
//                        clsReportParameters.DateTo = DateTime.Now.Date;
//                    }

//                    //������� ��� �������� ���������� �� �� ������.
//                    DataSet dsDataFromDB = new DataSet();
//                    //��������� �� �� ������ ��� ���������� ��������� � �������� ��.
//                    dsDataFromDB = mclsInstance.GetVehicles();

//                    clsReportParameters.dsParamsForEditor = dsDataFromDB.Tables["Vehicle"];;
 
//                    //�������� ���������� �� �� ���� ������� �� � ������ ����������.
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.DataSourceTable = dsDataFromDB.Tables["Vehicle"];
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberText = "Garage_Number";
//                    FORIS.TSS.TransportDispatcher.Editors.ListBoxEditor.ValueMemberTag = "Garage_Number";

//                    // �������� ��������� ��������
//                    if(dsDataFromDB.Tables.Count>0 && clsReportParameters.VehicleProp.Trim()!="")
//                    {
//                        int find = 0; 
//                        for(int i=0; i<dsDataFromDB.Tables["Vehicle"].Rows.Count; i++)
//                        {
//                            if(clsReportParameters.VehicleProp.Trim() == dsDataFromDB.Tables["Vehicle"].Rows[i]["Garage_Number"].ToString().Trim())
//                            {
//                                find++;
//                            }
//                        }
//                        if(find == 0)
//                        {
//                            MessageBox.Show(Strings.WebGisTSNotRegistered);// "��������� ����� �� �� ���������������!");
//                            //Trace.WriteLine(Strings.TSNotRegistered);
//                            clsReportParameters.VehicleProp = "";
//                        }
//                    }
					
//                    //������������ ����������������� ���������.
//                    return clsReportParameters;
//                }
//                else
//                {
//                    //���� ����� ���������� �� ��� ������� � ������ �����, �� ������� ��� ������.
//                    return new MoveReportParameters();
//                }
//            }
//            catch (Exception ex)
//            {
//                //����� ��������� �� ������.
//#if DEBUG
//                Trace.WriteLine("� ������� " + ex.Source + " ��������� ������: " + ex.Message);
//#endif
//                //������� ���������� ������ ����� ����������.
//                return clsReportParameters;
//            }		
//        }

//        /// <summary>
//        /// ������� ��������� ���������� ����� ���������� �� ������������ ����������.
//        /// </summary>
//        /// <param name="iReportParameters">����������� ����� ���������� ������</param>
//        /// <returns>� ������ ������������ ���������� ���������� true, � ������ ������������ ��� �������� ���������� ���������� false</returns>
//        public override bool ReportParametersInstanceCheck(ReportParameters iReportParameters)
//        {
//            try
//            {
//                //���������� ���������� ���������� ������ � ��������������� ��������� �� � ��������� ������ ��� ����� �����.
//                MoveReportParameters clsReportParameters = (MoveReportParameters)iReportParameters;
//                //��������� �������� ����+����� ������ ������� �� ���������� ����� ������������� ��������� "�����" � ��������� "����".
//                DateTime dtmReportDateFrom = DateTimeMerge(clsReportParameters.DateFrom.Date, clsReportParameters.ReportTimeFromProp);
//                //��������� �������� ����+����� ��������� ������� �� ���������� ����� ������������� ��������� "�����" � ��������� "����".
//                DateTime dtmReportDateTo = DateTimeMerge(clsReportParameters.DateTo.Date, clsReportParameters.ReportTimeToProp);
//                //��������� �� ���������� ���������� �����, ������������� ������������ �� ������ ����� �����������, ��� �� ����� ������������ ������.
//                bool blnShowBlankReport = clsReportParameters.ShowBlankReportProp;

//                //��������� ���������� ������ �������?
//                if (blnShowBlankReport == false)
//                {
//                    //[��������� ���������� ������ �������].
//                    //������ ������������ ���������� ����������.
//                    bool blnParametersCheckResult = false;
//                    blnParametersCheckResult = (clsReportParameters.DateFrom <= clsReportParameters.DateTo) 
//                        && (clsReportParameters.VehicleProp.Trim() != "")
//                        && (clsReportParameters.ReportTimeFromProp != "") 
//                        && (clsReportParameters.ReportTimeToProp != "")
//                        && (clsReportParameters.Interval > 0)
//                        && (clsReportParameters.Positions�ount > 0)
//                        && (dtmReportDateFrom.Ticks < dtmReportDateTo.Ticks);
//                    //������� ���������� ��������� �������� ����������.
//                    return blnParametersCheckResult;
//                }
//                else
//                {
//                    //[���������� ������ ������� �� ���������].
//                    //������� ���������� ������������� ��������� �������� ����������, ������ ��� ��� ������� ������ ������������ ���������� ������ �� �����, � ������ ������� ���������� ������ ���� ������ ���������.
//                    return true;
//                }
//            }
//            catch (Exception ex)
//            {
//                //����� ��������� �� ������.
//#if DEBUG
//                Trace.WriteLine("� ������� " + ex.Source + " ��������� ������: " + ex.Message);
//#endif
//                return false;
//            }
//        }
        #endregion //

        /// <summary>
		/// ������� �����, ��������� ���������� ���������
		/// </summary>
		public override ReportClass Create(ReportParameters iReportParameters)
		{
			//���������� ���������� ���������� ������ � ��������������� ��������� �� � ��������� ������ ��� ����� �����.
			MoveReportParameters clsReportParameters;
            clsReportParameters = (MoveReportParameters)iReportParameters;
            //�������� �������� ��� �������� ���������� ������.
			DataSet dsMoveDetailHistory; 

			try
			{
				//���������� ��� �������� ���� ������ �������, �� ������� ������� �������� �����. 
				DateTime dtmReportDateFrom;
				//���������� ��� �������� ���� ��������� �������, �� ������� ������� �������� �����.
				DateTime dtmReportDateTo;

				//����������� ���� �� ������� ������� ����������� �����: ���� �� ����, ��������� � ����������, ���� �� ������� ����. ����� ������������ ��������� ����� AlwaysForCurrentDateProp gfhf
				if (clsReportParameters.AlwaysForCurrentDateProp == true)
				{
					//[����� ��������� ����������� �� ������� ����].
					//��������� �������� �������� From ���������� �������� ���� ��� ��� �����. ����� �������� �� ��������� ��� ���.
					dtmReportDateFrom = DateTime.Now.AddDays(-2).Date;
					//��������� �������� �������� To ���������� �������� ������� ����.
					dtmReportDateTo = DateTime.Now.Date;
				}
				else
				{
					//[����� ����������� �� ����, ��������� � ����������].
					//��������� ���� ���� ������ �������, �� ������� ������� �������� �����, �� ����������.
					dtmReportDateFrom = clsReportParameters.DateFrom;
					//��������� ���� ���� ��������� �������, �� ������� ������� �������� �����, �� ����������.
					dtmReportDateTo = clsReportParameters.DateTo;
				}
			
				//��������� �� ���������� ������ �������� ������� ���������� ����� � �����.
				//�������� ���������� �������� ���������� � ������ set ��������.
				DateTime dtmReportDateTimeFrom = DateTimeMerge(dtmReportDateFrom, clsReportParameters.ReportTimeFromProp);
				//���������� ������� � UTC ��� ������������ ����������� ������� �� ��.
				dtmReportDateTimeFrom = dtmReportDateTimeFrom.ToUniversalTime();

				DateTime dtmReportDateTimeTo = DateTimeMerge(dtmReportDateTo, clsReportParameters.ReportTimeToProp);
				//���������� ������� � UTC ��� ������������ ����������� ������� �� ��.
				dtmReportDateTimeTo = dtmReportDateTimeTo.ToUniversalTime();

				//��������� �� ���������� ���������� �����, ������������� ������������ �� ������ ����� �����������, ��� �� ����� ������������ ������.
				bool blnShowBlankReport = clsReportParameters.ShowBlankReportProp;

				//����� ��������� ���������� �������� ������� ��� ������ �� ��.
                dsMoveDetailHistory = this.iDataSupplier.GetDataFromDB(
                    this.ParamsPreparingForStorProc(
                        clsReportParameters.Positions�ount,
                        clsReportParameters.Interval,
                        dtmReportDateTimeFrom,
                        dtmReportDateTimeTo,
                        clsReportParameters.VehicleProp.Trim()),
                    "dbo.GetMoveDetailHistory",
                    new string[] { "CountAll", "CountInterval", "Interval", "Drivers", "MoveHistory" }
                    );

                float distSum = 0;

				int group=0;
				string data=string.Empty;
				dsMoveDetailHistory.Tables["MoveHistory"].Columns.Add(new DataColumn("Group", Type.GetType("System.Int32")));			
				dsMoveDetailHistory.Tables["MoveHistory"].Columns.Add(new DataColumn("Temperature", Type.GetType("System.String")));

				// ����� ������ �����
				if (clsReportParameters.ShowAddress /*&& mclsInstance is IServer*/)
				{
					Guid guid = Guid.Empty;

					try
					{
						if (clsReportParameters.MapGuid != null &&
						    clsReportParameters.MapGuid != String.Empty)
							guid = new Guid(clsReportParameters.MapGuid);
					}
					catch(Exception exc)
					{
						Trace.WriteLine("Invalid GUID for report! " + exc.ToString());
					}

                    if (guid != Guid.Empty)
                        dsMoveDetailHistory = this.iDataSupplier.GetAddressByPoint(dsMoveDetailHistory);
				}

				foreach (DataRow dr in dsMoveDetailHistory.Tables["MoveHistory"].Rows)
				{
					dr.BeginEdit();

					dr[5]=FORIS.TSS.BusinessLogic.TimeHelper.GetLocalTime(Convert.ToInt32(dr.ItemArray[5].ToString()));

					//��������� ������� ����� (���� ��������� ����� ��������� ����������� ������)
					if (dr[4].ToString()!=data)
					{
						data=dr[4].ToString();
						group++;
                        distSum = 0;
					}
					dr["Group"]=group;

                    // ������ ����������� ����� �������� ����������������� ������ �� ������ ����� �� �������
                    if (dr["dist"] != DBNull.Value && dr["dist"].ToString() != String.Empty)
                    {
                        distSum += (float)Convert.ToDouble(dr["dist"].ToString());
                    }
                    string[] dist = distSum.ToString().Split(',');
                    if (dist.Length > 1)
                        dist[0] += "," + (dist[1].Length > 3 ? dist[1].Substring(0, 3) : dist[1]);
                    dr["Temperature"] = dist[0];

					dr.EndEdit();
				}
				dsMoveDetailHistory.Tables["MoveHistory"].Columns.Remove("Temperature_");
		
				
				foreach (DataRow dr in dsMoveDetailHistory.Tables["Interval"].Rows)
				{
					if (dr.ItemArray[0].ToString()!=string.Empty && dr.ItemArray[1].ToString()!=string.Empty)
					{
						dr.BeginEdit();
						dr[0]=FORIS.TSS.BusinessLogic.TimeHelper.GetLocalTime(Convert.ToInt32(dr.ItemArray[0].ToString()));
						dr[1]=FORIS.TSS.BusinessLogic.TimeHelper.GetLocalTime(Convert.ToInt32(dr.ItemArray[1].ToString()));
						dr.EndEdit();
					}
				}

                #region ��������� ������ ��� ������ �����, �� ������ ���� �����-������ ������� � ������ �����
                if (dsMoveDetailHistory.Tables["Interval"].Rows.Count < 1)
                {
                    DataRow row = dsMoveDetailHistory.Tables["Interval"].NewRow();
                    row["From"] = dtmReportDateFrom.ToShortDateString() + " " + clsReportParameters.ReportTimeFromProp;
                    row["To"] = dtmReportDateTimeTo.ToShortDateString() + " " + clsReportParameters.ReportTimeToProp;
                    dsMoveDetailHistory.Tables["Interval"].Rows.Add(row);
                }
                else if (dsMoveDetailHistory.Tables["Interval"].Rows[0]["From"].ToString().Trim() == ""
                    || dsMoveDetailHistory.Tables["Interval"].Rows[0]["To"].ToString().Trim() == "")
                {
                    DataRow row = dsMoveDetailHistory.Tables["Interval"].Rows[0];
                    row["From"] = dtmReportDateFrom.ToShortDateString() + " " + clsReportParameters.ReportTimeFromProp;
                    row["To"] = dtmReportDateTimeTo.ToShortDateString() + " " + clsReportParameters.ReportTimeToProp;
                }
                if (dsMoveDetailHistory.Tables["MoveHistory"].Rows.Count < 1)
                {
                    DataRow row = dsMoveDetailHistory.Tables["MoveHistory"].NewRow();
                    row["PUBLIC_NUMBER"] = clsReportParameters.VehicleProp.Trim();
                    dsMoveDetailHistory.Tables["MoveHistory"].Rows.Add(row);
                }
                #endregion ��������� ������ ��� ������ �����, �� ������ ���� �����-������ ������� � ������ �����


                //����� ������� ���������� ������� � ������ ����� � ���������� ����������� (�������� �����������, ������� ��������� ������ � �.�.).
                ConstantsListDataset dsEstablishmentAttributes = TssReportBase.ReportEstablishmentAttributesFill(this.iDataSupplier);
				//���������� � ������� ������ ��������� �����������.
				dsMoveDetailHistory.Merge(dsEstablishmentAttributes);

				//�������� ���������� ������.
				ReportClass rptResultReport = new MoveReport(); 
				//����������� ������ � ������, ��������� � DataSet'�.
				rptResultReport.SetDataSource(dsMoveDetailHistory);	
				//������������ ��������� ������ ����� ������.
				rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
				//������� ���������� �������������� ��������� ������.
				return rptResultReport;
			}
			catch (Exception ex)
			{
				//����� ��������� �� ������.
                Trace.WriteLine("� ������� " + ex.Source + " ��������� ������: " + ex.Message);
				Trace.WriteLine(ex.StackTrace);
				return null;
			}
		}

	    protected override void SetParamsDictionary()
	    {
            reportParamsDictionary.Add("Interval", new ReportParamsCommon(
                0,
                ReportParamsType.Int32,
                ReportParamsCategory.ReportParams,
                "��������, �",
                "����������� �������� ����� ��������� ������� ��������.",
                false,
                true,
                0));

            reportParamsDictionary.Add("Positions�ount", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "���������� �������",
                "������������ ���������� ������� ������� ��������, ������� ����� ��������� �� ���� ������.",
                false,
                true,
                1));

            reportParamsDictionary.Add("DateFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "���� ������ �������",
                "���� ������ �������, �� ������� ����������� �����.",
                false,
                true,
                2));

            reportParamsDictionary.Add("TimeFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "����� ������ �������",
                "����� ������ �������, �� ������� ����������� �����.",
                false,
                true,
                3));

            reportParamsDictionary.Add("DateTo", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "���� ��������� �������",
                "���� ��������� �������, �� ������� ����������� �����.",
                false,
                true,
                4));

            reportParamsDictionary.Add("TimeTo", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "����� ��������� �������",
                "����� ��������� �������, �� ������� ����������� �����.",
                false,
                true,
                5));

            reportParamsDictionary.Add("Mo", new ReportParamsCommon(
                0,
                ReportParamsType.String,
                ReportParamsCategory.ReportParams,
                "������ ��������",
                "�������� (�����) ������� ��������.",
                false,
                true,
                6));

            reportParamsDictionary.Add("ShowAddress", new ReportParamsCommon(
                0,
                ReportParamsType.Boolean,
                ReportParamsCategory.ReportParams,
                "���������� �����",
                "����������/�� ���������� ����� � ������.",
                false,
                true,
                7));

	    }

	    #region ���������� ������ ���������� ��� �������� � ������� ������ �������� ���������
        private ParamValue[] ParamsPreparingForStorProc(int pos�ount, int interval, DateTime dtFrom, DateTime dtTo, string vehicle)
        {
            ParamValue[] paramValues = new ParamValue[5];

            paramValues[0] = new ParamValue("@count", pos�ount);
            paramValues[1] = new ParamValue("@interval", interval);
            paramValues[2] = new ParamValue("@time_from", dtFrom);
            paramValues[3] = new ParamValue("@time_to", dtTo);
            paramValues[4] = new ParamValue("@vehicle", vehicle);
                                    
            return paramValues;
        }
        #endregion ���������� ������ ���������� ��� �������� � ������� ������ �������� ���������


		/// <summary>
		/// ��������� ��� ������� ������.
		/// </summary>
		[Serializable]
		public class  MoveReportParameters : ReportParameters
		{

			/// <summary>
			/// ����������/�� ���������� ����� � ������
			/// </summary>
			protected bool showAddress = false;

			/// <summary>
			/// GUID �����
			/// </summary>
			protected string mapGuid = Guid.Empty.ToString();

			/// <summary>
			/// ��� �������� �������� �����, ������������� ��� ������� ����������� ������� ����. ���� �� ����� true, �� ������� ���� �� ���������, ����� ������ ������������� �������.
			/// </summary>
			protected bool mblnAlwaysCurrentDate = false;
			/// <summary>
			/// ����� ������ �������, �� ������� ����������� �����.
			/// </summary>
			protected string mstrReportTimeFrom = DateTime.Now.ToShortTimeString();
			/// <summary>
			/// ����� ��������� �������, �� ������� ����������� �����.
			/// </summary>
			protected string mstrReportTimeTo = DateTime.Now.ToShortTimeString();
			/// <summary>
			/// �������� ����� ��.
			/// </summary>
			protected string strVehicleNumber = "";
			//protected FORIS.TSS.TransportDispatcher.TagComboBoxItem tcbiVehicleNumber = new TagComboBoxItem("", null);

			protected int count=1000;
			protected int interval=60;

			// ����� ������ ��� Editor-��
			public DataTable dsParamsForEditor;


			/// <summary>
			/// ����������/�� ���������� ����� � ������
			/// </summary>
			[Browsable(false)]
			public bool ShowAddress
			{
				get
				{
					return showAddress;
				}
				set
				{
					showAddress = value;
				}
			}


			/// <summary>
			/// GUID �����
			/// </summary>
			[Browsable(false)]
			public string MapGuid
			{
				get { return mapGuid; }
				set { mapGuid = value; }
			}

			/// <summary>
			/// �������� ����� ��.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("TSNumberDN", 8, true), Category("ReportParsCat"), Description("TSNumberDesc")]
			[Editor(typeof(FORIS.TSS.Report.Base.Editors.ListBoxEditor), typeof(UITypeEditor))]
				//[Editor(typeof(FORIS.TSS.TransportDispatcher.ComboBoxEditor), typeof(UITypeEditor))]
				//[TypeConverter(typeof(ConvInToClean))]
				//public FORIS.TSS.TransportDispatcher.TagComboBoxItem VehicleProp
			public string VehicleProp
			{
				get
				{ 
					return strVehicleNumber;
				}
				set
				{ 
					strVehicleNumber = value; 
				}
			}

			/// <summary>
			/// ����, ������������ ��� ������� ����������� ������� ����. ���� �� ����� true, �� ������� ���� �� ���������, ����� ������ ������������� �������.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("InsertCurrDateDN"), Category("ReportParsCat"), Description("InsertCurrDateDesc")]
			[Browsable(false)]
			public bool AlwaysForCurrentDateProp
			{
				get
				{ 
					return mblnAlwaysCurrentDate; 
				}
				set
				{ 
					mblnAlwaysCurrentDate = value; 
				}
			}

			/// <summary>
			/// �������� - ���� ������ �������, �� ������� ����������� ����� (���� ����� ������������� �� ������).
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
			[Browsable(true)]
			public override DateTime DateFrom
			{
				get
				{
					return dtDateFrom;
				}
				set
				{
					dtDateFrom = value;
				}
			}

			/// <summary>
			/// ����� ������ �������, �� ������� ����������� ������.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
			[PropertyFormat(typeof(FormaterTimeExt))] 
			public string ReportTimeFromProp
			{
				get
				{ 
					return mstrReportTimeFrom; 
				}
				set
				{ 
					//����� ������� �������� ���������� ������������� �������� �������.
					mstrReportTimeFrom = TssReportBase.TimeInputCheck(value);
				}
			}

			/// <summary>
			/// �������� - ���� ��������� �������, �� ������� ����������� ����� (���� ����� ����������� �� ������).
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
			[Browsable(true)]
			public override DateTime DateTo
			{
				get
				{
					return dtDateTo;
				}
				set
				{
					dtDateTo = value;
				}
			}

			/// <summary>
			/// ����� ��������� �������, �� ������� ����������� ������.
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
			[PropertyFormat(typeof(FormaterTimeExt))] 
			public string ReportTimeToProp
			{
				get
				{ 
					return mstrReportTimeTo; 
				}
				set
				{ 
					//����� ������� �������� ���������� ������������� �������� �������.
					mstrReportTimeTo = TssReportBase.TimeInputCheck(value);
				}
			}

			/// <summary>
			/// ���������� �������
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
			public int Positions�ount
			{
				get
				{ 
					return count; 
				}
				set
				{ 
					count = value; 
				}
			}
			/// <summary>
			/// ��������
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("IntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
			public int Interval
			{
				get
				{ 
					return interval; 
				}
				set
				{ 
					interval = value; 
				}
			}


			/// <summary>
			/// ����, ���������� ����������� ��� ��� (�� ��������� ���)
			/// </summary>
			protected bool bTemperatureProp = false;
			/// <summary>
			/// ����, ���������� ����������� ��� ��� (�� ��������� ���)
			/// </summary>
			[FORIS.TSS.TransportDispatcher.DisplayName("PrintTempDN", 3, true), Category("ReportParsCat"), Description("PrintTempDesc")]
			public bool TemperatureProp
			{
				get
				{ 
					return bTemperatureProp; 
				}
				set
				{ 
					bTemperatureProp = value; 
				}
			}


		}
	}
}