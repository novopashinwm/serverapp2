﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.TspTerminal;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.Helpers;
using FORIS.TSS.Report.Base;
using FORIS.TSS.WorkplaceSharnier.TspTerminal;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.ReportService
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ReportDataSupplier : IReportDataSupplier
	{
		private readonly IDatabaseDataSupplier iDataSupplier;
		private readonly IAddressSupplier iAddressSupplier;
        private readonly ITspTerminalPersonalServer iTspTerminalPersonalServer;

        public ReportDataSupplier(IDatabaseDataSupplier iDataSupplier, IAddressSupplier iAddressSupplier, ITspTerminalPersonalServer iTspTerminalPersonalServer)
		{
			this.iDataSupplier = iDataSupplier;
			this.iAddressSupplier = iAddressSupplier;
            this.iTspTerminalPersonalServer = iTspTerminalPersonalServer;
		}

		public DataSet GetDataFromDB( ParamValue[] @params, string procedureName, string[] strTablesNames )
		{
            if (this.iDataSupplier == null)
                return null;

            return this.iDataSupplier.GetDataFromDB(@params, procedureName, strTablesNames);
		}

		public string GetConstant( string name )
		{
            if (this.iDataSupplier == null)
                return "";
            
            return this.iDataSupplier.GetConstant(name);
		}

		public string GetNearestAddress( double latitude, double longitude )
		{
            if (this.iAddressSupplier == null)
                return "";

            return this.iAddressSupplier.GetNearestAddress(latitude, longitude);
		}

		public DataSet GetAddressByPoint( DataSet dsMoveDetailHistory )
		{
			if ( this.iAddressSupplier == null )
				return dsMoveDetailHistory;

			return this.iAddressSupplier.GetAddressByPoint( dsMoveDetailHistory );
		}

        public DataSet GetMoveHistoryAddresses(Guid mapGuid, HistoryDataSet dsMoveHistory)
	    {
            if (this.iAddressSupplier == null)
                return dsMoveHistory;

            return this.iAddressSupplier.GetMoveHistoryAddresses(mapGuid, dsMoveHistory);
        }

	    /// <summary>
        /// Возвращает отсортированный по времени список 
        /// последних позиций из БД за указанный период времени
        /// </summary>
        /// <param name="beginTime">начало периода. UTC</param>
        /// <param name="endTime">конец периода. UTC</param>
        /// <param name="controllerId">инедтификатор контроллера</param>
        /// <param name="interval">интервал между позициями в секундах</param>
        /// <param name="count">максимальное количество возвращаемых позиций</param>
        /// <returns>отсортированный по времени список последних позиций</returns>
        public DataSet GetLogFromDB( //HistoryDataSet GetLogFromDB(
            DateTime beginTime, DateTime endTime,
            int controllerId,
            int interval, int count
            )
        {
            return
                this.iTspTerminalPersonalServer.GetLogFromDB(
                    beginTime,
                    endTime,
                    controllerId,
                    interval,
                    count);
        }

	}
}