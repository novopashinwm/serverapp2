using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Interfaces.ReportService;

namespace FORIS.TSS.ServerApplication.ReportService
{
	public class ReportPersonalServer:
		PersonalServerBase<ReportServer>,
		IReportPersonalServer
	{

        /// <summary>
        /// ������� ������� ��������� � ���������
        /// </summary>
        public event AnonymousEventHandler<CreatedReportEventArgs> CreatedReportEvent; 

		public override string ApplicationName
		{
			get { return "ReportServer"; }
		}

		public override DataSet GetRecords( string tablename, string where )
		{
			throw new NotImplementedException();
		}

		protected override bool CheckPassword( string password )
		{
			throw new NotImplementedException();
		}

		protected override void SetPassword( string password, int idOperator )
		{
			throw new NotImplementedException();
		}

		protected override ISecurity GetSecurity()
		{
			throw new NotImplementedException();
		}

		public override IPersonalServer GetAdmin()
		{
			throw new NotImplementedException();
		}

		public override TssPrincipalInfo GetPrincipal()
		{
			return
				new TssPrincipalInfo(
					this.SessionInfo.OperatorInfo.Login,
					"OPERATOR",
					this.SessionInfo.OperatorInfo.OperatorID,
					TssPrincipalType.Operator
					);
		}


        private delegate CreatedReportEventArgs ReportMakeDelegate(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName);

        public void ReportMake(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName)
	    {
            ReportMakeDelegate rmd = new ReportMakeDelegate(ReportMakeThread);
            rmd.BeginInvoke(operatorID, guid, paramsReport, formatType, fileName, new AsyncCallback(ReportMakeCallBack), rmd);
        }

        private CreatedReportEventArgs ReportMakeThread(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName)
        {
            // �������� ��������������� ���� � ����������� ������
            return this.server.ReportMake(operatorID, guid, paramsReport, formatType, fileName);
        } 

        private void ReportMakeCallBack(IAsyncResult ar)
        {
            ReportMakeDelegate rmd = (ReportMakeDelegate)ar.AsyncState;
            CreatedReportEventArgs row = rmd.EndInvoke(ar);

            // ����� ������������ ������� � ���������� ����������, � �������
            // ����� �������������� ����� ��������� � ������ � ���� � ����� ������

            if (CreatedReportEvent != null)
                CreatedReportEvent(row);
        }

        public ReportParamsDictionary GetReportParams(Guid reportGuid)
        {
            return this.server.GetReportParams(reportGuid);
        }

	    public DataSet GetGeneratedReportData(int operatorID)
	    {
            return this.server.GetGeneratedReportData(operatorID);
	    }
	}

}