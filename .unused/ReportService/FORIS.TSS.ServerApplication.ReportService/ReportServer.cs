using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.Report.Base;
using FORIS.TSS.WorkplaceShadow.Geo;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.TspTerminal;

namespace FORIS.TSS.ServerApplication.ReportService
{
	public class ReportServer : ServerBase<ReportServer>
	{

        private readonly ReportServerDispatcher serverDispatcher;
        private readonly ReportDatabaseManager databaseManager;

	    private ReportsFactory reportsFactory;
	    private ReportDataSupplier reportDataSupplier;

        public ReportServer(SessionManager<ReportServer> sessionManager, NameValueCollection properties)
			: base(sessionManager, properties)
		{
            this.geoServerUrl = Globals.AppSettings["geoServer"];
            this.terminalServerUrl = Globals.AppSettings["terminalServer"];

            this.serverDispatcher = new ReportServerDispatcher();
			this.databaseManager = new ReportDatabaseManager(); 
            this.databaseManager.Database = Globals.TssDatabaseManager.DefaultDataBase; 

			Init();
        }

		protected override ServerDispatcher<ReportServer> GetServerDispatcher()
		{
			return this.serverDispatcher;
		}

		protected override IDatabaseDataSupplier GetDatabaseDataSupplier()
		{
			return this.databaseManager;
		}

		#region IServer Members

		protected override void Start()
		{
            this.ConnectGeoClient();
            this.ConnectTerminalClient();

            this.reportDataSupplier = new ReportDataSupplier(this.databaseManager, this.geoClient, this.terminalClient.Session);
            
            this.reportsFactory = ReportsFactory.Instance(this.reportDataSupplier);

            Trace.WriteLine( "Start", "ReportServer" ); 

        }

		protected override void Stop()
		{
            this.DisconnectTerminalClient();
            this.DisconnectGeoClient();

            Trace.WriteLine("Stop", "ReportServer");
		}

		#endregion

		#region Security

		//public override IList<TssPrincipalInfo> GetPrincipals()
		//{
		//    throw new System.NotImplementedException();
		//}

		public override ISecurityModel GetAcl( SecureObjectInfo objectInfo )
		{
			throw new System.NotImplementedException();
		}

		public override void SetACL( SecureObjectInfo objectInfo, ISecurityModel security )
		{
			throw new System.NotImplementedException();
		}

		#endregion // Security 


        public ReportParamsDictionary GetReportParams(Guid guid)
        {
            return this.reportsFactory.GetReportParams(guid);
        }


        public CreatedReportEventArgs ReportMake(int operatorID, Guid guid, PARAMS paramsReport, int formatType, string fileName)
        {
            return this.reportsFactory.ReportMake(operatorID, guid, paramsReport, formatType, fileName);
        }

        #region GeoClient ��� ������ ������� ��� ������

        private readonly object mapLockObject = new object();

        private readonly string geoServerUrl;
        private GeoClient geoClient;

        private void ConnectGeoClient()
        {
            lock (this.mapLockObject)
            {
                try
                {
                    if (geoServerUrl == null || geoServerUrl == String.Empty)
                        return;

                    geoClient = new GeoClient();
                    geoClient.Connect(geoServerUrl, false);
                }
                catch (Exception)
                {
                    Trace.WriteLine("������ ����������� � ������� �����");

                    geoClient = null;
                }
            }
        }

        private void DisconnectGeoClient()
        {
            lock (this.mapLockObject)
            {
                if (geoClient != null)
                {
                    geoClient.Dispose();
                    geoClient = null;
                }
            }
        }

        #endregion GeoClient ��� ������ ������� ��� ������


        #region TerminalClient ��� ��������� �������

        private readonly object terminalLockObject = new object();

        private readonly string terminalServerUrl;
        private TspTerminalClient terminalClient;

        private void ConnectTerminalClient()
        {
            lock (this.terminalLockObject)
            {
                try
                {
                    if (terminalServerUrl == null || terminalServerUrl == String.Empty)
                        return;

                    if (this.terminalClient != null)
                    {
                        throw new InvalidOperationException("������ ���������� ��� ���������");
                    }

                    this.terminalClient = new TspTerminalClient(new NameValueCollection());
                    this.terminalClient.Connect(this.terminalServerUrl, false);
                    ((IClient)this.terminalClient).Start();

                }
                catch (Exception)
                {
                    Trace.WriteLine("������ ����������� � ������������� �������");

                    terminalClient = null;
                }
            }
        }

        private void DisconnectTerminalClient()
        {
            lock (this.terminalLockObject)
            {
                if (terminalClient != null)
                {
                    //((IClient)this.terminalClient).Stop();
                    terminalClient.Dispose();
                    terminalClient = null;
                }
            }
        }

        #endregion TerminalClient ��� ��������� �������

	    public DataSet GetGeneratedReportData(int operatorID)
	    {
            return this.reportsFactory.GetGeneratedReportData(operatorID);
	    }
	}
}