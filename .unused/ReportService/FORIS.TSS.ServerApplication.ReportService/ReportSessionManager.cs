namespace FORIS.TSS.ServerApplication.ReportService
{
	public class ReportSessionManager: 
		SessionManager<ReportServer>
	{
		private readonly ReportServerDispatcher serverDispatcher =
			new ReportServerDispatcher();

		protected override ServerDispatcher<ReportServer> GetServerDispatcher()
		{
			return this.serverDispatcher;
		}

		/// <summary>
		/// ����� ������� ������ ������ �� �������
		/// </summary>
		/// <returns>������ ����� - ������������ ������</returns>
		/// <remarks>
		/// �������� ������� ���������� �����-������ ��������� 
		/// � ���� �����, ���� ����� ���� ��������� �������
		/// ������ ��������� ����� � ����������� �� ������� �������
		/// </remarks>
		protected override PersonalServerBase<ReportServer> OnCreatePersonalServer()
		{
			return new ReportPersonalServer();
		}
	}
}