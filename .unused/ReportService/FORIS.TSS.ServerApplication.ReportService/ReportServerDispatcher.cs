using System.ComponentModel;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.ReportService
{
	public class ReportServerDispatcher:
		ServerDispatcher<ReportServer>
	{
		#region Constructor & 

		public ReportServerDispatcher()
		{
			
		}
		public ReportServerDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion // Constructor &

        private readonly ReportServerAmbassadorCollection ambassadors =
            new ReportServerAmbassadorCollection();

		protected override AmbassadorCollection<IServerItem<ReportServer>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new ReportServerAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
	}

	public class ReportServerAmbassadorCollection:
		ServerAmbassadorCollection<ReportServer>
	{
        public new ReportServerAmbassador this[int index]
		{
            get { return (ReportServerAmbassador)base[index]; }
		}
	}

    public class ReportServerAmbassador :
		ServerAmbassador<ReportServer>
	{
		
	}
}