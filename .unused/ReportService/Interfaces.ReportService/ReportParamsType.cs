﻿using System;

namespace FORIS.TSS.Interfaces.ReportService
{
    [Serializable]
    public enum ReportParamsType
    {
        #region Common Types
        /// <summary>
        /// Параметр - GUID. 
        /// </summary>
        Guid,
        /// <summary>
        /// Параметр - String. 
        /// </summary>
        String,
        /// <summary>
        /// Параметр - Int32. 
        /// </summary>
        Int32,
        /// <summary>
        /// Параметр - DateTime.
        /// </summary>
        DateTime,
        /// <summary>
        /// Параметр - Boolean
        /// </summary>
        Boolean,
        #endregion // Common Types

        #region TSP Types
        /// <summary>
        /// Параметр - Календарная Дата. 
        /// </summary>
        Date,
        /// <summary>
        /// Параметр - Время в виде строки формата "HH:mm". 
        /// </summary>
        Time_HHmm_String,
        /// <summary>
        /// Параметр - Имя Объекта Слежения, редактируется в виде выпадающего списка.
        /// </summary>
        MobileName

        #endregion // TSP Types
    }
}
