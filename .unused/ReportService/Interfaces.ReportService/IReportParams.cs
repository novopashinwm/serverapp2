﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Interfaces.ReportService
{
    public interface IReportParams
    {
        /// <summary>
        /// Категория параметра. Это свойство полезно при редактировании параметра отчёта с помощью PropertyGrid.
        /// </summary>
        string Category { get;  }
        
        /// <summary>
        /// Уникальный идентификатор параметра отчёта. Данное свойство должно инициализироваться из БД.
        /// </summary>
        int ID { get; }
        
        /// <summary>
        /// Тип параметра отчёта.
        /// </summary>
        ReportParamsType Type { get; }
        
        /// <summary>
        /// Название параметра отчёта. Это свойство полезно при редактировании параметра отчёта с помощью PropertyGrid.
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// Описание параметра отчёта. Это свойство полезно при редактировании параметра отчёта с помощью PropertyGrid.
        /// </summary>
        string Description { get; }
        
        /// <summary>
        /// Флаг "только чтение". Это свойство влияет на возможность редактирования параметра отчёта.
        /// </summary>
        bool ReadOnly { get; }
        
        /// <summary>
        /// Флаг "видимость". Это свойство влияет на возможность отображения параметра отчёта.
        /// </summary>
        bool Visable { get; }
        
        /// <summary>
        /// Порядковый номер параметра отчёта. Это свойство полезно при редактировании параметра отчёта с помощью PropertyGrid.
        /// </summary>
        int Order { get; }

        /// <summary>
        /// Значение параметра.
        /// </summary>
        object Value { get; set; }        
    }
}
