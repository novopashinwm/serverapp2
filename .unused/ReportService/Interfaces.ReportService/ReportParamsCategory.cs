﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Interfaces.ReportService
{
    public class ReportParamsCategory
    {
        public static string Report
        {
            get { return "Отчёт";  }
        }

        public static string ReportParams
        {
            get { return "Параметры отчёта"; }
        }
    }
}
