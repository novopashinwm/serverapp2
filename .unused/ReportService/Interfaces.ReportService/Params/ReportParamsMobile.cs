﻿using System;

namespace FORIS.TSS.Interfaces.ReportService.Params
{
    [Serializable]
    public class ReportParamsMobile : ReportParamsBase
    {
        public ReportParamsMobile(int id, ReportParamsType type, string category, string name, string description, bool readOnly, bool visable, int order)
            : base(id, type, category, name, description, readOnly, visable, order)
        {
        }

        public ReportParamsMobile(int id, ReportParamsType type, string category, string name, string description, bool readOnly, bool visable, int order, object value)
            : base(id, type, category, name, description, readOnly, visable, order, value)
        {
        }
    }
}
