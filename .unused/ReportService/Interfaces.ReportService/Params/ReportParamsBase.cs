﻿using System;

namespace FORIS.TSS.Interfaces.ReportService.Params
{
    [Serializable]
    public abstract class ReportParamsBase : IReportParams
    {
        int id;
        ReportParamsType type;
        string name;
        string description;
        bool readOnly;
        bool visable;
        string category;
        int order;
        object value;

        /// <summary>
        /// Базовый класс параметра отчёта
        /// </summary>
        /// <param name="id">Идентификатор параметра в БД</param>
        /// <param name="type">Тип параметра</param>
        /// <param name="category">Категория</param>
        /// <param name="name">Название параметра</param>
        /// <param name="description">Описание параметра</param>
        /// <param name="readOnly">Флаг "только чтение"</param>
        /// <param name="visable">Флаг видимости</param>
        /// <remarks>Данный объект может быть передан в PropertyGridEx для отображения и редактирования.</remarks>
        public ReportParamsBase(int id, ReportParamsType type, string category, string name, string description, bool readOnly, bool visable, int order)
        {
            this.id = id;
            this.type = type;
            this.category = category;
            this.name = name;
            this.description = description;
            this.readOnly = readOnly;
            this.visable = visable;
            this.order = order;
        }

        /// <summary>
        /// Базовый класс параметра отчёта
        /// </summary>
        /// <param name="id">Идентификатор параметра в БД</param>
        /// <param name="type">Тип параметра</param>
        /// <param name="category">Категория</param>
        /// <param name="name">Название параметра</param>
        /// <param name="description">Описание параметра</param>
        /// <param name="readOnly">Флаг "только чтение"</param>
        /// <param name="visable">Флаг видимости</param>
        /// <param name="visable">Значение по-умолчанию</param>
        /// <remarks>Данный объект может быть передан в PropertyGridEx для отображения и редактирования.</remarks>
        public ReportParamsBase(int id, ReportParamsType type, string category, string name, string description, bool readOnly, bool visable, int order, object value)
        {
            this.id = id;
            this.type = type;
            this.category = category;
            this.name = name;
            this.description = description;
            this.readOnly = readOnly;
            this.visable = visable;
            this.order = order;
            this.value = value;
        }

        public string Category
        {
            get { return category;  }
        }

        public int ID
        {
            get { return id; }
        }

        public ReportParamsType Type
        {
            get { return type; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Description
        {
            get { return description; }
        }

        public bool ReadOnly
        {
            get { return readOnly; }
        }

        public bool Visable
        {
            get { return visable; }
        }

        public int Order
        {
            get { return order; }
        }

        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public override string ToString()
        {
            return String.Format("{0}", name);
        }
    }
}
