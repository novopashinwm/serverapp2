﻿using System;
using System.Collections;
using System.Text;
using FORIS.TSS.Common;

namespace FORIS.TSS.Interfaces.ReportService
{
    [Serializable]
    public class ReportParamsFactory
    {
        public static string GetParamsString(PARAMS ps)
        {
            StringBuilder sb = new StringBuilder();
            foreach(DictionaryEntry p in ps)
                sb.AppendLine(p.ToString());

            return sb.ToString();
        }
    }
}
