using System;
using System.Runtime.Serialization;
using System.Text;
using System.Collections.Generic;

namespace FORIS.TSS.Interfaces.ReportService
{
	/// <summary>
	/// command params class
	/// </summary>
	[Serializable]
	public class ReportParamsDictionary : Dictionary<String, IReportParams>  
	{
        public ReportParamsDictionary() : base()
        {
             
        }

        public ReportParamsDictionary(SerializationInfo info, StreamingContext context) : base() 
        {

        }
        
	    public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			foreach(KeyValuePair<String, IReportParams> kv in this)
				sb.AppendFormat("{0}\t= {1}\n", kv.Key, kv.Value);
			return sb.ToString();
		}
	}
}