﻿using System;


namespace FORIS.TSS.Interfaces.ReportService
{
    [Serializable]
    public enum ReportStateEnum : int
    {
        /// <summary>
        /// Состояние не определено
        /// </summary>
        UnKnown,
        /// <summary>
        /// Генерируется в данный момент
        /// </summary>
        Generating,
        /// <summary>
        /// Сгенерирован
        /// </summary>
        Generated
    }
}
