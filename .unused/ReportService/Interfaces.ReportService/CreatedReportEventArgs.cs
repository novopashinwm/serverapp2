﻿using System;

namespace FORIS.TSS.Interfaces.ReportService
{
    /// <summary>
    /// Аргумент события генерации отчёта на стороне сервиса отчётов
    /// </summary>
    /// <remarks>Данный класс содержит избыточную информацию о сформированном отчёте, т.к. пока неясно
    /// как реализовать схему данных. Скорее всего, половину информации придётся выкинуть в будущем.
    /// 30.07.08 Стефанов А.А.</remarks>
    [Serializable]
    public class CreatedReportEventArgs : EventArgs
    {
        #region Data

        /// <summary>
        /// Идентификатор экземпляра отчёта 
        /// </summary>
        Guid thisGuid;

        /// <summary>
        /// Идентификатор отчёта в БД?
        /// </summary>
        int id;

        /// <summary>
        /// Название отчёта
        /// </summary>
        /// <remarks>Избыточная информация</remarks>
        string name;

        /// <summary>
        /// GUID типа отчёта 
        /// </summary>
        Guid reportGuid;

        /// <summary>
        /// Название объекта слежения (Mobile)
        /// </summary>
        string mo;

        /// <summary>
        /// Время формирования отчёта в UTC
        /// </summary>
        DateTime dateTime;

        /// <summary>
        /// Начало интервала данных отчёта
        /// </summary>
        DateTime dateTimeFrom;

        /// <summary>
        /// Окончание интервала данных отчёта
        /// </summary>
        DateTime dateTimeTo;

        /// <summary>
        /// Состояние завершения формирования отчёта
        /// </summary>
        ReportStateEnum state;

        /// <summary>
        /// Описание отчёта
        /// </summary>
        /// <remarks>Избыточная информация</remarks>
        string description;

        #endregion // Data

        #region Properties

        /// <summary>
        /// Идентификатор экземпляра отчёта 
        /// </summary>
        public Guid ThisGuid
        {
            get { return thisGuid; }
            set { thisGuid = value; }
        }

        /// <summary>
        /// Идентификатор отчёта в БД?
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Название отчёта
        /// </summary>
        /// <remarks>Избыточная информация</remarks>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// GUID типа отчёта 
        /// </summary>
        public Guid ReportGuid
        {
            get { return reportGuid; }
            set { reportGuid = value; }
        }

        /// <summary>
        /// Название объекта слежения (Mobile)
        /// </summary>
        public string Mo
        {
            get { return mo; }
            set { mo = value; }
        }

        /// <summary>
        /// Время формирования отчёта в UTC
        /// </summary>
        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }

        /// <summary>
        /// Начало интервала данных отчёта
        /// </summary>
        public DateTime DateTimeFrom
        {
            get { return dateTimeFrom; }
            set { dateTimeFrom = value; }
        }

        /// <summary>
        /// Окончание интервала данных отчёта
        /// </summary>
        public DateTime DateTimeTo
        {
            get { return dateTimeTo; }
            set { dateTimeTo = value; }
        }

        /// <summary>
        /// Состояние завершения формирования отчёта
        /// </summary>
        public ReportStateEnum State
        {
            get { return state; }
            set { state = value; }
        }

        /// <summary>
        /// Описание отчёта
        /// </summary>
        /// <remarks>Избыточная информация</remarks>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion // Properties

        /// <summary>
        /// Аргумент события генерации отчёта на стороне сервиса отчётов
        /// </summary>
        public CreatedReportEventArgs(Guid thisGuid, int id, string name, Guid reportGuid, string mo, DateTime dateTime, DateTime dateTimeFrom, DateTime dateTimeTo, ReportStateEnum state, string description)
        {
            this.thisGuid = thisGuid;
            this.id = id;
            this.name = name;
            this.reportGuid = reportGuid;
            this.mo = mo;
            this.dateTime = dateTime;
            this.dateTimeFrom = dateTimeFrom;
            this.dateTimeTo = dateTimeTo;
            this.state = state;
            this.description = description;
        }
    }

}
