﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing.Design;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.Interfaces.ReportService.Params;
using FORIS.TSS.Report.Base;
using System.Runtime.InteropServices;
using FORIS.TSS.TransportDispatcher;


namespace ReportGeoZoneOver
{
	/// <summary>
	/// Отчёт о прохождениии через зоны 
	/// </summary>
    [Guid("6E2DE7AD-2656-4812-BC1C-0EEA32613662")] 
    public class GeoZoneOverTspReport : TssReportBase
    {

		/// <summary>
		/// </summary>
        public GeoZoneOverTspReport()
		{
		}

        /// <summary>
		/// Текстовое описание данного отчета.
		/// </summary>
		public override string ReportNameGet()
		{
            return Strings.ReportZoneOver; // "Отчёт о прохождениии через зон";
        }

        /// <summary>
        /// Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета.
        /// </summary>
        public override ReportParameters ReportParametersInstanceGet()
        {
            ReportParametersThis zoneOverReportParameters = new ReportParametersThis();

            return zoneOverReportParameters;
        }

        /// <summary>
        /// Создает отчет, используя переданные параметры
        /// </summary>
        public override ReportClass Create(ReportParameters iReportParameters)
        {
            GeoZoneOverDataSet dsReport = null;

            try
            {
                dsReport = new GeoZoneOverDataSet();

                if (iReportParameters != null && iReportParameters.ShowBlankReportProp == false)
                {
                    ReportParametersThis reportParameters = (ReportParametersThis)iReportParameters;

                    DateTime dtFrom = DateTimeMerge(reportParameters.DateFrom, reportParameters.TimeFrom);
                    DateTime dtTo = DateTimeMerge(reportParameters.DateTo, reportParameters.TimeTo);

                    // Заполнение заголовка отчета (Формирование строки в таблице ReportHeader)
                    GeoZoneOverDataSet.HeaderRow rowHeader = dsReport.Header.NewHeaderRow();
                    rowHeader.mo = reportParameters.Mo;
                    rowHeader.From = dtFrom.ToShortDateString() + " " + dtFrom.ToShortTimeString();
                    rowHeader.To = dtTo.ToShortDateString() + " " + dtTo.ToShortTimeString();
                    dsReport.Header.AddHeaderRow(rowHeader);

                    // Получение данных для отчета из БД.
                    DataSet ds = this.iDataSupplier.GetLogFromDB(
                        dtFrom.ToUniversalTime(),
                        dtTo.ToUniversalTime(),
                        reportParameters.ControllerID,
                        reportParameters.Interval,
                        reportParameters.PositionsСount
                        );

                    ////Установка текста прогрессбара на текущем этапе.
                    //argsProgressBarArgs.message = "Обработка данных...";
                    ////Установка максимального значения шкалы прогрессбара.
                    //argsProgressBarArgs.count = ds.Tables["MoveHistory"].Rows.Count + 1;
                    ////Обновление прогрессбара.
                    //OnProgress(argsProgressBarArgs);



                    if (ds.Tables["MoveHistory"].Rows.Count > 0)
                    {
                        #region Address

                        //ds.Tables["History"].TableName = "MoveHistory";
                        //ds.Tables["MoveHistory"].Columns["LONGITUDE"].ColumnName = "X";
                        //ds.Tables["MoveHistory"].Columns["LATITUDE"].ColumnName = "Y";
                        //ds.Tables["MoveHistory"].Columns.Add(new DataColumn("Address", Type.GetType("System.String")));

                        //Guid guid = Guid.Empty;
                        //try
                        //{
                        //    if (reportParameters.ShowAddress == true 
                        //        && reportParameters.MapGuid != null && reportParameters.MapGuid != String.Empty)
                        //    {
                        //        guid = new Guid(reportParameters.MapGuid);
                        //        if (guid != Guid.Empty)
                        //            ;// ds = this.iDataSupplier.GetMoveHistoryAddresses(guid, (HistoryDataSet)ds);
                        //    }
                        //}
                        //catch (Exception exc)
                        //{
                        //    Trace.WriteLine("Invalid GUID for report! " + exc.ToString());
                        //}

                        #endregion Address

                        // Получение данных для отчета из БД.
                        DataSet dsZone = this.iDataSupplier.GetDataFromDB(
                            this.ParamsPreparingForStorProc(),
                            "dbo.GetZone",
                            new string[] { "Zone", "ZonePoint" }
                        );


                        // Проверка прохождения
                        ds.Tables["MoveHistory"].Columns.Add(new DataColumn("zone", Type.GetType("System.String")));
                        ds.Tables["MoveHistory"].Columns.Add(new DataColumn("group", Type.GetType("System.Int32")));
                        string zone = "";
                        int group = 0;
                        for (int i = 0; i < ds.Tables["MoveHistory"].Rows.Count; i++)
                        {
                            DataRow drHistory = ds.Tables["MoveHistory"].Rows[i];

                            if (drHistory["VALID"] != DBNull.Value && (bool)drHistory["VALID"] == true)
                            {
                                
                                string zoneTrue = "";

                                foreach(DataRow drZone in dsZone.Tables["Zone"].Rows)
                                {
					                DataRow[] drsZonePoints = dsZone.Tables["ZonePoint"].Select("ZONE_ID = " + drZone["ZONE_ID"]);

                                    if (drsZonePoints != null && GeoZoneCheckerContain.Contains(drsZonePoints, (int)drZone["ZONE_TYPE_ID"], (float)drHistory["X"], (float)drHistory["Y"]) == true)
                                    {
                                        GeoZoneOverDataSet.DataRow rowData = dsReport.Data.NewDataRow();

                                        if (drHistory["LOG_TIME"] != DBNull.Value)
                                        {
                                            DateTime dt = (DateTime)drHistory["LOG_TIME"];
                                            rowData.DateTime = dt.ToLocalTime().ToString();
                                        }

                                        rowData.Zone = drZone["NAME"].ToString();
                                
                                        dsReport.Data.AddDataRow(rowData);

                                        break;

                                        zoneTrue = drZone["NAME"].ToString();
                                        break;

                                    }
                                }

                                if (zone != zoneTrue)
                                {
                                    zone = zoneTrue;
                                    group++;
                                }

                                ds.Tables["MoveHistory"].Rows[i]["zone"] = zone;
                                ds.Tables["MoveHistory"].Rows[i]["group"] = group;

                                ////Продвижение текущего положения прогрессбара.
                                //argsProgressBarArgs.value++;
                                //OnProgress(argsProgressBarArgs);

                            }
                        }

                        for (int i = 0; i <= group; i++)
                        {
                            DataRow[] drLogGroup = ds.Tables["MoveHistory"].Select("group = " + i.ToString());

                            if (drLogGroup == null || drLogGroup.Length < 1)
                                continue;

                            GeoZoneOverDataSet.DataRow rowData = dsReport.Data.NewDataRow();

                            string dt = ((DateTime)drLogGroup[0]["LOG_DATETIME"]).ToLocalTime().ToString();
                            if (drLogGroup.Length > 1)
                            {
                                dt += " - " + ((DateTime)drLogGroup[drLogGroup.Length - 1]["LOG_DATETIME"]).ToLocalTime().ToString();
                            }
                            rowData.DateTime = dt;

                            string zoneCurrent = drLogGroup[0]["zone"].ToString().Trim();
                            if (zoneCurrent == "")
                                zoneCurrent = " - ";
                            rowData.Zone = zoneCurrent;

                            dsReport.Data.AddDataRow(rowData);
                        }


                    }
                }

                //Вызов функции заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
                ConstantsListDataset dsEstablishmentAttributes = TssReportBase.ReportEstablishmentAttributesFill(this.iDataSupplier);
                //Добавление в датасет данных атрибутов предприятия.
                dsReport.Merge(dsEstablishmentAttributes);

                //Создание экземпляра отчета.
                ReportClass rptResultReport = new GeoZoneOverReport();
                //Подключение отчета к данным, собранным в DataSet'е.
                rptResultReport.SetDataSource(dsReport);
                //Вертикальное положение бланка формы отчета.
                rptResultReport.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                //Функция возвращает сформированный экземпляр отчета.
                return rptResultReport;
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return null;
            }
            finally
            {
                if (dsReport != null)
                {
                    dsReport.Dispose();
                }
            }
        }

	    protected override void SetParamsDictionary()
	    {
            reportParamsDictionary.Add("Interval", new ReportParamsCommon(
                0,
                ReportParamsType.Int32,
                ReportParamsCategory.ReportParams,
                "Интервал, с",
                "Минимальный интервал между позициями истории движения.",
                false,
                true,
                0));

            reportParamsDictionary.Add("PositionsСount", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Количество позиций",
                "Максимальное количество позиций истории движения, которое будет запрошено из базы данных.",
                false,
                true,
                1));

            reportParamsDictionary.Add("DateFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Дата начала периода",
                "Дата начала периода, за который формируется отчёт.",
                false,
                true,
                2));

            reportParamsDictionary.Add("TimeFrom", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "Время начала периода",
                "Время начала периода, за который формируется отчёт.",
                false,
                true,
                3));

            reportParamsDictionary.Add("DateTo", new ReportParamsCommon(
                0,
                ReportParamsType.Date,
                ReportParamsCategory.ReportParams,
                "Дата окончания периода",
                "Дата окончания периода, за который формируется отчёт.",
                false,
                true,
                4));

            reportParamsDictionary.Add("TimeTo", new ReportParamsCommon(
                0,
                ReportParamsType.Time_HHmm_String,
                ReportParamsCategory.ReportParams,
                "Время окончания периода",
                "Время окончания периода, за который формируется отчёт.",
                false,
                true,
                5));

            reportParamsDictionary.Add("Mo", new ReportParamsCommon(
                0,
                ReportParamsType.String,
                ReportParamsCategory.ReportParams,
                "Объект слежения",
                "Название (номер) объекта слежения.",
                false,
                true,
                6));

            reportParamsDictionary.Add("ShowAddress", new ReportParamsCommon(
                0,
                ReportParamsType.Boolean,
                ReportParamsCategory.ReportParams,
                "Показывать адрес",
                "Показывать/не показывать адрес в отчёте.",
                false,
                true,
                7));
        }

	    //protected override void SetParamsDictionary()
        //{
        //}

	    #region Подготовка списка параметров для передачи в функцию вызова хранимой процедуры
        private ParamValue[] ParamsPreparingForStorProc()
        {
            Trace.WriteLine("ParamsPreparingForStorProc()");

            ParamValue[] paramValues = new ParamValue[2];
            paramValues[0] = new ParamValue("@vertexGet", 1);
            paramValues[1] = new ParamValue("@zone_id", 0);

            Trace.WriteLine(paramValues[0].value);
            Trace.WriteLine(paramValues[1].value);

            return paramValues;
        }
        #endregion Подготовка списка параметров для передачи в функцию вызова хранимой процедуры


    }


	/// <summary>
	/// Параметры для данного отчета.
	/// </summary>
	[Serializable]
    public class ReportParametersThis : ReportParameters
    {


        /// <summary>
        /// Время начала периода, на который формируется отчет.
        /// </summary>
        protected string timeFrom = DateTime.Now.ToShortTimeString();
        /// <summary>
        /// Время окончания периода, на который формируется отчет.
        /// </summary>
        protected string timeTo = DateTime.Now.ToShortTimeString();
        /// <summary>
        /// Объект слежения
        /// </summary>
        protected string mo = "";
        /// <summary>
        /// Объект слежения
        /// </summary>
        protected int controllerID = 0;

        protected int interval = 60;
        protected int count = 1000;

        /// <summary>
        /// Показывать/не показывать адрес в отчёте
        /// </summary>
        protected bool showAddress = false;

        /// <summary>
        /// GUID карты
        /// </summary>
        protected string mapGuid = Guid.Empty.ToString();


        /// <summary>
        /// Гаражный номер ТС.
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("TSNumberDN", 8, true), Category("ReportParsCat"), Description("TSNumberDesc")]
        [Editor(typeof(FORIS.TSS.Report.Base.Editors.ListBoxEditor), typeof(UITypeEditor))]
        //[Editor(typeof(FORIS.TSS.TransportDispatcher.ComboBoxEditor), typeof(UITypeEditor))]
        //[TypeConverter(typeof(ConvInToClean))]
        //public FORIS.TSS.TransportDispatcher.TagComboBoxItem VehicleProp
        public string Mo
        {
            get
            {
                return mo;
            }
            set
            {
                mo = value;
            }
        }

        /// <summary>
        /// ControllerID
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("ControllerDN", 2, true), Category("ReportParsCat"), Description("ControllerDesc")]
        public int ControllerID
        {
            get
            {
                return controllerID;
            }
            set
            {
                controllerID = value;
            }
        }

        /// <summary>
        /// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginDateDN", 4, true), Category("ReportParsCat"), Description("PeriodBeginDateDesc")]
        [Browsable(true)]
        public override DateTime DateFrom
        {
            get
            {
                return dtDateFrom;
            }
            set
            {
                dtDateFrom = value;
            }
        }

        /// <summary>
        /// Время начала периода, на который формируется доклад.
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("PeriodBeginTimeDN", 5, true), Category("ReportParsCat"), Description("PeriodBeginTimeDesc")]
        [PropertyFormat(typeof(FormaterTimeExt))]
        public string TimeFrom
        {
            get
            {
                return timeFrom;
            }
            set
            {
                //Вызов функции проверки введенного пользователем значения времени.
                timeFrom = TssReportBase.TimeInputCheck(value);
            }
        }

        /// <summary>
        /// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndDateDN", 6, true), Category("ReportParsCat"), Description("PeriodEndDateDesc")]
        [Browsable(true)]
        public override DateTime DateTo
        {
            get
            {
                return dtDateTo;
            }
            set
            {
                dtDateTo = value;
            }
        }

        /// <summary>
        /// Время окончания периода, на который формируется доклад.
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("PeriodEndTimeDN", 7, true), Category("ReportParsCat"), Description("PeriodEndTimeDesc")]
        [PropertyFormat(typeof(FormaterTimeExt))]
        public string TimeTo
        {
            get
            {
                return timeTo;
            }
            set
            {
                //Вызов функции проверки введенного пользователем значения времени.
                timeTo = TssReportBase.TimeInputCheck(value);
            }
        }

        /// <summary>
        /// Интервал
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("IntervalDN", 2, true), Category("ReportParsCat"), Description("IntervalDesc")]
        public int Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
            }
        }

        /// <summary>
        /// Количество позиций
        /// </summary>
        [FORIS.TSS.TransportDispatcher.DisplayName("HowManyPosDN", 1, true), Category("ReportParsCat"), Description("HowManyPosDesc")]
        public int PositionsСount
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
            }
        }

        /// <summary>
        /// Показывать/не показывать адрес в отчёте
        /// </summary>
        [Browsable(false)]
        public bool ShowAddress
        {
            get
            {
                return showAddress;
            }
            set
            {
                showAddress = value;
            }
        }

        /// <summary>
        /// GUID карты
        /// </summary>
        [Browsable(false)]
        public string MapGuid
        {
            get { return mapGuid; }
            set { mapGuid = value; }
        }
    }

}
