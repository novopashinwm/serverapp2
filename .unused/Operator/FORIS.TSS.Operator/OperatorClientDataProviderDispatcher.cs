using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Operator;

namespace FORIS.TSS.Operator
{
    public class OperatorClientDataProviderDispatcher:
		ClientDispatcher<IOperatorClientDataProvider>
	{
		public OperatorClientDataProviderDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public OperatorClientDataProviderDispatcher()
		{

		}

		private readonly OperatorClientDataProviderAmbassadorCollection ambassadors =
			new OperatorClientDataProviderAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new OperatorClientDataProviderAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem<IOperatorClientDataProvider>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

    public class OperatorClientDataProviderAmbassadorCollection:
		ClientAmbassadorCollection<IOperatorClientDataProvider>
	{
		public new OperatorClientDataProviderAmbassador this[int index]
		{
			get { return (OperatorClientDataProviderAmbassador)base[index]; }
		}
	}

    public class OperatorClientDataProviderAmbassador:
		ClientAmbassador<IOperatorClientDataProvider>
	{

	}
}