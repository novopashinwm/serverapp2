using System.Windows.Forms;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator;
using FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup;
using FORIS.TSS.WorkplaceShadow.Operator.Data.Operator;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Operator;
using System.Threading;

namespace FORIS.TSS.Operator
{
	public class RB_Operator: 
		PluginForm,
		IClientItem<IOperatorClientDataProvider>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private OperatorClientDataProviderDispatcher clientDispatcher;
		private OperatorDataDispatcher dataDispatcher;
		private System.Windows.Forms.ToolStripContainer tscMain;
		private OperatorDataSupplier operatorDataSupplier;
		private OperatorData operatorData;
		private OperatorDataAmbassador daOperatorGroupTableControl;
		private OperatorDataAmbassador daOperatorTableControl;
		private OperatorEditorProvider<OperatorPropertiesForm> operatorEditorProvider;
		private OperatorDataAmbassador daOperatorGroupEditorProvider;
		private OperatorDataAmbassador daOperatorEditorProvider;
		private OperatorGroupTableControl ctrlOperatorGroupTable;
		private OperatorTableControl ctrlOperatorTable;
		private SplitContainer splitContainer;
		private OperatorClientDataProviderAmbassador caOperatorDataSupplier;
		private OperatorClientDataProviderAmbassador caOperatorEditorProvider;
		private FORIS.TSS.Common.Commands.CommandManager commandManager;
		private FORIS.TSS.Common.Commands.Command cmdFilter;
		private ToolBarButton tbsFilter;
		private ToolBarButton tbbFilter;
		private ToolStripSeparator tssFilter;
		private ToolStripMenuItem tsmiFilter;
		private FORIS.TSS.Common.Commands.CommandInstance ciFilterToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciFilterContextMenuItem;
		private OperatorGroupEditorProvider operatorGroupEditorProvider;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public RB_Operator()
		{
			InitializeComponent();
		}

		#region IClientItem<IOperatorClientDataProvider> Members

		public IOperatorClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set { this.clientDispatcher.Client = value; }
		}

		#endregion // IClientItem<IOperatorClientDataProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IOperatorClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( RB_Operator ) );
			this.tscMain = new System.Windows.Forms.ToolStripContainer();
			this.splitContainer = new System.Windows.Forms.SplitContainer();
			this.ctrlOperatorGroupTable = new FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupTableControl();
			this.tbsFilter = new System.Windows.Forms.ToolBarButton();
			this.tbbFilter = new System.Windows.Forms.ToolBarButton();
			this.tssFilter = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiFilter = new System.Windows.Forms.ToolStripMenuItem();
			this.operatorGroupEditorProvider = new FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupEditorProvider( this.components );
			this.ctrlOperatorTable = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorTableControl();
			this.operatorEditorProvider = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorEditorProvider<OperatorPropertiesForm>( this.components );
			this.clientDispatcher = new FORIS.TSS.Operator.OperatorClientDataProviderDispatcher( this.components );
			this.caOperatorDataSupplier = new FORIS.TSS.Operator.OperatorClientDataProviderAmbassador();
			this.operatorDataSupplier = new FORIS.TSS.WorkplaceShadow.Operator.Data.Operator.OperatorDataSupplier( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher( this.components );
			this.daOperatorGroupEditorProvider = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.daOperatorGroupTableControl = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.daOperatorEditorProvider = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.daOperatorTableControl = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.operatorData = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorData( this.components );
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.cmdFilter = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciFilterToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.ciFilterContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.caOperatorEditorProvider = new FORIS.TSS.Operator.OperatorClientDataProviderAmbassador();
			this.tscMain.ContentPanel.SuspendLayout();
			this.tscMain.SuspendLayout();
			this.splitContainer.Panel1.SuspendLayout();
			this.splitContainer.Panel2.SuspendLayout();
			this.splitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// tscMain
			// 
			this.tscMain.BottomToolStripPanelVisible = false;
			// 
			// tscMain.ContentPanel
			// 
			this.tscMain.ContentPanel.Controls.Add( this.splitContainer );
			resources.ApplyResources( this.tscMain.ContentPanel, "tscMain.ContentPanel" );
			resources.ApplyResources( this.tscMain, "tscMain" );
			this.tscMain.LeftToolStripPanelVisible = false;
			this.tscMain.Name = "tscMain";
			this.tscMain.RightToolStripPanelVisible = false;
			this.tscMain.TopToolStripPanelVisible = false;
			// 
			// splitContainer
			// 
			resources.ApplyResources( this.splitContainer, "splitContainer" );
			this.splitContainer.Name = "splitContainer";
			// 
			// splitContainer.Panel1
			// 
			this.splitContainer.Panel1.Controls.Add( this.ctrlOperatorGroupTable );
			// 
			// splitContainer.Panel2
			// 
			this.splitContainer.Panel2.Controls.Add( this.ctrlOperatorTable );
			// 
			// ctrlOperatorGroupTable
			// 
			this.ctrlOperatorGroupTable.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbsFilter,
            this.tbbFilter} );
			// 
			// 
			// 
			this.ctrlOperatorGroupTable.ContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.tssFilter,
            this.tsmiFilter} );
			this.ctrlOperatorGroupTable.ContextMenuStrip.Name = "cmsTable";
			this.ctrlOperatorGroupTable.ContextMenuStrip.Size = ( (System.Drawing.Size)( resources.GetObject( "resource.Size" ) ) );
			resources.ApplyResources( this.ctrlOperatorGroupTable, "ctrlOperatorGroupTable" );
			this.ctrlOperatorGroupTable.EditorProvider = this.operatorGroupEditorProvider;
			this.ctrlOperatorGroupTable.Name = "ctrlOperatorGroupTable";
			this.ctrlOperatorGroupTable.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler( this.ctrlOperatorGroupTable_SelectionChanged );
			// 
			// tbsFilter
			// 
			this.tbsFilter.Name = "tbsFilter";
			this.tbsFilter.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbbFilter
			// 
			resources.ApplyResources( this.tbbFilter, "tbbFilter" );
			this.tbbFilter.Name = "tbbFilter";
			this.tbbFilter.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
			// 
			// tssFilter
			// 
			this.tssFilter.Name = "tssFilter";
			resources.ApplyResources( this.tssFilter, "tssFilter" );
			// 
			// tsmiFilter
			// 
			this.tsmiFilter.CheckOnClick = true;
			this.tsmiFilter.Name = "tsmiFilter";
			resources.ApplyResources( this.tsmiFilter, "tsmiFilter" );
			// 
			// ctrlOperatorTable
			// 
			resources.ApplyResources( this.ctrlOperatorTable, "ctrlOperatorTable" );
			this.ctrlOperatorTable.EditorProvider = this.operatorEditorProvider;
			this.ctrlOperatorTable.Name = "ctrlOperatorTable";
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.Ambassadors.Add( this.caOperatorDataSupplier );
			this.clientDispatcher.Ambassadors.Add( this.caOperatorEditorProvider );
			// 
			// caOperatorDataSupplier
			// 
			this.caOperatorDataSupplier.Item = this.operatorDataSupplier;
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daOperatorGroupEditorProvider );
			this.dataDispatcher.Ambassadors.Add( this.daOperatorGroupTableControl );
			this.dataDispatcher.Ambassadors.Add( this.daOperatorEditorProvider );
			this.dataDispatcher.Ambassadors.Add( this.daOperatorTableControl );
			// 
			// daOperatorGroupEditorProvider
			// 
			this.daOperatorGroupEditorProvider.Item = this.operatorGroupEditorProvider;
			// 
			// daOperatorGroupTableControl
			// 
			this.daOperatorGroupTableControl.Item = this.ctrlOperatorGroupTable;
			// 
			// daOperatorEditorProvider
			// 
			this.daOperatorEditorProvider.Item = this.operatorEditorProvider;
			// 
			// daOperatorTableControl
			// 
			this.daOperatorTableControl.Item = this.ctrlOperatorTable;
			// 
			// operatorData
			// 
			this.operatorData.DataSupplier = this.operatorDataSupplier;
			this.operatorData.InvokeBroker = this.invokeBroker;
			this.operatorData.Loaded += new System.EventHandler( this.operatorData_Loaded );
			this.operatorData.Loading += new System.EventHandler( this.operatorData_Loading );
			// 
			// cmdFilter
			// 
			this.cmdFilter.Instances.Add( this.ciFilterToolbarButton );
			this.cmdFilter.Instances.Add( this.ciFilterContextMenuItem );
			this.cmdFilter.Manager = this.commandManager;
			this.cmdFilter.Execute += new System.EventHandler( this.cmdFilter_Execute );
			// 
			// ciFilterToolbarButton
			// 
			this.ciFilterToolbarButton.Item = this.tbbFilter;
			// 
			// ciFilterContextMenuItem
			// 
			this.ciFilterContextMenuItem.Item = this.tsmiFilter;
			// 
			// caOperatorEditorProvider
			// 
			this.caOperatorEditorProvider.Item = this.operatorEditorProvider;
			// 
			// RB_Operator
			// 
			resources.ApplyResources( this, "$this" );
			this.Controls.Add( this.tscMain );
			this.Name = "RB_Operator";
			this.tscMain.ContentPanel.ResumeLayout( false );
			this.tscMain.ResumeLayout( false );
			this.tscMain.PerformLayout();
			this.splitContainer.Panel1.ResumeLayout( false );
			this.splitContainer.Panel2.ResumeLayout( false );
			this.splitContainer.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion // Windows Forms Designer generated code

		#region Actions



		#endregion // Actions

		#region View

		private void OnUpdateView()
		{
			ThreadStart updateHandler =
				delegate
				{
					this.ctrlOperatorTable.BeginUpdate();

					foreach ( OperatorXpRow xpRow in this.ctrlOperatorTable.Rows )
					{
						xpRow.Visible = this.calcVisibility( xpRow.Row );
						xpRow.Checked = this.calcChecked( xpRow.Row );
					}

					this.ctrlOperatorTable.EndUpdate();
				};

			if ( this.InvokeRequired )
			{
				this.BeginInvoke( updateHandler );
			}
			else
			{
				updateHandler();
			}
		}

		private bool calcVisibility( OperatorRow operatorRow )
		{
			if ( this.tbbFilter.Pushed && this.ctrlOperatorGroupTable.SelectedRows.Length > 0 )
			{
				return operatorRow.InGroup( this.ctrlOperatorGroupTable.SelectedRows[0].Row );
			}

			return true;
		}

		private bool calcChecked( OperatorRow operatorRow )
		{
			if ( this.ctrlOperatorGroupTable.SelectedRows.Length > 0 )
			{
				return operatorRow.InGroup( this.ctrlOperatorGroupTable.SelectedRows[0].Row );
			}

			return false;
		}

		#endregion // View

		#region Handle operatorData events

		private void operatorData_Loading( object sender, System.EventArgs e )
		{
			this.dataDispatcher.Data = null;
		}		
		
		private void operatorData_Loaded( object sender, System.EventArgs e )
		{
			this.dataDispatcher.Data = this.operatorData;
		}

		void ctrlOperatorGroupTable_SelectionChanged( object sender, FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle operatorData events

		#region Handle controls' events

		//private void cbxDepartment_SelectedIndexChanged( object sender, System.EventArgs e )
		//{
		//    int? SelectedDepartment =
		//        this.cbxDepartment.SelectedItem == this.siDepartmentNo
		//            ?
		//                (int?)null
		//            :
		//                this.cbxDepartment.SelectedId;

		//    this.dataDispatcher.Data = null;

		//    this.operatorDataSupplier.Params =
		//        new DepartmentDataParams( SelectedDepartment );

		//    this.dataDispatcher.Data = this.operatorData;

		//    this.operatorEditorProvider.Department = SelectedDepartment;

		//    if( SelectedDepartment != null )
		//        this.cbxDepartment.SelectedId = (int)SelectedDepartment;
		//    else
		//        this.cbxDepartment.SelectedItem = this.siDepartmentNo;
		//}

		private void cmdFilter_Execute( object sender, System.EventArgs e )
		{
			this.cmdFilter.Checked = !this.cmdFilter.Checked;
			this.OnUpdateView();
		}

		#endregion // Handle controls' events
	}
}
