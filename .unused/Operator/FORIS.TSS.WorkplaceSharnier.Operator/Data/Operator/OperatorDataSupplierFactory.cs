using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator;

namespace FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator
{
	public class OperatorDataSupplierFactory:
		ClientDataSupplierFactory<IOperatorClientDataProvider, OperatorClientData, DataParams>
	{
		public OperatorDataSupplierFactory( IOperatorClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override OperatorClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			OperatorClientData Supplier =new OperatorClientData( this.Client );

			return Supplier;
		}
	}
}