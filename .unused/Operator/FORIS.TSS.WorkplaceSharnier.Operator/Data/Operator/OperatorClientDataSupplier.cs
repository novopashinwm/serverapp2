using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Operator;

namespace FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator
{
	internal class OperatorClientDataSupplier:
		ClientDataSupplier<IOperatorClientDataProvider, IOperatorDataSupplier, IOperatorDataTreater>,
		IOperatorDataSupplier
	{
		public OperatorClientDataSupplier( IOperatorClientDataProvider clientProvider )
			: base( clientProvider, clientProvider.GetOperatorDataSupplier() )
		{

		}
	}
}