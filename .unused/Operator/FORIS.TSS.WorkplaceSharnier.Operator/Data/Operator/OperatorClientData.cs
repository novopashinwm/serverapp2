using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator
{
	/// <summary>
	/// ��������� ������ ������� �������
	/// ������ "���������"
	/// </summary>
	public class OperatorClientData:
		OperatorData
	{
		#region Constructor & Dispose

		public OperatorClientData( IOperatorClientDataProvider clientProvider )
		{
			this.dataSupplier = new OperatorClientDataSupplier( clientProvider );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// ������������ ��� ��������� ������ ������� DataTreater
		/// </summary>
		private readonly IOperatorDataSupplier dataSupplier;

		public IDataSupplier SecurityDataSupplier
		{
			get { throw new NotSupportedException(); }
		}
	}
}