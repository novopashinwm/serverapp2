using FORIS.TSS.Interfaces.Operator.Data;
using FORIS.TSS.WorkplaceSharnier.Operator.Data.Operator;

namespace FORIS.TSS.WorkplaceSharnier.Operator
{
	public interface IOperatorClientDataProvider:
		IClientDataProvider,
		IOperatorDataSupplierProvider
	{
		OperatorClientData GetOperatorClientData();
	}
}
