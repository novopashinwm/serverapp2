﻿using FORIS.TSS.ServerApplication.Operator.Data.Operator;

namespace FORIS.TSS.ServerApplication.Operator
{
	public interface IOperatorServerDataProvider :
		IDatabaseProvider
	{
		OperatorServerData OperatorData { get; }
	}
}