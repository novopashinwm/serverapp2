using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	public class OperatorDatabaseDataSupplier:
		DatabaseDataSupplier<IOperatorServerDataProvider>,
		IOperatorDataSupplier
	{
		public OperatorDatabaseDataSupplier( IOperatorServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{

		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerOperator",
					new string[] { "OPERATORGROUP", "OPERATOR", "OPERATORGROUP_OPERATOR" }
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IOperatorDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}