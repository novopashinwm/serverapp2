﻿using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	public class OperatorPersonalDataSupplier :
		PersonalDataSupplier<OperatorServerData, IOperatorDataTreater>,
		IOperatorDataSupplier
	{
		public OperatorPersonalDataSupplier(OperatorServerData dataSupplier, IPersonalServer personalServer)
			: base(dataSupplier, personalServer)
		{
		}
		public override IOperatorDataTreater GetDataTreater()
		{
			return DataSupplier.GetTreater(PersonalServer.SessionInfo);
		}
	}
}