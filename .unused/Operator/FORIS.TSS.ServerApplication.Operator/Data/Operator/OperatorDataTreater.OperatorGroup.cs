﻿using System;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	partial class OperatorDataTreater
	{
		public void OperatorGroupInsert(
			string name,
			string description
			)
		{
			lock (this.Data)
			{
				try
				{
					OperatorGroupRow operatorGroupRow = this.Data.OperatorGroup.NewRow();

					operatorGroupRow.Name = name;
					operatorGroupRow.Description = description;

					this.Data.OperatorGroup.Rows.Add(operatorGroupRow);

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OperatorGroupUpdate(
			int operatorGroupId,
			string name,
			string description
			)
		{
			lock (this.Data)
			{
				try
				{
					OperatorGroupRow operatorGroupRow = this.Data.OperatorGroup.FindRow(operatorGroupId);

					operatorGroupRow.BeginEdit();

					operatorGroupRow.Name = name;
					operatorGroupRow.Description = description;

					operatorGroupRow.EndEdit();

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OperatorGroupDelete(
			int operatorGroupId
			)
		{
			lock (this.Data)
			{
				try
				{
					OperatorGroupRow operatorGroupRow = this.Data.OperatorGroup.FindRow(operatorGroupId);

					operatorGroupRow.Delete();

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}