﻿using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	/// <summary> Поставщик данных для модуля "Права" </summary>
	/// <remarks>
	/// Всегда можно изменить способ получения,
	/// при необходимости - кэширования данных,
	/// выполнения операций и т.д. и т.п.
	/// 
	/// Именно этот самый объект осуществляет
	/// соединение данных непосредственно с БД
	/// </remarks>
	public class OperatorServerData : OperatorData
	{
		#region Constructor & Dispose

		/// <summary> Конструктор загружает данные и формирует обертку </summary>
		public OperatorServerData(IOperatorServerDataProvider serverDataProvider)
		{
			this.serverDataProvider = serverDataProvider;
			this.DataSupplier       = new OperatorDatabaseDataSupplier(this.serverDataProvider);
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				RemotingServices.Disconnect(this);

				this.Destroy();
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		/// <summary> Передается в объекты DataTreater для обновления данных в БД </summary>
		private readonly IOperatorServerDataProvider serverDataProvider;
		/// <summary> Создает новый объект DataTreater </summary>
		/// <param name="sessionInfo"></param>
		/// <returns>объект OperatorDataTreater</returns>
		public IOperatorDataTreater GetTreater(ISessionInfo sessionInfo)
		{
			return new OperatorDataTreater(
				this.serverDataProvider.Database,
				this,
				sessionInfo);
		}
	}
}