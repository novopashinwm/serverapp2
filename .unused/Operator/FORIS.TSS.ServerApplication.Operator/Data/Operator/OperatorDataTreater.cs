using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	public partial class OperatorDataTreater:
		DataTreater<IOperatorData>,
		IOperatorDataTreater
	{
		public OperatorDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IOperatorData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{

		}
	}
}