using System;
using System.Collections.Generic;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.ServerApplication.Operator.Data.Operator
{
	partial class OperatorDataTreater
	{
		public void OperatorInsert(
			string name, 
			string login, 
			string phone, 
			string email, 
			IList<int> operatorGroups
			)
		{
			lock( this.Data )
			{
				OperatorRow operatorRow;
				try
				{
					operatorRow = this.Data.Operator.NewRow();

					operatorRow.Name = name;
					operatorRow.Login = login;
					operatorRow.Phone = phone;
					operatorRow.Email = email;
                    operatorRow.Guid = Guid.NewGuid();

					this.Data.Operator.Rows.Add( operatorRow );

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}

				try
				{
					foreach( int operatorGroupId in operatorGroups )
					{
						OperatorGroupRow RowVehicleGroup = this.Data.OperatorGroup.FindRow( operatorGroupId );

						operatorRow.ToGroup( RowVehicleGroup );
					}

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OperatorUpdate(
			int operatorId, 
			string name, 
			string login, 
			string phone, 
			string email, 
			IList<int> operatorGroups
			)
		{
			lock( this.Data )
			{
				try
				{
					OperatorRow operatorRow = this.Data.Operator.FindRow( operatorId );

					operatorRow.BeginEdit();

					operatorRow.Name = name;
					operatorRow.Login = login;
					operatorRow.Phone = phone;
					operatorRow.Email = email;

					operatorRow.EndEdit();

					#region operatorGroups

					foreach( OperatorGroupRow operatorGroupRow in operatorRow.RowsOperatorGroup.Clone() )
					{
						if( !operatorGroups.Contains( operatorGroupRow.ID ) )
						{
							operatorRow.FromGroup( operatorGroupRow );
						}
					}

					foreach( int operatorGroupId in operatorGroups )
					{
						OperatorGroupRow RowVehicleGroup = this.Data.OperatorGroup.FindRow( operatorGroupId );

						if( !operatorRow.InGroup( RowVehicleGroup ) )
						{
							operatorRow.ToGroup( RowVehicleGroup );
						}
					}

					#endregion // operatorGroups

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OperatorDelete( int rightId )
		{
			lock( this.Data )
			{
				try
				{
					OperatorRow rightRow = this.Data.Operator.FindRow( rightId );

					rightRow.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}