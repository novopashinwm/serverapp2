﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Operator.Data.Operator
{
	public interface IOperatorDataTreater:
		IDataTreater
	{
		#region Operator

		void OperatorInsert(
			string name,
			string login,
			string phone,
			string email,
			IList<int> operatorGroups
			);

		void OperatorUpdate(
			int operatorId,
			string name,
			string login,
			string phone,
			string email,
			IList<int> operatorGroups
			);

		void OperatorDelete(
			int operatorId
			);

		#endregion // Operator

		#region OperatorGroup

		void OperatorGroupInsert(
			string name,
			string description
			);

		void OperatorGroupUpdate(
			int operatorGroupId,
			string name,
			string description
			);

		void OperatorGroupDelete(
			int operatorGroupId
			);

		#endregion // OperatorGroup
	}
}