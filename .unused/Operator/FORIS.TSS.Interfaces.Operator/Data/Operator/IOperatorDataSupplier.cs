﻿using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;

namespace FORIS.TSS.Interfaces.Operator.Data.Operator
{
	public interface IOperatorDataSupplier :
		IDataSupplier<IOperatorDataTreater>,
		IPrincipalDataSupplier
	{
	}
}