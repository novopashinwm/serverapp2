﻿using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.Interfaces.Operator.Data
{
	public interface IOperatorDataSupplierProvider
	{
		IOperatorDataSupplier GetOperatorDataSupplier();
	}
}