using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;

namespace FORIS.TSS.Helpers.Operator.Data.Operator
{
	public class OperatorTable: 
		PrincipalTable<OperatorRow, OperatorTable, IOperatorData>
	{
		public override string Name
		{
			get { return "OPERATOR"; }
		}

		public OperatorTable()
		{
			
		}
		public OperatorTable( IContainer container )
		{
			container.Add( this );
		}

		#region Indexes

		private int idIndex = -1;
		public int IdIndex { get { return this.idIndex; } }

		private int nameIndex = -1;
		public int NameIndex { get { return this.nameIndex; } }

		private int loginIndex = -1;
		public int LoginIndex { get { return this.loginIndex; } }

		private int phoneIndex = -1;
		public int PhoneIndex { get { return this.phoneIndex; } }

		private int emailIndex = -1;
		public int EmailIndex { get { return this.emailIndex; } }

        private int guidIndex = -1;
        public int GuidIndex { get { return this.guidIndex; } }
        
        #endregion // Indexes

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.nameIndex = -1;
			this.loginIndex = -1;
			this.phoneIndex = -1;
			this.emailIndex = -1;
            this.guidIndex = -1;
        }
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.idIndex = this.DataTable.Columns.IndexOf( "OPERATOR_ID" );
			this.nameIndex = this.DataTable.Columns.IndexOf( "NAME" );
			this.loginIndex = this.DataTable.Columns.IndexOf( "LOGIN" );
			this.phoneIndex = this.DataTable.Columns.IndexOf( "PHONE" );
			this.emailIndex = this.DataTable.Columns.IndexOf( "EMAIL" );
            this.guidIndex = this.DataTable.Columns.IndexOf("GUID");
        }
		
		protected override OperatorRow OnCreateRow( DataRow dataRow )
		{
			return new OperatorRow( dataRow );
		}
	}

	public class OperatorRow:
		PrincipalRow<OperatorRow, OperatorTable, IOperatorData>,
		IManyToManyRow<OperatorGroupRow, OperatorGroupTable, IOperatorData, OperatorGroupOperatorTable>
	{
		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Login
		{
			get { return (string)this.DataRow[this.Table.LoginIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LoginIndex] = value; }
		}

		public string Phone
		{
			get { return (string)this.DataRow[this.Table.PhoneIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.PhoneIndex] = value; }
		}

		public string Email
		{
			get { return (string)this.DataRow[this.Table.EmailIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.EmailIndex] = value; }
		}

        public Guid Guid
        {
            get { return (Guid)this.DataRow[this.Table.GuidIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.GuidIndex] = value; }
        }

        public bool InGroup(OperatorGroupRow operatorGroupRow)
		{
			return this.helper.Linked( operatorGroupRow, this );
		}
		public void ToGroup( OperatorGroupRow operatorGroupRow )
		{
			this.helper.Link( operatorGroupRow, this );
		}
		public void FromGroup( OperatorGroupRow operatorGroupRow )
		{
			this.helper.Unlink( operatorGroupRow, this );
		}

		#endregion // Fields

		private class PrincipalChildRowCollection<TRow, TTable, TData>:
			ChildRowCollection<TRow, TTable, TData>,
			ICollection<TssPrincipalInfo>
			where TRow: PrincipalRow<TRow, TTable, TData>
			where TTable: PrincipalTable<TRow, TTable, TData>
			where TData: IPrincipalData
		{
			private readonly Dictionary<TssPrincipalInfo, TRow> infoHash =
				new Dictionary<TssPrincipalInfo, TRow>();

			protected override void OnInserted(FORIS.TSS.Common.CollectionChangeEventArgs<TRow> e)
			{
				this.infoHash.Add( e.Item.Info, e.Item );

				base.OnInserted(e);
			}

			protected override void OnRemoving(FORIS.TSS.Common.CollectionChangeEventArgs<TRow> e)
			{
				base.OnRemoving(e);

				this.infoHash.Remove( e.Item.Info );
			}

			protected override void OnClearing(FORIS.TSS.Common.CollectionEventArgs<TRow> e)
			{
				base.OnClearing(e);

				this.infoHash.Clear();
			}

			#region ICollection<TssPrincipalInfo> Members

			///<summary>
			///Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
			///</summary>
			///
			///<param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
			///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
			void ICollection<TssPrincipalInfo>.Add( TssPrincipalInfo item )
			{
				throw new NotImplementedException();
			}

			///<summary>
			///Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> contains a specific value.
			///</summary>
			///
			///<returns>
			///true if item is found in the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
			///</returns>
			///
			///<param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
			bool ICollection<TssPrincipalInfo>.Contains( TssPrincipalInfo item )
			{
				throw new NotImplementedException();
			}

			///<summary>
			///Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"></see> to an <see cref="T:System.Array"></see>, starting at a particular <see cref="T:System.Array"></see> index.
			///</summary>
			///
			///<param name="array">The one-dimensional <see cref="T:System.Array"></see> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"></see>. The <see cref="T:System.Array"></see> must have zero-based indexing.</param>
			///<param name="arrayIndex">The zero-based index in array at which copying begins.</param>
			///<exception cref="T:System.ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
			///<exception cref="T:System.ArgumentNullException">array is null.</exception>
			///<exception cref="T:System.ArgumentException">array is multidimensional.-or-arrayIndex is equal to or greater than the length of array.-or-The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"></see> is greater than the available space from arrayIndex to the end of the destination array.-or-Type T cannot be cast automatically to the type of the destination array.</exception>
			void ICollection<TssPrincipalInfo>.CopyTo( TssPrincipalInfo[] array, int arrayIndex )
			{
				throw new NotImplementedException();
			}

			///<summary>
			///Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
			///</summary>
			///
			///<returns>
			///true if item was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false. This method also returns false if item is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"></see>.
			///</returns>
			///
			///<param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
			///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
			bool ICollection<TssPrincipalInfo>.Remove( TssPrincipalInfo item )
			{
				throw new NotImplementedException();
			}

			#endregion

			#region IEnumerable<TssPrincipalInfo> Members

			///<summary>
			///Returns an enumerator that iterates through the collection.
			///</summary>
			///
			///<returns>
			///A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
			///</returns>
			///<filterpriority>1</filterpriority>
			IEnumerator<TssPrincipalInfo> IEnumerable<TssPrincipalInfo>.GetEnumerator()
			{
				throw new NotImplementedException();
			}

			#endregion
		}

		#region Child rows

		private readonly PrincipalChildRowCollection<OperatorGroupRow, OperatorGroupTable, IOperatorData> rowsOperatorGroup;

		public ChildRowCollection<OperatorGroupRow, OperatorGroupTable, IOperatorData> RowsOperatorGroup
		{
			get { return this.rowsOperatorGroup; }
		}

		#endregion // Child rows

		#region IManyToManyRow<OperatorGroupRow, OperatorGroupTable, IOperatorData, OperatorGroupOperatorTable> Members

		public ICollection<OperatorGroupRow> OppositeRows
		{
			get { return this.rowsOperatorGroup; }
		}

		private OperatorGroupOperatorTable helper;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public OperatorGroupOperatorTable Helper
		{
			get 
            { 
                return this.helper; 
            }
			set 
            { 
                this.helper = value; 
            }
		}

		#endregion // IManyToManyRow<OperatorGroupRow, OperatorGroupTable, IOperatorData, OperatorGroupOperatorTable> Members

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public OperatorRow( DataRow dataRow )
			: base( dataRow )
		{
			this.rowsOperatorGroup = new PrincipalChildRowCollection<OperatorGroupRow, OperatorGroupTable, IOperatorData>();
		}

		#region Implement PrincipalRow Members

		protected override string GetPrincipalName()
		{
			return this.Login;
		}
		
		protected override TssPrincipalType GetPrincipalType()
		{
			return TssPrincipalType.Operator;
		}

		protected override ICollection<TssPrincipalInfo> GetPrincipalParents()
		{
			return this.rowsOperatorGroup;
		}

		#endregion // Implement PrincipalRow Members

		protected override void OnBuildFields()
		{
			/* Нельзя допускать получение паролей 
			 * пользователей приложением
			 */

			if( this.DataRow.Table.Columns.Contains("PASSWORD") )
			{
				throw new ApplicationException( "Password access denied" );
			}
		}

		public override string ToString()
		{
			return
				String.Format(
					"Type: {0}, Name: {1}",
					this.GetType().FullName,
					this.Name
					);
		}

		protected override int CompareTo( OperatorRow row )
		{
			int result;

			result = this.Name.CompareTo( row.Name );

			if( result != 0 ) return result;

			return base.CompareTo( row );
		}
	}
}