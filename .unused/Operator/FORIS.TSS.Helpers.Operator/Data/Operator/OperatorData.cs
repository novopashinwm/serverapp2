using System;
using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Operator.Data.Operator
{
	public class OperatorData:
		FORIS.TSS.Helpers.Data.Data<IOperatorDataTreater>,
		IOperatorData
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private OperatorDataDispatcher tablesDataDispatcher;

		private OperatorGroupTable operatorGroup;
		private OperatorTable @operator;
		private OperatorGroupOperatorTable operatorGroupOperator;

		private OperatorDataAmbassador daOperatorGroup;
		private OperatorDataAmbassador daOperator;
		private OperatorDataAmbassador daOperatorGroupOperator;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorData( IContainer container ):this()
		{
			container.Add( this );
		}
		public OperatorData()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing)
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new OperatorDataDispatcher( this.components );

			this.@operator = new OperatorTable( this.components );
			this.operatorGroup = new OperatorGroupTable( this.components );
			this.operatorGroupOperator = new OperatorGroupOperatorTable( this.components);
			
			this.daOperatorGroup = new OperatorDataAmbassador();
			this.daOperator = new OperatorDataAmbassador();
			this.daOperatorGroupOperator = new OperatorDataAmbassador();

			this.daOperatorGroup.Item = this.operatorGroup;
			this.daOperator.Item = this.@operator;
			this.daOperatorGroupOperator.Item = this.operatorGroupOperator;

			this.tablesDataDispatcher.Ambassadors.Add( this.daOperatorGroup);
			this.tablesDataDispatcher.Ambassadors.Add( this.daOperator );
			this.tablesDataDispatcher.Ambassadors.Add( this.daOperatorGroupOperator );
		}

		#endregion // Componane Designer generated code

		#region Properties

		public new IOperatorDataSupplier DataSupplier
		{
			get { return (IOperatorDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get
			{
				return this.tablesDataDispatcher;
			}
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.operatorGroup.Rows.Inserted -= this.operatorGroupRows_Inserted;
			this.operatorGroup.Rows.Removing -= this.operatorGroupRows_Removing;

			this.@operator.Rows.Inserted -= this.operatorRows_Inserted;
			this.@operator.Rows.Removing -= this.operatorRows_Removing;

			this.principals.Clear();
			this.tablesDataDispatcher.Data = null;
		}
		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;

			foreach( OperatorGroupRow operatorGroupRow in this.operatorGroup.Rows )
			{
				this.principals.Add( operatorGroupRow.Info );
			}

			foreach( OperatorRow operatorRow in this.@operator.Rows )
			{
				this.principals.Add( operatorRow.Info );
			}

			this.operatorGroup.Rows.Inserted += this.operatorGroupRows_Inserted;
			this.operatorGroup.Rows.Removing += this.operatorGroupRows_Removing;

			this.@operator.Rows.Inserted += this.operatorRows_Inserted;
			this.@operator.Rows.Removing += this.operatorRows_Removing;
		}

		#endregion // Build & Destroy

		#region IPrincipalData Members

		private readonly CollectionWithEvents<TssPrincipalInfo> principals =
			new CollectionWithEvents<TssPrincipalInfo>();

		public ICollectionWithEvents<TssPrincipalInfo> Principals
		{
			get { return this.principals; }
		}

		IList<TssPrincipalInfo> IPrincipalData.GetPrincipalParents( TssPrincipalInfo principal )
		{
			#region Preconditions

			if( principal.PrincipalType != TssPrincipalType.Operator )
			{
				throw new ArgumentException();
			}

			#endregion // Preconditions

			OperatorRow operatorRow = this.@operator.FindRow( principal.Id );

			List<TssPrincipalInfo> result = 
				new List<TssPrincipalInfo>( operatorRow.RowsOperatorGroup.Count );

			foreach( OperatorGroupRow operatorGroupRow in operatorRow.RowsOperatorGroup )
			{
				result.Add( operatorGroupRow.Info );
			}

			return result;
		}

		#endregion // IPrincipalData Members

		#region Tables properties

		[Browsable(false)]
		public OperatorGroupTable OperatorGroup
		{
			get { return this.operatorGroup; }
		}

		[Browsable(false)]
		public OperatorTable Operator
		{
			get { return this.@operator; }
		}

		#endregion // Tables properties

		#region Rows' events

		private void operatorGroupRows_Inserted( object sender, CollectionChangeEventArgs<OperatorGroupRow> e )
		{
			this.principals.Insert( e.Index, e.Item.Info );
		}
		private void operatorGroupRows_Removing( object sender, CollectionChangeEventArgs<OperatorGroupRow> e )
		{
			this.principals.RemoveAt( e.Index );			
		}

		private void operatorRows_Inserted( object sender, CollectionChangeEventArgs<OperatorRow> e )
		{
			this.principals.Insert( this.operatorGroup.Rows.Count + e.Index, e.Item.Info );			
		}
		private void operatorRows_Removing( object sender, CollectionChangeEventArgs<OperatorRow> e )
		{
			this.principals.RemoveAt( this.operatorGroup.Rows.Count + e.Index );			
		}

		#endregion // Rows' events
	}

	public interface IOperatorData:
		IData<IOperatorDataTreater>,
		IOperatorDataSupplier,
		IPrincipalData
	{
		OperatorTable Operator { get; }
		OperatorGroupTable OperatorGroup { get; }
	}
}