using System.ComponentModel;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Operator.Data.Operator
{
	public class OperatorGroupOperatorTable:
		ManyToManyTable<OperatorGroupOperatorTable, OperatorGroupRow, OperatorGroupTable, OperatorRow, OperatorTable, IOperatorData>
	{
		public OperatorGroupOperatorTable( IContainer container )
		{
			container.Add( this );
		}

		public override string Name
		{
			get { return "OPERATORGROUP_OPERATOR"; }
		}

		protected override OperatorGroupTable LeftTable
		{
			get { return this.Data.OperatorGroup; }
		}

		protected override OperatorTable RightTable
		{
			get { return this.Data.Operator; }
		}

		public override int IdIndex
		{
			get { return this.idIndex; }
		}

		protected override int LeftKeyIndex
		{
			get { return this.leftKeyIndex; }
		}

		protected override int RightKeyIndex
		{
			get { return this.rightKeyIndex; }
		}

		private int idIndex = -1;
		private int leftKeyIndex = -1;
		private int rightKeyIndex = -1;

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.leftKeyIndex = -1;
			this.rightKeyIndex = -1;
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.idIndex = this.DataTable.Columns.IndexOf( "OPERATORGROUP_OPERATOR_ID" );
			this.leftKeyIndex = this.DataTable.Columns.IndexOf( "OPERATORGROUP_ID" );
			this.rightKeyIndex = this.DataTable.Columns.IndexOf( "OPERATOR_ID" );
		}

	}
}
