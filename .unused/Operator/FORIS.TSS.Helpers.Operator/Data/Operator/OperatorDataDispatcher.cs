using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Operator.Data.Operator
{
	public class OperatorDataDispatcher:
		DataDispatcher<IOperatorData>
	{
		#region Constructor &

		public OperatorDataDispatcher( IContainer container ): this()
		{
			container.Add( this );
		}

		public OperatorDataDispatcher()
		{

		}

		#endregion // Constructor

		#region Ambassadors

		private readonly OperatorDataAmbassadorCollection ambassadors =
			new OperatorDataAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new OperatorDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IOperatorData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors
	}

	public class OperatorDataAmbassadorCollection:
		DataAmbassadorCollection<IOperatorData>
	{
		public new OperatorDataAmbassador this[int index]
		{
			get { return (OperatorDataAmbassador)base[index]; }
		}
	}

	public class OperatorDataAmbassador:
		DataAmbassador<IOperatorData>
	{
		
	}
}