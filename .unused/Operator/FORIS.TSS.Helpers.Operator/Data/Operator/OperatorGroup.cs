using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;

namespace FORIS.TSS.Helpers.Operator.Data.Operator
{
	public class OperatorGroupTable:
		PrincipalTable<OperatorGroupRow, OperatorGroupTable, IOperatorData>
	{
		public override string Name
		{
			get { return "OPERATORGROUP"; }
		}

		public OperatorGroupTable()
		{
			
		}
		public OperatorGroupTable( IContainer container )
		{
			container.Add( this );
		}

		protected override OperatorGroupRow OnCreateRow( DataRow dataRow )
		{
			return new OperatorGroupRow( dataRow );
		}
	}

	public class OperatorGroupRow:
		PrincipalRow<OperatorGroupRow, OperatorGroupTable, IOperatorData>,
		IManyToManyRow<OperatorRow, OperatorTable, IOperatorData, OperatorGroupOperatorTable>
	{
		#region Fields

		private const int OPERATORGROUP_ID = 0;
		private const int NAME = 1;
		private const int DESCRIPTION = 2;

		public string Name
		{
			get { return (string)this.DataRow[OperatorGroupRow.NAME, DataRowVersion.Current]; }
			set { this.DataRow[OperatorGroupRow.NAME] = value; }
		}

		public string Description
		{
			get { return (string)this.DataRow[OperatorGroupRow.DESCRIPTION, DataRowVersion.Current]; }
			set { this.DataRow[OperatorGroupRow.DESCRIPTION] = value; }
		}

		#endregion // Fields

		#region Child rows

		private readonly ChildRowCollection<OperatorRow, OperatorTable, IOperatorData> rowsOperator;

		public ChildRowCollection<OperatorRow, OperatorTable, IOperatorData> RowsOperator
		{
			get { return this.rowsOperator; }
		}

		#endregion // Child rows 

		#region IManyToManyRow<OperatorRow, OperatorTable, IOperatorData, OperatorGroupOperatorTable> Members

		public ICollection<OperatorRow> OppositeRows
		{
			get { return this.rowsOperator; }
		}

		private OperatorGroupOperatorTable operatorGroupOperatorTable;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public OperatorGroupOperatorTable Helper
		{
			get { return this.operatorGroupOperatorTable; }
			set { this.operatorGroupOperatorTable = value; }
		}

		#endregion // IManyToManyRow<OperatorRow, OperatorTable, IOperatorData, OperatorGroupOperatorTable> Members

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[OperatorGroupRow.OPERATORGROUP_ID];
		}

		#endregion // Id

		public OperatorGroupRow( DataRow dataRow ): base( dataRow )
		{
			this.rowsOperator = new ChildRowCollection<OperatorRow, OperatorTable, IOperatorData>();
		}

		#region Implement PrincipalRow Members

		protected override string GetPrincipalName()
		{
			return this.Name;
		}
		
		protected override TssPrincipalType GetPrincipalType()
		{
			return TssPrincipalType.OperatorGroup;
		}

		private static readonly ICollection<TssPrincipalInfo> s_parents =
			new List<TssPrincipalInfo>( 0 );

		protected override ICollection<TssPrincipalInfo> GetPrincipalParents()
		{
			return OperatorGroupRow.s_parents;
		}

		#endregion // Implement PrincipalRow Members

		protected override void OnBuildFields()
		{
			
		}

		public override string ToString()
		{
			return
				String.Format(
					"Type: {0}, Name: {1}",
					this.GetType().FullName,
					this.Name
					);
		}

		protected override int CompareTo(OperatorGroupRow row)
		{
			int result;

			result = this.Name.CompareTo( row.Name );

			if( result != 0 ) return result;

			return base.CompareTo( row );
		}
	}
}