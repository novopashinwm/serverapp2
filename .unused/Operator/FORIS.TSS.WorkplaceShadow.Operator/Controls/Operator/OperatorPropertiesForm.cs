using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;
using OperatorGroupTableControl=FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupTableControl;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorPropertiesForm: 
		TssUserForm,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private IContainer components;
		private OperatorDataDispatcher dataDispatcher;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Label lblName;
        protected TabControl tcMain;
		private TabPage tpCommon;
		private OperatorGroupTableControl ctrlOperatorGroupTable;
		private OperatorDataAmbassador daOperatorGroupTableControl;
		private Label lblLogin;
		private Label lblPhone;
		private Label lblEmail;
		private TextBox edtLogin;
		private TextBox edtPhone;
		private TextBox edtEmail;
		private System.Windows.Forms.TextBox edtName;


		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TabPage tpGroups;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorPropertiesForm));
            this.ctrlOperatorGroupTable = new FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupTableControl();
            this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher(this.components);
            this.daOperatorGroupTableControl = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblName = new System.Windows.Forms.Label();
            this.edtName = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.edtLogin = new System.Windows.Forms.TextBox();
            this.edtPhone = new System.Windows.Forms.TextBox();
            this.edtEmail = new System.Windows.Forms.TextBox();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpCommon = new System.Windows.Forms.TabPage();
            tpGroups = new System.Windows.Forms.TabPage();
            this.panelFill.SuspendLayout();
            tpGroups.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tpCommon.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFill
            // 
            this.panelFill.Controls.Add(this.tcMain);
            resources.ApplyResources(this.panelFill, "panelFill");
            // 
            // tpGroups
            // 
            tpGroups.Controls.Add(this.ctrlOperatorGroupTable);
            resources.ApplyResources(tpGroups, "tpGroups");
            tpGroups.Name = "tpGroups";
            tpGroups.UseVisualStyleBackColor = true;
            // 
            // ctrlOperatorGroupTable
            // 
            // 
            // 
            // 
            this.ctrlOperatorGroupTable.ContextMenuStrip.Name = "cmsTable";
            this.ctrlOperatorGroupTable.ContextMenuStrip.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size")));
            resources.ApplyResources(this.ctrlOperatorGroupTable, "ctrlOperatorGroupTable");
            this.ctrlOperatorGroupTable.Editable = false;
            this.ctrlOperatorGroupTable.Name = "ctrlOperatorGroupTable";
            // 
            // dataDispatcher
            // 
            this.dataDispatcher.Ambassadors.Add(this.daOperatorGroupTableControl);
            // 
            // daOperatorGroupTableControl
            // 
            this.daOperatorGroupTableControl.Item = this.ctrlOperatorGroupTable;
            // 
            // tableLayoutPanelMain
            // 
            resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
            this.tableLayoutPanelMain.Controls.Add(this.lblName, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.edtName, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.lblLogin, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.lblPhone, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.lblEmail, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.edtLogin, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.edtPhone, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.edtEmail, 1, 3);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            // 
            // lblName
            // 
            resources.ApplyResources(this.lblName, "lblName");
            this.lblName.Name = "lblName";
            // 
            // edtName
            // 
            resources.ApplyResources(this.edtName, "edtName");
            this.edtName.Name = "edtName";
            // 
            // lblLogin
            // 
            resources.ApplyResources(this.lblLogin, "lblLogin");
            this.lblLogin.Name = "lblLogin";
            // 
            // lblPhone
            // 
            resources.ApplyResources(this.lblPhone, "lblPhone");
            this.lblPhone.Name = "lblPhone";
            // 
            // lblEmail
            // 
            resources.ApplyResources(this.lblEmail, "lblEmail");
            this.lblEmail.Name = "lblEmail";
            // 
            // edtLogin
            // 
            resources.ApplyResources(this.edtLogin, "edtLogin");
            this.edtLogin.Name = "edtLogin";
            // 
            // edtPhone
            // 
            resources.ApplyResources(this.edtPhone, "edtPhone");
            this.edtPhone.Name = "edtPhone";
            // 
            // edtEmail
            // 
            resources.ApplyResources(this.edtEmail, "edtEmail");
            this.edtEmail.Name = "edtEmail";
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpCommon);
            this.tcMain.Controls.Add(tpGroups);
            resources.ApplyResources(this.tcMain, "tcMain");
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            // 
            // tpCommon
            // 
            this.tpCommon.Controls.Add(this.tableLayoutPanelMain);
            resources.ApplyResources(this.tpCommon, "tpCommon");
            this.tpCommon.Name = "tpCommon";
            this.tpCommon.UseVisualStyleBackColor = true;
            // 
            // OperatorPropertiesForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "OperatorPropertiesForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatorPropertiesForm_FormClosing);
            this.panelFill.ResumeLayout(false);
            tpGroups.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tpCommon.ResumeLayout(false);
            this.tpCommon.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		#region IDataItem<IOperatorData> Members

		public IOperatorData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if( this.dataDispatcher.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if( this.dataDispatcher.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IOperatorData> Members

		#region Properties

		private DataItemControlMode mode;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set { this.mode = value; }
		}

		private int idOperator;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdOperator
		{
			get { return this.idOperator; }
			set
			{
				if( this.idOperator != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idOperator = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public string OperatorName
		{
			get { return this.edtName.Text; }
		}

		public string Login
		{
			get { return this.edtLogin.Text; }
		}

		public string Phone
		{
			get { return this.edtPhone.Text; }
		}

		public string Email
		{
			get { return this.edtEmail.Text; }
		}

		public int[] OperatorGroups
		{
			get
			{
				List<int> Result = new List<int>(this.ctrlOperatorGroupTable.CheckedOperatorGroups.Count);

				foreach( OperatorGroupRow rowOperatorGroup in this.ctrlOperatorGroupTable.CheckedOperatorGroups )
				{
					Result.Add( rowOperatorGroup.ID );					
				}

				return Result.ToArray();
			}
		}
		
		#endregion // Properties

		#region Data

		private OperatorRow rowOperator;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowOperator != null )
			{
				this.rowOperator.AfterChange -=  rowOperator_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowOperator != null )
			{
				this.rowOperator.AfterChange += rowOperator_AfterChange;
			}
		}

		private void rowOperator_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.ctrlOperatorGroupTable.CheckedOperatorGroups.Clear();

			this.rowOperator = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowOperator = this.Data.Operator.FindRow( this.idOperator );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:
					
					if( this.rowOperator == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.edtName.Text = this.rowOperator.Name;
					this.edtLogin.Text = this.rowOperator.Login;
					this.edtPhone.Text = this.rowOperator.Phone;
					this.edtEmail.Text = this.rowOperator.Email;

					#region Groups

					this.ctrlOperatorGroupTable.CheckedOperatorGroups = this.rowOperator.RowsOperatorGroup;

					#endregion // Groups

					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.edtName.Text = String.Empty;
					this.edtLogin.Text = String.Empty;
					this.edtPhone.Text = String.Empty;
					this.edtEmail.Text = String.Empty;

					#region Groups

					this.ctrlOperatorGroupTable.CheckedOperatorGroups.Clear();

					#endregion // Groups

					break;

					#endregion // New
			}
		}

		#endregion // View

		private void OperatorPropertiesForm_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorPropertiesForm_Warning_NameRequired );
				}

				if( this.edtLogin.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorPropertiesForm_Warning_LoginIsRequired );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}
	}
}