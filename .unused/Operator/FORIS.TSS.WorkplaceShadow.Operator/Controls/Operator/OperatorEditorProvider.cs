using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier;
using System.Diagnostics;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
    public class OperatorEditorProvider<TOperatorPropertiesForm> :
		EditorProvider<IOperatorDataTreater, IOperatorData>,
		IOperatorEditorProvider
        where TOperatorPropertiesForm : OperatorPropertiesForm, new()
    {
		public OperatorEditorProvider()
		{
			
		}
		public OperatorEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#region IClientItem<IOperatorClientDataProvider> Members

		private IOperatorClientDataProvider client;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IOperatorClientDataProvider Client
		{
			get { return this.client; }
			set { this.client = value; }
		}

		#endregion // IClientItem<IOperatorClientDataProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IOperatorClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		#region IOperatorEditorProvider Members

		void IOperatorEditorProvider.Password( int idOperator )
		{
			this.Password( idOperator );
		}

		#endregion // IOperatorEditorProvider Members

		protected override void New()
		{
			using( TOperatorPropertiesForm propertiesForm = new TOperatorPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IOperatorDataTreater Treater =
							((IOperatorDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.OperatorInsert(
								propertiesForm.OperatorName,
								propertiesForm.Login,
								propertiesForm.Phone,
								propertiesForm.Email,
								propertiesForm.OperatorGroups
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorEditorProvider_Exception_CanNotCreateNewOperator,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Properties( int idOperator )
		{
			using( OperatorPropertiesForm propertiesForm = new OperatorPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.Edit;
				propertiesForm.IdOperator = idOperator;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IOperatorDataTreater Treater =
							( (IOperatorDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.OperatorUpdate(
								idOperator,
								propertiesForm.OperatorName,
								propertiesForm.Login,
								propertiesForm.Phone,
								propertiesForm.Email,
								propertiesForm.OperatorGroups
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorEditorProvider_Exception_CanNotUpdateOperator,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Remove( int idOperator )
		{
			if(
				MessageBox.Show(
					Strings.OperatorEditorProvider_Warning_AreYouSureToDeleteOperator,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IOperatorDataTreater Treater =
						( (IOperatorDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.OperatorDelete( idOperator );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorEditorProvider_Exception_CanNotDeleteOperator,
							Ex.Message
							)
						);
				}
			}
		}

		protected virtual void Password( int idOperator )
		{
			using( OperatorPasswordForm PasswordForm = new OperatorPasswordForm() )
			{
				PasswordForm.Mode = DataItemControlMode.Edit;
				PasswordForm.IdOperator = idOperator;

				this.dataDispatcher.Ambassadors.Add( PasswordForm );

				try
				{
					if( PasswordForm.ShowDialog() == DialogResult.OK )
					{
						Debug.Assert( this.Client != null, "Client object has not been specified" );

						this.client.Session.SetPassword(
							PasswordForm.Password,
							idOperator
							);
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorEditorProvider_Exception_CanNotSetOperatorPassword,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( PasswordForm );
				}
			}
		}


		protected override ISecureObject GetSecureObject( int idOperator )
		{
			throw new NotImplementedException();
			// return this.Data.Operator.FindRow( idOperator );
		}
	}
}