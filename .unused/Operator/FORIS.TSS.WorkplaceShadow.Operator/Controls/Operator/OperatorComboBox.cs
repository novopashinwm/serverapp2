using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorComboBox:
		DataComboBox<OperatorItem, OperatorRow, OperatorTable, IOperatorData>
	{

		protected override OperatorTable GetTable()
		{
			return this.Data.Operator;
		}

		protected override OperatorItem CreateItem( int idOperator )
		{
			return new OperatorItem( idOperator );
		}
	}

	public class OperatorItem:
		DataItem<OperatorRow, OperatorTable, IOperatorData>
	{
		public OperatorItem( int idOperator ): base( idOperator )
		{
			
		}

		protected override OperatorRow GetRow()
		{
			return this.Data.Operator.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.Name;
		}
		
	}
}
