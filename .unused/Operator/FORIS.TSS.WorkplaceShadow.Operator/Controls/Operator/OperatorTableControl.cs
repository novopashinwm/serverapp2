using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorTableControl: 
		UserControl,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private OperatorDataDispatcher dataDispatcher;
		private CommandManager commandManager;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel columnModel;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn columnCheck;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnName;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnDescription;
		private OperatorDataAmbassador daTableModel;
		private OperatorXpRowFactory rowFactory;
		private FORIS.TSS.Common.Commands.Command cmdNew;
		private FORIS.TSS.Common.Commands.Command cmdProperties;
		private FORIS.TSS.Common.Commands.Command cmdRemove;
		private FORIS.TSS.Common.Commands.Command cmdCheckAll;
		private FORIS.TSS.Common.Commands.Command cmdUncheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciNewToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciPropertiesToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciRemoveToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllContextMenuItem;
		private ToolStripMenuItem tsmiCheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllContextMenuItem;
		private ContextMenuStrip cmsTable;
        private ToolStripMenuItem tsmiUncheckAll;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton separator1;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton separator2;
		private ToolBarButton tbbRemove;
		private ToolStripSeparator tssCheck;
		private ToolStripMenuItem tsmiNew;
		private ToolStripMenuItem tsmiProperties;
		private ToolStripSeparator tssEdit;
		private ToolStripMenuItem tsmiRemove;
		private CommandInstance ciNewContextMenuItem;
		private CommandInstance ciPropertiesContextMenuItem;
		private CommandInstance ciRemoveContextMenuItem;
		private ToolBarButton tbsPassword;
		private ToolBarButton tbbPassword;
		private Command cmdPassword;
        private CommandInstance ciPasswordToolbarButton;
        private Table table;
        private GroupBox groupBox1;
		private OperatorTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( OperatorTableControl ) );
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher( this.components );
			this.daTableModel = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorTableModel( this.components );
			this.rowFactory = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorXpRowFactory();
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
			this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.cmsTable = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tssCheck = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.tssEdit = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdNew = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbNew = new System.Windows.Forms.ToolBarButton();
			this.ciNewContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdProperties = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbProperties = new System.Windows.Forms.ToolBarButton();
			this.ciPropertiesContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdRemove = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbRemove = new System.Windows.Forms.ToolBarButton();
			this.ciRemoveContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciUncheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciUncheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar( this.components );
			this.separator1 = new System.Windows.Forms.ToolBarButton();
			this.separator2 = new System.Windows.Forms.ToolBarButton();
			this.tbsPassword = new System.Windows.Forms.ToolBarButton();
			this.tbbPassword = new System.Windows.Forms.ToolBarButton();
			this.cmdPassword = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPasswordToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmsTable.SuspendLayout();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daTableModel );
			// 
			// daTableModel
			// 
			this.daTableModel.Item = this.tableModel;
			// 
			// tableModel
			// 
			this.tableModel.Factory = this.rowFactory;
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnDescription} );
			// 
			// columnCheck
			// 
			this.columnCheck.Editor = null;
			resources.ApplyResources( this.columnCheck, "columnCheck" );
			this.columnCheck.Width = 20;
			// 
			// columnName
			// 
			this.columnName.Editable = false;
			this.columnName.Editor = null;
			resources.ApplyResources( this.columnName, "columnName" );
			this.columnName.Width = 200;
			// 
			// columnDescription
			// 
			this.columnDescription.Editable = false;
			this.columnDescription.Editor = null;
			resources.ApplyResources( this.columnDescription, "columnDescription" );
			this.columnDescription.Width = 400;
			// 
			// cmsTable
			// 
			this.cmsTable.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll,
            this.tssCheck,
            this.tsmiNew,
            this.tsmiProperties,
            this.tssEdit,
            this.tsmiRemove} );
			this.cmsTable.Name = "cmsTable";
			resources.ApplyResources( this.cmsTable, "cmsTable" );
			// 
			// tsmiCheckAll
			// 
			this.tsmiCheckAll.Name = "tsmiCheckAll";
			resources.ApplyResources( this.tsmiCheckAll, "tsmiCheckAll" );
			// 
			// tsmiUncheckAll
			// 
			this.tsmiUncheckAll.Name = "tsmiUncheckAll";
			resources.ApplyResources( this.tsmiUncheckAll, "tsmiUncheckAll" );
			// 
			// tssCheck
			// 
			this.tssCheck.Name = "tssCheck";
			resources.ApplyResources( this.tssCheck, "tssCheck" );
			// 
			// tsmiNew
			// 
			this.tsmiNew.Name = "tsmiNew";
			resources.ApplyResources( this.tsmiNew, "tsmiNew" );
			// 
			// tsmiProperties
			// 
			this.tsmiProperties.Name = "tsmiProperties";
			resources.ApplyResources( this.tsmiProperties, "tsmiProperties" );
			// 
			// tssEdit
			// 
			this.tssEdit.Name = "tssEdit";
			resources.ApplyResources( this.tssEdit, "tssEdit" );
			// 
			// tsmiRemove
			// 
			this.tsmiRemove.Name = "tsmiRemove";
			resources.ApplyResources( this.tsmiRemove, "tsmiRemove" );
			// 
			// cmdNew
			// 
			this.cmdNew.Instances.Add( this.ciNewToolbarButton );
			this.cmdNew.Instances.Add( this.ciNewContextMenuItem );
			this.cmdNew.Manager = this.commandManager;
			this.cmdNew.Execute += new System.EventHandler( this.cmdNew_Execute );
			// 
			// ciNewToolbarButton
			// 
			this.ciNewToolbarButton.Item = this.tbbNew;
			// 
			// tbbNew
			// 
			resources.ApplyResources( this.tbbNew, "tbbNew" );
			this.tbbNew.Name = "tbbNew";
			// 
			// ciNewContextMenuItem
			// 
			this.ciNewContextMenuItem.Item = this.tsmiNew;
			// 
			// cmdProperties
			// 
			this.cmdProperties.Instances.Add( this.ciPropertiesToolbarButton );
			this.cmdProperties.Instances.Add( this.ciPropertiesContextMenuItem );
			this.cmdProperties.Manager = this.commandManager;
			this.cmdProperties.Execute += new System.EventHandler( this.cmdProperties_Execute );
			this.cmdProperties.Update += new System.EventHandler( this.cmdProperties_Update );
			// 
			// ciPropertiesToolbarButton
			// 
			this.ciPropertiesToolbarButton.Item = this.tbbProperties;
			// 
			// tbbProperties
			// 
			resources.ApplyResources( this.tbbProperties, "tbbProperties" );
			this.tbbProperties.Name = "tbbProperties";
			// 
			// ciPropertiesContextMenuItem
			// 
			this.ciPropertiesContextMenuItem.Item = this.tsmiProperties;
			// 
			// cmdRemove
			// 
			this.cmdRemove.Instances.Add( this.ciRemoveToolbarButton );
			this.cmdRemove.Instances.Add( this.ciRemoveContextMenuItem );
			this.cmdRemove.Manager = this.commandManager;
			this.cmdRemove.Execute += new System.EventHandler( this.cmdRemove_Execute );
			this.cmdRemove.Update += new System.EventHandler( this.cmdRemove_Update );
			// 
			// ciRemoveToolbarButton
			// 
			this.ciRemoveToolbarButton.Item = this.tbbRemove;
			// 
			// tbbRemove
			// 
			resources.ApplyResources( this.tbbRemove, "tbbRemove" );
			this.tbbRemove.Name = "tbbRemove";
			// 
			// ciRemoveContextMenuItem
			// 
			this.ciRemoveContextMenuItem.Item = this.tsmiRemove;
			// 
			// cmdCheckAll
			// 
			this.cmdCheckAll.Instances.Add( this.ciCheckAllToolbarButton );
			this.cmdCheckAll.Instances.Add( this.ciCheckAllContextMenuItem );
			this.cmdCheckAll.Manager = this.commandManager;
			this.cmdCheckAll.Execute += new System.EventHandler( this.cmdCheckAll_Execute );
			// 
			// ciCheckAllToolbarButton
			// 
			this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
			// 
			// tbbCheckAll
			// 
			resources.ApplyResources( this.tbbCheckAll, "tbbCheckAll" );
			this.tbbCheckAll.Name = "tbbCheckAll";
			// 
			// ciCheckAllContextMenuItem
			// 
			this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
			// 
			// cmdUncheckAll
			// 
			this.cmdUncheckAll.Instances.Add( this.ciUncheckAllToolbarButton );
			this.cmdUncheckAll.Instances.Add( this.ciUncheckAllContextMenuItem );
			this.cmdUncheckAll.Manager = this.commandManager;
			this.cmdUncheckAll.Execute += new System.EventHandler( this.cmdUncheckAll_Execute );
			// 
			// ciUncheckAllToolbarButton
			// 
			this.ciUncheckAllToolbarButton.Item = this.tbbUncheckAll;
			// 
			// tbbUncheckAll
			// 
			resources.ApplyResources( this.tbbUncheckAll, "tbbUncheckAll" );
			this.tbbUncheckAll.Name = "tbbUncheckAll";
			// 
			// ciUncheckAllContextMenuItem
			// 
			this.ciUncheckAllContextMenuItem.Item = this.tsmiUncheckAll;
			// 
			// globalToolbar
			// 
			resources.ApplyResources( this.globalToolbar, "globalToolbar" );
			this.globalToolbar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.separator1,
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove,
            this.tbsPassword,
            this.tbbPassword} );
			this.globalToolbar.Divider = false;
			this.globalToolbar.Name = "globalToolbar";
			// 
			// separator1
			// 
			this.separator1.Name = "separator1";
			this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbsPassword
			// 
			this.tbsPassword.Name = "tbsPassword";
			this.tbsPassword.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbbPassword
			// 
			resources.ApplyResources( this.tbbPassword, "tbbPassword" );
			this.tbbPassword.Name = "tbbPassword";
			// 
			// cmdPassword
			// 
			this.cmdPassword.Instances.Add( this.ciPasswordToolbarButton );
			this.cmdPassword.Manager = this.commandManager;
			this.cmdPassword.Execute += new System.EventHandler( this.cmdPassword_Execute );
			this.cmdPassword.Update += new System.EventHandler( this.cmdPassword_Update );
			// 
			// ciPasswordToolbarButton
			// 
			this.ciPasswordToolbarButton.Item = this.tbbPassword;
			// 
			// table
			// 
			this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.ContextMenuStrip = this.cmsTable;
			resources.ApplyResources( this.table, "table" );
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.TableModel = this.tableModel;
			// 
			// groupBox1
			// 
			resources.ApplyResources( this.groupBox1, "groupBox1" );
			this.groupBox1.Controls.Add( this.table );
			this.groupBox1.Controls.Add( this.globalToolbar );
			this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			this.groupBox1.UseCompatibleTextRendering = true;
			// 
			// OperatorTableControl
			// 
			this.Controls.Add( this.groupBox1 );
			this.Name = "OperatorTableControl";
			resources.ApplyResources( this, "$this" );
			this.cmsTable.ResumeLayout( false );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.groupBox1.ResumeLayout( false );
			this.groupBox1.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion // Component Designer generated code

		#region IDataItem<IOperatorData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IOperatorData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IOperatorData> Members

		#region Properties

		private IOperatorEditorProvider editorProvider;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IOperatorEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public OperatorXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		public OperatorTableModel.OperatorXpRowCollection Rows
		{
			get { return this.tableModel.Rows; }
		}

		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int OperatorId = ( (OperatorXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Properties( OperatorId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if (this.table.SelectedItems.Length == 1)
			{
				int OperatorId = ( (OperatorXpRow)this.table.SelectedItems[0] ).Id;

				if (this.editorProvider != null)
				{
					this.editorProvider.Remove( OperatorId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionPassword()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int IdOperator = ( (OperatorXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Password( IdOperator );
				}
				else
					throw new ApplicationException(
						"EditorProvider must be specified"
						);
			}
		}

		private void ActionCheckAll()
		{
			foreach( OperatorXpRow row in this.tableModel.Rows )
			{
				row.Checked = true;
			}
		}
		private void ActionUncheckAll()
		{
			foreach( OperatorXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Actions

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdPassword_Execute( object sender, EventArgs e )
		{
			this.ActionPassword();
		}

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdPassword_Update( object sender, EventArgs e )
		{
			this.cmdPassword.Enabled = this.table.SelectedItems.Length == 1;
		}

		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		#endregion // Events

		#region Update

		public void BeginUpdate()
		{
			this.table.BeginUpdate();
		}
		public void EndUpdate()
		{
			this.table.EndUpdate();
		}

		#endregion // Update
	}
}
