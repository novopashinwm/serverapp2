using System.ComponentModel;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorTableModel:
		DataTableModel<OperatorXpRow, OperatorRow, OperatorTable, IOperatorData>
	{
		public OperatorTableModel( IContainer container )
		{
			container.Add( this );
		}
		public OperatorTableModel()
		{

		}

		#region Rows

		private readonly OperatorXpRowCollection rows =
			new OperatorXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new OperatorXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<OperatorXpRow, OperatorRow, OperatorTable, IOperatorData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new OperatorXpRowFactory Factory
		{
			get { return (OperatorXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override OperatorTable GetTable()
		{
			return this.Data.Operator;
		}
		public class OperatorXpRowCollection:
			DataXpRowCollection<OperatorXpRow, OperatorRow, OperatorTable, IOperatorData>
		{

		}
	}
}
