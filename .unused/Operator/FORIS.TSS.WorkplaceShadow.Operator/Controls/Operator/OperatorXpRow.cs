using System;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorXpRow:
		DataXpRow<OperatorRow, OperatorTable, IOperatorData>
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellLogin;
		private readonly Cell cellPhone;
		private readonly Cell cellEmail;

		#endregion // Cells

		public OperatorXpRow( int idOperator )
			: base( idOperator )
		{
			this.cellName = new Cell();
			this.cellLogin = new Cell();
			this.cellPhone = new Cell();
			this.cellEmail = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellName,
						this.cellLogin,
						this.cellPhone,
						this.cellEmail
					}
				);
		}

		protected override OperatorRow GetRow()
		{
			return this.Data.Operator.FindRow( this.Id );
		}

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellName.Text = this.Row.Name;
				this.cellLogin.Text = this.Row.Login;
				this.cellPhone.Text = this.Row.Phone;
				this.cellEmail.Text = this.Row.Email;
			}
			else
			{
				this.cellName.Text = String.Empty;
				this.cellLogin.Text = String.Empty;
				this.cellPhone.Text = String.Empty;
				this.cellEmail.Text = String.Empty;
			}
		}
	}
}