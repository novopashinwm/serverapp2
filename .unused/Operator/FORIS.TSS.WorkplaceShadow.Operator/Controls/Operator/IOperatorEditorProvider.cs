using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Operator;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public interface IOperatorEditorProvider: 
		IEditorProvider,
		IClientItem<IOperatorClientDataProvider>
	{
		void Password( int idOperator );
	}
}