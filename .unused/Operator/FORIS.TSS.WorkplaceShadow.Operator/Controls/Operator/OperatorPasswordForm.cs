using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorPasswordForm: 
		TssUserForm,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private OperatorDataDispatcher dataDispatcher;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
		private Label lblLogin;
		private Label lblPassword;
		private Label lblConfirmPassword;
		private Label txtLogin;
		private TextBox edtPassword;
		private TextBox edtConfirmPassword;
		private Label lblName;
		private Label txtName;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorPasswordForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorPasswordForm));
            this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher(this.components);
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblConfirmPassword = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.Label();
            this.edtPassword = new System.Windows.Forms.TextBox();
            this.edtConfirmPassword = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFill
            // 
            resources.ApplyResources(this.panelFill, "panelFill");
            // 
            // tableLayoutPanelMain
            // 
            resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
            this.tableLayoutPanelMain.Controls.Add(this.lblLogin, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.lblPassword, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.lblConfirmPassword, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.txtLogin, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.edtPassword, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.edtConfirmPassword, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.lblName, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.txtName, 1, 1);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            // 
            // lblLogin
            // 
            resources.ApplyResources(this.lblLogin, "lblLogin");
            this.lblLogin.Name = "lblLogin";
            // 
            // lblPassword
            // 
            resources.ApplyResources(this.lblPassword, "lblPassword");
            this.lblPassword.Name = "lblPassword";
            // 
            // lblConfirmPassword
            // 
            resources.ApplyResources(this.lblConfirmPassword, "lblConfirmPassword");
            this.lblConfirmPassword.Name = "lblConfirmPassword";
            // 
            // txtLogin
            // 
            resources.ApplyResources(this.txtLogin, "txtLogin");
            this.txtLogin.Name = "txtLogin";
            // 
            // edtPassword
            // 
            resources.ApplyResources(this.edtPassword, "edtPassword");
            this.edtPassword.Name = "edtPassword";
            // 
            // edtConfirmPassword
            // 
            resources.ApplyResources(this.edtConfirmPassword, "edtConfirmPassword");
            this.edtConfirmPassword.Name = "edtConfirmPassword";
            // 
            // lblName
            // 
            resources.ApplyResources(this.lblName, "lblName");
            this.lblName.Name = "lblName";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            // 
            // OperatorPasswordForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "OperatorPasswordForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatorPasswordForm_FormClosing);
            this.Controls.SetChildIndex(this.panelFill, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanelMain, 0);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		#region IDataItem<IOperatorData> Members

		public IOperatorData Data
		{
			get { return this.dataDispatcher.Data;}
			set
			{
				if( this.dataDispatcher.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if( this.dataDispatcher.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IOperatorData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set { this.mode = value; }
		}

		private int idOperator;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdOperator
		{
			get { return this.idOperator; }
			set
			{
				if( this.idOperator != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idOperator = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string Password
		{
			get { return this.edtPassword.Text; }
		}
		
		#endregion // Properties

		#region Data

		private OperatorRow rowOperator;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowOperator != null )
			{
				this.rowOperator.AfterChange -=  rowOperator_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowOperator != null )
			{
				this.rowOperator.AfterChange += rowOperator_AfterChange;
			}
		}

		private void rowOperator_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowOperator = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowOperator = this.Data.Operator.FindRow( this.idOperator );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:
					
					if( this.rowOperator == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.txtLogin.Text = this.rowOperator.Login;
					this.txtName.Text = this.rowOperator.Name;

					break;

					#endregion // Edit

				default:
					throw new NotSupportedException( Strings.OnlyEditModeIsSupported );
			}
		}

		#endregion // View

		private void OperatorPasswordForm_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtPassword.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorPasswordForm_Warning_PasswordIsRrequired );
				}

				if( this.edtConfirmPassword.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorPasswordForm_Warning_ConfirmPasswordIsRequired );
				}

				if( this.edtPassword.Text != this.edtConfirmPassword.Text )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorPasswordForm_Warning_DifferentPasswordsSpecified );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}
	}
}