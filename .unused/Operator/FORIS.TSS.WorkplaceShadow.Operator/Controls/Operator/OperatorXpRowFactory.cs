using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator
{
	public class OperatorXpRowFactory:
		DataXpRowFactory<OperatorXpRow, OperatorRow, OperatorTable, IOperatorData>
	{
		public override OperatorXpRow CreateXpRow( int idOperator )
		{
			return new OperatorXpRow( idOperator );
		}
	}
}
