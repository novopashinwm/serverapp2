using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupFilter: DataFilter<OperatorGroupRow, OperatorGroupTable, IOperatorData>
	{
		public override bool Check( OperatorGroupRow row )
		{
			return true;
		}
	}
}