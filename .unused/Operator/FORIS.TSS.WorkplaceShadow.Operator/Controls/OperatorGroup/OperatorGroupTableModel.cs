using System.ComponentModel;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;
using OperatorGroupXpRowCollection = FORIS.TSS.WorkplaceShadow.Controls.DataXpRowCollection<FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupXpRow, FORIS.TSS.Helpers.Operator.Data.Operator.OperatorGroupRow, FORIS.TSS.Helpers.Operator.Data.Operator.OperatorGroupTable, FORIS.TSS.Helpers.Operator.Data.Operator.IOperatorData>;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupTableModel:
		DataTableModel<OperatorGroupXpRow, OperatorGroupRow, OperatorGroupTable, IOperatorData>
	{
		public OperatorGroupTableModel( IContainer container )
		{
			container.Add( this );
		}
		public OperatorGroupTableModel()
		{

		}

		#region Rows

		private readonly OperatorGroupXpRowCollection rows =
			new OperatorGroupXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new OperatorGroupXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<OperatorGroupXpRow, OperatorGroupRow, OperatorGroupTable, IOperatorData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public new OperatorGroupXpRowFactory Factory
		{
			get { return (OperatorGroupXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override OperatorGroupTable GetTable()
		{
			return this.Data.OperatorGroup;
		}
		public class OperatorGroupXpRowCollection:
			DataXpRowCollection<OperatorGroupXpRow, OperatorGroupRow, OperatorGroupTable, IOperatorData>
		{

		}
	}
}