using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupXpRowFactory:
		DataXpRowFactory<OperatorGroupXpRow, OperatorGroupRow, OperatorGroupTable, IOperatorData>
	{
		public override OperatorGroupXpRow CreateXpRow( int idOperatorGroup )
		{
			return new OperatorGroupXpRow( idOperatorGroup );
		}
	}
}