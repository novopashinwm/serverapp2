using System;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupXpRow:
		DataXpRow<OperatorGroupRow, OperatorGroupTable, IOperatorData>
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellDescription;

		#endregion // Cells

		public OperatorGroupXpRow( int idOperatorGroup )
			: base( idOperatorGroup )
		{
			this.cellName = new Cell();
			this.cellDescription = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellName,
						this.cellDescription
					}
				);
		}

		protected override OperatorGroupRow GetRow()
		{
			return this.Data.OperatorGroup.FindRow( this.Id );
		}

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellName.Text = this.Row.Name;
				this.cellName.ToolTipText = this.Row.Description;

				this.cellDescription.Text = this.Row.Description;
			}
			else
			{
				this.cellName.Text = String.Empty;
				this.cellName.ToolTipText = String.Empty;

				this.cellDescription.Text = String.Empty;
			}
		}
	}
}