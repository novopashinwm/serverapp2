using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup;
using FORIS.TSS.WorkplaceShadow.Taxi.Controls.OperatorGroup;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupEditorProvider:
		EditorProvider<IOperatorDataTreater, IOperatorData>,
		IOperatorGroupEditorProvider
	{
		public OperatorGroupEditorProvider()
		{
			
		}
		public OperatorGroupEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		protected override void New()
		{
			using( OperatorGroupPropertiesForm Form = new OperatorGroupPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if( Form.ShowDialog() == DialogResult.OK )
					{
						IOperatorDataTreater Treater =
							this.Data.GetDataTreater();

						using( Treater )
						{
							Treater.OperatorGroupInsert(
								Form.OperatorGroupName,
								Form.OperatorGroupDescription
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorGroupEditorProvider_Exception_CanNotCreateNewOperatorGroup,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Properties( int idOperatorGroup )
		{
			using( OperatorGroupPropertiesForm Form = new OperatorGroupPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.Edit;
				Form.IdOperatorGroup = idOperatorGroup;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if( Form.ShowDialog() == DialogResult.OK )
					{
						IOperatorDataTreater Treater =
							this.Data.GetDataTreater();

						using( Treater )
						{
							Treater.OperatorGroupUpdate(
								idOperatorGroup,
								Form.OperatorGroupName,
								Form.OperatorGroupDescription
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorGroupEditorProvider_Exception_CanNotUpdateOperatorGroup,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Remove( int idOperatorGroup )
		{
			if(
				MessageBox.Show(
					Strings.OperatorGroupEditorProvider_Warning_AreYouSureToDeleteOperatorGroup,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IOperatorDataTreater Treater =
						this.Data.GetDataTreater();

					using( Treater )
					{
						Treater.OperatorGroupDelete( idOperatorGroup );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OperatorGroupEditorProvider_Exception_CanNotDeleteOperatorGroup,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int id )
		{
			throw new NotSupportedException();
		}
	}
}