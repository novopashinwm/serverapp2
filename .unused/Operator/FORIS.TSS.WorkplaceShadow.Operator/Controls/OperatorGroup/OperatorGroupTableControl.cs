using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Taxi.Controls.OperatorGroup;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.TransportDispatcher.XPTable.Events;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupTableControl: 
		UserControl,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private CommandManager commandManager;
		private OperatorDataDispatcher dataDispatcher;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel columnModel;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn columnCheck;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnName;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnDescription;
		private OperatorDataAmbassador daTableModel;
		private OperatorGroupXpRowFactory rowFactory;
		private FORIS.TSS.Common.Commands.Command cmdNew;
		private FORIS.TSS.Common.Commands.Command cmdProperties;
		private FORIS.TSS.Common.Commands.Command cmdRemove;
		private FORIS.TSS.Common.Commands.Command cmdCheckAll;
		private FORIS.TSS.Common.Commands.Command cmdUncheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciNewToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciPropertiesToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciRemoveToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllContextMenuItem;
		private ToolStripMenuItem tsmiCheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllContextMenuItem;
		private ContextMenuStrip cmsTable;
		private ToolStripMenuItem tsmiUncheckAll;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton tbbRemove;
		private ToolBarButton tbsCheck;
		private ToolBarButton tbsEdit;
		private ToolStripSeparator tssCheck;
		private ToolStripMenuItem tsmiNew;
		private ToolStripMenuItem tsmiProperties;
		private ToolStripSeparator tssEdit;
		private ToolStripMenuItem tsmiRemove;
		private CommandInstance ciNewContextMenuItem;
		private CommandInstance ciPropertiesContextMenuItem;
		private CommandInstance ciRemoveContextMenuItem;
		private OperatorGroupTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorGroupTableControl()
		{
			InitializeComponent();

			Initialize();

			this.Padding = new Padding( 0 );
		}
		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorGroupTableControl));
            this.commandManager = new FORIS.TSS.Common.Commands.CommandManager(this.components);
            this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher(this.components);
            this.daTableModel = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
            this.tableModel = new FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupTableModel(this.components);
            this.rowFactory = new FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup.OperatorGroupXpRowFactory();
            this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
            this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
            this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.cmsTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tssCheck = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.tssEdit = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdNew = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbNew = new System.Windows.Forms.ToolBarButton();
            this.ciNewContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdProperties = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbProperties = new System.Windows.Forms.ToolBarButton();
            this.ciPropertiesContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdRemove = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbRemove = new System.Windows.Forms.ToolBarButton();
            this.ciRemoveContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
            this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciUncheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
            this.ciUncheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.tbsCheck = new System.Windows.Forms.ToolBarButton();
            this.tbsEdit = new System.Windows.Forms.ToolBarButton();
            this.cmsTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.SuspendLayout();
            // 
            // dataDispatcher
            // 
            this.dataDispatcher.Ambassadors.Add(this.daTableModel);
            // 
            // daTableModel
            // 
            this.daTableModel.Item = this.tableModel;
            // 
            // tableModel
            // 
            this.tableModel.Factory = this.rowFactory;
            this.tableModel.SelectionAdjuster = null;
            // 
            // columnModel
            // 
            this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnDescription});
            // 
            // columnCheck
            // 
            this.columnCheck.Editor = null;
            resources.ApplyResources(this.columnCheck, "columnCheck");
            this.columnCheck.Width = 20;
            // 
            // columnName
            // 
            this.columnName.Editable = false;
            this.columnName.Editor = null;
            resources.ApplyResources(this.columnName, "columnName");
            this.columnName.Width = 100;
            // 
            // columnDescription
            // 
            this.columnDescription.Editable = false;
            this.columnDescription.Editor = null;
            resources.ApplyResources(this.columnDescription, "columnDescription");
            this.columnDescription.Width = 240;
            // 
            // cmsTable
            // 
            this.cmsTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll,
            this.tssCheck,
            this.tsmiNew,
            this.tsmiProperties,
            this.tssEdit,
            this.tsmiRemove});
            this.cmsTable.Name = "cmsTable";
            resources.ApplyResources(this.cmsTable, "cmsTable");
            // 
            // tsmiCheckAll
            // 
            this.tsmiCheckAll.Name = "tsmiCheckAll";
            resources.ApplyResources(this.tsmiCheckAll, "tsmiCheckAll");
            // 
            // tsmiUncheckAll
            // 
            this.tsmiUncheckAll.Name = "tsmiUncheckAll";
            resources.ApplyResources(this.tsmiUncheckAll, "tsmiUncheckAll");
            // 
            // tssCheck
            // 
            this.tssCheck.Name = "tssCheck";
            resources.ApplyResources(this.tssCheck, "tssCheck");
            // 
            // tsmiNew
            // 
            this.tsmiNew.Name = "tsmiNew";
            resources.ApplyResources(this.tsmiNew, "tsmiNew");
            // 
            // tsmiProperties
            // 
            this.tsmiProperties.Name = "tsmiProperties";
            resources.ApplyResources(this.tsmiProperties, "tsmiProperties");
            // 
            // tssEdit
            // 
            this.tssEdit.Name = "tssEdit";
            resources.ApplyResources(this.tssEdit, "tssEdit");
            // 
            // tsmiRemove
            // 
            this.tsmiRemove.Name = "tsmiRemove";
            resources.ApplyResources(this.tsmiRemove, "tsmiRemove");
            // 
            // cmdNew
            // 
            this.cmdNew.Instances.Add(this.ciNewToolbarButton);
            this.cmdNew.Instances.Add(this.ciNewContextMenuItem);
            this.cmdNew.Manager = this.commandManager;
            this.cmdNew.Execute += new System.EventHandler(this.cmdNew_Execute);
            // 
            // ciNewToolbarButton
            // 
            this.ciNewToolbarButton.Item = this.tbbNew;
            // 
            // tbbNew
            // 
            resources.ApplyResources(this.tbbNew, "tbbNew");
            this.tbbNew.Name = "tbbNew";
            // 
            // ciNewContextMenuItem
            // 
            this.ciNewContextMenuItem.Item = this.tsmiNew;
            // 
            // cmdProperties
            // 
            this.cmdProperties.Instances.Add(this.ciPropertiesToolbarButton);
            this.cmdProperties.Instances.Add(this.ciPropertiesContextMenuItem);
            this.cmdProperties.Manager = this.commandManager;
            this.cmdProperties.Execute += new System.EventHandler(this.cmdProperties_Execute);
            this.cmdProperties.Update += new System.EventHandler(this.cmdProperties_Update);
            // 
            // ciPropertiesToolbarButton
            // 
            this.ciPropertiesToolbarButton.Item = this.tbbProperties;
            // 
            // tbbProperties
            // 
            resources.ApplyResources(this.tbbProperties, "tbbProperties");
            this.tbbProperties.Name = "tbbProperties";
            // 
            // ciPropertiesContextMenuItem
            // 
            this.ciPropertiesContextMenuItem.Item = this.tsmiProperties;
            // 
            // cmdRemove
            // 
            this.cmdRemove.Instances.Add(this.ciRemoveToolbarButton);
            this.cmdRemove.Instances.Add(this.ciRemoveContextMenuItem);
            this.cmdRemove.Manager = this.commandManager;
            this.cmdRemove.Execute += new System.EventHandler(this.cmdRemove_Execute);
            this.cmdRemove.Update += new System.EventHandler(this.cmdRemove_Update);
            // 
            // ciRemoveToolbarButton
            // 
            this.ciRemoveToolbarButton.Item = this.tbbRemove;
            // 
            // tbbRemove
            // 
            resources.ApplyResources(this.tbbRemove, "tbbRemove");
            this.tbbRemove.Name = "tbbRemove";
            // 
            // ciRemoveContextMenuItem
            // 
            this.ciRemoveContextMenuItem.Item = this.tsmiRemove;
            // 
            // cmdCheckAll
            // 
            this.cmdCheckAll.Instances.Add(this.ciCheckAllToolbarButton);
            this.cmdCheckAll.Instances.Add(this.ciCheckAllContextMenuItem);
            this.cmdCheckAll.Manager = this.commandManager;
            this.cmdCheckAll.Execute += new System.EventHandler(this.cmdCheckAll_Execute);
            // 
            // ciCheckAllToolbarButton
            // 
            this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
            // 
            // tbbCheckAll
            // 
            resources.ApplyResources(this.tbbCheckAll, "tbbCheckAll");
            this.tbbCheckAll.Name = "tbbCheckAll";
            // 
            // ciCheckAllContextMenuItem
            // 
            this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
            // 
            // cmdUncheckAll
            // 
            this.cmdUncheckAll.Instances.Add(this.ciUncheckAllToolbarButton);
            this.cmdUncheckAll.Instances.Add(this.ciUncheckAllContextMenuItem);
            this.cmdUncheckAll.Manager = this.commandManager;
            this.cmdUncheckAll.Execute += new System.EventHandler(this.cmdUncheckAll_Execute);
            // 
            // ciUncheckAllToolbarButton
            // 
            this.ciUncheckAllToolbarButton.Item = this.tbbUncheckAll;
            // 
            // tbbUncheckAll
            // 
            resources.ApplyResources(this.tbbUncheckAll, "tbbUncheckAll");
            this.tbbUncheckAll.Name = "tbbUncheckAll";
            // 
            // ciUncheckAllContextMenuItem
            // 
            this.ciUncheckAllContextMenuItem.Item = this.tsmiUncheckAll;
            // 
            // table
            // 
            this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            this.table.BackColor = System.Drawing.Color.White;
            this.table.ColumnModel = this.columnModel;
            this.table.ContextMenuStrip = this.cmsTable;
            resources.ApplyResources(this.table, "table");
            this.table.EnableHeaderContextMenu = false;
            this.table.FullRowSelect = true;
            this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.table.HeaderContextMenu = null;
            this.table.Name = "table";
            this.table.NoItemsText = "";
            this.table.TableModel = this.tableModel;
            // 
            // globalToolbar
            // 
            resources.ApplyResources(this.globalToolbar, "globalToolbar");
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.tbsCheck,
            this.tbbNew,
            this.tbbProperties,
            this.tbsEdit,
            this.tbbRemove});
            this.globalToolbar.Divider = false;
            this.globalToolbar.Name = "globalToolbar";
            // 
            // tbsCheck
            // 
            this.tbsCheck.Name = "tbsCheck";
            this.tbsCheck.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbsEdit
            // 
            this.tbsEdit.Name = "tbsEdit";
            this.tbsEdit.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // OperatorGroupTableControl
            // 
            this.Controls.Add(this.table);
            this.Controls.Add(this.globalToolbar);
            this.Name = "OperatorGroupTableControl";
            resources.ApplyResources(this, "$this");
            this.cmsTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region Initialize

		private void Initialize()
		{
			this.checkedOperatorGroups.Inserted +=new CollectionChangeEventHandler<OperatorGroupRow>(checkedOperatorGroups_Inserted);	
			this.checkedOperatorGroups.Removing +=new CollectionChangeEventHandler<OperatorGroupRow>(checkedOperatorGroups_Removing);
			this.checkedOperatorGroups.Clearing +=new CollectionEventHandler<OperatorGroupRow>(checkedOperatorGroups_Clearing);

			this.tableModel.Rows.Inserted +=new CollectionChangeEventHandler<OperatorGroupXpRow>(Rows_Inserted);
			this.tableModel.Rows.Removing +=new CollectionChangeEventHandler<OperatorGroupXpRow>(Rows_Removing);
			this.tableModel.Rows.Clearing +=new CollectionEventHandler<OperatorGroupXpRow>(Rows_Clearing);
		}


		#endregion // Initialize

		#region IDataItem<IOperatorData> Members

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IOperatorData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }			
		}

		#endregion // IDataItem<IOperatorData> Members

		#region Properties

		private readonly CollectionWithEvents<OperatorGroupRow> checkedOperatorGroups =
			new CollectionWithEvents<OperatorGroupRow>();
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ICollection<OperatorGroupRow> CheckedOperatorGroups
		{
			get { return this.checkedOperatorGroups; }
			set
			{
				this.checkedOperatorGroups.Clear();

				foreach( OperatorGroupRow row in value )
				{
					this.checkedOperatorGroups.Add( row );
				}
			}
		}

		private bool editable = true;
		[Browsable(true)]
		[Category("Behavior")]
		[DefaultValue(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool Editable
		{
			get { return this.editable; }
			set
			{
				this.editable = value;

				this.tbbNew.Visible =
					this.tbbProperties.Visible =
					this.tbbRemove.Visible =
					this.tbsEdit.Visible = this.editable;
			}
		}

		private IOperatorGroupEditorProvider editorProvider;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IOperatorGroupEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		public OperatorGroupXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public ToolBar.ToolBarButtonCollection Buttons
		{
			get { return this.globalToolbar.Buttons; }
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new ContextMenuStrip ContextMenuStrip
		{
			get { return this.cmsTable; }
		}

		public event SelectionEventHandler SelectionChanged
		{
			add { this.table.SelectionChanged += value; }
			remove { this.table.SelectionChanged -= value; }
		}

		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int IdOperatorGroup = ( (OperatorGroupXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Properties( IdOperatorGroup );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);

			}

		}

		private void ActionRemove()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int IdOperatorGroup = ( (OperatorGroupXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Remove( IdOperatorGroup );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);

			}

		}

		private void ActionCheckAll()
		{
			foreach( OperatorGroupXpRow row in this.tableModel.Rows )
			{
				row.Checked = true;
			}
		}
		private void ActionUncheckAll()
		{
			foreach( OperatorGroupXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Actions

		#region Handle tableModel.Rows events

		private void Rows_Inserted( object sender, CollectionChangeEventArgs<OperatorGroupXpRow> e )
		{
			e.Item.CheckedChanged += new EventHandler(Row_CheckedChanged);
		}

		private void Rows_Removing( object sender, CollectionChangeEventArgs<OperatorGroupXpRow> e )
		{
			e.Item.CheckedChanged -=new EventHandler(Row_CheckedChanged);
		}

		private void Rows_Clearing( object sender, CollectionEventArgs<OperatorGroupXpRow> e )
		{
			foreach( OperatorGroupXpRow xpRow in this.tableModel.Rows )
			{
				xpRow.CheckedChanged -= new EventHandler(Row_CheckedChanged);
			}
		}

		private void Row_CheckedChanged( object sender, EventArgs e )
		{
			OperatorGroupXpRow XpRow = (OperatorGroupXpRow)sender;

			if( XpRow.Checked )
			{
				if( !this.checkedOperatorGroups.Contains( XpRow.Row ) )
				{
					this.checkedOperatorGroups.Add( XpRow.Row );
				}
			}
			else
			{
				if( this.checkedOperatorGroups.Contains( XpRow.Row ) )
				{
					this.checkedOperatorGroups.Remove( XpRow.Row );
				}
			}
		}

		#endregion // Handle tableModel.Rows events

		#region Handle checkedOperatorGroups events

		private void checkedOperatorGroups_Inserted( object sender, CollectionChangeEventArgs<OperatorGroupRow> e )
		{
			this.tableModel.Rows[e.Item.ID].Checked = true;
		}
		private void checkedOperatorGroups_Removing( object sender, CollectionChangeEventArgs<OperatorGroupRow> e )
		{
			this.tableModel.Rows[e.Item.ID].Checked = false;
		}
		private void checkedOperatorGroups_Clearing( object sender, CollectionEventArgs<OperatorGroupRow> e )
		{
			foreach( OperatorGroupXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Handle checkedOperatorGroups events

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		#endregion // Handle controls events
	}
}