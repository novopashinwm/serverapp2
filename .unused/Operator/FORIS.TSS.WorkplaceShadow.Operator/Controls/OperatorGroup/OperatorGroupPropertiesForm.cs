using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Operator.Controls.OperatorGroup
{
	public class OperatorGroupPropertiesForm: 
		TssUserForm,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;
		private OperatorDataDispatcher dataDispatcher;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.TextBox edtName;
		private System.Windows.Forms.TextBox edtDescription;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public OperatorGroupPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorGroupPropertiesForm));
			this.dataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher(this.components);
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblName = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.panelFill.SuspendLayout();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add(this.tableLayoutPanelMain);
			resources.ApplyResources(this.panelFill, "panelFill");
			// 
			// tableLayoutPanelMain
			// 
			resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
			this.tableLayoutPanelMain.Controls.Add(this.lblName, 0, 0);
			this.tableLayoutPanelMain.Controls.Add(this.lblDescription, 0, 1);
			this.tableLayoutPanelMain.Controls.Add(this.edtName, 1, 0);
			this.tableLayoutPanelMain.Controls.Add(this.edtDescription, 1, 1);
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblName
			// 
			resources.ApplyResources(this.lblName, "lblName");
			this.lblName.Name = "lblName";
			// 
			// lblDescription
			// 
			resources.ApplyResources(this.lblDescription, "lblDescription");
			this.lblDescription.Name = "lblDescription";
			// 
			// edtName
			// 
			resources.ApplyResources(this.edtName, "edtName");
			this.edtName.Name = "edtName";
			// 
			// edtDescription
			// 
			resources.ApplyResources(this.edtDescription, "edtDescription");
			this.edtDescription.Name = "edtDescription";
			// 
			// OperatorGroupPropertiesForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "OperatorGroupPropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatorGroupPropertiesForm_FormClosing);
			this.panelFill.ResumeLayout(false);
			this.panelFill.PerformLayout();
			this.tableLayoutPanelMain.ResumeLayout(false);
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		#region IDataItem<IOperatorData> Members

		public IOperatorData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if( this.dataDispatcher.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if( this.dataDispatcher.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IOperatorData> Members

		#region Properties

		private DataItemControlMode mode;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set { this.mode = value; }
		}

		private int idOperatorGroup;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdOperatorGroup
		{
			get { return this.idOperatorGroup; }
			set
			{
				if( this.idOperatorGroup != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idOperatorGroup = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public string OperatorGroupName
		{
			get { return this.edtName.Text; }
		}

		public string OperatorGroupDescription
		{
			get { return this.edtDescription.Text; }
		}

		#endregion // Properties

		#region Data

		private OperatorGroupRow rowOperatorGroup;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowOperatorGroup != null )
			{
				this.rowOperatorGroup.AfterChange -=  rowOperatorGroup_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowOperatorGroup != null )
			{
				this.rowOperatorGroup.AfterChange += rowOperatorGroup_AfterChange;
			}
		}

		private void rowOperatorGroup_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowOperatorGroup = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowOperatorGroup = this.Data.OperatorGroup.FindRow( this.idOperatorGroup );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:
					
					if( this.rowOperatorGroup == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.edtName.Text = this.rowOperatorGroup.Name;
					this.edtDescription.Text = this.rowOperatorGroup.Description;
					
					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.edtName.Text = string.Empty;
					this.edtDescription.Text = string.Empty;
					break;

					#endregion // New
			}
		}

		#endregion // View

		private void OperatorGroupPropertiesForm_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorGroupPropertiesForm_Warning_NameRequired );
				}

				if( this.edtDescription.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OperatorGroupPropertiesForm_Warning_DescriptionRequired );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}
	}
}