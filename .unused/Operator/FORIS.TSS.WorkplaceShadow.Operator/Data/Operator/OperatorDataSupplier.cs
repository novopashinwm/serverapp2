using System.ComponentModel;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Operator;

namespace FORIS.TSS.WorkplaceShadow.Operator.Data.Operator
{
	/// <summary>
	/// ��������� ������ ������ ��� �������
	/// </summary>
	public class OperatorDataSupplier:
		ModuleDataSupplier<IOperatorClientDataProvider,IOperatorDataSupplier>,
		IOperatorDataSupplier
	{
		#region Constructor & Dispose

		public OperatorDataSupplier( IContainer container )
		{
			container.Add( this );
		}

		public OperatorDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

		public IOperatorDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IOperatorDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetOperatorClientData();
		}
	}
}