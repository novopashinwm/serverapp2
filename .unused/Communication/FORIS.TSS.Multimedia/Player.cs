using System;
using System.Text;
using System.Runtime.InteropServices;

namespace FORIS.TSS.Multimedia
{
	/// <summary>
	/// ����� ��� ������������ wave, mp3 ������.
	/// </summary>
	public class Player
	{
		
		private string Pcommand;
		private bool isOpen;

		[DllImport("winmm.dll")]
		private static extern int mciSendString(string strCommand, StringBuilder strReturn, int iReturnLength, IntPtr hwndCallback);

		[DllImport ("winmm.dll")] 
		private static extern int mciGetErrorString(int retVal, StringBuilder strReturn, int iReturnLength);
		
		public Player()
		{
			
		}

		public bool IsFileOpen
		{
			get{return isOpen;}
		}

		/// <summary>
		/// ������� ����
		/// </summary>
		public void Close()
		{
			int ret = 0;
			int sbLength = 255;
			StringBuilder sb = new StringBuilder(sbLength);

			Pcommand = "close MediaFile";
			ret = mciSendString(Pcommand, null, 0, IntPtr.Zero);
			if (ret != 0) 
			{ 
				int retval = mciGetErrorString(ret, sb, sbLength);
				throw new Exception("MCI error: "+sb.ToString());
			}
			isOpen=false;
		}


		/// <summary>
		/// ������� ����
		/// </summary>
		/// <param name="sFileName"></param>
		public void Open(string sFileName)
		{
			int ret = 0;
			int sbLength = 255;
			StringBuilder sb = new StringBuilder(sbLength);
			
			Pcommand = "open \"" + sFileName + "\" type mpegvideo alias MediaFile";
			ret = mciSendString(Pcommand, null, 0, IntPtr.Zero);
			if (ret != 0) 
			{ 
				int retval = mciGetErrorString(ret, sb, sbLength);
				throw new Exception("MCI error: "+sb.ToString());
			}
			isOpen = true;
		}


		/// <summary>
		/// ���������
		/// </summary>
		/// <param name="loop">True - ������</param>
		public void Play(bool loop)
		{
			int ret = 0;
			int sbLength = 255;
			StringBuilder sb = new StringBuilder(sbLength);

			if(isOpen)
			{
				Pcommand = "play MediaFile";
				if (loop)
					Pcommand += " REPEAT";
				ret = mciSendString(Pcommand, null, 0, IntPtr.Zero);
				if (ret != 0) 
				{ 
					int retval = mciGetErrorString(ret, sb, sbLength);
					throw new Exception("MCI error: "+sb.ToString());
				}
			}
		}

		/// <summary>
		/// ����
		/// </summary>
		public void Stop()
		{
			int ret = 0;
			int sbLength = 255;
			StringBuilder sb = new StringBuilder(sbLength);

			if(isOpen)
			{
				Pcommand = "stop MediaFile";
				ret = mciSendString(Pcommand, null, 0, IntPtr.Zero);
				if (ret != 0) 
				{ 
					int retval = mciGetErrorString(ret, sb, sbLength);
					throw new Exception("MCI error: "+sb.ToString());
				}
			}
		}
	}

}
