﻿using System;
using System.Collections.Generic;
using System.Text;
using DShowNET;
using System.Runtime.InteropServices;
using DShowNET.Device;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;



namespace FORIS.TSS.IO.AudioCapture
{
	[ComVisible(false)]
	public class AudioCapturer : Component
	{
		private IGraphBuilder graphBuilder;

		private IMediaControl mediaCtrl;

		/// <summary>
		/// Установленные аудио устройства
		/// </summary>
		private ArrayList capDevices;

		IBaseFilter capFilter;
		IBaseFilter compressFilter;
		IBaseFilter dumpFilter;

		/// <summary>
		/// Установленные фильтры сжатия аудио
		/// </summary>
		private ArrayList audioCompressFilters;

		private ArrayList filters;

		string fileName = string.Empty;

		[Description("Имя MP3 файла, в который призводится сохранение")]
		[Browsable(true)]
		public string FileName
		{
			get { return fileName; }
			set 
			{ 
				fileName = value;

				if(dumpFilter != null)
					((IFileSinkFilter)dumpFilter).SetFileName(value, null);
			}
		}

		public AudioCapturer()
		{
			InitializeComponent();

			Initialize();
		}

		public AudioCapturer(IContainer container)
		{
			container.Add(this);

			InitializeComponent();

			Initialize();
		}

		private void Initialize()
		{
			if (!GetInterfaces())
			{
				return;
			}
			if (!Init())
			{
				return;
			}

			if (!Connect())
			{
				return;
			}
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
				this.mediaCtrl.Stop();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion Component Designer generated code


		public void Start()
		{
			try
			{
				Initialize();
				int hr;
				if (mediaCtrl != null)
				{
					hr = mediaCtrl.Run();
					if (hr < 0)
					{
						Trace.WriteLine("PhoneRec: Can't start record. Error code: " + hr.ToString("X"));
					}
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("PhoneRec: Can't start record. " + ex.Message);
			}
		}

		public void Stop()
		{
			int hr;
			if (mediaCtrl != null)
			{
				hr = mediaCtrl.Stop();
				if (hr < 0)
				{
					Trace.WriteLine("PhoneRec: Can't stop record. Error code: " + hr.ToString("X"));
				}
			}
		}

		private bool Init()
		{
			if (!DsUtils.IsCorrectDirectXVersion())
			{
				//MessageBox.Show("DirectX 8.1 NOT installed!", "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: DirectX is NOT installed!");
				return false;
			}

			if (!DsDev.GetDevicesOfCat(FilterCategory.AudioInputDevice, out capDevices))
			{
				//MessageBox.Show("No audio capture devices found!", "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: No audio capture devices found!");
				return false;
			}

			DsDevice dev = null;
			dev = capDevices[0] as DsDevice;

			if (dev == null)
			{
				return false;
			}


			if (!CreateCaptureDevice(dev.Mon))
			{
				Trace.WriteLine("PhoneRec: Cant't create capture device");
				return false;
			}

		   
			//Ищем Mpeg layer 3
			DsDevice mp3Device = null;
			
			//Сначала смотрим не установлелен ли кодек LAME MPEG LAYER-3
			if (!DsDev.GetDevicesOfCat(FilterCategory.Filter, out filters))
			{
				Trace.WriteLine("PhoneRec: Can't obtain DS Filters category");
				return false;
			}

			if (mp3Device == null)
			{
				foreach (DsDevice device in filters)
				{
					if (device.Name != null &&
						device.Name.ToUpper().Contains("LAME") &&
						device.Name.ToUpper().Contains("AUDIO") &&
						device.Name.ToUpper().Contains("ENCODER"))
					{
						mp3Device = device;
						break;
					}
				}
			}

			if (mp3Device == null)
			{
				foreach (DsDevice device in filters)
				{
					if (device.Name != null &&
						device.Name.ToUpper().Contains("LAME") &&
						device.Name.ToUpper().Contains("MPEG") &&
						device.Name.ToUpper().Contains("LAYER") &&
						(device.Name.ToUpper().Contains("3") ||
						device.Name.ToUpper().Contains("III")))
					{
						mp3Device = device;
						break;
					}
				}
			}

			if (mp3Device == null)
			{
				if (!DsDev.GetDevicesOfCat(FilterCategory.AudioCompressor, out audioCompressFilters))
				{
					Trace.WriteLine("PhoneRec: Can't obtain Audio Comressors category");
					return false;
				}
				foreach (DsDevice device in audioCompressFilters)
				{
					if (device.Name != null &&
						device.Name.ToUpper().Contains("MPEG") &&
						device.Name.ToUpper().Contains("LAYER") &&
						(device.Name.ToUpper().Contains("3") ||
						device.Name.ToUpper().Contains("III")))
					{
						mp3Device = device;
						break;
					}
				}
			}
			else //Установлен кодек LAME
			{
			}

			if (mp3Device == null)
			{
				Trace.WriteLine("PhoneRec: MPEG Layer-3 codec is not insatalled");
				return false;
			}

			if (!CreateCompressDevice(mp3Device.Mon))
			{
				Trace.WriteLine("PhoneRec: Can't create audio compress filter");
				return false;
			}

			DsDevice dumpDevice = null;
			foreach (DsDevice device in filters)
			{
				if (device.Name.ToUpper() == "DUMP")
				{
					dumpDevice = device;
					break;
				}
			}

			if (dumpDevice == null)
			{
				Trace.WriteLine("PhoneRec: Dump filter is not installed");
				return false;
			}

			if (!CreateDumpDevice(dumpDevice.Mon))
			{
				Trace.WriteLine("PhoneRec: Cant't create dump device");
				return false;
			}

			if (((IFileSinkFilter)dumpFilter).SetFileName(FileName, null) < 0)
			{
				Trace.WriteLine("PhoneRec: Cant't set file name");
				//return false;
			}

			return true;
		}

		private bool Connect()
		{
			int hr;
			hr = graphBuilder.AddFilter(capFilter, "Capture sound");
			if (hr < 0)
			{
				Trace.WriteLine("PhoneRec: Can't add Capture sound filter");
				return false;
			}
			hr = graphBuilder.AddFilter(compressFilter, "Compress audio");
			if (hr < 0)
			{
				Trace.WriteLine("PhoneRec: Can't add Compress audio filter");
				return false;
			}
			hr = graphBuilder.AddFilter(dumpFilter, "Write audio");
			if (hr < 0)
			{
				Trace.WriteLine("PhoneRec: Can't add Write audio filter");
				return false;
			}

			IEnumPins ppCapPins;
			hr = capFilter.EnumPins(out ppCapPins);
			if ((hr < 0) || (ppCapPins == null))
			{
				Trace.WriteLine("PhoneRec: Cant't enum Capture filter pins");
				return false;
			}
			IEnumPins ppCompPins;
			hr = compressFilter.EnumPins(out ppCompPins);
			if ((hr < 0) || (ppCompPins == null))
			{
				Trace.WriteLine("PhoneRec: Cant't enum Compress filter pins");
				return false;
			}
			IEnumPins ppDumpPins;
			hr = dumpFilter.EnumPins(out ppDumpPins);
			if ((hr < 0) || (ppDumpPins == null))
			{
				Trace.WriteLine("PhoneRec: Cant't enum Dump filter pins");
				return false;
			}


			IPin[] pins = new IPin[2];
			int pcFetched = 0;
			ppCapPins.Next(2, pins, out pcFetched);

			IPin CapPin = null;
			foreach (IPin pin in pins)
			{
				PinDirection pPinDir;
				pin.QueryDirection(out pPinDir);
				if (pPinDir == PinDirection.Output)
				{
					CapPin = pin;
					break;
				}
			}

			if (CapPin == null)
			{
				Trace.WriteLine("PhoneRec: Capture filter output pin was not found");
				return false;
			}

			ppCompPins.Next(2, pins, out pcFetched);

			IPin CmpPinIn = null;
			IPin CmpPinOut= null;
			foreach (IPin pin in pins)
			{
				PinDirection pPinDir;
				pin.QueryDirection(out pPinDir);
				if (pPinDir == PinDirection.Input)
					CmpPinIn = pin;
				if (pPinDir == PinDirection.Output)
					CmpPinOut = pin;
			}

			if (CmpPinIn == null)
			{
				Trace.WriteLine("PhoneRec: Compress filter input pin was not found");
				return false;
			}
			if (CmpPinOut == null)
			{
				Trace.WriteLine("PhoneRec: Compress filter output pin was not found");
				return false;
			}

			ppDumpPins.Next(2, pins, out pcFetched);
			IPin DumpPin = null;
			foreach (IPin pin in pins)
			{
				PinDirection pPinDir;
				pin.QueryDirection(out pPinDir);
				if (pPinDir == PinDirection.Input)
				{
					DumpPin = pin;
					break;
				}
			}
			if (DumpPin == null)
			{
				Trace.WriteLine("PhoneRec: Dump filter input pin was not found");
				return false;
			}

			hr = graphBuilder.Connect(CapPin, CmpPinIn);
			if (hr < 0)
			{
				Trace.WriteLine("PhoneRec: Can't connect capture and compress filters");
				return false;
			}
			hr = graphBuilder.Connect(CmpPinOut, DumpPin);
			if (hr < 0)
			{
				Trace.WriteLine("PhoneRec: Can't connect dump and compress filters");
				return false;
			}

			return true;
		}

		private bool GetInterfaces()
		{
			Type comType = null;
			object comObj = null;
			try
			{
				comType = Type.GetTypeFromCLSID(Clsid.FilterGraph);
				if (comType == null)
					throw new NotImplementedException(@"DirectShow FilterGraph not installed/registered!");
				comObj = Activator.CreateInstance(comType);
				graphBuilder = (IGraphBuilder)comObj; comObj = null;

				mediaCtrl = (IMediaControl)graphBuilder;
			}
			catch (Exception ee)
			{
				//MessageBox.Show("Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: Could not get interfaces\r\n" + ee.Message);
				return false;
			}
			finally
			{
				if (null != comObj)
					Marshal.ReleaseComObject(comObj);
				comObj = null;
			}
			return true;
		}

		bool CreateCaptureDevice(UCOMIMoniker mon)
		{
			object capObj = null;
			try
			{
				Guid gbf = typeof(IBaseFilter).GUID;
				mon.BindToObject(null, null, ref gbf, out capObj);
				capFilter = (IBaseFilter)capObj; capObj = null;
				return true;
			}
			catch (Exception ee)
			{
				//MessageBox.Show("Could not create capture device\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: Could not create capture device\r\n" + ee.Message);
				return false;
			}
			finally
			{
				if (null != capObj)
					Marshal.ReleaseComObject(capObj);
				capObj = null;
			}
		}

		bool CreateCompressDevice(UCOMIMoniker mon)
		{
			object cmpObj = null;
			try
			{
				Guid gbf = typeof(IBaseFilter).GUID;
				mon.BindToObject(null, null, ref gbf, out cmpObj);
				compressFilter = (IBaseFilter)cmpObj; cmpObj = null;
				return true;
			}
			catch (Exception ee)
			{
				//MessageBox.Show("Could not create compress device\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: Could not create compress device\r\n" + ee.Message);
				return false;
			}
			finally
			{
				if (null != cmpObj)
					Marshal.ReleaseComObject(cmpObj);
				cmpObj = null;
			}
		}

		bool CreateDumpDevice(UCOMIMoniker mon)
		{
			object dumpObj = null;
			try
			{
				Guid gbf = typeof(IBaseFilter).GUID;
				mon.BindToObject(null, null, ref gbf, out dumpObj);
				dumpFilter = (IBaseFilter)dumpObj; dumpObj = null;
				return true;
			}
			catch (Exception ee)
			{
				//MessageBox.Show("Could not create dump device\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				Trace.WriteLine("PhoneRec: Could not create dump device\r\n" + ee.Message);
				return false;
			}
			finally
			{
				if (null != dumpObj)
					Marshal.ReleaseComObject(dumpObj);
				dumpObj = null;
			}
		}
	}
}