using System;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;


namespace FORIS.TSS.IO.AudioCapture
{
    public static class Mp3Reader
    {

        private static Hashtable TagMap;
        private static Hashtable BitrateMap;
        private static Hashtable RateMap;
        private static int picCounter = 0;

        static Mp3Reader()
        {
            TagMap = new Hashtable();
            TagMap.Add("TIT2", "v2-song-title");
            TagMap.Add("TALB", "v2-album");
            TagMap.Add("TRCK", "v2-track");
            TagMap.Add("TPE1", "v2-artist");
            TagMap.Add("TCOM", "v2-composer");
            TagMap.Add("TDRL", "v2-release-date");
            TagMap.Add("TLEN", "v2-length");
            TagMap.Add("TCON", "v2-genre");
            BitrateMap = new Hashtable();
            BitrateMap.Add("011", "free");
            BitrateMap.Add("012", "free");
            BitrateMap.Add("013", "free");
            BitrateMap.Add("021", "free");
            BitrateMap.Add("022", "free");
            BitrateMap.Add("023", "free");
            BitrateMap.Add("111", "32");
            BitrateMap.Add("112", "32");
            BitrateMap.Add("113", "32");
            BitrateMap.Add("121", "32");
            BitrateMap.Add("122", "8");
            BitrateMap.Add("123", "8");
            BitrateMap.Add("211", "64");
            BitrateMap.Add("212", "48");
            BitrateMap.Add("213", "40");
            BitrateMap.Add("221", "48");
            BitrateMap.Add("222", "16");
            BitrateMap.Add("223", "16");
            BitrateMap.Add("311", "96");
            BitrateMap.Add("312", "56");
            BitrateMap.Add("313", "48");
            BitrateMap.Add("321", "56");
            BitrateMap.Add("322", "24");
            BitrateMap.Add("323", "24");
            BitrateMap.Add("411", "128");
            BitrateMap.Add("412", "64");
            BitrateMap.Add("413", "56");
            BitrateMap.Add("421", "64");
            BitrateMap.Add("422", "32");
            BitrateMap.Add("423", "32");
            BitrateMap.Add("511", "160");
            BitrateMap.Add("512", "80");
            BitrateMap.Add("513", "64");
            BitrateMap.Add("521", "80");
            BitrateMap.Add("522", "40");
            BitrateMap.Add("523", "40");
            BitrateMap.Add("611", "192");
            BitrateMap.Add("612", "96");
            BitrateMap.Add("613", "80");
            BitrateMap.Add("621", "96");
            BitrateMap.Add("622", "48");
            BitrateMap.Add("623", "48");
            BitrateMap.Add("711", "224");
            BitrateMap.Add("712", "112");
            BitrateMap.Add("713", "96");
            BitrateMap.Add("721", "112");
            BitrateMap.Add("722", "56");
            BitrateMap.Add("723", "56");
            BitrateMap.Add("811", "256");
            BitrateMap.Add("812", "128");
            BitrateMap.Add("813", "112");
            BitrateMap.Add("821", "128");
            BitrateMap.Add("822", "64");
            BitrateMap.Add("823", "64");
            BitrateMap.Add("911", "288");
            BitrateMap.Add("912", "160");
            BitrateMap.Add("913", "128");
            BitrateMap.Add("921", "144");
            BitrateMap.Add("922", "80");
            BitrateMap.Add("923", "80");
            BitrateMap.Add("1011", "320");
            BitrateMap.Add("1012", "192");
            BitrateMap.Add("1013", "160");
            BitrateMap.Add("1021", "160");
            BitrateMap.Add("1022", "96");
            BitrateMap.Add("1023", "96");
            BitrateMap.Add("1111", "352");
            BitrateMap.Add("1112", "224");
            BitrateMap.Add("1113", "192");
            BitrateMap.Add("1121", "176");
            BitrateMap.Add("1122", "112");
            BitrateMap.Add("1123", "112");
            BitrateMap.Add("1211", "384");
            BitrateMap.Add("1212", "256");
            BitrateMap.Add("1213", "224");
            BitrateMap.Add("1221", "192");
            BitrateMap.Add("1222", "128");
            BitrateMap.Add("1223", "128");
            BitrateMap.Add("1311", "416");
            BitrateMap.Add("1312", "320");
            BitrateMap.Add("1313", "256");
            BitrateMap.Add("1321", "224");
            BitrateMap.Add("1322", "144");
            BitrateMap.Add("1323", "144");
            BitrateMap.Add("1411", "448");
            BitrateMap.Add("1412", "384");
            BitrateMap.Add("1413", "320");
            BitrateMap.Add("1421", "256");
            BitrateMap.Add("1422", "160");
            BitrateMap.Add("1423", "160");
            BitrateMap.Add("1511", "bad");
            BitrateMap.Add("1512", "bad");
            BitrateMap.Add("1513", "bad");
            BitrateMap.Add("1521", "bad");
            BitrateMap.Add("1522", "bad");
            BitrateMap.Add("1523", "bad");
            RateMap = new Hashtable();
            RateMap.Add("01", "44100");
            RateMap.Add("02", "22050");
            RateMap.Add("03", "11025");
            RateMap.Add("11", "48000");
            RateMap.Add("12", "24000");
            RateMap.Add("13", "12000");
            RateMap.Add("21", "32000");
            RateMap.Add("22", "16000");
            RateMap.Add("23", "8000");
            RateMap.Add("31", "bad");
            RateMap.Add("32", "bad");
            RateMap.Add("33", "bad");

        }


        private static double[] versions = { 2.5, 0, 2, 1 };

        private static int[] layers = { 0, 3, 2, 1 };

        private static string[] channelModes = { "Stereo", "JointStereo", "DualChannel", "Mono" };

        private static string[] genres = {"Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk","Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", 
									  "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial","Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", 
									  "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion","Trance", "Classical", "Instrumental", "Acid", "House", "Game", 
									  "Sound Clip", "Gospel", "Noise", "Alt. Rock", "Bass", "Soul","Punk", "Space", "Meditative", "Instrum. Pop", "Instrum. Rock", 
									  "Ethnic", "Gothic", "Darkwave", "Techno-Indust.", "Electronic","Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", 
									  "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle","Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", 
									  "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz","Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", 
									  "Folk/Rock", "National Folk", "Swing", "Fusion", "Bebob", "Latin","Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", 
									  "Progress. Rock", "Psychadel. Rock", "Symphonic Rock", "Slow Rock","Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", 
									  "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony","Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", 
									  "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad","Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", 
									  "A Capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass","Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Negerpunk", 
									  "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal","Black Metal", "Crossover", "Contemporary Christian", "Christian Rock",
									  "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop"};


        public static string Genre(int index)
        {
            return ((index < genres.Length) ? genres[index] : "Unknown");
        }

        private static int GetLength(byte[] ba)
        {
            int len = (ba[3] + (byte)(ba[2] << 7));
            len += ((byte)(ba[2] >> 1) + (byte)(ba[1] << 6)) * 256;
            len += ((byte)(ba[1] >> 2) + (byte)(ba[0] << 5)) * 65536/*4096?*/;
            len += (byte)(ba[0] >> 3) * 16776960;
            return len;
        }

        public static string TransformV2Tag(string tagName, byte[] tagContentArray)
        {

            //the only binary tag we are going to handle
            string rv = "";
            if (tagName != "APIC")
            {
                rv = System.Text.Encoding.ASCII.GetString(tagContentArray);
            }

            string tmp;
            if (tagName == "TLEN")
            {
                rv = rv.Replace('\0', ' ').Trim();
                int sLength = Int32.Parse(rv);
                rv = "";
                int ln;
                sLength = sLength / 1000;
                if (sLength > 3600)
                {
                    ln = (int)Math.Floor((double)sLength / 3600);
                    rv += ln.ToString();
                    sLength -= ln * 3600;
                    rv += ":";
                }
                if (sLength > 60)
                {
                    ln = ((int)Math.Floor((double)sLength / 60));
                    tmp = ln.ToString();
                    if (tmp.Length == 1) tmp = "0" + tmp;
                    rv += tmp;
                    sLength -= ln * 60;
                    rv += ":";
                }
                else
                {
                    rv += "00:";
                }
                tmp = sLength.ToString();
                if (tmp.Length == 1) tmp = "0" + tmp;
                rv += tmp;
            }
            if (tagName == "APIC")
            {
                byte[] tmpStart = new byte[40];
                Array.Copy(tagContentArray, tmpStart, Math.Min(40, tagContentArray.Length));
                string tagContent = System.Text.Encoding.ASCII.GetString(tmpStart);

                int zeroCount = 0, ii = tagContent.IndexOf("image/");
                while (zeroCount < 3)
                {
                    if (tagContentArray[ii] == 0)
                    {
                        zeroCount++;
                    }
                    ii++;
                }

                tagContent = tagContent.Remove(0, tagContent.IndexOf("image/") + 6);
                string imgExt = tagContent.Substring(0, tagContent.IndexOf('\0'));



                if ((tagContentArray.Length - ii) > 0)
                {
                    FileStream picFile = new FileStream("image" + picCounter.ToString() + "." + imgExt, FileMode.Create, FileAccess.Write);
                    picCounter++;

                    picFile.Write(tagContentArray, ii, tagContentArray.Length - ii);
                    /*for (int i = ii; i < tagContentArray.Length; i++)
                    {
                        picFile.WriteByte((byte)tagContentA[i]);
                    }*/
                    picFile.Close();
                    rv = "image" + (picCounter - 1).ToString() + "." + imgExt;
                }
                else
                {
                    rv = "empty";
                }
            }

            rv = rv.Replace('\0', ' ').Trim();
            return rv;

        }




        public static void getTagInfo(string fileName, ref XmlElement tagInfo, ref XmlDocument xmlDoc)
        {
            FileStream mp3File;
            long startPos = 0;
            byte[] ba = new byte[6];
            XmlAttribute xmlAttrib;
            try
            {
                mp3File = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                xmlAttrib = xmlDoc.CreateAttribute("file-error");
                xmlAttrib.Value = e.Message;
                tagInfo.Attributes.Append(xmlAttrib);
                return;
            }

            mp3File.Read(ba, 0, 6);
            xmlAttrib = xmlDoc.CreateAttribute("id3v2");
            if ((((char)ba[0]).ToString() + ((char)ba[1]).ToString() + ((char)ba[2]).ToString()) == "ID3")
            {
                xmlAttrib.Value = "1";
                tagInfo.Attributes.Append(xmlAttrib);
                int version = ba[3];
                int thsize;
                int tfsize;
                thsize = (version > 2) ? 4 : 3;
                tfsize = (version > 2) ? 2 : 0;
                bool isExtended = false;
                //check 6th byte of ba for flags ( left bits : unsync-extended-experimental ) 
                if ((byte)(ba[5] << 1) > 127)
                {
                    isExtended = true;
                }
                mp3File.Read(ba, 0, 4);
                int headerLength = GetLength(ba);
                if (isExtended)
                {
                    mp3File.Read(ba, 0, 4);
                    int extHeaderLength = GetLength(ba) - 4;
                    ba = new byte[extHeaderLength];
                    mp3File.Read(ba, 0, extHeaderLength);
                }
                ba = new byte[headerLength];
                mp3File.Read(ba, 0, headerLength);
                startPos = mp3File.Position;

                int pos = 0;
                byte[] tag = new byte[thsize];
                byte[] len = new byte[thsize];
                byte[] str;
                string tagName, tagContent;
                int tagLength = 0;
                do
                {
                    if ((pos + 10) > headerLength)
                    {
                        break;
                    }
                    tagName = ""; tagContent = "";
                    Array.Copy(ba, pos, tag, 0, thsize);
                    pos += thsize;

                    Array.Copy(ba, pos, len, 0, thsize);


                    pos += thsize;
                    if (tfsize > 0)
                    {
                        int shift = 0;
                        if (ba[pos + 1] > 127)
                        {
                            shift += 4;
                            tagContent += "compressed; ";
                        }
                        if ((byte)(ba[pos + 1] << 1) > 127)
                        {
                            shift += 1;
                            tagContent += "encrypted; ";
                        }
                        if ((byte)(ba[pos + 1] << 1) > 127)
                        {
                            shift += 1;
                        }
                        pos += (2 + shift);
                    }

                    //tagLength = len[0]*65536*256+len[1]*65536+len[2]*256+len[3];
                    tagLength = 0;
                    for (int i = thsize - 1; i >= 0; i--)
                    {
                        int ml = 1;
                        for (int j = i; j < thsize - 1; j++)
                        {
                            ml *= 256;
                        }
                        //get multiplier
                        tagLength += len[i] * ml;
                    }
                    str = new byte[tagLength];
                    if (tagLength > ba.Length)
                    {
                        //means someone was too bored to stuff the end of the header with \0s and used fancy text instead so out length detection screwed up
                        SetXmlAttribute("potential-error", "1", ref xmlDoc, ref tagInfo);
                        break;
                    }
                    Array.Copy(ba, pos, str, 0, tagLength);
                    pos += tagLength;


                    tagName = System.Text.Encoding.ASCII.GetString(tag);

                    if ((tagLength > 0) && (tagName.Length > 0))
                    {
                        //int p;
                        tagContent = Mp3Reader.TransformV2Tag(tagName, str);
                        tagName = ((Mp3Reader.TagMap.Contains(tagName)) ? ((string)Mp3Reader.TagMap[tagName]) : "v2-tag-" + tagName);
                        SetXmlAttribute(tagName, tagContent, ref xmlDoc, ref tagInfo);
                    }
                }
                while (tagLength > 0);
            }
            else
            {
                xmlAttrib.Value = "0";
                tagInfo.Attributes.Append(xmlAttrib);
            }


            mp3File.Seek(-128, SeekOrigin.End);
            ba = new byte[128];
            mp3File.Read(ba, 0, 128);
            //Console.WriteLine((((char)ba[0]).ToString()+((char)ba[1]).ToString()+((char)ba[2]).ToString() ));
            if ((((char)ba[0]).ToString() + ((char)ba[1]).ToString() + ((char)ba[2]).ToString()) == "TAG")
            {
                xmlAttrib = xmlDoc.CreateAttribute("id3v1");
                xmlAttrib.Value = "1";
                tagInfo.Attributes.Append(xmlAttrib);
                string tagContent;
                tagContent = Mp3Reader.GetV1Tag(ba, 3, 33);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("song-title", tagContent, ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 33, 63);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("artist", tagContent, ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 63, 93);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("album-title", tagContent, ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 93, 97);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("year", tagContent, ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 97, 126);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("comment", tagContent, ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 126, 127);
                if ((tagContent.Length > 0) && (ba[125] == '\0'))
                {
                    SetXmlAttribute("track", ((int)tagContent[0]).ToString(), ref xmlDoc, ref tagInfo);
                }
                tagContent = Mp3Reader.GetV1Tag(ba, 127, 128);
                if (tagContent.Length > 0)
                {
                    SetXmlAttribute("genre", Mp3Reader.Genre((int)tagContent[0]), ref xmlDoc, ref tagInfo);
                }
            }
            else
            {
                xmlAttrib = xmlDoc.CreateAttribute("id3v1");
                xmlAttrib.Value = "0";
                tagInfo.Attributes.Append(xmlAttrib);
            }

            mp3File.Seek(startPos, SeekOrigin.Begin);
            ba = new byte[2];
            mp3File.Read(ba, 0, 2);
            while ((ba[0] != 255) || (ba[1] < 224))
            {
                ba[0] = ba[1];
                mp3File.Read(ba, 1, 1);
            }
            byte tmp = ba[1];
            ba = new byte[3];
            ba[0] = tmp;
            mp3File.Read(ba, 1, 2);
            byte mpegLayer, mpegVersion, bitrateBits, rateBits, chanMode;
            mpegVersion = (byte)(((byte)(ba[0] << 3)) >> 6);
            mpegLayer = (byte)(((byte)(ba[0] << 5)) >> 6);
            bitrateBits = (byte)(ba[1] >> 4);
            rateBits = (byte)(((byte)(ba[1] << 4)) >> 6);
            chanMode = (byte)(ba[2] >> 6);
            xmlAttrib = xmlDoc.CreateAttribute("mpeg-version");
            xmlAttrib.Value = versions[mpegVersion].ToString();
            tagInfo.Attributes.Append(xmlAttrib);
            xmlAttrib = xmlDoc.CreateAttribute("mpeg-layer");
            xmlAttrib.Value = layers[mpegLayer].ToString();
            tagInfo.Attributes.Append(xmlAttrib);
            string mask = bitrateBits.ToString() + ((int)Math.Floor(versions[mpegVersion])).ToString() + layers[mpegLayer].ToString();
            xmlAttrib = xmlDoc.CreateAttribute("bitrate");
            xmlAttrib.Value = (string)Mp3Reader.BitrateMap[mask] + "Kbps";
            tagInfo.Attributes.Append(xmlAttrib);
            mask = rateBits.ToString() + ((int)Math.Ceiling(versions[mpegVersion])).ToString();
            xmlAttrib = xmlDoc.CreateAttribute("sampling-rate");
            xmlAttrib.Value = (string)Mp3Reader.RateMap[mask] + "Hz";
            tagInfo.Attributes.Append(xmlAttrib);
            xmlAttrib = xmlDoc.CreateAttribute("channel-mode");
            xmlAttrib.Value = (string)Mp3Reader.channelModes[chanMode];
            tagInfo.Attributes.Append(xmlAttrib);


            mp3File.Close();
        }

        private static string GetV1Tag(byte[] ba, int sp, int ep)
        {
            string tagContent = "";
            for (int i = sp; i < ep; i++)
            {
                if (ba[i] == 0)
                {
                    break;
                }
                tagContent += (char)ba[i];
            }
            return tagContent;
        }

        private static void SetXmlAttribute(string attributeName, string attributeValue, ref XmlDocument xmlDoc, ref XmlElement xmlElement)
        {

            XmlAttribute xmlAttrib;
            string separator = "";

            if (xmlElement.GetAttributeNode(attributeName) == null)
            {
                xmlAttrib = xmlDoc.CreateAttribute(attributeName);
                xmlElement.Attributes.Append(xmlAttrib);
                xmlAttrib.Value = "";
            }
            else
            {
                separator = "; ";
                xmlAttrib = xmlElement.GetAttributeNode(attributeName);
            }

            for (int i = 0; i < attributeValue.Length; i++)
            {
                if ((attributeValue[i] < '\x20') && (attributeValue[i] != '\t') && (attributeValue[i] != '\n') && (attributeValue[i] != '\r'))
                {
                    attributeValue = attributeValue.Remove(i, 1);
                    i--;
                }
            }
            xmlAttrib.Value += separator + attributeValue.Replace("\"", "&quot;");

        }
    }
}
