using System;
using System.Diagnostics;
using System.Threading;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// adapter for communication with remote client
	/// </summary>
	public class Comm : System.ComponentModel.Component
	{
		private System.ComponentModel.IContainer components = null;

		TraceSwitch tsComm = new TraceSwitch("Comm", "Communication module info");

		public Comm(System.ComponentModel.IContainer container) : this()
		{
			// Required for Windows.Forms Class Composition Designer support
			container.Add(this);
		}

		public Comm()
		{
			// Required for Windows.Forms Class Composition Designer support
			InitializeComponent();

			if(DesignMode) return;
			
			lnk = CommGateway.Inst;
			lnk.CallChange += (callHandler = new CallEventHandler(lnk_CallChange));
			lnk.Offering += (offerHandler = new OfferingEventHandler(lnk_Offering));
		}

		~Comm()
		{
			Debug.WriteLine("... ~Comm()");
			Dispose(false);
		}


		#region IDisposable Members

		public new void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			Debug.WriteLine("... Comm.Dispose(true)");
			if (disposing)
			{
				if(lnk != null)
				{
					lnk.CallChange -= callHandler;
					lnk.Offering -= offerHandler;
					lnk.Dispose();
					//HangUp();
				}
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		/// <summary>
		/// for voice communication. unable use tapi
		/// </summary>
		CommGateway lnk;

		CallEventHandler callHandler;
		OfferingEventHandler offerHandler;

		/// <summary>
		/// incoming call event
		/// </summary>
		public event OfferingEventHandler OfferingEvent;
		/// <summary>
		/// call state changed
		/// </summary>
		public event CallEventHandler CallEvent;

		public enum CallType : byte
		{
			Data	= 0,
			Voice	= 1
		}
		struct CallInfo
		{
			public string phone;
			public CallType type;

			public CallInfo(string _phone, CallType _type)
			{
				phone = _phone;
				type = _type;
			}
		}

		/// <summary>
		/// ���������� ������ � ��������� ����������� ������
		/// </summary>
		public bool MakeCall(string phone)
		{
			return MakeCall(phone, CallType.Voice, true);
		}

		/// <summary>
		/// make call
		/// </summary>
		/// <param name="phone">target phone</param>
		public bool MakeCall(string phone, CallType type, bool async)
		{
			if(!async) throw new NotImplementedException("���������� ����� �� ����������");

			ThreadPool.QueueUserWorkItem(
				new WaitCallback(MakeCall), new CallInfo(phone, type));
			return true;
		}

		void MakeCall(object state)
		{
			CallInfo ci = (CallInfo)state;
			for(int i = 0; i < 5; ++i, Thread.Sleep(500))
				try
				{
					if(lnk.MakeCall(ci.phone, ci.type == CallType.Voice))
					{
						OnCall(new CallEventArgs(LinkStatus.OK, ErrorCode.OK, lnk.gMode));
						return;
					}
				}
				catch
				{
					Trace.WriteLineIf(tsComm.TraceError, "Unable to make call", "Comm");
				}
			OnCall(new CallEventArgs(LinkStatus.Error, ErrorCode.Unknown, lnk.gMode));
		}

//		public bool MakeCallEx(string phone)
//		{
//			ThreadPool.QueueUserWorkItem(new WaitCallback(MakeCallEx), phone);
//			return true;
//		}
//
//		private void MakeCallEx(object state)
//		{
//			string phone = "";
//			if (state!=null)
//				phone = (string)state;
//
//			if (phone!="")
//				if (lnk.MakeCallSync(phone))
//				{
//					OnCall(new CallEventArgs(LinkStatus.OK, ErrorCode.OK));
//					return;
//				}
//			OnCall(new CallEventArgs(LinkStatus.Error, ErrorCode.Unknown));
//		}

		public void HangUp()
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(HangUp));
		}

		void HangUp(object state)
		{
			try
			{
				lnk.HangUp();
			}
			catch
			{
				Trace.WriteLineIf(tsComm.TraceError, "Unable to drop call", "Comm");
			}
		}


//		public void Busy()
//		{
//			ThreadPool.QueueUserWorkItem(new WaitCallback(Busy));
//		}

//		void Busy(object state)
//		{
//			try
//			{
//				lnk.Busy();
//			}
//			catch
//			{
//				Trace.WriteLineIf(tsComm.TraceError, "Unable to send busy", "Comm");
//			}
//		}

		void OnCall(CallEventArgs e)
		{
			if(CallEvent != null) CallEvent(this, e);
		}

		private void lnk_CallChange(object sender, CallEventArgs e)
		{
			OnCall(e);
		}

		void OnOffer(OfferingEventArgs e)
		{
			if(OfferingEvent != null) OfferingEvent(this, e);
		}

		private void lnk_Offering(object sender, OfferingEventArgs e)
		{
			OnOffer(e);
		}

		public bool AnswerCall(string phone)
		{
			ThreadPool.QueueUserWorkItem(
				new WaitCallback(AnswerCall), phone != null? phone: "");
			return true;
		}

		void AnswerCall(object state)
		{
			string phone = state.ToString();
			for(int i = 0; i < 5; ++i, Thread.Sleep(500))
				try
				{
					if(lnk.AnswerCall(phone))
						return;
//						OnCall(new CallEventArgs(LinkStatus.CallAnswer, ErrorCode.OK));
//					else
//						OnCall(new CallEventArgs(LinkStatus.CallAnswer, ErrorCode.Unknown));
//					return;
				}
				catch
				{
					Trace.WriteLineIf(tsComm.TraceError, "Unable to answer call", "Comm");
				}
			OnCall(new CallEventArgs(LinkStatus.Error, ErrorCode.Unknown, lnk.gMode));
		}
	}
}