using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using FORIS.TSS.Common;
using FORIS.TSS.Config;
using FORIS.TSS.IO;

namespace FORIS.TSS.Communication
{
	#region Event

	/// <summary>
	/// incoming call delegate
	/// </summary>
	public delegate void OfferingEventHandler(object sender, OfferingEventArgs e);
	/// <summary>
	/// incoming call event arguments
	/// </summary>
	public class OfferingEventArgs : EventArgs
	{
		public AnswerStatus Answer
		{
			get
			{
				return State.Answer;
			}
			set
			{
				State.Answer = value;
			}
		}
		public string Phone
		{
			get
			{
				return State.Phone;
			}
			set
			{
				State.Phone = value != null? value.Trim(): "";
			}
		}
		public readonly OfferState State = new OfferState();
	}
	public class OfferState
	{
		public AnswerStatus Answer;
		public string Phone = "";
	}
	public enum AnswerStatus : sbyte
	{
		Unspecified,
		Answer,
		Refuse,
	}

	/// <summary>
	/// call related delegate
	/// </summary>
	public delegate void CallEventHandler(object sender, CallEventArgs e);
	/// <summary>
	/// call related event arguments
	/// </summary>
	public class CallEventArgs : EventArgs
	{
		public LinkStatus State
		{
			get
			{
				return CallState.State;
			}
		}
		public ErrorCode Error
		{
			get
			{
				return CallState.Error;
			}
		}
		public readonly CallStatus CallState;
		public Mode Mode
		{
			get
			{
				return CallState.Mode;
			}
		}

		public CallEventArgs(LinkStatus stat, ErrorCode err)
		{
			CallState = new CallStatus();
			CallState.State = stat;
			CallState.Error = err;
		}
		public CallEventArgs(LinkStatus stat, ErrorCode err, Mode _mode)
		{
			CallState = new CallStatus();
			CallState.State = stat;
			CallState.Error = err;
			CallState.Mode = _mode;
		}
	}
	public class CallStatus
	{
		public LinkStatus	State;
		public ErrorCode	Error;
		public Mode			Mode;
	}

	/// <summary>
	/// sms received delegate
	/// </summary>
	public delegate void ReceiveSMSEventHandler(object sender, ReceiveSMSEventArgs args);
	/// <summary>
	/// sms received event arguments
	/// </summary>
	public class ReceiveSMSEventArgs : EventArgs
	{
		/// <summary>
		/// sender phone
		/// </summary>
		public readonly string target;
		/// <summary>
		/// message as text
		/// </summary>
		public readonly string text;
		/// <summary>
		/// message as data. no alphabet conversion
		/// </summary>
		public readonly byte[] data;

		public ReceiveSMSEventArgs(string _target, string _text, byte[] _data)
		{
			target = _target;
			text = _text;
			data = _data;
		}
	}

	#endregion Event

	/// <summary>
	/// working with modem using com port
	/// </summary>
	public class CommGateway : Link, IDisposable
	{
		object lockThis = new Object();

		/// <summary>
		/// instance
		/// </summary>
		/// <remarks>must be one per port</remarks>
		static CommGateway cgInst = new CommGateway();

		/// <summary>
		/// ���� ���������� � true, ���� ����� �������� ���������� WaitResponse()
		/// </summary>
		private bool stopWaitingResponse = false;

		/// <summary>
		/// �������� ���������� WaitResponse()
		/// </summary>
		public void StopWaitingResponse()
		{
			stopWaitingResponse = true;
		}

		CommGateway()
		{
			bw = new BinaryWriter(ss);
			sr = new StreamReader(ss);

			// get mode from config
			string startMode = Globals.AppSettings["CommGateway"];
			if(startMode != null && startMode.ToUpper() == "OFF")
			{	// explicit off -> don't start gateway
				Trace.WriteLineIf(tsGW.TraceWarning, "CommGateway not started due to config", 
					tsGW.DisplayName);
				return;
			}
			bHoldPort = startMode == null || startMode.ToUpper() != "SHARED";
			iCheckInterval = bHoldPort? 500: 5000;
			
			string port = Globals.AppSettings["CommPort"];
			if(port != null) 
				sPort = port;

			Trace.WriteLineIf(tsGW.TraceWarning, 
				string.Format("Starting gateway on port {0} in {1} mode", sPort, 
					bHoldPort? "exclusive": "shared"), tsGW.DisplayName);

			thWorker = new Thread(new ThreadStart(Worker));
			thWorker.IsBackground = true;
			thWorker.Name = "CommGateway";
			thWorker.Start();
		}

		~CommGateway()
		{
			Debug.WriteLine("... ~CommGateway()");
			Dispose(false);
		}

		/// <summary>
		/// the one instance
		/// </summary>
		public static CommGateway Inst
		{
			get
			{
				return cgInst;
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		protected virtual void Dispose(bool disposing)
		{
			Debug.WriteLine("... CommGateway.Dispose(true)");
			try
			{
				bStop = true;
				if(disposing && thWorker.IsAlive)
				{
					Debug.WriteLine("... CommGateway.Dispose(true) -> thWorker.Abort()");
					thWorker.Join();
					Debug.WriteLine("... CommGateway.Dispose(true) -> thWorker.Join()");
				}
			}
			catch(Exception exc)	// suppress errors
			{
				Trace.WriteLine(exc.ToString());
			}
			finally
			{
				//StreamClose();
			}
		}


		private Stat GetStatVal(string str)
		{
			if(str == "")
				return Stat.none;
			try
			{
				int res = int.Parse(str);
				Stat ret = Stat.none;
				switch(res)
				{
					case 0: ret = Stat.active; break;
					case 1: ret = Stat.held; break;
					case 2: ret = Stat.dialing; break;
					case 3: ret = Stat.alerting; break;
					case 4: ret = Stat.incoming; break;
					case 5: ret = Stat.waiting; break;
					default : ret = Stat.none; break;
				}
				return ret;
			}
			catch
			{
				return Stat.none;
			}
		}

		#region enum, struct, static
		/// <summary>
		/// ������ ������ (at+clcc)
		/// </summary>
		private enum Stat
		{
			/// <summary>
			/// "0"
			/// </summary>
			active		= 0,
			/// <summary>
			/// "1"
			/// </summary>
			held		= 1,
			/// <summary>
			/// "2"
			/// </summary>
			dialing		= 2,
			/// <summary>
			/// "3"
			/// </summary>
			alerting	= 3,
			/// <summary>
			/// "4"
			/// </summary>
			incoming	= 4,
			/// <summary>
			/// "5"
			/// </summary>
			waiting		= 5,
			/// <summary>
			/// ""
			/// </summary>
			none		= -1
		};

		//��� ��� enum Mode

		/// <summary>
		/// gateway work bit switches
		/// </summary>
		/// <remarks>do not use profiles with other switches</remarks>
		[Flags]
		public enum WorkSwitch : uint
		{
			/// <summary>
			/// allow receive sms
			/// </summary>
			ReceiveSMS		= 0x1,
			/// <summary>
			/// allow offer data call
			/// </summary>
			OfferDataCall	= 0x2,
			/// <summary>
			/// allow auto answer to data offer
			/// </summary>
			AutoAnswerData	= 0x4,
			/// <summary>
			/// allow offer voice call
			/// </summary>
			OfferVoiceCall	= 0x8,
			/// <summary>
			/// allow auto answer to voice offer
			/// </summary>
			AutoAnswerVoice	= 0x10,

			/************************************************************************/
			/* profiles                                                             */
			/************************************************************************/
			/// <summary>
			/// common server profile. receive sms, offer and auto answer data call
			/// </summary>
			Server			= 0x40000000,
			/// <summary>
			/// common client profile. offer voice call
			/// </summary>
			Client			= 0x80000000,
		}

		/// <summary>
		/// message to send by sms
		/// </summary>
		struct Message
		{
			/// <summary>
			/// target phone
			/// </summary>
			public string phone;
			/// <summary>
			/// text message
			/// </summary>
			public string text;
			/// <summary>
			/// byte sequence. for data commands
			/// </summary>
			public byte[] data;

			public Message(string _phone, string _text)
			{
				phone = _phone;
				text = _text;
				data = null;
			}

			public Message(string _phone, byte[] _data)
			{
				phone = _phone;
				data = _data;
				text = null;
			}

			/// <summary>
			/// message isn't ready
			/// </summary>
			public bool Empty
			{
				get
				{
					return phone == null || phone.Length == 0 || 
						(text == null || text.Length == 0) && 
						(data == null || data.Length == 0);
				}
			}
		}

		/************************************************************************/
		/* commands list                                                        */
		/************************************************************************/
		const char cCR = '\r';
		//const char LF = '\n';
		const string sCRLF = "\r\n";
		const string sEcho = "ATE{0}" + sCRLF;
		const string sCall = "ATD{0}{1}" + sCRLF;
		const string sAnswer = "ATA" + sCRLF;
		const string sHoldCall = "ATD2;" + sCRLF;
		const string sTransferCall = "ATD*70#;" + sCRLF;
		const string sSendBusy = "AT+CHLD=0" + sCRLF;	//������� ������ "������"
		const string sDropSecondCall = "AT+CHLD=12" + sCRLF;	//��������� ���������� �� ������ �������
		const string sSelectSMSMem = "AT+CPMS=\"ME\"" + sCRLF;
		const string sSMSArrivalEvt = "AT+CNMI=1,0" + sCRLF;
		//const string ResultFormat = "ATX{0}" + sCRLF;
		const string sDrop = "ATH" + sCRLF;
		const string sListCalls = "AT+CLCC" + sCRLF;
		const string sListMsgs = "AT+CMGL=4" + sCRLF;
		const string sDelMsg = "AT+CMGD={0}" + sCRLF;
		//const string SendMsg = "AT+CMGS={0}" + sCRLF;
		//const string SwitchOff = "AT^SMSO" + sCRLF;
		const string sRestart = "AT+CFUN=1,1" + sCRLF;

		const string sRing = "RING";

		/************************************************************************/
		/* response list                                                        */
		/************************************************************************/
		const string sOK = "OK";
		//const string NoCarrier = "NO CARRIER";
		const string sBusy = "BUSY";
		//const string NoTone = "NO DIALTONE";
		const string sError = "ERROR";
		const string sConnect = "CONNECT";

		#endregion enum, struct, static

		TraceSwitch tsGW = new TraceSwitch("CommPort", "Comm port related info");
		TraceSwitch tsCals = new TraceSwitch("CommPortCalls", "Comm port calls info");

		/// <summary>
		/// hold comm port till stop
		/// </summary>
		readonly bool bHoldPort = true;
		/// <summary>
		/// default com port
		/// </summary>
		readonly string sPort = "COM1";

		private static int DefaultReadWriteCommPortTimeout
		{
			get
			{
				int ret = 5000;
				string st = Globals.AppSettings["DefaultReadWriteCommPortTimeout"];
				if(st!=null)
				{
					try
					{
						ret = int.Parse(st);
					}
					catch{}
				}
				return ret;
			}
		}

		/// <summary>
		/// com port pool interval in Mode.Normal
		/// </summary>
		readonly int iCheckInterval = 5000;
		// classes for operation with com port
		SerialStream ss = new SerialStream(DefaultReadWriteCommPortTimeout);	//was 5000
		BinaryWriter bw;
		StreamReader sr;
		// buf for text responses from port
		const int BUFF_SIZE = 256;
		char[] acResp = new char[BUFF_SIZE];
		/// <summary>
		/// working thread
		/// </summary>
		Thread thWorker;
		/// <summary>
		/// should stop flag
		/// </summary>
		volatile bool bStop;
		/// <summary>
		/// connection status
		/// </summary>
		volatile LinkStatus lsStat = LinkStatus.Closed;
		/// <summary>
		/// current working mode
		/// </summary>
		volatile Mode mode = Mode.Normal;
		/// <summary>
		/// voice or data call. don't have meaning in Mode.Normal
		/// </summary>
		bool bVoice;

		public Mode gMode
		{
			get{return mode;}
		}

		ArrayList alMsg = new ArrayList();

		/// <summary>
		/// allow receive sms
		/// </summary>
		volatile bool bReceiveSMS = false;
		/// <summary>
		/// allow offer data call
		/// </summary>
		volatile bool bOfferDataCall = false;
		/// <summary>
		/// allow auto answer to data offer
		/// </summary>
		volatile bool bAutoAnswerData = false;
		/// <summary>
		/// allow offer voice call
		/// </summary>
		volatile bool bOfferVoiceCall = true;
		/// <summary>
		/// allow auto answer to voice offer
		/// </summary>
		volatile bool bAutoAnswerVoice = false;

		/// <summary>
		/// sms center phone
		/// </summary>
		string SMSC	= "+79168999100";
			// SiemensSMS|+70957699100,5000,L|serial|COM1,57600,8N1,none

		/// <summary>
		/// call related event
		/// </summary>
		public event CallEventHandler CallChange;
		/// <summary>
		/// incoming call event
		/// </summary>
		public event OfferingEventHandler Offering;
		/// <summary>
		/// sms received event
		/// </summary>
		public event ReceiveSMSEventHandler ReceiveSMS;

		#region ILink Members

		public override void Write(string data)
		{
			bw.Write(data.ToCharArray());
		}

		public override void Write(byte[] data)
		{
			bw.Write(data);
		}

		public override bool Valid
		{
			get
			{
				return !ss.Closed;
			}
		}

		public override int Read(char[] data)
		{
			return sr.Read(data, 0, data.Length);
		}

		public override int Read(byte[] data)
		{
			return ss.Read(data);
		}

		#endregion

		/// <summary>
		/// worker thread
		/// </summary>
		/// <remarks>read incoming calls and sms</remarks>
		void Worker()
		{
			Trace.WriteLineIf(tsGW.TraceInfo, "Gateway started", tsGW.DisplayName);
			while(ShouldWork())
			{
				try
				{
					if(InitStream()) DoWork();
					else
					{
						Trace.WriteLineIf(tsGW.TraceError, "Unable to init stream", tsGW.DisplayName);
						ResetModem();
						StreamClose();
					}
				}
				catch(ThreadAbortException)
				{
					break;
				}
				catch(FileNotFoundException ex)
				{
					if(ex.InnerException is Win32Exception && 
						((Win32Exception)ex.InnerException).NativeErrorCode == 31)
					{
						Trace.WriteLineIf(tsGW.TraceError, ex.Message, tsGW.DisplayName);
						OnCall(ErrorCode.NotFound);
						break;
					}
					else
					{
					    StreamClose();
					}
				}
				catch
				{
					StreamClose();
				}
				finally
				{
					if(!bHoldPort) UninitStream();
				}
			}
			StreamClose();
			Trace.WriteLineIf(tsGW.TraceWarning, "Gateway stopped", tsGW.DisplayName);
		}

		/// <summary>
		/// send common init commands
		/// </summary>
		/// <returns>success</returns>
		protected virtual bool InitSequence()
		{
			// send echo off
			SendCommand(sEcho, 0);
			if(!Succeeded())
			{
				Trace.WriteLineIf(tsGW.TraceVerbose, "Unable to init modem", tsGW.DisplayName);
				return false;
			}

			if(mode != Mode.Normal)
			{
//				// set result codes to NO CARRIER, OK, CONNECT. extended error with AT+CEER
//				SendCommand(ResultFormat, 0);
//				if(!Succeeded()) return false;

				return true;
			}

			if(!bReceiveSMS) return true;

			// select SMS memory; ignore answer since tc35 does not have this command 
			// (but s45 does)
			SendCommand(sSelectSMSMem);
			if(!Succeeded()) return false;

			SendCommand(sSMSArrivalEvt);
			if(!Succeeded()) return false;

			return true;
		}

		/// <summary>
		/// prepare and write command to com port
		/// </summary>
		/// <param name="cmd">cmd template</param>
		/// <param name="par">params</param>
		void SendCommand(string cmd, params object[] par)
		{
			cmd = string.Format(cmd, par);
			Trace.WriteLineIf(tsGW.TraceVerbose, "Sending command: " + cmd, tsGW.DisplayName);
			Write(cmd);
		}
		
		/// <summary>
		/// read from stream no more than BUFF_SIZE bytes
		/// </summary>
		/// <returns>data read</returns>
		string ReadResponse()
		{
			try
			{
				StreamOpen();
				string res = new string(acResp, 0, Read(acResp));
				Trace.WriteLineIf(tsGW.TraceVerbose, "Response: " + res, tsGW.DisplayName);
				return res;
			}
			catch
			{
				return "";
			}
		}

		/// <summary>
		/// read from stream till at least one of conditions found
		/// </summary>
		/// <param name="checks">list of condition strings</param>
		/// <returns>data read</returns>
		string ReadResponse(params string[] checks)
		{
			try
			{
				StreamOpen();
				string res;
				StringBuilder sb = new StringBuilder(BUFF_SIZE);
				do 
				{
					res = sb.Append(acResp, 0, Read(acResp)).ToString();
				}
				while(!Succeeded(res, checks));
				Trace.WriteLineIf(tsGW.TraceVerbose, "Response: " + res, tsGW.DisplayName);
				return res;
			}
			catch
			{
				return "";
			}
		}

		/// <summary>
		/// test res for containing of checks
		/// </summary>
		/// <param name="res">string to test</param>
		/// <param name="checks">check conditions</param>
		/// <returns>contain or not</returns>
		bool Succeeded(string res, params string[] checks)
		{
			foreach(string check in checks)
				if(res.IndexOf(check) != -1) return true;
			return false;
		}
		/// <summary>
		/// get response and test for OK
		/// </summary>
		/// <returns>contain or not</returns>
		bool Succeeded()
		{
			return Succeeded(sOK);
		}
		/// <summary>
		/// get response and test for check
		/// </summary>
		/// <param name="check">check to seek in response</param>
		/// <returns>contain or not</returns>
		bool Succeeded(string check)
		{
			return Succeeded(ReadResponse(), check);
		}
//		bool Failed()
//		{
//			Debug.Assert(false);
//			return ReadResponse().IndexOf(NoCarrier) != -1;
//		}

		/// <summary>
		/// make call
		/// </summary>
		/// <param name="phone">target phone</param>
		/// <param name="voice">voice call</param>
		/// <returns>success</returns>
		public bool MakeCall(string phone, bool voice)
		{
			// in use
			if(mode == Mode.Answer || mode == Mode.AnswerHold) Debug.Assert(mode == Mode.AnswerHold);
			else if(lsStat != LinkStatus.Closed && lsStat != LinkStatus.Error) 
				throw new LineBusyException("Line is busy");
			
			lock(lockThis)
			{
				if(mode == Mode.Answer || mode == Mode.AnswerHold) Debug.Assert(mode == Mode.AnswerHold);
				else if(lsStat != LinkStatus.Closed && lsStat != LinkStatus.Error) 
					throw new LineBusyException("Line is busy");
				
				try
				{
					Trace.WriteLineIf(tsGW.TraceWarning, 
						string.Format("Making {0} call {1}", voice? "voice": "data", phone), tsGW.DisplayName);
					// set mode before open for short init
					if(mode == Mode.Answer || mode == Mode.AnswerHold) mode = Mode.AnswerCalling;
					else mode = Mode.Calling;
					bVoice = voice;
					// open but unable to init. no response
					if(!StreamOpen()) throw new ModemException("Unable to init modem", 
						LinkStatus.Error, ErrorCode.NoResponse);
					// dial
					SendCommand(sCall, phone, voice? ";": "");
					return true;
				}
				catch(Exception ex)
				{
					mode = Mode.Normal;																//sv
					lsStat = LinkStatus.Closed;														//sv
					Trace.WriteLineIf(tsGW.TraceError, ex.Message, tsGW.DisplayName);
					StreamClose();
					throw;
				}
			}
		}

		/// <summary>
		/// ������ � ���������� ������
		/// ���������� ������ �� ������ �����, true - ���� ������� ������, false - ���
		/// </summary>
		/// <param name="phone"></param>
		/// <returns></returns>
		public bool MakeCallSync(string phone)
		{
//			if(lsStat != LinkStatus.Closed && lsStat != LinkStatus.Error) 
//				throw new LineBusyException("Line is busy");
			lock(lockThis)
			{
//				if(lsStat != LinkStatus.Closed && lsStat != LinkStatus.Error) 
//					throw new LineBusyException("Line is busy");
				try
				{
					Trace.WriteLineIf(tsCals.TraceWarning, 
						string.Format("MakeCallSync phone: {0}", phone), tsCals.DisplayName);
					
					// close and set modes for sure
					//StreamClose();
					//set mode before open for short init
					//Debug.Assert(mode == Mode.Normal, "mode must be normal");
				
					mode = Mode.Calling;
					// open but unable to init. no response
					if(!StreamOpen()) throw new ModemException("Unable to init modem", 
										  LinkStatus.Error, ErrorCode.NoResponse);

					bool res = false;
					bool outResult = true;

					//���� ��� ����� ������ ������
					if(GetActiveCallsNum() == 0)
					{
						SendCommand(sCall, phone, ";");
						
						res = WaitResponse("OK", out outResult);

						if(outResult == false)
							StreamClose();
					}
					else//�� ����� ������� ���� �� ���� ������
					{
						SendCommand(sCall, phone, ";");
						
						res = WaitForCallAnswer(phone, out outResult);

//						if(outResult == false)
//							StreamClose();
						if(outResult == false)
						{
							SendCommand(sDropSecondCall);
							if(Succeeded())
							{
								Trace.WriteLineIf(tsCals.TraceInfo, "MakeCallSync drop second call succeeded", tsCals.DisplayName);
							}
							else
							{
								Trace.WriteLineIf(tsCals.TraceError, "MakeCallSync drop second call error", tsCals.DisplayName);
								throw new CommunicationException("MakeCallSync drop second call error");
							}
						}
					}

					Trace.WriteLineIf(tsCals.TraceInfo, "MakeCallSync result: "+res.ToString(), tsCals.DisplayName);

//					if(res) mode = Mode.Calling;

					return res;
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsCals.TraceError, ex.Message, tsCals.DisplayName);
					StreamClose();
					throw;
				}
			}
		}


		public class CallInfo
		{
			public string idx = "";
			public string stat = "";
			public string number = "";
			public string type = "";
		}

		/// <summary>
		/// ���������� ��� ���������� ������
		/// </summary>
		/// <param name="phone1">1-� �����</param>
		/// <param name="phone2">2-� �����</param>
		/// <returns>true - ������ ����������, false - ������</returns>
		private bool ComparePhoneNumbers(string phone1, string phone2)
		{
			string ph1;
			string ph2;

			if(phone1.IndexOf("+")>-1)
				ph1 = phone1;
			else
				ph1 = "+7"+phone1.Remove(0,1);

			if(phone2.IndexOf("+")>-1)
				ph2 = phone2;
			else
				ph2 = "+7"+phone2.Remove(0,1);

			return (ph1 == ph2);
		}


		/// <summary>
		/// ���������� ���������� �������� �������
		/// </summary>
		/// <returns></returns>
		private int GetActiveCallsNum()
		{
			int ret = 0;
			lock(lockThis)
			{
				SendCommand(sListCalls);
				string res = ReadResponse();
				if(Succeeded(res, sOK))
				{
					res = res.Replace("\r\n", ";");
					string[] ress = res.Split(';');
					foreach(string s in ress)
					{
						if(s.IndexOf("+CLCC:") > -1)
						{
							string[] cInf = s.Split(',');
							//���� ���� ������ ��������, ��� �� ���������
//							if(cInf[2] == "0" || cInf[2] == "1")
							if(GetStatVal(cInf[2]) == Stat.active || GetStatVal(cInf[2]) == Stat.held)
							{
								ret++;
							}
						}
					}
				}
			}
			Trace.WriteLineIf(tsCals.TraceVerbose, "GetActiveCallsNum result: "+ret.ToString(), tsCals.DisplayName);
			return ret;
		}

		private int GetIncomingCallsNum()
		{
			int ret = 0;
			lock(lockThis)
			{
				SendCommand(sListCalls);
				string res = ReadResponse();
				if(Succeeded(res, sOK))
				{
					res = res.Replace("\r\n", ";");
					string[] ress = res.Split(';');
					foreach(string s in ress)
					{
						if(s.IndexOf("+CLCC:") > -1)
						{
							string[] cInf = s.Split(',');
							//���� ���� �������� ������
//							if(cInf[2] == "4")
							if(GetStatVal(cInf[2]) == Stat.incoming)
							{
								ret++;
							}
						}
					}
				}
			}
			Trace.WriteLineIf(tsCals.TraceVerbose, "GetIncomingCallsNum result: "+ret.ToString(), tsCals.DisplayName);
			return ret;
		}

		/// <summary>
		/// ����������� ������ ������ (�������� ������ ���� �� ����� ��� ������� ���� �������� ������)
		/// </summary>
		/// <param name="phone">���. �����</param>
		/// <returns>������
		/// 0 active
		/// 1 held
		/// 2 dialing (MO call)
		/// 3 alerting (MO call)
		/// 4 incoming (MT call)
		/// 5 waiting (MT call)
		/// </returns>
		private Stat TraceCallStatus(string phone)
		{
//			CallInfo callInfo = new CallInfo();
			lock(lockThis)
			{
				SendCommand(sListCalls);
				Thread.Sleep(100);
				string res = ReadResponse();
				if(Succeeded(res, sOK))
				{
					// [+CLCC: <idx>,<dir>,<stat>,<mode>,<mpty>,[<number>,<type>,[<alpha>]]]
					// [+CLCC: <idx>,<dir>,<stat>,<mode>,<mpty>,[<number>,<type>,[<alpha>]]]
					// [...]]]
					// OK

					/*
						<stat> state of the call (numeric)
							0 active
							1 held
							2 dialing (MO call)
							3 alerting (MO call)
							4 incoming (MT call)
							5 waiting (MT call)
					*/

					res = res.Replace("\r\n", ";");
					string[] ress = res.Split(';');

					/*  example, ress contains:
						[0]	""	string
						[1]	"RING"	string
						[2]	""	string
						[3]	"+CLCC: 1,1,4,0,0,\"89166419322\",129"	string
						[4]	""	string
						[5]	"OK"	string
						[6]	""	string
					*/

					foreach(string s in ress)
					{
						if(s.IndexOf("+CLCC:") > -1)
						{
							string[] callStringInf = s.Split(',');
							/*
							 * -	callStringInf	{Length=7}	string[]
							[0]	"+CLCC: 1"	string				<idx>		+
							[1]	"1"	string						<dir>
							[2]	"4"	string						<stat>		+
							[3]	"0"	string						<mode>
							[4]	"0"	string						<mpty>
							[5]	"\"89166419322\""	string		<number>	+
							[6]	"129"	string					<type>		+
							 * */
//							string tmp;
							// "+CLCC: 1"
							//	|||||||
							//  0123456 - 7 chars to remove
							callStringInf[0] = callStringInf[0].Replace("+CLCC: ","");
							//������� ������ � ��������� ������� (")
							callStringInf[5] = callStringInf[5].Replace("\"", "");

							//���� ������ �����
							if(ComparePhoneNumbers(phone, callStringInf[5]))
							{
								return GetStatVal(callStringInf[2]);
							}

							#region CallInfo
							/*
							
							tmp = callStringInf[0].Remove(0, 7);

							callInfo.idx = tmp;										//<idx>
							callInfo.stat = callStringInf[2];						//<stat>
							
							if(callStringInf[6] == "129")
							{
								tmp = callStringInf[5].Remove(0,2);	//9166419322"
								tmp = tmp.Remove(tmp.Length-1,1);	//9166419322
								callInfo.number = "+7"+tmp;							//<number>
							}
							else if(callStringInf[6] == "145")
							{
								tmp = callStringInf[5].Remove(0,1);
								tmp = tmp.Remove(tmp.Length-1,1)
								callInfo.number = tmp;					//<number>
							}

							callInfo.type = callStringInf[6];						//<type>
						
								//callInfo:
							//idx	"1"	string
							//number	"+7916641932\""	string
							//stat	"4"	string
							//type	"129"	string

							*/
							#endregion
						}
					}
				}
			}
			return Stat.none;
		}


		private Stat TraceSecondCallStatus()
		{
			int i = 0;
			lock(lockThis)
			{
				SendCommand(sListCalls);
				Thread.Sleep(100);
				string res = ReadResponse();
				if(Succeeded(res, sOK))
				{
					res = res.Replace("\r\n", ";");
					string[] ress = res.Split(';');

					foreach(string s in ress)
					{
						if(s.IndexOf("+CLCC:") > -1)
						{
							if(i == 0)
							{
								i++;
								continue;
							}

							string[] callStringInf = s.Split(',');
						
							callStringInf[0] = callStringInf[0].Replace("+CLCC: ","");
							//������� ������ � ��������� ������� (")
							callStringInf[5] = callStringInf[5].Replace("\"", "");

							return GetStatVal(callStringInf[2]);
						}
					}
				}
			}
			return Stat.none;
		}
		/// <summary>
		/// ������� ������-���� ������ �� ���-�����
		/// </summary>
		/// <param name="respCompareTo">����� (�������� "OK")</param>
		/// <param name="outResult">true - ������ ��������� ����������� ����
		/// false - ������ �� �������� ���������� ����� stopWaitingResponse</param>
		/// <returns></returns>
		private bool WaitResponse(string respCompareTo, out bool outResult)
		{
			stopWaitingResponse = false;
			outResult = true;
			bool res = false;

			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitResponse cycle begin", tsCals.DisplayName);
			for(int i=1; i<10; i++)
			{
				if(stopWaitingResponse)
				{
					outResult = false;
					break;
				}
				string resp = ReadResponse();

				Trace.WriteLineIf(tsCals.TraceVerbose, "i="+i+"  responce: \""+resp.Replace("\r\n", "")+"\"", tsCals.DisplayName);

				resp = resp.Replace("\r\n", "");
				if(resp!="")
				{
					if(resp.IndexOf(respCompareTo)>-1)
					{
						res = true;
					}
					else
					{
						res = false;
					}
					break;
				}
				Thread.Sleep(100);
			}

			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitResponse cycle end", tsCals.DisplayName);
			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitResponse result "+res.ToString(), tsCals.DisplayName);
			return res;
		}

		/// <summary>
		/// ������� �������� ������ ���������, �������� ������ ������. 
		/// True - ����� ������, false - ����� ��������
		/// </summary>
		/// <param name="phone">����� �� ������� �������������� ������</param>
		/// <param name="outResult">true - ������ ��������� ����������� ����
		/// false - ������ �� �������� ���������� ����� stopWaitingResponse
		/// ��� ��������� ����������� �� ��������, �� ���������� ������</param>
		/// <returns></returns>
		private bool WaitForCallAnswer(string phone, out bool outResult)
		{
			stopWaitingResponse = false;
			outResult = true;
			bool res = false;
			Stat cellStat = Stat.none;

			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitForCallAnswer cycle begin", tsCals.DisplayName);
			int waitNum = 10;
			for(int i=1; i<=waitNum; i++)
			{
				if(stopWaitingResponse)
				{
					outResult = false;
					break;
				}
				Thread.Sleep(500);

				cellStat = TraceCallStatus(phone);

				Trace.WriteLineIf(tsCals.TraceVerbose, "i="+i+" of "+waitNum+"  status = \""+cellStat+"\"", tsCals.DisplayName);
				/*
						<stat> state of the call (numeric)
							0 active
							1 held
							2 dialing (MO call)
							3 alerting (MO call)
							4 incoming (MT call)
							5 waiting (MT call)
				*/
//				if(cellStat == "0")
				if(cellStat == Stat.active)
				{
					res = true;
					break;
				}
//				else if(cellStat == "")
				else if(cellStat == Stat.none)
				{
					res = false;
					break;
				}
				//����� �� ��������
				if(i == waitNum)
					outResult = false;
			}

			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitForCallAnswer cycle end", tsCals.DisplayName);
			Trace.WriteLineIf(tsCals.TraceVerbose, "WaitForCallAnswer result "+res.ToString(), tsCals.DisplayName);
			return res;
		}

		/// <summary>
		/// interrupt any call related state
		/// </summary>
		public void HangUp()
		{
			lock(lockThis)
			{
//				string res = "";
				try
				{
					Trace.WriteLineIf(tsGW.TraceWarning, "Enter in HangUp()", tsGW.DisplayName);

					if(ss.Closed) 
						return;

//					Trace.WriteLineIf(tsGW.TraceWarning, "Drop", tsGW.DisplayName);

					if(mode == Mode.AnswerCalling)
					{
						Trace.WriteLineIf(tsGW.TraceWarning, "Drop second", tsGW.DisplayName);
						SendCommand(sDropSecondCall);
//						OnCall(ErrorCode.Disconnected, LinkStatus.Open);
						//���������� � DoWork()
					}
					else
					{
						if(lsStat == LinkStatus.Closed)
						{
							mode = Mode.Normal;
							lsStat = LinkStatus.Closed;
							if(mode == Mode.Calling)
							{
								Trace.WriteLineIf(tsGW.TraceWarning, "Drop", tsGW.DisplayName);
								SendCommand(sDrop);
								StreamClose();
								OnCall(ErrorCode.Disconnected, LinkStatus.Closed, mode);
							}
							else
							{
								Trace.WriteLineIf(tsGW.TraceWarning, "Busy", tsGW.DisplayName);
								SendCommand(sSendBusy);
								StreamClose();
							}
						}
						else
						{
							Trace.WriteLineIf(tsGW.TraceWarning, "Drop", tsGW.DisplayName);
							SendCommand(sDrop);
							StreamClose();
							OnCall(ErrorCode.Disconnected, LinkStatus.Closed, mode);
						}
					}
//					res = ReadResponse();

					Trace.WriteLineIf(tsGW.TraceWarning, "Exit from HangUp()", tsGW.DisplayName);
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsGW.TraceError, ex.Message, tsGW.DisplayName);
				}
//				finally
//				{
//					StreamClose();
////					if(res != "") OnCall(ErrorCode.Disconnected);
//				}
			}
		}

		/// <summary>
		/// open serial stream if not already
		/// </summary>
		/// <returns>true - open and inited, false - open and not inited</returns>
		bool StreamOpen()
		{
			bool res = true;
			if(ss.Closed)
			{
				ss.Open(sPort);
				ss.SetTimeouts(500, 0, 0, 0, 5000);
				res = InitSequence();
			}
			if(!ss.Cts) Trace.WriteLineIf(
				tsGW.TraceError, "Modem not ready to exchange data", tsGW.DisplayName);
			if(!ss.Dsr) Trace.WriteLineIf(
				tsGW.TraceError, "Modem not ready to establish a link", tsGW.DisplayName);
			ss.Dtr = ss.Rts = !(ss.Break = false);
			return res;
		}
		
		/// <summary>
		/// close serial stream if not already. set Mode.Normal and LinkStatus.Closed
		/// </summary>
		void StreamClose()
		{
			lock(lockThis)
			{
				try
				{
					Debug.WriteLine("... CommGateway.StreamClose() -> closing serial stream");

					mode = Mode.Normal;
					lsStat = LinkStatus.Closed;
					if (!ss.Closed)
						ss.Close();
				}
				catch(Exception ex)
				{
					Trace.WriteLine("... CommGateway.StreamClose()" + ex.Message);
				}
				finally 
				{
					Trace.WriteLine("... CommGateway.StreamClose() -> finally");
				}
			}
		}

		/// <summary>
		/// state of link (serial stream)
		/// </summary>
		public LinkStatus State
		{
			get
			{
				return lsStat;
			}
		}

		/// <summary>
		/// composite open and init stream
		/// </summary>
		/// <returns></returns>
		bool InitStream()
		{
			Trace.WriteLineIf(tsGW.TraceVerbose, "Init stream", tsGW.DisplayName);
			if(!Monitor.TryEnter(this)) 
				return true;

			try
			{
				return StreamOpen();
			}
			finally
			{
				Monitor.Exit(this);
			}
		}
		/// <summary>
		/// composite uninit and close stream
		/// </summary>
		void UninitStream()
		{
			Trace.WriteLineIf(tsGW.TraceVerbose, "Uninit stream", tsGW.DisplayName);
			if(mode != Mode.Normal) return;
			lock(lockThis)
			{
				if(mode == Mode.Normal) StreamClose();
			}
		}
		
		/// <summary>
		/// read and process data from com port
		/// </summary>
		void DoWork()
		{
			Trace.WriteLineIf(tsGW.TraceVerbose, "Check response", tsGW.DisplayName);
			
			lock(lockThis)
			{
				Trace.WriteLineIf(tsCals.TraceVerbose, "mode: "+mode.ToString()+"  lsStat: "+lsStat.ToString(), "DoWork()");
				// no call on horizon. read/write sms
				if(mode == Mode.Normal)
				{
					Trace.WriteLineIf(tsGW.TraceVerbose, "Normal mode", tsGW.DisplayName);
					// incoming call
					if(Succeeded(sRing))
					{
						if(bOfferDataCall || bOfferVoiceCall) OnOffer();
						else Trace.WriteLineIf(
							tsGW.TraceWarning, "All calls rejected", tsGW.DisplayName);
						return;
					}
					
					if(bReceiveSMS) ReadMessages();
					return;
				}
				// data call. read/write bytes
				if(!bVoice && lsStat == LinkStatus.Open)
				{
					Trace.WriteLineIf(tsGW.TraceVerbose, "Data call", tsGW.DisplayName);
					return;
				}
				// if voice call open
				if(lsStat == LinkStatus.Open)
				{
					if(mode == Mode.AnswerCalling)				//sv
					{
						//����� ����� ��������� ����� ���� ��� ������
						//�.�. ��������� ������ ������� ������
						//���� ������ == Stat.none - �� ������ ������� ��� �� �������
						//���� ������ == Stat.active - �� �� ������ ��������
						Stat trStat = TraceSecondCallStatus();
						if(trStat == Stat.active)
						{
							Trace.WriteLineIf(tsGW.TraceWarning, 
								string.Format("Second {0} call started", bVoice? "Voice": "Data"), 
								tsGW.DisplayName);

							SendCommand(sTransferCall);
							if(Succeeded())
							{
								lsStat = LinkStatus.Closed;
								mode = Mode.Normal;
								StreamClose();

								OnCall(ErrorCode.OK, LinkStatus.OK, mode);
							}
							else
							{
								OnCall(ErrorCode.Disconnected, LinkStatus.Open, mode);
							}
						}
						else if(trStat == Stat.none)
						{
							Trace.WriteLineIf(tsGW.TraceWarning, "Second voice call stopped", tsGW.DisplayName);
							lsStat = LinkStatus.Open;
							mode = Mode.AnswerHold;
							OnCall(ErrorCode.Disconnected, LinkStatus.Open, mode);
						}
					}
					else
					{
						// read
						if(ReadResponse() == "") return;

						Trace.WriteLineIf(tsGW.TraceWarning, "Voice call stopped", 
							tsGW.DisplayName);
						lsStat = LinkStatus.Closed;
						mode = Mode.Normal;
						StreamClose();
						OnCall(ErrorCode.Disconnected, LinkStatus.Closed, mode);
					}
				}
				else // voice call waiting
				{
					string res = ReadResponse();
					if(Succeeded(res, sOK, sConnect)) // connected
					{
						Debug.Assert(mode != Mode.Offering);
						if(mode == Mode.Answer) bVoice = Succeeded(res, sOK);
						else Debug.Assert(bVoice == Succeeded(res, sOK));
						
						Trace.WriteLineIf(tsGW.TraceWarning, 
							string.Format("{0} call started", bVoice? "Voice": "Data"), tsGW.DisplayName);
						lsStat = LinkStatus.Open;
						mode = Mode.Answer;													//sv
//						OnCall(ErrorCode.OK);
//						OnCall(ErrorCode.OK, LinkStatus.CallAnswer);
						OnCall(ErrorCode.OK, LinkStatus.Open, mode);
					}
					else if(mode == Mode.Offering)	// if incoming check if still offering
					{
						if(GetIncomingCallsNum() == 0)
						{
							Trace.WriteLineIf(tsGW.TraceWarning, "Call offer stopped: " + res, tsGW.DisplayName);
							mode = Mode.Normal;
//							OnCall(ErrorCode.OK);
							OnCall(ErrorCode.OK, LinkStatus.Closed, mode);
						}
					}
					else if(res != "")	// error occurred
					{
						Trace.WriteLineIf(tsGW.TraceError, "Unable to conne�t: " + res, tsGW.DisplayName);
						ErrorCode err;
						LinkStatus ls;
						if(Succeeded(res, sBusy)) // line busy
						{
//							lsStat = LinkStatus.Busy;
							ls = LinkStatus.Busy;
							err = ErrorCode.Busy;
						}
						else	// unable to connect
						{
//							lsStat = LinkStatus.Error;
							ls = LinkStatus.Error;
							err = ErrorCode.Unknown;
						}
						lsStat = LinkStatus.Closed;
						mode = Mode.Normal;
						StreamClose();
						
//						OnCall(err);
						OnCall(err, ls, mode);
					}
				}
			}
		}

		bool ShouldWork()
		{
			if(bStop) 
				return false;

			if(mode == Mode.Normal) 
				Thread.Sleep(iCheckInterval);
			else 
				Thread.Sleep(500);
			
			return !bStop;
		}

		/// <summary>
		/// fire call state
		/// </summary>
		void OnCall(ErrorCode err)
		{
			CallChange?.Invoke(this, new CallEventArgs(lsStat, err));
		}

		void OnCall(ErrorCode err, LinkStatus linkStat)
		{
			CallChange?.Invoke(this, new CallEventArgs(linkStat, err));
		}

		void OnCall(ErrorCode err, LinkStatus linkStat, Mode _mode)
		{
			CallChange?.Invoke(this, new CallEventArgs(linkStat, err, _mode));
		}

		void OnOffer()
		{
			Trace.WriteLineIf(tsGW.TraceWarning, "Incoming call", tsGW.DisplayName);

			mode = Mode.Offering;
			if(Offering == null) return;

			string phone = "";
			try
			{
				SendCommand(sListCalls);
				string res = ReadResponse();
				if(Succeeded(res, sOK))
				{
					// [+CLCC: <idx>,<dir>,<stat>,<mode>,<mpty>,[<number>,<type>,[<alpha>]]]
					// [+CLCC: <idx>,<dir>,<stat>,<mode>,<mpty>,[<number>,<type>,[<alpha>]]]
					// [...]]]
					// OK

					/*
					<stat> state of the call (numeric)
						0 active
						1 held
						2 dialing (MO call)
						3 alerting (MO call)
						4 incoming (MT call)
						5 waiting (MT call)
						
					
					*/

					// split calls
					string[] calls = 
						res.Replace(sCRLF + sCRLF, cCR.ToString()).Replace(sCRLF, cCR.ToString()).Split(cCR);
					foreach(string call in calls)
					{
						Trace.WriteLineIf(tsGW.TraceWarning, call, tsGW.DisplayName);
						int i = call.IndexOf("+CLCC:");
						if(i == -1) continue;

						// split call info
						string[] info = call.Substring(i + 6).Split(',');
						// if not incoming call
						if(int.Parse(info[1]) == 0 || int.Parse(info[2]) != 4) continue;

						// is voice
						bVoice = int.Parse(info[3]) == 0;
						// rejected mode
						if(bVoice && !bOfferVoiceCall || !bVoice && !bOfferDataCall)
						{
							Trace.WriteLineIf(tsGW.TraceWarning, (bVoice? "Voice": "Data") + 
								" calls rejected", tsGW.DisplayName);
							return;
						}

						// ~(voice | data)
						Debug.Assert(int.Parse(info[3]) < 2);
						if(int.Parse(info[3]) > 1)
						{
							Trace.WriteLineIf(tsGW.TraceError, "Unsupported mode " + info[3], 
								tsGW.DisplayName);
							break;
						}
						
						if(info.Length == 6) break;

						info[5] = info[5].Replace("\"", "");
						if(int.Parse(info[6]) == 129) phone = "+7" + info[5].Substring(1);
						else phone = info[5];
						break;
					}
				}
			}
			catch
			{
				Trace.WriteLineIf(tsGW.TraceError, "Unable to read call info", tsGW.DisplayName);
			}

			Trace.WriteLineIf(tsGW.TraceWarning, "Notify incoming call", tsGW.DisplayName);
			OfferingEventArgs e = new OfferingEventArgs();
			e.Phone = phone;
			Offering?.Invoke(this, e);
			if(e.Answer == AnswerStatus.Answer) AnswerCall(phone);
			if(e.Answer == AnswerStatus.Refuse)
			{
				Trace.WriteLineIf(tsGW.TraceWarning, "Call refused", tsGW.DisplayName);
				HangUp();
			}
		}

		public bool AnswerCall(string phone)
		{
			// if already answered
			if(mode == Mode.Answer) return true;
			if(mode != Mode.Offering) throw new LineBusyException("Line is busy");

			lock(lockThis)
			{
				if(lsStat != LinkStatus.Closed) throw new LineBusyException("Line is busy");
				try
				{
					Trace.WriteLineIf(tsGW.TraceWarning, 
						string.Format("Answer {0} call {1}", bVoice? "voice": "data", phone), tsGW.DisplayName);
					
					mode = Mode.Answer;
					StreamOpen();
					// dial
					SendCommand(sAnswer);
					return true;
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsGW.TraceError, ex.Message, tsGW.DisplayName);
					StreamClose();
					throw;
				}
			}
		}

		/// <summary>
		/// add message to send list
		/// </summary>
		/// <param name="phone">target phone</param>
		/// <param name="text">text to send</param>
		/// <returns>success</returns>
		public bool SendMessage(string phone, string text)
		{
			if(SMSC == null) throw new CommunicationException("SMS center not specified");

			Message msg = new Message(phone, text);
			if(msg.Empty) return false;

			lock(alMsg.SyncRoot)
			{
				alMsg.Add(msg);
			}
			return true;
		}

		/// <summary>
		/// gateway work mode. combination of WorkSwitch excluding Server and Client
		/// </summary>
		public uint WorkMode
		{
			get
			{
				int workMode = 0;
				if(bReceiveSMS) BitMask.SetValue(ref workMode, (int)WorkSwitch.ReceiveSMS);
				if(bOfferDataCall) 
					BitMask.SetValue(ref workMode, (int)WorkSwitch.OfferDataCall);
				if(bAutoAnswerData) 
					BitMask.SetValue(ref workMode, (int)WorkSwitch.AutoAnswerData);
				if(bOfferVoiceCall) 
					BitMask.SetValue(ref workMode, (int)WorkSwitch.OfferVoiceCall);
				if(bAutoAnswerVoice) 
					BitMask.SetValue(ref workMode, (int)WorkSwitch.AutoAnswerVoice);
				return (uint)workMode;
			}
			set
			{
				int workMode = (int)value;
				if(value == (uint)WorkSwitch.Server)
				{
					bReceiveSMS = BitMask.GetValue(workMode, (int)WorkSwitch.ReceiveSMS) != 0;
					bOfferDataCall = 
						BitMask.GetValue(workMode, (int)WorkSwitch.OfferDataCall) != 0;
					bAutoAnswerData = 
						BitMask.GetValue(workMode, (int)WorkSwitch.AutoAnswerData) != 0;
				}
				if(value == (uint)WorkSwitch.Client) bOfferVoiceCall = 
					BitMask.GetValue(workMode, (int)WorkSwitch.OfferVoiceCall) != 0;
				bReceiveSMS = BitMask.GetValue(workMode, (int)WorkSwitch.ReceiveSMS) != 0;
				bOfferDataCall = 
					BitMask.GetValue(workMode, (int)WorkSwitch.OfferDataCall) != 0;
				bAutoAnswerData = 
					BitMask.GetValue(workMode, (int)WorkSwitch.AutoAnswerData) != 0;
				bOfferVoiceCall = 
					BitMask.GetValue(workMode, (int)WorkSwitch.OfferVoiceCall) != 0;
				bAutoAnswerVoice = 
					BitMask.GetValue(workMode, (int)WorkSwitch.AutoAnswerVoice) != 0;
			}
		}

		/// <summary>
		/// read incoming sms
		/// </summary>
		void ReadMessages()
		{
			// get all messages
			SendCommand(sListMsgs);
			string data = ReadResponse(sOK, sError);
			if(data == "") return;

			// [+CMGL: <index>,<stat>,[<alpha>],<length><CR><LF><pdu><CR><LF>]
			// [+CMGL: <index>,<stat>,[<alpha>],<length><CR><LF><pdu><CR><LF>]
			// [...]
			// OK
			int end = 0;
			do
			{
				int info_beg = data.IndexOf("+CMGL:", end);
				if(info_beg == -1 || end == -1) return;

				info_beg += 6;
				int msgid = Int32.Parse(
					data.Substring(info_beg, data.IndexOf(',', info_beg) - info_beg));
				int beg = data.IndexOf(sCRLF, info_beg) + 2;
				end = data.IndexOf(sCRLF, beg);
				string pdu = data.Substring(beg, end - beg);
				string sender, msg;
				byte[] arr;

				if(0 != PDU.pdu_decode(pdu, out sender, out msg, out arr)) continue;

				OnNotify(sender, msg, arr);

				SendCommand(sDelMsg, msgid);
				if(!Succeeded()) return;
			}
			while(true);
		}

		void OnNotify(string sender, string text, byte[] data)
		{
			ReceiveSMS?.Invoke(this, new ReceiveSMSEventArgs(sender, text, data));
		}

		bool ResetModem()
		{
			try
			{
				Trace.WriteLineIf(tsGW.TraceWarning, "Restarting modem", tsGW.DisplayName);
				SendCommand(sRestart);
				bool res = Succeeded();
				Debug.Assert(res, "Unable to restart modem");
				if(!res) Trace.WriteLineIf(tsGW.TraceError, "Unable to restart modem", tsGW.DisplayName);
				Thread.Sleep(5000);
				ss.Dtr = true;
				return res;
			}
			catch
			{
				throw;
			}
		}
	}

	#region Exception

	/// <summary>
	/// base communication exception
	/// </summary>
	class CommunicationException : ApplicationException
	{
		public CommunicationException(string message) : base(message)
		{
		}
	}

	/// <summary>
	/// modem specific exception
	/// </summary>
	class ModemException : CommunicationException
	{
		public readonly LinkStatus stat = LinkStatus.Error;
		public readonly ErrorCode err = ErrorCode.Unknown;
		public ModemException(string message) : base(message)
		{
		}
		public ModemException(string message, LinkStatus ls, ErrorCode ec) : base(message)
		{
			stat = ls;
			err = ec;
		}
	}

	/// <summary>
	/// phone line in use
	/// </summary>
	class LineBusyException : ModemException
	{
		public LineBusyException(string message) 
			: base(message, LinkStatus.Busy, ErrorCode.Busy)
		{
		}
	}

	#endregion Exception
}