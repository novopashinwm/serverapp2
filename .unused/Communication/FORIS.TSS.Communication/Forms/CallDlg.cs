using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

using FORIS.TSS.Common;
using FORIS.TSS.Communication;
using FORIS.TSS.Multimedia;

namespace FORIS.TSS.Communication.Forms
{
	/// <summary>
	/// base dialog for voice communication
	/// </summary>
	/// <remarks>for more rich functionality create own</remarks>
	public class CallDlg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListView lvInfo;
		private System.Windows.Forms.ColumnHeader colObj;
		private System.Windows.Forms.ColumnHeader colInfo;
		private System.Windows.Forms.TextBox edStat;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btCancel;
		private System.Windows.Forms.Button btAnswer;

		/// <summary>
		/// communication component
		/// </summary>
		FORIS.TSS.Communication.Comm comm;

		/// <summary>
		/// true - auto handle events from comm
		/// </summary>
		bool bHandleEvents;
		CallEventHandler callHandler;
		OfferingEventHandler offerHandler;

//		private Player pl = null;

		/// <summary>
		/// create modeless dialog and Comm with default event handling from Comm
		/// </summary>
		public CallDlg() : this(new Comm(), true)
		{
		}

		/// <summary>
		/// create modeless dialog with default event handling from Comm
		/// </summary>
		public CallDlg(Comm comm) : this(comm, true)
		{
		}

		/// <summary>
		/// create modeless dialog
		/// </summary>
		/// <param name="_comm">communication component</param>
		/// <param name="_handleEvents">true - auto handle events from comm</param>
		public CallDlg(Comm _comm, bool _handleEvents)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//�������������
//			InitPlayer();

			// ���������� � ������� ������, ����� ��������� ��� �������� ��
			// ������� �� ������������� ����� �������
			this.Opacity = 0;
			Show();
			Hide();
			this.Opacity = 100;

			comm = _comm;
			if(!(bHandleEvents = _handleEvents)) return;

			comm.CallEvent += (callHandler = new CallEventHandler(comm_CallEvent));
			comm.OfferingEvent += (offerHandler = new OfferingEventHandler(comm_OfferingEvent));
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			SmartHide();
			if( disposing )
			{
				StopSound();
//				CloseSound();
				if(bHandleEvents)
				{
					comm.CallEvent -= callHandler;
					comm.OfferingEvent -= offerHandler;
				}
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallDlg));
			this.btAnswer = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.lvInfo = new System.Windows.Forms.ListView();
			this.colObj = new System.Windows.Forms.ColumnHeader();
			this.colInfo = new System.Windows.Forms.ColumnHeader();
			this.edStat = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btAnswer
			// 
			this.btAnswer.AccessibleDescription = null;
			this.btAnswer.AccessibleName = null;
			resources.ApplyResources(this.btAnswer, "btAnswer");
			this.btAnswer.BackgroundImage = null;
			this.btAnswer.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btAnswer.Font = null;
			this.btAnswer.Name = "btAnswer";
			this.btAnswer.Click += new System.EventHandler(this.btAnswer_Click);
			// 
			// btCancel
			// 
			this.btCancel.AccessibleDescription = null;
			this.btCancel.AccessibleName = null;
			resources.ApplyResources(this.btCancel, "btCancel");
			this.btCancel.BackgroundImage = null;
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Font = null;
			this.btCancel.Name = "btCancel";
			this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
			// 
			// lvInfo
			// 
			this.lvInfo.AccessibleDescription = null;
			this.lvInfo.AccessibleName = null;
			resources.ApplyResources(this.lvInfo, "lvInfo");
			this.lvInfo.BackgroundImage = null;
			this.lvInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colObj,
            this.colInfo});
			this.lvInfo.Font = null;
			this.lvInfo.FullRowSelect = true;
			this.lvInfo.GridLines = true;
			this.lvInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvInfo.Name = "lvInfo";
			this.lvInfo.TabStop = false;
			this.lvInfo.UseCompatibleStateImageBehavior = false;
			this.lvInfo.View = System.Windows.Forms.View.Details;
			// 
			// colObj
			// 
			resources.ApplyResources(this.colObj, "colObj");
			// 
			// colInfo
			// 
			resources.ApplyResources(this.colInfo, "colInfo");
			// 
			// edStat
			// 
			this.edStat.AccessibleDescription = null;
			this.edStat.AccessibleName = null;
			resources.ApplyResources(this.edStat, "edStat");
			this.edStat.BackgroundImage = null;
			this.edStat.Font = null;
			this.edStat.Name = "edStat";
			this.edStat.ReadOnly = true;
			this.edStat.TabStop = false;
			// 
			// CallDlg
			// 
			this.AcceptButton = this.btAnswer;
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.BackgroundImage = null;
			this.CancelButton = this.btCancel;
			this.Controls.Add(this.edStat);
			this.Controls.Add(this.lvInfo);
			this.Controls.Add(this.btAnswer);
			this.Controls.Add(this.btCancel);
			this.Font = null;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "CallDlg";
			this.TopMost = true;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.CallDlg_Closing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		void comm_CallEvent(object sender, CallEventArgs e)
		{
			SetCallState(e.CallState);
		}
		
		public void SetCallState(CallStatus cs)
		{
			if(InvokeRequired)
			{
				Invoke(new delCallState(SetCallState), new object[]{cs});
				return;
			}

			btAnswer.Enabled = false;
			StopSound();
			switch(cs.State)
			{
			case LinkStatus.Open: edStat.Text = "��������"; break;
			case LinkStatus.Error: 
				edStat.Text = "������"; 
				if(cs.Error == ErrorCode.NoResponse) edStat.Text += ". ���������� �� ��������";
				break;
			case LinkStatus.Closed: 
				edStat.Text = cs.Error == ErrorCode.Disconnected? "���������� ���������": "";
				break;
			case LinkStatus.Busy: edStat.Text = "����� ������"; break;
			default: edStat.Text = ""; break;
			}
		}

		#region media
		
		private void PlaySound()
		{
//			WSounds ws = new WSounds();
			WSounds.Play("Ring.wav");
		}

		private void StopSound()
		{
//			WSounds ws = new WSounds();
			WSounds.StopPlay();
		}

//		private void InitPlayer()
//		{
//			//������� �������������
//			pl = new Player();
//			//��������� ���� �� �������
//			try
//			{
//				pl.Open("Ring.wav");
//			}
//			catch(Exception e)
//			{
//				MessageBox.Show(e.Message);
//			}
//		}
//
//		private void PlaySound()
//		{
//			try
//			{
//				pl.Play(true);
//			}
//			catch(Exception e)
//			{
//				Debug.WriteLine(e.Message);
//			}
//		}
//
//		private void StopSound()
//		{
//			try
//			{
//				pl.Stop();
//			}
//			catch(Exception e)
//			{
//				Debug.WriteLine(e.Message);
//			}
//		}
//		private void CloseSound()
//		{
//			try
//			{
//				if (pl.IsFileOpen)
//					pl.Close();
//			}
//			catch(Exception e)
//			{
//				Debug.WriteLine(e.Message);
//			}
//		}

		#endregion media
		
		void comm_OfferingEvent(object sender, OfferingEventArgs e)
		{
			PARAMS par = new PARAMS(true);
			par["�������"] = e.Phone;
			Offer(par);
		}
		
		/// <summary>
		/// reflect to offer
		/// </summary>
		/// <param name="par">list of strings to show in dlg</param>
		public void Offer(PARAMS par)
		{
			if(InvokeRequired)
			{
				Invoke(new delOffer(Offer), new object[]{par});
				return;
			}
			btAnswer.Enabled = true;
			btAnswer.Select();
			PlaySound();

			edStat.Text = "�������� ������";
			SetInfo(par);
			SmartShow();
		}

		private void btCancel_Click(object sender, System.EventArgs e)
		{
			StopSound();
			SmartHide();
			btAnswer.Enabled = false;
			comm.HangUp();
			edStat.Text = "";
		}

		public void Call(string phone, PARAMS par)
		{
			try
			{
				SetInfo(par);
				SmartShow();
				edStat.Text = "��� ���������� ...";
				if(!comm.MakeCall(phone, Comm.CallType.Voice, true)) SmartHide();
			}
			catch
			{
				SmartHide();
				SetInfo(null);
				throw;
			}
		}

		private void btAnswer_Click(object sender, System.EventArgs e)
		{
			StopSound();
			btAnswer.Enabled = false;
			edStat.Text = "�������� ...";
			comm.AnswerCall(null);
		}

		/// <summary>
		/// set call information in dialog
		/// </summary>
		/// <param name="par">info list</param>
		public void SetInfo(PARAMS par)
		{
			try
			{
				lvInfo.BeginUpdate();
				lvInfo.Items.Clear();
				if(par == null) return;

				IDictionaryEnumerator de = par.GetEnumerator();
				while(de.MoveNext()) 
					lvInfo.Items.Add(new ListViewItem(new string[]{de.Key.ToString(), de.Value.ToString()}));
				foreach(ColumnHeader col in lvInfo.Columns) col.Width = -1;
			}
			catch(Exception ex)
			{
				edStat.Text = ex.Message;
				Debug.WriteLine(ex.Message, "CallDlg");
			}
			finally
			{
				lvInfo.EndUpdate();
			}
		}

		private void CallDlg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			StopSound();
		}
		
		delegate void delCallState(CallStatus cs);
		delegate void delOffer(PARAMS par);
		
		/// <summary>
		/// exclusive lock for visibility for dialog class
		/// </summary>
		static int iHold;
		/// <summary>
		/// lock acquired by this instance
		/// </summary>
		volatile bool bHold;
		/// <summary>
		/// try to acquire and show instance
		/// </summary>
		/// <returns>true - success, false - already locked</returns>
		bool SmartShow()
		{
			if(Visible) return true;
			if(Interlocked.CompareExchange(ref iHold, 1, 0) != 0) return bHold;
		
			Show();
			return bHold = true;
		}
		bool SmartHide()
		{
			if(!Visible) return false;

			int i = Interlocked.CompareExchange(ref iHold, 0, 1);
			Debug.Assert(i == 1, "hiding not acquired window");
			Hide();
			return !(bHold = false);
		}
	}
}