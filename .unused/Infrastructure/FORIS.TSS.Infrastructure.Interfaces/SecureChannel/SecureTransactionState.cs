namespace FORIS.TSS.Infrastructure.Interfaces.SecureChannel
{
	/// <summary>Enumeration of handshake and secure transaction message types.</summary>
	public enum SecureTransactionState
	{
		/// <summary>
		/// An uninitialized transaction; no information is being sent.
		/// </summary>
		/// <remarks>
		/// This is explicitly set to 0 (even though in its current position it
		/// will automatically have the value of 0) to prevent future mistakes should
		/// the order of the items in the enumeration be rearranged.
		/// The system counts on Uninitialized being 0 because Convert.ToInt32() returns
		/// 0 for a null string.  We can then be sure that if Convert.ToInt32() on the
		/// respective header returns Uninitialized, either it was explicitly set
		/// to 0 or no header was set at all.  Regardless, it cuts down on headaches.
		/// </remarks>
		Uninitialized = 0,

		/// <summary>
		/// The client is sending a 
		/// public key to the server.
		/// </summary>
		SendingPublicKey,
		
		/// <summary>
		/// The server is sending an encrypted 
		/// shared key to the client.
		/// </summary>
		SendingSharedKey,
		
		/// <summary>
		/// The client is sending an encrypted 
		/// request to the server.
		/// </summary>
		SendingEncryptedMessage,
		
		/// <summary>
		/// The server is sending an encrypted 
		/// response to the client.
		/// </summary>
		SendingEncryptedResult,
		
		/// <summary>
		/// The server does not recognize 
		/// the client's identification.
		/// </summary>
		UnknownIdentifier
	}
}