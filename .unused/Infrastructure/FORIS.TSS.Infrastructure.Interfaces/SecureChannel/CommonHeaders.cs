namespace FORIS.TSS.Infrastructure.Interfaces.SecureChannel
{
	/// <summary>
	/// Names of transport headers used by client and server
	/// </summary>
	public class CommonHeaders
	{
		/// <summary>
		/// Header to hold the id of the client
		/// </summary>
		public const string ID = "sc_TransactionID";
		
		/// <summary>
		/// Header to hold the SecureTransactionState state
		/// </summary>
		public const string Transaction = "sc_TransactionType";
		
		/// <summary>
		/// Header to hold the RSA public key
		/// </summary>
		public const string PublicKey = "sc_PublicKey";
		
		/// <summary>
		/// Header to hold the encrypted shared key
		/// </summary>
		public const string SharedKey = "sc_SharedKey";
		
		/// <summary>
		/// Header to hold the encrypted IV
		/// </summary>
		public const string SharedIV = "sc_SharedIV";
	}
}