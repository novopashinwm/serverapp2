using System;
using System.Collections;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace FORIS.TSS.Infrastructure.Interfaces.SecureChannel
{
	[Serializable]
	public class SharedKeyRequestMessage: IMethodCallMessage
	{
		private readonly object[] args = new object[] { };
		private readonly object[] inArgs = new object[] { };
		private readonly MethodBase methodBase = 
			typeof(SharedKeyRequestMessage).GetConstructor( new Type[] {} );
		
		private readonly IDictionary properties = new Hashtable( 0 );

		#region IMethodCallMessage Members

		///<summary>
		///Returns the name of the specified argument that is not marked as an out parameter.
		///</summary>
		///
		///<returns>
		///The name of a specific argument that is not marked as an out parameter.
		///</returns>
		///
		///<param name="index">The number of the requested in argument. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public string GetInArgName( int index )
		{
			return string.Empty;
		}

		///<summary>
		///Returns the specified argument that is not marked as an out parameter.
		///</summary>
		///
		///<returns>
		///The requested argument that is not marked as an out parameter.
		///</returns>
		///
		///<param name="argNum">The number of the requested in argument. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public object GetInArg( int argNum )
		{
			return this.inArgs[argNum];
		}

		///<summary>
		///Gets the number of arguments in the call that are not marked as out parameters.
		///</summary>
		///
		///<returns>
		///The number of arguments in the call that are not marked as out parameters.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public int InArgCount
		{
			get { return this.inArgs.Length; }
		}

		///<summary>
		///Gets an array of arguments that are not marked as out parameters.
		///</summary>
		///
		///<returns>
		///An array of arguments that are not marked as out parameters.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public object[] InArgs
		{
			get { return this.inArgs; }
		}

		#endregion

		#region IMethodMessage Members

		///<summary>
		///Gets the name of the argument passed to the method.
		///</summary>
		///
		///<returns>
		///The name of the specified argument passed to the method, or null if the current method is not implemented.
		///</returns>
		///
		///<param name="index">The number of the requested argument. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public string GetArgName( int index )
		{
			return string.Empty;
		}

		///<summary>
		///Gets a specific argument as an <see cref="T:System.Object"></see>.
		///</summary>
		///
		///<returns>
		///The argument passed to the method.
		///</returns>
		///
		///<param name="argNum">The number of the requested argument. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public object GetArg( int argNum )
		{
			return this.args[argNum];
		}

		///<summary>
		///Gets the URI of the specific object that the call is destined for.
		///</summary>
		///
		///<returns>
		///The URI of the remote object that contains the invoked method.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public string Uri
		{
			get { return String.Empty; }
		}

		///<summary>
		///Gets the name of the invoked method.
		///</summary>
		///
		///<returns>
		///The name of the invoked method.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public string MethodName
		{
			get { return String.Empty; }
		}

		///<summary>
		///Gets the full <see cref="T:System.Type"></see> name of the specific object that the call is destined for.
		///</summary>
		///
		///<returns>
		///The full <see cref="T:System.Type"></see> name of the specific object that the call is destined for.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public string TypeName
		{
			get { return typeof(SharedKeyRequestMessage).Name; }
		}

		///<summary>
		///Gets an object containing the method signature.
		///</summary>
		///
		///<returns>
		///An object containing the method signature.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public object MethodSignature
		{
			get { return null; }
		}

		///<summary>
		///Gets the number of arguments passed to the method.
		///</summary>
		///
		///<returns>
		///The number of arguments passed to the method.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public int ArgCount
		{
			get { return this.args.Length; }
		}

		///<summary>
		///Gets an array of arguments passed to the method.
		///</summary>
		///
		///<returns>
		///An <see cref="T:System.Object"></see> array containing the arguments passed to the method.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public object[] Args
		{
			get { return this.args; }
		}

		///<summary>
		///Gets a value indicating whether the message has variable arguments.
		///</summary>
		///
		///<returns>
		///true if the method can accept a variable number of arguments; otherwise, false.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public bool HasVarArgs
		{
			get { return false; }
		}

		///<summary>
		///Gets the <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext"></see> for the current method call.
		///</summary>
		///
		///<returns>
		///Gets the <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext"></see> for the current method call.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public LogicalCallContext LogicalCallContext
		{
			get { return null; }
		}

		///<summary>
		///Gets the <see cref="T:System.Reflection.MethodBase"></see> of the called method.
		///</summary>
		///
		///<returns>
		///The <see cref="T:System.Reflection.MethodBase"></see> of the called method.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public MethodBase MethodBase
		{
			get { return this.methodBase; }
		}

		#endregion

		#region IMessage Members

		///<summary>
		///Gets an <see cref="T:System.Collections.IDictionary"></see> that represents a collection of the message's properties.
		///</summary>
		///
		///<returns>
		///A dictionary that represents a collection of the message's properties.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IDictionary Properties
		{
			get { return this.properties; }
		}

		#endregion
	}
}
