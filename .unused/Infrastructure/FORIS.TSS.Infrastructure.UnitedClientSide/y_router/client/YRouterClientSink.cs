using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Services;

namespace FORIS.TSS.Infrastructure
{
	public class YRouterClientSink : IClientChannelSink
	{
		protected IClientChannelSink nextClient = null;
		public string name;

		public YRouterClientSink(IClientChannelSink nextClient, string name)
		{
			this.nextClient = nextClient;
			this.name = name;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
#if DEBUG
			Diagnostics.RemotingUtilities.DumpMessageObj(msg);
#endif
			requestHeaders["y_router"] = this.name;
			nextClient.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
		}

		public IClientChannelSink NextChannelSink
		{
			get
			{
				return nextClient;
			}
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add YRouterClientSink.AsyncProcessRequest implementation
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add YRouterClientSink.AsyncProcessResponse implementation
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add YRouterClientSink.GetRequestStream implementation
			return null;
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add YRouterClientSink.Properties getter implementation
				return null;
			}
		}
	}

}
