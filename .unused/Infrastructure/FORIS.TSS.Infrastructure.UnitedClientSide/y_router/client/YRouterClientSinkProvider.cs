using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace FORIS.TSS.Infrastructure
{
	public class YRouterClientSinkProvider : IClientChannelSinkProvider
	{
		IClientChannelSinkProvider nextProvider = null;

		protected string name;

		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = nextProvider.CreateSink
				(channel, url, remoteChannelData);

			return new YRouterClientSink(next, name);
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}
		public YRouterClientSinkProvider(IDictionary properties, ICollection providerData)
		{
			// Obtain name from configuration file
			foreach (DictionaryEntry de in properties)
			{
				if (de.Key.Equals("y_router"))
				{
					name = (string)de.Value;
				}
			}
		}
		public YRouterClientSinkProvider(string name)
		{
			this.name = name;
		}
	}
}
