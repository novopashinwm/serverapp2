using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace FORIS.TSS.Infrastructure.Diagnostics
{
	public class ClientCallTrackerSink : IClientChannelSink
	{
		protected IClientChannelSink nextClient = null;
		public ClientCallTrackerSink(IClientChannelSink nextClient)
		{
			this.nextClient = nextClient;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
			RemotingUtilities.DumpMessageObj(msg);
			nextClient.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
		}

		public IClientChannelSink NextChannelSink
		{
			get
			{
				return nextClient;
			}
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientCallTrackerSink.AsyncProcessRequest implementation
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientCallTrackerSink.AsyncProcessResponse implementation
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ClientCallTrackerSink.GetRequestStream implementation
			return null;
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ClientCallTrackerSink.Properties getter implementation
				return null;
			}
		}
	}

}
