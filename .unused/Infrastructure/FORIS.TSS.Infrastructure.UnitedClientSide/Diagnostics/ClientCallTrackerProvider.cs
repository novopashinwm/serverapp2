using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace FORIS.TSS.Infrastructure.Diagnostics
{
	public class ClientCallTrackerProvider : IClientChannelSinkProvider
	{
		IClientChannelSinkProvider nextProvider = null;
		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = nextProvider.CreateSink
				(channel, url, remoteChannelData);

			return new ClientCallTrackerSink(next);
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}
		public ClientCallTrackerProvider(IDictionary properties, ICollection providerData)
		{
		}
	}
}
