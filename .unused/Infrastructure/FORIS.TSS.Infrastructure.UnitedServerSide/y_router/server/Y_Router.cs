﻿using System;
using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure
{
	/// <summary>
	/// Summary description for Y_Router.
	/// </summary>
	public class Y_Router
	{
		protected static Y_Router instance = new Y_Router();
		public static Y_Router Instance
		{
			get
			{
				return instance;
			}
		}

		public Hashtable sinks = new Hashtable();

		public Y_Router()
		{
			Console.WriteLine("Y_Router created");
		}

		public void SetSink(string name, IServerChannelSink sink)
		{
			if (sinks.ContainsKey(name))
			{
				sinks[name] = sink;
			}
			else
			{
				sinks.Add(name, sink);
			}
		}

		public bool IsSinkExist(string type)
		{
			if (sinks.ContainsKey(type))
			{
				return true;
			}
			return false;
		}

		public IServerChannelSink GetSink(string type)
		{
			if (sinks.ContainsKey(type))
			{
				return (IServerChannelSink)sinks[type];
			}
			return null;
		}

		public System.Runtime.Remoting.Channels.ServerProcessing ProcessMessage(
			IServerChannelSinkStack sinkStack, 
			System.Runtime.Remoting.Messaging.IMessage requestMsg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out System.Runtime.Remoting.Messaging.IMessage responseMsg, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
			ServerProcessing myServerProcessing = ServerProcessing.Complete;
			IServerChannelSink sink = GetSink((string)requestHeaders["y_router"]);
			if (sink != null)
			{
				myServerProcessing = sink.ProcessMessage(sinkStack, requestMsg, requestHeaders, requestStream, out responseMsg, out responseHeaders, out responseStream);
			}
			else
			{
				responseMsg = null;
				responseHeaders = null;
				responseStream = null;
			}
			return myServerProcessing;
		}

		/*
		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
			IClientChannelSink sink = GetSink(msg);
			sink.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
		}
		*/
	}
}
