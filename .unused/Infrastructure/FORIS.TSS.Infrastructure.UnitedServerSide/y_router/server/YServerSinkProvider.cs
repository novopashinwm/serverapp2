﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure
{
	public class YServerSinkProvider : IServerChannelSinkProvider
	{
		protected IServerChannelSinkProvider provider = null;

		protected Y_Router router;

		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			IServerChannelSink next = provider.CreateSink(channel);
			return new YServerSink(next, router);
		}

		public IServerChannelSinkProvider Next
		{
			get
			{
				return provider;
			}
			set
			{
				provider = value;
			}
		}

		public YServerSinkProvider(IDictionary properties, ICollection providerData)
		{
			this.router = Y_Router.Instance;
		}
		public YServerSinkProvider(Y_Router router)
		{
			this.router = router;
		}
		public void GetChannelData(IChannelDataStore channelData)
		{
			// TODO:  Add YServerSinkProvider.GetChannelData implementation
		}
	}
}
