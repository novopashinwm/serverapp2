﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure
{
	public class Y2ServerSink : IServerChannelSink
	{
		protected IServerChannelSink nextSink = null;

		protected Y_Router router;

		public Y2ServerSink(IServerChannelSink nextSink, Y_Router router)
		{
			this.nextSink = nextSink;
			this.router = router;
		}

		public System.Runtime.Remoting.Channels.ServerProcessing ProcessMessage(
			IServerChannelSinkStack sinkStack, 
			System.Runtime.Remoting.Messaging.IMessage requestMsg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out System.Runtime.Remoting.Messaging.IMessage responseMsg, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
			responseMsg = null;
			responseHeaders = null;
			responseStream = null;

			sinkStack.Push(this, null);

			ServerProcessing myServerProcessing = ServerProcessing.Complete;


			if (nextSink != null)
			{
				myServerProcessing = nextSink.ProcessMessage(sinkStack, requestMsg, requestHeaders, requestStream, out responseMsg, out responseHeaders, out responseStream);
			}

			switch (myServerProcessing)
			{
				case ServerProcessing.Complete:
				{
					sinkStack.Pop(this);
					break;
				}

				case ServerProcessing.OneWay:
				{
					sinkStack.Pop(this);
					break;
				}

				case ServerProcessing.Async:
				{
					sinkStack.Store(this, null);
					break;
				}
			}
			return myServerProcessing;
		}

		public System.IO.Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add YServerSink.GetResponseStream implementation
			return null;
		}

		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add YServerSink.AsyncProcessResponse implementation
		}

		public IServerChannelSink NextChannelSink
		{
			get
			{
				return nextSink;
			}
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add YServerSink.Properties getter implementation
				return null;
			}
		}
	}
}
