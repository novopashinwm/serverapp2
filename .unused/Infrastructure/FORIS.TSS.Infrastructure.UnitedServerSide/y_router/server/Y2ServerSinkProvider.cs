﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure
{
	public class Y2ServerSinkProvider : IServerChannelSinkProvider
	{
		IServerChannelSinkProvider nextProvider = null;
		protected Y_Router router;
		protected string channelName;
		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			IServerChannelSink next = nextProvider.CreateSink
				(channel);

			IServerChannelSink sink = new Y2ServerSink(next, router);

			router.SetSink(channelName, sink);

			return sink;
		}

		public IServerChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}
		public Y2ServerSinkProvider(IDictionary properties, ICollection providerData)
		{
			this.router = Y_Router.Instance;
			this.channelName = (string)properties["name"];
		}
		public Y2ServerSinkProvider(Y_Router router, string typename)
		{
			this.router = router;
		}
		public void GetChannelData(IChannelDataStore channelData)
		{
			// TODO:  Add YServerSinkProvider.GetChannelData implementation
		}
	}
}
