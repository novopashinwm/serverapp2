using System;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Lifetime;

namespace FORIS.TSS.Infrastructure.Diagnostics
{

    /// <summary>
    ///    RemotingUtilities is a class library containing utilities that can be
    ///    very helpful in Remoting projects. This class contains static methods.
    ///    Some of the methods can be very helpful in dumping various objects
    ///    used in the remoting applications e.g. IMessage, ILease, ObjRef, etc.
    /// </summary>
    public class RemotingUtilities
    {
        public RemotingUtilities()
        {
            //
            // TODO: Add Constructor Logic here
            //
        }

		///<summary>
		///Static function can be used to Dump the information contained in
		///IMessage object.
		///</summary>
		/// <param name="msg">Indicates the IMessage object to be dumped.</param>
		/// <permission cref="System.Security.PermissionSet">
		/// Everyone can access this method.
		/// </permission>
		public static void DumpMessageObj (IMessage msg)
		{
			return;
//			Trace.WriteLine ("Dump of Message Object - Start");
//			Trace.WriteLine ("------------------------------");
//
//			if (null == msg)
//			{
//				Trace.WriteLine ("\tNULL IMessage");
//				return;
//			}
//
//			// Check if the recieved message is og MethodMessage type or not. A method
//			// message is specialized for methods.
//			if (msg is IMethodMessage)
//			{
//				Trace.WriteLine ("\tMessage Type: IMethodMessage");
//			}
//
//			// Chekc if the message recieved is of ReturnMethod type. It represents
//			// the response to a method call on an object at the end of the message
//			// sink.
//			if (msg is IMethodReturnMessage)
//			{
//				Trace.WriteLine ("\tMessage Type: IMethodReturnMessage");
//			}
//
//			// IConstructionReturnMessage interface has not been implemented
//			// yet in Beta1 release.
//			if (msg is IConstructionCallMessage)
//			{
//				Trace.WriteLine ("\tMessage Type: IConstructionCallMessage");
//			}
//
//			// IConstructionReturnMessage interface has not been implemented
//			// yet in Beta1 release.
//			if (msg is IConstructionReturnMessage)
//			{
//				Trace.WriteLine ("\tMessage Type: IConstructionReturnMessage");
//			}
//
//			// Get the message type.
//			Type msgType = msg.GetType ();
//			Trace.WriteLine("\tMessage Type: {0}", msgType.ToString());
//
//			// Get some properties of the message. The properties are returned in
//			// IDictionary interface. Dictionary represents a collection of
//			// addociated keys and values.
//			IDictionary dict = msg.Properties;
//			int nProps = dict.Count;
//			Trace.WriteLine ("\tNumber of properties = " + nProps);
//
//			// Enumerate the contents of the IDictionary interface.
//			IDictionaryEnumerator eDict = dict.GetEnumerator ();
//
//			while (eDict.MoveNext ())
//			{
//				// Get object's key.
//				Object oKey = eDict.Key;
//
//				// Get object's KeyName.
//				String strName = oKey.ToString ();
//
//				// Get value of the key.
//				Object oValue = eDict.Value;
//
//				Trace.WriteLine ("\tDictionary Entry: " + strName + ":" + oValue);
//
//				// Dictionary's key "__Args" represents the array of arguments.
//				if ((oKey as string) == "__Args")
//				{
//					// Get the array of arguments. This array is represented by
//					// Value corresponding to the key.
//					Object [] args = (Object[])oValue;
//
//					// Printout all the arguments for the method.
//					for (int i = 0; i < args.Length ; i++)
//					{
//						Trace.WriteLine ("\t\tArgument: " + i + " Value: " + args[i]);
//						// If the argument is ObjRef, then we can dump its values.
//						if (args[i] is ObjRef)
//						{
//							DumpObjectRef ((ObjRef)args[i]);
//						}
//					}
//				}
//
//				// Dictionary key for output arguments.
//				if ((oKey as string) == "__OutArgs")
//				{
//					// Get the array of arguments. This array is represented by
//					// Value corresponding to the key.
//					Object [] args = (Object[])oValue;
//
//					// Printout all the arguments for the method.
//					for (int i = 0; i < args.Length ; i++)
//					{
//						Trace.WriteLine ("\t\tArgument: " + i + "  Value: " + args[i]);
//						// If the argument is ObjRef, then we can dump its values.
//						if (args[i] is ObjRef)
//						{
//							DumpObjectRef ((ObjRef)args[i]);
//						}
//					}
//				}
//
//				// Dictionary's key "__MethodSignature" is method signature. Its value
//				// corresponds to an array of byte's containing the signature.
//				if ((oKey as string) == "__MethodSignature")
//				{
//					if (oValue != null)
//					{
//						Object [] args = (Object [])oValue;
//						for (int i = 0; i < args.Length ; i++)
//						{
//							Trace.WriteLine ("\t\tMethodSignature: " + i + " Value: " + args[i]);
//						}
//					}
//					else
//					{
//						Trace.WriteLine ("\t\tMethodSignature in null");
//					}
//				}
//
//				if ((oKey as string) == "__URI")
//				{
//					Trace.WriteLine ("\t\tURI: {0}", (String)oValue);
//				}
//
//				if ((oKey as string) == "__MethodName")
//				{
//					Trace.WriteLine ("\t\tMethodName: {0}", (String)oValue);
//				}
//
//				if ((oKey as string) == "__TypeName")
//				{
//					Trace.WriteLine ("\t\tTypeName: {0}", (String)oValue);
//				}
//
//				// The call context provides a set of properties that are carried
//				// with the execution code path. Entries can be added to the Call
//				// Context as it travels down and back up the execution code path. 
//				if ((oKey as string) == "__CallContext")
//				{
//					if (null != oValue)
//					{
//						LogicalCallContext logCtx = (LogicalCallContext)oValue;
//						// Get the call id into the call context.
//						Object callID = logCtx.GetData ("__CallID");
//						if (null != callID)
//						{
//							Trace.WriteLine ("\t\tCall Context's Call ID: {0}", callID.ToString ());
//						}
//					}
//				}
//
//				// CallSiteActivationAttributes is used by IConstructionCallMessage. This
//				// interface and attribute has not been implemented in Beta1 release.
//				if ((oKey as string) == "__CallSiteActivationAttributes")
//				{
//					if (null != oValue)
//					{
//						if (oValue is Object[])
//						{
//							foreach (Object attrObj in (Object [])oValue)
//							{
//								IContextAttribute ctxAttr = (IContextAttribute)attrObj;
//								Trace.WriteLine ("\t\t\tCtx Attribute: " + ctxAttr);
//							}
//						}
//					}
//				}
//
//				if ((oKey as string) == "__ServerType")
//				{
//					if (null != oValue)
//					{
//					}
//				}
//
//				if ((oKey as string) == "__ContextProperties")
//				{
//					if (null != oValue)
//					{
//						ArrayList ctxProps = (ArrayList)oValue;
//						IEnumerator enumColl = (IEnumerator)ctxProps.GetEnumerator ();
//						while (enumColl.MoveNext ())
//						{
//							Trace.WriteLine ("\t\t\tCtx Properties: " + enumColl.Current);
//						}
//					}
//				}
//
//				if ((oKey as string) == "_Activator")
//				{
//					if (null != oValue)
//					{
//					}
//				}
//
//				// When remote object construction is in second stage, construction
//				// return message is created and the OBJREF is placed in the __Return
//				// entry.
//				if ((oKey as string) == "__Return")
//				{
//					if (null != oValue)
//					{
//					}
//				}
//			}
//			Trace.WriteLine ("Dump of Message Object - Finish");
		}

		/// <summary>
		/// Method for dumping ObjRef.
		/// ObjRef defines a marshalled object reference. An ObjRef is a
		/// serializable representation of an object used to transfer an
		/// object reference across an AppDomain boundary.
		/// </summary>
		/// <param name="oRef"> Represent the ObjRef object that will be dumped</param>
		/// <permission cref="System.Security.PermissionSet">
		/// Everyone can access this method.
		/// </permission>
		/// 
		public static void DumpObjectRef (ObjRef oRef)
		{
			Trace.WriteLine ("ObjRef Dump - Start");
			Trace.WriteLine ("-------------------");

			IChannelInfo chnlInfo = oRef.ChannelInfo;
			IEnvoyInfo envoyInfo = oRef.EnvoyInfo;
			IRemotingTypeInfo rmtTypeInfo = oRef.TypeInfo;
//			String uri = oRef.URI;

			Trace.WriteLine ("\t\tObject Type: {0}", rmtTypeInfo.TypeName);

			// Dump the channels...
			Trace.WriteLine ("\tChannel Information");
			Trace.WriteLine ("\t-------------------");

			//String [] chnlNames = chnlInfo.ChannelNames;
			Object [] chnlData = chnlInfo.ChannelData;
			for (int i = 0; i < chnlData.Length; i++)
			{
				//Trace.WriteLine ("\tChannel Name: {0}", chnlNames[i]);
				if (chnlData[i] is System.String[])
				{
					String[] strData = (String[])(chnlData[i]);
						foreach (String chnlStrData in strData)
						{
							Trace.WriteLine ("\t\tChannel Data: {0}", chnlStrData);
						}
				}
				else
				{
					Trace.WriteLine ("\t\tChannel Data : {0}", chnlData[i].ToString ());
				}
			}

			// Dump the Envoy Information.
			Trace.WriteLine ("\tEnvoy Information");
			Trace.WriteLine ("\t-----------------");

			IMessageSink msgSink = envoyInfo.EnvoySinks;
			while (null != msgSink)
			{
				Trace.WriteLine ("\t\t{0}", msgSink.ToString ());
				msgSink = msgSink.NextSink;
			}

			Trace.WriteLine ("ObjRef Dump - Finish");
		}

		/// <summary>
		/// Method for dumping IChannel object information.
		/// </summary>
		/// <param name="obj"> Represent the IChannel object whose information will be dumped.</param>
		/// <permission cref="System.Security.PermissionSet">
		/// Everyone can access this method.
		/// </permission>
		/// 
		public static void DumpChannelObj (IChannel obj)
		{
			Trace.WriteLine ("Dump Channel - Start");
			Trace.WriteLine ("---------------------");

			Trace.WriteLine ("\tChannel Name: " + obj.ChannelName);
			Trace.WriteLine ("\t\tPriority: " + obj.ChannelPriority);

			Trace.WriteLine ("Dump Channel - Finish");
		}

		/// <summary>
		/// Method can be used to dump the information contained in ILease object
		/// interface.
		/// </summary>
		/// <param name="objLease"> Represent ILease object to be dumped.</param>
		/// <permission cref="System.Security.PermissionSet">
		/// Everyone can access this method.
		/// </permission>
		/// 
		public static void DumpLeaseObj (ILease objLease)
		{
			Trace.WriteLine ("Dump Lease - Start");
			Trace.WriteLine ("------------------");

			String strVal = "";

			// Get the initial lease time.
			TimeSpan span = objLease.InitialLeaseTime;
			Trace.WriteLine ("\tCurrent Lease Time: {0}", span.ToString ());
			// Get curent lease time.
			span = objLease.CurrentLeaseTime;
			Trace.WriteLine ("\tCurrent Lease Time: {0}", span.ToString ());
			// Get current lease state.
			LeaseState state = objLease.CurrentState;
			switch (state)
			{
				case LeaseState.Null:
					strVal = "Not Initialized";
					break;
				case LeaseState.Active:
					strVal = "Active";
					break;
				case LeaseState.Expired:
					strVal = "Expired";
					break;
				case LeaseState.Initial:
					strVal = "Initial";
					break;
				case LeaseState.Renewing:
					strVal = "Renewing";
					break;
				default:
					break;
			}
			Trace.WriteLine ("\tCurrent Lease State: {0}", strVal);

			// Get renew time that wil be added when method on object is called.
			span = objLease.RenewOnCallTime;
			Trace.WriteLine ("\tRenewOnCallTime: {0}", span.ToString ());

			// Get sponsorship timeout.
			span = objLease.SponsorshipTimeout;
			Trace.WriteLine ("\tSponsorshipTimeout: {0}", span.ToString ());

			Trace.WriteLine ("Dump Lease - Finish");
		}

		/// <summary>
		/// Method canbe used to dump information contained in an Object interface.
		/// </summary>
		/// <param name="obj"> Represents object to be dumped.</param>
		/// <permission cref="System.Security.PermissionSet">
		/// Everyone can access this method.
		/// </permission>
		/// 
		public static void DumpObject (Object obj)
		{
			Trace.WriteLine ("Dump Object - Start");
			Trace.WriteLine ("-------------------");

			Trace.WriteLine ("\tType: " + obj.GetType ());
			Trace.WriteLine ("\tName: " + obj.ToString ());

			Trace.WriteLine ("Dump Object - Finish");
		}

		public static void DumpChannelSinkProperies (IDictionary dictObj)
		{
			if (null == dictObj)
			{
				return;
			}

			Trace.WriteLine ("Dump ChannelSinkProperties - Start");
			Trace.WriteLine ("----------------------------------");

			int nCount = dictObj.Count;
			Trace.WriteLine("\tNumber of properties: " + nCount);

			// Enumerate the contents of the IDictionary interface.
//			IDictionaryEnumerator eDict = dictObj.GetEnumerator ();
			Trace.WriteLine ("Dump ChannelSinkProperties - Finish");
		}
    }
}
