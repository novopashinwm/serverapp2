﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure.Diagnostics
{
	public class ServerCallTrackerProvider : IServerChannelSinkProvider
	{
		IServerChannelSinkProvider provider = null;
		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			IServerChannelSink next = provider.CreateSink(channel);
			return new ServerCallTrackerSink(next);
		}
		public IServerChannelSinkProvider Next
		{
			get
			{
				return provider;
			}
			set
			{
				provider = value;
			}
		}
		public void GetChannelData(IChannelDataStore channelData)
		{
		}
		public ServerCallTrackerProvider(IDictionary properties, ICollection providerData)
		{
		}
	}
}