﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using FORIS.TSS.TransportDispatcher.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ReportsUnitTest
{
    [TestClass]
    public class ReportParametersSerializationTest
    {
        private const string ConnectionString = @"Data Source=localhost\SQLEXPRESS; Initial Catalog=med_tss_small; Integrated Security=SSPI;";

        [TestMethod]
        public void TestSavedReportParametersSerialization()
        {
            var savedParametersReportsList = GetSavedReportParameters_2012_10_12();
            foreach (var record in savedParametersReportsList)
            {
                if (string.IsNullOrWhiteSpace(record.Value))
                    continue;

                var array = Encoding.Default.GetBytes(record.Value);
                using (var stm = new MemoryStream(array))
                {
                    try
                    {
                        var formatter = new SoapFormatter { AssemblyFormat = FormatterAssemblyStyle.Full };
                        var obj = formatter.Deserialize(stm);
                        var reportParameters = obj as ReportParameters;

                        if (reportParameters != null)
                        {
                            var reportDatesInterval = (reportParameters.DateTimeInterval.DateTo -
                                                  reportParameters.DateTimeInterval.DateFrom);

                            var creationInterval = (reportParameters.DateReport -
                                                    reportParameters.DateTimeInterval.DateTo);

                            if (Math.Abs(creationInterval.TotalSeconds) > 60 * 60 * 24)
                            {
                                var paramsAre =
                                    string.Format(
                                        "qid: {5}, reportDate: {0}, from: {1}, to: {2}, reportInterval: {3}, creationInterval: {4}",
                                        reportParameters.DateReport,
                                        reportParameters.DateTimeInterval.DateFrom,
                                        reportParameters.DateTimeInterval.DateTo,
                                        reportDatesInterval,
                                        creationInterval,
                                        record.Key
                                        );
                                Console.WriteLine(paramsAre);
                            }
                            //var report = ReportsFactory.Instance(Server.Instance(), Repository.Instance).ReportsList[10];
                            //using (var rc = report.Create(reportParameters))
                            //{
                            //    rc.ExportToDisk(ReportTypeEnum.Acrobat, string.Format(@"C:\temp\{0}.pdf", DateTime.Now.Ticks));
                            //}
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exceptional SCHEDULERQUEUE_ID: {0}", record.Key);
                        Console.WriteLine(e.Message);
                    }
                    
                    //Assert.IsNotNull(reportParameters);
                }
            }
        }

        private static Dictionary<int, string> GetSavedReportParameters_2012_10_12()
        {
            var result = new Dictionary<int, string>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                const string cmdText = @"select SCHEDULERQUEUE_ID, CONFIG_XML from SCHEDULERQUEUE_WORK9 where enabled = 1";
                var sqlCommand = new SqlCommand(cmdText, sqlConnection);
                using (var reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                        result.Add((int) reader["SCHEDULERQUEUE_ID"], reader["CONFIG_XML"].ToString());
                }
            }
            return result;
        }
    }
}
