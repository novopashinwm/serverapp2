if exists (
	select *
		from sys.procedures 
		where name = 'Move_Data_To_Controller_Sensor_Log_New')
	drop procedure Move_Data_To_Controller_Sensor_Log_New
go

create procedure Move_Data_To_Controller_Sensor_Log_New
(
	@interval int,
	@max_log_time int,
	@debug bit = null
)
as

set nocount on

if @debug is null
	set @debug = 0

declare @vehicle_sensor table (vehicle_id int, number int, log_time_from int)

insert into @vehicle_sensor
	select v.Vehicle_ID, cs.Number, l.Log_Time
		from vehicle v
		cross apply (
			select distinct cs.Number
				from Controller c
				join Controller_Sensor_Map m on m.Controller_ID = c.Controller_ID
				join Controller_Sensor cs on cs.Controller_Sensor_ID = m.Controller_Sensor_ID
				where c.Vehicle_ID = v.Vehicle_ID
		) cs
		cross apply (
			select top(1) l.Log_Time
				from Controller_Sensor_Log l
				where l.Vehicle_ID = v.VEHICLE_ID
				  and l.Number = cs.NUMBER
				  and l.Log_Time < @max_log_time
				order by l.Log_Time asc) l
				

insert into Controller_Sensor_Log_New (Vehicle_ID, Number, Log_Time, Value)
	select vs.Vehicle_ID
		 , vs.Number
		 , l.Log_Time
		 , l.Value
		from @vehicle_sensor vs
		join Controller_Sensor_Log l on l.Vehicle_ID = vs.vehicle_id
									and l.Number = vs.number
									and l.Log_Time between vs.log_time_from and vs.log_time_from + @interval
									and l.Log_Time < @max_log_time

declare @rows_copied bigint = @@rowcount
									
delete l
	from @vehicle_sensor vs
	join Controller_Sensor_Log l on l.Vehicle_ID = vs.vehicle_id
								and l.Number = vs.number
								and l.Log_Time between vs.log_time_from and vs.log_time_from + @interval
								and l.Log_Time < @max_log_time

declare @rows_deleted bigint = @@rowcount

if (@debug = 1 and (@rows_copied <> 0 or @rows_deleted <> 0))
begin

	print convert(varchar(100), getdate()) + ': ' + convert(varchar(100), @rows_copied) + ' / ' + convert(varchar(100), @rows_deleted)

end

select Copied = @rows_copied, Deleted = @rows_deleted