create table if not exists Geo_Log_Ignored
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Reason smallint not null,
	Lng numeric(8, 5) not null,
	Lat numeric(8, 5) not null,
	Speed smallint null,
	Course smallint null,
	Height smallint null,
	HDOP numeric(4, 2) null,
	InsertTime timestamp without time zone not null constraint DF_Geo_Log_Ignored default(now() at time zone 'utc'),
	
	constraint PK_Geo_Log_Ignored primary key(Vehicle_ID,Log_Time,Reason)
);

create table if not exists IP_Log
(
	Vehicle_ID int not null,
	IP varchar(40) not null,
	FirstTime timestamp without time zone not null,
	LastTime timestamp without time zone not null,
	
	constraint PK_IP_Log primary key(Vehicle_ID)
);

create table IF NOT EXISTS Geo_Log
(
	Vehicle_ID integer not null,
	LOG_TIME integer not null,
	Lng numeric(8, 5) null,
	Lat numeric(8, 5) null,
	
	constraint PK_Geo_Log primary key (Vehicle_ID,LOG_TIME)
);

create table if not exists Statistic_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Odometer bigint null,
	IgnitionOnTime integer null,
	
	constraint PK_Statistic_Log primary key(Vehicle_ID, Log_Time)
);

create table if not exists Statistic_Log_Recalc
(
	Vehicle_ID integer not null,
	Log_Time integer null,
	Pending boolean null,
	Pending_date timestamp without time zone null,
	
	constraint PK_Statistic_Log_Recalc primary key(Vehicle_ID)
);

create table if not exists State_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Type smallint not null,
	Value integer not null,
	
	constraint PK_State_Log primary key(Vehicle_ID,	Log_Time)
);

create table IF NOT EXISTS Controller_Sensor_Log 
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Number integer not null,
	Value bigint not null,
	
	constraint PK_Controller_Sensor_Log primary key (Vehicle_ID, Number, Log_Time)
);

create table if not exists Log_Time
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Media smallint null,
	InsertTime timestamp without time zone null,
	
	constraint PK_Log_Time primary key(Vehicle_ID,	Log_Time)
);

create index IX_Log_Time_Vehicle_ID_InsertTime on Log_Time (Vehicle_ID, InsertTime);

create table if not exists Firmware
(
	ID serial not null,
	Name varchar(32) null,
	constraint PK_Firmware primary key(ID)
);

create table if not exists Position_Radius
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Radius integer null,
	Type smallint not null constraint DF_Position_Radius_Type default(0),
	
	constraint PK_Position_Radius primary key(Vehicle_ID, Log_Time)
);

create table if not exists GPS_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Satellites smallint null,
	Firmware smallint null,
	Altitude integer null,
	Speed smallint null,
	Course smallint null,
	
	constraint PK_GPS_Log primary key (Vehicle_ID,	Log_Time)
);

create table IF NOT EXISTS Cell_Network_Log 
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Number smallint not null,
	Country_Code_ID smallint not null,
	Network_Code_ID smallint not null,
	Cell_ID integer not null,
	LAC integer not null,
	SignalStrength smallint not null,

	constraint PK_Cell_Network_Log primary key (Vehicle_ID, Log_Time, Number)
);

create table if not exists Picture_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Picture bit varying null,
	Camera_Number integer not null constraint DF_Picture_Log_Camera_Number  default(0),
	Photo_Number integer not null constraint DF_Picture_Log_Photo_Number  default(0),
	Url varchar(255) null,
	
	constraint PK_Picture_Log_VID_LT_CN_PN primary key(Vehicle_ID, Log_Time, Camera_Number,	Photo_Number)
);

create table if not exists Sensor_Base_Log
(
	Vehicle_ID integer not null,
	Number integer not null,
	Log_Time integer not null,
	Multiplier integer not null,
	Value bigint null,
	
	constraint PK_Sensor_Base_Log primary key(Vehicle_ID, Number, Log_Time)
); 

create table if not exists Status_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Status integer not null,
	
	constraint PK_Status_Log primary key(Vehicle_ID, Log_Time)
);

create table if not exists WLAN_Log
(
	Vehicle_ID integer not null,
	Log_Time integer not null,
	Number smallint not null,
	WLAN_MAC_Address_ID integer not null,
	SignalStrength smallint not null,
	WLAN_SSID_ID integer not null,
	Channel_Number smallint null,
	
	constraint PK_WLAN_Log primary key(Vehicle_ID,	Log_Time, Number)
);

create table if not exists CAN_INFO
(
	Vehicle_ID integer not null,
	LOG_TIME integer not null,
	CAN_TIME integer not null,
	SPEED smallint null,
	CRUISE_CONTROL boolean null,
	BRAKE boolean null,
	PARKING_BRAKE boolean null,
	CLUTCH boolean null,
	ACCELERATOR smallint null,
	FUEL_RATE integer null,
	FUEL_LEVEL1 smallint null,
	FUEL_LEVEL2 smallint null,
	FUEL_LEVEL3 smallint null,
	FUEL_LEVEL4 smallint null,
	FUEL_LEVEL5 smallint null,
	FUEL_LEVEL6 smallint null,
	REVS smallint null,
	RUN_TO_MNTNC integer null,
	ENG_HOURS integer null,
	COOLANT_T smallint null,
	ENG_OIL_T smallint null,
	FUEL_T smallint null,
	TOTAL_RUN integer null,
	DAY_RUN smallint null,
	AXLE_LOAD1 integer null,
	AXLE_LOAD2 integer null,
	AXLE_LOAD3 integer null,
	AXLE_LOAD4 integer null,
	AXLE_LOAD5 integer null,
	AXLE_LOAD6 integer null,

	constraint PK_CAN_INFO primary key(Vehicle_ID, LOG_TIME)
);
