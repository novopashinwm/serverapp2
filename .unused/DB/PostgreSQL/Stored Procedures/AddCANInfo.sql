create or replace function AddCANInfo
(
	_log_time integer,
	_vehicle_id integer,
	_can_time	integer,				-- UTC time CAN info
	_speed smallint = NULL,
	_cruise_control boolean = NULL,
	_brake boolean = NULL,
	_parking_brake boolean = NULL,
	_clutch boolean = NULL,
	_accelerator smallint = NULL,
	_fuel_rate integer = NULL,
	_fuel_level1 smallint = NULL,
	_fuel_level2 smallint = NULL,
	_fuel_level3 smallint = NULL,
	_fuel_level4 smallint = NULL,
	_fuel_level5 smallint = NULL,
	_fuel_level6 smallint = NULL,
	_revs smallint = NULL,
	_run_to_mntnc integer = NULL,
	_eng_hours integer = NULL,
	_coolant_t smallint = NULL,
	_eng_oil_t smallint = NULL,
	_fuel_t smallint = NULL,
	_total_run integer = NULL,
	_day_run smallint = NULL,
	_axle_load1 integer = NULL,
	_axle_load2 integer = NULL,
	_axle_load3 integer = NULL,
	_axle_load4 integer = NULL,
	_axle_load5 integer = NULL,
	_axle_load6 integer = NULL
) returns integer as $$
declare
	affected integer = 0;
	inserted integer;
	updated integer;
begin

with inserted as
(
	insert into CAN_INFO (vehicle_id, log_time,
		CAN_TIME, SPEED, CRUISE_CONTROL,BRAKE,PARKING_BRAKE,CLUTCH,ACCELERATOR,
		FUEL_RATE,FUEL_LEVEL1,FUEL_LEVEL2,FUEL_LEVEL3,FUEL_LEVEL4,FUEL_LEVEL5,
		FUEL_LEVEL6,REVS,RUN_TO_MNTNC,ENG_HOURS,COOLANT_T,ENG_OIL_T,FUEL_T,
		TOTAL_RUN,DAY_RUN,AXLE_LOAD1,AXLE_LOAD2,AXLE_LOAD3,AXLE_LOAD4,
		AXLE_LOAD5,AXLE_LOAD6)
	select 	_vehicle_id, _log_time,
		_can_time, _speed, _cruise_control, _brake, _parking_brake, _clutch, _accelerator, 
		_fuel_rate, _fuel_level1, _fuel_level2,_fuel_level3,_fuel_level4, _fuel_level5, 
		_fuel_level6, _revs, _run_to_mntnc, _eng_hours, _coolant_t, _eng_oil_t, _fuel_t,
		_total_run, _day_run, _axle_load1, _axle_load2,	_axle_load3, _axle_load4, 
		_axle_load5, _axle_load6
	where not exists 
	(
		select *
		from CAN_INFO
		where vehicle_id = _vehicle_id and
			log_time = _log_time
	)
	returning 1
)
select count(*) into inserted from inserted;
affected := affected + inserted;
if inserted = 0 then
	with updated as
	(
		update CAN_INFO 
		set
			CAN_TIME = _can_time,
			vehicle_id = _vehicle_id, 
			log_time = _log_time,
			SPEED = _speed, 
			CRUISE_CONTROL=_cruise_control,
			BRAKE=_brake,
			PARKING_BRAKE=_parking_brake,
			CLUTCH=_clutch,
			ACCELERATOR=_accelerator,
			FUEL_RATE=_fuel_rate,
			FUEL_LEVEL1=_fuel_level1,
			FUEL_LEVEL2=_fuel_level2,
			FUEL_LEVEL3=_fuel_level3,
			FUEL_LEVEL4=_fuel_level4,
			FUEL_LEVEL5=_fuel_level5,
			FUEL_LEVEL6=_fuel_level6,
			REVS = _revs,
			RUN_TO_MNTNC=_run_to_mntnc,
			ENG_HOURS=_eng_hours,
			COOLANT_T=_coolant_t,
			ENG_OIL_T=_eng_oil_t,
			FUEL_T=_fuel_t,
			TOTAL_RUN=_total_run,
			DAY_RUN=_day_run,
			AXLE_LOAD1=_axle_load1,
			AXLE_LOAD2=_axle_load2,
			AXLE_LOAD3=_axle_load3,
			AXLE_LOAD4=_axle_load4,
			AXLE_LOAD5=_axle_load5,
			AXLE_LOAD6=_axle_load6
		where vehicle_id = _vehicle_id and
			log_time = _log_time
		returning 1
	)
	select count(*) into updated from updated;
	affected := affected + updated;
end if;
return affected;
end;
$$ language plpgsql;
