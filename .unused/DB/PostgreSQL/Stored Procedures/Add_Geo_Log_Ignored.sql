create or replace function Add_Geo_Log_Ignored
(
	_Vehicle_ID integer,
	_Log_Time integer,
	_Reason smallint,
	_Lat numeric(8,5),
	_Lng numeric(8,5), 
	_Speed smallint = null,
	_Course smallint = null,
	_Height smallint = null,
	_HDOP numeric(4,2) = null
)returns void as $$
begin
	insert into Geo_Log_Ignored (Vehicle_ID, Log_Time, Reason, Lng, Lat, Speed, Course, Height, HDOP)
	select _Vehicle_ID, _Log_Time, _Reason, _Lng, _Lat, _Speed, _Course, _Height, _HDOP
	where not exists (
		select *
		from Geo_Log_Ignored e
		where e.Vehicle_ID = _Vehicle_ID 
			and e.Log_Time = _Log_Time 
			and e.Reason = _Reason
	);
end;
$$ language plpgsql;
