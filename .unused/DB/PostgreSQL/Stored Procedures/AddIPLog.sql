create or replace function AddIPLog
(
	_vehicle_id integer,
	_ip varchar(40) = null
) returns void as $$
declare
	now timestamp without time zone := (now() at time zone 'utc');
	cnt integer := 0;
begin
	_ip := coalesce(_ip, ':');
	with ins as 
	(
		insert into ip_log (vehicle_id, ip, firsttime, lasttime)
		select _vehicle_id, _ip, now, now
		where not exists(select * from ip_log where vehicle_id = _vehicle_id)
		returning 1
	)
	select count(*) into cnt from ins;
	if cnt = 0 then
		update IP_Log l
		set IP = _ip,
		    FirstTime = case l.IP when _ip then l.FirstTime else now end,
		    LastTime = now
		where l.Vehicle_ID = _vehicle_id;
	end if;
end;
$$ language plpgsql;
