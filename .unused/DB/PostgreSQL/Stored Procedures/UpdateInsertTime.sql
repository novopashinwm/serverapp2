create or replace function UpdateInsertTime
(
	_vehicle_id integer,
	_log_time integer
) returns void 
as $$
declare
	now timestamp without time zone := (now() at time zone 'utc');
begin
	update Log_Time
	set InsertTime = now
	where vehicle_id = _vehicle_id
	  and log_time   = _log_time;	
end;
$$ language plpgsql;
