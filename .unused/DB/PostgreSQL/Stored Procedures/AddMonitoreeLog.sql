﻿create or replace function AddMonitoreeLog
(
	_uniqueID integer,
	_log_time integer,
	_long numeric(8,5),
	_lat numeric(8,5),
	_media integer,
	_speed smallint = null,
	_course smallint = null,
	_satellites integer = null,
	_hdop numeric(4,2) = null,
	_firmware varchar(32) = null,
	_run integer = null,
	_height smallint = null,
	_validPosition boolean = null,
	_ip varchar(40) = null,
	_debug boolean = true,

	_maxLogAge integer = 180 * 24 * 60 * 60
) returns integer as $$
declare
	compressedCourse smallint;
	firmwareId smallint;
	skipPosition boolean = false;
	currentTimeDelta integer;
	log_time_inserted integer;

	prevlogtime integer;
	prevlng numeric;
	prevlat numeric;
	prevdistance float(53);

	affected integer = 0;
	geo_log_inserted integer;
	geo_log_updated integer;
	gps_log_inserted integer;
	gps_log_updated integer;
begin
	perform AddIpLog(_uniqueID, _ip);
	
	if _speed < -1 then _speed := null; end if;
	if _satellites < 0 or _satellites > 255 then _satellites := null; end if;
	
	compressedCourse := (256 / 360) * (case when _course < 0 then (_course % 360) + 360 else _course % 360 end);
	if _validPosition and 1.1 <= _hdop then
		_validPosition := false;
		perform Add_Geo_Log_Ignored(_uniqueID, _log_time, 0::smallint, _lat, _long, _speed, _course, _height, _hdop);
	end if;

	if _speed > 255 then
		_speed := 255;
	end if;

	if _uniqueID is null or _uniqueID < 1 then
		return -1;
	end if;

	-- all recs newer more than 2 hours are discarded
	if extract(epoch from (select now() at time zone 'utc')) + 7200 < _log_time then
		return -3;
	end if;

	-- записи старше 1970 года игнорируются
	if _log_time < 0 then
		return -4;
	end if;

	if _media < 0 or 255 < _media then 
		_media := 255;
	end if;
	
	if _firmware is not null then
		firmwareId := (select GetOrCreateFirmware(_firmware));
	else
		firmwareId := 0;
	end if;

	--Записываются в БД только те позиции, время которых не старше полугода и не принадлежит будущему более чем на месяц
	currentTimeDelta = _log_time - cast(extract(epoch from (select now() at time zone 'utc')) as int);
	if (currentTimeDelta < -_maxLogAge) or ((30 * 24 * 3600) < currentTimeDelta) then
		if _debug then 
			raise notice 'Position skipped due to time being out of range';
		end if;
	
		skipPosition := true;
		perform Add_Geo_Log_Ignored(_uniqueID, _log_time, 3::smallint, _lat, _long, _speed, _course, _height, _hdop);
	else
		insert into Log_Time(Vehicle_ID, Log_Time, Media, InsertTime)
		select _uniqueID, _log_time, coalesce(_media, 0), null
		where not exists
		(
			select 1 
			from Log_Time
			where Vehicle_ID = _uniqueID
			and Log_Time = _log_time
		);

		select into prevlogtime, prevlng, prevlat
			log_time, lng, lat
		from geo_log
		where vehicle_id = _uniqueID
		order by log_time desc
		limit 1;

		if skipPosition and _validPosition and prevlogtime is not null and prevlng is not null and prevlat is not null 
		then
			prevdistance := (select GetTwoGeoPointsDistance(prevlng, prevlat, _long, _lat));
			if ((prevdistance / 1000) / (_log_time - prevlogtime)::float(53) / 3600) > 800 then
				if not exists 
				(
					select *
					from position_radius
					where vehicle_id = _uniqueID
						and log_time = prevlogtime
						and type <> 0
				) then
					skipPosition := true;
					perform Add_Geo_Log_Ignored(_uniqueID, _log_time, 1::smallint, _lat, _long, _speed, _course, _height, _hdop);
				end if;
			end if;
		end if;
	end if;

	if skipPosition is false and _validPosition then
		with inserted as
		(
			insert into geo_log(vehicle_id, log_time, lng, lat)
			select _uniqueID, _log_time, _long, _lat
			where not exists 
			(
				select *
				from geo_log
				where vehicle_id = _uniqueID and
					log_time = _log_time
			)
			returning 1
		)
		select count(*) into geo_log_inserted from inserted;
		affected := affected + geo_log_inserted;
		if geo_log_inserted = 0 then
			with updated as
			(
				update geo_log 
				set lat = _lat, lng = _long
				where vehicle_id = _uniqueID and
					log_time = _log_time and
					(
						lat <> _lat or lng <> _long
					)
				returning 1
			)
			select count(*) into geo_log_updated from updated;
			affected := affected + geo_log_updated;
		end if;
		
		if firmwareId > 0 or _satellites is not null or _height is not null or _speed is not null or compressedCourse is not null then
			with inserted as
			(
				insert into gps_log (vehicle_id, log_time, satellites, firmware, altitude, speed, course)
				select _uniqueID, _log_time, _satellites, firmwareId, _height, _speed, compressedCourse
				where not exists 
				(
					select *
					from gps_log
					where vehicle_id = _uniqueID and
						log_time = _log_time
				)
				returning 1
			)
			select count(*) into gps_log_inserted from inserted;
			affected := affected + gps_log_inserted;
			if gps_log_inserted = 0 then
				with updated as
				(
					update gps_log 
					set
						satellites=_satellites, 
						firmware=firmwareId,  
						altitude=_height, 
						speed=_speed,
						course=compressedCourse
					where vehicle_id = _uniqueID and
						log_time = _log_time and
						(
							satellites <> _satellites or
							firmware   <> firmwareId or
							altitude   <> _height or 
							speed      <> _speed or
							course     <> compressedCourse
						)
					returning 1
				)
				select count(*) into gps_log_updated from updated;
				affected := affected + gps_log_updated;
			end if;
		end if;
	end if;

	if skipPosition is false and affected > 0 then	
--		perform AddStatisticLog _uniqueID, _log_time, 0;		--Считаем статистику
	end if;


	return 1;
end;
$$ language plpgsql;
