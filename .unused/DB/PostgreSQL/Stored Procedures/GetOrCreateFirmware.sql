create or replace function GetOrCreateFirmware
(
	_name varchar(32)
) returns integer as $$
declare 
	_id integer;
begin
	_id := (select id from Firmware where name = _name);
	if _id is null then
		with ins as
		(
			insert into Firmware (Name)
			select _name
			where not exists (select 1 from Firmware where Name = _name)
			returning id
		)	
		select id into _id from ins;
	end if;
	
	if _id is null then
		_id := (select id from Firmware where name = _name);
	end if;
		
	return _id;
end
$$ language plpgsql;
