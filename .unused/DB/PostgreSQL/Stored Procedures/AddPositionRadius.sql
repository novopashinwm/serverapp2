create or replace function AddPositionRadius
(
	_vehicle_id integer,
	_log_time integer,
	_radius integer,
	_type smallint = 0
) returns integer 
as $$
declare 
	affected int = 0;
	insertedCount int = 0;
	updatedCount int = 0;
begin
	with inserted as 
	(
		insert into position_radius(vehicle_id, log_time, Radius, Type)
		select _vehicle_id, _log_time, _radius, _type
		where not exists 
		(
			select 1
			from position_radius
			where log_time = _log_time and vehicle_id = _vehicle_id
		)
		returning 1
	)
	select count(*) into insertedCount from inserted;
	affected := affected + insertedCount;
	
	if affected = 0 then
		with updated as
		(
			update position_radius
			set radius = _radius, type = _type
			where vehicle_id = _vehicle_id and log_time = _log_time
			returning 1
		)
		select count(*) into updatedCount from updated;
		affected := affected + updatedCount;
	end if;

	return affected;
end;
$$ language plpgsql;