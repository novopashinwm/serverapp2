create or replace function AddSensorValues
(
	_vehicle_id integer,
	_log_time integer,
	_sensorValues varchar[][]
) returns integer
as $$
declare
	affected integer = 0;
	updatedCount integer;
	insertedCount integer;
begin
	drop table if exists values;
	create temp table values
	as 
	with data as (select replace(replace(unnest(_sensorValues),'[','{'),']','}')::varchar[] as sensor)
	select sensor[1]::integer as number, sensor[2]::bigint as value
	from data;

	-- TODO: ������� �������������������� ��������
	
	delete 
	from values v 
	using controller_sensor_log l 
	where l.vehicle_id = _vehicle_id and 
		l.log_time = _log_time	and 
		l.number = v.number and 
		l.value = v.value;

		
	with updated as
	(
		update controller_sensor_log l
		set value = v.value
		from values v
		where l.vehicle_id = _vehicle_id 
		  and l.log_time = _log_time
		  and l.number = v.number
		returning 1
	)
	select count(*) into updatedCount from updated;
	affected := affected + updatedCount;

	with inserted as
	(
		insert into controller_sensor_log(Vehicle_ID,Log_Time,Number,Value)
		select _vehicle_id, _log_time,v.number, v.value
		from values as v
		where not exists
		(
			select 0 
			from controller_sensor_log
			where vehicle_id = _vehicle_id 
				and log_time = _log_time 
				and Number = v.number
		)
		returning 1
	)
	select count(*) into insertedCount from inserted;
	affected := affected + insertedCount;

	drop table if exists values;
	
	return affected;
end;
$$ language plpgsql;