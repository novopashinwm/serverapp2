create or replace function GetTwoGeoPointsDistance
(
	x1 real,
	y1 real,
	x2 real,
	y2 real
) returns float(53)
as $$
declare 
	lat1 float(53);
	lon1 float(53);
	lat2 float(53);
	lon2 float(53);
	a float(53);
	cos float(53);
	acos float(53);
begin
	if x1 = x2 and y1 = y2 then
		return 0;
	end if;

	lat1 := y1 * pi() / 180;
	lon1 := x1 * pi() / 180;
	lat2 := y2 * pi() / 180;
	lon2 := x2 * pi() / 180;

	a := lon1 - lon2;
	if a < 0.0 then a := -a; end if;
	if a > pi() then a := 2.0 * pi() - a; end if;

	cos := sin(lat2) * sin(lat1) + cos(lat2) * cos(lat1) * cos(a);
	acos := case 
			when cos >= 1 then 0
			when cos <= -1 then pi()
			else acos(cos)
		end;

	return 40075160 * acos / (2.0 * pi());
end;
$$ language plpgsql;
