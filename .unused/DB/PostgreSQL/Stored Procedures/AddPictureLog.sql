create or replace function AddPictureLog
(
	_vehicle_id integer,
	_log_time integer,
	_camera_number integer,
	_photo_number integer,
	_picture bit varying = null,
	_url varchar(255) = null
) returns integer as $$
declare
	affected integer = 0;
	inserted integer;
	updated integer;
begin

with inserted as
(
	insert into Picture_Log (Vehicle_ID, Log_Time, Camera_Number, Photo_Number, Picture, Url)
	select _vehicle_id, _log_time, _camera_number, _photo_number, _picture, _url
	where not exists 
	(
		select *
		from Picture_Log
		where vehicle_id = _vehicle_id and
			log_time = _log_time
	)
	returning 1
)
select count(*) into inserted from inserted;
affected := affected + inserted;
if inserted = 0 then
	with updated as
	(
		update Picture_Log 
		set Picture = _picture, Url = _url
		where Vehicle_ID = _vehicle_id
			and Log_Time = _log_time
			and Camera_Number = _camera_number
			and Photo_Number = _photo_number
		returning 1
	)
	select count(*) into updated from updated;
	affected := affected + updated;
end if;
return affected;
end;
$$ language plpgsql;
