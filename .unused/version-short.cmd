cd c:\work\good4\ServerApp2\

cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" TerminalService\TerminalService.sln && C:\Work\Tools\CopyFiles.exe TerminalService\server\Release ..\Versions\LatestVersion\services\Terminal dll;pdb;exe || pause

cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" TransportDispatcher\TransportDispatcher.sln && C:\Work\Tools\CopyFiles.exe TransportDispatcher\server\Release ..\Versions\LatestVersion\services\Business dll;pdb;xsl;exe || pause

rem Только файлы библиотек WebMap
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" WebMapG\WebMapG.sln && C:\Work\Tools\CopyFiles.exe WebMapG\bin ..\Versions\LatestVersion\Web\WebMap\bin

rem Копирование SQL
C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Changes" ..\Versions\LatestVersion\SQL\Changes sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Meta" ..\Versions\LatestVersion\SQL\Meta sql&& C:\Work\Tools\CopyFiles.exe "DB\MSSQL\SP" ..\Versions\LatestVersion\SQL\SP sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Changes" ..\Versions\LatestVersion\SQL\Changes sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\RefBooks" ..\Versions\LatestVersion\SQL\RefBooks sql || pause

rem Подготовка архива
"C:\Program Files\7-Zip\7z.exe" a -r -tzip -mx3 ..\versions\LatestVersion.zip ..\Versions\LatestVersion\* && C:\Work\Tools\SetFileNameAsDateTime.exe ..\Versions\LatestVersion.zip && rd ..\Versions\LatestVersion /S /Q || pause

goto end

:error
echo Unsuccessfull

:end