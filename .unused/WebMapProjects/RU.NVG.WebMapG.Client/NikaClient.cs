﻿using System;
using System.Collections.Specialized;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using FORIS.TSS.Helpers;
using Microsoft.AspNet.SignalR.Client;
using RU.NVG.UI.WebMapG.Model.Common;
using RU.NVG.UI.WebMapG.Model.Services.Messages;

namespace RU.NVG.WebMapG.Client
{
	public class NikaClientSettings
	{
		public string SimId;

		public string Login;

		public string Password;
	}

	public class NikaClient
	{
		private readonly WebClientWithCookies _clientWithCookies;

		private readonly string _simId;

		private readonly Uri _url;

		private IHubProxy _proxy;

		public bool IsConnected { get; private set; }

		public NikaClient(Uri url, NikaClientSettings settings)
		{
			_url = url;
			_simId = settings.SimId;
			_clientWithCookies = new WebClientWithCookies { BaseAddress = url.AbsoluteUri };
		}

		public async Task ConnectAsync()
		{
			var @params = new NameValueCollection
			{
				{"a", "sign_in"},
				{"sim_id", _simId},
				{"appId", Guid.NewGuid().ToString("N")},
				{"gcmRegistrationId", Guid.NewGuid().ToString("N")},
				{"name", "Nika client"}
			};
			var bytes = await _clientWithCookies.UploadValuesTaskAsync("login.aspx", @params);
			var json = Encoding.ASCII.GetString(bytes);
			var result = (SignInResult) JsonHelper.DeserializeObjectFromJson(typeof (SignInResult), json);
			if(result.Result != LoginResult.Success)
				throw new SecurityException("auth not valid");
			IsConnected = true;
		}

		protected async Task<IHubProxy> ConnectHubAsync()
		{
			var token = await GetTokenAsync();
			var uri = new Uri(_url, "signalr");
			var hubConnection = new HubConnection(uri.AbsoluteUri, "UserToken=" + token);
			var proxy = hubConnection.CreateHubProxy("MessagesHub");
			await hubConnection.Start();
			return proxy;
		}

		protected async Task<string> GetTokenAsync()
		{
			if (!IsConnected)
				throw new SecurityException("not connected to hub");

			_clientWithCookies.QueryString.Add("a", "get-token");
			var json = await _clientWithCookies.DownloadStringTaskAsync("services/messages.aspx");
			var result = (GetTokenResponse)JsonHelper.DeserializeObjectFromJson(typeof(GetTokenResponse), json);
			return result.Token;
		}
	}
}