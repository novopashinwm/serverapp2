﻿using System;
using System.Net;

namespace RU.NVG.WebMapG.Client
{
    public class WebClientWithCookies : WebClient
    {
        private readonly CookieContainer _cookieContainer;

        public WebClientWithCookies(CookieContainer cookieContainer = null)
        {
            _cookieContainer = cookieContainer ?? new CookieContainer();
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);
            var httpRequest = request as HttpWebRequest;
            if (httpRequest == null)
                return request;
            httpRequest.CookieContainer = _cookieContainer;
            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = base.GetWebResponse(request);
            var httpResponse = response as HttpWebResponse;
            if (httpResponse == null)
                return response;

            _cookieContainer.Add(httpResponse.Cookies);
            return response;
        }
    }
}
