using System.Collections.Specialized;
using FORIS.TSS.WorkplaceSharnier;
using Interfaces.Web;

namespace FORIS.TSS.WebClient
{
	public class WebClient :
		ClientBase<IWebPersonalServer>
	{
		public WebClient( NameValueCollection parameters )
			: base( parameters )
		{

		}

		protected override void Start()
		{
		}

		protected override void Stop()
		{
		}

	    protected override void ReConnect()
	    {
	        throw new System.NotImplementedException();
	    }
	}
}