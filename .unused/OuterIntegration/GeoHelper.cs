﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.Remoting;
using FORIS.TSS.WorkplaceShadow.Geo;
using Interfaces.Geo;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	public static class GeoHelper
	{
		static GeoHelper()
		{
			RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);
			ConnectGeoClient();
		}
		public static GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng)
		{
			if (GeoClient == null)
			{
				lat = null;
				lng = null;
				return GetLatLngByAddressResult.FailedGeocoderTooBusy;
			}
			return GeoClient.GetLatLngByAddress(address, lat: out lat, lng: out lng);
		}
		#region Geo_Client
		readonly static object mapLockObject = new object();
		private static GeoClient m_geoClient;
		public static GeoClient GeoClient
		{
			get
			{
				if (m_geoClient == null)
					ConnectGeoClient();
				return m_geoClient;
			}
		}
		private static void ConnectGeoClient()
		{
			lock (mapLockObject)
			{
				try
				{
					string geoClientURL = ConfigurationManager.AppSettings["MapServer"];

					if (string.IsNullOrWhiteSpace(geoClientURL))
					{
						Trace.TraceWarning("MapServer url is not set");
						return;
					}

					m_geoClient = new GeoClient();
					m_geoClient.NeedReConnect += OnNeedReConnect;
					m_geoClient.Connect(geoClientURL, false);
				}
				catch (Exception ex)
				{
					Trace.WriteLine("Ошибка подключения к серверу карты");
					Trace.WriteLine("url: " + ConfigurationManager.AppSettings["MapServer"]);
					Trace.WriteLine(ex.ToString());
					if (m_geoClient != null)
					{
						m_geoClient.Dispose();
						m_geoClient = null;
					}
				}
			}
		}
		private static void OnNeedReConnect(object sender, EventArgs e)
		{
			DisconnectGeoClient();
			ConnectGeoClient();
		}
		private static void DisconnectGeoClient()
		{
			lock (mapLockObject)
			{
				if (m_geoClient != null)
				{
					m_geoClient.Dispose();
					m_geoClient = null;
				}
			}
		}
		#endregion Geo_Client
	}
}