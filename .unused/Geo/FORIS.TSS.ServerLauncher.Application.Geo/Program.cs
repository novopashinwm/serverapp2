﻿using System;

namespace FORIS.TSS.ServerLauncher.Application.Geo
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			/// Приложение сервера конфигурируется при старте домена приложения сервера
			TrayIconApplicationContext.RunInteractive("Geo Server", true, true, true);
		}
	}
}