using System;
using System.Drawing;

namespace Interfaces.Geo
{
    /// <summary>
    /// ���������� ��������� ������� ����� ��� WEB
    /// </summary>
    [Serializable]
    public struct MapObject
    {
        /// <summary>
        /// ���������� ������������� �������
        /// </summary>
        //public readonly int cod;

        public readonly string name;

        //public readonly MapObjectType type;

        /// <summary>
        /// ����������� ����� �� ������� (�� ���� �� )
        /// </summary>
        public readonly PointF pointCenter;

        public readonly PointF pointExtMin; 
        public readonly PointF pointExtMax; 

        //public readonly List<PointF> points;
        //public readonly PointF[] points;

        public MapObject(string name, PointF pointExtMin, PointF pointExtMax, PointF pointCenter)
        {
            //this.cod = cod;
            this.name = name;
            //this.type = mapObjectType;

            this.pointCenter = pointCenter;
            this.pointExtMin = pointExtMin;
            this.pointExtMax = pointExtMax;

            //this.points = points;
        }
    }
}