cd c:\work\good4\ServerApp2\

cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" TerminalService\TerminalService.sln && C:\Work\Tools\CopyFiles.exe TerminalService\server\Release ..\Versions\LatestVersion\services\Terminal dll;pdb;exe || pause

cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" DiagnosticService\FORIS.TSS.DiagnosticService\FORIS.TSS.DiagnosticService.sln && C:\Work\Tools\CopyFiles.exe DiagnosticService\Server\Release\ ..\Versions\LatestVersion\services\DiagnosticService dll;pdb;exe || pause

cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" Geo\Geo.sln && C:\Work\Tools\CopyFiles.exe geo\server\Release ..\Versions\LatestVersion\services\geo dll;pdb;exe || pause

rem Фоновый сервер
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" TransportDispatcher\TransportDispatcher.sln && C:\Work\Tools\CopyFiles.exe TransportDispatcher\server\Release ..\Versions\LatestVersion\services\Business dll;pdb;xsl;exe || pause

rem SOAP API для внешних систем
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" OuterIntegration\OuterIntegration.sln /p:Configuration=Release;DeployOnBuild=True;PackageAsSingleFile=False /p:outdir=c:\work\Temp\WebPublish\ && C:\Work\Tools\CopyFiles.exe c:\work\Temp\WebPublish\_PublishedWebsites\FORIS.TSS.WebServices.OuterIntegration\ ..\Versions\LatestVersion\Web\OuterIntegration || pause

rem Только файлы библиотек WebMap
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:Configuration=Release /verbosity:normal /p:NoWarn="1591;1573" WebMapG\WebMapG.sln && C:\Work\Tools\CopyFiles.exe WebMapG\bin ..\Versions\LatestVersion\Web\WebMap\bin || pause

rem Только WebMap
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:NoWarn="1591;1573" c:\Work\good4\ServerApp2\webmapg\webmapg.sln /p:Configuration=Release;DeployOnBuild=True;PackageAsSingleFile=False /p:outdir=c:\work\Temp\WebPublish\ && C:\Work\Tools\CopyFiles.exe c:\work\Temp\WebPublish\_PublishedWebsites\FORIS.TSS.UI.WebMapG\ ..\Versions\LatestVersion\Web\WebMap && rd c:\work\Temp\WebPublish /S /Q || pause

rem Публикация сайта WebMap 
cls && "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe" /m:2 /p:NoWarn="1591;1573" c:\Work\good4\ServerApp2\webmapg\webmapg.sln /p:Configuration=Release;DeployOnBuild=True;PackageAsSingleFile=False /p:outdir=c:\work\Temp\WebPublish\ && C:\Work\Tools\CopyFiles.exe c:\work\Temp\WebPublish\_PublishedWebsites\FORIS.TSS.UI.WebMapG\ ..\Versions\LatestVersion\Web\WebMap && rd c:\work\Temp\WebPublish /S /Q || pause
cls && cd ..\WebApp\includes\nodeutils\ && grunt release && cd ..\..\..\ServerApp\ || pause
C:\Work\Tools\CopyFiles.exe ..\WebApp\includes\production\ ..\Versions\LatestVersion\Web\WebMap\includes\production\ && C:\Work\Tools\CopyFiles.exe ..\WebApp\includes\resources\ ..\Versions\LatestVersion\Web\WebMap\includes\resources\ || pause

rem Копирование статических материалов
cls && C:\Work\Tools\CopyFiles.exe "..\Static\includes" ..\Versions\LatestVersion\Web\Static\includes

rem Копирование SQL
C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Changes" ..\Versions\LatestVersion\SQL\Changes sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Meta" ..\Versions\LatestVersion\SQL\Meta sql&& C:\Work\Tools\CopyFiles.exe "DB\MSSQL\SP" ..\Versions\LatestVersion\SQL\SP sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\Changes" ..\Versions\LatestVersion\SQL\Changes sql && C:\Work\Tools\CopyFiles.exe "DB\MSSQL\RefBooks" ..\Versions\LatestVersion\SQL\RefBooks sql || pause

rem Подготовка архива
"C:\Program Files\7-Zip\7z.exe" a -r -tzip -mx3 ..\versions\LatestVersion.zip ..\Versions\LatestVersion\* && C:\Work\Tools\SetFileNameAsDateTime.exe ..\Versions\LatestVersion.zip && rd ..\Versions\LatestVersion /S /Q || pause

