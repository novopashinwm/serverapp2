using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TerminalService.Interfaces
{
    public interface IDeviceInfo
    {
        /// <summary>
        /// ID ����������� �� ��
        /// </summary>
        int ControllerID { get; set; }
        /// <summary>
        /// IP ����� ����������
        /// </summary>
        IPEndPoint EndPoint { get; set;}
        /// <summary>
        /// ���������� ����� ����������
        /// </summary>
        string PhoneNumber { get; set; }

        /// <summary>
        /// ����� ����������� ����������� ��� ����� �������������
        /// </summary>
        int DeviceNumber { get; set; }

        /// <summary>
        /// IMEI ����������
        /// </summary>
        Int64 Imei { get; set;}

        /// <summary>
        /// ��� �����������
        /// </summary>
        string ControllerType { get; set;}
    }
}
