using System;

namespace TerminalService.Interfaces
{
	/// <summary>
	/// ������� �������� �������
	/// </summary>
	[Flags]
	public enum GuardEvents
	{
		/// <summary>
		/// ������� �����������
		/// </summary>
		None = 0,
		/// <summary>
		/// ������ SOS
		/// </summary>
		SOS = 0x1,
		/// <summary>
		/// ������������ ������� ������
		/// </summary>
		Cowling = 0x2,
		/// <summary>
		/// ������������ ������� ������
		/// </summary>
		Doors = 0x4,
		/// <summary>
		/// ���������� �� ������
		/// </summary>
		Guard =	0x8,
		/// <summary>
		/// ��������� ������������
		/// </summary>
		Signalling = 0x10
	}

	[Serializable]
	public struct DPoint
	{
		public double x;
		public double y;

		public DPoint(double x, double y)
		{
			this.x = x;
			this.y = y;
		}
	}

	[Serializable]
	public struct FPoint
	{
		public float x;
		public float y;

		public FPoint(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
	}

	[Serializable]
	public struct IPoint
	{
		public int x;
		public int y;

		public IPoint(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}

	[Serializable]
	public struct DRect
	{
		public double x1;
		public double x2;
		public double y1;
		public double y2;

		public DRect(double x1, double x2, double y1, double y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public DPoint Center() 
		{
			DPoint local0;

			local0 = new DPoint(this.x1 + this.x2 / 2, this.y1 + this.y2 / 2);
			return local0;
		}
		
		public double Height() 
		{
			double local0;

			local0 = this.y2 - this.y1;
			return local0;
		}

		public double Width() 
		{
			double local0;

			local0 = this.x2 - this.x1;
			return local0;
		}
	}

	[Serializable]
	public struct IRect
	{
		public int x1;
		public int x2;
		public int y1;
		public int y2;

		public IRect(int x1, int x2, int y1, int y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public IPoint Center() 
		{
			IPoint local0;

			local0 = new IPoint(this.x1 + this.x2 / 2, this.y1 + this.y2 / 2);
			return local0;
		}
		
		public int Height() 
		{
			int local0;

			local0 = this.y2 - this.y1;
			return local0;
		}

		public int Width() 
		{
			int local0;

			local0 = this.x2 - this.x1;
			return local0;
		}
	}

	[Serializable]
	public struct FRect
	{
		public float x1;
		public float x2;
		public float y1;
		public float y2;

		public FRect(float x1, float x2, float y1, float y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public FPoint Center() 
		{
			FPoint local0;

			local0 = new FPoint(this.x1 + this.x2 / 2, this.y1 + this.y2 / 2);
			return local0;
		}
		
		public float Height() 
		{
			float local0;

			local0 = this.y2 - this.y1;
			return local0;
		}

		public float Width() 
		{
			float local0;

			local0 = this.x2 - this.x1;
			return local0;
		}
	}
}