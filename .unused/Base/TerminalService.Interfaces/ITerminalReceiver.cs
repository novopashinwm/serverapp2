using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;

namespace TerminalService.Interfaces
{
    public interface ITerminalReceiver : IDisposable
    {
        DateTime LastReceiveTime { get;}
        void Send(byte[] Data, int length);
        event EventHandler OnData;
        bool Disposed { get;}
        int ControllerID { get; set; }
        object Tag { get; set;}
    }

    public interface ITCPReceiver : ITerminalReceiver
    {
        IPEndPoint EndPoint { get;}
        TcpClient Client { get; }
        void CloseClient();
    }
}
