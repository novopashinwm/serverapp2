using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace TerminalService.Interfaces.Common
{
    [Serializable]
    public abstract class TSEventArgs : EventArgs
    {
        private readonly string protocol;

        public TSEventArgs(string Protocol)
        {
            this.protocol = Protocol;
        }

       
        /// <summary>
        /// ��� ���������, �� �������� �������� ���������
        /// </summary>
        public string Protocol
        {
            get { return protocol; }
        }
    }

	/// <summary>
	/// �������� ������� ������� �������� ���������� ������ 
	/// </summary>
	[Serializable]
	public class ReceiveLogEventArgs : EventArgs
	{
		/// <summary>
		/// ������ ������, ������� ������� ����� ��������
		/// </summary>
		readonly int unique;

		/// <summary>
		/// �������� ����� 
		/// </summary>
		readonly string garageNumber = String.Empty;

		/// <summary>
		/// ������ �� ���������� � ��������� ��������
		/// </summary>
		private readonly SortedList mobilUnits; // IMobilUnit

		private readonly int iLogLength;

		private readonly int iLogLengthNeeded;

		/// <summary>
		/// ���������� � ��������� ��������
		/// </summary>
		public SortedList MobilUnits  // IMobilUnit
		{
			get { return mobilUnits; }
		}

		public int Unique
		{
			get { return unique; }
		}

		/// <summary>
		/// ����� ����� ��������� � ���� �������� ������� �� �������� ������
		/// </summary>
		public int LogLength
		{
			get
			{
				return iLogLength;
			}
		}

		/// <summary>
		/// ����� �������� �������, ������������� �������������
		/// </summary>
		public int LogLengthNeeded
		{
			get
			{
				return iLogLengthNeeded;
			}
		}

		/// <summary>
		/// �������� ����� 
		/// </summary>
		public string GarageNumber
		{
			get { return garageNumber; }
		}

		#region .ctor
		/// <summary>
		/// �������� ������� ������� �������� ���������� ������ � ������� ��������
		/// </summary>
		/// <param name="mobilUnits">������ �� ���������� � ��������� ��������</param>
		/// <param name="unique"></param>
		/// <param name="iLogLength"></param>
		/// <param name="iLogLengthNeeded"></param>
		public ReceiveLogEventArgs(SortedList mobilUnits, int unique, int iLogLength, int iLogLengthNeeded)
		{
			this.unique = unique;
			this.mobilUnits = mobilUnits;
			this.iLogLength = iLogLength;
			this.iLogLengthNeeded = iLogLengthNeeded;
		}

		public ReceiveLogEventArgs(SortedList mobilUnits, int unique, int iLogLength, int iLogLengthNeeded, string garageNumber)
			:
			this(mobilUnits, unique, iLogLength, iLogLengthNeeded)
		{
			this.garageNumber = garageNumber;
		}
		#endregion // .ctor
	}

    public class ReceiveDataEventArgs : EventArgs
    {
        #region .ctor
        public ReceiveDataEventArgs(byte[] data, string protocol, object tag)
        {
            this.data = data;
            this.tag = tag;
            this.protocol = protocol;
        }

        #endregion

        /// <summary>
        /// C����� �� ���������� ������
        /// </summary>
        protected readonly byte[] data;

        private readonly string protocol;

        private readonly object tag;

        /// <summary>
        /// C����� �� ���������� ������
        /// </summary>
        public byte[] Data
        {
            get { return data; }
        }

        /// <summary>
        /// ��������, �� �������� �������� ������
        /// </summary>
        public string Protocol
        {
            get { return protocol; }
        }

        public object Tag
        {
            get { return tag; }
        }
    }

    public class PhoneReceiveEventArgs : ReceiveDataEventArgs
    {
        public PhoneReceiveEventArgs(byte[] data, string protocol, string phoneNumber, object tag) : base(data, protocol, tag)
        {
            this.phoneNumber = phoneNumber;
        }

        private readonly string phoneNumber;

        public string PhoneNumber
        {
            get { return phoneNumber; }
        }
    }

    public class GPRSReceiveDataEventArgs : ReceiveDataEventArgs
    {
        private IPEndPoint endPoint;

        public GPRSReceiveDataEventArgs(IPEndPoint ipep, byte[] data, string protocol, object tag)
            : base(data, protocol, tag)
        {
            this.endPoint = ipep;
        }

        public IPEndPoint EndPoint
        {
            get { return endPoint; }
            set { endPoint = value; }
        }
    }

    public class TcpOnDataEventArgs : EventArgs
    {
        IPEndPoint address;
        byte[] data;

        public IPEndPoint Address
        {
            get { return address; }
            set { address = value; }
        }

        public byte[] Data
        {
            get { return data; }
            set { data = value; }
        }
    }
}