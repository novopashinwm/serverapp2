using System;
using System.Runtime.InteropServices;

namespace TerminalService.Interfaces
{
	/// <summary>
	/// ������� ��������� ��� ���� ����� �������
	/// </summary>
	[Guid("5ECBEE5A-F3B9-4a37-B8FA-EFB19228B3DE")]
	public interface ICommand
	{
		/// <summary>
		/// �������������� ���������� � �������, ��������� �� ���������
		/// </summary>
		object Tag { get; set; }

		/// <summary>
		/// ����� �������
		/// </summary>
		string Text { get; set; }
	}
}

/*Avatar.NET*/