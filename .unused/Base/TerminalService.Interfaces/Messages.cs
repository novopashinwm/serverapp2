using System.Net;
using System;

namespace TerminalService.Interfaces
{
    /// <summary>
    /// notification importance, Lvl0 - highest
    /// </summary>
    public enum Severity : byte
    {
        /// <summary>
        /// only errs
        /// </summary>
        Lvl0,
        /// <summary>
        /// alarm
        /// </summary>
        Lvl1,
        Lvl2,
        Lvl3,
        /// <summary>
        /// normal
        /// </summary>
        Lvl4,
        Lvl5,
        Lvl6,
        Lvl7
    }

    public enum NotifyEventType
    {
        /// <summary>
        /// general exception
        /// </summary>
        NE_EXCEPTION = 0,

        /// <summary>
        /// terminal exception
        /// Tag[0] - LinkStatus value, Tag[1] - unit
        /// </summary>
        TM_EXCEPTION = 1,

        /// <summary>
        /// LinkStatus change, unitInfo is mandatory. 
        /// Tag[0] - LinkStatus value, Tag[1] - unit, Tag[2] - current operator, 
        /// Tag[3] - CmdType
        /// </summary>
        TM_LINKSTATUS = 2,

        /// <summary>
        /// alarm. Tag[0] - unit; Tag[1] - original message; Tag[2] - 1 - on, 0 - off; 
        /// [Tag[3] - message no. (-1 not specified)]
        /// </summary>
        NE_ALARM = 3,

        /// <summary>
        /// message. Tag[0] - unit; Tag[1] - code (-1 not specified); 
        /// Tag[2] - original message; [Tag[3] - message no. (-1 not specified)]
        /// </summary>
        NE_MESSAGE = 4,

        /// <summary>
        /// confirm message. Tag[0] - unit; Tag[1] - original message; 
        /// Tag[2] - number of message to confirm; 
        /// Tag[3] - confirm state, source message dependent;
        /// Tag[4] - message no. (-1 not specified); Tag[5] - CmdType to confirm
        /// </summary>
        NE_CONFIRM = 5,

        TM_MESSAGE = 6,

        /// <summary>
        /// ������������� ��������� ������ � ���������
        /// </summary>
        NE_CONFIRM_FIRMWARE = 7,

        /// <summary>
        /// ��������� �������� ����������
        /// </summary>
        NE_SETTINGS
    }

    [Serializable]
    public class DeviceInfo : IDeviceInfo
    {
        private IPEndPoint endPoint;
        private string phoneNumber;
        private int deviceNumber = -1;
        private long imei = -1;
        private int controllerID = -1;
        private string controllerType;

        /// <summary>
        /// ID ����������� �� ��
        /// </summary>
        public int ControllerID
        {
            get { return controllerID; }
            set { controllerID = value; }
        }

        /// <summary>
        /// IP ����� ����������
        /// </summary>
        public IPEndPoint EndPoint
        {
            get { return endPoint; }
            set { endPoint = value; }
        }

        /// <summary>
        /// ���������� ����� ����������
        /// </summary>
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        /// <summary>
        /// ����� ����������� ����������� ��� ����� �������������
        /// </summary>
        public int DeviceNumber
        {
            get { return deviceNumber; }
            set { deviceNumber = value; }
        }

        /// <summary>
        /// IMEI ����������
        /// </summary>
        public long Imei
        {
            get { return imei; }
            set { imei = value; }
        }

        public override string ToString()
        {
            string str = string.Format("ID: {0}\t Phone: {1}\t Number: {2}\t IMEI: {3}", ControllerID, PhoneNumber, DeviceNumber, Imei);
            return str;
        }

        public string ControllerType
        {
            get { return controllerType; }
            set { controllerType = value; }
        }

        #region IDeviceInfo Members



        #endregion
    }
}
