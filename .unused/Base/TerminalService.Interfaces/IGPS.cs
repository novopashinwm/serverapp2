namespace TerminalService.Interfaces
{
    public interface IGPS
    {
        /// <summary>
        /// �������
        /// </summary>
        float Longitude{ get; set; }
        /// <summary>
        /// ������
        /// </summary>
        float Latitude { get; set; }
        /// <summary>
        /// ������ ��� ������� ����
        /// </summary>
        int Height { get; set; }
        /// <summary>
        /// ����� (����� ������ �� 00:00:00 01.01.1970)
        /// </summary>
        int Time { get; set; }
        /// <summary>
        /// ����� ��������� ���������� ������� (����� ������ �� 00:00:00 01.01.1970)
        /// </summary>
        int CorrectTime { get; set; }
        /// <summary>
        /// �������� (��/�)
        /// </summary>
        float Speed { get; set; }
        /// <summary>
        /// ����
        /// </summary>
        float Course{ get; set;}
      
        /// <summary>
        /// ������������ ���������� �������
        /// </summary>
        bool CorrectGPS { get; set;}
        /// <summary>
        /// ����� ��������
        /// </summary>
        int Sattelites { get; set; }

        PositionType Type { get; set;}
    }

    public enum PositionType
    {
        Trace, //������� �������
        Log    //������������ �������
    }
}
