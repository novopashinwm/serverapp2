
using System.Collections.Generic;
using System.Net;
using System.Text;
using TerminalService.Interfaces;
using System;
using FORIS.TSS.Config;

namespace TerminalService.Interfaces
{
    [Serializable]
    public class DeviceData : DeviceInfo, IGPS, ISensor
    {
        /// <summary>
        /// ����������� ���������� ��������� (��� ����������� ���������� �������)
        /// </summary>
        public static int MinSatellites = 3;
        /// <summary>
        /// ������������ �������� (��� ����������� ���������� �������)
        /// </summary>
        public static int GPSLimitMaxSpeed = 150;
        static DeviceData()
        {
            if (Globals.AppSettings["GPSLimitMaxSpeed"] != null &&
                Globals.AppSettings["GPSLimitMaxSpeed"] != string.Empty)
                GPSLimitMaxSpeed = int.Parse(Globals.AppSettings["GPSLimitMaxSpeed"]);
            if (Globals.AppSettings["MinSatellites"] != null &&
               Globals.AppSettings["MinSatellites"] != string.Empty)
                MinSatellites = int.Parse(Globals.AppSettings["MinSatellites"]);
        }

        #region IGPS Members

        #region members

        private float longitude;
        private float latitude;
        private int height;
        private float speed;
        private float course;
        //private int id;

        #endregion

        #region Properties

        /// <summary>
        /// �������
        /// </summary>
        public float Longitude
        {
            get { return this.longitude; }
            set { this.longitude = value; }
        }

        /// <summary>
        /// ������
        /// </summary>
        public float Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        /// <summary>
        /// ������ ��� ������� ����
        /// </summary>
        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        /// <summary>
        /// ����� (����� ������ �� 00:00:00 01.01.1970)
        /// </summary>
        public int Time { get; set; }

        /// <summary>
        /// ����� ��������� ���������� ������� (����� ������ �� 00:00:00 01.01.1970)
        /// </summary>
        public int CorrectTime { get; set; }

        /// <summary>
        /// �������� (��/�)
        /// </summary>
        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        /// <summary>
        /// ����
        /// </summary>
        public float Course
        {
            get { return course; }
            set { course = value; }
        }

        
        /// <summary>
        /// ������������ ���������� �������
        /// </summary>
        public bool CorrectGPS {
            get { return correctGPS; }
            set { correctGPS = value; }
        }
        /// <summary>
        /// ����� ���������
        /// </summary>
        public int Sattelites {
            get { return sattelites; }
            set { sattelites = value; }
        }

        public PositionType Type
        {
            get { return type; }
            set { type = value; }
        }
        #endregion

        #endregion

        #region ISensor Members

        #region members

        private readonly Dictionary<int, object> sensors = new Dictionary<int, object>();
        private bool correctGPS;
        private int sattelites;
        //private IPEndPoint endPoint;
        //private string phoneNumber;
        private PositionType type;

        #endregion

        #region Properties

        public Dictionary<int, object> Sensors
        {
            get { return this.sensors; }
        }

        public byte[] ToByteArray()
        {
            List<byte> bytes = new List<byte>();

            bytes.Add((byte)this.sensors.Count);

            foreach (KeyValuePair<int, object> sensor in this.sensors)
            {
                
                if (sensor.Value is bool)
                {
                    byte[] data = BitConverter.GetBytes((Int32)sensor.Key);
                    bytes.AddRange(data);
                    bytes.Add(0);
                    data = BitConverter.GetBytes((bool)sensor.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (sensor.Value is Int32)
                {
                    byte[] data = BitConverter.GetBytes((Int32)sensor.Key);
                    bytes.AddRange(data);
                    bytes.Add(1);
                    data = BitConverter.GetBytes((Int32)sensor.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
            }

           
            return bytes.ToArray();
        }

        public void FromByteArray(byte[] data)
        {
            this.sensors.Clear();

            List<byte> bytes = new List<byte>(data);

            int counter = 0;

            int count = bytes[counter++];

            for (int i = 0; i < count; i++)
            {
                int len = 4;

                byte[] bts = bytes.GetRange(counter, len).ToArray();
                counter += len;
                int key = BitConverter.ToInt32(bts, 0);

                int type_ = bytes[counter++];
                len = bytes[counter++];

                switch (type_)
                {
                    case 0:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        bool bv = BitConverter.ToBoolean(bts, 0);
                        this.sensors.Add(key, bv);
                        break;
                    case 1:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        int iv = BitConverter.ToInt32(bts, 0);
                        this.sensors.Add(key, iv);
                        break;
                    default:
                        throw new Exception("������������ ��� ������");
                }
            }
            if (counter >= data.Length)
                return;

        }

        #endregion

        #endregion
    }

    /// <summary>
    /// ��� ������ ������� �� ��
    /// </summary>
    public class Sensor : ISensor
    {
        private readonly Dictionary<int, object> sensors = new Dictionary<int, object>();

        #region ISensor Members

        public Dictionary<int, object> Sensors
        {
            get { return sensors; }
        }

        public byte[] ToByteArray()
        {
            List<byte> bytes = new List<byte>();

            bytes.Add((byte)this.sensors.Count);

            foreach (KeyValuePair<int, object> sensor in this.sensors)
            {

                if (sensor.Value is bool)
                {
                    byte[] data = BitConverter.GetBytes((Int32)sensor.Key);
                    bytes.AddRange(data);
                    bytes.Add(0);
                    data = BitConverter.GetBytes((bool)sensor.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (sensor.Value is Int32)
                {
                    byte[] data = BitConverter.GetBytes((Int32)sensor.Key);
                    bytes.AddRange(data);
                    bytes.Add(1);
                    data = BitConverter.GetBytes((Int32)sensor.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
            }


            return bytes.ToArray();
        }

        public void FromByteArray(byte[] data)
        {
            this.sensors.Clear();

            List<byte> bytes = new List<byte>(data);

            int counter = 0;

            int count = bytes[counter++];

            for (int i = 0; i < count; i++)
            {
                int len = 4;

                byte[] bts = bytes.GetRange(counter, len).ToArray();
                counter += len;
                int key = BitConverter.ToInt32(bts, 0);

                int type_ = bytes[counter++];
                len = bytes[counter++];

                switch (type_)
                {
                    case 0:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        bool bv = BitConverter.ToBoolean(bts, 0);
                        this.sensors.Add(key, bv);
                        break;
                    case 1:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        int iv = BitConverter.ToInt32(bts, 0);
                        this.sensors.Add(key, iv);
                        break;
                    default:
                        throw new Exception("������������ ��� ������");
                }
            }
            if (counter >= data.Length)
                return;
        }

        #endregion

        public static ISensor CreateFromByteArray(byte[] data)
        {
            Sensor sensor = new Sensor();

            sensor.FromByteArray(data);

            return sensor;
        }
    }
}