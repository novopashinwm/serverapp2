using System;
using System.Collections.Generic;
using System.Text;

namespace TerminalService.Interfaces
{
	public interface IMobilStatus
	{
		/// <summary>
		/// ������ ���������� �������
		/// </summary>
		Status Status { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	[Flags]
	public enum Status : int
	{
		// ������� ��� �������������� ��������� 
		None = 0x0000,
		// ������� (��������� ������ SOS)
		Alarm = 0x0001,
		// �����������, �� �������� (��������� ��� ����)
		ParcingMoving = 0x0002,
		// �������� 
		Parcing = 0x0004
	}
}