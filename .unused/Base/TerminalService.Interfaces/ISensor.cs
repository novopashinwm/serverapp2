
using System;
using System.Collections.Generic;

namespace TerminalService.Interfaces
{
    
    public interface ISensor
    {
        Dictionary<int, object> Sensors { get;}
        byte[] ToByteArray();
        void FromByteArray(byte[] data);
    }
}
