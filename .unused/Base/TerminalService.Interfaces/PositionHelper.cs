using System;
using FORIS.TSS.BusinessLogic.Map;

namespace TerminalService.Interfaces
{
	public static class PositionHelper
	{
		/// <summary>
		/// ������ ������� �����
		/// </summary>
		public static double EarthRadius = 6367444.0;

		public static double DegreesToRadians = Math.PI / 180f;
		public static double RadiansToDegrees = 180f / Math.PI;
		
		/// <summary>
		/// ���������� ����� ���������� ������� �� 2� ��������� �������������� ������
		/// </summary>
		/// <param name="firstLatitude">������������� ������ ������������� �������</param>
		/// <param name="firstLongitude">������������� ������� ������������� �������</param>
		/// <param name="secondLatitude">������������� ������ ��������� �������</param>
		/// <param name="secondLongitude">������������� ������� ��������� �������</param>
		/// <returns>����� ���������� �������</returns>
		public static int CalcCourse( double firstLatitude, double firstLongitude, double secondLatitude, double secondLongitude )
		{
			if ( !( firstLatitude < 90f && firstLongitude < 180f ) )
				return 0;

			double norm = SLClose( firstLatitude, firstLongitude, secondLatitude, secondLongitude );
			if ( norm == 0.0f ) return 0;
			double arg = EarthRadius * Math.Abs( DegreesToRadians * ( secondLatitude - firstLatitude ) ) / norm;
			if ( arg < -1 ) arg = -1;
			else if ( arg > 1 ) arg = 1;
			double arc = Math.Asin( arg );
			arc *= RadiansToDegrees; // (180.0 / Math.PI); // degree in first kvadrant
			int signx = 1;
			if ( secondLongitude - firstLongitude < 0 ) signx = -1;
			int signy = 1;
			if ( secondLatitude - firstLatitude < 0 ) signy = -1;
			if ( signx > 0 && signy > 0 ) arc = 90.0 - arc;
			if ( signx > 0 && signy < 0 ) arc = 90.0 + arc;
			if ( signx < 0 && signy < 0 ) arc = 270.0 - arc;
			if ( signx < 0 && signy > 0 ) arc = 270.0 + arc;
			return Convert.ToInt32( arc ); // ????
		}

		/// <summary>
		/// ���������� ���������� ����� ����� �������
		/// </summary>
		/// <param name="firstLatitude">������������� ������ ������ �����</param>
		/// <param name="firstLongitude">������������� ������� ������ �����</param>
		/// <param name="secondLatitude">������������� ������ ������ �����</param>
		/// <param name="secondLongitude">������������� ������� ������ �����</param>
		/// <returns></returns>
		public static double SLClose(
			double firstLatitude,
			double firstLongitude,
			double secondLatitude,
			double secondLongitude
			)
		{
			double dx = DegreesToRadians * ( secondLongitude - firstLongitude );
			double dy = DegreesToRadians * ( secondLatitude - firstLatitude );
			double xm = EarthRadius * Math.Cos( DegreesToRadians * ( firstLatitude ) ) * dx;
			double ym = EarthRadius * dy;
			return Math.Sqrt( xm * xm + ym * ym );
		}

		/// <summary>
		/// ���������� �������������� ���������� �����, ���������
		/// ������������ �������� �� �� �������� ���������� ������
		/// �� ����������� � ���������
		/// </summary>
		/// <param name="longitude">������� �������� �����</param>
		/// <param name="latitude">������ �������� �����</param>
		/// <param name="xm">�������� �� ����������� (� ������)</param>
		/// <param name="ym">�������� �� ��������� (� ������)</param>
		/// <returns>���������� �����</returns>
		public static GeoPoint ShiftGeo(
			double longitude,
			double latitude,
			double xm,
			double ym
			)
		{
			double dx = xm / ( EarthRadius * Math.Cos( DegreesToRadians * ( latitude ) ) );
			double dy = ym / EarthRadius;
			latitude = ( DegreesToRadians * latitude + dy ) / DegreesToRadians;
			longitude = ( DegreesToRadians * longitude + dx ) / DegreesToRadians;

			return 
				GeoPoint.CreateGeo( longitude, latitude );
		}
	}
}