using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace TerminalService.Interfaces
{
	[Serializable]
	public class OperatorInfo: IOperatorInfo
	{
		readonly int operatorID;
		public int OperatorID
		{
			get { return this.operatorID; }
		}

		private readonly string login;
		public string Login
		{
			get { return this.login; }
		}

		private readonly string name;
		public string Name
		{
			get { return name; }
		}


		public OperatorInfo(
			int operatorID,
			string login,
			string name
			)
		{
			this.operatorID = operatorID;
			this.login = login;
			this.name = name;
		}

		public override string ToString()
		{
			return operatorID.ToString() + ( name == null ? "" : " - " + name );
		}

		public override bool Equals( object obj )
		{
			/* ������������ ��������� OperatorInfo?, ���� ��������� ��������� � 
			 * ���������� �������������������� ��������� OperatorInfo
			 */

			IOperatorInfo oi = (IOperatorInfo)obj;
			
			return this.operatorID == oi.OperatorID;
		}

		public override int GetHashCode()
		{
			return this.operatorID.GetHashCode();
		}

		public static bool operator ==(OperatorInfo operator1, OperatorInfo operator2 )
		{
			return OperatorInfo.Equals(operator1, operator2);
		}
		public static bool operator !=(OperatorInfo operator1, OperatorInfo operator2)
		{
			return !(operator1 == operator2);
		}
            
	}
}