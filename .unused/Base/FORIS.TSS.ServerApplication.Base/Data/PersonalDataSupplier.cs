﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using System.Threading;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.Data
{
	public class PersonalDataSupplier<TServerData, TDataTreater> :
		DataSupplierWrapper<TServerData, TDataTreater>,
		ISponsor
		where TServerData : MarshalByRefObject, IDataSupplier<TDataTreater>
		where TDataTreater : IDataTreater
	{
		#region Constructor & Dispose

		public PersonalDataSupplier(TServerData serverData, IPersonalServer personalServer)
			: base(serverData)
		{
			#region Preconditions 

			if (object.Equals(serverData, default(TServerData)))
			{
				throw new ArgumentException("Value cannot be null", "serverData");
			}

			#endregion // Preconditions

			this.personalServer = personalServer;

			#region Sponsor lease of server data

			/* Танас:
			 * Против метода RemotingServices.GetLifeTimeService(),
			 * вызов dataSupplier.InitializeLifeTimeService() вызывая 
			 * метод базового класса MarshalByRefObject.InitializeLifeTimeService()
			 * может создать лицензию, если для объекта она еще не создавалась.
			 * При следующих запросах будет возвращен уже существующий объект лицензии.
			 * RemotingServices.GetLifeTimeService() не создает лицензий.
			 * Источник: Reflector.
			 */

			ILease serverDataLease = (ILease)serverData.InitializeLifetimeService();

			if (serverDataLease.CurrentState == LeaseState.Initial)
			{
				// Чтоб активировать лицензию
				ObjRef objRef = RemotingServices.Marshal(serverData);
			}

			serverDataLease.Register(this);

			#endregion // Sponsor lease of server data

			/* Этим вызовом отправляем оъекту сессии
			 * удерживающую ссылку на объект персонального
			 * поставщика данных, что есть не очень хорошо
			 * в отношении утечек
			 */
			this.personalServer.Closing += personalServer_Closing;
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				personalServer.Closing -= personalServer_Closing;

				RemotingServices.Disconnect(this);
			}

			base.Dispose(disposing);
		}
		#endregion Constructor & Dispose

		public override object InitializeLifetimeService()
		{
			ILease Lease = (ILease)base.InitializeLifetimeService();

			if (Lease.CurrentState == LeaseState.Initial)
			{
				Lease.Register(personalServer);
			}

			return Lease;
		}

		#region ISponsor Members

		TimeSpan ISponsor.Renewal(ILease lease)
		{
			ILease ownLease = (ILease)RemotingServices.GetLifetimeService(this);

			return ownLease.CurrentLeaseTime;
		}

		#endregion ISponsor Members

		private readonly IPersonalServer personalServer;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IPersonalServer PersonalServer
		{
			get { return personalServer; }
		}
		private void personalServer_Closing(object sender, EventArgs e)
		{
			/* При получении от объекта сессии пользователя 
			 * уведомления о закрытии сессии совершаем
			 * ритуальное самоубийство
			 */
			Dispose();
		}
		/// <summary> Прореживает данные в соответствии с правами доступа пользователя </summary>
		/// <returns></returns>
		protected override DataInfo GetDataInfo()
		{
			TssPrincipalInfo p = PersonalServer.GetPrincipal();
			if (p != default(TssPrincipalInfo))
				Thread.CurrentPrincipal = p;
			DataInfo Result = base.GetDataInfo();

			/* Если объект сессии не поддерживает проверку
			 * доступа пользователя к данным, то и не будем
			 * ее вызывать. Однако, настучим.
			 */

			if (personalServer.Security != null)
			{
				Result = personalServer.Security.Check(Result);
			}
			else
			{
				Trace.TraceWarning(
					"Объект {0} сессии пользователя {1} не поддерживает проверку доступа к данным",
					personalServer.GetType().Name,
					personalServer.SessionInfo.OperatorInfo.Login);
			}

			return Result;
		}
	}
}