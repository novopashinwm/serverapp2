﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.Data
{
	/// <summary></summary>
	/// <typeparam name="TDatabaseProvider"></typeparam>
	/// <remarks>
	/// Поставщик данных из БД и на самом деле не 
	/// может ничего кроме как запросить данные из БД.
	/// Никакого извещения об изменении 
	/// данных из БД прийти не может.
	/// </remarks>
	public abstract class DatabaseDataSupplier<TDatabaseProvider> :
		FinalDataSupplier
		where TDatabaseProvider : IDatabaseProvider
	{
		public DatabaseDataSupplier(TDatabaseProvider databaseProvider)
		{
			this.databaseProvider = databaseProvider;
		}
		private readonly TDatabaseProvider databaseProvider;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TDatabaseProvider DatabaseProvider
		{
			get { return databaseProvider; }
		}
	}
}