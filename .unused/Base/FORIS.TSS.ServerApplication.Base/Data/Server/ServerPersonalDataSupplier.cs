﻿using FORIS.TSS.BusinessLogic.Data.Server;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication.Data.Server
{
	public class ServerPersonalDataSupplier : PersonalDataSupplier<ServerServerData, IServerDataTreater>, IServerDataSupplier
	{
		public ServerPersonalDataSupplier(ServerServerData serverData, IPersonalServer personalServer)
			: base(serverData, personalServer)
		{
			this.serverData = serverData;
		}
		private readonly ServerServerData serverData;
		public override IServerDataTreater GetDataTreater()
		{
			return new ServerDataTreater(serverData, PersonalServer.SessionInfo);
		}
	}
}