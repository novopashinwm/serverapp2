﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.ServerApplication.Security
{
	public abstract class SecurityChecker<TPersonalServer, TServer> :
		ISecurity
		where TPersonalServer : PersonalServerBase<TServer>
		where TServer : ServerBase<TServer>
	{
		private readonly TPersonalServer personalServer;
		public SecurityChecker(TPersonalServer personalServer)
		{
			this.personalServer = personalServer;
		}

		#region ISecurity Members

		ObjectRight ISecurity.GetRight(DataRow row)
		{
			throw new NotImplementedException();
		}

		DataInfo ISecurity.Check(DataInfo dataInfo)
		{
			return new DataInfo(this.Check(dataInfo.DataSet), dataInfo.Version);
		}

		#endregion ISecurity Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="dataSet"></param>
		/// <returns></returns>
		/// <remarks>
		/// Теоретически этот метод можно реализовать
		/// с использованием метода GetRight() для каждой 
		/// строки данных, вопрос в том будет ли такое 
		/// решение оптимально по быстродействию
		///  </remarks>
		protected abstract DataSet Check(DataSet dataSet);
	}
}