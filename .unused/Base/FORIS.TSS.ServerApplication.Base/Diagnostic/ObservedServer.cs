﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Interfaces.Diagnostic;

namespace FORIS.TSS.ServerApplication.Diagnostic
{
	/// <summary> Компонент, реализующий функциональность интерфейса диагностирования сервера </summary>
	public class ObservedServer : Component, IObservedServer
	{
		#region Constructor & Dispose

		public ObservedServer()
		{
		}
		public ObservedServer( IContainer container )
		{
			container.Add( this );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing)
			{
				if( !this.serverStopped )
				{
					this.ActionServerStop();
				}

				this.delegateServerStop = null;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region IObservedServer Members

		object IObservedServer.IsAlive()
		{
			return new object();
		}

		IList<Exception> IObservedServer.GetErrorCodes()
		{
			return this.errorCodesList;
		}

		void IObservedServer.ClearErrorCodes()
		{
			this.errorCodesList.Clear();
		}

		private event AnonymousEventHandler<EventArgs> delegateServerStop;
		
		event AnonymousEventHandler<EventArgs> IObservedServer.ServerStop
		{
			add { this.delegateServerStop += value; }
			remove { this.delegateServerStop -= value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <remarks>
		/// Возбуждение события должно быть 
		/// реализовано именно таким способом,
		/// так как событие посылается по ремотингу
		/// </remarks>
		protected virtual void OnServerStop()
		{
			if( this.delegateServerStop != null )
			{
				Delegate[] Handlers = this.delegateServerStop.GetInvocationList();

				foreach( AnonymousEventHandler<EventArgs> handler in Handlers )
				{
					handler.BeginInvoke(
						EventArgs.Empty,
						new AsyncCallback(ServerStop_Callback),
						handler
						);
				}
			}
		}

		private void ServerStop_Callback( IAsyncResult asyncResult )
		{
			AnonymousEventHandler<EventArgs> Handler =
				(AnonymousEventHandler<EventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke( asyncResult);
			}
			catch( Exception )
			{
				this.delegateServerStop -= Handler;
			}
		}

		#endregion // IObservedServer Members

		#region ErrorCode

		private readonly List<Exception> errorCodesList = new List<Exception>();

		public void PushErrorCode(Exception ex)
		{
			errorCodesList.Add(ex);
		}

		#endregion // ErrorCode

		#region Stop

		private bool serverStopped = false;

		public void ActionServerStop()
		{
			this.serverStopped = true;

			this.OnServerStop();
		}

		#endregion // Stop
	}
}