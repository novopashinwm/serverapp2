using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// ������� ����� ��������� ������ �������
	/// </summary>
	/// <remarks>
	/// ����� ����� ��������� ����� ���������� 
	/// � ���������� � ����� ������������ �����������
	/// � 
	/// ��������� � ��� ����� ��������� � 
	/// </remarks>
	public class TssServiceBase: 
		System.ServiceProcess.ServiceBase
	{
		#region Controls & Components

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public TssServiceBase()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			// configure client side
			RemotingConfiguration.Configure( AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false );
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// TaxiService
			// 
			this.ServiceName = "FORIS.TSS.BaseServer";
		}

		#endregion // Component Designer generated code

		private AppDomain serverDomain = null;
		private IEntryPoint entryPoint = null;

		
		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			try
			{

				string nameOfNewDomain = GlobalsBase.AppSettings["nameOfNewDomain"];
				string nameOfAssemblyFile = GlobalsBase.AppSettings["nameOfAssemblyFile"];
				string nameOfConfigFile = GlobalsBase.AppSettings["nameOfConfigFile"];
				string nameOfType = GlobalsBase.AppSettings["nameOfType"];

				#region Conditions

				/* �����
				 * ����� ���������� ��������� ������� � ����� 
				 * ������������ ����������� ���������� � ���
				 * �� ���������� ������ ��������������� ���������
				 * ���������� � ��������� ����������� ����������
				 * 
				 * ����������� ����������� ������� �������� 
				 * ������ �������� nameOfConfigFile
				 */

				#endregion // Conditions

				string appBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

				this.entryPoint = (IEntryPoint)DomainHelper.CreateDomainAndGetEntryPoint(
				                              	nameOfNewDomain, 
				                              	nameOfConfigFile, 
				                              	nameOfAssemblyFile, 
				                              	nameOfType, 
				                              	out serverDomain,
				                              	appBase
				                              	);
				this.entryPoint.Start( true, true, true );

				/* �����
				 * �������, ��� ���� ����� ����� �� ����� 
				 * 30 ������, ���������� ��� �� ���������� SCM.
				 * ��� ��������, ��� � ���������� ������ 
				 * IEntryPoint.Start() �� ����� ���� ������� 
				 * standard application message loop
				 */
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				throw;
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		/// <remarks>
		/// When you exit the application, either by calling Environment.Exit or when
		/// the last foreground thread has terminated, all threads are suspended,
		///	finalizers for all objects are run on a separate thread, 
		/// but finally blocks are not run (the threads are suspended) and then the application exits. 
		/// It's actually far more complex then that but that's the basic idea. This avoids
		/// the issues related to hung threads, regardless of its being in managed or
		///	unmanaged code at shutdown time.
		///</remarks>
		protected override void OnStop()
		{
			/* �����:
			 * ��� ���� �� ����� ������� ����� ��������� ����� �����,
			 * ���� ��������� ��� �� �������� �����.
			 * 
			 * ������, ������ Service Control Manager (SCM) 
			 * �������� OnStop() � ������ ��������� ����
			 * ������� BeginInvoke �������� (���������)
			 */

			this.entryPoint.Stop();

			Thread stopperThread = 
				new Thread(
					delegate()
						{
							#region Code to stop server in stopperThread

							try
							{
								/*
							 * SessionList ��� ������ �� �������� AppDomain.DomainUnload
							 * 
							Trace.WriteLine("Connecting to the server in order to stop it", "OnStop");
							string serverUrl = Globals.AppSettings["Server"];
							// create proxy for SessionList
							ISessionListAdmin server = (ISessionListAdmin)Activator.GetObject(typeof(ISessionList), serverUrl);
							// stop the server
							Trace.WriteLine("Calling OnStop", "OnStop");]
							server.Stop();
							 */


								Trace.WriteLine( "Unloading domain", "OnStop" );
								for( int i = 0; i < 2; ++i )
								{
									Trace.WriteLine( "Attempt No " + i, "OnStop" );

									try
									{
										if( i != 0 )
										{
											Thread.Sleep( 1 );
											GC.Collect();
											GC.WaitForPendingFinalizers();
											GC.Collect();
										}

										AppDomain.Unload( serverDomain );
										Trace.WriteLine( "Domain was unloaded", "OnStop" );

										GC.Collect();
										GC.WaitForPendingFinalizers();
										GC.Collect();
										Trace.WriteLine( "Garbage was collected", "OnStop" );

										Trace.Flush();
										return;
									}
									catch( CannotUnloadAppDomainException cuade )
									{
										string prefix = "System.CannotUnloadAppDomainException: AppDomain can not be unloaded because the thread ";
										if( cuade.Message.StartsWith( prefix ) )
										{
											string threadName = cuade.Message.Substring( prefix.Length, cuade.Message.IndexOf( " ", prefix.Length ) );
											Trace.WriteLine( "ThreadName is " + threadName, "OnStop" );
										}
										else
										{
											Trace.WriteLine( cuade.ToString() );
											Trace.Flush();
											continue;
										}
									}
									catch( Exception ex )
									{
										Trace.WriteLine( ex.ToString() );
										Trace.Flush();
										continue;
									}
								}
								Trace.WriteLine( "Domain unloading failed. Stopping the entire process...", "OnStop" );
								System.Environment.Exit( 0 );
								Trace.WriteLine( "Process was stopped", "OnStop" );
								Trace.Flush();
							}
							catch( Exception ex )
							{
								Trace.WriteLine( ex.ToString() );
								throw;
							}
							Trace.WriteLine( "Exiting from OnStop", "OnStop" );

							#endregion // Code to stop server in stopperThread
						}
					);

			stopperThread.IsBackground = false; // the process will wait for this thread
			stopperThread.Start();

			//stoperThread.Join();
		}
	}
}