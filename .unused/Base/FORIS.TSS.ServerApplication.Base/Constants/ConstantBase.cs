using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Constants
{
	public abstract class ConstantBase : Component, IConstant
	{
		#region Properties

		private string name;
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private bool readOnly = true;
		[Browsable(true)]
		[DefaultValue(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool ReadOnly
		{
			get { return this.readOnly; }
			set { this.readOnly = value; }
		}


		#endregion // Properties

		#region IConstant members

		string IConstant.Name
		{
			get { return this.Name; }
		}

		#endregion // IConstant members

		public abstract string GetValue();
		public abstract void SetValue(string value);

		#region Events

		private EventHandler delegateChanged;

		public event EventHandler Changed
		{
			add { this.delegateChanged += value; }
			remove { this.delegateChanged -= value; }
		}

		protected virtual void OnChanged()
		{
			if (this.delegateChanged != null)
			{
				this.delegateChanged(this, EventArgs.Empty);
			}
		}

		#endregion // Events
	}

	public abstract class ConstantBase<TValue> : ConstantBase, IConstant<TValue>
	{
		#region IConstant<TValue> members

		protected TValue value;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		TValue IConstant<TValue>.Value
		{
			get { return this.value; }
			set
			{
				if (this.ReadOnly)
				{
					throw new NotSupportedException(this.Name + " is ReadOnly constant");
				}
				if (!this.value.Equals(value))
				{
					this.value = value;

					this.OnValueChanged();
				}
			}
		}

		private AnonymousEventHandler<EventArgs> delegateValueChanged;

		event AnonymousEventHandler<EventArgs> IConstant<TValue>.ValueChanged
		{
			add { this.delegateValueChanged += value; }
			remove { this.delegateValueChanged -= value; }
		}

		protected virtual void OnValueChanged()
		{
			this.OnChanged();

			if (this.delegateValueChanged != null)
			{
				Delegate[] Handlers = this.delegateValueChanged.GetInvocationList();

				foreach (AnonymousEventHandler<EventArgs> handler in Handlers)
				{
					handler.BeginInvoke(EventArgs.Empty, new AsyncCallback(Changed_Callback), handler);
				}
			}
		}

		private void Changed_Callback(IAsyncResult asyncResult)
		{
			AnonymousEventHandler<EventArgs> Handler =
				(AnonymousEventHandler<EventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				this.delegateValueChanged -= Handler;
			}
		}

		#endregion // IConstant<TValue> members

		#region Constructor & Dispose

		public ConstantBase()
		{

		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.delegateValueChanged = null;
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor &

		public override object InitializeLifetimeService()
		{
			/* ����� ����� ������� �� ��������� � ���� �� 
				 * ��� �� ������ �������� �������� ������ null
				 */

			return null;
		}
	}

	public class DateTimeConstant : ConstantBase<DateTime>
	{
		public DateTimeConstant()
		{

		}
		public DateTimeConstant(IContainer container)
		{
			container.Add(this);
		}


		public override string GetValue()
		{
			return this.value.ToString();
		}
		public override void SetValue(string value)
		{
			this.value = Convert.ToDateTime(value);
		}
	}

	public class StringConstant : ConstantBase<string>
	{
		public StringConstant()
		{

		}
		public StringConstant(IContainer container)
		{
			container.Add(this);
		}

		public override string GetValue()
		{
			return this.value;
		}
		public override void SetValue(string value)
		{
			this.value = value;
		}
	}

	public class IntegerConstant : ConstantBase<int>
	{
		public IntegerConstant()
		{

		}
		public IntegerConstant(IContainer container)
		{
			container.Add(this);
		}

		public override string GetValue()
		{
			return this.value.ToString();
		}
		public override void SetValue(string value)
		{
			this.value = Convert.ToInt32(value);
		}
	}
}