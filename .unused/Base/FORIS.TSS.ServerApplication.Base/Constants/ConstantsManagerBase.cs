﻿using System.ComponentModel;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Constants
{
	public class ConstantsManagerBase : Component
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		protected FORIS.TSS.ServerApplication.Constants.ConstantsPound constantsPound;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public ConstantsManagerBase()
		{
			InitializeComponent();
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.constantsPound = new FORIS.TSS.ServerApplication.Constants.ConstantsPound(this.components);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.components != null)
				{
					this.components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion // Component Designer generated code

		public override object InitializeLifetimeService()
		{
			/* Класс ходит ссылкой по ремотингу и чтоб за
			 * ним не следил менеджер лицензий вернем null
			 */

			return null;
		}

		#region Properties

		public IDatabaseDataSupplier DatabaseDataSupplier
		{
			get { return this.constantsPound.DatabaseDataSupplier; }
			set { this.constantsPound.DatabaseDataSupplier = value; }
		}

		#endregion // Properties
	}
}