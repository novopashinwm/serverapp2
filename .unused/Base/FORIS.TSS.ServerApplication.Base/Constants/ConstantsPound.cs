using System;
using System.ComponentModel;
using System.Diagnostics;

using FORIS.TSS.Common;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Constants
{
	public class ConstantsPound : Component
	{
		#region Properties

		private ConstantBaseCollection constants;
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ConstantBaseCollection Constants
		{
			get { return constants; }
		}

		private IDatabaseDataSupplier databaseDataSupplier;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IDatabaseDataSupplier DatabaseDataSupplier
		{
			get { return databaseDataSupplier; }
			set
			{
				databaseDataSupplier = value;

				foreach (ConstantBase constant in constants)
				{
					constant.SetValue(databaseDataSupplier.GetConstant(constant.Name));
				}
			}
		}

		#endregion // Properties

		#region Constructor & Dispose

		public ConstantsPound()
		{
			Initialize();
		}
		public ConstantsPound(IContainer container)
		{
			container.Add(this);

			Initialize();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				constants.Clear();

				constants.Inserted -= new CollectionChangeEventHandler<ConstantBase>(constants_Inserted);
				constants.Removing -= new CollectionChangeEventHandler<ConstantBase>(constants_Removing);
				constants.Clearing -= new CollectionEventHandler<ConstantBase>(constants_Clearing);
			}
			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Initialize

		private void Initialize()
		{
			constants = new ConstantBaseCollection();

			constants.Inserted += new CollectionChangeEventHandler<ConstantBase>(constants_Inserted);
			constants.Removing += new CollectionChangeEventHandler<ConstantBase>(constants_Removing);
			constants.Clearing += new CollectionEventHandler<ConstantBase>(constants_Clearing);
		}

		#endregion // Initialize

		#region Handle constants events

		private void constants_Inserted(object sender, CollectionChangeEventArgs<ConstantBase> e)
		{
			ConstantBase Constant = e.Item;

			Constant.Changed += new EventHandler(Constant_Changed);
		}
		private void constants_Removing(object sender, CollectionChangeEventArgs<ConstantBase> e)
		{
			ConstantBase Constant = e.Item;

			Constant.Changed -= new EventHandler(Constant_Changed);
		}
		private void constants_Clearing(object sender, CollectionEventArgs<ConstantBase> e)
		{
			foreach (ConstantBase constant in constants)
			{
				constant.Changed -= new EventHandler(Constant_Changed);
			}
		}

		#endregion // Handle constants events

		#region Handle Constant events

		private void Constant_Changed(object sender, EventArgs e)
		{
			Trace.Assert(databaseDataSupplier != null);

			ConstantBase Constant = (ConstantBase)sender;

			databaseDataSupplier.SetConstant(Constant.Name, Constant.GetValue());
		}

		#endregion // Handle Constant events
	}

	public class ConstantBaseCollection : CollectionWithEvents<ConstantBase>
	{
	}
}