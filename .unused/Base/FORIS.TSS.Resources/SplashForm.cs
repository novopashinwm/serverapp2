using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace FORIS.TSS.Resources
{
	/// <summary>
	/// Summary description for SplashForm.
	/// </summary>
	public class SplashForm : System.Windows.Forms.Form, ISplashForm
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label labelAppName;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Label labelText;
		private System.Windows.Forms.ProgressBar progressBar;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SplashForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SplashForm));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.labelAppName = new System.Windows.Forms.Label();
			this.labelVersion = new System.Windows.Forms.Label();
			this.labelText = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Location = new System.Drawing.Point(0, 64);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(288, 208);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// labelAppName
			// 
			this.labelAppName.Location = new System.Drawing.Point(8, 8);
			this.labelAppName.Name = "labelAppName";
			this.labelAppName.Size = new System.Drawing.Size(264, 16);
			this.labelAppName.TabIndex = 2;
			// 
			// labelVersion
			// 
			this.labelVersion.Location = new System.Drawing.Point(8, 32);
			this.labelVersion.Name = "labelVersion";
			this.labelVersion.Size = new System.Drawing.Size(264, 16);
			this.labelVersion.TabIndex = 3;
			// 
			// labelText
			// 
			this.labelText.Location = new System.Drawing.Point(8, 280);
			this.labelText.Name = "labelText";
			this.labelText.Size = new System.Drawing.Size(272, 40);
			this.labelText.TabIndex = 4;
			// 
			// progressBar
			// 
			this.progressBar.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.progressBar.Location = new System.Drawing.Point(0, 328);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(288, 8);
			this.progressBar.TabIndex = 5;
			// 
			// SplashForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(288, 336);
			this.ControlBox = false;
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.labelText);
			this.Controls.Add(this.labelVersion);
			this.Controls.Add(this.labelAppName);
			this.Controls.Add(this.pictureBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SplashForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SplashForm";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion
		
		public string Message
		{
			get
			{
				return String.Empty; // messageLabel.Text
			}
			set
			{
				//        End Get
				//        Set(ByVal Value As String)
				//            _messageLabel.Text = Value
				//            _messageLabel.Refresh()
				//        End Set
			}
		}

		public void Initialise(string title, Version version)
		{
			//throw new NotImplementedException();
		}

		void ISplashForm.ShowDialog(IWin32Window owner)
		{
			ShowDialog(owner);
		}
		
		//		''' <summary>
		//		''' Hides the splash screen form.
		//		''' </summary>
		void ISplashForm.Hide()
		{
			this.Owner = null;
			Hide();
		}

		//		''' <summary>
		//		''' Hides the splash screen form.
		//		''' </summary>
		void ISplashForm.Close()
		{
			this.Owner = null;
			Close();
		}
		
		public void CloseSplashForm(object sender, EventArgs args)
		{
			Close();
		}		
	}
}