using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;

namespace FORIS.TSS.Resources
{
	/// <summary>
	/// Summary description for AboutDlg.
	/// </summary>
	public class AboutDlg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RichTextBox richTextBox;
		private System.Windows.Forms.PictureBox pictureBox;
		private System.ComponentModel.IContainer components;

		public enum IconStyle
		{
			About			= 0,
			CommonError		= 1,
			Info			= 2,
			Question		= 3,
			LostConnection	= 4,
			Warning			= 5
		}

		private IconStyle m_IconStyle = IconStyle.About;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.ImageList imageList;

		public AboutDlg()
		{
			InitializeComponent();

			AssemblyName libName = this.GetType().Assembly.GetName();
			richTextBox.Text =
				"Ufin\r\n" + 
				"����� - ������������������ ������� �������� � ���������� ���������.\r\n\r\n" +
				"������: " + Strings.ApplicationVersion + "\r\n" + 
				"������: " + Strings.ApplicationBuild; //+ libName.Version.Major + "." + libName.Version.Minor + "." + libName.Version.Build + "." + libName.Version.Revision;
		}
		public AboutDlg(string strText)
		{
			InitializeComponent();

			richTextBox.Text = strText;
		}

		public AboutDlg(string strText, string strCaption)
		{
			InitializeComponent();

			richTextBox.Text = strText;
			this.Text = strCaption;
		}
		public AboutDlg(string strText, string strCaption, IconStyle iconStyle)
		{
			InitializeComponent();

			richTextBox.Text = strText;
			m_IconStyle = iconStyle;
			this.Text = strCaption;
		}
		public AboutDlg(string strText, IconStyle iconStyle)
		{
			InitializeComponent();

			richTextBox.Text = strText;
			m_IconStyle = iconStyle;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutDlg));
			this.richTextBox = new System.Windows.Forms.RichTextBox();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.buttonClose = new System.Windows.Forms.Button();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// richTextBox
			// 
			this.richTextBox.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.richTextBox.Location = new System.Drawing.Point(72, 12);
			this.richTextBox.Name = "richTextBox";
			this.richTextBox.ReadOnly = true;
			this.richTextBox.Size = new System.Drawing.Size(360, 96);
			this.richTextBox.TabIndex = 0;
			this.richTextBox.Text = "";
			// 
			// pictureBox
			// 
			this.pictureBox.Location = new System.Drawing.Point(12, 36);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(48, 48);
			this.pictureBox.TabIndex = 1;
			this.pictureBox.TabStop = false;
			// 
			// buttonClose
			// 
			this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonClose.Location = new System.Drawing.Point(312, 128);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(88, 23);
			this.buttonClose.TabIndex = 2;
			this.buttonClose.Text = "�������";
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "");
			this.imageList.Images.SetKeyName(1, "");
			this.imageList.Images.SetKeyName(2, "");
			this.imageList.Images.SetKeyName(3, "");
			this.imageList.Images.SetKeyName(4, "");
			this.imageList.Images.SetKeyName(5, "");
			// 
			// AboutDlg
			// 
			this.AcceptButton = this.buttonClose;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonClose;
			this.ClientSize = new System.Drawing.Size(440, 166);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.pictureBox);
			this.Controls.Add(this.richTextBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutDlg";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Ufin";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.AboutDlg_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void AboutDlg_Load(object sender, System.EventArgs e)
		{
			int idx = (int)m_IconStyle;
			this.pictureBox.Image = imageList.Images[idx];
		}
	}
}
