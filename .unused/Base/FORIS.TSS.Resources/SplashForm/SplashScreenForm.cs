using System;
using System.Windows.Forms;
using System.Threading;

namespace FORIS.TSS.Resources.SplashScreen
{
	/// <summary>
	/// Summary description for SplashScreenForm.
	/// </summary>
	sealed public class SplashScreenForm : System.Windows.Forms.Form
	{
		// Threading
		static SplashScreenForm ms_frmSplash = null;
		static Thread ms_oThread = null;
		private System.Threading.Timer threadUpdate = null;

		private System.Threading.Timer timer1;
		private Label textBoxAppName;
		private Label textBoxVershion;
		private Label textBoxBuild;
		private Label textBoxState;

		private string[] stateString = new string[]{
			"������������� ���������� . . .",
			"���������� � �������� . . .",
			"�������� ������ . . .",
			"�������� ���� ���������� . . ."
		};

		/// <summary>
		/// Constructor
		/// </summary>
		public SplashScreenForm()
		{
			InitializeComponent();

			string version = Strings.ApplicationVersion;
			string build = Strings.ApplicationBuild;

			this.textBoxAppName.Text = Strings.ApplicationName;
			this.textBoxVershion.Text = String.Format("������ {0}", version);
			this.textBoxBuild.Text = String.Format("������ {0}", build);

			OnSetStatus(0);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashScreenForm));
			this.textBoxAppName = new System.Windows.Forms.Label();
			this.textBoxVershion = new System.Windows.Forms.Label();
			this.textBoxBuild = new System.Windows.Forms.Label();
			this.textBoxState = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// textBoxAppName
			// 
			this.textBoxAppName.BackColor = System.Drawing.Color.White;
			this.textBoxAppName.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxAppName.ForeColor = System.Drawing.Color.Black;
			this.textBoxAppName.Location = new System.Drawing.Point(12, 129);
			this.textBoxAppName.Name = "textBoxAppName";
			this.textBoxAppName.Size = new System.Drawing.Size(397, 20);
			this.textBoxAppName.TabIndex = 3;
			this.textBoxAppName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxVershion
			// 
			this.textBoxVershion.BackColor = System.Drawing.Color.White;
			this.textBoxVershion.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxVershion.ForeColor = System.Drawing.Color.Gray;
			this.textBoxVershion.Location = new System.Drawing.Point(12, 149);
			this.textBoxVershion.Name = "textBoxVershion";
			this.textBoxVershion.Size = new System.Drawing.Size(397, 20);
			this.textBoxVershion.TabIndex = 2;
			this.textBoxVershion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxBuild
			// 
			this.textBoxBuild.BackColor = System.Drawing.Color.White;
			this.textBoxBuild.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxBuild.ForeColor = System.Drawing.Color.Gray;
			this.textBoxBuild.Location = new System.Drawing.Point(12, 169);
			this.textBoxBuild.Name = "textBoxBuild";
			this.textBoxBuild.Size = new System.Drawing.Size(397, 20);
			this.textBoxBuild.TabIndex = 1;
			this.textBoxBuild.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxState
			// 
			this.textBoxState.BackColor = System.Drawing.Color.White;
			this.textBoxState.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxState.ForeColor = System.Drawing.Color.Silver;
			this.textBoxState.Location = new System.Drawing.Point(12, 199);
			this.textBoxState.Name = "textBoxState";
			this.textBoxState.Size = new System.Drawing.Size(397, 20);
			this.textBoxState.TabIndex = 0;
			// 
			// SplashScreenForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.LightGray;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(421, 225);
			this.Controls.Add(this.textBoxState);
			this.Controls.Add(this.textBoxBuild);
			this.Controls.Add(this.textBoxVershion);
			this.Controls.Add(this.textBoxAppName);
			this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SplashScreenForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Ufin";
			this.ResumeLayout(false);

		}
		#endregion

		// ************* Static Methods *************** //

		// A static method to create the thread and 
		// launch the SplashScreenForm.
		static public void ShowSplashScreen()
		{
			// Make sure it's only launched once.
			if( ms_frmSplash != null )
				return;
			ms_oThread = new Thread( new ThreadStart(SplashScreenForm.ShowForm));
			ms_oThread.IsBackground = true;
			ms_oThread.SetApartmentState(ApartmentState.STA);
			ms_oThread.Start();
		}

		// A property returning the splash screen instance
		static public SplashScreenForm SplashForm 
		{
			get
			{
				return ms_frmSplash;
			} 
		}

		// A private entry point for the thread.
		static private void ShowForm()
		{
			ms_frmSplash = new SplashScreenForm();
			Application.Run(ms_frmSplash);
		}

		static public void SetOwner(Form form)
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.Owner = form;
			}
		}
			
		// A static method to close the SplashScreenForm
		static public void CloseForm()
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.OnClose(); 
			}
		}

		public void OnClose()
		{
			timer1 = new System.Threading.Timer(timer1_Tick, null, 1000, 0);
		}

		//********* Event Handlers ************

		// Tick Event handler for the Timer control.  Handle fade in and fade out.  Also
		// handle the smoothed progress bar.
		private void timer1_Tick(object sender)
		{
			timer1.Dispose();
			ms_oThread = null;	// we don't need these any more.
			ms_frmSplash = null;

			//this.Close();
			CloseFormSafe();
		}

		static public void SetStatus(int statusID)
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.OnSetStatus(statusID);
			}
		}

		private void OnSetStatus(int statusID)
		{
			//this.textBoxState.Text = stateString[statusID];
			SetTextBoxStateText(stateString[statusID]);

			if (threadUpdate == null)
				threadUpdate = new System.Threading.Timer(OnThreadUpdate, null, 1000, 0);
			else
				threadUpdate.Change(1000, 0);
		}

		private void OnThreadUpdate(object o)
		{
			//this.textBoxState.Text += " .";
			SetTextBoxStateText(this.textBoxState.Text + " .");

			threadUpdate.Change(1000, 0);
		}

		delegate void CloseFormCallback();
		private void CloseFormSafe()
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (this.InvokeRequired)
			{
				CloseFormCallback d = new CloseFormCallback(CloseFormSafe);
				this.Invoke(d, new object[] { });
			}
			else
			{
				this.Close();
			}
		}

		delegate void SetTextCallback(string text);

		private void SetTextBoxStateText(string text)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (this.textBoxState.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetTextBoxStateText);
				this.textBoxState.Invoke(d, new object[] { text });
			}
			else
			{
				this.textBoxState.Text = text;
			}
		}


		private void OnHide()
		{
			threadUpdate.Change(0, 0);
			Hide();
		}

		private void OnShow()
		{
			threadUpdate.Change(0, 0);
			Show();
		}

		new static public void Hide()
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.OnHide();
			}
		}

		new static public void Show()
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.OnShow(); 
			}
		}

		new static public void Dispose()
		{
			if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
			{
				ms_frmSplash.Dispose(true);
			}
		}
	}
}
