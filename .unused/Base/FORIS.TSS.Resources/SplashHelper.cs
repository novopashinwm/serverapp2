using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace FORIS.TSS.Resources
{
	public interface ISplashForm
	{
		
//		<summary>
//		Use this property to set/get the progess message.
//		</summary>
		string Message { get; set; }
		
//		''' <summary>
//		''' Basic initialisation parameters for splash screen.
//		''' </summary>
//		''' <param name="title">Application title to display.</param>
//		''' <param name="version">Application version to display.</param>
		void Initialise(string title, Version version);

//		''' <summary>
//		''' Hides the splash screen form.
//		''' </summary>
		void Hide();

//		''' <summary>
//		''' Shows the splash screen form as a modal dialog with the specified owner.
//		''' </summary>
//		''' <param name="owner">Any object that implements System.Windows.Forms.IWin32Window that represents the top-level window that will own the modal dialog box.</param>
		void ShowDialog(IWin32Window owner);

//		''' <summary>
//		''' Closes the splash screen form. 
//		''' </summary>
		void Close();
		
		void CloseSplashForm(object sender, EventArgs args);
	}
	
	/// <summary>
	/// Summary description for SplashHelper.
	/// </summary>
	public class SplashHelper
	{
		public event EventHandler Close;
		
		ISplashForm form;
		IWin32Window formOwner;
		Thread thread;
		
		public SplashHelper()
		{
		}
		
		public void ShowSplashForm(IWin32Window owner)
		{
			if(thread == null)
			{
				formOwner = owner;
				
				thread = new Thread(new ThreadStart(ThreadEntryPoint));
				thread.Name = "SplashFormThread";
				//thread.IsBackground = true;
				thread.Start();
			}
		}

		public void CloseSplashForm()
		{
			OnCloseSplashForm(this, null);
		}
		
		private void ThreadEntryPoint()
		{
			form = new SplashForm();
			Close += new EventHandler(form.CloseSplashForm);
			form.ShowDialog(formOwner);
			Trace.WriteLine(" Splash thread stoped");
		}
		
		private void OnCloseSplashForm(object sender, EventArgs args)
		{
			if(Close == null)
				return;
			
			Close(sender, args);
		}
	}
}
