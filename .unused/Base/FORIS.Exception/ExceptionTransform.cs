using System;
using System.IO;
using System.Xml;
using System.Collections;


namespace FORIS.Common.ErrorHandling
{
	/// <summary>
	/// Summary description for ExceptionTransform.
	/// </summary>
	public class ExceptionTransform
	{
		public ExceptionTransform()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		static void TryWriteFORISException( XmlWriter writer, Exception e )
		{
			FORIS.Common.ErrorHandling.FORISException fe = e as FORIS.Common.ErrorHandling.FORISException;
			if( fe == null )
				return;
			writer.WriteStartElement( "Arguments" );
			foreach( DictionaryEntry entry in fe.Arguments )
			{
				writer.WriteStartElement( "Argument" );
				writer.WriteAttributeString( "Name", entry.Key.ToString() );
				writer.WriteAttributeString( "Value", entry.Value.ToString() );
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		static void WriteException( XmlWriter writer, Exception e, bool inner )
		{
			writer.WriteStartElement( "Exception" );
			writer.WriteAttributeString( "Type", e.GetType().FullName );
			writer.WriteAttributeString( "Message", e.Message );
			writer.WriteAttributeString( "StackTrace", e.StackTrace );
			TryWriteFORISException( writer, e );
			if( e.InnerException != null && inner )
				WriteException( writer, e.InnerException, inner );
			writer.WriteEndElement();
		}

		public static string FormatXML( Exception e )
		{
			return FormatXML( e, true );
		}
		
		public static string  FormatXML( Exception e, bool inner )
		{
			StringWriter str_writer = new StringWriter();
			XmlTextWriter writer = new XmlTextWriter( str_writer );
			writer.WriteStartElement( "ExceptionInformation" );
			WriteException( writer, e, true );
			writer.WriteEndElement();
			return str_writer.ToString();
		}
	}
}
