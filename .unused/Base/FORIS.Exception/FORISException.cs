using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Runtime.Serialization;

namespace FORIS.Common.ErrorHandling
{
	[Flags]
	public enum ExceptionFlag
	{
		SaveDataSet
	}
	[Serializable]
	public class FORISException : Exception, ISerializable
	{
		//[NonSerialized]
		Hashtable _arguments = new Hashtable();
		Guid	ExceptionID = Guid.NewGuid();
		ExceptionFlag _flag;
		void Initilize( params object[] arg_value_list  )
		{
			StackFrame frame = new StackFrame( 2 );
			MethodBase method = frame.GetMethod();
			int i = 0;
			foreach( ParameterInfo  param_info in method.GetParameters() )
			{
				if( i < arg_value_list.Length )
				{
					AddAruments( param_info.Name, FormatValue( arg_value_list[i], _flag ) );
				} 
				else
				{
					AddAruments( param_info.Name, FormatValue( null, _flag ) );
				}
				i++;
			}
			Trace.WriteLine(this.ToString() + method.ToString() );
		}
		public FORISException() : base()
		{
		}
		public FORISException( string message ) : base( message )
		{
		}
		public FORISException( string message, Exception innerException) : base( message, innerException )
		{
		}
		protected FORISException(
			SerializationInfo info,
			StreamingContext context
			) : base( info, context )
		{
			DictionaryEntry[] entries = (DictionaryEntry[])info.GetValue( "_arguments", typeof( DictionaryEntry[] ) );
			foreach( DictionaryEntry entry in entries )
			{
				_arguments.Add( entry.Key, entry.Value );
			}
		}
		string FormatValue( object val )
		{
			return FormatValue( val, (ExceptionFlag)0 );
		}
		string FormatValue( object val , ExceptionFlag flag)
		{
			if( val == null )
				return "<null value>";

			if( ( flag & ExceptionFlag.SaveDataSet ) == ExceptionFlag.SaveDataSet )
			{
				Type obj_type = val.GetType();
				if( obj_type.IsSubclassOf( typeof( System.Data.DataSet ) ) )
				{
					System.Data.DataSet ds = val as System.Data.DataSet;
					return ds.GetXml();
				}
				if( obj_type.IsSubclassOf( typeof( System.Data.DataTable ) ) )
				{
					System.Data.DataTable table= val as System.Data.DataTable;
				}
			}
			
			return val.ToString();
		}
		public void AddAruments( string name, string value )
		{
			_arguments[ name ] = value;
		}
		public Hashtable Arguments
		{
			get
			{
				return _arguments;
			}
		}
		static FORISException CreateMethodNotImplemented()
		{
			StackFrame frame = new StackFrame( 1 );
			MethodBase method = frame.GetMethod();
			System.Reflection.ParameterInfo[] args = method.GetParameters();
			string[] args_types = new string[args.Length];
			for(int i = 0; i < args.Length; i++)
			{
				args_types[i] = args[i].ParameterType.Name;
			}
			throw new NotImplementedException(string.Format("Method {0}.{1}({2}) doesn't implemented", method.DeclaringType.Name, method.Name, string.Join(", ", args_types)));
		}
		public override string ToString()
		{
			return string.Format("����� ����������: {0} ", Message );
		}
		public static FORISException FormatWithArguments( string message, Exception innerException, params object[] arg_value_list )
		{
			FORISException exception = new FORISException( message, innerException );
			exception.Initilize( arg_value_list );
			return exception;
		}

		public static FORISException FormatWithArguments( string message, Exception innerException, ExceptionFlag flag, params object[] arg_value_list )
		{
			FORISException exception = new FORISException( message, innerException );
			exception._flag = flag;
			exception.Initilize( arg_value_list );
			return exception;
		}

		public static FORISException FormatWithArguments( string message, ExceptionFlag flag, params object[] arg_value_list )
		{
			FORISException exception = new FORISException( message );
			exception._flag = flag;
			exception.Initilize( arg_value_list );
			return exception;
		}

		public static FORISException FormatWithArguments( string message, params object[] arg_value_list )
		{
			FORISException exception = new FORISException( message );
			exception.Initilize( arg_value_list );
			return exception;
		}

		public static FORISException Format( string message, params DictionaryEntry[] entries )
		{
			FORISException exception = new FORISException( message );
			foreach( DictionaryEntry entry in entries )
			{
				exception._arguments.Add( entry.Key.ToString(), exception.FormatValue( entry.Value ) );
			}
			return exception;
		}
		public static FORISException Format( string message, Exception innerException, params DictionaryEntry[] entries )
		{
			FORISException exception = new FORISException( message, innerException );
			foreach( DictionaryEntry entry in entries )
			{
				exception._arguments.Add( entry.Key.ToString(), exception.FormatValue( entry.Value ) );
			}
			return exception;
		}
		public static FORISException Format( string message, ExceptionFlag flag, Exception innerException, params DictionaryEntry[] entries )
		{
			FORISException exception = new FORISException( message, innerException );
			exception._flag = flag;
			foreach( DictionaryEntry entry in entries )
			{
				exception._arguments.Add( entry.Key.ToString(), exception.FormatValue( entry.Value ) );
			}
			return exception;
		}
		#region ISerializable Members

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			ArrayList arr = new ArrayList();
			foreach( DictionaryEntry entry in _arguments )
			{
				arr.Add( entry );
			}
			info.AddValue( "_arguments", arr.ToArray( typeof( DictionaryEntry ) ) );
			base.GetObjectData( info, context );
		}

		#endregion
	}


	[Serializable]
	public class ADCacheInitialisationException: Exception, ISerializable
	{
		public ADCacheInitialisationException (): base () {}
		public ADCacheInitialisationException (String message): base (message) {}
		public ADCacheInitialisationException (String message, Exception innerException): base (message, innerException) {}

		protected ADCacheInitialisationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
		
        
		void ISerializable.GetObjectData (SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData (info, context);
		}
	}
}
