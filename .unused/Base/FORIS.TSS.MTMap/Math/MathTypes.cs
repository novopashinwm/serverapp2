// xml_summary_tag.cs
using System;
using FORIS.TSS.BusinessLogic.Map;
using MTMap.Common;
using MTMap.LineMath;
using System.Collections;
using System.Diagnostics;

namespace MTMap.MathTypes 
{
	/// <summary>
	/// ���������� �������� ���������.
	/// </summary>
	public class MathConst {

		/// <summary>
		/// �������� ��������� ���������
		/// </summary>
		public const float NearlyZero = 1E-01f; 

		/// <summary>
		/// �������� ��������� �����, ��������� � �.�.
		/// </summary>
		public const double NearlyZero2 = 1E-08; 

		/// <summary>
		/// �������� �������������
		/// </summary>
		public const float Infinity = 1E+20f; 

		/// <summary>
		/// �������� ����
		/// </summary>
		public const double AboutZero = 1E-18; 

		/// <summary>
		/// ����� "��"
		/// </summary>
		public const double PI = 3.1415926535897932385;

		/// <summary>
		/// ����� "2��"
		/// </summary>
		public const double twoPI = 3.1415926535897932385 * 2;
	}
	/// <summary>
	/// �������������� �������
	/// </summary>
	public class MathFunc 
	{
		/// <summary>
		/// ��������� �������������� �����
		/// </summary>
		/// <returns> 0 - a � b ����� </returns>
		/// <returns> 1 - a ������ b </returns>
		/// <returns>-1 - a ������ b </returns>
		public static int CompareDouble( double a, double b)
		{
			if ( a > b + MathConst.NearlyZero)
				return +1;
			else if ( a < b - MathConst.NearlyZero)
				return -1;
			else
				return 0;
		}
		/// <summary>
		/// ������������� �������� ����, ���� ����� ���������� � �������� (-Pi..+Pi].
		/// </summary>
		public static double normAngle( double angle) 
		{
			while ( angle > +MathConst.PI) 
				angle = angle - MathConst.twoPI;
			
			while ( angle <= -MathConst.PI) 
				angle = angle + MathConst.twoPI;
			
			return angle;
		}

		/// <summary>
		/// ���������� ����, ��������������� ��������� ����� � ������������ (dX,dY) 
		/// ������������ ����� ������ ��������� � ��������� (-PI..+PI].
		/// </summary>
		/// <param name="dY"></param>
		/// <param name="dX"></param>
		/// <returns>���� � ��������, ������ ������� �������, 0 ������������� ����������� ��� X</returns>
		public static double ArcTan2Ext( double dY, double dX)
		{
			if (dY != 0)
				if (Math.Abs(dX) < MathConst.NearlyZero2)
					if (dY > 0)
						return +MathConst.PI / 2;
					else
						return -MathConst.PI / 2;
				else
					return Math.Atan2(dY, dX);
			else
				if (dX >= 0)
				return 0;
			else
				return +MathConst.PI;
		}

		/// <summary>
		/// ��������� ����� �� ���������� �������.
		/// </summary>
		/// <param name="aValue">����������� ��������</param>
		/// <param name="aDigit">���������� ������ ����� �������</param>
		public static double roundTo( double aValue, int aDigit)
		{
			double lFactor;
			lFactor = Math.Pow( 10, aDigit);
			return Math.Round( aValue / lFactor) * lFactor;
		}

		/// <summary>
		/// ������������ ��� ������ � ���������� �������������� ����� � �������.
		/// </summary>
		public class FloatComparer: IComparer
		{
			/// <summary>
			/// ���������� ��� �������������� ����� (float).
			/// </summary>
			public int Compare(object x, object y)
			{
				float first = ( float) x;
				float second = ( float) y;
				if ( first < second)
					return -1;
				else
				if ( first > second)
					return +1;
				else
					return 0;
			}
		}

		/// <summary>
		/// ������������ ��� ������ � ���������� �������������� ����� � �������.
		/// </summary>
		public static FloatComparer floatComparer = new FloatComparer();
	}

	/// <summary>
	/// ������� ��������� �������� ����� ��� ������� ������� �����.
	/// </summary>
	public class Math2D
	{
		/// <summary>
		/// �������� ���������� ����� ������������ ��������
		/// </summary>
		public enum PointInPolygonState 
		{
			/// <summary>
			/// ������� �������� �����������
			/// </summary>
			ppsNoPolygon, 
			/// <summary>
			/// ����� ����� ��� ��������
			/// </summary>
			ppsOutside, 
			/// <summary>
			/// ����� ����� ������ ��������
			/// </summary>
			ppsInside, 
			/// <summary>
			/// ����� ����� �� ������� ��������
			/// </summary>
			ppsOnEdge
		};

		private const int b0  = 0x001;   // AB ����� �� ������������ � l
		private const int b1  = 0x002;   // AB ������������ � l
		private const int b2  = 0x004;   // P == A
		private const int b3  = 0x008;   // P == B
		private const int b4  = 0x010;   // P ����� ������ AB, AB ����� ��������� ����� ����� � l
		private const int b5  = 0x020;   // B ����� �� l, ��������� ������� ���� ���� ��� b8
		private const int b6  = 0x040;   // A ����� �� l, ��������� ������� ���� ���� ��� b8
		private const int b7  = 0x080;   // ��������� ���������� �����������
		private const int b8  = 0x100;   // ��������� ������� ����

		private const int isOnEdgeMask    = b2 + b3 + b4;
		private const int isOnVertexMask  = b2 + b3;
		private const int isExtTurnMask   = b5 + b6;
		private const int NeedCalcMask    = b7;

		/// <summary>
		/// ����� ������������.
		/// </summary>
		public const int fCrossing		= 0x001;
		/// <summary>
		/// ����� ����������� ����� � �������� ����� �������.
		/// </summary>
		public const int fOnThisPiece	= 0x002;
		/// <summary>
		/// ����� ����������� ����� � �������� ������� ������� (����������� ������� � �������� ���������).
		/// </summary>
		public const int fOnParamPiece	= 0x004;
		/// <summary>
		/// ����� ����������� ����� � �������� ����� ��������.
		/// </summary>
		public const int fOnBothPieces = fCrossing | fOnThisPiece | fOnParamPiece;
	
		private static readonly int[,,,] m = new int[3,3,3,3] 
		{
			{
				{
					{1, 1, 1}, {1, 8, 32}, {1, 1, 128}
				}, 
				{
					{1, 1, 1}, {1, 8, 32}, {1, 16, 2}
				}, 
				{
					{1, 1, 1}, {1, 8, 32}, {128, 2, 2}
				}
			}, 
			{
				{
					{1, 1, 1}, {1, 8, 16}, {1, 1, 1}
				}, 
				{
					{4, 4, 4}, {4, 4, 4}, {4, 4, 4}
				}, 
				{
					{64, 64, 64}, {80, 72, 96}, {320, 320, 320}
				}
			}, 
			{
				{
					{1, 1, 128}, {1, 8, 288}, {1, 1, 1}
				}, 
				{
					{1, 16, 2}, {1, 8, 288}, {1, 1, 1}
				}, 
				{
					{128, 2, 2}, {1, 8, 288}, {1, 1, 1}
				}
			}
		};

		/// <summary>
		/// ��������������� �������, ���������� �� ������� findPointInPolygon.
		/// </summary>
		protected static void calcLP( ref DLineMath sL, ref DPoint bP, bool rightDir, int pointCount, int iA, 
			ref DPointList points, ref DPoint p, ref double r)
		{
			DLineMath pL = new DLineMath(), tL = new DLineMath();
			int jA, jB;
			int  cK;
			DPoint pCur;
			float tMin, tCur, dCur, dTmp;

			pL.createPerpendOnBP( ref sL, ref bP, rightDir);

			tMin = MathConst.Infinity;
			for ( jA = 0; jA < pointCount; jA++)
			{
				if ( jA == iA) continue;

				jB = ( jA + 1) % pointCount;

				tL.create(ref points.points[jA], ref points.points[jB]);
				if ( tL.pieceLength() < MathConst.NearlyZero) continue;

				cK = pL.crossPiece( ref tL, out tCur);
				if (( Math2D.fCrossing & cK) != 0)
				{
					if ((( Math2D.fOnParamPiece & cK) != 0) && ( tCur > MathConst.NearlyZero2) && ( tCur < tMin))
						tMin = tCur;
				}
				else 
				{
					tCur = pL.lineDistanceTo( ref points.points[ jA]) / pL.pieceLength();
					if ( ( tCur < tMin))
					tMin = tCur;
					tCur = pL.lineDistanceTo( ref points.points[ jB]) / pL.pieceLength();
					if ( ( tCur < tMin))
					tMin = tCur;
				}
			}

			if ( tMin < MathConst.Infinity) 
			{
				tMin = tMin / 2;
				dCur = pL.pieceLength() * tMin;
				pCur = pL.calcXY( tMin);

				for ( jA = 0; jA < pointCount; jA++)
				{
					jB = ( jA + 1) % pointCount;
					tL.create( ref points.points[jA], ref points.points[jB]);
					if ( tL.pieceLength() < MathConst.NearlyZero) continue;

					dTmp = tL.pieceDistanceTo( ref pCur);
					if ( dTmp < dCur)
					dCur = dTmp;
				}

				if (( dCur > r) && ( isPointIn�ontour( pCur, points) == PointInPolygonState.ppsInside)) 
				{
					r = dCur;
					p = pCur;
				}
			}
		}

		/// <summary>
		/// ��������� ������� ������������ �� ������.
		/// </summary>
		public static float calcSignedTriangleArea(ref DPoint a,ref DPoint b,ref DPoint c)
		{
			return ( float) 0.5 * ( a.x * ( b.y - c.y) + b.x * ( c.y - a.y) + c.x * ( a.y - b.y));
		}
		
		/// <summary>
		/// ����������, �������� �� �������� ������ �������������� (�.�. ����� 
		/// ����� ����� ������� ���� � ����������� �� ������� �������).
		/// </summary>
		public static bool isContourRight( ref DPointList poly, int polySize)
		{
			int i, a, b, c;
			DPoint mx;

			b = 0;
			mx = poly.points[ b];
			for ( i = 1; i < polySize; i++)
				if (( poly.points[ i].y > mx.y) || (( poly.points[ i].y == mx.y) && ( poly.points[ i].x > mx.x)))
				{
					mx = poly.points[ i];
					b = i;
				}

			c = ( b + 1) % polySize;                 // �������� �����
			a = ( b - 1 + polySize) % polySize;      // ���������� �����
			return calcSignedTriangleArea(ref poly.points[ a], ref poly.points[ b], ref poly.points[ c]) < 0;
		}

		/// <summary>
		/// ������� ����� ������ �������� (��� �� �������������� �����).
		/// </summary>
		public static void findPointInPolygon( ref DPointList points, out DPoint p, out double r)
		{
			int iA, iB;
			int pointCount = points.points.Length;
			DLineMath sL = new DLineMath();
			bool rightDir;

			if ( pointCount < 3)
			{
				if ( pointCount < 1)
				{
					r = MathConst.Infinity;
					p = new DPoint( MathConst.Infinity, MathConst.Infinity);
				}
				else
				{
					r = 0;
					p = points.points[ 0];
				}
				return;
			}

			p = points.points[ 0];
			rightDir = isContourRight( ref points, pointCount);
			r = -MathConst.Infinity;
			for ( iA = 0; iA < pointCount; iA++)
			{
				iB = ( iA + 1) % pointCount;
				sL.create(ref points.points[iA], ref points.points[iB]);

				if ( sL.pieceLength() < MathConst.NearlyZero) continue;

				calcLP( ref sL, ref points.points[ iA], rightDir, pointCount, iA, ref points, ref p, ref r);
				calcLP( ref sL, ref points.points[ iB], rightDir, pointCount, iA, ref points, ref p, ref r);
				DPoint tempPoint = sL.calcXY( 0.5f);
				calcLP( ref sL, ref tempPoint,   rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY( 0.33f);
				calcLP( ref sL, ref tempPoint,  rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY( 0.67f);
				calcLP( ref sL, ref tempPoint,  rightDir, pointCount, iA, ref points, ref p, ref r);
			}
		}


		/// <summary>
		/// ��������: ��������� �� ����� ������ ������� ��������
		/// </summary>
		public static PointInPolygonState isPointIn�ontour(DPoint p, DPointList poly)
		{
			int polySize = poly.points.Length;
			int v;
			double x;
			DPoint a, b;
			int turnCount, i;
			int a_on_X, b_on_X, a_on_Y, b_on_Y;

			if ( polySize < 2) 
				return PointInPolygonState.ppsNoPolygon;

			turnCount = 0;
			b = poly.points[ polySize - 1];
			b_on_X = MathFunc.CompareDouble( b.x, p.x) + 1;
			b_on_Y = MathFunc.CompareDouble( b.y, p.y) + 1;
			for ( i = 0; i <= polySize - 1; i++)
			{
				a = b;
				a_on_X = b_on_X;
				a_on_Y = b_on_Y;
				b = poly.points[ i];
				b_on_X = MathFunc.CompareDouble( b.x, p.x) + 1;
				b_on_Y = MathFunc.CompareDouble( b.y, p.y) + 1;

				v = m[ a_on_Y, a_on_X, b_on_Y, b_on_X];

				if ( v != b0) 
				{
					if (( v & isOnEdgeMask) != 0)
						return PointInPolygonState.ppsOnEdge;

					if (( v & b1) != 0)
						turnCount ++;
					else if (( v & NeedCalcMask) != 0)
					{
						x = ( b.x - a.x) * ( p.y - a.y) / ( b.y - a.y) + a.x;
						if ( Math.Abs( x - p.x) < MathConst.NearlyZero)
							return PointInPolygonState.ppsOnEdge;

						if ( x > p.x)
							turnCount++;
					}
					else if (( v & b8) != 0)
						turnCount++;
				}
			}

			if (( turnCount & 1) != 0)
				return PointInPolygonState.ppsInside;
			else
				return PointInPolygonState.ppsOutside;
		}

		private static DPoint iPoint( int i, ref DPointList polygon, int nPoints)
		{
			return polygon.points[(i + nPoints) / nPoints];
		}

		/// <summary>
		/// ��������� �������� ��������/���������. ��� ��������� � ��������� ��������� 
		/// </summary>
		public static float calcPerimeter( ref DPointList points, bool closed)
		{
			float s = 0;
			int nPoints = points.points.Length;
			for ( int i = nPoints - 2; i >= 0; i--)
				s += points.points[ i].distanceTo( points.points[ i + 1]);
			if ( closed)
				s += points.points[ 0].distanceTo( points.points[ nPoints - 1]);
			return s;
		}

		/// <summary>
		/// ��������� ������� �������� �� ������, ��� ������� �������� ������� ������������.
		/// </summary>
		public static float calcSignedPolygonArea( ref DPointList polygon, int nPoints)
		{
			double s, s1;
			int i;
			s = 0;
			for (i = 0; i <= nPoints - 1; i++)
			{
				s1 = iPoint( i, ref polygon, nPoints).x * ( iPoint( i - 1, ref polygon, nPoints).y 
					- iPoint(i + 1, ref polygon, nPoints).y);
				s = s + s1;
			}
			return ( float) (s / 2);
		}

		/// <summary>
		/// ��������� ���������� �� ��������� �� �����.
		/// </summary>
		public static float polylineToPointDistance( ref DPointList vPoints, bool closed, ref DPoint sample)
		{
			long pieceCount = vPoints.points.Length - 1;
			if( pieceCount < 0 )
				return ( float)MathConst.Infinity;
			if( pieceCount == 0 )
				return vPoints.points[0].distanceTo( sample);
			
			DPoint P1;
			DPoint P2 = vPoints.points[ 0];
			DPoint P0;
			float MinDist = MathConst.Infinity, Dist;
			DLineMath l = new DLineMath();

			long StartPInd = 0;
			for( long i = 1; i <= pieceCount; i++)
			{
				P1 = P2;
				P2 = vPoints.points[ i];
				l.create( ref P1, ref P2);
				float t;
				Dist = l.pieceDistanceToExt( ref sample, out t);
				if ( Dist < MinDist - MathConst.NearlyZero)
					MinDist = Dist;
			}
			
			if ( closed)
			{
				P1 = P2;
				P0 = vPoints.points[ StartPInd];
				l.create( ref P1, ref P0);
				float t;
				Dist = l.pieceDistanceToExt( ref sample, out t);
				if ( Dist < MinDist) 
					MinDist = Dist;
			}
				
			return MinDist;
		}
	}
	
	/// <summary>
	/// ������� ������
	/// </summary>
	public struct DLine 
	{
		/// <summary>
		/// ������ �����, ����� ������� �������� �������.
		/// </summary>
		public DPoint a;
		/// <summary>
		/// ������ �����, ����� ������� �������� �������.
		/// </summary>
		public DPoint b;
		
		/// <summary>
		/// ��������� ����� �������
		/// </summary>
		public double length() 
		{
			return a.distanceTo( b);
		}

		/// <summary>
		/// ��������� ���������� �������� ������� � ���������� �� � �������� ����������
		/// </summary>
		public DPoint getCenter() 
		{
			return new DPoint((a.x + b.x) / 2, (a.y + b.y) / 2);
		}

		/// <summary>
		/// ������ ���� �� ������.
		/// </summary>
		public void read(Serializer s) 
		{
			a = s._DPoint();
			b = s._DPoint();
		}

		/// <summary>
		/// ����� ���� � �����.
		/// </summary>
		public void write(Serializer s) 
		{
			s._DPoint(a);
			s._DPoint(b);
		}
	}

	/// <summary>
	/// ������������� �������� ��������������� �������.
	/// </summary>
	public struct DExtents
	{
		/// <summary>
		/// ����������� ���������� ��������.
		/// </summary>
		public DPoint min;
		/// <summary>
		/// ������������ ���������� ��������.
		/// </summary>
		public DPoint max;

		public DExtents(
			DPoint min,
			DPoint max
			)
		{
			this.min = min;
			this.max = max;
		}

		/// <summary>
		/// ������������ � �������������� ����� ��������
		/// </summary>
		public DPoint centerPoint 
		{
			get 
			{
				if( this.IsEmpty )
				{
					throw new InvalidOperationException();
				}

				return new DPoint((min.x + max.x) / 2, (min.y + max.y) / 2);
			}
		}
		
		/// <summary>
		/// ������ ��������
		/// </summary>
		public double dX 
		{
			get 
			{
				return max.x - min.x;
			}
		}

		/// <summary>
		/// ������ ��������
		/// </summary>
		public double dY 
		{
			get 
			{
				return max.y - min.y;
			}
		}

		public bool IsEmpty
		{
			get { return this.min.IsEmpty && this.max.IsEmpty; }
		}

		/// <summary>
		/// ���������� ����������� ��������
		/// </summary>
		public void setEmpty()  
		{
			this.min = DExtents.Empty.min;
			this.max = DExtents.Empty.max;
		}
		/// <summary>
		/// ���������� ������������ ��������
		/// </summary>
		public void setMax() 
		{
			min.x = (float)-MathConst.Infinity;
			min.y = (float)-MathConst.Infinity;
			max.x = (float)+MathConst.Infinity;
			max.y = (float)+MathConst.Infinity;
		}

		/// <summary>
		/// ������ ���� �� ������.
		/// </summary>
		public void read(Serializer s) 
		{
			min = s._DPoint();
			max = s._DPoint();
		}

		/// <summary>
		/// ����� ���� � �����.
		/// </summary>
		public void write(Serializer s) 
		{
			s._DPoint(min);
			s._DPoint(max);
		}
		
		/// <summary>
		/// ���������, ������������ �� (��� �������� ��) ��� �������� � ��������.
		/// </summary>
		public bool isIntersects(ref DExtents extents) 
		{
			return
				!this.IsEmpty
					? ( extents.max.x >= min.x ) &&
					  ( extents.min.x <= max.x ) &&
					  ( extents.max.y >= min.y ) &&
					  ( extents.min.y <= max.y )
					: false;
		}

		public bool isIntersects( DExtents extent )
		{
			return
				!this.IsEmpty
					? ( extent.max.x >= min.x ) && ( extent.min.x <= max.x ) &&
					  ( extent.max.y >= min.y ) && ( extent.min.y <= max.y )
					: false;
		}
	
		/// <summary>
		/// ��������� �������� ���, ����� �������� ����� point ��������� �������� � ��� ��������
		/// </summary>
		public void includePoint(ref DPoint point) 
		{
			if( this.IsEmpty )
			{
				this.min = point;
				this.max = point;
			}
			else
			{
				if( point.x < min.x ) min.x = point.x;
				if( point.x > max.x ) max.x = point.x;
				if( point.y < min.y ) min.y = point.y;
				if( point.y > max.y ) max.y = point.y;
			}
		}

		public void includePoint( params DPoint[] points )
		{
			for( int i = 0; i < points.Length; i++ )
			{
				this.includePoint( ref points[i] );
			}
		}

		/// <summary>
		/// ��������� �������� ���, ����� �������� �������� 
		/// (extent) ��������� �������� � ��� ��������
		/// </summary>
		public void includeExtents(ref DExtents extent) 
		{
			this.includePoint( extent.min );
			this.includePoint( extent.max );
		}
		
		/// <summary>
		/// �������� � �������� �������� �������
		/// </summary>
		public void includeLine(ref DLine line) 
		{
			this.includePoint(ref line.a);
			this.includePoint(ref line.b);
		}

		/// <summary>
		/// �������� � �������� ��� ����� ��������� ������ �����
		/// </summary>
		public void includePointList(ref DPointList pointList) 
		{
			for( int i = 0; i < pointList.points.Length; i++ )
			{
				includePoint( ref pointList.points[i] );
			}
		}

		public static readonly DExtents Empty = new DExtents( DPoint.Empty , DPoint.Empty );
	}

	/// <summary>
	/// ������ ����� � ������������
	/// </summary>
	public struct DPointList 
	{
		/// <summary>
		/// ������ ��������� �����.
		/// </summary>
		public DPoint[] points;

		/// <summary>
		/// ������ ���-�� �����.
		/// </summary>
		/// <param name="size">����� ���-�� �����</param>
		public void setSize(int size) 
		{
			points = new DPoint[size];
		}

		/// <summary>
		/// ������ ���� �� ������.
		/// </summary>
		public void read(Serializer s) 
		{
			setSize(s._int());
			for(int i = 0; i < points.Length; i++) 
				points[i] = s._DPoint();
		}

		/// <summary>
		/// ����� ���� � �����.
		/// </summary>
		public void write(Serializer s) 
		{
			s._int(points.Length);
			for(int i = 0; i < points.Length; i++) 
				s._DPoint(points[i]);
		}

		public DPoint[] removeLast(int count)
		{
			DPoint[] points=new DPoint[this.points.Length-count];
			for (int i=0; i<this.points.Length-count; i++)
				points[i]=this.points[i];
			return points;
												
		}
		/// <summary>
		/// ��������� ���������� �����, �������� ���������� t �� ���� ���������.
		/// </summary>
		/// <param name="t">����� ����� ���������� ����� ������� � ���������, ������� - ������ ������� ������ �������; ������ ���� � �������� [0..points.Length]</param>
		/// <returns>���������� ���������� �����</returns>
		public DPoint calcPointByT( float t)
		{
			int aIndex = ( int) Math.Floor( t); 
			DLineMath l = new DLineMath();
			l.create( ref points[ aIndex], ref points[ aIndex + 1]);
			return l.calcXY( t - aIndex);
		}

		/// <summary>
		/// ��������� ���������� ����� � � �������� (t), ����������� �� ���������� dist ����� ��������� 
		/// ���� ���������, ������� �� �����, ������������� �� ���� �� ���������, �������� ���������� fromT.
		/// </summary>
		/// <param name="fromT">�������� �������� �����, �� ������� ������������� �������� ���������� (dist)</param>
		/// <param name="dist">���������� ����� ���������</param>
		/// <param name="t">���������� �������� ��������� �����</param>
		/// <returns>���������� true, ���� ������� ����� ��������� � ������ ���������</returns>
		public bool calcPointTAtDistanceFrom( float fromT, float dist, out float t)
		{
			int fromP1Index = ( int) Math.Floor( fromT);
			DLineMath l = new DLineMath();
			l.create( ref points[ fromP1Index], ref points[ fromP1Index + 1]);
			DPoint fromP = l.calcXY( fromT - ( float) fromP1Index);
			dist += points[ fromP1Index].distanceTo( fromP);
			do
			{
				l.create( ref points[ fromP1Index], ref points[ fromP1Index + 1]);
				float pl = l.pieceLength();
				if ( pl < dist)
					dist -= pl;
				else
				{
					DPoint p;
					t = fromP1Index + l.calcPointAtDistFromA( dist, out p);
					return true;
				}
				fromP1Index++;
			}
			while ( fromP1Index < points.Length - 1);
			t = fromP1Index;
			return false;
		}

		/// <summary>
		/// ��������� ���������� ����� ��������� (�������������� ���� ������� �����) 
		/// ����� ������� �� ���, ��������� ��������������.
		/// </summary>
		/// <param name="startT">������ ����� �� ���������</param>
		/// <param name="endT">������ ����� �� ���������</param>
		/// <returns>����������� ����������</returns>
		public float calcDistanceBetweenTs( float startT, float endT)
		{
			Debug.Assert( startT <= endT);

			int s = ( int) Math.Ceiling( startT);
			int e = ( int) Math.Floor( endT);

			if ( e < s)
				return calcPointByT( startT).distanceTo( calcPointByT( endT));

			float dist = 0;
			for ( int i = s; i < e; i++)
				dist += points[ i].distanceTo( points[ i + 1]);

			if ( startT != ( float) s)
				dist += calcPointByT( startT).distanceTo( points[ s]);

			if ( endT != ( float) e)
				dist += calcPointByT( startT).distanceTo( points[ s]);

			return dist;
		}

		/// <summary>
		/// ����� ����� ������� ������� � �������� ����� ���������, �������� �������������� 
		/// ��������� startT � endT. � ������� ����� ���������� ������� � ��������������� �������������.
		/// </summary>
		public float findLargestPieceCenter( float startT, float endT)
		{
			int s = ( int) Math.Ceiling( startT);
			int e = ( int) Math.Floor( endT);

			if ( e < s)
				return ( startT + endT) / 2;

			float foundT = -1;
			float foundDist = -1;
			for ( int i = s; i < e; i++)
			{
				float d = points[ i].distanceTo( points[ i + 1]);
				if ( d > foundDist)
				{
					foundDist = d;
					foundT = 0.5f + i;
				}
			}

			if ( startT != ( float) s)
			{
				DPoint p = calcPointByT( startT);
				float d = points[ s].distanceTo( p);
				if ( d > foundDist)
				{
					foundDist = d;
					foundT = ( s + startT) / 2;
				}
			}

			if ( endT != ( float) e)
			{
				DPoint p = calcPointByT( endT);
				float d = points[ e].distanceTo( p);
				if ( d > foundDist)
				{
					foundDist = d;
					foundT = ( e + endT) / 2;
				}
			}
			return foundT;
		}

		/// <summary>
		/// ��������� �������� ���������.
		/// </summary>
		/// <param name="closed">true - ��� �������� ����������� ���������</param>
		/// <returns>����������� �������� ���������</returns>
		public float calcPerimeter( bool closed)
		{
			return Math2D.calcPerimeter( ref this, closed);
		}

		/// <summary>
		/// ����� ��� ����������� ���� ��������� � �������� ���������� � ������� ������
		/// ����� ����������� � ��������������� �������������.
		/// </summary>
		/// <param name="pp">������ ���������</param>
		/// <param name="crossTs">����������� ����� �����������</param>
		public void calcAllCrossesWith( ref DPointList pp, ArrayList crossTs)
		{
			DLineMath d = new DLineMath();
			DLineMath s = new DLineMath();
			for ( int i = points.Length - 2; i >= 0; i--)
			{
				s.create( ref points[ i], ref points[ i + 1]);
				for ( int j = pp.points.Length - 2; j >= 0; j--)
				{
					d.create( ref pp.points[ j], ref pp.points[ j + 1]);
					float t;
					if ( s.crossPiece( ref d, out t) == Math2D.fOnBothPieces)
						crossTs.Add( t);
				}
			} 
		}
	}
}
