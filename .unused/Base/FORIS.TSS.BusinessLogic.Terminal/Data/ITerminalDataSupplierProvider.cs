using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Data
{
    public interface ITspTerminalDataSupplierProvider:
		IControllerDataSupplierProvider
    {

    }

	public interface IControllerDataSupplierProvider
	{
		IControllerDataSupplier GetControllerDataSupplier();
	}
}
