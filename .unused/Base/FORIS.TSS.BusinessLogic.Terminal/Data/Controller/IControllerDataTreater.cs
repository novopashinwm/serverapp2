using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller
{
    public interface IControllerDataTreater:
		IDataTreater
	{
		#region ControllerType

    	void ControllerTypeInsert(
			string name,
            ControllerTypeSettings settings
    		);

    	void ControllerTypeUpdate(
			int controllerTypeId,
			string name,
            ControllerTypeSettings settings
    		);

    	void ControllerTypeDelete(
			int controllerTypeId
    		);

		#endregion // ControllerType

		#region Controller

		void ControllerInsert(
			int controllerType,
			string phone,
			int number,
			string firmware,
			long imei
            );

        void ControllerUpdate(
            int controllerId,
			int controllerType,
			string phone,
			int number,
			string firmware,
			long imei,
            ControllerSettings settings
            );

    	void ControllerDelete(
			int controllerId
    		);

		#endregion // Controller

        #region ControllerSensorType

        void ControllerSensorTypeInsert(string Name);
        void ControllerSensorTypeUpdate(int id, string Name);
        void ControllerSensorTypeDelete(int id);

        #endregion ControllerSensorType

        #region ControllerSensorLegend

        void ControllerSensorLegendInsert(string name, int iDSensorType);
        void ControllerSensorLegendUpdate(int id, string name, int iDSensorType);
        void ControllerSensorLegendDelete(int id);

        #endregion //ControllerSensorLegend

        #region ControllerSensor

        void ControllerSensorInsert(int controllerType, int sensorType, int number, int minValue, int maxValue, int defaultValue);
        void ControllerSensorUpdate(int id, int controllerType, int sensorType, int number, int minValue, int maxValue, int defaultValue);
        void ControllerSensorDelete(int id);

        #endregion //ControllerSensor

        #region ControllerSensorMap

        void ControllerSensorMapInsert(int controller, int controllerSensor, 
                                   int controllerSensorLegend, string description);

        void ControllerSensorMapUpdate(int id, int controller, int controllerSensor, int controllerSensorLegend,
                                       string description);

        void ControllerSensorMapDelete(int id);

        #endregion // ControllerSensorMap
    }
}
