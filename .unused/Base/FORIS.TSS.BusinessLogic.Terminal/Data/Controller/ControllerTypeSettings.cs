using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Text;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller
{
    [Serializable]
    public class ControllerTypeSettings : ControllerSettingsBase
    {
        
        public ControllerTypeSettings()
        {
            sendingProtocolPriority.AddRange(new string[] { "UDP", "TCP", "SMS", "GSM" });
        }

        /// <summary>
        /// ������ ����� ���������� ����������� �����������, � ������������ � �����������, ����� ��������
        /// </summary>
        [Browsable(true)]
        [Category("Settings")]
        [Description("Sets priority of the channels for data sending")]
        public List<string> SendingProtocolPriority
        {
            get { return sendingProtocolPriority; }
        }

        
        [Browsable(true)]
        [Category("Settings")]
        [Description("Controller settings")]
        public Dictionary<string, object> Settings
        {
            get{ return this.settings; }
        }
    }

    [Serializable]
    public abstract class ControllerSettingsBase
    {
        protected readonly List<string> sendingProtocolPriority = new List<string>();
        protected readonly Dictionary<string, object> settings = new Dictionary<string, object>();

        public byte[] ToByteArray()
        {
            List<byte> bytes = new List<byte>();

            bytes.Add((byte)settings.Count);

            foreach (KeyValuePair<string, object> setting in settings)
            {
                bytes.Add((byte)setting.Key.Length);
                bytes.AddRange(ASCIIEncoding.ASCII.GetBytes(setting.Key));

                if (setting.Value is bool)
                {
                    bytes.Add(0);
                    byte[] data = BitConverter.GetBytes((bool)setting.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (setting.Value is Int32)
                {
                    bytes.Add(1);
                    byte[] data = BitConverter.GetBytes((Int32)setting.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (setting.Value is double)
                {
                    bytes.Add(2);
                    byte[] data = BitConverter.GetBytes((double)setting.Value);
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (setting.Value is IPAddress)
                {
                    bytes.Add(3);

                    IPAddress val = (IPAddress)setting.Value;
                    byte[] data = val.GetAddressBytes();
                    bytes.Add((byte)data.Length);
                    bytes.AddRange(data);
                }
                else if (setting.Value is string)
                {
                    string val = (string)setting.Value;
                    bytes.Add(4);
                    bytes.Add((byte)val.Length);
                    bytes.AddRange(ASCIIEncoding.ASCII.GetBytes(val));
                }
            }

            bytes.Add((byte)this.sendingProtocolPriority.Count);
            foreach (string s in sendingProtocolPriority)
            {
                bytes.Add((byte)s.Length);
                bytes.AddRange(ASCIIEncoding.ASCII.GetBytes(s));
            }

            return bytes.ToArray();
        }

        public void CreateFromByteArray(byte[] data)
        {
            this.settings.Clear();

            List<byte> bytes = new List<byte>(data);

            int counter = 0;

            int count = bytes[counter++];

            for (int i = 0; i < count; i++)
            {
                int len = bytes[counter++];

                string key = ASCIIEncoding.ASCII.GetString(bytes.GetRange(counter, len).ToArray());
                counter += len;

                int type = bytes[counter++];
                len = bytes[counter++];

                switch (type)
                {
                    case 0:
                        byte[] bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        bool bv = BitConverter.ToBoolean(bts, 0);
                        this.settings.Add(key, bv);
                        break;
                    case 1:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        int iv = BitConverter.ToInt32(bts, 0);
                        this.settings.Add(key, iv);
                        break;
                    case 2:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        double dv = BitConverter.ToDouble(bts, 0);
                        this.settings.Add(key, dv);
                        break;
                    case 3:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        IPAddress ipv = new IPAddress(bts);
                        this.settings.Add(key, ipv);
                        break;
                    case 4:
                        bts = bytes.GetRange(counter, len).ToArray();
                        counter += len;
                        string sv = ASCIIEncoding.ASCII.GetString(bts);
                        this.settings.Add(key, sv);
                        break;
                    default:
                        throw new Exception("������������ ��� ������");
                }
            }
            if (counter >= data.Length)
                return;
            sendingProtocolPriority.Clear();
            count = bytes[counter++];
            for (int i = 0; i < count; i++)
            {
                int len = bytes[counter++];
                byte[] bts = bytes.GetRange(counter, len).ToArray();
                counter += len;
                string sv = ASCIIEncoding.ASCII.GetString(bts);
                this.sendingProtocolPriority.Add(sv);
            }
        }
    }
}
