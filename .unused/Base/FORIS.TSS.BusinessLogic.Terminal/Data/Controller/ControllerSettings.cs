using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller
{
    [Serializable]
    public class ControllerSettings : ControllerSettingsBase
    {
        public ControllerSettings()
        {
            sendingProtocolPriority.AddRange(new string[] { "UDP", "TCP", "SMS", "GSM" });
        }

        /// <summary>
        /// ������ ����� ���������� ����������� �����������, � ������������ � �����������, ����� ��������
        /// </summary>
        [Browsable(true)]
        [Category("Settings")]
        [Description("Sets priority of the channels for data sending")]
        public List<string> SendingProtocolPriority
        {
            get { return sendingProtocolPriority; }
        }

        [Browsable(true)]
        [Category("Settings")]
        [Description("Device Settings")]
        public Dictionary<string, object> Settings
        {
            get { return this.settings; }
        }
    }
}
