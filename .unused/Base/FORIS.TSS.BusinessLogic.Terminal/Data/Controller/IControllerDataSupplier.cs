using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller
{
    public interface IControllerDataSupplier:
        IDataSupplier<IControllerDataTreater>
    {

    }
}
