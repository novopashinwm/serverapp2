using System;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Schema
{
    [Serializable]
    public struct Cl0000000004PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000004PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000005PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000005PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000006PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000006PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000020PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000020PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000022PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000022PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000023PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000023PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000032PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000032PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000035PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000035PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000036PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000036PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000037PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000037PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000038PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000038PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000039PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000039PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000040PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000040PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000041PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000041PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000042PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000042PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000043PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000043PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000044PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000044PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000045PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000045PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000046PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000046PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000049PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000049PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000050PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000050PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct Cl0000000051PrimaryKey
    {
        public System.DateTime LogTime;
        public Cl0000000051PrimaryKey(System.DateTime LogTime)
        {
            this.LogTime = LogTime;
        }
        public object[] GetObjects()
        {
            return new object[]{LogTime};
        }
    }
    [Serializable]
    public struct ConstantsPrimaryKey
    {
        public System.String Name;
        public ConstantsPrimaryKey(System.String Name)
        {
            this.Name = Name;
        }
        public object[] GetObjects()
        {
            return new object[]{Name};
        }
    }
    [Serializable]
    public struct ConsumerPrimaryKey
    {
        public System.Int32 ConsumerId;
        public ConsumerPrimaryKey(System.Int32 ConsumerId)
        {
            this.ConsumerId = ConsumerId;
        }
        public object[] GetObjects()
        {
            return new object[]{ConsumerId};
        }
    }
    [Serializable]
    public struct ControllerPrimaryKey
    {
        public System.Int32 ControllerId;
        public ControllerPrimaryKey(System.Int32 ControllerId)
        {
            this.ControllerId = ControllerId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerId};
        }
    }
    [Serializable]
    public struct ControllerSensorPrimaryKey
    {
        public System.Int32 ControllerSensorId;
        public ControllerSensorPrimaryKey(System.Int32 ControllerSensorId)
        {
            this.ControllerSensorId = ControllerSensorId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorId};
        }
    }
    [Serializable]
    public struct ControllerSensorLegendPrimaryKey
    {
        public System.Int32 ControllerSensorLegendId;
        public ControllerSensorLegendPrimaryKey(System.Int32 ControllerSensorLegendId)
        {
            this.ControllerSensorLegendId = ControllerSensorLegendId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorLegendId};
        }
    }
    [Serializable]
    public struct ControllerSensorMapPrimaryKey
    {
        public System.Int32 ControllerSensorMapId;
        public ControllerSensorMapPrimaryKey(System.Int32 ControllerSensorMapId)
        {
            this.ControllerSensorMapId = ControllerSensorMapId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorMapId};
        }
    }
    [Serializable]
    public struct ControllerSensorTypePrimaryKey
    {
        public System.Int32 ControllerSensorTypeId;
        public ControllerSensorTypePrimaryKey(System.Int32 ControllerSensorTypeId)
        {
            this.ControllerSensorTypeId = ControllerSensorTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerSensorTypeId};
        }
    }
    [Serializable]
    public struct ControllerTypePrimaryKey
    {
        public System.Int32 ControllerTypeId;
        public ControllerTypePrimaryKey(System.Int32 ControllerTypeId)
        {
            this.ControllerTypeId = ControllerTypeId;
        }
        public object[] GetObjects()
        {
            return new object[]{ControllerTypeId};
        }
    }
    [Serializable]
    public struct LolitaPrimaryKey
    {
        public System.Int32 Lol;
        public LolitaPrimaryKey(System.Int32 Lol)
        {
            this.Lol = Lol;
        }
        public object[] GetObjects()
        {
            return new object[]{Lol};
        }
    }
    [Serializable]
    public struct MediaPrimaryKey
    {
        public System.Int32 MediaId;
        public MediaPrimaryKey(System.Int32 MediaId)
        {
            this.MediaId = MediaId;
        }
        public object[] GetObjects()
        {
            return new object[]{MediaId};
        }
    }
    [Serializable]
    public struct MediaAcceptorsPrimaryKey
    {
        public System.Int32 MaId;
        public MediaAcceptorsPrimaryKey(System.Int32 MaId)
        {
            this.MaId = MaId;
        }
        public object[] GetObjects()
        {
            return new object[]{MaId};
        }
    }
    [Serializable]
    public struct MediaTypePrimaryKey
    {
        public System.Byte Id;
        public MediaTypePrimaryKey(System.Byte Id)
        {
            this.Id = Id;
        }
        public object[] GetObjects()
        {
            return new object[]{Id};
        }
    }
    [Serializable]
    public struct OperatorPrimaryKey
    {
        public System.Int32 OperatorId;
        public OperatorPrimaryKey(System.Int32 OperatorId)
        {
            this.OperatorId = OperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId};
        }
    }
    [Serializable]
    public struct OperatorgroupPrimaryKey
    {
        public System.Int32 OperatorgroupId;
        public OperatorgroupPrimaryKey(System.Int32 OperatorgroupId)
        {
            this.OperatorgroupId = OperatorgroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupId};
        }
    }
    [Serializable]
    public struct OperatorgroupOperatorPrimaryKey
    {
        public System.Int32 OperatorgroupOperatorId;
        public OperatorgroupOperatorPrimaryKey(System.Int32 OperatorgroupOperatorId)
        {
            this.OperatorgroupOperatorId = OperatorgroupOperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorgroupOperatorId};
        }
    }
    [Serializable]
    public struct PasswordPrimaryKey
    {
        public System.Int32 OperatorId;
        public PasswordPrimaryKey(System.Int32 OperatorId)
        {
            this.OperatorId = OperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{OperatorId};
        }
    }
    [Serializable]
    public struct RRightOperatorPrimaryKey
    {
        public System.Int32 RRightOperatorId;
        public RRightOperatorPrimaryKey(System.Int32 RRightOperatorId)
        {
            this.RRightOperatorId = RRightOperatorId;
        }
        public object[] GetObjects()
        {
            return new object[]{RRightOperatorId};
        }
    }
    [Serializable]
    public struct RRightOperatorgroupPrimaryKey
    {
        public System.Int32 RRightOperatorgroupId;
        public RRightOperatorgroupPrimaryKey(System.Int32 RRightOperatorgroupId)
        {
            this.RRightOperatorgroupId = RRightOperatorgroupId;
        }
        public object[] GetObjects()
        {
            return new object[]{RRightOperatorgroupId};
        }
    }
    [Serializable]
    public struct RightPrimaryKey
    {
        public System.Int32 RightId;
        public RightPrimaryKey(System.Int32 RightId)
        {
            this.RightId = RightId;
        }
        public object[] GetObjects()
        {
            return new object[]{RightId};
        }
    }
    [Serializable]
    public struct SessionPrimaryKey
    {
        public System.Int32 SessionId;
        public SessionPrimaryKey(System.Int32 SessionId)
        {
            this.SessionId = SessionId;
        }
        public object[] GetObjects()
        {
            return new object[]{SessionId};
        }
    }
    [Serializable]
    public struct SysdiagramsPrimaryKey
    {
        public System.Int32 DiagramId;
        public SysdiagramsPrimaryKey(System.Int32 DiagramId)
        {
            this.DiagramId = DiagramId;
        }
        public object[] GetObjects()
        {
            return new object[]{DiagramId};
        }
    }
    [Serializable]
    public struct TrailPrimaryKey
    {
        public System.Int32 TrailId;
        public TrailPrimaryKey(System.Int32 TrailId)
        {
            this.TrailId = TrailId;
        }
        public object[] GetObjects()
        {
            return new object[]{TrailId};
        }
    }
}
