using System;
using System.Diagnostics;
using System.Collections;
using System.Data;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Schema
{
    ///<summary>
    /// Class for creating table schema on client
    ///</summary>
    public class DatasetHelper : DatasetHelperBase
    {
        ///<summary>
        /// [CL_0000000004]
        ///</summary>
        public static void CascadeRemove_Cl0000000004(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000004"));
            DataRow row = dataset.Tables["CL_0000000004"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000004]
        ///</summary>
        public static void Compact_Cl0000000004(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000004"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000004"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000004]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000004(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000004"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000004"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000004"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000005]
        ///</summary>
        public static void CascadeRemove_Cl0000000005(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000005"));
            DataRow row = dataset.Tables["CL_0000000005"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000005]
        ///</summary>
        public static void Compact_Cl0000000005(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000005"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000005"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000005]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000005(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000005"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000005"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000005"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000006]
        ///</summary>
        public static void CascadeRemove_Cl0000000006(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000006"));
            DataRow row = dataset.Tables["CL_0000000006"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000006]
        ///</summary>
        public static void Compact_Cl0000000006(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000006"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000006"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000006]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000006(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000006"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000006"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000006"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000020]
        ///</summary>
        public static void CascadeRemove_Cl0000000020(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000020"));
            DataRow row = dataset.Tables["CL_0000000020"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000020]
        ///</summary>
        public static void Compact_Cl0000000020(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000020"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000020"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000020]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000020(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000020"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000020"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000020"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000022]
        ///</summary>
        public static void CascadeRemove_Cl0000000022(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000022"));
            DataRow row = dataset.Tables["CL_0000000022"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000022]
        ///</summary>
        public static void Compact_Cl0000000022(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000022"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000022"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000022]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000022(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000022"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000022"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000022"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000023]
        ///</summary>
        public static void CascadeRemove_Cl0000000023(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000023"));
            DataRow row = dataset.Tables["CL_0000000023"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000023]
        ///</summary>
        public static void Compact_Cl0000000023(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000023"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000023"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000023]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000023(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000023"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000023"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000023"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000032]
        ///</summary>
        public static void CascadeRemove_Cl0000000032(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000032"));
            DataRow row = dataset.Tables["CL_0000000032"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000032]
        ///</summary>
        public static void Compact_Cl0000000032(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000032"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000032"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000032]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000032(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000032"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000032"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000032"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000035]
        ///</summary>
        public static void CascadeRemove_Cl0000000035(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000035"));
            DataRow row = dataset.Tables["CL_0000000035"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000035]
        ///</summary>
        public static void Compact_Cl0000000035(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000035"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000035"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000035]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000035(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000035"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000035"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000035"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000036]
        ///</summary>
        public static void CascadeRemove_Cl0000000036(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000036"));
            DataRow row = dataset.Tables["CL_0000000036"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000036]
        ///</summary>
        public static void Compact_Cl0000000036(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000036"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000036"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000036]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000036(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000036"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000036"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000036"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000037]
        ///</summary>
        public static void CascadeRemove_Cl0000000037(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000037"));
            DataRow row = dataset.Tables["CL_0000000037"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000037]
        ///</summary>
        public static void Compact_Cl0000000037(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000037"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000037"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000037]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000037(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000037"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000037"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000037"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000038]
        ///</summary>
        public static void CascadeRemove_Cl0000000038(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000038"));
            DataRow row = dataset.Tables["CL_0000000038"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000038]
        ///</summary>
        public static void Compact_Cl0000000038(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000038"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000038"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000038]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000038(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000038"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000038"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000038"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000039]
        ///</summary>
        public static void CascadeRemove_Cl0000000039(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000039"));
            DataRow row = dataset.Tables["CL_0000000039"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000039]
        ///</summary>
        public static void Compact_Cl0000000039(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000039"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000039"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000039]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000039(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000039"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000039"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000039"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000040]
        ///</summary>
        public static void CascadeRemove_Cl0000000040(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000040"));
            DataRow row = dataset.Tables["CL_0000000040"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000040]
        ///</summary>
        public static void Compact_Cl0000000040(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000040"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000040"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000040]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000040(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000040"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000040"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000040"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000041]
        ///</summary>
        public static void CascadeRemove_Cl0000000041(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000041"));
            DataRow row = dataset.Tables["CL_0000000041"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000041]
        ///</summary>
        public static void Compact_Cl0000000041(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000041"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000041"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000041]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000041(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000041"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000041"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000041"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000042]
        ///</summary>
        public static void CascadeRemove_Cl0000000042(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000042"));
            DataRow row = dataset.Tables["CL_0000000042"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000042]
        ///</summary>
        public static void Compact_Cl0000000042(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000042"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000042"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000042]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000042(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000042"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000042"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000042"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000043]
        ///</summary>
        public static void CascadeRemove_Cl0000000043(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000043"));
            DataRow row = dataset.Tables["CL_0000000043"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000043]
        ///</summary>
        public static void Compact_Cl0000000043(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000043"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000043"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000043]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000043(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000043"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000043"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000043"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000044]
        ///</summary>
        public static void CascadeRemove_Cl0000000044(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000044"));
            DataRow row = dataset.Tables["CL_0000000044"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000044]
        ///</summary>
        public static void Compact_Cl0000000044(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000044"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000044"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000044]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000044(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000044"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000044"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000044"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000045]
        ///</summary>
        public static void CascadeRemove_Cl0000000045(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000045"));
            DataRow row = dataset.Tables["CL_0000000045"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000045]
        ///</summary>
        public static void Compact_Cl0000000045(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000045"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000045"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000045]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000045(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000045"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000045"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000045"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000046]
        ///</summary>
        public static void CascadeRemove_Cl0000000046(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000046"));
            DataRow row = dataset.Tables["CL_0000000046"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000046]
        ///</summary>
        public static void Compact_Cl0000000046(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000046"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000046"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000046]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000046(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000046"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000046"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000046"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000049]
        ///</summary>
        public static void CascadeRemove_Cl0000000049(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000049"));
            DataRow row = dataset.Tables["CL_0000000049"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000049]
        ///</summary>
        public static void Compact_Cl0000000049(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000049"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000049"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000049]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000049(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000049"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000049"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000049"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000050]
        ///</summary>
        public static void CascadeRemove_Cl0000000050(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000050"));
            DataRow row = dataset.Tables["CL_0000000050"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000050]
        ///</summary>
        public static void Compact_Cl0000000050(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000050"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000050"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000050]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000050(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000050"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000050"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000050"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CL_0000000051]
        ///</summary>
        public static void CascadeRemove_Cl0000000051(DataSet dataset, System.DateTime LogTime)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000051"));
            DataRow row = dataset.Tables["CL_0000000051"].Rows.Find(LogTime);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000051]
        ///</summary>
        public static void Compact_Cl0000000051(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000051"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000051"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CL_0000000051]
        ///</summary>
        public static void CompactFreshRecords_Cl0000000051(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CL_0000000051"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CL_0000000051"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CL_0000000051"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void CascadeRemove_Constants(DataSet dataset, System.String Name)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            DataRow row = dataset.Tables["CONSTANTS"].Rows.Find(Name);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void Compact_Constants(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSTANTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void CompactFreshRecords_Constants(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSTANTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONSTANTS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONSUMER]
        ///</summary>
        public static void CascadeRemove_Consumer(DataSet dataset, System.Int32 ConsumerId)
        {
            Debug.Assert(dataset.Tables.Contains("CONSUMER"));
            DataRow row = dataset.Tables["CONSUMER"].Rows.Find(ConsumerId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSUMER]
        ///</summary>
        public static void Compact_Consumer(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSUMER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSUMER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSUMER]
        ///</summary>
        public static void CompactFreshRecords_Consumer(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSUMER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSUMER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONSUMER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void CascadeRemove_Controller(DataSet dataset, System.Int32 ControllerId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            DataRow row = dataset.Tables["CONTROLLER"].Rows.Find(ControllerId);
            if( row != null){
                DataRow[] rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER = row.GetChildRows("FK_CONTROLLER_SENSOR_MAP_CONTROLLER");
                if( rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER != null && rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER in rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void Compact_Controller(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void CompactFreshRecords_Controller(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void CascadeRemove_ControllerSensor(DataSet dataset, System.Int32 ControllerSensorId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR"].Rows.Find(ControllerSensorId);
            if( row != null){
                DataRow[] rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR = row.GetChildRows("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR");
                if( rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR != null && rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR in rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void Compact_ControllerSensor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void CascadeRemove_ControllerSensorLegend(DataSet dataset, System.Int32 ControllerSensorLegendId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows.Find(ControllerSensorLegendId);
            if( row != null){
                DataRow[] rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND = row.GetChildRows("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND");
                if( rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND != null && rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND in rowsFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void Compact_ControllerSensorLegend(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorLegend(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void CascadeRemove_ControllerSensorMap(DataSet dataset, System.Int32 ControllerSensorMapId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows.Find(ControllerSensorMapId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void Compact_ControllerSensorMap(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorMap(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void CascadeRemove_ControllerSensorType(DataSet dataset, System.Int32 ControllerSensorTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows.Find(ControllerSensorTypeId);
            if( row != null){
                DataRow[] rowsFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE = row.GetChildRows("FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE");
                if( rowsFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE != null && rowsFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE in rowsFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE)
                    {
                        System.Int32 childControllerSensorId = (System.Int32)rowFK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE["CONTROLLER_SENSOR_ID"];
                        CascadeRemove_ControllerSensor(dataset, childControllerSensorId);
                    }
                }
                DataRow[] rowsFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE = row.GetChildRows("FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE");
                if( rowsFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE != null && rowsFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE in rowsFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE)
                    {
                        System.Int32 childControllerSensorLegendId = (System.Int32)rowFK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE["CONTROLLER_SENSOR_LEGEND_ID"];
                        CascadeRemove_ControllerSensorLegend(dataset, childControllerSensorLegendId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void Compact_ControllerSensorType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void CascadeRemove_ControllerType(DataSet dataset, System.Int32 ControllerTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            DataRow row = dataset.Tables["CONTROLLER_TYPE"].Rows.Find(ControllerTypeId);
            if( row != null){
                DataRow[] rowsFK_CONTROLLER_CONTROLLER_TYPE = row.GetChildRows("FK_CONTROLLER_CONTROLLER_TYPE");
                if( rowsFK_CONTROLLER_CONTROLLER_TYPE != null && rowsFK_CONTROLLER_CONTROLLER_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_CONTROLLER_TYPE in rowsFK_CONTROLLER_CONTROLLER_TYPE)
                    {
                        System.Int32 childControllerId = (System.Int32)rowFK_CONTROLLER_CONTROLLER_TYPE["CONTROLLER_ID"];
                        CascadeRemove_Controller(dataset, childControllerId);
                    }
                }
                DataRow[] rowsFK_CONTROLLER_SENSOR_CONTROLLER_TYPE = row.GetChildRows("FK_CONTROLLER_SENSOR_CONTROLLER_TYPE");
                if( rowsFK_CONTROLLER_SENSOR_CONTROLLER_TYPE != null && rowsFK_CONTROLLER_SENSOR_CONTROLLER_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_SENSOR_CONTROLLER_TYPE in rowsFK_CONTROLLER_SENSOR_CONTROLLER_TYPE)
                    {
                        System.Int32 childControllerSensorId = (System.Int32)rowFK_CONTROLLER_SENSOR_CONTROLLER_TYPE["CONTROLLER_SENSOR_ID"];
                        CascadeRemove_ControllerSensor(dataset, childControllerSensorId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void Compact_ControllerType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void CompactFreshRecords_ControllerType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOLITA]
        ///</summary>
        public static void CascadeRemove_Lolita(DataSet dataset, System.Int32 Lol)
        {
            Debug.Assert(dataset.Tables.Contains("LOLITA"));
            DataRow row = dataset.Tables["LOLITA"].Rows.Find(Lol);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [LOLITA]
        ///</summary>
        public static void Compact_Lolita(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOLITA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOLITA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOLITA]
        ///</summary>
        public static void CompactFreshRecords_Lolita(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOLITA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOLITA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOLITA"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void CascadeRemove_Media(DataSet dataset, System.Int32 MediaId)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            DataRow row = dataset.Tables["MEDIA"].Rows.Find(MediaId);
            if( row != null){
                DataRow[] rowsFK_MEDIA_ACCEPTORS_MEDIA = row.GetChildRows("FK_MEDIA_ACCEPTORS_MEDIA");
                if( rowsFK_MEDIA_ACCEPTORS_MEDIA != null && rowsFK_MEDIA_ACCEPTORS_MEDIA.Length > 0 ){
                    foreach (DataRow rowFK_MEDIA_ACCEPTORS_MEDIA in rowsFK_MEDIA_ACCEPTORS_MEDIA)
                    {
                        System.Int32 childMaId = (System.Int32)rowFK_MEDIA_ACCEPTORS_MEDIA["MA_ID"];
                        CascadeRemove_MediaAcceptors(dataset, childMaId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void Compact_Media(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void CompactFreshRecords_Media(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void CascadeRemove_MediaAcceptors(DataSet dataset, System.Int32 MaId)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            DataRow row = dataset.Tables["MEDIA_ACCEPTORS"].Rows.Find(MaId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void Compact_MediaAcceptors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_ACCEPTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void CompactFreshRecords_MediaAcceptors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_ACCEPTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA_ACCEPTORS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void CascadeRemove_MediaType(DataSet dataset, System.Byte Id)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            DataRow row = dataset.Tables["MEDIA_TYPE"].Rows.Find(Id);
            if( row != null){
                DataRow[] rowsFK_MEDIA_MEDIA_TYPE = row.GetChildRows("FK_MEDIA_MEDIA_TYPE");
                if( rowsFK_MEDIA_MEDIA_TYPE != null && rowsFK_MEDIA_MEDIA_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_MEDIA_MEDIA_TYPE in rowsFK_MEDIA_MEDIA_TYPE)
                    {
                        System.Int32 childMediaId = (System.Int32)rowFK_MEDIA_MEDIA_TYPE["MEDIA_ID"];
                        CascadeRemove_Media(dataset, childMediaId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void Compact_MediaType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void CompactFreshRecords_MediaType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void CascadeRemove_Operator(DataSet dataset, System.Int32 OperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            DataRow row = dataset.Tables["OPERATOR"].Rows.Find(OperatorId);
            if( row != null){
                DataRow[] rowsFK_CONSUMER_OPERATOR = row.GetChildRows("FK_CONSUMER_OPERATOR");
                if( rowsFK_CONSUMER_OPERATOR != null && rowsFK_CONSUMER_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_CONSUMER_OPERATOR in rowsFK_CONSUMER_OPERATOR)
                    {
                        System.Int32 childConsumerId = (System.Int32)rowFK_CONSUMER_OPERATOR["CONSUMER_ID"];
                        CascadeRemove_Consumer(dataset, childConsumerId);
                    }
                }
                DataRow[] rowsFK_OPERATORGROUP_OPERATOR_OPERATOR = row.GetChildRows("FK_OPERATORGROUP_OPERATOR_OPERATOR");
                if( rowsFK_OPERATORGROUP_OPERATOR_OPERATOR != null && rowsFK_OPERATORGROUP_OPERATOR_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_OPERATORGROUP_OPERATOR_OPERATOR in rowsFK_OPERATORGROUP_OPERATOR_OPERATOR)
                    {
                        System.Int32 childOperatorgroupOperatorId = (System.Int32)rowFK_OPERATORGROUP_OPERATOR_OPERATOR["OPERATORGROUP_OPERATOR_ID"];
                        CascadeRemove_OperatorgroupOperator(dataset, childOperatorgroupOperatorId);
                    }
                }
                DataRow[] rowsFK_PASSWORD_OPERATOR = row.GetChildRows("FK_PASSWORD_OPERATOR");
                if( rowsFK_PASSWORD_OPERATOR != null && rowsFK_PASSWORD_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_PASSWORD_OPERATOR in rowsFK_PASSWORD_OPERATOR)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowFK_PASSWORD_OPERATOR["OPERATOR_ID"];
                        CascadeRemove_Password(dataset, childOperatorId);
                    }
                }
                DataRow[] rowsFK_R_OPERATOR_RIGHT_OPERATOR = row.GetChildRows("FK_R_OPERATOR_RIGHT_OPERATOR");
                if( rowsFK_R_OPERATOR_RIGHT_OPERATOR != null && rowsFK_R_OPERATOR_RIGHT_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_R_OPERATOR_RIGHT_OPERATOR in rowsFK_R_OPERATOR_RIGHT_OPERATOR)
                    {
                        System.Int32 childRRightOperatorId = (System.Int32)rowFK_R_OPERATOR_RIGHT_OPERATOR["R_RIGHT_OPERATOR_ID"];
                        CascadeRemove_RRightOperator(dataset, childRRightOperatorId);
                    }
                }
                DataRow[] rowsFK_R_RIGHT_OPERATOR_OPERATOR = row.GetChildRows("FK_R_RIGHT_OPERATOR_OPERATOR");
                if( rowsFK_R_RIGHT_OPERATOR_OPERATOR != null && rowsFK_R_RIGHT_OPERATOR_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_R_RIGHT_OPERATOR_OPERATOR in rowsFK_R_RIGHT_OPERATOR_OPERATOR)
                    {
                        System.Int32 childRRightOperatorId = (System.Int32)rowFK_R_RIGHT_OPERATOR_OPERATOR["R_RIGHT_OPERATOR_ID"];
                        CascadeRemove_RRightOperator(dataset, childRRightOperatorId);
                    }
                }
                DataRow[] rowsFK_SESSION_OPERATOR = row.GetChildRows("FK_SESSION_OPERATOR");
                if( rowsFK_SESSION_OPERATOR != null && rowsFK_SESSION_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_SESSION_OPERATOR in rowsFK_SESSION_OPERATOR)
                    {
                        System.Int32 childSessionId = (System.Int32)rowFK_SESSION_OPERATOR["SESSION_ID"];
                        CascadeRemove_Session(dataset, childSessionId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void Compact_Operator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_Operator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void CascadeRemove_Operatorgroup(DataSet dataset, System.Int32 OperatorgroupId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP"].Rows.Find(OperatorgroupId);
            if( row != null){
                DataRow[] rowsFK_OPERATORGROUP_OPERATOR_OPERATORGROUP = row.GetChildRows("FK_OPERATORGROUP_OPERATOR_OPERATORGROUP");
                if( rowsFK_OPERATORGROUP_OPERATOR_OPERATORGROUP != null && rowsFK_OPERATORGROUP_OPERATOR_OPERATORGROUP.Length > 0 ){
                    foreach (DataRow rowFK_OPERATORGROUP_OPERATOR_OPERATORGROUP in rowsFK_OPERATORGROUP_OPERATOR_OPERATORGROUP)
                    {
                        System.Int32 childOperatorgroupOperatorId = (System.Int32)rowFK_OPERATORGROUP_OPERATOR_OPERATORGROUP["OPERATORGROUP_OPERATOR_ID"];
                        CascadeRemove_OperatorgroupOperator(dataset, childOperatorgroupOperatorId);
                    }
                }
                DataRow[] rowsFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP = row.GetChildRows("FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP");
                if( rowsFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP != null && rowsFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP.Length > 0 ){
                    foreach (DataRow rowFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP in rowsFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP)
                    {
                        System.Int32 childRRightOperatorgroupId = (System.Int32)rowFK_R_OPERATORGROUP_RIGHT_OPERATORGROUP["R_RIGHT_OPERATORGROUP_ID"];
                        CascadeRemove_RRightOperatorgroup(dataset, childRRightOperatorgroupId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void Compact_Operatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void CompactFreshRecords_Operatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void CascadeRemove_OperatorgroupOperator(DataSet dataset, System.Int32 OperatorgroupOperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            DataRow row = dataset.Tables["OPERATORGROUP_OPERATOR"].Rows.Find(OperatorgroupOperatorId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void Compact_OperatorgroupOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [PASSWORD]
        ///</summary>
        public static void CascadeRemove_Password(DataSet dataset, System.Int32 OperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("PASSWORD"));
            DataRow row = dataset.Tables["PASSWORD"].Rows.Find(OperatorId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [PASSWORD]
        ///</summary>
        public static void Compact_Password(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("PASSWORD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["PASSWORD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [PASSWORD]
        ///</summary>
        public static void CompactFreshRecords_Password(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("PASSWORD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["PASSWORD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["PASSWORD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATOR]
        ///</summary>
        public static void CascadeRemove_RRightOperator(DataSet dataset, System.Int32 RRightOperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATOR"));
            DataRow row = dataset.Tables["R_RIGHT_OPERATOR"].Rows.Find(RRightOperatorId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATOR]
        ///</summary>
        public static void Compact_RRightOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["R_RIGHT_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_RRightOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["R_RIGHT_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["R_RIGHT_OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATORGROUP]
        ///</summary>
        public static void CascadeRemove_RRightOperatorgroup(DataSet dataset, System.Int32 RRightOperatorgroupId)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATORGROUP"));
            DataRow row = dataset.Tables["R_RIGHT_OPERATORGROUP"].Rows.Find(RRightOperatorgroupId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATORGROUP]
        ///</summary>
        public static void Compact_RRightOperatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["R_RIGHT_OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [R_RIGHT_OPERATORGROUP]
        ///</summary>
        public static void CompactFreshRecords_RRightOperatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("R_RIGHT_OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["R_RIGHT_OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["R_RIGHT_OPERATORGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void CascadeRemove_Right(DataSet dataset, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            DataRow row = dataset.Tables["RIGHT"].Rows.Find(RightId);
            if( row != null){
                DataRow[] rowsFK_R_OPERATOR_RIGHT_RIGHT = row.GetChildRows("FK_R_OPERATOR_RIGHT_RIGHT");
                if( rowsFK_R_OPERATOR_RIGHT_RIGHT != null && rowsFK_R_OPERATOR_RIGHT_RIGHT.Length > 0 ){
                    foreach (DataRow rowFK_R_OPERATOR_RIGHT_RIGHT in rowsFK_R_OPERATOR_RIGHT_RIGHT)
                    {
                        System.Int32 childRRightOperatorId = (System.Int32)rowFK_R_OPERATOR_RIGHT_RIGHT["R_RIGHT_OPERATOR_ID"];
                        CascadeRemove_RRightOperator(dataset, childRRightOperatorId);
                    }
                }
                DataRow[] rowsFK_R_OPERATORGROUP_RIGHT_RIGHT = row.GetChildRows("FK_R_OPERATORGROUP_RIGHT_RIGHT");
                if( rowsFK_R_OPERATORGROUP_RIGHT_RIGHT != null && rowsFK_R_OPERATORGROUP_RIGHT_RIGHT.Length > 0 ){
                    foreach (DataRow rowFK_R_OPERATORGROUP_RIGHT_RIGHT in rowsFK_R_OPERATORGROUP_RIGHT_RIGHT)
                    {
                        System.Int32 childRRightOperatorgroupId = (System.Int32)rowFK_R_OPERATORGROUP_RIGHT_RIGHT["R_RIGHT_OPERATORGROUP_ID"];
                        CascadeRemove_RRightOperatorgroup(dataset, childRRightOperatorgroupId);
                    }
                }
                DataRow[] rowsFK_R_RIGHT_OPERATOR_RIGHT = row.GetChildRows("FK_R_RIGHT_OPERATOR_RIGHT");
                if( rowsFK_R_RIGHT_OPERATOR_RIGHT != null && rowsFK_R_RIGHT_OPERATOR_RIGHT.Length > 0 ){
                    foreach (DataRow rowFK_R_RIGHT_OPERATOR_RIGHT in rowsFK_R_RIGHT_OPERATOR_RIGHT)
                    {
                        System.Int32 childRRightOperatorId = (System.Int32)rowFK_R_RIGHT_OPERATOR_RIGHT["R_RIGHT_OPERATOR_ID"];
                        CascadeRemove_RRightOperator(dataset, childRRightOperatorId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void Compact_Right(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void CompactFreshRecords_Right(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RIGHT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void CascadeRemove_Session(DataSet dataset, System.Int32 SessionId)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            DataRow row = dataset.Tables["SESSION"].Rows.Find(SessionId);
            if( row != null){
                DataRow[] rowsFK_TRAIL_SESSION = row.GetChildRows("FK_TRAIL_SESSION");
                if( rowsFK_TRAIL_SESSION != null && rowsFK_TRAIL_SESSION.Length > 0 ){
                    foreach (DataRow rowFK_TRAIL_SESSION in rowsFK_TRAIL_SESSION)
                    {
                        System.Int32 childTrailId = (System.Int32)rowFK_TRAIL_SESSION["TRAIL_ID"];
                        CascadeRemove_Trail(dataset, childTrailId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void Compact_Session(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SESSION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void CompactFreshRecords_Session(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SESSION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SESSION"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void CascadeRemove_Sysdiagrams(DataSet dataset, System.Int32 DiagramId)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            DataRow row = dataset.Tables["sysdiagrams"].Rows.Find(DiagramId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void Compact_Sysdiagrams(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["sysdiagrams"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void CompactFreshRecords_Sysdiagrams(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["sysdiagrams"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["sysdiagrams"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void CascadeRemove_Trail(DataSet dataset, System.Int32 TrailId)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            DataRow row = dataset.Tables["TRAIL"].Rows.Find(TrailId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void Compact_Trail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void CompactFreshRecords_Trail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["TRAIL"].Rows.Remove(row);
            }
        }
        
        public static void Compact(DataSet dataset)
        {
        }
        
        public static void CompactFreshRecords(DataSet dataset)
        {
        }
        public static void Merge(DataSet cache, DataSet newdata)
        {
            bool oldForce = cache.EnforceConstraints;
            DatabaseSchema.MakeSchema(cache);
            cache.EnforceConstraints = false;
            // foreach table with PK
            bool processCl0000000004 = newdata.Tables.Contains("CL_0000000004");
            if (processCl0000000004)
            {
                if (cache.Tables.Contains("CL_0000000004") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000004(cache);
                }
            }
            bool processCl0000000005 = newdata.Tables.Contains("CL_0000000005");
            if (processCl0000000005)
            {
                if (cache.Tables.Contains("CL_0000000005") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000005(cache);
                }
            }
            bool processCl0000000006 = newdata.Tables.Contains("CL_0000000006");
            if (processCl0000000006)
            {
                if (cache.Tables.Contains("CL_0000000006") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000006(cache);
                }
            }
            bool processCl0000000020 = newdata.Tables.Contains("CL_0000000020");
            if (processCl0000000020)
            {
                if (cache.Tables.Contains("CL_0000000020") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000020(cache);
                }
            }
            bool processCl0000000022 = newdata.Tables.Contains("CL_0000000022");
            if (processCl0000000022)
            {
                if (cache.Tables.Contains("CL_0000000022") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000022(cache);
                }
            }
            bool processCl0000000023 = newdata.Tables.Contains("CL_0000000023");
            if (processCl0000000023)
            {
                if (cache.Tables.Contains("CL_0000000023") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000023(cache);
                }
            }
            bool processCl0000000032 = newdata.Tables.Contains("CL_0000000032");
            if (processCl0000000032)
            {
                if (cache.Tables.Contains("CL_0000000032") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000032(cache);
                }
            }
            bool processCl0000000035 = newdata.Tables.Contains("CL_0000000035");
            if (processCl0000000035)
            {
                if (cache.Tables.Contains("CL_0000000035") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000035(cache);
                }
            }
            bool processCl0000000036 = newdata.Tables.Contains("CL_0000000036");
            if (processCl0000000036)
            {
                if (cache.Tables.Contains("CL_0000000036") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000036(cache);
                }
            }
            bool processCl0000000037 = newdata.Tables.Contains("CL_0000000037");
            if (processCl0000000037)
            {
                if (cache.Tables.Contains("CL_0000000037") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000037(cache);
                }
            }
            bool processCl0000000038 = newdata.Tables.Contains("CL_0000000038");
            if (processCl0000000038)
            {
                if (cache.Tables.Contains("CL_0000000038") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000038(cache);
                }
            }
            bool processCl0000000039 = newdata.Tables.Contains("CL_0000000039");
            if (processCl0000000039)
            {
                if (cache.Tables.Contains("CL_0000000039") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000039(cache);
                }
            }
            bool processCl0000000040 = newdata.Tables.Contains("CL_0000000040");
            if (processCl0000000040)
            {
                if (cache.Tables.Contains("CL_0000000040") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000040(cache);
                }
            }
            bool processCl0000000041 = newdata.Tables.Contains("CL_0000000041");
            if (processCl0000000041)
            {
                if (cache.Tables.Contains("CL_0000000041") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000041(cache);
                }
            }
            bool processCl0000000042 = newdata.Tables.Contains("CL_0000000042");
            if (processCl0000000042)
            {
                if (cache.Tables.Contains("CL_0000000042") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000042(cache);
                }
            }
            bool processCl0000000043 = newdata.Tables.Contains("CL_0000000043");
            if (processCl0000000043)
            {
                if (cache.Tables.Contains("CL_0000000043") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000043(cache);
                }
            }
            bool processCl0000000044 = newdata.Tables.Contains("CL_0000000044");
            if (processCl0000000044)
            {
                if (cache.Tables.Contains("CL_0000000044") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000044(cache);
                }
            }
            bool processCl0000000045 = newdata.Tables.Contains("CL_0000000045");
            if (processCl0000000045)
            {
                if (cache.Tables.Contains("CL_0000000045") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000045(cache);
                }
            }
            bool processCl0000000046 = newdata.Tables.Contains("CL_0000000046");
            if (processCl0000000046)
            {
                if (cache.Tables.Contains("CL_0000000046") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000046(cache);
                }
            }
            bool processCl0000000049 = newdata.Tables.Contains("CL_0000000049");
            if (processCl0000000049)
            {
                if (cache.Tables.Contains("CL_0000000049") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000049(cache);
                }
            }
            bool processCl0000000050 = newdata.Tables.Contains("CL_0000000050");
            if (processCl0000000050)
            {
                if (cache.Tables.Contains("CL_0000000050") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000050(cache);
                }
            }
            bool processCl0000000051 = newdata.Tables.Contains("CL_0000000051");
            if (processCl0000000051)
            {
                if (cache.Tables.Contains("CL_0000000051") == false)
                {
                    DatabaseSchema.MakeTable_Cl0000000051(cache);
                }
            }
            bool processConstants = newdata.Tables.Contains("CONSTANTS");
            if (processConstants)
            {
                if (cache.Tables.Contains("CONSTANTS") == false)
                {
                    DatabaseSchema.MakeTable_Constants(cache);
                }
            }
            bool processConsumer = newdata.Tables.Contains("CONSUMER");
            if (processConsumer)
            {
                if (cache.Tables.Contains("CONSUMER") == false)
                {
                    DatabaseSchema.MakeTable_Consumer(cache);
                }
            }
            bool processController = newdata.Tables.Contains("CONTROLLER");
            if (processController)
            {
                if (cache.Tables.Contains("CONTROLLER") == false)
                {
                    DatabaseSchema.MakeTable_Controller(cache);
                }
            }
            bool processControllerSensor = newdata.Tables.Contains("CONTROLLER_SENSOR");
            if (processControllerSensor)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensor(cache);
                }
            }
            bool processControllerSensorLegend = newdata.Tables.Contains("CONTROLLER_SENSOR_LEGEND");
            if (processControllerSensorLegend)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_LEGEND") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorLegend(cache);
                }
            }
            bool processControllerSensorMap = newdata.Tables.Contains("CONTROLLER_SENSOR_MAP");
            if (processControllerSensorMap)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_MAP") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorMap(cache);
                }
            }
            bool processControllerSensorType = newdata.Tables.Contains("CONTROLLER_SENSOR_TYPE");
            if (processControllerSensorType)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorType(cache);
                }
            }
            bool processControllerType = newdata.Tables.Contains("CONTROLLER_TYPE");
            if (processControllerType)
            {
                if (cache.Tables.Contains("CONTROLLER_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_ControllerType(cache);
                }
            }
            bool processLolita = newdata.Tables.Contains("LOLITA");
            if (processLolita)
            {
                if (cache.Tables.Contains("LOLITA") == false)
                {
                    DatabaseSchema.MakeTable_Lolita(cache);
                }
            }
            bool processMedia = newdata.Tables.Contains("MEDIA");
            if (processMedia)
            {
                if (cache.Tables.Contains("MEDIA") == false)
                {
                    DatabaseSchema.MakeTable_Media(cache);
                }
            }
            bool processMediaAcceptors = newdata.Tables.Contains("MEDIA_ACCEPTORS");
            if (processMediaAcceptors)
            {
                if (cache.Tables.Contains("MEDIA_ACCEPTORS") == false)
                {
                    DatabaseSchema.MakeTable_MediaAcceptors(cache);
                }
            }
            bool processMediaType = newdata.Tables.Contains("MEDIA_TYPE");
            if (processMediaType)
            {
                if (cache.Tables.Contains("MEDIA_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_MediaType(cache);
                }
            }
            bool processOperator = newdata.Tables.Contains("OPERATOR");
            if (processOperator)
            {
                if (cache.Tables.Contains("OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_Operator(cache);
                }
            }
            bool processOperatorgroup = newdata.Tables.Contains("OPERATORGROUP");
            if (processOperatorgroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Operatorgroup(cache);
                }
            }
            bool processOperatorgroupOperator = newdata.Tables.Contains("OPERATORGROUP_OPERATOR");
            if (processOperatorgroupOperator)
            {
                if (cache.Tables.Contains("OPERATORGROUP_OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupOperator(cache);
                }
            }
            bool processPassword = newdata.Tables.Contains("PASSWORD");
            if (processPassword)
            {
                if (cache.Tables.Contains("PASSWORD") == false)
                {
                    DatabaseSchema.MakeTable_Password(cache);
                }
            }
            bool processRRightOperator = newdata.Tables.Contains("R_RIGHT_OPERATOR");
            if (processRRightOperator)
            {
                if (cache.Tables.Contains("R_RIGHT_OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_RRightOperator(cache);
                }
            }
            bool processRRightOperatorgroup = newdata.Tables.Contains("R_RIGHT_OPERATORGROUP");
            if (processRRightOperatorgroup)
            {
                if (cache.Tables.Contains("R_RIGHT_OPERATORGROUP") == false)
                {
                    DatabaseSchema.MakeTable_RRightOperatorgroup(cache);
                }
            }
            bool processRight = newdata.Tables.Contains("RIGHT");
            if (processRight)
            {
                if (cache.Tables.Contains("RIGHT") == false)
                {
                    DatabaseSchema.MakeTable_Right(cache);
                }
            }
            bool processSession = newdata.Tables.Contains("SESSION");
            if (processSession)
            {
                if (cache.Tables.Contains("SESSION") == false)
                {
                    DatabaseSchema.MakeTable_Session(cache);
                }
            }
            bool processSysdiagrams = newdata.Tables.Contains("sysdiagrams");
            if (processSysdiagrams)
            {
                if (cache.Tables.Contains("sysdiagrams") == false)
                {
                    DatabaseSchema.MakeTable_Sysdiagrams(cache);
                }
            }
            bool processTrail = newdata.Tables.Contains("TRAIL");
            if (processTrail)
            {
                if (cache.Tables.Contains("TRAIL") == false)
                {
                    DatabaseSchema.MakeTable_Trail(cache);
                }
            }
            // Make schema once
            DatabaseSchema.MakeSchema(cache);
            // For each table with PK
            if (processCl0000000004)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000004"];
                DataTable targetTable = cache.Tables["CL_0000000004"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000004PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000005)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000005"];
                DataTable targetTable = cache.Tables["CL_0000000005"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000005PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000006)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000006"];
                DataTable targetTable = cache.Tables["CL_0000000006"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000006PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000020)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000020"];
                DataTable targetTable = cache.Tables["CL_0000000020"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000020PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000022)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000022"];
                DataTable targetTable = cache.Tables["CL_0000000022"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000022PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000023)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000023"];
                DataTable targetTable = cache.Tables["CL_0000000023"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000023PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000032)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000032"];
                DataTable targetTable = cache.Tables["CL_0000000032"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000032PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000035)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000035"];
                DataTable targetTable = cache.Tables["CL_0000000035"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000035PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000036)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000036"];
                DataTable targetTable = cache.Tables["CL_0000000036"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000036PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000037)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000037"];
                DataTable targetTable = cache.Tables["CL_0000000037"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000037PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000038)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000038"];
                DataTable targetTable = cache.Tables["CL_0000000038"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000038PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000039)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000039"];
                DataTable targetTable = cache.Tables["CL_0000000039"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000039PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000040)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000040"];
                DataTable targetTable = cache.Tables["CL_0000000040"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000040PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000041)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000041"];
                DataTable targetTable = cache.Tables["CL_0000000041"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000041PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000042)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000042"];
                DataTable targetTable = cache.Tables["CL_0000000042"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000042PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000043)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000043"];
                DataTable targetTable = cache.Tables["CL_0000000043"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000043PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000044)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000044"];
                DataTable targetTable = cache.Tables["CL_0000000044"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000044PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000045)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000045"];
                DataTable targetTable = cache.Tables["CL_0000000045"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000045PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000046)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000046"];
                DataTable targetTable = cache.Tables["CL_0000000046"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000046PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000049)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000049"];
                DataTable targetTable = cache.Tables["CL_0000000049"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000049PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000050)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000050"];
                DataTable targetTable = cache.Tables["CL_0000000050"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000050PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCl0000000051)
            {
                DataTable sourceTable = newdata.Tables["CL_0000000051"];
                DataTable targetTable = cache.Tables["CL_0000000051"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    Cl0000000051PrimaryKey pk;
                    pk.LogTime = (System.DateTime)sourceRow["LOG_TIME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogTime);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_TIME"] = sourceRow["LOG_TIME"];
                    targetRow["VALID"] = sourceRow["VALID"];
                    targetRow["LONGITUDE"] = sourceRow["LONGITUDE"];
                    targetRow["LATITUDE"] = sourceRow["LATITUDE"];
                    targetRow["SPEED"] = sourceRow["SPEED"];
                    targetRow["SATELLITES"] = sourceRow["SATELLITES"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processConstants)
            {
                DataTable sourceTable = newdata.Tables["CONSTANTS"];
                DataTable targetTable = cache.Tables["CONSTANTS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ConstantsPrimaryKey pk;
                    pk.Name = (System.String)sourceRow["NAME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Name);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["VALUE"] = sourceRow["VALUE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processConsumer)
            {
                DataTable sourceTable = newdata.Tables["CONSUMER"];
                DataTable targetTable = cache.Tables["CONSUMER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ConsumerPrimaryKey pk;
                    pk.ConsumerId = (System.Int32)sourceRow["CONSUMER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ConsumerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONSUMER_ID"] = sourceRow["CONSUMER_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processController)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER"];
                DataTable targetTable = cache.Tables["CONTROLLER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerPrimaryKey pk;
                    pk.ControllerId = (System.Int32)sourceRow["CONTROLLER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["PHONE"] = sourceRow["PHONE"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["FIRMWARE"] = sourceRow["FIRMWARE"];
                    targetRow["SETTINGS"] = sourceRow["SETTINGS"];
                    targetRow["IMEI"] = sourceRow["IMEI"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensor)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorPrimaryKey pk;
                    pk.ControllerSensorId = (System.Int32)sourceRow["CONTROLLER_SENSOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_ID"] = sourceRow["CONTROLLER_SENSOR_ID"];
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["MAX_VALUE"] = sourceRow["MAX_VALUE"];
                    targetRow["MIN_VALUE"] = sourceRow["MIN_VALUE"];
                    targetRow["DEFAULT_VALUE"] = sourceRow["DEFAULT_VALUE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorLegend)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_LEGEND"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_LEGEND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorLegendPrimaryKey pk;
                    pk.ControllerSensorLegendId = (System.Int32)sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorLegendId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_LEGEND_ID"] = sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorMap)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_MAP"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_MAP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorMapPrimaryKey pk;
                    pk.ControllerSensorMapId = (System.Int32)sourceRow["CONTROLLER_SENSOR_MAP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorMapId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_MAP_ID"] = sourceRow["CONTROLLER_SENSOR_MAP_ID"];
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["CONTROLLER_SENSOR_ID"] = sourceRow["CONTROLLER_SENSOR_ID"];
                    targetRow["CONTROLLER_SENSOR_LEGEND_ID"] = sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorType)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_TYPE"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorTypePrimaryKey pk;
                    pk.ControllerSensorTypeId = (System.Int32)sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerType)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_TYPE"];
                DataTable targetTable = cache.Tables["CONTROLLER_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerTypePrimaryKey pk;
                    pk.ControllerTypeId = (System.Int32)sourceRow["CONTROLLER_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["SETTINGS"] = sourceRow["SETTINGS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLolita)
            {
                DataTable sourceTable = newdata.Tables["LOLITA"];
                DataTable targetTable = cache.Tables["LOLITA"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LolitaPrimaryKey pk;
                    pk.Lol = (System.Int32)sourceRow["LOL"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Lol);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOL"] = sourceRow["LOL"];
                    targetRow["LOL2"] = sourceRow["LOL2"];
                    targetRow["LOL3"] = sourceRow["LOL3"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMedia)
            {
                DataTable sourceTable = newdata.Tables["MEDIA"];
                DataTable targetTable = cache.Tables["MEDIA"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaPrimaryKey pk;
                    pk.MediaId = (System.Int32)sourceRow["MEDIA_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MediaId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MEDIA_ID"] = sourceRow["MEDIA_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["TYPE"] = sourceRow["TYPE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMediaAcceptors)
            {
                DataTable sourceTable = newdata.Tables["MEDIA_ACCEPTORS"];
                DataTable targetTable = cache.Tables["MEDIA_ACCEPTORS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaAcceptorsPrimaryKey pk;
                    pk.MaId = (System.Int32)sourceRow["MA_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MaId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MA_ID"] = sourceRow["MA_ID"];
                    targetRow["MEDIA_ID"] = sourceRow["MEDIA_ID"];
                    targetRow["INITIALIZATION"] = sourceRow["INITIALIZATION"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMediaType)
            {
                DataTable sourceTable = newdata.Tables["MEDIA_TYPE"];
                DataTable targetTable = cache.Tables["MEDIA_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaTypePrimaryKey pk;
                    pk.Id = (System.Byte)sourceRow["ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Id);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ID"] = sourceRow["ID"];
                    targetRow["Name"] = sourceRow["Name"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperator)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR"];
                DataTable targetTable = cache.Tables["OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["LOGIN"] = sourceRow["LOGIN"];
                    targetRow["PHONE"] = sourceRow["PHONE"];
                    targetRow["EMAIL"] = sourceRow["EMAIL"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorgroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupOperator)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_OPERATOR"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupOperatorPrimaryKey pk;
                    pk.OperatorgroupOperatorId = (System.Int32)sourceRow["OPERATORGROUP_OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorgroupOperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_OPERATOR_ID"] = sourceRow["OPERATORGROUP_OPERATOR_ID"];
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processPassword)
            {
                DataTable sourceTable = newdata.Tables["PASSWORD"];
                DataTable targetTable = cache.Tables["PASSWORD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    PasswordPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["PASSWORD"] = sourceRow["PASSWORD"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRRightOperator)
            {
                DataTable sourceTable = newdata.Tables["R_RIGHT_OPERATOR"];
                DataTable targetTable = cache.Tables["R_RIGHT_OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RRightOperatorPrimaryKey pk;
                    pk.RRightOperatorId = (System.Int32)sourceRow["R_RIGHT_OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RRightOperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["R_RIGHT_OPERATOR_ID"] = sourceRow["R_RIGHT_OPERATOR_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ALLOW_MASK"] = sourceRow["ALLOW_MASK"];
                    targetRow["DENY_MASK"] = sourceRow["DENY_MASK"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRRightOperatorgroup)
            {
                DataTable sourceTable = newdata.Tables["R_RIGHT_OPERATORGROUP"];
                DataTable targetTable = cache.Tables["R_RIGHT_OPERATORGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RRightOperatorgroupPrimaryKey pk;
                    pk.RRightOperatorgroupId = (System.Int32)sourceRow["R_RIGHT_OPERATORGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RRightOperatorgroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["R_RIGHT_OPERATORGROUP_ID"] = sourceRow["R_RIGHT_OPERATORGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ALLOW_MASK"] = sourceRow["ALLOW_MASK"];
                    targetRow["DENY_MASK"] = sourceRow["DENY_MASK"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRight)
            {
                DataTable sourceTable = newdata.Tables["RIGHT"];
                DataTable targetTable = cache.Tables["RIGHT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RightPrimaryKey pk;
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RightId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["RIGHT_NAME"] = sourceRow["RIGHT_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSession)
            {
                DataTable sourceTable = newdata.Tables["SESSION"];
                DataTable targetTable = cache.Tables["SESSION"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SessionPrimaryKey pk;
                    pk.SessionId = (System.Int32)sourceRow["SESSION_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SessionId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SESSION_ID"] = sourceRow["SESSION_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["HOST_IP"] = sourceRow["HOST_IP"];
                    targetRow["HOST_NAME"] = sourceRow["HOST_NAME"];
                    targetRow["SESSION_START"] = sourceRow["SESSION_START"];
                    targetRow["SESSION_END"] = sourceRow["SESSION_END"];
                    targetRow["CLIENT_VERSION"] = sourceRow["CLIENT_VERSION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSysdiagrams)
            {
                DataTable sourceTable = newdata.Tables["sysdiagrams"];
                DataTable targetTable = cache.Tables["sysdiagrams"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SysdiagramsPrimaryKey pk;
                    pk.DiagramId = (System.Int32)sourceRow["diagram_id"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DiagramId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["name"] = sourceRow["name"];
                    targetRow["principal_id"] = sourceRow["principal_id"];
                    targetRow["diagram_id"] = sourceRow["diagram_id"];
                    targetRow["version"] = sourceRow["version"];
                    targetRow["definition"] = sourceRow["definition"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processTrail)
            {
                DataTable sourceTable = newdata.Tables["TRAIL"];
                DataTable targetTable = cache.Tables["TRAIL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    TrailPrimaryKey pk;
                    pk.TrailId = (System.Int32)sourceRow["TRAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TrailId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TRAIL_ID"] = sourceRow["TRAIL_ID"];
                    targetRow["SESSION_ID"] = sourceRow["SESSION_ID"];
                    targetRow["TRAIL_TIME"] = sourceRow["TRAIL_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            DatabaseSchema.MakeSchema(cache);
            //cache.EnforceConstraints = oldForce;
        }
    }
}
