using System.Data;

namespace FORIS.TSS.BusinessLogic.TspTerminal.Schema
{
    ///<summary>
    /// Class for creating table schema on client
    ///</summary>
    public class DatabaseSchema : DatabaseSchemaBase, IDatabaseSchema
    {
        #region IDatabaseSchema Members
        void IDatabaseSchema.MakeSchema(DataSet dataSet)
        {
            DatabaseSchema.MakeSchema(dataSet);
        }
        #endregion // IDatabaseSchema Members
        /// <summary>
        /// Initialises list of tables
        /// </summary>
        static DatabaseSchema()
        {
            tables.Add("CL_0000000004");
            tables.Add("CL_0000000005");
            tables.Add("CL_0000000006");
            tables.Add("CL_0000000020");
            tables.Add("CL_0000000022");
            tables.Add("CL_0000000023");
            tables.Add("CL_0000000032");
            tables.Add("CL_0000000035");
            tables.Add("CL_0000000036");
            tables.Add("CL_0000000037");
            tables.Add("CL_0000000038");
            tables.Add("CL_0000000039");
            tables.Add("CL_0000000040");
            tables.Add("CL_0000000041");
            tables.Add("CL_0000000042");
            tables.Add("CL_0000000043");
            tables.Add("CL_0000000044");
            tables.Add("CL_0000000045");
            tables.Add("CL_0000000046");
            tables.Add("CL_0000000049");
            tables.Add("CL_0000000050");
            tables.Add("CL_0000000051");
            tables.Add("CONSTANTS");
            tables.Add("CONTROLLER_SENSOR_TYPE");
            tables.Add("CONTROLLER_SENSOR_LEGEND");
            tables.Add("CONTROLLER_TYPE");
            tables.Add("CONTROLLER");
            tables.Add("CONTROLLER_SENSOR");
            tables.Add("CONTROLLER_SENSOR_MAP");
            tables.Add("DEPARTMENT");
            tables.Add("LOLITA");
            tables.Add("MEDIA_TYPE");
            tables.Add("MEDIA");
            tables.Add("MEDIA_ACCEPTORS");
            tables.Add("OPERATOR");
            tables.Add("CONSUMER");
            tables.Add("OPERATORGROUP");
            tables.Add("OPERATORGROUP_OPERATOR");
            tables.Add("PASSWORD");
            tables.Add("RIGHT");
            tables.Add("R_RIGHT_OPERATOR");
            tables.Add("R_RIGHT_OPERATORGROUP");
            tables.Add("SESSION");
            tables.Add("sysdiagrams");
            tables.Add("TRAIL");
            h_tables.Add("H_CONTROLLER_SENSOR_TYPE");
            h_tables.Add("H_CONTROLLER_SENSOR_LEGEND");
            h_tables.Add("H_CONTROLLER_TYPE");
            h_tables.Add("H_CONTROLLER");
            h_tables.Add("H_CONTROLLER_SENSOR");
            h_tables.Add("H_CONTROLLER_SENSOR_MAP");
            h_tables.Add("H_DEPARTMENT");
            h_tables.Add("H_LOLITA");
            h_tables.Add("H_MEDIA_TYPE");
            h_tables.Add("H_MEDIA");
            h_tables.Add("H_MEDIA_ACCEPTORS");
            h_tables.Add("H_OPERATOR");
            h_tables.Add("H_CONSUMER");
            h_tables.Add("H_OPERATORGROUP");
            h_tables.Add("H_OPERATORGROUP_OPERATOR");
            h_tables.Add("H_PASSWORD");
            h_tables.Add("H_RIGHT");
            h_tables.Add("H_R_RIGHT_OPERATOR");
            h_tables.Add("H_R_RIGHT_OPERATORGROUP");
        }
        ///<summary>
        /// [CL_0000000004]
        ///</summary>
        public static void MakeTable_Cl0000000004(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000004") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000004");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000004
        ///</summary>
        public static void MakePK_Cl0000000004(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000004"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000005]
        ///</summary>
        public static void MakeTable_Cl0000000005(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000005") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000005");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000005
        ///</summary>
        public static void MakePK_Cl0000000005(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000005"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000006]
        ///</summary>
        public static void MakeTable_Cl0000000006(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000006") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000006");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000006
        ///</summary>
        public static void MakePK_Cl0000000006(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000006"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000020]
        ///</summary>
        public static void MakeTable_Cl0000000020(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000020") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000020");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000020
        ///</summary>
        public static void MakePK_Cl0000000020(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000020"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000022]
        ///</summary>
        public static void MakeTable_Cl0000000022(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000022") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000022");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000022
        ///</summary>
        public static void MakePK_Cl0000000022(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000022"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000023]
        ///</summary>
        public static void MakeTable_Cl0000000023(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000023") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000023");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000023
        ///</summary>
        public static void MakePK_Cl0000000023(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000023"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000032]
        ///</summary>
        public static void MakeTable_Cl0000000032(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000032") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000032");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000032
        ///</summary>
        public static void MakePK_Cl0000000032(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000032"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000035]
        ///</summary>
        public static void MakeTable_Cl0000000035(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000035") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000035");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000035
        ///</summary>
        public static void MakePK_Cl0000000035(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000035"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000036]
        ///</summary>
        public static void MakeTable_Cl0000000036(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000036") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000036");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000036
        ///</summary>
        public static void MakePK_Cl0000000036(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000036"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000037]
        ///</summary>
        public static void MakeTable_Cl0000000037(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000037") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000037");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000037
        ///</summary>
        public static void MakePK_Cl0000000037(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000037"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000038]
        ///</summary>
        public static void MakeTable_Cl0000000038(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000038") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000038");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000038
        ///</summary>
        public static void MakePK_Cl0000000038(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000038"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000039]
        ///</summary>
        public static void MakeTable_Cl0000000039(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000039") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000039");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000039
        ///</summary>
        public static void MakePK_Cl0000000039(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000039"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000040]
        ///</summary>
        public static void MakeTable_Cl0000000040(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000040") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000040");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000040
        ///</summary>
        public static void MakePK_Cl0000000040(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000040"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000041]
        ///</summary>
        public static void MakeTable_Cl0000000041(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000041") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000041");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000041
        ///</summary>
        public static void MakePK_Cl0000000041(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000041"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000042]
        ///</summary>
        public static void MakeTable_Cl0000000042(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000042") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000042");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000042
        ///</summary>
        public static void MakePK_Cl0000000042(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000042"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000043]
        ///</summary>
        public static void MakeTable_Cl0000000043(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000043") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000043");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000049
        ///</summary>
        public static void MakePK_Cl0000000043(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000043"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000044]
        ///</summary>
        public static void MakeTable_Cl0000000044(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000044") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000044");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000044
        ///</summary>
        public static void MakePK_Cl0000000044(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000044"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000045]
        ///</summary>
        public static void MakeTable_Cl0000000045(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000045") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000045");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000045
        ///</summary>
        public static void MakePK_Cl0000000045(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000045"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000046]
        ///</summary>
        public static void MakeTable_Cl0000000046(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000046") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000046");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000046
        ///</summary>
        public static void MakePK_Cl0000000046(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000046"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000049]
        ///</summary>
        public static void MakeTable_Cl0000000049(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000049") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000049");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000052
        ///</summary>
        public static void MakePK_Cl0000000049(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000049"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000050]
        ///</summary>
        public static void MakeTable_Cl0000000050(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000050") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000050");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000050
        ///</summary>
        public static void MakePK_Cl0000000050(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000050"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CL_0000000051]
        ///</summary>
        public static void MakeTable_Cl0000000051(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000051") ) return;
            DataTable table = dataset.Tables.Add("CL_0000000051");
            table.Columns.Add("LOG_TIME", typeof(System.DateTime)); // datetime
            table.Columns.Add("VALID", typeof(System.Boolean)); // bit
            table.Columns.Add("LONGITUDE", typeof(System.Single)); // real
            table.Columns.Add("LATITUDE", typeof(System.Single)); // real
            table.Columns.Add("SPEED", typeof(System.Single)); // real
            table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
            table.Columns.Add("PROPERTIES", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CL_0000000051
        ///</summary>
        public static void MakePK_Cl0000000051(DataSet dataset)
        {
            DataTable table = dataset.Tables["CL_0000000051"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOG_TIME"]
            }
            ;
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void MakeTable_Constants(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONSTANTS") ) return;
            DataTable table = dataset.Tables.Add("CONSTANTS");
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("VALUE", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONSTANTS
        ///</summary>
        public static void MakePK_Constants(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONSTANTS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["NAME"]
            }
            ;
        }
        ///<summary>
        /// [CONSUMER]
        ///</summary>
        public static void MakeTable_Consumer(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONSUMER") ) return;
            DataTable table = dataset.Tables.Add("CONSUMER");
            table.Columns.Add("CONSUMER_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONSUMER
        ///</summary>
        public static void MakePK_Consumer(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONSUMER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONSUMER_ID"]
            }
            ;
        }
        ///<summary>
        /// CONSUMER
        ///</summary>
        public static void MakeIdentities_Consumer(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONSUMER"];
            table.Columns["CONSUMER_ID"].AutoIncrement = true;
            table.Columns["CONSUMER_ID"].AutoIncrementSeed = -1;
            table.Columns["CONSUMER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void MakeTable_Controller(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER");
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("PHONE", typeof(System.String)); // nvarchar
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("FIRMWARE", typeof(System.String)); // nvarchar
            table.Columns.Add("SETTINGS", typeof(System.Byte[])); // varbinary
            table.Columns.Add("IMEI", typeof(System.Int64)); // bigint
        }
        ///<summary>
        /// PK_CONTROLLER
        ///</summary>
        public static void MakePK_Controller(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER
        ///</summary>
        public static void MakeIdentities_Controller(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER"];
            table.Columns["CONTROLLER_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void MakeTable_ControllerSensor(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR");
            table.Columns.Add("CONTROLLER_SENSOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NUMBER", typeof(System.Int32)); // int
            table.Columns.Add("MAX_VALUE", typeof(System.Int32)); // int
            table.Columns.Add("MIN_VALUE", typeof(System.Int32)); // int
            table.Columns.Add("DEFAULT_VALUE", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR
        ///</summary>
        public static void MakePK_ControllerSensor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR
        ///</summary>
        public static void MakeIdentities_ControllerSensor(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR"];
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void MakeTable_ControllerSensorLegend(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_LEGEND");
            table.Columns.Add("CONTROLLER_SENSOR_LEGEND_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_LEGEND
        ///</summary>
        public static void MakePK_ControllerSensorLegend(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_LEGEND_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_LEGEND
        ///</summary>
        public static void MakeIdentities_ControllerSensorLegend(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_LEGEND"];
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_LEGEND_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void MakeTable_ControllerSensorMap(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_MAP");
            table.Columns.Add("CONTROLLER_SENSOR_MAP_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("CONTROLLER_SENSOR_LEGEND_ID", typeof(System.Int32)); // int
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_MAP
        ///</summary>
        public static void MakePK_ControllerSensorMap(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_MAP_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_MAP
        ///</summary>
        public static void MakeIdentities_ControllerSensorMap(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_MAP"];
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_MAP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void MakeTable_ControllerSensorType(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_SENSOR_TYPE");
            table.Columns.Add("CONTROLLER_SENSOR_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakePK_ControllerSensorType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_SENSOR_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakeIdentities_ControllerSensorType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_SENSOR_TYPE"];
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_SENSOR_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void MakeTable_ControllerType(DataSet dataset)
        {
            if (dataset.Tables.Contains("CONTROLLER_TYPE") ) return;
            DataTable table = dataset.Tables.Add("CONTROLLER_TYPE");
            table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SETTINGS", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK_CONTROLLER_TYPE
        ///</summary>
        public static void MakePK_ControllerType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["CONTROLLER_TYPE_ID"]
            }
            ;
        }
        ///<summary>
        /// CONTROLLER_TYPE
        ///</summary>
        public static void MakeIdentities_ControllerType(DataSet dataset)
        {
            DataTable table = dataset.Tables["CONTROLLER_TYPE"];
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrement = true;
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementSeed = -1;
            table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [DEPARTMENT]
        ///</summary>
        public static void MakeTable_Department(DataSet dataset)
        {
            if (dataset.Tables.Contains("DEPARTMENT") ) return;
            DataTable table = dataset.Tables.Add("DEPARTMENT");
            table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// [LOLITA]
        ///</summary>
        public static void MakeTable_Lolita(DataSet dataset)
        {
            if (dataset.Tables.Contains("LOLITA") ) return;
            DataTable table = dataset.Tables.Add("LOLITA");
            table.Columns.Add("LOL", typeof(System.Int32)); // int
            table.Columns.Add("LOL2", typeof(System.Int32)); // int
            table.Columns.Add("LOL3", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_LOLITA
        ///</summary>
        public static void MakePK_Lolita(DataSet dataset)
        {
            DataTable table = dataset.Tables["LOLITA"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["LOL"]
            }
            ;
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void MakeTable_Media(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA") ) return;
            DataTable table = dataset.Tables.Add("MEDIA");
            table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("TYPE", typeof(System.Byte)); // tinyint
        }
        ///<summary>
        /// PK_MEDIA
        ///</summary>
        public static void MakePK_Media(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MEDIA_ID"]
            }
            ;
        }
        ///<summary>
        /// MEDIA
        ///</summary>
        public static void MakeIdentities_Media(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA"];
            table.Columns["MEDIA_ID"].AutoIncrement = true;
            table.Columns["MEDIA_ID"].AutoIncrementSeed = -1;
            table.Columns["MEDIA_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void MakeTable_MediaAcceptors(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS") ) return;
            DataTable table = dataset.Tables.Add("MEDIA_ACCEPTORS");
            table.Columns.Add("MA_ID", typeof(System.Int32)); // int
            table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
            table.Columns.Add("INITIALIZATION", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MEDIA_ACCEPTORS
        ///</summary>
        public static void MakePK_MediaAcceptors(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["MA_ID"]
            }
            ;
        }
        ///<summary>
        /// MEDIA_ACCEPTORS
        ///</summary>
        public static void MakeIdentities_MediaAcceptors(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
            table.Columns["MA_ID"].AutoIncrement = true;
            table.Columns["MA_ID"].AutoIncrementSeed = -1;
            table.Columns["MA_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void MakeTable_MediaType(DataSet dataset)
        {
            if (dataset.Tables.Contains("MEDIA_TYPE") ) return;
            DataTable table = dataset.Tables.Add("MEDIA_TYPE");
            table.Columns.Add("ID", typeof(System.Byte)); // tinyint
            table.Columns.Add("Name", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_MEDIA_TYPE
        ///</summary>
        public static void MakePK_MediaType(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_TYPE"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["ID"]
            }
            ;
        }
        ///<summary>
        /// MEDIA_TYPE
        ///</summary>
        public static void MakeIdentities_MediaType(DataSet dataset)
        {
            DataTable table = dataset.Tables["MEDIA_TYPE"];
            table.Columns["ID"].AutoIncrement = true;
            table.Columns["ID"].AutoIncrementSeed = -1;
            table.Columns["ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void MakeTable_Operator(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("OPERATOR");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("LOGIN", typeof(System.String)); // nvarchar
            table.Columns.Add("PHONE", typeof(System.String)); // nvarchar
            table.Columns.Add("EMAIL", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_OPERATOR
        ///</summary>
        public static void MakePK_Operator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATOR
        ///</summary>
        public static void MakeIdentities_Operator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATOR"];
            table.Columns["OPERATOR_ID"].AutoIncrement = true;
            table.Columns["OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void MakeTable_Operatorgroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP");
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_OPERATORGROUP
        ///</summary>
        public static void MakePK_Operatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATORGROUP
        ///</summary>
        public static void MakeIdentities_Operatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP"];
            table.Columns["OPERATORGROUP_ID"].AutoIncrement = true;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATORGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void MakeTable_OperatorgroupOperator(DataSet dataset)
        {
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("OPERATORGROUP_OPERATOR");
            table.Columns.Add("OPERATORGROUP_OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_OPERATORGROUP_OPERATOR
        ///</summary>
        public static void MakePK_OperatorgroupOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATORGROUP_OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// OPERATORGROUP_OPERATOR
        ///</summary>
        public static void MakeIdentities_OperatorgroupOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["OPERATORGROUP_OPERATOR"];
            table.Columns["OPERATORGROUP_OPERATOR_ID"].AutoIncrement = true;
            table.Columns["OPERATORGROUP_OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATORGROUP_OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [PASSWORD]
        ///</summary>
        public static void MakeTable_Password(DataSet dataset)
        {
            if (dataset.Tables.Contains("PASSWORD") ) return;
            DataTable table = dataset.Tables.Add("PASSWORD");
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("PASSWORD", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_PASSWORD
        ///</summary>
        public static void MakePK_Password(DataSet dataset)
        {
            DataTable table = dataset.Tables["PASSWORD"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// PASSWORD
        ///</summary>
        public static void MakeIdentities_Password(DataSet dataset)
        {
            DataTable table = dataset.Tables["PASSWORD"];
            table.Columns["OPERATOR_ID"].AutoIncrement = true;
            table.Columns["OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [R_RIGHT_OPERATOR]
        ///</summary>
        public static void MakeTable_RRightOperator(DataSet dataset)
        {
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR") ) return;
            DataTable table = dataset.Tables.Add("R_RIGHT_OPERATOR");
            table.Columns.Add("R_RIGHT_OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOW_MASK", typeof(System.Int32)); // int
            table.Columns.Add("DENY_MASK", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_R_RIGHT_OPERATOR
        ///</summary>
        public static void MakePK_RRightOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["R_RIGHT_OPERATOR"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["R_RIGHT_OPERATOR_ID"]
            }
            ;
        }
        ///<summary>
        /// R_RIGHT_OPERATOR
        ///</summary>
        public static void MakeIdentities_RRightOperator(DataSet dataset)
        {
            DataTable table = dataset.Tables["R_RIGHT_OPERATOR"];
            table.Columns["R_RIGHT_OPERATOR_ID"].AutoIncrement = true;
            table.Columns["R_RIGHT_OPERATOR_ID"].AutoIncrementSeed = -1;
            table.Columns["R_RIGHT_OPERATOR_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [R_RIGHT_OPERATORGROUP]
        ///</summary>
        public static void MakeTable_RRightOperatorgroup(DataSet dataset)
        {
            if (dataset.Tables.Contains("R_RIGHT_OPERATORGROUP") ) return;
            DataTable table = dataset.Tables.Add("R_RIGHT_OPERATORGROUP");
            table.Columns.Add("R_RIGHT_OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATORGROUP_ID", typeof(System.Int32)); // int
            table.Columns.Add("ALLOW_MASK", typeof(System.Int32)); // int
            table.Columns.Add("DENY_MASK", typeof(System.Int32)); // int
        }
        ///<summary>
        /// PK_R_RIGHT_OPERATORGROUP
        ///</summary>
        public static void MakePK_RRightOperatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["R_RIGHT_OPERATORGROUP"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["R_RIGHT_OPERATORGROUP_ID"]
            }
            ;
        }
        ///<summary>
        /// R_RIGHT_OPERATORGROUP
        ///</summary>
        public static void MakeIdentities_RRightOperatorgroup(DataSet dataset)
        {
            DataTable table = dataset.Tables["R_RIGHT_OPERATORGROUP"];
            table.Columns["R_RIGHT_OPERATORGROUP_ID"].AutoIncrement = true;
            table.Columns["R_RIGHT_OPERATORGROUP_ID"].AutoIncrementSeed = -1;
            table.Columns["R_RIGHT_OPERATORGROUP_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void MakeTable_Right(DataSet dataset)
        {
            if (dataset.Tables.Contains("RIGHT") ) return;
            DataTable table = dataset.Tables.Add("RIGHT");
            table.Columns.Add("RIGHT_ID", typeof(System.Int32)); // int
            table.Columns.Add("NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
            table.Columns.Add("RIGHT_NAME", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_RIGHT
        ///</summary>
        public static void MakePK_Right(DataSet dataset)
        {
            DataTable table = dataset.Tables["RIGHT"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["RIGHT_ID"]
            }
            ;
        }
        ///<summary>
        /// RIGHT
        ///</summary>
        public static void MakeIdentities_Right(DataSet dataset)
        {
            DataTable table = dataset.Tables["RIGHT"];
            table.Columns["RIGHT_ID"].AutoIncrement = true;
            table.Columns["RIGHT_ID"].AutoIncrementSeed = -1;
            table.Columns["RIGHT_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void MakeTable_Session(DataSet dataset)
        {
            if (dataset.Tables.Contains("SESSION") ) return;
            DataTable table = dataset.Tables.Add("SESSION");
            table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
            table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
            table.Columns.Add("HOST_IP", typeof(System.String)); // nvarchar
            table.Columns.Add("HOST_NAME", typeof(System.String)); // nvarchar
            table.Columns.Add("SESSION_START", typeof(System.DateTime)); // datetime
            table.Columns.Add("SESSION_END", typeof(System.DateTime)); // datetime
            table.Columns.Add("CLIENT_VERSION", typeof(System.String)); // nvarchar
        }
        ///<summary>
        /// PK_SESSION
        ///</summary>
        public static void MakePK_Session(DataSet dataset)
        {
            DataTable table = dataset.Tables["SESSION"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["SESSION_ID"]
            }
            ;
        }
        ///<summary>
        /// SESSION
        ///</summary>
        public static void MakeIdentities_Session(DataSet dataset)
        {
            DataTable table = dataset.Tables["SESSION"];
            table.Columns["SESSION_ID"].AutoIncrement = true;
            table.Columns["SESSION_ID"].AutoIncrementSeed = -1;
            table.Columns["SESSION_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void MakeTable_Sysdiagrams(DataSet dataset)
        {
            if (dataset.Tables.Contains("sysdiagrams") ) return;
            DataTable table = dataset.Tables.Add("sysdiagrams");
            table.Columns.Add("name", typeof(System.String)); // nvarchar
            table.Columns.Add("principal_id", typeof(System.Int32)); // int
            table.Columns.Add("diagram_id", typeof(System.Int32)); // int
            table.Columns.Add("version", typeof(System.Int32)); // int
            table.Columns.Add("definition", typeof(System.Byte[])); // varbinary
        }
        ///<summary>
        /// PK__sysdiagrams__3A81B327
        ///</summary>
        public static void MakePK_Sysdiagrams(DataSet dataset)
        {
            DataTable table = dataset.Tables["sysdiagrams"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["diagram_id"]
            }
            ;
        }
        ///<summary>
        /// sysdiagrams
        ///</summary>
        public static void MakeIdentities_Sysdiagrams(DataSet dataset)
        {
            DataTable table = dataset.Tables["sysdiagrams"];
            table.Columns["diagram_id"].AutoIncrement = true;
            table.Columns["diagram_id"].AutoIncrementSeed = -1;
            table.Columns["diagram_id"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void MakeTable_Trail(DataSet dataset)
        {
            if (dataset.Tables.Contains("TRAIL") ) return;
            DataTable table = dataset.Tables.Add("TRAIL");
            table.Columns.Add("TRAIL_ID", typeof(System.Int32)); // int
            table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
            table.Columns.Add("TRAIL_TIME", typeof(System.DateTime)); // datetime
        }
        ///<summary>
        /// PK_TRAIL
        ///</summary>
        public static void MakePK_Trail(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRAIL"];
            if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
            table.PrimaryKey = new DataColumn[]{
                table.Columns["TRAIL_ID"]
            }
            ;
        }
        ///<summary>
        /// TRAIL
        ///</summary>
        public static void MakeIdentities_Trail(DataSet dataset)
        {
            DataTable table = dataset.Tables["TRAIL"];
            table.Columns["TRAIL_ID"].AutoIncrement = true;
            table.Columns["TRAIL_ID"].AutoIncrementSeed = -1;
            table.Columns["TRAIL_ID"].AutoIncrementStep = -1;
        }
        ///<summary>
        /// FK_CONSUMER_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_CONSUMER_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONSUMER_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["CONSUMER"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_CONSUMER_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_CONTROLLER_TYPE
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_CONTROLLER_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_CONTROLLER_TYPE")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_TYPE"].Columns["CONTROLLER_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_TYPE_ID"];
            dataset.Relations.Add("FK_CONTROLLER_CONTROLLER_TYPE", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_CONTROLLER_TYPE
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_CONTROLLER_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_CONTROLLER_TYPE")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_TYPE"].Columns["CONTROLLER_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_TYPE_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_CONTROLLER_TYPE", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Columns["CONTROLLER_SENSOR_TYPE_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_MAP_CONTROLLER
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_MAP_CONTROLLER")) return;
            DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_MAP_CONTROLLER", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR"].Columns["CONTROLLER_SENSOR_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_SENSOR_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR", parent, child);
        }
        ///<summary>
        /// FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND
        ///</summary>
        public static void MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND")) return;
            DataColumn parent = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Columns["CONTROLLER_SENSOR_LEGEND_ID"];
            DataColumn child = dataset.Tables["CONTROLLER_SENSOR_MAP"].Columns["CONTROLLER_SENSOR_LEGEND_ID"];
            dataset.Relations.Add("FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND", parent, child);
        }
        ///<summary>
        /// FK_MEDIA_ACCEPTORS_MEDIA
        ///</summary>
        public static void MakeRelation_FK_MEDIA_ACCEPTORS_MEDIA(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_MEDIA_ACCEPTORS_MEDIA")) return;
            DataColumn parent = dataset.Tables["MEDIA"].Columns["MEDIA_ID"];
            DataColumn child = dataset.Tables["MEDIA_ACCEPTORS"].Columns["MEDIA_ID"];
            dataset.Relations.Add("FK_MEDIA_ACCEPTORS_MEDIA", parent, child);
        }
        ///<summary>
        /// FK_MEDIA_MEDIA_TYPE
        ///</summary>
        public static void MakeRelation_FK_MEDIA_MEDIA_TYPE(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_MEDIA_MEDIA_TYPE")) return;
            DataColumn parent = dataset.Tables["MEDIA_TYPE"].Columns["ID"];
            DataColumn child = dataset.Tables["MEDIA"].Columns["TYPE"];
            dataset.Relations.Add("FK_MEDIA_MEDIA_TYPE", parent, child);
        }
        ///<summary>
        /// FK_OPERATORGROUP_OPERATOR_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_OPERATORGROUP_OPERATOR_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_OPERATORGROUP_OPERATOR_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_OPERATORGROUP_OPERATOR_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_OPERATORGROUP_OPERATOR_OPERATORGROUP
        ///</summary>
        public static void MakeRelation_FK_OPERATORGROUP_OPERATOR_OPERATORGROUP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_OPERATORGROUP_OPERATOR_OPERATORGROUP")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["OPERATORGROUP_OPERATOR"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("FK_OPERATORGROUP_OPERATOR_OPERATORGROUP", parent, child);
        }
        ///<summary>
        /// FK_PASSWORD_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_PASSWORD_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_PASSWORD_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["PASSWORD"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_PASSWORD_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_R_OPERATOR_RIGHT_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_R_OPERATOR_RIGHT_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_OPERATOR_RIGHT_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_R_OPERATOR_RIGHT_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_R_OPERATOR_RIGHT_RIGHT
        ///</summary>
        public static void MakeRelation_FK_R_OPERATOR_RIGHT_RIGHT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_OPERATOR_RIGHT_RIGHT")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATOR"].Columns["RIGHT_ID"];
            dataset.Relations.Add("FK_R_OPERATOR_RIGHT_RIGHT", parent, child);
        }
        ///<summary>
        /// FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP
        ///</summary>
        public static void MakeRelation_FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP")) return;
            DataColumn parent = dataset.Tables["OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATORGROUP"].Columns["OPERATORGROUP_ID"];
            dataset.Relations.Add("FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP", parent, child);
        }
        ///<summary>
        /// FK_R_OPERATORGROUP_RIGHT_RIGHT
        ///</summary>
        public static void MakeRelation_FK_R_OPERATORGROUP_RIGHT_RIGHT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_OPERATORGROUP_RIGHT_RIGHT")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATORGROUP"].Columns["RIGHT_ID"];
            dataset.Relations.Add("FK_R_OPERATORGROUP_RIGHT_RIGHT", parent, child);
        }
        ///<summary>
        /// FK_R_RIGHT_OPERATOR_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_R_RIGHT_OPERATOR_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_RIGHT_OPERATOR_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATOR"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_R_RIGHT_OPERATOR_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_R_RIGHT_OPERATOR_RIGHT
        ///</summary>
        public static void MakeRelation_FK_R_RIGHT_OPERATOR_RIGHT(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_R_RIGHT_OPERATOR_RIGHT")) return;
            DataColumn parent = dataset.Tables["RIGHT"].Columns["RIGHT_ID"];
            DataColumn child = dataset.Tables["R_RIGHT_OPERATOR"].Columns["RIGHT_ID"];
            dataset.Relations.Add("FK_R_RIGHT_OPERATOR_RIGHT", parent, child);
        }
        ///<summary>
        /// FK_SESSION_OPERATOR
        ///</summary>
        public static void MakeRelation_FK_SESSION_OPERATOR(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_SESSION_OPERATOR")) return;
            DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
            DataColumn child = dataset.Tables["SESSION"].Columns["OPERATOR_ID"];
            dataset.Relations.Add("FK_SESSION_OPERATOR", parent, child);
        }
        ///<summary>
        /// FK_TRAIL_SESSION
        ///</summary>
        public static void MakeRelation_FK_TRAIL_SESSION(DataSet dataset)
        {
            if (dataset.Relations.Contains("FK_TRAIL_SESSION")) return;
            DataColumn parent = dataset.Tables["SESSION"].Columns["SESSION_ID"];
            DataColumn child = dataset.Tables["TRAIL"].Columns["SESSION_ID"];
            dataset.Relations.Add("FK_TRAIL_SESSION", parent, child);
        }
        
        public static void MakeSchema(DataSet dataset)
        {
            if (dataset.Tables.Contains("CL_0000000004"))
            {
                MakePK_Cl0000000004(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000005"))
            {
                MakePK_Cl0000000005(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000006"))
            {
                MakePK_Cl0000000006(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000020"))
            {
                MakePK_Cl0000000020(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000022"))
            {
                MakePK_Cl0000000022(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000023"))
            {
                MakePK_Cl0000000023(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000032"))
            {
                MakePK_Cl0000000032(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000035"))
            {
                MakePK_Cl0000000035(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000036"))
            {
                MakePK_Cl0000000036(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000037"))
            {
                MakePK_Cl0000000037(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000038"))
            {
                MakePK_Cl0000000038(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000039"))
            {
                MakePK_Cl0000000039(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000040"))
            {
                MakePK_Cl0000000040(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000041"))
            {
                MakePK_Cl0000000041(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000042"))
            {
                MakePK_Cl0000000042(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000043"))
            {
                MakePK_Cl0000000043(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000044"))
            {
                MakePK_Cl0000000044(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000045"))
            {
                MakePK_Cl0000000045(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000046"))
            {
                MakePK_Cl0000000046(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000049"))
            {
                MakePK_Cl0000000049(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000050"))
            {
                MakePK_Cl0000000050(dataset);
            }
            if (dataset.Tables.Contains("CL_0000000051"))
            {
                MakePK_Cl0000000051(dataset);
            }
            if (dataset.Tables.Contains("CONSTANTS"))
            {
                MakePK_Constants(dataset);
            }
            if (dataset.Tables.Contains("CONSUMER"))
            {
                MakePK_Consumer(dataset);
                MakeIdentities_Consumer(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER"))
            {
                MakePK_Controller(dataset);
                MakeIdentities_Controller(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR"))
            {
                MakePK_ControllerSensor(dataset);
                MakeIdentities_ControllerSensor(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"))
            {
                MakePK_ControllerSensorLegend(dataset);
                MakeIdentities_ControllerSensorLegend(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"))
            {
                MakePK_ControllerSensorMap(dataset);
                MakeIdentities_ControllerSensorMap(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakePK_ControllerSensorType(dataset);
                MakeIdentities_ControllerSensorType(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakePK_ControllerType(dataset);
                MakeIdentities_ControllerType(dataset);
            }
            if (dataset.Tables.Contains("LOLITA"))
            {
                MakePK_Lolita(dataset);
            }
            if (dataset.Tables.Contains("MEDIA"))
            {
                MakePK_Media(dataset);
                MakeIdentities_Media(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS"))
            {
                MakePK_MediaAcceptors(dataset);
                MakeIdentities_MediaAcceptors(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_TYPE"))
            {
                MakePK_MediaType(dataset);
                MakeIdentities_MediaType(dataset);
            }
            if (dataset.Tables.Contains("OPERATOR"))
            {
                MakePK_Operator(dataset);
                MakeIdentities_Operator(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakePK_Operatorgroup(dataset);
                MakeIdentities_Operatorgroup(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR"))
            {
                MakePK_OperatorgroupOperator(dataset);
                MakeIdentities_OperatorgroupOperator(dataset);
            }
            if (dataset.Tables.Contains("PASSWORD"))
            {
                MakePK_Password(dataset);
                MakeIdentities_Password(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR"))
            {
                MakePK_RRightOperator(dataset);
                MakeIdentities_RRightOperator(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATORGROUP"))
            {
                MakePK_RRightOperatorgroup(dataset);
                MakeIdentities_RRightOperatorgroup(dataset);
            }
            if (dataset.Tables.Contains("RIGHT"))
            {
                MakePK_Right(dataset);
                MakeIdentities_Right(dataset);
            }
            if (dataset.Tables.Contains("SESSION"))
            {
                MakePK_Session(dataset);
                MakeIdentities_Session(dataset);
            }
            if (dataset.Tables.Contains("sysdiagrams"))
            {
                MakePK_Sysdiagrams(dataset);
                MakeIdentities_Sysdiagrams(dataset);
            }
            if (dataset.Tables.Contains("TRAIL"))
            {
                MakePK_Trail(dataset);
                MakeIdentities_Trail(dataset);
            }
            if (dataset.Tables.Contains("CONSUMER") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_CONSUMER_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER") && dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakeRelation_FK_CONTROLLER_CONTROLLER_TYPE(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") && dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_CONTROLLER_SENSOR_TYPE(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR") && dataset.Tables.Contains("CONTROLLER_TYPE"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_CONTROLLER_TYPE(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND") && dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_LEGEND_CONTROLLER_SENSOR_TYPE(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER_SENSOR"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR(dataset);
            }
            if (dataset.Tables.Contains("CONTROLLER_SENSOR_MAP") && dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"))
            {
                MakeRelation_FK_CONTROLLER_SENSOR_MAP_CONTROLLER_SENSOR_LEGEND(dataset);
            }
            if (dataset.Tables.Contains("MEDIA_ACCEPTORS") && dataset.Tables.Contains("MEDIA"))
            {
                MakeRelation_FK_MEDIA_ACCEPTORS_MEDIA(dataset);
            }
            if (dataset.Tables.Contains("MEDIA") && dataset.Tables.Contains("MEDIA_TYPE"))
            {
                MakeRelation_FK_MEDIA_MEDIA_TYPE(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_OPERATORGROUP_OPERATOR_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("OPERATORGROUP_OPERATOR") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_FK_OPERATORGROUP_OPERATOR_OPERATORGROUP(dataset);
            }
            if (dataset.Tables.Contains("PASSWORD") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_PASSWORD_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_R_OPERATOR_RIGHT_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_FK_R_OPERATOR_RIGHT_RIGHT(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATORGROUP") && dataset.Tables.Contains("OPERATORGROUP"))
            {
                MakeRelation_FK_R_OPERATORGROUP_RIGHT_OPERATORGROUP(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATORGROUP") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_FK_R_OPERATORGROUP_RIGHT_RIGHT(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_R_RIGHT_OPERATOR_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("R_RIGHT_OPERATOR") && dataset.Tables.Contains("RIGHT"))
            {
                MakeRelation_FK_R_RIGHT_OPERATOR_RIGHT(dataset);
            }
            if (dataset.Tables.Contains("SESSION") && dataset.Tables.Contains("OPERATOR"))
            {
                MakeRelation_FK_SESSION_OPERATOR(dataset);
            }
            if (dataset.Tables.Contains("TRAIL") && dataset.Tables.Contains("SESSION"))
            {
                MakeRelation_FK_TRAIL_SESSION(dataset);
            }
        }
}
}
