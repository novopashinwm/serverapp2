﻿using System;
using MTMap.MathTypes;

namespace MTMap.LineMath
{
	/// <summary> Математические функции на отрезке </summary>
	public struct DLineMath //: DLine
	{
		float deltaX, aX;
		float deltaY, aY;
		/// <summary> Создать объект отрезка по его начальной и конечной точкам </summary>
		public void create(ref DPoint p1, ref DPoint p2)
		{
			deltaX = p2.x - p1.x;
			aX = p1.x;
			deltaY = p2.y - p1.y;
			aY = p1.y;
		}
		/// <summary> Создать объект отрезка по его начальной точке и приращениям до конечной точки по X и по Y </summary>
		public void create(ref DPoint p1, float dX, float dY)
		{
			deltaX = dX;
			aX = p1.x;
			deltaY = dY;
			aY = p1.y;
		}
		/// <summary> Создать отрезок, параллельный данному исходящий из заданной точки </summary>
		public void createParallelOnBP(ref DLineMath l, ref DPoint p)
		{
			deltaX = l.deltaX;
			aX = p.x;
			deltaY = l.deltaY;
			aY = p.y;
		}
		/// <summary> Создать отрезок, перпендикулярный данному исходящий из заданной точки и в заданном направлении </summary>
		public void createPerpendOnBP(ref DLineMath l, ref DPoint p, bool ToRight)
		{
			if (ToRight) deltaX = +l.deltaY; else deltaX = -l.deltaY;
			aX = p.x;
			if (ToRight) deltaY = -l.deltaX; else deltaY = +l.deltaX;
			aY = p.y;
		}
		/// <summary> Создать отрезок, который располагается под заданным углом к данному исходящий из заданной точки </summary>
		public void createCrossOnBP(ref DLineMath l, ref DPoint p, double angle)
		{
			deltaX = l.deltaX;
			aX = p.x;
			deltaY = l.deltaY;
			aY = p.y;

			rotate(angle);
		}
		/// <summary> Создать отрезок, параллельный данному исходящий из точки, находящейся слева от вектора на расстоянии distance </summary>
		public void createParallelAtDistance(ref DLineMath l, float distance)
		{
			deltaX = -l.deltaY;
			deltaY = +l.deltaX;
			DPoint p;

			calcPointAtDistFromA(distance, out p);

			aX = p.x;
			aY = p.y;
			deltaX = l.deltaX;
			deltaY = l.deltaY;
		}
		private float DistancePrim(float dx, float dy)
		{
			return (float)Math.Sqrt(dx * dx + dy * dy);
		}
		/// <summary> Расстояние от отрезка до точки (с учётом крайних точек отрезка) </summary>
		public float pieceDistanceTo(ref DPoint p)
		{
			float t;
			t = kABPerpend(p);

			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary> Вычисляет расстояние от этого отрезка данной точки и параметр t - точки на отрезке, ближайшей к данной </summary>
		/// <param name="p">Данная точка</param>
		/// <param name="t">Параметр t - точки на отрезке, ближайшей к данной точке</param>
		/// <returns>Вычисленное расстояние</returns>
		public float pieceDistanceToExt(ref DPoint p, out float t)
		{
			t = kABPerpend(p);
			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary> Расстояние от прямой до точки </summary>
		public float lineDistanceTo(ref DPoint p)
		{
			float t;
			t = tPerpend(p);
			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary> Расстояние от прямой до точки со знаком, обозначающим их взаимное расположение (левое/правое) </summary>
		public float vectorDistanceTo(ref DPoint p)
		{
			float t, result;
			t = tPerpend(p);

			result = DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
			if (result > MathConst.NearlyZero)
			{
				DPoint tempPoint1 = new DPoint(aX, aY);
				DPoint tempPoint2 = new DPoint(aX + deltaX, aY + deltaY);
				if (Math2D.calcSignedTriangleArea(ref tempPoint1,
					ref tempPoint2, ref p) < 0) return -result;
			}
			return result;
		}
		/// <summary>Опустить перпендикуляр из точки на прямую, возвращает параметр t </summary>
		public float tPerpend(DPoint p)
		{
			float aCSqr;
			try
			{
				aCSqr = deltaX * deltaX + deltaY * deltaY;
				return (p.x - aX) * (deltaX / aCSqr) + (p.y - aY) * (deltaY / aCSqr);
			}
			catch
			{
				return (float)MathConst.Infinity;
			}
		}
		/// <summary> Опустить перпендикуляр из точки на отрезок, возвращает параметр t, с ограничением пределами отрезка </summary>
		/// <param name="p">Точка, из которой опускается перпендикуляр</param>
		/// <returns>Результат в диапазоне [0..1]</returns>
		public float kABPerpend(DPoint p)
		{
			float t;
			t = tPerpend(p);
			if (t < 0)
			{
				return 0;
			}
			else if (t > 1)
			{
				return 1;
			}
			else return t;
		}
		/// <summary> Первая точка отрезка </summary>
		public DPoint getFirstPoint()
		{
			DPoint result = new DPoint(aX, aY);
			result.x = aX;
			result.y = aY;

			return result;
		}
		/// <summary> Вторая точка отрезка </summary>
		public DPoint getSecondPoint()
		{
			DPoint result;
			result.x = deltaX + aX;
			result.y = deltaY + aY;

			return result;
		}
		/// <summary> Вычисляет координаты точки, для заданного параметра t </summary>
		public DPoint calcXY(float t)
		{
			DPoint result;
			result.x = deltaX * t + aX;
			result.y = deltaY * t + aY;

			return result;
		}
		float calcX(float t)
		{
			return deltaX * t + aX;
		}
		float calcY(float t)
		{
			return deltaY * t + aY;
		}

		/// <summary> Вычисляет пересечение двух прямых </summary>
		public bool crossLine(DLineMath l, ref float t)
		{
			float lt, dd;

			if (Math.Abs(deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return false;
				if (Math.Abs(deltaY) < MathConst.AboutZero) return false;
				lt = (aX - l.aX) / l.deltaX;
				t = (l.deltaY * lt + l.aY - aY) / deltaY;
			}
			else if (Math.Abs(l.deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaX) < MathConst.AboutZero) return false;
				t = (l.aX - aX) / deltaX;
			}
			else if (Math.Abs(deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return false;
				if (Math.Abs(deltaX) < MathConst.AboutZero) return false;
				lt = (aY - l.aY) / l.deltaY;
				t = (l.deltaX * lt + l.aX - aX) / deltaX;
			}
			else if (Math.Abs(l.deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaY) < MathConst.AboutZero) return false;
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return false;
				t = (l.aY - aY) / deltaY;
			}
			else
			{
				dd = l.deltaX * deltaY - l.deltaY * deltaX;
				if (Math.Abs(dd) < MathConst.AboutZero) return false;
				t = (l.deltaY * (aX - l.aX) - l.deltaX * (aY - l.aY)) / dd;
			}

			return true;
		}
		/// <summary> Вычисляет пересечение двух отрезков </summary>
		/// <returns>0 - пересечения нет</returns>
		/// <returns>fCrossing - точка пересечения вне обоих отрезков</returns>
		/// <returns>fCrossing | fOnThisPiece - точка пересечения только вне второго отрезка</returns>
		/// <returns>fCrossing | fOnParamPiece - точка пересечения только вне первого отрезка</returns>
		/// <returns>fOnBothPieces - точка пересечения внутри обоих отрезков</returns>
		public int crossPiece(ref DLineMath l, out float t)
		{
			float lt, dd;
			int result = 0;
			t = 0;

			if (Math.Abs(deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return result;
				if (Math.Abs(deltaY) < MathConst.AboutZero) return result;
				lt = (aX - l.aX) / l.deltaX;
				t = (l.deltaY * lt + l.aY - aY) / deltaY;
			}
			else if (Math.Abs(l.deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaX) < MathConst.AboutZero) return result;
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return result;
				t = (l.aX - aX) / deltaX;
				lt = (deltaY * t + aY - l.aY) / l.deltaY;
			}
			else if (Math.Abs(deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return result;
				if (Math.Abs(deltaX) < MathConst.AboutZero) return result;
				lt = (aY - l.aY) / l.deltaY;
				t = (l.deltaX * lt + l.aX - aX) / deltaX;
			}
			else if (Math.Abs(l.deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaY) < MathConst.AboutZero) return result;
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return result;
				t = (l.aY - aY) / deltaY;
				lt = (deltaX * t + aX - l.aX) / l.deltaX;
			}
			else
			{
				dd = l.deltaX * deltaY - l.deltaY * deltaX;
				if (Math.Abs(dd) < MathConst.AboutZero) return result;
				t = (l.deltaY * (aX - l.aX) - l.deltaX * (aY - l.aY)) / dd;
				lt = (deltaY * (l.aX - aX) - deltaX * (l.aY - aY)) / (-dd);
			}

			result = result | Math2D.fCrossing;
			if ((t >= -MathConst.AboutZero) & (t <= 1 + MathConst.AboutZero))
				result = result | Math2D.fOnThisPiece;
			if ((lt >= -MathConst.AboutZero) & (lt <= 1 + MathConst.AboutZero))
				result = result | Math2D.fOnParamPiece;
			return result;
		}
		/// <summary> отличается от CrossPiece тем что условие "внутренности" точки пересечения является строгим </summary>
		public int crossPieceStrict(ref DLineMath l, ref float t)
		{
			DPoint cp;
			int result;
			result = crossPiece(ref l, out t);
			if ((result & (Math2D.fOnThisPiece | Math2D.fOnParamPiece)) != 0)
			{
				cp = calcXY(t);
				if ((Math2D.fOnThisPiece & result) != 0)
					if ((cp.distanceTo(getFirstPoint()) < MathConst.NearlyZero) || (cp.distanceTo(getSecondPoint()) < MathConst.NearlyZero))
						result = result & ~Math2D.fOnThisPiece;
				if ((Math2D.fOnParamPiece & result) != 0)
					if ((cp.distanceTo(l.getFirstPoint()) < MathConst.NearlyZero) || (cp.distanceTo(l.getSecondPoint()) < MathConst.NearlyZero))
						result = result & ~Math2D.fOnParamPiece;
			}
			return result;
		}
		/// <summary> Вычисляет координаты точки, лежащей на прямой и находящейся на заданном расстоянии от первой точки отрезка </summary>
		public float calcPointAtDistFromA(float aDistance, out DPoint p)
		{
			double k, t;
			k = DistancePrim(deltaX, deltaY);
			if (k < MathConst.NearlyZero2) throw (new ApplicationException("Вычисления на отрезке нулевой длины!"));
			t = aDistance / k;
			p.x = (float)(deltaX * t + aX);
			p.y = (float)(deltaY * t + aY);
			return (float)t;
		}
		/// <summary> Угол наклона вектора </summary>
		public double slopping()
		{
			return MathFunc.ArcTan2Ext(deltaY, deltaX);
		}
		/// <summary> Возвращает угол между этим вектором и данным (L), положительный если L направлен левее чем этот </summary>
		public double angleWithVector(DLineMath l)
		{
			return MathFunc.normAngle(l.slopping() - slopping());
		}
		/// <summary> Изменить (установить) угол наклона вектора (первая точка отрезка остаётся на месте) </summary>
		public void setSlopping(double angle)
		{
			float l;
			l = pieceLength();
			deltaX = (float)(Math.Cos(angle) * l);
			deltaY = (float)(Math.Sin(angle) * l);
		}
		/// <summary> Повернуть вектор на заданный угол </summary>
		public void rotate(double angle)
		{
			double alfa;
			float l;
			alfa = MathFunc.normAngle(slopping() + angle);
			l = pieceLength();
			deltaX = (float)(Math.Cos(alfa) * l);
			deltaY = (float)(Math.Sin(alfa) * l);
		}
		/// <summary> Вычисляет длину отрезка </summary>
		public float pieceLength()
		{
			return DistancePrim(deltaX, deltaY);
		}
	}
}