﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using DPoint = MTMap.MathTypes.DPoint;

namespace MTMap.Common
{

	/// <summary> Поток для чтения-записи данных из файла </summary>
	public class Serializer :
		Component
	{
		#region Constructor & Dispose

		/// <summary>
		/// Создание потока. Указывается имя соответствующего ему файла и режим работы (чтение/запись).
		/// </summary>
		/// <param name="map">Рабочая карта</param>
		/// <param name="fileName">Имя файла</param>
		/// <param name="readMode">Режим чтения. true == чтение, false == запись.</param>
		public Serializer(string fileName, bool readMode)
		{
			this.readMode = readMode;
			if (readMode)
			{
				fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None, bufferSize);
				binR = new BinaryReader(fileStream, Encoding.GetEncoding(1251));
				fVersion = _int();
			}
			else
			{
				fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize);
				binW = new BinaryWriter(fileStream, Encoding.GetEncoding(1251));
				_int(currentVersion);
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				close();
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// Номер текущей версии формата хранения данных (карты) в файле. Всегда используется при 
		/// сохранении/записи данных (карты) в файл. При появлении изменений в формате этот номер
		/// увеличивается каждый раз на единицу.
		/// </summary>
		public const int currentVersion = 1;

		private const int bufferSize = 2097152;
		private const string endOfFile = "Конец файла";

		/// <summary>
		/// Номер версии читаемого в данный момент файла.
		/// </summary>
		private int fVersion;
		/// <summary>
		/// Номер версии формата загружаемого файла. 
		/// Имеет смысл только в режиме чтения (загрузки карты из файла).
		/// </summary>
		public int version
		{
			get { return fVersion; }
		}

		/// <summary> Текущий режим работы потока, true - чтение из файла, false - запись в файл </summary>
		private bool readMode;

		private FileStream fileStream;
		private BinaryWriter binW;
		private BinaryReader binR;

		/// <summary> Возвращает полный размер рабочего потока </summary>
		public int getStreamLength()
		{
			return (int)fileStream.Length;
		}

		/// <summary> Закрыть поток. Вызывать когда все операции чтения/записи закончены </summary>
		public void close()
		{
			if (readMode)
			{
				if (_string() != endOfFile)
				{
					throw (new ArgumentException("Неверное окончание файла"));
				}

				binR.Close();
			}
			else
			{
				_string(endOfFile);
				binW.Close();
			}

			fileStream.Close();
		}

		/// <summary> Читает из потока значение типа bool </summary>
		/// <returns>Прочитанное значение</returns>
		public bool _bool()
		{
			Debug.Assert(readMode);
			return binR.ReadBoolean();
		}

		/// <summary> Записывает в поток значение типа bool </summary>
		public void _bool(bool value)
		{
			Debug.Assert(!readMode);
			binW.Write((bool)value);
		}

		/// <summary> Читает из потока значение типа int </summary>
		/// <returns>Прочитанное значение</returns>
		public int _int()
		{
			Debug.Assert(readMode);
			return binR.ReadInt32();
		}

		/// <summary> Записывает в поток значение типа int </summary>
		public void _int(int value)
		{
			Debug.Assert(!readMode);
			binW.Write((int)value);
		}

		/// <summary> Читает из потока значение типа double </summary>
		/// <returns>Прочитанное значение</returns>
		public double _double()
		{
			Debug.Assert(readMode);
			return binR.ReadDouble();
		}

		/// <summary> Записывает в поток значение типа double </summary>
		public void _double(double value)
		{
			Debug.Assert(!readMode);
			binW.Write((double)value);
		}

		/// <summary> Читает из потока значение типа float </summary>
		/// <returns>Прочитанное значение</returns>
		public float _float()
		{
			Debug.Assert(readMode);
			return binR.ReadSingle();
		}

		/// <summary> Записывает в поток значение типа float </summary>
		public void _float(float value)
		{
			Debug.Assert(!readMode);
			binW.Write((float)value);
		}

		/// <summary> Читает из потока значение типа string </summary>
		/// <returns>Прочитанное значение</returns>
		public string _string()
		{
			Debug.Assert(readMode);
			return binR.ReadString();
		}

		/// <summary> Записывает в поток значение типа string </summary>
		public void _string(string value)
		{
			Debug.Assert(!readMode);
			binW.Write((string)value);
		}

		/// <summary> Читает из потока значение типа DPoint </summary>
		/// <returns>Прочитанное значение</returns>
		public DPoint _DPoint()
		{
			return new DPoint(_float(), _float());
		}

		/// <summary> Записывает в поток значение типа DPoint </summary>
		public void _DPoint(DPoint value)
		{
			_float(value.x);
			_float(value.y);
		}

		private int lastReadStreamPos;

		/// <summary> Запомнить действительную текущую позицию рабочего потока </summary>
		public void refreshLastStreamPos()
		{
			lastReadStreamPos = (int)fileStream.Position;
		}

		/// <summary> Получить текущую (запомненную в refreshLastStreamPos()) позицию рабочего потока </summary>
		/// <returns></returns>
		public int getLastStreamPos()
		{
			return lastReadStreamPos;
		}
	}
}