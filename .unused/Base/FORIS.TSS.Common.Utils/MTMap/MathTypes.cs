﻿// xml_summary_tag.cs
using System;
using System.Collections;
using System.Diagnostics;
using MTMap.Common;
using MTMap.LineMath;

namespace MTMap.MathTypes
{
	/// <summary>
	/// Функции преобразования географических координат (WGS84) в локальные метровые координаты.
	/// </summary>
	public class Geo
	{
		const double kx = 63166;    //111324.72603;
		const double ky = 111411;   //197104.85314;

		//const double kxOldToNew = 0,56741;// == 63166 / 111324.72603;			kOldToNew ~= 0,566
		//const double kyOldToNew = 0,56524;// == 111411 / 197104.85314;

		const double origCenterX = 38.000;  //	( 41.00 + 35.00) / 2 = 38.00
		const double origCenterY = 55.665;  //	( 57.33 + 54.00) / 2 = 55.665

		float[] geo = new float[10] { 2.89510866630289E+32f, 3.50205541649329E-12f, 1.80114793122323E+25f, 3.38803551186428E-12f, 4.84456568460734E+27f, 1.21406087226884E+33f, 4.85854109611686E+30f, 1.75635012853238E+25f, 6.13317441410025E+28f, 2.74870113605582E+20f };

		//	x	3820576.2771426425	double
		//  y	7135589.7825076375	double

		/// <summary>
		/// Преобразовать координаты точки из глобальных (WGS84) в локальные  в метрах.
		/// </summary>
		public static void geoToPlanar(double geoX, double geoY, out DPoint planar)
		{
			planar.x = (float)(kx * (geoX - origCenterX));
			planar.y = (float)(ky * (geoY - origCenterY));
		}

		/// <summary>
		/// Преобразовать координаты точки из локальных метровых в глобальные (WGS84).
		/// </summary>
		public static void planarToGeo(DPoint planar, out double geoX, out double geoY)
		{
			geoX = planar.x / kx + origCenterX;
			geoY = planar.y / ky + origCenterY;
		}
	}

	/// <summary>
	/// Глобальные числовые константы.
	/// </summary>
	public class MathConst
	{

		/// <summary>
		/// точность сравнения координат
		/// </summary>
		public const float NearlyZero = 1E-01f;

		/// <summary>
		/// точность сравнения углов, тангенсов и т.п.
		/// </summary>
		public const double NearlyZero2 = 1E-08;

		/// <summary>
		/// условная бесконечность
		/// </summary>
		public const float Infinity = 1E+20f;

		/// <summary>
		/// условный ноль
		/// </summary>
		public const double AboutZero = 1E-18;

		/// <summary>
		/// Число "Пи"
		/// </summary>
		public const double PI = 3.1415926535897932385;

		/// <summary>
		/// Число "2Пи"
		/// </summary>
		public const double twoPI = 3.1415926535897932385 * 2;
	}
	/// <summary>
	/// Алгебраические функции
	/// </summary>
	public class MathFunc
	{
		/// <summary>
		/// Сравнение действительных чисел
		/// </summary>
		/// <returns> 0 - a и b равны </returns>
		/// <returns> 1 - a больше b </returns>
		/// <returns>-1 - a меньше b </returns>
		public static int CompareDouble(double a, double b)
		{
			if (a > b + MathConst.NearlyZero)
				return +1;
			else if (a < b - MathConst.NearlyZero)
				return -1;
			else
				return 0;
		}
		/// <summary>
		/// Нормализовать значение угла, угол будет находиться в пределах (-Pi..+Pi].
		/// </summary>
		public static double normAngle(double angle)
		{
			while (angle > +MathConst.PI)
				angle = angle - MathConst.twoPI;

			while (angle <= -MathConst.PI)
				angle = angle + MathConst.twoPI;

			return angle;
		}

		/// <summary>
		/// Возвращает угол, соответствующий положению точки с координатами (dX,dY) 
		/// относительно точки начала координат в диапазоне (-PI..+PI].
		/// </summary>
		/// <param name="dY"></param>
		/// <param name="dX"></param>
		/// <returns>Угол в радианах, против часовой стрелки, 0 соответствует направлению оси X</returns>
		public static double ArcTan2Ext(double dY, double dX)
		{
			if (dY != 0)
				if (Math.Abs(dX) < MathConst.NearlyZero2)
					if (dY > 0)
						return +MathConst.PI / 2;
					else
						return -MathConst.PI / 2;
				else
					return Math.Atan2(dY, dX);
			else
				if (dX >= 0)
				return 0;
			else
				return +MathConst.PI;
		}

		/// <summary>
		/// Округляет число до указанного порядка.
		/// </summary>
		/// <param name="aValue">Округляемое значение</param>
		/// <param name="aDigit">Количество знаков после запятой</param>
		public static double roundTo(double aValue, int aDigit)
		{
			double lFactor;
			lFactor = Math.Pow(10, aDigit);
			return Math.Round(aValue / lFactor) * lFactor;
		}

		/// <summary>
		/// Используется для поиска и сортировки действительных чисел в списках.
		/// </summary>
		public class FloatComparer : IComparer
		{
			/// <summary>
			/// Сравнивает два действительных числа (float).
			/// </summary>
			public int Compare(object x, object y)
			{
				float first = (float)x;
				float second = (float)y;
				if (first < second)
					return -1;
				else
				if (first > second)
					return +1;
				else
					return 0;
			}
		}

		/// <summary>
		/// Используется для поиска и сортировки действительных чисел в списках.
		/// </summary>
		public static FloatComparer floatComparer = new FloatComparer();
	}

	/// <summary>
	/// Функции обработки множеств точек как простых плоских фигур.
	/// </summary>
	public class Math2D
	{
		/// <summary>
		/// Варианты размещения точки относительно полигона
		/// </summary>
		public enum PointInPolygonState
		{
			/// <summary>
			/// Полигон является вырожденным
			/// </summary>
			ppsNoPolygon,
			/// <summary>
			/// Точка лежит вне полигона
			/// </summary>
			ppsOutside,
			/// <summary>
			/// Точка лежит внутри полигона
			/// </summary>
			ppsInside,
			/// <summary>
			/// Точка лежит на границе полигона
			/// </summary>
			ppsOnEdge
		};

		private const int b0 = 0x001;   // AB никак не пересекается с l
		private const int b1 = 0x002;   // AB пересекается с l
		private const int b2 = 0x004;   // P == A
		private const int b3 = 0x008;   // P == B
		private const int b4 = 0x010;   // P лежит внутри AB, AB имеет ненулевую общую часть с l
		private const int b5 = 0x020;   // B лежит на l, остальной отрезок выше если нет b8
		private const int b6 = 0x040;   // A лежит на l, остальной отрезок выше если нет b8
		private const int b7 = 0x080;   // требуется вычисление пересечения
		private const int b8 = 0x100;   // остальной отрезок ниже

		private const int isOnEdgeMask = b2 + b3 + b4;
		private const int isOnVertexMask = b2 + b3;
		private const int isExtTurnMask = b5 + b6;
		private const int NeedCalcMask = b7;

		/// <summary>
		/// Линии пересекаются.
		/// </summary>
		public const int fCrossing = 0x001;
		/// <summary>
		/// Точка пересечения лежит в пределах этого отрезка.
		/// </summary>
		public const int fOnThisPiece = 0x002;
		/// <summary>
		/// Точка пересечения лежит в пределах данного отрезка (переданного функции в качестве параметра).
		/// </summary>
		public const int fOnParamPiece = 0x004;
		/// <summary>
		/// Точка пересечения лежит в пределах обоих отрезков.
		/// </summary>
		public const int fOnBothPieces = fCrossing | fOnThisPiece | fOnParamPiece;

		private static int[,,,] m = new int[3, 3, 3, 3]
		{
			{
				{
					{1, 1, 1}, {1, 8, 32}, {1, 1, 128}
				},
				{
					{1, 1, 1}, {1, 8, 32}, {1, 16, 2}
				},
				{
					{1, 1, 1}, {1, 8, 32}, {128, 2, 2}
				}
			},
			{
				{
					{1, 1, 1}, {1, 8, 16}, {1, 1, 1}
				},
				{
					{4, 4, 4}, {4, 4, 4}, {4, 4, 4}
				},
				{
					{64, 64, 64}, {80, 72, 96}, {320, 320, 320}
				}
			},
			{
				{
					{1, 1, 128}, {1, 8, 288}, {1, 1, 1}
				},
				{
					{1, 16, 2}, {1, 8, 288}, {1, 1, 1}
				},
				{
					{128, 2, 2}, {1, 8, 288}, {1, 1, 1}
				}
			}
		};

		/// <summary>
		/// Вспомогательная функция, вызывается из функции findPointInPolygon.
		/// </summary>
		protected static void calcLP(ref DLineMath sL, ref DPoint bP, bool rightDir, int pointCount, int iA,
			ref DPointList points, ref DPoint p, ref double r)
		{
			DLineMath pL = new DLineMath(), tL = new DLineMath();
			int jA, jB;
			int cK;
			DPoint pCur;
			float tMin, tCur, dCur, dTmp;

			pL.createPerpendOnBP(ref sL, ref bP, rightDir);

			tMin = MathConst.Infinity;
			for (jA = 0; jA < pointCount; jA++)
			{
				if (jA == iA) continue;

				jB = (jA + 1) % pointCount;

				tL.create(ref points.points[jA], ref points.points[jB]);
				if (tL.pieceLength() < MathConst.NearlyZero) continue;

				cK = pL.crossPiece(ref tL, out tCur);
				if ((Math2D.fCrossing & cK) != 0)
				{
					if (((Math2D.fOnParamPiece & cK) != 0) && (tCur > MathConst.NearlyZero2) && (tCur < tMin))
						tMin = tCur;
				}
				else
				{
					tCur = pL.lineDistanceTo(ref points.points[jA]) / pL.pieceLength();
					if ((tCur < tMin))
						tMin = tCur;
					tCur = pL.lineDistanceTo(ref points.points[jB]) / pL.pieceLength();
					if ((tCur < tMin))
						tMin = tCur;
				}
			}

			if (tMin < MathConst.Infinity)
			{
				tMin = tMin / 2;
				dCur = pL.pieceLength() * tMin;
				pCur = pL.calcXY(tMin);

				for (jA = 0; jA < pointCount; jA++)
				{
					jB = (jA + 1) % pointCount;
					tL.create(ref points.points[jA], ref points.points[jB]);
					if (tL.pieceLength() < MathConst.NearlyZero) continue;

					dTmp = tL.pieceDistanceTo(ref pCur);
					if (dTmp < dCur)
						dCur = dTmp;
				}

				if ((dCur > r) && (isPointInСontour(pCur, points) == PointInPolygonState.ppsInside))
				{
					r = dCur;
					p = pCur;
				}
			}
		}

		/// <summary>
		/// Вычисляет площадь треугольника со знаком.
		/// </summary>
		public static float calcSignedTriangleArea(ref DPoint a, ref DPoint b, ref DPoint c)
		{
			return (float)0.5 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
		}

		/// <summary>
		/// Определяет, является ли заданный контур правосторонным (т.е. обход 
		/// точек этого контура идет в направлении по часовой стрелке).
		/// </summary>
		public static bool isContourRight(ref DPointList poly, int polySize)
		{
			int i, a, b, c;
			DPoint mx;

			b = 0;
			mx = poly.points[b];
			for (i = 1; i < polySize; i++)
				if ((poly.points[i].y > mx.y) || ((poly.points[i].y == mx.y) && (poly.points[i].x > mx.x)))
				{
					mx = poly.points[i];
					b = i;
				}

			c = (b + 1) % polySize;                 // следущая точка
			a = (b - 1 + polySize) % polySize;      // предыдущая точка
			return calcSignedTriangleArea(ref poly.points[a], ref poly.points[b], ref poly.points[c]) < 0;
		}

		/// <summary>
		/// Находит точку внутри полигона (как бы географический центр).
		/// </summary>
		public static void findPointInPolygon(ref DPointList points, out DPoint p, out double r)
		{
			int iA, iB;
			int pointCount = points.points.Length;
			DLineMath sL = new DLineMath();
			bool rightDir;

			if (pointCount < 3)
			{
				if (pointCount < 1)
				{
					r = MathConst.Infinity;
					p = new DPoint(MathConst.Infinity, MathConst.Infinity);
				}
				else
				{
					r = 0;
					p = points.points[0];
				}
				return;
			}

			p = points.points[0];
			rightDir = isContourRight(ref points, pointCount);
			r = -MathConst.Infinity;
			for (iA = 0; iA < pointCount; iA++)
			{
				iB = (iA + 1) % pointCount;
				sL.create(ref points.points[iA], ref points.points[iB]);

				if (sL.pieceLength() < MathConst.NearlyZero) continue;

				calcLP(ref sL, ref points.points[iA], rightDir, pointCount, iA, ref points, ref p, ref r);
				calcLP(ref sL, ref points.points[iB], rightDir, pointCount, iA, ref points, ref p, ref r);
				DPoint tempPoint = sL.calcXY(0.5f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY(0.33f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY(0.67f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
			}
		}

		/// <summary>
		/// Проверка: находится ли точка внутри контура полигона
		/// </summary>
		public static PointInPolygonState isPointInСontour(DPoint p, DPointList poly)
		{
			return isPointInСontour(p, poly.points);
		}

		/// <summary>
		/// Проверка: находится ли точка внутри контура полигона
		/// </summary>
		public static PointInPolygonState isPointInСontour(DPoint p, DPoint[] poly)
		{
			int polySize = poly.Length;
			int v;
			double x;
			DPoint a, b;
			int turnCount, i;
			int a_on_X, b_on_X, a_on_Y, b_on_Y;

			if (polySize < 2)
				return PointInPolygonState.ppsNoPolygon;

			turnCount = 0;
			b = poly[polySize - 1];
			b_on_X = MathFunc.CompareDouble(b.x, p.x) + 1;
			b_on_Y = MathFunc.CompareDouble(b.y, p.y) + 1;
			for (i = 0; i <= polySize - 1; i++)
			{
				a = b;
				a_on_X = b_on_X;
				a_on_Y = b_on_Y;
				b = poly[i];
				b_on_X = MathFunc.CompareDouble(b.x, p.x) + 1;
				b_on_Y = MathFunc.CompareDouble(b.y, p.y) + 1;

				v = m[a_on_Y, a_on_X, b_on_Y, b_on_X];

				if (v != b0)
				{
					if ((v & isOnEdgeMask) != 0)
						return PointInPolygonState.ppsOnEdge;

					if ((v & b1) != 0)
						turnCount++;
					else if ((v & NeedCalcMask) != 0)
					{
						x = (b.x - a.x) * (p.y - a.y) / (b.y - a.y) + a.x;
						if (Math.Abs(x - p.x) < MathConst.NearlyZero)
							return PointInPolygonState.ppsOnEdge;

						if (x > p.x)
							turnCount++;
					}
					else if ((v & b8) != 0)
						turnCount++;
				}
			}

			if ((turnCount & 1) != 0)
				return PointInPolygonState.ppsInside;
			else
				return PointInPolygonState.ppsOutside;
		}

		private static DPoint iPoint(int i, ref DPointList polygon, int nPoints)
		{
			return polygon.points[(i + nPoints) / nPoints];
		}

		/// <summary>
		/// Вычислить периметр полигона/полилинии. Для полигонов и замкнутых полилиний 
		/// </summary>
		public static float calcPerimeter(ref DPointList points, bool closed)
		{
			float s = 0;
			int nPoints = points.points.Length;
			for (int i = nPoints - 2; i >= 0; i--)
				s += points.points[i].distanceTo(points.points[i + 1]);
			if (closed)
				s += points.points[0].distanceTo(points.points[nPoints - 1]);
			return s;
		}

		/// <summary>
		/// Вычислить площадь полигона со знаком, для правого полигона площадь положительна.
		/// </summary>
		public static float calcSignedPolygonArea(ref DPointList polygon, int nPoints)
		{
			double s, s1;
			int i;
			s = 0;
			for (i = 0; i <= nPoints - 1; i++)
			{
				s1 = iPoint(i, ref polygon, nPoints).x * (iPoint(i - 1, ref polygon, nPoints).y
					- iPoint(i + 1, ref polygon, nPoints).y);
				s = s + s1;
			}
			return (float)(s / 2);
		}

		/// <summary>
		/// Вычисляет расстояние от полилинии до точки.
		/// </summary>
		public static float polylineToPointDistance(ref DPointList vPoints, bool closed, ref DPoint sample)
		{
			long pieceCount = vPoints.points.Length - 1;
			if (pieceCount < 0)
				return (float)MathConst.Infinity;
			if (pieceCount == 0)
				return vPoints.points[0].distanceTo(sample);

			DPoint P1;
			DPoint P2 = vPoints.points[0];
			DPoint P0;
			float MinDist = MathConst.Infinity, Dist;
			DLineMath l = new DLineMath();

			long StartPInd = 0;
			for (long i = 1; i <= pieceCount; i++)
			{
				P1 = P2;
				P2 = vPoints.points[i];
				l.create(ref P1, ref P2);
				float t;
				Dist = l.pieceDistanceToExt(ref sample, out t);
				if (Dist < MinDist - MathConst.NearlyZero)
					MinDist = Dist;
			}

			if (closed)
			{
				P1 = P2;
				P0 = vPoints.points[StartPInd];
				l.create(ref P1, ref P0);
				float t;
				Dist = l.pieceDistanceToExt(ref sample, out t);
				if (Dist < MinDist)
					MinDist = Dist;
			}

			return MinDist;
		}
	}

	/// <summary>
	/// Геометрическая точка с действительными координатами.
	/// </summary>
	public struct DPoint
	{
		/// <summary>
		/// Координата X точки.
		/// </summary>
		public float x;
		/// <summary>
		/// Координата Y точки.
		/// </summary>
		public float y;
		/// <summary>
		/// Инициализирует точку с заданными координатами.
		/// </summary>
		public DPoint(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// Вычисляет расстояние между этой точкой и заданной
		/// </summary>
		/// <returns>Возвращает действительное расстояние до точки point</returns>
		public float distanceTo(DPoint point)
		{
			float dx = x - point.x;
			float dy = y - point.y;
			return (float)Math.Sqrt(dx * dx + dy * dy);
		}

		/// <summary>
		/// Устанавливает обе координаты точки в заданные значения
		/// </summary>
		public void setXY(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// Возвращается координаты точки
		/// </summary>
		public void getXY(ref float x, ref float y)
		{
			x = this.x;
			y = this.y;
		}

		/// <summary>
		/// Читает себя из потока.
		/// </summary>
		public void read(Serializer s)
		{
			x = s._float();
			y = s._float();
		}

		/// <summary>
		/// Пишет себя в поток.
		/// </summary>
		public void write(Serializer s)
		{
			s._float(x);
			s._float(y);
		}
	}

	/// <summary>
	/// Отрезок прямой
	/// </summary>
	public struct DLine
	{
		/// <summary>
		/// Первая точка, через которую проходит отрезок.
		/// </summary>
		public DPoint a;
		/// <summary>
		/// Вторая точка, через которую проходит отрезок.
		/// </summary>
		public DPoint b;

		/// <summary>
		/// Вычисляет длину отрезка
		/// </summary>
		public double length()
		{
			return a.distanceTo(b);
		}
		/// <summary> Вычисляет координаты середины отрезка и возвращает их в качестве результата </summary>
		public DPoint getCenter()
		{
			return new DPoint((a.x + b.x) / 2, (a.y + b.y) / 2);
		}
		/// <summary> Читает себя из потока </summary>
		public void read(Serializer s)
		{
			a.read(s);
			b.read(s);
		}
		/// <summary> Пишет себя в поток </summary>
		public void write(Serializer s)
		{
			a.write(s);
			b.write(s);
		}
	}
	/// <summary> Прямоугольная оболочка геометрического объекта </summary>
	public struct DExtents
	{
		/// <summary> Минимальные координаты оболочки </summary>
		public DPoint min;
		/// <summary> Максимальные координаты оболочки </summary>
		public DPoint max;

		/// <summary> Режим инициализации оболочки </summary>
		public enum InitType
		{
			/// <summary> Пустая оболочка </summary>
			empty,
			/// <summary> Включающая всё </summary>
			max
		};
		/// <summary> Инициализирует оболочка значениями в соответствии с заданным режимом инициализации </summary>
		public DExtents(InitType initType)
		{
			min.x = 0;
			min.y = 0;
			max.x = 0;
			max.y = 0;
			switch (initType)
			{
				case InitType.empty:
					setEmpty();
					break;
				case InitType.max:
					setMax();
					break;
			}
		}
		/// <summary> вертикальный и горизонтальный центр оболочки </summary>
		public DPoint centerPoint
		{
			get
			{
				return new DPoint((min.x + max.x) / 2, (min.y + max.y) / 2);
			}
		}
		/// <summary> ширина оболочки </summary>
		public double dX
		{
			get
			{
				return max.x - min.x;
			}
		}
		/// <summary> высота оболочки </summary>
		public double dY
		{
			get
			{
				return max.y - min.y;
			}
		}
		/// <summary> Записывает вырожденную оболочку </summary>
		public void setEmpty()
		{
			min.x = (float)+MathConst.Infinity;
			min.y = (float)+MathConst.Infinity;
			max.x = (float)-MathConst.Infinity;
			max.y = (float)-MathConst.Infinity;
		}
		/// <summary> Записывает максимальную оболочку </summary>
		public void setMax()
		{
			min.x = (float)-MathConst.Infinity;
			min.y = (float)-MathConst.Infinity;
			max.x = (float)+MathConst.Infinity;
			max.y = (float)+MathConst.Infinity;
		}
		/// <summary> Читает себя из потока </summary>
		public void read(Serializer s)
		{
			min.read(s);
			max.read(s);
		}
		/// <summary> Пишет себя в поток </summary>
		public void write(Serializer s)
		{
			min.write(s);
			max.write(s);
		}
		/// <summary> Проверяет, пересекается ли (или касается ли) эта оболочка с заданной </summary>
		public bool isIntersects(ref DExtents extents)
		{
			return (extents.max.x >= min.x) && (extents.min.x <= max.x) && (extents.max.y >= min.y) && (extents.min.y <= max.y);
		}
		/// <summary> Расширяет оболочку так, чтобы заданная точка point оказалась включена в эту оболочку </summary>
		public void includePoint(ref DPoint point)
		{
			if (point.x < min.x)
				min.x = point.x;

			if (point.x > max.x)
				max.x = point.x;

			if (point.y < min.y)
				min.y = point.y;

			if (point.y > max.y)
				max.y = point.y;
		}
		/// <summary> Расширяет оболочку так, чтобы заданная оболочка extents оказалась включена в эту оболочку </summary>
		public void includeExtents(ref DExtents extents)
		{
			if (extents.min.x < min.x)
				min.x = extents.min.x;

			if (extents.max.x > max.x)
				max.x = extents.max.x;

			if (extents.min.y < min.y)
				min.y = extents.min.y;

			if (extents.max.y > max.y)
				max.y = extents.max.y;
		}
		/// <summary> Включить в оболочку заданный отрезок </summary>
		public void includeLine(ref DLine line)
		{
			includePoint(ref line.a);
			includePoint(ref line.b);
		}
		/// <summary> Включить в оболочку все точки заданного списка точек </summary>
		public void includePointList(ref DPointList pointList)
		{
			for (int i = 0; i < pointList.points.Length; i++)
				includePoint(ref pointList.points[i]);
		}
	}
	/// <summary> Массив точек с координатами </summary>
	public struct DPointList
	{
		/// <summary> Массив координат точек </summary>
		public DPoint[] points;

		/// <summary> Задать кол-во точек </summary>
		/// <param name="size">Новое кол-во точек</param>
		public void setSize(int size)
		{
			points = new DPoint[size];
		}
		/// <summary> Читает себя из потока </summary>
		public void read(Serializer s)
		{
			setSize(s._int());
			for (int i = 0; i < points.Length; i++)
				points[i].read(s);
		}
		/// <summary> Пишет себя в поток </summary>
		public void write(Serializer s)
		{
			s._int(points.Length);
			for (int i = 0; i < points.Length; i++)
				points[i].write(s);
		}
		public DPoint[] removeLast(int count)
		{
			DPoint[] points = new DPoint[this.points.Length - count];
			for (int i = 0; i < this.points.Length - count; i++)
				points[i] = this.points[i];
			return points;

		}
		/// <summary> Вычисляет координаты точки, заданной параметром t на этой полилинии </summary>
		/// <param name="t">целая часть определяет номер отрезка в полилинии, дробная - точную позицию внутри отрезка; должно быть в пределах [0..points.Length]</param>
		/// <returns>Возвращает координаты точки</returns>
		public DPoint calcPointByT(float t)
		{
			int aIndex = (int)Math.Floor(t);
			DLineMath l = new DLineMath();
			l.create(ref points[aIndex], ref points[aIndex + 1]);
			return l.calcXY(t - aIndex);
		}
		/// <summary>
		/// Вычисляет координаты точки и её параметр (t), находящейся на расстоянии dist вдоль периметра 
		/// этой полилинии, начиная от точки, расположенной на этой же полилинии, заданной параметром fromT.
		/// </summary>
		/// <param name="fromT">параметр исходной точки, от которой откладывается заданное расстояние (dist)</param>
		/// <param name="dist">Расстояние вдоль периметра</param>
		/// <param name="t">Возвращает параметр найденной точки</param>
		/// <returns>Возвращает true, если искомая точка находится в рамках полилинии</returns>
		public bool calcPointTAtDistanceFrom(float fromT, float dist, out float t)
		{
			int fromP1Index = (int)Math.Floor(fromT);
			DLineMath l = new DLineMath();
			l.create(ref points[fromP1Index], ref points[fromP1Index + 1]);
			DPoint fromP = l.calcXY(fromT - (float)fromP1Index);
			dist += points[fromP1Index].distanceTo(fromP);
			do
			{
				l.create(ref points[fromP1Index], ref points[fromP1Index + 1]);
				float pl = l.pieceLength();
				if (pl < dist)
					dist -= pl;
				else
				{
					DPoint p;
					t = fromP1Index + l.calcPointAtDistFromA(dist, out p);
					return true;
				}
				fromP1Index++;
			}
			while (fromP1Index < points.Length - 1);
			t = fromP1Index;
			return false;
		}
		/// <summary>
		/// Вычислить расстояние вдоль полилинии (представленной этим списком точек) 
		/// между точками на ней, заданными параметрически.
		/// </summary>
		/// <param name="startT">Первая точка на полилинии</param>
		/// <param name="endT">Вторая точка на полилинии</param>
		/// <returns>Вычисленное расстояние</returns>
		public float calcDistanceBetweenTs(float startT, float endT)
		{
			Debug.Assert(startT <= endT);

			int s = (int)Math.Ceiling(startT);
			int e = (int)Math.Floor(endT);

			if (e < s)
				return calcPointByT(startT).distanceTo(calcPointByT(endT));

			float dist = 0;
			for (int i = s; i < e; i++)
				dist += points[i].distanceTo(points[i + 1]);

			if (startT != (float)s)
				dist += calcPointByT(startT).distanceTo(points[s]);

			if (endT != (float)e)
				dist += calcPointByT(startT).distanceTo(points[s]);

			return dist;
		}
		/// <summary>
		/// Найти самый длинный отрезок в пределах части полилинии, заданной параметрически 
		/// границами startT и endT. И вернуть центр найденного отрезка в параметрическом представлении.
		/// </summary>
		public float findLargestPieceCenter(float startT, float endT)
		{
			int s = (int)Math.Ceiling(startT);
			int e = (int)Math.Floor(endT);

			if (e < s)
				return (startT + endT) / 2;

			float foundT = -1;
			float foundDist = -1;
			for (int i = s; i < e; i++)
			{
				float d = points[i].distanceTo(points[i + 1]);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = 0.5f + i;
				}
			}

			if (startT != (float)s)
			{
				DPoint p = calcPointByT(startT);
				float d = points[s].distanceTo(p);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = (s + startT) / 2;
				}
			}

			if (endT != (float)e)
			{
				DPoint p = calcPointByT(endT);
				float d = points[e].distanceTo(p);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = (e + endT) / 2;
				}
			}
			return foundT;
		}
		/// <summary> Вычисляет периметр полилинии </summary>
		/// <param name="closed">true - для указания замкнутости полилинии</param>
		/// <returns>Вычисленное значение периметра</returns>
		public float calcPerimeter(bool closed)
		{
			return Math2D.calcPerimeter(ref this, closed);
		}
		/// <summary>
		/// Найти все пересечения этой полилинии с заданной полилинией и вернуть список
		/// точек пересечения в параметрическом представлении.
		/// </summary>
		/// <param name="pp">Вторая полилиния</param>
		/// <param name="crossTs">Вычисленные точки пересечения</param>
		public void calcAllCrossesWith(ref DPointList pp, ArrayList crossTs)
		{
			DLineMath d = new DLineMath();
			DLineMath s = new DLineMath();
			for (int i = points.Length - 2; i >= 0; i--)
			{
				s.create(ref points[i], ref points[i + 1]);
				for (int j = pp.points.Length - 2; j >= 0; j--)
				{
					d.create(ref pp.points[j], ref pp.points[j + 1]);
					float t;
					if (s.crossPiece(ref d, out t) == Math2D.fOnBothPieces)
						crossTs.Add(t);
				}
			}
		}
	}
}