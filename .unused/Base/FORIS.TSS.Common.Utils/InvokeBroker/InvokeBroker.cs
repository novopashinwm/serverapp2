﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// NOTE: Желательно отказаться от вызовов метода Invoke формы.
	/// Поскольку вызовы методов формы происходят в основном потоке формы, можно
	/// отказаться от вызовов метода Invoke.
	/// Для этого нужно удалить dlgtInvoke, Invoke и OnInvoke , а в Application_Idle
	/// вызывать метод напрямую через handler.
	/// </remarks>
	public class InvokeBroker : Component, IInvokeBroker
	{
		private readonly Queue<InvokeEventArgs> argumentsQueue = new Queue<InvokeEventArgs>();

		#region Implement IInvokeBroker

		/// <summary>
		/// Этот метод ставит задачу в очередь на исполнение и сразу возвращает управление
		/// (возвращаемое значение всегда null)
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="args"></param>
		/// <returns>null</returns>
		object IInvokeBroker.Invoke(Delegate handler, params object[] args)
		{
			lock (argumentsQueue)
			{
				this.argumentsQueue.Enqueue(new InvokeEventArgs(handler, args));
				return null;
			}
		}

		#endregion // Implement IInvokeBroker

		public InvokeBroker(IContainer container)
		{
			Application.Idle += new EventHandler(this.Application_Idle);
			container.Add(this);
		}

		protected override void Dispose(bool disposing)
		{
			lock (argumentsQueue)
			{
				if (disposing)
				{
					Application.Idle -= new EventHandler(this.Application_Idle);
					this.dlgtInvoke = null;
				}
			}

			base.Dispose(disposing);
		}

		private InvokeEventHandler dlgtInvoke;

		public event InvokeEventHandler Invoke
		{
			add
			{
				this.dlgtInvoke += value;
			}

			remove
			{
				this.dlgtInvoke -= value;
			}
		}

		protected virtual object OnInvoke(InvokeEventArgs e)
		{
			if (this.dlgtInvoke != null)
			{
				return this.dlgtInvoke(e);
			}

			return null;
		}

		private void Application_Idle(object sender, System.EventArgs e)
		{
			InvokeEventArgs args = null;

			lock (this.argumentsQueue)
			{
				if (this.argumentsQueue.Count > 0)
				{
					args = this.argumentsQueue.Dequeue();
				}
			}

			if (args != null)
			{
				this.OnInvoke(args);
			}
		}
	}

	public delegate object InvokeEventHandler(InvokeEventArgs e);

	public class InvokeEventArgs : EventArgs
	{
		private readonly Delegate handler;
		public Delegate Handler { get { return this.handler; } }

		private readonly object[] args;
		public object[] Args { get { return this.args; } }

		public InvokeEventArgs(Delegate handler, params object[] args)
		{
			this.handler = handler;
			this.args = args;
		}
	}
}