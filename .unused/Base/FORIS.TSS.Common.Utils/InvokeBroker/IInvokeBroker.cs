﻿using System;

namespace FORIS.TSS.Common
{
	public interface IInvokeBroker
	{
		object Invoke(Delegate handler, params object[] args);
	}
}