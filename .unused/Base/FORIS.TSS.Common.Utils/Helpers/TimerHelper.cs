using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FORIS.TSS.Helpers
{
    public static class TimerHelper
    {
        public static void StopTimer( Timer timer )
        {
            if (timer != null)
            {
                ManualResetEvent mre = new ManualResetEvent(false);
                timer.Dispose(mre);
                mre.WaitOne();
            }
        }
    }
}
