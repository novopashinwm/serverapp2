using System;

namespace FORIS.TSS.Helpers
{
	public delegate void IntegerEventHandler( object sender, IntegerEventArgs e );

	public class IntegerEventArgs: EventArgs
	{
		#region Properties

		private readonly int m_Value;
		public int Value { get { return m_Value; } }


		#endregion Properties

		#region Constructor

		public IntegerEventArgs( int Value )
		{
			m_Value = Value;
		}

		#endregion Constructor
	}
}
