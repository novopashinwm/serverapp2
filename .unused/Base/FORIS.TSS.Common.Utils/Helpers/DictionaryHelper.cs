﻿using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.Helpers
{
	public static class DictionaryHelper
	{
		public static IEnumerable<KeyValuePair<string, object>> Flatten(this IDictionary<string, object> dic, string key = null)
		{
			return dic
				.SelectMany(kv =>
					kv.Value is IDictionary<string, object>
						? ((IDictionary<string, object>)kv.Value).Flatten(string.IsNullOrWhiteSpace(key) ? kv.Key : string.Join(".", key, kv.Key))
						: new List<KeyValuePair<string, object>>()
						{
						new KeyValuePair<string, object>(string.IsNullOrWhiteSpace(key)
							? kv.Key
							: string.Join(".", key, kv.Key), kv.Value)
						});
		}
		public static Dictionary<TValue, List<TKey>> Transponate<TKey, TValue>(this Dictionary<TKey, List<TValue>> x)
		{
			var result = new Dictionary<TValue, List<TKey>>();
			foreach (var pair in x)
			{
				var key = pair.Key;
				var values = pair.Value;
				foreach (var value in values)
				{
					List<TKey> list;
					if (!result.TryGetValue(value, out list))
						result.Add(value, list = new List<TKey>());
					list.Add(key);
				}
			}
			return result;
		}

		public static List<TValue> GetUnicalValues<TKey, TValue>(this Dictionary<TKey, List<TValue>> x)
		{
			var result = new Dictionary<TValue, bool>();
			foreach (var pair in x)
			{
				foreach (var value in pair.Value)
					result[value] = true;
			}
			return result.Keys.ToList();
		}

		public static TValue? GetValueOrNull<TKey, TValue>(this Dictionary<TKey, TValue> x, TKey key) where TValue : struct
		{
			TValue value;
			if (x.TryGetValue(key, out value))
				return value;
			return null;
		}

		public static TValue GetObjectOrNull<TKey, TValue>(this Dictionary<TKey, TValue> x, TKey key) where TValue : class
		{
			TValue value;
			if (x.TryGetValue(key, out value))
				return value;
			return null;
		}
	}
}