﻿using FORIS.TSS.Config;

namespace FORIS.TSS.BusinessLogic
{
	public class GlobalsBase : Globals
	{
		public  static bool tracingInUpdate              = false;
		private static bool isTracingInUpdateInitialized = false;
		public  static bool TraceDataSets
		{
			get
			{
				// double checking to optimize performance
				if (isTracingInUpdateInitialized == false)
				{
					lock (GlobalsLock)
					{
						if (isTracingInUpdateInitialized == false)
						{
							isTracingInUpdateInitialized = true;
							string cfg = AppSettings["TraceDataSets"];
							if (cfg != null)
							{
								tracingInUpdate = cfg.ToUpper() == "TRUE";
							}
						}
					}
				}
				return tracingInUpdate;
			}
		}
	}
}