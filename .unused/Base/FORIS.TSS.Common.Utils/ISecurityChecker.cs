﻿using System;

namespace FORIS.TSS.Common
{
	/// <summary> Summary description for ISecurityChecker </summary>
	public interface ISecurityChecker
	{
		bool Check(string key /*, IRightData rightData */);
	}
}