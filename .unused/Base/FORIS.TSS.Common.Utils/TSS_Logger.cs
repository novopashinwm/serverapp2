﻿using System.Diagnostics;

namespace FORIS.TSS.IO
{
	/// <summary> Приоритет сообщения </summary>
	public enum LogPriority
	{
		Unimportant = 0,
		VeryLow,
		Low,
		Normal,
		High,
		VeryHigh,
		Urgent
	}
	/// <summary> Категория сообщения в журнале сообщений </summary>
	public enum LogCategory : short
	{
		General = 0,
		Terminal,
		DataBase,
		Remoting,
		InputOutput, 
		Security,
		Unit
	}
	/// <summary> Тип сообщения </summary>
	public enum LogType
	{
		Information = 0,
		Warning,
		Error,
	}

	/// <summary> Журнал сообщений </summary>
	public class Logger
	{
		/// <summary> Запись в журнал сообщений </summary>
		/// <param name="message">сообщение</param>
		/// <param name="type">тип сообщения</param>
		/// <param name="category">категория сообщения</param>
		/// <param name="priority">приоритет</param>
		/// <param name="eventID">идентификатор сообщения</param>
		public static void WriteEntry(string message, LogType type, LogCategory category, LogPriority priority, int eventID)
		{
			try
			{
				// TODO: реализацию объекта лога
				switch(type)
				{
					case LogType.Error:
						Trace.TraceError(message);
						break;
					case LogType.Information:
						Trace.TraceInformation(message);
						break;
					case LogType.Warning:
						Trace.TraceWarning(message);
						break;
					default:
						Trace.WriteLine(message);
						break;
				}

				
			}
			catch
			{
			}
		}
		/// <summary> Запись в журнал сообщений </summary>
		/// <param name="message">сообщение</param>
		/// <param name="type">тип сообщения</param>
		/// <param name="category">категория сообщения</param>
		/// <param name="priority">приоритет</param>
		/// <param name="eventID">идентификатор сообщения</param>
		public static void WriteDebugEntry(string message, LogType type, LogCategory category, LogPriority priority, int eventID)
		{
#if DEBUG
			WriteEntry(message, type, category, priority, eventID);
#endif
		}
	}
}