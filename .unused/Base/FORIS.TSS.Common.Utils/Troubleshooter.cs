using System;
using System.Text;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// Summary description for Troubleshooter.
	/// </summary>
	public class Troubleshooter
	{
		/// <remarks>
		/// http://www.codeproject.com/csharp/testingsocketservers.asp
		/// </remarks>
		/// <param name="url"></param>
		/// <param name="report"></param>
		/// <returns></returns>
		public static bool ConnectToPortForRemotingObject(string url, StringBuilder report)
		{
			TcpClient client = new TcpClient();

			// Contruct URI object
			Uri uri = new Uri(url);
			string hostName = uri.Host;
			int port = uri.Port;

			// Resolve hostname into IP through DNS
			Trace.WriteLine("Resolving " + hostName);
			IPHostEntry entry = Dns.GetHostEntry( uri.Host );

			// Try to connect to each IP address
			foreach (IPAddress address in entry.AddressList)
			{
				EndPoint endPoint = new IPEndPoint(address, port);
				Trace.WriteLine("Trying to connect to " + address.ToString() + " : " + port.ToString());

				using (Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
				{
					// socket.Bind(); bind to local endpoint
					socket.Connect(endPoint);
					if (socket.Connected)
					{
						Trace.WriteLine("Connection was established");
						return true;
					}
				}
			}
			return false;
		}
	}
}
