using System;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// Summary description for Handshake.
	/// </summary>
	public class Handshake
	{
		/*
The Basic process of detecting whether a port is open 
or not has been described below:
1.)      You send a TCP Packet containing the SYN flag 
(which in turn contains the Port Number to remote host.

2.)      Now the remote host checks whether the port 
is open or not. If the port is open then it replies 
with a TCP packet containing both an ACK message 
confirming that the port is open and a SYN flag. 
On the other hand if the port is closed then the remote 
host sends the RST flag which resets the connection, 
in short closes the connection.

3.)      This third phase is optional and involves the 
sending of an ACK message by the client.
		*/
	}
}
