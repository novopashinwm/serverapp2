﻿using System;

namespace FORIS.TSS.Common.Caching
{
	/// <summary> Кэш для данных, привязанных к географическим координатам </summary>
	/// <typeparam name="TValue">Тип данных, которые можно получить по координате</typeparam>
	public sealed class GeoPointCache<TValue>
	{
		private readonly int _cellSizeMeters;

		public GeoPointCache(Func<double, double, TValue> getValue, int capacity, int cellSizeMeters = 50)
		{
			_cellSizeMeters = cellSizeMeters;
			_valueByPoint = new TimingCache<GeoPointStruct, TValue>(
				TimeSpan.MaxValue,
				point => getValue(point.Lat, point.Lng),
				capacity
				);
		}
		public TValue this[double lat, double lng]
		{
			get
			{
				var hash = new GeoPointStruct(lat, lng, null, _cellSizeMeters);
				return _valueByPoint[hash];
			}
		}
		private readonly TimingCache<GeoPointStruct, TValue> _valueByPoint;
	}
}