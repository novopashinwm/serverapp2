﻿
using System;
using FORIS.TSS.BusinessLogic.TspTerminal;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.Base
{
    public interface ILogic
    {
        /// <summary>
        /// Запрос на чтение истории движения из контроллера или из БД
        /// </summary>
        /// <param name="method">Метод получения истории, из БД или из контроллера,
        /// по каналу связи</param>
        /// <param name="beginTime">Начальный интервал</param>
        /// <param name="endTime">Конечный интервал</param>
        /// <param name="controllerId">ID контроллера</param>
        /// <param name="interval">интерал между позициями</param>
        /// <param name="maxCount">максимальное количество позиций</param>
        void SendGetLogCommand(GetLogMethod method, DateTime beginTime, DateTime endTime,
                                      int controllerId,
                                      int interval, int maxCount);

        event AnonymousEventHandler<LogReceivedEventArgs> LogReceived;

        /// <summary>
        /// Возвращает историю движения из БД, полученную вызовом SendGetLogCommand 
        /// </summary>
        /// <param name="controllerID">ID контроллера</param>
        /// <returns>Датасет с историей движения, null в случае неудачи,
        /// например, не было вызова SendGetLogCommand для данного контроллера</returns>
        HistoryDataSet GetLog(int controllerID);
    }

    [Serializable]
    public class LogReceivedEventArgs : EventArgs
    {
    }
}
