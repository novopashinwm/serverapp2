
namespace FORIS.TSS.Common.Helpers
{
    public enum VisualStyle
    {
        IDE,
        Plain
    }

    public enum Direction
    {
        Vertical,
        Horizontal
    }
    
    public enum Edge
    {
        Top,
        Left,
        Bottom,
        Right,
        None
    }
}

