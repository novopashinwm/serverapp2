﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Terminal.Data;

namespace FORIS.TSS.Report.Base
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportDataSupplier
    {
        #region DataBase
        
        /// <summary>
        /// Получить данные из хранимой процедуры,
        /// возвращающей несколько наборов записей
        /// </summary>
        /// <param name="params">параметры хранимой процедуры</param>
        /// <param name="procedureName">имя хранимой процедуры</param>
        /// <param name="strTablesNames">
        /// массив имен таблиц в том порядке в котором 
        /// они возвращаются хранимой процедурой
        /// </param>
        /// <returns></returns>
        DataSet GetDataFromDB(ParamValue[] @params, string procedureName, string[] strTablesNames);

        string GetConstant(string name);

        #endregion DataBase

 
        #region Geo

        string GetNearestAddress(double latitude, double longitude);

        DataSet GetAddressByPoint(DataSet dsMoveDetailHistory);

        DataSet GetMoveHistoryAddresses(Guid mapGuid, HistoryDataSet dsMoveHistory);

        #endregion Geo


        #region Terminal

        DataSet GetLogFromDB(DateTime time, DateTime endTime, int id, int interval, int count);

        #endregion Terminal

    }
}