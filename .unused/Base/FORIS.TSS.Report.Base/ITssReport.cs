using CrystalDecisions.CrystalReports.Engine;

using FORIS.TSS.Interfaces.ReportService;

namespace FORIS.TSS.Report.Base
{
	/// <summary>
	/// ��������� ��� ���������� ������ ��� ������� ������
	/// </summary>
	public interface ITssReport 
	{
		/// <summary>
		/// ��������� �������� ������.
		/// </summary>
		string ReportNameGet();

		/// <summary>
		///	���������� ������� ������ �� ��������� ������ ����������� ��������, ������� ���������� ���������� �� � ����� � ������� � ���.
		///	������ �� ��������� ������ ����������� ������������ � �������, ���������� ��� ��������� ������ �������� ���������.
		/// </summary>
        /// <param name="iDataSupplier">������ �� ������, ���������� ����������</param>
        void IBasicFunctionSetInstanceSet(IReportDataSupplier iDataSupplier);

		/// <summary>
		/// ������� ���������� ��������� ������, ���������� ����� ���������� ��� ������� ������.
		/// </summary>
		/// <returns></returns>
		ReportParameters ReportParametersInstanceGet();

		/// <summary>
		/// ������� ���������� ��������� ������, ���������� ����� ����������������� ���������� ��� ������� ������.
		/// ��������, ���� ���� �������� ������ ������� �� �������, ����������� �������������, �� ���� ����� ������������� ����������� ��������� ��������
		/// � ��������� �������� �� ������ ������, ��������� ������������� � ������ ��������.
		/// </summary>
		/// <param name="iReportParameters">�������� ����������� ����� ���������� ������</param>
		/// <returns>����������������� ����� ����������</returns>
		ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters);

		/// <summary>
		/// ������� ��������� ���������� ����� ���������� �� ������������ ����������.
		/// ����� ��������� ������ �� ���������� ���� ����� ���� ��������� �� ������������ ����������.
		/// ���� ��������� ���������� �����������, �� ������ "������" ������� ����� ��������� �����������.
		/// </summary>
		/// <param name="iReportParameters">����������� ����� ���������� ������</param>
		/// <returns>� ������ ������������ ���������� ���������� true, � ������ ������������ ��� �������� ���������� ���������� false</returns>
		bool ReportParametersInstanceCheck(ReportParameters iReportParameters);

		/// <summary>
		/// �������� ��������� ������������ �� �������� ������� ������ � ������ �������.
		/// </summary>
		/// <returns></returns>
		bool VisibleInReportsList
		{
			get;
		}

		/// <summary>
        /// ������� �����, ��������� ���������� ��������� � ��������� ������
        /// </summary>
		/// <param name="iReportParameters">������ �� ��������� ������ �� ����������</param>
		/// <returns></returns>
		ReportClass Create(ReportParameters iReportParameters);

        /// <summary>
        /// ������ ���������� ������
        /// </summary>
        ReportParamsDictionary ParamsDictionary { get; }
	}
}