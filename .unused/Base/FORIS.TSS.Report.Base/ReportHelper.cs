using System;
using System.Globalization;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace FORIS.TSS.Report.Base
{
	public class ReportHelper
	{
		/// <summary>
		/// Export report to file
		/// </summary>
		/// <param name="crReportDocument">ReportDocument</param>
		/// <param name="expType">Export Type (pdf, xls, doc, rpt, htm)</param>
		/// <param name="exportPath">Export Path (physical path 
		/// on the disk were exported document will be stored on)</param>
		/// <param name="filename">File name (file name without 
		/// extension f.e. "MyReport1")</param>
		/// <returns>returns true if export was succesfull</returns>
		public static string ExportReport(ReportDocument crReportDocument,
			string expType,string exportPath, string filename)
		{
			//creating full report file name 
			//for example if the filename was "MyReport1"
			//and ExpType was "pdf", full file name will be "MyReport1.pdf"
			filename = filename + "." + expType;

			//creating storage directory if not exists
			if (!Directory.Exists(exportPath)) 
				Directory.CreateDirectory(exportPath);
      
			//creating new instance representing disk file destination 
			//options such as filename, export type etc.
			var crDiskFileDestinationOptions = 
				new DiskFileDestinationOptions();
			ExportOptions crExportOptions = crReportDocument.ExportOptions;


			switch(expType)
			{
				case "rtf": 
				{
					//setting disk file name 
					crDiskFileDestinationOptions.DiskFileName = 
						exportPath + filename;
					//setting destination type in our case disk file
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					//setuing export format type
					crExportOptions.ExportFormatType = ExportFormatType.RichText;
					//setting previously defined destination 
					//opions to our input report document
					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					break;
				}
					//NOTE following code is similar to previous, so I want comment it again
				case "pdf":
				{
					crDiskFileDestinationOptions.DiskFileName =
						exportPath + filename;
					crExportOptions.DestinationOptions = 
						crDiskFileDestinationOptions;
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = 
						ExportFormatType.PortableDocFormat;
					break;
				}
				case "doc":
				{
					crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					break;
				}
				case "xls":
				{
					crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.Excel;
					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					break;
				}
				case "rpt":
				{
					crDiskFileDestinationOptions.DiskFileName = exportPath + filename;
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.CrystalReport;
					crExportOptions.DestinationOptions = crDiskFileDestinationOptions; 
					break;
				}
				case "htm":
				{
					var html40Formatopts = new HTMLFormatOptions();
					crExportOptions.ExportDestinationType = 
						ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.HTML40;
					html40Formatopts.HTMLBaseFolderName = exportPath + filename; 
					html40Formatopts.HTMLFileName = "HTML40.html";
					html40Formatopts.HTMLEnableSeparatedPages = true;
					html40Formatopts.HTMLHasPageNavigator = true;
					html40Formatopts.FirstPageNumber = 1;
					html40Formatopts.LastPageNumber = 3;
					crExportOptions.FormatOptions = html40Formatopts;
					break;
				}

			}
			//trying to export input report document, 
			//and if success returns true
			crReportDocument.Export();
			return crDiskFileDestinationOptions.DiskFileName;
		}

		/// <summary>
		/// Export report to byte array
		/// </summary>
		/// <param name="crReportDocument">
		/// ReportDocument</param>
		/// <param name="exptype">
		/// CrystalDecisions.Shared.ExportFormatType</param>
		/// <returns>byte array representing current report</returns>
		public static byte[] ExportReportToStream(ReportDocument 
			crReportDocument,ExportFormatType exptype)
		{//this code exports input report document into stream, 
			//and returns array of bytes

		    Stream st = crReportDocument.ExportToStream(exptype);

			var arr = new byte[st.Length];
			st.Read(arr,0,(int) st.Length);

			return arr;

		}

        /// <summary>
        /// ���������� �������� ����� � ����������� � ��������� �������
        /// </summary>
        /// <param name="reportsBase"></param>
        /// <param name="operatorID"></param>
        /// <returns></returns>
        public static string GetReportXMLFileName(string reportsBase, int operatorID)
        {
            if (String.IsNullOrEmpty(reportsBase))
                reportsBase = ".";

            return String.Format(@"{0}\{1}\reports.xml", reportsBase, operatorID.ToString(CultureInfo.InvariantCulture));
        }


	}
}
