﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Config;
using FORIS.TSS.Interfaces.ReportService;
using System.Collections.Generic;


namespace FORIS.TSS.Report.Base
{

    #region // Список GUID-ов отчетов

    //AnalyticsTssReport.cs                 "256B9CD5-C7E8-4821-BF80-CB639B440EB0"
    //CheckpointIntervalsTssReport.cs       "F02DAF74-E1D6-4cb9-996F-F07D057C083E"
    //GPRSAccessibilityTssReport.cs         "E551E65A-FF61-4612-8E07-68042FA9893E"
    //GPRSInaccessibilityTSSReport.cs       "32B443E2-0D60-41f6-973B-84E968DA2243"
    //MissedTripsDetailTssReport.cs         "7C10794F-0951-46e3-972B-A51A1A0827F0"
    //MissedTripsResultsTssReport.cs        "51059295-9D4D-4b13-AAA8-DF62906DFBB7"
    //MissedTripsTssReport.cs               "1A5EAF1D-CA46-4480-8C55-E1803A611FD1"
    //MoveDistanceTssReport.cs              "D403FAC9-65A1-4747-B18A-A2A53062E220"
    //MovementIntervalsTssReport.cs         "1EEDF92B-3C9D-4686-8B69-7ED32CC053B0"
    //MovementRegularityTssReport.cs        "58CEC1AD-F9BB-4a30-9CB6-D44A6BC245D0"
    //MoveTssReport.cs                      "E87D142D-50F1-410c-B125-F5D38760AE35"
    //RosterTssReport.cs                    "11EF2A02-C407-49a0-80C6-962AE5733371"
    //SeparateWaybillScheduleTssReport.cs   "6054C10E-4780-43e5-A0E9-206A13FA3231"
    //VehiclesByRoutesTssReport.cs          "F5D4F9BD-14D8-4fa2-ACBE-979E38DBF440"
    //VehiclesOutsTssReport.cs              "DF504E68-27D0-4259-824B-B21888527BAD"
    //WaybillHistoryTssReport               "46039364-E424-4393-BEFE-8F677F27F55B"
    //WaybillNewTssReport.cs                "73157D33-19AC-4d05-A9E4-10E48F32E4AB"
    //WaybillsTransferTssReport.cs          "6D4AE7A2-ECAE-4ed9-889A-C8DFF0120826"

    #endregion // Список GUID-ов отчетов

    #region class ReportsFactory

    /// <summary>
    /// Класс "Фабрика отчетов"  
    /// </summary>
    public class ReportsFactory
    {
        private IDictionary<int, object> lockReportsXML = new System.Collections.Generic.Dictionary<int, object>();

        /// <summary>
        /// Диреткория для хранения отчётов
        /// </summary>
        DirectoryInfo di;

        /// <summary>
        /// Url web-ГИС
        /// </summary>
        private string urlWebGis = @"http://localhost/WebMap";

        private string reportsBase = String.Empty;
        // Список GUID-ов доступных отчетов запрашивается из базы
        Guid[] GuidsReports;

        // Ссылка на объект, содержащий интерфейсы для клиента или сервера
        IReportDataSupplier iDataSupplier;

        // Предварительный список созданных отчетов для сортировки
        public ArrayList alReports;

        // Список созданных отчетов
        TssReportBase[] maclsReportList;
        public TssReportBase[] ReportsList
        {
            get
            {
                return maclsReportList;
            }
        }

        //Счетчик полученных отчетов.
        public int Count
        {
            get
            {
                if (this.maclsReportList != null)
                {
                    return this.maclsReportList.Length;
                }
                else
                {
                    return 0;
                }
            }
        }


        // Singleton (чтобы каждый раз не создавать фабрику и не сканировать директорию с отчетами)
        //public static readonly ReportsFactory Instance = new ReportsFactory();
        static ReportsFactory instance = null;
        public static ReportsFactory Instance(IReportDataSupplier iDataSupplier)
        {
            if (instance == null)
            {
                lock (typeof(ReportsFactory))
                {
                    if (instance == null)
                    {
                        instance = new ReportsFactory(iDataSupplier);
                    }
                }
            }
            return instance;
        }

        // Конструктор
        private ReportsFactory(IReportDataSupplier iDataSupplier) 
        {
            this.iDataSupplier = iDataSupplier;

            urlWebGis = Globals.AppSettings["UrlWebGis"];
            reportsBase = Globals.AppSettings["ReportsPath"];
            // Если директория указана в конфиге
            if (reportsBase != null && reportsBase != String.Empty)
            {
                reportsBase += @"\Reports\"; //+ operatorID.ToString();

                this.di = new DirectoryInfo(reportsBase);
            }
            else
            {
                reportsBase += @"\Reports.ForWebGIS\"; //+ operatorID.ToString();

                this.di = new DirectoryInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
            }

            // Предварительный список созданных отчетов для сортировки
            this.alReports = new ArrayList();

            // Запрос GUID-ов отчетов разрешенных данному пользователю
            GetGuidsReportsForUser();

            if (this.GuidsReports != null)
            {
                // Загрузка отчетов
                ReportsLoad();
                // Сортировка отчетов
                ReportsSort();
            } 
        }

        // Функция получения GUID-ов отчетов разрешенных данному пользователю
        private void GetGuidsReportsForUser()
        {
            // Датасет, содержащий информацию для отчета.
            DataSet ds;

            ParamValue[] paramValue = new ParamValue[1];
            paramValue[0] = new ParamValue("@for_OperatorID", 0);

            ds = this.iDataSupplier.GetDataFromDB(paramValue, "GetGuidsReportsForUser", new string[] { "REPORT" });

            if (ds == null)
                return;

            // По количеству запрошенных GUID-ов создаем список GUID-ов 
            this.GuidsReports = new Guid[ds.Tables["REPORT"].Rows.Count];
            // Заполнение списка полученных GUID-ов отчетов
            for (int i = 0; i < this.GuidsReports.Length; i++)
            {
                this.GuidsReports[i] = (Guid)ds.Tables["REPORT"].Rows[i]["REPORT_GUID"];
            }
        }



        // Функция проверки прав на каждый загружаемый отчет при множественной загрузке
        private bool UserPermissionReportCheck(TssReportBase clsReportInstance)
        {
            // Получаем GUID отчета
            Attribute AttributeReportGUID = Attribute.GetCustomAttribute(clsReportInstance.GetType(), typeof(GuidAttribute));

            // Ищем в списке GUID-ов отчетов разрешенных данному пользователю
            for (int i = 0; i < this.GuidsReports.Length; i++)
            {
                if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == this.GuidsReports[i].ToString().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }

        // Функция для поиска и загрузки отчетов из текущей директории
        private int ReportsLoad()
        {
            Trace.WriteLine("reports: ReportsLoad()");

            if (this.GuidsReports.Length == 0)
            {
                return 0;
            }

            try
            {
                //Просмотр файлов происходит в текущей директории приложения.

                //DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory + "\\Reports"); 
                DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\Reports");
                //Просматриваются только .dll отчетов.
                FileInfo[] iAssembleFiles = di.GetFiles("*Report*.dll");

                Trace.WriteLine("reports: ReportsLoad()->iAssembleFiles.Length = " + iAssembleFiles.Length);

                //Просмотр последовательно всех файлов сборки.
                foreach (FileInfo fliProccessingFileInfo in iAssembleFiles)
                {
                    //Пропускаются файлы, заведомо являющиеся частью сборки "Report".
                    if (fliProccessingFileInfo.FullName.EndsWith("Report.dll") || fliProccessingFileInfo.FullName.EndsWith("Reports.dll"))
                    {
                        continue;
                    }

                    //Создается экземпляр рассматриваемой .dll.
                    Assembly assProcessingAssembly = Assembly.LoadFile(fliProccessingFileInfo.FullName);

                    Trace.WriteLine("reports: ReportsLoad()->Assembly.LoadFile(fliProccessingFileInfo.FullName) " + fliProccessingFileInfo.FullName);

                    //Сохраняется имя рассматриваемой .dll.
                    string strAssembleName = assProcessingAssembly.FullName;
                    //Массив для хранения списка типов в рассматриваемой .dll.
                    //Получение списка типов в рассматриваемой .dll.
                    Type[] typeList = assProcessingAssembly.GetTypes();

                    //Последовательный перебор всех типов в рассматриваемой .dll. - поиск класса отчета
                    foreach (Type type in typeList)
                    {
                        //Последовательный перебор всех интерфейсов в рассматриваемом типе.
                        if (type.BaseType != null && type.BaseType.Name == typeof(TssReportBase).Name)
                        {
                            Trace.WriteLine("reports: before Activator.CreateInstance");

                            ObjectHandle h = Activator.CreateInstance(strAssembleName, type.ToString());
                            TssReportBase clsReportInstance = h.Unwrap() as TssReportBase;

                            Trace.WriteLine("reports: after Activator.CreateInstance");

                            // Проверка прав пользователя на отчет и Проверка свойства VisibleInReportsList. Нужно ли отображать данный отчет в списке отчетов.
                            if (UserPermissionReportCheck(clsReportInstance) && clsReportInstance.VisibleInReportsList == true)
                            {
                                clsReportInstance.IBasicFunctionSetInstanceSet(this.iDataSupplier);
                                Trace.WriteLine("reports: set IBasicFunctionSetInstance");

                                // Выделение большей области, если количество отчетов привысило длину массива хранения ссылок на отчеты
                                /*if(this.intReportsCounter >= this.maclsReportList.Length)
                                {
                                    TssReportBase[] trbTmp = new TssReportBase[this.maclsReportList.Length + 10];
                                    Array.Copy(this.maclsReportList, 0, trbTmp, 0, this.maclsReportList.Length);
                                    this.maclsReportList = trbTmp;
                                }*/

                                this.alReports.Add(clsReportInstance);

                                // Если счетчик сравнялся с количеством выделенного места под отчеты, то есть все разрешенные отчеты найдены, то далее не сканируем
                                if (this.alReports.Count >= this.GuidsReports.Length)
                                {
                                    return this.alReports.Count; 
                                }
                            }
                        }
                    }
                }
                return this.alReports.Count;
            }
            catch (ReflectionTypeLoadException e)
            {
                foreach (Exception exc in e.LoaderExceptions)
                    Trace.WriteLine(exc.Message);
                return this.alReports.Count;
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
                Trace.TraceError("{0}", ex);
                return this.alReports.Count;
            }
        }

        // Функция сортировки загруженных отчетов
        /*private*/public void ReportsSort()
        {
            // Сортируем 
            //this.alReports.Sort(new SortReports());

            // Переписываем в массив ссылок
            // Соответственно по количеству найденных в директории отчетов выделяемем память под список ссылок на доступные отчеты
            this.maclsReportList = new TssReportBase[this.alReports.Count];

            for (int i = 0; i < this.alReports.Count; i++)
            {
                this.maclsReportList[i] = (TssReportBase)this.alReports[i];
            }
        }

        /// <summary>
        /// Функция создания конкретного отчета по его GUID и заданным параметрам
        /// </summary>
        /// <param name="ReportGuig"></param>
        /// <param name="paramsReport"></param>
        /// <param name="bPrintNow"></param>
        /// <param name="printed">указывает, был ли отчёт отправлен на печать или нет</param>
        public void ReportDisplayPrint(Guid ReportGuig, PARAMS paramsReport, bool bPrintNow, out bool printed)
        {
            printed = false;

            // 
            try
            {
                // Если получены ссылки на доступные отчеты, то перебираем их GUID-ы и сравниваем с заданным
                for (int i = 0; i < this.Count; i++)
                {
                    // Получаем GUID отчета
                    Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.maclsReportList[i].GetType(), typeof(GuidAttribute));
                    // Заданный GUID найден, создаем отчет
                    if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ReportGuig.ToString().ToUpper())
                    {
                        //Переменная для хранения набора параметров, передаваемых в создаваемый отчет.
                        ReportParameters clsReportParameters;
                        //Получение класса с набором параметров для отчета.
                        clsReportParameters = this.maclsReportList[i].ReportParametersInstanceGet();

                        //PARAMS paramsReport = new PARAMS(true);
                        //paramsReport["WaybillHeaderIDProp"] = 3904;

                        //Data Data = null;

                        // Поиск заданных параметрв в классе отчета и задание им полученных значений из переданного словаря
                        if (paramsReport != null)
                        {
                            IDictionaryEnumerator dictionaryEnumerator = paramsReport.GetEnumerator();

                            while (dictionaryEnumerator.MoveNext())
                            {
                                if (dictionaryEnumerator.Key.ToString().ToUpper() == "CONTAINER")
                                {
                                    //Data = dictionaryEnumerator.Value as Data;									
                                }
                                else
                                {
                                    // Поиск заданного параметра
                                    PropertyInfo propertyInfo = clsReportParameters.GetType().GetProperty(dictionaryEnumerator.Key.ToString());

                                    Debug.Assert(propertyInfo != null, "Среди параметров есть неподдерживаемый отчетом");

                                    // Если данный параметр существует, задаем значение
                                    if (propertyInfo != null)
                                    {
                                        propertyInfo.SetValue(clsReportParameters, dictionaryEnumerator.Value, null);
                                    }
                                }
                            }
                        }

                        ReportClass clsResultReportForShow = null;

                        //Создание ссылки на экземпляр готового отчета.
                        //if( Data != null )
                        //  clsResultReportForShow = this.maclsReportList[i].Create( clsReportParameters, Data );
                        //else
                        clsResultReportForShow = this.maclsReportList[i].Create(clsReportParameters);

                        //Отчет был подготовлен (экземпляр отчета существует)
                        if (clsResultReportForShow != null)
                        {
                            //Подготовка отчета к отображению.
                            using (PrintPreviewForm frmReportBlank = new PrintPreviewForm(this.maclsReportList[i], clsResultReportForShow))
                            {
                                //Какое значение имеет флаг печати?
                                if (bPrintNow)
                                {
                                    // Печать отчета на принтере.
                                    frmReportBlank.PrintReport();
                                }
                                else
                                {
                                    //[ПЛ надо отобразить на экране].
                                    //Отображение формы отчета на экране.
                                    frmReportBlank.ShowDialog();
                                }

                                // флаг печати отчёта
                                printed = frmReportBlank.Printed;
                            }

                            //clsResultReportForShow.Close();
                            clsResultReportForShow.Dispose();
                        }
                        // отчет был найден и сформирован, далее не ищем
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
                Trace.TraceError("{0}", ex);
            }
        }

        /// <summary>
        /// Функция создания конкретного отчета по его GUID и заданным параметрам
        /// </summary>
        /// <param name="ReportGuig"></param>
        /// <param name="paramsReport"></param>
        public void ReportDisplayPrint(Guid ReportGuig, PARAMS paramsReport, bool bPrintNow)
        {
            // 
            try
            {
                // Если получены ссылки на доступные отчеты, то перебираем их GUID-ы и сравниваем с заданным
                for (int i = 0; i < this.Count; i++)
                {
                    // Получаем GUID отчета
                    Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.maclsReportList[i].GetType(), typeof(GuidAttribute));
                    // Заданный GUID найден, создаем отчет
                    if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ReportGuig.ToString().ToUpper())
                    {
                        //Переменная для хранения набора параметров, передаваемых в создаваемый отчет.
                        ReportParameters clsReportParameters;
                        //Получение класса с набором параметров для отчета.
                        clsReportParameters = this.maclsReportList[i].ReportParametersInstanceGet();

                        //PARAMS paramsReport = new PARAMS(true);
                        //paramsReport["WaybillHeaderIDProp"] = 3904;

                        //Data Data = null;

                        // Поиск заданных параметрв в классе отчета и задание им полученных значений из переданного словаря
                        if (paramsReport != null)
                        {
                            IDictionaryEnumerator dictionaryEnumerator = paramsReport.GetEnumerator();

                            while (dictionaryEnumerator.MoveNext())
                            {
                                if (dictionaryEnumerator.Key.ToString().ToUpper() == "CONTAINER")
                                {
                                    //Data = dictionaryEnumerator.Value as Data;									
                                }
                                else
                                {
                                    // Поиск заданного параметра
                                    PropertyInfo propertyInfo = clsReportParameters.GetType().GetProperty(dictionaryEnumerator.Key.ToString());

                                    Debug.Assert(propertyInfo != null, "Среди параметров есть неподдерживаемый отчетом");

                                    // Если данный параметр существует, задаем значение
                                    if (propertyInfo != null)
                                    {
                                        propertyInfo.SetValue(clsReportParameters, dictionaryEnumerator.Value, null);
                                    }
                                }
                            }
                        }

                        ReportClass clsResultReportForShow = null;

                        //Создание ссылки на экземпляр готового отчета.
                        //if( Data != null )
                        //    clsResultReportForShow = this.maclsReportList[i].Create( clsReportParameters, Data );
                        //else
                        clsResultReportForShow = this.maclsReportList[i].Create(clsReportParameters);

                        //Отчет был подготовлен (экземпляр отчета существует)
                        if (clsResultReportForShow != null)
                        {
                            //Подготовка отчета к отображению.
                            using (PrintPreviewForm frmReportBlank = new PrintPreviewForm(this.maclsReportList[i], clsResultReportForShow))
                            {
                                //Какое значение имеет флаг печати?
                                if (bPrintNow)
                                {
                                    // Печать отчета на принтере.
                                    frmReportBlank.PrintReport();
                                }
                                else
                                {
                                    //[ПЛ надо отобразить на экране].
                                    //Отображение формы отчета на экране.
                                    frmReportBlank.ShowDialog();
                                }
                            }

                            //clsResultReportForShow.Close();
                            clsResultReportForShow.Dispose();
                        }
                        // отчет был найден и сформирован, далее не ищем
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
                Trace.TraceError("{0}", ex);
            }
        }


        /// <summary>
        /// Функция создания и печати в файл заданного формата (*.pdf, *.xls) конкретного отчета по его GUID и заданным параметрам
        /// </summary>
        /// <param name="ReportGuig"></param>
        /// <param name="paramsReport"></param>
        public void ReportPrintToFile(Guid ReportGuig, PARAMS paramsReport, string fileName, CrystalDecisions.Shared.ExportFormatType formatType)
        {
            // 
            try
            {
                Debug.WriteLine("1 ************************");

                // Если получены ссылки на доступные отчеты, то перебираем их GUID-ы и сравниваем с заданным
                for (int i = 0; i < this.Count; i++)
                {
                    // Получаем GUID отчета
                    Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.maclsReportList[i].GetType(), typeof(GuidAttribute));
                    // Заданный GUID найден, создаем отчет
                    if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ReportGuig.ToString().ToUpper())
                    {
                        Debug.WriteLine("2 ************************");

                        //Переменная для хранения набора параметров, передаваемых в создаваемый отчет.
                        ReportParameters clsReportParameters;
                        //Получение класса с набором параметров для отчета.
                        clsReportParameters = this.maclsReportList[i].ReportParametersInstanceGet();

                        //PARAMS paramsReport = new PARAMS(true);
                        //paramsReport["WaybillHeaderIDProp"] = 3904;

                        //Data Data = null;

                        // Поиск заданных параметрв в классе отчета и задание им полученных значений из переданного словаря
                        if (paramsReport != null)
                        {
                            Debug.WriteLine("3 ************************");

                            IDictionaryEnumerator dictionaryEnumerator = paramsReport.GetEnumerator();

                            while (dictionaryEnumerator.MoveNext())
                            {
                                if (dictionaryEnumerator.Key.ToString().ToUpper() == "CONTAINER")
                                {
                                    //Data = dictionaryEnumerator.Value as Data;
                                }
                                else
                                {
                                    // Поиск заданного параметра
                                    PropertyInfo propertyInfo = clsReportParameters.GetType().GetProperty(dictionaryEnumerator.Key.ToString());

                                    Debug.Assert(propertyInfo != null, "Среди параметров есть неподдерживаемый отчетом");

                                    // Если данный параметр существует, задаем значение
                                    if (propertyInfo != null)
                                    {
                                        propertyInfo.SetValue(clsReportParameters, dictionaryEnumerator.Value, null);
                                    }
                                }
                            }

                            Debug.WriteLine("4 ************************");
                        }

                        ReportClass clsResultReportForShow = null;

                        //Создание ссылки на экземпляр готового отчета.
                        //if (Data != null)
                        //    clsResultReportForShow = this.maclsReportList[i].Create(clsReportParameters, Data);
                        //else
                        clsResultReportForShow = this.maclsReportList[i].Create(clsReportParameters);

                        Debug.WriteLine("5 ************************" + clsResultReportForShow == null + " " + fileName);
                         
                        //Отчет был подготовлен (экземпляр отчета существует)
                        if (clsResultReportForShow != null && fileName != string.Empty)
                        {
                            // Печатаем в файл
                            clsResultReportForShow.ExportToDisk(formatType, fileName);

                            //clsResultReportForShow.Close();
                            clsResultReportForShow.Dispose();

                            Debug.WriteLine("6 ************************");
                        }
                        // отчет был найден и сформирован, далее не ищем
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
                Trace.TraceError("{0}", ex);
            }
        }

        /// <summary>
        /// Возвращает параметры конкретного отчета по его GUID и заданным параметрам
        /// </summary>
        /// <param name="ReportGuig"></param>
        public ReportParamsDictionary GetReportParams(Guid ReportGuig)
        {
            try
            {
                // Если получены ссылки на доступные отчеты, то перебираем их GUID-ы и сравниваем с заданным
                for (int i = 0; i < this.Count; i++)
                {
                    // Получаем GUID отчета
                    Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.maclsReportList[i].GetType(), typeof(GuidAttribute));
                    // если заданный GUID найден, возвращаем параметры отчеты
                    if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ReportGuig.ToString().ToUpper())
                    {
                        return this.maclsReportList[i].ParamsDictionary;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("{0}", ex);
            }
            return null;
        }

        /// <summary>
        /// Формирует название и путь к файлу запрашиваемого отчета 
        /// </summary>
        /// <returns></returns>
        private string BuildReportFileName(int operatorID, Guid guid, ExportFormatType formatType)
        {
            // Поиск заданных параметрв для составления по ним названия файла
            string fileName = String.Format("{0}_{1}_{2}",
                                            operatorID,
                                            guid.ToString().ToLower(),
                                            DateTime.Now.ToString("yyMMdd_HHmm")
                );

            // Формат файла, в котором требуется сохранить отчет
            if (formatType == ExportFormatType.PortableDocFormat)
            {
                fileName += ".pdf";
            }
            else if (formatType == ExportFormatType.Excel)
            {
                fileName += ".xls";
            }

            return fileName;
        }

        /// <summary>
        /// Формирует, печатает в файл и возвращает сформированный отчет 
        /// </summary>
        /// <returns></returns>
        public CreatedReportEventArgs ReportMake(int operatorID, Guid guid, PARAMS reportParams, int format_type, string fileName)
        {
            CrystalDecisions.Shared.ExportFormatType formatType; //(ExportFormatType)format_type;
            switch (format_type)
            {
                case 1:
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
                case 2:
                    formatType = ExportFormatType.Excel;
                    break;
                default:
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }

            // проверяем наличие директории для отчётов пользователя
            DirectoryInfo diOperatorDir = new DirectoryInfo(reportsBase + operatorID.ToString());
            if (!diOperatorDir.Exists)
                diOperatorDir.Create();

            // БД с информацией о сгенерированных отчётах
            GeneratedReportsDataSet dataSet = new GeneratedReportsDataSet();
            // ищем файл с информацией об отчётах
            string xmlFileName = ReportHelper.GetReportXMLFileName(reportsBase, operatorID);
            FileInfo fi = new FileInfo(xmlFileName);
            if (fi.Exists)
                dataSet.ReadXml(xmlFileName);

            if (fileName.Trim() == String.Empty)
                fileName = BuildReportFileName(operatorID, guid, formatType);
            string filePathName = diOperatorDir.FullName + @"\" + fileName;

            GeneratedReportsDataSet.GENERATED_REPORTRow row = null;

            lock (LockOperatorXML(operatorID))
            {
                // создаём новую строку отчёта
                row = dataSet.GENERATED_REPORT.NewGENERATED_REPORTRow();

                // добавляем строку с информацией
                row.OPERATOR_ID = operatorID;
                row.FILE_NAME = fileName;
                //row.REPORT_ID = 0;
                row.REPORT_GUID = guid;
                row.PARAMS = ReportParamsFactory.GetParamsString(reportParams);
                row.STATE = (int)ReportStateEnum.Generating;

                dataSet.GENERATED_REPORT.AddGENERATED_REPORTRow(row);

                dataSet.AcceptChanges();

                dataSet.WriteXml(xmlFileName);
            }

            Debug.WriteLine("ReportPrintToFile Start...");
            ReportPrintToFile(guid, reportParams, filePathName, formatType);
            Debug.WriteLine("ReportPrintToFile End... " + filePathName);
            
            // создаём аргумент для бизнес сервера

            return new CreatedReportEventArgs(new Guid(),
                row.REPORT_ID, 
                "", 
                guid,
                (string)reportParams["Mo"], 
                DateTime.Now,
                DateTime.Now, 
                DateTime.Now, 
                ReportStateEnum.Generated, 
                "");
        }

        public DataSet GetGeneratedReportData(int operatorID)
        {
            // проверяем наличие директории для отчётов пользователя
            DirectoryInfo diOperatorDir = new DirectoryInfo(reportsBase + operatorID.ToString());
            if (!diOperatorDir.Exists)
                diOperatorDir.Create();

            // БД с информацией о сгенерированных отчётах
            GeneratedReportsDataSet dataSet = new GeneratedReportsDataSet();
            
            // ищем файл с информацией об отчётах
            string xmlFileName = ReportHelper.GetReportXMLFileName(reportsBase, operatorID);
            FileInfo fi = new FileInfo(xmlFileName);
            if (fi.Exists)
                dataSet.ReadXml(xmlFileName);

            return dataSet;
        }


        /// <summary>
        /// Блокирует обращение к файлу XML с данными об отчётах
        /// </summary>
        /// <param name="operatorID">id оператора</param>
        public object LockOperatorXML(int operatorID)
        {
            lock (lockReportsXML)
            {
                if (lockReportsXML.ContainsKey(operatorID))
                {
                    return lockReportsXML[operatorID];
                }
                else
                {
                    lockReportsXML.Add(operatorID, new object());
                    return lockReportsXML[operatorID];
                }
            }
        }

        private void WriteXML()
        {

        }
    }

    #endregion // class ReportsFactory


    #region // class SortReports
    public class SortReports : IComparer
    {
        int IComparer.Compare(object oReport1, object oReport2)
        {
            TssReportBase Report1 = (TssReportBase)oReport1;
            TssReportBase Report2 = (TssReportBase)oReport2;

            return Report1.ReportNameGet().CompareTo(Report2.ReportNameGet());
        }
    }
    #endregion // class SortReports


    #region class ReportsCollection
    /// <summary>
    /// Класс коллекция отчетов
    /// </summary>
    public class ReportsCollection : CollectionBase
    {
        public new void Clear()
        {
            List.Clear();
        }

        public int Add(TssReportBase value)
        {
            return List.Add(value);
        }

        public bool Contains(TssReportBase value)
        {
            return List.Contains(value);
        }

        public int IndexOf(TssReportBase value)
        {
            return List.IndexOf(value);
        }

        public void Insert(int index, TssReportBase value)
        {
            List.Insert(index, value);
        }

        public void Remove(TssReportBase value)
        {
            List.Remove(value);
        }

        public TssReportBase this[int index]
        {
            get
            {
                return (TssReportBase)List[index];
            }
            set
            {
                List[index] = value;
            }
        }
    }
    #endregion // class ReportsCollection



}
