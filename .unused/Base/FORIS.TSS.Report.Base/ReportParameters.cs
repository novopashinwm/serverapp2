﻿using System;
using System.ComponentModel;
using FORIS.TSS.TransportDispatcher;

namespace FORIS.TSS.Report.Base
{

    /// <summary>
    /// Параметры для создания отчетов.
    /// </summary>
    [Serializable]
    public class ReportParameters
    {
        /// Класс создан для формализации типа. Все свойства этого класса задаются в каждом отчете отдельно.

        /// <summary>
        /// Дата, на которую формируется отчет (если отчет формируется за один день).
        /// </summary>
        protected DateTime dtDateReport = DateTime.Now.Date;
        /// <summary>
        /// Свойство - Дата, на которую формируется отчет (если отчет формируется за один день).
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("DateDN"), Category("ReportParsCat"), Description("DateDesc")]
        [Browsable(false)]
        public virtual DateTime DateReport
        {
            get
            {
                return dtDateReport;
            }
            set
            {
                dtDateReport = value;
            }
        }

        /// <summary>
        /// Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
        /// </summary>
        protected DateTime dtDateFrom = DateTime.Now.AddDays(-2).Date;
        /// <summary>
        /// Свойство - Дата начала периода, на который формируется отчет (если отчет формимуруется за период).
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodBeginDN"), Category("ReportParsCat"), Description("PeriodBeginDesc")]
        [Browsable(false)]
        public virtual DateTime DateFrom
        {
            get
            {
                return dtDateFrom;
            }
            set
            {
                dtDateFrom = value;
            }
        }

        /// <summary>
        /// Дата окончания периода, на который формируется отчет (если отчет формируется за период).
        /// </summary>
        protected DateTime dtDateTo = DateTime.Now.Date;
        /// <summary>
        /// Свойство - Дата окончания периода, на который формируется отчет (если отчет формируется за период).
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("PeriodEndDN"), Category("ReportParsCat"), Description("PeriodEndDesc")]
        [Browsable(false)]
        public virtual DateTime DateTo
        {
            get
            {
                return dtDateTo;
            }
            set
            {
                dtDateTo = value;
            }
        }


        /// <summary>
        /// Переменная для хранения значения флага, определяющего отображается ли отчет заполненным, или он будет отображаться пустым.
        /// </summary>
        bool mblnShowBlankReport = false;
        /// <summary>
        /// Свойство-флаг, определяет отображается ли отчет заполненным данными, или он будет отображаться пустым.
        /// </summary>
        [BusinessLogic.Attributes.DisplayName("EmpryReportDN", true), Category("ReportParsCat"), Description("EmpryReportDesc")]
        public bool ShowBlankReportProp
        {
            get
            {
                return mblnShowBlankReport;
            }
            set
            {
                mblnShowBlankReport = value;
            }
        }


        /// <summary>
        /// Набор для параметров проверяемых на права (ТС, групп ТС) 
        /// </summary>
        //public DataSet dsParamsCheckSecurity = 

    }
}
