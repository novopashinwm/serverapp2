using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.ComponentModel;

using System.Diagnostics;
using FORIS.TSS.TransportDispatcher;


namespace FORIS.TSS.Report.Base.Editors
{
	/// <summary>
	/// Summary description for ListBoxEditor.
	/// </summary>
	//---------------------
	//������������� ���������� ������ � ������� �������� ��� PropertyGrid. ������������ ���, ��� � PropertyGrid ������������ �������� ������ ���� ListBox.
	//����� ������������ � ������ �������� �������� � ���� ValueMemberText ������� DataSourceTable.
	//����� ��������, ��������������� ������������ � ������, �������� � ���� ValueMemberDisplay ������� DataSourceTable.
	//[Serializable]
	public class ListBoxEditor : System.Drawing.Design.UITypeEditor
	{
		//��������� ������ ��� ���������� ������.
		//�������, ���������� �������� ��� ���������� ������.
		public static DataTable DataSourceTable;
		//�������� �������, �������� ������� ����� ������������ � ������.
		public static string ValueMemberText;
		//�������� �������, �������� ������� ����� ��������������� ������������ ��������� ������ � ���������� �� �����.
		//��� ��������� �� ������ ��� �� ��������, ������� ���������� � ������, ������� ������� ValueMemberText � ValueMemberTag ������������.
		public static string ValueMemberTag;
		//�������� ������� ����������� ������.
		ListBox lbxList = new ListBox();	
		//������, ������� �������� �� ����������� ��������� �� ������.
		IWindowsFormsEditorService clsEditorService;
            
		//����������� (����������� ��������).
		public ListBoxEditor()
		{          
			//����������� �������-����������� ������ ������ �������� �� ������.
			lbxList.SelectedIndexChanged += new System.EventHandler(this.lbx_SelectedIndexChanged); 
			//�� ���� ����������� ������ ���������� �� ��������! ����� ��� ������ �������� �� ������ 
			//������������ �������� �� �������� � ����������. � �������� ������� ���������� ��� ���������������
			//������. �� ������� ����������������� ��������� ������.
			//lbxList.Sorted = true;
		}

		//�����, ���������� ��� �������������� �������� � PropertyGrid.
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			//���������� ��� �������� ������ ���������� �� ������ ������.
			int intSelectedIndex = -1;

			try
			{
				//�������� ���������� ������� ��� �������� ��������������� ��������, ������������� ��������.
				TagListBoxItem clsResultValue = new TagListBoxItem("", null);

				//���������� ������ �� EditorService.
				clsEditorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
				//������� ������.
				lbxList.Items.Clear(); 

				/*// ��������� ���������� ��� ������ { 
				try
				{
					PropDispNameWrapper pdnwPropDispNameWrapper = (PropDispNameWrapper)((System.Object)(context.Instance));
					// ����� ����� ����������, �� � ��� ����� �������� DataTable
					object o = pdnwPropDispNameWrapper.Unwrap; 
					ReportParameters reportParameters = o as ReportParameters;

					for(int i = 0; i < ReportsFactory.Instance(false).Count; i++)
					{
						//ReportsFactory.Instance(false).ReportsList[i].IBasicFunctionSetInstanceSet(Interface.IPersonalServer);
						if(ReportsFactory.Instance(false).ReportsList[i].ReportParametersInstanceGet().GetType() == reportParameters.GetType() )
						{
							ReportsFactory.Instance(false).ReportsList[i].ReportParametersInstanceRefresh(reportParameters);
							break;
						}
					}  
				}
				catch (Exception ex)
				{
					//����� ��������� �� ������.
#if DEBUG
					Trace.WriteLine("� ������� " + ex.Source + " ��������� ������: " + ex.Message);
#endif
				}
				// } */

                //������� �������� �� �����?
				if (DataSourceTable != null)
				{
					//��������� ���������� ������� � ������� ��������.
					int intRecordsCount = DataSourceTable.Rows.Count;

					//���������� ������ ����������.
					for(int intRowsCounter = 0; intRowsCounter < intRecordsCount; intRowsCounter++)
					{
						//�������� � ������ ������ ���������� ��������.
						// ���� ���� DateTime, �� ������� ������ ���� (��� �������)
						DateTime dtDate;
						if(DataSourceTable.Rows[intRowsCounter][ValueMemberText].GetType() == typeof(DateTime))
						{
							dtDate = (DateTime)DataSourceTable.Rows[intRowsCounter][ValueMemberText];
							lbxList.Items.Add(dtDate.ToShortDateString()); 
						}
						else
						{
							lbxList.Items.Add(DataSourceTable.Rows[intRowsCounter][ValueMemberText].ToString());
						}
					}
				}

				//���������� ������.
				clsEditorService.DropDownControl(lbxList);
				//��������� ������� ���������� �� ����������� ������ ��������.
				intSelectedIndex = lbxList.SelectedIndex;

				//��� �� ������ ������� �� ����������� ������?
				if (intSelectedIndex > -1)
				{
					//������� �������� �� �� ��������, ��������������� ���������� ������������� �� ����������� ������?
					if (DataSourceTable.Rows[intSelectedIndex][ValueMemberTag] != DBNull.Value)
					{
						//������������ ������������� �������, ����������� ��������� �������� � ������ �� ������, ������������� ���������� �������� �� ������.
						//��������� ���������� �������� � ������� ��� ����������� � ���������� ������.
						DateTime dtDate;
						if(DataSourceTable.Rows[intSelectedIndex][ValueMemberText].GetType() == typeof(DateTime))
						{
							dtDate = (DateTime)DataSourceTable.Rows[intSelectedIndex][ValueMemberText];
							clsResultValue.Text = dtDate.ToShortDateString();
						}
						else
						{
							clsResultValue.Text = (DataSourceTable.Rows[intSelectedIndex][ValueMemberText]).ToString();
						}
						//��������� ������ � �������� �������.
						clsResultValue.Tag = DataSourceTable.Rows[intSelectedIndex][ValueMemberTag];
					}
				}
			
				//����������� ��������������� �������.
				if(value.GetType()==typeof(string)) //clsResultValue.GetType());
				{
					return clsResultValue.Text.Trim();
				}
				return clsResultValue;
			}
			catch (Exception ex)
			{
				//����� ��������� �� ������.
#if DEBUG
                Trace.WriteLine("� ������� " + ex.Source + " ��������� ������: " + ex.Message);
#endif
				return null;
			}
		}

		//�����, ������������ ��� ����� ��������� ��������. 
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			//� ������ ������ �������� ����� ��� ����������� ������.
			return UITypeEditorEditStyle.DropDown;
		}

		//������� ���������, � ����� ������ ��� ������� ��������� ���������� �������� ������
		private void lbx_SelectedIndexChanged(object sender, System.EventArgs e)  
		{
			//���������� ������ �������� � ������.
			clsEditorService.CloseDropDown();
		}
	}
}


