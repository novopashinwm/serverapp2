﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.Interfaces.ReportService.Params;

namespace FORIS.TSS.Report.Base
{
    /// <summary>
    /// Класс содержит функции, общие для всех отчетов.
    /// </summary>
    public abstract class TssReportBase : ITssReport
    {
        /// <summary>
        /// Список параметров отчёта
        /// </summary>
        protected ReportParamsDictionary reportParamsDictionary;

        /// <summary>
        /// Обработчик движения прогрессбара. 
        /// </summary>
        public event ProgressEventHandler Progress;

        /// <summary>
        /// Событие печати отчёта
        /// </summary>
        public event EventHandler Print;

        /// <summary>
        /// Обрабатывает событие печати отчёта
        /// </summary>
        public virtual bool BeforePrint()
        {
            return true;
        }

        /// <summary>
        /// Обрабатывает событие печати отчёта
        /// </summary>
        public virtual void OnPrinted(int iOperatorID)
        {
            if (Print != null)
                Print(this, null);
        }

        /// <summary>
        /// Функция генерации события прогрессбара в отчете.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnProgress(ProgressEventArgs e)
        {
            if (Progress != null)
                Progress(this, e);
        }

        #region statics

        /// <summary>
        /// Функция заполнения данными в отчете полей с атрибутами предприятия (название предприятия, подпись создателя отчете и т.д.).
        /// </summary>
        /// <param name="iDataSupplier">Ссылка на экземпляр класса интерфейсов.</param>
        /// <returns></returns>
        public static ConstantsListDataset ReportEstablishmentAttributesFill(IReportDataSupplier iDataSupplier)
        {
            //Создание датасета со структурой таблицы с данными предприятия.
            ConstantsListDataset dsEstablishmentAttributes = new ConstantsListDataset();

            try
            {
                //Переменная для хранения названия предприятия.
                string strEstablishmentName = "";
                //Переменная для хранения адреса предприятия.
                string strEstablishmentAddress = "";
                //Переменная для хранения телефона предприятия.
                string strEstablishmentPhone = "";
                //Переменная для хранения подписи создателя отчета.
                string strReportCreatorSignature = "";

                ////Получение констант из БД.
                //DataSet dsConstantsFromBD = clsInstance.ConstantsGet();
                ////Перебор всех записей из данных с константами.
                //foreach (DataRow rowConstantData in dsConstantsFromBD.Tables["ConstantsList"].Rows)
                //{
                //    //Рассматриваемая на данной итерации константа - название предприятия?
                //    if (rowConstantData["ConstantName"].ToString() == "ESTABLISHMENT_NAME")
                //    {
                //        //[Рассматриваемая на данной итерации константа - название предприятия].
                //        //Сохранение значения названия предприятия.
                //        strEstablishmentName = rowConstantData["ConstantValue"].ToString();
                //    }

                //    //Рассматриваемая на данной итерации константа - подпись создателя отчета?
                //    if (rowConstantData["ConstantName"].ToString() == "REPORT_CREATOR_SIGNATURE")
                //    {
                //        //[Рассматриваемая на данной итерации константа - подпись создателя отчета].
                //        //Сохранение значения подписи создателя отчета.
                //        strReportCreatorSignature = rowConstantData["ConstantValue"].ToString();
                //    }

                //    //Рассматриваемая на данной итерации константа - подпись создателя отчета?
                //    if (rowConstantData["ConstantName"].ToString() == "ESTABLISHMENT_ADDRESS")
                //    {
                //        //[Рассматриваемая на данной итерации константа - адрес предприятия].
                //        strEstablishmentAddress = rowConstantData["ConstantValue"].ToString();
                //    }

                //    //Рассматриваемая на данной итерации константа - подпись создателя отчета?
                //    if (rowConstantData["ConstantName"].ToString() == "ESTABLISHMENT_PHONE")
                //    {
                //        //[Рассматриваемая на данной итерации константа - телефон предприятия].
                //        strEstablishmentPhone = rowConstantData["ConstantValue"].ToString();
                //    }
                //}

                // константа - название предприятия?
                strEstablishmentName = iDataSupplier.GetConstant("ESTABLISHMENT_NAME");
                // константа - подпись создателя отчета?
                strReportCreatorSignature = iDataSupplier.GetConstant("REPORT_CREATOR_SIGNATURE");
                // константа - подпись создателя отчета?
                strEstablishmentAddress = iDataSupplier.GetConstant("ESTABLISHMENT_ADDRESS");
                // константа - подпись создателя отчета?
                strEstablishmentPhone = iDataSupplier.GetConstant("ESTABLISHMENT_PHONE");


                //Создание строки, имеющей структуру данных как у таблицы ConstantsList.
                ConstantsListDataset.ConstantsListRow rowConstantsData = dsEstablishmentAttributes.ConstantsList.NewConstantsListRow();
                //Установка значений для полей отчета с атрибутами предприятия.
                rowConstantsData.EstablishmentName = strEstablishmentName;
                rowConstantsData.EstablishmentAddress = strEstablishmentAddress;
                rowConstantsData.EstablishmentPhone = strEstablishmentPhone;
                rowConstantsData.ReportCreatorSignature = strReportCreatorSignature;
                //Отправка данных в датасет.
                dsEstablishmentAttributes.ConstantsList.AddConstantsListRow(rowConstantsData);

                //Функция возвращает сформированный датасет с атрибутами предприятия.
                return dsEstablishmentAttributes;
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
#if DEBUG
                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
#endif
                return null;
            }
            finally
            {
                //Датасет еще не унитчтожен?
                if (dsEstablishmentAttributes != null)
                //[Датасет еще не уничтожен].
                {
                    //Уничтожение датасета.
                    dsEstablishmentAttributes.Dispose();
                    // нет смысла переменная локальная
                    // dsEstablishmentAttributes = null;
                }
            }
        }

        /// <summary>
        /// Функция объединения значений даты (DateTime) и времени (String).
        /// </summary>
        /// <param name="dtmReportDate">Введенное значение времени</param>
        /// <param name="strReportTime">Строковое значение времени</param>
        /// <returns>Скорректированное значение времени</returns>
        public static DateTime DateTimeMerge(DateTime dtmReportDate, string strReportTime)
        {
            //Переменная для хранения даты, на которую следует получить отчет.
            DateTime dtmSplitDateTime;

            //Объединение введенного значения времени и даты.
            //Проверка введенного значения происходит в методе set свойства.
            string[] astrTimeParst = strReportTime.Split(':');
            //Получение количества часов во введенном значении времени.
            double dblReportTimeHours = System.Convert.ToDouble(astrTimeParst[0]);
            //Получение количества минут во введенном значении времени.
            double dblReportTimeMinutes = System.Convert.ToDouble(astrTimeParst[1]);
            //Получение даты и времени путем соединения двух переданых параметров: даты и времени, на которые формируется отчет.
            dtmSplitDateTime = dtmReportDate.AddHours(dblReportTimeHours);
            dtmSplitDateTime = dtmSplitDateTime.AddMinutes(dblReportTimeMinutes);

            //Приведение времени к UTC для произведения корректного запроса из БД.
            //dtmSplitDateTime = dtmSplitDateTime.ToUniversalTime();

            return dtmSplitDateTime;
        }

        /// <summary>
        /// Функция проверки и преобразования вводимого пользователем значения времени в свойствах отчета.
        /// </summary>
        /// <param name="iCheckingTimeValue">Проверяемое значение введенного времени</param>
        /// <returns>Скорректированное значение времени</returns>
        public static string TimeInputCheck(string iCheckingTimeValue)
        {
            //Получение временини на текущий момент для показа примера верного значения.
            string strCurrentTimeValue = DateTime.Now.ToShortTimeString();
            //Переменная для хранения результирующего значения функции.
            string strReturnValue = iCheckingTimeValue;

            try
            {
                //Переменная для хранения результатов проверки введенного значения.
                bool blnWrongTimePartValue = false;
                //Переменная для хранения введенного количества часов.
                int intTimePartHoursValue = 0;
                //Переменная для хранения введенного количества минут.
                int intTimePartMinutesValue = 0;

                //Проверка того, что значение было вобще введено.
                if (iCheckingTimeValue == null)
                {
                    //Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
                    blnWrongTimePartValue = true;
                }

                //Выделение из введенного значения времени количества часов и минут.
                string[] astrTimeParst = iCheckingTimeValue.ToString().Split(':');

                //Во введенном значении был найден разделитель-двоеточие?
                if (astrTimeParst.Length == 2)
                {
                    //Получение введенного количества часов.
                    intTimePartHoursValue = Convert.ToInt32(astrTimeParst[0]);
                    //Получение введенного количества минут.
                    intTimePartMinutesValue = Convert.ToInt32(astrTimeParst[1]);
                }
                else
                {
                    //Получение введенного значения в виде строки.
                    string strEnteredString = astrTimeParst[0];
                    //Вычисление количества символов во введенной строке.
                    int intSymbolsCount = strEnteredString.Length;

                    //Определение введенного времени.
                    switch (intSymbolsCount)
                    {
                        case 3:
                            //[Введено три цифры (кол-во часов может оказаться меньше 10)].
                            //Получение количества часов в веденном значении времени.
                            intTimePartHoursValue = Convert.ToInt32(strEnteredString.Substring(0, 1));
                            //Получение количества минут в веденном значении времени.
                            intTimePartMinutesValue = Convert.ToInt32(strEnteredString.Substring(1, 2));
                            break;
                        case 4:
                            //[Введено четыре цифры (две - количество часов, две - количество минут)].
                            //Получение количества часов в веденном значении времени.
                            intTimePartHoursValue = Convert.ToInt32(strEnteredString.Substring(0, 2));
                            //Получение количества часов в веденном значении времени.
                            intTimePartMinutesValue = Convert.ToInt32(strEnteredString.Substring(2, 2));
                            break;
                        default:
                            //Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
                            blnWrongTimePartValue = true;
                            break;
                    }

                    //В строку времени добавляется опущенное пользователем двоеточие.
                    strReturnValue = intTimePartHoursValue + ":" + intTimePartMinutesValue;
                }

                //Проверка корректности введенного количества часов.
                if (intTimePartHoursValue < 0 || intTimePartHoursValue > 23)
                {
                    //Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
                    blnWrongTimePartValue = true;
                }

                //Проверка корректности введенного количества минут.
                if (intTimePartMinutesValue < 0 || intTimePartMinutesValue > 59)
                {
                    //Взводится флаг-признак того, что введенное значения не прошло проверку на корректность.
                    blnWrongTimePartValue = true;
                }
                else
                {
                    //Пользователь ввел одну цифру в значение минут?
                    if (astrTimeParst[1].Length == 1)
                    {
                        //[Пользователь ввел одну цифру в значение минут].
                        //Следует добавить ноль в значение минут в конец или в начало?
                        if (intTimePartMinutesValue < 6)
                        {
                            //[Следует добавить ноль в значение минут в конец].
                            //Корректировка введенного пользователем значения минут: к возвращаемому результату добавляется ноль (например, 16:2 превращается в 16:20).
                            strReturnValue += "0";
                        }
                        else
                        {
                            //[Следует добавить ноль в значение минут в конец].
                            //Корректировка введенного пользователем значения минут: к возвращаемому результату добавляется ноль (например, 16:7 превращается в 16:07).
                            strReturnValue = intTimePartHoursValue + ":0" + intTimePartMinutesValue; ;
                        }
                    }
                }

                //Значение времени введено корректно?
                if (blnWrongTimePartValue)
                {
                    //[Значение времени введено некорректно].
                    //Возвращение старого неизмененного значения времени.
                    strReturnValue = strCurrentTimeValue;
                    //Сообщение для пользователя о некорректном вводе значения времени.
                    MessageBox.Show("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
                    //Trace.WriteLine("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
                }

                //Функция возвращает скорректированное значение.
                return strReturnValue;
            }
            catch
            {
                //[Значение времени введено некорректно].
                //Сообщение для пользователя о некорректном вводе значения времени.
                //Trace.WriteLine("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
                MessageBox.Show("Время введено неверно. Пример правильного значения: " + strCurrentTimeValue);
                //Возвращение старого неизмененного значения времени.
                return strCurrentTimeValue;
            }
        }

        /// <summary>
        /// Функция возвращает название статуса ПЛ по ID его статуса.
        /// </summary>
        /// <param name="iWaybillStatus">ID статуса рассматриваемого ПЛ</param>
        /// <param name="oWaybillStatusName">Текстовое описание статуса</param>
        /// <param name="oWaybillIsActive">Флаг, указывающие состояние ПЛ - основной или неосновной</param>
        /// <returns></returns>
        public static void WaybillStatusNameByIDGet(int iWaybillStatus, ref string oWaybillStatusName, ref bool oWaybillIsActive)
        {
            try
            {
                //Переменные для хранения возвращаемых значений.
                string strWaybillStatusName;
                bool blnWaybillIsActive = false;

                //Установка названия статуса ПЛ по ID статуса.
                if (((iWaybillStatus >= 1) && (iWaybillStatus <= 7)) || (iWaybillStatus == 10))
                {
                    strWaybillStatusName = "основной";
                    //Для рейсаов с таким состоянием ПЛ считаются активными.
                    /*if(iWaybillStatus == 2)
                    {
                        strWaybillStatusName += " (отстой)";
                    }
                    else if(iWaybillStatus == 3)
                    {
                        strWaybillStatusName += " (обед)";
                    }
                    else if(iWaybillStatus == 7)
                    {
                        strWaybillStatusName += " (заправка)";
                    }
                    else if(iWaybillStatus == 10)
                    {
                        strWaybillStatusName += " (простой)";
                    }*/

                    blnWaybillIsActive = true;
                }
                else if (iWaybillStatus == 8)
                {
                    strWaybillStatusName = "резерв";
                }
                else if (iWaybillStatus == 9)
                {
                    strWaybillStatusName = "заказ";
                    //Для рейсаов с таким состоянием ПЛ считаются активными.
                    blnWaybillIsActive = true;
                }
                else if ((iWaybillStatus == 11) || (iWaybillStatus == 12))
                {
                    strWaybillStatusName = "ремонт";
                }
                else if (iWaybillStatus == 13)
                {
                    strWaybillStatusName = "без выезда";
                }
                else
                {
                    strWaybillStatusName = "неизвестный";
                }

                //Функция возвращает значение.
                oWaybillStatusName = strWaybillStatusName;
                oWaybillIsActive = blnWaybillIsActive;
            }
            catch (Exception ex)
            {
                //Вывод сообщения об ошибке.
#if DEBUG
                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
#endif
            }
        }

        #endregion //  statics

        //Переменная для хранения ссылки на экземпляр класса интерфейсов.
        //Ссылка на экземпляр класса интерфейсов используется в отчетах, вызывающих для получения данных хранимые процедуры.
        protected IReportDataSupplier iDataSupplier;

        #region ITssReport members

        /// <summary>
        /// Текстовое описание отчета.
        /// </summary>
        public virtual string ReportNameGet()
        {
            return String.Empty;
        }

        /// <summary>
        ///	Невозможно создать ссылку на экземпляр класса интерфейсов напрямую, поэтому приходится передавать ее в класс и хранить в нем.
        ///	Ссылка на экземпляр класса интерфейсов используется в отчетах, вызывающих для получения данных хранимые процедуры.
        /// </summary>
        /// <param name="iDataSupplier">Ссылка на объект, содержащий интерфейсы</param>
        public void IBasicFunctionSetInstanceSet(IReportDataSupplier iDataSupplier)
        {
            this.iDataSupplier = iDataSupplier;
        }

        /// <summary>
        /// Функция возвращает экземпляр класса, содержащий набор параметров для данного отчета.
        /// </summary>
        /// <returns></returns>
        public virtual ReportParameters ReportParametersInstanceGet()
        {
            return null;
        }

        /// <summary>
        /// Функция возвращает экземпляр класса, содержащий набор скорректированных параметров для данного отчета.
        /// Например, если один параметр отчета зависит от другого, выбираемого пользователем, то этот метод автоматически подставляет требуемые значения
        /// в зависимый параметр на основе данных, введенных пользователем в первый параметр.
        /// </summary>
        /// <param name="iReportParameters">Частично заполненный набор параметров отчета</param>
        /// <returns>Скорректированный набор параметров</returns>
        public virtual ReportParameters ReportParametersInstanceRefresh(ReportParameters iReportParameters)
        {
            return null;
        }

        /// <summary>
        /// Функция проверяет переданный набор параметров на корректность заполнения.
        /// После изменения любого из параметров весь набор надо проверять на корректность заполнения.
        /// Если параметры заполненны некорректно, то кнопку "Запуск" следует вобще оставлять недоступной.
        /// </summary>
        /// <param name="iReportParameters">Заполненный набор параметров отчета</param>
        /// <returns>В случае корректности заполнения возвращает true, в случае некорректных или неполных параметров возвращает false</returns>
        public virtual bool ReportParametersInstanceCheck(ReportParameters iReportParameters)
        {
            //У этого отчета нет параметров, которые следует проверять, результат проверки всегда положительный.
            return true;
        }

        /// <summary>
        /// Свойство указывает отображается ли название данного отчета в списке отчетов.
        /// </summary>
        /// <returns></returns>
        public virtual bool VisibleInReportsList
        {
            get { return true; }
        }

        /// <summary>
        /// Создает отчет, используя переданные параметры
        /// </summary>
        /// <param name="iReportParameters">Ссылка на экземпляр ссылок на интерфейсы</param>
        /// <returns></returns>
        public virtual ReportClass Create(ReportParameters iReportParameters)
        {
            return new ReportClass();
        }

        public ReportParamsDictionary ParamsDictionary
        {
            get { return reportParamsDictionary; }
        }

        #endregion // ITSS members

        protected virtual void SetParamsDictionary(){}
        //protected abstract void SetParamsDictionary();

        public TssReportBase()
        {
            reportParamsDictionary = new ReportParamsDictionary(); 
            
            reportParamsDictionary.Add("Name", new ReportParamsCommon(
                0,
                ReportParamsType.String,
                ReportParamsCategory.Report,
                "Название",
                "Название отчёта",
                true,
                true,
                0));
            reportParamsDictionary.Add("Guid", new ReportParamsCommon(
                0,
                ReportParamsType.Guid,
                ReportParamsCategory.Report,
                "GUID",
                "Уникальный идентификатор отчёта",
                true,
                true,
                1));
        }
    }
}
