using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System.Runtime.InteropServices;

//����������� ������������ ���� � �������� �����������������.
using FORIS.TSS.BusinessLogic;


namespace FORIS.TSS.Report.Base
{
	/// <summary>
	/// Summary description for PrintPreviewrForm.
	/// </summary>
	public class PrintPreviewForm : Form
	{
		#region Data

		/// <summary>
		/// ���� ������ ������: true-����� ���������, false-����� �� ���������
		/// </summary>
		bool printed = false;
		
		/// <summary>
		/// ������� ������ ������ 
		/// </summary>
		//public event EventHandler Print;

		private int iLogOperatorId;

		private string sLogComment;

		/// <summary>
		/// ������ �� ������ ������
		/// </summary>
		private TssReportBase report;	

		/// <summary>
		/// ���������� ��� �������� ������ �� ������ � "Interfaces".
		/// </summary>
		//protected IBasicFunctionSet Instance;

		/// <summary>
		/// 
		/// </summary>
		private CrystalReportViewer crystalReportViewer1;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		/// <summary>
		/// ��������� ������
		/// </summary>
		protected ReportClass rc = null;

		#endregion // Data

		#region Properties

		public int LogOperatorId
		{
			get
			{
				return iLogOperatorId;
			}

			set
			{
				iLogOperatorId = value;
			}
		}

		public string LogComment
		{
			get
			{
				return sLogComment;
			}

			set
			{
				sLogComment = value;
			}
		}

		/// <summary>
		/// ���� ������ ������: true-����� ���������, false-����� �� ���������
		/// </summary>
		public bool Printed
		{
			get { return printed; }
		}

		#endregion // Properties

		#region .ctor

		public PrintPreviewForm()
		{
			this.Closing += new CancelEventHandler(PrintPreviewForm_Closing);

			InitializeComponent();
		}

		public PrintPreviewForm(ReportClass rc) : this()
		{
			this.rc = rc;
		}

		public PrintPreviewForm(TssReportBase report, ReportClass rc) : this()
		{
			this.report = report;
			this.rc = rc;
		}

		public PrintPreviewForm(TssReportBase report, ReportParameters parameters) : this()
		{
			this.report = report;
			ReportClass reportClass = report.Create(parameters);
			this.rc = reportClass != null ? reportClass : new ReportClass();
		}

		#endregion // .ctor

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PrintPreviewForm));
			this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
			this.SuspendLayout();
			// 
			// crystalReportViewer1
			// 
			this.crystalReportViewer1.ActiveViewIndex = -1;
			this.crystalReportViewer1.DisplayBackgroundEdge = false;
			this.crystalReportViewer1.DisplayGroupTree = false;
			this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.crystalReportViewer1.EnableDrillDown = false;
			this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
			this.crystalReportViewer1.Name = "crystalReportViewer1";
			this.crystalReportViewer1.ReportSource = null;
			this.crystalReportViewer1.Size = new System.Drawing.Size(512, 406);
			this.crystalReportViewer1.TabIndex = 1;
			this.crystalReportViewer1.Navigate += new CrystalDecisions.Windows.Forms.NavigateEventHandler(this.crystalReportViewer1_Navigate);
			// 
			// PrintPreviewForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(512, 406);
			this.Controls.Add(this.crystalReportViewer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "PrintPreviewForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "��������������� ��������";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.PrintPreviewrForm_Load);
			this.ResumeLayout(false);

		}
		#endregion

		protected PaperOrientation paperOrientation;

		public PaperOrientation PaperOrientation
		{
			get
			{
				return paperOrientation;
			}
			set
			{
				paperOrientation = value;
			}
		}

		protected DataSet dataSet = null;

		public DataSet DataSet
		{
			get
			{
				return dataSet;
			}
			set
			{
				dataSet = value;
			}
		}

		protected string reportName = null;

		public string ReportName
		{
			get
			{
				return reportName;
			}
			set
			{
				reportName = value;
			}
		}

		#region SetControls()

		/// <summary>
		/// ����������� ������ ������������ � ���� �������.
		/// </summary>
		/// <remarks>����� ���������� - ����������� ���� ������ � ������������ � �������. ������ CrystalReportViewer 
		/// ������ ������ �������� ������, � ������� ���������� ��������� ������ �� �����.</remarks>
		private void SetControls()
		{
			// ������ ������� � ����������� �� �������
			foreach(Control c in crystalReportViewer1.Controls)
			{
				string type = c.GetType().ToString();

				switch(type)
				{
					case "System.Windows.Forms.StatusBar":
						c.Visible = false;
						break;
					case "CrystalDecisions.Windows.Forms.PageView": 
						PageView pv = (PageView)c;
						foreach(Control pvc in pv.Controls)
							if(pvc.GetType().ToString() == "System.Windows.Forms.TabControl")
							{
								TabControl tbc = (TabControl)pvc;
								if(tbc.TabPages.Count > 0)
								{
									tbc.TabPages[0].Text = "�����";
									break;
								}
							}
						pv.ShowGotoPageButton = false;
						pv.ShowGroupTreeButton = false;
						pv.ShowTextSearchButton = false;
						break;
                    case "System.Windows.Forms.ToolBar":
						{
							// ����� ������ ��� ������ �������
							ToolBarButton printButton = new ToolBarButton();
	//						//������������ ����� ������ ������.
	//						ToolBarButton tbbCustomPrintButton = new ToolBarButton();
	//						tbbCustomPrintButton.Text = "Test";
	//						//���������� ����� ������ � ������.
							ToolBar tb = (ToolBar)c;
							//						tb.Buttons.Add(tbbCustomPrintButton);
							tb.ButtonClick +=new ToolBarButtonClickEventHandler(tb_ButtonClick);  
							
							foreach(ToolBarButton b in tb.Buttons)
							{
								switch(b.ToolTipText)
								{
									case "Go to First Page":
										b.ToolTipText = "�� ������ ��������";
										break;
									case "Go to Previous Page":
										b.ToolTipText = "�� ���������� ��������";
										break;
									case "Go to Next Page":
										b.ToolTipText = "�� ��������� ��������";
										break;
									case "Go to Last Page":
										b.ToolTipText = "�� ��������� ��������";
										break;
									case "Goto Page":
										b.ToolTipText = "������� �� ��������";
										break;
									case "Close Current View":
										b.ToolTipText = "������� �����";
										break;
									case "Print Report":
										printButton.ToolTipText = "�������� �����";
										printButton.ImageIndex = b.ImageIndex;
										// ��� ����� 1 - ������� �� ������ ��������
										printButton.Tag = 1;
										int index = tb.Buttons.IndexOf(b);
										tb.Buttons.Insert(index, printButton);
										// ������� ������ ������
										tb.Buttons.Remove(b);
										break;
									case "Refresh":
										b.ToolTipText = "��������";
										break;
									case "Export Report":
										b.ToolTipText = "�������������� � ����"; 
										break;
									case "Toggle Group Tree":
										b.ToolTipText = "������";
										break;
									case "Zoom":
										b.ToolTipText = "�������";
										break;
									case "Search Text":
										b.ToolTipText = "�����";
										break;
								}
							}
						}
						break;
                    case "System.Windows.Forms.ToolStrip": 
                        {
                            ToolStrip tb = (ToolStrip)c;
                            tb.ItemClicked += new ToolStripItemClickedEventHandler(tb_ButtonClick);

                            int index = -1;
                            int imageIndex = -1;

                            foreach (ToolStripItem b in tb.Items)
                            {
                                switch (b.ToolTipText)
                                {
                                    case "Go to First Page":
                                        b.ToolTipText = "�� ������ ��������";
                                        break;
                                    case "Go to Previous Page":
                                        b.ToolTipText = "�� ���������� ��������";
                                        break;
                                    case "Go to Next Page":
                                        b.ToolTipText = "�� ��������� ��������";
                                        break;
                                    case "Go to Last Page":
                                        b.ToolTipText = "�� ��������� ��������";
                                        break;
                                    case "Goto Page":
                                        b.ToolTipText = "������� �� ��������";
                                        break;
                                    case "Close Current View":
                                        b.ToolTipText = "������� �����";
                                        break;
                                    case "Print Report":
                                        // ����� ������ ��� ������
                                        index = tb.Items.IndexOf(b);
                                        imageIndex = b.ImageIndex;
                                        break;
                                    case "Refresh":
                                        b.ToolTipText = "��������";
                                        break;
                                    case "Export Report":
                                        b.ToolTipText = "�������������� � ����";
                                        break;
                                    case "Toggle Group Tree":
                                        b.ToolTipText = "������";
                                        break;
                                    case "Zoom":
                                        b.ToolTipText = "�������";
                                        break;
                                    case "Search Text":
                                        b.ToolTipText = "�����";
                                        break;
                                }
                            }

                            if (index != -1)
                            {
                                // ������ ������ ��� ������ �������
                                ToolStripButton printButton = new ToolStripButton();
                                printButton.ToolTipText = "�������� �����";
                                printButton.ImageIndex = imageIndex;
                                // ��� ����� 1 - ������� �� ������ ��������
                                printButton.Tag = 1;
                                //printButton.Click += new EventHandler(tb_ButtonClick); 
                                // ������� ������ ������
                                tb.Items.RemoveAt(index);
                                // ��������� �� ����� ����� ������
                                tb.Items.Insert(index, printButton);
                            }
                        }
                        break;

				}

			}
		}

		#endregion // SetControls()

		private void PrintPreviewrForm_Load(object sender, EventArgs e)
		{
			try
			{
//				if (this.rc == null)
//				{
//					Type t = Type.GetType("FORIS.TSS.TransportDispatcher." + reportName);
//					ReportClass source = (ReportClass)t.GetConstructor(new Type[]{}).Invoke(new object[]{});
//					source.SetDataSource(this.DataSet);
//					source.PrintOptions.PaperOrientation = this.PaperOrientation;
//					rc = source;
//				}
				this.crystalReportViewer1.ReportSource = rc;
				SetControls();
			}
			catch(Exception exception)
			{
				Trace.WriteLine(exception.Message);
				throw exception;
			}
		}

		/// <summary>
		/// ������ �������
		/// </summary>
		/// <remarks>������� ������������ ��-���������, ���������� 1 ����� ���� ������� ������.</remarks>
		public void PrintReport()
		{
			if(!this.report.BeforePrint()) 
			{
				return;
			}
			
			rc.PrintToPrinter(1, false, 0, 0);

			//this.report.OnPrinted(Client.Instance.CurrentSessionID); //CurrentUserID);
		}

		/// <summary>
		/// ������ ������
		/// </summary>
		/// <param name="printerName">�������� ��������</param>
		/// <param name="copies">���-�� ����� ������</param>
		/// <param name="startPage">�������� �� �������� ...</param>
		/// <param name="endPage">... �� ��������</param>
		public void PrintReport(string printerName, int copies, int startPage, int endPage)
		{
			if(!this.report.BeforePrint())
			{
				return;
			}

			rc.PrintOptions.PrinterName = printerName;
			rc.PrintToPrinter(copies, false, startPage, endPage);

			//this.report.OnPrinted(Client.Instance.CurrentSessionID); //CurrentUserID);
			
			printed = true;
			
			this.Close();
		}

		/// <summary>
        /// ��������� ������� ������ ������ ������������ ��� ToolBar
		/// </summary>
		/// <param name="sender">������ �� ������ ������������</param>
		/// <param name="e">��������� �������</param>
		private void tb_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
		{
			if(e.Button.ToolTipText == "�������� �����")
			{
				// ���� ������ ����������, ����� ����������� ���������� ������������� ��,
				// �� ��� ���� ��� ����������� ����������� ��������� ������� �� �����.
				// ������� ��� �� ��������� ���� ������, 
				// � ��������� ������ �������� ����������, ����� crystalReportViewer1
				Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.report.GetType(), typeof(GuidAttribute));
				if( ((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ("73157D33-19AC-4d05-A9E4-10E48F32E4AB").ToUpper() )
				{
					using(PrintDialog pd = new PrintDialog())
					{
						pd.Document = new PrintDocument();
						//��������� ����������� ������ ���������� �������.
						pd.AllowSomePages = true;

						if(pd.ShowDialog() == DialogResult.OK)
						{
							// ������ ������
							PrintReport(pd.PrinterSettings.PrinterName, pd.PrinterSettings.Copies, pd.PrinterSettings.FromPage, pd.PrinterSettings.ToPage);
						}
					}
				}
				else
				{
					this.crystalReportViewer1.PrintReport();
					this.Close();
				}

			}
		}

        /// <summary>
        /// ��������� ������� ������ ������ ������������ ��� ToolStrip
        /// </summary>
        /// <param name="sender">������ �� ������ ������������</param> 
        /// <param name="e">��������� �������</param>
        private void tb_ButtonClick(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.ToolTipText == "�������� �����")
            {
                // ���� ������ ����������, ����� ����������� ���������� ������������� ��,
                // �� ��� ���� ��� ����������� ����������� ��������� ������� �� �����.
                // ������� ��� �� ��������� ���� ������, 
                // � ��������� ������ �������� ����������, ����� crystalReportViewer1
                Attribute AttributeReportGUID = Attribute.GetCustomAttribute(this.report.GetType(), typeof(GuidAttribute));
                if (((GuidAttribute)AttributeReportGUID).Value.ToUpper() == ("73157D33-19AC-4d05-A9E4-10E48F32E4AB").ToUpper())
                {
                    using (PrintDialog pd = new PrintDialog())
                    {
                        pd.Document = new PrintDocument();
                        //��������� ����������� ������ ���������� �������.
                        pd.AllowSomePages = true;

                        if (pd.ShowDialog() == DialogResult.OK)
                        {
                            // ������ ������
                            PrintReport(pd.PrinterSettings.PrinterName, pd.PrinterSettings.Copies, pd.PrinterSettings.FromPage, pd.PrinterSettings.ToPage);
                        }
                    }
                }
                else
                {
                    this.crystalReportViewer1.PrintReport();
                    this.Close();
                }

            }
        }

		/// <summary>
		/// ���������� ������� �������� ����� �������
		/// </summary>
		/// <param name="sender">������ �� �����</param>
		/// <param name="e">��������� �������</param>
		private void PrintPreviewForm_Closing(object sender, CancelEventArgs e)
		{
			this.crystalReportViewer1.ReportSource = null;
			this.rc = null;
		}

		/// <summary>
		/// ���������� ��������� �� ������
		/// </summary>
		/// <param name="source">������ �� �������� ������</param>
		/// <param name="e">��������� �������</param>
		private void crystalReportViewer1_Navigate(object source, CrystalDecisions.Windows.Forms.NavigateEventArgs e)
		{
			CrystalDecisions.Windows.Forms.PageView pv=source as CrystalDecisions.Windows.Forms.PageView;
			if (pv.ReportDocumentManager!=null) 
			{
				SupressToolTips(pv, e.NewPageNumber);
			}
		}

		/// <summary>
		/// ��������� ��������� �� �������� ������
		/// </summary>
		/// <param name="pv">������ �� �������� ������</param>
		/// <param name="pageN">����� ��������</param>
		/// <remarks>������������ ��� ����� �������� �������������� ���������� � ������������ ������.
		/// ��� ���� � rsdn.ru.</remarks>
		public static void SupressToolTips(CrystalDecisions.Windows.Forms.PageView pv, int pageN) 
		{
			CrystalDecisions.Windows.Forms.ReportDocumentBase dc=pv.GetActiveDocument();
			CrystalDecisions.CrystalReports.ViewerObjectModel.PageObject po = dc.GetPage(pageN);
			foreach(CrystalDecisions.CrystalReports.ViewerObjectModel.SectionInstance si in po.SectionInstances) 
			{
				foreach(CrystalDecisions.CrystalReports.ViewerObjectModel.ReportObjectInstance ro in si.ReportObjectInstances) 
				{
					ro.ToolTip="\0x13\0x8";
				}
			}
		}
	}
}
