namespace Report
{
    
    
    /// <summary>
    /// <P>maps project name to project default namespace
    /// </summary>
    public class ProjectDefaultNamespace
    {
        
        /// <summary>
        /// Name
        /// </summary>
        public static string Name
        {
            get
            {
                return "FORIS.TSS.Reports";
            }
        }
    }
}
namespace FORIS.TSS.Report.Base
{
    
    
    /// <summary>
    /// <P>This class provides checked access to string resources</P>
    /// <P>This file is autogenerated from<BR/>
    /// C:\VSS_TELESIS\FORIS OSS\TSS\1.3\FORIS.TSS.Reports\Report\Strings.resx</P>
    /// </summary>
    /// <remarks>
    /// <P>It allows compile time checking for presence of resource strings.</P>
    /// <P>Don't change anything manually.
    /// The contents of file will be automatically regenerated
    /// by StringResourceGenerator tool. All changes will be lost.</P>
    /// </remarks>
    public class Strings
    {
        
        private static System.Resources.ResourceManager ResourceManager = new System.Resources.ResourceManager("FORIS.TSS.Reports.Strings", System.Reflection.Assembly.GetExecutingAssembly());
        
        /// <summary>
        /// ������ �����
        /// </summary>
        public static string EmpryReportDN
        {
            get
            {
                return ResourceManager.GetString("EmpryReportDN");
            }
        }
        
        /// <summary>
        /// ��������� ������
        /// </summary>
        public static string ReportParsCat
        {
            get
            {
                return ResourceManager.GetString("ReportParsCat");
            }
        }
        
        /// <summary>
        /// ���������� ���������� ������ ����� ������ ��� ���������� �����, ����������� �������.
        /// </summary>
        public static string EmpryReportDesc
        {
            get
            {
                return ResourceManager.GetString("EmpryReportDesc");
            }
        }
        
        /// <summary>
        /// ����
        /// </summary>
        public static string DateDN
        {
            get
            {
                return ResourceManager.GetString("DateDN");
            }
        }
        
        /// <summary>
        /// ����, �� ������� ����������� �����
        /// </summary>
        public static string DateDesc
        {
            get
            {
                return ResourceManager.GetString("DateDesc");
            }
        }
        
        /// <summary>
        /// ���� ������ �������
        /// </summary>
        public static string PeriodBeginDN
        {
            get
            {
                return ResourceManager.GetString("PeriodBeginDN");
            }
        }
        
        /// <summary>
        /// ���� ������ �������, �� ������� ����������� �����
        /// </summary>
        public static string PeriodBeginDesc
        {
            get
            {
                return ResourceManager.GetString("PeriodBeginDesc");
            }
        }
        
        /// <summary>
        /// ���� ��������� �������
        /// </summary>
        public static string PeriodEndDN
        {
            get
            {
                return ResourceManager.GetString("PeriodEndDN");
            }
        }
        
        /// <summary>
        /// ���� ��������� �������, �� ������� ����������� �����
        /// </summary>
        public static string PeriodEndDesc
        {
            get
            {
                return ResourceManager.GetString("PeriodEndDesc");
            }
        }
    }
}
