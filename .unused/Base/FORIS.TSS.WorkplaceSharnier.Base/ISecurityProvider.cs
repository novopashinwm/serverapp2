﻿using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;

namespace FORIS.TSS.WorkplaceSharnier
{
	public interface ISecurityProvider
	{
		ICollectionWithEvents<TssPrincipalInfo> Principals { get; }
	}
}