﻿using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.WorkplaceSharnier.Data
{
	public abstract class ClientDataSupplierFactory<TClient, TClientData, TParams> :
		Component
		where TClient : IClientDataProvider
		where TParams : DataParams
		where TClientData : FORIS.TSS.Helpers.Data.Data
	{
		#region Constructor & Dispose

		public ClientDataSupplierFactory(TClient client)
		{
			this.client = client;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				lock (this.hash)
				{
					foreach (WeakReference<TClientData> reference in this.hash.Values)
					{
						TClientData dataHolder = reference.Target;

						if (dataHolder != null)
						{
							dataHolder.Dispose();
						}
					}

					this.hash.Clear();
				}
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		private readonly Dictionary<TParams, WeakReference<TClientData>> hash =
			new Dictionary<TParams, WeakReference<TClientData>>();

		private readonly TClient client;

		public TClientData this[TParams @params]
		{
			get
			{
				lock (this.hash)
				{
					TClientData dataHolder = default(TClientData);

					if (this.hash.ContainsKey(@params))
					{
						dataHolder = this.hash[@params].Target;
					}

					if (object.Equals(dataHolder, default(TClientData)))
					{
						dataHolder = this.CreateDataSupplier(@params);

						if (this.hash.ContainsKey(@params))
						{
							this.hash.Remove(@params);
						}

						this.hash.Add(
							@params,
							new WeakReference<TClientData>(
								dataHolder
								)
							);
					}

					return dataHolder;
				}
			}
		}

		public TClient Client
		{
			get { return this.client; }
		}

		protected abstract TClientData CreateDataSupplier(TParams @params);
	}
}