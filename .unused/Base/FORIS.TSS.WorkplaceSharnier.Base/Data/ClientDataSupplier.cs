﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.WorkplaceSharnier.Data
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TClientProvider"></typeparam>
	/// <typeparam name="TDataSupplier"></typeparam>
	/// <typeparam name="TDataTreater"></typeparam>
	/// <remarks>
	/// Потребителем этого поставщика данных является
	/// контейнер хэша данных клиента, управляемый
	/// фабрикой контейнеров данных клиента.
	/// Поставщиком является прокси-объект 
	/// персонального поставщика данных на сервере
	/// </remarks>
	public class ClientDataSupplier<TClientProvider, TDataSupplier, TDataTreater> :
		DataSupplierWrapper<TDataSupplier, TDataTreater>
		where TDataSupplier : IDataSupplier<TDataTreater>
		where TDataTreater : IDataTreater
		where TClientProvider : IClientDataProvider
	{
		#region Constructor & Dispose

		public ClientDataSupplier(TClientProvider clientProvider, TDataSupplier dataSupplier)
			: base(dataSupplier)
		{
			#region Preconditions

			if (!dataSupplier.GetType().IsMarshalByRef)
			{
				throw new InvalidOperationException(
					"Only remote object can be supplier for client data"
					);
			}

			#endregion // Preconditions

			this.clientProvider = clientProvider;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{

			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		private readonly TClientProvider clientProvider;

		public TClientProvider ClientProvider
		{
			get { return this.clientProvider; }
		}
	}
}