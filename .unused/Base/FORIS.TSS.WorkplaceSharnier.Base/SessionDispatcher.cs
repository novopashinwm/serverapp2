﻿using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;

namespace FORIS.TSS.WorkplaceSharnier
{
	public class SessionDispatcher<TPersonalServer> :
		Dispatcher<ISessionItem<TPersonalServer>>,
		ISessionItem<TPersonalServer>
		where TPersonalServer : IPersonalServer
	{
		#region ISessionItem<TPersonalServer> Members

		private TPersonalServer session;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TPersonalServer Session
		{
			get { return this.session; }
			set
			{
				if (!object.Equals(this.session, value))
				{
					this.OnBeforeChange();

					if (!object.Equals(value, default(TPersonalServer)))
					{
						this.session = value;

						for (int i = 0; i < this.Ambassadors.Count; i++)
						{
							Ambassador<ISessionItem<TPersonalServer>> ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Session = value;
						}
					}
					else
					{
						for (int i = this.Ambassadors.Count - 1; i >= 0; i--)
						{
							Ambassador<ISessionItem<TPersonalServer>> ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Session = value;
						}

						this.session = value;
					}

					this.OnAfterChange();

					/* Вполне вероятно, что коллекция Ambassadors 
					 * изменится после обработки OnAfterChange()
					 */
				}
			}
		}

		#endregion // ISessionItem<TPersonalServer> Members

		#region Ambasadors

		private readonly SessionAmbassadorCollection<TPersonalServer> ambassadors =
			new SessionAmbassadorCollection<TPersonalServer>();

		public SessionAmbassadorCollection<TPersonalServer> Ambassadors
		{
			get { return (SessionAmbassadorCollection<TPersonalServer>)this.GetAmbassadorCollection(); }
		}

		protected override AmbassadorCollection<ISessionItem<TPersonalServer>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors

		protected override void OnBeforeSetItem(Ambassador<ISessionItem<TPersonalServer>> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Session = default(TPersonalServer);
			}
		}
		protected override void OnAfterSetItem(Ambassador<ISessionItem<TPersonalServer>> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Session = this.session;
			}
		}
	}

	public class SessionAmbassadorCollection<TPersonalServer> :
		AmbassadorCollection<ISessionItem<TPersonalServer>>
		where TPersonalServer : IPersonalServer
	{
		public new SessionAmbassador<TPersonalServer> this[int index]
		{
			get { return (SessionAmbassador<TPersonalServer>)base[index]; }
		}
	}

	public class SessionAmbassador<TPersonalServer> :
		Ambassador<ISessionItem<TPersonalServer>>
		where TPersonalServer : IPersonalServer
	{
		public SessionAmbassador(ISessionItem<TPersonalServer> item)
			: base(item)
		{

		}
	}

	public interface ISessionItem<TPersonalServer>
		where TPersonalServer : IPersonalServer
	{
		TPersonalServer Session { get; set; }
	}
}