﻿using FORIS.TSS.Helpers.Security;

namespace FORIS.TSS.WorkplaceSharnier
{
	public interface ISecurityEditor
	{
		void Edit(ISecureObject secureObject);
	}
}