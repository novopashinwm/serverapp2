﻿using System.ComponentModel;
using FORIS.TSS.Common;

namespace FORIS.TSS.WorkplaceSharnier
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TClient"></typeparam>
	/// <remarks>
	/// Этот компонент не годится для использования в дизайнере
	/// Для этих целей от него необходимо наследоваться
	/// </remarks>
	public class ClientDispatcher<TClient> :
		Dispatcher<IClientItem<TClient>>,
		IClientItem<TClient>
		where TClient : IClient
	{
		#region Constructor & Dispose

		public ClientDispatcher(IContainer container) : this()
		{
			container.Add(this);
		}
		public ClientDispatcher()
		{

		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Client = default(TClient);
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (TClient)value; }
		}

		#endregion // ICLientItem Members

		#region IClientItem<TPersonalServer> Members

		private TClient client;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TClient Client
		{
			get { return this.client; }
			set
			{
				if (!object.Equals(this.client, value))
				{
					this.OnBeforeChange();

					if (!object.Equals(value, default(TClient)))
					{
						this.client = value;

						for (int i = 0; i < this.Ambassadors.Count; i++)
						{
							Ambassador<IClientItem<TClient>> ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Client = value;
						}
					}
					else
					{
						for (int i = this.Ambassadors.Count - 1; i >= 0; i--)
						{
							Ambassador<IClientItem<TClient>> ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Client = value;
						}

						this.client = value;
					}

					this.OnAfterChange();

					/* Вполне вероятно, что коллекция Ambassadors 
					 * изменится после обработки OnAfterChange()
					 */
				}
			}
		}

		#endregion // IClientItem<TPersonalServer> Members

		#region Ambassadors

		private readonly AmbassadorCollection<IClientItem<TClient>> ambassadors =
			new AmbassadorCollection<IClientItem<TClient>>();

		protected override AmbassadorCollection<IClientItem<TClient>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		public AmbassadorCollection<IClientItem<TClient>> Ambassadors
		{
			get { return this.GetAmbassadorCollection(); }
		}

		#endregion // Ambassadors

		protected override void OnBeforeSetItem(Ambassador<IClientItem<TClient>> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Client = default(TClient);
			}
		}
		protected override void OnAfterSetItem(Ambassador<IClientItem<TClient>> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Client = this.client;
			}
		}
	}

	public class ClientAmbassadorCollection<TClient> :
		AmbassadorCollection<IClientItem<TClient>>
		where TClient : IClient
	{
		public new ClientAmbassador<TClient> this[int index]
		{
			get { return (ClientAmbassador<TClient>)base[index]; }
		}
	}

	public class ClientAmbassador<TClient> :
		Ambassador<IClientItem<TClient>>
		where TClient : IClient
	{

	}

	public class ClientDispatcher :
		Dispatcher<IClientItem>,
		IClientItem
	{
		#region IClientItem Members

		private IClient client;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IClient Client
		{
			get { return this.client; }
			set
			{
				if (!object.Equals(this.client, value))
				{
					this.OnBeforeChange();

					if (value != null)
					{
						this.client = value;

						for (int i = 0; i < this.Ambassadors.Count; i++)
						{
							ClientAmbassador ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Client = value;
						}
					}
					else
					{
						for (int i = this.Ambassadors.Count - 1; i >= 0; i--)
						{
							ClientAmbassador ambassador = this.Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Client = value;
						}

						this.client = value;
					}

					this.OnAfterChange();

					/* Вполне вероятно, что коллекция Ambassadors 
					 * изменится после обработки OnAfterChange()
					 */
				}
			}
		}

		#endregion // IClientItem Members

		#region Ambassadors

		private readonly ClientAmbassadorCollection ambassadors =
			new ClientAmbassadorCollection();

		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ClientAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors

		protected override void OnBeforeSetItem(Ambassador<IClientItem> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Client = null;
			}
		}

		protected override void OnAfterSetItem(Ambassador<IClientItem> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Client = this.client;
			}
		}
	}

	public class ClientAmbassadorCollection : AmbassadorCollection<IClientItem>
	{
		public new ClientAmbassador this[int index]
		{
			get { return (ClientAmbassador)base[index]; }
		}
	}

	public class ClientAmbassador : Ambassador<IClientItem>
	{

	}

	public interface IClientItem<TClient> :
		IClientItem
		where TClient : IClient
	{
		new TClient Client { get; set; }
	}

	public interface IClientItem
	{
		IClient Client { get; set; }
	}
}