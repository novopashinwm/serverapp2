﻿using FORIS.TSS.EntityModel.History;

namespace FORIS.TSS.EntityModel.Result
{
    public class MessageCompare
    {
        public H_MESSAGE Update;

        public H_MESSAGE Previous;

        public MESSAGE Actual;
    }
}
