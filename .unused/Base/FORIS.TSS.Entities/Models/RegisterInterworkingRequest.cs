﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel.Models
{
    public class RegisterInterworkingRequest
    {
        public Interworking.Result Result;

        public VEHICLE Vehicle;
    }
}
