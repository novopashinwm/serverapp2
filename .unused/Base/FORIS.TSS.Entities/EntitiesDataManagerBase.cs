﻿using System;

namespace FORIS.TSS.EntityModel
{
    public abstract class EntitiesDataManagerBase
    {
        private readonly Entities _context;
        private readonly OPERATOR _operator;

        protected Entities Context
        {
            get
            {
                return _context;
            }
        }

        protected EntitiesDataManagerBase(
            Entities context,
            int operatorID)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;

            _operator = context.GetOperator(operatorID);
        }

        public void SaveChanges()
        {
            _context.SaveChangesByOperator(_operator != null ? _operator.OPERATOR_ID : (int?)null);
        }
    }
}
