﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.EntityModel
{
	public static class CommandQueryableExtensions
	{
		public static IQueryable<Command> FilterByType(this IQueryable<Command> commands, CmdType type)
		{
			return commands.Where(c => c.Type_ID == (int)type);
		}

		public static IQueryable<Command> FilterByID(this IQueryable<Command> commands, IdType idType, int id)
		{
			switch (idType)
			{
				case IdType.Operator:
					return commands.Where(c => c.Sender_ID == id);
				case IdType.OperatorGroup:
					return (from c in commands
							from og in c.Sender.ContainingGroups
							where og.OPERATORGROUP_ID == id
							group c by c into c
							select c.Key);
				case IdType.Vehicle:
					return (from c in commands
							where c.Target_ID == id
							select c);
				case IdType.VehicleGroup:
					return (from c in commands
							from vg in c.Target.VEHICLEGROUP
							where vg.VEHICLEGROUP_ID == id
							group c by c into c
							select c.Key);
				case IdType.Department:
					return (from c in commands
							where c.Target.Department_ID == id
							select c);
				default:
					throw new ArgumentOutOfRangeException("idType", idType, @"Value is not supported");
			}
		}

		public static IQueryable<Command> FilterByDate(this IQueryable<Command> commands, DateTime dateFrom, DateTime dateTo, TimeZoneInfo tz)
		{
			dateFrom = TimeZoneInfo.ConvertTimeToUtc(dateFrom, tz);
			dateTo = TimeZoneInfo.ConvertTimeToUtc(dateTo, tz);

			return (from c in commands
					where
						c.Date_Received >= dateFrom &&
						c.Date_Received <= dateTo
					select c);
		}
	}
}