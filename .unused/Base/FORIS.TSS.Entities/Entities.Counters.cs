﻿using System.Linq;
using FORIS.TSS.BusinessLogic.Counter;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{

		/// <summary>
		/// Получает или создает новый счетчик
		/// </summary>
		/// <param name="counterTypeEnum"></param>
		/// <param name="fromContactId">контакт 1</param>
		/// <param name="toContactId">контакт 2</param>
		/// <returns></returns>
		public ContactCounter GetCounter(BusinessLogic.Enums.CounterType counterTypeEnum, int fromContactId, int toContactId)
		{
			var counterKey = Counters.GetCounter(BusinessLogic.Enums.CounterType.Month);
			var currentKey = counterKey.GetCurrentKey();
			var counter = ContactCounter.FirstOrDefault(c => c.CounterType.Value == currentKey
															 && c.From_Id == fromContactId
															 && c.To_Id == toContactId);
			var counterType = CounterType.FirstOrDefault(ct => ct.Value == currentKey && ct.Type == (int)counterTypeEnum);
			if (counterType == null)
			{
				counterType = new CounterType
				{
					Value = currentKey,
					Type = (byte)BusinessLogic.Enums.CounterType.Month
				};
				CounterType.AddObject(counterType);
			}

			if (counter == null)
			{
				counter = new ContactCounter
				{
					From_Id = fromContactId,
					To_Id = toContactId,
					CounterType = counterType,
				};

				ContactCounter.AddObject(counter);
			}

			return counter;
		}
	}
}