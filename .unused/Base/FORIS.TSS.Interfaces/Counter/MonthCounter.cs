﻿using System;
using System.Globalization;

namespace FORIS.TSS.BusinessLogic.Counter
{
    public class MonthCounter : ICounter
    {
        public string GetCurrentKey()
        {
            return GetKey(DateTime.UtcNow);
        }

        public string GetKey(DateTime date)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}{1}", (date.Year - 2000).ToString("00"), date.Month.ToString("00"));
        }
    }
}
