﻿namespace FORIS.TSS.BusinessLogic.Counter
{
    public interface ICounter
    {
        string GetCurrentKey();
    }
}
