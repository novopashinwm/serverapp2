﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.Counter
{
	public sealed class Counters
	{
		public static ICounter GetCounter(CounterType type)
		{
			switch (type)
			{
				case CounterType.Month:
					return new MonthCounter();
			}
			throw new NotImplementedException("type");
		}
	}
}