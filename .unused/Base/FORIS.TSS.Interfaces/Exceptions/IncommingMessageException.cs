﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
    [Serializable]
    public class IncommingMessageException : Exception
    {
        public IncommingMessageException(string message) : base(message)
        {
            
        }

        public IncommingMessageException(string message, Exception ex) : base(message, ex)
        {
            
        }
    }
}
