using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace FORIS.TSS.Common.PropertyGrid.Converters
{
	/// <summary>
	/// Summary description for TimeSpanFormatConverter.
	/// </summary>
	public class TimeSpanFormatConverter : TypeConverter
	{
		public TimeSpanFormatConverter()
		{
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			TimeSpan curTime = TimeSpan.FromSeconds(0);
			try
			{
				string val = value as String;
				val = val + ":00";
				curTime = TimeSpan.Parse(val);
			}
			catch(FormatException)
			{
				MessageBox.Show("����� ������� �������. ������ ����������� ��������: " + ((int)curTime.TotalHours).ToString("00") + ":" + curTime.Minutes.ToString("00"));
				return curTime;
			}
			return curTime;
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			TimeSpan curTime = TimeSpan.FromSeconds(0);

			if(value is TimeSpan)
				curTime = (TimeSpan)value;

			return ((int)curTime.TotalHours).ToString("00") + ":" + curTime.Minutes.ToString("00");
		}
	}
}
