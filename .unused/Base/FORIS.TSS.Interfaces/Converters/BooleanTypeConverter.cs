using System;
using System.ComponentModel;
using System.Globalization;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// 
	/// </summary>
	public class BooleanTypeConverter : BooleanConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture,
			object value, Type destType)
		{
			return (bool)value ? "��" : "���";
//			LocalizationManager.Instance.GetResourceString("BooleanTrue") : LocalizationManager.Instance.GetResourceString("BooleanFalse");
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture,
			object value)
		{
			return (string)value == "��"; //LocalizationManager.Instance.GetResourceString("BooleanTrue");
		}
	}
}
