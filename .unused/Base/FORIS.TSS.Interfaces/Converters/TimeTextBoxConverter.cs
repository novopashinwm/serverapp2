using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace FORIS.TSS.Common.PropertyGrid.Converters
{
	/// <summary>
	/// ����� ��� ��������������� ���� � ������ ������� HH:mm
	/// </summary>
	public class TimeTextBoxConverter : TypeConverter
	{
		/// <summary>
		/// ��������� ������ ��� ��������������� ���� � ������ ������� HH:mm
		/// </summary>
		public TimeTextBoxConverter()
		{
		}

//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="context"></param>
//		/// <param name="sourceType"></param>
//		/// <returns></returns>
//		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//		{
//			if(sourceType.Equals(typeof(String)))
//				return true;
//
//			return base.CanConvertFrom(context, sourceType);
//		}
//
//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="context"></param>
//		/// <param name="destinationType"></param>
//		/// <returns></returns>
//		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//		{
//			if(destinationType.Equals(typeof(DateTime)))
//				return true;
//
//			return base.CanConvertTo(context, destinationType);
//		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			DateTime dateTime = DateTime.Now; 
			try
			{
				dateTime = DateTime.Parse(value as String);
			}
			catch(FormatException)
			{
				MessageBox.Show("����� ������� �������. ������ ����������� ��������: " + DateTime.Now.ToString("HH:mm"));
				return DateTime.Now;
			}
			return dateTime;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			DateTime dateTime = DateTime.Now;

			if(value is DateTime)
				dateTime = (DateTime)value;

			return dateTime.ToString("HH:mm");

			//return base.ConvertTo(context, culture, value, destinationType);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}