using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Resources;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// TypeConverter ��� ������, ����������������� ����� � ������ � 
	/// ������ �������� DisplayName
	/// </summary>
	public class EnumTypeConverter :  EnumConverter
	{
		private Type _EnumType;

		static Hashtable rms = new Hashtable();

		/// <summary>
		/// �������������� ���������
		/// </summary>
		/// <param name="type">��� �����</param>
		public EnumTypeConverter(Type type) : base(type)
		{
			_EnumType = type;
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
		{
			return destType == typeof (string);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture,
			object value, Type destType)
		{
			FieldInfo fi = _EnumType.GetField(Enum.GetName(_EnumType, value));
			
			/*FieldInfo fi = null;
			FieldInfo[] fis = _EnumType.GetFields();
			foreach (FieldInfo _fi in fis) //_EnumType.GetFields())
			{ 
				if((string)value == _fi.Name)
					fi = _fi;
			}*/

			DisplayNameAttribute dna = 
				(DisplayNameAttribute)Attribute.GetCustomAttribute(fi, typeof (DisplayNameAttribute));

			if (dna != null)
			{
				//���� ������� ��������� ���� ������� �������������� �������
				if(dna.HasResource)
				{
					ResourceManager rm;
					string nameSpace = _EnumType.Namespace + ".Strings";

					if (rms.ContainsKey(_EnumType.Assembly.FullName))
						rm = (ResourceManager)rms[_EnumType.Assembly.FullName];
					else
					{
						rm = new ResourceManager(nameSpace, _EnumType.Assembly);
						rms.Add(/*nameSpace*/_EnumType.Assembly.FullName, rm);
					}

					return rm.GetString(dna.DisplayName);
				}//
				return dna.DisplayName;
			}
			else
				return value.ToString();
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type srcType)
		{
			return srcType == typeof (string);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture,
			object value)
		{
			
			FieldInfo[] fis = _EnumType.GetFields();
			foreach (FieldInfo fi in fis) 
			{ 
				DisplayNameAttribute dna = 
					(DisplayNameAttribute)Attribute.GetCustomAttribute(fi, typeof(DisplayNameAttribute));

				if (dna != null)
				{
					//���� ������� ��������� ���� ������� �������������� �������
					if(dna.HasResource)
					{
						ResourceManager rm;
						string nameSpace = _EnumType.Namespace + ".Strings";

						if (rms.ContainsKey(_EnumType.Assembly.FullName))
							rm = (ResourceManager)rms[_EnumType.Assembly.FullName];
						else
						{
							rm = new ResourceManager(nameSpace, _EnumType.Assembly);
							rms.Add(/*nameSpace*/_EnumType.Assembly.FullName, rm);
						}

						if ((string)value == rm.GetString(dna.DisplayName))
							return Enum.Parse(_EnumType, fi.Name);
					}
					else if((string)value == dna.DisplayName)
					{
						return Enum.Parse(_EnumType, fi.Name);
					}
				}
			}
			return Enum.Parse(_EnumType, (string)value);
		}

	}
}
