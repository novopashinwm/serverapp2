using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace FORIS.TSS.Common.PropertyGrid.Converters
{
	/// <summary>
	/// Summary description for DateTimeFormatConverter.
	/// </summary>
	public class DateTimeFormatConverter : TypeConverter
	{
		public DateTimeFormatConverter()
		{
		}

//		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//		{
//			DateTime dateTime = DateTime.Now; 
//			try
//			{
//				dateTime = DateTime.Parse(value as String);
//			}
//			catch(FormatException)
//			{
//				MessageBox.Show("����� ������� �������. ������ ����������� ��������: " + DateTime.Now.ToString("HH:mm"));
//				return DateTime.Now;
//			}
//			return dateTime;
//		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			DateTime dateTime = DateTime.Now;

			if(value is DateTime)
				dateTime = (DateTime)value;

			return dateTime.ToString("HH:mm");

			//return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
