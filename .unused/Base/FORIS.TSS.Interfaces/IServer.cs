﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Базовый интерфейс для всех типов серверов </summary>
	public interface IServer
	{
		ServerState State { get;}
		void Start();
		void Stop();
		void SetSessionManager(object sessionManager);
		event ServerExitEventHandler Exit;
	}
	[Serializable]
	public enum ServerState
	{
		Undefined,
		Starting,
		Running,
		Stopping,
		Stopped
	}
	public delegate void ServerExitEventHandler(string message);
}