﻿using System;

namespace FORIS.TSS.BusinessLogic.Security
{
	/// <summary> Описание защищаемого объекта </summary>
	/// <remarks>
	/// Класс предназначен для пересечения границ приложений.
	/// Он не содержит сам защищаемый объект, и не содержит
	/// никакой информации, доступ к которой должен быть ограничен
	/// </remarks>
	[Serializable]
	public class SecureObjectInfo
	{
		private readonly string name;
		public string Name
		{
			get { return name; }
		}
		private readonly string tableName;
		public string TableName
		{
			get { return tableName; }
		}
		private readonly int id;
		public int Id
		{
			get { return id; }
		}
		public SecureObjectInfo(string name, string tableName, int id)
		{
			this.name      = name;
			this.tableName = tableName;
			this.id        = id;
		}
	}
}