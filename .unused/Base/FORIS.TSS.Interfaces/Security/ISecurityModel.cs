﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Security
{
	public interface ISecurityModel
	{
		SecureObjectInfo Info { get; }
		Dictionary<TssPrincipalInfo, AccessControlInfo> Acl { get; }
		AccessRight[] AccessRightRights { get; }
		AccessControlInfo DefaultAccessControl { get; }
	}
	[Serializable]
	public struct AccessRight
	{
		private readonly int mask;
		public int Mask
		{
			get { return mask; }
		}
		private readonly string name;
		public string Name
		{
			get { return name; }
		}
		public AccessRight(int mask, string name)
		{
			this.mask = mask;
			this.name = name;
		}
	}
}