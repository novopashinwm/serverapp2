﻿using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Security
{
	/// <summary> Это может быть как оператор, так и группа операторов </summary>
	public interface ITssPrincipal
	{
		TssPrincipalInfo Info { get; }
		ICollection<TssPrincipalInfo> Parents { get; }
	}
}