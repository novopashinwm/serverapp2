using System;

namespace FORIS.TSS.BusinessLogic.Security
{
	[Serializable]
	public class AccessControlInfo
	{
		private int allowMask;

		public int AllowMask
		{
			get { return this.allowMask; }
			set
			{
				if( this.readOnly )
				{
					throw new ApplicationException();
				}

				this.allowMask = value;
			}
		}

		private int denyMask;

		public int DenyMask
		{
			get { return this.denyMask; }
			set
			{
				if( this.readOnly )
				{
					throw new ApplicationException();
				}

				this.denyMask = value;
			}
		}

		private readonly int inheritedAllowMask;
		
		public int InheritedAllowMask
		{
			get { return this.inheritedAllowMask; }
		}

		private readonly int inheritedDenyMask;

		public int InheritedDenyMask
		{
			get { return this.inheritedDenyMask; }					
		}

		private readonly bool readOnly;

		public bool ReadOnly
		{
			get { return this.readOnly; }
		}

		public bool IsEmpty
		{
			get { return this.allowMask == 0 && this.denyMask == 0; }
		}

		public AccessControlInfo(
			int allowMask,
			int denyMask,
			bool readOnly
			)
		{
			this.allowMask = allowMask;
			this.denyMask = denyMask;
			this.readOnly = readOnly;
			this.inheritedAllowMask = 0;
			this.inheritedDenyMask = 0;
		}

		public AccessControlInfo(
			AccessControlInfo accessControl,
			int inheritedAllowMask,
			int inheritedDenyMask
			)
		{
			this.allowMask = accessControl.allowMask;
			this.denyMask = accessControl.denyMask;
			this.readOnly = accessControl.readOnly;
			this.inheritedAllowMask = inheritedAllowMask;
			this.inheritedDenyMask = inheritedDenyMask;
		}

		#region Empty

		private static readonly AccessControlInfo s_empty =
			new AccessControlInfo( 0, 0, false );

		public static AccessControlInfo Empty
		{
			get { return AccessControlInfo.s_empty; }
		}

		#endregion Empty
	}
}