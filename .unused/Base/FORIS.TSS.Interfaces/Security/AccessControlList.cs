using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Security
{
	[Serializable]
	public class AccessControlList: 
		ISecurityModel
	{
		private readonly SecureObjectInfo info;

		public SecureObjectInfo Info
		{
			get { return this.info; }
		}

		private readonly Dictionary<TssPrincipalInfo, AccessControlInfo> acl;

		public Dictionary<TssPrincipalInfo, AccessControlInfo> Acl
		{
			get { return this.acl; }
		}

		private readonly AccessRight[] accessRightRights;

		public AccessRight[] AccessRightRights
		{
			get { return this.accessRightRights; }
		}

		private readonly AccessControlInfo defaultAccessControl;

		public AccessControlInfo DefaultAccessControl
		{
			get { return this.defaultAccessControl; }
		}

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="info"></param>
		/// <param name="acl"></param>
		/// <param name="accessRightRights">�������� ����� ���� �������</param>
		/// <param name="defaultAccessControl">
		/// �����, ��������������� ����� ����������� � ������ 
		/// �������� ������� � ������� ����������������
		/// </param>
		public AccessControlList(
			SecureObjectInfo info,
			Dictionary<TssPrincipalInfo, AccessControlInfo> acl,
			AccessRight[] accessRightRights,
			AccessControlInfo defaultAccessControl
			)
		{
			this.info = info;
			this.acl = acl;
			this.accessRightRights = accessRightRights;
			this.defaultAccessControl = defaultAccessControl;
		}
	}
}
