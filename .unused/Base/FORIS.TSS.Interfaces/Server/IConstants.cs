using System;

namespace FORIS.TSS.BusinessLogic.Server
{
	public interface IConstants
	{
		IConstant<int>      Version                { get; }
		IConstant<string>   ApplicationName        { get; }
		IConstant<string>   EstablishmentName      { get; }
		IConstant<string>   EstablishmentAddress   { get; }
		IConstant<string>   EstablishmentPhone     { get; }
		IConstant<string>   ReportCreatorSignature { get; }
	}
}