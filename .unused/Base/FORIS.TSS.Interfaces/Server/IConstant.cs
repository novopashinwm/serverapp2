using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.Server
{
	public interface IConstant<TValue>: IConstant
	{
		TValue Value { get; set; }

		event AnonymousEventHandler<EventArgs> ValueChanged;
	}

	public interface IConstant
	{
		string Name { get; }
	}
}