﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
    public class HistoryPointCANInfo
    {
        public HistoryPointCANInfo()
        {
        }

        /// <summary>
        /// Время, когда данные были сняты с шины CAN (локальное для пользователя время)
        /// </summary>
        public DateTime time;

        /// <summary>
        /// Уровень топлива (0.1% /1)
        /// </summary>
        public int fuel1;

        /// <summary>
        /// Пробег до ТО (10км /1)
        /// </summary>
        public int runToMntns;

        /// <summary>
        /// Моточасы (часов)
        /// </summary>
        public int engHrs;

        /// <summary>
        /// Общий пробег (км)
        /// </summary>
        public int odmtr;
    }
}
