﻿using System;
using System.Collections.Generic;
using System.Web;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
//using FORIS.TSS.Terminal;
//using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
    [Serializable]
    public class HistoryPoint
    {
        /// <summary>
        /// номер в группе
        /// </summary>
        public int num;

        /// <summary>
        /// Локальное для пользователя время
        /// </summary>
        public DateTime? timeObj;

        /// <summary>
        /// Group Number
        /// </summary>
        public int grNum;

        /// <summary>
        /// Course/10
        /// </summary>
        public int? angle;

        public bool valid;
        public double lat = 180;
        public double lng = 180;
        public int? speed;
        public bool? ign;
        public double? radius;
        public int? fuel;
        /// <summary>
        /// температура
        /// </summary>
        public int? t;

        /// <summary>
        ///1=движется, 2=медленно едет, 3=стоит на месте
        /// </summary>
        public int status;

        public HistoryPointCANInfo can;
        
        public HistoryPoint()
        {
        }

/*        public HistoryPoint(
            Vehicles.VehiclesInfo v,
            IWebMobileInfoCP mobileUnit,
            int groupSerialNumber,
            TimeZoneInfo timeZoneInfo)
        {
            grNum = groupSerialNumber;
            angle = (mobileUnit.Course / 10);
            valid = mobileUnit.ValidPosition && !(mobileUnit.Latitude >= 180.0 || mobileUnit.Longitude >= 180.0);
            //timeObj = TimeZoneInfo.ConvertTimeFromUtc(TimeHelper.GetDateTimeUTC(mobileUnit.Time), timeZoneInfo);
            //даты на клиенте переведутся в локальное время
            timeObj = TimeHelper.GetDateTimeUTC(mobileUnit.Time);
            if (v.Capabilities.Contains(DeviceCapability.IgnitionMonitoring))
                ign = (mobileUnit.DigitalSensors & 0x20) == 0;
            lat = mobileUnit.Latitude;
            lng = mobileUnit.Longitude;
            radius = mobileUnit.Radius;
            speed = mobileUnit.Speed;
            status = mobileUnit.Status;
            t = mobileUnit.Temperature;
            fuel = mobileUnit.FuelVolume;
            if (mobileUnit.CANInfo != null)
            {
                can = new HistoryPointCANInfo();
                can.time = TimeHelper.GetDateTimeUTC(mobileUnit.CANInfo.Time);
                can.engHrs = mobileUnit.CANInfo.EngHours;
                can.fuel1 = mobileUnit.CANInfo.FuelLevel1;
                can.odmtr = mobileUnit.CANInfo.TotalRun;
                can.runToMntns = mobileUnit.CANInfo.RunToCarMaintenance;
            }
        }
*/
    }
}