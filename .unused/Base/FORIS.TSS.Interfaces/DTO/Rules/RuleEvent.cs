﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class RuleEvent
	{
		/// <summary> Правило и его параметры </summary>
		public Rule Rule;
		/// <summary> Идентификатор объекта, для которого зафиксировано событие </summary>
		public int VehicleID;
		/// <summary> Дополнительные сведения для восстановления события, например, идентификатор геозоны </summary>
		public int Reason;
		/// <summary> Время начала события </summary>
		public int LogTimeFrom;
		/// <summary> Время конца события </summary>
		public int LogTimeTo;
	}
}