﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
    [Serializable]
    public class OperatorRights
    {
        public OperatorRights()
        {
        }

        public Operator[] operators;
    }
}