using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;

namespace FORIS.TSS.BusinessLogic.Data
{
	/// <summary>
	/// ��������� ���������� ������, ���������������
	/// ����������� ���� ������� � �������� ���������� ������
	/// </summary>
	public interface ISecureDataSupplier<TSecurityDataSupplier, TPrincipalDataSupplier>:
		IDataSupplier
		where TSecurityDataSupplier: ISecurityDataSupplier<TPrincipalDataSupplier>
		where TPrincipalDataSupplier: IPrincipalDataSupplier
	{
		TSecurityDataSupplier SecurityDataSupplier { get; }
	}
}