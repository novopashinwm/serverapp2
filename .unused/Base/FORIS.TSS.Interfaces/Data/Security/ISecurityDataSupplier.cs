
namespace FORIS.TSS.BusinessLogic.Data.Security
{
	/// <summary>
	/// ��������� ������ ������������ ��� ����������� ������,
	/// �������������� ����������� ���� ������� � ��������
	/// ���������� ������
	/// </summary>
	public interface ISecurityDataSupplier<TPrincipalDataSupplier>:
		IDataSupplier
		where TPrincipalDataSupplier: IPrincipalDataSupplier
	{
		TPrincipalDataSupplier PrincipalDataSupplier { get; }
	}
}
