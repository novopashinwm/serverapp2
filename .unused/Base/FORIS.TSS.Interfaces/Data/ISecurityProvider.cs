﻿using System.Data;

namespace FORIS.TSS.BusinessLogic.Data
{
	public interface ISecurity
	{
		ObjectRight GetRight(DataRow dataRow);

		/// <summary> Прореживает данные в соответствии с правами доступа пользователя </summary>
		/// <param name="dataInfo"></param>
		DataInfo Check(DataInfo dataInfo);
	}


	public class ObjectRight
	{
		private readonly bool read;
		private readonly bool write;
		private readonly bool delete;
		private readonly bool create;

		public ObjectRight(bool read, bool write, bool delete, bool create)
		{
			this.read = read;
			this.write = write;
			this.delete = delete;
			this.create = create;
		}

		static ObjectRight()
		{
			s_full = new ObjectRight(true, true, true, true);
		}

		private static readonly ObjectRight s_full;

		public static ObjectRight Full
		{
			get { return ObjectRight.s_full; }
		}

		public bool Read
		{
			get { return read; }
		}

		public bool Write
		{
			get { return write; }
		}

		public bool Delete
		{
			get { return delete; }
		}

		public bool Create
		{
			get { return create; }
		}
	}

	public class ContainerObjectRight
	{
	}
}