﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.Interfaces.Diagnostic
{
	/// <summary> Интерфейс диагностирования сервера </summary>
	public interface IObservedServer
	{
		object IsAlive();
		IList<Exception> GetErrorCodes();
		void ClearErrorCodes();
		event AnonymousEventHandler<EventArgs> ServerStop;
	}
}