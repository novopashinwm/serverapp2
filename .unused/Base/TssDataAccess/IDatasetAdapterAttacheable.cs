﻿namespace FORIS.DataAccess
{
	/// <summary>
	/// Те, кто имплементирует этот интерфейс, могут подключиться к IDbDatasetAdapter
	/// для того, чтобы получать события во время обновления датасета внутри сессии
	/// (но ведь в принципе события могут быть и внутри простого обновления из ядра сервера?)
	/// </summary>
	/// <remarks>
	/// На данный момент этот интерфейс имплементируется только классом Notifications
	/// </remarks>
	public interface IDatasetAdapterAttacheable
	{
		void AttachEvents(IDatasetAdapter da);
		void DetachEvents(IDatasetAdapter da);
	}
}