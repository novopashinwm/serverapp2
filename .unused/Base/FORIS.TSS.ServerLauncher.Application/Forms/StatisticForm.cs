using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace FORIS.TSS.ServerApplication.Forms
{
	/// <summary>
	/// Summary description for StatisticForm.
	/// </summary>
	public class StatisticForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListView lvSession;
		private System.Windows.Forms.ColumnHeader clmId;
		private System.Windows.Forms.ColumnHeader clmLogin;
		private System.Windows.Forms.ColumnHeader clmLease;
		private System.Windows.Forms.Label lblSession;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public StatisticForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvSession = new System.Windows.Forms.ListView();
			this.clmId = new System.Windows.Forms.ColumnHeader();
			this.clmLogin = new System.Windows.Forms.ColumnHeader();
			this.clmLease = new System.Windows.Forms.ColumnHeader();
			this.lblSession = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lvSession
			// 
			this.lvSession.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.clmId,
																						this.clmLogin,
																						this.clmLease});
			this.lvSession.Location = new System.Drawing.Point(8, 32);
			this.lvSession.Name = "lvSession";
			this.lvSession.Size = new System.Drawing.Size(480, 144);
			this.lvSession.TabIndex = 0;
			this.lvSession.View = System.Windows.Forms.View.Details;
			// 
			// clmId
			// 
			this.clmId.Text = "Id";
			this.clmId.Width = 61;
			// 
			// clmLogin
			// 
			this.clmLogin.Text = "�����";
			this.clmLogin.Width = 131;
			// 
			// clmLease
			// 
			this.clmLease.Text = "��������";
			this.clmLease.Width = 223;
			// 
			// lblSession
			// 
			this.lblSession.Location = new System.Drawing.Point(8, 16);
			this.lblSession.Name = "lblSession";
			this.lblSession.Size = new System.Drawing.Size(40, 16);
			this.lblSession.TabIndex = 1;
			this.lblSession.Text = "������";
			// 
			// StatisticForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(504, 333);
			this.Controls.Add(this.lblSession);
			this.Controls.Add(this.lvSession);
			this.Name = "StatisticForm";
			this.Text = "StatisticForm";
			this.Load += new System.EventHandler(this.StatisticForm_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void StatisticForm_Load(object sender, System.EventArgs e)
		{
//            Server.		
		}
	}
}
