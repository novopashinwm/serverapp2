﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using FORIS.TSS.Common;
using FORIS.TSS.Config;

namespace FORIS.TSS.ServerLauncher.Application
{
	/// <summary> В данной версии сервер работает как Windows приложение </summary>
	public class ServerNotifyIcon : Component
	{
		#region Controls & Components

		private IContainer components;
		private System.Windows.Forms.ContextMenuStrip   contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem  MI_EXIT;
		private System.Windows.Forms.ToolStripMenuItem  MI_ABOUT;
		private System.Windows.Forms.ToolStripSeparator MI_SEPARATOR;
		private System.Windows.Forms.NotifyIcon         notifyIcon;

		#endregion Controls & Components

		#region Constructor & Dispose

		public ServerNotifyIcon(string serverName)
		{
			InitializeComponent();

			var callingAssembly = Assembly.GetCallingAssembly();
			MI_ABOUT.Text       = $"{serverName}({callingAssembly.VersionFull()}), {callingAssembly.Copyright()}";
			notifyIcon.Text     = serverName;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerNotifyIcon));
			this.MI_EXIT = new System.Windows.Forms.ToolStripMenuItem();
			this.MI_ABOUT = new System.Windows.Forms.ToolStripMenuItem();
			this.MI_SEPARATOR = new System.Windows.Forms.ToolStripSeparator();
			this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.contextMenuStrip.SuspendLayout();
			// 
			// MI_EXIT
			// 
			this.MI_EXIT.Name = "MI_EXIT";
			this.MI_EXIT.Size = new System.Drawing.Size(138, 22);
			this.MI_EXIT.Text = "Остановить";
			this.MI_EXIT.Click += new System.EventHandler(this.MI_EXIT_Click);
			// 
			// MI_ABOUT
			// 
			this.MI_ABOUT.Enabled = false;
			this.MI_ABOUT.Name = "MI_ABOUT";
			this.MI_ABOUT.Size = new System.Drawing.Size(138, 22);
			// 
			// MI_SEPARATOR
			// 
			this.MI_SEPARATOR.Name = "MI_SEPARATOR";
			this.MI_SEPARATOR.Size = new System.Drawing.Size(135, 6);
			// 
			// notifyIcon
			// 
			this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
			this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
			this.notifyIcon.Visible = true;
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MI_EXIT,
			this.MI_SEPARATOR,
			this.MI_ABOUT});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(139, 54);
			this.contextMenuStrip.ResumeLayout(false);

		}
		#endregion InitializeComponent()

		public void RunInteractive(bool runLoop, bool activate, bool staticServer)
		{
			/// В данном случае эти перемеренные вполне могут быть локальными
			AppDomain serverDomain;
			IEntryPoint entryPoint;

			// Create new domain to setup configuration file
			string nameOfNewDomain    = Globals.AppSettings["nameOfNewDomain"];
			string nameOfAssemblyFile = Globals.AppSettings["nameOfAssemblyFile"];
			string nameOfConfigFile   = Globals.AppSettings["nameOfConfigFile"];
			string nameOfType         = Globals.AppSettings["nameOfType"];

			string appBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

			entryPoint = (IEntryPoint)DomainHelper
				.CreateDomainAndGetEntryPoint(
					nameOfNewDomain,
					nameOfConfigFile,
					nameOfAssemblyFile,
					nameOfType,
					out serverDomain,
					appBase);
			try
			{
				using (new Stopwatcher($@"entryPoint.Start({runLoop}, {activate}, {staticServer});"))
					entryPoint.Start(runLoop, activate, staticServer);

				System.Windows.Forms.Application.Run();

				using (new Stopwatcher("entryPoint.Stop()"))
					entryPoint.Stop();
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
			}
			try
			{
				AppDomain.Unload(serverDomain);
			}
			catch (Exception ex)
			{
				Trace.TraceError("Second try to unload AppDomain failed: {0}", ex);
			}
		}

		#region Handle controls' events

		private void MI_EXIT_Click(object sender, EventArgs e)
		{
			try
			{
				notifyIcon.Visible = false;
				System.Windows.Forms.Application.Exit();
			}
			catch (Exception ex)
			{
				Trace.TraceError("Ошибка сервера : {0}", ex);
			}
		}

		#endregion Handle controls' events
	}
}