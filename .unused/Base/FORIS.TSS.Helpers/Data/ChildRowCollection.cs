using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TRow"></typeparam>
	/// <typeparam name="TTable"></typeparam>
	/// <typeparam name="TData"></typeparam>
	/// <remarks>
	/// ����� �� ������������, ��� ��� ��� ��������� 
	/// ��������� �������� ����� ������ ������ 
	/// �������������� ����������, �����������
	/// � ����������� �������
	/// </remarks>
	public class ChildRowCollection<TRow, TTable, TData>: 
		SortedCollectionWithEvents<TRow>
		where TRow: Row<TRow, TTable, TData>
		where TTable: Table<TRow, TTable, TData>
		where TData: IData
	{
		public TRow First { get { return this[0]; } }
		public TRow Last { get { return this[this.Count - 1]; } }
	}
}