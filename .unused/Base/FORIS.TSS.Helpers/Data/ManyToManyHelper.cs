using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	public abstract class ManyToManyTable<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData> :
		Table<
			ManyToManyRow<THelper,TLeftRow,TLeftTable,TRightRow,TRightTable,TData>, 
			ManyToManyTable<THelper,TLeftRow,TLeftTable,TRightRow,TRightTable,TData>, 
			TData
			>
		where TLeftRow : 
			Row<TLeftRow, TLeftTable, TData>, 
			IManyToManyRow<TRightRow, TRightTable, TData, THelper>
		where TLeftTable : 
			Table<TLeftRow, TLeftTable, TData>//,
		where TRightRow : 
			Row<TRightRow, TRightTable, TData>, 
			IManyToManyRow<TLeftRow, TLeftTable, TData, THelper>
		where TRightTable : 
			Table<TRightRow, TRightTable, TData>//, 
		where TData: IData
		where THelper: ManyToManyTable<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData>
	{
		#region Properties

		public abstract int IdIndex { get; }

		internal protected abstract int LeftKeyIndex { get; }

		internal protected abstract int RightKeyIndex { get; }

		protected abstract TLeftTable LeftTable { get; }
		
		protected abstract TRightTable RightTable { get; }

		#endregion Properties

		#region Data

		protected override void OnBeforeSetData()
		{
			this.LeftTable.Rows.Inserted -= this.LeftTableRows_Inserted;
			this.LeftTable.Rows.Removing -= this.LeftTableRows_Removing;

			this.RightTable.Rows.Inserted -= this.RightTableRows_Inserted;
			this.RightTable.Rows.Removing -= this.RightTableRows_Removing;

			this.Rows.Inserted -= this.Rows_Inserted;
			this.Rows.Removing -= this.Rows_Removing;

			base.OnBeforeSetData();
		}
		protected override void OnAfterSetData()
		{
			base.OnAfterSetData();

			this.Rows.Inserted += this.Rows_Inserted;
			this.Rows.Removing += this.Rows_Removing;

			this.LeftTable.Rows.Inserted += this.LeftTableRows_Inserted;
			this.LeftTable.Rows.Removing += this.LeftTableRows_Removing;

			this.RightTable.Rows.Inserted += this.RightTableRows_Inserted;
			this.RightTable.Rows.Removing += this.RightTableRows_Removing;
		}

		#endregion // Data

		#region Handle Rows events

		private void Rows_Inserted( object sender, CollectionChangeEventArgs<ManyToManyRow<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData>> e )
		{
			TLeftRow leftRow = this.LeftTable.FindRow( e.Item.LeftKey );
			TRightRow rightRow = this.RightTable.FindRow( e.Item.RightKey );

			leftRow.OppositeRows.Add( rightRow );
			rightRow.OppositeRows.Add( leftRow );
		}

		private void Rows_Removing( object sender, CollectionChangeEventArgs<ManyToManyRow<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData>> e )
		{
			TLeftRow leftRow = this.LeftTable.FindRow( e.Item.LeftKey );
			TRightRow rightRow = this.RightTable.FindRow( e.Item.RightKey );

			rightRow.OppositeRows.Remove( leftRow );
			leftRow.OppositeRows.Remove( rightRow );
		}

		#endregion // Handle Rows events

		#region Handle Left & Right tables

		private void LeftTableRows_Inserted( object sender, CollectionChangeEventArgs<TLeftRow> e )
		{
			e.Item.Helper = (THelper)this;
		}
		private void LeftTableRows_Removing(object sender, CollectionChangeEventArgs<TLeftRow> e)
		{
			e.Item.Helper = default( THelper );
		}

		private void RightTableRows_Inserted(object sender, CollectionChangeEventArgs<TRightRow> e)
		{
			e.Item.Helper = (THelper)this;
		}
		private void RightTableRows_Removing(object sender, CollectionChangeEventArgs<TRightRow> e)
		{
			e.Item.Helper = default( THelper );
		}

		#endregion // Handle Left & & Right tables

		#region View

		protected override void OnDestroyView()
		{
			foreach( TLeftRow leftRow in this.LeftTable.Rows )
			{
				leftRow.OppositeRows.Clear();
				leftRow.Helper = default(THelper);
			}
			foreach( TRightRow rightRow in this.RightTable.Rows )
			{
				rightRow.OppositeRows.Clear();
				rightRow.Helper = default(THelper);
			}

			base.OnDestroyView();
		}
		
		protected override void OnUpdateView()
		{
			base.OnUpdateView();

			foreach( TLeftRow leftRow in this.LeftTable.Rows )
			{
				leftRow.Helper = (THelper)this;
			}
			foreach( TRightRow rightRow in this.RightTable.Rows )
			{
				rightRow.Helper = (THelper)this;				
			}
			
			foreach( DataRow dataRow in this.DataTable.Rows )
			{
				TLeftRow leftRow = this.LeftTable.FindRow( (int)dataRow[this.LeftKeyIndex] );
				TRightRow rightRow = this.RightTable.FindRow( (int)dataRow[this.RightKeyIndex] );

				leftRow.OppositeRows.Add( rightRow );
				rightRow.OppositeRows.Add( leftRow );
			}
		}

		#endregion // View

		protected override ManyToManyRow<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData> OnCreateRow(DataRow dataRow)
		{
			return new ManyToManyRow<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData>( dataRow );
		}

		private DataRow GetLinkRow( int leftKey, int rightKey )
		{
			DataRow[] result =
				this.DataTable.Select(
					string.Format(
						"[{0}] = {1} and [{2}] = {3}",
						this.DataTable.Columns[this.LeftKeyIndex],
						leftKey,
						this.DataTable.Columns[this.RightKeyIndex],
						rightKey
						)
					);

			switch( result.Length )
			{
				case 0:
					return null;

				case 1:
					return result[0];

				default:
					throw new InvalidDataException();
			}
		}

		#region IManyToManyHelper<TLeftRow, TLeftTable, TRightRow, TRightTable, TData> Members

		public bool Linked( TLeftRow leftRow, TRightRow rightRow )
		{
			return this.GetLinkRow( leftRow.ID, rightRow.ID ) != null;
		}

		/// <summary>
		/// Links leftRow and rightRow
		/// </summary>
		/// <param name="leftRow"></param>
		/// <param name="rightRow"></param>
		/// <remarks>
		/// Create row in link DataTable
		/// Add leftRow to rightRow.OppositeRows
		/// Add rightRow to leftRow.OppositeRows
		/// </remarks>
		public void Link(TLeftRow leftRow, TRightRow rightRow)
		{
			#region Preconditions

			if( this.GetLinkRow( leftRow.ID, rightRow.ID ) != null )
			{
				throw new InvalidOperationException( "����� ��� ����" );
			}

			#endregion // Preconditions

			DataRow linkDataRow = this.DataTable.NewRow();

			linkDataRow[this.LeftKeyIndex] = leftRow.ID;
			linkDataRow[this.RightKeyIndex] = rightRow.ID;

			this.DataTable.Rows.Add( linkDataRow );
		}

		/// <summary>
		/// Unlinks rightRow and leftRow
		/// </summary>
		/// <param name="rightRow"></param>
		/// <param name="leftRow"></param>
		/// Delete row in link DataTable
		/// Remove leftRow to rightRow.OppositeRows
		/// Remove rightRow to leftRow.OppositeRows
		public void Unlink( TLeftRow leftRow, TRightRow rightRow )
		{
			DataRow linkDataRow = this.GetLinkRow( leftRow.ID, rightRow.ID );

			#region Preconditions

			if( linkDataRow == null )
			{
				throw new InvalidOperationException( "����� ��� ���" );
			}

			#endregion // Preconditions

			linkDataRow.Delete();
		}

		#endregion // IManyToManyHelper<TLeftRow, TLeftTable, TRightRow, TRightTable, TData> Members
	}

	public class ManyToManyRow<THelper,TLeftRow,TLeftTable,TRightRow,TRightTable,TData>:
		Row<
			ManyToManyRow<THelper,TLeftRow,TLeftTable,TRightRow,TRightTable,TData>, 
			ManyToManyTable<THelper,TLeftRow,TLeftTable,TRightRow,TRightTable,TData>, 
			TData
			>
		where TLeftRow :
			Row<TLeftRow, TLeftTable, TData>,
			IManyToManyRow<TRightRow, TRightTable, TData, THelper>
		where TLeftTable :
			Table<TLeftRow, TLeftTable, TData>//,
		where TRightRow :
			Row<TRightRow, TRightTable, TData>,
			IManyToManyRow<TLeftRow, TLeftTable, TData, THelper>
		where TRightTable :
			Table<TRightRow, TRightTable, TData>//, 
		where TData : IData
		where THelper : ManyToManyTable<THelper, TLeftRow, TLeftTable, TRightRow, TRightTable, TData>
	{
		#region Fields

		public int LeftKey
		{
			get { return (int)this.DataRow[this.Table.LeftKeyIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LeftKeyIndex] = value; }
		}

		public int RightKey
		{
			get { return (int)this.DataRow[this.Table.RightKeyIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RightKeyIndex] = value; }
		}

		#endregion // Fields

		#region Id 

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public ManyToManyRow( DataRow dataRow ):
			base( dataRow)
		{

		}

		protected override void OnBuildFields()
		{

		}
	}

	/* ��� �� ������ ���� ������� �����.
	 * ������ ��������� �� �����.
	 */

	public interface IManyToManyRow<TOppositeRow, TOppositeTable, TData, THelper>
		where TOppositeRow: Row<TOppositeRow, TOppositeTable, TData>
		where TOppositeTable: Table<TOppositeRow, TOppositeTable, TData>
		where TData: IData
	{
		ICollection<TOppositeRow> OppositeRows { get; }

		THelper Helper { get; set; }
	}
}
