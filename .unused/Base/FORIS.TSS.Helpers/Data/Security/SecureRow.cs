﻿using System.Collections.Generic;
using System.Data;
using System.Security;
using System.Threading;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class SecureRow<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData> :
		Row<TSecureRow, TSecureTable, TProtectedData>,
		ISecureObject
		where TSecureRow : SecureRow<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData>
		where TSecureTable : SecureTable<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData>
		where TProtectedData : ISecureData<TSecurityData>
		where TSecurityData : ISecurityData<TSecurityData, TProtectedData, TPrincipalData>
		where TPrincipalData : IPrincipalData
	{
		public SecureRow(DataRow dataRow)
			: base(dataRow)
		{

		}

		#region ISecureObject Members

		ICollection<ISecureObject> ISecureObject.Parents
		{
			get { return this.GetSecureObjectParents(); }
		}

		IList<IAccess> ISecureObject.Acl
		{
			get { return this.GetSecureObjectAcl(); }
		}

		public SecureObjectInfo Info
		{
			get
			{
				return
					new SecureObjectInfo(
						this.GetSecureObjectName(),
						this.Table.Name,
						this.ID
						);
			}
		}

		public bool TestRight(GeneralRights demandRights)
		{
			#region Preconditions

			if (!(Thread.CurrentPrincipal is TssPrincipalInfo))
			{
				throw new SecurityException("Thread principal type must be object of type TssPrincipalInfo");
			}

			#endregion // Preconditions

			TssPrincipalInfo currentPrincipal = (TssPrincipalInfo)Thread.CurrentPrincipal;

			return this.TestRight(demandRights, currentPrincipal);
		}

		public bool TestRight(GeneralRights demandRights, TssPrincipalInfo principal)
		{
			return ((int)demandRights & this.GetEffectiveRights(principal)) == (int)demandRights;
		}

		#endregion // ISecureObject Members

		protected abstract string GetSecureObjectName();

		protected abstract ICollection<ISecureObject> GetSecureObjectParents();

		protected abstract IList<IAccess> GetSecureObjectAcl();

		public abstract void ClearAcl();

		protected int EffectiveRights
		{
			get
			{
				#region Preconditions

				if (!(Thread.CurrentPrincipal is TssPrincipalInfo))
				{
					throw new SecurityException("Thread principal type must be object of type TssPrincipalInfo");
				}

				#endregion // Preconditions

				IList<IAccess> acl = this.GetSecureObjectAcl();
				ICollection<ISecureObject> objectParents = this.GetSecureObjectParents();

				TssPrincipalInfo currentPrincipal = (TssPrincipalInfo)Thread.CurrentPrincipal;
				IList<TssPrincipalInfo> principalParents =
					this.Data.SecurityData.PrincipalData.GetPrincipalParents(currentPrincipal);

				int result = 0;

				#region Allow

				foreach (ISecureObject secureObject in objectParents)
				{
					foreach (IAccess access in secureObject.Acl)
					{
						if (
							principalParents.Contains(access.Principal.Info) ||
							currentPrincipal == access.Principal.Info
							)
						{
							result |= access.Info.AllowMask;
						}
					}
				}

				foreach (IAccess access in acl)
				{
					if (
						principalParents.Contains(access.Principal.Info) ||
						currentPrincipal == access.Principal.Info
						)
					{
						result |= access.Info.AllowMask;
					}
				}

				#endregion Allow

				#region Deny

				foreach (ISecureObject secureObject in objectParents)
				{
					foreach (IAccess access in secureObject.Acl)
					{
						if (
							principalParents.Contains(access.Principal.Info) ||
							currentPrincipal == access.Principal.Info
							)
						{
							result &= ~access.Info.DenyMask;
						}
					}
				}

				foreach (IAccess access in acl)
				{
					if (
						principalParents.Contains(access.Principal.Info) ||
						currentPrincipal == access.Principal.Info
						)
					{
						result &= ~access.Info.DenyMask;
					}
				}

				#endregion // Deny

				return result;
			}
		}

		protected int GetEffectiveRights(TssPrincipalInfo principal)
		{
			IList<IAccess> acl = this.GetSecureObjectAcl();
			ICollection<ISecureObject> objectParents = this.GetSecureObjectParents();

			IList<TssPrincipalInfo> principalParents =
				this.Data.SecurityData.PrincipalData.GetPrincipalParents(principal);

			int result = 0;

			#region Allow

			foreach (ISecureObject secureObject in objectParents)
			{
				foreach (IAccess access in secureObject.Acl)
				{
					if (
						principalParents.Contains(access.Principal.Info) ||
						principal == access.Principal.Info
						)
					{
						result |= access.Info.AllowMask;
					}
				}
			}

			foreach (IAccess access in acl)
			{
				if (
					principalParents.Contains(access.Principal.Info) ||
					principal == access.Principal.Info
					)
				{
					result |= access.Info.AllowMask;
				}
			}

			#endregion Allow

			#region Deny

			foreach (ISecureObject secureObject in objectParents)
			{
				foreach (IAccess access in secureObject.Acl)
				{
					if (
						principalParents.Contains(access.Principal.Info) ||
						principal == access.Principal.Info
						)
					{
						result &= ~access.Info.DenyMask;
					}
				}
			}

			foreach (IAccess access in acl)
			{
				if (
					principalParents.Contains(access.Principal.Info) ||
					principal == access.Principal.Info
					)
				{
					result &= ~access.Info.DenyMask;
				}
			}

			#endregion // Deny

			return result;
		}
	}
}