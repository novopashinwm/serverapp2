using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data.Security
{
	/// <summary>
	/// ��������� ���������� ������ � �����������������
	/// </summary>
	public interface IPrincipalData:
		IData
	{
		ICollectionWithEvents<TssPrincipalInfo> Principals { get;}

		IList<TssPrincipalInfo> GetPrincipalParents( TssPrincipalInfo principal );
	}
}
