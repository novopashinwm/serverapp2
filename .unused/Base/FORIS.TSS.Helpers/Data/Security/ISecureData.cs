namespace FORIS.TSS.Helpers.Data.Security
{
	/// <summary>
	/// ��������� ���������� ������, ���������������
	/// ����������� ���� ������� � �������� ���������� ������
	/// </summary>
	/// <remarks>
	/// ���� ������������ ����������� ���� �������
	/// � �������� ���������� ������ ����������
	/// ����� ���������� � ������ ������� ����������������
	/// <see cref="IPrincipalData"/> � �������� ���������� ������.
	/// ��������� �������� ���������� � ������ �������
	/// � ��������� ����������
	/// </remarks>
	public interface ISecureData<TSecurityData>:
		IData
		where TSecurityData: IData
	{
		TSecurityData SecurityData { get; }
	}
}