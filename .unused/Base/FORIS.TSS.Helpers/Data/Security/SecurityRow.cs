﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	/// <summary>
	/// Строка данных безопасности (обертка)
	/// </summary>
	/// <typeparam name="TSecurityRow"></typeparam>
	/// <typeparam name="TSecurityTable"></typeparam>
	/// <typeparam name="TSecurityData">тип контейнера данных безопасности</typeparam>
	/// <typeparam name="TProtectedData"></typeparam>
	/// <typeparam name="TPrincipalData"></typeparam>
	public abstract class SecurityRow<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData> :
		Row<TSecurityRow, TSecurityTable, TSecurityData>,
		IAccess
		where TSecurityRow : SecurityRow<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData>
		where TSecurityTable : SecurityTable<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData>
		where TSecurityData : ISecurityData<TSecurityData, TProtectedData, TPrincipalData>
		where TProtectedData : ISecureData<TSecurityData>
		where TPrincipalData : IPrincipalData
	{
		public SecurityRow(DataRow dataRow)
			: base(dataRow)
		{

		}

		#region IAccess

		AccessControlInfo IAccess.Info
		{
			get
			{
				return new AccessControlInfo(
					this.AllowMask,
					this.DenyMask,
					false
					);
			}
		}

		ITssPrincipal IAccess.Principal
		{
			get { return this.GetPrincipal(); }
		}

		ISecureObject IAccess.SecureObject
		{
			get { throw new NotImplementedException(); }
		}

		#endregion // IAccess

		public abstract int AllowMask { get; set; }
		public abstract int DenyMask { get; set; }

		protected abstract ITssPrincipal GetPrincipal();
	}
}