﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using System.Runtime.Remoting.Lifetime;

namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class SecureData<TDataTreater>:
		FORIS.TSS.Helpers.Data.Data<TDataTreater>,
		ISponsor
		where TDataTreater: IDataTreater
	{
		#region ISponsor Members

		TimeSpan ISponsor.Renewal(ILease lease)
		{
			ILease ownLease = (ILease)this.GetLifetimeService();

			return ownLease.CurrentLeaseTime;
		}

		#endregion // ISponsor Members
	}
}