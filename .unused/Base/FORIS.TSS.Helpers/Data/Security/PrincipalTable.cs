namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class PrincipalTable<TPrincipalRow, TPrincipalTable, TPrincipalData>:
		Table<TPrincipalRow, TPrincipalTable, TPrincipalData>
		where TPrincipalRow: PrincipalRow<TPrincipalRow, TPrincipalTable, TPrincipalData>
		where TPrincipalTable: PrincipalTable<TPrincipalRow, TPrincipalTable, TPrincipalData>
		where TPrincipalData: IPrincipalData
	{

	}
}
