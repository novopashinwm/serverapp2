﻿using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class SecureTable<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData> :
		Table<TSecureRow, TSecureTable, TProtectedData>,
		ISecureTable
		where TSecureRow : SecureRow<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData>
		where TSecureTable : SecureTable<TSecureRow, TSecureTable, TProtectedData, TSecurityData, TPrincipalData>
		where TProtectedData : ISecureData<TSecurityData>
		where TSecurityData : ISecurityData<TSecurityData, TProtectedData, TPrincipalData>
		where TPrincipalData : IPrincipalData
	{
		protected override void OnDestroyView()
		{
			if (this.DataTable != null)
			{
				this.DataTable.RowChanging -= DataTable_RowChanging;
				this.DataTable.RowDeleting -= DataTable_RowDeleting;
			}

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if (this.DataTable != null)
			{
				this.DataTable.RowChanging += DataTable_RowChanging;
				this.DataTable.RowDeleting += DataTable_RowDeleting;
			}
		}


		private void DataTable_RowChanging(object sender, DataRowChangeEventArgs e)
		{
			/* Отладочную информацию с данными строки 
			 * нельзя получить для удаляемой строки 
			 * в действии Commit 
			 */

			switch (e.Action)
			{
				case DataRowAction.Add:
					// TODO: проверить права доступа identity потока на создание/добавление
					/* При создании/добавлении проверять права доступа к таблице, а не к строке
					 */

					break;

				case DataRowAction.Change:

					break;
			}
		}

		private void DataTable_RowDeleting(object sender, DataRowChangeEventArgs e)
		{
			/* Отладочную информацию с данными строки 
			 * нельзя получить для удаляемой строки 
			 * в действии Commit 
			 */

			ISecureObject SecureObject = this.rows[e.Row];

			switch (e.Action)
			{
				case DataRowAction.Delete:
					break;
			}
		}

		// TODO: нарисовать применение и ограничение эффективных прав

		public abstract AccessRight[] AccessRights { get; }

		public ISecureObject GetSecureObject(int id)
		{
			return this.FindRow(id);
		}
	}

	public interface ISecureTable :
		ITable
	{
		AccessRight[] AccessRights { get; }

		ISecureObject GetSecureObject(int id);
	}
}