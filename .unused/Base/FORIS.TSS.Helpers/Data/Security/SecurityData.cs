﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class SecurityData<TProtectedData, TSecurityData> :
		FORIS.TSS.Helpers.Data.Data,
		ISecurityData
		where TProtectedData : ISponsor
	{
		public SecurityData(TProtectedData protectedData)
		{
			this.protectedData = protectedData;
		}

		protected readonly TProtectedData protectedData;

		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			if (lease.CurrentState == LeaseState.Initial)
			{
				lease.Register(this.protectedData);
			}

			return lease;
		}

		#region ISecurityData Members

		ISecurityTable ISecurityData.GetSecurityTable(SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo)
		{
			return this.GetSecurityTable(objectInfo, principalInfo);
		}

		#endregion // ISecurityData Members

		protected abstract ISecurityTable GetSecurityTable(SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo);
	}
}