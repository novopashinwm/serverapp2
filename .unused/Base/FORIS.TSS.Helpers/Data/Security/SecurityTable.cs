﻿
using System;
using System.Data;

namespace FORIS.TSS.Helpers.Data.Security
{
	/// <summary>
	/// Таблица данных безопасности (обертка)
	/// </summary>
	/// <typeparam name="TProtectedData">тип контейнера с защищаемыми данными</typeparam>
	/// <typeparam name="TPrincipalData">тип контейнера с правообладателями</typeparam>
	/// <typeparam name="TSecurityRow"></typeparam>
	/// <typeparam name="TSecurityTable"></typeparam>
	/// <typeparam name="TSecurityData">тип контейнера данных безопасности</typeparam>
	/// <remarks>
	/// В кадждой таблице безопасности можно 
	/// разобрать/рассортировать записи 
	/// безопасности по признаку принадлежности 
	/// защищаемому объекту еще на этапе 
	/// построения обертки таблицы безопасности
	/// </remarks>
	public abstract class SecurityTable<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData> :
		Table<TSecurityRow, TSecurityTable, TSecurityData>,
		ISecurityTable
		where TSecurityRow : SecurityRow<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData>
		where TSecurityTable : SecurityTable<TSecurityRow, TSecurityTable, TSecurityData, TProtectedData, TPrincipalData>
		where TSecurityData : ISecurityData<TSecurityData, TProtectedData, TPrincipalData>
		where TProtectedData : ISecureData<TSecurityData>
		where TPrincipalData : IPrincipalData
	{
		protected abstract string SecureObjectIdColumnName { get; }
		protected abstract string PrincipalIdColumnName { get; }

		public void CreateAccess(
			int objectId,
			int principalId,
			int allowMask,
			int denyMask
			)
		{
			TSecurityRow Row = this.NewRow();

			Row.DataRow[this.SecureObjectIdColumnName] = objectId;
			Row.DataRow[this.PrincipalIdColumnName] = principalId;
			Row.AllowMask = allowMask;
			Row.DenyMask = denyMask;

			this.Rows.Add(Row);
		}

		public void UpdateAccess(
			int objectId,
			int principalId,
			int allowMask,
			int denyMask
			)
		{
			TSecurityRow Row = this.GetAccess(objectId, principalId);

			Row.BeginEdit();

			Row.AllowMask = allowMask;
			Row.DenyMask = denyMask;

			Row.EndEdit();
		}

		public void DeleteAccess(
			int objectId,
			int principalId
			)
		{
			TSecurityRow Row = this.GetAccess(objectId, principalId);

			Row.Delete();
		}

		private TSecurityRow GetAccess(
			int objectId,
			int principalId
			)
		{
			DataRow[] SelectedRows =
				this.DataTable.Select(
					String.Format(
						"{0}={1} and {2}={3}",
						this.SecureObjectIdColumnName,
						objectId,
						this.PrincipalIdColumnName,
						principalId
						)
					);

			if (SelectedRows.Length == 0)
			{
				return null;
			}
			else if (SelectedRows.Length == 1)
			{
				return this.rows[SelectedRows[0]];
			}
			else
			{
				throw new ApplicationException(
					"Too many security records"
					);
			}
		}
	}

	public interface ISecurityTable :
		ITable
	{
		void CreateAccess(
			int objectId,
			int principalId,
			int allowMask,
			int denyMask
			);

		void UpdateAccess(
			int objectId,
			int principalId,
			int allowMask,
			int denyMask
			);

		void DeleteAccess(
			int objectId,
			int principalId
			);
	}
}