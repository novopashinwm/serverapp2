using FORIS.TSS.BusinessLogic.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	public interface ISecurityData:
		IData
	{
		ISecurityTable GetSecurityTable( SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo );
	}

	/// <summary>
	/// ��������� ���������� ������ � ������� 
	/// � ������ ������� ���������������� � �������� 
	/// ������ � ����������, �������������� ����������� 
	/// ���� ������� � ������� ������.
	/// </summary>
	public interface ISecurityData<TSecurityData, TProtectedData, TPrincipalData>:
		ISecurityData
		where TSecurityData: ISecurityData<TSecurityData, TProtectedData, TPrincipalData>
		where TProtectedData: ISecureData<TSecurityData>
		where TPrincipalData: IPrincipalData
	{
		TPrincipalData PrincipalData { get; }
		TProtectedData ProtectedData { get; }
	}
}
