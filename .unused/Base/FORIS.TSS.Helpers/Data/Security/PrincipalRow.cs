using System.Collections.Generic;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;

namespace FORIS.TSS.Helpers.Data.Security
{
	public abstract class PrincipalRow<TPrincipalRow, TPrincipalTable, TPrincipalData>:
		Row<TPrincipalRow, TPrincipalTable, TPrincipalData>,
		ITssPrincipal
		where TPrincipalRow: PrincipalRow<TPrincipalRow, TPrincipalTable, TPrincipalData>
		where TPrincipalTable: PrincipalTable<TPrincipalRow,TPrincipalTable,TPrincipalData>
		where TPrincipalData: IPrincipalData
	{
		public PrincipalRow( DataRow dataRow )
			:base( dataRow )
		{

		}

		#region ITssPrincipal Members

		public TssPrincipalInfo Info
		{
			get
			{
				return
					new TssPrincipalInfo(
						this.GetPrincipalName(),
						this.Table.Name,
						this.ID,
						this.GetPrincipalType()
						);
			}
		}

		ICollection<TssPrincipalInfo> ITssPrincipal.Parents
		{
			get { return this.GetPrincipalParents(); }
		}

		#endregion // ITssPrincipal Members

		protected abstract string GetPrincipalName();
		protected abstract TssPrincipalType GetPrincipalType();
		protected abstract ICollection<TssPrincipalInfo> GetPrincipalParents();
	}
}
