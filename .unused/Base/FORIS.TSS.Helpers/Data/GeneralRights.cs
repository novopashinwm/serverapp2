using System;

namespace FORIS.TSS.Helpers.Data
{
	[Flags]
	public enum GeneralRights: int
	{
		FullControl = 0x000F,
		Read = 0x0001,
		Update = 0x0003,
		Delete = 0x0007,
		AccessControl = 0x000B
	}
}