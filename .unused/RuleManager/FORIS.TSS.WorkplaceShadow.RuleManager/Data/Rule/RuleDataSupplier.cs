using System.ComponentModel;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.ServerApplication.RuleManager;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule
{
	/// <summary>
	/// ��������� ������ ������ ��� �������
	/// </summary>
	public class RuleDataSupplier:
		ModuleDataSupplier<IRuleClientDataProvider,IRuleDataSupplier>,
		IRuleDataSupplier
	{
		#region Constructor & Dispose

		public RuleDataSupplier( IContainer container )
		{
			container.Add( this );
		}

		public RuleDataSupplier()
		{
			this.ruleSecurityDataSupplier = new RuleSecurityDatabaseDataSupplier( (IRuleServerDataProvider)this );
		}

		#endregion // Constructor & Dispose

		public IRuleDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IRuleDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetRuleClientData();
		}

		IControllerDataSupplier IRuleDataSupplier.ControllerDataSupplier
		{
			get
			{
				IControllerDataSupplier result = null;

				if( this.dataSupplier != null )
				{
					result = this.dataSupplier.ControllerDataSupplier;
				}

				return result;
			}
		}

		MobileRuleSettings IRuleDataSupplier.CreateMobileRuleSettings( int ruleId )
		{
			return this.dataSupplier.CreateMobileRuleSettings( ruleId );
		}

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

	    private readonly IRuleSecurityDataSupplier ruleSecurityDataSupplier;

        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.ruleSecurityDataSupplier; }
        }

        #endregion
    }
}