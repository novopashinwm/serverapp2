using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule
{
	/// <summary>
	/// ��������� ������ ������� �������
	/// ������ "������� ������"
	/// </summary>
	public class RuleClientData:
		RuleData
	{
		#region Constructor & Dispose

		public RuleClientData( IRuleClientDataProvider clientProvider )
		{
			this.dataSupplier = new RuleClientDataSupplier( clientProvider );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// ������������ ��� ��������� ������ ������� DataTreater
		/// </summary>
		private readonly IRuleDataSupplier dataSupplier;

		public IDataSupplier SecurityDataSupplier
		{
			get { throw new NotSupportedException(); }
		}
	}
}