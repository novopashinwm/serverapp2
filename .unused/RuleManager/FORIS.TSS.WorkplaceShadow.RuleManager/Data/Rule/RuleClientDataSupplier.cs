using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule
{
	internal class RuleClientDataSupplier:
		ClientDataSupplier<IRuleClientDataProvider, IRuleDataSupplier, IRuleDataTreater>,
		IRuleDataSupplier
	{
		public RuleClientDataSupplier( IRuleClientDataProvider clientProvider )
			: base( clientProvider, clientProvider.GetRuleDataSupplier() )
		{

		}

		IControllerDataSupplier IRuleDataSupplier.ControllerDataSupplier
		{
			get
			{
				return
					this.ClientProvider != null
						? this.ClientProvider.GetControllerClientData()
						: null;
			}
		}

		MobileRuleSettings IRuleDataSupplier.CreateMobileRuleSettings( int ruleId )
		{
			return 
				this.ClientProvider.GetRuleDataSupplier().CreateMobileRuleSettings( ruleId );
		}

        

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.DataSupplier.SecurityDataSupplier; }
        }

        #endregion
    }
}