using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule
{
	public class RuleDataSupplierFactory:
		ClientDataSupplierFactory<IRuleClientDataProvider, RuleClientData, DataParams>
	{
		public RuleDataSupplierFactory( IRuleClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override RuleClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			RuleClientData Supplier =new RuleClientData( this.Client );

			return Supplier;
		}
	}
}