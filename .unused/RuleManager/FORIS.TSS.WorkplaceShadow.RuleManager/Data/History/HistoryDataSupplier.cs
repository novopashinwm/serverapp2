using System;
using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.History;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.Common;
using TerminalService.Interfaces;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Data.History
{
	/// <summary>
	/// ��������� ������ ������ ��� �������
	/// </summary>
	public class HistoryDataSupplier :
		Component,
		IHistoryDataSupplier
	{
		#region Constructor & Dispose

		private readonly HistoryDataSet dataSet;

		public HistoryDataSupplier( IEnumerable<IPositionMessage> positionMessages )
		{
			this.dataSet = new HistoryDataSet();

			foreach ( IPositionMessage message in positionMessages )
			{
				HistoryDataSet.HISTORYRow historyDataRow = dataSet.HISTORY.NewHISTORYRow();
				
				historyDataRow.LOG_TIME = TimeHelper.year1970.AddSeconds( message.GpsData.Time );
				historyDataRow.LONGITUDE = message.GpsData.Longitude;
				historyDataRow.LATITUDE = message.GpsData.Latitude;
				historyDataRow.LONGITUDE = message.GpsData.Longitude;
				historyDataRow.SPEED = message.GpsData.Speed;
				historyDataRow.SATELLITES = (byte)message.GpsData.Sattelites;
				historyDataRow.PROPERTIES = new byte[0];

				dataSet.HISTORY.Rows.Add(historyDataRow);
			}
		}

		public HistoryDataSupplier( HistoryDataSet dataSet )
		{
			this.dataSet = dataSet;			
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.delegateChanged = null;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region IHistoryDataSupplier members
		
		public DataInfo add_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			return
				new DataInfo(
					this.dataSet,
					Guid.Empty
					);
		}

		public void remove_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			/* �� ���� ������ ������, � ���������� ����������� ���� �� ����
			 */
		}

		public void Tick()
		{
			throw new NotImplementedException();
		}

		private AnonymousEventHandler<EventArgs> delegateChanged;
		public event AnonymousEventHandler<EventArgs> Changed
		{
			add { this.delegateChanged += value; }
			remove { this.delegateChanged -= value; }
		}

		#endregion  // IHistoryDataSupplier members
	}
}