﻿using FORIS.TSS.Common;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType
{
	public class MobileTypeFilter :
		Filter<MobileTypeRow, IRuleData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.MobileType.Rows.Inserted -= MobileType_Inserted;
			this.Data.MobileType.Rows.Removing -= MobileType_Removing;
		}

		protected override void OnAfterSetData()
		{
			this.Data.MobileType.Rows.Inserted += MobileType_Inserted;
			this.Data.MobileType.Rows.Removing += MobileType_Removing;
		}

		#endregion  // Data

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}

		protected override void OnBuildView()
		{
			foreach ( MobileTypeRow row in this.Data.MobileType.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View

		#region Data events

		void MobileType_Inserted( object sender, CollectionChangeEventArgs<MobileTypeRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}

		void MobileType_Removing( object sender, CollectionChangeEventArgs<MobileTypeRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion  // Data events
	}
}