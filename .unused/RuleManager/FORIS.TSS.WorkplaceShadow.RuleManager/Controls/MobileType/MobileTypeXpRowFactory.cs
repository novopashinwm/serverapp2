﻿using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType
{
	public class MobileTypeXpRowFactory :
		ViewObjectFactory<MobileTypeRow, MobileTypeXpRow, IRuleData>
	{
		public override MobileTypeXpRow CreateViewObject( 
			int viewObjectId, 
			MobileTypeRow mobileTypeRow 
			)
		{
			return
				new MobileTypeXpRow(
					viewObjectId,
					mobileTypeRow.Name
					);
		}
	}
}