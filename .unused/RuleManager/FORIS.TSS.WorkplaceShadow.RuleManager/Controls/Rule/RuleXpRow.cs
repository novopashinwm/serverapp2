using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleXpRow:
		DataXpRow<RuleRow, RuleTable, IRuleData>
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellDescription;
		private readonly Cell cellCode;

		#endregion // Cells

		public RuleXpRow( int idRule )
			: base( idRule )
		{
			this.cellName = new Cell();
			this.cellDescription = new Cell();
			this.cellCode = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellName,
						this.cellDescription,
						this.cellCode
					}
				);
		}

		protected override RuleRow GetRow()
		{
			return this.Data.Rule.FindRow( this.Id );
		}

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellName.Text = this.Row.Name;
				this.cellDescription.Text = this.Row.Description;
				this.cellCode.Text = this.Row.Code;
			}
			else
			{
				this.cellName.Text = String.Empty;
				this.cellDescription.Text = String.Empty;
				this.cellCode.Text = String.Empty;
			}
		}
	}
}