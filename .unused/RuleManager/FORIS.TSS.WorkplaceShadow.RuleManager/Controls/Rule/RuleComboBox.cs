using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleComboBox:
		DataComboBox<RuleItem, RuleRow, RuleTable, IRuleData>
	{

		protected override RuleTable GetTable()
		{
			return this.Data.Rule;
		}

		protected override RuleItem CreateItem( int idRule )
		{
			return new RuleItem( idRule );
		}
	}

	public class RuleItem:
		DataItem<RuleRow, RuleTable, IRuleData>
	{
		public RuleItem( int idRule ): base( idRule )
		{
			
		}

		protected override RuleRow GetRow()
		{
			return this.Data.Rule.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.Name;
		}
		
	}
}
