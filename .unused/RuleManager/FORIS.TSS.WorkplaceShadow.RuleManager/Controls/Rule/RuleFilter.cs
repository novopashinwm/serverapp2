using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleFilter: 
		DataFilter<RuleRow, RuleTable, IRuleData>
	{
		public override bool Check( RuleRow row )
		{
			return true;
		}
	}
}
