using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleEditorProvider:
		EditorProvider<IRuleDataTreater, IRuleData>,
		IRuleEditorProvider
	{
		public RuleEditorProvider()
		{
			
		}
		public RuleEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		protected override void New()
		{
			using( RulePropertiesForm Form = new RulePropertiesForm() )
			{
				Form.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if( Form.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							((IRuleDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.RuleInsert(
								Form.RuleName,
								Form.Description,
								Form.Code
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RuleEditorProvider_Exception_CanNotCreateNewRule,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Properties( int idRule )
		{
			using( RulePropertiesForm Form = new RulePropertiesForm() )
			{
				Form.Mode = DataItemControlMode.Edit;
				Form.IdRule = idRule;

				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if( Form.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							( (IRuleDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.RuleUpdate(
								idRule,
								Form.RuleName,
								Form.Description,
								Form.Code
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RuleEditorProvider_Exception_CanNotUpdateRule,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Remove( int idRule )
		{
			if(
				MessageBox.Show(
					Strings.RuleEditorProvider_Warning_AreYouSureToDeleteRule,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRuleDataTreater Treater =
						( (IRuleDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.RuleDelete( idRule );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.RuleEditorProvider_Exception_CanNotDeleteRule,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idRule )
		{
			throw new NotImplementedException();
			// return this.Data.Rule.FindRow( idRule );
		}
	}
}