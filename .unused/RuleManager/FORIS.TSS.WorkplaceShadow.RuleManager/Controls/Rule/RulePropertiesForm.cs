using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RulePropertiesForm: 
		TssUserForm,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;
		private RuleDataDispatcher dataDispatcher;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.TextBox edtName;
		private TextBox edtCode;
		private TextBox edtDescription;
		private Label lblCode;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public RulePropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulePropertiesForm));
			this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher(this.components);
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblName = new System.Windows.Forms.Label();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.edtCode = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblCode = new System.Windows.Forms.Label();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.UpdateView += new System.EventHandler(this.dataDispatcher_UpdateView);
			this.dataDispatcher.BuildView += new System.EventHandler(this.dataDispatcher_BuildView);
			this.dataDispatcher.DestroyView += new System.EventHandler(this.dataDispatcher_DestroyView);
			this.dataDispatcher.BeforeSetData += new System.EventHandler(this.dataDispatcher_BeforeSetData);
			this.dataDispatcher.AfterSetData += new System.EventHandler(this.dataDispatcher_AfterSetData);
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.AccessibleDescription = null;
			this.tableLayoutPanelMain.AccessibleName = null;
			resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
			this.tableLayoutPanelMain.BackgroundImage = null;
			this.tableLayoutPanelMain.Controls.Add(this.lblName, 0, 0);
			this.tableLayoutPanelMain.Controls.Add(this.edtDescription, 1, 1);
			this.tableLayoutPanelMain.Controls.Add(this.edtCode, 1, 2);
			this.tableLayoutPanelMain.Controls.Add(this.lblDescription, 0, 1);
			this.tableLayoutPanelMain.Controls.Add(this.edtName, 1, 0);
			this.tableLayoutPanelMain.Controls.Add(this.lblCode, 0, 2);
			this.tableLayoutPanelMain.Font = null;
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblName
			// 
			this.lblName.AccessibleDescription = null;
			this.lblName.AccessibleName = null;
			resources.ApplyResources(this.lblName, "lblName");
			this.lblName.Font = null;
			this.lblName.Name = "lblName";
			// 
			// edtDescription
			// 
			this.edtDescription.AccessibleDescription = null;
			this.edtDescription.AccessibleName = null;
			resources.ApplyResources(this.edtDescription, "edtDescription");
			this.edtDescription.BackgroundImage = null;
			this.edtDescription.Font = null;
			this.edtDescription.Name = "edtDescription";
			// 
			// edtCode
			// 
			this.edtCode.AccessibleDescription = null;
			this.edtCode.AccessibleName = null;
			resources.ApplyResources(this.edtCode, "edtCode");
			this.edtCode.BackgroundImage = null;
			this.edtCode.Font = null;
			this.edtCode.Name = "edtCode";
			// 
			// lblDescription
			// 
			this.lblDescription.AccessibleDescription = null;
			this.lblDescription.AccessibleName = null;
			resources.ApplyResources(this.lblDescription, "lblDescription");
			this.lblDescription.Font = null;
			this.lblDescription.Name = "lblDescription";
			// 
			// edtName
			// 
			this.edtName.AccessibleDescription = null;
			this.edtName.AccessibleName = null;
			resources.ApplyResources(this.edtName, "edtName");
			this.edtName.BackgroundImage = null;
			this.edtName.Font = null;
			this.edtName.Name = "edtName";
			// 
			// lblCode
			// 
			this.lblCode.AccessibleDescription = null;
			this.lblCode.AccessibleName = null;
			resources.ApplyResources(this.lblCode, "lblCode");
			this.lblCode.Font = null;
			this.lblCode.Name = "lblCode";
			// 
			// RulePropertiesForm
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = null;
			this.Controls.Add(this.tableLayoutPanelMain);
			this.Font = null;
			this.Icon = null;
			this.Name = "RulePropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RulePropertiesForm_FormClosing);
			this.Controls.SetChildIndex(this.tableLayoutPanelMain, 0);
			this.tableLayoutPanelMain.ResumeLayout(false);
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		#region IDataItem<IRuleData> Members

		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if (this.dataDispatcher.Data != null)
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if (this.dataDispatcher.Data != null)
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IRuleData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idRule;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdRule
		{
			get { return this.idRule; }
			set
			{
				if( this.idRule != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idRule = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public string RuleName
		{
			get { return this.edtName.Text; }
		}

		public string Description
		{
			get { return this.edtDescription.Text; }
		}

		public string Code
		{
			get { return this.edtCode.Text;  }
		}

		#endregion // Properties

		#region Data

		private RuleRow rowRule;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowRule != null )
			{
				this.rowRule.AfterChange -=  rowRule_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowRule != null )
			{
				this.rowRule.AfterChange += rowRule_AfterChange;
			}
		}

		private void rowRule_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowRule = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowRule = this.Data.Rule.FindRow( this.idRule );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:

					if( this.rowRule != null )
					{
						this.edtName.Text = this.rowRule.Name;
						this.edtCode.Text = this.rowRule.Code;
						this.edtDescription.Text = this.rowRule.Description;
					}

					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.edtName.Text = string.Empty;
					this.edtCode.Text = string.Empty;
					this.edtDescription.Text = string.Empty;

					break;

					#endregion // New
			}
		}

		#endregion // View

		#region Handle dataDispatcher events

		private void dataDispatcher_BeforeSetData( object sender, EventArgs e )
		{
			this.OnBeforeSetData();
		}

		private void dataDispatcher_AfterSetData( object sender, EventArgs e )
		{
			this.OnAfterSetData();
		}

		private void dataDispatcher_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}

		private void dataDispatcher_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}

		private void dataDispatcher_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle dataDispatcher events

		#region Handle controls' events

		private void RulePropertiesForm_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.RulePropertiesForm_Warning_RuleNameRequired );
				}

				if( this.edtCode.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.RulePropertiesForm_Warning_RuleCodeRequired );
				}

				if( this.edtDescription.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.RulePropertiesForm_Warning_RuleDescriptionRequired );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}

		#endregion // Handle controls' events
	}
}