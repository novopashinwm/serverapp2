using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleXpRowFactory:
		DataXpRowFactory<RuleXpRow, RuleRow, RuleTable, IRuleData>
	{
		public override RuleXpRow CreateXpRow( int idRule )
		{
			return new RuleXpRow( idRule );
		}
	}
}
