using System.ComponentModel;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule
{
	public class RuleTableModel:
		DataTableModel<RuleXpRow, RuleRow, RuleTable, IRuleData>
	{
		public RuleTableModel( IContainer container )
		{
			container.Add( this );
		}
		public RuleTableModel()
		{

		}

		#region Rows

		private readonly RuleXpRowCollection rows =
			new RuleXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new RuleXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<RuleXpRow, RuleRow, RuleTable, IRuleData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new RuleXpRowFactory Factory
		{
			get { return (RuleXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override RuleTable GetTable()
		{
			return this.Data.Rule;
		}
		public class RuleXpRowCollection:
			DataXpRowCollection<RuleXpRow, RuleRow, RuleTable, IRuleData>
		{

		}
	}
}
