using System.ComponentModel;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleTableModel:
		DataTableModel<MobileRuleXpRow, MobileRuleRow, MobileRuleTable, IRuleData>
	{
		public MobileRuleTableModel( IContainer container )
		{
			container.Add( this );
		}
		public MobileRuleTableModel()
		{

		}

		#region Rows

		private readonly MobileRuleXpRowCollection rows =
			new MobileRuleXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new MobileRuleXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<MobileRuleXpRow, MobileRuleRow, MobileRuleTable, IRuleData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new MobileRuleXpRowFactory Factory
		{
			get { return (MobileRuleXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override MobileRuleTable GetTable()
		{
			return this.Data.MobileRule;
		}
		public class MobileRuleXpRowCollection:
			DataXpRowCollection<MobileRuleXpRow, MobileRuleRow, MobileRuleTable, IRuleData>
		{

		}
	}
}
