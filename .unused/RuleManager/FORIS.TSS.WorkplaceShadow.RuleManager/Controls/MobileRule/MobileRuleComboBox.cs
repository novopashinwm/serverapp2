using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleComboBox:
		DataComboBox<MobileRuleItem, MobileRuleRow, MobileRuleTable, IRuleData>
	{

		protected override MobileRuleTable GetTable()
		{
			return this.Data.MobileRule;
		}

		protected override MobileRuleItem CreateItem( int idMobileRule )
		{
			return new MobileRuleItem( idMobileRule );
		}
	}

	public class MobileRuleItem:
		DataItem<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		public MobileRuleItem( int idMobileRule ): base( idMobileRule )
		{
			
		}

		protected override MobileRuleRow GetRow()
		{
			return this.Data.MobileRule.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.RowRule.Name;
		}
		
	}
}
