using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleXpRowFactory:
		DataXpRowFactory<MobileRuleXpRow, MobileRuleRow, MobileRuleTable, IRuleData>
	{
		public override MobileRuleXpRow CreateXpRow( int idMobileRule )
		{
			return new MobileRuleXpRow( idMobileRule );
		}
	}
}
