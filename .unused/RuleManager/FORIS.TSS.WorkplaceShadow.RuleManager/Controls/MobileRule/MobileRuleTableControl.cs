using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleTableControl: 
		UserControl,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private RuleDataDispatcher dataDispatcher;
		private CommandManager commandManager;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel columnModel;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn columnCheck;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnMobile;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnRule;
		private RuleDataAmbassador daTableModel;
		private MobileRuleXpRowFactory rowFactory;
		private FORIS.TSS.Common.Commands.Command cmdNew;
		private FORIS.TSS.Common.Commands.Command cmdProperties;
		private FORIS.TSS.Common.Commands.Command cmdRemove;
		private FORIS.TSS.Common.Commands.Command cmdCheckAll;
		private FORIS.TSS.Common.Commands.Command cmdUncheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciNewToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciPropertiesToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciRemoveToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllContextMenuItem;
		private ToolStripMenuItem tsmiCheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllContextMenuItem;
		private ContextMenuStrip cmsTable;
		private ToolStripMenuItem tsmiUncheckAll;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton separator1;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton separator2;
		private ToolBarButton tbbRemove;
		private TextColumn columnState;
		private ToolStripSeparator tssCheck;
		private ToolStripMenuItem tsmiNew;
		private ToolStripMenuItem tsmiProperties;
		private ToolStripSeparator tssEdit;
		private ToolStripMenuItem tsmiRemove;
		private CommandInstance ciNewContextMenuItem;
		private CommandInstance ciPropertiesContextMenuItem;
		private CommandInstance ciRemoveContextMenuItem;
		private MobileRuleTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public MobileRuleTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MobileRuleTableControl ) );
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher( this.components );
			this.daTableModel = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule.MobileRuleTableModel( this.components );
			this.rowFactory = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule.MobileRuleXpRowFactory();
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
			this.columnMobile = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnRule = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnState = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.cmsTable = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tssCheck = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.tssEdit = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdNew = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbNew = new System.Windows.Forms.ToolBarButton();
			this.ciNewContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdProperties = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbProperties = new System.Windows.Forms.ToolBarButton();
			this.ciPropertiesContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdRemove = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbRemove = new System.Windows.Forms.ToolBarButton();
			this.ciRemoveContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciUncheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciUncheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar( this.components );
			this.separator1 = new System.Windows.Forms.ToolBarButton();
			this.separator2 = new System.Windows.Forms.ToolBarButton();
			this.cmsTable.SuspendLayout();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daTableModel );
			// 
			// daTableModel
			// 
			this.daTableModel.Item = this.tableModel;
			// 
			// tableModel
			// 
			this.tableModel.Factory = this.rowFactory;
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnMobile,
            this.columnRule,
            this.columnState} );
			// 
			// columnCheck
			// 
			this.columnCheck.Editor = null;
			resources.ApplyResources( this.columnCheck, "columnCheck" );
			this.columnCheck.Width = 20;
			// 
			// columnMobile
			// 
			this.columnMobile.Editable = false;
			this.columnMobile.Editor = null;
			resources.ApplyResources( this.columnMobile, "columnMobile" );
			this.columnMobile.Width = 200;
			// 
			// columnRule
			// 
			this.columnRule.Editable = false;
			this.columnRule.Editor = null;
			resources.ApplyResources( this.columnRule, "columnRule" );
			this.columnRule.Width = 100;
			// 
			// columnState
			// 
			this.columnState.Editable = false;
			this.columnState.Editor = null;
			resources.ApplyResources( this.columnState, "columnState" );
			this.columnState.Width = 200;
			// 
			// cmsTable
			// 
			this.cmsTable.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll,
            this.tssCheck,
            this.tsmiNew,
            this.tsmiProperties,
            this.tssEdit,
            this.tsmiRemove} );
			this.cmsTable.Name = "cmsTable";
			resources.ApplyResources( this.cmsTable, "cmsTable" );
			// 
			// tsmiCheckAll
			// 
			this.tsmiCheckAll.Name = "tsmiCheckAll";
			resources.ApplyResources( this.tsmiCheckAll, "tsmiCheckAll" );
			// 
			// tsmiUncheckAll
			// 
			this.tsmiUncheckAll.Name = "tsmiUncheckAll";
			resources.ApplyResources( this.tsmiUncheckAll, "tsmiUncheckAll" );
			// 
			// tssCheck
			// 
			this.tssCheck.Name = "tssCheck";
			resources.ApplyResources( this.tssCheck, "tssCheck" );
			// 
			// tsmiNew
			// 
			this.tsmiNew.Name = "tsmiNew";
			resources.ApplyResources( this.tsmiNew, "tsmiNew" );
			// 
			// tsmiProperties
			// 
			this.tsmiProperties.Name = "tsmiProperties";
			resources.ApplyResources( this.tsmiProperties, "tsmiProperties" );
			// 
			// tssEdit
			// 
			this.tssEdit.Name = "tssEdit";
			resources.ApplyResources( this.tssEdit, "tssEdit" );
			// 
			// tsmiRemove
			// 
			this.tsmiRemove.Name = "tsmiRemove";
			resources.ApplyResources( this.tsmiRemove, "tsmiRemove" );
			// 
			// cmdNew
			// 
			this.cmdNew.Instances.Add( this.ciNewToolbarButton );
			this.cmdNew.Instances.Add( this.ciNewContextMenuItem );
			this.cmdNew.Manager = this.commandManager;
			this.cmdNew.Execute += new System.EventHandler( this.cmdNew_Execute );
			// 
			// ciNewToolbarButton
			// 
			this.ciNewToolbarButton.Item = this.tbbNew;
			// 
			// tbbNew
			// 
			resources.ApplyResources( this.tbbNew, "tbbNew" );
			this.tbbNew.Name = "tbbNew";
			// 
			// ciNewContextMenuItem
			// 
			this.ciNewContextMenuItem.Item = this.tsmiNew;
			// 
			// cmdProperties
			// 
			this.cmdProperties.Instances.Add( this.ciPropertiesToolbarButton );
			this.cmdProperties.Instances.Add( this.ciPropertiesContextMenuItem );
			this.cmdProperties.Manager = this.commandManager;
			this.cmdProperties.Execute += new System.EventHandler( this.cmdProperties_Execute );
			this.cmdProperties.Update += new System.EventHandler( this.cmdProperties_Update );
			// 
			// ciPropertiesToolbarButton
			// 
			this.ciPropertiesToolbarButton.Item = this.tbbProperties;
			// 
			// tbbProperties
			// 
			resources.ApplyResources( this.tbbProperties, "tbbProperties" );
			this.tbbProperties.Name = "tbbProperties";
			// 
			// ciPropertiesContextMenuItem
			// 
			this.ciPropertiesContextMenuItem.Item = this.tsmiProperties;
			// 
			// cmdRemove
			// 
			this.cmdRemove.Instances.Add( this.ciRemoveToolbarButton );
			this.cmdRemove.Instances.Add( this.ciRemoveContextMenuItem );
			this.cmdRemove.Manager = this.commandManager;
			this.cmdRemove.Execute += new System.EventHandler( this.cmdRemove_Execute );
			this.cmdRemove.Update += new System.EventHandler( this.cmdRemove_Update );
			// 
			// ciRemoveToolbarButton
			// 
			this.ciRemoveToolbarButton.Item = this.tbbRemove;
			// 
			// tbbRemove
			// 
			resources.ApplyResources( this.tbbRemove, "tbbRemove" );
			this.tbbRemove.Name = "tbbRemove";
			// 
			// ciRemoveContextMenuItem
			// 
			this.ciRemoveContextMenuItem.Item = this.tsmiRemove;
			// 
			// cmdCheckAll
			// 
			this.cmdCheckAll.Instances.Add( this.ciCheckAllToolbarButton );
			this.cmdCheckAll.Instances.Add( this.ciCheckAllContextMenuItem );
			this.cmdCheckAll.Manager = this.commandManager;
			this.cmdCheckAll.Execute += new System.EventHandler( this.cmdCheckAll_Execute );
			// 
			// ciCheckAllToolbarButton
			// 
			this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
			// 
			// tbbCheckAll
			// 
			resources.ApplyResources( this.tbbCheckAll, "tbbCheckAll" );
			this.tbbCheckAll.Name = "tbbCheckAll";
			// 
			// ciCheckAllContextMenuItem
			// 
			this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
			// 
			// cmdUncheckAll
			// 
			this.cmdUncheckAll.Instances.Add( this.ciUncheckAllToolbarButton );
			this.cmdUncheckAll.Instances.Add( this.ciUncheckAllContextMenuItem );
			this.cmdUncheckAll.Manager = this.commandManager;
			this.cmdUncheckAll.Execute += new System.EventHandler( this.cmdUncheckAll_Execute );
			// 
			// ciUncheckAllToolbarButton
			// 
			this.ciUncheckAllToolbarButton.Item = this.tbbUncheckAll;
			// 
			// tbbUncheckAll
			// 
			resources.ApplyResources( this.tbbUncheckAll, "tbbUncheckAll" );
			this.tbbUncheckAll.Name = "tbbUncheckAll";
			// 
			// ciUncheckAllContextMenuItem
			// 
			this.ciUncheckAllContextMenuItem.Item = this.tsmiUncheckAll;
			// 
			// table
			// 
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.ContextMenuStrip = this.cmsTable;
			resources.ApplyResources( this.table, "table" );
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.TableModel = this.tableModel;
			// 
			// globalToolbar
			// 
			resources.ApplyResources( this.globalToolbar, "globalToolbar" );
			this.globalToolbar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.separator1,
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove} );
			this.globalToolbar.Divider = false;
			this.globalToolbar.Name = "globalToolbar";
			// 
			// separator1
			// 
			this.separator1.Name = "separator1";
			this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// MobileRuleTableControl
			// 
			this.Controls.Add( this.table );
			this.Controls.Add( this.globalToolbar );
			this.Name = "MobileRuleTableControl";
			resources.ApplyResources( this, "$this" );
			this.cmsTable.ResumeLayout( false );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region IDataItem<IRuleData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRuleData> Members

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public MobileRuleFilter Filter
		{
			get { return (MobileRuleFilter)this.tableModel.Filter; }
			set { this.tableModel.Filter = value; }
		}


		private IMobileRuleEditorProvider editorProvider;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IMobileRuleEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MobileRuleXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int MobileRuleId = ( (MobileRuleXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Properties( MobileRuleId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if (this.table.SelectedItems.Length == 1)
			{
				int MobileRuleId = ( (MobileRuleXpRow)this.table.SelectedItems[0] ).Id;

				if (this.editorProvider != null)
				{
					this.editorProvider.Remove( MobileRuleId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionCheckAll()
		{
			foreach( MobileRuleXpRow row in this.tableModel.Rows )
			{
				row.Checked = true;
			}
		}
		private void ActionUncheckAll()
		{
			foreach( MobileRuleXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Actions

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		#endregion // Events
	}
}