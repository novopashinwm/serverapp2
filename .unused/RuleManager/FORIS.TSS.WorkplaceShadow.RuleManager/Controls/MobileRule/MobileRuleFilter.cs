using System.ComponentModel;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleFilter:
		DataFilter<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		#region Constructor

		public MobileRuleFilter( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public MobileRuleFilter()
		{

		}

		#endregion // Constructor

		#region Properties

		private int mobile;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public int Mobile
		{
			get { return this.mobile; }
			set
			{
				if( this.mobile != value )
				{
					this.OnBeforeChange();

					this.mobile = value;

					this.OnAfterChange();
				}
			}
		}

		#endregion // Properties

		#region Check engine

		public override bool Check( MobileRuleRow row )
		{
			return row.Mobile == this.mobile;
		}

		#endregion // Check engine
	}
}