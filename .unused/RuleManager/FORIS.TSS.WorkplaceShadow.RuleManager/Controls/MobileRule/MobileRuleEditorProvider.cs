using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleEditorProvider:
		EditorProvider<IRuleDataTreater, IRuleData>,
		IMobileRuleEditorProvider
	{
		public MobileRuleEditorProvider()
		{
			
		}
		public MobileRuleEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#region Properties

		private int mobile;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int Mobile
		{
			get { return this.mobile; }
			set { this.mobile = value; }
		}

		#endregion // Properties


		protected override void New()
		{
			using( MobileRulePropertiesForm propertiesForm = new MobileRulePropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				propertiesForm.Mobile = this.mobile;

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							((IRuleDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.MobileRuleInsert(
								propertiesForm.Mobile,
								propertiesForm.Rule,
								propertiesForm.Settings
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileRuleEditorProvider_Exception_CanNotCreateNewMobileRule,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Properties( int idMobileRule )
		{
			using( MobileRulePropertiesForm propertiesForm = new MobileRulePropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.Edit;
				propertiesForm.IdMobileRule = idMobileRule;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							( (IRuleDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.MobileRuleUpdate(
								idMobileRule,
								propertiesForm.Mobile,
								propertiesForm.Rule,
								propertiesForm.Settings
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileRuleEditorProvider_Exception_CanNotUpdateMobileRule,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Remove( int idMobileRule )
		{
			if(
				MessageBox.Show(
					Strings.MobileRuleEditorProvider_Warning_AreYouSureToDeleteMobileRule,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRuleDataTreater Treater =
						( (IRuleDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.MobileRuleDelete( idMobileRule );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileRuleEditorProvider_Exception_CanNotDeleteMobileRule,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idMobileRule )
		{
			throw new NotImplementedException();
			// return this.Data.MobileRule.FindRow( idMobileRule );
		}
	}
}