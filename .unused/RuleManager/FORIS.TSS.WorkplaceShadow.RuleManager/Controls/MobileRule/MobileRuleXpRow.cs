using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRuleXpRow:
		DataXpRow<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		#region Cells

		private readonly Cell cellMobile;
		private readonly Cell cellRule;
		private readonly Cell cellState;

		#endregion // Cells

		public MobileRuleXpRow( int idMobileRule )
			: base( idMobileRule )
		{
			this.cellMobile = new Cell();
			this.cellRule = new Cell();
			this.cellState = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellMobile,
						this.cellRule,
						this.cellState
					}
				);
		}

		protected override MobileRuleRow GetRow()
		{
			return this.Data.MobileRule.FindRow( this.Id );
		}

		#region Data

		protected override void OnBeforeSetData()
		{
			if (this.Row != null)
			{
				if (this.Row.RowMobile != null)
				{
					this.Row.RowMobile.AfterChange -= this.RowMobile_AfterChange;
				}
				if (this.Row.RowRule != null)
				{
					this.Row.RowRule.AfterChange -= this.RowRule_AfterChange;
				}

				this.Row.AfterStateChanged -= this.Row_AfterStateChanged;
			}

			base.OnBeforeSetData();
		}

		protected override void OnAfterSetData()
		{
			base.OnAfterSetData();

			if( this.Row != null )
			{
				this.Row.AfterStateChanged += this.Row_AfterStateChanged;

				if( this.Row.RowMobile != null )
				{
					this.Row.RowMobile.AfterChange += this.RowMobile_AfterChange;
				}
				if( this.Row.RowRule != null )
				{
					this.Row.RowRule.AfterChange += this.RowRule_AfterChange;
				}
			}
		}

		#endregion // Data

		#region Data events

		private void RowMobile_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		private void RowRule_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		private void Row_AfterStateChanged( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data events

		#region View

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellMobile.Text = this.Row.RowMobile.Name;
				this.cellRule.Text = this.Row.RowRule.Name;

				if( this.Row.RowState != null )
				{
					this.cellState.Text =
						this.Row.RowState.State != null
							? this.Row.RowState.State.ToString()
							: string.Empty;
				}
				else
				{
					this.cellState.Text = "�/�";
				}
			}
			else
			{
				this.cellMobile.Text = String.Empty;
				this.cellRule.Text = String.Empty;
				this.cellState.Text = String.Empty;
			}
		}

		#endregion // View
	}
}