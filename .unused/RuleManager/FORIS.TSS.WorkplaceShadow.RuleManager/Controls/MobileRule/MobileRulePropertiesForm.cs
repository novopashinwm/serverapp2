using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule
{
	public class MobileRulePropertiesForm: 
		TssUserForm,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;
		private RuleDataDispatcher dataDispatcher;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
		private System.Windows.Forms.Label lblMobile;
		private System.Windows.Forms.Label lblRule;
		private MobileComboBox cbxMobile;
		private RuleDataAmbassador daMobileComboBox;
		private RuleDataAmbassador daRuleComboBox;
		private SplitContainer splitContainer1;
		private PropertyGrid pgSettings;
		private RuleComboBox cbxRule;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public MobileRulePropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MobileRulePropertiesForm));
			this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher(this.components);
			this.daMobileComboBox = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.cbxMobile = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileComboBox();
			this.daRuleComboBox = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.cbxRule = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule.RuleComboBox();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblMobile = new System.Windows.Forms.Label();
			this.lblRule = new System.Windows.Forms.Label();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.pgSettings = new System.Windows.Forms.PropertyGrid();
			this.panelFill.SuspendLayout();
			this.tableLayoutPanelMain.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add(this.splitContainer1);
			resources.ApplyResources(this.panelFill, "panelFill");
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add(this.daMobileComboBox);
			this.dataDispatcher.Ambassadors.Add(this.daRuleComboBox);
			this.dataDispatcher.UpdateView += new System.EventHandler(this.dataDispatcher_UpdateView);
			this.dataDispatcher.BuildView += new System.EventHandler(this.dataDispatcher_BuildView);
			this.dataDispatcher.DestroyView += new System.EventHandler(this.dataDispatcher_DestroyView);
			this.dataDispatcher.BeforeSetData += new System.EventHandler(this.dataDispatcher_BeforeSetData);
			this.dataDispatcher.AfterSetData += new System.EventHandler(this.dataDispatcher_AfterSetData);
			// 
			// daMobileComboBox
			// 
			this.daMobileComboBox.Item = this.cbxMobile;
			// 
			// cbxMobile
			// 
			resources.ApplyResources(this.cbxMobile, "cbxMobile");
			this.cbxMobile.Name = "cbxMobile";
			// 
			// daRuleComboBox
			// 
			this.daRuleComboBox.Item = this.cbxRule;
			// 
			// cbxRule
			// 
			resources.ApplyResources(this.cbxRule, "cbxRule");
			this.cbxRule.Name = "cbxRule";
			this.cbxRule.SelectedIndexChanged += new System.EventHandler(this.cbxRule_SelectedIndexChanged);
			// 
			// tableLayoutPanelMain
			// 
			resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
			this.tableLayoutPanelMain.Controls.Add(this.lblMobile, 0, 0);
			this.tableLayoutPanelMain.Controls.Add(this.cbxRule, 1, 1);
			this.tableLayoutPanelMain.Controls.Add(this.lblRule, 0, 1);
			this.tableLayoutPanelMain.Controls.Add(this.cbxMobile, 1, 0);
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblMobile
			// 
			resources.ApplyResources(this.lblMobile, "lblMobile");
			this.lblMobile.Name = "lblMobile";
			// 
			// lblRule
			// 
			resources.ApplyResources(this.lblRule, "lblRule");
			this.lblRule.Name = "lblRule";
			// 
			// splitContainer1
			// 
			resources.ApplyResources(this.splitContainer1, "splitContainer1");
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanelMain);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.pgSettings);
			// 
			// pgSettings
			// 
			resources.ApplyResources(this.pgSettings, "pgSettings");
			this.pgSettings.Name = "pgSettings";
			// 
			// MobileRulePropertiesForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "MobileRulePropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MobileRulePropertiesForm_FormClosing);
			this.panelFill.ResumeLayout(false);
			this.tableLayoutPanelMain.ResumeLayout(false);
			this.tableLayoutPanelMain.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		#region IDataItem<IRuleData> Members

		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}
		#endregion // IDataItem<IRuleData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idMobileRule;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdMobileRule
		{
			get { return this.idMobileRule; }
			set
			{
				if( this.idMobileRule != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idMobileRule = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public int Mobile
		{
			get { return this.cbxMobile.SelectedId; }
			set { this.cbxMobile.SelectedId = value; }
		}

		public int Rule
		{
			get { return this.cbxRule.SelectedId; }
		}

		public MobileRuleSettings Settings
		{
			get { return (MobileRuleSettings)this.pgSettings.SelectedObject; }
		}

		#endregion // Properties

		#region Data

		private MobileRuleRow rowMobileRule;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowMobileRule != null )
			{
				this.rowMobileRule.AfterChange -=  rowMobileRule_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowMobileRule != null )
			{
				this.rowMobileRule.AfterChange += rowMobileRule_AfterChange;
			}
		}

		private void rowMobileRule_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowMobileRule = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowMobileRule = this.Data.MobileRule.FindRow( this.idMobileRule );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:
					
					if( this.rowMobileRule == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.cbxMobile.SelectedId = this.rowMobileRule.Mobile;
					this.cbxRule.SelectedId = this.rowMobileRule.Rule;
					this.pgSettings.SelectedObject = this.rowMobileRule.Settings;
					
					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.cbxMobile.SelectedId = 0;
					this.cbxRule.SelectedId = 0;
					this.pgSettings.SelectedObject = null;

					break;

					#endregion // New
			}
		}

		#endregion // View

		#region Handle dataDispatcher events

		private void dataDispatcher_BeforeSetData( object sender, EventArgs e )
		{
			this.OnBeforeSetData();
		}

		private void dataDispatcher_AfterSetData( object sender, EventArgs e )
		{
			this.OnAfterSetData();
		}

		private void dataDispatcher_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}

		private void dataDispatcher_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}

		private void dataDispatcher_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle dataDispatcher events

		#region Handle controls' events

		private void cbxRule_SelectedIndexChanged(object sender, EventArgs e)
		{
			/* ������ ������������ ������� ������ �������� 
			 * � �������� ������ ������� �������� �������,
			 * ���������������� ����������� �������
			 */

			MobileRuleSettings settings = this.Data.CreateMobileRuleSettings( this.cbxRule.SelectedId );

			this.pgSettings.SelectedObject = settings;
		}

		private void MobileRulePropertiesForm_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.cbxMobile.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobileRulePropertiesForm_Warning_MobileRequired );
				}

				if( this.cbxRule.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobileRulePropertiesForm_Warning_RuleRequired );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}

		#endregion // Handle controls' events
	}
}