using System.ComponentModel;
using System.Drawing;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History
{
	public class HistoryTableModel:
		DataTableModel<HistoryXpRow, HistoryRow, HistoryTable, IHistoryData>
	{
		private readonly RowStyle invalidPositionRowStyle;

		public HistoryTableModel( IContainer container ) :
			this()
		{
			container.Add( this );
		}
		public HistoryTableModel()
		{
			this.invalidPositionRowStyle = new RowStyle();
			this.invalidPositionRowStyle.ForeColor = Color.Red;

			this.rows.Inserted += this.rows_Inserted;
		}

		void rows_Inserted( object sender, CollectionChangeEventArgs<HistoryXpRow> e )
		{
			if ( !e.Item.Row.Valid )
			{
				e.Item.RowStyle = this.invalidPositionRowStyle;
			}
		}

		#region Rows

		private readonly HistoryXpRowCollection rows =
			new HistoryXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new HistoryXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<HistoryXpRow, HistoryRow, HistoryTable, IHistoryData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new HistoryXpRowFactory Factory
		{
			get { return (HistoryXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override HistoryTable GetTable()
		{
			return this.Data.History;
		}
		public class HistoryXpRowCollection:
			DataXpRowCollection<HistoryXpRow, HistoryRow, HistoryTable,  IHistoryData>
		{

		}
	}
}
