using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History
{
	public class HistoryXpRowFactory:
		DataXpRowFactory<HistoryXpRow, HistoryRow, HistoryTable, IHistoryData>
	{
		public override HistoryXpRow CreateXpRow( int idHistory )
		{
			return new HistoryXpRow( idHistory );
		}
	}
}
