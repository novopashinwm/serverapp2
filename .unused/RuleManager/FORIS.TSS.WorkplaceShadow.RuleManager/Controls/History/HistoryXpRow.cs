using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History
{
	public class HistoryXpRow: 
		DataXpRow<HistoryRow, HistoryTable, IHistoryData>
	{
		private readonly Cell cellTime;
		private readonly Cell cellSpeed;
		private readonly Cell cellLongitude;
		private readonly Cell cellLatitude;
		private readonly Cell cellAddress;

		public HistoryXpRow( int historyId ): base( historyId )
		{
			this.cellTime = new Cell();
			this.cellSpeed = new Cell();
			this.cellLongitude = new Cell();
			this.cellLatitude = new Cell();
			this.cellAddress = new Cell();

			this.Cells.AddRange(
				new Cell[]
					{
						this.cellTime,
						this.cellSpeed,
						this.cellLongitude,
						this.cellLatitude,
						this.cellAddress
					}
				);
		}

		protected override HistoryRow GetRow()
		{
			return this.Data.History.FindRow( this.Id );
		}

		protected override void OnUpdateView()
		{
			this.cellTime.Text = this.Row.LogTime.ToLocalTime().ToString();
			this.cellSpeed.Text = this.Row.Speed.ToString();
			this.cellLongitude.Text = this.Row.Longitude.ToString();
			this.cellLatitude.Text = this.Row.Latitude.ToString();
            this.cellAddress.Text = this.Row.Address;
		}
	}
}