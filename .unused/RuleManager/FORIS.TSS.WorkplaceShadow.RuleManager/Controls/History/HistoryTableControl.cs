using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History
{
	public class HistoryTableControl: 
		UserControl,
		IDataItem<IHistoryData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private HistoryDataDispatcher dataDispatcher;
		private CommandManager commandManager;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnName;
		private HistoryDataAmbassador daTableModel;
		private HistoryXpRowFactory rowFactory;
		private FORIS.TSS.Common.Commands.Command cmdCheckAll;
		private FORIS.TSS.Common.Commands.Command cmdUncheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllContextMenuItem;
		private ToolStripMenuItem tsmiCheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllContextMenuItem;
		private ContextMenuStrip cmsTable;
		private ToolStripMenuItem tsmiUncheckAll;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton separator1;
		private TextColumn columnController;
		private TextColumn columnPosition;
		private ToolStripSeparator tssCheck;
		private ToolStripMenuItem tsmiNew;
		private ToolStripMenuItem tsmiProperties;
		private ToolStripSeparator tssEdit;
		private ToolStripMenuItem tsmiRemove;
		private CommandInstance ciNewContextMenuItem;
		private CommandInstance ciPropertiesContextMenuItem;
		private CommandInstance ciRemoveContextMenuItem;
		private ColumnModel columnModel;
		private TextColumn columnTime;
		private TextColumn columnSpeed;
		private TextColumn columnLongitude;
		private TextColumn columnLatitude;
		private TextColumn columnAddress;
		private CheckBoxColumn columnCheck;
		private HistoryTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public HistoryTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistoryTableControl));
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager(this.components);
			this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryDataDispatcher(this.components);
			this.daTableModel = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryDataAmbassador();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History.HistoryTableModel(this.components);
			this.rowFactory = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History.HistoryXpRowFactory();
			this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnController = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnPosition = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.cmsTable = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tssCheck = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.tssEdit = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
			this.ciNewContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.ciPropertiesContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.ciRemoveContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
			this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
			this.ciUncheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciUncheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
			this.columnTime = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnSpeed = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLongitude = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnLatitude = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnAddress = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
			this.separator1 = new System.Windows.Forms.ToolBarButton();
			this.cmsTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add(this.daTableModel);
			// 
			// daTableModel
			// 
			this.daTableModel.Item = this.tableModel;
			// 
			// tableModel
			// 
			this.tableModel.Factory = this.rowFactory;
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnName
			// 
			this.columnName.Editable = false;
			this.columnName.Editor = null;
			resources.ApplyResources(this.columnName, "columnName");
			this.columnName.Width = 100;
			// 
			// columnController
			// 
			this.columnController.Editable = false;
			this.columnController.Editor = null;
			resources.ApplyResources(this.columnController, "columnController");
			this.columnController.Width = 70;
			// 
			// columnPosition
			// 
			this.columnPosition.Editable = false;
			this.columnPosition.Editor = null;
			resources.ApplyResources(this.columnPosition, "columnPosition");
			this.columnPosition.Width = 140;
			// 
			// cmsTable
			// 
			this.cmsTable.AccessibleDescription = null;
			this.cmsTable.AccessibleName = null;
			resources.ApplyResources(this.cmsTable, "cmsTable");
			this.cmsTable.BackgroundImage = null;
			this.cmsTable.Font = null;
			this.cmsTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll,
            this.tssCheck,
            this.tsmiNew,
            this.tsmiProperties,
            this.tssEdit,
            this.tsmiRemove});
			this.cmsTable.Name = "cmsTable";
			// 
			// tsmiCheckAll
			// 
			this.tsmiCheckAll.AccessibleDescription = null;
			this.tsmiCheckAll.AccessibleName = null;
			resources.ApplyResources(this.tsmiCheckAll, "tsmiCheckAll");
			this.tsmiCheckAll.BackgroundImage = null;
			this.tsmiCheckAll.Name = "tsmiCheckAll";
			this.tsmiCheckAll.ShortcutKeyDisplayString = null;
			// 
			// tsmiUncheckAll
			// 
			this.tsmiUncheckAll.AccessibleDescription = null;
			this.tsmiUncheckAll.AccessibleName = null;
			resources.ApplyResources(this.tsmiUncheckAll, "tsmiUncheckAll");
			this.tsmiUncheckAll.BackgroundImage = null;
			this.tsmiUncheckAll.Name = "tsmiUncheckAll";
			this.tsmiUncheckAll.ShortcutKeyDisplayString = null;
			// 
			// tssCheck
			// 
			this.tssCheck.AccessibleDescription = null;
			this.tssCheck.AccessibleName = null;
			resources.ApplyResources(this.tssCheck, "tssCheck");
			this.tssCheck.Name = "tssCheck";
			// 
			// tsmiNew
			// 
			this.tsmiNew.AccessibleDescription = null;
			this.tsmiNew.AccessibleName = null;
			resources.ApplyResources(this.tsmiNew, "tsmiNew");
			this.tsmiNew.BackgroundImage = null;
			this.tsmiNew.Name = "tsmiNew";
			this.tsmiNew.ShortcutKeyDisplayString = null;
			// 
			// tsmiProperties
			// 
			this.tsmiProperties.AccessibleDescription = null;
			this.tsmiProperties.AccessibleName = null;
			resources.ApplyResources(this.tsmiProperties, "tsmiProperties");
			this.tsmiProperties.BackgroundImage = null;
			this.tsmiProperties.Name = "tsmiProperties";
			this.tsmiProperties.ShortcutKeyDisplayString = null;
			// 
			// tssEdit
			// 
			this.tssEdit.AccessibleDescription = null;
			this.tssEdit.AccessibleName = null;
			resources.ApplyResources(this.tssEdit, "tssEdit");
			this.tssEdit.Name = "tssEdit";
			// 
			// tsmiRemove
			// 
			this.tsmiRemove.AccessibleDescription = null;
			this.tsmiRemove.AccessibleName = null;
			resources.ApplyResources(this.tsmiRemove, "tsmiRemove");
			this.tsmiRemove.BackgroundImage = null;
			this.tsmiRemove.Name = "tsmiRemove";
			this.tsmiRemove.ShortcutKeyDisplayString = null;
			// 
			// ciNewContextMenuItem
			// 
			this.ciNewContextMenuItem.Item = this.tsmiNew;
			// 
			// ciPropertiesContextMenuItem
			// 
			this.ciPropertiesContextMenuItem.Item = this.tsmiProperties;
			// 
			// ciRemoveContextMenuItem
			// 
			this.ciRemoveContextMenuItem.Item = this.tsmiRemove;
			// 
			// cmdCheckAll
			// 
			this.cmdCheckAll.Instances.Add(this.ciCheckAllToolbarButton);
			this.cmdCheckAll.Instances.Add(this.ciCheckAllContextMenuItem);
			this.cmdCheckAll.Manager = this.commandManager;
			this.cmdCheckAll.Execute += new System.EventHandler(this.cmdCheckAll_Execute);
			// 
			// ciCheckAllToolbarButton
			// 
			this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
			// 
			// tbbCheckAll
			// 
			resources.ApplyResources(this.tbbCheckAll, "tbbCheckAll");
			this.tbbCheckAll.Name = "tbbCheckAll";
			this.tbbCheckAll.Visible = false;
			// 
			// ciCheckAllContextMenuItem
			// 
			this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
			// 
			// cmdUncheckAll
			// 
			this.cmdUncheckAll.Instances.Add(this.ciUncheckAllToolbarButton);
			this.cmdUncheckAll.Instances.Add(this.ciUncheckAllContextMenuItem);
			this.cmdUncheckAll.Manager = this.commandManager;
			this.cmdUncheckAll.Execute += new System.EventHandler(this.cmdUncheckAll_Execute);
			// 
			// ciUncheckAllToolbarButton
			// 
			this.ciUncheckAllToolbarButton.Item = this.tbbUncheckAll;
			// 
			// tbbUncheckAll
			// 
			resources.ApplyResources(this.tbbUncheckAll, "tbbUncheckAll");
			this.tbbUncheckAll.Name = "tbbUncheckAll";
			this.tbbUncheckAll.Visible = false;
			// 
			// ciUncheckAllContextMenuItem
			// 
			this.ciUncheckAllContextMenuItem.Item = this.tsmiUncheckAll;
			// 
			// table
			// 
			this.table.AccessibleDescription = null;
			this.table.AccessibleName = null;
			resources.ApplyResources(this.table, "table");
			this.table.BackColor = System.Drawing.Color.White;
			this.table.BackgroundImage = null;
			this.table.ColumnModel = this.columnModel;
			this.table.ContextMenuStrip = this.cmsTable;
			this.table.EnableHeaderContextMenu = false;
			this.table.Font = null;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.TableModel = this.tableModel;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnTime,
            this.columnSpeed,
            this.columnLongitude,
            this.columnLatitude,
            this.columnAddress});
			// 
			// columnCheck
			// 
			this.columnCheck.Editor = null;
			this.columnCheck.Sortable = false;
			this.columnCheck.Text = null;
			this.columnCheck.Width = 20;
			this.columnCheck.Visible = false;
			// 
			// columnTime
			// 
			this.columnTime.Editable = false;
			this.columnTime.Editor = null;
			this.columnTime.Sortable = false;
			resources.ApplyResources(this.columnTime, "columnTime");
			this.columnTime.Width = 110;
			// 
			// columnSpeed
			// 
			this.columnSpeed.Editable = false;
			this.columnSpeed.Editor = null;
			this.columnSpeed.Sortable = false;
			resources.ApplyResources(this.columnSpeed, "columnSpeed");
			this.columnSpeed.Width = 45;
			// 
			// columnLongitude
			// 
			this.columnLongitude.Editable = false;
			this.columnLongitude.Editor = null;
			this.columnLongitude.Sortable = false;
			resources.ApplyResources(this.columnLongitude, "columnLongitude");
			// 
			// columnLatitude
			// 
			this.columnLatitude.Editable = false;
			this.columnLatitude.Editor = null;
			this.columnLatitude.Sortable = false;
			resources.ApplyResources(this.columnLatitude, "columnLatitude");
			// 
			// columnAddress
			// 
			this.columnAddress.Editable = false;
			this.columnAddress.Editor = null;
			this.columnAddress.Sortable = false;
			resources.ApplyResources(this.columnAddress, "columnAddress");
			// 
			// globalToolbar
			// 
			this.globalToolbar.AccessibleDescription = null;
			this.globalToolbar.AccessibleName = null;
			resources.ApplyResources(this.globalToolbar, "globalToolbar");
			this.globalToolbar.BackgroundImage = null;
			this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.separator1});
			this.globalToolbar.Divider = false;
			this.globalToolbar.Font = null;
			this.globalToolbar.Name = "globalToolbar";
			// 
			// separator1
			// 
			resources.ApplyResources(this.separator1, "separator1");
			this.separator1.Name = "separator1";
			this.separator1.Visible = false;
			this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// HistoryTableControl
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.BackgroundImage = null;
			this.Controls.Add(this.table);
			this.Controls.Add(this.globalToolbar);
			this.Font = null;
			this.Name = "HistoryTableControl";
			this.cmsTable.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region IDataItem<IRuleData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IHistoryData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRuleData> Members

		#region Properties

		[Browsable(false)]
		public HistoryTableModel.HistoryXpRowCollection Rows
		{
			get { return this.tableModel.Rows; }
		}

		[Browsable(true)]
		[DefaultValue(true)]
		public bool ToolbarVisible
		{
			get { return this.globalToolbar.Visible; }
			set { this.globalToolbar.Visible = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public HistoryXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}


		#endregion // Properties

		#region Actions

		private void ActionCheckAll()
		{
			foreach( HistoryXpRow row in this.tableModel.Rows )
			{
				row.Checked = true;
			}
		}
		private void ActionUncheckAll()
		{
			foreach( HistoryXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Actions

		#region Handle controls' events

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		public new event EventHandler DoubleClick
		{
			add { this.table.DoubleClick += value; }
			remove { this.table.DoubleClick -= value; }
		}

		#endregion // Events
	}
}