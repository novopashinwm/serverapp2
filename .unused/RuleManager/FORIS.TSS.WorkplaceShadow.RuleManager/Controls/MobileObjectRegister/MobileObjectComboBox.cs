﻿using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileObjectRegister
{
	public class MobileObjectComboBox :
		DataComboBox<MobileObjectItem, MobileObjectRegisterRow, MobileObjectRegisterTable, IRuleData>
	{

		protected override MobileObjectRegisterTable GetTable()
		{
			return this.Data.MobileObjectRegister;
		}

		protected override MobileObjectItem CreateItem( int globalId )
		{
			return new MobileObjectItem( globalId );
		}
	}

	public class MobileObjectItem:
		DataItem<MobileObjectRegisterRow, MobileObjectRegisterTable, IRuleData>
	{
		public MobileObjectItem( int globalId )
			: base( globalId )
		{
			
		}

		protected override MobileObjectRegisterRow GetRow()
		{
			return this.Data.MobileObjectRegister.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.Name;
		}
	}
}