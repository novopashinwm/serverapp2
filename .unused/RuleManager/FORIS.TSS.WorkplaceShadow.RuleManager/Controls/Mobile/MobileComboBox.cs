using System.Diagnostics;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileComboBox:
		DataComboBox<MobileItem, MobileRow, MobileTable, IRuleData>
	{

		protected override MobileTable GetTable()
		{
			return this.Data.Mobile;
		}

		protected override MobileItem CreateItem( int idMobile )
		{
			return new MobileItem( idMobile );
		}

        public int GetGlobalId(int mobileId)
        {
            MobileRow rowMobile = this.Data.Mobile.FindRow(mobileId);
            if (rowMobile != null)
                return rowMobile.GlobalId;

            Debug.Assert(false, "MobileComboBox.GetGlobalId - can't find needed mobile ID = " + mobileId);
            return 0;
        }
	}

	public class MobileItem:
		DataItem<MobileRow, MobileTable, IRuleData>
	{
		public MobileItem( int idMobile ): base( idMobile )
		{
			
		}

		protected override MobileRow GetRow()
		{
			return this.Data.Mobile.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.Name;
		}
		
	}
}
