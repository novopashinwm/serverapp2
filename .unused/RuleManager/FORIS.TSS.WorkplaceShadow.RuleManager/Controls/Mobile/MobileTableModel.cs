using System.ComponentModel;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileTableModel:
		DataTableModel<MobileXpRow, MobileRow, MobileTable, IRuleData>
	{
		public MobileTableModel( IContainer container )
		{
			container.Add( this );
		}
		public MobileTableModel()
		{

		}

		#region Rows

		private readonly MobileXpRowCollection rows =
			new MobileXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new MobileXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<MobileXpRow, MobileRow, MobileTable, IRuleData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new MobileXpRowFactory Factory
		{
			get { return (MobileXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override MobileTable GetTable()
		{
			return this.Data.Mobile;
		}
		public class MobileXpRowCollection:
			DataXpRowCollection<MobileXpRow, MobileRow, MobileTable,  IRuleData>
		{

		}
	}
}
