using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileFilter: 
		DataFilter<MobileRow, MobileTable, IRuleData>
	{
		public override bool Check( MobileRow row )
		{
			return true;
		}
	}
}
