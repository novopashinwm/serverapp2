using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.Common;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileTableControl: 
		UserControl,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		private IContainer components;
		private RuleDataDispatcher dataDispatcher;
		private CommandManager commandManager;
		private ColumnModel columnModel;
		private CheckBoxColumn columnCheck;
		private TextColumn columnName;
		private RuleDataAmbassador daTableModel;
		private MobileXpRowFactory rowFactory;
		private Command cmdNew;
		private Command cmdProperties;
		private Command cmdRemove;
		private Command cmdCheckAll;
		private Command cmdUncheckAll;
		private CommandInstance ciNewToolbarButton;
		private CommandInstance ciPropertiesToolbarButton;
		private CommandInstance ciRemoveToolbarButton;
		private CommandInstance ciCheckAllToolbarButton;
		private CommandInstance ciCheckAllContextMenuItem;
        private ToolStripMenuItem tsmiCheckAll;
		private ContextMenuStrip cmsTable;
		private ToolStripMenuItem tsmiUncheckAll;
		private Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton separator1;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton separator2;
		private ToolBarButton tbbRemove;
		private TextColumn columnController;
		private TextColumn columnPosition;
		private ToolStripSeparator tssCheck;
		private ToolStripMenuItem tsmiNew;
		private ToolStripMenuItem tsmiProperties;
		private ToolStripSeparator tssEdit;
		private ToolStripMenuItem tsmiRemove;
		private CommandInstance ciNewContextMenuItem;
		private CommandInstance ciPropertiesContextMenuItem;
        private CommandInstance ciRemoveContextMenuItem;
        private CommandInstance ciUncheckAll;
        private CommandInstance citsmiUncheckAll;
		private TextColumn columnMobileObject;
		private MobileTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public MobileTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );

			this.tableModel.Rows.Inserted += this.Rows_Inserted;
			this.tableModel.Rows.Removing += this.Rows_Removing;
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MobileTableControl ) );
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher( this.components );
			this.daTableModel = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.tableModel = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileTableModel( this.components );
			this.rowFactory = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileXpRowFactory();
			this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
			this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
			this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnController = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnPosition = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.columnMobileObject = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
			this.cmsTable = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
			this.tssCheck = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.tssEdit = new System.Windows.Forms.ToolStripSeparator();
			this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
			this.cmdNew = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbNew = new System.Windows.Forms.ToolBarButton();
			this.ciNewContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdProperties = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbProperties = new System.Windows.Forms.ToolBarButton();
			this.ciPropertiesContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdRemove = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbRemove = new System.Windows.Forms.ToolBarButton();
			this.ciRemoveContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
			this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
			this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciUncheckAll = new FORIS.TSS.Common.Commands.CommandInstance();
			this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
			this.citsmiUncheckAll = new FORIS.TSS.Common.Commands.CommandInstance();
			this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
			this.globalToolbar = new System.Windows.Forms.GlobalToolbar( this.components );
			this.separator1 = new System.Windows.Forms.ToolBarButton();
			this.separator2 = new System.Windows.Forms.ToolBarButton();
			this.cmsTable.SuspendLayout();
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).BeginInit();
			this.SuspendLayout();
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daTableModel );
			// 
			// daTableModel
			// 
			this.daTableModel.Item = this.tableModel;
			// 
			// tableModel
			// 
			this.tableModel.Factory = this.rowFactory;
			this.tableModel.SelectionAdjuster = null;
			// 
			// columnModel
			// 
			this.columnModel.Columns.AddRange( new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnController,
            this.columnPosition,
            this.columnMobileObject} );
			// 
			// columnCheck
			// 
			this.columnCheck.Editor = null;
			resources.ApplyResources( this.columnCheck, "columnCheck" );
			this.columnCheck.Width = 20;
			// 
			// columnName
			// 
			this.columnName.Editable = false;
			this.columnName.Editor = null;
			resources.ApplyResources( this.columnName, "columnName" );
			this.columnName.Width = 100;
			// 
			// columnController
			// 
			this.columnController.Editable = false;
			this.columnController.Editor = null;
			resources.ApplyResources( this.columnController, "columnController" );
			this.columnController.Width = 70;
			// 
			// columnPosition
			// 
			this.columnPosition.Editable = false;
			this.columnPosition.Editor = null;
			resources.ApplyResources( this.columnPosition, "columnPosition" );
			this.columnPosition.Width = 140;
			// 
			// columnMobileObject
			// 
			this.columnMobileObject.Editor = null;
			resources.ApplyResources( this.columnMobileObject, "columnMobileObject" );
			// 
			// cmsTable
			// 
			this.cmsTable.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll,
            this.tssCheck,
            this.tsmiNew,
            this.tsmiProperties,
            this.tssEdit,
            this.tsmiRemove} );
			this.cmsTable.Name = "cmsTable";
			resources.ApplyResources( this.cmsTable, "cmsTable" );
			// 
			// tsmiCheckAll
			// 
			this.tsmiCheckAll.Name = "tsmiCheckAll";
			resources.ApplyResources( this.tsmiCheckAll, "tsmiCheckAll" );
			// 
			// tsmiUncheckAll
			// 
			this.tsmiUncheckAll.Name = "tsmiUncheckAll";
			resources.ApplyResources( this.tsmiUncheckAll, "tsmiUncheckAll" );
			// 
			// tssCheck
			// 
			this.tssCheck.Name = "tssCheck";
			resources.ApplyResources( this.tssCheck, "tssCheck" );
			// 
			// tsmiNew
			// 
			this.tsmiNew.Name = "tsmiNew";
			resources.ApplyResources( this.tsmiNew, "tsmiNew" );
			// 
			// tsmiProperties
			// 
			this.tsmiProperties.Name = "tsmiProperties";
			resources.ApplyResources( this.tsmiProperties, "tsmiProperties" );
			// 
			// tssEdit
			// 
			this.tssEdit.Name = "tssEdit";
			resources.ApplyResources( this.tssEdit, "tssEdit" );
			// 
			// tsmiRemove
			// 
			this.tsmiRemove.Name = "tsmiRemove";
			resources.ApplyResources( this.tsmiRemove, "tsmiRemove" );
			// 
			// cmdNew
			// 
			this.cmdNew.Instances.Add( this.ciNewToolbarButton );
			this.cmdNew.Instances.Add( this.ciNewContextMenuItem );
			this.cmdNew.Manager = this.commandManager;
			this.cmdNew.Execute += new System.EventHandler( this.cmdNew_Execute );
			// 
			// ciNewToolbarButton
			// 
			this.ciNewToolbarButton.Item = this.tbbNew;
			// 
			// tbbNew
			// 
			resources.ApplyResources( this.tbbNew, "tbbNew" );
			this.tbbNew.Name = "tbbNew";
			// 
			// ciNewContextMenuItem
			// 
			this.ciNewContextMenuItem.Item = this.tsmiNew;
			// 
			// cmdProperties
			// 
			this.cmdProperties.Instances.Add( this.ciPropertiesToolbarButton );
			this.cmdProperties.Instances.Add( this.ciPropertiesContextMenuItem );
			this.cmdProperties.Manager = this.commandManager;
			this.cmdProperties.Execute += new System.EventHandler( this.cmdProperties_Execute );
			this.cmdProperties.Update += new System.EventHandler( this.cmdProperties_Update );
			// 
			// ciPropertiesToolbarButton
			// 
			this.ciPropertiesToolbarButton.Item = this.tbbProperties;
			// 
			// tbbProperties
			// 
			resources.ApplyResources( this.tbbProperties, "tbbProperties" );
			this.tbbProperties.Name = "tbbProperties";
			// 
			// ciPropertiesContextMenuItem
			// 
			this.ciPropertiesContextMenuItem.Item = this.tsmiProperties;
			// 
			// cmdRemove
			// 
			this.cmdRemove.Instances.Add( this.ciRemoveToolbarButton );
			this.cmdRemove.Instances.Add( this.ciRemoveContextMenuItem );
			this.cmdRemove.Manager = this.commandManager;
			this.cmdRemove.Execute += new System.EventHandler( this.cmdRemove_Execute );
			this.cmdRemove.Update += new System.EventHandler( this.cmdRemove_Update );
			// 
			// ciRemoveToolbarButton
			// 
			this.ciRemoveToolbarButton.Item = this.tbbRemove;
			// 
			// tbbRemove
			// 
			resources.ApplyResources( this.tbbRemove, "tbbRemove" );
			this.tbbRemove.Name = "tbbRemove";
			// 
			// ciRemoveContextMenuItem
			// 
			this.ciRemoveContextMenuItem.Item = this.tsmiRemove;
			// 
			// cmdCheckAll
			// 
			this.cmdCheckAll.Instances.Add( this.ciCheckAllToolbarButton );
			this.cmdCheckAll.Instances.Add( this.ciCheckAllContextMenuItem );
			this.cmdCheckAll.Manager = this.commandManager;
			this.cmdCheckAll.Execute += new System.EventHandler( this.cmdCheckAll_Execute );
			// 
			// ciCheckAllToolbarButton
			// 
			this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
			// 
			// tbbCheckAll
			// 
			resources.ApplyResources( this.tbbCheckAll, "tbbCheckAll" );
			this.tbbCheckAll.Name = "tbbCheckAll";
			// 
			// ciCheckAllContextMenuItem
			// 
			this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
			// 
			// cmdUncheckAll
			// 
			this.cmdUncheckAll.Instances.Add( this.ciUncheckAll );
			this.cmdUncheckAll.Instances.Add( this.citsmiUncheckAll );
			this.cmdUncheckAll.Manager = this.commandManager;
			this.cmdUncheckAll.Execute += new System.EventHandler( this.cmdUncheckAll_Execute );
			// 
			// ciUncheckAll
			// 
			this.ciUncheckAll.Item = this.tbbUncheckAll;
			// 
			// tbbUncheckAll
			// 
			resources.ApplyResources( this.tbbUncheckAll, "tbbUncheckAll" );
			this.tbbUncheckAll.Name = "tbbUncheckAll";
			// 
			// citsmiUncheckAll
			// 
			this.citsmiUncheckAll.Item = this.tsmiUncheckAll;
			// 
			// table
			// 
			this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
			this.table.BackColor = System.Drawing.Color.White;
			this.table.ColumnModel = this.columnModel;
			this.table.ContextMenuStrip = this.cmsTable;
			resources.ApplyResources( this.table, "table" );
			this.table.EnableHeaderContextMenu = false;
			this.table.FullRowSelect = true;
			this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
			this.table.HeaderContextMenu = null;
			this.table.Name = "table";
			this.table.NoItemsText = "";
			this.table.TableModel = this.tableModel;
			// 
			// globalToolbar
			// 
			resources.ApplyResources( this.globalToolbar, "globalToolbar" );
			this.globalToolbar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.separator1,
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove} );
			this.globalToolbar.Divider = false;
			this.globalToolbar.Name = "globalToolbar";
			// 
			// separator1
			// 
			this.separator1.Name = "separator1";
			this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// MobileTableControl
			// 
			this.Controls.Add( this.table );
			this.Controls.Add( this.globalToolbar );
			this.Name = "MobileTableControl";
			resources.ApplyResources( this, "$this" );
			this.cmsTable.ResumeLayout( false );
			( (System.ComponentModel.ISupportInitialize)( this.table ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region IDataItem<IRuleData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRuleData> Members

		#region Properties

		[Browsable(false)]
		public MobileTableModel.MobileXpRowCollection Rows
		{
			get { return this.tableModel.Rows; }
		}

		private IMobileEditorProvider editorProvider;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IMobileEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable(true)]
		[DefaultValue(true)]
		public bool ToolbarVisible
		{
			get { return this.globalToolbar.Visible; }
			set { this.globalToolbar.Visible = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MobileXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		private List<int> checkedRows = new List<int>();
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public List<int> CheckedRows
		{
			get { return checkedRows; }
			set { this.checkedRows = value; }
		}

        /// <summary>
        /// ����������� ���� ������� �������� ��������
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	    public new ContextMenuStrip ContextMenu
	    {
	        get
	        {
	            return this.cmsTable;
	        }
	    }


		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int MobileId = ( (MobileXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Properties( MobileId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if (this.table.SelectedItems.Length == 1)
			{
				int MobileId = ( (MobileXpRow)this.table.SelectedItems[0] ).Id;

				if (this.editorProvider != null)
				{
					this.editorProvider.Remove( MobileId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionCheckAll()
		{
			this.allowColumnCheckedChangedEvent = false;

			foreach ( MobileXpRow mobileRow in this.tableModel.Rows )
			{
				mobileRow.Checked = true;
			}

			this.allowColumnCheckedChangedEvent = true;

			this.OnColumnCheckedChanged();
		}
		private void ActionUncheckAll()
		{
			this.allowColumnCheckedChangedEvent = false;

			foreach ( MobileXpRow mobileRow in this.tableModel.Rows )
			{
				mobileRow.Checked = false;
			}

			this.allowColumnCheckedChangedEvent = true;

			this.OnColumnCheckedChanged();
		}

		#endregion // Actions

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}


		void Rows_Inserted( object sender, CollectionChangeEventArgs<MobileXpRow> e )
		{
			e.Item.CheckedChanged += this.Item_CheckedChanged;
		}

		void Rows_Removing( object sender, CollectionChangeEventArgs<MobileXpRow> e )
		{
			e.Item.CheckedChanged -= this.Item_CheckedChanged;
		}
        
		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		public new event EventHandler DoubleClick
		{
			add { this.table.DoubleClick += value; }
			remove { this.table.DoubleClick -= value; }
		}

		#endregion // Events

		void Item_CheckedChanged( object sender, EventArgs e )
		{
			MobileXpRow mobileXpRow = (MobileXpRow)sender;

			if ( mobileXpRow.Checked )
			{
				this.checkedRows.Add( mobileXpRow.Id );
			}
			else
			{
				this.checkedRows.Remove( mobileXpRow.Id );
			}

			this.OnColumnCheckedChanged();
		}

		private bool allowColumnCheckedChangedEvent = true;
		public event EventHandler ColumnCheckedChanged;
		private void OnColumnCheckedChanged()
		{
			if ( this.ColumnCheckedChanged != null && this.allowColumnCheckedChangedEvent )
			{
				this.ColumnCheckedChanged( this, EventArgs.Empty );
			}
		}
	}
}