using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileEditorProvider:
		EditorProvider<IRuleDataTreater, IRuleData>,
		IMobileEditorProvider
	{
		public MobileEditorProvider()
		{
			
		}
		public MobileEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		protected override void New()
		{
			using( MobilePropertiesForm propertiesForm = new MobilePropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							((IRuleDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.MobileInsert(
								propertiesForm.MobileType,
								propertiesForm.MobileName,
								propertiesForm.Controller,
								propertiesForm.GlobalId
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileEditorProvider_Exception_CanNotCreateNewMobile,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Properties( int idMobile )
		{
			using( MobilePropertiesForm propertiesForm = new MobilePropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.Edit;
				propertiesForm.IdMobile = idMobile;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRuleDataTreater Treater =
							( (IRuleDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.MobileUpdate(
								idMobile,
								propertiesForm.MobileType,
								propertiesForm.MobileName,
								propertiesForm.Controller,
								propertiesForm.GlobalId
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileEditorProvider_Exception_CanNotUpdateMobile,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Remove( int idMobile )
		{
			if(
				MessageBox.Show(
					Strings.MobileEditorProvider_Warning_AreYouSureToDeleteMobile,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRuleDataTreater Treater =
						( (IRuleDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.MobileDelete( idMobile );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.MobileEditorProvider_Exception_CanNotDeleteMobile,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idMobile )
		{
			throw new NotImplementedException();
			// return this.Data.Mobile.FindRow( idMobile );
		}
	}
}