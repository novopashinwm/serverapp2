using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileObjectRegister;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType;
using FORIS.TSS.WorkplaceShadow.TspTerminal.Controls.Controller;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobilePropertiesForm: 
		TssUserForm,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private new IContainer components;
		private RuleDataDispatcher ruleDataDispatcher;
		private ControllerDataDispatcher controllerDataDispatcher;
		private TableLayoutPanel tableLayoutPanelMain;
		private Label lblName;
		private Label lblController;
		private TextBox edtName;
		private ControllerDataAmbassador daControllerComboBox;
		private Label lblMobileType;
		private MobileTypeComboBox cbxMobileType;
		private MobileTypeXpRowFactory mobileTypeXpRowFactory;
		private MobileTypeFilter mobileTypeFilter;
		private RuleDataAmbassador daMobileTypeFilter;
		private Label lblMobileObject;
		private MobileObjectComboBox cbxMobileObject;
		private RuleDataAmbassador daMobileObjectComboBox;
		private ControllerComboBox cbxController;
		private SpecialItem siNoController;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public MobilePropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MobilePropertiesForm ) );
			this.ruleDataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher( this.components );
			this.daMobileTypeFilter = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.mobileTypeFilter = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType.MobileTypeFilter();
			this.daMobileObjectComboBox = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.cbxMobileObject = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileObjectRegister.MobileObjectComboBox();
			this.controllerDataDispatcher = new FORIS.TSS.Helpers.TspTerminal.Data.Controller.ControllerDataDispatcher( this.components );
			this.daControllerComboBox = new FORIS.TSS.Helpers.TspTerminal.Data.Controller.ControllerDataAmbassador();
			this.cbxController = new FORIS.TSS.WorkplaceShadow.TspTerminal.Controls.Controller.ControllerComboBox();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblName = new System.Windows.Forms.Label();
			this.lblController = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblMobileType = new System.Windows.Forms.Label();
			this.cbxMobileType = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType.MobileTypeComboBox();
			this.lblMobileObject = new System.Windows.Forms.Label();
			this.mobileTypeXpRowFactory = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileType.MobileTypeXpRowFactory();
			this.siNoController = new FORIS.TSS.WorkplaceShadow.Controls.SpecialItem();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			resources.ApplyResources( this.panelFill, "panelFill" );
			// 
			// ruleDataDispatcher
			// 
			this.ruleDataDispatcher.Ambassadors.Add( this.daMobileTypeFilter );
			this.ruleDataDispatcher.Ambassadors.Add( this.daMobileObjectComboBox );
			this.ruleDataDispatcher.UpdateView += new System.EventHandler( this.dataDispatcher_UpdateView );
			this.ruleDataDispatcher.BuildView += new System.EventHandler( this.dataDispatcher_BuildView );
			this.ruleDataDispatcher.AfterSetData += new System.EventHandler( this.dataDispatcher_AfterSetData );
			this.ruleDataDispatcher.DestroyView += new System.EventHandler( this.dataDispatcher_DestroyView );
			this.ruleDataDispatcher.BeforeSetData += new System.EventHandler( this.dataDispatcher_BeforeSetData );
			// 
			// daMobileTypeFilter
			// 
			this.daMobileTypeFilter.Item = this.mobileTypeFilter;
			// 
			// daMobileObjectComboBox
			// 
			this.daMobileObjectComboBox.Item = this.cbxMobileObject;
			// 
			// cbxMobileObject
			// 
			resources.ApplyResources( this.cbxMobileObject, "cbxMobileObject" );
			this.cbxMobileObject.Name = "cbxMobileObject";
			// 
			// controllerDataDispatcher
			// 
			this.controllerDataDispatcher.Ambassadors.Add( this.daControllerComboBox );
			// 
			// daControllerComboBox
			// 
			this.daControllerComboBox.Item = this.cbxController;
			// 
			// cbxController
			// 
			resources.ApplyResources( this.cbxController, "cbxController" );
			this.cbxController.FirstItems.Add( this.siNoController );
			this.cbxController.Name = "cbxController";
			// 
			// siNoController
			// 
			this.siNoController.Text = "Not specified";
			// 
			// tableLayoutPanelMain
			// 
			resources.ApplyResources( this.tableLayoutPanelMain, "tableLayoutPanelMain" );
			this.tableLayoutPanelMain.Controls.Add( this.lblName, 0, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.cbxController, 1, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.lblController, 0, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.edtName, 1, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.lblMobileType, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.cbxMobileType, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.lblMobileObject, 0, 3 );
			this.tableLayoutPanelMain.Controls.Add( this.cbxMobileObject, 1, 3 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblName
			// 
			resources.ApplyResources( this.lblName, "lblName" );
			this.lblName.Name = "lblName";
			// 
			// lblController
			// 
			resources.ApplyResources( this.lblController, "lblController" );
			this.lblController.Name = "lblController";
			// 
			// edtName
			// 
			resources.ApplyResources( this.edtName, "edtName" );
			this.edtName.Name = "edtName";
			// 
			// lblMobileType
			// 
			resources.ApplyResources( this.lblMobileType, "lblMobileType" );
			this.lblMobileType.Name = "lblMobileType";
			// 
			// cbxMobileType
			// 
			resources.ApplyResources( this.cbxMobileType, "cbxMobileType" );
			this.cbxMobileType.Name = "cbxMobileType";
			// 
			// lblMobileObject
			// 
			resources.ApplyResources( this.lblMobileObject, "lblMobileObject" );
			this.lblMobileObject.Name = "lblMobileObject";
			// 
			// mobileTypeXpRowFactory
			// 
			this.mobileTypeXpRowFactory.Filter = this.mobileTypeFilter;
			this.mobileTypeXpRowFactory.ViewControl = this.cbxMobileType;
			// 
			// MobilePropertiesForm
			// 
			resources.ApplyResources( this, "$this" );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "MobilePropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.MobilePropertiesForm_FormClosing );
			this.Controls.SetChildIndex( this.panelFill, 0 );
			this.Controls.SetChildIndex( this.tableLayoutPanelMain, 0 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		#region IDataItem<IRuleData> Members

		public IRuleData Data
		{
			get { return this.ruleDataDispatcher.Data; }
			set
			{
				this.controllerDataDispatcher.Data = value;
				this.ruleDataDispatcher.Data = value;
			}
		}
		
		#endregion // IDataItem<IRuleData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idMobile;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdMobile
		{
			get { return this.idMobile; }
			set
			{
				if( this.idMobile != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idMobile = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public int MobileType
		{
			get { return this.cbxMobileType.SelectedId; }
		}

		public string MobileName
		{
			get { return this.edtName.Text; }
		}

		public int? Controller
		{
			get
			{
				return
					this.cbxController.SelectedItem != this.siNoController
						?
							this.cbxController.SelectedId
						:
							(int?)null;
			}
		}

		public int GlobalId
		{
			get { return this.cbxMobileObject.SelectedId; }
		}

		#endregion // Properties

		#region Data

		private MobileRow rowMobile;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowMobile != null )
			{
				this.rowMobile.AfterChange -=  rowMobile_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowMobile != null )
			{
				this.rowMobile.AfterChange += rowMobile_AfterChange;
			}
		}

		private void rowMobile_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowMobile = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowMobile = this.Data.Mobile.FindRow( this.idMobile );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:
					
					if( this.rowMobile == null )
					{
						throw new ApplicationException( Strings.NoDataForEdit );
					}

					this.cbxMobileType.SelectedId	= this.rowMobile.MobileType;
					this.edtName.Text				= this.rowMobile.Name;
					this.cbxMobileObject.SelectedId = this.rowMobile.GlobalId;

					if ( this.rowMobile.Controller != null )
						this.cbxController.SelectedId = (int)this.rowMobile.Controller;
					else
						this.cbxController.SelectedItem = this.siNoController;
					
					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.cbxMobileType.SelectedId	= 0;
					this.edtName.Text				= string.Empty;
					this.cbxMobileObject.SelectedId = 0;

					this.cbxController.SelectedItem = this.siNoController;

					break;

					#endregion // New
			}
		}

		#endregion // View

		#region Handle ruleDataDispatcher events

		private void dataDispatcher_BeforeSetData( object sender, EventArgs e )
		{
			this.OnBeforeSetData();
		}

		private void dataDispatcher_AfterSetData( object sender, EventArgs e )
		{
			this.OnAfterSetData();
		}

		private void dataDispatcher_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}

		private void dataDispatcher_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}

		private void dataDispatcher_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle ruleDataDispatcher events

		#region Handle controls' events

		private void MobilePropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if ( this.cbxMobileType.SelectedItem == null )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobilePropertiesForm_Warning_MobileTypeRequired );
				}

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobilePropertiesForm_Warning_MobileNameRequired );
				}

				if( this.cbxController.SelectedItem == null )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobilePropertiesForm_Warning_MobileControllerRequired );
				}

				if ( this.cbxMobileObject.SelectedItem == null )
				{
					e.Cancel = true;

					Warnings.Add( Strings.MobilePropertiesForm_Warning_GlobalIdRequired );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}

		#endregion // Handle controls' events
	}
}