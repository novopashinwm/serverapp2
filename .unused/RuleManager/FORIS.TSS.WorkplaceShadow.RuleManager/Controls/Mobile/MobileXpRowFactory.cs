using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileXpRowFactory:
		DataXpRowFactory<MobileXpRow, MobileRow, MobileTable, IRuleData>
	{
		public override MobileXpRow CreateXpRow( int idMobile )
		{
			return new MobileXpRow( idMobile );
		}
	}
}
