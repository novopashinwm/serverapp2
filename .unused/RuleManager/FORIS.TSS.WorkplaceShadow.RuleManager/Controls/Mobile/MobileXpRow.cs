using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile
{
	public class MobileXpRow:
		DataXpRow<MobileRow, MobileTable, IRuleData>
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellController;
		private readonly Cell cellPosition;
		private readonly Cell cellMobileObject;

		#endregion // Cells

		public MobileXpRow( int idMobile )
			: base( idMobile )
		{
			this.cellName = new Cell();
			this.cellController = new Cell();
			this.cellPosition = new Cell();
			this.cellMobileObject = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellName,
						this.cellController,
						this.cellPosition,
						this.cellMobileObject
					}
				);
		}

		protected override MobileRow GetRow()
		{
			return this.Data.Mobile.FindRow( this.Id );
		}

		#region Data

		protected override void OnBeforeSetData()
		{
			if( this.Row != null )
			{
				if (this.Row.RowController != null)
				{
					this.Row.RowController.AfterChange -= this.RowController_AfterChange;
				}

				this.Row.PositionChanged -= this.Row_PositionChanged;
			}

			base.OnBeforeSetData();
		}

		protected override void OnAfterSetData()
		{
			base.OnAfterSetData();

			if( this.Row != null )
			{
				this.Row.PositionChanged += this.Row_PositionChanged;

				if( this.Row.RowController != null )
				{
					this.Row.RowController.AfterChange += this.RowController_AfterChange;
				}
			}
		}

		#endregion Data

		#region Data events

		private void Row_PositionChanged( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}
		private void RowController_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data events

		#region View

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellName.Text = this.Row.Name;

				this.cellController.Text =
					this.Row.Controller != null
						? this.Row.RowController != null
						  	? this.Row.RowController.Number.ToString()
						  	: Strings.EmptyContoller
						: string.Empty;

				if( this.Row.RowPosition == null )
				{
					this.cellPosition.Text = Strings.EmptyPosition;
				}
				else if( this.Row.RowPosition.Empty )
				{
                    this.cellPosition.Text = Strings.EmptyPosition;
                }
				else
				{
					this.cellPosition.Text =
						((DateTime)this.Row.RowPosition.Time).ToLocalTime().ToString( "dd.MM.yy HH:mm:ss" );
				}

				this.cellMobileObject.Text = this.Row.RowMobileObject.Name;
			}
			else
			{
				this.cellName.Text			= String.Empty;
				this.cellController.Text	= String.Empty;
				this.cellPosition.Text		= String.Empty;
				this.cellMobileObject.Text	= String.Empty;
			}
		}

		#endregion // View
	}
}