using FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Data;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.TspTerminal;

namespace FORIS.TSS.WorkplaceShadow.RuleManager
{
	public interface IRuleClientDataProvider:
		IClientDataProvider,
		IControllerClientDataProvider,
		IRuleDataSupplierProvider
	{
		RuleClientData GetRuleClientData();
	}
}
