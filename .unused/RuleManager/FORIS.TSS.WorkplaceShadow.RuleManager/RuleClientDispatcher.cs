﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.RuleManager
{
	public class RuleClientDispatcher : 
        ClientDispatcher<IRuleClientDataProvider>
	{
		public RuleClientDispatcher()
		{
			
		}

		public RuleClientDispatcher( IContainer container )
		{
			container.Add( this );
		}

		#region Ambassadors

		private readonly RuleClientAmbassadorCollection ambassadors =
			new RuleClientAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new RuleClientAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem<IRuleClientDataProvider>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors
	}

	public class RuleClientAmbassadorCollection : AmbassadorCollection<IClientItem<IRuleClientDataProvider>>
	{
		public new RuleClientAmbassador this[ int index ]
		{
			get { return (RuleClientAmbassador)base[index]; }
		}
	}

	public class RuleClientAmbassador : Ambassador<IClientItem<IRuleClientDataProvider>>
	{
		
	}
}