using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.RuleManager;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule;
using FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.RuleManager.Rule
{
	public class RB_Rule: 
		PluginForm,
		IClientItem<IRuleClientDataProvider>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;
		private RuleClientDataProviderDispatcher clientDispatcher;
		private RuleDataSupplier ruleDataSupplier;
		private RuleData ruleData;
		private RuleDataDispatcher ruleDataDispatcher;
		private RuleTableControl ctrlRuleTable;
		private RuleDataAmbassador daRuleEditorProvider;
		private RuleDataAmbassador daRuleTableControl;
		private MobileTableControl ctrlMobileTable;
		private RuleEditorProvider ruleEditorProvider;
		private RuleDataAmbassador daMobileEditorProvider;
		private RuleDataAmbassador daMobileTableControl;
		private MobileRuleTableControl ctrlMobileRuleTable;
		private MobileEditorProvider mobileEditorProvider;
		private MobileRuleFilter mobileRuleFilter;
		private RuleDataAmbassador daMobileRuleTableControl;
		private RuleDataAmbassador daMobileRuleEditorProvider;
		private MobileRuleEditorProvider mobileRuleEditorProvider;

		#endregion // Controls & Components

		#region Constructor & Dispose
		
		public RB_Rule()
		{
			InitializeComponent();
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RB_Rule));
			this.ctrlMobileTable = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileTableControl();
			this.mobileEditorProvider = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileEditorProvider(this.components);
			this.ctrlRuleTable = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule.RuleTableControl();
			this.ruleEditorProvider = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Rule.RuleEditorProvider(this.components);
			this.clientDispatcher = new FORIS.TSS.RuleManager.Rule.RuleClientDataProviderDispatcher(this.components);
			this.ruleDataSupplier = new FORIS.TSS.WorkplaceShadow.RuleManager.Data.Rule.RuleDataSupplier(this.components);
			this.ruleData = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleData(this.components);
			this.ruleDataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher(this.components);
			this.daRuleEditorProvider = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.daRuleTableControl = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.daMobileEditorProvider = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.daMobileTableControl = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.daMobileRuleEditorProvider = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.mobileRuleEditorProvider = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule.MobileRuleEditorProvider(this.components);
			this.daMobileRuleTableControl = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
			this.ctrlMobileRuleTable = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule.MobileRuleTableControl();
			this.mobileRuleFilter = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.MobileRule.MobileRuleFilter(this.components);
			this.SuspendLayout();
			// 
			// ctrlMobileTable
			// 
			this.ctrlMobileTable.AccessibleDescription = null;
			this.ctrlMobileTable.AccessibleName = null;
			resources.ApplyResources(this.ctrlMobileTable, "ctrlMobileTable");
			this.ctrlMobileTable.BackgroundImage = null;
			this.ctrlMobileTable.EditorProvider = this.mobileEditorProvider;
			this.ctrlMobileTable.Font = null;
			this.ctrlMobileTable.Name = "ctrlMobileTable";
			this.ctrlMobileTable.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler(this.ctrlMobileTable_SelectionChanged);
			// 
			// ctrlRuleTable
			// 
			this.ctrlRuleTable.AccessibleDescription = null;
			this.ctrlRuleTable.AccessibleName = null;
			resources.ApplyResources(this.ctrlRuleTable, "ctrlRuleTable");
			this.ctrlRuleTable.BackgroundImage = null;
			this.ctrlRuleTable.EditorProvider = this.ruleEditorProvider;
			this.ctrlRuleTable.Font = null;
			this.ctrlRuleTable.Name = "ctrlRuleTable";
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.AfterChange += new System.EventHandler(this.clientDispatcher_AfterChange);
			this.clientDispatcher.BeforeChange += new System.EventHandler(this.clientDispatcher_BeforeChange);
			// 
			// ruleData
			// 
			this.ruleData.DataSupplier = this.ruleDataSupplier;
			this.ruleData.InvokeBroker = this.invokeBroker;
			this.ruleData.Loaded += new System.EventHandler(this.ruleData_Loaded);
			this.ruleData.Loading += new System.EventHandler(this.ruleData_Loading);
			// 
			// ruleDataDispatcher
			// 
			this.ruleDataDispatcher.Ambassadors.Add(this.daRuleEditorProvider);
			this.ruleDataDispatcher.Ambassadors.Add(this.daRuleTableControl);
			this.ruleDataDispatcher.Ambassadors.Add(this.daMobileEditorProvider);
			this.ruleDataDispatcher.Ambassadors.Add(this.daMobileTableControl);
			this.ruleDataDispatcher.Ambassadors.Add(this.daMobileRuleEditorProvider);
			this.ruleDataDispatcher.Ambassadors.Add(this.daMobileRuleTableControl);
			// 
			// daRuleEditorProvider
			// 
			this.daRuleEditorProvider.Item = this.ruleEditorProvider;
			// 
			// daRuleTableControl
			// 
			this.daRuleTableControl.Item = this.ctrlRuleTable;
			// 
			// daMobileEditorProvider
			// 
			this.daMobileEditorProvider.Item = this.mobileEditorProvider;
			// 
			// daMobileTableControl
			// 
			this.daMobileTableControl.Item = this.ctrlMobileTable;
			// 
			// daMobileRuleEditorProvider
			// 
			this.daMobileRuleEditorProvider.Item = this.mobileRuleEditorProvider;
			// 
			// daMobileRuleTableControl
			// 
			this.daMobileRuleTableControl.Item = this.ctrlMobileRuleTable;
			// 
			// ctrlMobileRuleTable
			// 
			this.ctrlMobileRuleTable.AccessibleDescription = null;
			this.ctrlMobileRuleTable.AccessibleName = null;
			resources.ApplyResources(this.ctrlMobileRuleTable, "ctrlMobileRuleTable");
			this.ctrlMobileRuleTable.BackgroundImage = null;
			this.ctrlMobileRuleTable.EditorProvider = this.mobileRuleEditorProvider;
			this.ctrlMobileRuleTable.Filter = this.mobileRuleFilter;
			this.ctrlMobileRuleTable.Font = null;
			this.ctrlMobileRuleTable.Name = "ctrlMobileRuleTable";
			// 
			// mobileRuleFilter
			// 
			this.mobileRuleFilter.Mobile = 0;
			// 
			// RB_Rule
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = null;
			this.Controls.Add(this.ctrlMobileRuleTable);
			this.Controls.Add(this.ctrlMobileTable);
			this.Controls.Add(this.ctrlRuleTable);
			this.Font = null;
			this.Name = "RB_Rule";
			this.ResumeLayout(false);

		}

		#endregion

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IRuleClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		#region IClientItem<AmbulanceClient> Members

		public IRuleClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set { this.clientDispatcher.Client = value; }
		}

		#endregion // IClientItem<AmbulanceClient> Members

		#region Handle clientDispatcher events

		private void clientDispatcher_BeforeChange( object sender, System.EventArgs e )
		{
			this.ruleDataSupplier.ClientProvider = null;
		}

		private void clientDispatcher_AfterChange( object sender, System.EventArgs e )
		{
			this.ruleDataSupplier.ClientProvider = this.Client;
		}

		#endregion // Handle clientDispatcher events

		#region Handle ruleData events

		private void ruleData_Loading( object sender, System.EventArgs e )
		{
			this.ruleDataDispatcher.Data = null;
		}

		private void ruleData_Loaded( object sender, System.EventArgs e )
		{
			this.ruleDataDispatcher.Data = this.ruleData;
		}

		#endregion // Handle ruleData events

		private void ctrlMobileTable_SelectionChanged( object sender, FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventArgs e )
		{
			MobileXpRow[] selectedRows = this.ctrlMobileTable.SelectedRows;

			if( selectedRows.Length == 1 )
			{
				int mobileId = selectedRows[0].Id;

				this.mobileRuleFilter.Mobile = mobileId;
				this.mobileRuleEditorProvider.Mobile = mobileId;
			}
			else
			{
				this.mobileRuleFilter.Mobile = 0;
				this.mobileRuleEditorProvider.Mobile = 0;
			}
		}
	}
}