using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FORIS.TSS.Common;

namespace FORIS.TSS.RuleManager.Rule.Controls
{
	public class HistoryImages :
		Component
	{
		#region Controls & Components

		private IContainer components;
		private ImageList rest;
		private ImageList free;
		private ImageList busy;
		private ImageList freeSelected;
		private ImageList busySelected;
		private ImageList restSelected;

		#endregion // Controls & Components

		#region Constructor & Dispose

		private HistoryImages()
		{
			InitializeComponent();

			this.Free = new ImageIndexer( this.free.Images );
			this.FreeSelected = new ImageIndexer( this.freeSelected.Images );
			this.Busy = new ImageIndexer( this.busy.Images );
			this.BusySelected = new ImageIndexer( this.busySelected.Images );
			this.Rest = new ImageIndexer( this.rest.Images );
			this.RestSelected = new ImageIndexer( this.restSelected.Images );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistoryImages));
			this.rest = new System.Windows.Forms.ImageList(this.components);
			this.free = new System.Windows.Forms.ImageList(this.components);
			this.busy = new System.Windows.Forms.ImageList(this.components);
			this.freeSelected = new System.Windows.Forms.ImageList(this.components);
			this.busySelected = new System.Windows.Forms.ImageList(this.components);
			this.restSelected = new System.Windows.Forms.ImageList(this.components);
			// 
			// rest
			// 
			this.rest.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("rest.ImageStream")));
			this.rest.TransparentColor = System.Drawing.Color.Transparent;
			this.rest.Images.SetKeyName(0, "h_rest0.png");
			this.rest.Images.SetKeyName(1, "h_rest1.png");
			this.rest.Images.SetKeyName(2, "h_rest2.png");
			this.rest.Images.SetKeyName(3, "h_rest3.png");
			this.rest.Images.SetKeyName(4, "h_rest4.png");
			this.rest.Images.SetKeyName(5, "h_rest5.png");
			this.rest.Images.SetKeyName(6, "h_rest6.png");
			this.rest.Images.SetKeyName(7, "h_rest7.png");
			this.rest.Images.SetKeyName(8, "h_rest8.png");
			this.rest.Images.SetKeyName(9, "h_rest9.png");
			this.rest.Images.SetKeyName(10, "h_rest10.png");
			this.rest.Images.SetKeyName(11, "h_rest11.png");
			this.rest.Images.SetKeyName(12, "h_rest12.png");
			this.rest.Images.SetKeyName(13, "h_rest13.png");
			this.rest.Images.SetKeyName(14, "h_rest14.png");
			this.rest.Images.SetKeyName(15, "h_rest15.png");
			this.rest.Images.SetKeyName(16, "h_rest16.png");
			this.rest.Images.SetKeyName(17, "h_rest17.png");
			this.rest.Images.SetKeyName(18, "h_rest18.png");
			this.rest.Images.SetKeyName(19, "h_rest19.png");
			this.rest.Images.SetKeyName(20, "h_rest20.png");
			this.rest.Images.SetKeyName(21, "h_rest21.png");
			this.rest.Images.SetKeyName(22, "h_rest22.png");
			this.rest.Images.SetKeyName(23, "h_rest23.png");
			this.rest.Images.SetKeyName(24, "h_rest24.png");
			this.rest.Images.SetKeyName(25, "h_rest25.png");
			this.rest.Images.SetKeyName(26, "h_rest26.png");
			this.rest.Images.SetKeyName(27, "h_rest27.png");
			this.rest.Images.SetKeyName(28, "h_rest28.png");
			this.rest.Images.SetKeyName(29, "h_rest29.png");
			this.rest.Images.SetKeyName(30, "h_rest30.png");
			this.rest.Images.SetKeyName(31, "h_rest31.png");
			this.rest.Images.SetKeyName(32, "h_rest32.png");
			this.rest.Images.SetKeyName(33, "h_rest33.png");
			this.rest.Images.SetKeyName(34, "h_rest34.png");
			this.rest.Images.SetKeyName(35, "h_rest35.png");
			// 
			// free
			// 
			this.free.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("free.ImageStream")));
			this.free.TransparentColor = System.Drawing.Color.Transparent;
			this.free.Images.SetKeyName(0, "h_free0.png");
			this.free.Images.SetKeyName(1, "h_free1.png");
			this.free.Images.SetKeyName(2, "h_free2.png");
			this.free.Images.SetKeyName(3, "h_free3.png");
			this.free.Images.SetKeyName(4, "h_free4.png");
			this.free.Images.SetKeyName(5, "h_free5.png");
			this.free.Images.SetKeyName(6, "h_free6.png");
			this.free.Images.SetKeyName(7, "h_free7.png");
			this.free.Images.SetKeyName(8, "h_free8.png");
			this.free.Images.SetKeyName(9, "h_free9.png");
			this.free.Images.SetKeyName(10, "h_free10.png");
			this.free.Images.SetKeyName(11, "h_free11.png");
			this.free.Images.SetKeyName(12, "h_free12.png");
			this.free.Images.SetKeyName(13, "h_free13.png");
			this.free.Images.SetKeyName(14, "h_free14.png");
			this.free.Images.SetKeyName(15, "h_free15.png");
			this.free.Images.SetKeyName(16, "h_free16.png");
			this.free.Images.SetKeyName(17, "h_free17.png");
			this.free.Images.SetKeyName(18, "h_free18.png");
			this.free.Images.SetKeyName(19, "h_free19.png");
			this.free.Images.SetKeyName(20, "h_free20.png");
			this.free.Images.SetKeyName(21, "h_free21.png");
			this.free.Images.SetKeyName(22, "h_free22.png");
			this.free.Images.SetKeyName(23, "h_free23.png");
			this.free.Images.SetKeyName(24, "h_free24.png");
			this.free.Images.SetKeyName(25, "h_free25.png");
			this.free.Images.SetKeyName(26, "h_free26.png");
			this.free.Images.SetKeyName(27, "h_free27.png");
			this.free.Images.SetKeyName(28, "h_free28.png");
			this.free.Images.SetKeyName(29, "h_free29.png");
			this.free.Images.SetKeyName(30, "h_free30.png");
			this.free.Images.SetKeyName(31, "h_free31.png");
			this.free.Images.SetKeyName(32, "h_free32.png");
			this.free.Images.SetKeyName(33, "h_free33.png");
			this.free.Images.SetKeyName(34, "h_free34.png");
			this.free.Images.SetKeyName(35, "h_free35.png");
			// 
			// busy
			// 
			this.busy.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("busy.ImageStream")));
			this.busy.TransparentColor = System.Drawing.Color.Transparent;
			this.busy.Images.SetKeyName(0, "h_busy0.png");
			this.busy.Images.SetKeyName(1, "h_busy1.png");
			this.busy.Images.SetKeyName(2, "h_busy2.png");
			this.busy.Images.SetKeyName(3, "h_busy3.png");
			this.busy.Images.SetKeyName(4, "h_busy4.png");
			this.busy.Images.SetKeyName(5, "h_busy5.png");
			this.busy.Images.SetKeyName(6, "h_busy6.png");
			this.busy.Images.SetKeyName(7, "h_busy7.png");
			this.busy.Images.SetKeyName(8, "h_busy8.png");
			this.busy.Images.SetKeyName(9, "h_busy9.png");
			this.busy.Images.SetKeyName(10, "h_busy10.png");
			this.busy.Images.SetKeyName(11, "h_busy11.png");
			this.busy.Images.SetKeyName(12, "h_busy12.png");
			this.busy.Images.SetKeyName(13, "h_busy13.png");
			this.busy.Images.SetKeyName(14, "h_busy14.png");
			this.busy.Images.SetKeyName(15, "h_busy15.png");
			this.busy.Images.SetKeyName(16, "h_busy16.png");
			this.busy.Images.SetKeyName(17, "h_busy17.png");
			this.busy.Images.SetKeyName(18, "h_busy18.png");
			this.busy.Images.SetKeyName(19, "h_busy19.png");
			this.busy.Images.SetKeyName(20, "h_busy20.png");
			this.busy.Images.SetKeyName(21, "h_busy21.png");
			this.busy.Images.SetKeyName(22, "h_busy22.png");
			this.busy.Images.SetKeyName(23, "h_busy23.png");
			this.busy.Images.SetKeyName(24, "h_busy24.png");
			this.busy.Images.SetKeyName(25, "h_busy25.png");
			this.busy.Images.SetKeyName(26, "h_busy26.png");
			this.busy.Images.SetKeyName(27, "h_busy27.png");
			this.busy.Images.SetKeyName(28, "h_busy28.png");
			this.busy.Images.SetKeyName(29, "h_busy29.png");
			this.busy.Images.SetKeyName(30, "h_busy30.png");
			this.busy.Images.SetKeyName(31, "h_busy31.png");
			this.busy.Images.SetKeyName(32, "h_busy32.png");
			this.busy.Images.SetKeyName(33, "h_busy33.png");
			this.busy.Images.SetKeyName(34, "h_busy34.png");
			this.busy.Images.SetKeyName(35, "h_busy35.png");
			// 
			// freeSelected
			// 
			this.freeSelected.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("freeSelected.ImageStream")));
			this.freeSelected.TransparentColor = System.Drawing.Color.Transparent;
			this.freeSelected.Images.SetKeyName(0, "h_free_s0.png");
			this.freeSelected.Images.SetKeyName(1, "h_free_s1.png");
			this.freeSelected.Images.SetKeyName(2, "h_free_s2.png");
			this.freeSelected.Images.SetKeyName(3, "h_free_s3.png");
			this.freeSelected.Images.SetKeyName(4, "h_free_s4.png");
			this.freeSelected.Images.SetKeyName(5, "h_free_s5.png");
			this.freeSelected.Images.SetKeyName(6, "h_free_s6.png");
			this.freeSelected.Images.SetKeyName(7, "h_free_s7.png");
			this.freeSelected.Images.SetKeyName(8, "h_free_s8.png");
			this.freeSelected.Images.SetKeyName(9, "h_free_s9.png");
			this.freeSelected.Images.SetKeyName(10, "h_free_s10.png");
			this.freeSelected.Images.SetKeyName(11, "h_free_s11.png");
			this.freeSelected.Images.SetKeyName(12, "h_free_s12.png");
			this.freeSelected.Images.SetKeyName(13, "h_free_s13.png");
			this.freeSelected.Images.SetKeyName(14, "h_free_s14.png");
			this.freeSelected.Images.SetKeyName(15, "h_free_s15.png");
			this.freeSelected.Images.SetKeyName(16, "h_free_s16.png");
			this.freeSelected.Images.SetKeyName(17, "h_free_s17.png");
			this.freeSelected.Images.SetKeyName(18, "h_free_s18.png");
			this.freeSelected.Images.SetKeyName(19, "h_free_s19.png");
			this.freeSelected.Images.SetKeyName(20, "h_free_s20.png");
			this.freeSelected.Images.SetKeyName(21, "h_free_s21.png");
			this.freeSelected.Images.SetKeyName(22, "h_free_s22.png");
			this.freeSelected.Images.SetKeyName(23, "h_free_s23.png");
			this.freeSelected.Images.SetKeyName(24, "h_free_s24.png");
			this.freeSelected.Images.SetKeyName(25, "h_free_s25.png");
			this.freeSelected.Images.SetKeyName(26, "h_free_s26.png");
			this.freeSelected.Images.SetKeyName(27, "h_free_s27.png");
			this.freeSelected.Images.SetKeyName(28, "h_free_s28.png");
			this.freeSelected.Images.SetKeyName(29, "h_free_s29.png");
			this.freeSelected.Images.SetKeyName(30, "h_free_s30.png");
			this.freeSelected.Images.SetKeyName(31, "h_free_s31.png");
			this.freeSelected.Images.SetKeyName(32, "h_free_s32.png");
			this.freeSelected.Images.SetKeyName(33, "h_free_s33.png");
			this.freeSelected.Images.SetKeyName(34, "h_free_s34.png");
			this.freeSelected.Images.SetKeyName(35, "h_free_s35.png");
			// 
			// busySelected
			// 
			this.busySelected.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("busySelected.ImageStream")));
			this.busySelected.TransparentColor = System.Drawing.Color.Transparent;
			this.busySelected.Images.SetKeyName(0, "h_busy_s0.png");
			this.busySelected.Images.SetKeyName(1, "h_busy_s1.png");
			this.busySelected.Images.SetKeyName(2, "h_busy_s2.png");
			this.busySelected.Images.SetKeyName(3, "h_busy_s3.png");
			this.busySelected.Images.SetKeyName(4, "h_busy_s4.png");
			this.busySelected.Images.SetKeyName(5, "h_busy_s5.png");
			this.busySelected.Images.SetKeyName(6, "h_busy_s6.png");
			this.busySelected.Images.SetKeyName(7, "h_busy_s7.png");
			this.busySelected.Images.SetKeyName(8, "h_busy_s8.png");
			this.busySelected.Images.SetKeyName(9, "h_busy_s9.png");
			this.busySelected.Images.SetKeyName(10, "h_busy_s10.png");
			this.busySelected.Images.SetKeyName(11, "h_busy_s11.png");
			this.busySelected.Images.SetKeyName(12, "h_busy_s12.png");
			this.busySelected.Images.SetKeyName(13, "h_busy_s13.png");
			this.busySelected.Images.SetKeyName(14, "h_busy_s14.png");
			this.busySelected.Images.SetKeyName(15, "h_busy_s15.png");
			this.busySelected.Images.SetKeyName(16, "h_busy_s16.png");
			this.busySelected.Images.SetKeyName(17, "h_busy_s17.png");
			this.busySelected.Images.SetKeyName(18, "h_busy_s18.png");
			this.busySelected.Images.SetKeyName(19, "h_busy_s19.png");
			this.busySelected.Images.SetKeyName(20, "h_busy_s20.png");
			this.busySelected.Images.SetKeyName(21, "h_busy_s21.png");
			this.busySelected.Images.SetKeyName(22, "h_busy_s22.png");
			this.busySelected.Images.SetKeyName(23, "h_busy_s23.png");
			this.busySelected.Images.SetKeyName(24, "h_busy_s24.png");
			this.busySelected.Images.SetKeyName(25, "h_busy_s25.png");
			this.busySelected.Images.SetKeyName(26, "h_busy_s26.png");
			this.busySelected.Images.SetKeyName(27, "h_busy_s27.png");
			this.busySelected.Images.SetKeyName(28, "h_busy_s28.png");
			this.busySelected.Images.SetKeyName(29, "h_busy_s29.png");
			this.busySelected.Images.SetKeyName(30, "h_busy_s30.png");
			this.busySelected.Images.SetKeyName(31, "h_busy_s31.png");
			this.busySelected.Images.SetKeyName(32, "h_busy_s32.png");
			this.busySelected.Images.SetKeyName(33, "h_busy_s33.png");
			this.busySelected.Images.SetKeyName(34, "h_busy_s34.png");
			this.busySelected.Images.SetKeyName(35, "h_busy_s35.png");
			// 
			// restSelected
			// 
			this.restSelected.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("restSelected.ImageStream")));
			this.restSelected.TransparentColor = System.Drawing.Color.Transparent;
			this.restSelected.Images.SetKeyName(0, "h_rest_s0.png");
			this.restSelected.Images.SetKeyName(1, "h_rest_s1.png");
			this.restSelected.Images.SetKeyName(2, "h_rest_s2.png");
			this.restSelected.Images.SetKeyName(3, "h_rest_s3.png");
			this.restSelected.Images.SetKeyName(4, "h_rest_s4.png");
			this.restSelected.Images.SetKeyName(5, "h_rest_s5.png");
			this.restSelected.Images.SetKeyName(6, "h_rest_s6.png");
			this.restSelected.Images.SetKeyName(7, "h_rest_s7.png");
			this.restSelected.Images.SetKeyName(8, "h_rest_s8.png");
			this.restSelected.Images.SetKeyName(9, "h_rest_s9.png");
			this.restSelected.Images.SetKeyName(10, "h_rest_s10.png");
			this.restSelected.Images.SetKeyName(11, "h_rest_s11.png");
			this.restSelected.Images.SetKeyName(12, "h_rest_s12.png");
			this.restSelected.Images.SetKeyName(13, "h_rest_s13.png");
			this.restSelected.Images.SetKeyName(14, "h_rest_s14.png");
			this.restSelected.Images.SetKeyName(15, "h_rest_s15.png");
			this.restSelected.Images.SetKeyName(16, "h_rest_s16.png");
			this.restSelected.Images.SetKeyName(17, "h_rest_s17.png");
			this.restSelected.Images.SetKeyName(18, "h_rest_s18.png");
			this.restSelected.Images.SetKeyName(19, "h_rest_s19.png");
			this.restSelected.Images.SetKeyName(20, "h_rest_s20.png");
			this.restSelected.Images.SetKeyName(21, "h_rest_s21.png");
			this.restSelected.Images.SetKeyName(22, "h_rest_s22.png");
			this.restSelected.Images.SetKeyName(23, "h_rest_s23.png");
			this.restSelected.Images.SetKeyName(24, "h_rest_s24.png");
			this.restSelected.Images.SetKeyName(25, "h_rest_s25.png");
			this.restSelected.Images.SetKeyName(26, "h_rest_s26.png");
			this.restSelected.Images.SetKeyName(27, "h_rest_s27.png");
			this.restSelected.Images.SetKeyName(28, "h_rest_s28.png");
			this.restSelected.Images.SetKeyName(29, "h_rest_s29.png");
			this.restSelected.Images.SetKeyName(30, "h_rest_s30.png");
			this.restSelected.Images.SetKeyName(31, "h_rest_s31.png");
			this.restSelected.Images.SetKeyName(32, "h_rest_s32.png");
			this.restSelected.Images.SetKeyName(33, "h_rest_s33.png");
			this.restSelected.Images.SetKeyName(34, "h_rest_s34.png");
			this.restSelected.Images.SetKeyName(35, "h_rest_s35.png");

		}

		#endregion // Component Designer generated code

		#region Image indexers

		public readonly IIndexer<Image> Free;
		public readonly IIndexer<Image> FreeSelected;
		public readonly IIndexer<Image> Busy;
		public readonly IIndexer<Image> BusySelected;
		public readonly IIndexer<Image> Rest;
		public readonly IIndexer<Image> RestSelected;

		#endregion // Image indexers

		public static readonly HistoryImages Instance;

		static HistoryImages()
		{
			HistoryImages.Instance = new HistoryImages();
		}

		private class ImageIndexer :
			IIndexer<Image>
		{
			public ImageIndexer( ImageList.ImageCollection images )
			{
				this.images = images;
			}

			private readonly ImageList.ImageCollection images;
			public Image this[int index]
			{
				get { return this.images[index]; }
			}
		}
	}
}
