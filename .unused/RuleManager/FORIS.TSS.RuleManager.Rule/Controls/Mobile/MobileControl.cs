using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.MTMap.Controls;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile;
using MTMap.MathTypes;

namespace FORIS.TSS.RuleManager.Rule.Controls.Mobile
{
	public class MobileControl:
		UserControl,
		IDataItem<IRuleData>
	{
		#region Controls & Components
		
		private IContainer components;
        private RuleDataDispatcher dataDispatcher;
        private MobileLayer mobileLayer;
        private RuleDataAmbassador daMobileTableControl;
        private RuleDataAmbassador daMobileLayer;
        private TableLayoutPanel tableLayoutPanelMain;
        private TableLayoutPanel tableLayoutPanelTop;
        private Label lblFixLayer;
        private CheckBox chbFixLayer;
        private MobileTableControl ctrlMobileTable;
        private RuleDataAmbassador daMobileEditorProvider;
        private MobileEditorProvider mobileEditorProvider;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public MobileControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MobileControl));
            this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher(this.components);
            this.daMobileTableControl = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.ctrlMobileTable = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileTableControl();
            this.mobileEditorProvider = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileEditorProvider(this.components);
            this.daMobileLayer = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.mobileLayer = new FORIS.TSS.RuleManager.Rule.Controls.Mobile.MobileLayer(this.components);
            this.daMobileEditorProvider = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.lblFixLayer = new System.Windows.Forms.Label();
            this.chbFixLayer = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataDispatcher
            // 
            this.dataDispatcher.Ambassadors.Add(this.daMobileTableControl);
            this.dataDispatcher.Ambassadors.Add(this.daMobileLayer);
            this.dataDispatcher.Ambassadors.Add(this.daMobileEditorProvider);
            // 
            // daMobileTableControl
            // 
            this.daMobileTableControl.Item = this.ctrlMobileTable;
            // 
            // ctrlMobileTable
            // 
            resources.ApplyResources(this.ctrlMobileTable, "ctrlMobileTable");
            this.ctrlMobileTable.EditorProvider = this.mobileEditorProvider;
            this.ctrlMobileTable.Name = "ctrlMobileTable";
            this.ctrlMobileTable.ToolbarVisible = true;
            this.ctrlMobileTable.DoubleClick += new System.EventHandler(this.ctrlMobileTable_DoubleClick);
            this.ctrlMobileTable.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler(this.ctrlMobileTable_SelectionChanged);
            // 
            // daMobileLayer
            // 
            this.daMobileLayer.Item = this.mobileLayer;
            // 
            // mobileLayer
            // 
            this.mobileLayer.SelectionChanged += new System.EventHandler(this.mobileLayer_SelectionChanged);
            // 
            // daMobileEditorProvider
            // 
            this.daMobileEditorProvider.Item = this.mobileEditorProvider;
            // 
            // tableLayoutPanelMain
            // 
            resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
            this.tableLayoutPanelMain.Controls.Add(this.ctrlMobileTable, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelTop, 0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            // 
            // tableLayoutPanelTop
            // 
            resources.ApplyResources(this.tableLayoutPanelTop, "tableLayoutPanelTop");
            this.tableLayoutPanelTop.Controls.Add(this.lblFixLayer, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.chbFixLayer, 1, 0);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            // 
            // lblFixLayer
            // 
            resources.ApplyResources(this.lblFixLayer, "lblFixLayer");
            this.lblFixLayer.Name = "lblFixLayer";
            // 
            // chbFixLayer
            // 
            resources.ApplyResources(this.chbFixLayer, "chbFixLayer");
            this.chbFixLayer.Name = "chbFixLayer";
            this.chbFixLayer.UseVisualStyleBackColor = true;
            this.chbFixLayer.CheckedChanged += new System.EventHandler(this.chbFixLayer_CheckedChanged);
            // 
            // MobileControl
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "MobileControl";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanelTop.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion  // Windows Form Designer generated code

		#region IDataItem<IRuleData>

		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRuleData>

		#region Properties

		private IMapControl mapControl;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IMapControl MapControl
		{
			get { return this.mapControl; }
			set
			{
				if( this.mapControl != null )
				{
					this.mapControl.ExtraLayers.Remove( this.mobileLayer );
				}

				this.mapControl = value; 

				if( this.mapControl != null )
				{
					this.mapControl.ExtraLayers.Add( this.mobileLayer );
				}
			}
		}

		private bool layerVisible = true;
		[Browsable(true)]
		[DefaultValue(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool LayerVisible
		{
			get { return this.layerVisible; }
			set
			{
				if( this.layerVisible != value )
				{
					this.layerVisible = value;

					this.mobileLayer.Visible =
						this.chbFixLayer.Checked ? true : this.layerVisible;
				}
			}
		}

        /// <summary>
        /// ������� �� ������� �������� ��������
        /// </summary>
        [Browsable(false)]
        public MobileTableControl MobileTableControl
        {
            get { return ctrlMobileTable;  }
        }

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public List<int> CheckedMobiles
		{
			get { return this.ctrlMobileTable.CheckedRows; }
			set { this.ctrlMobileTable.CheckedRows = value; }
		}

		#endregion // Properties

		#region Handle controls' events

		private void mobileLayer_SelectionChanged( object sender, EventArgs e )
		{
			/* ������ ������������, ��� ������� �������� 
			 * � ���� ����� � � ������� ���������
			 */

			this.ctrlMobileTable.SelectionChanged -= this.ctrlMobileTable_SelectionChanged;

			foreach( MobileXpRow mobileXpRow in this.ctrlMobileTable.Rows )
			{
				MobileObject mobileObject = this.mobileLayer.Objects[mobileXpRow.Id];

				mobileXpRow.Selected = mobileObject.Selected;
			}

			this.ctrlMobileTable.SelectionChanged += this.ctrlMobileTable_SelectionChanged;
		}

		private void ctrlMobileTable_SelectionChanged( object sender, SelectionEventArgs e )
		{
			/* ������ ������������, ��� ������� �������� 
			 * � ���� ����� � � ������� ���������
			 */
			this.mobileLayer.SelectionChanged -= this.mobileLayer_SelectionChanged;
			this.mobileLayer.UpdateBegin();

			foreach( MobileXpRow mobileXpRow in this.ctrlMobileTable.Rows )
			{
				MobileObject mobileObject = this.mobileLayer.Objects[ mobileXpRow.Id];

				mobileObject.Selected = mobileXpRow.Selected;
			}

			this.mobileLayer.UpdateEnd();
			this.mobileLayer.SelectionChanged += this.mobileLayer_SelectionChanged;
		}

		private void ctrlMobileTable_DoubleClick( object sender, EventArgs e )
		{
			if( this.mapControl != null && this.ctrlMobileTable.SelectedRows.Length > 0 )
			{
				DExtents extent = DExtents.Empty;

				foreach( MobileXpRow xpRow in this.ctrlMobileTable.SelectedRows )
				{
					if( !xpRow.Row.RowPosition.Empty )
					{
						GeoPoint geoPoint =
							GeoPoint.CreateGeo(
								(double)xpRow.Row.RowPosition.Longitude,
								(double)xpRow.Row.RowPosition.Latitude
								);

						extent.includePoint( geoPoint.Planar );
					}
				}

				if( !extent.IsEmpty )
				{
					this.mapControl.Zoom( extent, MapZoomMode.Zoom );
				}
				else
				{
					MessageBox.Show(
						"��������������� ����������"
						);
				}
			}
		}		

		private void chbFixLayer_CheckedChanged( object sender, EventArgs e )
		{
			this.LayerVisible = this.layerVisible;
		}

		#endregion // Handle controls' events

		public event EventHandler ColumnCheckedChanged
		{
			add { this.ctrlMobileTable.ColumnCheckedChanged += value; }
			remove { this.ctrlMobileTable.ColumnCheckedChanged -= value; }
		}
	}
}