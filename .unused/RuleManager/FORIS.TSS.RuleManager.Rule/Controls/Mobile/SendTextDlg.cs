using System;
using System.ComponentModel;

namespace FORIS.TSS.RuleManager.Rule.Controls.Mobile
{
    /// <summary>
    /// Summary description for SendTextDlg.
    /// </summary>
    public class SendTextDlg : System.Windows.Forms.Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.Container components = null;
        private System.Windows.Forms.Panel panButt;
        private System.Windows.Forms.Panel panText;
        private System.Windows.Forms.TextBox edText;
        private System.Windows.Forms.ListBox lbMessage;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Splitter spText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbCheckedVehicles;
        private System.Windows.Forms.RadioButton rbSelectedVehicle;
        private System.Windows.Forms.TextBox textBoxWarrnings;

        private bool IsMobileDefined = false;

        [Description("��������� �� ��� �������� ���������")]
        public string SelectedVehicle
        {
            get { return this.rbSelectedVehicle.Text; }
            set
            {
                this.rbSelectedVehicle.Text = value;
                if(!string.IsNullOrEmpty(value))
                    IsMobileDefined = true;
            }
        }

        public bool SendToAllVehicles
        {
            get { return rbCheckedVehicles.Checked; }
        }

        readonly object[] aMsg;

        public SendTextDlg()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            aMsg = new object[]
                {
                    new MessageItem("���������� �� ����� # "),
                    new MessageItem("������� � ����"),
                    new MessageItem("������� � ������������� ��������"),
                    new MessageItem("������� � ������������� �������"),
                    new MessageItem("������� � ������������� 14 �/�-�"),
                    new MessageItem("������� � ������������� 16 �/�-�"),
                    new MessageItem("������ ������")
                };
            //DataSet ds = Client.Instance.Session.GetDriverMsgTemplate();

            //foreach (DataRow row in ds.Tables["DRIVER_MSG_TEMPLATE"].Rows)
            //{
            //    lbMessage.Items.Add(new MessageItem((int)row["DRIVER_MSG_TEMPLATE_ID"], (string)row["MESSAGE"]));
            //}
			
            lbMessage.Items.AddRange(aMsg);

            textBoxWarrnings.Text = String.Format("{0}/{1}", 0, maxLength);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendTextDlg));
            this.panButt = new System.Windows.Forms.Panel();
            this.textBoxWarrnings = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbCheckedVehicles = new System.Windows.Forms.RadioButton();
            this.rbSelectedVehicle = new System.Windows.Forms.RadioButton();
            this.btOK = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.panText = new System.Windows.Forms.Panel();
            this.spText = new System.Windows.Forms.Splitter();
            this.edText = new System.Windows.Forms.TextBox();
            this.lbMessage = new System.Windows.Forms.ListBox();
            this.panButt.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panText.SuspendLayout();
            this.SuspendLayout();
            // 
            // panButt
            // 
            this.panButt.Controls.Add(this.textBoxWarrnings);
            this.panButt.Controls.Add(this.groupBox1);
            this.panButt.Controls.Add(this.btOK);
            this.panButt.Controls.Add(this.btCancel);
            resources.ApplyResources(this.panButt, "panButt");
            this.panButt.Name = "panButt";
            // 
            // textBoxWarrnings
            // 
            resources.ApplyResources(this.textBoxWarrnings, "textBoxWarrnings");
            this.textBoxWarrnings.Name = "textBoxWarrnings";
            this.textBoxWarrnings.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbCheckedVehicles);
            this.groupBox1.Controls.Add(this.rbSelectedVehicle);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // rbCheckedVehicles
            // 
            resources.ApplyResources(this.rbCheckedVehicles, "rbCheckedVehicles");
            this.rbCheckedVehicles.Name = "rbCheckedVehicles";
            this.rbCheckedVehicles.UseVisualStyleBackColor = true;
            // 
            // rbSelectedVehicle
            // 
            resources.ApplyResources(this.rbSelectedVehicle, "rbSelectedVehicle");
            this.rbSelectedVehicle.Checked = true;
            this.rbSelectedVehicle.Name = "rbSelectedVehicle";
            this.rbSelectedVehicle.TabStop = true;
            this.rbSelectedVehicle.UseVisualStyleBackColor = true;
            // 
            // btOK
            // 
            resources.ApplyResources(this.btOK, "btOK");
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Name = "btOK";
            // 
            // btCancel
            // 
            resources.ApplyResources(this.btCancel, "btCancel");
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Name = "btCancel";
            // 
            // panText
            // 
            this.panText.Controls.Add(this.spText);
            this.panText.Controls.Add(this.edText);
            this.panText.Controls.Add(this.lbMessage);
            resources.ApplyResources(this.panText, "panText");
            this.panText.Name = "panText";
            // 
            // spText
            // 
            this.spText.Cursor = System.Windows.Forms.Cursors.HSplit;
            resources.ApplyResources(this.spText, "spText");
            this.spText.Name = "spText";
            this.spText.TabStop = false;
            // 
            // edText
            // 
            resources.ApplyResources(this.edText, "edText");
            this.edText.Name = "edText";
            this.edText.TextChanged += new System.EventHandler(this.edText_TextChanged);
            // 
            // lbMessage
            // 
            resources.ApplyResources(this.lbMessage, "lbMessage");
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.SelectedIndexChanged += new System.EventHandler(this.lbMessage_SelectedIndexChanged);
            // 
            // SendTextDlg
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panText);
            this.Controls.Add(this.panButt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SendTextDlg";
            this.ShowInTaskbar = false;
            this.Shown += new System.EventHandler(this.SendTextDlg_Shown);
            this.panButt.ResumeLayout(false);
            this.panButt.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panText.ResumeLayout(false);
            this.panText.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private void lbMessage_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            edText.Text = "";
            MessageItem msg = lbMessage.SelectedItem as MessageItem;
            if(msg == null) return;

            edText.Text = msg.ToString();
        }

        class MessageItem
        {
            readonly string text;
			
            public MessageItem(string _text)
            {
                text = _text;
            }

            public override string ToString()
            {
                return text;
            }
        }

        /// <summary>
        /// text to send
        /// </summary>
        public string Message
        {
            get
            {
                return edText.Text;
            }
        }

        /// <summary>
        /// ������������ ���-�� �������� ��� �������� SMS-���������
        /// </summary>
        private readonly int maxLength = 60;

        private void edText_TextChanged(object sender, System.EventArgs e)
        {
            int length = edText.TextLength;
			
            if(length > maxLength)
            {
                textBoxWarrnings.Text =
                    String.Format("�������� ����� ��� SMS {0} ��������! �������� ������ �� GPRS", maxLength);
            }
            else
            {
                textBoxWarrnings.Text = String.Format("{0}/{1}", length, maxLength);
            }
        }

        private void SendTextDlg_Shown(object sender, EventArgs e)
        {
            if(!this.IsMobileDefined)
            {
                this.rbSelectedVehicle.Enabled = false;
                this.rbCheckedVehicles.Checked = true;
            }
        }
    }
}