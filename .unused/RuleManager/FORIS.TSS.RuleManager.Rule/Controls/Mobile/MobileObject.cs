using System;
using System.Drawing;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.MTMap.Spatial;
using FORIS.TSS.WorkplaceShadow.Controls;
using MTMap.MathTypes;

namespace FORIS.TSS.RuleManager.Rule.Controls.Mobile
{
	public class MobileObject:
		DataExtraObject<MobileRow, MobileTable, IRuleData>,
		IImageConsumer<MobileObject, MobileLayer>,
		ILabeledMarkLayer
	{
		private readonly LabeledMarkObject labeledMarkObject;

		#region Constructor & Dispose

		public MobileObject( int mobileId )
			: base( mobileId )
		{
			this.labeledMarkObject = new LabeledMarkObject();
		}

		#endregion // Constructor & Dispose

		#region IImageConsumer<MobileObject, MobileLayer> members

		private MobileLayer imageSupplier;
		public MobileLayer ImageSupplier
		{
			get { return this.imageSupplier; }
			set
			{
				if ( this.imageSupplier != value )
				{
					this.imageSupplier = value;
					this.labeledMarkObject.ImageSupplier = this;
				}
			}
		}

		public Bitmap Image
		{
			get { return this.labeledMarkObject.Image; }
			set
			{
				if ( this.Image != value )
				{
					this.labeledMarkObject.Image = value;
					this.OnChanged();
				}
			}
		}

		#endregion  // IImageConsumer<MobileObject, MobileLayer> members

		#region ILabeledMarkLayer members

		public Bitmap GetImage( LabeledMarkObject obj )
		{
			return
				this.imageSupplier != null
					? this.imageSupplier.GetImage( this )
					: null;
		}

		#endregion  // ILabeledMarkLayer members

		#region Properties

		protected override MobileRow GetDataRow( int mobileId )
		{
			return this.Data.Mobile.FindRow( mobileId );
		}

		protected override DExtents Extent
		{
			get { return ( (IExtraObject)this.labeledMarkObject ).Extent; }
		}

		public GeoPoint Position
		{
			get { return this.labeledMarkObject.Position; }
		}

		private int course;
		public int Course
		{
			get { return this.course; }
		}

		public override bool Selectable
		{
			get { return this.labeledMarkObject.Selectable; }
			set { this.labeledMarkObject.Selectable = value; }
		}

		public override bool Selected
		{
			get { return this.labeledMarkObject.Selected; }
			set
			{
				if ( this.Selected != value )
				{
					this.labeledMarkObject.Selected = value;

					this.OnSelectionChanged();
				}
			}
		}

		#endregion  // Proprties

		#region Data

		protected override void OnBeforeSetData()
		{
			this.Row.PositionChanged -= this.row_PositionChanged;

			base.OnBeforeSetData();
		}

		protected override void OnAfterSetData()
		{
			base.OnAfterSetData();

			this.Row.PositionChanged += this.row_PositionChanged;
		}

		#endregion // Data

		#region Data events

		private void row_PositionChanged( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data events

		#region View

		/* ������ � ������ �������� �� ����� 
		 * ���� ��������� �� ����� �������������
		 */

		protected override void OnDestroyView()
		{
			this.Visible = false;

			this.labeledMarkObject.Position = GeoPoint.Empty;
			this.course = 0;

			base.OnDestroyView();
		}

		protected override void OnUpdateView()
		{
			base.OnUpdateView();

			#region Preconditions

			if( this.Row == null )
			{
				throw new ApplicationException( "There is no data row" );
			}

			#endregion // Preconditions

			if( this.Row.RowPosition != null )
			{
				if( !this.Row.RowPosition.Empty )
				{
					this.labeledMarkObject.Position =
						GeoPoint.CreateGeo(
							(double)this.Row.RowPosition.Longitude,
							(double)this.Row.RowPosition.Latitude
							);

					this.course = this.Row.RowPosition.Course;
					
					this.Visible = true;
				}

				this.labeledMarkObject.Text = this.Row.Name;
				
				this.labeledMarkObject.Refresh();
				this.OnChanged();
			}
		}

		#endregion // View

		protected override void Draw( IMapWindow window, Graphics graphics )
		{
			( (IExtraObject)this.labeledMarkObject ).Draw( window, graphics );
		}

		protected override bool Contains( IMapWindow window, GDI.POINT point )
		{
			return ( (IExtraObject)this.labeledMarkObject ).Contains( window, point );
		}

		protected override string GetToolTip( IMapWindow window, GDI.POINT point )
		{
			return ( (IExtraObject)this.labeledMarkObject ).GetToolTip( window, point );
		}
	}
}