using System;
using System.ComponentModel;
using System.Drawing;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.MTMap.Spatial;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.RuleManager.Rule.Controls.Mobile
{
	/// <summary>
	/// ���� ��������� ��������, ������������
	/// ������� ������� �������� ��������,
	/// ������������������ � �������
	/// </summary>
	[DesignTimeVisible( true )]
	public class MobileLayer:
		DataExtraLayer<MobileObject, MobileRow, MobileTable, IRuleData>,
		IImageSupplier<MobileObject, MobileLayer>
	{
		#region Constructor & Dispose

		public MobileLayer( IContainer container)
			: this ()
		{
			container.Add( this );
		}

		public MobileLayer()
		{
			this.Objects.Cleared += this.Objects_Cleared;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.Objects.Cleared -= this.Objects_Cleared;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region IImageSupplier<MobileObject, MobileLayer> Members

		public Bitmap GetImage( MobileObject obj )
		{
			int index = ((obj.Course%360 + 360)%360)/10;

			if (obj.Selected)
			{
				return (Bitmap) MobileImages.Instance.RestSelected[index];
			}
			else
			{
				return (Bitmap) MobileImages.Instance.Rest[index];
			}
		}

		#endregion // IImageList Members

		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Mobile.Rows.Inserted -= this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing -= this.MobileRows_Removing;
		}

		protected override void OnAfterSetData()
		{
			this.Data.Mobile.Rows.Inserted += this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing += this.MobileRows_Removing;
		}

		#endregion // Data		

		#region Data events

		private void MobileRows_Removing( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.HideMobile( e.Item );
		}

		private void MobileRows_Inserted( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.ShowMobile( e.Item );
		}

		#endregion // Data events
		
		#region Show & Hide

		private void HideMobile( MobileRow mobileRow )
		{
			MobileObject soughtMobileObject = null;

			// TODO: ������� ����� �� ����
			foreach ( MobileObject mobileObject in this.Objects )
			{
				if( mobileObject.Id == mobileRow.ID )
				{
					soughtMobileObject = mobileObject;
					break;
				}
			}

			if ( soughtMobileObject != null )
			{
				soughtMobileObject.ImageSupplier = null;
				this.Objects.Remove(soughtMobileObject);
			}
		}

		private void ShowMobile( MobileRow mobileRow )
		{
			MobileObject mobileObject = new MobileObject( mobileRow.ID );
			
			mobileObject.ImageSupplier = this;
			
			this.Objects.Add( mobileObject );
		}

		#endregion // show & Hide

		#region View

		protected override void OnDestroyView()
		{
			this.Objects.Clear();
		}
		
		protected override void OnBuildView()
		{
			this.UpdateBegin();

			foreach( MobileRow mobileRow in this.Data.Mobile.Rows )
			{
				this.ShowMobile( mobileRow );
			}

			this.UpdateEnd();
		}

		protected override void OnUpdateView()
		{
			
		}

		#endregion // View

		#region Handle Objects events

		private void Objects_Cleared( object sender, CollectionEventArgs<MobileObject> e )
		{
			foreach( MobileObject mobileObject in e.Collection )
			{
				mobileObject.ImageSupplier = null;
			}
		}

		#endregion // Handle Objects events
	}
}