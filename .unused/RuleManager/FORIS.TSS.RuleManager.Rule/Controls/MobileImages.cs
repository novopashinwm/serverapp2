using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FORIS.TSS.Common;

namespace FORIS.TSS.RuleManager.Rule.Controls
{
	public class MobileImages :
		Component
	{
		#region Controls & Components

		private IContainer components;
		private ImageList rest;
		private ImageList restSelected;

		#endregion // Controls & Components

		#region Constructor & Dispose

		private MobileImages()
		{
			InitializeComponent();

			this.Rest = new ImageIndexer( this.rest.Images );
			this.RestSelected = new ImageIndexer( this.restSelected.Images );
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.components != null)
				{
					this.components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MobileImages ) );
			this.rest = new System.Windows.Forms.ImageList( this.components );
			this.restSelected = new System.Windows.Forms.ImageList( this.components );
			// 
			// rest
			// 
			this.rest.ImageStream = ( (System.Windows.Forms.ImageListStreamer)( resources.GetObject( "rest.ImageStream" ) ) );
			this.rest.TransparentColor = System.Drawing.Color.Transparent;
			this.rest.Images.SetKeyName( 0, "rest0.png" );
			this.rest.Images.SetKeyName( 1, "rest1.png" );
			this.rest.Images.SetKeyName( 2, "rest2.png" );
			this.rest.Images.SetKeyName( 3, "rest3.png" );
			this.rest.Images.SetKeyName( 4, "rest4.png" );
			this.rest.Images.SetKeyName( 5, "rest5.png" );
			this.rest.Images.SetKeyName( 6, "rest6.png" );
			this.rest.Images.SetKeyName( 7, "rest7.png" );
			this.rest.Images.SetKeyName( 8, "rest8.png" );
			this.rest.Images.SetKeyName( 9, "rest9.png" );
			this.rest.Images.SetKeyName( 10, "rest10.png" );
			this.rest.Images.SetKeyName( 11, "rest11.png" );
			this.rest.Images.SetKeyName( 12, "rest12.png" );
			this.rest.Images.SetKeyName( 13, "rest13.png" );
			this.rest.Images.SetKeyName( 14, "rest14.png" );
			this.rest.Images.SetKeyName( 15, "rest15.png" );
			this.rest.Images.SetKeyName( 16, "rest16.png" );
			this.rest.Images.SetKeyName( 17, "rest17.png" );
			this.rest.Images.SetKeyName( 18, "rest18.png" );
			this.rest.Images.SetKeyName( 19, "rest19.png" );
			this.rest.Images.SetKeyName( 20, "rest20.png" );
			this.rest.Images.SetKeyName( 21, "rest21.png" );
			this.rest.Images.SetKeyName( 22, "rest22.png" );
			this.rest.Images.SetKeyName( 23, "rest23.png" );
			this.rest.Images.SetKeyName( 24, "rest24.png" );
			this.rest.Images.SetKeyName( 25, "rest25.png" );
			this.rest.Images.SetKeyName( 26, "rest26.png" );
			this.rest.Images.SetKeyName( 27, "rest27.png" );
			this.rest.Images.SetKeyName( 28, "rest28.png" );
			this.rest.Images.SetKeyName( 29, "rest29.png" );
			this.rest.Images.SetKeyName( 30, "rest30.png" );
			this.rest.Images.SetKeyName( 31, "rest31.png" );
			this.rest.Images.SetKeyName( 32, "rest32.png" );
			this.rest.Images.SetKeyName( 33, "rest33.png" );
			this.rest.Images.SetKeyName( 34, "rest34.png" );
			this.rest.Images.SetKeyName( 35, "rest35.png" );
			// 
			// restSelected
			// 
			this.restSelected.ImageStream = ( (System.Windows.Forms.ImageListStreamer)( resources.GetObject( "restSelected.ImageStream" ) ) );
			this.restSelected.TransparentColor = System.Drawing.Color.Transparent;
			this.restSelected.Images.SetKeyName( 0, "rest_s0.png" );
			this.restSelected.Images.SetKeyName( 1, "rest_s1.png" );
			this.restSelected.Images.SetKeyName( 2, "rest_s2.png" );
			this.restSelected.Images.SetKeyName( 3, "rest_s3.png" );
			this.restSelected.Images.SetKeyName( 4, "rest_s4.png" );
			this.restSelected.Images.SetKeyName( 5, "rest_s5.png" );
			this.restSelected.Images.SetKeyName( 6, "rest_s6.png" );
			this.restSelected.Images.SetKeyName( 7, "rest_s7.png" );
			this.restSelected.Images.SetKeyName( 8, "rest_s8.png" );
			this.restSelected.Images.SetKeyName( 9, "rest_s9.png" );
			this.restSelected.Images.SetKeyName( 10, "rest_s10.png" );
			this.restSelected.Images.SetKeyName( 11, "rest_s11.png" );
			this.restSelected.Images.SetKeyName( 12, "rest_s12.png" );
			this.restSelected.Images.SetKeyName( 13, "rest_s13.png" );
			this.restSelected.Images.SetKeyName( 14, "rest_s14.png" );
			this.restSelected.Images.SetKeyName( 15, "rest_s15.png" );
			this.restSelected.Images.SetKeyName( 16, "rest_s16.png" );
			this.restSelected.Images.SetKeyName( 17, "rest_s17.png" );
			this.restSelected.Images.SetKeyName( 18, "rest_s18.png" );
			this.restSelected.Images.SetKeyName( 19, "rest_s19.png" );
			this.restSelected.Images.SetKeyName( 20, "rest_s20.png" );
			this.restSelected.Images.SetKeyName( 21, "rest_s21.png" );
			this.restSelected.Images.SetKeyName( 22, "rest_s22.png" );
			this.restSelected.Images.SetKeyName( 23, "rest_s23.png" );
			this.restSelected.Images.SetKeyName( 24, "rest_s24.png" );
			this.restSelected.Images.SetKeyName( 25, "rest_s25.png" );
			this.restSelected.Images.SetKeyName( 26, "rest_s26.png" );
			this.restSelected.Images.SetKeyName( 27, "rest_s27.png" );
			this.restSelected.Images.SetKeyName( 28, "rest_s28.png" );
			this.restSelected.Images.SetKeyName( 29, "rest_s29.png" );
			this.restSelected.Images.SetKeyName( 30, "rest_s30.png" );
			this.restSelected.Images.SetKeyName( 31, "rest_s31.png" );
			this.restSelected.Images.SetKeyName( 32, "rest_s32.png" );
			this.restSelected.Images.SetKeyName( 33, "rest_s33.png" );
			this.restSelected.Images.SetKeyName( 34, "rest_s34.png" );
			this.restSelected.Images.SetKeyName( 35, "rest_s35.png" );

		}

		#endregion // Component Designer generated code

		#region Image indexers

		public readonly IIndexer<Image> Rest;
		public readonly IIndexer<Image> RestSelected;

		#endregion // Image indexers

		public static readonly MobileImages Instance;

		static MobileImages()
		{
			MobileImages.Instance = new MobileImages();
		}

		private class ImageIndexer :
			IIndexer<Image>
		{
			public ImageIndexer(ImageList.ImageCollection images)
			{
				this.images = images;
			}

			private readonly ImageList.ImageCollection images;
			public Image this[int index]
			{
				get { return this.images[index]; }
			}
		}
	}
}