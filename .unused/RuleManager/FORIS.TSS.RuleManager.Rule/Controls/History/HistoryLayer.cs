using System;
using System.ComponentModel;
using System.Drawing;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.MTMap.Spatial;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.RuleManager.Rule.Controls.History
{
	public class HistoryLayer :
		DataExtraLayer<HistoryObject, HistoryRow, HistoryTable, IHistoryData>,
		IImageSupplier<HistoryObject, HistoryLayer>,
		IDataItem<IRuleData>
	{
		#region Constructor & Dispose

		public HistoryLayer( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public HistoryLayer()
		{
			this.Objects.Cleared += this.Objects_Cleared;

			this.defaultIcon = Strings.History;
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				this.Objects.Cleared -= this.Objects_Cleared;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region IImageSupplier<HistoryObject, HistoryLayer> Members

		public Bitmap GetImage( HistoryObject obj )
		{
			int index = ( ( obj.Course % 360 + 360 ) % 360 ) / 10;

			if ( obj.Selected )
			{
				return (Bitmap)HistoryImages.Instance.RestSelected[index];
			}
			else
			{
				return (Bitmap)HistoryImages.Instance.Rest[index];
			}
		}

		#endregion // IImageSupplier<HistoryObject, HistoryLayer> Members

		#region IDataItem<IRuleData> Members

		private IRuleData ruleData;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		IRuleData IDataItem<IRuleData>.Data
		{
			get { return this.ruleData; }
			set { this.ruleData = value; }
		}

		#endregion  // IDataItem<IRuleData> Members

		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.History.Rows.Inserted -= this.HistoryRows_Inserted;
			this.Data.History.Rows.Removing -= this.HistoryRows_Removing;
		}

		protected override void OnAfterSetData()
		{
			this.Data.History.Rows.Inserted += this.HistoryRows_Inserted;
			this.Data.History.Rows.Removing += this.HistoryRows_Removing;
		}

		#endregion // Data

		#region Data events

		private void HistoryRows_Removing( object sender, CollectionChangeEventArgs<HistoryRow> e )
		{
			this.HideHistory( e.Item );
		}

		private void HistoryRows_Inserted( object sender, CollectionChangeEventArgs<HistoryRow> e )
		{
			this.ShowHistory( e.Item, this.mobileName );
		}

		#endregion // Data events

		#region View

		private string mobileName;

		protected override void OnDestroyView()
		{
			this.Objects.Clear();

			this.mobileName = String.Empty;
		}

		protected override void OnBuildView()
		{
			#region Preconditions

			if ( this.ruleData == null ) throw new ApplicationException( "��� ������ �� �������� ��������!" );

			#endregion  // Preconditions

			this.mobileName = this.Data.MobileName;

			this.UpdateBegin();

			foreach ( HistoryRow historyRow in this.Data.History.Rows )
			{
				this.ShowHistory( historyRow, mobileName );
			}

			this.UpdateEnd();
		}

		protected override void OnUpdateView()
		{

		}

		#endregion // View

		#region Show & Hide

		private void HideHistory( HistoryRow historyRow )
		{
			HistoryObject soughtHistoryObject = null;

			// TODO: ������� ����� �� ����
			foreach ( HistoryObject historyObject in this.Objects )
			{
				if ( historyObject.Id == historyRow.ID )
				{
					soughtHistoryObject = historyObject;
					break;
				}
			}

			if ( soughtHistoryObject != null )
			{
				soughtHistoryObject.ImageSupplier = null;
				this.Objects.Remove( soughtHistoryObject );
			}
		}

		private void ShowHistory( HistoryRow historyRow, string mobileName )
		{
			HistoryObject historyObject = new HistoryObject( historyRow.ID, mobileName );

			this.Objects.Add( historyObject );

			historyObject.ImageSupplier = this;
		}

		#endregion // Show & Hide

		#region Handle Objects events

		private void Objects_Cleared( object sender, CollectionEventArgs<HistoryObject> e )
		{
			this.UpdateBegin();

			foreach ( HistoryObject historyObject in e.Collection )
			{
				historyObject.ImageSupplier = null;
			}

			this.UpdateEnd();
		}

		#endregion // Handle Objects events

		private readonly Image defaultIcon;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override Image DefaultIcon
		{
			get
			{
				return this.defaultIcon;
			}
		}
	}
}