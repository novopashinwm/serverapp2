using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile;

namespace FORIS.TSS.RuleManager.Rule.Controls.History
{
	/// <summary>
	/// ������ �������� ��������.
	/// </summary>
	/// <remarks>
	/// ���� � ������� �������� ��� �����������, �� �� �� ������ ������������.
	/// </remarks>
	public class MobileWithControllerFilter :
		MobileFilter,
		IDataItem<IRuleData>
	{
		#region IDataItem<IRuleData> Members

		private IRuleData data;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		IRuleData IDataItem<IRuleData>.Data
		{
			get { return this.data; }
			set
			{
				if ( this.data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.data = value;

				if ( this.data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IRuleData> Members

		private readonly List<int> mobilesWithController = new List<int>();

		#region Data

		protected virtual void OnBeforeSetData()
		{
			this.data.Mobile.Rows.Inserted -= this.Mobile_Inserted;
			this.data.Mobile.Rows.Removing -= this.Mobile_Removing;
		}

		protected virtual void OnAfterSetData()
		{
			this.data.Mobile.Rows.Inserted += this.Mobile_Inserted;
			this.data.Mobile.Rows.Removing += this.Mobile_Removing;
		}

		#endregion // Data

		#region Data events

		private void Mobile_Inserted( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.MobileShow( e.Item.ID );
		}

		private void Mobile_Removing( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.MobileHide( e.Item.ID );
		}

		#endregion // Data events

		#region Show & Hide

		private void MobileHide( int mobile )
		{
			if ( this.mobilesWithController.Contains( mobile ) )
			{
				this.mobilesWithController.Remove( mobile );
			}
		}

		private void MobileShow( int mobile )
		{
			if ( !this.mobilesWithController.Contains( mobile ) && this.InternalCheck( mobile ) )
			{
				this.mobilesWithController.Add( mobile );
			}
		}

		#endregion // Show & Hide

		#region View

		protected virtual void OnDestroyView()
		{
		}

		protected virtual void OnBuildView()
		{
		}

		protected virtual void OnUpdateView()
		{
			foreach ( MobileRow mobileRow in this.data.Mobile.Rows )
			{
				this.MobileShow( mobileRow.ID );
			}
		}

		#endregion // View

		private bool InternalCheck( int call )
		{
			MobileRow currentRow = this.data.Mobile.FindRow( call );

			return currentRow.Controller != null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		public override bool Check( MobileRow row )
		{
			return this.mobilesWithController.Contains( row.ID );
		}
	}
}
