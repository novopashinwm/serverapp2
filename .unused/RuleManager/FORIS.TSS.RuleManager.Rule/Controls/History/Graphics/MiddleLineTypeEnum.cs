using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.TransportDispatcher;

namespace FORIS.TSS.RuleManager.Rule.Controls.History.Graphics
{
	public enum MiddleLineTypeEnum : int
	{
		/// <summary>
		/// ���������� �����������
		/// </summary>
		[DisplayName("������������", 1)]
		None,
		/// <summary>
		/// ���������� ������� ���������
		/// </summary>
		[DisplayName("������� ���������", 1)]
		Square,
		/// <summary>
		/// ���������� �� ������, � �������� �������� ����� 0
		/// </summary>
		[DisplayName("�� ������ � V=0 ��/�")]
		Speed0, 
		/// <summary>
		/// ���������� �� ������, � �������� ��������� ��������
		/// </summary>
		[DisplayName("�� ������ � V>0 ��/�")]
		Ignition 
	}
}
