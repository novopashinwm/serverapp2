using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace FORIS.TSS.RuleManager.Rule.Controls.History.Graphics
{
	/// <summary>
	/// Summary description for TempGraphControl.
	/// </summary>
	public class TempGraphControl : PositionsGraphControl
	{
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TempGraphControl));
			this.SuspendLayout();
			// 
			// TempGraphControl
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.BackgroundImage = null;
			this.Font = null;
			this.Name = "TempGraphControl";
			this.ResumeLayout(false);

		}
	}
}
