﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FORIS.TSS.Common.Controls.Graph;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using TerminalService.Interfaces;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;

namespace FORIS.TSS.RuleManager.Rule.Controls.History.Graphics
{
    public class HistoryGraphicsControl : UserControl
    {
        #region Controls & Components
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #endregion // Controls & Components


        #region Constructor & Dispose

        public HistoryGraphicsControl()
        {
            InitializeComponent();

            
            InitChartControl();

            //checkBoxFuelShowPoints.Checked = showPointsFuel;
            //checkBoxFuelMiddleLine.Checked = showMidleLineFuel;

            checkBoxSpeedShowPoints.Checked = showPointsSpeed;
            checkBoxSpeedMiddleLine.Checked = showMidleLineSpeed;

            checkBoxTempShowPoints.Checked = showPointsTemp;
            checkBoxTempMiddleLine.Checked = showMidleLineTemp;

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion // Constructor & Dispose


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageFuel = new System.Windows.Forms.TabPage();
            this.tabPageSpeed = new System.Windows.Forms.TabPage();
            this.tabPageTemperature = new System.Windows.Forms.TabPage();
            this.fuelControl1 = new FORIS.TSS.RuleManager.Rule.Controls.History.Graphics.FuelControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxMinSpeedValue = new System.Windows.Forms.TextBox();
            this.checkBoxSpeedMiddleLine = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeedShowPoints = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxMaxSpeedValue = new System.Windows.Forms.TextBox();
            this.speedGraphControl1 = new FORIS.TSS.RuleManager.Rule.Controls.History.Graphics.SpeedGraphControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxMinTempValue = new System.Windows.Forms.TextBox();
            this.checkBoxTempMiddleLine = new System.Windows.Forms.CheckBox();
            this.checkBoxTempShowPoints = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxMaxTempValue = new System.Windows.Forms.TextBox();
            this.tempGraphControl1 = new FORIS.TSS.RuleManager.Rule.Controls.History.Graphics.TempGraphControl();
            this.tabControl1.SuspendLayout();
            this.tabPageFuel.SuspendLayout();
            this.tabPageSpeed.SuspendLayout();
            this.tabPageTemperature.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPageFuel);
            this.tabControl1.Controls.Add(this.tabPageSpeed);
            this.tabControl1.Controls.Add(this.tabPageTemperature);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(299, 285);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageFuel
            // 
            this.tabPageFuel.Controls.Add(this.fuelControl1);
            this.tabPageFuel.Location = new System.Drawing.Point(4, 25);
            this.tabPageFuel.Name = "tabPageFuel";
            this.tabPageFuel.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFuel.Size = new System.Drawing.Size(291, 256);
            this.tabPageFuel.TabIndex = 0;
            this.tabPageFuel.Text = "Топливо";
            this.tabPageFuel.UseVisualStyleBackColor = true;
            // 
            // tabPageSpeed
            // 
            this.tabPageSpeed.Controls.Add(this.speedGraphControl1);
            this.tabPageSpeed.Controls.Add(this.panel3);
            this.tabPageSpeed.Location = new System.Drawing.Point(4, 25);
            this.tabPageSpeed.Name = "tabPageSpeed";
            this.tabPageSpeed.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSpeed.Size = new System.Drawing.Size(291, 256);
            this.tabPageSpeed.TabIndex = 1;
            this.tabPageSpeed.Text = "Скорость";
            this.tabPageSpeed.UseVisualStyleBackColor = true;
            // 
            // tabPageTemperature
            // 
            this.tabPageTemperature.Controls.Add(this.tempGraphControl1);
            this.tabPageTemperature.Controls.Add(this.panel4);
            this.tabPageTemperature.Location = new System.Drawing.Point(4, 25);
            this.tabPageTemperature.Name = "tabPageTemperature";
            this.tabPageTemperature.Size = new System.Drawing.Size(291, 256);
            this.tabPageTemperature.TabIndex = 2;
            this.tabPageTemperature.Text = "Температура";
            this.tabPageTemperature.UseVisualStyleBackColor = true;
            // 
            // fuelControl1
            // 
            this.fuelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelControl1.Location = new System.Drawing.Point(3, 3);
            this.fuelControl1.Name = "fuelControl1";
            this.fuelControl1.Size = new System.Drawing.Size(285, 250);
            this.fuelControl1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.textBoxMinSpeedValue);
            this.panel3.Controls.Add(this.checkBoxSpeedMiddleLine);
            this.panel3.Controls.Add(this.checkBoxSpeedShowPoints);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.textBoxMaxSpeedValue);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(285, 40);
            this.panel3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(16, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "min";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMinSpeedValue
            // 
            this.textBoxMinSpeedValue.Location = new System.Drawing.Point(40, 16);
            this.textBoxMinSpeedValue.Name = "textBoxMinSpeedValue";
            this.textBoxMinSpeedValue.Size = new System.Drawing.Size(80, 20);
            this.textBoxMinSpeedValue.TabIndex = 13;
            this.textBoxMinSpeedValue.Text = "1500";
            // 
            // checkBoxSpeedMiddleLine
            // 
            this.checkBoxSpeedMiddleLine.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxSpeedMiddleLine.Location = new System.Drawing.Point(128, 16);
            this.checkBoxSpeedMiddleLine.Name = "checkBoxSpeedMiddleLine";
            this.checkBoxSpeedMiddleLine.Size = new System.Drawing.Size(176, 24);
            this.checkBoxSpeedMiddleLine.TabIndex = 12;
            this.checkBoxSpeedMiddleLine.Text = "Показывать среднюю линию";
            // 
            // checkBoxSpeedShowPoints
            // 
            this.checkBoxSpeedShowPoints.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxSpeedShowPoints.Location = new System.Drawing.Point(128, 0);
            this.checkBoxSpeedShowPoints.Name = "checkBoxSpeedShowPoints";
            this.checkBoxSpeedShowPoints.Size = new System.Drawing.Size(168, 24);
            this.checkBoxSpeedShowPoints.TabIndex = 11;
            this.checkBoxSpeedShowPoints.Text = "Показывать точки графика";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(12, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "max";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMaxSpeedValue
            // 
            this.textBoxMaxSpeedValue.Location = new System.Drawing.Point(40, 0);
            this.textBoxMaxSpeedValue.Name = "textBoxMaxSpeedValue";
            this.textBoxMaxSpeedValue.Size = new System.Drawing.Size(80, 20);
            this.textBoxMaxSpeedValue.TabIndex = 9;
            this.textBoxMaxSpeedValue.Text = "1500";
            // 
            // speedGraphControl1
            // 
            this.speedGraphControl1.BeginTime = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
            this.speedGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.speedGraphControl1.IsAutoScrollRange = false;
            this.speedGraphControl1.IsEnableHPan = true;
            this.speedGraphControl1.IsEnableHZoom = true;
            this.speedGraphControl1.IsEnableVPan = true;
            this.speedGraphControl1.IsEnableVZoom = true;
            this.speedGraphControl1.IsPrintFillPage = true;
            this.speedGraphControl1.IsPrintKeepAspectRatio = true;
            this.speedGraphControl1.IsScrollY2 = false;
            this.speedGraphControl1.IsShowContextMenu = true;
            this.speedGraphControl1.IsShowCopyMessage = true;
            this.speedGraphControl1.IsShowCursorValues = false;
            this.speedGraphControl1.IsShowHScrollBar = false;
            this.speedGraphControl1.IsShowPointValues = true;
            this.speedGraphControl1.IsShowVScrollBar = false;
            this.speedGraphControl1.IsZoomOnMouseCenter = false;
            this.speedGraphControl1.Location = new System.Drawing.Point(3, 43);
            this.speedGraphControl1.Name = "speedGraphControl1";
            this.speedGraphControl1.PanButtons = System.Windows.Forms.MouseButtons.None;
            this.speedGraphControl1.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.speedGraphControl1.PanModifierKeys2 = System.Windows.Forms.Keys.None;
            this.speedGraphControl1.PointDateFormat = "g";
            this.speedGraphControl1.PointValueFormat = "G";
            this.speedGraphControl1.ScrollMaxX = 0;
            this.speedGraphControl1.ScrollMaxY = 0;
            this.speedGraphControl1.ScrollMaxY2 = 0;
            this.speedGraphControl1.ScrollMinX = 0;
            this.speedGraphControl1.ScrollMinY = 0;
            this.speedGraphControl1.ScrollMinY2 = 0;
            this.speedGraphControl1.ShowMidleLine = true;
            this.speedGraphControl1.ShowPoints = true;
            this.speedGraphControl1.Size = new System.Drawing.Size(285, 210);
            this.speedGraphControl1.TabIndex = 3;
            this.speedGraphControl1.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
            this.speedGraphControl1.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
            this.speedGraphControl1.ZoomModifierKeys = System.Windows.Forms.Keys.None;
            this.speedGraphControl1.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
            this.speedGraphControl1.ZoomStepFraction = 0.1;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.textBoxMinTempValue);
            this.panel4.Controls.Add(this.checkBoxTempMiddleLine);
            this.panel4.Controls.Add(this.checkBoxTempShowPoints);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.textBoxMaxTempValue);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(291, 40);
            this.panel4.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(16, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "min";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMinTempValue
            // 
            this.textBoxMinTempValue.Location = new System.Drawing.Point(40, 16);
            this.textBoxMinTempValue.Name = "textBoxMinTempValue";
            this.textBoxMinTempValue.Size = new System.Drawing.Size(80, 20);
            this.textBoxMinTempValue.TabIndex = 13;
            this.textBoxMinTempValue.Text = "1500";
            // 
            // checkBoxTempMiddleLine
            // 
            this.checkBoxTempMiddleLine.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxTempMiddleLine.Location = new System.Drawing.Point(128, 16);
            this.checkBoxTempMiddleLine.Name = "checkBoxTempMiddleLine";
            this.checkBoxTempMiddleLine.Size = new System.Drawing.Size(176, 24);
            this.checkBoxTempMiddleLine.TabIndex = 12;
            this.checkBoxTempMiddleLine.Text = "Показывать среднюю линию";
            // 
            // checkBoxTempShowPoints
            // 
            this.checkBoxTempShowPoints.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxTempShowPoints.Location = new System.Drawing.Point(128, 0);
            this.checkBoxTempShowPoints.Name = "checkBoxTempShowPoints";
            this.checkBoxTempShowPoints.Size = new System.Drawing.Size(168, 24);
            this.checkBoxTempShowPoints.TabIndex = 11;
            this.checkBoxTempShowPoints.Text = "Показывать точки графика";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(12, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "max";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMaxTempValue
            // 
            this.textBoxMaxTempValue.Location = new System.Drawing.Point(40, 0);
            this.textBoxMaxTempValue.Name = "textBoxMaxTempValue";
            this.textBoxMaxTempValue.Size = new System.Drawing.Size(80, 20);
            this.textBoxMaxTempValue.TabIndex = 9;
            this.textBoxMaxTempValue.Text = "1500";
            // 
            // tempGraphControl1
            // 
            this.tempGraphControl1.BeginTime = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
            this.tempGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tempGraphControl1.IsAutoScrollRange = false;
            this.tempGraphControl1.IsEnableHPan = true;
            this.tempGraphControl1.IsEnableHZoom = true;
            this.tempGraphControl1.IsEnableVPan = true;
            this.tempGraphControl1.IsEnableVZoom = true;
            this.tempGraphControl1.IsPrintFillPage = true;
            this.tempGraphControl1.IsPrintKeepAspectRatio = true;
            this.tempGraphControl1.IsScrollY2 = false;
            this.tempGraphControl1.IsShowContextMenu = true;
            this.tempGraphControl1.IsShowCopyMessage = true;
            this.tempGraphControl1.IsShowCursorValues = false;
            this.tempGraphControl1.IsShowHScrollBar = false;
            this.tempGraphControl1.IsShowPointValues = true;
            this.tempGraphControl1.IsShowVScrollBar = false;
            this.tempGraphControl1.IsZoomOnMouseCenter = false;
            this.tempGraphControl1.Location = new System.Drawing.Point(0, 40);
            this.tempGraphControl1.Name = "tempGraphControl1";
            this.tempGraphControl1.PanButtons = System.Windows.Forms.MouseButtons.None;
            this.tempGraphControl1.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.tempGraphControl1.PanModifierKeys2 = System.Windows.Forms.Keys.None;
            this.tempGraphControl1.PointDateFormat = "g";
            this.tempGraphControl1.PointValueFormat = "G";
            this.tempGraphControl1.ScrollMaxX = 0;
            this.tempGraphControl1.ScrollMaxY = 0;
            this.tempGraphControl1.ScrollMaxY2 = 0;
            this.tempGraphControl1.ScrollMinX = 0;
            this.tempGraphControl1.ScrollMinY = 0;
            this.tempGraphControl1.ScrollMinY2 = 0;
            this.tempGraphControl1.ShowMidleLine = true;
            this.tempGraphControl1.ShowPoints = true;
            this.tempGraphControl1.Size = new System.Drawing.Size(291, 216);
            this.tempGraphControl1.TabIndex = 3;
            this.tempGraphControl1.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
            this.tempGraphControl1.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
            this.tempGraphControl1.ZoomModifierKeys = System.Windows.Forms.Keys.None;
            this.tempGraphControl1.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
            this.tempGraphControl1.ZoomStepFraction = 0.1;
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Controls.Add(this.tabControl1);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(299, 285);
            this.tabControl1.ResumeLayout(false);
            this.tabPageFuel.ResumeLayout(false);
            this.tabPageSpeed.ResumeLayout(false);
            this.tabPageTemperature.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPageFuel;
        private TabPage tabPageSpeed;
        private TabPage tabPageTemperature;
        private FuelControl fuelControl1;
        private SpeedGraphControl speedGraphControl1;
        private Panel panel3;
        private Label label5;
        private TextBox textBoxMinSpeedValue;
        private CheckBox checkBoxSpeedMiddleLine;
        private CheckBox checkBoxSpeedShowPoints;
        private Label label6;
        private TextBox textBoxMaxSpeedValue;
        private Panel panel4;
        private Label label7;
        private TextBox textBoxMinTempValue;
        private CheckBox checkBoxTempMiddleLine;
        private CheckBox checkBoxTempShowPoints;
        private Label label8;
        private TextBox textBoxMaxTempValue;
        private TempGraphControl tempGraphControl1;

        public IRuleData ruleData;

        //Рисовать ли точки графика кружочками

        PointPair curPointPair = null;

        #region График скорости

        private bool isSymbolsCircleSpeed = true;
        private bool isLeftButtonZoomSpeed = true;

        private bool showPointsSpeed = true;
        private bool showMidleLineSpeed = true;

        //Максимальное значение на графике
        private double minSpeedValue = 0;
        private double maxSpeedValue = 0;

        private GraphPane speedPane = null;

        //Линия с точками
        private LineItem speedCurve = null;
        private LineItem speedMiddleLine = null;

        // точки с данными
        PointPairList speedList = new PointPairList();

        #endregion // График скорости

        #region График температуры

        private bool isSymbolsCircleTemp = true;
        private bool isLeftButtonZoomTemp = true;

        private bool showPointsTemp = true;
        private bool showMidleLineTemp = true;

        //Максимальное значение на графике
        private double minTempValue = 0;
        private double maxTempValue = 0;

        private GraphPane tempPane = null;

        //Линия с точками
        private LineItem tempCurve = null;
        private LineItem tempMiddleLine = null;

        // точки с данными
        PointPairList tempList = new PointPairList();

        #endregion // График температуры

        #region private void InitChartControl()

        /// <summary>
        /// Установка начальных значений контрола диаграмм
        /// </summary>
        private void InitChartControl()
        {
            speedPane = speedGraphControl1.GraphPane;

            // заголовок и названия осей
            speedPane.Title = "Скорость, км/ч";
            speedPane.XAxis.Title = "Время";
            speedPane.YAxis.Title = "Значения";
            speedPane.XAxis.Type = AxisType.Date;
            speedPane.XAxis.IsShowGrid = true;
            speedPane.YAxis.IsShowGrid = true;
            speedPane.YAxis.IsOppositeTic = false;
            speedPane.YAxis.IsMinorOppositeTic = false;
            speedPane.YAxis.IsZeroLine = true;
            speedPane.YAxis.ScaleAlign = AlignP.Inside;
            speedPane.XAxis.IsZeroLine = true;
            speedGraphControl1.IsShowPointValues = true;

            speedGraphControl1.ContextMenuBuilder += new GraphControl.ContextMenuBuilderEventHandler(ContextMenuBuilderSpeed);

            tempPane = tempGraphControl1.GraphPane;

            // заголовок и названия осей
            tempPane.Title = "Температура в салоне, С";
            tempPane.XAxis.Title = "Время";
            tempPane.YAxis.Title = "Значения";
            tempPane.XAxis.Type = AxisType.Date;
            tempPane.XAxis.IsShowGrid = true;
            tempPane.YAxis.IsShowGrid = true;
            tempPane.YAxis.IsOppositeTic = false;
            tempPane.YAxis.IsMinorOppositeTic = false;
            tempPane.YAxis.IsZeroLine = true;
            tempPane.YAxis.ScaleAlign = AlignP.Inside;
            tempPane.XAxis.IsZeroLine = true;
            tempGraphControl1.IsShowPointValues = true;

            tempGraphControl1.ContextMenuBuilder += new GraphControl.ContextMenuBuilderEventHandler(ContextMenuBuilderTemp);
        }

        #endregion // private void InitChartControl()



        #region Обработка событий контрола графиков

        public void GraphicsControlUpdate(MobileRow mobileRow, HistoryTable historyTable)
        {
            ClearGraphics();

            if (historyTable == null || historyTable.Rows.Count < 1)
                return;

            DateTime beginTime = historyTable.Rows[1].LogTime.ToLocalTime();

            SetBeginTime(beginTime);
            
            TimeSpan beginSpan = new TimeSpan(beginTime.Hour, beginTime.Minute, beginTime.Second);

            int controllerID = (int)mobileRow.Controller; 
            int keyFuel = 0;
            int keyTemp = 0;

            ControllerSensorMapRow controllerSensorMapRow;
            ControllerSensorRow controllerSensorRow;

            // Ключ датчика топлива
            controllerSensorMapRow = this.ruleData.ControllerSensorMap.FindRow(controllerID, 1);
            if(controllerSensorMapRow != null)
            {
                controllerSensorRow = this.ruleData.ControllerSensor.FindRow(controllerSensorMapRow.ControllerSensor);
                keyFuel = controllerSensorRow.Number;
            }
            // Ключ датчика температуры
            controllerSensorMapRow = this.ruleData.ControllerSensorMap.FindRow(controllerID, 3);
            if (controllerSensorMapRow != null)
            {
                controllerSensorRow = this.ruleData.ControllerSensor.FindRow(controllerSensorMapRow.ControllerSensor);
                keyTemp = controllerSensorRow.Number;
            }

            foreach (HistoryRow historyRow in historyTable.Rows)
            {
                DateTime time = historyRow.LogTime.ToLocalTime();
                TimeSpan ts = beginSpan + (time - beginTime);

                ISensor sensor = Sensor.CreateFromByteArray(historyRow.Properties);

                // топливо
                if (sensor.Sensors.ContainsKey(keyFuel))
                {
                    int fuelVolume = (int) sensor.Sensors[keyFuel];
                    if (fuelVolume >= 0)
                        fuelControl1.GraphControl.PointList.Add(ts.TotalSeconds, fuelVolume, historyRow.Speed, historyRow.ID.ToString());
                }
                // скорость
                speedList.Add(ts.TotalSeconds, historyRow.Speed, historyRow.ID.ToString());
                // температура
                if (sensor.Sensors.ContainsKey(keyTemp))
                {
                    int tempVolume = (int) sensor.Sensors[keyTemp];
                    tempList.Add(ts.TotalSeconds, tempVolume, historyRow.ID.ToString());
                }
            }

            //Добавляем аппроксимированную кривую
            fuelControl1.Update();

            #region Скорость

            if (speedList.Count > 0)
            {
                maxSpeedValue = speedList[0].Y;
                minSpeedValue = speedList[0].Y;

                //Вычисляем точки для аппроксимированной прямой
                PointPairList listSpeedAprox = new PointPairList();
                for (int i = 0; i < speedList.Count - 1; i++)
                {
                    PointPair pp1 = speedList[i];
                    PointPair pp2 = speedList[i + 1];

                    if (maxSpeedValue < pp1.Y) maxSpeedValue = pp1.Y;
                    if (minSpeedValue > pp1.Y) minSpeedValue = pp1.Y;

                    double yMax = Math.Max(pp1.Y, pp2.Y);
                    double y = yMax - Math.Abs((pp2.Y - pp1.Y)) / 2;
                    listSpeedAprox.Add(pp1.X, y);
                }

                //Добавляем точки в контрол
                speedCurve = speedPane.AddCurve("Скорость км/ч", speedList, Color.Black,
                    isSymbolsCircleSpeed ? SymbolType.Circle : SymbolType.None);
                // Заполним символы белым цветом
                speedCurve.Symbol.Fill = new Fill(Color.White);
                speedCurve.IsLegendLabelVisible = false;
                speedCurve.IsVisible = true; //showPointsFuel;

                //Добавляем аппроксимированную кривую
                speedMiddleLine = speedPane.AddCurve("Аппрокс Скорость",
                    listSpeedAprox, Color.Blue, SymbolType.None);
                speedMiddleLine.IsLegendLabelVisible = false;
                speedMiddleLine.IsVisible = isSymbolsCircleSpeed;
                speedMiddleLine.Line.Width = 2;

                textBoxMaxSpeedValue.Text = maxSpeedValue.ToString();
                textBoxMinSpeedValue.Text = minSpeedValue.ToString();

            }
            #endregion // Топливо

            #region Температура

            if (tempList.Count > 0)
            {
                maxTempValue = tempList[0].Y;
                minTempValue = tempList[0].Y;

                PointPairList listTempAprox = new PointPairList();
                for (int i = 0; i < tempList.Count - 1; i++)
                {
                    PointPair pp1 = tempList[i];
                    PointPair pp2 = tempList[i + 1];

                    if (maxTempValue < pp1.Y) maxTempValue = pp1.Y;
                    if (minTempValue > pp1.Y) minTempValue = pp1.Y;

                    double yMax = Math.Max(pp1.Y, pp2.Y);
                    double y = yMax - Math.Abs((pp2.Y - pp1.Y)) / 2;
                    listTempAprox.Add(pp1.X, y);
                }

                //Добавляем точки в контрол
                tempCurve = tempPane.AddCurve("Показания топливного датчика", tempList, Color.Black,
                    isSymbolsCircleTemp ? SymbolType.Circle : SymbolType.None);
                // Заполним символы белым цветом
                tempCurve.Symbol.Fill = new Fill(Color.White);
                tempCurve.IsLegendLabelVisible = false;
                tempCurve.IsVisible = true; //showPointsFuel;

                //Добавляем аппроксимированную кривую
                tempMiddleLine = tempPane.AddCurve("Аппрокс Темп",
                    listTempAprox, Color.Blue, SymbolType.None);
                tempMiddleLine.IsLegendLabelVisible = false;
                tempMiddleLine.IsVisible = isSymbolsCircleTemp;
                tempMiddleLine.Line.Width = 2;

                textBoxMaxTempValue.Text = maxTempValue.ToString();
                textBoxMinTempValue.Text = minTempValue.ToString();

            }
            #endregion // Топливо

            //масштаб по-умолчанию

            this.Invoke(
                new ThreadStart(
                    delegate()
                    {
                        fuelControl1.GraphControl.RestoreScale(fuelControl1.GraphControl.GraphPane);
                        this.speedGraphControl1.RestoreScale(speedGraphControl1.GraphPane);
                        this.tempGraphControl1.RestoreScale(tempGraphControl1.GraphPane);
                    }
                    )
                );


        }

        /// <summary>
        /// Очищает содержимое графиков истории движения
        /// </summary>
        private void ClearGraphics()
        {
            fuelControl1.ClearGraphics();

            if (speedList != null)
                speedList.Clear();
            if (speedPane != null)
                speedPane.CurveList.Clear();
            if (tempList != null)
                tempList.Clear();
            if (tempPane != null)
                tempPane.CurveList.Clear();
        }

        /// <summary>
        /// Устанавливает дату начала графиков
        /// </summary>
        /// <param name="beginTime"></param>
        private void SetBeginTime(DateTime beginTime)
        {
            fuelControl1.GraphControl.BeginTime = beginTime;
            speedGraphControl1.BeginTime = beginTime;
            tempGraphControl1.BeginTime = beginTime;
        }

        void OnPointPairSelected(PositionsGraphControl sender, int selectedID)
        {

        }

        /// <summary>
        /// Меняет точки на графике: кружки / нет
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SymbolsCircleSpeed(object sender, EventArgs args)
        {
            if (speedCurve != null)
            {
                isSymbolsCircleSpeed = !isSymbolsCircleSpeed;
                speedCurve.Symbol.Type = isSymbolsCircleSpeed == true ? SymbolType.Circle : SymbolType.None;
                speedGraphControl1.Invalidate();
                ((MenuItem)sender).Checked = isSymbolsCircleSpeed;
            }
        }

        /// <summary>
        /// Меняет точки на графике: кружки / нет
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SymbolsCircleTemp(object sender, EventArgs args)
        {
            if (tempCurve != null)
            {
                isSymbolsCircleTemp = !isSymbolsCircleTemp;
                tempCurve.Symbol.Type = isSymbolsCircleTemp == true ? SymbolType.Circle : SymbolType.None;
                tempGraphControl1.Invalidate();
                ((MenuItem)sender).Checked = isSymbolsCircleTemp;
            }
        }

        private void ZoomSpeed(object sender, EventArgs args)
        {
            isLeftButtonZoomSpeed = !isLeftButtonZoomSpeed;
            ((MenuItem)sender).Checked = !isLeftButtonZoomSpeed;
            if (isLeftButtonZoomSpeed)
                speedGraphControl1.PanButtons2 = MouseButtons.Left;
            else
                speedGraphControl1.PanButtons2 = MouseButtons.None;
        }

        private void ZoomTemp(object sender, EventArgs args)
        {
            isLeftButtonZoomTemp = !isLeftButtonZoomTemp;
            ((MenuItem)sender).Checked = !isLeftButtonZoomTemp;
            if (isLeftButtonZoomTemp)
                tempGraphControl1.PanButtons2 = MouseButtons.Left;
            else
                tempGraphControl1.PanButtons2 = MouseButtons.None;
        }

        private string PointValueEvent(GraphControl control, GraphPane pane, CurveItem curve, int iPt)
        {
            curPointPair = curve[iPt];
            TimeSpan ts = TimeSpan.FromSeconds(curPointPair.X);
            DateTime time = ((PositionsGraphControl)control).BeginTime.Date.AddSeconds(curPointPair.X);
            return String.Format("Значение: {0}\r\nВремя: {1}\r\nДата: {2}",
                                curPointPair.Y.ToString("f0"),
                                ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00"),
                                time.ToShortDateString());
        }

        private void checkBoxSpeedShowPoints_CheckedChanged(object sender, System.EventArgs e)
        {
            showPointsSpeed = checkBoxSpeedShowPoints.Checked;
            if (speedCurve != null)
            {
                speedCurve.IsVisible = showPointsSpeed;
                speedGraphControl1.Invalidate();
            }
        }

        private void checkBoxSpeedMiddleLine_CheckedChanged(object sender, System.EventArgs e)
        {
            showMidleLineSpeed = checkBoxSpeedMiddleLine.Checked;
            if (speedMiddleLine != null)
            {
                speedMiddleLine.IsVisible = showMidleLineSpeed;
                speedGraphControl1.Invalidate();
            }
        }

        private void checkBoxTempShowPoints_CheckedChanged(object sender, System.EventArgs e)
        {
            showPointsTemp = checkBoxTempShowPoints.Checked;
            if (tempCurve != null)
            {
                tempCurve.IsVisible = showPointsTemp;
                tempGraphControl1.Invalidate();
            }
        }

        private void checkBoxTempMiddleLine_CheckedChanged(object sender, System.EventArgs e)
        {
            showMidleLineTemp = checkBoxTempMiddleLine.Checked;
            if (tempMiddleLine != null)
            {
                tempMiddleLine.IsVisible = showMidleLineTemp;
                tempGraphControl1.Invalidate();
            }
        }

        /// <summary>
        /// Добавляем пункт меню для контекстного меню
        /// </summary>
        private void ContextMenuBuilderSpeed(GraphControl control, ContextMenu menu, Point mousePt)
        {
            MenuItem menuItem;

            menuItem = new MenuItem();
            menuItem.Index = menu.MenuItems.Count + 1;
            menuItem.Text = "Левая кнопка мыши - перемещение";
            menuItem.Checked = isLeftButtonZoomSpeed;
            menuItem.Click += new System.EventHandler(ZoomSpeed);
            menu.MenuItems.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Index = menu.MenuItems.Count + 1;
            menuItem.Text = "Показывать точки графика";
            menuItem.Checked = isSymbolsCircleSpeed;
            menuItem.Click += new System.EventHandler(SymbolsCircleSpeed);
            menu.MenuItems.Add(menuItem);
        }

        /// <summary>
        /// Добавляем пункт меню для контекстного меню
        /// </summary>
        private void ContextMenuBuilderTemp(GraphControl control, ContextMenu menu, Point mousePt)
        {
            MenuItem menuItem;

            menuItem = new MenuItem();
            menuItem.Index = menu.MenuItems.Count + 1;
            menuItem.Text = "Левая кнопка мыши - перемещение";
            menuItem.Checked = isLeftButtonZoomTemp;
            menuItem.Click += new System.EventHandler(ZoomTemp);
            menu.MenuItems.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Index = menu.MenuItems.Count + 1;
            menuItem.Text = "Показывать точки графика";
            menuItem.Checked = isSymbolsCircleTemp;
            menuItem.Click += new System.EventHandler(SymbolsCircleTemp);
            menu.MenuItems.Add(menuItem);
        }

        private void speedGraphControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);

        }

        private void tempGraphControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);

        }

        #endregion // Обработка событий контрола графиков



    }


    #region class HistoryInformation

    /// <summary>
    /// 
    /// </summary>
    public class HistoryInformation
    {
        #region Data

        /// <summary>
        /// Государственный номер ТС
        /// </summary>
        string publicNumber = String.Empty;

        /// <summary>
        /// Гаражный номер ТС
        /// </summary>
        string garageNumber = String.Empty;

        /// <summary>
        /// Время начала периода истории движения
        /// </summary>
        DateTime timeFrom;

        /// <summary>
        /// Время конца периода истории движения
        /// </summary>
        DateTime timeTo;

        /// <summary>
        /// по умолчанию 50 для отсечения шумов при построении топливного графика
        /// </summary>
        private int fuelTank = 50;

        #endregion // Data


        #region Properties

        /// <summary>
        /// Государственный номер ТС
        /// </summary>
        public string PublicNumber
        {
            get { return publicNumber; }
            set { publicNumber = value; }
        }

        /// <summary>
        /// Гаражный номер ТС
        /// </summary>
        public string GarageNumber
        {
            get { return garageNumber; }
            set { garageNumber = value; }
        }

        /// <summary>
        /// Время начала периода истории движения
        /// </summary>
        public DateTime TimeFrom
        {
            get { return timeFrom; }
            set { timeFrom = value; }
        }

        /// <summary>
        /// Время конца периода истории движения
        /// </summary>
        public DateTime TimeTo
        {
            get { return timeTo; }
            set { timeTo = value; }
        }

        /// <summary>
        /// по умолчанию 50 для отсечения шумов при построении топливного графика
        /// </summary>
        public int FuelTank
        {
            get { return this.fuelTank; }
        }

        #endregion // Properties

        #region .ctor

        public HistoryInformation()
        {
        }

        #endregion // .ctor

        #region Methods

        public void SetHistoryInformation(string garageNumber, string publicNumber, DateTime timeFrom, DateTime timeTo, int fuelTank)
        {
            this.garageNumber = garageNumber;
            this.publicNumber = publicNumber;
            this.timeFrom = timeFrom;
            this.timeTo = timeTo;

            if (fuelTank > 0)
                this.fuelTank = fuelTank;
        }

        #endregion // Methods
    }

    #endregion // class HistoryInformation


}
