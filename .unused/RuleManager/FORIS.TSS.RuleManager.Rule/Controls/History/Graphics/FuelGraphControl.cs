using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using FORIS.TSS.Common.Controls.Graph;

namespace FORIS.TSS.RuleManager.Rule.Controls.History.Graphics
{
	/// <summary>
	/// Summary description for FuelGraphControl.
	/// </summary>
	public class FuelGraphControl : PositionsGraphControl
	{
		#region .ctor

		private static int historyStopSpeed = 0;
        // �� ��������� �����, � ������� �������� ������� < 10% �� ������������� � ������
        private static double historyFuelLevelUproar = 0.1; //0.01;
        // ������ ������������� ��� ���������� ���������� ������� � ��������� �� ���������� ��������� ������� (�� ��������� 30%) 
        private static readonly double stepPercent = 0.3;

        //
	    private int fuelTank = 50;

		static FuelGraphControl()
		{
			if (FORIS.TSS.Config.Globals.AppSettings["HistoryStopSpeed"] != null)
				historyStopSpeed = Int32.Parse(FORIS.TSS.Config.Globals.AppSettings["HistoryStopSpeed"]);
            if (FORIS.TSS.Config.Globals.AppSettings["HistoryFuelLevelUproar"] != null)
                historyFuelLevelUproar = 0.01 * Int32.Parse(FORIS.TSS.Config.Globals.AppSettings["HistoryFuelLevelUproar"]);
            if (FORIS.TSS.Config.Globals.AppSettings["HistoryFuelDiscret"] != null)
                stepPercent = 0.01 * Int32.Parse(FORIS.TSS.Config.Globals.AppSettings["HistoryFuelDiscret"]);
        }

		public FuelGraphControl() : base()
		{
			CheckForIllegalCrossThreadCalls = false;

			InitializeComponent();

			InitChartControl();
		}

		#endregion // .ctor

		#region private void InitChartControl()

		/// <summary>
		/// ��������� ��������� �������� �������� ��������
		/// </summary>
		public override void InitChartControl()
		{
			base.InitChartControl();

			graphPane = this.GraphPane;

			// ��������� � �������� ����
			resources.ApplyResources(graphPane, "graphPane");
			resources.ApplyResources(graphPane.XAxis, "XAxis");
			resources.ApplyResources(graphPane.YAxis, "YAxis");
			//graphPane.XAxis.Title = "�����";
			//graphPane.YAxis.Title = "��������";
			graphPane.XAxis.Type = AxisType.Date;
			graphPane.XAxis.IsShowGrid = true;
			graphPane.YAxis.IsShowGrid = true;
			graphPane.YAxis.IsOppositeTic = false;
			graphPane.YAxis.IsMinorOppositeTic = false;
			graphPane.YAxis.IsZeroLine = true;
			graphPane.YAxis.ScaleAlign = AlignP.Inside;
			graphPane.XAxis.IsZeroLine = true;
			IsShowPointValues = true;
		}

		#endregion // private void InitChartControl()

		#region protected override void OnContextMenuBuilder(GraphControl control, ContextMenu menu, Point mousePt)

		/// <summary>
		/// ��������� ����� ���� ��� ������������ ����
		/// </summary>
        protected override void OnContextMenuBuilder(GraphControl control, ContextMenu menu, Point mousePt)
		{
			MenuItem menuItem;

			//menuItem = new MenuItem();
			//menuItem.Index = menu.MenuItems.Count + 1;
			//menuItem.Text = "������������ �������";
			//menuItem.Click += new EventHandler(SetMaxScale);
			//menu.MenuItems.Add(menuItem);

			//menuItem = new MenuItem();
			//menuItem.Index = menu.MenuItems.Count + 1;
			//menuItem.Text = "-";
			//menu.MenuItems.Add(menuItem);

			menuItem = new MenuItem();
			resources.ApplyResources(menuItem, "menuItem1");
			menuItem.Index = menu.MenuItems.Count + 1;
			//menuItem.Text = "����� ������ ���� - �����������";
			menuItem.Checked = isLeftButtonZoom;
			menuItem.Click += new EventHandler(Zoom);
			menu.MenuItems.Add(menuItem);

			menuItem = new MenuItem();
			resources.ApplyResources(menuItem, "menuItem2");
			menuItem.Index = menu.MenuItems.Count + 1;
			//menuItem.Text = "���������� ����� �������";
			menuItem.Checked = isSymbolsCircle;
			menuItem.Click += new EventHandler(SymbolsCircle);
			menu.MenuItems.Add(menuItem);
		}

		#endregion // protected override void OnContextMenuBuilder(GraphControl control, ContextMenu menu, Point mousePt)

		#region protected override void SetMaxScale(object sender, EventArgs args)

		/*
		/// <summary>
		/// ������ ������������ �������
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		protected override void SetMaxScale(object sender, EventArgs args)
		{
			base.SetMaxScale(sender, args);

			GraphPane.YAxis.Scale.MaxAuto = false;
			GraphPane.YAxis.Scale.MinAuto = false;
			GraphPane.YAxis.Scale.Max = maxValue * 2;
			GraphPane.YAxis.Scale.Min = 0;
			Invalidate();
		}
		*/
		 
		#endregion // protected override void SetMaxScale(object sender, EventArgs args)

		#region protected override void Zoom(object sender, EventArgs args)
		/// <summary>
		/// ������ ����� �� �������: ������ / ���
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		protected override void SymbolsCircle(object sender, EventArgs args)
		{
			if (mainLine != null)
			{
				isSymbolsCircle = !isSymbolsCircle;
				mainLine.Symbol.Type = isSymbolsCircle == true ? SymbolType.Circle : SymbolType.None;
				Invalidate();
				((MenuItem)sender).Checked = isSymbolsCircle;
			}
		}

		#endregion // protected override void Zoom(object sender, EventArgs args)

		#region protected override void Zoom(object sender, EventArgs args)

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		protected override void Zoom(object sender, EventArgs args)
		{
			isLeftButtonZoom = !isLeftButtonZoom;
			((MenuItem)sender).Checked = !isLeftButtonZoom;
			if (isLeftButtonZoom)
				PanButtons2 = MouseButtons.Left;
			else
				PanButtons2 = MouseButtons.None;
		}

		#endregion // protected override void Zoom(object sender, EventArgs args)

		#region Component Designer generated code
		System.ComponentModel.ComponentResourceManager resources;
		/// <summary>
		/// 
		/// </summary>
		private void InitializeComponent()
		{
			resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelGraphControl));
			this.SuspendLayout();
			// 
			// FuelGraphControl
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			resources.ApplyResources(this, "$this");
			this.BackgroundImage = null;
			this.Font = null;
			this.Name = "FuelGraphControl";
			this.PointValueEvent += new FORIS.TSS.Common.Controls.Graph.GraphControl.PointValueHandler(this.OnPointValueEvent);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
			this.ResumeLayout(false);

		}
		#endregion // Component Designer generated code

		#region public override void AddMiddleLine(int delta, out double minValue, out double maxValue)

        public void SetFuelTank()
        {
        }


	    public override void AddMiddleLine(int delta)
		{
			base.AddMiddleLine(delta);

			if (pointList.Count == 0)
				return;

			//��������� ����� ��� ������������������ ������
			int curTimePount = (int)pointList[0].X;
			int maxTimePoint = curTimePount + delta;
			int nTimePoint = 2; // ���-�� ����� � ����� ������������

			PointPairList listFuelAprox = new PointPairList();

			ArrayList xTimePoint = new ArrayList();
			ArrayList yTimePoint = new ArrayList();

			for (int i = 0; i < pointList.Count; i++)
			{
				// ����������� ����� ��������
				//if(pointList[i].Y == 0.0F)
				if (pointList[i].Y < maxValue * 0.1F)
					continue;

				// y = a+b*x							
				// �������� ������ �����
				xTimePoint.Add(pointList[i].X);
				yTimePoint.Add(pointList[i].Y);

				if ((pointList[i].X < maxTimePoint || xTimePoint.Count < nTimePoint)
				    && i < pointList.Count - 1)
				{
					continue;
				}

				// ��������� ����� ����� ������������
				double[] x = new double[xTimePoint.Count];
				double[] y = new double[yTimePoint.Count];
				double a = 0;

				xTimePoint.CopyTo(x);
				yTimePoint.CopyTo(y);

				MathUtils.LinearLeastSquares2(ref x, ref y, xTimePoint.Count, ref a);

				for (int j = 0; j < x.Length - 1; j++)
				{
					listFuelAprox.Add(x[j], a);
				}

				maxTimePoint = (int)pointList[i].X + delta;
				xTimePoint = new ArrayList();
				yTimePoint = new ArrayList();
			}

			CurveItem curveAprox = graphPane.CurveList["�������"];
			if (curveAprox != null)
				graphPane.CurveList.Remove(curveAprox);

			middleLine = graphPane.AddCurve( resources.GetString("Approx"), listFuelAprox, Color.Blue, SymbolType.None);
			middleLine.IsLegendLabelVisible = false;
			middleLine.IsVisible = ShowMidleLine;
			middleLine.Line.Width = 2;

			Invalidate();
		}

		#endregion // public override void AddMiddleLine(int delta, out double minValue, out double maxValue)

		#region public override void AddMiddleLine(int delta, out double minValue, out double maxValue, MiddleLineTypeEnum type)

        /// <summary>
        /// ������ ����� ���������� ������� ����� � ��� ���������� ������ �����/������
        /// </summary>
        private PointPairList pplMiddle = new PointPairList();

		public override void AddMiddleLine(int delta, MiddleLineTypeEnum type)
		{
			base.AddMiddleLine(delta);

			if (pointList.Count == 0)
				return;

            if (type == MiddleLineTypeEnum.Speed0)
            {
                PointPairList listFuelAprox = new PointPairList();

                // ����������� ����� ��������
                PointPairList pointList2 = new PointPairList();
                foreach (PointPair pp in pointList)
                    if (pp.Y >= this.fuelTank * historyFuelLevelUproar) //0.1F)
                        pointList2.Add(pp);

                // ����� ������������ ������ �����, � ������� �������� ������ 10%-�� ��������
                if (pointList2.Count < 2)
                    return;

                // ������ �����
                listFuelAprox.Add(pointList2[0].X, pointList2[0].Y);

                for (int i = 1; i < pointList2.Count - 1; i++)
                {
                    if (pointList2[i].Z <= historyStopSpeed)
                        listFuelAprox.Add(pointList2[i].X, pointList2[i].Y);
                }

                // ��������� �����
                listFuelAprox.Add(pointList2[pointList2.Count - 1].X, pointList2[pointList2.Count - 1].Y);

                CurveItem curveAprox = graphPane.CurveList["�������"];
                if (curveAprox != null)
                    graphPane.CurveList.Remove(curveAprox);

                middleLine = graphPane.AddCurve(resources.GetString("Approx"), listFuelAprox, Color.Blue, SymbolType.None);
                middleLine.IsLegendLabelVisible = false;
                middleLine.IsVisible = ShowMidleLine;
                middleLine.Line.Width = 2;

                Invalidate();
            }
            else if(type == MiddleLineTypeEnum.Ignition)
            {
                // ��������� �� �������
                pointList.Sort(SortType.XValues);
                
                // �������� ����� ��� "�����", � ������� �������� ������ 10%-�� ��������, �.�. ����� ���� ���������
                //PointPairList pointPairList = new PointPairList();
                //foreach (PointPair pp in pointList)
                //{
                //    if (pp.Y >= maxValue*historyFuelLevelUproar)
                //        pointPairList.Add(pp);
                //}
                //if (pointPairList.Count < 2)
                //    return;
                //CurveItem curveAprox = graphPane.CurveList["�������"];
                //if (curveAprox != null)
                //    graphPane.CurveList.Remove(curveAprox);
                //middleLine = graphPane.AddCurve(resources.GetString("Approx"), pointPairList, Color.Blue, SymbolType.None);
                //middleLine.IsLegendLabelVisible = false;
                //middleLine.IsVisible = ShowMidleLine;
                //middleLine.Line.Width = 2;
                //Invalidate(); 

                PointPairList pointPairList = PointsMiddle();

                CurveItem curveAprox = graphPane.CurveList["�������"];
                if (curveAprox != null)
                    graphPane.CurveList.Remove(curveAprox);

                middleLine = graphPane.AddCurve(resources.GetString("Approx"), pointPairList, Color.Blue, SymbolType.None);
                middleLine.IsLegendLabelVisible = false;
                middleLine.IsVisible = ShowMidleLine;
                middleLine.Line.Width = 2;
                Invalidate();
            }
            else
            {
                AddMiddleLine(delta);
            }
		}


        /// <summary>
        /// ���������� ����� ������� �����
        /// </summary>
        /// <returns></returns>
        protected PointPairList PointsMiddle()
        {
            List<PointPairList> pointPairSegmList = new List<PointPairList>();
            for (int i = 0; i < pointList.Count - 1; i++)
            {
                PointPair pp = pointList[i];
                if (pp.Y >= this.fuelTank * historyFuelLevelUproar)
                {
                    // ������ �����, ����� ���� ���������
                    PointPairList ppl = new PointPairList();

                    // ������ ����� (��������� ����� ������� ����� ������� ��������)
                    if (i > 0)
                        ppl.Add(pointList[i - 1]);

                    // ��������� ����� (�� �����)
                    while (pp.Y >= this.fuelTank * historyFuelLevelUproar && i < pointList.Count - 1)
                    {
                        ppl.Add(pp);
                        pp = pointList[++i];
                    }

                    // ��������� ����� (��������� ����� ������� ����� ������� ��������)
                    ppl.Add(pointList[i]);

                    // � ����� ������ ���� ����� 1 ����� (;
                    if (ppl.Count > 1)
                        pointPairSegmList.Add(ppl);
                }
            }

            this.pplMiddle.Clear(); //= new PointPairList();
            PointPairList pointPairList = new PointPairList();
            foreach (PointPairList ppl in pointPairSegmList)
            {
                // ����������� 
                PointPairList ppl_ = ApproxSensorMiddleVal_(ppl);
                foreach (PointPair pp in ppl_)
                {
                    pointPairList.Add(pp);
                    // ��������� ����� ����� ������� ����� � ��� ���������� ������ �����/������
                    this.pplMiddle.Add(pp);
                }
            }

            return pointPairList;
        }

        /// <summary>
        /// ���������� ����� ������� �����, ����� �� ����� 
        /// </summary>
        /// <returns></returns>
        protected PointPairList PointsMiddleStop()
        {
            List<PointPairList> pointPairSegmList = new List<PointPairList>();
            
            // �������� ����
            for (int i = 0; i < pointList.Count - 1; i++)
            {
                PointPair pp = pointList[i];
                if (pp.Y >= this.fuelTank * historyFuelLevelUproar)
                {
                    // ������ �����, ����� ���� ���������
                    PointPairList ppl = new PointPairList();

                    // ����� ����� �� ������� ����� �� ����� � ����� ����
                    if (pp.Z <= historyStopSpeed)
                    {
                        // ������ ����� (��������� ����� ������� ����� ������� ��������)
                        if (i > 0)
                            ppl.Add(pointList[i - 1]);

                        // ��������� ����� 
                        while (i < pointList.Count - 1
                            && pp.Y >= this.fuelTank * historyFuelLevelUproar 
                            && pp.Z <= historyStopSpeed)
                        {
                            ppl.Add(pp);
                            pp = pointList[++i];
                        }

                        // ��������� ����� (��������� ����� ������� ����� ������� ��������)
                        ppl.Add(pointList[i]);

                        // � ����� ������ ���� ����� 1 ����� (;
                        if (ppl.Count > 1)
                            pointPairSegmList.Add(ppl);
                    }
                    else //if(pp.Z > historyStopSpeed)
                    {
                        // ������ ����� (��������� ����� ������� ����� ������� ��������)
                        if (i > 0)
                            ppl.Add(pointList[i - 1]);

                        // ��������� ����� 
                        while (i < pointList.Count - 1
                            && pp.Y >= this.fuelTank * historyFuelLevelUproar
                            && pp.Z > historyStopSpeed)
                        {
                            ppl.Add(pp);
                            pp = pointList[++i];
                        }

                        // ��������� ����� (��������� ����� ������� ����� ������� ��������)
                        ppl.Add(pointList[i]);

                        // � ����� ������ ���� ����� 1 ����� (;
                        if (ppl.Count > 1)
                            pointPairSegmList.Add(ppl);
                    }
                }
            }


            this.pplMiddle.Clear(); //= new PointPairList();
            PointPairList pointPairList = new PointPairList();
            for (int i = 0; i < pointPairSegmList.Count; i++)
            {
                PointPairList ppl = pointPairSegmList[i];
                PointPairList ppl_ = new PointPairList(); 
                // ����������� 
                if (ppl[0].Z <= historyStopSpeed || i == 0 || i == pointPairSegmList.Count - 1)
                {
                    ppl_ = ApproxSensorMiddleVal_(ppl);
                }
                else //if(pp.Z > historyStopSpeed)
                {
                    ppl_ = new PointPairList();

                    //ppl_.Add(ppl[0]);
                    //ppl_.Add(ppl[ppl.Count-1]);
                    // ���� ����� ��� �����
                    // ������ ����� 
                    for (int k = 0; k < ppl.Count; k++)
                    {
                        if (ppl[k].Y >= this.fuelTank * historyFuelLevelUproar)
                        {
                            ppl_.Add(ppl[k]);
                            break;
                        }
                    }
                    // ��������� �����
                    for (int k = ppl.Count - 1; k > 0; k--)
                    {
                        if (ppl[k].Y >= this.fuelTank * historyFuelLevelUproar)
                        {
                            ppl_.Add(ppl[k]);
                            break;
                        }
                    }

                }

                foreach (PointPair pp in ppl_)
                {
                    pointPairList.Add(pp);
                    // ��������� ����� ����� ������� ����� � ��� ���������� ������ �����/������
                    this.pplMiddle.Add(pp);
                }
            }

            return pointPairList;
        }


        /// <summary>
        /// ��������� ����� �� �������������� ��������� ������� � �������� �������� ������������� (��������� � ����� ����� ����� �������)
        /// </summary>
        /// <param name="ppl"></param>
        protected PointPairList ApproxSensorVal(PointPairList ppl)
        {
            if (ppl == null || ppl.Count < 3)
                return ppl;

            PointPairList pplNew = new PointPairList();

            // ������ ������������� ��� �������
            double p = (ppl[ppl.Count - 1].X - ppl[0].X) * stepPercent;

            int i = 0;
            int iCount = ppl[ppl.Count - 1].Y < this.fuelTank * historyFuelLevelUproar ? ppl.Count - 2 : ppl.Count - 1;
            // ������(��) �����(�)
            if (ppl[i].Y < this.fuelTank * historyFuelLevelUproar)
            {
                //pplNew.Add(ppl[i]); // ����������� ���� �� ����� ������� ����� ������� ����� 
                i++;
            }
            pplNew.Add(ppl[i]);
            PointPair pp = ppl[i];
            for (++i; i < iCount; i++)
            {
                if ((ppl[i].X - pp.X) < p)
                    continue;
                pplNew.Add(ppl[i]);
                pp = ppl[i];
            }
            // ���������(��) �����(�)
            pplNew.Add(ppl[iCount]);
            if (iCount < ppl.Count - 1)//(ppl[ppl.Count - 1].Y < maxValue * historyFuelLevelUproar)
            {
                //pplNew.Add(ppl[++iCount]); // ����������� ���� �� ����� ������� ����� ������� ����� 
            }

            return pplNew;
        }


	    /// <summary>
        /// ��������� ����� �� �������� �������� �������� �� ������� � �������� �������� ������������� (�������� ������� ������ ����� �� ������ �������)
        /// </summary>
        /// <param name="ppl"></param>
        protected PointPairList ApproxSensorMiddleVal(PointPairList ppl)
        {
            if (ppl == null || ppl.Count < 2)
                return ppl;

            PointPairList pplNew = new PointPairList();

            // ������ ������������� ��� �������
            double p = (ppl[ppl.Count - 1].X - ppl[0].X) * stepPercent;    

            // ��������� ������� ����� ������� ����� (���� ��� ����) 
            int i = 0;
            int iCount = ppl[ppl.Count - 1].Y < this.fuelTank * historyFuelLevelUproar ? ppl.Count - 2 : ppl.Count - 1;
            if (ppl[i].Y < this.fuelTank * historyFuelLevelUproar)
            {
                i++;
            }

            while (i < iCount) 
            {
                // ������� ��������
                double middle = 0;
                int count = 0;

                // ������ �����
                PointPair ppA = new PointPair(ppl[i].X, ppl[i].Y, ppl[i].Z);
                middle += ppl[i].Y;
                count++;
                while (i < iCount && (ppl[++i].X - ppA.X) < p)
                {
                    middle += ppl[i].Y;
                    count++;
                }
                // ������ �����
                PointPair ppB = new PointPair(ppl[i].X, ppl[i].Y, ppl[i].Z);
                middle += ppl[i].Y;
                count++;

                // ������� �� ����������� �������� ��������
                ppA.Y = ppB.Y = middle /= count;
                pplNew.Add(ppA);
                // 1) �������������� �����������
                //pplNew.Add(ppB);
                // 2) ��� ������������� ��������
                if(i == iCount) pplNew.Add(ppB);
            }

            return pplNew;
        }

        /// <summary>
        /// ��������� ����� �� �������� �������� �������� �� ������� � �������� �������� ������������� (�������� ������� ������ ����� �� ������ �������)
        /// ��������� ��������� ����� ������� ����� ��� ����� � ������ ��� �������
        /// </summary>
        /// <param name="ppl"></param>
        protected PointPairList ApproxSensorMiddleVal_(PointPairList ppl)
        {
            if (ppl == null || ppl.Count < 2)
                return ppl;

            PointPairList pplNew = new PointPairList();

            // ������ ������������� ��� �������
            double p = (ppl[ppl.Count - 1].X - ppl[0].X) * stepPercent;    

            // ��������� ������� ����� ������� ����� (���� ��� ����) 
            int i = 0;
            int iCount = ppl[ppl.Count - 1].Y < this.fuelTank * historyFuelLevelUproar ? ppl.Count - 2 : ppl.Count - 1;
            if (ppl[i].Y < this.fuelTank * historyFuelLevelUproar)
            {
                i++;
            }

            while (i < iCount)
            {
                // ������� ��������
                double middle = 0;
                int count = 0;

                // ������ �����
                PointPair ppA = new PointPair(ppl[i].X, ppl[i].Y, ppl[i].Z);
                middle += ppl[i].Y;
                count++;
                while (i < iCount && (ppl[++i].X - ppA.X) < p)
                {
                    middle += ppl[i].Y;
                    count++;
                }
                // ������ �����
                PointPair ppB = new PointPair(ppl[i].X, ppl[i].Y, ppl[i].Z);
                middle += ppl[i].Y;
                count++;

                // ������� �� ����������� �������� ��������
                ppA.Y = ppB.Y = middle /= count;
                pplNew.Add(ppA);
                // 1) �������������� �����������
                pplNew.Add(ppB);
                // 2) ��� ������������� ��������
                //if (i == iCount) pplNew.Add(ppB);
            }

            // ����� ���� ������ � ���������� ��������� Y (������� �����), ���������� ������� �������� ��� ����� � �������
            PointPairList pplDel = new PointPairList();
            for(i = 2; i < pplNew.Count-2; i+=2) 
            {
                PointPair pp1 = pplNew[i];
                PointPair pp2 = pplNew[i+1];
                PointPair ppPre1 = pplNew[i-2];
                PointPair ppPre2 = pplNew[i-1];

                if (pp1.Y < ppPre2.Y)
                    pplDel.Add(pp1);
                else if (pp1.Y > ppPre2.Y)
                    pplDel.Add(pp2);
            }
            foreach (PointPair pp in pplDel)
            {
                pplNew.Remove(pp);
            }

            return pplNew;
        }

        #endregion // public override void AddMiddleLine(int delta, out double minValue, out double maxValue, MiddleLineTypeEnum type)


        #region protected override string OnPointValueEvent(GraphControl control, GraphPane pane, CurveItem mainLine, int iPt)

        protected override string OnPointValueEvent(GraphControl control, GraphPane pane, CurveItem curve, int iPt)
		{
			curPointPair = curve[iPt];
			TimeSpan ts = TimeSpan.FromSeconds(curPointPair.X);
			DateTime time = ((PositionsGraphControl)control).BeginTime.Date.AddSeconds(curPointPair.X);
			return String.Format( resources.GetString("ValueTimeDate"),
			                     curPointPair.Y.ToString("f0"),
			                     ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00"),
			                     time.ToShortDateString());
		}

		#endregion // protected override string OnPointValueEvent(GraphControl control, GraphPane pane, CurveItem curve, int iPt)

		#region protected override void OnMouseUp(object sender, MouseEventArgs e)

		protected override void OnMouseUp(object sender, MouseEventArgs e)
		{
			if (curPointPair != null && curPointPair.Tag != null)
				SelectedPointPairID = Int32.Parse(curPointPair.Tag as string);
			else
				SelectedPointPairID = -1;
		}

		#endregion // protected override void OnMouseUp(object sender, MouseEventArgs e)

		/// <summary>
		/// ������� ���������� �������� ������� ��������
		/// </summary>
		public override void ClearGraphics()
		{
			base.ClearGraphics();
		}

		/// <summary>
		/// ��������� ������� ����� �������
		/// </summary>
		public override void FillMainLine(out double minValue, out double maxValue)
		{
            minValue = pointList[0].Y;
            maxValue = pointList[0].Y;

			for (int i = 0; i < pointList.Count - 1; i++)
			{
				PointPair pp1 = pointList[i];

                if (minValue > pp1.Y)
                    minValue = pp1.Y;
                if (maxValue < pp1.Y) 
					maxValue = pp1.Y;
			}

            this.minValue = minValue;
            this.maxValue = maxValue;

			pointList.Sort(SortType.XValues);

			// ������� ������ ������
			CurveItem curve = graphPane.CurveList["�������"];
			if (curve != null)
				graphPane.CurveList.Remove(curve);

			mainLine = graphPane.AddCurve( resources.GetString("Fuel"), pointList, Color.Black, isSymbolsCircle ? SymbolType.Circle : SymbolType.None);
			// �������� ������� ����� ������
			mainLine.Symbol.Fill = new Fill(Color.White);
			mainLine.IsLegendLabelVisible = false;
			mainLine.IsVisible = ShowPoints;

		}

		/// <summary>
		/// ��������� ����� ����\����� �������
		/// </summary>
        public void FillDischargeLines()
		{
            if (pointList.Count < 2)
                return;

            pointList.Sort(SortType.XValues);
            
            #region // ������ ����� �� ��������� 0 ��� ���������� ������ ����/����� - ������� ��� ��� �� ���������� ������������ 2007
            //List<PointPairList> speed0Lines = new List<PointPairList>();
            //for (int i=0; i<pointList.Count-1; i++)
            //{
            //    PointPair pp = pointList[i];
            //    if (pp.Z <= historyStopSpeed 
            //        )//&& pp.Y >= maxValue * historyFuelLevelUproar) //���� �� �������
            //    {
            //        PointPairList ppl = new PointPairList();
            //        // ��������� ���������� �����
            //        if (i > 0)
            //            ppl.Add(pointList[i - 1]);

            //        while (pp.Z <= historyStopSpeed && i < pointList.Count - 1)
            //        {
            //            ppl.Add(pp);
            //            pp = pointList[++i];
            //        }
            //        // ��������� ����������� �����
            //        if (i < pointList.Count)
            //            ppl.Add(pp);

            //        // � ����� ������ ���� ����� 1 ����� (;
            //        if (ppl.Count > 1)
            //            speed0Lines.Add(ppl);
            //    }
            //}

            //Trace.WriteLine("");

            //// ������� ������ �����\������
            //ClearDischargeCurves();

            //foreach (PointPairList ppl in speed0Lines)
            //    FillDischargeCurves(ppl);
            #endregion // ������ ����� �� ��������� 0 ��� ���������� ������ ����/����� - ������� ��� ��� �� ���������� ������������ 2007

            #region // �������!!! ������ ����/����� ����� �� �����

            //List<PointPairList> linesSpeed0 = new List<PointPairList>();
            //for (int i = 0; i < pointList.Count - 1; i++)
            //{
            //    PointPair pp = pointList[i];
            //    if (pp.Z <= historyStopSpeed
            //        && pp.Y >= maxValue * historyFuelLevelUproar) //��� �� ���������, ����� ����� ��� ���������� ���������
            //    {
            //        // ������ �����, ����� �� �����
            //        PointPairList ppl = new PointPairList();

            //        // ��������� ����� (�� �����)
            //        while (pp.Z <= historyStopSpeed && i < pointList.Count - 1)
            //        {
            //            if (pp.Y >= maxValue * historyFuelLevelUproar) //��� �� ���������, ����� ����� ��� ���������� ���������
            //                ppl.Add(pp); 
            //            pp = pointList[++i];
            //        }

            //        // � ����� ������ ���� ����� 1 ����� (;
            //        if (ppl.Count > 1)
            //            linesSpeed0.Add(ppl);
            //    }
            //}

            //PointPairList pointPairList = new PointPairList();
            //foreach (PointPairList ppl in linesSpeed0)
            //{
            //    foreach (PointPair pp in ppl)
            //    {
            //        pointPairList.Add(pp);
            //    }
            //}

            //// ������� ������ �����\������
            //ClearDischargeCurves();
            //// ��������������
            //FillDischargeCurves(pointPairList);

            #endregion // �������!!! ������ ����/����� ����� �� �����

            #region ������ ����/����� �� ������� �����

            if(this.pplMiddle == null || this.pplMiddle.Count < 2)
                return;

            PointPairList pointPairList = new PointPairList();
            foreach (PointPair pp in this.pplMiddle)
            {
                pointPairList.Add(pp);
            }
 
            // ������� ������ �����\������
            ClearDischargeCurves();
            // ��������������
            FillDischargeCurves(pointPairList);

            #endregion ������ ����/����� �� ������� �����

            // ������� ������ �� �����
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			Invalidate();
		}

		// ���-�� ������ ����\�����
		private int dischageLinesCount = 0;

		// ������� �����/������, ������� ���� ��������
		private double dischagePercent = 60.0;
        /// <summary>
        /// ������� �����/������, ������� ���� ��������
        /// </summary>
	    public double DischagePercent
	    {
            set { this.dischagePercent = value; }
	    }

		/// <summary>
		/// ������ ������ ����\�����
		/// </summary>
		/// <param name="ppl">������ ����� �� �������� 0</param>
		private void FillDischargeCurves(PointPairList ppl)
        {
            // ����� ������
			List<PointPairList> tankLines = new List<PointPairList>();
			// ����� �����
			List<PointPairList> dischageLines = new List<PointPairList>();

			double delta = 0;

            #region // ������ �� ������� ������ ����� (������� ��� �� ���������� ������������ 2007)
            //// 1. ������� ��� ����� �����
            //if (ppl[0].Y > ppl[ppl.Count - 1].Y)
            //{
            //    delta = 100F * (ppl[0].Y - ppl[ppl.Count - 1].Y) / ppl[0].Y;
            //    if (delta > dischagePercent)
            //    {
            //        PointPairList dischageLine = new PointPairList();
            //        dischageLine.Add(ppl[0]);
            //        dischageLine.Add(ppl[ppl.Count - 1]);
            //        dischageLines.Add(dischageLine);

            //        LineItem discargeCurve = graphPane.AddCurve("Discharge" + dischageLinesCount, dischageLine, Color.Red,
            //                                                    SymbolType.Triangle);
            //        // �������� ������� ����� ������
            //        discargeCurve.IsLegendLabelVisible = false;
            //        discargeCurve.IsVisible = true; //showPointsFuel;
            //        discargeCurve.Line.Fill = new Fill(Color.White, Color.Red, 45F);
            //        discargeCurve.Symbol.Size = 8.0F;

            //        dischageLinesCount++;
            //    }
            //}

            //// 2. ������� ��� ����� ������
            //if (ppl[0].Y < ppl[ppl.Count - 1].Y)
            //{
            //    delta = 100F * (ppl[ppl.Count - 1].Y - ppl[0].Y) / ppl[ppl.Count - 1].Y;
            //    if (delta > dischagePercent)
            //    {
            //        PointPairList tankLine = new PointPairList();
            //        tankLine.Add(ppl[0]);
            //        tankLine.Add(ppl[ppl.Count - 1]);

            //        tankLines.Add(tankLine);

            //        LineItem discargeCurve = graphPane.AddCurve("Discharge" + dischageLinesCount, tankLine, Color.Green,
            //                                                    SymbolType.Triangle);
            //        // �������� ������� ����� ������
            //        discargeCurve.IsLegendLabelVisible = false;
            //        discargeCurve.IsVisible = true; //showPointsFuel;
            //        discargeCurve.Line.Fill = new Fill(Color.White, Color.Green, 45F);
            //        discargeCurve.Symbol.Size = 8.0F;

            //        dischageLinesCount++;
            //    }
            //}
            #endregion // ������ �� ������� ������ ����� (������� ��� �� ���������� ������������ 2007)

            #region ������ �� ���������� ����� ������ ����� ����� �����
            // ������� ��� ����� ����� � ������
            for (int i = 0; i < ppl.Count - 1; i++)
            {
                // 1. ������� ��� ����� �����
                if (ppl[i].Y > ppl[i + 1].Y)
                {
                    delta = (ppl[i].Y - ppl[i + 1].Y) * 100F / ppl[i].Y;
                    if( delta > dischagePercent)
                    {
                        PointPairList dischageLine = new PointPairList();
                        // ������ �����
                        dischageLine.Add(ppl[i]);

                        // ���� ��� ������� ������
                        i++;
                        while (i < ppl.Count - 1
                            && ppl[i].Y > ppl[i + 1].Y 
                            && ((ppl[i].Y - ppl[i + 1].Y) * 100F / ppl[i].Y) > dischagePercent
                            )
                        {
                            i++;
                            continue; 
                        }
                        i--;

                        // ������ �����
                        dischageLine.Add(ppl[i + 1]);  
                        dischageLines.Add(dischageLine);

                        LineItem discargeCurve = graphPane.AddCurve("Discharge" + dischageLinesCount, dischageLine, Color.Red, SymbolType.Triangle);
                        // �������� ������� ����� ������
                        discargeCurve.IsLegendLabelVisible = false;
                        discargeCurve.IsVisible = true; //showPointsFuel;
                        discargeCurve.Line.Fill = new Fill(Color.White, Color.Red, 45F);
                        discargeCurve.Symbol.Size = 8.0F;

                        dischageLinesCount++;
                    }
                }
                // 2. ������� ��� ����� ������
                else if (ppl[i].Y < ppl[i + 1].Y)
                {
                    delta = (ppl[i + 1].Y - ppl[i].Y) * 100F / ppl[i + 1].Y;
                    if (delta > dischagePercent)
                    {
                        PointPairList tankLine = new PointPairList();
                        // ������ �����
                        tankLine.Add(ppl[i]);

                        // ���� ��� ������� ������
                        i++;
                        while (i < ppl.Count - 1
                            && ppl[i].Y < ppl[i + 1].Y
                            && ((ppl[i + 1].Y - ppl[i].Y) * 100F / ppl[i + 1].Y) > dischagePercent
                            )
                        {
                            i++;
                            continue;
                        }
                        i--;

                        // ������ �����
                        tankLine.Add(ppl[i + 1]);
                        tankLines.Add(tankLine);

                        LineItem discargeCurve = graphPane.AddCurve("Discharge" + dischageLinesCount, tankLine, Color.Green, SymbolType.Triangle);
                        // �������� ������� ����� ������
                        discargeCurve.IsLegendLabelVisible = false;
                        discargeCurve.IsVisible = true; //showPointsFuel;
                        discargeCurve.Line.Fill = new Fill(Color.White, Color.Green, 45F);
                        discargeCurve.Symbol.Size = 8.0F;

                        dischageLinesCount++;
                    }
                }
            }
		    #endregion ������ �� ���������� ����� ������ ����� ����� �����

        }

		/// <summary>
		/// ������� ������ �����\������
		/// </summary>
		private void ClearDischargeCurves()
		{
			if (dischageLinesCount == 0)
				return;

			// ������� ������ ������ � �����
			for (int i = 0; i < dischageLinesCount; i++ )
			{
				CurveItem curve = graphPane.CurveList["Discharge" + i];
				if (curve != null)
				    graphPane.CurveList.Remove(curve);
			}
			
			dischageLinesCount = 0;
		}

	}
}
