using System;
using System.Windows.Forms;
using FORIS.TSS.TransportDispatcher;

namespace FORIS.TSS.RuleManager.Rule.Controls.History.Graphics
{
	public class FuelControl : UserControl
	{
		private delegate void MinMaxEventHandler( double minValue, double maxValue );

		/// <summary>
		/// �������� ��� ��������� ������� � ������ ������������ �������
		/// </summary>
		public event PointPairSelectedHandler PointPairSelected;

		private Panel panel2;
		private ComboBox comboBoxMiddleTime;
		private Label label4;
		private TextBox textBoxMinValue;
		private CheckBox checkBoxFuelMiddleLine;
		private CheckBox checkBoxFuelShowPoints;
		private Label label20;
		private TextBox textBoxMaxValue;
		private CheckBox checkBoxFuelMiddleLineSpeed0;
		private ComboBox comboBoxPercents;
		private ToolTip toolTip1;
		private System.ComponentModel.IContainer components;
        private Label label1;

		private FuelGraphControl graphControl;


		public FuelControl()
		{
			CheckForIllegalCrossThreadCalls = false;

			InitializeComponent();
		}

		#region Component Designer generated code
		/// <summary>
		/// 
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelControl));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxFuelShowPoints = new System.Windows.Forms.CheckBox();
            this.comboBoxPercents = new System.Windows.Forms.ComboBox();
            this.checkBoxFuelMiddleLineSpeed0 = new System.Windows.Forms.CheckBox();
            this.comboBoxMiddleTime = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMinValue = new System.Windows.Forms.TextBox();
            this.checkBoxFuelMiddleLine = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxMaxValue = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.graphControl = new FuelGraphControl();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.checkBoxFuelShowPoints);
            this.panel2.Controls.Add(this.comboBoxPercents);
            this.panel2.Controls.Add(this.checkBoxFuelMiddleLineSpeed0);
            this.panel2.Controls.Add(this.comboBoxMiddleTime);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.textBoxMinValue);
            this.panel2.Controls.Add(this.checkBoxFuelMiddleLine);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.textBoxMaxValue);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBoxFuelShowPoints
            // 
            resources.ApplyResources(this.checkBoxFuelShowPoints, "checkBoxFuelShowPoints");
            this.checkBoxFuelShowPoints.Name = "checkBoxFuelShowPoints";
            this.checkBoxFuelShowPoints.CheckedChanged += new System.EventHandler(this.checkBoxFuelShowPoints_CheckedChanged);
            // 
            // comboBoxPercents
            // 
            resources.ApplyResources(this.comboBoxPercents, "comboBoxPercents");
            this.comboBoxPercents.Name = "comboBoxPercents";
            this.toolTip1.SetToolTip(this.comboBoxPercents, resources.GetString("comboBoxPercents.ToolTip"));
            // 
            // checkBoxFuelMiddleLineSpeed0
            // 
            this.checkBoxFuelMiddleLineSpeed0.Checked = true;
            this.checkBoxFuelMiddleLineSpeed0.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.checkBoxFuelMiddleLineSpeed0, "checkBoxFuelMiddleLineSpeed0");
            this.checkBoxFuelMiddleLineSpeed0.Name = "checkBoxFuelMiddleLineSpeed0";
            this.checkBoxFuelMiddleLineSpeed0.CheckedChanged += new System.EventHandler(this.checkBoxFuelMiddleLineSpeed0_CheckedChanged);
            // 
            // comboBoxMiddleTime
            // 
            resources.ApplyResources(this.comboBoxMiddleTime, "comboBoxMiddleTime");
            this.comboBoxMiddleTime.Name = "comboBoxMiddleTime";
            this.toolTip1.SetToolTip(this.comboBoxMiddleTime, resources.GetString("comboBoxMiddleTime.ToolTip"));
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // textBoxMinValue
            // 
            resources.ApplyResources(this.textBoxMinValue, "textBoxMinValue");
            this.textBoxMinValue.Name = "textBoxMinValue";
            // 
            // checkBoxFuelMiddleLine
            // 
            this.checkBoxFuelMiddleLine.Checked = true;
            this.checkBoxFuelMiddleLine.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.checkBoxFuelMiddleLine, "checkBoxFuelMiddleLine");
            this.checkBoxFuelMiddleLine.Name = "checkBoxFuelMiddleLine";
            this.checkBoxFuelMiddleLine.CheckedChanged += new System.EventHandler(this.checkBoxFuelMiddleLine_CheckedChanged);
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // textBoxMaxValue
            // 
            resources.ApplyResources(this.textBoxMaxValue, "textBoxMaxValue");
            this.textBoxMaxValue.Name = "textBoxMaxValue";
            // 
            // graphControl
            // 
            this.graphControl.BeginTime = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
            resources.ApplyResources(this.graphControl, "graphControl");
            this.graphControl.IsAutoScrollRange = false;
            this.graphControl.IsEnableHPan = true;
            this.graphControl.IsEnableHZoom = true;
            this.graphControl.IsEnableVPan = true;
            this.graphControl.IsEnableVZoom = true;
            this.graphControl.IsPrintFillPage = true;
            this.graphControl.IsPrintKeepAspectRatio = true;
            this.graphControl.IsScrollY2 = false;
            this.graphControl.IsShowContextMenu = true;
            this.graphControl.IsShowCopyMessage = true;
            this.graphControl.IsShowCursorValues = false;
            this.graphControl.IsShowHScrollBar = false;
            this.graphControl.IsShowPointValues = true;
            this.graphControl.IsShowVScrollBar = false;
            this.graphControl.IsZoomOnMouseCenter = false;
            this.graphControl.Name = "graphControl";
            this.graphControl.PanButtons = System.Windows.Forms.MouseButtons.Left;
            this.graphControl.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.graphControl.PanModifierKeys2 = System.Windows.Forms.Keys.None;
            this.graphControl.PointDateFormat = "g";
            this.graphControl.PointValueFormat = "G";
            this.graphControl.ScrollMaxX = 0;
            this.graphControl.ScrollMaxY = 0;
            this.graphControl.ScrollMaxY2 = 0;
            this.graphControl.ScrollMinX = 0;
            this.graphControl.ScrollMinY = 0;
            this.graphControl.ScrollMinY2 = 0;
            this.graphControl.ShowMidleLine = true;
            this.graphControl.ShowPoints = true;
            this.graphControl.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
            this.graphControl.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
            this.graphControl.ZoomModifierKeys = System.Windows.Forms.Keys.None;
            this.graphControl.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
            this.graphControl.ZoomStepFraction = 0.1;
            this.graphControl.DoubleClick += new System.EventHandler(this.graphControl_DoubleClick);
            // 
            // FuelControl
            // 
            this.Controls.Add(this.graphControl);
            this.Controls.Add(this.panel2);
            this.Name = "FuelControl";
            resources.ApplyResources(this, "$this");
            this.Load += new System.EventHandler(this.FuelControl_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion // Component Designer generated code


		public FuelGraphControl GraphControl
		{
			get { return graphControl; }
		}

		#region private void FuelControl_Load(object sender, EventArgs e)

		private void FuelControl_Load(object sender, EventArgs e)
		{
            graphControl.ShowPoints = false; 
            checkBoxFuelShowPoints.Checked = graphControl.ShowPoints;
			checkBoxFuelMiddleLine.Checked = graphControl.ShowMidleLine;

			TagComboBoxItem tgi = new TagComboBoxItem("1 ���", 60);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("5 ���", 300);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("10 ���", 600);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("20 ���", 1200);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("30 ���", 1800);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("1 ���", 3600);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("3 ����", 10800);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("6 �����", 21600);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("12 �����", 43200);
			comboBoxMiddleTime.Items.Add(tgi);
			tgi = new TagComboBoxItem("�����", 86400);
			comboBoxMiddleTime.Items.Add(tgi);

			comboBoxMiddleTime.SelectedIndex = 1;

			tgi = new TagComboBoxItem("�� ���������", Double.MaxValue);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("5%", 5.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("10%", 10.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("20%", 20.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("30%", 30.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("40%", 40.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("50%", 50.0);
			comboBoxPercents.Items.Add(tgi);
			tgi = new TagComboBoxItem("60%", 60.0);
			comboBoxPercents.Items.Add(tgi);

            comboBoxPercents.SelectedIndex = 7; //4; 

			this.comboBoxMiddleTime.SelectedIndexChanged += new System.EventHandler(this.comboBoxMiddleTime_SelectedIndexChanged);
			this.comboBoxPercents.SelectedIndexChanged += new System.EventHandler(this.comboBoxPercents_SelectedIndexChanged);
		}

		#endregion // private void FuelControl_Load(object sender, EventArgs e)

		// �������� ������� ��� ����������� ������� ����� ������� (10 ���)
		int delta = 600;

		private void comboBoxMiddleTime_SelectedIndexChanged(object sender, EventArgs e)
		{
			delta = (int)((TagComboBoxItem)comboBoxMiddleTime.SelectedItem).Tag;

			graphControl.AddMiddleLine(delta);
		}

		public void Update()
		{
			if (this.graphControl.PointList.Count <= 0)
				return;

		    graphControl.SetFuelTank(); 

			double minValue, maxValue;

			graphControl.FillMainLine(out minValue, out maxValue);

			Invoke(
				new MinMaxEventHandler(
					delegate(double min, double max)
					{
						this.textBoxMinValue.Text = min.ToString();
						this.textBoxMaxValue.Text = max.ToString();
					}
				),
				minValue,
				maxValue
				);

          
            // ������� �����
			AddMiddleLine();
        
            // ����� / ���� 
            graphControl.FillDischargeLines();
        }

		private void AddMiddleLine()
		{
			delta = (int)((TagComboBoxItem)comboBoxMiddleTime.SelectedItem).Tag;

            if (checkBoxFuelMiddleLineSpeed0.Checked)
			{
				comboBoxMiddleTime.Enabled = false;
				//graphControl.AddMiddleLine(delta, MiddleLineTypeEnum.Speed0);
                graphControl.AddMiddleLine(delta, MiddleLineTypeEnum.Ignition);
			}
            //else
            //{
            //    comboBoxMiddleTime.Enabled = true;
            //    graphControl.AddMiddleLine(delta);
            //}
		}

		void graphControl_DoubleClick(object sender, EventArgs e)
		{
			if (graphControl.SelectedPointPairID != -1 && PointPairSelected != null)
				PointPairSelected(graphControl, graphControl.SelectedPointPairID);
		}

		private void checkBoxFuelMiddleLineSpeed0_CheckedChanged(object sender, EventArgs e)
		{
            this.checkBoxFuelMiddleLine.Checked = checkBoxFuelMiddleLineSpeed0.Checked;
            graphControl.ShowMidleLine = checkBoxFuelMiddleLine.Checked;

            AddMiddleLine(); 
		}

		private void checkBoxFuelShowPoints_CheckedChanged(object sender, EventArgs e)
		{
			graphControl.ShowPoints = checkBoxFuelShowPoints.Checked;
		}

		private void checkBoxFuelMiddleLine_CheckedChanged(object sender, EventArgs e)
		{
			graphControl.ShowMidleLine = checkBoxFuelMiddleLine.Checked;
		}

		private void comboBoxPercents_SelectedIndexChanged(object sender, EventArgs e)
		{
            graphControl.DischagePercent = (double)((TagComboBoxItem)comboBoxPercents.SelectedItem).Tag;
            // ��������� ������
			graphControl.FillDischargeLines();
		}

		public void ClearGraphics()
		{
			graphControl.ClearGraphics();

            graphControl.ShowPoints = false;
			checkBoxFuelShowPoints.Checked = graphControl.ShowPoints;
			checkBoxFuelMiddleLine.Checked = graphControl.ShowMidleLine;
		} 

        //public HistoryInformation HistoryInformation
        //{
        //    set { graphControl.HistoryInformation = value; }
        //}

	}
}
