using System;
using System.Drawing;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.MTMap.Spatial;
using FORIS.TSS.WorkplaceShadow.Controls;
using MTMap.MathTypes;

namespace FORIS.TSS.RuleManager.Rule.Controls.History
{
	public class HistoryObject:
		DataExtraObject<HistoryRow, HistoryTable, IHistoryData>,
		IImageConsumer<HistoryObject, HistoryLayer>,
		ILabeledMarkLayer
	{
		private readonly LabeledMarkObject labeledMarkObject;
		private readonly string mobileName;

		#region Constructor & Dispose

		public HistoryObject( int historyId, string mobileName )
			: base( historyId )
		{
			this.labeledMarkObject = new LabeledMarkObject();
			this.labeledMarkObject.Size = new Size( 32, 38 );

			this.mobileName = mobileName;
		}

		#endregion  // Constructor & Dispose

		#region Properties

		protected override DExtents Extent
		{
			get { return ( (IExtraObject)this.labeledMarkObject ).Extent; }
		}

		public GeoPoint Position
		{
			get { return this.labeledMarkObject.Position; }
		}

		private int course;
		public int Course
		{
			get { return this.course; }
			set
			{
				if ( this.course != value )
				{
					this.course = value;
					this.labeledMarkObject.Refresh();
				}
			}
		}

		public override bool Selectable
		{
			get { return this.labeledMarkObject.Selectable; }
			set { this.labeledMarkObject.Selectable = value; }
		}

		public override bool Selected
		{
			get { return this.labeledMarkObject.Selected; }
			set
			{
				if ( this.Selected != value )
				{
					this.labeledMarkObject.Selected = value;

					this.OnSelectionChanged();
				}
			}
		}

		#endregion  // Properties
		
		#region IImageConsumer<HistoryObject, HistoryLayer> Members

		private HistoryLayer imageSupplier;
		public HistoryLayer ImageSupplier
		{
			get { return this.imageSupplier; }
			set
			{
				if ( this.imageSupplier != value )
				{
					this.imageSupplier = value;
					this.labeledMarkObject.ImageSupplier = this;
				}
			}
		}

		public Bitmap Image
		{
			get { return this.labeledMarkObject.Image; }
			set
			{
				if ( this.Image != value )
				{
					this.labeledMarkObject.Image = value;
					this.OnChanged();
				}
			}
		}

		#endregion  // IImageConsumer<HistoryObject, HistoryLayer> Members

		#region ILabeledMarkLayer Members

		public Bitmap GetImage( LabeledMarkObject obj )
		{
			return
				this.imageSupplier != null
					? this.imageSupplier.GetImage( this )
					: null;
		}

		#endregion  // ILabeledMarkLayer Members

		#region View

		protected override void OnDestroyView()
		{
			this.labeledMarkObject.Position = GeoPoint.Empty;
			this.course = 0;

			base.OnDestroyView();
		}

		protected override void OnUpdateView()
		{
			base.OnUpdateView();

			if( this.Row.Valid )
			{
				this.labeledMarkObject.Position =
					GeoPoint.CreateGeo(
						this.Row.Longitude,
						this.Row.Latitude
						);

				this.course = this.Row.Course;
				this.OnChanged();
			}
		}

		#endregion // View

        protected override void Draw(IMapWindow window, System.Drawing.Graphics graphics)
		{
			( (IExtraObject)this.labeledMarkObject ).Draw( window, graphics );
		}

		protected override bool Contains( IMapWindow window, GDI.POINT point )
		{
			return ( (IExtraObject)this.labeledMarkObject ).Contains( window, point );
		}

		protected override string GetToolTip( IMapWindow window, GDI.POINT point )
		{
			return
				this.Contains( window, point )
					? String.Format(
					  	"{0}\r\n{1}\r\n�������� {2:F0}��/�",
						this.mobileName,
					  	this.Row.LogTime.ToLocalTime().ToString( "HH:mm:ss dd.MM.yy" ),
					  	this.Row.Speed
					  	)
					: String.Empty;
		}


		protected override HistoryRow GetDataRow(int historyId)
		{
			return this.Data.History.FindRow( historyId );
		}

	}
}