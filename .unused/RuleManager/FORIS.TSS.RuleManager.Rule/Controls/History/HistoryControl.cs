using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Base;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.Common.Controls;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.MTMap.Controls;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.RuleManager;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile;
using FORIS.TSS.WorkplaceSharnier;
using MTMap.MathTypes;
using FORIS.TSS.Helpers.RuleManager.Data.History;
using FORIS.TSS.WorkplaceShadow.RuleManager.Data.History;
using FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History;
using FORIS.TSS.BusinessLogic.TspTerminal;


namespace FORIS.TSS.RuleManager.Rule.Controls.History
{
	public class HistoryControl:
		UserControl,
		IClientItem<IRuleClientDataProvider>,
		IDataItem<IRuleData>
	{
		#region Controls & Components

		private IContainer components;
		private TableLayoutPanel tableLayoutPanel1;
		private Label lblBeginTime;
		private Label lblMobile;
		private Label lblEndTime;
		private Label lblInterval;
		private Label lblMaxCount;
		private NumericUpDown nudMaxCount;
		private NumericUpDown nudInterval;
		private MobileComboBox cbxMobile;
		private MobileWithControllerFilter mobileWithControllerFilter;
		private DateTimePicker dtpBeginTime;
		private TableLayoutPanel tlpMain;
		private TextColumn columnTime;
		private TextColumn columnSpeed;
		private TextColumn columnLongitude;
		private TextColumn columnLatitude;
		private Button getButton;
		private RuleClientDataProviderDispatcher clientDispatcher;
		private TextColumn columnAddress;
		private DateTimePicker dtpEndTime;
		private RuleDataAmbassador daMobileWithControllerFilter;
		private RuleDataAmbassador daMobileComboBox;
		private RuleDataDispatcher dataDispatcher;
		private HistoryLayer historyLayer;
		private HistoryTableControl ctrlHistoryTable;
		private HistoryData historyData;
		private HistoryDataAmbassador daHistoryLayer;
        private HistoryDataAmbassador daHistoryTableControl;
        private TableLayoutPanel tableLayoutPanel3;
        private RadioButton rbDB;
        private RadioButton rbGPRS;
        private RadioButton rbGSM;
		private MegaPanel megaPanel;
		private TableLayoutPanel tlpButtons;
		private Button clearButton;
        private HistoryDataDispatcher historyDataDispatcher;
        private FORIS.TSS.RuleManager.Rule.Controls.History.Graphics.HistoryGraphicsControl historyGraphicsControl;
		private RuleDataAmbassador daHistoryLayerRule;
        private Splitter splitter1;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public HistoryControl()
		{
			InitializeComponent();

			this.Padding = new Padding(0);
			this.getButton.Enabled = false;

			DateTime currentTime = DateTime.Now;
			currentTime = 
				new DateTime(
					currentTime.Year,
					currentTime.Month,
					currentTime.Day,
					currentTime.Hour,
					currentTime.Minute,
					0,
					0
					);

			this.dtpBeginTime.Value = currentTime.AddMinutes( -30 );
			this.dtpEndTime.Value = currentTime;

			this.ctrlHistoryTable.SelectionChanged += this.ctrlHistoryTable_SelectionChanged;
			this.historyLayer.SelectionChanged += this.historyLayer_SelectionChanged;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.historyLayer.SelectionChanged -= this.historyLayer_SelectionChanged;
				this.ctrlHistoryTable.SelectionChanged -= this.ctrlHistoryTable_SelectionChanged;

				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistoryControl));
            this.dataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataDispatcher(this.components);
            this.daMobileWithControllerFilter = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.mobileWithControllerFilter = new FORIS.TSS.RuleManager.Rule.Controls.History.MobileWithControllerFilter();
            this.daMobileComboBox = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.cbxMobile = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.Mobile.MobileComboBox();
            this.daHistoryLayerRule = new FORIS.TSS.Helpers.RuleManager.Data.Rule.RuleDataAmbassador();
            this.historyLayer = new FORIS.TSS.RuleManager.Rule.Controls.History.HistoryLayer(this.components);
            this.historyData = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryData(this.components);
            this.historyDataDispatcher = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryDataDispatcher(this.components);
            this.daHistoryLayer = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryDataAmbassador();
            this.daHistoryTableControl = new FORIS.TSS.Helpers.RuleManager.Data.History.HistoryDataAmbassador();
            this.ctrlHistoryTable = new FORIS.TSS.WorkplaceShadow.RuleManager.Controls.History.HistoryTableControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblBeginTime = new System.Windows.Forms.Label();
            this.lblMobile = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.lblMaxCount = new System.Windows.Forms.Label();
            this.nudMaxCount = new System.Windows.Forms.NumericUpDown();
            this.nudInterval = new System.Windows.Forms.NumericUpDown();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbDB = new System.Windows.Forms.RadioButton();
            this.rbGPRS = new System.Windows.Forms.RadioButton();
            this.rbGSM = new System.Windows.Forms.RadioButton();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.getButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.columnTime = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnSpeed = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnLongitude = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnLatitude = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnAddress = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.megaPanel = new FORIS.TSS.Common.Controls.MegaPanel();
            this.historyGraphicsControl = new FORIS.TSS.RuleManager.Rule.Controls.History.Graphics.HistoryGraphicsControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.clientDispatcher = new FORIS.TSS.RuleManager.Rule.RuleClientDataProviderDispatcher(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInterval)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.megaPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataDispatcher
            // 
            this.dataDispatcher.Ambassadors.Add(this.daMobileWithControllerFilter);
            this.dataDispatcher.Ambassadors.Add(this.daMobileComboBox);
            this.dataDispatcher.Ambassadors.Add(this.daHistoryLayerRule);
            // 
            // daMobileWithControllerFilter
            // 
            this.daMobileWithControllerFilter.Item = this.mobileWithControllerFilter;
            // 
            // daMobileComboBox
            // 
            this.daMobileComboBox.Item = this.cbxMobile;
            // 
            // cbxMobile
            // 
            resources.ApplyResources(this.cbxMobile, "cbxMobile");
            this.cbxMobile.Filter = this.mobileWithControllerFilter;
            this.cbxMobile.Name = "cbxMobile";
            this.cbxMobile.SelectedIndexChanged += new System.EventHandler(this.cbxMobile_SelectedIndexChanged);
            // 
            // daHistoryLayerRule
            // 
            this.daHistoryLayerRule.Item = this.historyLayer;
            // 
            // historyLayer
            // 
            resources.ApplyResources(this.historyLayer, "historyLayer");
            // 
            // historyData
            // 
            this.historyData.MobileName = null;
            this.historyData.Loaded += new System.EventHandler(this.historyData_Loaded);
            this.historyData.Loading += new System.EventHandler(this.historyData_Loading);
            // 
            // historyDataDispatcher
            // 
            this.historyDataDispatcher.Ambassadors.Add(this.daHistoryLayer);
            this.historyDataDispatcher.Ambassadors.Add(this.daHistoryTableControl);
            // 
            // daHistoryLayer
            // 
            this.daHistoryLayer.Item = this.historyLayer;
            // 
            // daHistoryTableControl
            // 
            this.daHistoryTableControl.Item = this.ctrlHistoryTable;
            // 
            // ctrlHistoryTable
            // 
            resources.ApplyResources(this.ctrlHistoryTable, "ctrlHistoryTable");
            this.ctrlHistoryTable.Name = "ctrlHistoryTable";
            this.ctrlHistoryTable.ToolbarVisible = false;
            this.ctrlHistoryTable.DoubleClick += new System.EventHandler(this.ctrlHistoryTable_DoubleClick);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.lblBeginTime, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblMobile, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblEndTime, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblInterval, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblMaxCount, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.nudMaxCount, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.nudInterval, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbxMobile, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dtpBeginTime, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dtpEndTime, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tlpButtons, 1, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // lblBeginTime
            // 
            resources.ApplyResources(this.lblBeginTime, "lblBeginTime");
            this.lblBeginTime.Name = "lblBeginTime";
            // 
            // lblMobile
            // 
            resources.ApplyResources(this.lblMobile, "lblMobile");
            this.lblMobile.Name = "lblMobile";
            // 
            // lblEndTime
            // 
            resources.ApplyResources(this.lblEndTime, "lblEndTime");
            this.lblEndTime.Name = "lblEndTime";
            // 
            // lblInterval
            // 
            resources.ApplyResources(this.lblInterval, "lblInterval");
            this.lblInterval.Name = "lblInterval";
            // 
            // lblMaxCount
            // 
            resources.ApplyResources(this.lblMaxCount, "lblMaxCount");
            this.lblMaxCount.Name = "lblMaxCount";
            // 
            // nudMaxCount
            // 
            this.nudMaxCount.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            resources.ApplyResources(this.nudMaxCount, "nudMaxCount");
            this.nudMaxCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudMaxCount.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudMaxCount.Name = "nudMaxCount";
            this.nudMaxCount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // nudInterval
            // 
            this.nudInterval.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            resources.ApplyResources(this.nudInterval, "nudInterval");
            this.nudInterval.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.nudInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudInterval.Name = "nudInterval";
            this.nudInterval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // dtpBeginTime
            // 
            resources.ApplyResources(this.dtpBeginTime, "dtpBeginTime");
            this.dtpBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.ShowUpDown = true;
            // 
            // dtpEndTime
            // 
            resources.ApplyResources(this.dtpEndTime, "dtpEndTime");
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.rbDB, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbGPRS, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbGSM, 2, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // rbDB
            // 
            resources.ApplyResources(this.rbDB, "rbDB");
            this.rbDB.Checked = true;
            this.rbDB.Name = "rbDB";
            this.rbDB.TabStop = true;
            this.rbDB.UseVisualStyleBackColor = true;
            // 
            // rbGPRS
            // 
            resources.ApplyResources(this.rbGPRS, "rbGPRS");
            this.rbGPRS.Name = "rbGPRS";
            this.rbGPRS.UseVisualStyleBackColor = true;
            // 
            // rbGSM
            // 
            resources.ApplyResources(this.rbGSM, "rbGSM");
            this.rbGSM.Name = "rbGSM";
            this.rbGSM.UseVisualStyleBackColor = true;
            // 
            // tlpButtons
            // 
            resources.ApplyResources(this.tlpButtons, "tlpButtons");
            this.tlpButtons.Controls.Add(this.getButton, 0, 0);
            this.tlpButtons.Controls.Add(this.clearButton, 1, 0);
            this.tlpButtons.Name = "tlpButtons";
            // 
            // getButton
            // 
            resources.ApplyResources(this.getButton, "getButton");
            this.getButton.Name = "getButton";
            this.getButton.UseVisualStyleBackColor = true;
            this.getButton.Click += new System.EventHandler(this.getButton_Click);
            // 
            // clearButton
            // 
            resources.ApplyResources(this.clearButton, "clearButton");
            this.clearButton.Name = "clearButton";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // tlpMain
            // 
            resources.ApplyResources(this.tlpMain, "tlpMain");
            this.tlpMain.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tlpMain.Controls.Add(this.ctrlHistoryTable, 0, 1);
            this.tlpMain.Name = "tlpMain";
            // 
            // columnTime
            // 
            this.columnTime.Editable = false;
            this.columnTime.Editor = null;
            this.columnTime.Sortable = false;
            resources.ApplyResources(this.columnTime, "columnTime");
            this.columnTime.Width = 85;
            // 
            // columnSpeed
            // 
            this.columnSpeed.Editable = false;
            this.columnSpeed.Editor = null;
            this.columnSpeed.Sortable = false;
            resources.ApplyResources(this.columnSpeed, "columnSpeed");
            this.columnSpeed.Width = 45;
            // 
            // columnLongitude
            // 
            this.columnLongitude.Editable = false;
            this.columnLongitude.Editor = null;
            this.columnLongitude.Sortable = false;
            resources.ApplyResources(this.columnLongitude, "columnLongitude");
            // 
            // columnLatitude
            // 
            this.columnLatitude.Editable = false;
            this.columnLatitude.Editor = null;
            this.columnLatitude.Sortable = false;
            resources.ApplyResources(this.columnLatitude, "columnLatitude");
            // 
            // columnAddress
            // 
            this.columnAddress.Editable = false;
            this.columnAddress.Editor = null;
            this.columnAddress.Sortable = false;
            resources.ApplyResources(this.columnAddress, "columnAddress");
            // 
            // megaPanel
            // 
            this.megaPanel.Collapsed = true;
            this.megaPanel.Controls.Add(this.historyGraphicsControl);
            resources.ApplyResources(this.megaPanel, "megaPanel");
            this.megaPanel.Name = "megaPanel";
            this.megaPanel.WidthHidden = 246;
            // 
            // historyGraphicsControl
            // 
            this.historyGraphicsControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            resources.ApplyResources(this.historyGraphicsControl, "historyGraphicsControl");
            this.historyGraphicsControl.Name = "historyGraphicsControl";
            // 
            // splitter1
            // 
            resources.ApplyResources(this.splitter1, "splitter1");
            this.splitter1.Name = "splitter1";
            this.splitter1.TabStop = false;
            // 
            // HistoryControl
            // 
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tlpMain);
            this.Controls.Add(this.megaPanel);
            this.Name = "HistoryControl";
            resources.ApplyResources(this, "$this");
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInterval)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tlpButtons.ResumeLayout(false);
            this.tlpButtons.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.megaPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion // Windows Form Designer generated code

		#region IClientItem<IHistoryClientProvider> Members 

		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility((DesignerSerializationVisibility.Hidden))]
		public IRuleClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set { this.clientDispatcher.Client = value; }
		}

		#endregion // IClientItem<IHistoryClientProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IRuleClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		#region IDataItem<IRuleData> Members

		[Browsable(false)]
		[DefaultValue(0)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IRuleData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRuleData> Members

		#region Properties

		private IMapControl mapControl;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IMapControl MapControl
		{
			get { return this.mapControl; }
			set
			{
				if( this.mapControl != null )
				{
					this.mapControl.ExtraLayers.Remove( this.historyLayer );
				}

				this.mapControl = value;

				if( this.mapControl != null )
				{
					this.mapControl.ExtraLayers.Add( this.historyLayer );
				}
			}
		}

		[Browsable(false)]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int MobileId
		{
			get { return this.cbxMobile.SelectedId; }
			set { this.cbxMobile.SelectedId = value; }
		}

		/// <summary>
		/// ������ �������. UTC
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DateTime BeginTime
		{
			get { return this.dtpBeginTime.Value.ToUniversalTime(); }
			set { this.dtpBeginTime.Value = value.ToLocalTime(); }
		}

		/// <summary>
		/// ��������� �������. UTC
		/// </summary>
		[Browsable(false) ]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DateTime EndTime
		{
			get { return this.dtpEndTime.Value.ToUniversalTime(); }
			set { this.dtpEndTime.Value = value.ToLocalTime(); }
		}

		#endregion // Properties

		#region Sync historyLayer and table selection

		private void historyLayer_SelectionChanged( object sender, EventArgs e )
		{
			#region Preconditions

			if( this.historyLayer.Objects.Count != this.historyData.History.Rows.Count )
			{
				throw new ApplicationException( "��������� ����� ������ ����� �������� ������������ ������� � ����� ���� ����� ������������ �������" );
			}

			#endregion // Preconditions

			this.ctrlHistoryTable.SelectionChanged -= this.ctrlHistoryTable_SelectionChanged;

			foreach (HistoryRow historyRow in this.historyData.History.Rows)
			{
				HistoryXpRow row = this.ctrlHistoryTable.Rows[historyRow.ID];
				HistoryObject historyObject = this.historyLayer.Objects[historyRow.ID]; 

				row.Selected = historyObject.Selected;
			}

			this.ctrlHistoryTable.SelectionChanged += this.ctrlHistoryTable_SelectionChanged;
		}
		private void ctrlHistoryTable_SelectionChanged( object sender, SelectionEventArgs e )
		{
			#region Preconditions

			if( this.historyLayer.Objects.Count != this.historyData.History.Rows.Count )
			{
				throw new ApplicationException( "��������� ����� ������ ����� �������� ������������ ������� � ����� ���� ����� ������������ �������" );
			}

			#endregion // Preconditions

			this.historyLayer.SelectionChanged -= this.historyLayer_SelectionChanged;
			this.historyLayer.UpdateBegin();

			foreach( HistoryRow historyRow in this.historyData.History.Rows )
			{
				HistoryXpRow row = this.ctrlHistoryTable.Rows[historyRow.ID];
				HistoryObject historyObject = this.historyLayer.Objects[historyRow.ID];

				historyObject.Selected = row.Selected;
			}

			this.historyLayer.UpdateEnd();
			this.historyLayer.SelectionChanged += this.historyLayer_SelectionChanged;
		}

		#endregion // Sync historyLayer and table selection

		#region Handle historyData's events

		private void historyData_Loading( object sender, EventArgs e )
		{
			// ��������
			this.historyLayer.ClearSelections();

			this.historyDataDispatcher.Data = null;
		}

		private void historyData_Loaded( object sender, EventArgs e )
		{
			this.historyDataDispatcher.Data = this.historyData;
            this.historyGraphicsControl.ruleData = this.Data;
            this.historyGraphicsControl.GraphicsControlUpdate(mobileRow, this.historyData.History);
		}

		#endregion  // Handle historyData's events
		
		#region Handle controls' events

		private void cbxMobile_SelectedIndexChanged( object sender, EventArgs e )
		{
			this.getButton.Enabled = this.cbxMobile.SelectedId != 0;
		}

        MobileRow mobileRow = null;

		private void getButton_Click( object sender, EventArgs e )
		{
			try
			{
                getButton.Enabled = false;

				mobileRow = this.Data.Mobile.FindRow( this.cbxMobile.SelectedId );

			    ILogic iLogic = this.Client.Session as ILogic;
                if(iLogic == null)
                    return;
			    this.historyData.DataSupplier = null;

                iLogic.LogReceived += tspLogic_LogReceived;

			    GetLogMethod method;
                if(this.rbDB.Checked)
                    method = GetLogMethod.DB;
                else if(this.rbGPRS.Checked)
                    method = GetLogMethod.GPRS;
                else
                    method = GetLogMethod.GSM;
                
                iLogic.SendGetLogCommand(method, this.dtpBeginTime.Value.ToUniversalTime(),
                        this.dtpEndTime.Value.ToUniversalTime(),
                        (int)mobileRow.Controller,
                        (int)this.nudInterval.Value,
                        (int)this.nudMaxCount.Value);
			}
			catch (Exception ex)
			{
				this.historyData.DataSupplier = null;

				MessageBox.Show(
					"�� ������� �������� �������" + "\r\n" +
					"�������: " + ex.Message
					);
			}
		}

		private void clearButton_Click( object sender, EventArgs e )
		{
			this.historyData.DataSupplier = null;
            this.historyGraphicsControl.GraphicsControlUpdate(null, null);
		}

	    private delegate void LogReceived(HistoryDataSet data);

        void tspLogic_LogReceived(LogReceivedEventArgs args)
        {
            ILogic iLogic = this.Client.Session as ILogic;
            if (iLogic == null)
                return;

            iLogic.LogReceived -= tspLogic_LogReceived;
            //MobileRow mobileRow = this.Data.Mobile.FindRow( this.cbxMobile.SelectedId );

            HistoryDataSet data = iLogic.GetLog((int) mobileRow.Controller);

            LogReceived del = OnLogReceived;

            this.Invoke(del, data);
        }

	    private void OnLogReceived(HistoryDataSet data)
	    {
            try
            {
                if (data == null)
                    throw new Exception("������ ��������� ������� �� �����������.");
            	this.historyData.MobileName = this.mobileRow.Name;
            	this.historyData.DataSupplier = 
					new HistoryDataSupplier(data);


                if (this.historyData.History.Rows.Count > 0)
                {
                    if (this.mapControl != null)
                    {
                        DExtents extent = DExtents.Empty;

                        foreach (HistoryRow historyRow in this.historyData.History.Rows)
                        {
                            if (historyRow.Valid)
                            {
                                extent.includePoint(
                                    GeoPoint.CreateGeo(historyRow.Longitude, historyRow.Latitude).Planar
                                    );
                            }
                        }

                        if (!extent.IsEmpty)
                        {
                            this.mapControl.Zoom(extent, MapZoomMode.Zoom);
                        }
                        else
                        {
                            MessageBox.Show("������ ����������");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("��� ������� �� ��������� ������");
                }
            }
            catch (Exception ex)
            {
                this.historyData.DataSupplier = null;

                MessageBox.Show(
                    "�� ������� �������� �������" + "\r\n" +
                    "�������: " + ex.Message
                    );
            }
            finally
            {
                this.getButton.Enabled = true;
            }
	    }

	    private void ctrlHistoryTable_DoubleClick( object sender, EventArgs e )
		{
			if( this.mapControl != null && this.ctrlHistoryTable.SelectedRows.Length > 0 )
			{
				DExtents extent = DExtents.Empty;

				foreach( HistoryXpRow xpRow in this.ctrlHistoryTable.SelectedRows )
				{
					if (xpRow.Row.Valid )
					{
						GeoPoint geoPoint =
							GeoPoint.CreateGeo( xpRow.Row.Longitude, xpRow.Row.Latitude );

						extent.includePoint( geoPoint.Planar );
					}
				}

				if ( !extent.IsEmpty )
				{
					this.mapControl.Zoom( extent, MapZoomMode.Zoom );
				}
			}
		}

		#endregion // Handle controls' events

    }
}
