using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceShadow.RuleManager;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.RuleManager.Rule
{
	class RuleClientDataProviderDispatcher:
		ClientDispatcher<IRuleClientDataProvider>
	{
		public RuleClientDataProviderDispatcher( IContainer container) :this()
		{
			container.Add( this );
		}
		public RuleClientDataProviderDispatcher()
		{
			
		}

		private readonly RuleClientDataProviderAmbassadorCollection ambassadors =
			new RuleClientDataProviderAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new RuleClientDataProviderAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem<IRuleClientDataProvider>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	class RuleClientDataProviderAmbassadorCollection:
		ClientAmbassadorCollection<IRuleClientDataProvider>
	{
		public new RuleClientDataProviderAmbassador this [ int index ]
		{
			get { return (RuleClientDataProviderAmbassador)base[index]; }			
		}
	}

	class RuleClientDataProviderAmbassador:
		ClientAmbassador<IRuleClientDataProvider>
	{
		
	}
}