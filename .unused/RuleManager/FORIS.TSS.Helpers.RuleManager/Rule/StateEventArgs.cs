using System;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.Helpers.RuleManager.Rule
{
	public delegate void MobileStateEventHandler<TMobileState>(
		MobileStateEventArgs<TMobileState> e
		)
		where TMobileState: MobileRuleState;

	public class MobileStateEventArgs<TMobileState>:
		EventArgs
		where TMobileState: MobileRuleState
	{
		public readonly TMobileState State;

		public MobileStateEventArgs( TMobileState state )
		{
			this.State = state;
		}
	}
}
