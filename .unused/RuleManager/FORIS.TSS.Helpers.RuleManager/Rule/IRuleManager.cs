
using TerminalService.Interfaces;

namespace FORIS.TSS.Helpers.RuleManager.Rule
{
	public interface IRuleManager
	{
		IPositionMessageSupplier PositionMessageSupplier { get; }
	}
}