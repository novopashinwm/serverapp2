using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.Helpers.RuleManager.Rule
{
	/// <summary>
	/// ������������ ��� ��������� ������� 
	/// ������������� �� ����� ������� ������
	/// �� ����������� ������� ��������
	/// </summary>
	/// <remarks>
	/// �� ��� ������� ��� ��������� �������
	/// ������ ��������� � ������ ���������
	/// </remarks>
	public interface IPossitionMessageProcessor
	{
		void Process( IPositionMessage mobileUnit );
	}

	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// ��� ��������� ��������� ������ � �������������
	/// �������� ��������� � ������ ��������� �������
	/// ��� ������������� ���� ������� ��������� ���������.
	/// ���� ���, �������� ������ ��������� �������
	/// ������ ��������� � ������ ���������,
	/// ������� ����� Process()
	/// ������� ������� �������� ���� ������� ��������� 
	/// ���������
	/// </remarks>
	public interface ICockableRule
	{
		/// <summary>
		/// ���� ������� ��������� ���������
		/// </summary>

		event MobileRuleEventHandler Cocked;
	}

	public delegate void MobileRuleEventHandler( int mobileRuleId, MobileRuleState state );
}
