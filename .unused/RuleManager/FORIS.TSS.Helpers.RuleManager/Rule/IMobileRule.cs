using System;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.Helpers.RuleManager.Rule
{
	/// <summary>
	/// ������� ��������� ��� ���� �������� ������
	/// </summary>
	public interface IMobileRule
	{
		MobileRuleState State { get; }

		MobileRuleSettings Settings { get; }

		event MobileRuleStateEventHandler StateChanged;

		void ApplyChange(IRuleData ruleData, MobileRuleState state );
	}

	public interface IMobileRule<TData>:
		IMobileRule
		where TData: IData
	{
		void ApplyChange( TData data, MobileRuleState state, RuleStateChangeApplyOrder order );
	}

	public enum RuleStateChangeApplyOrder
	{
		None,
		BeforeRule,
		AfterRule
	}

	public class MobileRuleStateEventArgs : EventArgs
	{
		private int idMobileRule;
		private IMobileRule mobileRuleObject;
		private MobileRuleState mobileRuleState;

		public MobileRuleStateEventArgs( int idMobileRule, IMobileRule mobileRuleObject, MobileRuleState mobileRuleState )
		{
			this.idMobileRule = idMobileRule;
			this.mobileRuleObject = mobileRuleObject;
			this.mobileRuleState = mobileRuleState;
		}

		public int IdMobileRule { get { return this.idMobileRule; } }
		public IMobileRule MobileRuleObject { get { return this.mobileRuleObject; } }
		public MobileRuleState MobileRuleState { get { return this.mobileRuleState; } }
	}

	public delegate void MobileRuleStateEventHandler( MobileRuleStateEventArgs e );

}