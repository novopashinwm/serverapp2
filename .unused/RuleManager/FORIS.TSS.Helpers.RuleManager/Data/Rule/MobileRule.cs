using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class MobileRuleTable:
		Table<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		public override string Name
		{
			get { return "MOBILE_RULE"; }
		}

		public MobileRuleTable( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public MobileRuleTable()
		{

		}

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int mobileIndex;
		internal int MobileIndex { get { return this.mobileIndex; } }

		private int ruleIndex;
		internal int RuleIndex { get { return this.ruleIndex; } }

		private int settingsIndex;
		internal int SettingsIndex { get { return this.settingsIndex; } }

		#endregion // Fields' indexes

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex		= -1;
			this.mobileIndex	= -1;
			this.ruleIndex		= -1;
			this.settingsIndex	= -1;
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.DataTable != null )
			{
				this.idIndex		= this.DataTable.Columns.IndexOf( "MOBILE_RULE_ID" );
				this.mobileIndex	= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
				this.ruleIndex		= this.DataTable.Columns.IndexOf( "RULE_ID" );
				this.settingsIndex	= this.DataTable.Columns.IndexOf( "SETTINGS" );
			}
		}

		protected override MobileRuleRow OnCreateRow( DataRow dataRow )
		{
			return new MobileRuleRow( dataRow );
		}

		protected override void OnRowActionHide( MobileRuleRow row )
		{
			try
			{
				#region MobileRow.RowsMobileRule

				MobileRow rowMobile = this.Data.Mobile.FindRow( row.Mobile );

				if( rowMobile != null )
				{
					rowMobile.RowsMobileRule.Remove( row );
				}
				else 
					throw new ApplicationException("There is no Mobile row for MobileRule row");

				#endregion // MobileRule.RowsMobileRule
			}
			finally
			{
				base.OnRowActionHide( row );
			}
		}
		protected override void OnRowActionShow( MobileRuleRow row )
		{
			base.OnRowActionShow( row );

			try
			{
				#region MobileRow.RowsMobileRule

				MobileRow rowMobile = this.Data.Mobile.FindRow( row.Mobile );

				if( rowMobile != null )
				{
					rowMobile.RowsMobileRule.Add( row );
				}
				else
					throw new ApplicationException( "There is no Mobile row for MobileRule row" );

				#endregion // MobileRule.RowsMobileRule
			}
			finally {}
		}
	}

	public class MobileRuleRow:
		Row<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		#region Fields

		public int Mobile
		{
            get { return (int)this.DataRow[this.Table.MobileIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.MobileIndex] = value; }
		}

		public int Rule
		{
            get { return (int)this.DataRow[this.Table.RuleIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RuleIndex] = value; }
		}

		private MobileRuleSettings settings;
		public MobileRuleSettings Settings
		{
			get { return this.settings; }
			set 
			{
				if( this.settings != value )
				{
					using( MemoryStream stream = new MemoryStream() )
					{
						// TODO: �� ������������ �����
						BinaryFormatter formatter = new BinaryFormatter();

						formatter.Serialize( stream, value );

						stream.Close();

						this.DataRow[this.Table.SettingsIndex] = stream.ToArray();
					}
				}
			}
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Parent row

		private MobileRow rowMobile;
		public MobileRow RowMobile
		{
			get { return this.rowMobile; }
		}

		private RuleRow rowRule;
		public RuleRow RowRule
		{
			get { return this.rowRule; }
		}

		#endregion // Parent row

		#region Child rows

		private StateRow rowState;
		
		[Browsable(false)]
		public StateRow RowState
		{
			get { return this.rowState; }
			set
			{
				if( this.rowState != value )
				{
					this.OnBeforeStateChanged();

					this.rowState = value;

					this.OnAfterStateChanged();
				}
			}
		}

		#endregion // Child rows

		#region View

		protected override void OnDestroyView()
		{
			this.rowRule = null;
			this.rowMobile = null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowMobile = this.Data.Mobile.FindRow( this.Mobile );
			this.rowRule = this.Data.Rule.FindRow( this.Rule );
		}

		#endregion // View

		#region Constructor & Dispose

		public MobileRuleRow( DataRow dataRow )
			: base( dataRow )
		{

		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.beforeStateChanged = null;
				this.afterStateChanged = null;
			}

			base.Dispose( disposing );
		}
		#endregion // Constructor & Dispose

		protected override void OnBuildFields()
		{
			if( !this.DataRow.IsNull( this.Table.SettingsIndex ) )
			{
				try
				{
					byte[] settingsData  = (byte[])this.DataRow[this.Table.SettingsIndex, DataRowVersion.Current];

					using( MemoryStream stream = new MemoryStream( settingsData ) )
					{
						// TODO: �� ������������ �����
						BinaryFormatter formatter = new BinaryFormatter();

						this.settings = (MobileRuleSettings)formatter.Deserialize( stream );
					}
				}
				catch( Exception ex )
				{
					this.settings = null;

					Trace.WriteLine(
						"������ ������������ ������� �������� �������: " + ex.Message
						);
				}
			}
			else
			{
				this.settings = null;
			}
		}

		#region Events

		private EventHandler beforeStateChanged;
		[Browsable(false)]
		public event EventHandler BeforeStateChanged
		{
			add { this.beforeStateChanged += value; }
			remove { this.beforeStateChanged -= value; }
		}

		protected virtual void OnBeforeStateChanged()
		{
			if ( this.beforeStateChanged != null )
			{
				this.beforeStateChanged( this, EventArgs.Empty );
			}
		}

		private EventHandler afterStateChanged;
		[Browsable( false )]
		public event EventHandler AfterStateChanged
		{
			add { this.afterStateChanged += value; }
			remove { this.afterStateChanged -= value; }
		}

		protected virtual void OnAfterStateChanged()
		{
			if ( this.afterStateChanged != null )
			{
				this.afterStateChanged( this, EventArgs.Empty );
			}
		}

		#endregion // Events
	}

	public delegate void StateEventHandler( MobileRuleState mobileState );

	//public abstract class Rule
	//{

	//}
}
