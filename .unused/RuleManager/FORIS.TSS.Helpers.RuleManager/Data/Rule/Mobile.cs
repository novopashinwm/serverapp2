using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class MobileTable:
		Table<MobileRow, MobileTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "MOBILE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public MobileTable( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public MobileTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int mobileTypeIdIndex = -1;
		public int MobileTypeIdIndex { get { return this.mobileTypeIdIndex; } }

		private int nameIndex = -1;
		public int NameIndex { get { return this.nameIndex; } }

		private int controllerIdIndex = -1;
		public int ControllerIdIndex { get { return this.controllerIdIndex; } }

		private int globalIdIndex = -1;
		public int GlobalIdIndex { get { return this.globalIdIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex			= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
				this.mobileTypeIdIndex	= this.DataTable.Columns.IndexOf( "MOBILE_TYPE_ID" );
				this.nameIndex			= this.DataTable.Columns.IndexOf( "NAME" );
				this.controllerIdIndex	= this.DataTable.Columns.IndexOf( "CONTROLLER_ID" );
				this.globalIdIndex		= this.DataTable.Columns.IndexOf( "GLOBAL_ID" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex			= -1;
			this.mobileTypeIdIndex	= -1;
			this.nameIndex			= -1;
			this.controllerIdIndex	= -1;
			this.globalIdIndex		= -1;
		}

		#endregion  // View

		protected override MobileRow OnCreateRow( DataRow dataRow )
		{
			return new MobileRow( dataRow );
		}

        protected override void OnRowActionHide(MobileRow RowMobile)
		{
            if (RowMobile.Controller != null)
			{
                this.rowByController.Remove((int)RowMobile.Controller);
			}

            base.OnRowActionHide(RowMobile);

            #region MobileObjectRegisterRow.RowsMobile

            MobileObjectRegisterRow RowMobileObjectRegister = this.Data.MobileObjectRegister.FindRow(RowMobile.GlobalId);

            if (RowMobileObjectRegister != null)
            {
                RowMobileObjectRegister.RowsMobile.Remove(RowMobile);
            }
            else
                throw new ApplicationException("There is no parent RowMobileObjectRegister row for Mobile row");

            #endregion // MobileObjectRegisterRow.RowsMobile
        }
        protected override void OnRowActionShow(MobileRow RowMobile)
        {
            #region RowMobileObjectRegisterRow.RowsDriver

            MobileObjectRegisterRow RowMobileObjectRegister = this.Data.MobileObjectRegister.FindRow(RowMobile.GlobalId);

            if (RowMobileObjectRegister != null)
            {
                RowMobileObjectRegister.RowsMobile.Add(RowMobile);
            }
            else
                throw new ApplicationException("There is no parent RowMobileObjectRegister row for Mobile row");

            #endregion // RowMobileObjectRegisterRow.RowsDriver

            base.OnRowActionShow(RowMobile);

            if (RowMobile.Controller != null)
			{
                if (!this.rowByController.ContainsKey((int)RowMobile.Controller))
				{
                    this.rowByController.Add((int)RowMobile.Controller, RowMobile);
				}
				else
				{
					throw new ApplicationException("Creation of two mobiles with the same controller not allowed!");
				}
			}


		}

		private readonly Dictionary<int, MobileRow> rowByController =
			new Dictionary<int, MobileRow>();

		public IDictionary<int, MobileRow> RowByController
		{
			get { return this.rowByController; }
		}
	}

	public class MobileRow:
		Row<MobileRow, MobileTable, IRuleData>
	{
		#region Constructor & Dispose

		public MobileRow( DataRow dataRow ) :
			base( dataRow )
		{
			this.rowsMobileRule = new MobileRuleChildRowCollection();
            this.rowsMobileOperator = new ChildRowCollection<MobileOperatorRow, MobileOperatorTable, IRuleSecurityData>();
            this.rowsMobileOperatorGroup = new ChildRowCollection<MobileOperatorGroupRow, MobileOperatorGroupTable, IRuleSecurityData>();

			this.rowsMobileRule.Inserted += this.rowsMobileRule_Inserted;
			this.rowsMobileRule.Removing += this.rowsMobileRule_Removing;
			this.rowsMobileRule.Clearing += this.rowsMobileRule_Clearing;
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				this.positionChanged = null;
				this.stateChanged = null;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Fields

		public int MobileType
		{
			get { return (int)this.DataRow[this.Table.MobileTypeIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.MobileTypeIdIndex] = value; }
		}

		public string Name
		{
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public int? Controller
		{
			get
			{
				object Value = this.DataRow[this.Table.ControllerIdIndex, DataRowVersion.Current];

				return 
					Value != DBNull.Value 
						? (int)Value 
						: (int?)null;
			}
			set
			{
				this.DataRow[this.Table.ControllerIdIndex] = (object)value ?? DBNull.Value;
			}
		}

		public int GlobalId
		{
			get { return (int)this.DataRow[this.Table.GlobalIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.GlobalIdIndex] = value; }
		}

		#endregion // Fields

		#region Child rows

		private readonly MobileRuleChildRowCollection rowsMobileRule;

		public MobileRuleChildRowCollection RowsMobileRule
		{
			get { return this.rowsMobileRule; }
		}

		private PositionRow rowPosition;
		/// <summary>
		/// ������ ������ � ������� ������� ��������
		/// </summary>
		/// <remarks>
		/// ��������� ����� MOBILE � POSITION - 1:1,
		/// �� ��������� POSITION - ��������
		/// </remarks>
		[Browsable(false)]
		public PositionRow RowPosition
		{
			get { return this.rowPosition; }
			set
			{
				this.rowPosition = value;

				this.OnPositionChanged();
			}
		}

		#endregion // Child rows

		#region Parent rows

		private ControllerRow rowController;
		[Browsable(false)]
		public ControllerRow RowController
		{
			get { return this.rowController; }
		}

		private MobileTypeRow rowMobileType;
		[Browsable( false )]
		public MobileTypeRow RowMobileType
		{
			get { return this.rowMobileType; }
		}

		private MobileObjectRegisterRow rowMobileObject;
		[Browsable( false )]
		public MobileObjectRegisterRow RowMobileObject
		{
			get { return this.rowMobileObject; }
		}

		#endregion // Parent row

        #region Child security rows

        private readonly ChildRowCollection<MobileOperatorGroupRow, MobileOperatorGroupTable, IRuleSecurityData> rowsMobileOperatorGroup;

        internal ChildRowCollection<MobileOperatorGroupRow, MobileOperatorGroupTable, IRuleSecurityData> RowsMobileOperatorGroup
        {
            get { return this.rowsMobileOperatorGroup; }
        }

        private readonly ChildRowCollection<MobileOperatorRow, MobileOperatorTable, IRuleSecurityData> rowsMobileOperator;

        internal ChildRowCollection<MobileOperatorRow, MobileOperatorTable, IRuleSecurityData> RowsMobileOperator
        {
            get { return this.rowsMobileOperator; }
        }

        #endregion Child security rows

        #region View

        protected override void OnDestroyView()
		{
			this.rowController		= null;
			this.rowMobileType		= null;
			this.rowMobileObject	= null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowController		= this.Data.Controller.FindRow( this.Controller );
			this.rowMobileType		= this.Data.MobileType.FindRow( this.MobileType );
			this.rowMobileObject	= this.Data.MobileObjectRegister.FindRow( this.GlobalId );
		}

		#endregion // View

		protected override void OnBuildFields() {}

		#region Handle rowsMobileRule events

		private void rowsMobileRule_Inserted( object sender, CollectionChangeEventArgs<MobileRuleRow> e )
		{
			MobileRuleRow mobileRuleRow = e.Item;

			mobileRuleRow.AfterStateChanged += this.mobileRuleRow_StateChanged;
		}

		private void rowsMobileRule_Removing( object sender, CollectionChangeEventArgs<MobileRuleRow> e )
		{
			MobileRuleRow mobileRuleRow = e.Item;

			mobileRuleRow.AfterStateChanged -= this.mobileRuleRow_StateChanged;
		}

		private void rowsMobileRule_Clearing( object sender, CollectionEventArgs<MobileRuleRow> e )
		{
			foreach( MobileRuleRow mobileRuleRow in this.rowsMobileRule )
			{
				mobileRuleRow.AfterStateChanged -= this.mobileRuleRow_StateChanged;
			}
		}

		private void mobileRuleRow_StateChanged( object sender, EventArgs e )
		{
			this.OnStateChanged();
		}

		#endregion // Handle rowsMobileRule events

		#region Events

		private EventHandler positionChanged;
		[Browsable(false)]
		public event EventHandler PositionChanged
		{
			add
			{
				Trace.WriteLine( this.GetType().Name + ".add_PositionChanged() " +this.ID);
				this.positionChanged += value;
			}
			remove
			{
				Trace.WriteLine( this.GetType().Name + ".remove_PositionChanged() " + this.ID );

				this.positionChanged -= value;
			}
		}

		protected virtual void OnPositionChanged()
		{
			if( this.positionChanged != null )
			{
				this.positionChanged( this, EventArgs.Empty );
			}
			else
			{
				// Trace.WriteLine( this.GetType().Name + ".OnPositionChanged() " + this.ID );
			}
		}

		private EventHandler stateChanged;
		[Browsable( false )]
		[Obsolete]
		public event EventHandler StateChanged
		{
			add { this.stateChanged += value; }
			remove { this.stateChanged -= value; }
		}

		protected virtual void OnStateChanged()
		{
			if (this.stateChanged != null)
			{
				this.stateChanged( this, EventArgs.Empty );
			}
		}

		#endregion // Events
	}

	public class MobileRuleChildRowCollection:
		ChildRowCollection<MobileRuleRow, MobileRuleTable, IRuleData>
	{
		public MobileRuleRow this[ RuleRow ruleRow ]
		{
			get
			{
				if( !this.hashByRuleRow.ContainsKey( ruleRow ) )
				{
					return null;
				}

				return this.hashByRuleRow[ruleRow];
			}
		}

		public MobileRuleRow this[ string ruleName ]
		{
			get
			{
				if( !this.hashByRuleName.ContainsKey( ruleName ) )
				{
					return null;
				}

				return this.hashByRuleName[ruleName];
			}
		}

		public bool ContainsKey( RuleRow ruleRow )
		{
			return this.hashByRuleRow.ContainsKey( ruleRow );
		}

		public bool ContainsKey( string ruleName )
		{
			return this.hashByRuleName.ContainsKey( ruleName );
		}

		private readonly Dictionary<RuleRow, MobileRuleRow> hashByRuleRow =
			new Dictionary<RuleRow, MobileRuleRow>();

		private readonly Dictionary<string, MobileRuleRow> hashByRuleName =
			new Dictionary<string, MobileRuleRow>();

		protected override void OnInserted( CollectionChangeEventArgs<MobileRuleRow> e )
		{
			this.hashByRuleRow.Add( e.Item.RowRule, e.Item );
			this.hashByRuleName.Add( e.Item.RowRule.Name, e.Item );

			base.OnInserted( e );
		}
		protected override void OnRemoving( CollectionChangeEventArgs<MobileRuleRow> e )
		{
			base.OnRemoving( e );

			this.hashByRuleName.Remove( e.Item.RowRule.Name );
			this.hashByRuleRow.Remove( e.Item.RowRule );
		}
		protected override void OnClearing( CollectionEventArgs<MobileRuleRow> e )
		{
			base.OnClearing( e );

			this.hashByRuleName.Clear();
			this.hashByRuleRow.Clear();
		}
	}
}