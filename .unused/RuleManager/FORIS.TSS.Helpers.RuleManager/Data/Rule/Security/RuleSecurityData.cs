﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule.Security
{
    public class RuleSecurityData :
        SecurityData<IRuleData, RuleSecurityData>,
        IRuleSecurityData,
        IRuleSecurityDataSupplier
    {
        #region Controls & Components

        private System.ComponentModel.IContainer components;
        private RuleSecurityDataDispatcher tablesDataDispatcher;
        private OperatorData principalData;

        private MobileOperatorGroupTable mobileOperatorGroup;
        private MobileOperatorTable mobileOperator;

        private MobileSecurityDataAmbassador daMobileOperatorGroup;
        private MobileSecurityDataAmbassador daMobileOperator;

        #endregion // Controls & Components

        #region Constructor & Dispose

        public RuleSecurityData(IRuleData protectedData)
            : base(protectedData)
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        #endregion // Constructor & Dispose

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tablesDataDispatcher = new RuleSecurityDataDispatcher(this.components);
            this.principalData = new OperatorData(this.components);

            this.mobileOperatorGroup = new MobileOperatorGroupTable(this.components);
            this.mobileOperator = new MobileOperatorTable(this.components);

            this.daMobileOperatorGroup = new MobileSecurityDataAmbassador();
            this.daMobileOperator = new MobileSecurityDataAmbassador();

            this.daMobileOperatorGroup.Item = this.mobileOperatorGroup;
            this.daMobileOperator.Item = this.mobileOperator;

            this.tablesDataDispatcher.Ambassadors.Add(this.daMobileOperatorGroup);
            this.tablesDataDispatcher.Ambassadors.Add(this.daMobileOperator);
        }

        #endregion // Component Designer generated code

        #region Properties

        [Browsable(true)]
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public new IRuleSecurityDataSupplier DataSupplier
        {
            get { return (IRuleSecurityDataSupplier)base.DataSupplier; }
            set { base.DataSupplier = value; }
        }

        [Browsable(true)]
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public new IInvokeBroker InvokeBroker
        {
            get { return base.InvokeBroker; }
            set
            {
                base.InvokeBroker =
                    this.principalData.InvokeBroker = value;
            }
        }

        #endregion // Properties

        #region Tables' order

        protected override FORIS.TSS.Helpers.Data.ITableOrder Tables
        {
            get { return this.tablesDataDispatcher; }
        }

        #endregion // Tables' order

        #region Build & Destroy

        protected override void Destroy()
        {
            this.tablesDataDispatcher.Data = null;

            this.principalData.DataSupplier = null;
        }
        protected override void Build()
        {
            this.principalData.DataSupplier = this.DataSupplier.PrincipalDataSupplier;

            this.tablesDataDispatcher.Data = this;
        }

        #endregion // Build & Destroy

        #region Security

        protected override ISecurityTable GetSecurityTable(SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo)
        {
            switch (objectInfo.TableName)
            {
                case "MOBILE":
                    switch (principalInfo.TableName)
                    {
                        case "OPERATORGROUP":
                            return this.mobileOperatorGroup;

                        case "OPERATOR":
                            return this.mobileOperator;
                    }
                    break;
            }

            throw new ApplicationException();
        }

        public IRuleData ProtectedData
        {
            get { return this.protectedData; }
        }

        public IOperatorData PrincipalData
        {
            get { return this.principalData; }
        }

        public IOperatorDataSupplier PrincipalDataSupplier
        {
            get { return this.principalData; }
        }

        #endregion // Securtiy

        #region Tables properties

        
        [Browsable(false)]
        public MobileOperatorGroupTable MobileOperatorGroup
        {
            get { return this.mobileOperatorGroup; }
        }

        [Browsable(false)]
        public MobileOperatorTable MobileOperator
        {
            get { return this.mobileOperator; }
        }

        #endregion // Tables properties

        public IRuleSecurityDataTreater GetDataTreater()
        {
            return this.DataSupplier.GetDataTreater();
        }
    }

    public interface IRuleSecurityData :
        ISecurityData<IRuleSecurityData, IRuleData, IOperatorData>
    {
        MobileOperatorGroupTable MobileOperatorGroup { get; }
        MobileOperatorTable MobileOperator { get; }
    }
}
