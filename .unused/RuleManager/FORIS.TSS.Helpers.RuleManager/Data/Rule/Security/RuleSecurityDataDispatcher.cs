using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule.Security
{
    public class RuleSecurityDataDispatcher:
        DataDispatcher<IRuleSecurityData>
    {
        public RuleSecurityDataDispatcher( IContainer container ): this()
        {
            container.Add( this );
        }
        public RuleSecurityDataDispatcher()
        {
			
        }

        private readonly MobileSecurityDataAmbassadorCollection ambassadors =
            new MobileSecurityDataAmbassadorCollection();

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new MobileSecurityDataAmbassadorCollection Ambassadors
        {
            get { return this.ambassadors; }
        }

        protected override AmbassadorCollection<IDataItem<IRuleSecurityData>> GetAmbassadorCollection()
        {
            return this.ambassadors;
        }

    }

    public class MobileSecurityDataAmbassadorCollection:
        DataAmbassadorCollection<IRuleSecurityData>
    {
        public new MobileSecurityDataAmbassador this[int index ]
        {
            get { return (MobileSecurityDataAmbassador)base[index]; }
        }
    }

    public class MobileSecurityDataAmbassador:
        DataAmbassador<IRuleSecurityData>
    {
		
    }
}