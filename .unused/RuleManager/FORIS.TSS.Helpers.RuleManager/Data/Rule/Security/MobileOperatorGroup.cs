using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule.Security
{
    public class MobileOperatorGroupTable: 
        SecurityTable<MobileOperatorGroupRow, MobileOperatorGroupTable, IRuleSecurityData, IRuleData, IOperatorData>
    {
        public override string Name
        {
            get { return "R_MOBILE_OPERATORGROUP"; }
        }

        public MobileOperatorGroupTable()
        {
			
        }
        public MobileOperatorGroupTable( IContainer container )
        {
            container.Add( this );
        }

        protected override MobileOperatorGroupRow OnCreateRow( DataRow dataRow )
        {
            return new MobileOperatorGroupRow( dataRow );
        }

        protected override void OnRowActionHide( MobileOperatorGroupRow rowMobileOperatorGroup )
        {
            if( this.Data.ProtectedData != null )
            {
                #region MobileRow.RowsMobileOperatorGroup

                MobileRow RowMobile =
                    this.Data.ProtectedData.Mobile.FindRow( rowMobileOperatorGroup.Mobile );

                if( RowMobile != null )
                {
                    RowMobile.RowsMobileOperatorGroup.Remove( rowMobileOperatorGroup );
                }

                #endregion // MobileRow.RowsMobileOperatorGroup
            }

            base.OnRowActionHide( rowMobileOperatorGroup );
        }

        protected override void OnRowActionShow( MobileOperatorGroupRow rowMobileOperatorGroup )
        {
            base.OnRowActionShow( rowMobileOperatorGroup );

            if( this.Data.ProtectedData != null )
            {
                #region MobileRow.RowsMobileOperatorGroup

                MobileRow RowMobile =
                    this.Data.ProtectedData.Mobile.FindRow( rowMobileOperatorGroup.Mobile );

                if( RowMobile != null )
                {
                    RowMobile.RowsMobileOperatorGroup.Add( rowMobileOperatorGroup );
                }

                #endregion // MobileRow.RowsMobileOperatorGroup
            }
        }

        protected override string SecureObjectIdColumnName
        {
            get { return "MOBILE_ID"; }
        }

        protected override string PrincipalIdColumnName
        {
            get { return "OPERATORGROUP_ID"; }
        }
    }

    public class MobileOperatorGroupRow: 
        SecurityRow<MobileOperatorGroupRow, MobileOperatorGroupTable, IRuleSecurityData, IRuleData, IOperatorData>
    {
        #region Fields

        private const int R_Mobile_OPERATORGROUP_ID = 0;
        private const int Mobile_ID = 1;
        private const int OPERATORGROUP_ID = 2;
        private const int ALLOW_MASK = 3;
        private const int DENY_MASK = 4;
		
        public int Mobile
        {
            get { return (int)this.DataRow[MobileOperatorGroupRow.Mobile_ID, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorGroupRow.Mobile_ID] = value; }
        }
		
        public int OperatorGroup
        {
            get { return (int)this.DataRow[MobileOperatorGroupRow.OPERATORGROUP_ID, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorGroupRow.OPERATORGROUP_ID] = value; }
        }

        public override int AllowMask
        {
            get { return (int)this.DataRow[MobileOperatorGroupRow.ALLOW_MASK, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorGroupRow.ALLOW_MASK] = value; }
        }

        public override int DenyMask
        {
            get { return (int)this.DataRow[MobileOperatorGroupRow.DENY_MASK, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorGroupRow.DENY_MASK] = value; }
        }

        #endregion // Fields

        #region Parent row 

        private OperatorGroupRow rowOperatorGroup;

        public OperatorGroupRow RowOperatorGroup
        {
            get { return this.rowOperatorGroup; }
        }

        private MobileRow rowMobile;

        public MobileRow RowMobile
        {
            get { return this.rowMobile; }
        }

        #endregion // Parent row

        #region Id

        protected override int GetId()
        {
            return (int)this.DataRow[MobileOperatorGroupRow.R_Mobile_OPERATORGROUP_ID];
        }

        #endregion // Id

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.rowOperatorGroup = null;
            this.rowMobile = null;
        }

        protected override void OnBuildView()
        {
            this.rowOperatorGroup = this.Data.PrincipalData.OperatorGroup.FindRow( this.OperatorGroup );
            this.rowMobile = this.Data.ProtectedData.Mobile.FindRow( this.Mobile );

            base.OnBuildView();
        }

        public MobileOperatorGroupRow( DataRow dataRow )
            : base( dataRow )
        {

        }

        protected override void OnBuildFields()
        {
			
        }

        protected override ITssPrincipal GetPrincipal()
        {
            return this.RowOperatorGroup;
        }
    }
}