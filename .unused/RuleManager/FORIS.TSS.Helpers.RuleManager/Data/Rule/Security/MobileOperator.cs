using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule.Security
{
    public class MobileOperatorTable: 
        SecurityTable<MobileOperatorRow, MobileOperatorTable, IRuleSecurityData,IRuleData, IOperatorData>
    {
        public override string Name
        {
            get { return "R_MOBILE_OPERATOR"; }
        }

        public MobileOperatorTable()
        {

        }
        public MobileOperatorTable( IContainer container )
        {
            container.Add( this );
        }

        protected override MobileOperatorRow OnCreateRow( DataRow dataRow )
        {
            return new MobileOperatorRow( dataRow );
        }

        protected override void OnRowActionHide( MobileOperatorRow rowMobileOperator )
        {
            if( this.Data.ProtectedData != null )
            {
                #region MobileRow.RowsMobileOperator

                MobileRow RowMobile =
                    this.Data.ProtectedData.Mobile.FindRow( rowMobileOperator.Mobile );

                if( RowMobile != null )
                {
                    RowMobile.RowsMobileOperator.Remove( rowMobileOperator );
                }

                #endregion // MobileRow.RowsMobileOperator
            }

            base.OnRowActionHide( rowMobileOperator );
        }

        protected override void OnRowActionShow( MobileOperatorRow rowMobileOperator )
        {
            base.OnRowActionShow( rowMobileOperator );
			
            if( this.Data.ProtectedData != null )
            {
                #region MobileRow.RowsMobileOperator

                MobileRow RowMobile =
                    this.Data.ProtectedData.Mobile.FindRow( rowMobileOperator.Mobile );

                if( RowMobile != null )
                {
                    RowMobile.RowsMobileOperator.Add( rowMobileOperator );
                }

                #endregion // MobileRow.RowsMobileOperator
            }
        }

        protected override string SecureObjectIdColumnName
        {
            get { return "MOBILE_ID"; }
        }

        protected override string PrincipalIdColumnName
        {
            get { return "OPERATOR_ID"; }
        }
    }

    public class MobileOperatorRow: 
        SecurityRow<MobileOperatorRow, MobileOperatorTable, IRuleSecurityData, IRuleData, IOperatorData>
    {
        #region Fields

        private const int R_Mobile_OPERATOR_ID = 0;
        private const int Mobile_ID = 1;
        private const int OPERATOR_ID = 2;
        private const int ALLOW_MASK = 3;
        private const int DENY_MASK = 4;

        public int Mobile
        {
            get { return (int)this.DataRow[MobileOperatorRow.Mobile_ID, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorRow.Mobile_ID] = value; }
        }

        public int Operator
        {
            get { return (int)this.DataRow[MobileOperatorRow.OPERATOR_ID, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorRow.OPERATOR_ID] = value; }
        }

        public override int AllowMask
        {
            get { return (int)this.DataRow[MobileOperatorRow.ALLOW_MASK, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorRow.ALLOW_MASK] = value; }
        }

        public override int DenyMask
        {
            get { return (int)this.DataRow[MobileOperatorRow.DENY_MASK, DataRowVersion.Current]; }
            set { this.DataRow[MobileOperatorRow.DENY_MASK] = value; }
        }

        #endregion // Fields

        #region Parent row

        private OperatorRow rowOperator;

        public OperatorRow RowOperator
        {
            get { return this.rowOperator; }
        }

        private MobileRow rowMobile;

        public MobileRow RowMobile
        {
            get { return this.rowMobile; }
        }

        #endregion // Parent row

        #region Id

        protected override int GetId()
        {
            return (int)this.DataRow[MobileOperatorRow.R_Mobile_OPERATOR_ID];
        }

        #endregion // Id

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.rowOperator = null;
            this.rowMobile = null;
        }
        protected override void OnBuildView()
        {
            this.rowOperator = this.Data.PrincipalData.Operator.FindRow( this.Operator );
            this.rowMobile = this.Data.ProtectedData.Mobile.FindRow( this.Mobile );

            base.OnBuildView();
        }
        public MobileOperatorRow( DataRow dataRow )
            : base( dataRow )
        {

        }

        protected override void OnBuildFields()
        {

        }

        protected override ITssPrincipal GetPrincipal()
        {
            return this.RowOperator;
        }
    }
}