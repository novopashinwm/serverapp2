﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class MobileObjectRegisterTable:
		Table<MobileObjectRegisterRow, MobileObjectRegisterTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "MOBILE_OBJECT_REGISTER"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public MobileObjectRegisterTable( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public MobileObjectRegisterTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int mobileObjectTypeIdIndex = -1;
		public int MobileObjectTypeIdIndex { get { return this.mobileObjectTypeIdIndex; } }

		private int nameIndex = -1;
		public int NameIndex { get { return this.nameIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex				 = this.DataTable.Columns.IndexOf( "GLOBAL_ID" );
				this.mobileObjectTypeIdIndex = this.DataTable.Columns.IndexOf( "MOBILE_OBJECT_TYPE_ID" );
				this.nameIndex				 = this.DataTable.Columns.IndexOf( "NAME" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex					= -1;
			this.mobileObjectTypeIdIndex	= -1;
			this.nameIndex					= -1;
		}

		#endregion  // View

		protected override MobileObjectRegisterRow OnCreateRow( DataRow dataRow )
		{
			return new MobileObjectRegisterRow( dataRow );
		}
	}

	public class MobileObjectRegisterRow:
		Row<MobileObjectRegisterRow, MobileObjectRegisterTable, IRuleData>
	{
		#region Constructor & Dispose

		public MobileObjectRegisterRow( DataRow dataRow ) : base( dataRow ) 
        {
            this.rowsMobile = new ChildRowCollection<MobileRow, MobileTable, IRuleData>();
        }

		#endregion // Constructor & Dispose

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Fields

		public int MobileObjectType
		{
			get { return (int)this.DataRow[this.Table.MobileObjectTypeIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.MobileObjectTypeIdIndex] = value; }
		}

		public string Name
		{
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		#endregion // Fields

		#region Parent rows

		private MobileObjectTypeRow rowMobileObjectType;
		[Browsable( false )]
		public MobileObjectTypeRow rowMobileObjectTypeRowMobileType
		{
			get { return this.rowMobileObjectType; }
		}

		#endregion  // Parent row

        #region Child rows

        private readonly ChildRowCollection<MobileRow, MobileTable, IRuleData> rowsMobile;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ChildRowCollection<MobileRow, MobileTable, IRuleData> RowsMobile
        {
            get { return this.rowsMobile; }
        }

        #endregion // Child rows

        #region View

        protected override void OnDestroyView()
		{
			this.rowMobileObjectType = null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowMobileObjectType = this.Data.MobileObjectType.FindRow( this.MobileObjectType );
		}

		#endregion  // View

		protected override void OnBuildFields() {}

        public MobileRow GetMobileRow(int GlobalId)
        {
            foreach (MobileRow mobileRow in this.RowsMobile)
            {
                if (mobileRow.GlobalId == GlobalId)
                    return mobileRow;
            }

			//Debug.Assert(false, "MobileObjectRegisterRow.GetMobileRow - can't find needed mobile by Global_Id!");

            return null;
        }
	}
}