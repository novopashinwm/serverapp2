using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using System.Runtime.Remoting.Lifetime;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	/// <summary>
	/// ��������� ���������� � ���������
	/// </summary>
	public interface IRuleData:
		IData<IRuleDataTreater>,
        ISecureData<IRuleSecurityData>,
		IControllerData,
        ISponsor
	{
		MobileRuleSettings CreateMobileRuleSettings( int ruleId );

		/// <summary>
		/// ��� ������� � ������� �������� Mobile
		/// </summary>
		MobileObjectTypeTable MobileObjectType { get; }

		/// <summary>
		/// ������ �������� � �������� ����� �������� Mobile
		/// </summary>
		MobileObjectRegisterTable MobileObjectRegister { get; }

		/// <summary>
		/// ������ ������, �������� �������
		/// </summary>
		RuleTable Rule { get; }

		/// <summary>
		/// ���� �������� ��������
		/// </summary>
		MobileTypeTable MobileType { get; }

		/// <summary>
		/// ������� ��������
		/// </summary>
		MobileTable Mobile { get; }

		/// <summary>
		/// ������� ��������� ��������
		/// </summary>
		PositionTable Position { get; }

		/// <summary>
		/// ���������� ������ �������� ��������
		/// </summary>
		MobileRuleTable MobileRule { get; }

		/// <summary>
		/// ��������� �������� �������� �� ���������� ������
		/// </summary>
		StateTable State { get; }
	}
}