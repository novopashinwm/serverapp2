using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class RuleDataDispatcher:
		DataDispatcher<IRuleData>
	{
		#region Constructor

		public RuleDataDispatcher( IContainer container ):this()
		{
			container.Add( this );
		}
		public RuleDataDispatcher()
		{
			
		}

		#endregion // Constructor

		private readonly RuleDataAmbassadorCollection ambassadors =
			new RuleDataAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new RuleDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRuleData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class RuleDataAmbassadorCollection:
		DataAmbassadorCollection<IRuleData>
	{
		public new RuleDataAmbassador this[int index ]
		{
			get { return (RuleDataAmbassador)base[index]; }
		}
	}

	public class RuleDataAmbassador:
		DataAmbassador<IRuleData>
	{
		
	}
}