using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class RuleData:
		SecureData<IRuleDataTreater>,
		IRuleData,
		IRuleDataSupplier
	{
		#region Controls & Component

		private IContainer components;
		private RuleDataDispatcher tablesDataDispatcher;

		private ControllerData controllerData;

		private MobileObjectTypeTable		mobileObjectType;
		private MobileObjectRegisterTable	mobileObjectRegister;
		private RuleTable					rule;
		private MobileTypeTable				mobileType;
		private MobileTable					mobile;
		private PositionTable				position;
		private MobileRuleTable				mobileRule;
		private StateTable					state;

		private RuleDataAmbassador daMobileObjectTypeTable;
		private RuleDataAmbassador daMobileObjectRegisterTable;
		private RuleDataAmbassador daRule;
		private RuleDataAmbassador daMobileType;
		private RuleDataAmbassador daMobile;
		private RuleDataAmbassador daPosition;
		private RuleDataAmbassador daMobileRule;
		private RuleDataAmbassador daState;
		
		#endregion // Controls & Components

		#region Constructor & Dispose

		public RuleData( IContainer container ): this()
		{
			container.Add( this );
		}

		public RuleData()
		{
            this.securityData = new RuleSecurityData(this);
			
            InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing)
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new RuleDataDispatcher( this.components );

			this.controllerData = new ControllerData( this.components );

			this.mobileObjectType		= new MobileObjectTypeTable		( this.components );
			this.mobileObjectRegister	= new MobileObjectRegisterTable	( this.components );
			this.rule						= new RuleTable					( this.components );
			this.mobileType					= new MobileTypeTable			( this.components );
			this.mobile						= new MobileTable				( this.components );
			this.position					= new PositionTable				( this.components );
			this.mobileRule					= new MobileRuleTable			( this.components );
			this.state						= new StateTable				( this.components );

			this.daMobileObjectTypeTable		= new RuleDataAmbassador();
			this.daMobileObjectRegisterTable	= new RuleDataAmbassador();
			this.daRule							= new RuleDataAmbassador();
			this.daMobileType					= new RuleDataAmbassador();
			this.daMobile						= new RuleDataAmbassador();
			this.daPosition						= new RuleDataAmbassador();
			this.daMobileRule					= new RuleDataAmbassador();
			this.daState						= new RuleDataAmbassador();

			this.daMobileObjectTypeTable.Item		= this.mobileObjectType;
			this.daMobileObjectRegisterTable.Item	= this.mobileObjectRegister;
			this.daRule.Item						= this.rule;
			this.daMobileType.Item					= this.mobileType;
			this.daMobile.Item						= this.mobile;
			this.daPosition.Item					= this.position;
			this.daMobileRule.Item					= this.mobileRule;
			this.daState.Item						= this.state;

			this.tablesDataDispatcher.Ambassadors.Add( this.daMobileObjectTypeTable );
			this.tablesDataDispatcher.Ambassadors.Add( this.daMobileObjectRegisterTable );
			this.tablesDataDispatcher.Ambassadors.Add( this.daRule );
			this.tablesDataDispatcher.Ambassadors.Add( this.daMobileType );
			this.tablesDataDispatcher.Ambassadors.Add( this.daMobile );
			this.tablesDataDispatcher.Ambassadors.Add( this.daPosition );
			this.tablesDataDispatcher.Ambassadors.Add( this.daMobileRule );
			this.tablesDataDispatcher.Ambassadors.Add( this.daState );
		}

		#endregion // Component Designer generated code

		#region Properties

		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public new IRuleDataSupplier DataSupplier
		{
			get { return (IRuleDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public new IInvokeBroker InvokeBroker
		{
			get { return base.InvokeBroker; }
			set
			{
				base.InvokeBroker =
                    this.securityData.InvokeBroker =
					this.controllerData.InvokeBroker = value;
			}
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
            this.securityData.DataSupplier = null;

			this.tablesDataDispatcher.Data = null;

			this.controllerData.DataSupplier = null;
		}
		protected override void Build()
		{
			this.controllerData.DataSupplier = this.DataSupplier.ControllerDataSupplier;

			this.tablesDataDispatcher.Data = this;

		    this.securityData.DataSupplier = this.DataSupplier.SecurityDataSupplier;
		}

		#endregion // Build & Destroy

		#region Tables properties

		#region Controller

		[Browsable(false)]
		public DepartmentTable Department
		{
			get { return this.controllerData.Department; }
		}

		[Browsable(false)]
		public ControllerTypeTable ControllerType
		{
			get { return this.controllerData.ControllerType; }
		}

		[Browsable( false )]
		public ControllerSensorTypeTable ControllerSensorType
		{
			get { return this.controllerData.ControllerSensorType; }
		}

		[Browsable( false )]
		public ControllerSensorTable ControllerSensor
		{
			get { return this.controllerData.ControllerSensor; }
		}

		[Browsable(false)]
		public ControllerTable Controller
		{
			get { return this.controllerData.Controller; }
		}

		[Browsable( false )]
		public ControllerSensorLegendTable ControllerSensorLegend
		{
			get { return this.controllerData.ControllerSensorLegend; }
		}

		[Browsable( false )]
		public ControllerSensorMapTable ControllerSensorMap
		{
			get { return this.controllerData.ControllerSensorMap; }
		}

		#endregion // Controller

		[Browsable( false )]
		public MobileObjectTypeTable MobileObjectType
		{
			get { return this.mobileObjectType; }
		}

		[Browsable( false )]
		public MobileObjectRegisterTable MobileObjectRegister
		{
			get { return this.mobileObjectRegister; }
		}

		[Browsable( false )]
		public RuleTable Rule
		{
			get { return this.rule; }
		}

		[Browsable( false )]
		public MobileTypeTable MobileType
		{
			get { return this.mobileType; }
		}

		[Browsable(false)]
		public PositionTable Position
		{
			get { return this.position; }
		}

		[Browsable(false)]
		public MobileTable Mobile
		{
			get { return this.mobile; }
		}

		[Browsable(false)]
		public MobileRuleTable MobileRule
		{
			get { return this.mobileRule; }
		}

		[Browsable(false)]
		public StateTable State
		{
			get { return this.state; }
		}

		#endregion // Tables properties

		#region IRuleData Members

		#region IControllerData Members

		public event EventHandler DataChanged
		{
			add { this.controllerData.DataChanged += value; }
			remove { this.controllerData.DataChanged -= value; }
		}

		#endregion  // IControllerData Members

		public virtual MobileRuleSettings CreateMobileRuleSettings( int ruleId )
		{
			return this.DataSupplier.CreateMobileRuleSettings( ruleId );
		}

		#endregion // IRuleData Members

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IControllerDataSupplier ControllerDataSupplier
		{
			get { return this.DataSupplier.ControllerDataSupplier; }
		}

		IControllerDataTreater IDataSupplier<IControllerDataTreater>.GetDataTreater()
		{
			return this.ControllerDataSupplier.GetDataTreater();
		}

	    private readonly RuleSecurityData securityData;
        [Browsable(false)]
	    public IRuleSecurityData SecurityData
	    {
            get { return this.securityData; }
	    }

	    public IRuleSecurityDataSupplier SecurityDataSupplier
	    {
            get { return this.securityData; }
	    }
	}
}