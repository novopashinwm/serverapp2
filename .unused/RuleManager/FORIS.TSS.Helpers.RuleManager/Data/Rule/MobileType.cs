﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class MobileTypeTable :
		Table<MobileTypeRow, MobileTypeTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "MOBILE_TYPE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public MobileTypeTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public MobileTypeTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int nameIdIndex = -1;
		public int NameIdIndex { get { return nameIdIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex		= this.DataTable.Columns.IndexOf( "MOBILE_TYPE_ID" );
				this.nameIdIndex	= this.DataTable.Columns.IndexOf( "NAME" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex		= -1;
			this.nameIdIndex	= -1;
		}

		#endregion  // View

		protected override MobileTypeRow OnCreateRow( DataRow dataRow )
		{
			return new MobileTypeRow( dataRow );
		}
	}

	public class MobileTypeRow : Row<MobileTypeRow, MobileTypeTable, IRuleData>
	{
		#region Fields

		/// <summary>
		/// Название
		/// </summary>
		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIdIndex] = value; }
		}

		#endregion //Fields

		#region Id

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Идентификатор строки</returns>
		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		public MobileTypeRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() { }
	}
}