﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class MobileObjectTypeTable :
		Table<MobileObjectTypeRow, MobileObjectTypeTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "MOBILE_OBJECT_TYPE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public MobileObjectTypeTable( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public MobileObjectTypeTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int nameIndex = -1;
		public int NameIndex { get { return this.nameIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex = this.DataTable.Columns.IndexOf( "MOBILE_OBJECT_TYPE_ID" );
				this.nameIndex = this.DataTable.Columns.IndexOf( "NAME" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.nameIndex = -1;
		}

		#endregion  // View

		protected override MobileObjectTypeRow OnCreateRow( DataRow dataRow )
		{
			return new MobileObjectTypeRow( dataRow );
		}
	}

	public class MobileObjectTypeRow :
		Row<MobileObjectTypeRow, MobileObjectTypeTable, IRuleData>
	{
		#region Constructor & Dispose

		public MobileObjectTypeRow( DataRow dataRow ) : base( dataRow ) { }

		#endregion // Constructor & Dispose

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		#endregion // Fields

		protected override void OnBuildFields() { }
	}
}