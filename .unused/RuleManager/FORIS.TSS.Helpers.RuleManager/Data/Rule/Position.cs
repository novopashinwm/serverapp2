using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class PositionTable:
		Table<PositionRow, PositionTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "POSITION"; }
		}

		#endregion // Properties

		#region Constructor & Dispose

		public PositionTable( IContainer container) : this()
		{
			container.Add( this );
		}
		public PositionTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return this.idIndex; } }

		private int mobileIdIndex = -1;
		public int MobileIdIndex { get { return this.mobileIdIndex; } }

		private int timeIndex = -1;
		public int TimeIndex { get { return this.timeIndex; } }

		private int latitudeIndex = -1;
		public int LatitudeIndex { get { return latitudeIndex; } }

		private int longitudeIndex = -1;
		public int LongitudeIndex { get { return this.longitudeIndex; } }

		private int courseIndex = -1;
		public int CourseIndex { get { return this.courseIndex; } }

		private int speedIndex = -1;
		public int SpeedIndex { get { return this.speedIndex; } }

	    private int validIndex = -1;
        public int ValidIndex { get { return this.validIndex; } }

	    private int correctTimeIndex = -1;
        public int CorrectTimeIndex {get { return this.correctTimeIndex; }}

		private int sensorsIndex = -1;
		public int SensorsIndex { get { return this.sensorsIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex		= this.DataTable.Columns.IndexOf( "POSITION_ID" );
				this.mobileIdIndex	= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
				this.timeIndex		= this.DataTable.Columns.IndexOf( "TIME" );
				this.latitudeIndex	= this.DataTable.Columns.IndexOf( "LATITUDE" );
				this.longitudeIndex = this.DataTable.Columns.IndexOf( "LONGITUDE" );
				this.courseIndex	= this.DataTable.Columns.IndexOf( "COURSE" );
				this.speedIndex		= this.DataTable.Columns.IndexOf( "SPEED" );
				this.sensorsIndex	= this.DataTable.Columns.IndexOf( "SENSORS" );
			    this.validIndex     = this.DataTable.Columns.IndexOf( "VALID" );
			    this.correctTimeIndex=this.DataTable.Columns.IndexOf( "CORRECT_TIME" );
			}
		}
		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex		= -1;
			this.mobileIdIndex	= -1;
			this.timeIndex		= -1;
			this.latitudeIndex	= -1;
			this.longitudeIndex = -1;
			this.courseIndex	= -1;
			this.speedIndex		= -1;
			this.sensorsIndex	= -1;
		    this.validIndex     = -1;
		    this.correctTimeIndex=-1;
		}

		#endregion  // View

		protected override PositionRow OnCreateRow( DataRow dataRow )
		{
			return new PositionRow( dataRow );
		}

		protected override void OnRowActionHide( PositionRow row )
		{
			try
			{
				#region MobileRow.RowPosition

				MobileRow mobileRow = this.Data.Mobile.FindRow( row.Mobile );

				if( mobileRow == null )
				{
					throw new ApplicationException( "There is no parent Mobile row for Position row" );
				}

				mobileRow.RowPosition = null;

				#endregion // MobileRow.RowPosition
			}
			finally
			{
				base.OnRowActionHide( row );
			}
		}
		protected override void OnRowActionShow( PositionRow row )
		{
			base.OnRowActionShow( row );

			#region MobileRow.RowPosition

			MobileRow mobileRow = this.Data.Mobile.FindRow( row.Mobile );

			if (mobileRow == null)
			{
				throw new ApplicationException("There is no parent Mobile row for Position row");
			}

			mobileRow.RowPosition = row;
				
			#endregion // MobileRow.RowPosition
		}
	}

	public class PositionRow:
		Row<PositionRow, PositionTable, IRuleData>
	{
		#region Fields

		/// <summary>
		/// ������ ��������.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		/// ������������� ����������� ������ �� ���������� 
		/// ������ � ������� ��� �������� ����� ������
		/// ������� �������� <see cref="MobileRow"/>
		/// </exception>
		public int Mobile
		{
			get { return (int)this.DataRow[this.Table.MobileIdIndex, DataRowVersion.Current]; }
			set
			{
				#region Preconditions

				if( this.DataRow.RowState != DataRowState.Detached )
				{
					throw new InvalidOperationException( "Only for new rows" );
				}

				#endregion // Preconditions

				this.DataRow[this.Table.MobileIdIndex] = value;
			}
		}

		/// <summary>
		/// ����� �������
		/// </summary>
		/// <exception cref="InvalidCastException">
		/// ����� �������� ��������� <see cref="Empty"/>
		/// </exception>
		public DateTime? Time
		{
			get
			{
				if ( this.DataRow.IsNull( this.Table.TimeIndex ) )
				{
					return null;
				}
				else
				{
					return (DateTime)this.DataRow[this.Table.TimeIndex, DataRowVersion.Current];
				}
			}
			set
			{
				this.DataRow[this.Table.TimeIndex] = 
					value == null 
						? DBNull.Value
						: (object)value;
			}
		}

        /// <summary>
        /// ����� �������� ������� �������
        /// </summary>
        /// <exception cref="InvalidCastException">
        /// ����� �������� ��������� <see cref="Empty"/>
        /// </exception>
        public DateTime? CorrectTime
        {
            get
            {
                if (this.DataRow.IsNull(this.Table.CorrectTimeIndex))
                {
                    return null;
                }
                return (DateTime)this.DataRow[this.Table.CorrectTimeIndex, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.CorrectTimeIndex] =
                    value == null
                        ? DBNull.Value
                        : (object)value;
            }
        }

		/// <summary>
		/// ������
		/// </summary>
		/// <exception cref="InvalidCastException">
		/// ����� �������� ��������� <see cref="Empty"/>
		/// </exception>
		public float? Latitude
		{
			get
			{
				if ( this.DataRow.IsNull( this.Table.LatitudeIndex ) ) { return null; }
				else { return (float)this.DataRow[this.Table.LatitudeIndex, DataRowVersion.Current]; }
			}
			set
			{
				this.DataRow[this.Table.LatitudeIndex] =
					value == null
						? DBNull.Value
						: (object)value;
			}
		}

		/// <summary>
		/// �������
		/// </summary>
		/// <exception cref="InvalidCastException">
		/// ����� �������� ��������� <see cref="Empty"/>
		/// </exception>
		public float? Longitude
		{
			get
			{
				if ( this.DataRow.IsNull( this.Table.LongitudeIndex ) ) { return null; }
				else { return (float)this.DataRow[this.Table.LongitudeIndex, DataRowVersion.Current]; }
			}
			set
			{
				this.DataRow[this.Table.LongitudeIndex] =
					value == null
						? DBNull.Value
						: (object)value;
			}
		}

		/// <summary>
		/// ����������� (������?)
		/// </summary>
		/// <exception cref="InvalidCastException">
		/// ����� �������� ��������� <see cref="Empty"/>
		/// </exception>
		public int Course
		{
			get
			{
				if ( this.DataRow.IsNull( this.Table.CourseIndex ) ) { return 0; }
				else { return (int)this.DataRow[this.Table.CourseIndex, DataRowVersion.Current]; }
			}
			set { this.DataRow[this.Table.CourseIndex] = value; }
		}

		/// <summary>
		/// ��������
		/// </summary>
		/// <exception cref="InvalidCastException">
		/// ����� �������� ��������� <see cref="Empty"/>
		/// </exception>
		public float Speed
		{
			get
			{
				if ( this.DataRow.IsNull( this.Table.SpeedIndex ) ) { return 0; }
				else { return (float)this.DataRow[this.Table.SpeedIndex, DataRowVersion.Current]; }
			}
			set { this.DataRow[this.Table.SpeedIndex] = value; }
		}

	    public bool Valid
	    {
	        get
	        {
	            if( this.DataRow.IsNull(this.Table.ValidIndex)) {
	                return false;}
	            return (bool) this.DataRow[this.Table.ValidIndex, DataRowVersion.Current];
	        }
            set { this.DataRow[this.Table.ValidIndex] = value; }
	    }

		private Dictionary<int, object> sensors = null;
		/// <summary>
		/// ��������� ��������
		/// </summary>
		public Dictionary<int, object> Sensors
		{
			get { return this.sensors; }
			set
			{
				using ( MemoryStream stream = new MemoryStream() )
				{
					/*
					 * TODO: �� ������������ �����
					 */
					BinaryFormatter formatter = new BinaryFormatter();

					formatter.Serialize( stream, value );

					stream.Close();

					this.DataRow[this.Table.SensorsIndex] = stream.ToArray();
				}
			}
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public PositionRow( DataRow dataRow )
			: base( dataRow )
		{

		}

		protected override void OnBuildFields()
		{
			//if ( !this.DataRow.IsNull( this.Table.SensorsIndex ) )
			//{
			//    try
			//    {
			//        byte[] stateData = 
			//            (byte[])this.DataRow[this.Table.SensorsIndex, DataRowVersion.Current];

			//        using ( MemoryStream stream = new MemoryStream( stateData ) )
			//        {
			//            /*
			//             * TODO: �� ������������ �����
			//             */
			//            BinaryFormatter formatter = new BinaryFormatter();

			//            this.sensors = (Dictionary<int, object>)formatter.Deserialize( stream );
			//        }
			//    }
			//    catch ( Exception ex )
			//    {
			//        // TODO: ������ �������� ��������� ��������

			//        /* ���� ��������� �������� � ��������� ������� 
			//         * �� ���� ������, �� ���������� 
			//         * ������������� �������� �� ���������.
			//         */

			//        this.sensors = null;

			//        Trace.WriteLine(
			//            "������ �������� ��������� �������� �� ������� POSITION: " 
			//            + ex.Message );
			//    }
			//}
			//else
			//{
			//    this.sensors = null;
			//}
		}

		#region Methods

		/// <summary>
		/// ������?
		/// </summary>
		public bool Empty
		{
			get
			{
				return
					this.DataRow[this.Table.TimeIndex, DataRowVersion.Current] == DBNull.Value
					|| this.DataRow[this.Table.LatitudeIndex, DataRowVersion.Current] == DBNull.Value
					|| this.DataRow[this.Table.LongitudeIndex, DataRowVersion.Current] == DBNull.Value;
			}
		}

		/// <summary>
		/// �������� �������
		/// </summary>
		public void Clear()
		{
			this.DataRow[this.Table.TimeIndex]		= DBNull.Value;
			this.DataRow[this.Table.LatitudeIndex]	= DBNull.Value;
			this.DataRow[this.Table.LongitudeIndex] = DBNull.Value;
			this.DataRow[this.Table.CourseIndex]	= DBNull.Value;
			this.DataRow[this.Table.SpeedIndex]		= DBNull.Value;
			this.DataRow[this.Table.SensorsIndex]	= DBNull.Value;
		    this.DataRow[this.Table.ValidIndex]     = DBNull.Value;
            this.DataRow[this.Table.CorrectTimeIndex]=DBNull.Value;
		}

		#endregion  // Methods
	}
}