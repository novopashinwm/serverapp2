using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class RuleTable: 
		Table<RuleRow, RuleTable, IRuleData>
	{
		public override string Name
		{
			get { return "RULE"; }
		}

		public RuleTable()
		{
			
		}
		public RuleTable( IContainer container )
		{
			container.Add( this );
		}

		#region Fields' indexes

		private int idIndex = -1;
		internal int IdIndex { get { return this.idIndex; } }

		private int nameIndex = -1;
		internal int NameIndex { get { return this.nameIndex; } }

		private int descriptionIndex = -1;
		internal int DescriptionIndex { get { return this.descriptionIndex; } }

		private int codeIndex = -1;
		internal int CodeIndex { get { return this.codeIndex; } }

		#endregion // Fields' indexes 

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.nameIndex = -1;
			this.descriptionIndex = -1;
			this.codeIndex = -1;
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.DataTable != null )
			{
				this.idIndex = this.DataTable.Columns.IndexOf( "RULE_ID" );
				this.nameIndex = this.DataTable.Columns.IndexOf( "NAME" );
				this.descriptionIndex = this.DataTable.Columns.IndexOf( "DESCRIPTION" );
				this.codeIndex = this.DataTable.Columns.IndexOf( "CODE" );
			}
		}

		protected override RuleRow OnCreateRow( DataRow dataRow )
		{
			return new RuleRow( dataRow );
		}

		protected override void OnRowActionHide( RuleRow row )
		{
			this.rowsByName.Remove( row.Name );

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( RuleRow row )
		{
			base.OnRowActionShow( row );

			this.rowsByName.Add( row.Name, row );
		}

		private readonly Dictionary<string, RuleRow> rowsByName =
			new Dictionary<string, RuleRow>();

		public IDictionary<string, RuleRow> RowsByName
		{
			get { return this.rowsByName; }
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// ��� ��� �� ������� ������� ���������� 
	/// ������ ������ (������ ���� ����������),
	/// �� �� ����� ���� �������� �������������
	/// ��� ������� �������. ������ ����� 
	/// ���������������� ������ ���� �������� 
	/// � ������� RuleServerData
	/// </remarks>
	public class RuleRow: 
		Row<RuleRow, RuleTable, IRuleData>
	{
		#region Fields

		public string Name
		{
            get { return (string)this.DataRow[ this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Description
		{
            get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DescriptionIndex] = value; }
		}

		public string Code
		{
            get { return (string)this.DataRow[this.Table.CodeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.CodeIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public RuleRow( DataRow dataRow): base( dataRow )
		{
			
		}

		protected override void OnBuildFields()
		{

		}
	}
}