using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.Rule
{
	public class StateTable:
		Table<StateRow, StateTable, IRuleData>
	{
		#region Properties

		public override string Name
		{
			get { return "STATE"; }
		}

		#endregion // Properties

		#region Constructor & Dispose

		public StateTable( IContainer container ): this()
		{
			container.Add( this );
		}
		public StateTable()
		{
			
		}

		#endregion // Constructor & Dispose

		protected override StateRow OnCreateRow( DataRow dataRow )
		{
			return new StateRow( dataRow );
		}

		protected override void OnRowActionHide( StateRow row )
		{
			try
			{
				#region MobileRuleRow.RowState

				MobileRuleRow mobileRuleRow = this.Data.MobileRule.FindRow( row.MobileRule );

				if( mobileRuleRow != null )
				{
					mobileRuleRow.RowState = null;
				}
				else
				{
					/* �� ����� ���������� ��������� � ��, �������� �����, 
					 * ����������� �����, ������-�� �� ������ ���������
					 * ������������ MobileRule ������ ��� ���������� 
					 * ����� �������� ��������� � ������� ������.
					 * 
					 * ������������ ������ �� ��������� �� ��� �������, 
					 * ��� ������� ������������� �������������� �������� 
					 * ������ (������� ����), � ���� ����� ������������ 
					 * ��������� ���������� ����� � ������������ ������.
					 * 
					 * ��� ������������� ��� ���������� Commit.
					 * 
					 * ������ ��� �������� ����� ����� ��� �������� 
					 * ����� � ������������ � �������� ����� �� 
					 * ���� �������� �������������� ������.
					 */

					// throw new ApplicationException( "There is no parent MobileRule row for State row" );
				}

				#endregion // MobileRuleRow.RowState
			}
			finally
			{
				base.OnRowActionHide( row );
			}
		}

		protected override void OnRowActionShow( StateRow row )
		{
			base.OnRowActionShow( row );

			try
			{
				#region MobileRuleRow.RowState

				MobileRuleRow mobileRuleRow = this.Data.MobileRule.FindRow( row.MobileRule );

				if( mobileRuleRow != null )
				{
					mobileRuleRow.RowState = row;
				}
				else
				{
					/* �� ����� ���������� ��������� � ��, �������� �����, 
					 * ����������� �����, ������-�� �� ������ ���������
					 * ������������ MobileRule ������ ��� ���������� 
					 * ����� �������� ��������� � ������� ������
					 */

					// throw new ApplicationException( "There is no parent MobileRule row for State row" );
				}

				#endregion // MobileRuleRow.RowState
			}
			finally{}
		}
	}

	public class StateRow:
		Row<StateRow, StateTable, IRuleData>
	{
		#region Fields

		private const int STATE_ID = 0;
		private const int MOBILE_RULE_ID = 1;
		private const int STATE = 2;

		public int MobileRule
		{
			get { return (int)this.DataRow[StateRow.MOBILE_RULE_ID, DataRowVersion.Current]; }
			set { this.DataRow[StateRow.MOBILE_RULE_ID] = value; }
		}

		private MobileRuleState state;
		public MobileRuleState State
		{
			get { return this.state; }
			set
			{
				if( !object.Equals( this.state, value ) )
				{
					using( MemoryStream stream = new MemoryStream() )
					{
						/* ����� ������ ������������ ������������ �� � �������� ����, 
						 * � � ���� xml ����� ����� ����� ���� ������ ������� �� ���������� 
						 * ��������������� � ���� ������.
						 */
						#region SoapFormatter
						/**/
						SoapFormatter formatter = new SoapFormatter();
						formatter.Serialize( stream, value );
						stream.Close();

						UTF8Encoding utf8 = new UTF8Encoding();
						this.DataRow[StateRow.STATE] = utf8.GetString( stream.ToArray() );
						//this.DataRow[StateRow.STATE] = stream.ToArray();

						this.state = value;
						/**/
						#endregion  // SoapFormatter

						#region BinaryFormatter
						/*
						// TODO: �� ������������ �����
						BinaryFormatter formatter = new BinaryFormatter();

						formatter.Serialize( stream, value );

						stream.Close();

						this.DataRow[StateRow.STATE] = stream.ToArray();
						/**/
						#endregion  // BinaryFormatter
					}
				}
			}
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[StateRow.STATE_ID];
		}

		#endregion // Id 

		public StateRow( DataRow dataRow )
			: base( dataRow )
		{

		}

		protected override void OnBuildFields()
		{
			if( !this.DataRow.IsNull( StateRow.STATE ) )
			{
				try
				{
					//byte[] stateData = (byte[])this.DataRow[StateRow.STATE, DataRowVersion.Current];
					string stateData = (string)this.DataRow[StateRow.STATE, DataRowVersion.Current];
					UTF8Encoding utf8 = new UTF8Encoding();

					using ( MemoryStream stream = 
						new MemoryStream( utf8.GetBytes( stateData ) ) )
					{
						/* ����� ������ ������������ ������������ �� � �������� ����, 
						 * � � ���� xml ����� ����� ����� ���� ������ ������� �� ���������� 
						 * ��������������� � ���� ������.
						 */
						#region SoapFormatter
						/**/
						SoapFormatter formatter = new SoapFormatter();

						this.state = (MobileRuleState)formatter.Deserialize( stream );
						/**/
						#endregion  // SoapFormatter

						#region BinaryFormatter
						/*
						// TODO: �� ������������ �����
						BinaryFormatter formatter = new BinaryFormatter();

						this.state = (MobileRuleState)formatter.Deserialize( stream );
						/**/
						#endregion  // BinaryFormatter
					}
				}
				catch( Exception ex )
				{
					// TODO: ������ �������� ���������

					/* ���� ��������� �������� � ��������� ������� 
					 * ��������� �� ���� ������ �� ���������� 
					 * ������������� ��������� ��������� (�� ���������)
					 */

					this.state = null;

					Trace.WriteLine(
						"������ ������������ ������� ��������� �������: " + ex.Message
						);
				}
			}
			else
			{
				this.state = null;
			}
		}
	}
}
