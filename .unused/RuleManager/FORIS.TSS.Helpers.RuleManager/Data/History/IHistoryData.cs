using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.History
{
	/// <summary>
	/// ��������� ���������� � ��������
	/// </summary>
	public interface IHistoryData :
		IData
	{
		/// <summary>
		/// �������
		/// </summary>
		HistoryTable History { get; }

		string MobileName { get; set; }
	}
}