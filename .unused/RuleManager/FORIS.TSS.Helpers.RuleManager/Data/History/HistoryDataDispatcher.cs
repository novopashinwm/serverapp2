using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.History
{
	public class HistoryDataDispatcher :
		DataDispatcher<IHistoryData>
	{
		public HistoryDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public HistoryDataDispatcher()
		{

		}

		private readonly HistoryDataAmbassadorCollection ambassadors =
			new HistoryDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new HistoryDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IHistoryData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

	public class HistoryDataAmbassadorCollection :
		DataAmbassadorCollection<IHistoryData>
	{
		public new HistoryDataAmbassador this[int index]
		{
			get { return (HistoryDataAmbassador)base[index]; }
		}
	}

	public class HistoryDataAmbassador :
		DataAmbassador<IHistoryData>
	{

	}
}