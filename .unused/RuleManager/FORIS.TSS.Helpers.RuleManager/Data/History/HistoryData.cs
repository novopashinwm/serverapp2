using System.ComponentModel;
using FORIS.TSS.BusinessLogic.RuleManager.Data.History;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.RuleManager.Data.History
{
	public class HistoryData :
		FORIS.TSS.Helpers.Data.Data,
		IHistoryData
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private HistoryDataDispatcher tablesDataDispatcher;

		private HistoryTable history;

		private HistoryDataAmbassador daHistory;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public HistoryData( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public HistoryData()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new HistoryDataDispatcher( this.components );

			this.history = new HistoryTable( this.components );
			this.daHistory = new HistoryDataAmbassador();
			this.daHistory.Item = this.history;
			this.tablesDataDispatcher.Ambassadors.Add( this.daHistory );
		}

		#endregion // Componane Designer generated code

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IHistoryDataSupplier DataSupplier
		{
			get { return (IHistoryDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		private string mobileName;
		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public string MobileName
		{
			get { return this.mobileName; }
			set { this.mobileName = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;
		}
		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;
		}

		#endregion // Build & Destroy

		#region Tables properties

		[Browsable( false )]
		public HistoryTable History
		{
			get { return this.history; }
		}

		#endregion // Tables properties
	}
}