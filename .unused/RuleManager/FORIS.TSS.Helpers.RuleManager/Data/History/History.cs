using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using TerminalService.Interfaces;

namespace FORIS.TSS.Helpers.RuleManager.Data.History
{
	public class HistoryTable :
		Table<HistoryRow, HistoryTable, IHistoryData>
	{
		#region Properties

		public override string Name
		{
			get { return "HISTORY"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public HistoryTable( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public HistoryTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int logTimeIndex;
		internal int LogTimeIndex { get { return this.logTimeIndex; } }

		private int validIndex;
		internal int ValidIndex { get { return this.validIndex; } }

		private int longitudeIndex;
		internal int LongitudeIndex { get { return this.longitudeIndex; } }

		private int latitudeIndex;
		internal int LatitudeIndex { get { return this.latitudeIndex; } }

		private int speedIndex;
		internal int SpeedIndex { get { return this.speedIndex; } }

		private int satellitesIndex;
		internal int SatellitesIndex { get { return this.satellitesIndex; } }

		private int propertiesIndex;
		internal int PropertiesIndex { get { return this.propertiesIndex; } }

		private int addressIndex;
        internal int AddressIndex { get { return this.addressIndex; } }

		#endregion // Fields' indexes

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.logTimeIndex		= -1;
			this.validIndex			= -1;
			this.longitudeIndex		= -1;
			this.latitudeIndex		= -1;
			this.speedIndex			= -1;
			this.satellitesIndex	= -1;
			this.propertiesIndex	= -1;
			this.addressIndex	    = -1;

			this.idCounter = 0;
		}

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.logTimeIndex		= this.DataTable.Columns.IndexOf("LOG_TIME");
				this.validIndex			= this.DataTable.Columns.IndexOf("VALID");
				this.longitudeIndex		= this.DataTable.Columns.IndexOf("LONGITUDE");
				this.latitudeIndex		= this.DataTable.Columns.IndexOf("LATITUDE");
				this.speedIndex			= this.DataTable.Columns.IndexOf("SPEED");
				this.satellitesIndex	= this.DataTable.Columns.IndexOf("SATELLITES");
				this.propertiesIndex	= this.DataTable.Columns.IndexOf("PROPERTIES");
				this.addressIndex	    = this.DataTable.Columns.IndexOf("Address");
			}
		}

		#endregion // View

		private int idCounter = 0;
		protected override HistoryRow OnCreateRow( DataRow dataRow )
		{
			return new HistoryRow( dataRow, ++this.idCounter );
		}
	}

	public class HistoryRow :
		Row<HistoryRow, HistoryTable, IHistoryData>
	{
		#region Fields

		public DateTime LogTime
		{
			get { return (DateTime)this.DataRow[this.Table.LogTimeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LogTimeIndex] = value; }
		}

		public float Longitude
		{
			get { return (float)this.DataRow[this.Table.LongitudeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LongitudeIndex] = value; }
		}

		public float Latitude
		{
			get { return (float)this.DataRow[this.Table.LatitudeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LatitudeIndex] = value; }
		}

		public float Speed
		{
			get { return (float)this.DataRow[this.Table.SpeedIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.SpeedIndex] = value; }
		}

        public byte Satellites
		{
            get { return (byte)this.DataRow[this.Table.SatellitesIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.SatellitesIndex] = value; }
		}

		public byte[] Properties
		{
			get { return (byte[])this.DataRow[this.Table.PropertiesIndex, DataRowVersion.Current]; } 
			set { this.DataRow[this.Table.PropertiesIndex] = value; }
		}

		public bool Valid
		{
			get { return (bool)this.DataRow[this.Table.ValidIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ValidIndex] = value; }
		}

		public int Course
		{
			get
			{
				HistoryRow previousPosition = this.Table.FindRow( this.id - 1 );

				if ( previousPosition != null )
				{
					return
						PositionHelper.CalcCourse( 
							previousPosition.Latitude, 
							previousPosition.Longitude, 
							this.Latitude, 
							this.Longitude );
				}

				return 0;
			}
		}

	    public string Address
	    {
            get
            {
            	return 
					this.Table.AddressIndex == -1
						? String.Empty
						: (string)this.DataRow[this.Table.AddressIndex, DataRowVersion.Current];
            }
            set { this.DataRow[this.Table.AddressIndex] = value; }
        }

		#endregion // Fields

		#region Id

		private readonly int id;
		
		protected override int GetId()
		{
			return id;
		}

		#endregion // Id

		#region Constructor & Dispose

		public HistoryRow( DataRow dataRow, int id )
			: base( dataRow )
		{
			this.id = id;
		}

		#endregion // Constructor & Dispose

		protected override void OnBuildFields()
		{

		}
	}
}
