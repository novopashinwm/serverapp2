using System;
using System.ComponentModel;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class ConnectionRuleState:
		MobileRuleState
	{
		public readonly bool TimeOut;

		public ConnectionRuleState( bool timeOut )
		{
			this.TimeOut = timeOut;
		}

		public override string ToString()
		{
			return string.Format( "TimeOut: {0}", this.TimeOut );
		}
	}

	[Serializable]
	public class ConnectionRuleSettings:
		MobileRuleSettings
	{
		private int timeOutInterval;
		[Browsable(true)]
		[Category("Settings")]
		[Description("Put description for time out setting")]
		public int TimeOutInterval
		{
			get { return this.timeOutInterval; }
			set { this.timeOutInterval = value; }
		}		

		public ConnectionRuleSettings()
		{
			this.timeOutInterval = 0;
		}
	}
}