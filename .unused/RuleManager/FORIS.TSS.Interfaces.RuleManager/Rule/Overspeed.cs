using System;
using System.ComponentModel;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class OverspeedRuleState:
		MobileRuleState
	{
		public readonly bool Overspeed;

		public OverspeedRuleState( bool overspeed )
		{
			this.Overspeed = overspeed;
		}

		public override string ToString()
		{
			return string.Format( "Overspeed: {0}", this.Overspeed );
		}
	}

	[Serializable]
	public class OverspeedRuleSettings:
		MobileRuleSettings
	{
		private int speedThreshold;
		[Browsable(true)]
		[Category("Settings")]
		[Description("Put description for speed threshold setting")]
		public int SpeedThreshold
		{
			get { return this.speedThreshold; }
			set { this.speedThreshold = value; }
		}

		public OverspeedRuleSettings()
		{
			this.speedThreshold = 60;
		}
	}
}
