using System;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class FuelRuleState:
		MobileRuleState
	{
		public readonly double Fuel;

		public FuelRuleState( double fuel )
		{
			this.Fuel = fuel;
		}

		public override string ToString()
		{
			return string.Format( "Fuel: {0}", this.Fuel );
		}
	}

	[Serializable]
	public class FuleRuleSettings:
		MobileRuleSettings
	{
		/* �������� ��� ����� �����-�� ��������� 
		 * ����������� ������� ������ �������
		 */
	}
}
