using System;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class IgnitionRuleState:
		MobileRuleState
	{
		public readonly bool Ignition;

		public IgnitionRuleState( bool ignition )
		{
			this.Ignition = ignition;
		}

		public override string ToString()
		{
			return string.Format( "Ignition: {0}", this.Ignition );
		}
	}

	[Serializable]
	public class IgnitionRuleSettings:
		MobileRuleSettings
	{
		
	}
}