using System;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class GpsSignalRuleState:
		MobileRuleState
	{
		public readonly bool Valid;

		public GpsSignalRuleState( bool valid )
		{
			this.Valid = valid;
		}

		public override string ToString()
		{
			return string.Format( "Valid: {0}", this.Valid );
		}
	}

	[Serializable]
	public class GpsSignalRuleSettings:
		MobileRuleSettings
	{
		/* ��� ���� �� ������������� ����� ��������� 
		 * ����� ���� ��� ������� ������������ ������
		 */
	}
}