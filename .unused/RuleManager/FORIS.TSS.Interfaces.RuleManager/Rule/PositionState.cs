using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.RuleManager.Rule
{
	[Serializable]
	public class PositionState: 
		MobileRuleState
	{
		/// <summary>
		/// ����� ���������
		/// </summary>
		public readonly DateTime Time;

		/// <summary>
		/// �������
		/// </summary>
		public readonly float Longitude;

		/// <summary>
		/// ������
		/// </summary>
		public readonly float Latitude;

		/// <summary>
		/// ���� (����������� ��������? ������?). �������.
		/// </summary>
		public readonly int Course;

		/// <summary>
		/// ��������, ��/�
		/// </summary>
		public readonly float Speed;

		/// <summary>
		/// ��������� ��������
		/// </summary>
		public readonly Dictionary<int, object> Sensors;

        /// <summary>
        /// ������������ �������
        /// </summary>
	    public readonly bool Valid;

        /// <summary>
        /// ����� ��������� "�������" �������
        /// </summary>
	    public readonly DateTime CorrectTime;

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="time">����� ���������</param>
		/// <param name="longitude">�������</param>
		/// <param name="latitude">������</param>
		/// <param name="course">���� (�������)</param>
		/// <param name="speed">�������� (��/�)</param>
        /// <param name="valid">������������ �������</param>
        /// <param name="correctTime">����� ��������� "�������" �������</param>
		/// <param name="sensors">�������</param>
		public PositionState( 
			DateTime time,
			float longitude,
			float latitude,
			int	course,
			float speed,
            bool valid,
            DateTime correctTime,
			Dictionary<int, object> sensors )
		{
			this.Time		= time;
			this.Longitude	= longitude;
			this.Latitude	= latitude;
			this.Course		= course;
			this.Speed		= speed;
			this.Sensors	= sensors;
		    this.Valid      = valid;
		    this.CorrectTime= correctTime;
		}

		public override string ToString()
		{
			return
				this.isEmpty
					?
						string.Format(
							"({0}; {1}) �� {2}", 
							this.Longitude, 
							this.Latitude,
							this.Time.ToLocalTime().ToString("dd:MM:yy HH:mm:ss"))
					:
						"Empty";
				
		}

		private bool isEmpty = false;
		public bool IsEmpty
		{
			get { return this.isEmpty; }
		}

		public static PositionState Empty
		{
			get
			{
				PositionState empty = new PositionState( DateTime.Now, 0, 0, 0, 0, false, DateTime.Now, null );
				empty.isEmpty = true;
				return empty;
			}
		}
	}
}