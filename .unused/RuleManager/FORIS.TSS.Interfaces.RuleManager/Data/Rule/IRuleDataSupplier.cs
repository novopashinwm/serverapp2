using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.BusinessLogic.RuleManager.Data.Rule
{
	public interface IRuleDataSupplier:
		IDataSupplier<IRuleDataTreater>,
        ISecureDataSupplier<IRuleSecurityDataSupplier, IOperatorDataSupplier>
	{
		IControllerDataSupplier ControllerDataSupplier { get; }

		MobileRuleSettings CreateMobileRuleSettings( int ruleId );
	}


    public interface IRuleSecurityDataSupplier :
        IDataSupplier<IRuleSecurityDataTreater>,
        ISecurityDataSupplier<IOperatorDataSupplier>
    {

    }
}