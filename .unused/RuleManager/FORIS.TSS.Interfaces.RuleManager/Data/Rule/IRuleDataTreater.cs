using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.BusinessLogic.RuleManager.Data.Rule
{
	public interface IRuleDataTreater:
		IDataTreater
	{
		#region Rule

		void RuleInsert(
			string name,
			string description,
			string code
			);

		void RuleUpdate(
			int ruleId,
			string name,
			string description,
			string code
			);

		void RuleDelete(
			int ruleId
			);

		#endregion // Rule

		#region Mobile

		void MobileInsert(
			int mobileType,
			string name,
			int? controller,
			int globalId
			);

		void MobileUpdate(
			int mobileId,
			int mobileType,
			string name,
			int? controller,
			int globalId
			);

		void MobileDelete(
			int mobileId
			);

		#endregion // Mobile

		#region MobileRule

		void MobileRuleInsert(
			int mobile,
			int rule,
			MobileRuleSettings settings
			);

		void MobileRuleUpdate(
			int mobileRuleId,
			int mobile,
			int rule,
			MobileRuleSettings settings
			);

		void MobileRuleDelete(
			int mobileRuleId
			);

		#endregion // MobileRule

		int MobileObjectInsert(
			int mobileObjectType,
			string name
			);

		void SetState(
			int mobileRuleId,
			MobileRuleState state
			);
	}

    public interface IRuleSecurityDataTreater : IDataTreater
    {
        
    }
}