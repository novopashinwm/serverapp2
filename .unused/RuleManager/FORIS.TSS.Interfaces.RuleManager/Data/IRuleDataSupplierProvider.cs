using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data;

namespace FORIS.TSS.BusinessLogic.RuleManager.Data
{
	public interface IRuleDataSupplierProvider:
		IControllerDataSupplierProvider
	{
		List<int> CheckedMobiles { get; set; }

		IRuleDataSupplier GetRuleDataSupplier();

		/// <summary>
		/// ���������� ��������������� �� ������� ������ ��������� ������� �� �� �� �� ��������� ������ �������
		/// </summary>
		/// <param name="beginTime">������ �������. UTC</param>
		/// <param name="endTime">����� �������. UTC</param>
		/// <param name="controllerId">������������� �����������</param>
		/// <param name="interval">�������� ����� ���������. �������</param>
		/// <param name="maxCount">������������ ���������� ������������ �������</param>
		/// <returns>��������������� �� ������� ������ ��������� �������</returns>
		HistoryDataSet GetLogFromDB(
			DateTime beginTime, DateTime endTime,
			int controllerId,
			int interval, int maxCount
			);
	}
}