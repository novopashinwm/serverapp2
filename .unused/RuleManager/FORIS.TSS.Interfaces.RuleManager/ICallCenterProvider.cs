﻿namespace FORIS.TSS.BusinessLogic.RuleManager
{
	public interface ICallCenterProvider
	{
		/// <summary>
		/// Голосовой вызов абоненту
		/// </summary>
		/// <param name="phoneNum">Телефонный номер</param>
		/// <param name="comment">Коментарий, отображаемы в окне, 
		/// при наборе номера</param>
		void Call( string phoneNum, string comment );
	}
}