using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.ServerApplication.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	public partial class RuleDataTreater
	{
		internal void ProcessPositionMessages(
			List<IPositionMessage> messages,
			Dictionary<int, List<IPositionMessageProcessor>> PositionMessageProcessors
			)
		{
			lock( this.Data )
			{
				try
				{
					foreach (IPositionMessage message in messages)
					{
						/* ���� � ����� ������ ������������ ��� ����������� 
						 * � ����� �������, �� ��� �� ��� ����� � �� ��� 
						 * ������������ �� �����, � ������ �������
						 */
						if ( this.Data.Controller.Rows.Contains( message.DeviceInfo.ControllerID ) && message.GpsData.CorrectGPS )
						{
							ControllerRow controllerRow =
								this.Data.Controller.Rows[message.DeviceInfo.ControllerID];
							
							#region Extract position from controller's data

							if ( this.Data.Mobile.RowByController.ContainsKey( controllerRow.ID ) )
							{
								MobileRow mobileRow = this.Data.Mobile.RowByController[controllerRow.ID];

								PositionRow positionRow = mobileRow.RowPosition;

								int course = 0;

								#region Calculate course relatively to previous position

								/* ���� ���������� ������� ���, �� ���������
									 * ������� ����� � ������ 0. ������ ��� ������ 
									 * ����� ������ ������� ������� ��������
									 */

								if (!positionRow.Empty)
								{
									course =
										message.CalcCourse(
											(double)positionRow.Latitude,
											(double)positionRow.Longitude
											);
								}

								#endregion /// Calculate course relatively to previous position

								positionRow.BeginEdit();

								positionRow.Time = TimeHelper.year1970.AddSeconds(message.GpsData.Time);
								positionRow.Longitude = message.GpsData.Longitude;
								positionRow.Latitude = message.GpsData.Latitude;
								positionRow.Course = course;

								positionRow.EndEdit();
							}
							else
							{
								Trace.WriteLineIf(
									RuleManager.TraceSwitch.TraceWarning,
									string.Format(
										"There come data from controller number {0} without mobile",
										message.DeviceInfo.DeviceNumber
										),
									this.GetType().Name
									);
							}

							#endregion // Extract positions from controller's data

							#region Processing controller's data through PositionMessage processors

							List<IPositionMessageProcessor> processorList = null;

							lock( PositionMessageProcessors )
							{
								if( PositionMessageProcessors.ContainsKey( controllerRow.ID ) )
								{
									processorList = PositionMessageProcessors[controllerRow.ID];
								}
							}

							if( processorList != null )
							{
								lock( processorList )
								{
									foreach( IPositionMessageProcessor rule in processorList )
									{
										rule.Process( message );
									}
								}
							}

							#endregion // Processing controller's data through PositionMessage processors
						}
						else
						{
							#region Unknown controller's data

							Trace.WriteLineIf(
								RuleManager.TraceSwitch.TraceWarning,
								string.Format(
									"There come data from unknown controller number {0}",
									message.DeviceInfo.DeviceNumber
									),
								this.GetType().Name
								);

							#endregion // Unknown controllers' data
						}
					}

					this.AcceptChanges();
				}
				catch( Exception )
				{
					/* �����: �������� �������� ����������.
					 * ���� ��������� ������ � ��������� ���
					 * ���������� ������ ������ ������ �����������,
					 * �� ��������� �� ���� ������� � ������� 
					 * ������� �� ����� ���������
					 */

					this.RejectChanges();

					throw;
				}
			}
		}
	}
}