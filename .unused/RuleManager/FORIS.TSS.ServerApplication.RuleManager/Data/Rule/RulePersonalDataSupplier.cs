using System.Collections.Generic;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.ServerApplication.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
    public class RulePersonalDataSupplier :
        PersonalDataSupplier<RuleServerData, IRuleDataTreater>,
        IRuleDataSupplier
    {
        public RulePersonalDataSupplier(
            RuleServerData dataSupplier,
            IPersonalServer personalServer
            )
            : base(dataSupplier, personalServer)
        {

        }

        public override IRuleDataTreater GetDataTreater()
        {
            return
                this.DataSupplier.GetTreater(this.PersonalServer.SessionInfo);
        }

        IControllerDataSupplier IRuleDataSupplier.ControllerDataSupplier
        {
            get { return this.DataSupplier.ControllerDataSupplier; }
        }

        MobileRuleSettings IRuleDataSupplier.CreateMobileRuleSettings(int ruleId)
        {
            return this.DataSupplier.CreateMobileRuleSettings(ruleId);
        }

        protected override void dataSupplier_DataChanged( DataChangedEventArgs e )
        {
        	DataChangedEventArgs dataChangedEventArgs = e;
        	List<int> checkedMobiles =
        		( (IRuleDataSupplierProvider)this.PersonalServer ).CheckedMobiles;

        	#region Position

        	if ( dataChangedEventArgs.Modified.Tables["POSITION"] != null )
        	{
        		List<DataRow> victims = new List<DataRow>();

        		foreach ( DataRow dataRow in dataChangedEventArgs.Modified.Tables["POSITION"].Rows )
        		{
        			if ( !checkedMobiles.Contains( (int)dataRow["MOBILE_ID"] ) )
        			{
        				victims.Add( dataRow );
        			}
        		}

        		//if ( dataChangedEventArgs.Modified.Tables["POSITION"].Rows.Count == victims.Count )
        		//{
        		//    dataChangedEventArgs.Modified.Tables.Remove( "POSITION" );
        		//}
        		//else
        		//{
        		foreach ( DataRow dataRow in victims )
        		{
        			dataChangedEventArgs.Modified.Tables["POSITION"].Rows.Remove( dataRow );
        		}
        		//}
        	}

        	#endregion  // Position

        	#region State

        	if ( dataChangedEventArgs.Modified.Tables["STATE"] != null )
        	{
        		List<DataRow> victims = new List<DataRow>();

        		foreach ( DataRow dataRow in dataChangedEventArgs.Modified.Tables["STATE"].Rows )
        		{
        			MobileRuleRow mobileRuleRow =
        				this.DataSupplier.MobileRule.FindRow( (int)dataRow["MOBILE_RULE_ID"] );

        			if ( !checkedMobiles.Contains( mobileRuleRow.Mobile ) )
        			{
        				victims.Add( dataRow );
        			}
        		}

        		//if ( dataChangedEventArgs.Modified.Tables["STATE"].Rows.Count == victims.Count )
        		//{
        		//    dataChangedEventArgs.Modified.Tables.Remove( "STATE" );
        		//}
        		//else
        		//{
        		foreach ( DataRow dataRow in victims )
        		{
        			dataChangedEventArgs.Modified.Tables["STATE"].Rows.Remove( dataRow );
        		}
        		//}
        	}

        	#endregion  // State

        	base.dataSupplier_DataChanged( dataChangedEventArgs );
        }

    	#region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.DataSupplier.SecurityDataSupplier; }
        }

        #endregion
    }
}