using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
    public class RuleSecurityDatabaseDataSupplier:
        DatabaseDataSupplier<IRuleServerDataProvider>,
        IRuleSecurityDataSupplier
    {
        public RuleSecurityDatabaseDataSupplier(IRuleServerDataProvider server)
            : base(server)
        {
			
        }

        protected override DataInfo GetData()
        {
            DataSet data =
                this.DatabaseProvider.Database.GetDataFromDB(
                    new ParamValue[] { },
                    "dbo.GetContainerRuleSecurity",
                    new string[]
                        {
                            "R_MOBILE_OPERATORGROUP",
                            "R_MOBILE_OPERATOR"
                        }
                    );

            return new DataInfo( data, Guid.NewGuid() );
        }

        IOperatorDataSupplier ISecurityDataSupplier<IOperatorDataSupplier>.PrincipalDataSupplier
        {
            get { return this.DatabaseProvider.PrincipalData; }
        }

        public IRuleSecurityDataTreater GetDataTreater()
        {
            throw new NotImplementedException();
        }
    }
}