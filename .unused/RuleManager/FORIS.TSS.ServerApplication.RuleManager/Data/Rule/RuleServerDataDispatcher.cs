using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	public class RuleServerDataDispatcher:
		DataDispatcher<IRuleServerData>
	{
		#region Constructor

		public RuleServerDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public RuleServerDataDispatcher()
		{

		}

		#endregion // Constructor

		private readonly RuleServerDataAmbassadorCollection ambassadors =
			new RuleServerDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new RuleServerDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRuleServerData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class RuleServerDataAmbassadorCollection:
		DataAmbassadorCollection<IRuleServerData>
	{
		public new RuleServerDataAmbassador this[int index]
		{
			get { return (RuleServerDataAmbassador)base[index]; }
		}
	}

	public class RuleServerDataAmbassador:
		DataAmbassador<IRuleServerData>
	{

	}
}