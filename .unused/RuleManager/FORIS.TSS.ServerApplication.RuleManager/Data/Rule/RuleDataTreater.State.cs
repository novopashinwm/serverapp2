using System;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	partial class RuleDataTreater
	{
		/// <summary>
		/// ����� ��������� ��������� �������
		/// </summary>
		/// <param name="mobileRuleId">ID ������ MobileRule ��������������� ������� �������</param>
		/// <param name="state">����� ��������� �������</param>
		/// <remarks>
		/// ����� ����� ������ �������� � ����, ��� �� ������� ��������������� ������ 
		/// ������� �������� � ������� �� ��������� (��� ���������� � RuleManager).
		/// </remarks>
		public void SetState(
			int mobileRuleId,
			MobileRuleState state
			)
		{
			this.Data.SetState(mobileRuleId, state);
		}

		/// <summary>
		/// ��������� ��������� ������
		/// </summary>
		/// <param name="stateChanges">������ ��� �������� ������ � �� ����� ���������</param>
		/// <remarks>
		/// ���� ����� ���������� �� RuleManager ����� �������� ������� ������� ������� ������� 
		/// (MobileRule) �������� ���� ��������� (�������������� ���� ������ ��� ��������� � 
		/// ������� ������� SetState, ������������ ����).
		/// </remarks>
		internal void ProcessStateChanges( MobileRuleStateEventArgs[] stateChanges )
		{
			lock ( this.Data )
			{
				try
				{
					foreach ( MobileRuleStateEventArgs e in stateChanges )
					{
						e.MobileRuleObject.ApplyChange( this.Data, e.MobileRuleState );
					}

					this.AcceptChanges();
				}
				catch ( Exception ex)
				{
                    Trace.WriteLine(DateTime.Now+" " + this.GetType()+" "+ ex.Message);
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}