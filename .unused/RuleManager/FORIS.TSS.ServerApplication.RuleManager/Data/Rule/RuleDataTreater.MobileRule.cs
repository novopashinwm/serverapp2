using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	public partial class RuleDataTreater
	{
		public void MobileRuleInsert(
			int mobile,
			int rule,
			MobileRuleSettings settings
			)
		{
			lock( this.Data )
			{
				MobileRuleRow mobileRuleRow;

				try
				{
					mobileRuleRow = this.Data.MobileRule.NewRow();

					mobileRuleRow.Mobile = mobile;
					mobileRuleRow.Rule = rule;
					mobileRuleRow.Settings = settings;

					this.Data.MobileRule.Rows.Add( mobileRuleRow );

					//try
					//{
						StateRow stateRow = this.Data.State.NewRow();

						stateRow.MobileRule = mobileRuleRow.ID;
						stateRow.State = this.Data.GetDefaultRuleStateValue( rule );

						this.Data.State.Rows.Add( stateRow );
					//}
					//catch( Exception )
					//{
					//    this.RejectChanges();

					//    throw;
					//}
					
					this.AcceptChanges();
				}
				catch( Exception ex)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void MobileRuleUpdate(
			int mobileRuleId,
			int mobile,
			int rule,
			MobileRuleSettings settings
			)
		{
			lock( this.Data )
			{
				try
				{
					// throw new NotSupportedException("Remove Rule to Mobile binding and create new ");

					MobileRuleRow row = this.Data.MobileRule.FindRow(mobileRuleId);

					row.BeginEdit();

					row.Mobile = mobile;
					row.Rule = rule;
					row.Settings = settings;

					row.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void MobileRuleDelete(
			int mobileRuleId
			)
		{
			lock( this.Data )
			{
				try
				{
					MobileRuleRow mobileRuleRow = this.Data.MobileRule.FindRow( mobileRuleId );
					StateRow stateRow = mobileRuleRow.RowState;

					if( stateRow != null )
					{
						stateRow.Delete();
					}

					mobileRuleRow.Delete();

					this.AcceptChanges( );
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}
