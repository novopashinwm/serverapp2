using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	partial class RuleDataTreater
	{
		public void RuleInsert(
			string name,
			string description,
			string code
			)
		{
			lock( this.Data )
			{
				try
				{
					RuleRow row = this.Data.Rule.NewRow();

					row.Name = name;
					row.Description = description;
					row.Code = code;

					this.Data.Rule.Rows.Add( row );

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RuleUpdate(
			int ruleId,
			string name,
			string description,
			string code
			)
		{
			lock( this.Data )
			{
				try
				{
					RuleRow row = this.Data.Rule.FindRow( ruleId );

					row.BeginEdit();

					row.Name = name;
					row.Description = description;
					row.Code = code;

					row.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RuleDelete(
			int ruleId
			)
		{
			lock( this.Data )
			{
				try
				{
					RuleRow row =
						this.Data.Rule.FindRow( ruleId );

					row.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}
