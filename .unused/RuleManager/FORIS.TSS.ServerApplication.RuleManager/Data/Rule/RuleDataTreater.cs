using System;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	public partial class RuleDataTreater:
		DataTreater<RuleServerData>,
		IRuleDataTreater
	{
		public RuleDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			RuleServerData data,
			ISessionInfo sessionInfo
			)
			:base( databaseDataSupplier, data, sessionInfo)
		{
		}

		public int MobileObjectInsert(
			int mobileObjectType,
			string name
			)
		{
			int result;

			lock ( this.Data )
			{
				try
				{
					MobileObjectRegisterRow row = this.Data.MobileObjectRegister.NewRow();

					row.MobileObjectType = mobileObjectType;
					row.Name = name;

					this.Data.MobileObjectRegister.Rows.Add( row );

					this.AcceptChanges();

					result = row.ID;
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}

			return result;
		}
	}
}
