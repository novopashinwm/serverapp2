using System;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	public partial class RuleDataTreater
	{
		public void MobileInsert(
			int mobileType,
			string name,
			int? controller,
			int vehicle
			)
		{
			lock( this.Data )
			{
				MobileRow mobileRow;

				try
				{
					mobileRow = this.Data.Mobile.NewRow();

					mobileRow.MobileType	= mobileType;
					mobileRow.Name			= name;
					mobileRow.Controller	= controller;
					mobileRow.GlobalId		= vehicle;

					this.Data.Mobile.Rows.Add( mobileRow );

					this.AcceptChanges();
				}

				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}

				try
				{
					PositionRow positionRow = this.Data.Position.NewRow();

					positionRow.Mobile = mobileRow.ID;

					this.Data.Position.Rows.Add( positionRow );

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void MobileUpdate(
			int mobileId,
			int mobileType,
			string name,
			int? controller,
			int vehicle
			)
		{
			lock( this.Data)
			{
				try
				{
					MobileRow mobileRow = this.Data.Mobile.FindRow( mobileId );

					if( mobileRow.Controller != controller )
					{
						PositionRow rowPosition = mobileRow.RowPosition;

						rowPosition.BeginEdit();
						rowPosition.Clear();
						rowPosition.EndEdit();
					}

					mobileRow.BeginEdit();

					mobileRow.MobileType	= mobileType;
					mobileRow.Name			= name;
					mobileRow.Controller	= controller;
					mobileRow.GlobalId		= vehicle;

					mobileRow.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
		
		public void MobileDelete(
			int mobileId
			)
		{
			lock (this.Data)
			{
				try
				{
					MobileRow mobileRow = this.Data.Mobile.FindRow( mobileId );
					PositionRow positionRow = mobileRow.RowPosition;

					if( positionRow != null )
					{
						positionRow.Delete();
					}

					mobileRow.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}
