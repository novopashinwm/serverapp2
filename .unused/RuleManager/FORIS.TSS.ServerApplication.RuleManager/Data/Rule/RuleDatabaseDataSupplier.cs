using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.ServerApplication.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	internal class RuleDatabaseDataSupplier:
		DatabaseDataSupplier<IRuleServerDataProvider>,
		IRuleDataSupplier
	{
		public RuleDatabaseDataSupplier( IRuleServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{
            this.ruleSecurityDatabaseDataSupplier =
                new RuleSecurityDatabaseDataSupplier(databaseServerProvider);
		}

		protected override DataInfo GetData()
		{
			DataSet data = 
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerRule",
					new string[]
						{
							"MOBILE_OBJECT_REGISTER",
							"MOBILE_OBJECT_TYPE",
							"RULE", 
							"MOBILE_TYPE", 
							"MOBILE", 
							"POSITION", 
							"MOBILE_RULE", 
							"STATE"
		}
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		#region IDataSupplier<IRuleDataTreater> Members

		public IRuleDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}

		#endregion // IDataSupplier<IRuleDataTreater> Members

		#region IRuleDataSupplier Members

		public MobileRuleSettings CreateMobileRuleSettings(int ruleId)
		{
			throw new NotSupportedException();
		}

		#endregion // IRuleDataSupplier Members

		#region IRuleDataSupplier Members

		public IControllerDataSupplier ControllerDataSupplier
		{
			get { return this.DatabaseProvider.GetControllerDataSupplier(); }
		}

		#endregion // IRuleDataSupplier Members

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        private readonly RuleSecurityDatabaseDataSupplier ruleSecurityDatabaseDataSupplier;
        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.ruleSecurityDatabaseDataSupplier; }
        }

        #endregion
    }
}