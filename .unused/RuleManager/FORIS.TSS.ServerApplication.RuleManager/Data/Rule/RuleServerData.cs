using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager.Data.Rule
{
	/// <summary>
	/// ��������� ������ ��� ������ "����������� �������"
	/// </summary>
	/// <remarks>
	/// ������ ����� �������� ������ ���������,
	/// ��� ������������� - ����������� ������,
	/// ���������� �������� � �.�. � �.�.
	/// </remarks>
	public class RuleServerData:
		RuleData,
		IRuleServerData
	{
		#region Constructor & Dispose

		public RuleServerData( IRuleServerDataProvider serverDataProvider )
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new RuleDatabaseDataSupplier( serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.mobileRuleStateChanged = null;

				RemotingServices.Disconnect( this );
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		private readonly IRuleServerDataProvider serverDataProvider;

		public IRuleDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new RuleDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		public override IRuleDataTreater GetDataTreater()
		{
			return
				new RuleDataTreater(
					this.serverDataProvider.Database,
					this,
					null
					);
		}

		private readonly Dictionary<int, Type> ruleTypes = new Dictionary<int, Type>();

		protected override void Destroy()
		{
			this.ruleTypes.Clear();

			this.Rule.Rows.Removing -= this.RuleRows_Removing;
			this.Rule.Rows.Inserted -= this.RuleRows_Inserted;

			base.Destroy();
		}
		protected override void Build()
		{
			base.Build();

			foreach( RuleRow ruleRow in this.Rule.Rows )
			{
				this.RuleShow( ruleRow );
			}

			this.Rule.Rows.Removing += this.RuleRows_Removing;
			this.Rule.Rows.Inserted += this.RuleRows_Inserted;
		}

		#region Data events

		private void RuleRows_Inserted( object sender, CollectionChangeEventArgs<RuleRow> e )
		{
			RuleRow ruleRow = e.Item;

			this.RuleShow( ruleRow );
		}

		private void RuleRows_Removing( object sender, CollectionChangeEventArgs<RuleRow> e )
		{
			RuleRow ruleRow = e.Item;

			this.RuleHide( ruleRow );
		}

		#endregion // Data events

		#region Rule Show & Hide

		private void RuleHide( RuleRow ruleRow )
		{
			this.ruleTypes.Remove( ruleRow.ID );
		}
		private void RuleShow( RuleRow ruleRow )
		{
			this.ruleTypes.Add(
				ruleRow.ID,
				RuleServerData.CreateMobileRuleType( ruleRow )
				);
		}

		#endregion // Rule Show & Hide

		public override MobileRuleSettings CreateMobileRuleSettings(int ruleId)
		{
			MobileRuleSettings result;

			try
			{
				Type ruleType = this.ruleTypes[ruleId];

				PropertyInfo ruleSettingsPropertyInfo =
					ruleType.GetProperty(
						"DefaultSettings",
						BindingFlags.Public | BindingFlags.Static
						);

				#region Conditions

				if (ruleSettingsPropertyInfo == null)
				{
					throw new ApplicationException("Rule type must have public static 'DefaultSettings' property");
				}

				if (
					ruleSettingsPropertyInfo.PropertyType != typeof(MobileRuleSettings) &&
					!ruleSettingsPropertyInfo.PropertyType.IsSubclassOf(typeof(MobileRuleSettings))
					)
				{
					throw new ApplicationException("Settings property type must be derived from 'RuleSettings'");
				}

				#endregion Conditions

				result = (MobileRuleSettings)ruleSettingsPropertyInfo.GetValue(null, null);
			}
			catch (Exception ex)
			{
				result = null;

				Trace.WriteLineIf(
					RuleManager.TraceSwitch.TraceInfo,
					string.Format(
						"Error get rule settings object for rule {0}: {1} ",
						ruleId,
						ex.Message
						)
					);
			}

			return result;
		}

		public MobileRuleState GetDefaultRuleStateValue( int ruleId )
		{
			MobileRuleState result;

			try
			{
				Type ruleType = this.ruleTypes[ruleId];

				PropertyInfo defaultStatePropertyInfo =
					ruleType.GetProperty(
						"DefaultState",
						BindingFlags.Public | BindingFlags.Static
						);

				#region Conditions

				if (defaultStatePropertyInfo == null)
				{
					throw new ApplicationException( "Rule type must have public static 'DefaultState' property" );
				}

				if (!defaultStatePropertyInfo.PropertyType.IsSubclassOf( typeof(MobileRuleState) ))
				{
					throw new ApplicationException( "DefaultState property type must be derived from 'MobileRuleState'" );
				}

				#endregion Conditions

				result = (MobileRuleState)defaultStatePropertyInfo.GetValue( null, null );
			}
			catch (Exception ex)
			{
				result = null;

				Trace.WriteLineIf(
					RuleManager.TraceSwitch.TraceInfo,
					string.Format(
						"Error get default state value for rule {0}: {1} ",
						ruleId,
						ex.Message
						)
					);
			}

			return result;
		}

		private static Type CreateMobileRuleType( RuleRow ruleRow )
		{
			Type result;

			try
			{
				result = Type.GetType( ruleRow.Code, true );
			}
			catch( Exception ex )
			{
				result = null;

				throw new MobileRuleInvalidCodeException(ruleRow.Code, ex);
			}

			return result;
		}


		private MobileRuleStateEventHandler mobileRuleStateChanged;
		/// <summary>
		/// ��� ������� ���������� ��� ������� ��������� ��������� ������� �� �������.
		/// �� ��� ������� ������������� RuleManager ��� ���� ����� ������� ��������������� 
		/// ������ ������� � ������� �� ��������� ���������.
		/// </summary>
		public event MobileRuleStateEventHandler MobileRuleStateChanged
		{
			add { this.mobileRuleStateChanged += value; }
			remove { this.mobileRuleStateChanged -= value; }
		}

		/// <summary>
		/// ���� ����� ���������� �� RuleDataTreater � ������ raises StateChanged event.
		/// </summary>
		/// <param name="mobileRuleId"></param>
		/// <param name="state"></param>
		internal void SetState( int mobileRuleId, MobileRuleState state )
		{
			MobileRuleRow mobileRuleRow = this.MobileRule.FindRow( mobileRuleId );

			if ( mobileRuleRow != null && this.mobileRuleStateChanged != null )
			{
				this.mobileRuleStateChanged( new MobileRuleStateEventArgs(
					mobileRuleId,
					null,
					state));
			}
		}
	}

	public interface IRuleServerData:
		IRuleData
	{
		event MobileRuleStateEventHandler MobileRuleStateChanged;
	}

	[Serializable]
	sealed public class MobileRuleInvalidCodeException : Exception
	{
		public MobileRuleInvalidCodeException() : base() { }
		public MobileRuleInvalidCodeException(string message) : base(message) { }
		public MobileRuleInvalidCodeException(string message, Exception innerException) 
			: base(message, innerException) { }

		public override string Message
		{
			get
			{
				return String.Format("'{0}' - is not valid rule code", base.Message);
			}
		}
	}
}
