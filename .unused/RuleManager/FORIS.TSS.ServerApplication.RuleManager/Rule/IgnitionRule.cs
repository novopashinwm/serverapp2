using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	class IgnitionRule :
		MobileRuleBase<IgnitionRuleState, IgnitionRuleSettings>,
		IPositionMessageProcessor
	{
		public IgnitionRule(IRuleManager ruleManager, int mobileRuleId)
			:
			base(ruleManager, mobileRuleId)
		{

		}

		#region Default

		private static readonly IgnitionRuleState s_defaultState =
			new IgnitionRuleState(false);
		public static IgnitionRuleState DefaultState
		{
			get { return IgnitionRule.s_defaultState; }
		}

		private static readonly IgnitionRuleSettings s_defaultSettings =
			new IgnitionRuleSettings();
		public static IgnitionRuleSettings DefaultSettings
		{
			get { return IgnitionRule.s_defaultSettings; }
		}

		#endregion // Default

		#region IMobileUnitProcessor Members

		public void Process(IPositionMessage mobileUnit)
		{
			/* ��� ���������� �������� ������� � ������� ��������,
			 * ������ ��� �������� ��������� ���� ������ ���� �������
			 * ����������� � ��� �� ��������
			 */

			// TODO: �������� ��� ��������� ������ ��������
			/* ���������� �������� ������� � ������ �����������
			 * �������� ����� �������� �����������
			 * this.mobileRuleRow.RowMobile.RowController...
			 */
			IgnitionRuleState newState = new IgnitionRuleState((bool)mobileUnit.Sensors.Sensors[1]);

			if ( !object.Equals( this.state, newState ) )
			{
				this.OnStateChanged( newState );
			}
		}

		#endregion // IMobileUnitProcessor

		public override void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			IgnitionRuleState newState = (IgnitionRuleState) mobileRuleState;
			StateRow stateRow = data.State.FindRow( this.rowMobileRule.RowState.ID );

			stateRow.BeginEdit();
			stateRow.State = newState;
			stateRow.EndEdit();
		}
	}
}