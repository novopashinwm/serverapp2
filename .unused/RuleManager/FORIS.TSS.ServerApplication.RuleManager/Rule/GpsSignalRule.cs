using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	/// <summary>
	/// ������� ����������� ��� ��������� �� 
	/// �� ������ � ������������� ������� GPS.
	/// ����� ��� ��������� ������ � 
	/// ����������� ������� GPS.
	/// </summary>
	class GpsSignalRule:
		MobileRuleBase<GpsSignalRuleState, GpsSignalRuleSettings>,
		IPositionMessageProcessor
	{
		public GpsSignalRule( IRuleManager ruleManager, int mobileRuleId ):
			base( ruleManager, mobileRuleId )
		{

		}

		#region Default

		private static readonly GpsSignalRuleState s_defaultState =
			new GpsSignalRuleState( true );
		public static GpsSignalRuleState DefaultState
		{
			get { return GpsSignalRule.s_defaultState; }
		}

		private static readonly GpsSignalRuleSettings s_defaultSettings =
			new GpsSignalRuleSettings();
		public static GpsSignalRuleSettings DefaultSettings
		{
			get { return GpsSignalRule.s_defaultSettings; }
		}

		#endregion // Default 

		#region IMobileUnitProcessor Members

		public void Process( IPositionMessage mobileUnit )
		{
			GpsSignalRuleState newState = new GpsSignalRuleState( mobileUnit.GpsData.CorrectGPS );

			if( !object.Equals( this.state, newState ) )
			{
				this.OnStateChanged( newState );
			}
		}

		#endregion // IMobileUnitProcessor Members

		public override void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			GpsSignalRuleState newState = (GpsSignalRuleState)mobileRuleState;
			StateRow stateRow = data.State.FindRow( this.rowMobileRule.RowState.ID );

			stateRow.BeginEdit();
			stateRow.State = newState;
			stateRow.EndEdit();
		}
	}
}
