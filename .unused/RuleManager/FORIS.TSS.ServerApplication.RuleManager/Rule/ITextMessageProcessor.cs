using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	public interface ITextMessageProcessor
	{
		void Process( ITextMessage textMessage );
	}
}