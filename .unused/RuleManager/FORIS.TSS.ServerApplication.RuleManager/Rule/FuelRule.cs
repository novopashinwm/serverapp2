using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	class FuelRule:
		MobileRuleBase<FuelRuleState, FuleRuleSettings>,
		IPositionMessageProcessor
	{
		public  FuelRule( IRuleManager ruleManager, int mobileRuleId ):
			base( ruleManager, mobileRuleId )
		{

		}

		#region Default

		private static readonly FuelRuleState s_defaultState = 
			new FuelRuleState( 0 );
		public static FuelRuleState DefaultState
		{
			get { return FuelRule.s_defaultState; }
		}

		private static readonly FuleRuleSettings s_defaultSettings =
			new FuleRuleSettings();
		public static FuleRuleSettings DefaultSettings
		{
			get { return FuelRule.s_defaultSettings; }
		}

		#endregion // Default

		#region IMobileUnitProcessor Members

		public void Process( IPositionMessage mobileUnit )
		{
			/* ��� ���������� �������� ������� � ������� ��������,
			 * ������ ��� �������� ��������� ���� ������ ���� �������
			 * ����������� � ��� �� ��������
			 */

			// TODO: �������� ��� ��������� ������ ��������
			/* ���������� �������� ������� � ������ �����������
			 * �������� ����� �������� �����������
			 * this.mobileRuleRow.RowMobile.RowController...
			 */
			if ( mobileUnit.Sensors.Sensors.ContainsKey( 0 ) )
			{
				FuelRuleState newState = new FuelRuleState( (double)mobileUnit.Sensors.Sensors[0] );

				if ( !object.Equals( this.state, newState ) )
				{
					this.OnStateChanged( newState );
				}
			}
		}

		#endregion // IMobileUnitProcessor

		public override void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			FuelRuleState newState = (FuelRuleState)mobileRuleState;
			StateRow stateRow = data.State.FindRow( this.rowMobileRule.RowState.ID );

			stateRow.BeginEdit();
			stateRow.State = newState;
			stateRow.EndEdit();
		}
	}
}