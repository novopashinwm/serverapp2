using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	public interface IPositionMessageProcessor
	{
		void Process(IPositionMessage positionMessage);
	}
}