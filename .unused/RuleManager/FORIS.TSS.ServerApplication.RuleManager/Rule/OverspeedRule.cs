using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	class OverspeedRule :
		MobileRuleBase<OverspeedRuleState, OverspeedRuleSettings>,
		IPositionMessageProcessor
	{
		public OverspeedRule(IRuleManager ruleManager, int mobileRuleId)
			:
			base(ruleManager, mobileRuleId)
		{

		}

		#region Default

		private static readonly OverspeedRuleState s_defaultState =
			new OverspeedRuleState( false );
		public static OverspeedRuleState DefaultState
		{
			get { return OverspeedRule.s_defaultState; }
		}

		private static readonly OverspeedRuleSettings s_defaultSettings =
			new OverspeedRuleSettings();
		public static OverspeedRuleSettings DefaultSettings
		{
			get { return OverspeedRule.s_defaultSettings; }
		}

		#endregion // Default

		#region View

		#endregion // View

		#region IMobileUnitProcessor Members

		public void Process(IPositionMessage mobileUnit)
		{
			/* ��� ���������� �������� ������� � ������� ��������,
			 * ������ ��� �������� ��������� ���� ������ ���� �������
			 * ����������� � ��� �� ��������
			 */

			// TODO: �������� ��� ��������� ������ ��������
			/* ���������� �������� ������� � ������ �����������
			 * �������� ����� �������� �����������
			 * this.mobileRuleRow.RowMobile.RowController...
			 */
			OverspeedRuleState newState =
				new OverspeedRuleState( (int)mobileUnit.Sensors.Sensors[3] > this.settings.SpeedThreshold );

			if (!object.Equals(this.state, newState))
			{
				this.OnStateChanged(newState);
			}
		}

		#endregion // IMobileUnitProcessor

		public override void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			OverspeedRuleState newState = (OverspeedRuleState) mobileRuleState;
			StateRow stateRow = data.State.FindRow( this.rowMobileRule.RowState.ID );

			stateRow.BeginEdit();
			stateRow.State = newState;
			stateRow.EndEdit();
		}
	}
}