using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	public abstract class MobileRuleBase<TState, TSettings> :
		Component,
		IMobileRule,
		IDataItem<IRuleServerData>
		where TState : MobileRuleState
		where TSettings : MobileRuleSettings
	{
		#region Constructor & Dispose

		public MobileRuleBase( IRuleManager ruleManager, int mobileRuleId )
		{
			this.ruleManager = ruleManager;
			this.mobileRuleId = mobileRuleId;
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				this.stateChanged = null;
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		private readonly IRuleManager ruleManager;
		private readonly int mobileRuleId;

		#region IDataItem<IRuleServerData> Members

		private IRuleServerData data;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IRuleServerData Data
		{
			get { return this.data; }
			set
			{
				if ( this.data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.data = value;

				if ( this.data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IRuleServerData> Members

		#region IMobileRule Members

		MobileRuleState IMobileRule.State
		{
			get { return this.state; }
		}

		MobileRuleSettings IMobileRule.Settings
		{
			get { return this.settings; }
		}

		event MobileRuleStateEventHandler IMobileRule.StateChanged
		{
			add { this.stateChanged += value; }
			remove { this.stateChanged -= value; }
		}

		public abstract void ApplyChange( IRuleData data, MobileRuleState state );

		#endregion  // IMobileRule Members

		#region Data

		protected virtual void OnBeforeSetData()
		{
			this.rowMobileRule.AfterStateChanged -= this.rowMobileRule_AfterStateChanged;
		}
		protected virtual void OnAfterSetData()
		{
			this.rowMobileRule.AfterStateChanged += this.rowMobileRule_AfterStateChanged;
		}

		#endregion  // Data

		#region Data events

		private void rowMobileRule_AfterStateChanged( object sender, EventArgs e )
		{
			#region Preconditions

			if ( this.rowMobileRule.RowState != null && !(this.rowMobileRule.RowState.State is TState) )
			{
				throw new ApplicationException( "Wrong RuleState type" );
			}

			#endregion  // Preconditions

			/* ��� ��������� RowState ������� ���������� RowStateHide � 
			 * this.rowMobileRule.RowState ���������� ������ null, � �����
			 * RowStateShow � ����� ���� ��� ������ �����������.
			 */
			if ( this.rowMobileRule.RowState != null )
			{
				this.state = this.rowMobileRule.RowState.State as TState;
			}
		}

		#endregion  // Data events

		#region View

		protected MobileRuleRow rowMobileRule;
		protected TState state;
		protected TSettings settings;

		protected virtual void OnDestroyView()
		{
			this.state = default(TState);
			this.settings = null;
			this.rowMobileRule = null;
		}
		protected virtual void OnBuildView()
		{
			this.rowMobileRule = this.Data.MobileRule.FindRow(this.mobileRuleId);
			this.settings = this.rowMobileRule.Settings as TSettings;

			/* ��� �������� ������ MobileRule ������� ��������� ������ MobileRuleRow, 
			 * � ����� ������ StateRow. ������� � ��� ������, ����� ���������� ���� 
			 * ����� RowState ����� ��� �� ������������.
			 */
			if ( this.rowMobileRule.RowState != null )
			{
				this.state = (TState) this.rowMobileRule.RowState.State;
			}
		}

		protected virtual void OnUpdateView()
		{

		}

		#endregion View

		#region Events

		private MobileRuleStateEventHandler stateChanged;

		protected virtual void OnStateChanged(MobileRuleState state)
		{
			if (this.stateChanged != null)
			{
				this.stateChanged( new MobileRuleStateEventArgs(
					this.rowMobileRule.ID, 
					this,
					state));
			}
		}

		#endregion // Events

		protected IPositionMessageSupplier PositionMessageSupplier
		{
			get
			{
				return this.ruleManager.PositionMessageSupplier;
			}
		}
	}
}