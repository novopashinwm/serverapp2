using System.Diagnostics;
using System.Threading;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	/// <summary>
	/// ������� ����������� ��� ���������� 
	/// ������� ������ �� �� � ������� 
	/// ������� �������� ��������� ��������.
	/// ����� ��� ��������� ������ ������ �� ��
	/// </summary>
	class ConnectionRule:
		MobileRuleBase<ConnectionRuleState, ConnectionRuleSettings>,
		IPositionMessageProcessor
	{

		#region Constructor & Dispose

		public ConnectionRule( IRuleManager ruleManager, int mobileRuleId ):
			base( ruleManager, mobileRuleId )
		{
			this.timer = new Timer( this.timer_Callback );
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				this.timer.Dispose();
			}

			base.Dispose(disposing);
		}

		private readonly Timer timer;

		#endregion // Constructor & Dispose

		#region Default

		private static readonly ConnectionRuleState s_defaultState = 
			new ConnectionRuleState( false );
		public static ConnectionRuleState DefaultState
		{
			get { return ConnectionRule.s_defaultState; }
		}

		private static readonly ConnectionRuleSettings s_defaultSettings = 
			new ConnectionRuleSettings();
		public static ConnectionRuleSettings DefaultSettings
		{
			get { return ConnectionRule.s_defaultSettings; }
		}

		#endregion Default

		#region View

		protected override void OnDestroyView()
		{
			this.timer.Change( -1, -1 );

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.state != null && !this.state.TimeOut )
			{
				this.timer.Change( this.settings.TimeOutInterval, -1 );
			}
		}

		#endregion // View
		
		#region IPositionMessageProcessor Members

		public void Process( IPositionMessage mobileUnit )
		{
			this.timer.Change( this.settings.TimeOutInterval, -1 );

			/* ������ ������ �� ����������� ������� ��������.
			 * ���� ����������� ��������� ��������� ����� (�������),
			 * �� ���������� ���������� ����� ��������� ������� �����
			 * (�������� �������).
			 * 
			 * � ���������� ��������� ������ ������ �� ����������� 
			 * ������� �������� ��������� ���������.
			 */

			if( this.state.TimeOut )
			{
				this.OnStateChanged( new ConnectionRuleState(false) );
			}
		}

		#endregion // IPositionMessageProcessor Members

		private void timer_Callback( object state )
		{
			//Debug.Assert( !this.state.TimeOut );

			/* �� ����� �� ���� ������ �� ����������� ������� ��������.
			 */

			this.timer.Change( -1, -1 );

			this.OnStateChanged( new ConnectionRuleState(true) );
		}

		public override void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			lock ( this.rowMobileRule )
			{
				if ( this.rowMobileRule != null )
				{
					lock ( this.rowMobileRule.RowState )
					{
						if ( this.rowMobileRule.RowState != null )
						{
							ConnectionRuleState newState = (ConnectionRuleState)mobileRuleState;
							StateRow stateRow = data.State.FindRow( this.rowMobileRule.RowState.ID );

							stateRow.BeginEdit();
							stateRow.State = newState;
							stateRow.EndEdit();
						}
					}
				}
			}
		}
	}
}
