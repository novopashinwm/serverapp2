using System;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.RuleManager.Rule
{
	/// <summary>
	/// ������� ������������� ��� ��������� ������� ������� �������� ��� ����������� 
	/// ���������������� IPositionMessage. ������� ������� ������� ����� �� ����������� � ����, 
	/// ��� �������� ������� �������� ������������� ��������� ������� PositionRule � ������������� 
	/// �� ��������� ���������.
	/// </summary>
	class PositionRule:
		IMobileRule,
		IPositionMessageProcessor
	{
		#region ID
		/*
		 * ��������� ��� ������� ������ � � ���� ��� ID ����������� � ����,
		 * ��������� �������� - ��� ��������� ��������� ����� ������� 
		 * (�������� ����� �������) ����� �������� ID ���������� � �������.
		 * ����� �������, ��� � ���� �� ����� ID-������ ����������� ������
		 * ������ ������� ����� (�������� ������ ��������), ������� �����
		 * ������������ ID ��� ����� ������� �� ���� ��������� ����� �������
		 * �� ��������� �����. 
		 * �.�. ����� ������������ �������� �������� Int32 � �����.
		 */
		private static int currentId = Int32.MaxValue;
		private readonly int id;

		#endregion  // ID

		#region Constructor & Dispose

		private readonly int mobileId;

		public PositionRule( MobileRow mobileRow )
		{
			this.id = currentId--;

			this.mobileId = mobileRow.ID;

			if ( mobileRow.RowPosition != null && !mobileRow.RowPosition.Empty)
			{
				PositionRow position = mobileRow.RowPosition;
				this.state = 
					new PositionState( 
						(DateTime)position.Time,
						(float)position.Longitude,
						(float)position.Latitude, 
						position.Course,
						position.Speed,
                        position.Valid,
                        position.CorrectTime == null? DateTime.MinValue:(DateTime) position.CorrectTime,
						position.Sensors );
			}
			else
			{
				this.state = PositionState.Empty;
			}
		}

		#endregion  // Constructor & Dispose

		#region IMobileRule Members

		private PositionState state;
		MobileRuleState IMobileRule.State
		{
			get { return this.state; }
		}

		public MobileRuleSettings Settings
		{
			get { return null; }
		}

		event MobileRuleStateEventHandler IMobileRule.StateChanged
		{
			add { this.stateChanged += value; }
			remove { this.stateChanged -= value; }
		}

		public void ApplyChange( IRuleData data, MobileRuleState mobileRuleState )
		{
			try
			{
				PositionState newState = (PositionState)mobileRuleState;
				PositionRow positionRow = data.Mobile.Rows[this.mobileId].RowPosition;

				positionRow.BeginEdit();

				positionRow.Time		= newState.Time;
				positionRow.Longitude	= newState.Longitude;
				positionRow.Latitude	= newState.Latitude;
				positionRow.Course		= newState.Course;
				positionRow.Speed		= newState.Speed;
			    positionRow.Valid       = newState.Valid;
			    positionRow.CorrectTime = newState.CorrectTime;
				positionRow.Sensors		= newState.Sensors;

				positionRow.EndEdit();

				this.state = newState;
			}
			catch (Exception e)
			{
				Trace.WriteLine(e);
			}
		}

		#endregion  // IMobileRule Members

		#region IPositionMessageProcessor Members

		public void Process( IPositionMessage message )
		{
			if ( message.GpsData.Type == PositionType.Log ) return;

			#region Extract position from controller's data

			DateTime time = TimeHelper.year1970.AddSeconds(message.GpsData.Time);
		    DateTime correctTime = TimeHelper.year1970.AddSeconds(message.GpsData.CorrectTime);
		    bool valid = message.GpsData.CorrectGPS;
			float longitude = message.GpsData.Longitude;
			float latitude = message.GpsData.Latitude;

			int course = 0;
			#region Calculate course relatively to previous position

			/* ���� ���������� ������� ���, �� ���������
			 * ������� ����� � ������ 0. ������ ��� ������ 
			 * ����� ������ ������� ������� ��������
			 */

			if ( !this.state.IsEmpty )
			{
				course =
					message.CalcCourse(
						this.state.Latitude,
						this.state.Longitude
						);
			}

			#endregion  // Calculate course relatively to previous position

			float speed = message.GpsData.Speed;

			PositionState newState =
				new PositionState(
					time,
					longitude,
					latitude,
					course,
					speed,
                    valid,
                    correctTime,
					message.Sensors == null
						? null
						: message.Sensors.Sensors
					);

			#endregion // Extract positions from controller's data

			this.OnStateChanged( newState );
		}

		#endregion  // IPositionMessageProcessor Members

		#region Events

		private MobileRuleStateEventHandler stateChanged;

		protected virtual void OnStateChanged( PositionState positionState )
		{
			if ( this.stateChanged != null )
			{
				this.stateChanged( new MobileRuleStateEventArgs(
					this.id,
					this,
					positionState ) );
			}
		}

		#endregion  // Events
	}
}