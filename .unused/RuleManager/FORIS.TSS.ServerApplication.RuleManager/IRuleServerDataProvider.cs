using FORIS.TSS.BusinessLogic.TspTerminal.Data;
using FORIS.TSS.ServerApplication.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.RuleManager
{
	public interface IRuleServerDataProvider:
		IDatabaseProvider,
		IControllerDataSupplierProvider

	{
		RuleServerData RuleData { get; }
        OperatorServerData PrincipalData { get; }
	}
}
