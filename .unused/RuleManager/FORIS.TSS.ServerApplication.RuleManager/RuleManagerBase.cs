using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Rule;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;
using FORIS.TSS.ServerApplication.RuleManager.Rule;
using TerminalService.Interfaces;
using TerminalService.Interfaces.Common;

namespace FORIS.TSS.ServerApplication.RuleManager
{
	public class RuleManagerBase:
			Component,
			IRuleManager
	{
		#region Controls & Components

		protected IContainer components;
		private RuleServerDataDispatcher dataDispatcher;

		#endregion // Controls & Component

		#region Constructor & Dispose

		public RuleManagerBase()
		{
			InitializeComponent();

			this.mobileRules = new DictionaryWithEvents<int, IMobileRule>();
			this.mobileRules.Inserted += this.mobileRules_Inserted;
			this.mobileRules.Removing += this.mobileRules_Removing;
			this.mobileRules.Clearing += this.mobileRules_Clearing;

			this.positionRules = new DictionaryWithEvents<int, PositionRule>();
			this.positionRules.Inserted += this.positionRules_Inserted;
			this.positionRules.Removing += this.positionRules_Removing;
			this.positionRules.Clearing += this.positionRules_Clearing;

			this.queueTimer = new Timer( this.queueTimer_Callback );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dataDispatcher = new RuleServerDataDispatcher( this.components );
		}

		#endregion // Component Designer generated code

		private readonly ManualResetEvent startUp = new ManualResetEvent( false );

		protected readonly DictionaryWithEvents<int, IMobileRule> mobileRules;

		private readonly DictionaryWithEvents<int, PositionRule> positionRules;

		private readonly Dictionary<int, List<IPositionMessageProcessor>> positionMessageProcessors =
			new Dictionary<int, List<IPositionMessageProcessor>>();

		private readonly Dictionary<int, List<ITextMessageProcessor>> textMessageProcessors =
			new Dictionary<int, List<ITextMessageProcessor>>();

		#region Data

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRuleServerData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if( this.Data != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.dataDispatcher.Data = value;

					if( this.Data != null )
					{
						this.OnBuildView();

						this.OnAfterSetData();
					}
				}
			}
		}

		#endregion  // Data

		#region Properties

		private IDatabaseDataSupplier database;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public IDatabaseDataSupplier Database
		{
			get { return this.database; }
			set { this.database = value; }
		}

		private IPositionMessageSupplier positionMessageSupplier;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public virtual IPositionMessageSupplier PositionMessageSupplier
		{
			get { return this.positionMessageSupplier; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.positionMessageSupplier = value;

				if( this.Data != null )
				{
					this.OnBuildView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // Properties

		#region Data

		protected virtual void OnBeforeSetData()
		{
			this.Data.MobileRuleStateChanged -= this.mobileRuleObject_StateChanged;

			if ( this.positionMessageSupplier != null )
			{
				this.positionMessageSupplier.Receive -= this.positionMessageSupplier_Receive;
				this.positionMessageSupplier.Notify -= this.positionMessageSupplier_Notify;
			}

			this.Data.MobileRule.Rows.Inserted -= this.MobileRuleRows_Inserted;
			this.Data.MobileRule.Rows.Removing -= this.MobileRuleRows_Removing;

			this.Data.Controller.Rows.Inserted -= this.ControllerRows_Inserted;
			this.Data.Controller.Rows.Removing -= this.ControllerRows_Removing;

			this.Data.Mobile.Rows.Inserted -= this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing -= this.MobileRows_Removing;
		}

		protected virtual void OnAfterSetData()
		{
			this.Data.Mobile.Rows.Inserted += this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing += this.MobileRows_Removing;

			this.Data.Controller.Rows.Inserted += this.ControllerRows_Inserted;
			this.Data.Controller.Rows.Removing += this.ControllerRows_Removing;

			this.Data.MobileRule.Rows.Inserted += this.MobileRuleRows_Inserted;
			this.Data.MobileRule.Rows.Removing += this.MobileRuleRows_Removing;

			if( this.positionMessageSupplier != null )
			{
				this.positionMessageSupplier.Receive += this.positionMessageSupplier_Receive;
				this.positionMessageSupplier.Notify += this.positionMessageSupplier_Notify;
			}

			this.Data.MobileRuleStateChanged += this.mobileRuleObject_StateChanged;
		}

		#endregion // Data

		#region Data events

		private void MobileRows_Removing( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.MobileHide( e.Item );
		}
		private void MobileRows_Inserted( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			this.MobileShow( e.Item );
		}

		private void ControllerRows_Removing( object sender, CollectionChangeEventArgs<ControllerRow> e )
		{
			this.ControllerHide( e.Item );
		}
		private void ControllerRows_Inserted( object sender, CollectionChangeEventArgs<ControllerRow> e )
		{
			this.ControllerShow( e.Item );
		}

		private void MobileRuleRows_Removing( object sender, CollectionChangeEventArgs<MobileRuleRow> e )
		{
			this.MobileRuleHide( e.Item );
		}
		private void MobileRuleRows_Inserted( object sender, CollectionChangeEventArgs<MobileRuleRow> e )
		{
			this.MobileRuleShow( e.Item );
		}

		#endregion // Data events

		#region Show & Hide

		private void MobileHide( MobileRow mobileRow )
		{
			if (
				mobileRow.Controller != null &&
				mobileRow.RowController != null
				)
			{
				int controllerId = (int) mobileRow.Controller;

				lock ( positionRules )
				{
					this.positionRules.Remove( controllerId );
				}
			}
		}
		private void MobileShow( MobileRow mobileRow )
		{
			if(
				mobileRow.Controller != null &&
				mobileRow.RowController != null
				)
			{
				int controllerId = (int) mobileRow.Controller;

				PositionRule positionRuleObject = new PositionRule( mobileRow );

				lock ( positionRules )
				{
					this.positionRules.Add( controllerId, positionRuleObject );
				}
			}
		}

		private void ControllerHide( ControllerRow controllerRow )
		{
			lock( this.positionMessageProcessors )
			{
				this.positionMessageProcessors.Remove( controllerRow.ID );
			}

			lock ( this.textMessageProcessors )
			{
				this.textMessageProcessors.Remove( controllerRow.Number );
			}
		}
		private void ControllerShow( ControllerRow controllerRow )
		{
			lock( this.positionMessageProcessors )
			{
				this.positionMessageProcessors.Add( controllerRow.ID, new List<IPositionMessageProcessor>() );
			}

			lock ( this.textMessageProcessors )
			{
				this.textMessageProcessors.Add( controllerRow.Number, new List<ITextMessageProcessor>() );
			}
		}

		private void MobileRuleHide( MobileRuleRow mobileRuleRow )
		{
			if(
				mobileRuleRow.RowMobile.Controller != null &&
				mobileRuleRow.RowMobile.RowController != null
				)
			{
				lock ( this.mobileRules )
				{
					this.mobileRules.Remove( mobileRuleRow.ID );
				}
			}
		}
		private void MobileRuleShow( MobileRuleRow mobileRuleRow )
		{
			if(
				mobileRuleRow.RowMobile.Controller != null &&
				mobileRuleRow.RowMobile.RowController != null
				)
			{
				/* ��������� ������������ ������ �� ����� 
				 * �������� �������� ������ � ���������� 
				 * �� � ��������������� ���������� ������
				 */

				IMobileRule mobileRuleObject =
					RuleManagerBase.CreateMobileRuleObject( this, mobileRuleRow );

				if (mobileRuleObject != null)
				{
					/* ������ ����� ���������� ����� ������� 
					 * ���� � ����� ������� ������� � ��������� ��� 
					 * � ��������������� ���������� �����
					 */

					lock (this.mobileRules)
					{
						this.mobileRules.Add( mobileRuleRow.ID, mobileRuleObject );
					}
				}

				/* ���� ��� �������� �������� ������ ��������� ������, 
				 * �� ������ �� ���� ������������ � �������.
				 */
			}
		}

		#endregion // Show & Hide

		#region View

		protected virtual void OnDestroyView()
		{
			this.startUp.Reset();

			this.queueTimer.Change( Timeout.Infinite, Timeout.Infinite );

			this.positionMessageProcessors.Clear();
			this.textMessageProcessors.Clear();
			this.mobileRules.Clear();
			this.positionRules.Clear();
		}
		protected virtual void OnBuildView()
		{
			foreach( ControllerRow controllerRow in this.Data.Controller.Rows )
			{
				this.ControllerShow( controllerRow );
			}

			foreach( MobileRuleRow mobileRuleRow in this.Data.MobileRule.Rows )
			{
				this.MobileRuleShow( mobileRuleRow );
			}

			foreach ( MobileRow mobileRow in this.Data.Mobile.Rows )
			{
				this.MobileShow( mobileRow );
			}

			this.startUp.Set();

			this.queueTimer.Change( 100, Timeout.Infinite );
		}

		#endregion // View

		#region CreateMobileRuleObject

		/// <summary>
		/// ������� ������ ������� ���������������� ���� 
		/// </summary>
		/// <param name="ruleManager"></param>
		/// <param name="mobileRuleRow"></param>
		/// <returns>������ �������</returns>
		/// <remarks>
		/// ����� �� ����������� ����������.
		/// ���� �������� �������� ��� �������� 
		/// �������, �� ���������� null
		/// </remarks>
		private static IMobileRule CreateMobileRuleObject( IRuleManager ruleManager, MobileRuleRow mobileRuleRow )
		{
			IMobileRule result;

			try
			{
				Type mobileRuleType = Type.GetType( mobileRuleRow.RowRule.Code, true );

				ConstructorInfo mobileRuleConstructorInfo =
					mobileRuleType.GetConstructor(
						new Type[] { typeof(IRuleManager), typeof(int) }
						);

				#region Condition

				if( mobileRuleConstructorInfo == null )
				{
					throw new ApplicationException( "Mobile rule object type must have .ctor(IRuleManager ruleManager, int mobileRuleId )" );
				}

				#endregion // Condition

				result =
					(IMobileRule)mobileRuleConstructorInfo.Invoke(
					             	new object[] { ruleManager, mobileRuleRow.ID }
					             	);
			}
			catch( Exception ex )
			{
				result = null;

				Trace.WriteLineIf(
					RuleManager.TraceSwitch.TraceInfo,
					string.Format(
						"Error creating rule '{0}' for mobile '{1}': {2}",
						mobileRuleRow.RowRule.Name,
						mobileRuleRow.RowMobile.Name,
						ex.Message
						),
						ruleManager.GetType().Name
					);
			}

			return result;
		}

		#endregion // CreateMobileRuleObject

		#region Handle mobileRules events

		private void mobileRules_Inserted( object sender, HashChangeEventArgs<int, IMobileRule> e )
		{
			IMobileRule mobileRuleObject = e.Value;
			int mobileRuleRowId = e.Key;

			mobileRuleObject.StateChanged += mobileRuleObject_StateChanged;

			#region IDataItem<IRuleData>

			this.dataDispatcher.Ambassadors.Add( (IDataItem<IRuleServerData>)mobileRuleObject );

			#endregion // IDataItem<IRuleData>

			#region IPositionMessageProcessor

			if ( mobileRuleObject is IPositionMessageProcessor )
			{
				MobileRuleRow mobileRuleRow = this.Data.MobileRule.Rows[mobileRuleRowId];

				if ( mobileRuleRow.RowMobile.Controller != null )
				{
					List<IPositionMessageProcessor> processorList;

					lock ( this.positionMessageProcessors )
					{
						processorList =
							this.positionMessageProcessors[mobileRuleRow.RowMobile.RowController.ID];
					}

					lock ( processorList )
					{
						processorList.Add( (IPositionMessageProcessor)mobileRuleObject );
					}
				}
			}

			#endregion // IPositionMessageProcessor

			#region ITextMessageProcessor

			if ( mobileRuleObject is ITextMessageProcessor )
			{
				MobileRuleRow mobileRuleRow = this.Data.MobileRule.Rows[mobileRuleRowId];

				if ( mobileRuleRow.RowMobile.Controller != null )
				{
					List<ITextMessageProcessor> processorList;

					lock ( this.textMessageProcessors )
					{
						processorList =
							this.textMessageProcessors[mobileRuleRow.RowMobile.RowController.Number];
					}

					lock ( processorList )
					{
						processorList.Add( (ITextMessageProcessor)mobileRuleObject );
					}
				}
			}

			#endregion  // ITextMessageProcessor
		}

		private void mobileRules_Removing( object sender, HashChangeEventArgs<int, IMobileRule> e )
		{
			IMobileRule mobileRuleObject = e.Value;
			int mobileRuleRowId = e.Key;

			#region ITextMessageProcessor

			if ( mobileRuleObject is ITextMessageProcessor )
			{
				MobileRuleRow mobileRuleRow = this.Data.MobileRule.Rows[mobileRuleRowId];

				if ( mobileRuleRow.RowMobile.Controller != null )
				{
					List<ITextMessageProcessor> processorList;

					lock ( this.textMessageProcessors )
					{
						processorList =
							this.textMessageProcessors[mobileRuleRow.RowMobile.RowController.Number];
					}

					lock ( processorList )
					{
						processorList.Remove( (ITextMessageProcessor)mobileRuleObject );
					}
				}
			}

			#endregion  // ITextMessageProcessor

			#region IPositionMessageProcessor

			if( mobileRuleObject is IPositionMessageProcessor )
			{
				MobileRuleRow mobileRuleRow = this.Data.MobileRule.Rows[mobileRuleRowId];

				if ( mobileRuleRow.RowMobile.Controller != null )
				{
					/* ������� ����� ������������ ������ ������������,
					 * ������ � ������, ���� ��� ���������� ������� 
					 * ������ ����������.
					 */
					List<IPositionMessageProcessor> processorList;

					lock( this.positionMessageProcessors )
					{
						processorList =
							this.positionMessageProcessors[mobileRuleRow.RowMobile.RowController.ID];
					}

					lock( processorList )
					{
						processorList.Remove( (IPositionMessageProcessor)mobileRuleObject );
					}
				}
			}

			#endregion // IPositionMessageProcessor

			#region IDataItem<IRuleServerData>

			if( mobileRuleObject is IDataItem<IRuleServerData> )
			{
				this.dataDispatcher.Ambassadors.Remove(
					(IDataItem<IRuleServerData>)mobileRuleObject
					);
			}

			#endregion // IDataItem<IRuleData>

			mobileRuleObject.StateChanged -= mobileRuleObject_StateChanged;
		}

		private void mobileRules_Clearing( object sender, HashEventArgs<int, IMobileRule> e )
		{
			#region ITextMessageProcessor

			lock ( this.textMessageProcessors )
			{
				this.textMessageProcessors.Clear();
			}

			#endregion  // ITextMessageProcessor

			#region IPositionMessageProcessor

			lock ( this.positionMessageProcessors )
			{
				this.positionMessageProcessors.Clear();
			}

			#endregion  // IPositionMessageProcessor

			#region IDataItem<IRuleServerData>

			this.dataDispatcher.Ambassadors.Clear();

			#endregion  // IDataItem<IRuleServerData>

			foreach ( IMobileRule mobileRuleObject in mobileRules.Values )
			{
				mobileRuleObject.StateChanged -= mobileRuleObject_StateChanged;
			}
		}

		#endregion // Handle mobileRules events

		#region Handle positionRules events

		private void positionRules_Inserted( object sender, HashChangeEventArgs<int, PositionRule> e )
		{
			PositionRule positionRuleObject = e.Value;
			int controllerId = e.Key;

			( (IMobileRule)positionRuleObject ).StateChanged += mobileRuleObject_StateChanged;

			#region IPositionMessageProcessor

			List<IPositionMessageProcessor> processorList;

			lock ( this.positionMessageProcessors )
			{
				processorList = this.positionMessageProcessors[controllerId];
			}

			lock ( processorList )
			{
				processorList.Add( positionRuleObject );
			}

			#endregion  // IPositionMessageProcessor
		}

		private void positionRules_Removing( object sender, HashChangeEventArgs<int, PositionRule> e )
		{
			PositionRule positionRuleObject = e.Value;
			int controllerId = e.Key;

			( (IMobileRule)positionRuleObject ).StateChanged -= mobileRuleObject_StateChanged;

			#region IPositionMessageProcessor

			List<IPositionMessageProcessor> processorList;

			lock ( this.positionMessageProcessors )
			{
				processorList = this.positionMessageProcessors[controllerId];
			}

			lock ( processorList )
			{
				processorList.Remove( positionRuleObject );
			}

			#endregion  // IPositionMessageProcessor
		}

		private void positionRules_Clearing( object sender, HashEventArgs<int, PositionRule> e )
		{
			foreach ( PositionRule positionRuleObject in positionRules.Values )
			{
				((IMobileRule)positionRuleObject).StateChanged -= mobileRuleObject_StateChanged;
			}
		}

		#endregion  // Handle positionRules events

		#region Handle positionMessageSupplier events

		private void positionMessageSupplier_Receive( ReceiveEventArgs e )
		{
			/* ������� ��������� ������ �� �������� ������ �� 
			 * ������ ���� �� ����� ������ ���������� �������.
			 * � ���������� ������� �� �������� ���������� ���� 
			 * ������, � �� ���������� ������� ������� �� ��������.
			 */

			this.startUp.WaitOne();

			/* ���� ��������� ������ ������ �� 
			 * ������������ � ������� ����������
			 */
			#region
			//using( RuleDataTreater treater = (RuleDataTreater)((IDataSupplier<IRuleDataTreater>)this.Data).GetDataTreater() )
			//{
			//    try
			//    {
			//        treater.ProcessPositionMessages( e.Messages, this.positionMessageProcessors );
			//    }
			//    catch( Exception )
			//    {
					
			//    }
			//}
			#endregion

			foreach ( IPositionMessage message in e.Messages )
			{
				Trace.WriteLineIf(
					RuleManager.TraceSwitch.TraceWarning,
					string.Format(
						"There come data from controller number {0}: ({1}; {2})",
						message.DeviceInfo.DeviceNumber, 
						message.GpsData.Longitude,
						message.GpsData.Latitude
						),
					this.GetType().Name
					);

				/* ���� � ����� ������ ������������ ��� ����������� 
				 * � ����� �������, �� ��� �� ��� ����� � �� ��� 
				 * ������������ �� �����, � ������ �������
				 */
				if ( this.positionMessageProcessors.ContainsKey( message.DeviceInfo.ControllerID ) /*&& message.GpsData.CorrectGPS*/ )
				{
					List<IPositionMessageProcessor> processorList;

					lock ( this.positionMessageProcessors )
					{
						processorList = this.positionMessageProcessors[message.DeviceInfo.ControllerID];
					}

					if ( processorList != null )
					{
						lock ( processorList )
						{
							foreach ( IPositionMessageProcessor rule in processorList )
							{
								rule.Process( message );
							}
						}
					}
				}
			}
		}

		private void positionMessageSupplier_Notify( NotifyEventArgs args )
		{
			/* ������� ��������� ������ �� �������� ������ �� 
			 * ������ ���� �� ����� ������ ���������� �������.
			 * � ���������� ������� �� �������� ���������� ���� 
			 * ������, � �� ���������� ������� ������� �� ��������.
			 */

			this.startUp.WaitOne();

			if ( args.Message is ITextMessage )
			{
				this.ProcessTextMessage( (ITextMessage)args.Message );
			}
		}

		#endregion // Handle positionMessageSupplier events

		#region Handle mobileRule objects' events

		private void mobileRuleObject_StateChanged( MobileRuleStateEventArgs e )
		{
			if ( e.MobileRuleObject == null && e.IdMobileRule != 0 )
			{
				e = 
					new MobileRuleStateEventArgs( 
						e.IdMobileRule, 
						this.mobileRules[e.IdMobileRule], 
						e.MobileRuleState );
			}

			lock ( this.queue )
			{
				if ( this.changedMobileRuleIds.Contains( e.IdMobileRule ) )
				{
					this.queueTimer_Callback( null );
				}

				this.queue.Add( e );
				this.changedMobileRuleIds.Add( e.IdMobileRule );
			}

			if ( this.queue.Count >= 100 )
			{
				this.queueTimer.Change( 0, Timeout.Infinite );
			}
		}

		#endregion // Handle mobileRule objects' events

		private readonly List<MobileRuleStateEventArgs> queue = 
			new List<MobileRuleStateEventArgs>( 200 );
		private readonly List<int> changedMobileRuleIds = 
			new List<int>();

		private readonly Timer queueTimer;

		/// <summary>
		/// ���� ����� ���������� ��������� ������ ��������� ��������� �������� ������.
		/// </summary>
		/// <param name="state"></param>
		/// <remarks>
		/// ����� ���� ��� ������� ������������ (�������� ������������� ���������� ���������) 
		/// ��� ��������� timeout (�������� ������) ������ ����� �������� ��������� ��������� 
		/// ��������������� �������� (����������� ������ this.ProcessStateChanges() ).
		/// </remarks>
		private void queueTimer_Callback( object state )
		{
			if ( this.queue.Count > 0 )
			{
				MobileRuleStateEventArgs[] stateChanges;

				lock ( this.queue )
				{
					stateChanges = this.queue.ToArray();

					this.queue.Clear();
					this.changedMobileRuleIds.Clear();
				}

				if ( stateChanges != null )
				{
					this.ProcessStateChanges( stateChanges );
				}
			}

			this.queueTimer.Change( 100, Timeout.Infinite );
		}

		/// <summary>
		/// ���� ����� �������� ��� ������� �������� ������ �������� ��� ���������� ��������� 
		/// ���������.
		/// </summary>
		/// <param name="stateChanges">������ �������� ������ (� �� ����� ���������), ��������� 
		/// ����� ������� ��� ��������� ���������</param>
		/// <remarks>
		/// ���� ����� ������� �������������� � �������� ������� ����� ������� ������ ����� 
		/// ����������� �������� ������ �� ������ � ���������� RuleServerData, �� � � ������ 
		/// ��������� ����������� ����������� ��������������� ��������.
		/// </remarks>
		protected virtual void ProcessStateChanges( MobileRuleStateEventArgs[] stateChanges )
		{
			using (
				RuleDataTreater ruleDataTreater =
					(RuleDataTreater)( ( (IDataSupplier<IRuleDataTreater>) this.Data ).GetDataTreater() )
			)
			{
				ruleDataTreater.ProcessStateChanges( stateChanges );
			}
		}

		/// <summary>
		/// ���������� ��������� ��������� ��������������� �������� �� ���������
		/// </summary>
		/// <param name="message">��������� ���������</param>
		/// <remarks>
		/// ���� ����� ������� �������������� � �������� ������� ��� �������� ���� ����������� 
		/// �� ���������� �����������. 
		/// </remarks>
		protected virtual void ProcessTextMessage( ITextMessage message )
		{
			Trace.WriteLineIf(
				RuleManager.TraceSwitch.TraceWarning,
				string.Format(
					"There come text message from controller number {0}: '{1}'",
					message.DeviceInfo.DeviceNumber, message.Text
					),
				this.GetType().Name
				);

			/* ���� � ����� ������ ������������ ��� ����������� 
			* � ����� �������, �� ��� �� ��� ����� � �� ��� 
			* ������������ �� �����, � ������ �������
			*/
			if ( this.textMessageProcessors.ContainsKey( message.DeviceInfo.DeviceNumber ) )
			{
				List<ITextMessageProcessor> processorList;

				lock ( this.textMessageProcessors )
				{
					processorList = this.textMessageProcessors[message.DeviceInfo.DeviceNumber];
				}

				if ( processorList != null )
				{
					lock ( processorList )
					{
						foreach ( ITextMessageProcessor rule in processorList )
						{
							rule.Process( message );
						}
					}
				}
			}
		}
	}

	public class RuleManager
	{
		public static readonly TraceSwitch TraceSwitch = 
			new TraceSwitch( "RuleManager", "", "Warning" );
	}
}