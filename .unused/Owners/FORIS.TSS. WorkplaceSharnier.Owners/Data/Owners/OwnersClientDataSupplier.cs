using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceSharnier.Owners.Data.Owners
{
	public class OwnersClientDataSupplier :
		ClientDataSupplier<IOwnersClientDataProvider, IOwnersDataSupplier, IOwnersDataTreater>,
		IOwnersDataSupplier
	{
		public OwnersClientDataSupplier( IOwnersClientDataProvider provider ) :
			base( provider, provider.GetOwnersDataSupplier() )
		{

		}

		public IRuleDataSupplier RuleDataSupplier
		{
			get { return this.DataSupplier.RuleDataSupplier; }
		}

	    public IRuleSecurityDataSupplier SecurityDataSupplier
	    {
	        get { return this.DataSupplier.SecurityDataSupplier; }
	    }
	}
}