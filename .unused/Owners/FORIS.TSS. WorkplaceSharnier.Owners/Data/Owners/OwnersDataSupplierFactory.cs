using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceSharnier.Owners.Data.Owners
{
	public class OwnersDataSupplierFactory :
		ClientDataSupplierFactory<IOwnersClientDataProvider, OwnersClientData, DataParams>
	{
		public OwnersDataSupplierFactory( IOwnersClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override OwnersClientData CreateDataSupplier( DataParams @params )
		{
			if ( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			OwnersClientData Supplier = new OwnersClientData( this.Client );

			return Supplier;
		}
	}
}