using FORIS.TSS.Interfaces.Owners.Data;
using FORIS.TSS.WorkplaceSharnier.Owners.Data.Owners;

namespace FORIS.TSS.WorkplaceSharnier.Owners
{
    public interface IOwnersClientDataProvider :
        IClientDataProvider,
        IOwnersDataSupplierProvider
    {
        OwnersClientData GetOwnersClientData();
    }
}