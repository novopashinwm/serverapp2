using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Owners.Controls;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners;
using FORIS.TSS.WorkplaceShadow.Owners.Data.Owners;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Owners;

namespace FORIS.TSS.Owners
{
    public class RB_Owners : 
		PluginForm,
        IClientItem<IOwnersClientDataProvider>
	{
		#region Control & Components

		private IContainer components;
        private OwnersDataDispatcher ownersDataDispatcher;
		private OwnersData ownersData;
		private OwnersTableControl ownersTableControl;
        private OwnersClientDataProviderDispatcher clientDispatcher;
        private OwnersDataSupplier ownersDataSupplier;
		private OwnersClientDataProviderAmbassador caOwnersDataSupplier;
    	private OwnersFilter ownersFilter;
    	private OwnersXpRowFactory ownersXpRowFactory;
		private OwnersDataAmbassador daOwnersFilter;
		private OwnersDataAmbassador daOwnersEditorProvider;
    	private OwnersEditorProvider ownersEditorProvider;
		private OperatorDataAmbassador daOwnersXpRowFactory;
		private OperatorDataAmbassador daOwnersEditorProviderOperator;
		private TableLayoutPanel tableLayoutPanelMain;
		private OperatorDataDispatcher operatorDataDispatcher;
		private ToolBarButton tbsFilter;
		private ToolBarButton tbbFilter;
    	private CommandManager commandManager;
		private CommandInstance ciFilter;
		private OwnerMobileControl ownerMobileControl;
		private OwnersDataAmbassador daOwnerMobileControl;
		private OperatorDataAmbassador daOwnerMobileControlOperator;
		private Command cmdFilter;

		#endregion  // Control & Components

		#region Constructor & Dispose

		public RB_Owners()
		{
			InitializeComponent();
		}

		#endregion  //Constructor & Dispose

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ownersTableControl = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersTableControl();
			this.tbsFilter = new System.Windows.Forms.ToolBarButton();
			this.tbbFilter = new System.Windows.Forms.ToolBarButton();
			this.ownersEditorProvider = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersEditorProvider( this.components );
			this.ownersData = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersData( this.components );
			this.ownersDataSupplier = new FORIS.TSS.WorkplaceShadow.Owners.Data.Owners.OwnersDataSupplier( this.components );
			this.clientDispatcher = new FORIS.TSS.Owners.OwnersClientDataProviderDispatcher( this.components );
			this.caOwnersDataSupplier = new FORIS.TSS.Owners.OwnersClientDataProviderAmbassador();
			this.ownersDataDispatcher = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataDispatcher( this.components );
			this.daOwnersFilter = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.ownersFilter = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersFilter();
			this.daOwnersEditorProvider = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.daOwnerMobileControl = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.ownerMobileControl = new FORIS.TSS.Owners.Controls.OwnerMobileControl();
			this.ownersXpRowFactory = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersXpRowFactory();
			this.operatorDataDispatcher = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataDispatcher( this.components );
			this.daOwnersXpRowFactory = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.daOwnersEditorProviderOperator = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.commandManager = new FORIS.TSS.Common.Commands.CommandManager( this.components );
			this.cmdFilter = new FORIS.TSS.Common.Commands.Command( this.components );
			this.ciFilter = new FORIS.TSS.Common.Commands.CommandInstance();
			this.daOwnerMobileControlOperator = new FORIS.TSS.Helpers.Operator.Data.Operator.OperatorDataAmbassador();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// ownersTableControl
			// 
			// 
			// 
			// 
			this.ownersTableControl.ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.ownersTableControl.ToolBar.Buttons.AddRange( new System.Windows.Forms.ToolBarButton[] {
            this.tbsFilter,
            this.tbbFilter} );
			this.ownersTableControl.ToolBar.Divider = false;
			this.ownersTableControl.ToolBar.DropDownArrows = true;
			this.ownersTableControl.ToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.ownersTableControl.ToolBar.Location = new System.Drawing.Point( 0, 0 );
			this.ownersTableControl.ToolBar.Name = "globalToolbar";
			this.ownersTableControl.ToolBar.ShowToolTips = true;
			this.ownersTableControl.ToolBar.Size = new System.Drawing.Size( 478, 26 );
			this.ownersTableControl.ToolBar.TabIndex = 0;
			this.ownersTableControl.Controls.Add( this.ownersTableControl.ToolBar );
			this.ownersTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ownersTableControl.EditorProvider = this.ownersEditorProvider;
			this.ownersTableControl.Location = new System.Drawing.Point( 3, 3 );
			this.ownersTableControl.Name = "ownersTableControl";
			this.ownersTableControl.Size = new System.Drawing.Size( 478, 343 );
			this.ownersTableControl.TabIndex = 0;
			this.ownersTableControl.ToolBarVisible = true;
			this.ownersTableControl.SelectionChanged += new System.EventHandler( this.ownersTableControl_SelectionChanged );
			// 
			// tbsFilter
			// 
			this.tbsFilter.Name = "tbsFilter";
			this.tbsFilter.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// tbbFilter
			// 
			this.tbbFilter.ImageIndex = 138;
			this.tbbFilter.Name = "tbbFilter";
			// 
			// ownersData
			// 
			this.ownersData.DataSupplier = this.ownersDataSupplier;
			this.ownersData.InvokeBroker = this.invokeBroker;
			this.ownersData.Loaded += new System.EventHandler( this.ownersData_Loaded );
			this.ownersData.Loading += new System.EventHandler( this.ownersData_Loading );
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.Ambassadors.Add( this.caOwnersDataSupplier );
			// 
			// caOwnersDataSupplier
			// 
			this.caOwnersDataSupplier.Item = this.ownersDataSupplier;
			// 
			// ownersDataDispatcher
			// 
			this.ownersDataDispatcher.Ambassadors.Add( this.daOwnersFilter );
			this.ownersDataDispatcher.Ambassadors.Add( this.daOwnersEditorProvider );
			this.ownersDataDispatcher.Ambassadors.Add( this.daOwnerMobileControl );
			// 
			// daOwnersFilter
			// 
			this.daOwnersFilter.Item = this.ownersFilter;
			// 
			// daOwnersEditorProvider
			// 
			this.daOwnersEditorProvider.Item = this.ownersEditorProvider;
			// 
			// daOwnerMobileControl
			// 
			this.daOwnerMobileControl.Item = this.ownerMobileControl;
			// 
			// ownerMobileControl
			// 
			this.ownerMobileControl.Data = null;
			this.ownerMobileControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ownerMobileControl.Location = new System.Drawing.Point( 487, 3 );
			this.ownerMobileControl.Name = "ownerMobileControl";
			this.ownerMobileControl.Size = new System.Drawing.Size( 202, 343 );
			this.ownerMobileControl.TabIndex = 1;
			// 
			// ownersXpRowFactory
			// 
			this.ownersXpRowFactory.Filter = this.ownersFilter;
			this.ownersXpRowFactory.ViewControl = this.ownersTableControl;
			// 
			// operatorDataDispatcher
			// 
			this.operatorDataDispatcher.Ambassadors.Add( this.daOwnersXpRowFactory );
			this.operatorDataDispatcher.Ambassadors.Add( this.daOwnerMobileControlOperator );
			this.operatorDataDispatcher.Ambassadors.Add( this.daOwnersEditorProviderOperator );
			// 
			// daOwnersXpRowFactory
			// 
			this.daOwnersXpRowFactory.Item = this.ownersXpRowFactory;
			// 
			// daOwnersEditorProviderOperator
			// 
			this.daOwnersEditorProviderOperator.Item = this.ownersEditorProvider;
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.ColumnCount = 2;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 70F ) );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 30F ) );
			this.tableLayoutPanelMain.Controls.Add( this.ownersTableControl, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.ownerMobileControl, 1, 0 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 1;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 692, 349 );
			this.tableLayoutPanelMain.TabIndex = 1;
			// 
			// cmdFilter
			// 
			this.cmdFilter.Instances.Add( this.ciFilter );
			this.cmdFilter.Manager = this.commandManager;
			this.cmdFilter.Execute += new System.EventHandler( this.cmdFilter_Execute );
			// 
			// ciFilter
			// 
			this.ciFilter.Item = this.tbbFilter;
			// 
			// daOwnerMobileControlOperator
			// 
			this.daOwnerMobileControlOperator.Item = this.ownerMobileControl;
			// 
			// RB_Owners
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.ClientSize = new System.Drawing.Size( 692, 349 );
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "RB_Owners";
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion  // Windows Form Designer generated code

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IOwnersClientDataProvider)value; }
		}

		#endregion  // IClientItem Members

		#region IClientItem<IOwnersClientDataProvider> Members

		public IOwnersClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set
			{
				IOperatorClientDataProvider client = value as IOperatorClientDataProvider;
				if ( client != null )
				{
					this.operatorDataDispatcher.Data = client.GetOperatorClientData();
				}

				this.clientDispatcher.Client = value;

				if ( client == null )
				{
					this.operatorDataDispatcher.Data = null;
				}
			}
		}

		#endregion  // IClientItem<IOwnersClientDataProvider> Members

		#region Handle ownersData events

		void ownersData_Loading(object sender, System.EventArgs e)
        {
            this.ownersDataDispatcher.Data = null;
        }

        void ownersData_Loaded(object sender, System.EventArgs e)
        {
            this.ownersDataDispatcher.Data = this.ownersData;
		}

		#endregion  // Handle ownersData events

		private void ownersTableControl_SelectionChanged( object sender, System.EventArgs e )
		{
			this.ownerMobileControl.SelectedOwner = this.ownersTableControl.SelectedOwner;
		}

		private void cmdFilter_Execute( object sender, System.EventArgs e )
		{
			this.ownerMobileControl.Filtered = 
				this.cmdFilter.Checked = !this.cmdFilter.Checked;
		}
	}
}