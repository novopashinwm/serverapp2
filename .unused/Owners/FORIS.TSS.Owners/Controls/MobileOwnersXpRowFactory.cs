﻿using System;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners;

namespace FORIS.TSS.Owners.Controls
{
	public class MobileOwnersXpRowFactory :
		ViewObjectFactory<OwnerMobileRow, OwnersXpRow, IOperatorData>
	{
		public override OwnersXpRow CreateViewObject( int viewObjectId, OwnerMobileRow ownerMobileRow )
		{
			#region Preconditions

			if ( this.Data == null ) throw new ApplicationException( "There is no data to create new item" );

			#endregion  // Preconditions

			OwnersRow ownerRow = ownerMobileRow.RowOwner;
			OperatorRow operatorRow = this.Data.Operator.FindRow( ownerRow.Operator );

			return
				new OwnersXpRow(
					viewObjectId,
					operatorRow.Name,
					ownerRow.Surname,
					ownerRow.Name,
					ownerRow.Patronimyc,
					ownerRow.Phone1,
					ownerRow.Phone2,
					ownerRow.Phone3
					);
		}
	}
}