﻿using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile;
using FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners;

namespace FORIS.TSS.Owners.Controls
{
	public class OwnerMobileControl : 
		UserControl,
		IDataItem<IOwnersData>,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private IContainer components;
		private MobileTableControl mobileTableControl;
		private OwnerMobileFilter ownerMobileFilter;
		private OwnerMobileXpRowFactory ownerMobileXpRowFactory;
		private MobileXpRowFactory mobileXpRowFactory;
		private OwnerMobileEditorProvider ownerMobileEditorProvider;
		private OwnersDataAmbassador daMobileXpRowFactory;
		private OwnersDataAmbassador daMobileFilter;
		private OwnersDataAmbassador daOwnerMobileXpRowFactory;
		private OwnersDataAmbassador daOwnerMobileEditorProvider;
		private TableLayoutPanel tableLayoutPanelMain;
		private OwnersTableControl ownersTableControl;
		private MobileOwnersXpRowFactory mobileOwnersXpRowFactory;
		private MobileOwnersFilter mobileOwnersFilter;
		private OwnersDataAmbassador daMobileOwnersFilter;
		private OwnersDataDispatcher ownersDataDispatcher;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public OwnerMobileControl()
		{
			InitializeComponent();

			this.mobileTableControl.Table.Sort( 1 );
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.mobileTableControl = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.MobileTableControl();
			this.ownerMobileEditorProvider = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.OwnerMobileEditorProvider( this.components );
			this.ownerMobileFilter = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.OwnerMobileFilter();
			this.ownerMobileXpRowFactory = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.OwnerMobileXpRowFactory();
			this.mobileXpRowFactory = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.MobileXpRowFactory();
			this.ownersDataDispatcher = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataDispatcher( this.components );
			this.daMobileXpRowFactory = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.daMobileFilter = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.daOwnerMobileXpRowFactory = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.daOwnerMobileEditorProvider = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.ownersTableControl = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersTableControl();
			this.daMobileOwnersFilter = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataAmbassador();
			this.mobileOwnersFilter = new FORIS.TSS.Owners.Controls.MobileOwnersFilter();
			this.mobileOwnersXpRowFactory = new FORIS.TSS.Owners.Controls.MobileOwnersXpRowFactory();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// mobileTableControl
			// 
			this.mobileTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mobileTableControl.EditorProvider = this.ownerMobileEditorProvider;
			this.mobileTableControl.Location = new System.Drawing.Point( 3, 3 );
			this.mobileTableControl.Name = "mobileTableControl";
			this.mobileTableControl.Size = new System.Drawing.Size( 255, 264 );
			this.mobileTableControl.TabIndex = 2;
			this.mobileTableControl.SelectionChanged += new System.EventHandler( this.mobileTableControl_SelectionChanged );
			// 
			// ownerMobileXpRowFactory
			// 
			this.ownerMobileXpRowFactory.Filter = this.ownerMobileFilter;
			this.ownerMobileXpRowFactory.ViewControl = this.mobileTableControl;
			// 
			// mobileXpRowFactory
			// 
			this.mobileXpRowFactory.Filter = this.ownerMobileFilter;
			this.mobileXpRowFactory.ViewControl = this.mobileTableControl;
			// 
			// ownersDataDispatcher
			// 
			this.ownersDataDispatcher.Ambassadors.Add( this.daMobileXpRowFactory );
			this.ownersDataDispatcher.Ambassadors.Add( this.daOwnerMobileXpRowFactory );
			this.ownersDataDispatcher.Ambassadors.Add( this.daMobileFilter );
			this.ownersDataDispatcher.Ambassadors.Add( this.daOwnerMobileEditorProvider );
			this.ownersDataDispatcher.Ambassadors.Add( this.daMobileOwnersFilter );
			// 
			// daMobileXpRowFactory
			// 
			this.daMobileXpRowFactory.Item = this.mobileXpRowFactory;
			// 
			// daMobileFilter
			// 
			this.daMobileFilter.Item = this.ownerMobileFilter;
			// 
			// daOwnerMobileXpRowFactory
			// 
			this.daOwnerMobileXpRowFactory.Item = this.ownerMobileXpRowFactory;
			// 
			// daOwnerMobileEditorProvider
			// 
			this.daOwnerMobileEditorProvider.Item = this.ownerMobileEditorProvider;
			// 
			// tableLayoutPanelMain
			// 
			this.tableLayoutPanelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanelMain.ColumnCount = 1;
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
			this.tableLayoutPanelMain.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
			this.tableLayoutPanelMain.Controls.Add( this.mobileTableControl, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.ownersTableControl, 0, 1 );
			this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelMain.Location = new System.Drawing.Point( 0, 0 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			this.tableLayoutPanelMain.RowCount = 2;
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 70F ) );
			this.tableLayoutPanelMain.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 30F ) );
			this.tableLayoutPanelMain.Size = new System.Drawing.Size( 261, 386 );
			this.tableLayoutPanelMain.TabIndex = 3;
			// 
			// ownersTableControl
			// 
			// 
			// 
			// 
			this.ownersTableControl.ToolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.ownersTableControl.ToolBar.Divider = false;
			this.ownersTableControl.ToolBar.DropDownArrows = true;
			this.ownersTableControl.ToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.ownersTableControl.ToolBar.Location = new System.Drawing.Point( 0, 0 );
			this.ownersTableControl.ToolBar.Name = "globalToolbar";
			this.ownersTableControl.ToolBar.ShowToolTips = true;
			this.ownersTableControl.ToolBar.Size = new System.Drawing.Size( 614, 40 );
			this.ownersTableControl.ToolBar.TabIndex = 0;
			this.ownersTableControl.ToolBar.Visible = false;
			this.ownersTableControl.Controls.Add( this.ownersTableControl.ToolBar );
			this.ownersTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ownersTableControl.Location = new System.Drawing.Point( 3, 273 );
			this.ownersTableControl.Name = "ownersTableControl";
			this.ownersTableControl.Size = new System.Drawing.Size( 255, 110 );
			this.ownersTableControl.TabIndex = 3;
			// 
			// daMobileOwnersFilter
			// 
			this.daMobileOwnersFilter.Item = this.mobileOwnersFilter;
			// 
			// mobileOwnersXpRowFactory
			// 
			this.mobileOwnersXpRowFactory.Filter = this.mobileOwnersFilter;
			this.mobileOwnersXpRowFactory.ViewControl = this.ownersTableControl;
			// 
			// OwnerMobileControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "OwnerMobileControl";
			this.Size = new System.Drawing.Size( 261, 386 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion

		#region IDataItem<IOwnersData> Members

		public IOwnersData Data
		{
			get { return this.ownersDataDispatcher.Data; }
			set { this.ownersDataDispatcher.Data = value; }
		}

		#endregion  // IDataItem<IOwnersData> Members

		#region IDataItem<IOperatorData> Members

		IOperatorData IDataItem<IOperatorData>.Data
		{
			get { return this.mobileOwnersXpRowFactory.Data; }
			set { this.mobileOwnersXpRowFactory.Data = value; }
		}

		#endregion  // IDataItem<IOperatorData> Members

		#region Properties

		private int selectedOwner;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedOwner
		{
			get { return this.selectedOwner; }
			set
			{
				if ( this.selectedOwner != value )
				{
					this.selectedOwner = value;


					this.mobileTableControl.SelectedOwner = this.selectedOwner;

					this.ownerMobileFilter.OwnerId = this.selectedOwner;

					this.mobileTableControl.Table.Sort();
					this.mobileTableControl.Table.Sort();
				}
			}
		}

		private bool filtered = false;
		[Browsable( true )]
		[DefaultValue( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public bool Filtered
		{
			get { return this.filtered; }
			set
			{
				if ( this.filtered != value )
				{
					this.filtered = value;

					if ( this.filtered )
					{
						this.mobileXpRowFactory.ViewControl = null;
					}
					else
					{
						this.mobileXpRowFactory.ViewControl = mobileTableControl;
					}
				}
			}
		}

		#endregion  // Properties

		private void mobileTableControl_SelectionChanged( object sender, System.EventArgs e )
		{
			this.mobileOwnersFilter.MobileId = this.mobileTableControl.SelectedMobile;
		}
	}
}