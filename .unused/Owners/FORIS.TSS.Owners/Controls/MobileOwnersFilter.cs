﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.Owners.Controls
{
	public class MobileOwnersFilter :
		Filter<OwnerMobileRow, IOwnersData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.OwnerMobile.Rows.Inserted -= this.OwnerMobileRows_Inserted;
			this.Data.OwnerMobile.Rows.Removing -= this.OwnerMobileRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.OwnerMobile.Rows.Inserted += this.OwnerMobileRows_Inserted;
			this.Data.OwnerMobile.Rows.Removing += this.OwnerMobileRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void OwnerMobileRows_Inserted( object sender, CollectionChangeEventArgs<OwnerMobileRow> e )
		{
			if ( this.Check( e.Item ) )
			{
				this.dataObjects.Add( e.Item.RowOwner.ID, e.Item );
			}
		}
		void OwnerMobileRows_Removing( object sender, CollectionChangeEventArgs<OwnerMobileRow> e )
		{
			if ( this.dataObjects.ContainsKey( e.Item.RowOwner.ID ) )
			{
				this.dataObjects.Remove( e.Item.RowOwner.ID );
			}
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( OwnerMobileRow row in this.Data.OwnerMobile.Rows )
			{
				if ( this.Check( row ) )
				{
					this.dataObjects.Add( row.RowOwner.ID, row );
				}
			}
		}

		#endregion  // View

		#region Properties

		private int mobileId;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int MobileId
		{
			get { return this.mobileId; }
			set
			{
				if ( this.mobileId != value )
				{
					if ( this.Data != null )
					{
						this.OnDestroyView();
					}

					this.mobileId = value;

					if ( this.Data != null )
					{
						this.OnBuildView();
					}
				}
			}
		}

		#endregion  // Properties

		private bool Check( OwnerMobileRow row )
		{
			return row.Mobile == this.mobileId;
		}
	}
}