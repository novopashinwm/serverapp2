using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Owners;

namespace FORIS.TSS.Owners
{
    class OwnersClientDataProviderDispatcher:
		ClientDispatcher<IOwnersClientDataProvider>
    {
        public OwnersClientDataProviderDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
        public OwnersClientDataProviderDispatcher()
		{
		}

        private readonly OwnersClientDataProviderAmbassadorCollection ambassadors =
            new OwnersClientDataProviderAmbassadorCollection();

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new OwnersClientDataProviderAmbassadorCollection Ambassadors
        {
            get { return this.ambassadors; }
        }

        protected override AmbassadorCollection<IClientItem<IOwnersClientDataProvider>> GetAmbassadorCollection()
        {
            return this.ambassadors;
        }
    }

    internal class OwnersClientDataProviderAmbassadorCollection :
        ClientAmbassadorCollection<IOwnersClientDataProvider>
    {
        public new OwnersClientDataProviderAmbassador this[int index]
        {
            get { return (OwnersClientDataProviderAmbassador)base[index]; }
        }
    }

    internal class OwnersClientDataProviderAmbassador :
        ClientAmbassador<IOwnersClientDataProvider>
    {
    }
}
