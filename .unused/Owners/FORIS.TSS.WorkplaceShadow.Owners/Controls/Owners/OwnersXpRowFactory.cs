using System;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersXpRowFactory :
		ViewObjectFactory<OwnersRow, OwnersXpRow, IOperatorData>
	{
		public override OwnersXpRow CreateViewObject( int viewObjectId, OwnersRow ownerRow )
		{
			#region Preconditions

			if ( this.Data == null ) throw new ApplicationException( "There is no data to create new item" );

			#endregion  // Preconditions

			OperatorRow operatorRow = this.Data.Operator.FindRow( ownerRow.Operator );

			return
				new OwnersXpRow(
					viewObjectId,
					operatorRow.Name,
					ownerRow.Surname,
					ownerRow.Name,
					ownerRow.Patronimyc,
					ownerRow.Phone1,
					ownerRow.Phone2,
					ownerRow.Phone3
					);
		}
	}
}