using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersFilter :
		Filter<OwnersRow, IOwnersData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Owners.Rows.Inserted -= this.OwnersRows_Inserted;
			this.Data.Owners.Rows.Removing -= this.OwnersRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.Owners.Rows.Inserted += this.OwnersRows_Inserted;
			this.Data.Owners.Rows.Removing += this.OwnersRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void OwnersRows_Inserted( object sender, CollectionChangeEventArgs<OwnersRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}
		void OwnersRows_Removing( object sender, CollectionChangeEventArgs<OwnersRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( OwnersRow row in this.Data.Owners.Rows )
			{
				this.dataObjects.Add( row.ID, row );
			}
		}

		#endregion  // View
	}
}