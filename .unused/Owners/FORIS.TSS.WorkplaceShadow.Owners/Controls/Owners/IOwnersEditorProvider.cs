using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public interface IOwnersEditorProvider :
		IEditorProvider
	{

	}
}