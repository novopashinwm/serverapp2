using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersTableControl :
		TssUserControl,
		IViewObjectControl<OwnersXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private TextColumn columnSurname;
		private TextColumn columnName;
		private TextColumn columnPatronymic;
		private TextColumn columnPhone1;
		private TextColumn columnPhone2;
		private TextColumn columnPhone3;
		private OwnersTableModel tableModel;
		private Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton tbbRemove;
		private Command cmdRemove;
		private Command cmdProperties;
		private Command cmdNew;
		private CommandInstance ciRemoveToolBarButton;
		private CommandInstance ciPropertiesToolBarButton;
		private CommandInstance ciNewToolBarButton;
		private ToolBarButton separator2;
		private TextColumn columnOperatorName;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public OwnersTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OwnersTableControl));
            this.tableModel = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners.OwnersTableModel(this.components);
            this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
            this.columnOperatorName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnSurname = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnPatronymic = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnPhone1 = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnPhone2 = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnPhone3 = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.tbbNew = new System.Windows.Forms.ToolBarButton();
            this.tbbProperties = new System.Windows.Forms.ToolBarButton();
            this.separator2 = new System.Windows.Forms.ToolBarButton();
            this.tbbRemove = new System.Windows.Forms.ToolBarButton();
            this.cmdRemove = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciRemoveToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdProperties = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciPropertiesToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdNew = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciNewToolBarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.SuspendLayout();
            // 
            // tableModel
            // 
            this.tableModel.SelectionAdjuster = null;
            // 
            // columnModel
            // 
            this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnOperatorName,
            this.columnSurname,
            this.columnName,
            this.columnPatronymic,
            this.columnPhone1,
            this.columnPhone2,
            this.columnPhone3});
            // 
            // columnOperatorName
            // 
            this.columnOperatorName.Editable = false;
            this.columnOperatorName.Editor = null;
            resources.ApplyResources(this.columnOperatorName, "columnOperatorName");
            // 
            // columnSurname
            // 
            this.columnSurname.Editable = false;
            this.columnSurname.Editor = null;
            resources.ApplyResources(this.columnSurname, "columnSurname");
            // 
            // columnName
            // 
            this.columnName.Editable = false;
            this.columnName.Editor = null;
            resources.ApplyResources(this.columnName, "columnName");
            // 
            // columnPatronymic
            // 
            this.columnPatronymic.Editable = false;
            this.columnPatronymic.Editor = null;
            resources.ApplyResources(this.columnPatronymic, "columnPatronymic");
            // 
            // columnPhone1
            // 
            this.columnPhone1.Editable = false;
            this.columnPhone1.Editor = null;
            resources.ApplyResources(this.columnPhone1, "columnPhone1");
            this.columnPhone1.Width = 100;
            // 
            // columnPhone2
            // 
            this.columnPhone2.Editable = false;
            this.columnPhone2.Editor = null;
            resources.ApplyResources(this.columnPhone2, "columnPhone2");
            this.columnPhone2.Width = 100;
            // 
            // columnPhone3
            // 
            this.columnPhone3.Editable = false;
            this.columnPhone3.Editor = null;
            resources.ApplyResources(this.columnPhone3, "columnPhone3");
            this.columnPhone3.Width = 100;
            // 
            // table
            // 
            this.table.AccessibleDescription = null;
            this.table.AccessibleName = null;
            this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            resources.ApplyResources(this.table, "table");
            this.table.BackColor = System.Drawing.Color.White;
            this.table.BackgroundImage = null;
            this.table.ColumnModel = this.columnModel;
            this.table.EnableHeaderContextMenu = false;
            this.table.Font = null;
            this.table.FullRowSelect = true;
            this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.table.HeaderContextMenu = null;
            this.table.Name = "table";
            this.table.NoItemsText = "";
            this.table.TableModel = this.tableModel;
            this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler(this.table_SelectionChanged);
            // 
            // globalToolbar
            // 
            this.globalToolbar.AccessibleDescription = null;
            this.globalToolbar.AccessibleName = null;
            resources.ApplyResources(this.globalToolbar, "globalToolbar");
            this.globalToolbar.BackgroundImage = null;
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove});
            this.globalToolbar.Divider = false;
            this.globalToolbar.Font = null;
            this.globalToolbar.Name = "globalToolbar";
            // 
            // tbbNew
            // 
            resources.ApplyResources(this.tbbNew, "tbbNew");
            this.tbbNew.Name = "tbbNew";
            // 
            // tbbProperties
            // 
            resources.ApplyResources(this.tbbProperties, "tbbProperties");
            this.tbbProperties.Name = "tbbProperties";
            // 
            // separator2
            // 
            resources.ApplyResources(this.separator2, "separator2");
            this.separator2.Name = "separator2";
            this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbRemove
            // 
            resources.ApplyResources(this.tbbRemove, "tbbRemove");
            this.tbbRemove.Name = "tbbRemove";
            // 
            // cmdRemove
            // 
            this.cmdRemove.Instances.Add(this.ciRemoveToolBarButton);
            this.cmdRemove.Manager = this.commandManager;
            this.cmdRemove.Execute += new System.EventHandler(this.cmdRemove_Execute);
            this.cmdRemove.Update += new System.EventHandler(this.cmdRemove_Update);
            // 
            // ciRemoveToolBarButton
            // 
            this.ciRemoveToolBarButton.Item = this.tbbRemove;
            // 
            // cmdProperties
            // 
            this.cmdProperties.Instances.Add(this.ciPropertiesToolBarButton);
            this.cmdProperties.Manager = this.commandManager;
            this.cmdProperties.Execute += new System.EventHandler(this.cmdProperties_Execute);
            this.cmdProperties.Update += new System.EventHandler(this.cmdProperties_Update);
            // 
            // ciPropertiesToolBarButton
            // 
            this.ciPropertiesToolBarButton.Item = this.tbbProperties;
            // 
            // cmdNew
            // 
            this.cmdNew.Instances.Add(this.ciNewToolBarButton);
            this.cmdNew.Manager = this.commandManager;
            this.cmdNew.Execute += new System.EventHandler(this.cmdNew_Execute);
            // 
            // ciNewToolBarButton
            // 
            this.ciNewToolBarButton.Item = this.tbbNew;
            // 
            // OwnersTableControl
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this.table);
            this.Controls.Add(this.globalToolbar);
            this.Font = null;
            this.Name = "OwnersTableControl";
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion  // Component Designer generated code

		#region Properties

		private int selectedOwner;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedOwner
		{
			get { return this.selectedOwner; }
			set
			{
				if ( this.tableModel.ContainsId( value ) )
				{
					this.tableModel.Selections.SelectCell(
						this.tableModel.IndexOf( value ),
						0
						);

					this.selectedOwner = value;
				}
				else
				{
					this.tableModel.Selections.Clear();
				}
			}
		}

		private IOwnersEditorProvider editorProvider;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public IOwnersEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public GlobalToolbar ToolBar
		{
			get { return this.globalToolbar; }
		}

		[Browsable( true )]
		[DefaultValue( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public bool ToolBarVisible
		{
			get { return this.globalToolbar.Visible; }
			set { this.globalToolbar.Visible = value; }
		}

		#endregion  // Properties

		#region Actions

		private void ActionNew()
		{
			if ( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdOwners = ( (OwnersXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Properties( IdOwners );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int IdOwners = ( (OwnersXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Remove( IdOwners );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		#endregion  // Actions

		#region Events

		private EventHandler delegateSelectionChanged;

		public event EventHandler SelectionChanged
		{
			add { this.delegateSelectionChanged += value; }
			remove { this.delegateSelectionChanged -= value; }
		}

		protected virtual void OnSelectionChanged()
		{
			if ( this.delegateSelectionChanged != null )
			{
				this.delegateSelectionChanged( this, EventArgs.Empty );
			}
		}

		#endregion  // Events

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void table_SelectionChanged( object sender, SelectionEventArgs e )
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				this.selectedOwner = ( (OwnersXpRow)this.table.SelectedItems[0] ).GetId();
			}
			else
			{
				this.selectedOwner = 0;
			}

			this.OnSelectionChanged();
		}

		#endregion  // Handle controls events

		#region IViewObjectControl<OwnersXpRow> Members

		public void RowShow( ViewObjectEventArgs<OwnersXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<OwnersXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<OwnersXpRow> Members
	}
}