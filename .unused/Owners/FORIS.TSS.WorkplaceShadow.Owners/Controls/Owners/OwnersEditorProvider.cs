using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersEditorProvider :
		EditorProvider<IOwnersDataTreater, IOwnersData>,
		IOwnersEditorProvider,
		IDataItem<IOperatorData>
	{
		private IContainer components;
		private OperatorDataDispatcher operatorDataDispatcher;

		#region Constructor & Dispose

		public OwnersEditorProvider()
		{
			this.components = new Container();
			this.operatorDataDispatcher = new OperatorDataDispatcher( this.components );
		}
		public OwnersEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				this.components.Dispose();
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region IDataItem<IOperatorData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		IOperatorData IDataItem<IOperatorData>.Data
		{
			get { return this.operatorDataDispatcher.Data; }
			set { this.operatorDataDispatcher.Data = value; }
		}

		#endregion  //IDataItem<IOperatorData> Members

		protected override void New()
		{
			using ( OwnersPropertiesForm Form = new OwnersPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.New;

				this.operatorDataDispatcher.Ambassadors.Add( Form );
				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IOwnersDataTreater Treater =
							( (IDataSupplier<IOwnersDataTreater>)this.Data ).GetDataTreater();

						using ( Treater )
						{
							Treater.OwnersInsert(
								Form.Operator,
								Form.Surname,
								Form.OwnerName,
								Form.Patronimic,
								Form.Phone1,
								Form.Phone2,
								Form.Phone3
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OwnersEditorProvider_Exception_CanNotCreateNewOwner,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Properties( int idOwners )
		{
			using ( OwnersPropertiesForm Form = new OwnersPropertiesForm() )
			{
				Form.Mode = DataItemControlMode.Edit;
				Form.IdOwners = idOwners;

				this.operatorDataDispatcher.Ambassadors.Add( Form );
				this.dataDispatcher.Ambassadors.Add( Form );

				try
				{
					if ( Form.ShowDialog() == DialogResult.OK )
					{
						IOwnersDataTreater Treater =
							( (IDataSupplier<IOwnersDataTreater>)this.Data ).GetDataTreater();

						using ( Treater )
						{
							Treater.OwnersUpdate(
								idOwners,
								Form.Operator,
								Form.Surname,
								Form.OwnerName,
								Form.Patronimic,
								Form.Phone1,
								Form.Phone2,
								Form.Phone3
								);
						}
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OwnersEditorProvider_Exception_CanNotUpdateOwner,
							Ex.Message
							)
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( Form );
				}
			}
		}

		protected override void Remove( int idOwner )
		{
			if (
				MessageBox.Show(
					Strings.OwnersEditorProvider_Warning_AreYouSureToDeleteOwner,
					Strings.Warning,
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IOwnersDataTreater Treater =
						( (IDataSupplier<IOwnersDataTreater>)this.Data ).GetDataTreater();

					using ( Treater )
					{
						Treater.OwnersDelete( idOwner );
					}
				}
				catch ( Exception Ex )
				{
					MessageBox.Show(
						String.Format(
							Strings.OwnersEditorProvider_Exception_CanNotDeleteOwner,
							Ex.Message
							)
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idOwner )
		{
			throw new NotImplementedException();
		}
	}
}