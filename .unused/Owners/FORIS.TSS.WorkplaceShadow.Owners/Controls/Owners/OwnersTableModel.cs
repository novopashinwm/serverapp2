using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersTableModel :
		ViewObjectTableModel<OwnersXpRow>
	{
		#region Constructor & Dispose

		public OwnersTableModel( IContainer container )
		{
			container.Add( this );
		}
		public OwnersTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}