using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	internal class OwnersPropertiesForm :
		TssUserForm,
		IDataItem<IOwnersData>,
		IDataItem<IOperatorData>
	{
		#region Controls & Components

		private new IContainer components;
		private TextBox edtSurname;
		private TextBox edtPhone3;
		private TextBox tbPhone2;
		private TextBox edtPhone1;
		private TextBox edtPatronymic;
		private TextBox edtName;
		private Label lblSurname;
		private Label lblPhone3;
		private Label lblPhone2;
		private Label lblPhone1;
		private Label lblPatronymic;
		private Label lblName;
		private TableLayoutPanel tableLayoutPanelMain;
		private OperatorComboBox cbxOperator;
		private Label lblOperator;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public OwnersPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				this.components.Dispose();
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OwnersPropertiesForm));
            this.ownersDataDispatcher = new FORIS.TSS.Helpers.Owners.Data.Owners.OwnersDataDispatcher(this.components);
            this.edtSurname = new System.Windows.Forms.TextBox();
            this.edtName = new System.Windows.Forms.TextBox();
            this.edtPatronymic = new System.Windows.Forms.TextBox();
            this.edtPhone1 = new System.Windows.Forms.TextBox();
            this.tbPhone2 = new System.Windows.Forms.TextBox();
            this.edtPhone3 = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPatronymic = new System.Windows.Forms.Label();
            this.lblPhone1 = new System.Windows.Forms.Label();
            this.lblPhone2 = new System.Windows.Forms.Label();
            this.lblPhone3 = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.cbxOperator = new FORIS.TSS.WorkplaceShadow.Operator.Controls.Operator.OperatorComboBox();
            this.lblOperator = new System.Windows.Forms.Label();
            this.panelFill.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFill
            // 
            this.panelFill.AccessibleDescription = null;
            this.panelFill.AccessibleName = null;
            resources.ApplyResources(this.panelFill, "panelFill");
            this.panelFill.BackgroundImage = null;
            this.panelFill.Controls.Add(this.tableLayoutPanelMain);
            this.panelFill.Font = null;
            // 
            // edtSurname
            // 
            this.edtSurname.AccessibleDescription = null;
            this.edtSurname.AccessibleName = null;
            resources.ApplyResources(this.edtSurname, "edtSurname");
            this.edtSurname.BackgroundImage = null;
            this.edtSurname.Font = null;
            this.edtSurname.Name = "edtSurname";
            // 
            // edtName
            // 
            this.edtName.AccessibleDescription = null;
            this.edtName.AccessibleName = null;
            resources.ApplyResources(this.edtName, "edtName");
            this.edtName.BackgroundImage = null;
            this.edtName.Font = null;
            this.edtName.Name = "edtName";
            // 
            // edtPatronymic
            // 
            this.edtPatronymic.AccessibleDescription = null;
            this.edtPatronymic.AccessibleName = null;
            resources.ApplyResources(this.edtPatronymic, "edtPatronymic");
            this.edtPatronymic.BackgroundImage = null;
            this.edtPatronymic.Font = null;
            this.edtPatronymic.Name = "edtPatronymic";
            // 
            // edtPhone1
            // 
            this.edtPhone1.AccessibleDescription = null;
            this.edtPhone1.AccessibleName = null;
            resources.ApplyResources(this.edtPhone1, "edtPhone1");
            this.edtPhone1.BackgroundImage = null;
            this.edtPhone1.Font = null;
            this.edtPhone1.Name = "edtPhone1";
            // 
            // tbPhone2
            // 
            this.tbPhone2.AccessibleDescription = null;
            this.tbPhone2.AccessibleName = null;
            resources.ApplyResources(this.tbPhone2, "tbPhone2");
            this.tbPhone2.BackgroundImage = null;
            this.tbPhone2.Font = null;
            this.tbPhone2.Name = "tbPhone2";
            // 
            // edtPhone3
            // 
            this.edtPhone3.AccessibleDescription = null;
            this.edtPhone3.AccessibleName = null;
            resources.ApplyResources(this.edtPhone3, "edtPhone3");
            this.edtPhone3.BackgroundImage = null;
            this.edtPhone3.Font = null;
            this.edtPhone3.Name = "edtPhone3";
            // 
            // lblSurname
            // 
            this.lblSurname.AccessibleDescription = null;
            this.lblSurname.AccessibleName = null;
            resources.ApplyResources(this.lblSurname, "lblSurname");
            this.lblSurname.Font = null;
            this.lblSurname.Name = "lblSurname";
            // 
            // lblName
            // 
            this.lblName.AccessibleDescription = null;
            this.lblName.AccessibleName = null;
            resources.ApplyResources(this.lblName, "lblName");
            this.lblName.Font = null;
            this.lblName.Name = "lblName";
            // 
            // lblPatronymic
            // 
            this.lblPatronymic.AccessibleDescription = null;
            this.lblPatronymic.AccessibleName = null;
            resources.ApplyResources(this.lblPatronymic, "lblPatronymic");
            this.lblPatronymic.Font = null;
            this.lblPatronymic.Name = "lblPatronymic";
            // 
            // lblPhone1
            // 
            this.lblPhone1.AccessibleDescription = null;
            this.lblPhone1.AccessibleName = null;
            resources.ApplyResources(this.lblPhone1, "lblPhone1");
            this.lblPhone1.Font = null;
            this.lblPhone1.Name = "lblPhone1";
            // 
            // lblPhone2
            // 
            this.lblPhone2.AccessibleDescription = null;
            this.lblPhone2.AccessibleName = null;
            resources.ApplyResources(this.lblPhone2, "lblPhone2");
            this.lblPhone2.Font = null;
            this.lblPhone2.Name = "lblPhone2";
            // 
            // lblPhone3
            // 
            this.lblPhone3.AccessibleDescription = null;
            this.lblPhone3.AccessibleName = null;
            resources.ApplyResources(this.lblPhone3, "lblPhone3");
            this.lblPhone3.Font = null;
            this.lblPhone3.Name = "lblPhone3";
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AccessibleDescription = null;
            this.tableLayoutPanelMain.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanelMain, "tableLayoutPanelMain");
            this.tableLayoutPanelMain.BackgroundImage = null;
            this.tableLayoutPanelMain.Controls.Add(this.edtPhone3, 1, 6);
            this.tableLayoutPanelMain.Controls.Add(this.lblPhone3, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.tbPhone2, 1, 5);
            this.tableLayoutPanelMain.Controls.Add(this.lblSurname, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.edtPhone1, 1, 4);
            this.tableLayoutPanelMain.Controls.Add(this.lblPhone2, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.edtPatronymic, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.lblName, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.edtName, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.lblPhone1, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.edtSurname, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.lblPatronymic, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.cbxOperator, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.lblOperator, 0, 0);
            this.tableLayoutPanelMain.Font = null;
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            // 
            // cbxOperator
            // 
            this.cbxOperator.AccessibleDescription = null;
            this.cbxOperator.AccessibleName = null;
            resources.ApplyResources(this.cbxOperator, "cbxOperator");
            this.cbxOperator.BackgroundImage = null;
            this.cbxOperator.Font = null;
            this.cbxOperator.Name = "cbxOperator";
            // 
            // lblOperator
            // 
            this.lblOperator.AccessibleDescription = null;
            this.lblOperator.AccessibleName = null;
            resources.ApplyResources(this.lblOperator, "lblOperator");
            this.lblOperator.Font = null;
            this.lblOperator.Name = "lblOperator";
            // 
            // OwnersPropertiesForm
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = null;
            this.Name = "OwnersPropertiesForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OwnersPropertiesForm_FormClosing);
            this.panelFill.ResumeLayout(false);
            this.panelFill.PerformLayout();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion  // Windows Form Designer generated code

		#region IDataItem<IOwnersData> Members

		private OwnersDataDispatcher ownersDataDispatcher;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IOwnersData Data
		{
			get { return this.ownersDataDispatcher.Data; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.ownersDataDispatcher.Data = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion  // IDataItem<IOwnersData> Members

		#region IDataItem<IOperatorData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		IOperatorData IDataItem<IOperatorData>.Data
		{
			get { return this.cbxOperator.Data; }
			set { this.cbxOperator.Data = value; }
		}

		#endregion  // IDataItem<IOperatorData> Members

		#region Data

		private OwnersRow rowOwners;

		protected virtual void OnBeforeSetData()
		{
			if ( this.rowOwners != null )
			{
				this.rowOwners.AfterChange -= rowOwners_AfterChange;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if ( this.rowOwners != null )
			{
				this.rowOwners.AfterChange += rowOwners_AfterChange;
			}
		}

		private void rowOwners_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.Enabled = false;

			#region Clear controls

			this.cbxOperator.SelectedId = 0;
			this.edtSurname.Text		= string.Empty;
			this.edtName.Text			= string.Empty;
			this.edtPatronymic.Text		= string.Empty;
			this.edtPhone1.Text			= string.Empty;
			this.tbPhone2.Text			= string.Empty;
			this.edtPhone3.Text			= string.Empty;

			#endregion  // Clear controls

			this.rowOwners = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowOwners = this.Data.Owners.FindRow( this.idOwners );
		}

		private void OnUpdateView()
		{
			switch ( this.Mode )
			{
				#region Edit

				case DataItemControlMode.Edit:

					if ( this.rowOwners == null )
					{
						throw new ApplicationException( "No data for edit " );
					}

					this.cbxOperator.SelectedId = this.rowOwners.Operator;
					this.edtSurname.Text		= this.rowOwners.Surname;
					this.edtName.Text			= this.rowOwners.Name;
					this.edtPatronymic.Text		= this.rowOwners.Patronimyc;
					this.edtPhone1.Text			= this.rowOwners.Phone1;
					this.tbPhone2.Text			= this.rowOwners.Phone2;
					this.edtPhone3.Text			= this.rowOwners.Phone3;

					break;

				#endregion // Edit

				#region New

				case DataItemControlMode.New:

					this.cbxOperator.SelectedId = 0;
					this.edtSurname.Text		= string.Empty;
					this.edtName.Text			= string.Empty;
					this.edtPatronymic.Text		= string.Empty;
					this.edtPhone1.Text			= string.Empty;
					this.tbPhone2.Text			= string.Empty;
					this.edtPhone3.Text			= string.Empty;

					break;

				#endregion // New
			}
		}

		#endregion // View

		#region Properties

		private DataItemControlMode mode;
		[Browsable( false )]
		[DefaultValue( DataItemControlMode.Undefined )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if ( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if ( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idOwners;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int IdOwners
		{
			get { return this.idOwners; }
			set
			{
				if ( this.idOwners != value )
				{
					if ( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idOwners = value;

					if ( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int Operator
		{
			get { return this.cbxOperator.SelectedId; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Surname
		{
			get { return this.edtSurname.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string OwnerName
		{
			get { return this.edtName.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Patronimic
		{
			get { return this.edtPatronymic.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Phone1
		{
			get { return this.edtPhone1.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Phone2
		{
			get { return this.tbPhone2.Text; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string Phone3
		{
			get { return this.edtPhone3.Text; }
		}

		#endregion  // Properties

		#region Handle controls' events

		private void OwnersPropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if ( this.cbxOperator.SelectedId == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OwnersPropertiesForm_Warning_OperatorRequired );
				}

				if ( this.Surname.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OwnersPropertiesForm_Warning_SurnameRequired );
				}

				if ( this.OwnerName.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OwnersPropertiesForm_Warning_NameRequired );
				}

				if ( this.Phone1.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( Strings.OwnersPropertiesForm_Warning_PhoneRequired );
				}

				if ( Warnings.Count > 0 )
				{
					MessageBox.Show(
						string.Format( Strings.Message_DataErrors, Warnings.ToString() )
						);
				}
			}
		}

		#endregion  // Handle controls' events
	}
}