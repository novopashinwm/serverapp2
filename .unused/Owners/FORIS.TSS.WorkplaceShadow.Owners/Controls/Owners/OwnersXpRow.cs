using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Owners
{
	public class OwnersXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellOperatorName;
		private readonly Cell cellSurname;
		private readonly Cell cellName;
		private readonly Cell cellPatronimyc;
		private readonly Cell cellPhone1;
		private readonly Cell cellPhone2;
		private readonly Cell cellPhone3;

		#endregion  // Cells

		private readonly int ID;

		public OwnersXpRow( 
			int id,
			string operatorName,
			string surname,
			string name,
			string patronimyc,
			string phone1,
			string phone2,
			string phone3
			)
		{
			this.ID = id;

			#region Cells

			this.cellOperatorName	= new Cell();
            this.cellSurname		= new Cell();
            this.cellName			= new Cell();
            this.cellPatronimyc		= new Cell();
            this.cellPhone1			= new Cell();
            this.cellPhone2			= new Cell();
            this.cellPhone3			= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellOperatorName,
						this.cellSurname,
						this.cellName,
						this.cellPatronimyc,
						this.cellPhone1,
						this.cellPhone2,
						this.cellPhone3
					}
				);

			#endregion  // Cells

			this.cellOperatorName.Text	= operatorName;
			this.cellSurname.Text		= surname;
			this.cellName.Text			= name;
			this.cellPatronimyc.Text	= patronimyc;
			this.cellPhone1.Text		= phone1;
			this.cellPhone2.Text		= phone2;
			this.cellPhone3.Text		= phone3;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}