﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class OwnerMobileEditorProvider :
		EditorProvider<IOwnersDataTreater, IOwnersData>,
		IOwnerMobileEditorProvider
	{
		#region Constructor & Dispose

		public OwnerMobileEditorProvider()
		{
		}
		public OwnerMobileEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		#region EditorProvider<IOwnersDataTreater, IOwnersData> Members

		protected override void New()
		{
			throw new NotImplementedException();
		}

		protected override void Properties( int idOwners )
		{
			throw new NotImplementedException();
		}

		protected override void Remove( int idOwner )
		{
			throw new NotImplementedException();
		}

		protected override ISecureObject GetSecureObject( int idOwner )
		{
			throw new NotImplementedException();
		}

		#endregion  // EditorProvider<IOwnersDataTreater, IOwnersData> Members

		#region IMobileEditorProvider Members

		public void Check( int ownerId, int mobileId )
		{
			try
			{
				IOwnersDataTreater treater =
					( (IDataSupplier<IOwnersDataTreater>)this.Data ).GetDataTreater();

				using ( treater )
				{
					treater.OwnerMobileInsert(
						ownerId,
						mobileId
						);
				}
			}
			catch ( Exception Ex )
			{
				MessageBox.Show(
					String.Format(
						Strings.OwnerMobileEditorProvider_Exception_CanNotCreateNewOwnerMobile,
						Ex.Message
						)
					);
			}
		}

		public void Uncheck( int id )
		{
			try
			{
				IOwnersDataTreater treater =
					( (IDataSupplier<IOwnersDataTreater>)this.Data ).GetDataTreater();

				using ( treater )
				{
					treater.OwnerMobileDelete( id );
				}
			}
			catch ( Exception Ex )
			{
				MessageBox.Show(
					String.Format(
						Strings.OwnerMobileEditorProvider_Exception_CanNotDeleteOwnerMobile,
						Ex.Message
						)
					);
			}
		}

		#endregion
	}
}