﻿using System;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class MobileXpRowFactory :
		ViewObjectFactory<MobileRow, MobileXpRow, IOwnersData>
	{
		public override MobileXpRow CreateViewObject( int viewObjectId, MobileRow mobileRow )
		{
			#region Preconditions

			if ( this.Data == null ) throw new ApplicationException( "There is no data to create new item" );

			#endregion  // Preconditions

			return
				new MobileXpRow(
					viewObjectId,
					false,
					mobileRow.Name,
					mobileRow.RowController == null
						? String.Empty
						: mobileRow.RowController.Number.ToString(),
					0
					);
		}
	}
}