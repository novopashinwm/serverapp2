﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class OwnerMobileFilter :
		Filter<MobileRow, IOwnersData>,
		IFilter<OwnerMobileRow>
	{

		#region IFilter<OwnerMobileRow>

		private readonly DictionaryWithEvents<int, OwnerMobileRow> ownerMobileDataObjects =
			new DictionaryWithEvents<int, OwnerMobileRow>();

		#region dataObjects event handlers

		void rows_Inserting( object sender, HashChangeEventArgs<int, OwnerMobileRow> e )
		{
			this.OnRowShow( e.Key, e.Value );
		}

		void rows_Removed( object sender, HashChangeEventArgs<int, OwnerMobileRow> e )
		{
			this.OnRowHide( e.Key );
		}

		void rows_Clearing( object sender, HashEventArgs<int, OwnerMobileRow> e )
		{
			this.OnRowsClear();
		}

		#endregion  // dataObjects event handlers

		#region Show & Hide

		private DataObjectEventHandler<OwnerMobileRow> rowShow;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		event DataObjectEventHandler<OwnerMobileRow> IFilter<OwnerMobileRow>.RowShow
		{
			add
			{
				foreach ( KeyValuePair<int, OwnerMobileRow> pair in this.ownerMobileDataObjects )
				{
					value( new DataObjectEventArgs<OwnerMobileRow>( pair.Key, pair.Value ) );
				}

				this.rowShow += value;
			}
			remove { this.rowShow -= value; }
		}

		private void OnRowShow( int id, OwnerMobileRow item )
		{
			if ( this.rowShow != null )
			{
				this.rowShow( new DataObjectEventArgs<OwnerMobileRow>( id, item ) );
			}
		}

		private DataObjectEventHandler<OwnerMobileRow> rowHide;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		event DataObjectEventHandler<OwnerMobileRow> IFilter<OwnerMobileRow>.RowHide
		{
			add { this.rowHide += value; }

			remove
			{
				this.rowHide -= value;

				foreach ( int key in this.ownerMobileDataObjects.Keys )
				{
					value( new DataObjectEventArgs<OwnerMobileRow>( key, default( OwnerMobileRow ) ) );
				}
			}
		}

		private void OnRowHide( int id )
		{
			if ( this.rowHide != null )
			{
				this.rowHide( new DataObjectEventArgs<OwnerMobileRow>( id, default( OwnerMobileRow ) ) );
			}
		}

		private EventHandler rowsClear;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		event EventHandler IFilter<OwnerMobileRow>.RowsClear
		{
			add { this.rowsClear += value; }
			remove { this.rowsClear -= value; }
		}

		private void OnRowsClear()
		{
			if ( this.rowsClear != null )
			{
				this.rowsClear( this, null );
			}
		}

		#endregion  // Show & Hide

		#endregion  // IFilter<OwnerMobileRow>

		#region Constructor & Dispose

		public OwnerMobileFilter()
		{
			this.ownerMobileDataObjects.Inserting	+= rows_Inserting;
			this.ownerMobileDataObjects.Removed		+= rows_Removed;
			this.ownerMobileDataObjects.Clearing	+= rows_Clearing;
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				this.ownerMobileDataObjects.Inserting	-= rows_Inserting;
				this.ownerMobileDataObjects.Removed		-= rows_Removed;
				this.ownerMobileDataObjects.Clearing	-= rows_Clearing;
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Mobile.Rows.Inserted -= this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing -= this.MobileRows_Removing;

			this.Data.OwnerMobile.Rows.Inserted -= this.OwnersMobileRows_Inserted;
			this.Data.OwnerMobile.Rows.Removing -= this.OwnersMobileRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.Mobile.Rows.Inserted += this.MobileRows_Inserted;
			this.Data.Mobile.Rows.Removing += this.MobileRows_Removing;

			this.Data.OwnerMobile.Rows.Inserted += this.OwnersMobileRows_Inserted;
			this.Data.OwnerMobile.Rows.Removing += this.OwnersMobileRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void MobileRows_Inserted( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			if ( this.Check( e.Item ) )
			{
				this.dataObjects.Add( e.Item.ID, e.Item );
			}
		}
		void MobileRows_Removing( object sender, CollectionChangeEventArgs<MobileRow> e )
		{
			if ( this.dataObjects.ContainsKey( e.Item.ID ) )
			{
				this.dataObjects.Remove( e.Item.ID );
			}
		}

		void OwnersMobileRows_Inserted( object sender, CollectionChangeEventArgs<OwnerMobileRow> e )
		{
			if ( e.Item.Owner == this.ownerId )
			{
				this.dataObjects.Remove( e.Item.Mobile );
				this.ownerMobileDataObjects.Add( e.Item.Mobile, e.Item );
			}
		}
		void OwnersMobileRows_Removing( object sender, CollectionChangeEventArgs<OwnerMobileRow> e )
		{
			if ( e.Item.Owner == this.ownerId )
			{
				this.ownerMobileDataObjects.Remove( e.Item.Mobile );
				this.dataObjects.Add( e.Item.Mobile, e.Item.RowMobile );
			}
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
			this.ownerMobileDataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( MobileRow row in this.Data.Mobile.Rows )
			{
				if ( this.Check( row ) )
				{
					this.dataObjects.Add( row.ID, row );
				}
			}

			foreach ( OwnerMobileRow row in this.Data.OwnerMobile.Rows )
			{
				if ( row.Owner == this.ownerId )
				{
					this.ownerMobileDataObjects.Add( row.RowMobile.ID, row );
				}
			}
		}

		#endregion  // View

		#region Properties

		private int ownerId;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int OwnerId
		{
			get { return this.ownerId; }
			set
			{
				if ( this.ownerId != value )
				{
					if ( this.Data != null )
					{
						this.OnDestroyView();
					}

					this.ownerId = value;

					if ( this.Data != null )
					{
						this.OnBuildView();
					}
				}
			}
		}

		#endregion  // Properties

		private bool Check( MobileRow row )
		{
			if ( this.Data.OwnerMobile.OwnersByMobile.ContainsKey( row.ID ) )
			{
				List<OwnersRow> owners = this.Data.OwnerMobile.OwnersByMobile[row.ID];

				foreach ( OwnersRow owner in owners )
				{
					if ( owner.ID == this.ownerId ) return false;
				}
			}

			return true;
		}
	}
}