﻿using System;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class OwnerMobileXpRowFactory :
		ViewObjectFactory<OwnerMobileRow, MobileXpRow, IOwnersData>
	{
		public override MobileXpRow CreateViewObject( int viewObjectId, OwnerMobileRow ownerMobileRow )
		{
			#region Preconditions

			if ( this.Data == null ) throw new ApplicationException( "There is no data to create new item" );

			#endregion  // Preconditions

			return
				new MobileXpRow(
					viewObjectId,
					true,
					ownerMobileRow.RowMobile.Name,
					ownerMobileRow.RowMobile.RowController == null
						? String.Empty
						: ownerMobileRow.RowMobile.RowController.Number.ToString(),
					ownerMobileRow.ID
					);
		}
	}
}