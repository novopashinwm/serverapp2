﻿using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class MobileTableModel :
		ViewObjectTableModel<MobileXpRow>
	{
		#region Constructor & Dispose

		public MobileTableModel( IContainer container )
		{
			container.Add( this );
		}
		public MobileTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}