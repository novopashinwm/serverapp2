﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class MobileTableControl :
		TssUserControl,
		IViewObjectControl<MobileXpRow>
	{
		#region Controls & Components

		private IContainer components;
		private ColumnModel columnModel;
		private CheckBoxColumn columnCheck;
		private TextColumn columnNumber;
		private MobileTableModel tableModel;
		private Table table;
		private TextColumn columnName;

		#endregion  // Controls & Components

		#region Constructor & Dispose

		public MobileTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MobileTableControl));
            this.tableModel = new FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile.MobileTableModel(this.components);
            this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
            this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
            this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnNumber = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.SuspendLayout();
            // 
            // tableModel
            // 
            this.tableModel.SelectionAdjuster = null;
            // 
            // columnModel
            // 
            this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnNumber});
            // 
            // columnCheck
            // 
            this.columnCheck.Alignment = FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnAlignment.Center;
            this.columnCheck.Editable = false;
            this.columnCheck.Editor = null;
            this.columnCheck.Sortable = false;
            resources.ApplyResources(this.columnCheck, "columnCheck");
            this.columnCheck.Width = 20;
            // 
            // columnName
            // 
            this.columnName.Editable = false;
            this.columnName.Editor = null;
            resources.ApplyResources(this.columnName, "columnName");
            this.columnName.Width = 100;
            // 
            // columnNumber
            // 
            this.columnNumber.Editable = false;
            this.columnNumber.Editor = null;
            resources.ApplyResources(this.columnNumber, "columnNumber");
            this.columnNumber.Width = 100;
            // 
            // table
            // 
            this.table.AccessibleDescription = null;
            this.table.AccessibleName = null;
            this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            resources.ApplyResources(this.table, "table");
            this.table.BackColor = System.Drawing.Color.White;
            this.table.BackgroundImage = null;
            this.table.ColumnModel = this.columnModel;
            this.table.EnableHeaderContextMenu = false;
            this.table.Font = null;
            this.table.FullRowSelect = true;
            this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.table.HeaderContextMenu = null;
            this.table.Name = "table";
            this.table.NoItemsText = "";
            this.table.TableModel = this.tableModel;
            this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler(this.table_SelectionChanged);
            // 
            // MobileTableControl
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this.table);
            this.Font = null;
            this.Name = "MobileTableControl";
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion  // Component Designer generated code

		#region Events

		private EventHandler delegateSelectionChanged;

		public event EventHandler SelectionChanged
		{
			add { this.delegateSelectionChanged += value; }
			remove { this.delegateSelectionChanged -= value; }
		}

		protected virtual void OnSelectionChanged()
		{
			if ( this.delegateSelectionChanged != null )
			{
				this.delegateSelectionChanged( this, EventArgs.Empty );
			}
		}

		#endregion  // Events

		#region Handle controls' events

		private void table_SelectionChanged( object sender, SelectionEventArgs e )
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				this.selectedMobile = ( (MobileXpRow)this.table.SelectedItems[0] ).GetId();
			}
			else
			{
				this.selectedMobile = 0;
			}

			this.OnSelectionChanged();
		}

		#endregion  // Handle controls events

		#region IViewObjectControl<MobileXpRow> Members

		public void RowShow( ViewObjectEventArgs<MobileXpRow> e )
		{
			this.tableModel.RowShow( e );

			e.ViewObject.CheckedChanged += ViewObject_CheckedChanged;

			if ( this.checkedChanged )
			{
				this.table.Sort();
				this.table.Sort();
				this.checkedChanged = false;
			}
		}

		public void RowHide( ViewObjectEventArgs<MobileXpRow> e )
		{
			MobileXpRow row = (MobileXpRow)this.tableModel.Rows[this.tableModel.IndexOf( e.Id )];
			row.CheckedChanged -= ViewObject_CheckedChanged;

			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion  // IViewObjectControl<MobileXpRow> Members

		#region Properties

		private int selectedOwner;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedOwner
		{
			get { return this.selectedOwner; }
			set
			{
				this.selectedOwner = value;

				this.columnCheck.Editable = this.selectedOwner != 0;
			}
		}

		private int selectedMobile;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int SelectedMobile
		{
			get { return this.selectedMobile; }
			set
			{
				if ( this.tableModel.ContainsId( value ) )
				{
					this.tableModel.Selections.SelectCell(
						this.tableModel.IndexOf( value ),
						0
						);

					this.selectedMobile = value;
				}
				else
				{
					this.tableModel.Selections.Clear();
				}
			}
		}

		private IOwnerMobileEditorProvider editorProvider;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public IOwnerMobileEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public Table Table
		{
			get { return this.table; }
		}

		#endregion  // Properties

		private bool checkedChanged;
		void ViewObject_CheckedChanged( object sender, EventArgs e )
		{
			this.checkedChanged = true;

			MobileXpRow row = (MobileXpRow)sender;

			if ( row.Checked )
			{
				this.editorProvider.Check( this.selectedOwner, row.GetId() );
			}
			else
			{
				this.editorProvider.Uncheck( row.OwnerMobileId );
			}
		}
	}
}