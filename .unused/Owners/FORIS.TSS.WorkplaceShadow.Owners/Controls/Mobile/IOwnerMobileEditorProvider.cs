﻿using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public interface IOwnerMobileEditorProvider :
		IEditorProvider
	{
		void Check( int ownerId, int mobileId );
		void Uncheck( int id );
	}
}