﻿using System;
using System.ComponentModel;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Owners.Controls.Mobile
{
	public class MobileXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellCheck;
		private readonly Cell cellName;
		private readonly Cell cellNumber;

		#endregion // Cells

		private readonly int ID;

		public MobileXpRow( 
			int id,
			bool checkValue,
			string mobileName,
			string controllerNumber,
			int ownerMobileId
			)
		{
			this.ID = id;
			this.OwnerMobileId = ownerMobileId;

			#region Cells

			this.cellCheck	= new Cell();
            this.cellName	= new Cell();
            this.cellNumber	= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellCheck,
						this.cellName,
						this.cellNumber,
					}
				);

			#endregion  // Cells

			this.cellCheck.Checked	= checkValue;
			this.cellName.Text		= mobileName;
			this.cellNumber.Text	= controllerNumber;

			this.cellCheck.PropertyChanged += this.cellCheck_PropertyChanged;
		}

		#region IViewObject Members

		public int GetId()
		{
			return this.ID;
		}

		#endregion  // IViewObject Members

		#region Events

		private void cellCheck_PropertyChanged( object sender, CellEventArgs e )
		{
			if ( e.EventType == CellEventType.CheckStateChanged )
			{
				this.OnCheckedChanged();
			}
		}

		private EventHandler delegateCheckedChanged;
		[Browsable( false )]
		[DesignerSerializationVisibility( ( DesignerSerializationVisibility.Hidden ) )]
		public event EventHandler CheckedChanged
		{
			add { this.delegateCheckedChanged += value; }
			remove { this.delegateCheckedChanged -= value; }
		}

		private void OnCheckedChanged()
		{
			if ( this.delegateCheckedChanged != null )
			{
				this.delegateCheckedChanged( this, EventArgs.Empty );
			}
		}

		#endregion  // Events

		#region Properties

		[Browsable( false )]
		[DesignerSerializationVisibility( ( DesignerSerializationVisibility.Hidden ) )]
		public bool Checked
		{
			get { return this.cellCheck.Checked; }
			set { this.cellCheck.Checked = value; }
		}

		public readonly int OwnerMobileId;

		#endregion  // Properties
	}
}