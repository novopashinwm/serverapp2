using System.ComponentModel;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.ServerApplication.RuleManager;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Owners;

namespace FORIS.TSS.WorkplaceShadow.Owners.Data.Owners
{
    public class OwnersDataSupplier :
        ModuleDataSupplier<IOwnersClientDataProvider, IOwnersDataSupplier>,
        IOwnersDataSupplier
    {

        #region Constructor & Dispose

		public OwnersDataSupplier( IContainer container )
		{
			container.Add( this );
		}

        public OwnersDataSupplier()
		{
            this.ruleSecurityDataSupplier = new RuleSecurityDatabaseDataSupplier((IRuleServerDataProvider) this);
		}

		#endregion  // Constructor & Dispose

        protected override IOwnersDataSupplier GetDataSupplier()
        {
            return this.ClientProvider.GetOwnersClientData();
        }

        #region IOwnersDataSupplier Members

        public IRuleDataSupplier RuleDataSupplier
        {
            get
            {
            	return
            		this.dataSupplier != null
						? this.dataSupplier.RuleDataSupplier
            			: null;
            }
		}

		#endregion  // IOwnersDataSupplier Members

		#region IDataSupplier<IOwnersDataTreater> Members

		public IOwnersDataTreater GetDataTreater()
        {
            return this.dataSupplier.GetDataTreater();
		}

		#endregion  // IDataSupplier<IOwnersDataTreater> Members

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,IOperatorDataSupplier> Members

        private IRuleSecurityDataSupplier ruleSecurityDataSupplier;
        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.ruleSecurityDataSupplier; }
        }

        #endregion
    }
}