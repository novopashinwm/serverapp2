﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Owners;

namespace FORIS.TSS.WorkplaceShadow.Owners
{
	public class OwnersClientDispatcher : ClientDispatcher<IOwnersClientDataProvider>
	{
		public OwnersClientDispatcher()
		{

		}

		public OwnersClientDispatcher( IContainer container )
		{
			container.Add( this );
		}

		#region Ambassadors

		private readonly OwnersClientAmbassadorCollection ambassadors =
			new OwnersClientAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new OwnersClientAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem<IOwnersClientDataProvider>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors
	}

	public class OwnersClientAmbassadorCollection : AmbassadorCollection<IClientItem<IOwnersClientDataProvider>>
	{
		public new OwnersClientAmbassador this[int index]
		{
			get { return (OwnersClientAmbassador)base[index]; }
		}
	}

	public class OwnersClientAmbassador : Ambassador<IClientItem<IOwnersClientDataProvider>>
	{

	}
}