using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.RuleManager.Rule;
using FORIS.TSS.BusinessLogic.TspTerminal.Data.Controller;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.RuleManager.Data.Rule.Security;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.Interfaces.Owners.Data.Owners;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public class OwnersData :
	SecureData<IOwnersDataTreater>,
	IOwnersData
	{
		#region Controls & Components

		private IContainer components;
		private OwnersDataDispatcher tablesDataDispatcher;

		private RuleData ruleData;

		private OwnersTable			owners;
		private OwnerMobileTable	ownerMobile;
		private SMSLimitTable		smsLimit;
		private SMSLogTable			smsLog;
		private SMSTypeTable		smsType;

		private OwnersDataAmbassador daOwners;
		private OwnersDataAmbassador daOwnerMobile;
		private OwnersDataAmbassador daSMSLimit;
		private OwnersDataAmbassador daSMSLog;
		private OwnersDataAmbassador daSMSType;

		#endregion //Controls & Components

		#region Constructor & Dispose

		public OwnersData( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public OwnersData()
		{
            this.securityData = new RuleSecurityData(this);

			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new Container();
			this.tablesDataDispatcher = new OwnersDataDispatcher( this.components );

			this.ruleData = new RuleData( this.components );

			this.owners			= new OwnersTable		( this.components );
			this.ownerMobile	= new OwnerMobileTable	( this.components );
			this.smsLimit		= new SMSLimitTable		( this.components );
			this.smsLog			= new SMSLogTable		( this.components );
			this.smsType		= new SMSTypeTable		( this.components );

			this.daOwnerMobile	= new OwnersDataAmbassador();
			this.daOwners		= new OwnersDataAmbassador();
			this.daSMSLimit		= new OwnersDataAmbassador();
			this.daSMSLog		= new OwnersDataAmbassador();
			this.daSMSType		= new OwnersDataAmbassador();

			this.daOwnerMobile.Item = this.ownerMobile;
			this.daOwners.Item		= this.owners;
			this.daSMSLimit.Item 	= this.smsLimit;
			this.daSMSLog.Item 		= this.smsLog;
			this.daSMSType.Item 	= this.smsType;

			this.tablesDataDispatcher.Ambassadors.Add( this.daOwners );
			this.tablesDataDispatcher.Ambassadors.Add( this.daOwnerMobile );
			this.tablesDataDispatcher.Ambassadors.Add( this.daSMSLimit );
			this.tablesDataDispatcher.Ambassadors.Add( this.daSMSLog );
			this.tablesDataDispatcher.Ambassadors.Add( this.daSMSType );
		}

		#endregion  // Component Designer generated code

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IOwnersDataSupplier DataSupplier
		{
			get { return (IOwnersDataSupplier)base.DataSupplier; }
			set
			{
				if ( value != null )
				{
					this.ruleData.DataSupplier = value.RuleDataSupplier;
				}

				base.DataSupplier = value;

				if ( value == null )
				{
					this.ruleData.DataSupplier = null;
				}
			}
		}

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IInvokeBroker InvokeBroker
		{
			get { return base.InvokeBroker; }
			set
			{
				base.InvokeBroker =
                    this.securityData.InvokeBroker =
					this.ruleData.InvokeBroker = value;
			}
		}

		#endregion  // Properties

		#region Tables' order

		/// <summary>
		/// ������� ������
		/// </summary>
		/// <remarks>
		/// ������������ � ������ ApplyChanges
		/// ��� ����������� ������� ������ ���
		/// ��������/����������/��������� ������
		/// </remarks>
		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		/// <summary>
		/// ��������� �������
		/// </summary>
		protected override void Destroy()
		{
            this.securityData.DataSupplier = null;

			this.tablesDataDispatcher.Data = null;

			this.ruleData.DataSupplier = null;
		}

		/// <summary>
		/// ��������� �������
		/// </summary>
		/// <remarks>
		/// ��� ��� ������� ����� �� ����� ������� � ���� 
		/// ������������ ������, �� �� �� ����� � � ���� 
		/// ���������� ��������� ������ (���������� ���)
		/// </remarks>
		protected override void Build()
		{
			this.ruleData.DataSupplier = this.DataSupplier.RuleDataSupplier;

			this.tablesDataDispatcher.Data = this;

            this.securityData.DataSupplier = this.DataSupplier.SecurityDataSupplier;
		}

		#endregion  // Build & Destroy

		#region Tables properties

		#region Rule

		#region Controller

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DepartmentTable Department
		{
			get { return this.ruleData.Department; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public ControllerTypeTable ControllerType
		{
			get { return this.ruleData.ControllerType; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		public ControllerSensorTypeTable ControllerSensorType
		{
			get { return this.ruleData.ControllerSensorType; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		public ControllerSensorTable ControllerSensor
		{
			get { return this.ruleData.ControllerSensor; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public ControllerTable Controller
		{
			get { return this.ruleData.Controller; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		public ControllerSensorLegendTable ControllerSensorLegend
		{
			get { return this.ruleData.ControllerSensorLegend; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		public ControllerSensorMapTable ControllerSensorMap
		{
			get { return this.ruleData.ControllerSensorMap; }
		}

	    #endregion  // Controller

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public MobileObjectTypeTable MobileObjectType
		{
			get { return this.ruleData.MobileObjectType; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public MobileObjectRegisterTable MobileObjectRegister
		{
			get { return this.ruleData.MobileObjectRegister; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public RuleTable Rule
		{
			get { return this.ruleData.Rule; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public MobileTypeTable MobileType
		{
			get { return this.ruleData.MobileType; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public MobileTable Mobile
		{
			get { return this.ruleData.Mobile; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public PositionTable Position
		{
			get { return this.ruleData.Position; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public MobileRuleTable MobileRule
		{
			get { return this.ruleData.MobileRule; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public StateTable State
		{
			get { return this.ruleData.State; }
		}

		#endregion  // Rule

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public OwnersTable Owners
		{
			get { return this.owners; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public OwnerMobileTable OwnerMobile
		{
			get { return this.ownerMobile; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public SMSLimitTable SMSLimit
		{
			get { return this.smsLimit; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public SMSLogTable SMSLog
		{
			get { return this.smsLog; }
		}

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public SMSTypeTable SMSType
		{
			get { return this.smsType; }
		}

		#endregion //Tables properties

		#region IOwnersDataSupplier Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRuleDataSupplier RuleDataSupplier
		{
			get
			{
				return this.DataSupplier.RuleDataSupplier;
			}
		}
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IControllerDataSupplier ControllerDataSupplier
		{
			get
			{
				return this.DataSupplier.RuleDataSupplier.ControllerDataSupplier;
			}
		}

		IRuleDataTreater IDataSupplier<IRuleDataTreater>.GetDataTreater()
		{
			return this.RuleDataSupplier.GetDataTreater();
		}
		IControllerDataTreater IDataSupplier<IControllerDataTreater>.GetDataTreater()
		{
			return this.ControllerDataSupplier.GetDataTreater();
		}

		#endregion  // IOwnersDataSupplier

		#region IRuleDataSupplier Members

		#region IControllerData Members

		public event EventHandler DataChanged
		{
			add { this.ruleData.DataChanged += value; }
			remove { this.ruleData.DataChanged -= value; }
		}

		#endregion  // IControllerData Members

		public MobileRuleSettings CreateMobileRuleSettings(int ruleId)
		{
			return this.DataSupplier.RuleDataSupplier.CreateMobileRuleSettings( ruleId );
		}

		#endregion


        #region ISecureData<IRuleSecurityData> Members

        private readonly RuleSecurityData securityData;
        [Browsable(false)]
        public IRuleSecurityData SecurityData
        {
            get { return this.securityData; }
        }

        #endregion

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.securityData; }
        }

        #endregion
    }
}
