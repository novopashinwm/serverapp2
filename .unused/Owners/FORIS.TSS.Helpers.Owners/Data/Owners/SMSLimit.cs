using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public class SMSLimitTable :
		Table<SMSLimitRow, SMSLimitTable, IOwnersData>
	{
		#region Properties

		public override string Name
		{
			get { return "SMS_LIMIT"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public SMSLimitTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public SMSLimitTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int mobileIdIndex = -1;
		public int MobileIdIndex { get { return this.mobileIdIndex; } }

		private int limitIndex = -1;
		public int LimitIndex { get { return this.limitIndex; } }

		private int remainsIndex = -1;
		public int RemainsIndex { get { return this.remainsIndex; } }

		private int beginTimeIndex = -1;
		public int BeginTimeIndex { get { return this.beginTimeIndex; } }

		private int endTimeIndex = -1;
		public int EndTimeIndex { get { return endTimeIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex		= this.DataTable.Columns.IndexOf( "SMS_LIMIT_ID" );
				this.mobileIdIndex	= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
				this.limitIndex		= this.DataTable.Columns.IndexOf( "LIMIT" );
				this.remainsIndex	= this.DataTable.Columns.IndexOf( "REMAINS" );
				this.beginTimeIndex = this.DataTable.Columns.IndexOf( "BEGIN_TIME" );
				this.endTimeIndex	= this.DataTable.Columns.IndexOf( "END_TIME" );
			}
		}

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex		= -1;
			this.mobileIdIndex	= -1;
			this.limitIndex		= -1;
			this.remainsIndex	= -1;
			this.beginTimeIndex = -1;
			this.endTimeIndex	= -1;
		}

		#endregion  // View

		protected override SMSLimitRow OnCreateRow( DataRow dataRow )
		{
			return new SMSLimitRow( dataRow );
		}
	}

	public class SMSLimitRow : 
		Row<SMSLimitRow, SMSLimitTable, IOwnersData>
	{
		#region Fields

		/// <summary>
		/// ������ ��������
		/// </summary>
		public int Mobile
		{
			get { return (int)this.DataRow[this.Table.MobileIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.MobileIdIndex] = value; }
		}

		/// <summary>
		/// ������������ ���������� SMS
		/// </summary>
		public int Limit
		{
			get { return (int)this.DataRow[this.Table.LimitIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.LimitIndex] = value; }
		}

		/// <summary>
		/// ���������� ���������� SMS
		/// </summary>
		public int Remains
		{
			get { return (int)this.DataRow[this.Table.RemainsIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RemainsIndex] = value; }
		}

		/// <summary>
		/// ����� ������ �������� �����������
		/// </summary>
		public DateTime BeginTime
		{
			get { return (DateTime)this.DataRow[this.Table.BeginTimeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.BeginTimeIndex] = value; }
		}

		/// <summary>
		/// ����� ��������� �������� �����������
		/// </summary>
		public DateTime EndTime
		{
			get { return (DateTime)this.DataRow[this.Table.EndTimeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.EndTimeIndex] = value; }
		}

		#endregion //Fields

		#region Id

		/// <summary>
		/// 
		/// </summary>
		/// <returns>������������� ������</returns>
		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		#region Parent row

		private MobileRow rowMobile;
		public MobileRow RowMobile
		{
			get { return this.rowMobile; }
		}

		#endregion // Parent row

		#region View

		protected override void OnDestroyView()
		{
			this.rowMobile = null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowMobile = this.Data.Mobile.FindRow( this.Mobile );
		}

		#endregion // View

		public SMSLimitRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() {}

		public bool CheckLimit( DateTime time )
		{
			return
				this.BeginTime <= time 
				&& this.EndTime > time;
		}
	}
}