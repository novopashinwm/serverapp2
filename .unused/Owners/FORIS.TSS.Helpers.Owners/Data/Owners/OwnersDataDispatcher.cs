using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public class OwnersDataDispatcher : DataDispatcher<IOwnersData>
	{
		public OwnersDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public OwnersDataDispatcher()
		{

		}

		private readonly OwnersDataAmbassadorCollection ambassadors =
			new OwnersDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new OwnersDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IOwnersData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class OwnersDataAmbassadorCollection :
		DataAmbassadorCollection<IOwnersData>
	{
		public new OwnersDataAmbassador this[int index]
		{
			get { return (OwnersDataAmbassador)base[index]; }
		}
	}

	public class OwnersDataAmbassador :
		DataAmbassador<IOwnersData>
	{

	}
}