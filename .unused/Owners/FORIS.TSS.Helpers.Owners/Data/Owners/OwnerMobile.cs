using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
    public class OwnerMobileTable :
        Table<OwnerMobileRow, OwnerMobileTable, IOwnersData>
    {
		#region Properties

		public override string Name
        {
            get { return "OWNER_MOBILE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public OwnerMobileTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public OwnerMobileTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int ownerIdIndex = -1;
		public int OwnerIdIndex { get { return ownerIdIndex; } }

		private int mobileIdIndex = -1;
		public int MobileIdIndex { get { return mobileIdIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable != null)
            {
				this.idIndex		= this.DataTable.Columns.IndexOf( "OWNER_MOBILE_ID" );
				this.ownerIdIndex	= this.DataTable.Columns.IndexOf( "OWNER_ID" );
				this.mobileIdIndex	= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
            }
        }

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.idIndex		= -1;
			this.ownerIdIndex	= -1;
			this.mobileIdIndex	= -1;
        }

		#endregion  // View

		protected override OwnerMobileRow OnCreateRow( DataRow dataRow )
		{
			return new OwnerMobileRow( dataRow );
		}

		protected override void OnRowActionHide( OwnerMobileRow RowOwnerMobile )
		{
			base.OnRowActionHide( RowOwnerMobile );

			#region OwnersRow.RowsOwnerMobile

			OwnersRow RowOwners = this.Data.Owners.FindRow( RowOwnerMobile.Owner );

			if ( RowOwners != null )
			{
				RowOwners.RowsOwnerMobile.Remove( RowOwnerMobile );
			}
			else
				throw new ApplicationException( "There is no parent Owners row for OwnerMobile row" );

			#endregion  // OwnersRow.RowsOwnerMobile

			#region OwnersByMobile

			if ( this.ownersByMobile.ContainsKey( RowOwnerMobile.Mobile ) )
			{
				List<OwnersRow> owners = this.ownersByMobile[RowOwnerMobile.Mobile];
				owners.Remove( RowOwners );

				if ( owners.Count == 0 )
				{
					this.ownersByMobile.Remove( RowOwnerMobile.Mobile );
				}
			}

			#endregion  // OwnersByMobile
		}
		protected override void OnRowActionShow( OwnerMobileRow RowOwnerMobile )
		{
			#region OwnersRow.RowsOwnerMobile

			OwnersRow RowOwners = this.Data.Owners.FindRow( RowOwnerMobile.Owner );

			if ( RowOwners != null )
			{
				RowOwners.RowsOwnerMobile.Add( RowOwnerMobile );
			}
			else
				throw new ApplicationException( "There is no parent Owners row for OwnerMobile row" );

			#endregion  // OwnersRow.RowsOwnerMobile

			#region OwnersByMobile

			if ( this.ownersByMobile.ContainsKey( RowOwnerMobile.Mobile ) )
			{
				List<OwnersRow> owners = this.ownersByMobile[RowOwnerMobile.Mobile];
				owners.Add( RowOwners );
			}
			else
			{
				List<OwnersRow> owners = new List<OwnersRow>();
				owners.Add( RowOwners );

				this.ownersByMobile.Add( RowOwnerMobile.Mobile, owners );
			}

			#endregion  // OwnersByMobile

			base.OnRowActionShow( RowOwnerMobile );
		}

    	private readonly Dictionary<int, List<OwnersRow>> ownersByMobile =
    		new Dictionary<int, List<OwnersRow>>();

    	public Dictionary<int, List<OwnersRow>> OwnersByMobile
    	{
			get { return this.ownersByMobile; }
    	}
	}

    public class OwnerMobileRow : Row<OwnerMobileRow, OwnerMobileTable, IOwnersData>
    {
        #region Fields

        public int Owner
        {
            get { return (int)this.DataRow[this.Table.OwnerIdIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.OwnerIdIndex] = value; }
        }

        public int Mobile
        {
            get { return (int)this.DataRow[this.Table.MobileIdIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.MobileIdIndex] = value; }
        }

        #endregion //Fields

        #region Parent row

        private OwnersRow rowOwner;
        [Browsable(false)]
        public OwnersRow RowOwner
        {
            get { return this.rowOwner; }
        }

        private MobileRow rowMobile;
        [Browsable(false)]
        public MobileRow RowMobile
        {
            get { return this.rowMobile; }
        }

        #endregion // Parent row 

		#region Id

		/// <summary>
        /// 
        /// </summary>
        /// <returns>������������� ������</returns>
        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		#region View

		protected override void OnDestroyView()
		{
			this.rowOwner = null;
			this.rowMobile = null;

			base.OnDestroyView();
		}

		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowOwner = this.Data.Owners.FindRow( this.Owner );
			this.rowMobile = this.Data.Mobile.FindRow( this.Mobile );
		}

		#endregion  // View

		public OwnerMobileRow( DataRow dataRow )
			: base( dataRow )
		{
		}

		protected override void OnBuildFields()
        {
        }
    }
}