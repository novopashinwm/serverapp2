using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public class SMSTypeTable :
		Table<SMSTypeRow, SMSTypeTable, IOwnersData>
	{
		#region Properties

		public override string Name
		{
			get { return "SMS_TYPE"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public SMSTypeTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public SMSTypeTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int typeIndex = -1;
		public int TypeIndex { get { return this.typeIndex; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex	= this.DataTable.Columns.IndexOf( "SMS_TYPE_ID" );
				this.typeIndex	= this.DataTable.Columns.IndexOf( "TYPE" );
			}
		}

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex	= -1;
			this.typeIndex	= -1;
		}

		#endregion  // View

		protected override SMSTypeRow OnCreateRow( DataRow dataRow )
		{
			return new SMSTypeRow( dataRow );
		}
	}

	public class SMSTypeRow : 
		Row<SMSTypeRow, SMSTypeTable, IOwnersData>
	{
		#region Fields

		/// <summary>
		/// �������� ���� ���������
		/// </summary>
		public string Type
		{
			get { return (string)this.DataRow[this.Table.TypeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.TypeIndex] = value; }
		}

		#endregion //Fields

		#region Id

		/// <summary>
		/// 
		/// </summary>
		/// <returns>������������� ������</returns>
		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		public SMSTypeRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() {}
	}

	public enum SMSTypeEnum
	{
		None	= 0,
		Parking = 1,
		Moving	= 2,
		Address = 3,
		Service = 4,
		Alarm	= 5,
		Unprocessed = 6
	}
}