using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public interface IOwnersData :
		IData<IOwnersDataTreater>,
		IRuleData,
		IOwnersDataSupplier
	{
		OwnersTable			Owners		{ get; }
		OwnerMobileTable	OwnerMobile { get; }
		SMSLimitTable		SMSLimit	{ get; }
		SMSLogTable			SMSLog		{ get; }
		SMSTypeTable		SMSType		{ get; }
	}
}