using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
	public class SMSLogTable :
		Table<SMSLogRow, SMSLogTable, IOwnersData>
	{
		#region Properties

		public override string Name
		{
			get { return "SMS_LOG"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public SMSLogTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public SMSLogTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return this.idIndex; } }

		private int mobileIdIndex = -1;
		public int MobileIdIndex { get { return this.mobileIdIndex; } }

		private int ownerIdIndex = -1;
		public int OwnerIdIndex { get { return this.ownerIdIndex; } }

		private int receiveTimeIndex = -1;
		public int ReceiveTimeIndex { get { return this.receiveTimeIndex; } }

		private int smsTypeIdIndex = -1;
		public int SMSTypeIdIndex { get { return this.smsTypeIdIndex; } }

		private int replyTimeIndex = -1;
		public int ReplyTimeIndex { get { return this.replyTimeIndex; } }

		private int replyTextIndex = -1;
		public int ReplyTextIndex { get { return this.replyTextIndex; } }

		#endregion  // Fields' indices

		#region Data

		/* 
		 * ��������� ������ ������� �������� ������� ����� ������, �� ������
		 * �� ��������� �� �� ���� � ������ � ��� �� �������.
		 */

		protected override void OnBeforeSetData()
		{
			this.Rows.Inserted -= this.Rows_Inserted;

			base.OnBeforeSetData();
		}

		protected override void OnAfterSetData()
		{
			base.OnAfterSetData();

			this.Rows.Inserted += this.Rows_Inserted;
		}

		void Rows_Inserted( object sender, CollectionChangeEventArgs<SMSLogRow> e )
		{
			if ( this.rows.Count > 1 )
			{
				List<SMSLogRow> unchangedRows = new List<SMSLogRow>();

				foreach ( SMSLogRow row in this.rows )
				{
					if ( row.DataRow.RowState == DataRowState.Unchanged )
					{
						unchangedRows.Add( row );
					}
				}

				foreach ( SMSLogRow row in unchangedRows )
				{
					this.DataTable.Rows.Remove( row.DataRow );
				}
			}
		}

		#endregion

		#region View

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex			= this.DataTable.Columns.IndexOf( "SMS_LOG_ID" );
				this.mobileIdIndex		= this.DataTable.Columns.IndexOf( "MOBILE_ID" );
				this.ownerIdIndex		= this.DataTable.Columns.IndexOf( "OWNER_ID" );
				this.receiveTimeIndex	= this.DataTable.Columns.IndexOf( "RECEIVE_TIME" );
				this.smsTypeIdIndex		= this.DataTable.Columns.IndexOf( "SMS_TYPE_ID" );
				this.replyTimeIndex		= this.DataTable.Columns.IndexOf( "REPLY_TIME" );
				this.replyTextIndex		= this.DataTable.Columns.IndexOf( "REPLY_TEXT" );
			}
		}

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex			= -1;
			this.mobileIdIndex		= -1;
			this.ownerIdIndex		= -1;
			this.receiveTimeIndex	= -1;
			this.smsTypeIdIndex		= -1;
			this.replyTimeIndex		= -1;
			this.replyTextIndex		= -1;
		}

		#endregion  // View

		protected override SMSLogRow OnCreateRow( DataRow dataRow )
		{
			return new SMSLogRow( dataRow );
		}
	}

	public class SMSLogRow : 
		Row<SMSLogRow, SMSLogTable, IOwnersData>
	{
		#region Fields

		/// <summary>
		/// ������ ��������
		/// </summary>
		public int Mobile
		{
			get { return (int)this.DataRow[this.Table.MobileIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.MobileIdIndex] = value; }
		}

		/// <summary>
		/// ��������
		/// </summary>
		public int Owner
		{
			get { return (int)this.DataRow[this.Table.OwnerIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OwnerIdIndex] = value; }
		}

		/// <summary>
		/// ����� ��������� ���������
		/// </summary>
		public DateTime ReceiveTime
		{
			get { return (DateTime)this.DataRow[this.Table.ReceiveTimeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReceiveTimeIndex] = value; }
		}

		/// <summary>
		/// ��� ���������
		/// </summary>
		public SMSTypeEnum SMSType
		{
			get { return (SMSTypeEnum)this.DataRow[this.Table.SMSTypeIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.SMSTypeIdIndex] = value; }
		}

		/// <summary>
		/// ����� ������
		/// </summary>
		public DateTime ReplyTime
		{
			get { return (DateTime)this.DataRow[this.Table.ReplyTimeIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReplyTimeIndex] = value; }
		}

		/// <summary>
		/// ����� ������
		/// </summary>
		public string ReplyText
		{
			get { return (string)this.DataRow[this.Table.ReplyTextIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReplyTextIndex] = value; }
		}

		#endregion //Fields

		#region Id

		/// <summary>
		/// 
		/// </summary>
		/// <returns>������������� ������</returns>
		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion  // Id

		public SMSLogRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() {}
	}
}