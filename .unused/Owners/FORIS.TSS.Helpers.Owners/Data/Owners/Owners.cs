using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Owners.Data.Owners
{
    public class OwnersTable :
        Table<OwnersRow, OwnersTable, IOwnersData>
	{
		#region Properties

		public override string Name
		{
			get { return "OWNERS"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public OwnersTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public OwnersTable()
		{

		}

		#endregion  // Constructor & Dispose

		#region Fields' indices

		private int idIndex = -1;
		public int IdIndex { get { return idIndex; } }

		private int operatorIdIndex = -1;
		public int OperatorIdIndex { get { return operatorIdIndex; } }

		private int surnameIndex = -1;
		public int SurnameIndex { get { return this.surnameIndex; } }

		private int nameIndex = -1;
		public int NameIndex { get { return this.nameIndex; } }

		private int patronimycIndex = -1;
		public int PatronimycIndex { get { return this.patronimycIndex; } }

		private int phone1Index = -1;
		public int Phone1Index { get { return phone1Index; } }

		private int phone2Index = -1;
		public int Phone2Index { get { return phone2Index; } }

		private int phone3Index = -1;
		public int Phone3Index { get { return phone3Index; } }

		#endregion  // Fields' indices

		#region View

		protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable != null)
            {
				this.idIndex			= this.DataTable.Columns.IndexOf( "OWNER_ID" );
				this.operatorIdIndex	= this.DataTable.Columns.IndexOf( "OPERATOR_ID" );
				this.surnameIndex		= this.DataTable.Columns.IndexOf( "SURNAME" );
                this.nameIndex			= this.DataTable.Columns.IndexOf( "NAME" );
                this.patronimycIndex	= this.DataTable.Columns.IndexOf( "PATRONYMIC" );
                this.phone1Index		= this.DataTable.Columns.IndexOf( "PHONE1" );
                this.phone2Index		= this.DataTable.Columns.IndexOf( "PHONE2" );
                this.phone3Index		= this.DataTable.Columns.IndexOf( "PHONE3" );
            }
        }

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

			this.idIndex			= -1;
			this.operatorIdIndex	= -1;
            this.surnameIndex		= -1;
            this.nameIndex			= -1;
            this.patronimycIndex	= -1;
            this.phone1Index		= -1;
            this.phone2Index		= -1;
            this.phone3Index		= -1;
        }

		#endregion  // View

		protected override OwnersRow OnCreateRow( DataRow dataRow )
		{
			return new OwnersRow( dataRow );
		}
/**/
		#region Hash by Operator

		protected override void OnRowActionHide( OwnersRow row )
		{
			List<OwnersRow> owners = this.ownersByOperator[row.Operator];
			owners.Remove( row );

			if ( owners.Count == 0 )
			{
				this.ownersByOperator.Remove( row.Operator );
			}

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( OwnersRow row )
		{
			base.OnRowActionShow( row );

			if ( this.ownersByOperator.ContainsKey( row.Operator ) )
			{
				List<OwnersRow> owners;
				owners = this.ownersByOperator[row.Operator];
				owners.Add( row );
			}
			else
			{
				List<OwnersRow> owners;
				owners = new List<OwnersRow>();
				owners.Add( row );

				this.ownersByOperator.Add( row.Operator, owners );
			}
		}

		private readonly Dictionary<int, List<OwnersRow>> ownersByOperator =
			new Dictionary<int, List<OwnersRow>>();

		public IDictionary<int, List<OwnersRow>> OwnersByOperator
    	{
    		get { return ownersByOperator; }
    	}

		#endregion  // Hash by Operator
/**/
	}

    public class OwnersRow : Row<OwnersRow, OwnersTable, IOwnersData>
    {
		#region Fields

		public int Operator
		{
			get { return (int)this.DataRow[this.Table.OperatorIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OperatorIdIndex] = value; }
		}

        /// <summary>
        /// �������
        /// </summary>
        public string Surname
        {
            get { return (string) this.DataRow[this.Table.SurnameIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.SurnameIndex] = value;}
        }

        /// <summary>
        /// ���
        /// </summary>
        public string Name
        {
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.NameIndex] = value; }
        }
        
        /// <summary>
        /// �������
        /// </summary>
        public string Patronimyc
        {
            get
            {
                return DataRow.IsNull(this.Table.PatronimycIndex) ? string.Empty :
                (string)this.DataRow[this.Table.PatronimycIndex, DataRowVersion.Current]; }
            set 
            { 
                this.DataRow[this.Table.PatronimycIndex] =
                        !string.IsNullOrEmpty(value) ? (object)value : DBNull.Value; ;
            }
        }

        public string Phone1
        {
            get { return (string)this.DataRow[this.Table.Phone1Index, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.Phone1Index] = value; } 
        }

        public string Phone2
        {
            get
            {
                return DataRow.IsNull(this.Table.Phone2Index) ? string.Empty : 
                    (string)this.DataRow[this.Table.Phone2Index, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.Phone2Index] = 
                    !string.IsNullOrEmpty(value) ? (object) value : DBNull.Value;
            }
        }

        public string Phone3
        {
            get
            {
                return DataRow.IsNull(this.Table.Phone3Index)?string.Empty:
                    (string)this.DataRow[this.Table.Phone3Index, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.Phone3Index] = 
                    !string.IsNullOrEmpty(value) ? (object)value : DBNull.Value; ;
            }
        }

		#endregion //Fields

		#region Child rows

		private readonly ChildRowCollection<OwnerMobileRow, OwnerMobileTable, IOwnersData> rowsOwnerMobile;

		public ChildRowCollection<OwnerMobileRow, OwnerMobileTable, IOwnersData> RowsOwnerMobile
		{
			get { return this.rowsOwnerMobile; }
		}

		#endregion // Child rows

		#region Id

		/// <summary>
        /// 
        /// </summary>
        /// <returns>������������� ������</returns>
        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

		#endregion  // Id

		public OwnersRow( DataRow dataRow ) : 
			base( dataRow )
		{
			this.rowsOwnerMobile = 
				new ChildRowCollection<OwnerMobileRow, OwnerMobileTable, IOwnersData>();
		}

		protected override void OnBuildFields() {}
    }
}