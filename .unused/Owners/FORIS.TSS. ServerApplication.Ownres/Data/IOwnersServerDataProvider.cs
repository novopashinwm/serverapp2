using FORIS.TSS.BusinessLogic.RuleManager.Data;
using FORIS.TSS.ServerApplication.Owners.Data.Owners;
using FORIS.TSS.ServerApplication.RuleManager;

namespace FORIS.TSS.ServerApplication.Owners.Data
{
    public interface IOwnersServerDataProvider : 
        IDatabaseProvider,
        IRuleDataSupplierProvider,
		IRuleServerDataProvider
    {
        OwnersServerData OwnersData{ get; }
	}
}