using System;
using FORIS.TSS.Helpers.Owners.Data.Owners;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	partial class OwnersDataTreater
	{
		#region IOwnersDataTreater Members

		public void SMSLimitInsert(
			int mobileId, 
			int limit, 
			int remains, 
			DateTime beginTime, 
			DateTime endTime)
		{
			lock ( this.Data )
			{
				try
				{
					SMSLimitRow row = this.Data.SMSLimit.NewRow();

					row.Mobile		= mobileId;
					row.Limit		= limit;
					row.Remains		= remains;
					row.BeginTime	= beginTime;
					row.EndTime		= endTime;

					this.Data.SMSLimit.Rows.Add( row );

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void SMSLimitUpdate(
			int smsLimitId,
			int mobileId,
			int limit, 
			int remains, 
			DateTime beginTime, 
			DateTime endTime)
		{
			lock ( this.Data )
			{
				try
				{
					SMSLimitRow row = this.Data.SMSLimit.FindRow( smsLimitId );

					row.BeginEdit();

					row.Mobile		= mobileId;
					row.Limit		= limit;
					row.Remains		= remains;
					row.BeginTime	= beginTime;
					row.EndTime		= endTime;

					row.EndEdit();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void SMSLimitDelete( int smsLimitId )
		{
			lock ( this.Data )
			{
				try
				{
					OwnersRow row = this.Data.Owners.FindRow( smsLimitId );

					row.Delete();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		#endregion
	}
}