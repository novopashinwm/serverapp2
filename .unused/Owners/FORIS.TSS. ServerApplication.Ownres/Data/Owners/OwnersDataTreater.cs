using System;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.RuleManager.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	public partial class OwnersDataTreater :
		DataTreater<IOwnersServerData>,
		IOwnersDataTreater
	{
		public OwnersDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IOwnersServerData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{
		}

		public void ProcessStateChanges( MobileRuleStateEventArgs[] stateChanges, RuleStateChangeApplyOrder order )
		{
			lock ( this.Data )
			{
				try
				{
					foreach ( MobileRuleStateEventArgs e in stateChanges )
					{
						if ( e.MobileRuleObject is IMobileRule<IOwnersServerData> )
						{
							( (IMobileRule<IOwnersServerData>)e.MobileRuleObject ).ApplyChange( this.Data, e.MobileRuleState, order );
						}
					}

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}