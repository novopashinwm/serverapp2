using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Owners.Data.Owners;
using FORIS.TSS.Helpers.RuleManager.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{

	public class OwnersServerData :
		OwnersData,
		IOwnersServerData
	{
		#region Constructor & Dispose

		public OwnersServerData( IOwnersServerDataProvider serverDataProvider )
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new OwnersDatabaseDataSupplier( serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				RemotingServices.Disconnect( this );
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		private readonly IOwnersServerDataProvider serverDataProvider;

		public IOwnersDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new OwnersDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		public override IOwnersDataTreater GetDataTreater()
		{
			return
				new OwnersDataTreater(
					this.serverDataProvider.Database,
					this,
					null
					);
		}

		/// <summary>
		/// ��� ������� ����������� ������ � RuleServerData �.�. ��������� 
		/// ��������� ������� ������������ ������ ����� RuleDataTreater.
		/// � ����� ������ ��������.
		/// </summary>
		public event MobileRuleStateEventHandler MobileRuleStateChanged
		{
			add { this.serverDataProvider.RuleData.MobileRuleStateChanged += value; }
			remove { this.serverDataProvider.RuleData.MobileRuleStateChanged -= value; }
		}
	}

	public interface IOwnersServerData :
		IOwnersData,
		IRuleServerData
	{

	}
}