using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
    public class OwnersPersonalDataSupplier:
		PersonalDataSupplier<OwnersServerData, IOwnersDataTreater>,
		IOwnersDataSupplier
    {
        public OwnersPersonalDataSupplier(OwnersServerData serverData, IPersonalServer personalServer)
            : base(serverData, personalServer)
        {
        }


        public IRuleDataSupplier RuleDataSupplier
        {
            get { return this.DataSupplier.RuleDataSupplier;}
        }


        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.DataSupplier.SecurityDataSupplier; }
        }

        #endregion
    }
}