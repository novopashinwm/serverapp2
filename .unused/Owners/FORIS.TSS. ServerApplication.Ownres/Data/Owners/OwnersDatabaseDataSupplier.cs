using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.ServerApplication.Data;
using FORIS.TSS.ServerApplication.RuleManager.Data.Rule;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	public class OwnersDatabaseDataSupplier :
		DatabaseDataSupplier<IOwnersServerDataProvider>,
		IOwnersDataSupplier
	{
		public OwnersDatabaseDataSupplier( IOwnersServerDataProvider databaseProvider )
			: base( databaseProvider )
		{
            this.ruleSecurityDatabaseDataSupplier =
                new RuleSecurityDatabaseDataSupplier(databaseProvider);
		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerOwners",
					new string[] { "OWNERS", "OWNER_MOBILE", "SMS_LIMIT", "SMS_LOG", "SMS_TYPE" } );

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IRuleDataSupplier RuleDataSupplier
		{
			get { return this.DatabaseProvider.GetRuleDataSupplier(); }
		}

		public IOwnersDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}

        #region ISecureDataSupplier<IRuleSecurityDataSupplier,FORIS.TSS.Interfaces.Operator.Data.Operator.IOperatorDataSupplier> Members

        private readonly RuleSecurityDatabaseDataSupplier ruleSecurityDatabaseDataSupplier;
        public IRuleSecurityDataSupplier SecurityDataSupplier
        {
            get { return this.ruleSecurityDatabaseDataSupplier; }
        }

        #endregion
    }
}