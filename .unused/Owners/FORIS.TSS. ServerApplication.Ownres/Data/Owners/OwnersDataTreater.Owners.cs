using System;
using FORIS.TSS.Helpers.Owners.Data.Owners;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	public partial class OwnersDataTreater
	{
		public void OwnersInsert(
			int operatorId,
			string surname,
			string name,
			string patronimyc,
			string phone1,
			string phone2,
			string phone3
			)
		{
			lock ( this.Data )
			{
				try
				{
					OwnersRow ownersRow = this.Data.Owners.NewRow();

					ownersRow.Operator		= operatorId;
					ownersRow.Surname		= surname;
					ownersRow.Name			= name;
					ownersRow.Patronimyc	= patronimyc;
					ownersRow.Phone1		= phone1;
					ownersRow.Phone2		= phone2;
					ownersRow.Phone3		= phone3;

					this.Data.Owners.Rows.Add( ownersRow );

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OwnersUpdate(
			int ownerId,
			int operatorId,
			string surname,
			string name,
			string patronimyc,
			string phone1,
			string phone2,
			string phone3
			)
		{
			lock ( this.Data )
			{
				try
				{
					OwnersRow ownersRow = this.Data.Owners.FindRow( ownerId );

					ownersRow.BeginEdit();

					ownersRow.Operator		= operatorId;
					ownersRow.Surname		= surname;
					ownersRow.Name			= name;
					ownersRow.Patronimyc	= patronimyc;
					ownersRow.Phone1		= phone1;
					ownersRow.Phone2		= phone2;
					ownersRow.Phone3		= phone3;

					ownersRow.EndEdit();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OwnersDelete( int ownerId )
		{
			lock ( this.Data )
			{
				try
				{
					OwnersRow ownersRow = this.Data.Owners.FindRow( ownerId );

					ownersRow.Delete();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}