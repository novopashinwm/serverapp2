using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	public class OwnersServerDataDispatcher:
		DataDispatcher<IOwnersServerData>
	{
		#region Constructor

		public OwnersServerDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public OwnersServerDataDispatcher()
		{

		}

		#endregion // Constructor

		private readonly RuleServerDataAmbassadorCollection ambassadors =
			new RuleServerDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new RuleServerDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IOwnersServerData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class RuleServerDataAmbassadorCollection:
		DataAmbassadorCollection<IOwnersServerData>
	{
		public new RuleServerDataAmbassador this[int index]
		{
			get { return (RuleServerDataAmbassador)base[index]; }
		}
	}

	public class RuleServerDataAmbassador:
		DataAmbassador<IOwnersServerData>
	{

	}
}