using System;
using FORIS.TSS.Helpers.Owners.Data.Owners;

namespace FORIS.TSS.ServerApplication.Owners.Data.Owners
{
	public partial class OwnersDataTreater
	{
		public void OwnerMobileInsert( 
			int ownerId, 
			int mobileId 
			)
		{
			lock ( this.Data )
			{
				try
				{
					OwnerMobileRow ownerMobileRow = this.Data.OwnerMobile.NewRow();

					ownerMobileRow.Owner = ownerId;
					ownerMobileRow.Mobile = mobileId;

					this.Data.OwnerMobile.Rows.Add( ownerMobileRow );

					this.AcceptChanges();

				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OwnerMobileUpdate( 
			int ownerMobleId, 
			int ownerId, 
			int mobileId 
			)
		{
			lock ( this.Data )
			{
				try
				{
					OwnerMobileRow ownerMobileRow = this.Data.OwnerMobile.FindRow( ownerMobleId );

					ownerMobileRow.BeginEdit();

					ownerMobileRow.Owner = ownerId;
					ownerMobileRow.Mobile = mobileId;

					ownerMobileRow.EndEdit();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void OwnerMobileDelete( int ownerMobleId )
		{
			lock ( this.Data )
			{
				try
				{
					OwnerMobileRow ownerMobileRow = this.Data.OwnerMobile.FindRow( ownerMobleId );

					ownerMobileRow.Delete();

					this.AcceptChanges();
				}
				catch ( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}