using System;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Owners.Data.Owners
{
	public interface IOwnersDataTreater : IDataTreater
	{
		#region Owners

		void OwnersInsert(
			int operatorId,
			string surname,
			string name,
			string patronimyc,
			string phone1,
			string phone2,
			string phone3 );

		void OwnersUpdate(
			int ownersId,
			int operatorId,
			string surname,
			string name,
			string patronimyc,
			string phone1,
			string phone2,
			string phone3 );

		void OwnersDelete( int ownersId );

		#endregion  // Owners

		#region OwnerMobile

		void OwnerMobileInsert( int ownerId, int mobileId );
		void OwnerMobileUpdate( int ownerMobleId, int ownerId, int mobileId );
		void OwnerMobileDelete( int ownerMobleId );

		#endregion  // OwnerMobile

		#region SMSLimit

		void SMSLimitInsert(
			int mobileId,
			int limit,
			int remains,
			DateTime beginTime,
			DateTime endTime
			);

		void SMSLimitUpdate(
			int smsLimitId,
			int mobileId,
			int limit,
			int remains,
			DateTime beginTime,
			DateTime endTime
			);

		void SMSLimitDelete(
			int smsLimitId
			);

		#endregion  // SMSLimit
	}
}