using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.RuleManager.Data.Rule;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.Interfaces.Owners.Data.Owners
{
    public interface IOwnersDataSupplier :
        IDataSupplier<IOwnersDataTreater>,
        ISecureDataSupplier<IRuleSecurityDataSupplier, IOperatorDataSupplier>
    {
        IRuleDataSupplier RuleDataSupplier { get; }
    }
    
}