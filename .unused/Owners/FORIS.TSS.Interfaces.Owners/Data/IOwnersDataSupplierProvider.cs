using System;
using FORIS.TSS.BusinessLogic.RuleManager.Data;
using FORIS.TSS.Interfaces.Owners.Data.Owners;
using FORIS.TSS.Interfaces.Owners.Data.SMSLog;

namespace FORIS.TSS.Interfaces.Owners.Data
{
    public interface IOwnersDataSupplierProvider : 
        IRuleDataSupplierProvider
    {
        IOwnersDataSupplier GetOwnersDataSupplier();

		/// <summary>
		/// ���������� ��������������� �� ������� ������ ��� �� �� �� ��������� ������ �������
		/// </summary>
		/// <param name="beginTime">������ �������.</param>
		/// <param name="endTime">����� �������.</param>
		/// <param name="mobileId">������������� ������� ��������</param>
		/// <returns>��������������� �� ������� ������ ���</returns>
		SMSLogDataSet GetSMSLogFromDB(
			DateTime beginTime, 
			DateTime endTime,
			int mobileId
			);
	}
}