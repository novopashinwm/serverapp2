﻿using System;
using System.Drawing;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Schedule.Data.Schedule
{
    public interface IScheduleDataTreater : IDataTreater
    {
        #region Schedule

        void ScheduleInsert(
            int routeID, 
            int dayKindID, 
            string extNumber, 
            string comment,
            DateTime beginTime,
            DateTime endTime,
            int wayoutID);
        void ScheduleUpdate(
            int scheduleID,
            int routeID,
            int dayKindID,
            string extNumber,
            string comment,
            DateTime beginTime,
            DateTime endTime,
            int wayoutID);
        void ScheduleDelete(int scheduleID);

        #endregion //Schedule

        #region Route

        void RouteInsert(
            int routeTypeID,
            string number,
            int length,
            string name,
            string description,
            Color color);
        void RouteUpdate(
            int routeID,
            int routeTypeID,
            string number,
            int length,
            string name,
            string description,
            Color color);
        void RouteDelete(int routeID);

        #endregion //Route

        #region Wayout

        void WayoutInsert(
            int route,
            string extNumber,
            int graphic
            );

        void WayoutUpdate(
            int wayoutId,
            int route,
            string extNumber,
            int graphic
            );

        void WayoutDelete(int wayoutId);

        #endregion  // Wayout

        #region DayKind

        void DayKindInsert(
            string name,
            int? dk_set,
            int weekdays
            );

        void DayKindUpdate(
            int dayKindId,
            string name,
            int? dk_set,
            int weekdays
            );

        void DayKindDelete(int dayKindId);

        #endregion  // DayKind

        #region DkSet

        void DkSetInsert(
            string name,
            string comment
            );

        void DkSetUpdate(
            int dkSetId,
            string name,
            string comment
            );

        void DkSetDelete(int dkSetId);

        #endregion  // DkSet
    }
}
