﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Interfaces.Schedule.Data.Schedule
{
    public interface IScheduleDataSupplier : 
        IDataSupplier<IScheduleDataTreater>
    {
    }
}
