﻿using FORIS.TSS.Interfaces.Schedule.Data.Schedule;

namespace FORIS.TSS.Interfaces.Schedule.Data
{
    public interface IScheduleDataSupplierProvider
    {
        IScheduleDataSupplier GetScheduleDataSupplier();
    }
}
