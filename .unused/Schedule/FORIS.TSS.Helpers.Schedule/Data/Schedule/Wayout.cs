﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class WayoutTable :
        Table<WayoutRow, WayoutTable, IScheduleData>
    {
        #region Propeties

        public override string Name
        {
            get { return "WAYOUT"; }
        }

        #endregion // Properties

        #region Constructor & Dispose

        public WayoutTable()
        {

        }
        public WayoutTable(IContainer container)
            : this()
        {
            container.Add(this);
        }

        #endregion // Constructor & Dispose

        #region Fields' indexes

        private int idIndex;
        internal int IdIndex { get { return this.idIndex; } }

        private int routeIndex;
        internal int RouteIndex { get { return this.routeIndex; } }

        private int extNumberIndex;
        internal int ExtNumberIndex { get { return this.extNumberIndex; } }

        private int graphicIndex;
        internal int GraphicIndex { get { return this.graphicIndex; } }

        #endregion // Fields' indexes

        #region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable != null)
            {
                this.idIndex = this.DataTable.Columns.IndexOf("WAYOUT_ID");
                this.routeIndex = this.DataTable.Columns.IndexOf("ROUTE");
                this.extNumberIndex = this.DataTable.Columns.IndexOf("EXT_NUMBER");
                this.graphicIndex = this.DataTable.Columns.IndexOf("graphic");
            }
        }
        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.idIndex = -1;
            this.routeIndex = -1;
            this.extNumberIndex = -1;
            this.graphicIndex = -1;
        }

        #endregion // View

        protected override WayoutRow OnCreateRow(DataRow dataRow)
        {
            return new WayoutRow(dataRow);
        }
    }

    public class WayoutRow :
        Row<WayoutRow, WayoutTable, IScheduleData>
    {
        #region Fields

        public int Route
        {
            get { return (int)this.DataRow[this.Table.RouteIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.RouteIndex] = value; }
        }

        public string ExtNumber
        {
            get { return (string)this.DataRow[this.Table.ExtNumberIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.ExtNumberIndex] = value; }
        }

        public int Graphic
        {
            get { return (int)this.DataRow[this.Table.GraphicIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.GraphicIndex] = value; }
        }

        #endregion // Fields

        #region Parent row

        public RouteRow RowRoute { get; private set; }

        #endregion // Parent row

        #region Child Rows

        private readonly ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> rowsSchedule =
            new ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData>();
        public ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> RowsSchedule
        {
            get { return this.rowsSchedule; }
        }

        #endregion //Child Rows

        #region Id

        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

        #endregion // Id

        #region View

        protected override void OnDestroyView()
        {
            this.RowRoute = null;

            base.OnDestroyView();
        }
        protected override void OnBuildView()
        {
            base.OnBuildView();

            this.RowRoute = this.Data.Route.FindRow(this.Route);
        }

        #endregion // View

        public WayoutRow(DataRow dataRow)
            : base(dataRow)
        {
        }

        protected override void OnBuildFields()
        {
        }
    }
}
