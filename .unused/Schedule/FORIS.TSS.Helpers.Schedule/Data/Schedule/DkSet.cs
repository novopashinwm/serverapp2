﻿using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class DkSetTable : Table<DkSetRow, DkSetTable, IScheduleData>
    {
        #region Properties

        public int CommentIndex { get; private set; }

        public int NameIndex { get; private set; }

        public int IdIndex { get; private set; }

        public override string Name
        {
            get { return "DK_SET"; }
        }

        #endregion //Properties
        
        #region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable == null) return;

            this.IdIndex        = this.DataTable.Columns.IndexOf("DK_SET_ID");
            this.NameIndex      = this.DataTable.Columns.IndexOf("NAME");
            this.CommentIndex   = this.DataTable.Columns.IndexOf("COMMENT");
        }

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.IdIndex = -1;
            this.NameIndex = -1;
            this.CommentIndex = -1;
        }

        #endregion  // View

        #region Constructor & Dispose

        public DkSetTable( IContainer container )
            : this()
        {
            container.Add( this );
        }

        public DkSetTable()
        {
            CommentIndex = -1;
            NameIndex = -1;
            IdIndex = -1;
        }

        #endregion  // Constructor & Dispose

        protected override DkSetRow OnCreateRow(DataRow dataRow)
        {
            return new DkSetRow(dataRow);
        }
    }

    public class DkSetRow : Row<DkSetRow, DkSetTable, IScheduleData>
    {
        #region Fields

        public string Name
        {
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.NameIndex] = value; }
        }

        public string Comment
        {
            get
            {
                return DataRow.IsNull(this.Table.CommentIndex) ? string.Empty :
                                                                                  (string)this.DataRow[this.Table.CommentIndex, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.CommentIndex] =
                    !string.IsNullOrEmpty(value) ? (object)value : DBNull.Value;
            }
        }

        #endregion //Fields

        #region Child rows

        private readonly ChildRowCollection<DayKindRow, DayKindTable, IScheduleData> rowsDayKind =
            new ChildRowCollection<DayKindRow, DayKindTable, IScheduleData>();
        public ChildRowCollection<DayKindRow, DayKindTable, IScheduleData> RowsDayKind
        {
            get { return this.rowsDayKind; }
        }

        #endregion // Child rows

        public DkSetRow(DataRow dataRow) : base(dataRow)
        {
        }

        #region Overrides of Row<DkSetRow,DkSetTable,IScheduleData>

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Идентификатор строки</returns>
        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

        protected override void OnBuildFields()
        {
            
        }

        #endregion
    }
}