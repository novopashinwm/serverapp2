﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class RouteTable :
        Table<RouteRow, RouteTable, IScheduleData>
    {
        #region Propeties

        public override string Name
        {
            get { return "ROUTE"; }
        }

        #endregion // Properties

        #region Constructor & Dispose

        public RouteTable()
        {

        }
        public RouteTable( IContainer container )
            : this()
        {
            container.Add( this );
        }

        #endregion // Constructor & Dispose

        #region Fields' indexes

        internal int IdIndex { get; private set; }

        internal int RouteTypeIdIndex { get; private set; }

        internal int NumberIndex { get; private set; }

        internal int LengthIndex { get; private set; }

        internal int NameIndex { get; private set; }

        internal int DescriptionIndex { get; private set; }

        internal int ColorIndex { get; private set; }

        public RouteRow RowDefault { get; private set; }

        #endregion // Fields' indexes

        #region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if ( this.DataTable != null )
            {
                this.IdIndex			= this.DataTable.Columns.IndexOf( "ROUTE_ID" );
                this.RouteTypeIdIndex	= this.DataTable.Columns.IndexOf( "ROUTE_TYPE_ID" );
                this.NumberIndex		= this.DataTable.Columns.IndexOf( "NUMBER" );
                this.LengthIndex		= this.DataTable.Columns.IndexOf( "LENGTH" );
                this.NameIndex			= this.DataTable.Columns.IndexOf( "NAME" );
                this.DescriptionIndex	= this.DataTable.Columns.IndexOf( "DESCRIPTION" );
                this.ColorIndex			= this.DataTable.Columns.IndexOf( "COLOR" );

                DataRow dr = this.DataTable.NewRow();
                this.RowDefault = new RouteRow( dr );
            }
        }
        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.IdIndex			= -1;
            this.RouteTypeIdIndex	= -1;
            this.NumberIndex		= -1;
            this.LengthIndex		= -1;
            this.NameIndex			= -1;
            this.DescriptionIndex	= -1;
            this.ColorIndex			= -1;
        }

        #endregion // View

        protected override RouteRow OnCreateRow( DataRow dataRow )
        {
            return new RouteRow( dataRow );
        }
    }

    public class RouteRow :
        Row<RouteRow, RouteTable, IScheduleData>
    {
        #region Fields

        public int RouteType
        {
            get { return (int)this.DataRow[this.Table.RouteTypeIdIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.RouteTypeIdIndex] = value; }
        }

        public string Number
        {
            get { return (string)this.DataRow[this.Table.NumberIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.NumberIndex] = value; }
        }

        public double Length
        {
            get { return Convert.ToDouble( this.DataRow[this.Table.LengthIndex, DataRowVersion.Current] ); }
            set { this.DataRow[this.Table.LengthIndex] = value; }
        }

        public string Name
        {
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.NameIndex] = value; }
        }

        public string Description
        {
            get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.DescriptionIndex] = value; }
        }

        public Color Color
        {
            get { return (Color) this.DataRow[this.Table.ColorIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.ColorIndex] = value; }
        }

        #endregion // Fields

        #region Id

        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

        #endregion // Id

        #region Child Rows

        private readonly ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> rowsSchedule =
            new ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData>();
        public ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> RowsSchedule
        {
            get { return this.rowsSchedule; }
        }

        #endregion //Child Rows

        public RouteRow( DataRow dataRow )
            : base( dataRow )
        {
        }

        protected override void OnBuildFields()
        {
        }
    }
}