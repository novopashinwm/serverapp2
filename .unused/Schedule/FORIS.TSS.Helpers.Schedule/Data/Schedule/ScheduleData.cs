﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class ScheduleData:
        Data<IScheduleDataTreater>,
        IScheduleData
    {
		#region Controls & Components

		private IContainer components;
		private ScheduleDataDispatcher tablesDataDispatcher;

		private ScheduleTable	schedule;

        private RouteTable route;

        private WayoutTable wayout;

        private DayKindTable dayKind;

        private DkSetTable dkSet;

		private ScheduleDataAmbassador daSchedule;
        private ScheduleDataAmbassador daRoute;
        private ScheduleDataAmbassador daWayout;
        private ScheduleDataAmbassador daDayKind;
        private ScheduleDataAmbassador daDkSet;

		#endregion //Controls & Components

		#region Constructor & Dispose

        public ScheduleData( IContainer container )
			: this()
		{
			container.Add( this );
		}

		public ScheduleData()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new Container();
			this.tablesDataDispatcher = new ScheduleDataDispatcher( this.components );

			this.schedule	  = new ScheduleTable( this.components );
            this.wayout       = new WayoutTable(this.components);
            this.route        = new RouteTable(this.components);
            this.dkSet        = new DkSetTable(this.components);
            this.dayKind      = new DayKindTable(this.components);

			this.daSchedule   = new ScheduleDataAmbassador();
            this.daWayout     = new ScheduleDataAmbassador();
		    this.daRoute      = new ScheduleDataAmbassador();
		    this.daDkSet      = new ScheduleDataAmbassador();
		    this.daDayKind    = new ScheduleDataAmbassador();

			this.daSchedule.Item  = this.Schedule;
		    this.daWayout.Item    = this.Wayout;
		    this.daRoute.Item     = this.Route;
		    this.daDkSet.Item     = this.DkSet;
		    this.daDayKind.Item   = this.DayKind;

            this.tablesDataDispatcher.Ambassadors.Add(this.daRoute);
			this.tablesDataDispatcher.Ambassadors.Add(this.daWayout);
            this.tablesDataDispatcher.Ambassadors.Add(this.daDkSet);
            this.tablesDataDispatcher.Ambassadors.Add(this.daDayKind);
            this.tablesDataDispatcher.Ambassadors.Add(this.daSchedule);
		}

		#endregion  // Component Designer generated code

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IScheduleDataSupplier DataSupplier
		{
			get { return (IScheduleDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion  // Properties

		#region Tables' order

		/// <summary>
		/// порядок таблиц
		/// </summary>
		/// <remarks>
		/// используется в методе ApplyChanges
		/// для определения порядка таблиц при
		/// удалении/добавлении/изменении данных
		/// </remarks>
		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		/// <summary>
		/// Разрушить обертки
		/// </summary>
		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;
		}

		/// <summary>
		/// Построить обертки
		/// </summary>
		/// <remarks>
		/// Так как базовый класс не имеет понятия о типе 
		/// наследуемого класса, то он не знает и о типе 
		/// диспетчера контейнер данных (обобщенный тип)
		/// </remarks>
		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;
		}

		#endregion  // Build & Destroy

		#region Tables properties

		[Browsable( false )]
		[Category( "Tables" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public ScheduleTable Schedule
		{
			get { return this.schedule; }
		}

        [Browsable(false)]
        [Category("Tables")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RouteTable Route
        {
            get { return route; }
        }

        [Browsable(false)]
        [Category("Tables")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public WayoutTable Wayout
        {
            get { return wayout; }
        }

        [Browsable(false)]
        [Category("Tables")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DayKindTable DayKind
        {
            get { return dayKind; }
        }

        [Browsable(false)]
        [Category("Tables")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DkSetTable DkSet
        {
            get { return dkSet; }
        }

        #endregion //Tables properties
    }
}
