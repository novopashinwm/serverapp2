﻿
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public interface IScheduleData : IData<IScheduleDataTreater>,
		IScheduleDataSupplier
    {
        ScheduleTable Schedule { get; }
        RouteTable Route { get; }
        WayoutTable Wayout { get; }
        DayKindTable DayKind { get; }
        DkSetTable DkSet { get; }
    }
}
