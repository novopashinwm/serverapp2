﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class ScheduleDataDispatcher : DataDispatcher<IScheduleData>
    {
        public ScheduleDataDispatcher(IContainer container)
            : this()
        {
            container.Add(this);
        }
        public ScheduleDataDispatcher()
        {

        }

        private readonly ScheduleDataAmbassadorCollection ambassadors =
            new ScheduleDataAmbassadorCollection();

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new ScheduleDataAmbassadorCollection Ambassadors
        {
            get { return this.ambassadors; }
        }

        protected override AmbassadorCollection<IDataItem<IScheduleData>> GetAmbassadorCollection()
        {
            return this.ambassadors;
        }
    }

    public class ScheduleDataAmbassadorCollection :
        DataAmbassadorCollection<IScheduleData>
    {
        public new ScheduleDataAmbassador this[int index]
        {
            get { return (ScheduleDataAmbassador)base[index]; }
        }
    }

    public class ScheduleDataAmbassador :
        DataAmbassador<IScheduleData>
    {

    }
}
