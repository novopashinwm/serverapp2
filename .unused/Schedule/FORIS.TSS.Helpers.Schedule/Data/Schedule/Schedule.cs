﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using System;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class ScheduleTable :
        Table<ScheduleRow, ScheduleTable, IScheduleData>
    {
        #region Properties

        public override string Name
        {
            get { return "SCHEDULE"; }
        }

        #region Fields' indices

        public int IdIndex { get; private set; }

        public int RouteIndex { get; private set; }

        public int DayKindIndex { get; private set; }

        public int ExtNumberIndex { get; private set; }

        public int CommentIndex { get; private set; }

        public int BeginTimeIndex { get; private set; }

        public int EndTimeIndex { get; private set; }

        public int WayoutIndex { get; private set; }

        #endregion  // Fields' indices

        #endregion //Properties

        protected override ScheduleRow OnCreateRow(DataRow dataRow)
        {
            return new ScheduleRow(dataRow);
        }

		#region Constructor & Dispose

		public ScheduleTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

        public ScheduleTable()
		{
            this.IdIndex = -1;
            this.RouteIndex = -1;
            this.DayKindIndex = -1;
            this.ExtNumberIndex = -1;
            this.CommentIndex = -1;
            this.BeginTimeIndex = -1;
            this.EndTimeIndex = -1;
            this.WayoutIndex = -1;
		}

		#endregion  // Constructor & Dispose

        #region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable == null) return;

            this.IdIndex = this.DataTable.Columns.IndexOf("SCHEDULE_ID");
            this.RouteIndex = this.DataTable.Columns.IndexOf("ROUTE_ID");
            this.DayKindIndex = this.DataTable.Columns.IndexOf("DAY_KIND_ID");
            this.ExtNumberIndex = this.DataTable.Columns.IndexOf("EXT_NUMBER");
            this.CommentIndex = this.DataTable.Columns.IndexOf("COMMENT");
            this.BeginTimeIndex = this.DataTable.Columns.IndexOf("BEGIN_TIME");
            this.EndTimeIndex = this.DataTable.Columns.IndexOf("END_TIME");
            this.WayoutIndex = this.DataTable.Columns.IndexOf("WAYOUT");
        }

        protected override void OnDestroyView()
        {
            this.IdIndex = -1;
            this.RouteIndex = -1;
            this.DayKindIndex = -1;
            this.ExtNumberIndex = -1;
            this.CommentIndex = -1;
            this.BeginTimeIndex = -1;
            this.EndTimeIndex = -1;
            this.WayoutIndex = -1;

            base.OnDestroyView();
        }

        protected override void OnRowActionHide(ScheduleRow row)
        {
            try
            {
                WayoutRow wayoutRow = this.Data.Wayout.FindRow(row.Wayout);

                if (wayoutRow != null)
                {
                    wayoutRow.RowsSchedule.Remove(row);
                }
                else
                {
                    throw new ApplicationException("There is no Wayout row for Schedule row");
                }
                if(row.DayKind != null)
                {
                    DayKindRow dayKindRow = this.Data.DayKind.FindRow(row.DayKind);
                    if (dayKindRow != null)
                        dayKindRow.RowsSchedule.Remove(row);
                    else
                        throw new ApplicationException("There is no DayKind row for Schedule row");
                }
                if(row.Route != null)
                {
                    RouteRow routeRow = this.Data.Route.FindRow(row.Route);
                    if (routeRow != null)
                        routeRow.RowsSchedule.Remove(row);
                    else
                        throw new ApplicationException("There is no Route row for Schedule row");
                }
            }
            finally
            {
                base.OnRowActionHide(row);
            }
        }
        protected override void OnRowActionShow(ScheduleRow row)
        {
            base.OnRowActionShow(row);

            WayoutRow wayoutRow = this.Data.Wayout.FindRow(row.Wayout);
            if (wayoutRow != null) 
                wayoutRow.RowsSchedule.Add(row);

            DayKindRow dayKindRow = this.Data.DayKind.FindRow(row.DayKind);
            if (dayKindRow != null)
                dayKindRow.RowsSchedule.Add(row);

            RouteRow routeRow = this.Data.Route.FindRow(row.Route);
            if(routeRow != null)
                routeRow.RowsSchedule.Add(row);
        }


        #endregion //View
    }

    public class ScheduleRow : Row<ScheduleRow, ScheduleTable, IScheduleData>
    {
        #region Fields

        public int? Route
        {
            get 
            {
                return this.DataRow[this.Table.RouteIndex, DataRowVersion.Current] == DBNull.Value
                           ? (int?) null
                           : (int) this.DataRow[this.Table.RouteIndex, DataRowVersion.Current];
            }
            set { this.DataRow[this.Table.RouteIndex] = value; }
        }

        public int? DayKind
        {
            get
            {
                return this.DataRow[this.Table.DayKindIndex, DataRowVersion.Current] == DBNull.Value
                           ? (int?)null
                           : (int)this.DataRow[this.Table.DayKindIndex, DataRowVersion.Current];
            }
            set { this.DataRow[this.Table.DayKindIndex] = value; }
        }

        public string ExtNumber
        {
            get
            {
                return
                    this.DataRow[this.Table.ExtNumberIndex, DataRowVersion.Current] == DBNull.Value
                        ? String.Empty
                        : (string)this.DataRow[this.Table.ExtNumberIndex, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.ExtNumberIndex] =
                    String.IsNullOrEmpty(value)
                        ? DBNull.Value
                        : (object)value;
            }
        }

        public string Comment
        {
            get
            {
                return
                    this.DataRow[this.Table.CommentIndex, DataRowVersion.Current] == DBNull.Value
                        ? String.Empty
                        : (string)this.DataRow[this.Table.CommentIndex, DataRowVersion.Current];
            }
            set
            {
                this.DataRow[this.Table.CommentIndex] =
                    String.IsNullOrEmpty(value)
                        ? DBNull.Value
                        : (object)value;
            }
        }

        public DateTime? BeginTime
        {
            get
            {
                return
                    this.DataRow[this.Table.BeginTimeIndex, DataRowVersion.Current] == DBNull.Value
                        ? null
                        : (DateTime?)this.DataRow[this.Table.BeginTimeIndex, DataRowVersion.Current];
            }
            set { this.DataRow[this.Table.BeginTimeIndex] = value; }
        }

        public DateTime? EndTime
        {
            get
            {
                return
                    this.DataRow[this.Table.EndTimeIndex, DataRowVersion.Current] == DBNull.Value
                        ? null
                        : (DateTime?)this.DataRow[this.Table.EndTimeIndex, DataRowVersion.Current];
            }
            set { this.DataRow[this.Table.EndTimeIndex] = value; }
        }

        public int Wayout
        {
            get { return (int) this.DataRow[this.Table.WayoutIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.EndTimeIndex] = value;}
        }

        #endregion //Fields

        #region Parent row

        public RouteRow RowRoute { get; private set; }
        public DayKindRow RowDayKind { get; private set; }
        public WayoutRow RowWayout { get; private set; }

        #endregion // Parent row

        
        public ScheduleRow(DataRow dataRow) : base(dataRow)
        {

        }

        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

        protected override void OnBuildFields()
        {
            
        }

        protected override void OnBuildView()
        {
            base.OnBuildView();

            this.RowRoute = this.Data.Route.FindRow(this.Route);
            this.RowDayKind = this.Data.DayKind.FindRow(this.DayKind);
            this.RowWayout = this.Data.Wayout.FindRow(this.Wayout);
        }

        protected override void OnDestroyView()
        {
            this.RowRoute = null;
            this.RowDayKind = null;
            this.RowWayout = null;

            base.OnDestroyView();
        }
    }
}
