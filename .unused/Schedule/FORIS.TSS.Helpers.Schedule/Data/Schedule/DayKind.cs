﻿using System;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.Helpers.Schedule.Data.Schedule
{
    public class DayKindTable : Table<DayKindRow, DayKindTable, IScheduleData>
    {
        #region Properties

        public override string Name
        {
            get { return "DAY_KIND"; }
        }

        #endregion //Properties

        #region Constructor & Dispose

        public DayKindTable( IContainer container )
            : this()
        {
            container.Add( this );
        }

        public DayKindTable()
        {

        }

        #endregion  // Constructor & Dispose

        #region Fields' indexes

        public int WeekdaysIndex { get; private set; }

        public int DkSetIndex { get; private set; }

        public int NameIndex { get; private set; }

        public int IdIndex { get; private set; }

        #endregion  // Fields' indexes

        #region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if ( this.DataTable == null ) return;

            this.IdIndex		= this.DataTable.Columns.IndexOf( "DAY_KIND_ID" );
            this.NameIndex		= this.DataTable.Columns.IndexOf( "NAME" );
            this.DkSetIndex		= this.DataTable.Columns.IndexOf( "DK_SET" );
            this.WeekdaysIndex	= this.DataTable.Columns.IndexOf( "WEEKDAYS" );
        }

        protected override void OnDestroyView()
        {
            base.OnDestroyView();

            this.IdIndex		= -1;
            this.NameIndex		= -1;
            this.DkSetIndex		= -1;
            this.WeekdaysIndex	= -1;
        }

        #endregion  // View

        protected override void OnRowActionHide( DayKindRow row )
        {
            try
            {
                if ( row.DkSet != null )
                {
                    DkSetRow dkSetRow = this.Data.DkSet.FindRow( row.DkSet );

                    if ( dkSetRow != null )
                    {
                        dkSetRow.RowsDayKind.Remove( row );
                    }
                    else
                    {
                        throw new ApplicationException( "There is no DkSet row for DayKind row" );
                    }
                }
            }
            finally
            {
                base.OnRowActionHide( row );
            }
        }
        protected override void OnRowActionShow( DayKindRow row )
        {
            base.OnRowActionShow( row );

            DkSetRow dkSetRow = this.Data.DkSet.FindRow( row.DkSet );

            if ( dkSetRow != null ) dkSetRow.RowsDayKind.Add( row );
        }

        protected override DayKindRow OnCreateRow( DataRow dataRow )
        {
            return new DayKindRow( dataRow );
        }
    }

    public class DayKindRow : 
        Row<DayKindRow, DayKindTable, IScheduleData>
    {
        #region Fields

        public string Name
        {
            get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.NameIndex] = value; }
        }

        public int? DkSet
        {
            get 
            { 
                return 
                    DataRow.IsNull(this.Table.DkSetIndex)
                        ? null
                        : (int?)this.DataRow[this.Table.DkSetIndex, DataRowVersion.Current]; 
            }
            set { this.DataRow[this.Table.DkSetIndex] = value; }
        }

        public int Weekdays
        {
            get { return (int)this.DataRow[this.Table.WeekdaysIndex, DataRowVersion.Current]; }
            set { this.DataRow[this.Table.WeekdaysIndex] = value; }
        }

        #endregion //Fields

        #region Parent row

        public DkSetRow RowDkSet { get; private set; }

        #endregion // Parent row

        #region Child Rows

        private readonly ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> rowsSchedule =
            new ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData>();
        public ChildRowCollection<ScheduleRow, ScheduleTable, IScheduleData> RowsSchedule
        {
            get { return this.rowsSchedule; }
        }

        #endregion //Child Rows

        public DayKindRow(DataRow dataRow) : base(dataRow)
        {
        }

        #region Overrides of Row<DayKindRow,DayKindTable,IScheduleData>

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Идентификатор строки</returns>
        protected override int GetId()
        {
            return (int)this.DataRow[this.Table.IdIndex];
        }

        protected override void OnBuildFields()
        {
            
        }

        protected override void OnBuildView()
        {
            base.OnBuildView();

            this.RowDkSet = this.Data.DkSet.FindRow(this.DkSet);
        }

        protected override void OnDestroyView()
        {
            this.RowDkSet = null;

            base.OnDestroyView();
        }

        #endregion
    }
}