﻿using FORIS.TSS.ServerApplication.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data
{
    public interface IScheduleServerDataProvider : 
		IDatabaseProvider
    {
        ScheduleServerData ScheduleData { get; }
    }
}
