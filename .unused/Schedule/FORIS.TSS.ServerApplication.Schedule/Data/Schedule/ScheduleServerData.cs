﻿using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    public class ScheduleServerData :
		ScheduleData
	{
		#region Constructor & Dispose

		public ScheduleServerData( IScheduleServerDataProvider serverDataProvider )
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new ScheduleDatabaseDataSupplier( serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				RemotingServices.Disconnect( this );
			}

			base.Dispose( disposing );
		}

		#endregion  // Constructor & Dispose

		private readonly IScheduleServerDataProvider serverDataProvider;

		public IScheduleDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new ScheduleDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		public override IScheduleDataTreater GetDataTreater()
		{
			return
				new ScheduleDataTreater(
					this.serverDataProvider.Database,
					this,
					null
					);
		}
	}
}
