﻿using System;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    partial class ScheduleDataTreater
    {
        #region Implementation of IScheduleDataTreater

        public void DkSetInsert(string name, string comment)
        {
            lock (this.Data)
            {
                try
                {
                    DkSetRow dkSetRow = this.Data.DkSet.NewRow();

                    dkSetRow.Name = name;
                    dkSetRow.Comment = comment;

                    this.Data.DkSet.Rows.Add(dkSetRow);

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void DkSetUpdate(int dkSetId, string name, string comment)
        {
            lock (this.Data)
            {
                try
                {
                    DkSetRow DkSetRow = this.Data.DkSet.FindRow(dkSetId);

                    DkSetRow.BeginEdit();

                    DkSetRow.Name = name;
                    DkSetRow.Comment = comment;

                    DkSetRow.EndEdit();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void DkSetDelete(int dkSetId)
        {
            lock (this.Data)
            {
                try
                {
                    DkSetRow DkSetRow = this.Data.DkSet.FindRow(dkSetId);

                    DkSetRow.Delete();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        #endregion
    }
}
