﻿using System;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    partial class ScheduleDataTreater
    {
        #region Implementation of IScheduleDataTreater

        public void DayKindInsert(string name, int? dk_set, int weekdays)
        {
            lock (this.Data)
            {
                try
                {
                    DayKindRow dayKindRow = this.Data.DayKind.NewRow();

                    dayKindRow.Name = name;
                    dayKindRow.DkSet = dk_set;
                    dayKindRow.Weekdays = weekdays;

                    this.Data.DayKind.Rows.Add(dayKindRow);
                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void DayKindUpdate(int dayKindId, string name, int? dk_set, int weekdays)
        {
            lock (this.Data)
            {
                try
                {
                    DayKindRow dayKindRow = this.Data.DayKind.FindRow(dayKindId);

                    dayKindRow.BeginEdit();

                    dayKindRow.Name = name;
                    dayKindRow.DkSet = dk_set;
                    dayKindRow.Weekdays = weekdays;

                    dayKindRow.EndEdit();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void DayKindDelete(int dayKindId)
        {
            lock (this.Data)
            {
                try
                {
                    DayKindRow dayKindRow = this.Data.DayKind.FindRow(dayKindId);

                    dayKindRow.Delete();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        #endregion
    }
}
