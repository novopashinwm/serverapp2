﻿using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    public partial class ScheduleDataTreater :
		DataTreater<IScheduleData>,
		IScheduleDataTreater
	{
		public ScheduleDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IScheduleData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{
		}
	}
}
