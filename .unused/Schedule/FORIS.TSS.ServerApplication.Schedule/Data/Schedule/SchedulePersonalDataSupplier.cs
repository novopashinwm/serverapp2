﻿using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    class SchedulePersonalDataSupplier:
		PersonalDataSupplier<ScheduleServerData, IScheduleDataTreater>,
		IScheduleDataSupplier
	{
		public SchedulePersonalDataSupplier(ScheduleServerData serverData, IPersonalServer personalServer)
			: base(serverData, personalServer)
		{
		}
	}
}
