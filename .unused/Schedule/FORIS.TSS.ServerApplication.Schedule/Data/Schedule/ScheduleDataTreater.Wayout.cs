﻿using System;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    public partial class ScheduleDataTreater
    {
        #region Implementation of IScheduleDataTreater

        public void WayoutInsert(int route, string extNumber, int graphic)
        {
            lock (this.Data)
            {
                try
                {
                    WayoutRow wayoutRow = this.Data.Wayout.NewRow();

                    wayoutRow.Route = route;
                    wayoutRow.ExtNumber = extNumber;
                    wayoutRow.Graphic = graphic;

                    this.Data.Wayout.Rows.Add(wayoutRow);
                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void WayoutUpdate(int wayoutId, int route, string extNumber, int graphic)
        {
            lock (this.Data)
            {
                try
                {
                    WayoutRow wayoutRow = this.Data.Wayout.FindRow(wayoutId);

                    wayoutRow.BeginEdit();

                    wayoutRow.Route = route;
                    wayoutRow.ExtNumber = extNumber;
                    wayoutRow.Graphic = graphic;

                    wayoutRow.EndEdit();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void WayoutDelete(int wayoutId)
        {
            lock (this.Data)
            {
                try
                {
                    WayoutRow wayoutRow = this.Data.Wayout.FindRow(wayoutId);

                    wayoutRow.Delete();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        #endregion
    }
}
