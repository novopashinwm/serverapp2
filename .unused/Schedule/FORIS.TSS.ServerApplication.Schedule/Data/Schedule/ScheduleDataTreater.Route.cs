﻿using System;
using System.Drawing;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    partial class ScheduleDataTreater
    {
        #region Implementation of IScheduleDataTreater

        public void RouteInsert(int routeTypeID, string number, int length, string name, string description, Color color)
        {
            lock (this.Data)
            {
                try
                {
                    RouteRow routeRow = this.Data.Route.NewRow();

                    routeRow.Name = name;
                    routeRow.Number = number;
                    routeRow.Length = length;
                    routeRow.Description = description;
                    routeRow.Color = color;

                    this.Data.Route.Rows.Add(routeRow);
                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void RouteUpdate(int routeID, int routeTypeID, string number, int length, string name, string description, Color color)
        {
            lock (this.Data)
            {
                try
                {
                    RouteRow routeRow = this.Data.Route.FindRow(routeID);

                    routeRow.BeginEdit();

                    routeRow.Name = name;
                    routeRow.Number = number;
                    routeRow.Length = length;
                    routeRow.Description = description;
                    routeRow.Color = color;

                    routeRow.EndEdit();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void RouteDelete(int routeID)
        {
            lock (this.Data)
            {
                try
                {
                    RouteRow routeRow = this.Data.Route.FindRow(routeID);

                    routeRow.Delete();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        #endregion
    }
}
