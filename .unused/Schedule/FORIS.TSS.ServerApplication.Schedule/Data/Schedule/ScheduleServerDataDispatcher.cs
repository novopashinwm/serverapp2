﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    class ScheduleServerDataDispatcher:
		DataDispatcher<IScheduleData>
	{
		#region Constructor

		public ScheduleServerDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public ScheduleServerDataDispatcher()
		{

		}

		#endregion // Constructor

		private readonly ScheduleServerDataAmbassadorCollection ambassadors =
			new ScheduleServerDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new ScheduleServerDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IScheduleData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	public class ScheduleServerDataAmbassadorCollection:
		DataAmbassadorCollection<IScheduleData>
	{
	}

	public class ScheduleServerDataAmbassador:
		DataAmbassador<IScheduleData>
	{
	}
}
