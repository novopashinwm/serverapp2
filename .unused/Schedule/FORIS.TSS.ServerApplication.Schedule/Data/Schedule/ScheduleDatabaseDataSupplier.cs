﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.Schedule.Data.Schedule;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
    class ScheduleDatabaseDataSupplier:
		DatabaseDataSupplier<IScheduleServerDataProvider>,
		IScheduleDataSupplier
	{
		public ScheduleDatabaseDataSupplier( IScheduleServerDataProvider databaseProvider )
			: base( databaseProvider )
		{
		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerSchedule",
					new[] { "SCHEDULE", "ROUTE", "WAYOUT", "DK_SET", "DAY_KIND" } );

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IScheduleDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}

	}
}
