﻿using System;
using FORIS.TSS.Helpers.Schedule.Data.Schedule;

namespace FORIS.TSS.ServerApplication.Schedule.Data.Schedule
{
	public partial class ScheduleDataTreater
	{
	    #region Implementation of IScheduleDataTreater

	    public void ScheduleInsert(int routeID, int dayKindID, string extNumber, string comment, DateTime beginTime, DateTime endTime, int wayoutID)
	    {
            lock (this.Data)
            {
                try
                {
                    ScheduleRow scheduleRow = this.Data.Schedule.NewRow();

                    scheduleRow.Route = routeID;
                    scheduleRow.DayKind = dayKindID;
                    scheduleRow.ExtNumber = extNumber;
                    scheduleRow.Comment = comment;
                    scheduleRow.BeginTime = beginTime;
                    scheduleRow.EndTime = endTime;
                    scheduleRow.Wayout = wayoutID;

                    this.Data.Schedule.Rows.Add(scheduleRow);

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
	    }

	    public void ScheduleUpdate(int scheduleID, int routeID, int dayKindID, string extNumber, string comment, DateTime beginTime, DateTime endTime, int wayoutID)
	    {
            lock (this.Data)
            {
                try
                {
                    ScheduleRow scheduleRow = this.Data.Schedule.FindRow(scheduleID);

                    scheduleRow.BeginEdit();

                    scheduleRow.Route = routeID;
                    scheduleRow.DayKind = dayKindID;
                    scheduleRow.ExtNumber = extNumber;
                    scheduleRow.Comment = comment;
                    scheduleRow.BeginTime = beginTime;
                    scheduleRow.EndTime = endTime;
                    scheduleRow.Wayout = wayoutID;

                    scheduleRow.EndEdit();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
	    }

	    public void ScheduleDelete(int scheduleID)
	    {
            lock (this.Data)
            {
                try
                {
                    ScheduleRow scheduleRow = this.Data.Schedule.FindRow(scheduleID);

                    scheduleRow.Delete();

                    this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
	    }

	    #endregion
	}
}
