using System;
using System.Diagnostics;
using System.Collections;
using System.Data.Common;
using System.Data;
using System.ComponentModel;

namespace FORIS.DataAccess
{

	public class GenericDataAdapter : DataAdapter, ICloneable, IDbDataAdapter
	{
		public const string DefaultSourceTableName = "Table";
		const string DefaultSourceColumnName = "Column";

		bool disposed = false;	
		IDbCommand deleteCommand;
		IDbCommand insertCommand;
		IDbCommand selectCommand;
		IDbCommand updateCommand;

		public GenericDataAdapter () 	
			: this (CreateCommand())
		{
		}

		public GenericDataAdapter (IDbCommand selectCommand) 
		{
			DeleteCommand = null;
			InsertCommand = null;
			SelectCommand = selectCommand;
			UpdateCommand = null;
		}

		public GenericDataAdapter (string selectCommandText, IDbConnection selectConnection) 
			: this (CreateCommand(selectCommandText, selectConnection))
		{ 
		}

		public GenericDataAdapter (string selectCommandText, string selectConnectionString)
			: this (selectCommandText, CreateConnection(selectConnectionString))
		{
		}

		protected static IDbCommand CreateCommand(string text, IDbConnection connection)
		{
			throw new NotImplementedException();
		}

		protected static IDbConnection CreateConnection(string selectConnectionString)
		{
			throw new NotImplementedException();
		}

		protected static IDbCommand CreateCommand()
		{
			return Globals.TssDatabaseManager.DefaultDataBase.Command("", null);
		}

		[DataSysDescription ("Used during Update for deleted rows in DataSet.")]
		[DefaultValue (null)]
		public IDbCommand DeleteCommand 
		{
			get { return deleteCommand; }
			set { deleteCommand = value; }
		}

		[DataSysDescription ("Used during Update for new rows in DataSet.")]
		[DefaultValue (null)]
		public IDbCommand InsertCommand 
		{
			get { return insertCommand; }
			set { insertCommand = value; }
		}

		[DataSysDescription ("Used during Fill/FillSchema.")]
		[DefaultValue (null)]
		public IDbCommand SelectCommand 
		{
			get { return selectCommand; }
			set { selectCommand = value; }
		}

		[DataSysDescription ("Used during Update for modified rows in DataSet.")]
		[DefaultValue (null)]
		public IDbCommand UpdateCommand 
		{
			get { return updateCommand; }
			set { updateCommand = value; }
		}

		IDbCommand IDbDataAdapter.DeleteCommand 
		{
			get { return DeleteCommand; }
			set 
			{ 
				DeleteCommand = value;
			}
		}

		IDbCommand IDbDataAdapter.InsertCommand 
		{
			get { return InsertCommand; }
			set 
			{ 
				InsertCommand = value;
			}
		}

		IDbCommand IDbDataAdapter.SelectCommand 
		{
			get { return SelectCommand; }
			set 
			{ 
				SelectCommand = value;
			}
		}

		IDbCommand IDbDataAdapter.UpdateCommand 
		{
			get { return UpdateCommand; }
			set 
			{ 
				UpdateCommand = value;
			}
		}


		ITableMappingCollection IDataAdapter.TableMappings 
		{
			get { return TableMappings; }
		}

		protected RowUpdatedEventArgs CreateRowUpdatedEvent (DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping) 
		{
			return new TssRowUpdatedEventArgs (dataRow, command, statementType, tableMapping);
		}


		protected RowUpdatingEventArgs CreateRowUpdatingEvent (DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping) 
		{
			return new TssRowUpdatingEventArgs (dataRow, command, statementType, tableMapping);
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing) 
			{
				IDbDataAdapter da = (IDbDataAdapter) this;
				if (da.SelectCommand != null) 
				{
					da.SelectCommand.Dispose();
					da.SelectCommand = null;
				}
				if (da.InsertCommand != null) 
				{
					da.InsertCommand.Dispose();
					da.InsertCommand = null;
				}
				if (da.UpdateCommand != null) 
				{
					da.UpdateCommand.Dispose();
					da.UpdateCommand = null;
				}
				if (da.DeleteCommand != null) 
				{
					da.DeleteCommand.Dispose();
					da.DeleteCommand = null;
				}
			}
			if (!disposed) 
			{
				if (disposing) 
				{
					// Release managed resources
				}
				// Release unmanaged resources
				disposed = true;
			}
		}

		protected void OnRowUpdated (RowUpdatedEventArgs value) 
		{
			if (RowUpdated != null)
				RowUpdated (this, value);
		}

		protected void OnRowUpdating (RowUpdatingEventArgs value) 
		{
			if (RowUpdating != null)
				RowUpdating (this, value);
		}

		private FillErrorEventArgs CreateFillErrorEvent (DataTable dataTable, object[] values, Exception e)
		{
			FillErrorEventArgs args = new FillErrorEventArgs (dataTable, values);
			args.Errors = e;
			args.Continue = false;
			return args;
		}

		[DataSysDescription ("Event triggered before every DataRow during Update.")]
		public event RowUpdatedEventHandler RowUpdated;

		[DataSysDescription ("Event triggered after every DataRow during Update.")]
		public event RowUpdatingEventHandler RowUpdating;

		public override int Fill(DataSet dataSet)
		{
			return Fill(dataSet, 0, 0, DefaultSourceTableName, SelectCommand, CommandBehavior.Default);
		}

		public int Fill(DataTable dataTable) 
		{
			if (dataTable == null)
			{
				throw new NullReferenceException();
			}

			return Fill(dataTable, SelectCommand, CommandBehavior.Default);
		}

		public int Fill(DataSet dataSet, string srcTable) 
		{
			return Fill(dataSet, 0, 0, srcTable, SelectCommand, CommandBehavior.Default);
		}

		protected virtual int Fill(DataTable dataTable, IDataReader dataReader) 
		{
			if (dataReader.FieldCount == 0) 
			{
				dataReader.Close ();
				return 0;
			}
			
			int count = 0;

			try 
			{
				string tableName = SetupSchema (SchemaType.Mapped, dataTable.TableName);
				if (tableName != null) 
				{
					dataTable.TableName = tableName;
					FillTable (dataTable, dataReader, 0, 0, ref count);
				}
			} 
			finally 
			{
				dataReader.Close ();
			}

			return count;
		}

		protected virtual int Fill(DataTable dataTable, IDbCommand command, CommandBehavior behavior) 
		{
			CommandBehavior commandBehavior = behavior;
			// first see that the connection is not close.
			if (command.Connection.State == ConnectionState.Closed) 
			{
				command.Connection.Open ();
				commandBehavior |= CommandBehavior.CloseConnection;
			}
			return Fill(dataTable, command.ExecuteReader (commandBehavior));
		}

		public int Fill(DataSet dataSet, int startRecord, int maxRecords, string srcTable) 
		{
			return this.Fill(dataSet, startRecord, maxRecords, srcTable, SelectCommand, CommandBehavior.Default);
		}

		protected virtual int Fill(DataSet dataSet, string srcTable, IDataReader dataReader, int startRecord, int maxRecords) 
		{
			if (startRecord < 0)
				throw new ArgumentException ("The startRecord parameter was less than 0.");
			if (maxRecords < 0)
				throw new ArgumentException ("The maxRecords parameter was less than 0.");

			DataTable dataTable;
			int resultIndex = 0;
			int count = 0;
			
			try 
			{
				string tableName = srcTable;
				do 
				{
					tableName = SetupSchema (SchemaType.Mapped, tableName);
					if (tableName != null) 
					{
						// check if the table exists in the dataset
						if (dataSet.Tables.Contains (tableName)) 
							// get the table from the dataset
							dataTable = dataSet.Tables [tableName];
						else 
						{
							dataTable = new DataTable(tableName);
							dataSet.Tables.Add (dataTable);
						}

						if (!FillTable (dataTable, dataReader, startRecord, maxRecords, ref count)) 
						{
							continue;
						}

						tableName = String.Format ("{0}{1}", srcTable, ++resultIndex);

						startRecord = 0;
						maxRecords = 0;
					}
				} while (dataReader.NextResult ());
			} 
			finally 
			{
				dataReader.Close ();
			}

			return count;
		}
		
		protected virtual int Fill(DataSet dataSet, int startRecord, int maxRecords, string srcTable, IDbCommand command, CommandBehavior behavior) 
		{
			if (MissingSchemaAction == MissingSchemaAction.AddWithKey)
				behavior |= CommandBehavior.KeyInfo;
			CommandBehavior commandBehavior = behavior;
			if (command.Connection.State == ConnectionState.Closed) 
			{
				command.Connection.Open ();
				commandBehavior |= CommandBehavior.CloseConnection;
			}
			return Fill(dataSet, srcTable, command.ExecuteReader (commandBehavior), startRecord, maxRecords);
		}

		private bool FillTable (DataTable dataTable, IDataReader dataReader, int startRecord, int maxRecords, ref int counter) 
		{
			if (dataReader.FieldCount == 0) 
			{
				return false;
			}

			int counterStart = counter;
			
			int[] mapping = BuildSchema (dataReader, dataTable, SchemaType.Mapped);

			for (int i = 0; i < startRecord; i++) 
			{
				dataReader.Read ();
			}

			while (dataReader.Read () && (maxRecords == 0 || (counter - counterStart) < maxRecords)) 
			{
				try 
				{
					dataTable.BeginLoadData ();
					//dataTable.LoadDataRow (dataReader, mapping, AcceptChangesDuringFill);
					dataTable.EndLoadData ();
					counter++;
				} 
				catch (Exception e) 
				{
					object[] readerArray = new object[dataReader.FieldCount];
					object[] tableArray = new object[mapping.Length];
					// we get the values from the datareader
					dataReader.GetValues (readerArray);
					// copy from datareader columns to table columns according to given mapping
					for (int i = 0; i < mapping.Length; i++) 
					{
						tableArray[i] = readerArray[mapping[i]];
					}
					FillErrorEventArgs args = CreateFillErrorEvent (dataTable, tableArray, e);
					//OnFillError (args);
					if(!args.Continue) 
					{
						return false;
					}
				}
			}
			return true;
		}

		public override DataTable[] FillSchema (DataSet dataSet, SchemaType schemaType) 
		{
			return FillSchema (dataSet, schemaType, SelectCommand, DefaultSourceTableName, CommandBehavior.Default);
		}

		public DataTable FillSchema (DataTable dataTable, SchemaType schemaType) 
		{
			return FillSchema (dataTable, schemaType, SelectCommand, CommandBehavior.Default);
		}

		public DataTable[] FillSchema (DataSet dataSet, SchemaType schemaType, string srcTable) 
		{
			return FillSchema (dataSet, schemaType, SelectCommand, srcTable, CommandBehavior.Default);
		}

		protected virtual DataTable FillSchema (DataTable dataTable, SchemaType schemaType, IDbCommand command, CommandBehavior behavior) 
		{
			behavior |= CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo;
			if (command.Connection.State == ConnectionState.Closed) 
			{
				command.Connection.Open ();
				behavior |= CommandBehavior.CloseConnection;
			}

			IDataReader reader = command.ExecuteReader (behavior);
			try
			{
				string tableName =  SetupSchema (schemaType, dataTable.TableName);
				if (tableName != null)
				{
					BuildSchema (reader, dataTable, schemaType);
				}
			}
			finally
			{
				reader.Close ();
			}
			return dataTable;
		}

		protected virtual DataTable[] FillSchema (DataSet dataSet, SchemaType schemaType, IDbCommand command, string srcTable, CommandBehavior behavior) 
		{
			behavior |= CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo;
			if (command.Connection.State == ConnectionState.Closed) 
			{
				command.Connection.Open ();
				behavior |= CommandBehavior.CloseConnection;
			}

			IDataReader reader = command.ExecuteReader (behavior);
			ArrayList output = new ArrayList ();
			string tableName = srcTable;
			int index = 0;
			DataTable table;
			try
			{
				tableName = SetupSchema (schemaType, tableName);
				if (tableName != null)
				{
					if (dataSet.Tables.Contains (tableName))
						table = dataSet.Tables [tableName];	
					else
					{
						table = new DataTable(tableName);
						dataSet.Tables.Add (table);
					}
					BuildSchema (reader, table, schemaType);
					output.Add (table);
					tableName = String.Format ("{0}{1}", srcTable, ++index);
				}
			}
			finally
			{
				reader.Close ();
			}
			return (DataTable[]) output.ToArray (typeof (DataTable));
		}

		private string SetupSchema (SchemaType schemaType, string sourceTableName)
		{
			DataTableMapping tableMapping = null;

			if (schemaType == SchemaType.Mapped) 
			{
				tableMapping = DataTableMappingCollection.GetTableMappingBySchemaAction (TableMappings, sourceTableName, sourceTableName, MissingMappingAction);

				if (tableMapping != null)
					return tableMapping.DataSetTable;
				return null;
			}
			else
				return sourceTableName;
		}

		[EditorBrowsable (EditorBrowsableState.Advanced)]
		public override IDataParameter[] GetFillParameters () 
		{
			IDataParameter[] parameters = new IDataParameter[SelectCommand.Parameters.Count];
			SelectCommand.Parameters.CopyTo (parameters, 0);
			return parameters;
		}
		
		// this method builds the schema for a given datatable. it returns a int array with 
		// "array[ordinal of datatable column] == index of source column in data reader".
		// each column in the datatable has a mapping to a specific column in the datareader,
		// the int array represents this match.
		private int[] BuildSchema (IDataReader reader, DataTable table, SchemaType schemaType)
		{
			int readerIndex = 0;
			int[] mapping = new int[reader.FieldCount]; // mapping the reader indexes to the datatable indexes
			ArrayList primaryKey = new ArrayList ();
			ArrayList sourceColumns = new ArrayList ();

			foreach (DataRow schemaRow in reader.GetSchemaTable ().Rows) 
			{
				// generate a unique column name in the source table.
				string sourceColumnName;
				if (schemaRow.IsNull("ColumnName"))
					sourceColumnName = DefaultSourceColumnName;
				else 
					sourceColumnName = (string) schemaRow ["ColumnName"];

				string realSourceColumnName = sourceColumnName;

				for (int i = 1; sourceColumns.Contains (realSourceColumnName); i += 1) 
					realSourceColumnName = String.Format ("{0}{1}", sourceColumnName, i);
				sourceColumns.Add(realSourceColumnName);

				// generate DataSetColumnName from DataTableMapping, if any
				string dsColumnName = realSourceColumnName;
				DataTableMapping tableMapping = null;
				tableMapping = DataTableMappingCollection.GetTableMappingBySchemaAction (TableMappings, table.TableName, table.TableName, MissingMappingAction); 
				if (tableMapping != null) 
				{
					
					table.TableName = tableMapping.DataSetTable;
					// check to see if the column mapping exists
					DataColumnMapping columnMapping = DataColumnMappingCollection.GetColumnMappingBySchemaAction(tableMapping.ColumnMappings, realSourceColumnName, MissingMappingAction);
					if (columnMapping != null)
					{
						DataColumn col =
							columnMapping.GetDataColumnBySchemaAction(
							table ,
							(Type)schemaRow["DataType"],
							MissingSchemaAction);

						if (col != null)
						{
							// if the column is not in the table - add it.
							if (table.Columns.IndexOf(col) == -1)
							{
								if (MissingSchemaAction == MissingSchemaAction.Add || MissingSchemaAction == MissingSchemaAction.AddWithKey)
									table.Columns.Add(col);
							}

							if (!schemaRow["IsKey"].Equals (DBNull.Value))
								if ((bool) (schemaRow ["IsKey"]))
									primaryKey.Add (col);
							
							// add the ordinal of the column as a key and the index of the column in the datareader as a value.
							mapping[col.Ordinal] = readerIndex;
						}
					}
				}
				readerIndex++;
			}
			if (primaryKey.Count > 0)
				table.PrimaryKey = (DataColumn[])(primaryKey.ToArray(typeof (DataColumn)));

			return mapping;
		}

		object ICloneable.Clone ()
		{
			throw new NotImplementedException ();
		}

		public int Update (DataRow[] dataRows) 
		{
			if (dataRows == null)
				throw new ArgumentNullException("dataRows");
			
			if (dataRows.Length == 0)
				return 0;

			if (dataRows[0] == null)
				throw new ArgumentException("dataRows[0].");

			DataTable table = dataRows[0].Table;
			if (table == null)
				throw new ArgumentException("table is null reference.");
			
			// all rows must be in the same table
			for (int i = 0; i < dataRows.Length; i++)
			{
				if (dataRows[i] == null)
					throw new ArgumentException("dataRows[" + i + "].");
				if (dataRows[i].Table != table)
					throw new ArgumentException(
						" DataRow["
						+ i
						+ "] is from a different DataTable than DataRow[0].");
			}
			
			// get table mapping for this rows
			DataTableMapping tableMapping = null;
			if (TableMappings.Contains(table.TableName))
			{
				tableMapping = TableMappings.GetByDataSetTable(table.TableName);
			}
			else
			{
				tableMapping = DataTableMappingCollection.GetTableMappingBySchemaAction(
					TableMappings,
					table.TableName,
					table.TableName,
					MissingMappingAction);
				if (tableMapping != null) 
				{
					foreach (DataColumn col in table.Columns) 
					{
						if (tableMapping.ColumnMappings.IndexOf (col.ColumnName) >= 0)
							continue;
						DataColumnMapping columnMapping = DataColumnMappingCollection.GetColumnMappingBySchemaAction (tableMapping.ColumnMappings, col.ColumnName, MissingMappingAction);
						if (columnMapping == null)
							columnMapping = new DataColumnMapping (col.ColumnName, col.ColumnName);
						tableMapping.ColumnMappings.Add (columnMapping);
					}
				} 
				else 
				{
					ArrayList cmc = new ArrayList ();
					foreach (DataColumn col in table.Columns)
						cmc.Add (new DataColumnMapping (col.ColumnName, col.ColumnName));
					tableMapping =
						new DataTableMapping (
						table.TableName,
						table.TableName,
						cmc.ToArray (typeof (DataColumnMapping)) as DataColumnMapping []);
				}
			}

			DataRow[] copy = new DataRow [dataRows.Length];
			Array.Copy(dataRows, 0, copy, 0, dataRows.Length);
			return Update(copy, tableMapping);
		}

		public override int Update (DataSet dataSet) 
		{
			return Update (dataSet, DefaultSourceTableName);
		}

		public int Update (DataTable dataTable) 
		{
			/*
				int index = TableMappings.IndexOfDataSetTable (dataTable.TableName);
				if (index < 0)
					throw new ArgumentException ();
				return Update (dataTable, TableMappings [index]);
			*/
			DataTableMapping tableMapping = TableMappings.GetByDataSetTable (dataTable.TableName);
			if (tableMapping == null)
			{
				tableMapping = DataTableMappingCollection.GetTableMappingBySchemaAction (
					TableMappings,
					dataTable.TableName,
					dataTable.TableName,
					MissingMappingAction);
				if (tableMapping != null) 
				{
					foreach (DataColumn col in dataTable.Columns) 
					{
						if (tableMapping.ColumnMappings.IndexOf (col.ColumnName) >= 0)
							continue;
						DataColumnMapping columnMapping = DataColumnMappingCollection.GetColumnMappingBySchemaAction (tableMapping.ColumnMappings, col.ColumnName, MissingMappingAction);
						if (columnMapping == null)
							columnMapping = new DataColumnMapping (col.ColumnName, col.ColumnName);
						tableMapping.ColumnMappings.Add (columnMapping);
					}
				} 
				else 
				{
					ArrayList cmc = new ArrayList ();
					foreach (DataColumn col in dataTable.Columns)
						cmc.Add (new DataColumnMapping (col.ColumnName, col.ColumnName));
					tableMapping =
						new DataTableMapping (
						dataTable.TableName,
						dataTable.TableName,
						cmc.ToArray (typeof (DataColumnMapping)) as DataColumnMapping []);
				}
			}
			return Update (dataTable, tableMapping);
		}

		private int Update (DataTable dataTable, DataTableMapping tableMapping)
		{
			DataRow[] rows = new DataRow [dataTable.Rows.Count];
			dataTable.Rows.CopyTo (rows, 0);
			return Update (rows, tableMapping);
		}

		
		protected virtual int Update (DataRow[] dataRows, DataTableMapping tableMapping) 
		{
			int updateCount = 0;

			foreach (DataRow row in dataRows) 
			{
				StatementType statementType = StatementType.Update;
				IDbCommand command = null;
				string commandName = String.Empty;
				bool useCommandBuilder = false;

				switch (row.RowState) 
				{
					case DataRowState.Added:
						statementType = StatementType.Insert;
						command = InsertCommand;
						commandName = "Insert";
						break;
					case DataRowState.Deleted:
						statementType = StatementType.Delete;
						command = DeleteCommand;
						commandName = "Delete";
						break;
					case DataRowState.Modified:
						statementType = StatementType.Update;
						command = UpdateCommand;
						commandName = "Update";
						break;
					case DataRowState.Unchanged:
						continue;
					case DataRowState.Detached:
						throw new NotImplementedException ();
				}

				if (command == null)
					useCommandBuilder = true;

				RowUpdatingEventArgs args = CreateRowUpdatingEvent (row, command, statementType, tableMapping);
				OnRowUpdating (args);

				if (args.Status == UpdateStatus.ErrorsOccurred)
					throw (args.Errors);

				if (command == null && args.Command != null)
					command = args.Command;
				else if (command == null)
					throw new InvalidOperationException (String.Format ("Update requires a valid {0}Command when passed a DataRow collection with modified rows.", commandName));

				if (!useCommandBuilder) 
				{
					DataColumnMappingCollection columnMappings = tableMapping.ColumnMappings;

					foreach (IDataParameter parameter in command.Parameters) 
					{
						string dsColumnName = parameter.SourceColumn;
						if (columnMappings.Contains(parameter.SourceColumn))
							dsColumnName = columnMappings [parameter.SourceColumn].DataSetColumn;
						DataRowVersion rowVersion = DataRowVersion.Default;

						// Parameter version is ignored for non-update commands
						if (statementType == StatementType.Update) 
							rowVersion = parameter.SourceVersion;
						if (statementType == StatementType.Delete) 
							rowVersion = DataRowVersion.Original;

						if (row.Table.Columns.Contains(dsColumnName))
						{
							parameter.Value = row [dsColumnName, rowVersion];
						}
					}
				}
				
				CommandBehavior commandBehavior = CommandBehavior.Default;
				if (command.Connection.State == ConnectionState.Closed) 
				{
					command.Connection.Open ();
					commandBehavior |= CommandBehavior.CloseConnection;
				}
				
				IDataReader reader = null;
				try
				{
					// use ExecuteReader because we want to use the commandbehavior parameter.
					// so the connection will be closed if needed.
					object obj = command.ExecuteScalar();
					if ((obj is Int16) || (obj is Int32))
					{
						int identity = -1;
						if (obj is Int16)
						{
							identity = (int)(Int16)obj;
						}
						if (obj is Int32)
						{
							identity = (int)obj;
						}
						DataColumn identityColumn = null;
						foreach (DataColumn c in row.Table.Columns)
						{
							if (c.AutoIncrement)
							{
								identityColumn = c;
							}
						}
						if (identityColumn != null)
						{
							row[identityColumn.ColumnName] = identity;
						}
					}
					/*
					int tmp = reader.RecordsAffected;
					// if the execute does not effect any rows we throw an exception.
					if (tmp == 0)
						throw new DBConcurrencyException("Concurrency violation: the " + commandName +"Command affected 0 records.");
					updateCount += tmp;
					*/
					updateCount ++;
					OnRowUpdated (CreateRowUpdatedEvent (row, command, statementType, tableMapping));
					row.AcceptChanges ();
				}
				catch (Exception e)
				{
					if (ContinueUpdateOnError)
						row.RowError = e.Message;// do somthing with the error
					else
						throw e;
				}
				finally
				{
					if (reader != null)
						reader.Close ();
				}
			}
			
			return updateCount;
		}

		public int Update (DataSet dataSet, string sourceTable) 
		{
			MissingMappingAction mappingAction = MissingMappingAction;
			if (mappingAction == MissingMappingAction.Ignore)
				mappingAction = MissingMappingAction.Error;
			DataTableMapping tableMapping = DataTableMappingCollection.GetTableMappingBySchemaAction (TableMappings, sourceTable, sourceTable, mappingAction);

			DataTable dataTable = dataSet.Tables[tableMapping.DataSetTable];
			if (dataTable == null)
				throw new ArgumentException ("sourceTable");

			return Update (dataTable, tableMapping);
		}
	}
}