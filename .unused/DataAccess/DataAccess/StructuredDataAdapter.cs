﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Data;
using System.Reflection;
using System.ComponentModel;

namespace FORIS.DataAccess
{
	public class StructuredDataAdapter : Component
	{
		bool disposed = false;

		IDbCommand selectCommand;

		public StructuredDataAdapter()
			: this(CreateCommand())
		{
		}
		public StructuredDataAdapter(IDbCommand selectCommand)
		{
			SelectCommand = selectCommand;
		}
		public StructuredDataAdapter(string selectCommandText, IDbConnection selectConnection)
			: this(CreateCommand(selectCommandText, selectConnection))
		{
		}
		public StructuredDataAdapter(string selectCommandText, string selectConnectionString)
			: this(selectCommandText, CreateConnection(selectConnectionString))
		{
		}
		protected static IDbCommand CreateCommand()
		{
			return Globals.TssDatabaseManager.DefaultDataBase.Command("", null);
		}
		protected static IDbCommand CreateCommand(string text, IDbConnection connection)
		{
			throw new NotImplementedException();
		}
		protected static IDbConnection CreateConnection(string selectConnectionString)
		{
			throw new NotImplementedException();
		}
		[DataSysDescription("Used during Fill/FillSchema.")]
		[DefaultValue(null)]
		public IDbCommand SelectCommand
		{
			get { return selectCommand; }
			set { selectCommand = value; }
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				StructuredDataAdapter da = this;
				if (da.SelectCommand != null)
				{
					da.SelectCommand.Dispose();
					da.SelectCommand = null;
				}
			}
			if (!disposed)
			{
				if (disposing)
				{
					// Release managed resources
				}
				// Release unmanaged resources
				disposed = true;
			}
		}
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IDataParameter[] GetFillParameters()
		{
			IDataParameter[] parameters = new IDataParameter[SelectCommand.Parameters.Count];
			SelectCommand.Parameters.CopyTo(parameters, 0);
			return parameters;
		}
		public int Fill(object ss, params string[] tables)
		{
			CommandBehavior commandBehavior = CommandBehavior.Default;
			// first see that the connection is not close.
			if (this.selectCommand.Connection.State == ConnectionState.Closed)
			{
				this.selectCommand.Connection.Open();
				commandBehavior |= CommandBehavior.CloseConnection;
			}
			return Fill(ss, this.selectCommand, commandBehavior, tables);
		}
		protected virtual int Fill(object ss, IDbCommand command, CommandBehavior commandBehavior, params string[] tables)
		{
			Type ssType = ss.GetType();
			Hashtable tablename_to_array = new Hashtable(tables.Length);
			// Load data
			IDataReader reader = command.ExecuteReader(commandBehavior);
			try
			{
				int pos = 0;
				do
				{
					string tablename = tables[pos++].ToUpper();
					FieldInfo fi = ssType.GetField("m_" + NameHelper.ToPascal(tablename));
					Type ft = fi.FieldType;
					if (ft.IsArray)
					{
						string basic_name = ft.FullName.Replace("[]", "");
						ft = ft.GetElementType();
						if (ft == null)
						{
							throw new ApplicationException("phase 5 " + basic_name);
						}
					}
					ArrayList list;
					if (tablename_to_array.ContainsKey(tablename))
					{
						list = (ArrayList)tablename_to_array[tablename];
					}
					else
					{
						list = new ArrayList(1000);
						tablename_to_array.Add(tablename, list);
					}
					for (; reader.Read() == true;)
					{
						object obj = Activator.CreateInstance(ft);
						IFieldAssignable fa = (IFieldAssignable)obj;
						fa.CopyFrom(reader);
						list.Add(fa);
					}
				}
				while (reader.NextResult());
			}
			finally
			{
				reader.Close();
			}
			IFillFrom ff = (IFillFrom)ss;
			ff.FillFrom(tablename_to_array);
			return 0;
		}
		public int Replicate(object ss)
		{
			using (IDbConnection connection = Globals.TssDatabaseManager.DefaultDataBase.Connection())
			{
				connection.Open();
				try
				{
					using (IDbTransaction transaction = connection.BeginTransaction())
					{
						try
						{
							int count = WalkThroughAllTables(ss, connection, transaction);
							transaction.Commit();
							return count;
						}
						catch (Exception ex)
						{
							Trace.WriteLine(ex.ToString());
							transaction.Rollback();
							throw;
						}
					}
				}
				finally
				{
					connection.Close();
				}
			}
		}
		protected int WalkThroughAllTables(object ss, IDbConnection connection, IDbTransaction transaction)
		{
			int res = 0;
			// walk through all tables in structure
			Type ssType = ss.GetType();
			foreach (FieldInfo fi in ssType.GetFields())
			{
				// Get array
				object array = fi.GetValue(ss);
				if (array != null)
				{
					// Get name of the table
					string name = fi.Name;
					if (fi.Name.StartsWith("m_"))
					{
						name = fi.Name.Substring(2);
					}
					// Get stored procedure for the table
					string proc_name = string.Format("Replicate{0}", name);
					using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand(proc_name, connection, transaction))
					{
						/*int cnt = */
						ProcessArray(array, cmd);
					}
				}
			}
			return res;
		}
		protected int ProcessArray(object array, IDbCommand cmd)
		{
			Array arr = (Array)array;
			int res = 0;
			foreach (object o in arr)
			{
				res += ProcessObject(o, cmd);
			}
			return res;
		}
		protected int ProcessObject(object o, IDbCommand cmd)
		{
			Type ft = o.GetType();
			foreach (FieldInfo fi in ft.GetFields())
			{
				if (fi.Name.EndsWith("IsNull"))
				{
					continue;
				}
				string name = fi.Name;
				if (fi.Name.StartsWith("m_"))
				{
					name = name.Substring(2);
				}
				FieldInfo isnull = ft.GetField(fi.Name + "IsNull");
				if (isnull != null)
				{
					bool isNullBoolVal = (bool)isnull.GetValue(o);
					if (isNullBoolVal)
					{
						// process null fields
						((IDataParameter)cmd.Parameters["@" + name]).Value = DBNull.Value;
					}
					else
					{
						((IDataParameter)cmd.Parameters["@" + name]).Value = fi.GetValue(o);
					}
				}
				else
				{
					((IDataParameter)cmd.Parameters["@" + name]).Value = fi.GetValue(o);
				}
			}
			int res = cmd.ExecuteNonQuery();
			return res;
		}
	}
}