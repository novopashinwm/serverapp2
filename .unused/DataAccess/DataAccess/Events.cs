﻿using System;
using System.Data;
using System.Data.Common;

namespace FORIS.DataAccess
{
	[Serializable]
	public delegate void RowUpdatedEventHandler(object sender, RowUpdatedEventArgs e);
	[Serializable]
	public delegate void RowUpdatingEventHandler(object sender, RowUpdatingEventArgs e);
	public class TssRowUpdatedEventArgs : RowUpdatedEventArgs
	{
		public TssRowUpdatedEventArgs(DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping)
			: base(dataRow, command, statementType, tableMapping)
		{
		}
	}
	public class TssRowUpdatingEventArgs : RowUpdatingEventArgs
	{
		public TssRowUpdatingEventArgs(DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping)
			: base(dataRow, command, statementType, tableMapping)
		{
		}
	}
}