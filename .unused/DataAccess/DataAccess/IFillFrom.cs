using System;
using System.Collections;

namespace FORIS.DataAccess
{
	public interface IFillFrom
	{
		void FillFrom(Hashtable tables);
	}
}
