using System.Data;

namespace FORIS.DataAccess
{
	/// <summary>
	/// Summary description for IFieldAssignable.
	/// </summary>
	public interface IFieldAssignable
	{
		void CopyFrom(IDataReader r);
	}
}
