﻿using System;
using System.Data;
using System.Text;
using FORIS.DataAccess;
using Npgsql;
using PostgreSQL;

namespace DataAccess.PostgreSQL
{
    public class TssSqlProvider : DbProvider
    {
        public string Server;

        public string Database;

        public string User;

        public string Password;

        public string ConnectionString;

        public override ICommandParameterAdapter PrepareParameters(string procedure, string connection)
        {
            return new PostgreSqlCommandParameterAdapter(procedure, connection);
        }

        public override ICommandParameterAdapter PrepareParameters(string procedure, IDbConnection connection, IDbTransaction tran)
        {
            return new PostgreSqlCommandParameterAdapter(procedure, connection, tran);
        }

        public override IDbConnection Connection(string connectionString)
        {
            return new NpgsqlConnection(connectionString);
        }

        public override IDbDataParameter Parameter()
        {
            return new NpgsqlParameter();
        }

        public override IDbCommand Command()
        {
            return new NpgsqlCommand();
        }

        public override IDbDataAdapter Adapter()
        {
            return new NpgsqlDataAdapter();
        }

        public string ConstructConnectionString()
        {
            StringBuilder sb = new StringBuilder(256);
            sb.AppendFormat("server={0};", Server);
            sb.AppendFormat("database={0};", Database);
            sb.AppendFormat("user={0};password={1}", User, Password);
            return sb.ToString();
        }
    }
}
