﻿using System.Reflection;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyProduct("АСКУО «Ufin»")]
[assembly: AssemblyTrademark("АСКУО «Ufin»")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.5.0.1")]
//[assembly: AssemblyFileVersion("2.5.0.0")]