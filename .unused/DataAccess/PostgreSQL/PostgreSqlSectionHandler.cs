﻿using System.Configuration;
using System.Xml;
using DataAccess.PostgreSQL;

namespace FORIS.DataAccess
{
    public class PostgreSqlSectionHandler : ISectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            var provider = new TssSqlProvider();
            PopulateSqlProvider(provider, section);
            var database = new TssDatabase(provider.ConnectionString, provider);
            return database;
        }

        protected void PopulateSqlProvider(TssSqlProvider provider, XmlNode node)
        {
            ParseChilds(provider, node);
            provider.ConnectionString = node.Attributes != null && node.Attributes["connectionString"] != null 
                ? node.Attributes["connectionString"].Value 
                : provider.ConstructConnectionString();
        }

        protected void ParseChilds(TssSqlProvider provider, XmlNode node)
        {
            foreach (XmlNode ch in node.ChildNodes)
            {
                if (ch.NodeType == XmlNodeType.Comment) continue;
                if (ch.Attributes == null) continue;
                string name = ch.Attributes["key"].Value;
                switch (name)
                {
                    case "server":
                    {
                        provider.Server = ch.Attributes["value"].Value;
                    }
                        break;
                    case "database":
                    {
                        provider.Database = ch.Attributes["value"].Value;
                    }
                        break;
                    case "user":
                    {
                        provider.User = ch.Attributes["value"].Value;
                    }
                        break;
                    case "password":
                    {
                        provider.Password = ch.Attributes["value"].Value;
                    }
                        break;
                    default:
                    {
                        throw new ConfigurationException("wrong key " + name);
                    }
                }
            }
        }
    }
}
