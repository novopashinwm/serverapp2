﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using FORIS.DataAccess;
using Npgsql;
using NpgsqlTypes;

namespace PostgreSQL
{
    public class PostgreSqlCommandParameterAdapter : ICommandParameterAdapter
    {
        private readonly PostgreSqlParameterInfo[] _paramsInfo;

        private const string ProcedureArgumentsQuery =
@"select n.id, n.name, coalesce(m.mode, 'i') as mode, coalesce(t1.typeid, t2.typeid) typeid, coalesce(pt1.typname, pt2.typname) typname
from pg_catalog.pg_proc p
join unnest(p.proargnames) with ordinality n(name, id) on true
left join unnest(p.proargtypes) with ordinality t1(typeid, id) on t1.id=n.id
left join unnest(p.proallargtypes) with ordinality t2(typeid, id) on t2.id=n.id
left join unnest(p.proargmodes) with ordinality m(mode, id) on m.id=n.id
left join pg_catalog.pg_type pt1 on pt1.oid = t1.typeid
left join pg_catalog.pg_type pt2 on pt2.oid = t2.typeid
where p.oid = '{0}'::regproc
order by n.id";

        public PostgreSqlCommandParameterAdapter(string procedure, string connection)
            : this(procedure, new NpgsqlConnection(connection), null)
        {
        }

        public PostgreSqlCommandParameterAdapter(string procedure, IDbConnection connection, IDbTransaction tran)
        {
            using (var cmd = new NpgsqlCommand(string.Format(ProcedureArgumentsQuery, procedure), (NpgsqlConnection)connection, (NpgsqlTransaction)tran))
            {
                var behavior = CommandBehavior.Default;
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                    behavior = CommandBehavior.CloseConnection;
                }
                using (NpgsqlDataReader reader = cmd.ExecuteReader(behavior))
                {
                    ArrayList list = new ArrayList();
                    var nameOrdinal = reader.GetOrdinal("name");
                    var typeIdOrdinal = reader.GetOrdinal("typeid");
                    var typeNameOrdinal = reader.GetOrdinal("typname");
                    var directionOrdinal = reader.GetOrdinal("mode");
                    while (reader.Read())
                    {
                        var name = reader.GetString(nameOrdinal);
                        var type = reader.GetString(typeNameOrdinal);
                        var direction = reader.GetString(directionOrdinal);

                        var paramInfo = new PostgreSqlParameterInfo
                        {
                            ParameterName = name,
                            SqlType = FormatType(type),
                            Direction = FormatDirection(name, direction, type)
                        };

                        list.Add(paramInfo);
                    }

                    _paramsInfo = (PostgreSqlParameterInfo[])list.ToArray(typeof(PostgreSqlParameterInfo));
                }
            }
        }

        private NpgsqlDbType FormatType(string type)
        {
            NpgsqlDbType dbType;
            var isArray = type.StartsWith("_");
            type = isArray ? type.Substring(1) : type;
            switch (type.ToLower())
            {
                case "int2":
                    dbType = NpgsqlDbType.Smallint;
                    break;
                case "int4":
                    dbType = NpgsqlDbType.Integer;
                    break;
                case "varchar":
                    dbType = NpgsqlDbType.Varchar;
                    break;
                case "bool":
                    dbType = NpgsqlDbType.Boolean;
                    break;
                case "float4":
                    dbType = NpgsqlDbType.Real;
                    break;
                case "numeric":
                    dbType = NpgsqlDbType.Numeric;
                    break;
                case "timestamp":
                    dbType = NpgsqlDbType.Timestamp;
                    break;
                default:
                    Debug.WriteLine(type);
                    dbType = NpgsqlDbType.Text;
                    break;
            }

            if (isArray)
                dbType |= NpgsqlDbType.Array;
            return dbType;
        }

        public static ParameterDirection FormatDirection(string name, string direction, string typeName)
        {
            if ((name == null) | (name == String.Empty))
            {
                return ParameterDirection.ReturnValue;
            }
            if (direction == "o")
                return ParameterDirection.Output;
            if (direction == "b")
                return ParameterDirection.InputOutput;
            if (direction == "i")
                return ParameterDirection.Input;
            throw new ApplicationException("Unexpected parameter mode");
        }

        public void Fill(IDbCommand icommand)
        {
            foreach (var paramInfo in _paramsInfo)
            {
                var param = new NpgsqlParameter(paramInfo.ParameterName , paramInfo.SqlType)
                {
                    Direction = paramInfo.Direction
                };
                param.SourceColumn = param.ParameterName;

                icommand.Parameters.Add(param);
            }
        }
    }
}
