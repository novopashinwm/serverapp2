﻿using System.Data;
using NpgsqlTypes;

namespace PostgreSQL
{
    public class PostgreSqlParameterInfo
    {
        public NpgsqlDbType SqlType;
        public string ParameterName;
        public ParameterDirection Direction;
    }
}
