﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:user="urn:my-scripts">

    <xsl:import href="common.xsl" /> 
    
    <xsl:output method="html" encoding="utf-8" doctype-public="yes" cdata-section-elements="script-noscript"/>
  
    <xsl:param name="date" />
    
    
    <xsl:template match="/">    
        <xsl:variable name="selectedPage">
            <xsl:apply-templates select="." mode="selectedPage" />
        </xsl:variable>
        
        <html>
            <head>
                <xsl:apply-templates select="." mode="head"/>
            </head>
            
            <body>
                <!--xsl:copy-of select="."/-->
                <div class="nav top">
                    <xsl:apply-templates select="." mode="headerMenu"/>
                    <!--xsl:apply-templates select="." mode="languageSelector"/-->
                    <div class="bl_mob">
                        <a href="/WebMap/mobile">
                            <xsl:value-of select="$locale/data[@name='mobileVersion']/value" />
                        </a>
                    </div>
                    <div class="clear"></div>
                </div>
                
                
                <div class="header">
                    <xsl:apply-templates select="." mode="header"/> 
                </div>
             
                <div class="content">
                    <div class="nav main">
                        <xsl:call-template name="mainMenu">
                            <xsl:with-param name="selectedPage" select="$selectedPage" />
                            <xsl:with-param name="path" select="$path" />
                        </xsl:call-template>
                    </div>
                    
                    <div class="article">
                        <xsl:apply-templates select="." mode="article"/>
                    </div>
                    
                    <div class="aside">
                        <xsl:apply-templates select="." mode="loginWindow"/>
                     
                        <xsl:choose>
                            <xsl:when test="($selectedPage = 'corporative')">
                                <xsl:call-template name="calculate">
                                    <xsl:with-param name="path" select="$path"/>
                                </xsl:call-template>
                            </xsl:when>
                       </xsl:choose>
                       
                        <xsl:call-template name="mobi"/>
                        
                        <xsl:choose>
                            <xsl:when test="($selectedPage = 'physical') or ($selectedPage = 'corporative')">
                                <xsl:call-template name="helpdesk"></xsl:call-template>
                            </xsl:when>
                        </xsl:choose>
                        
                    </div>
                    
                    
                    <div class="clear"></div>
 
                </div>
                
                <div class="empty"></div>
                <div class="footer">
                    
                    <xsl:apply-templates select="." mode="footer"/>
                </div>
                <xsl:apply-templates select="." mode="production_js"></xsl:apply-templates>
                
            </body>

        </html>
      
    </xsl:template>

</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
