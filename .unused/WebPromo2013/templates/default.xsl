﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
  <!ENTITY nbsp "&#160;">
  <!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:user="urn:my-scripts">
  <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />
  <!--xsl:output method="html" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="no" omit-xml-declaration="yes" standalone="omit" /-->

  <xsl:template match="/">
    <html>
      <body>
        Hello, world!
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>