﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:msxsl="urn:schemas-microsoft-com:xslt">


    <xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />
    
    
    <!-- Variable, that define language, name - locale -->
  <xsl:variable name="localMessageFile">
    <xsl:text>../includes/resources2/messages</xsl:text>
    <!--xsl:choose>
			<xsl:when test="contains(/response/@language, 'ru-') != 'true'">
				<xsl:text>.</xsl:text><xsl:value-of select="/response/@language" />
			</xsl:when>
		</xsl:choose-->
    <xsl:text>.</xsl:text>
    <xsl:value-of select="/response/@language" />
    <xsl:text>.xml</xsl:text>
  </xsl:variable>
  <xsl:variable name="locale" select="document($localMessageFile)/root" />
    
    
    <!-- Variable, that define current language path, name - path -->
   <xsl:variable name="path0" select="/response/@ApplicationPath"/>
   <xsl:variable name="path">
        <xsl:value-of select="$path0"/><xsl:text>/?lang=</xsl:text><xsl:value-of select="$lang"/>
   </xsl:variable>

    <xsl:variable name="resourceStringsFile">
    <xsl:text>../../includes/resources2/strings.xml</xsl:text>
  </xsl:variable>
    
  <xsl:variable name="resourceStrings" select="document($resourceStringsFile)/root" />



  <!-- AS: JS template without ajax'a -->
  <xsl:variable name="localJsName">

    <xsl:text>messages</xsl:text>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="/response/@language" />
    </xsl:variable>


    <xsl:variable name="lang">
        <xsl:choose>
            <xsl:when test="contains(/response/@language, 'en') = 'true'">
                <xsl:text>en</xsl:text>
            </xsl:when>
            <xsl:when test="contains(/response/@language, 'ru') = 'true'">
                <xsl:text>ru</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/response/@language" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="selectedPage" select="/response/@selectedPage"></xsl:variable>

    <xsl:variable name="logoPath">
        <xsl:value-of select="$path0"></xsl:value-of>
        <xsl:text>/includes/sources/images/</xsl:text>
        <xsl:if test="$lang= 'ru'">
            <xsl:text>logo_nika_ru.png</xsl:text>
        </xsl:if>
    </xsl:variable> 
    
  <!-- head -->
  <xsl:template match="response" mode="head">
      <xsl:param name="title" select="./@title" />
      <!-- for Facebook -->
      <meta property="og:title">
          <xsl:attribute name="content">
              <xsl:apply-templates select="." mode="title1"/>
          </xsl:attribute>
      </meta>
      <meta property="og:type" content="article" />
      <meta property="og:image" content="http://nika-glonass.ru{./@ApplicationPath}/includes/sources/images/nika_logo_small1.png" />
      <meta property="og:description" content="{/response/pageContent/promo/description/.}" />
      <!-- for Google -->
      <meta name="copyright" content="{$locale/data[@name='copyright']/value}" />
      <meta name="application-name" content="NIKA GLONASS" />


      <!-- for Twitter -->
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:image" content="{$logoPath}" />

      
      <title>
        <xsl:apply-templates select="." mode="title1"/>
      </title>
      <meta name="keywords" content="{/response/pageContent/promo/keywords/.}"/>

      <meta name="description" content="{/response/pageContent/promo/description/.}" />
      


          

            <link type="text/css" rel="stylesheet" href="{./@ApplicationPath}/includes/production/styles/production.css"/>

     
      <link rel="shortcut icon" href="{./@ApplicationPath}/includes/sources/images/favicon.ico"/>

      <xsl:call-template name="ie-fix"></xsl:call-template>

            
      
  </xsl:template>

    <xsl:template match="response" mode="headerMenu">
        <ul>
            <xsl:for-each select="/response/pageContent/promo/menu[@class='top']/page[@headerMenu]">
                <xsl:choose>
                    <xsl:when test="($selectedPage ='physical') or ($selectedPage ='corporative') or ($selectedPage ='about')">
                        <xsl:choose>
                            <xsl:when test="@id = $selectedPage">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="@id"/>
                                        <xsl:with-param name="target-id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="@id"/>
                                        <xsl:with-param name="target-id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="@id = 'physical'">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="@id"/>
                                        <xsl:with-param name="target-id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="@id"/>
                                        <xsl:with-param name="target-id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>   
                    </xsl:otherwise>
                </xsl:choose>
                
            </xsl:for-each>
        </ul>
    </xsl:template> 
    
  <!-- header -->
  <xsl:template match="response" mode="header">
      
     <a href="http://www.mts.ru" class="mts {$lang}" title="{$locale/data[@name='mtsCopyright']/svalue}" target="_blank"></a>

      <!--xsl:call-template name="mts_shop"/-->
      <xsl:call-template name="privateRoom"/>

     <a href="{$path}" class="nika {$lang}" title="{./@title}">
         <img src="{$logoPath}" />
     </a>

  </xsl:template>  

  <!-- footer -->
  <xsl:template match="response" mode="footer">
      <div class="inner-footer">
          <div class="info">

              
              <p>
                  &#169; 2009-<xsl:value-of select="/response/@year"/>.<xsl:text> </xsl:text><xsl:value-of select="$locale/data[@name='mtsCopyright']/value"></xsl:value-of>.<br/>
                  &#169; <xsl:value-of select="$locale/data[@name='stsCopyright']/value"></xsl:value-of>.<xsl:text> </xsl:text><xsl:value-of select="$locale/data[@name='rightsReserved']/value"></xsl:value-of> 
              </p>
          </div>
    </div>
  </xsl:template>
  
  




    <xsl:template match="response" mode="commonVariables">
        <script type="text/javascript">
            var NIKA_GLONASS = NIKA_GLONASS || {};
            NIKA_GLONASS.serverName = "<xsl:value-of select="./@ServerName"/>";
            NIKA_GLONASS.applicationPath = "<xsl:value-of select="./@ApplicationPath"/>";
            NIKA_GLONASS.departmentCountry = "<xsl:value-of select="./@DepartmentCountryName"/>";
            NIKA_GLONASS.currentMapName = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapinfoShort"/>";
            NIKA_GLONASS.currentMapShortName = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapShortName"/>";
            NIKA_GLONASS.currentMapGuid = "<xsl:apply-templates select="./mapdescriptions/MapDescription[current = 'true']" mode="mapGuid"/>";
            NIKA_GLONASS.currentCulture = "<xsl:value-of select="substring-before(./@language, '-')" />";
            NIKA_GLONASS.useDialer = ('<xsl:value-of select="./@useDialer" />' == 'true' ? true : false);
            NIKA_GLONASS.loggedUser = '<xsl:value-of select="./@loggedUser"/>';
            if(NIKA_GLONASS.loggedUser === 'guest'){NIKA_GLONASS.useDialer = false;}

            NIKA_GLONASS.serverDate = "<xsl:value-of select="./@serverDate" />";
            NIKA_GLONASS.defaultVehicleIcon = NIKA_GLONASS.applicationPath + "<xsl:value-of select="./@DefaultVehicleIcon"/>";
            NIKA_GLONASS.sessionStateTimeout = '<xsl:value-of select="./@sessionStateTimeout"/>';
            NIKA_GLONASS.localJsName = '<xsl:value-of select="$localJsName"/>';
        </script>
        
    </xsl:template>

    <xsl:template mode="production_js" match="response">
        
        <xsl:apply-templates select="." mode="commonVariables" />
        
        <xsl:choose>
            <xsl:when test="$lang = 'ru'">
                <script data-main="{./@ApplicationPath}/includes/production/scripts/build-ru/js/app.js" src="{./@ApplicationPath}/includes/production/scripts/build-ru/js/require.js"></script>
            </xsl:when>
            <xsl:when test="$lang = 'en'">
                <script data-main="{./@ApplicationPath}/includes/production/scripts/build-en/js/app.js" src="{./@ApplicationPath}/includes/production/scripts/build-en/js/require.js"></script>
            </xsl:when>
        </xsl:choose>

                
        <xsl:comment> BEGIN JIVOSITE CODE {literal} </xsl:comment>
        <script type='text/javascript'>
            (function(){ var widget_id = '42533';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
        </script>
        <div id='jivo_copyright' style='display: none'>
            <a href='http://www.jivosite.ru/features' target='_blank'>Установка чата на сайт</a> сервис Живой Сайт
        </div>
        <xsl:comment> {/literal} END JIVOSITE CODE </xsl:comment>

        <xsl:call-template name='counters'/>
        <xsl:if test="/response/@counters = 'true'">
             
        </xsl:if>

    </xsl:template>

    <!-- select language -->
    <xsl:template match="response" mode="languageSelector">

                    <ul class="languge_selector">
                        <!--xsl:value-of select="$mode" /-->
                        <xsl:choose>
                            <xsl:when test="$lang='ru'">
                                <li title="Текущий язык - русский">
                                    <span>Русский</span>
                                </li>

                                <li>
                                   <a href="{$path0}?lang=en" title="Switch to English language">English</a> 
                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <a href="{$path0}?lang=ru" title="Переключиться на русский язык">Русский</a>
                                </li>
                                <li title="Current language is English">
                                    <span>English</span>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </ul>
    </xsl:template>

    <!-- login window -->
    <xsl:template match="response" mode="loginWindow">
        <div class="bl-login">
            <div class="loading"></div>
                    <form id="loginForm" action="login.aspx" method="POST" data-fType="loginForm">
                        <input type="hidden" name="a" id="a" value="loginajax" />
                        <input type="hidden" name="tz" id="tz" value="" />
                        <input type="hidden" name="dst" id="dst" value=""/>
                        <input type="hidden" name="departmentId" id="departmentId" value="" />

                        <fieldset>
                            <div class="legend">

                                <xsl:value-of select="$locale/data[@name='systemEntrance']/value" />
                            </div>

                            <div>
                                <div class="error">
                                    <xsl:apply-templates select="/response/message" />
                                </div>


                                <div class="row">
                                    <div class="left-column">
                                        <label for="login" class="mandatory">
                                            <xsl:value-of select="$locale/data[@name='Login']/value" />:
                                        </label>
                                    </div>
                                    <div class="right-column">
                                        <a href="{$path}&#38;page=register" tabindex="4">
                                            <xsl:value-of select="$locale/data[@name='registration']/value" />
                                        </a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="row">
                                    <input type="text" name="login" id="txtLogin" value="{//@previousLogin}" tabindex="1" />
                                </div>

                                <div class="row">
                                    <div class="left-column">
                                        <label for="password" class="mandatory">
                                            <xsl:value-of select="$locale/data[@name='Password']/value" />:
                                        </label>
                                    </div>
                                    <div class="right-column">
                                        <a href="{$path}&#38;page=password_recover" tabindex="5">

                                            <xsl:value-of select="$locale/data[@name='forgotPassword']/value" />?
                                        </a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="row">
                                    <input type="password" name="password" id="password" tabindex="2" />
                                    <div class="clear"></div>
                                </div>

                                <div class="row">
                                    <div class="left-column">
                                        <button type="submit" id="login-submit" tabindex="3">
                                            <xsl:value-of select="$locale/data[@name='GetIn']/value" />
                                        </button>

                                    </div>

                                    <div class="right-column">
                                        <ul class="buttons">
                                            <li>
                                                <xsl:call-template name="demoLoginLink" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </fieldset>
                    </form>


            <div class="ie" style="display:none">
              <div class="legend">
                  <xsl:value-of select="$locale/data[@name='OldBrowser']/value/@header" />
               </div>
                  <xsl:copy-of select="$locale/data[@name='OldBrowser']/value/*" /> 


                <div style="float: left; margin-left: 7px;">
                    <a target="_blank" title="{$locale/data[@name='OldBrowser']/value/@txt} FireFox" href="http://www.mozilla-europe.org/firefox">
                        <img alt="{$locale/data[@name='OldBrowser']/value/@txt} FireFox" style="border: none;" src="/WebMap/images/ie6no-firefox.jpg" width="50" />
                    </a>
                </div>
                <div style="float: left; margin-left: 5px;">
                    <a target="_blank" title="{$locale/data[@name='OldBrowser']/value/@txt} Google Chrome" href="http://www.google.com/chrome">
                        <img alt="{$locale/data[@name='OldBrowser']/value/@txt} Google Chrome" style="border: none;" src="/WebMap/images/ie6no-chrome.jpg" width="50" />
                    </a>
                </div>
                <div style="float: left; margin-left: 5px;">
                    <a target="_blank" title="{$locale/data[@name='OldBrowser']/value/@txt} Opera" href="http://www.opera.com/">
                        <img alt="{$locale/data[@name='OldBrowser']/value/@txt} Opera" style="border: none;" src="/WebMap/images/ie6no-opera.jpg" width="50" />
                    </a>
                </div>
              
                <div style="float: left; margin-left: 5px;">
                    <a target="_blank" title="{$locale/data[@name='OldBrowser']/value/@txt} Internet Explorer" href="http://windows.microsoft.com/ru-ru/internet-explorer/download-ie">
                        <img alt="{$locale/data[@name='OldBrowser']/value/@txt} Internet Explorer" style="border: none;" src="/WebMap/images/ie6no-ie.jpg" width="50" />
                    </a>
                </div>

                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div style="display:none;">
                <div class="bl-loggedUser">
                    <div class="legend">
                        <xsl:value-of select="$locale/data[@name='loggedHeader']/value" />
                    </div>
                    <div class="bl-inner">
                        <div class="bl-photo">

                            <img src=""/>

                        </div>
                        <div class="bl-info">
                            <span class="uName"></span>
                            <p>
                                <xsl:value-of select="$locale/data[@name='my_phone']/value" />
                                <a href="" class="info-link"></a>
                            </p>

                        </div>
                        <div class="clear"></div>
                        <div class="bl-buttons">
                            <a id="gotomap" class="to_map" href="/WebMap/map.aspx/">
                                <xsl:value-of select="$locale/data[@name='gotomap']/value" />
                            </a>
                            <a href="#" class="logout">
                                <xsl:value-of select="$locale/data[@name='logout']/value" />
                            </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

            </div> 
         </div>
           


        

    </xsl:template>


    <xsl:template name="demoLoginLink">
        <xsl:param name="title" select="$locale/data[@name='demoEnter']/value" />

        <a class="demoLoginLink" href="/WebMap/login.aspx?a=demo" tabindex="6">
            <xsl:value-of select="$title"/>
        </a>
    </xsl:template>


    <xsl:template name="mainMenu">
        <xsl:param name="path"/>
        <xsl:param name="selectedPage"/>

       

        
        <xsl:variable name="fileName">
            <xsl:text>../includes/resources2/</xsl:text>
            <xsl:value-of select="$selectedPage"/>

            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

        </xsl:variable>
        <xsl:variable name="fileName2">
            <xsl:text>../includes/resources2/physical</xsl:text>
            
            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

        </xsl:variable>

        <xsl:variable name="firstLvl" select="document($fileName)/root"/>
        <xsl:variable name="firstLvl2" select="document($fileName2)/root"/>
        <xsl:variable name="selectedPageType" select="/response/@selectedPageType"/>
        <xsl:variable name="selectedPageSubType" select="/response/@selectedPageSubType"/>


        <ul class="toplevel">
            <xsl:choose>
                <xsl:when test="($selectedPage = 'physical')">
                     <xsl:for-each select="$firstLvl/submenu/page[@topMenu]">
                        <xsl:choose>
                            <xsl:when test="(@id=$selectedPageType)">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>

                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:when>

                <xsl:when test="$selectedPage = 'corporative'">
                    <xsl:for-each select="$firstLvl/submenu/page[@topMenu]">
                        <xsl:choose>
                            <xsl:when test="@id=$selectedPageType">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>

                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    
                </xsl:when>
                
                <xsl:when test="($selectedPage = 'about')">
                    <xsl:for-each select="$firstLvl/submenu/page[@topMenu]">
                        <xsl:choose>
                            <xsl:when test="(@id=$selectedPage)">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>

                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:when>

                
                <xsl:otherwise>
                    <xsl:for-each select="$firstLvl2/submenu/page[@topMenu]">
                        <xsl:choose>
                            <xsl:when test="(@id=$selectedPage)">
                                <li class="current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>

                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li>
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>
        </ul>

        <ul class="sublevel">

            <xsl:choose>
                <xsl:when test="($selectedPage = 'physical') or ($selectedPage = 'about') ">
                    <xsl:choose>
                        <xsl:when test="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl">
                                <li class="bold current">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                        <xsl:with-param name="target-id" select="$firstLvl/submenu/page[@id=$selectedPageType]/@id"/>
                                        <xsl:with-param name="class"/>
                                        <xsl:with-param name="text" select="$firstLvl/submenu/page[@id=$selectedPageType]/main/@title"></xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            <xsl:for-each select="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/page">
                                <xsl:choose>
                                    <xsl:when test="@id=$selectedPageSubType">
                                        <li class="{@quick-start} current">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="$selectedPageType"/>
                                                <xsl:with-param name="target-main-id" select="@id"/>
                                                <xsl:with-param name="class"/>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:when>
                                    
                                    <xsl:otherwise>
                                        <li class="{@quick-start}">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="$selectedPageType"/>
                                                <xsl:with-param name="target-main-id" select="@id"/>
                                                <xsl:with-param name="class"/>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:for-each>   
                        </xsl:when>
                        <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="not($firstLvl/submenu/page[@id=$selectedPageType])">
                                        <li class="bold current">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="physical"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="family"/>
                                                <xsl:with-param name="class"/>
                                                <xsl:with-param name="text" select="/response/pageContent/promo/services"></xsl:with-param>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:when>
                                    <xsl:when test="($firstLvl/submenu/page[@id=$selectedPageType]/@id = 'pets') or ($firstLvl/submenu/page[@id=$selectedPageType]/@id = 'family') or ($firstLvl/submenu/page[@id=$selectedPageType]/@id = 'auto')">
                                        <li class="{@quick-start} current">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="$firstLvl/submenu/page[@id=$selectedPageType]/@id"/>
                                                <xsl:with-param name="class"/>
                                                <xsl:with-param name="text" select="$firstLvl/submenu/page[@id=$selectedPageType]/main/@title"></xsl:with-param>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:when>
                                   <xsl:otherwise>     
                                        <li class="bold current">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="physical"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="family"/>
                                                <xsl:with-param name="class"/>
                                                <xsl:with-param name="text" select="/response/pageContent/promo/services"></xsl:with-param>
                                            </xsl:call-template>
                                        </li>
                                   
                                    </xsl:otherwise>
                                </xsl:choose>
                            
                            <xsl:for-each select="$firstLvl/submenu/page[@thirdLvlMenu]">
                                
                                <xsl:choose>
                                   
                                    <xsl:when test="$selectedPageType = 'child'">
                                        
                                        <xsl:choose>                                            
                                            <xsl:when test="@id=$selectedPageType">
                                                <xsl:if test="not(@id = 'equipment')">
                                                    <li class="{@quick-start} current">
                                                        <xsl:call-template name="menulink">
                                                            <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                            <xsl:with-param name="target-id" select="@id"/>
                                                            <xsl:with-param name="class"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:if test="not(@id = 'equipment')">
                                                    <li class="{@quick-start}">
                                                        <xsl:call-template name="menulink">
                                                            <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                            <xsl:with-param name="target-id" select="@id"/>
                                                            <xsl:with-param name="class"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </xsl:when>
                                    <xsl:when test="$selectedPageType = 'auto'">
                                        <xsl:choose>
                                            <xsl:when test="@id=$selectedPageType">
                                                <li class="{@quick-start} current">
                                                    <xsl:call-template name="menulink">
                                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                        <xsl:with-param name="target-id" select="@id"/>
                                                        <xsl:with-param name="class"/>
                                                    </xsl:call-template>
                                                </li>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <li class="{@quick-start}">
                                                    <xsl:call-template name="menulink">
                                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                        <xsl:with-param name="target-id" select="@id"/>
                                                        <xsl:with-param name="class"/>
                                                    </xsl:call-template>
                                                </li>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="@id=$selectedPageType">
                                                <li class="{@quick-start} current">
                                                    <xsl:call-template name="menulink">
                                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                        <xsl:with-param name="target-id" select="@id"/>
                                                        <xsl:with-param name="target-sub-id">personal-tracker</xsl:with-param>
                                                        <xsl:with-param name="class"/>
                                                        
                                                    </xsl:call-template>
                                                </li>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <li class="{@quick-start}">
                                                    <xsl:call-template name="menulink">
                                                        <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                        <xsl:with-param name="target-id" select="@id"/>
                                                        <xsl:with-param name="class"/>
                                                    </xsl:call-template>
                                                </li>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        
                                        
                                    </xsl:otherwise>
                                </xsl:choose>
                                
                            </xsl:for-each>    
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    
                </xsl:when>

                
                <xsl:when test="$selectedPage = 'corporative'">
                   <xsl:choose>
                       <xsl:when test="($selectedPageType = 'important') or ($selectedPageType = 'solutions') or ($selectedPageType = 'equipment') or ($selectedPageType = 'quick-start') or ($selectedPageType = 'help')"> 
                          <xsl:for-each select="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/page">
                              
                               <xsl:choose>
                                    <xsl:when test="@id=$selectedPageSubType">
                                        <li class="{@quick-start} current">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="$selectedPageType"/>
                                                <xsl:with-param name="target-main-id" select="@id"/>
                                                <xsl:with-param name="class"/>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <li class="{@quick-start}">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id" select="$selectedPageType"/>
                                                <xsl:with-param name="target-main-id" select="@id"/>
                                                <xsl:with-param name="class"/>
                                            </xsl:call-template>
                                        </li>
                                    </xsl:otherwise>
                                </xsl:choose>
                              
                            </xsl:for-each>
                           <xsl:if test="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/link">
                               <xsl:variable name="c_mts" select="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/link"></xsl:variable>
                               <li>
                                   <a class="{$c_mts/@class}" href="{$c_mts/@href}" title="{$c_mts/@title}" target="_blank">
                                       <xsl:value-of select="$c_mts/@title"/>
                                   </a>
                               </li>
                           </xsl:if>
                        </xsl:when>
                       <xsl:when test="$selectedPageType = 'services'">
                           
                           <xsl:for-each select="$firstLvl/submenu/page[@id='services']/thirdLvl/page">
                               <xsl:choose>
                                   <xsl:when test="@id=$selectedPageSubType">
                                       <li class="current">
                                           <xsl:call-template name="menulink">
                                               <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                               <xsl:with-param name="target-id" select="$selectedPageType"/>
                                               <xsl:with-param name="target-main-id" select="@id"/>
                                           </xsl:call-template>
                                       </li>
                                   </xsl:when>
                                   <xsl:otherwise>
                                       
                                       <li>
                                           <xsl:call-template name="menulink">
                                               <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                               <xsl:with-param name="target-id" select="$selectedPageType"/>
                                               <xsl:with-param name="target-main-id" select="@id"/>
                                           </xsl:call-template>
                                       </li>
                                   </xsl:otherwise>
                               </xsl:choose>
                           </xsl:for-each>
                           
                       </xsl:when>
                        <xsl:otherwise>
                            <xsl:for-each select="$firstLvl/submenu/page[@id='solutions']/thirdLvl/page">

                                        <li class="{@quick-start}">
                                            <xsl:call-template name="menulink">
                                                <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                <xsl:with-param name="target-id">solutions</xsl:with-param>
                                                <xsl:with-param name="target-main-id" select="@id"/>
                                                <xsl:with-param name="class"/>
                                            </xsl:call-template>
                                        </li>

                            </xsl:for-each>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                    
                </xsl:when>

                <xsl:otherwise>
                    <xsl:for-each select="$firstLvl2/submenu/page[@thirdLvlMenu]">
                        <xsl:choose>
                            <xsl:when test="$selectedPage = 'help'">
                                <li class="{@quick-start}">
                                    <xsl:call-template name="menulink">
                                        <xsl:with-param name="target-parent-id">physical</xsl:with-param>
                                        <xsl:with-param name="target-id" select="@id"/>
                                        <xsl:with-param name="class"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                
                            </xsl:otherwise>
                        </xsl:choose>
                                

                    </xsl:for-each>       
                </xsl:otherwise>
            </xsl:choose>
            
                
        </ul>

    </xsl:template>

    <xsl:template name="asideMenu">
        <xsl:param name="path"></xsl:param>
        <xsl:param name="selectedPage"/>
      
        <xsl:variable name="pageType">services</xsl:variable>
        <xsl:if test="$selectedPage">
            <xsl:variable name="fileName">
                <xsl:text>../includes/resources2/</xsl:text>
                <xsl:value-of select="$selectedPage"/>
                <xsl:choose>
                    <xsl:when test="$lang = 'ru'">
                        <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="firstLvl" select="document($fileName)/root"/>
            <xsl:variable name="selectedPageType" select="/response/@selectedPageType"/>
            <xsl:if test="$firstLvl/submenu/page[@leftMenu]">

                <div class="nav left">
                    <div class="legend">
                        <xsl:value-of select="$firstLvl/leftMenu/@title"/>
                        
                    </div>
                    <ul>
                        <xsl:choose>
                            <xsl:when test="$selectedPage = 'corporative'">

                                <xsl:for-each select="$firstLvl/submenu/page[@leftMenu]">
                                    <xsl:choose>
                                        <xsl:when test="@id=$selectedPageType">
                                            <li class="current">
                                                <xsl:choose>
                                                    <xsl:when test="@bullet">
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                current bullet
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                current
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                                <xsl:call-template name="menulink">
                                                    <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                    <xsl:with-param name="target-id" select="$pageType"/>
                                                    <xsl:with-param name="target-main-id" select="@id"/>
                                                </xsl:call-template>
                                            </li>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <li>
                                                <xsl:choose>
                                                    <xsl:when test="@bullet">
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                bullet
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    
                                                </xsl:choose>
                                                <xsl:call-template name="menulink">
                                                    <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                    <xsl:with-param name="target-id" select="$pageType"/>
                                                    <xsl:with-param name="target-main-id" select="@id"/>
                                                </xsl:call-template>
                                            </li>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:for-each>
                                
                            </xsl:when>
                            <xsl:otherwise>
                                
                                <xsl:for-each select="$firstLvl/submenu/page[@leftMenu]">
                                    <xsl:choose>
                                        <xsl:when test="@id=$selectedPageType">
                                            <li>
                                                <xsl:choose>
                                                    <xsl:when test="@bullet">
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                current bullet
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                current
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                                
                                                <xsl:call-template name="menulink">
                                                    <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                    <xsl:with-param name="target-id" select="@id"/>
                                                </xsl:call-template>
                                            </li>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <li>
                                                <xsl:choose>
                                                    <xsl:when test="@bullet">
                                                        <xsl:attribute name="class">
                                                            <xsl:text>
                                                                bullet
                                                            </xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                </xsl:choose>
                                                <xsl:call-template name="menulink">
                                                    <xsl:with-param name="target-parent-id" select="$selectedPage"></xsl:with-param>
                                                    <xsl:with-param name="target-id" select="@id"/>
                                                </xsl:call-template>
                                            </li>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:for-each>
                                
                            </xsl:otherwise>
                        </xsl:choose>

                        
        
                    </ul>
                </div>

            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="helpdesk">
        
        <div class="bl-help">
            <h4>
                <xsl:value-of select="$locale/data[@name='help']/@header" />
            </h4>
            <ul>
                <li>
                    <xsl:call-template name="menulink">
                        <xsl:with-param name="target-parent-id">help</xsl:with-param>
                        <xsl:with-param name="target-id">support</xsl:with-param>
                        <xsl:with-param name="text"><xsl:value-of select="$locale/data[@name='help']/value[1]" /></xsl:with-param>
                    </xsl:call-template>
                    
                </li>
                <li>
                    <xsl:choose>
                        <xsl:when test="$selectedPage = 'physical'">
                           <xsl:copy-of select="$locale/data[@name='help']/value[@type='physical']/*/." />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="$locale/data[@name='help']/value[@type='corporative']/*/." />
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
            </ul>
        </div>
    </xsl:template>

    <xsl:template name="calculate">
        <xsl:param name="path"/>
        <div class="bl-calculate">
            <xsl:call-template name="menulink">
                <xsl:with-param name="target-parent-id">corporative</xsl:with-param>
                <xsl:with-param name="target-id">calculate</xsl:with-param>
                <xsl:with-param name="text">
                    <xsl:value-of select="$locale/data[@name='Calculate']/value" />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>
    
    <xsl:template name="quick-start">
        <xsl:param name="path"/>
        <div class="bl-quick-start">
            <!--xsl:call-template name="menulink">
                <xsl:with-param name="target-parent-id">physical</xsl:with-param>
                <xsl:with-param name="target-id">quick-start</xsl:with-param>
                <xsl:with-param name="text">
                    <xsl:value-of select="$locale/data[@name='QuickStart']/value" />
                </xsl:with-param>
            </xsl:call-template-->
            <a href="https://login.mts.ru/" target="_blank">
                <xsl:value-of select="$locale/data[@name='QuickStart']/value" />
            </a>
        </div>
    </xsl:template>

    <xsl:template match="response" mode="article">
        <xsl:variable name="selectedPageSubType" select="./@selectedPageSubType"></xsl:variable>
        <xsl:variable name="selectedPageType" select="./@selectedPageType"></xsl:variable>
        <xsl:variable name="selectedPage" select="./@selectedPage"></xsl:variable>
     
        <xsl:variable name="fileName">
            <xsl:text>../includes/resources2/</xsl:text>
            <xsl:value-of select="$selectedPage"/>
            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="firstLvl" select="document($fileName)/root"/>

        <xsl:variable name="tempFileName">
            <xsl:text>../includes/resources2/help</xsl:text>
            
            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="tempFirstLvl" select="document($tempFileName)/root"/>
        <xsl:variable name="tempFileName3">
            <xsl:text>../includes/resources2/physical</xsl:text>

            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="tempFirstLvl3" select="document($tempFileName3)/root"/>

        
        <xsl:choose>
            <xsl:when test="not($selectedPage)">
                <div class="inner">

                    <xsl:variable name="page2"  select="/response/pageContent/promo/content"/>
                    <xsl:if test="/response/pageContent/promo/banner">
                        <a href="{/response/pageContent/promo/banner/@link}" title="{/response/pageContent/promo/banner/@title}" target="_blank" class="banner">
                            <img src="{$path0}/includes/sources/images/banners/{/response/pageContent/promo/banner/@src}" alt="{/response/pageContent/promo/banner/@alt}" title="{/response/pageContent/promo/banner/@alt}"/>
                        </a>
                    </xsl:if>

                    <xsl:if test="$page2/@title">
                        <h1>

                            <xsl:value-of select="$page2/@title"/>
                        </h1>
                    </xsl:if>
                    <xsl:apply-templates select="$page2/main/*/." mode="brief-description-content"/>
                </div>
            </xsl:when>

          <xsl:when test="$selectedPageType = 'news'">
            <xsl:call-template name="news-list">
              <xsl:with-param name="selectedPage" select="$selectedPage"/>
            </xsl:call-template>
          </xsl:when>

            <xsl:when test="(($selectedPage = 'help') and not($selectedPageType) )">
                <xsl:variable name="page"  select="$tempFirstLvl/submenu/page[@id='support']"/>
                <xsl:variable name="page2"  select="$tempFirstLvl/submenu/page[@id='support']"/>

                <div class="inner">
                    <h1>
                        <xsl:choose>
                            <xsl:when test="($selectedPageType = 'contacts')">
                                <xsl:value-of select="$firstLvl/submenu/page[@id=$selectedPageType]/@title"/>

                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$firstLvl/content/@title"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </h1>

                    <xsl:apply-templates select="$page2/*/." mode="brief-description-content"/>
                </div>

            </xsl:when>

            <xsl:when test="$selectedPage = 'register'">
                <xsl:variable name="page"  select="/response/pageContent/promo/menu/page[@id=$selectedPage]"/>

                <div class="inner">
                    
                        <xsl:choose>
                            <xsl:when test="$page/main[@title]">
                                <h1>
                                    <xsl:value-of select="$page/main/@title"/>
                                </h1>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="$page/@title">
                                    <h1>
                                        
                                        <xsl:value-of select="$page/@title"/>
                                    </h1>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    
                    <xsl:apply-templates select="$page/*/." mode="brief-description-content"/>
                </div>
                
            </xsl:when>
            <xsl:when test="$selectedPage = 'password_recover'">
                <xsl:variable name="page"  select="/response/pageContent/promo/menu/page[@id=$selectedPage]"/>
                
                <div class="inner">
                    <h1>
                        <xsl:value-of select="$page/@title"/>
                    </h1>
                    <xsl:apply-templates select="$page/*/." mode="brief-description-content"/>
                    
                </div>
            </xsl:when>
           
            <xsl:when test="$selectedPageSubType != ''">
                <xsl:variable name="page"  select="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/page[@id=$selectedPageSubType]"/>
                
                <div class="inner">
                    
                        <xsl:choose>
                            <xsl:when test="$page/main[@title]">
                                <h1>
                                    <xsl:value-of select="$page/main/@title"/>
                                </h1>

                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="$page/@title">
                                    <h1>
                                        <xsl:value-of select="$page/@title"/>
                                    </h1>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                   
                    <!--a href="/WebMap/includes/sources/images/promo/logistic-solutions.jpg"  class="promo" rel="lightbox"-->

                    <xsl:if test="$page/main[@default_image]">
                        <img src="{$path0}/includes/sources/images/{$page/main/@default_image}" title="{$page/@title}" class="promo"/>
                    </xsl:if>

                    <!--/a-->


                    <xsl:apply-templates select="$page/main/*/." mode="brief-description-content"/>
                    <xsl:apply-templates mode="additional" select=".">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:apply-templates>
                    
                    
                    <div class="clear"></div>
                    <xsl:choose>

                        <xsl:when test="$page[@button='quick-start']">
                            <xsl:call-template name="quick-start">
                                <xsl:with-param name="path" select="$path"/>
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:if test="$page[@button='calculate']">
                        <xsl:call-template name="calculate">
                            <xsl:with-param name="path" select="$path"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="social-link"/>
                    <div class="clear"></div>
                </div>

                
            </xsl:when>
            <xsl:when test="($selectedPageType = 'solutions' and $selectedPageSubType ='')">
                <xsl:variable name="page"  select="$firstLvl/submenu/page[@id=$selectedPageType]/thirdLvl/page[1]"/>

                <div class="inner">

                        <xsl:choose>
                            <xsl:when test="$page/main[@title]">
                                
                    <h1>
                        <xsl:value-of select="$page/main/@title"/>
                    </h1>

                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="$page/@title">
                                    <h1>
                                        <xsl:value-of select="$page/@title"/>
                                    </h1>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    
                    

                    <!--a href="/WebMap/includes/images/promo/logistic-solutions.jpg"  class="promo" rel="lightbox"-->

                    <xsl:if test="$page/main[@default_image]">
                        <img src="{$path0}/includes/sources/images/{$page/main/@default_image}" title="{$page/@title}" class="promo"/>
                    </xsl:if>

                    <!--/a-->


                    <xsl:apply-templates select="$page/main/*/." mode="brief-description-content"/>
                    <xsl:apply-templates mode="additional" select=".">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:apply-templates>

                    <div class="clear"></div>
                    <xsl:choose>

                        <xsl:when test="$page[@button='quick-start']">
                            <xsl:call-template name="quick-start">
                                <xsl:with-param name="path" select="$path"/>
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:if test="$page[@button='calculate']">
                        <xsl:call-template name="calculate">
                            <xsl:with-param name="path" select="$path"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="social-link"/>
                    <div class="clear"></div>
                </div>

            </xsl:when>          
            <xsl:when test="$selectedPageType">
                <xsl:variable name="page"  select="$firstLvl/submenu/page[@id=$selectedPageType]"/>


                <div class="inner">

                    <xsl:if test="not($page) or ($page = '')">
                        <xsl:call-template name="error"/>
                    </xsl:if>
                    
                    
                        <xsl:choose>
                            <xsl:when test="$page/main[@title]">
                                <h1><xsl:value-of select="$page/main/@title"/></h1>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="$page/@title">
                                    <h1>
                                        <xsl:value-of select="$page/@title"/>
                                    </h1>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    




                    <xsl:if test="$page/main[@default_image]">
                        <img src="{$path0}/includes/sources/images/{$page/main/@default_image}" title="{$page/@title}" class="promo"/>
                    </xsl:if>
                        
                    <!--/a-->

                    
                    <xsl:apply-templates select="$page/main/*/." mode="brief-description-content"/>
                    <xsl:apply-templates mode="additional" select=".">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:apply-templates>

                    <xsl:choose>

                        <xsl:when test="$page[@button='quick-start']">
                            <xsl:call-template name="quick-start">
                                <xsl:with-param name="path" select="$path"/>
                            </xsl:call-template>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:if test="$page[@button='calculate']">
                        <xsl:call-template name="calculate">
                            <xsl:with-param name="path" select="$path"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="social-link"/>
                    <div class="clear"></div>
                </div>

            </xsl:when>
            
            <xsl:when test="$selectedPage">
                <xsl:variable name="page"  select="$firstLvl/content"/>
                <xsl:variable name="page2"  select="/response/pageContent/promo"/>
                <xsl:if test="($selectedPage = 'corporative') or ($selectedPage = 'physical')">
                    <xsl:if test="$page2/banner">
                        <a href="{$page2/banner/@link}" title="{$page2/banner/@title}" target="_blank" class="banner">
                            <img src="{$path0}/includes/sources/images/banners/{$page2/banner/@src}" alt="{$page2/banner/@alt}" title="{$page2/banner/@alt}"/>
                        </a>
                    </xsl:if>
                </xsl:if>
                
                <div class="inner">
                    <xsl:if test="not($page) or ($page = '')">
                        <xsl:call-template name="error"/>
                    </xsl:if>
                    
                    <xsl:if test="$page/@title">
                        <h1>
                            <xsl:value-of select="$page/@title"/>
                        </h1>
                    </xsl:if>
                    <xsl:if test="$page/main[@default_image]">
                        <img src="{$path0}/includes/sources/images/img/{$selectedPage}.jpg" title="{$page/@title}" class="promo"/>
                    </xsl:if>

                    <xsl:apply-templates select="$page/main/*/." mode="brief-description-content"/>
                    
                    <xsl:apply-templates mode="additional" select=".">
                        <xsl:with-param name="page" select="$page"/>
                    </xsl:apply-templates>
                    
                    <div class="clear"></div>
                    <xsl:if test="$page[@button='calculate']">
                        <xsl:call-template name="calculate">
                            <xsl:with-param name="path" select="$path"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="social-link"/>
                    <div class="clear"></div>

                </div>
                
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="page"  select="/response/pageContent/promo/page"/>

               
                <div class="inner">
                    <xsl:if test="$page/@title">
                        <h1>
                            <xsl:value-of select="$page/@title"/>
                        </h1>
                    </xsl:if>
                    <xsl:if test="$page/@button">
                        <xsl:call-template name="calculate">
                            <xsl:with-param name="path" select="$path"/>
                        </xsl:call-template>
                    </xsl:if>
                   

                    <xsl:apply-templates select="$page/content/*/." mode="brief-description-content"/>
                    
                </div>
            </xsl:otherwise>
        </xsl:choose>
             

        <div class="clear"></div>
    </xsl:template>

    <xsl:template match="response" mode="selectedPage">
        <xsl:variable name="urlReferrer" select="./@UrlReferrer" />
        
        <xsl:choose>
            <xsl:when test="./@selectedPage">
                <xsl:variable name="selectedPageValue" select="./@selectedPage" />
                <xsl:choose>
                    <xsl:when test="./pageContent/promo/menu/page[@id=$selectedPageValue]">
                        <xsl:value-of select="./@selectedPage" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            
            <xsl:when test="./pageContent/promo/page">
                <xsl:value-of select="./pageContent/promo/page[1]/@id" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template mode="selectedPageType" match="response">
        <xsl:param name="selectedPage"></xsl:param>
        <xsl:variable name="urlReferrer" select="./@UrlReferrer" />
        <xsl:variable name="fileName">
            <xsl:text>../../../includes/resources2/</xsl:text>
            <xsl:value-of select="./@selectedPage"/>
            <xsl:text>.ru-RU.xml</xsl:text>
        </xsl:variable>
        <xsl:variable name="firstLvl" select="document($fileName)/root"/>
        <xsl:value-of select="document($fileName)/root"/>
        
        <xsl:choose>
            <xsl:when test="./@selectedPageType">
                <xsl:variable name="selectedPageValue" select="./@selectedPageType" />
                <xsl:value-of select="$selectedPageValue"/>
                <xsl:choose>
                    <xsl:when test="$firstLvl/submenu/page[@id=$selectedPageValue]">
                        <xsl:value-of select="$firstLvl/submenu/page/@selectedPageType" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$firstLvl/submenu/content" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>

            <xsl:when test="$firstLvl/submenu/page">
                <xsl:value-of select="$firstLvl/submenu/page[1]/@id" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$firstLvl/submenu[1]/page[1]/@id" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="inner">
        <xsl:param name="page"/>
        
        <xsl:copy-of select="$page/*/."/>
        
    </xsl:template>
    
    <!-- template for IE less than 8-->
    
    <xsl:template name="ie-fix">

        <xsl:comment>[if lt IE 9]&gt;
            &lt;style&gt;
            .bl-login {
            padding: 0;
            zoom: 1;
            }
            #loginForm {
            display: none !important;
            }
            .bl-loggedUser.cloned {
            display: none !important;
            }
            .inner, .bl-login, .bl-calculate, .nav.left {
            border: 1px solid #EDEDED;
            }
            ul.sublevel {
            border: 1px solid #EDEDED;
            width: 918px !important;
            zoom: 1;
            }
            .bl-login form {
            display: none;
            }
            .ie {
            display:block !important;
            }
            .article h1 {
            position: relative;
            }
            .yashare-auto-init {
            position: absolute;
            top:0;
            right: 0;
            }
            &lt;/style&gt;
        &lt;![endif]</xsl:comment>
    </xsl:template>


    <xsl:template match="response" mode="social-link">
           <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
           <div class="yashare-auto-init" style="float: right; margin: 10px 0 0 0" data-yashareL10n="ru" data-yashareType="none"  data-yashareQuickServices="linkedin,yaru,vkontakte,facebook,twitter,moimir,lj,gplus"></div>
        <div class="clear-right"></div>
    </xsl:template>


    <xsl:template mode="additional" match="response">
        <xsl:param name="page"></xsl:param>
        <xsl:if test="$page/additional">
            <xsl:if test="$page/additional/image">
                <a href="{$path0}/includes/sources/images{$page/additional/image/@big}"  class="lightbox" rel="lightbox"  title="{$page/@title}" >
                    <img src="{$path0}/includes/sources/images{$page/additional/image/@small}" align="center" title="{$page/@title}" />
                </a>
            </xsl:if>
            
            <xsl:for-each select="$page/additional/list">
                
            <xsl:choose>
                <xsl:when test="@type='more'">
                        <div class="more-div">
                            <xsl:if test="$page/additional/list[@header]">
                                <h2>
                                    <xsl:value-of select="@header"/>
                                </h2>
                            </xsl:if>
                            <p>
                                <xsl:apply-templates select="*" mode="brief-description-content"/>        
                            </p>
                        </div>
                    
                </xsl:when>
                <xsl:otherwise>
                        <xsl:if test="$page/additional/list[@header]">
                            <h2>
                                <xsl:value-of select="@header"/>
                            </h2>
                        </xsl:if>
                        <ul>
                            <xsl:for-each select="value">
                                <li>
                                    <xsl:choose>
                                        <xsl:when test="@type='blank'">
                                            <a href="{@link}" target="_blank">
                                                <xsl:value-of select="."/>
                                            </a>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="." mode="brief-description-content"/>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                </li>
                            </xsl:for-each>
                        </ul>

                </xsl:otherwise>
            </xsl:choose>
            </xsl:for-each>   
            


            <xsl:for-each select="$page/additional/smalltxt/txt">
                <p class="small">
                    <xsl:apply-templates select="." mode="brief-description-content"/>
                </p>
            </xsl:for-each>
            <xsl:if test="$page/additional/button">

                <xsl:apply-templates select="$page/additional/button/*" mode="brief-description-content"/>

            </xsl:if>

            <xsl:if test="$page/additional/or">
                <xsl:text> или </xsl:text>
                <xsl:apply-templates select="$page/additional/or/*" mode="brief-description-content"/>
                
            </xsl:if> 
        </xsl:if>
        
    </xsl:template>
    
    <xsl:template match="node()" mode="brief-description-content">
        <xsl:choose>
            <xsl:when test="local-name() = 'common-part'">
                <xsl:variable name="common-part-id" select="./@id" />
                <xsl:apply-templates select="/response/pageContent/promo/common-part[@id=$common-part-id]/*" mode="brief-description-content" />
            </xsl:when>
            <xsl:when test="local-name() = 'post'">
                <xsl:variable name="media-id" select="./@media" />
                <xsl:apply-templates select="." />
            </xsl:when>
            <xsl:when test="local-name() = 'ref'">
                <xsl:element name="a">
                    <xsl:apply-templates select="@*" mode="brief-description-content"/>
                    <xsl:apply-templates select="text()" mode="brief-description-content" />
                    <xsl:apply-templates select="./*" mode="brief-description-content" />
                </xsl:element>
            </xsl:when>
            <xsl:when test="local-name() = 'demo-login-link'">
                <xsl:call-template name="demoLoginLink">
                    <xsl:with-param name="title">здесь</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
          <xsl:when test="local-name() = 'sms'">
            <xsl:element name="strong">
              <xsl:attribute name="class">red</xsl:attribute>
              <xsl:attribute name="style">white-space: nowrap</xsl:attribute>
              <xsl:apply-templates select="text()" mode="brief-description-content" />
            </xsl:element>
          </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{local-name()}">
                    <xsl:apply-templates select="@* | ./* | text()" mode="brief-description-content"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()" mode="brief-description-content">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="@*" mode="brief-description-content">
        <xsl:choose>
            <xsl:when test="local-name(..) = 'div' and local-name() = 'video-data'">
                <embed src="{$path0}/includes/sources/{../@data}" width="{../@width}" height="{../@height}" scale="{../@scale}"
                   type="application/x-shockwave-flash"
                   pluginspage="http://www.macromedia.com/go/getflashplayer">
                    
                    <noembed>Это текст увидят пользователи, браузеры которых не поддерживают работу с плагинами</noembed>
                </embed>
            </xsl:when>
            <xsl:when test="local-name(..) = 'img' and local-name() = 'src'">
                    <xsl:attribute name="src">
                        <xsl:value-of select="$path0"/>/includes/sources/images/<xsl:value-of select="."/>
                    </xsl:attribute>                 
            </xsl:when>
            <xsl:when test="local-name(..) = 'ref' and local-name() = 'target-parent-id'">
                <xsl:attribute name="href">
                    <xsl:value-of select="$path"></xsl:value-of>&amp;page=<xsl:value-of select="."/>
                    <xsl:if test="../@target-id">
                        <xsl:text>&amp;type=</xsl:text>
                        <xsl:value-of select="../@target-id" />
                    </xsl:if>
                    <xsl:if test="../@target-main-id">
                        <xsl:text>&amp;id=</xsl:text>
                        <xsl:value-of select="../@target-main-id" />
                    </xsl:if>
                    <xsl:if test="../@target-sub-id">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="../@target-sub-id" />
                    </xsl:if>
                </xsl:attribute>
                
            </xsl:when>
            <xsl:when test="local-name(..) = 'ref' and ../@target-page = 'download'">
                <xsl:attribute name="href">
                    <xsl:value-of select="$path0"/>/includes/download/<xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="local-name(..) = 'ref' and ../@target-page = 'downloadClient'">
                <xsl:attribute name="href">
                    <xsl:text>http://www.nika-glonass.ru/download/</xsl:text><xsl:value-of select="../@href"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="../@rel='lightbox'">
                <xsl:attribute name="href">
                    <xsl:value-of select="$path0"/>/includes/sources/images/<xsl:value-of select="../@href"/>
                </xsl:attribute>
                <xsl:attribute name="title">
                    <xsl:value-of select="../@title"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="../@rel='pdf'">
                <xsl:attribute name="href">
                    <xsl:value-of select="$path0"/>/includes/sources/images/<xsl:value-of select="../@href"/>
                </xsl:attribute>
                <xsl:attribute name="target">
                    <xsl:text>_blank</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="title">
                    <xsl:value-of select="../@title"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
                <xsl:if test="local-name() = 'class'">
                    <xsl:attribute name="class">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="local-name() = 'title'">
                    <xsl:attribute name="title">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="local-name() = 'rel'">
                    <xsl:attribute name="rel">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:if>
    </xsl:template>

    <xsl:template name="menulink">
        <xsl:param name="target-parent-id"></xsl:param>
        <xsl:param name="target-id"></xsl:param>
        <xsl:param name="target-main-id"/>
        <xsl:param name="target-sub-id"></xsl:param>
        <xsl:param name="class"></xsl:param>
        <xsl:param name="title"></xsl:param>
        <xsl:param name="text"></xsl:param>
        
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="$path"/>
                <xsl:if test="$target-parent-id">
                    <xsl:text>&amp;page=</xsl:text><xsl:value-of select="$target-parent-id"/>
                </xsl:if>
                <xsl:if test="$target-id">
                    <xsl:text>&amp;type=</xsl:text><xsl:value-of select="$target-id"/>
                </xsl:if>
                <xsl:if test="$target-main-id">
                    <xsl:text>&amp;id=</xsl:text>
                    <xsl:value-of select="$target-main-id"/>
                </xsl:if>
                <xsl:if test="$target-sub-id">
                    <xsl:text>#</xsl:text>
                    <xsl:value-of select="$target-sub-id" />
                </xsl:if>
            </xsl:attribute>
            <xsl:if test="$class">
                <xsl:attribute name="class">
                    <xsl:value-of select="$class"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="$title">
                <xsl:attribute name="title">
                    <xsl:value-of select="$title"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@title">
                <xsl:value-of select="@title"/>    
            </xsl:if>
            <xsl:if test="$text">
                <xsl:value-of select="$text"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>
    
    <!-- create page title-->
    <xsl:template match="response" mode="title1">
        <xsl:variable name="selectedPage" select="./@selectedPage"></xsl:variable>
        <xsl:variable name="fileName">
            <xsl:text>../includes/resources2/</xsl:text>
            <xsl:value-of select="$selectedPage"/>

            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

        </xsl:variable>

        <xsl:variable name="firstLvl" select="document($fileName)/root"/>
        <xsl:variable name="selectedPageType" select="/response/@selectedPageType"/>
        <xsl:variable name="selectedPageSubType" select="/response/@selectedPageSubType"/>

        <xsl:choose>
            <xsl:when test="($selectedPageSubType) and not($selectedPageSubType = '') and ($firstLvl/submenu/*/*/page[@id=$selectedPageSubType]/main[@title])">
                <xsl:value-of select="$firstLvl/submenu/*/*/page[@id=$selectedPageSubType]/main/@title"/>
            </xsl:when>
            <xsl:when test="($selectedPageSubType) and not($selectedPageSubType = '')">
               <xsl:value-of select="$firstLvl/submenu/*/*/page[@id=$selectedPageSubType]/@title"/>
            </xsl:when>
            <xsl:when test="($selectedPageType) and ($firstLvl/submenu/page[@id=$selectedPageType]/main[@title])">
                <xsl:value-of select="$firstLvl/submenu/page[@id=$selectedPageType]/main/@title"/>
            </xsl:when>
            <xsl:when test="$selectedPageType">
                <xsl:value-of select="$firstLvl/submenu/page[@id=$selectedPageType]/@title"/>
            </xsl:when>
            <xsl:when test="$selectedPage">
                <xsl:value-of select="/response/pageContent/promo/menu/page[@id=$selectedPage]/@title"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/response/pageContent/promo/title"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text> - </xsl:text><xsl:value-of select="/response/pageContent/promo/title"/>

    </xsl:template>

    <xsl:template name="counters">
        <!-- Counters -->
        <xsl:if test="./@counters-enabled = 'true'">
            <!-- Yandex.Metrika counter -->
            <div style="display:none;">
                <script type="text/javascript">
                    (function(w, c) {
                    (w[c] = w[c] || []).push(function() {
                    try {
                    w.yaCounter9754051 = new Ya.Metrika({id:9754051, enableAll: true});
                    }
                    catch(e) { }
                    });
                    })(window, "yandex_metrika_callbacks");
                </script>
            </div>
            <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
            <noscript>
                <div>
                    <img src="//mc.yandex.ru/watch/9754051" style="position:absolute; left:-9999px;" alt="" />
                </div>
            </noscript>
            <!-- /Yandex.Metrika counter -->
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-25531980-1']);
                _gaq.push(['_trackPageview']);

                (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
        </xsl:if>

        

    </xsl:template>

    <xsl:template name="mts_shop">
        <div class="w-shop">
            <a href="http://www.shop.mts.ru/gps/" target="_blank" class="main_link">Интернет-магазин</a>
            <div class="basket_info">
                <div class="no_goods_in_basket">
                    <p>
                        Выбрать <a class="e-items" target="_blank" href="http://www.shop.mts.ru/gps/">GPS-трекер</a>
                    </p>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="privateRoom">
        <xsl:variable name="cabinet" select="$locale/data[@name='privateRoom']"/>
        <div class="cabinetsplash is-empty">
            <xsl:apply-templates mode="brief-description-content" select="$locale/data[@name='privateRoom']"/>
            <xsl:choose>
                <xsl:when test="$selectedPage = 'corporative'">
                    <a id="cabinetLink" target="_blank" href="{$cabinet/@corparative_link}">
                        <xsl:value-of select="$cabinet/@title"/>
                    </a> 
                </xsl:when>
                <xsl:otherwise>
                    <a id="cabinetLink" target="_blank" href="{$cabinet/@physical_link}">
                        <xsl:value-of select="$cabinet/@title"/>
                    </a>   
                </xsl:otherwise>
            </xsl:choose>
                
            </div>
    </xsl:template>


    <xsl:template name="news-list">
        <xsl:param name="selectedPage" />

       
        
        <xsl:variable name="fileName">
            <xsl:text>../includes/resources2/</xsl:text>
            <xsl:choose>
               <xsl:when test="$selectedPage=''">
                   <xsl:text>physical</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$selectedPage"/> 
                </xsl:otherwise>
            </xsl:choose>
            
            
            <xsl:choose>
                <xsl:when test="$lang = 'ru'">
                    <xsl:text>.</xsl:text>ru-RU<xsl:text>.xml</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>en-US<xsl:text>.xml</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="firstLvl" select="document($fileName)/root"/>

        <xsl:if test="$firstLvl/newslist/item">
                <div class="news inner">

                    
                    
                    <xsl:for-each select="/response/pageContent/promo/newslist/item">

                        <xsl:choose>
                            <xsl:when test="@target-id != ''">
                                 <xsl:variable name="link">
                                    <xsl:value-of select="$path"/>
                                    <xsl:if test="@target-parent-id">
                                        <xsl:text>&amp;page=</xsl:text>
                                        <xsl:value-of select="@target-parent-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-id">
                                        <xsl:text>&amp;type=</xsl:text>
                                        <xsl:value-of select="@target-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-main-id">
                                        <xsl:text>&amp;id=</xsl:text>
                                        <xsl:value-of select="@target-main-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-sub-id">
                                        <xsl:text>#</xsl:text>
                                        <xsl:value-of select="@target-sub-id" />
                                    </xsl:if>
                                </xsl:variable>

                                <xsl:call-template name="news-item">
                                    <xsl:with-param name="link" select="$link"/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="news-item">
                                    <xsl:with-param name="class">nolink</xsl:with-param>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                        

                
                    </xsl:for-each>

                    <xsl:for-each select="$firstLvl/newslist/item">
                        <xsl:choose>
                            <xsl:when test="@target-id != ''">
                                <xsl:variable name="link">
                                    <xsl:value-of select="$path"/>
                                    <xsl:if test="@target-parent-id">
                                        <xsl:text>&amp;page=</xsl:text>
                                        <xsl:value-of select="@target-parent-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-id">
                                        <xsl:text>&amp;type=</xsl:text>
                                        <xsl:value-of select="@target-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-main-id">
                                        <xsl:text>&amp;id=</xsl:text>
                                        <xsl:value-of select="@target-main-id"/>
                                    </xsl:if>
                                    <xsl:if test="@target-sub-id">
                                        <xsl:text>#</xsl:text>
                                        <xsl:value-of select="@target-sub-id" />
                                    </xsl:if>
                                </xsl:variable>

                                <xsl:call-template name="news-item">
                                    <xsl:with-param name="link" select="$link"/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="news-item">
                                    <xsl:with-param name="class">nolink</xsl:with-param>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                        
                    </xsl:for-each>
                </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="news-item">
        <xsl:param name="link"/>
        <xsl:param name="class"/>
        <xsl:choose>
            <xsl:when test="$link">
                <div class="item">
                    <h3>
                        <a href="{$link}">
                            <xsl:value-of select="@header"/>
                        </a>
                    </h3>
                    <a href="{$link}">
                        <xsl:apply-templates mode="brief-description-content" select="./*"/>
                    </a>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="item {$class}">
                    <h3>
                        <xsl:value-of select="@header"/>
                    </h3>
                    <xsl:apply-templates mode="brief-description-content" select="./*"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>

    

    <xsl:template name="mobi">
        <div class="bl-mobi">
            <h4>
                <xsl:apply-templates mode="brief-description-content" select="/response/pageContent/promo/mobile-page/*/."/>
            </h4>
            
            <a href="https://itunes.apple.com/ru/app/nika-glonass/id668566712?mt=8" target="_blank" style="margin:0 15px 0 0; text-decoration: none;">
                    <img src="/about/includes/sources/images/mobile_apps/appstore.png"/>
                </a>

                <a href="https://play.google.com/store/apps/details?id=ru.nvg.NikaMonitoring" target="_blank" style="margin:0 0px;">
                    <img src="/about/includes/sources/images/mobile_apps/android.jpg"/>
                </a>
        </div>
    </xsl:template>
    <xsl:template name="error">
        <xsl:variable name="page" select="/response/pageContent/promo/error-page"/>
        <h1>
            <xsl:value-of select="$page/@title"/>
        </h1>

        <xsl:apply-templates select="$page/*/." mode="brief-description-content"/>
    </xsl:template>
    
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="page.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
