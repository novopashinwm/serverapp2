﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html [
	<!ENTITY nbsp "&#160;">
	<!ENTITY hellip "&#8230;">
]>

<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns="http://www.w3.org/1999/xhtml" 
                xmlns:user="urn:my-scripts">
  <xsl:import href="xslt-scripts.xsl" />
  <xsl:import href="common.xsl" />
	<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />
	<!--xsl:output method="html" version="1.0" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" encoding="utf-8" indent="no" omit-xml-declaration="yes" standalone="omit" /-->

	<xsl:param name="date" />

    <xsl:template match="/">
      <xsl:variable name="selectedPage">
        <xsl:apply-templates select="." mode="selectedPage" />
      </xsl:variable>

    <html>
      
      <xsl:apply-templates select="." mode="headerLite" />
      <body class="loginPage">
        <script type="text/javascript" src="{/response/@ApplicationPath}/includes/scripts/lib/json2.js"></script>
        <script type="text/javascript" src="{/response/@ApplicationPath}/includes/scripts/login.js"></script>
        <xsl:choose>
          <xsl:when test="contains(/response/@language,'en-US')"></xsl:when>
          <xsl:otherwise>
            <div class="hlp">
              <span  class="basket">
                <a href="?page=calculator">Рассчитать стоимость</a>
              </span>&#160;<br/>
              <span class="quest">
                <a href="?page=question-support">Вопрос техподдержке</a>
              </span>
            </div>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="." mode="loginPageHeader" />
				<!--div class="divider" style="background-color:#fff;"><xsl:text> </xsl:text></div-->
				<div id="pageContent" class="content"> <!-- style="background-color:#f3f3f3;padding:0"-->
					<div class="divider"><xsl:text> </xsl:text></div>
					<div class="cols">
						<div class="col-left3"> <!--style="background-color:#f3f3f3;padding:10px;padding-right:0;"-->
						<xsl:comment>[if lt IE 9]&gt;		
						&lt;div class="ie" &gt;
						&lt;![endif]</xsl:comment>
              <div class="row">                
              <xsl:comment>[if lt IE 9]&gt;		
              &lt;div id="noIE" &gt;
                &lt;div class="legend" style="text-transform: uppercase" &gt;<xsl:value-of select="$locale/data[@name='attention']/value" />!&lt;/div&gt;
                &lt;div&gt;
                  &lt;div style="float: left; overflow: hidden;padding-left:1em;padding-right:1em;width:150px;"&gt;                    
                    &lt;div&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='dearUser']/value" />!&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='pleaseUseModernBrowsersWithTheService']/value" />&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='useOneOfLinksListedBelowToGetRecomendedBrowser']/value" />&lt;/p&gt;
                &lt;p>&lt;/p&gt;
                &lt;p><xsl:value-of select="$locale/data[@name='thanks']/value" />&lt;/p&gt;
                
                &lt;/div&gt;
                &lt;/div&gt;
                
                &lt;div style="float: left; margin-left: 7px;"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" href="http://www.mozilla-europe.org/firefox"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> FireFox" style="border: none;" src="<xsl:value-of select="/response/@ApplicationPath" />/images/ie6no-firefox.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/div&gt;
                &lt;div style="float: left; margin-left: 5px;"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" href="http://www.google.com/chrome"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Google Chrome" style="border: none;" src="<xsl:value-of select="/response/@ApplicationPath" />/images/ie6no-chrome.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/div&gt;
                &lt;div style="float: left; margin-left: 5px;"&gt;
                &lt;a target="_blank" title="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" href="http://www.opera.com/"&gt;
                &lt;img alt="<xsl:value-of select="$locale/data[@name='downloadBrowser']/value" /> Opera" style="border: none;" src="<xsl:value-of select="/response/@ApplicationPath" />/images/ie6no-opera.jpg" width="56" /&gt;
                &lt;/a&gt;
                &lt;/div&gt;

                &lt;div class="clear"&gt;
                &lt;/div&gt;

                

                &lt;/div&gt;
              &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class="row" style="display: none"&gt;
              &lt;![endif]</xsl:comment>
                <form id="loginForm" action="{//@ApplicationPath}/login.aspx" method="POST">
                  <input type="hidden" name="a" id="a" value="login" />
                  <input type="hidden" name="tz" id="tz" value="" />
                  <input type="hidden" name="dst" id="dst" value=""/>
                  <input type="hidden" name="departmentId" id="departmentId" value="" />

                  <fieldset>
                    <div class="legend">
                      <xsl:value-of select="$locale/data[@name='systemEntrance']/value" />
                    </div>

                    <div>
                      <xsl:apply-templates select="/response/message" />

                      <div class="row">
                        <div class="left-column">
                          <label for="login" class="mandatory">
                            <xsl:value-of select="$locale/data[@name='Login']/value" />:
                          </label>
                        </div>
                        <div class="right-column">
                          <a href="{/response/@ApplicationPath}/Register.aspx" tabindex="4">
                            <xsl:value-of select="$locale/data[@name='registration']/value" />
                          </a>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <div class="row">
                        <div class="left-column">
                          <input type="text" name="login" id="txtLogin" value="{//@previousLogin}" tabindex="1" />
                        </div>
                        <div class="clear"></div>
                      </div>

                      <div class="row">
                        <div class="left-column">
                          <label for="password" class="mandatory">
                            <xsl:value-of select="$locale/data[@name='Password']/value" />:
                          </label>
                        </div>
                        <div class="right-column">
                          <a href="{/response/@ApplicationPath}/PasswordRecovery.aspx" tabindex="5">
                            <xsl:value-of select="$locale/data[@name='forgotPassword']/value" />?
                          </a>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <div class="row">
                        <input type="password" name="password" id="password" tabindex="2" />
                        <div class="clear"></div>
                      </div>
                      
                      <div class="row">
                        <div class="left-column">
                          <button type="button" id="login-submit" tabindex="3">
                            <xsl:value-of select="$locale/data[@name='GetIn']/value" />
                          </button>
                          <img src="includes/images/wait.gif" style="position: relative;top: 5px;left: 3px; display:none;"/>
                        </div>

                        <div class="right-column">
                          <ul class="buttons">
                            <li>
                              <xsl:call-template name="demoLoginLink" />
                            </li>
                          </ul>
                        </div>
                        <div class="clear"></div>
                      </div>

                      <div class="clear"></div>
                    </div>
                  </fieldset>
                </form>
              </div>

              <div class="row">
                
                <xsl:call-template name="briefDescriptionTitle">
                  <xsl:with-param name="selectedPage" select="$selectedPage" />
                </xsl:call-template>
              </div>
              <div class="clear"></div>
			  
						<xsl:comment>[if lt IE 9]&gt;		
						&lt;/div&gt;
						&lt;![endif]</xsl:comment>
			  </div>
            
						<div class="col-main3">
              <xsl:call-template name="briefDescription">
                <xsl:with-param name="selectedPage" select="$selectedPage" />
              </xsl:call-template>
              <div class="clear"></div>
						</div>
					</div>
					<div class="clear"><xsl:text>&#160;</xsl:text></div>
				</div>
				<!--div class="divider"><xsl:text>&#160;</xsl:text></div-->
        <xsl:call-template name="fstPageFooter" />
			</body>
		</html>
	</xsl:template>

  <xsl:template match="response" mode="selectedPage">
    <xsl:variable name="urlReferrer" select="./@UrlReferrer" />
    
    <xsl:choose>
      <xsl:when test="./@selectedPage">
        <xsl:variable name="selectedPageValue" select="./@selectedPage" />
        <xsl:choose>
          <xsl:when test="./pageContent/promo/menu/page[@id=$selectedPageValue]">
            <xsl:value-of select="./@selectedPage" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="count(./pageContent/promo/UrlReferrer[user:Contains($urlReferrer, @from)]) &gt; 0">
        <xsl:value-of select="./pageContent/promo/UrlReferrer[user:Contains($urlReferrer, @from)]/@to"/>
      </xsl:when>
      <xsl:when test="./pageContent/promo/page">
        <xsl:value-of select="./pageContent/promo/page[1]/@id" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="./pageContent/promo/menu[1]/page[1]/@id" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--Заголовки описания системы-->
  <xsl:template name="briefDescriptionTitle">
    <xsl:param name="selectedPage"></xsl:param>
    <xsl:if test="/response/pageContent/promo">
      <div class="briefDescriptionTitle">
        <div class="legend">
          <xsl:choose>
            <xsl:when test="/response/pageContent/promo/page">
              <a href="{/response/@ApplicationPath}">
                <xsl:value-of select="/response/pageContent/promo/@title" />
              </a>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="/response/pageContent/promo/@title" />
            </xsl:otherwise>
          </xsl:choose>
        </div>
        <ul>

          <xsl:for-each select="/response/pageContent/promo/menu">
            <li id="{./@id}-bookmark">

              <xsl:variable name="is-selected" select="./page[@id=$selectedPage]" />
              <xsl:if test="$is-selected">

                <xsl:choose>
                  <xsl:when test="count(./page)>1">
                    <xsl:attribute name="class">selected</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="class">selected-free</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                
                
              </xsl:if>

              <xsl:choose>
                <xsl:when test="./@default">
                  <xsl:variable name="defaultPage" select="./@default" />
                  <xsl:apply-templates select="./page[@id=$defaultPage]" mode="hyperlink">
                    <xsl:with-param name="text" select="./@title" />
                  </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:apply-templates select="./page[1]" mode="hyperlink">
                    <xsl:with-param name="text" select="./@title" />
                  </xsl:apply-templates>
                </xsl:otherwise>
              </xsl:choose>

              <xsl:if test="count(./page)>1">
              
                <xsl:if test="$is-selected">
                  <ul>
                    <xsl:for-each select="./page">
                      <li id="{./@id}-bookmark">
                        <xsl:if test="./@id = $selectedPage">
                          <xsl:attribute name="class">selected</xsl:attribute>
                        </xsl:if>

                        <xsl:apply-templates select="." mode="hyperlink" />

                      </li>
                    </xsl:for-each>
                  </ul>
                </xsl:if>
                
              </xsl:if>
            </li>
          </xsl:for-each>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="page" mode="hyperlink">
    <xsl:param name="text" select="./@title" />
    <a href="{/response/@ApplicationPath}/?page={./@id}">
      <xsl:value-of select="$text"/>
    </a>
  </xsl:template>
  
  <!-- AS: краткое описание системы -->
  <xsl:template name="briefDescription">
    <xsl:param name="selectedPage"/>
    <xsl:if test="/response/pageContent/promo">
      <div id="briefDescription">
        <div>
          <!-- Способ именования закладок: -page - постфикс для именования страниц, -bookmark - закладок-->
          <!--<div id="-page" class="promoDescription">
          
        </div>-->
          <xsl:apply-templates select="/response/pageContent/promo//page[@id=$selectedPage]" mode="brief-description" />
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="page" mode="brief-description">
    <div id="{./@id}-page" class="promoDescription">
      <h1>
        <xsl:choose>
          <xsl:when test="./@pageTitle">
            <xsl:value-of select="./@pageTitle"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="./@title"/>
          </xsl:otherwise>
        </xsl:choose>
      </h1>
      <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
      <xsl:choose>
        <xsl:when test="contains(/response/@language,'en-US')">
          <div class="yashare-auto-init" style="float: right;position: relative;top: -55px;right:30px;" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="facebook,twitter,lj,gplus"></div>
        </xsl:when>
        <xsl:otherwise>
          <div class="yashare-auto-init" style="float: right;position: relative;top: -55px;right:30px;" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,moimir,lj,gplus"></div>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="./*" mode="brief-description-content" />
    </div>
  </xsl:template>

  <xsl:template match="node()" mode="brief-description-content">
    <xsl:choose>
      <xsl:when test="local-name() = 'common-part'">
        <xsl:variable name="common-part-id" select="./@id" />
        <xsl:apply-templates select="/response/pageContent/promo/common-part[@id=$common-part-id]/*" mode="brief-description-content" />
      </xsl:when>
      <xsl:when test="local-name() = 'post'">
        <xsl:variable name="media-id" select="./@media" />
        <xsl:apply-templates select="." />
      </xsl:when>
      <xsl:when test="local-name() = 'ref'">
        <xsl:element name="a">
          <xsl:apply-templates select="@*" mode="brief-description-content"/>
          <xsl:apply-templates select="text()" mode="brief-description-content" />
          <xsl:apply-templates select="./*" mode="brief-description-content" />
        </xsl:element>
      </xsl:when>
      <xsl:when test="local-name() = 'demo-login-link'">
        <xsl:call-template name="demoLoginLink">
          <xsl:with-param name="title">здесь</xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="{local-name()}">
          <xsl:apply-templates select="@* | ./* | text()" mode="brief-description-content"/>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="text()" mode="brief-description-content">
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <xsl:template match="@*" mode="brief-description-content">
    <xsl:choose>
      <xsl:when test="local-name(..) = 'img' and local-name() = 'src'">
        <xsl:attribute name="src">
          <xsl:value-of select="/response/@ApplicationPath"/>/includes/images/<xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="local-name(..) = 'ref' and local-name() = 'target-id'">
        <xsl:attribute name="href">
          <xsl:value-of select="/response/@ApplicationPath"/>/?page=<xsl:value-of select="."/>
          <xsl:if test="../@target-sub-id">
            <xsl:text>#</xsl:text>
            <xsl:value-of select="../@target-sub-id" />
          </xsl:if>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="post">
    <div style="width:100%; height:30px;"></div>
    <div class="media-post">
      <xsl:variable name="media-id" select="./@media" />
      <xsl:apply-templates select="/response/pageContent/promo/media[@id=$media-id]" mode="logo" />
      <h2>
        <a href="{./@link}">
          <xsl:value-of select="./@head" />
        </a>
      </h2>
      <p>
        <xsl:value-of select="text()"/>
      </p>      
    </div>
  </xsl:template>

  <xsl:template match="media" mode="logo">
    <img style="float:left;" width="150px"
      src="{/response/@ApplicationPath}/includes/images/promo/logo/{./@logo}"
      alt="{./@name}"
      title="{./@name}"/>
  </xsl:template>
  
	<!-- AS: заголовок страницы логина -->
	<xsl:template match="response" mode="loginPageHeader">
    <xsl:call-template name="common-page-header">
      <xsl:with-param name="additional-main-content">
        

        <span id="mobileVersionLink" >
          <xsl:attribute name="style">
            <xsl:choose>
              <xsl:when test="not(//./Person/Id) and contains(/response/@location, 'gu-')">
                <xsl:text>left: auto;right: 124px;</xsl:text>
              </xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <a href="{//@ApplicationPath}/mobile">
            <xsl:value-of select="$locale/data[@name='mobileVersion']/value" />
          </a>
        </span>
        

        <div id="newUrlRedirectMessage" style="display:none; color:red; font-weight: bold; font-size: 16px;position: absolute; left:400px; top:20px; width:500px;">
          <xsl:value-of select="$locale/data[@name='newUrlRedirectMessage1']/value" />
          <a style="font-size: 16px;" href="{./@ServerName}">http://<xsl-value-of select="./@ServerName" /></a>
          <br></br>
          <xsl:value-of select="$locale/data[@name='newUrlRedirectMessage2']/value" />
        </div>

        
        
      </xsl:with-param>
    </xsl:call-template>
	</xsl:template>

	<!-- AS: футер -->
	<xsl:template name="fstPageFooter">
		<div class="pageFooter">
      <div style="width:100%;">
				<div style="float:left;width:30%">
					<xsl:call-template name="copyrightSitelsText" />
				</div>
				<div style="float:left;width:70%;">
					<div style="padding-left:5px;">
            <xsl:if test="/response/@SalesPhone or /response/@SalesEmail">
              <h3>
                <xsl:value-of select="$locale/data[@name='salesDepartment']/value" />
              </h3>
              <xsl:if test="/response/@SalesPhone">
                <div>
                  <xsl:value-of select="$locale/data[@name='phone']/value" />: <xsl:value-of select="/response/@SalesPhone"/>
                </div>
              </xsl:if>
              <xsl:if test="/response/@SalesEmail">
                <div>
                  Email: <a href="mailto:{/response/@SalesEmail}">
                    <xsl:value-of select="/response/@SalesEmail"/>
                  </a>
                </div>
              </xsl:if>
            </xsl:if>
					</div>
				</div>
			</div>
			<div class="clear"><xsl:text> </xsl:text></div>
			<div class="divider"><xsl:text> </xsl:text></div>
		</div>
	</xsl:template>


	<!-- AS: облегченный заголовок для первой страницы -->
	<xsl:template match="response" mode="headerLite">
    
    <head>
			<meta name="description" content="Онлайн мониторинг и позиционирование транспортых средств" />
			<meta name="keywords" content="мониторинг, транспортных, средств, позиционирование, карта, местности, контроль, местонахождения, объектов, маршрут, движения" />

			<meta http-equiv="Expires" content="{$date}" />
      <title>
        <xsl:value-of select="./@title" />

        <xsl:variable name="selectedPage">
          <xsl:apply-templates select="." mode="selectedPage" />
        </xsl:variable>

        <xsl:variable name="subTitle" select="//page[@id=$selectedPage]/@title" />
        
        <xsl:if test="$selectedPage and $subTitle != ./@title">
          <xsl:text> – </xsl:text>
          <xsl:value-of select="$subTitle" />
        </xsl:if>
        

      </title>
			<link rel="stylesheet" href="{./@ApplicationPath}/includes/styles/global.css" />
			<link rel="stylesheet" href="{$localLogoCSS}" />
			<link rel="shortcut icon" href="{./@ApplicationPath}/{./@favicon}"/>
			<script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/lib/jquery/jquery.js"></script>
			<script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/iepngbfix.js"></script>

			<script type="text/javascript">
				var applicationPath = "<xsl:value-of select="./@ApplicationPath" />";
        var serverName = "<xsl:value-of select="./@ServerName"/>";
        var currentCulture = "<xsl:value-of select="substring-before(./@language, '-')" />";
        var serverDate = "<xsl:value-of select="./@serverDate" />";
      </script>

			<script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/common.js"></script>
			<style type="text/css">
				v\: * { behavior:url(#default#VML); display:inline-block }
				o\:* { behavior:url(#default#VML); }
			</style>
      <script type="text/javascript" src="{./@ApplicationPath}/includes/scripts/style.js"></script>

      <!-- Счетчики посещений -->
      <xsl:if test="./@counters-enabled = 'true'">
        <!-- Yandex.Metrika counter -->
        <div style="display:none;">
          <script type="text/javascript">
            (function(w, c) {
            (w[c] = w[c] || []).push(function() {
            try {
            w.yaCounter9754051 = new Ya.Metrika({id:9754051, enableAll: true});
            }
            catch(e) { }
            });
            })(window, "yandex_metrika_callbacks");
          </script>
        </div>
        <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
        <noscript>
          <div>
            <img src="//mc.yandex.ru/watch/9754051" style="position:absolute; left:-9999px;" alt="" />
          </div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-25531980-1']);
          _gaq.push(['_trackPageview']);

          (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        </script>
      </xsl:if>
      
      <xsl:if test="./@online-assistant = 'true'">
        <xsl:comment> BEGIN JIVOSITE CODE {literal} </xsl:comment>
        <script type='text/javascript'>
          (function(){ var widget_id = '42533';
          var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
        </script>
        <div id='jivo_copyright' style='display: none'>
          <a href='http://www.jivosite.ru/features' target='_blank'>Установка чата на сайт</a> сервис Живой Сайт
        </div>
        <xsl:comment> {/literal} END JIVOSITE CODE </xsl:comment>
      </xsl:if>

		<script type="text/javascript" src="{$localJsName}"></script>
    </head>
  </xsl:template>

  <xsl:template name="demoLoginLink">
    <xsl:param name="title" select="$locale/data[@name='demoEnter']/value" />
    
    <a class="demoLoginLink" href="{//@ApplicationPath}/login.aspx?a=demo" tabindex="6">
      <xsl:value-of select="$title"/>
    </a>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->
