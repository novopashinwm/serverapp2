using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Xsl;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;

namespace RU.NVG.Web.WebPromo2013
{
    /// <summary>
    /// ������� ����� ��� ���-�������, �����������:
    /// 1. ��������� �������� � ���� a=some-action
    /// 2. ���������� ���������� XSLT-������� � ����������� XML
    /// </summary>
    public class BasePage : System.Web.UI.Page
    {

        protected List<Message> messages;

        protected bool contentTypeSet = false;

        protected int operatorId = -1;

        protected HttpResponse response;

        protected List<Message> Messages
        {
            get
            {
                if (messages == null)
                {
                    messages = new List<Message>();
                }
                return messages;
            }
        }

        protected Dictionary<String, String> properties;

        protected CultureInfo currentCulture;

        protected virtual CultureInfo CurrentCulture
        {
            get
            {
                if (currentCulture == null)
                {
                    currentCulture = new CultureInfo("ru-RU");
                }

                return currentCulture;
            }
        }

        /// <summary>
        /// AS: ���� ����������� ����� ������� (��������)
        /// </summary>
        private XmlTextWriter writer;

        protected XmlTextWriter Writer
        {
            get { return writer; }
            set { writer = value; }
        }

        public bool IsAskPositionAvailable;
        public bool IsIginitionMonitoringAvailable;
        public bool HasSpeedMonitoring;
        public bool HasDigitalSensors;

        /// <summary>
        /// AS: ����, ���������� XSL ������ ��� ��������� ���������������� � XML ������ �������
        /// </summary>
        private String _templateFile = "";

        protected virtual String TemplateFile
        {
            get { return _templateFile; }
            set
            {
                if (IsMobile)
                {
                    _templateFile =
                        Server.MapPath(@"/" + Request.ApplicationPath + @"/templates/mobile/" + value + ".xsl");
                }
                else
                {
                    _templateFile = Server.MapPath(@"/" + Request.ApplicationPath + @"/templates/" + value + ".xsl");
                }
            }
        }

        private bool IsMobile
        {
            get
            {
                //TODO: ������������� ��������� �������
                return false;
            }
        }

        protected virtual Dictionary<String, String> Properties
        {
            get
            {
                if (properties == null)
                {
                    properties = new Dictionary<string, string>();
                }

                return properties;
            }
        }

        public BasePage()
        {
            writer = null;
        }

        /// <summary>
        /// AS: ���������� ������� ��������
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        protected virtual void AddProperty(String key, String val)
        {
            if (!Properties.ContainsKey(key))
            {
                Properties.Add(key, val);
            }
            else
            {
                Properties[key] = val;
            }
        }


        /// <summary>
        /// AS: ������ ������������ ������ �������
        /// </summary>
        /// <returns></returns>
        protected virtual void ResponseSerializationBegin()
        {
            try
            {
                Writer.Formatting = Formatting.None;
                Writer.WriteStartDocument();
                Writer.WriteStartElement("response");


                // AS: ������� ��� ���������� � �������� - ����� �������������
                var applicationPath = Request.ApplicationPath;
                if (applicationPath != null)
                    AddProperty("ApplicationPath", applicationPath.Length == 1 ? string.Empty : applicationPath);
                
                AddProperty("pageTitleNum", "1");
                // AS: ��� �������
                var webSiteUrl = ConfigurationManager.AppSettings["webSiteUrl"];

                //AS: ��������� �����
                
                if (!string.IsNullOrWhiteSpace(webSiteUrl))
                    AddProperty("ServerName", webSiteUrl.Replace("http://", "")); //Request.ServerVariables["HTTP_HOST"]);

                // AS: � ������� ����
                AddProperty("language", CurrentCulture.ToString());

                AddProperty("location", ConfigurationManager.AppSettings["defaultPromoCulture"]);

                // AS: ��� ��������
                AddProperty("browser", Request.Browser.Type);

                AddProperty("logo", ConfigurationManager.AppSettings["logo"] ?? "logo");
                AddProperty("favicon", ConfigurationManager.AppSettings["favicon"] ?? "includes/images/application/favicon.ico");

                TranslateConfig("sessionStateTimeout");

                if (Request.UrlReferrer != null)
                {
                    AddProperty("UrlReferrer", Request.UrlReferrer.ToString());
                }

#if DEBUG
                AddProperty("debug", "true");
#endif

                // AS: ����������� ��� ��������. ��� ��������� �������
                foreach (String key in properties.Keys)
                {
                    Writer.WriteAttributeString(key, properties[key]);
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        /// <summary>
        /// AS: ��������� ������������ ������ �������
        /// </summary>
        protected virtual void ResponseSerializationEnd()
        {
            // AS: ������������ ���������
            SerializeMessages();

            Writer.WriteEndElement(); // response
            Writer.WriteEndDocument();
        }


        /// <summary>
        /// AS: ������������ ���������
        /// </summary>
        protected virtual void SerializeMessages()
        {
            foreach (Message msg in Messages)
            {
                msg.WriteXml(Writer);
            }
        }



        /// <summary>
        /// AS: �������������� ������, ����������� � ������, � �����
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected virtual String GetStringOfStream(Stream s)
        {
            if (s == null) 
                throw new ArgumentNullException("s");
            s.Position = 0;
            byte[] buffer = new byte[s.Length];
            s.Read(buffer, 0, (int)s.Length);
            System.Text.UTF8Encoding utf8 = new UTF8Encoding();
            StringBuilder sb = new StringBuilder();
            //StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append(utf8.GetString(buffer));
            return sb.ToString();
        }

        /// <summary>
        /// AS: ������ ���������������� ������ ������� � �������� ����� ��� �������� �������
        /// </summary>
        /// <param name="stm"></param>
        /// <param name="outputStream"></param>
        protected virtual void WriteXmlToStream(Stream stm, Stream outputStream)
        {
            var methodStopwatch = new Stopwatch();
            methodStopwatch.Start();
            XmlReader reader = null;
            XmlTextWriter writer = null;
            try
            {
                #region Debug stuff
#if XML
            System.IO.FileStream fs = null;
            try
            {
                String xmlString = "";
                byte[] buffer = null;
                buffer = new byte[stm.Length];
                stm.Position = 0;
                stm.Read(buffer, 0, (int)stm.Length);

                System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding();
                xmlString = utf8.GetString(buffer);
                String tfile = TemplateFile.Length > 0 ? TemplateFile : "page";
                tfile = Path.GetFileName(tfile);
                String file_name = ConfigurationManager.AppSettings["debugOutputFile"] + tfile + "_" + this.OperatorId.ToString() + ".xml";
                //String file_name = @"d:\temp\page\page.xml";
                try
                {
                    fs = new FileStream(
                        file_name, 
                        File.Exists(file_name) ? FileMode.Truncate : FileMode.Create);
                }
                catch (Exception)
                {
                    fs = new FileStream(file_name, FileMode.Create);
                }

                fs.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("{0}", ex);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
#endif
                #endregion

                stm.Position = 0;
            
                // AS: ����� ��� ����� ������ ���� ����������� XmlDocument'� �������� BaseURI,
                // ������ ��� XML ������� � ������, � � ���� ��� ������ URI, ��� � XML �� �����.
                // ��� baseURI ������� document() ������ ��� ������� �����, ������ ��� �� ����� ���� URI.
                // ��� ����� ��������...
                XmlNameTable xnt = new NameTable();
                XmlParserContext xparCon = new XmlParserContext(xnt, new XmlNamespaceManager(xnt), null, XmlSpace.Preserve);
                xparCon.BaseURI = this.Request.Url.ToString();

                /*
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ProhibitDtd = false;
            reader = XmlTextReader.Create(stm, settings, xparCon);
            */
                reader = new XmlTextReader(stm, XmlNodeType.Document, xparCon);

                // AS: ���� �� ������� �������� � ������-�� ������������� (� ���� �� ���������),
                // AS: �� � XSL ������� document() �������� �� ��� �� �����, ������� MSDN
                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                var xslt = GetOrLoadXslt(TemplateFile);

                var xdoc = new XmlDocument();
                xdoc.Normalize();
                xdoc.Load(reader);

                outputStream.Flush();
                writer = new XmlTextWriter(outputStream, Encoding.UTF8);

                var args = new XsltArgumentList();
                DateTime nowDate = DateTime.Now;
                args.AddParam("date", "", nowDate.ToString());

                //
                if (string.Equals(Response.ContentType, "application/xhtml+xml", StringComparison.OrdinalIgnoreCase)
                    && IsMobile)
                {
                    Response.ContentType = "text/html";
                }

                if (string.Equals(Response.ContentType, "text/html", StringComparison.OrdinalIgnoreCase)
                    || string.Equals(Response.ContentType, "application/xhtml+xml", StringComparison.OrdinalIgnoreCase))
                {
                    var brVer = GetInternetExplorerVersion();
                    //if (brVer >= 7 || brVer < 0)
                    if (brVer >= 8)
                        writer.WriteRaw("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                    else if (brVer >= 7)
                        writer.WriteRaw("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
                    else
                        writer.WriteRaw("<!DOCTYPE html>");
                }

                var sw = new Stopwatch();
                sw.Start();
                xslt.Transform(xdoc, args, writer, resolver); //(xdoc, args, writer, resolver);
                sw.Stop();
                LogStopwatch("Document transformation by " + Path.GetFileName(TemplateFile), sw);
                sw.Reset();

#if XML
            String tfileout = TemplateFile.Length > 0 ? TemplateFile : "page";
            tfileout = Path.GetFileName(tfileout);
            String fnOutput = ConfigurationManager.AppSettings["debugOutputFile"] + tfileout + "_" + this.OperatorId.ToString() + ".output";
            FileStream fsOutput = null;
            try
            {
                try
                {
                    fsOutput = new System.IO.FileStream(fnOutput, System.IO.FileMode.Truncate);
                }
                catch (Exception)
                {
                    fsOutput = new System.IO.FileStream(fnOutput, System.IO.FileMode.Create);
                }
                if (fsOutput != null)
                {
                    XmlTextWriter writerd = new XmlTextWriter(fsOutput, Encoding.UTF8);
                    xslt.Transform(xdoc, args, writerd, resolver);
                    writerd.Close();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (fsOutput != null)
                {
                    fsOutput.Close();
                }
            }
#endif

                outputStream.Flush();
            }
            catch (ArgumentException ex)
            {
                WriteLog(ex);
                throw ex;
            }
            catch (XsltException ex)
            {
                WriteLog(ex);
                throw ex;
            }
            catch (XmlException ex)
            {
                WriteLog(ex);
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                WriteLog(ex);
                throw ex;
            }
            catch (FileNotFoundException ex)
            {
                WriteLog(ex);
                throw ex;
            }
            catch (Exception appEx)
            {
                WriteLog(appEx);
                throw appEx;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (writer != null)
                    writer.Close();
            
                methodStopwatch.Stop();
                LogStopwatch("WriteXmlToStream", methodStopwatch);
            }
        }

        private static readonly TimeSpan StopwatchLoggingThreshold = TimeSpan.FromSeconds(1);

        protected void LogStopwatch(string tag, Stopwatch sw)
        {
            if (StopwatchLoggingThreshold < sw.Elapsed)
                System.Diagnostics.Trace.TraceWarning(tag +" is too slow: " + sw.Elapsed);
        }

        private static readonly ConcurrentDictionary<string, XsltBucket> XslTemplates =
            new ConcurrentDictionary<string, XsltBucket>();

        private class XsltBucket
        {
            public DateTime FileDateTime;
            public XslTransform Xslt;
        }
    
        private static XslTransform LoadXslt(string templateFile)
        {
            System.Diagnostics.Trace.TraceInformation("Loading template " + templateFile);
            var xslt = new XslTransform();
            xslt.Load(templateFile);
            return xslt;
        }

        private static XslTransform GetOrLoadXslt(string templateFile)
        {
            XsltBucket xsltBucket;
            var lastWriteTime = File.GetLastWriteTime(templateFile);
            if (XslTemplates.TryGetValue(templateFile, out xsltBucket))
            {
                lock (xsltBucket)
                {
                    if (lastWriteTime != xsltBucket.FileDateTime)
                    {
                        xsltBucket.FileDateTime = lastWriteTime;
                        xsltBucket.Xslt = LoadXslt(templateFile);
                    }
                }
            }
            else
            {
                xsltBucket = new XsltBucket
                    {
                        FileDateTime = lastWriteTime,
                        Xslt = LoadXslt(templateFile)
                    };
                XslTemplates.TryAdd(templateFile, xsltBucket);
            }
            return xsltBucket.Xslt;
        }

        private float GetInternetExplorerVersion()
        {
            // ���������� ������ Internet Explorer ��� -1, ���� ��� ������ �������
            float rv = -1;
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            if (browser.Browser == "IE")
                rv = (float)(browser.MajorVersion + browser.MinorVersion);
            return rv;
        }

        /// <summary>
        /// AS: ��������� �������� ���������, ����������� �� �������
        /// </summary>
        /// <param name="paramKey">���� ���������</param>
        /// <returns>�������� ���������</returns>
        protected virtual String GetParamValue(String paramKey)
        {
            //String val = Server.HtmlDecode((Request.Params[paramKey] != null ? Request.Params[paramKey] : ""));
            //Request.ContentEncoding = System.Text.Encoding.UTF8;

            var val = string.Empty;
            switch (Request.HttpMethod.ToLower())
            {
                case "get":
                    val = Request.QueryString[paramKey] != null ? Server.HtmlDecode(Request.QueryString[paramKey]) : "";
                    break;
                case "post":
                    //val = Request.
                    val = Request.Form[paramKey] != null ? Server.HtmlDecode(Request.Form[paramKey]) : "";
                    break;
                default:
                    val = Request.Params[paramKey] != null ? Server.HtmlDecode(Request.Params[paramKey]) : "";
                    break;
            }

            return val;
        }

        /// <summary>
        /// AS: ���������� ������ �������� ����������, ����� ������� �������� ������ paramKeyTemplate
        /// </summary>
        /// <param name="paramKeyTemplate">������ ������ �����</param>
        /// <param name="value">�������� ��������� (���� ������ ������, ���������� ��� ��������, ��������������� ������� �����)</param>
        /// <returns></returns>
        protected virtual Dictionary<String, String> GetParamValues(String paramKeyTemplate, String value)
        {
            Dictionary<String, String> res = new Dictionary<string, string>();
            System.Collections.Specialized.NameValueCollection pars = null;
            switch (Request.HttpMethod.ToLower())
            {
                case "get":
                    pars = Request.QueryString;
                    break;
                case "post":
                    pars = Request.Form;
                    break;
            }

            foreach (String key in pars.AllKeys)
            {
                if (key.IndexOf(paramKeyTemplate) >= 0)
                {
                    String paramval = pars[key];
                    if (value == String.Empty || paramval == value)
                    {
                        res.Add(key, paramval);
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// AS: ������������� ��������
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            response = HttpContext.Current.Response;
            var siteUrl = ConfigurationManager.AppSettings["webSiteUrl"];

            if (!string.IsNullOrWhiteSpace(siteUrl) && !Request.Url.OriginalString.Contains(siteUrl))
            {
                response.Clear();
                response.Status = "302 Found";
                response.AddHeader("Location", siteUrl + (Request.ApplicationPath.Length == 1
                                                              ? "/"
                                                              : Request.ApplicationPath + "/") + (Request.Url.OriginalString.Contains("mobile") ? "mobile" : ""));
                response.End();
                response.Flush();
            }
        
            MemoryStream mem = new MemoryStream();
            writer = new XmlTextWriter(mem, System.Text.Encoding.UTF8);

            this.PreRenderComplete += new EventHandler(this.OnLoadComplete);
            this.PreRender += new EventHandler(documentHeaderRenderer);
        }

        /// <summary>
        ///  AS: ������� ����� Page_Load'a
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_Load(object sender, EventArgs e)
        {
            var a = GetParamValue("a");

            if (Page.IsPostBack) 
                return;
        
            Process();
        }

        protected virtual void documentHeaderRenderer(Object sender, EventArgs e)
        {
        }

        protected virtual void OnLoadComplete(Object sender, EventArgs e)
        {
            FinishPageRender();
        }

        protected virtual void InsertDOCTypeDeclaration(bool strict)
        {
            /*
        if (strict)
        {
            response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
        }
        else
        {
            response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        }
        */
        }

        protected virtual void FinishPageRender()
        {
            // AS: ������������ ���������
            /*SerializeMessages();

        Writer.WriteEndElement(); // response
        Writer.WriteEndDocument();*/

            //text/plain is used for json
            if (response.ContentType != "text/plain")
            {
                Writer.Flush();
                if (string.IsNullOrEmpty(TemplateFile))
                {
                    try
                    {
                        if (!contentTypeSet)
                        {
                            SetXmlContentType();
                        }
                        response.Write(GetStringOfStream(Writer.BaseStream));
                    }
                    catch (ArgumentNullException aex)
                    {
                        WriteLog(aex);
                    }
                }
                else
                {
                    if (!contentTypeSet)
                    {
                        if (Request.Browser.Type.Contains("IE"))
                        {
                            SetHtmlContentType();
                        }
                        else
                        {
                            SetXhtmlContentType();
                        }
                    }

                    WriteXmlToStream(Writer.BaseStream, response.OutputStream);
                }
            }
            Writer.Close();
        }

        /// <summary>
        /// AS: ������ ������, ��������������� ���������� � �������� MyActionAttribute �������� � �������� ��
        /// </summary>
        protected virtual void Process()
        {
            // AS: ���� ������ ��� �������� �������
            String action = GetParamValue("a");
            action = string.IsNullOrEmpty(action) ?  "default" : action.Trim();
        
            try
            {
                MethodInfo[] methods = this.GetType().GetMethods();
                foreach (MethodInfo method in methods)
                {
                    bool found = false;
                    var attributes = (PageActionAttribute[])method.GetCustomAttributes(typeof(PageActionAttribute), false);
                
                    foreach (PageActionAttribute myAtt in attributes)
                    {
                        if (myAtt.Action == action)
                        {
                            var stopWatch = new Stopwatch();
                        
                            try
                            {
                                //get method parameters
                                var @params = method.GetParameters();
                                stopWatch.Start();
                                if (!@params.Any())
                                {
                                    method.Invoke(this, null);
                                }
                                else
                                {
                                    List<object> metodParams = new List<object>();
                                    metodParams.AddRange(@params.Select(p => GetParamValue(p.Name)));
                                    method.Invoke(this, metodParams.ToArray());
                                }
                            }
                            catch (ThreadAbortException)
                            {
                                //��� ���������, ��������� � method.Invoke
                                return;
                            }
                            catch (Exception e)
                            {
                                System.Diagnostics.Trace.TraceError("{0}: {1}", method.Name, e);
                            }
                            finally
                            {
                                stopWatch.Stop();
                                LogStopwatch(Request.Url + " a = " + action, stopWatch);
                            } 

                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        break;
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        protected void RenderLoginPage()
        {
            RenderLoginPage(
                string.Equals(ConfigurationManager.AppSettings["RenderLoginPageCorporative"], "true", StringComparison.OrdinalIgnoreCase),
                string.Equals(ConfigurationManager.AppSettings["RenderLoginPagePhysical"], "true", StringComparison.OrdinalIgnoreCase));
        }

        protected void RenderLoginPage(bool corporative, bool physical)
        {
            TemplateFile = "basic";

            AddProperty("template", "login");
            
            var page = GetParamValue("page");
            if (!string.IsNullOrWhiteSpace(page))
            {
                AddProperty("selectedPage", page.ToLower());
            }
            var type = GetParamValue("type");
            if (!string.IsNullOrWhiteSpace(type))
            {
                type = type.ToLower();

                var redirectedType = GetRedirectedType(type);

                if (redirectedType != null)
                {
                    var rd = ConfigurationManager.AppSettings["webSiteUrl"]
                              + (Request.ApplicationPath == null || Request.ApplicationPath.Length == 1
                                     ? "/"
                                     : Request.ApplicationPath + "/") + "?lang=ru&page=physical&type=" + redirectedType;

                    Response.Redirect(rd);
                }

                AddProperty("selectedPageType", type);
            }

            
            var subType = GetParamValue("id");
            if (!string.IsNullOrWhiteSpace(type))
            {
                AddProperty("selectedPageSubType", subType.ToLower());
            }
            
            AddProperty("language", "ru-RU");

            var isTest = GetParamValue("isTest");
            if (!string.IsNullOrWhiteSpace(isTest))
            {
                AddProperty("isTest", isTest.ToLower());
            }

            AddProperty("year", DateTime.Today.Year.ToString());
            AddProperty("month", DateTime.Today.Month.ToString());
            AddProperty("day", DateTime.Today.Day.ToString());

            AddProperty("counters-enabled", ConfigurationManager.AppSettings["counters-enabled"] ?? "true");
            TranslateConfig("online-assistant");

            ResponseSerializationBegin();

            if (Request.PhysicalApplicationPath != null)
            {
                string cultureName = CurrentCulture.Name;

                var promoPrefix = WebConfigurationManager.AppSettings["promoPrefix"];

                var promoPagePath = Path.Combine(Request.PhysicalApplicationPath,
                                                 @"includes\resources2\promo." + promoPrefix + cultureName + ".xml");

                if (!File.Exists(promoPagePath))
                {
                    System.Diagnostics.Trace.WriteLine("File not found: " + promoPagePath);
                    promoPagePath = Path.Combine(Request.PhysicalApplicationPath, @"includes\resources2\promo.ru-RU.xml");
                }

                Writer.WriteStartElement("pageContent");
                Writer.WriteRaw(File.ReadAllText(promoPagePath));
                Writer.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Trace.WriteLine("Request.ApplicationPath is null");
            }
            ResponseSerializationEnd();
        }

        private string GetRedirectedType(string type)
        {
            switch (type)
            {
                case "notification":
                case "nika-lbs":
                    return "family";
                case "nika-glonass":
                    return "auto";
                default:
                    return null;
            }
        }

        protected virtual void RedirectToMainPage()
        {
            if (IsMobile)
            {
                Context.Response.Redirect(Context.Request.ApplicationPath + "/mobile", true);
            }
            else
            {
                response.Redirect(Request.ApplicationPath, true);
            }
        }

        /// <summary>
        /// AS: ��������� ���� ����������� � XML
        /// </summary>
        protected virtual void SetXmlContentType()
        {
            response.ContentType = "text/xml";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            contentTypeSet = true;
        }


        /// <summary>
        /// AS: ��������� ���� ����������� � JSON
        /// </summary>
        protected virtual void SetJSONContentType()
        {
            response.ContentType = "text/plain";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            contentTypeSet = true;
        }

        /// <summary>
        /// AS: ��������� ���� ����������� � HTML
        /// </summary>
        protected virtual void SetHtmlContentType()
        {
            response.ContentType = "text/html";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            contentTypeSet = true;
        }

        /// <summary>
        /// AS: ��������� ���� ����������� � XHTML
        /// </summary>
        protected virtual void SetXhtmlContentType()
        {
            response.ContentType = "application/xhtml+xml";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            contentTypeSet = true;
        }

        protected virtual void SetImageGifContentType()
        {
            response.ContentType = "image/gif";
            contentTypeSet = true;
        }

        protected virtual void SetImageJpgContentType()
        {
            response.ContentType = "image/jpeg";
            contentTypeSet = true;
        }

        protected virtual void SetSvgContentType()
        {
            response.ContentType = "image/svg+xml";
            contentTypeSet = true;
        }

        protected virtual void SetApplicationMultipartContentType()
        {
            response.ContentType = "application/multipart";
            contentTypeSet = true;
        }

        /// <summary>
        /// AS: ��������� ��������� ���� �������� - ��� ������
        /// </summary>
        protected virtual void SetBinaryContentType()
        {

        }

        /// <summary>
        /// AS: ��������� ����������� ��������
        /// </summary>
        protected virtual void CacheMe()
        {
            response.Cache.SetExpires(DateTime.Now.AddHours(2));
            response.Cache.SetCacheability(HttpCacheability.Public);
        }

        /// <summary>
        /// AS: ��������� ����������� ��������
        /// </summary>
        protected virtual void dontCacheMe()
        {
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetLastModified(DateTime.UtcNow);
            response.Cache.SetNoServerCaching();
            response.Cache.SetNoStore();
        }

        /// <summary>
        /// AS: ���������� ��������� � ���� ���������
        /// </summary>
        /// <param name="msg"></param>
        protected void AddMessage(Message msg)
        {
            Messages.Add(msg);
        }

        protected string CurrentLanguage
        {
            get { return CurrentCulture.TwoLetterISOLanguageName; }
        }

        /// <summary>
        /// ��������� �������� �� ��������� �� �����
        /// </summary>
        /// <param name="language">���� (ru, en)</param>
        protected virtual void SetCultureByLanguage(String language)
        { 
            currentCulture = new CultureInfo("ru-RU");
           /* switch (language)
            {
                   
                case "ru":
                    currentCulture = null;
                    currentCulture = new CultureInfo("ru-RU");
                    break;
                case "en":
                    currentCulture = null;
                    currentCulture = new CultureInfo("en-US");
                    break;
                default:
                    currentCulture = null;
                    currentCulture = new CultureInfo("en-US");
                    break;
            }*/
        }

        /// <summary>
        /// AS: ������ � ��� ���������
        /// </summary>
        /// <param name="msg"></param>
        protected void WriteLog(String msg)
        {
            try
            {
                System.Diagnostics.Trace.WriteLine(msg);
            }
            catch (Exception) { }
        }

        protected void WriteLog(Exception ex)
        {
            var sbMsg = new StringBuilder(Request.RawUrl);
            sbMsg.AppendLine();
            while (ex != null)
            {
                sbMsg.AppendLine(ex.Message);
                sbMsg.AppendLine(ex.Source);
                sbMsg.AppendLine(ex.StackTrace);
                ex = ex.InnerException;
            }
            System.Diagnostics.Trace.TraceError(sbMsg.ToString());
        }

        protected void RedirectResponse(string localUrl)
        {
            
            if (localUrl.Contains("http://"))
                Response.Redirect(localUrl);


            var url = //HttpContext.Current.Request.Url.Host
                ConfigurationManager.AppSettings["webSiteUrl"] 
                + (Request.ApplicationPath.Length == 1
                       ? "/"
                       : Request.ApplicationPath + "/");

            if (IsMobile)
            {
                url += "mobile/";
            }
            url = url + localUrl;
            Response.Redirect(url, true);
        }

        protected void RedirectTransfer(string localUrl)
        {
            var url = ConfigurationManager.AppSettings["webSiteUrl"] + (Request.ApplicationPath.Length == 1
                                                                            ? "/"
                                                                            : Request.ApplicationPath + "/");

            Server.TransferRequest(url + localUrl, true);
        }

        protected void AccessIsDenied()
        {
            Response.Redirect(Request.ApplicationPath + "/error/403.aspx");
        }

        protected bool TryParseDateTime(string s, out DateTime result)
        {
            return TimeHelper.TryParseDateTime(s, out result);
        }

        protected DateTime ParseDateTime(string s)
        {
            return TimeHelper.ParseDateTime(s);
        }

        protected void TranslateConfig(string key)
        {
            var configValue = ConfigurationManager.AppSettings[key];
            if (!string.IsNullOrWhiteSpace(configValue))
                AddProperty(key, configValue);
        }
    }
}
