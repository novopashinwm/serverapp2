﻿using System;

namespace RU.NVG.Web.WebPromo2013
{
    public class PageActionAttribute : Attribute
    {
        public string Action { get; set; }

        public PageActionAttribute(String act)
        {
            Action = act;
        }
    }
}