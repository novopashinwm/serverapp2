﻿using System;
using System.Configuration;
using System.Linq;

namespace RU.NVG.Web.WebPromo2013
{
    public partial class MobileApp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.UserAgent != null)
            {
                if (TryGoToAppStore("AndroidAppPath", @" Android ")) return;

                if (TryGoToAppStore("AppleAppPath", @" CriOS/", @"(iPhone;", @"(iPad;")) return;
            }

            var baseUrl =
               ConfigurationManager.AppSettings["webSiteUrl"]
               + (Request.ApplicationPath == null || Request.ApplicationPath.Length == 1
                      ? "/"
                      : Request.ApplicationPath + "/");
            
            Response.Redirect(baseUrl + "?lang=ru&page=physical&type=mobileapps");
        }

        private bool TryGoToAppStore(string configName, params string[] userAgentTag)
        {
            var appStorePath = ConfigurationManager.AppSettings[configName];
            if (string.IsNullOrWhiteSpace(appStorePath))
                return false;

            if (Request.UserAgent == null)
                return false;

            if (!userAgentTag.Any(uat => uat.Contains(Request.UserAgent)))
                return false;

            Response.Redirect(appStorePath);
            return true;
        }
    }
}