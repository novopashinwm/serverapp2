﻿// Обязательная обёртка
module.exports = function (grunt) {

    // Задачи
    grunt.initConfig({
        jshint: {
            files: [
                '../sources/scripts/js/app'
            ]
        },
        requirejs: {
            compile: {
                options: {
                    appDir: "../sources/scripts",
                    baseUrl: "./js",
                    dir: "../production/scripts/build-ru",
                    optimize: "none",
                    removeCombined: true,
                    fileExclusionRegExp: /^(((build.*))|((r|build.*)\.js$)|(.*\.txt$)|(messages\.en-US\.js))/,
                    modules: [
                        {
                            name: 'app'
                        }
                    ],
                    paths: {
                        jquery: 'lib/jquery',
                        lightbox: 'lib/lightbox',
                        tooltipped: 'lib/Tooltipped',
                        messages: 'lib/messages.ru-RU',
                        jqueryForm: 'lib/jquery.form',
                        validation: 'lib/validation'
                    },
                    shim: {
                        app: {
                            deps: [
                                'jquery',
                                'tooltipped',
                                'lightbox',
                                'validation',
                                'jqueryForm',
                                'messages'
                            ],
                            exports: 'app'
                        }
                    }
                }
            }
        },
        less: {
           /* development: {
                options: {
                    paths: ["../assets"]
                },
                files: {
                    "../dev/styles/global.dev.css": "../sources/assets/global.less",
                    "../dev/styles/options.dev.css": "../sources/assets/options.less"
                }
            },*/
            production: {
                options: {
                    paths: ["../assets"],
                    yuicompress: true
                },
                files: {
                    "../production/styles/global.css": "../sources/assets/global.less",
                    "../production/styles/options.css": "../sources/assets/options.less"
                }
            }
        },

        css_img_2_data_uri: {
            options: {
                files: [
                {
                    src: '../production/styles/global.css',
                    dest: '../production/styles/global.img2uri.css'
                }
            ]
            }
        },
        

        concat: {
            options: {
              separator: ' ',
            },
            dist: {
              src: [
                  "../production/styles/global.img2uri.css", 
                  "../production/styles/options.css"
              ],
              dest: "../production/styles/production.css"
            },
          },
        
        watch: {
          scripts: {
            files: [
                '../sources/scripts/js/app/*'
            ],
            tasks: ['jshint', 'requirejs'],
            options: {
              spawn: false,
            }
          },
          css: {
            files: "../sources/assets/*.less",
            tasks: ['less', 'css_img_2_data_uri', 'concat'],
            options: {
              livereload: true,
            }
          }
        }

    });

    // Загрузка плагинов, установленных с помощью npm install
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-css-img-2-data-uri');
    grunt.loadNpmTasks('grunt-xmlmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    
    // Задача по умолчанию
    grunt.registerTask('default', ['jshint', 'less', 'css_img_2_data_uri', 'requirejs', 'concat']); //, 'watch'
};
