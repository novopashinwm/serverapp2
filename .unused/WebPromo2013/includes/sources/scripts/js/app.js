﻿
    require.config({
        // The shim config allows us to configure dependencies for
        // scripts that do not call define() to register a module
        paths: {
            jquery: 'lib/jquery',
            lightbox: 'lib/lightbox',
            messages: 'lib/' + NIKA_GLONASS.localJsName
        },
        shim: {
            main: {
                deps: [
                'jquery',
                'lightbox',
                'messages'
            ],
                exports: 'main'
            }
        }
    });



    require(['app/main'], function (main) { });
  





