function Tooltipped(options){
    var link = options.link;
    var tooltip = $('<div class="tooltip" />').appendTo(link.parent());

    function init(){
        $(link).mouseover(function (event) {
            $data_tooltip = $(this).attr("data-title");
            tooltip.text($data_tooltip)
                .css({
                    "top": event.pageY - $('.content').offset().top - tooltip.height() - 20,
                    "left": event.pageX - $('.content').offset().left + 20
                })
                .show();

        }).mouseout(function () {
            tooltip.hide()
                    .text("")
                    .css({
                        "top": 0,
                        "left": 0
                    });
        });
    }
    return {init: init};
}