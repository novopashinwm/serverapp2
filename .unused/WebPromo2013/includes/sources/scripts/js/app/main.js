(function (a, $) {
    var aBuffer = a.StringBuffer,
            mapPath;


    /* TODO: extract mapPath variable from javascript */
    a.mapPath = '/WebMap/';


    mapPath = a.mapPath;


    function isLocalStorageAvailable() {
        try {
            return 'localStorage' in window && window.localStorage !== null;
        } catch (e) {
            return false;
        }
    }

    aBuffer = function () {
        this.buffer = [];
    };

    aBuffer.prototype.add = function add(string) {
        this.buffer.push(string);
        return this;
    };

    aBuffer.prototype.toString = function toString() {
        return this.buffer.join("");
    };

    a.getLastDaylightSavingChanges = function (yearCount) {
        var currentYear = new Date().getFullYear(),
                result = [],
                firstMonth = 0,
                middleMonth = 5,
                lastMonth = 11,
                i0 = -yearCount,
                offsetWinter,
                offsetSummer,
                baseOffset,
                i,
                yearBegin,
                halfYear,
                yearEnd,
                delta;

        offsetWinter = getUtcOffset(new Date(currentYear - i0, firstMonth, 1, 0, 0, 0, 0));
        offsetSummer = getUtcOffset(new Date(currentYear - i0, middleMonth, 15, 0, 0, 0, 0));
        baseOffset = offsetSummer < offsetWinter ? offsetSummer : offsetWinter;

        /*Получение данных за yearCount вперед и назад, чтобы исключить проблемы при формировании отчетов по подписке*/
        for (i = -yearCount; i !== yearCount; ++i) {
            yearBegin = new Date(currentYear - i, firstMonth, 1, 0, 0, 0, 0);
            halfYear = new Date(currentYear - i, middleMonth, 15, 0, 0, 0, 0);
            yearEnd = new Date(currentYear - i, lastMonth, 31, 23, 59, 59, 0);

            delta = getUtcOffset(halfYear) - baseOffset;
            if (delta === 0)
                continue;

            result.push({
                on: findOffsetChange(yearBegin, halfYear),
                off: findOffsetChange(halfYear, yearEnd),
                delta: delta
            });
        }

        return {
            dstChanges: result,
            baseOffset: baseOffset
        };

        //Бинарный поиск момента времени, когда происходит перевод часов

        function findOffsetChange(dateStart, dateEnd) {
            var result, timeStart, timeEnd, middleDate, middleTime;
            if (getUtcOffset(dateStart) === getUtcOffset(dateEnd))
                return null;

            timeStart = dateStart.getTime();
            timeEnd = dateEnd.getTime();
            while (1 < timeEnd - timeStart) {
                middleDate = new Date();
                middleTime = Math.floor((timeEnd - timeStart) / 2 + timeStart);
                middleDate.setTime(middleTime);
                if (getUtcOffset(middleDate) === getUtcOffset(dateEnd)) {
                    dateEnd = middleDate;
                    timeEnd = middleTime;
                } else {
                    dateStart = middleDate;
                    timeStart = middleTime;
                }
            }
            result = new Date();
            result.setTime(timeEnd - (timeEnd % 60));
            return result;
        }

        function getUtcOffset(date) {
            return -date.getTimezoneOffset();
        }
    };

    a.getRegionalDateOptions = function () {
        return {
            tz: -Math.floor(new Date().getTimezoneOffset() / 60),
            dst: a.getLastDaylightSavingChanges(3)
        };
    };

    a.toQueryString = function (o) {
        var sb = new aBuffer(),
                first = true,
                key;
        for (key in o) {
            if (first)
                first = false;
            else
                sb.add('&');

            sb.add(key);
            sb.add('=');
            sb.add(escape(JSON.stringify(o[key])));
        }
        return sb.toString();
    };


    /**
     change elements visible method with click on other element (using in corporative - equipment)
     */
    a.changeVisible = function (link, chanegedEl, msg) {
        link.click(function () {
            if ($(this).attr('data-clicked')) {
                chanegedEl.addClass('hidden');
                $(this).removeAttr('data-clicked');
                if (msg)
                    $(this).find('span').html($.messages.contentMessages.showBlock);

            } else {
                chanegedEl.removeClass('hidden');

                $(this).attr('data-clicked', 'true');



                $('html, body').animate({
                    scrollTop: $(this).parents('.show_hide_spoiler_parent').offset().top
                }, 1000);
                if (msg)
                    $(this).find('span').html($.messages.contentMessages.hideBlock);
            }

            return false;
        });
    };

    /**
     ajax GET response method. only GET response
     */

    a.ajaxGetResponse = function (url, sFunction, sForm, redirect) {
        $.ajax({
            method: "GET",
            url: mapPath + url,
            success: function (data) {

                sFunction(data, redirect);
                if (sForm)
                    $.enableButton(sForm.find('*[type="submit"]'));
            },
            beforeSend: function () {

                if (sForm)
                    $.disableButton(sForm.find('*[type="submit"]'));
            },
            error: function () {
            },
            cache: false
        });
    };



    /**
     ************************************************* ajax success methods ****************************************************
     */

    /**
     logout ajax method
     */
    a.logout = function (data) {
        if (data && data.status === "true") {
            a.clearLocalStorage();
            $('#loginForm').show();
            $('.bl-loggedUser.cloned').remove();

        }
    };

    /**
     clear local storage with login info method
     */
    a.clearLocalStorage = function () {
        localStorage.removeItem('isLogged');
        localStorage.removeItem('profileLink');
        localStorage.removeItem('userName');
        localStorage.removeItem('userPhoto');
        localStorage.removeItem('contactPhone');
        localStorage.removeItem('contactEmail');
    };

    /**
     check user session method
     */
    a.isAlive = function (data) {
        a.clearLocalStorage();
        if (data && data[0] === true) {
            a.ajaxGetResponse("login.aspx?a=getmyinfo", a.getUserInfo, false);
        }
    };

    /**
     get user info from response and create local storage login items method
     */

    a.getUserInfo = function (response, redirect) {
        var userName,
                contactPhone,
                contactEmail,
                userPhoto;

        userName = response.userName;
        contactPhone = response.contactPhone;
        contactEmail = response.contactEmail;
        userPhoto = response.userPhoto;

        localStorage.setItem('isLogged', true);
        localStorage.setItem('userName', userName);
        localStorage.setItem('userPhoto', userPhoto);

        if (contactPhone && contactPhone !== 'null') {
            localStorage.setItem('contactPhone', contactPhone);
        } else if (contactEmail && contactEmail !== 'null') {
            localStorage.setItem('contactEmail', contactEmail);
        }

        if (redirect) {
            location.href = redirect;
        }
    };

    /**
     fill user from from local storage method
     */

    a.fillUserForm = function () {
        $('.bl-loggedUser').clone().addClass('cloned').appendTo('.bl-login');
        var response,
                userName,
                contactPhone,
                contactEmail,
                userPhoto;


        userName = localStorage.getItem('userName');
        contactPhone = localStorage.getItem('contactPhone');
        contactEmail = localStorage.getItem('contactEmail');
        userPhoto = localStorage.getItem('userPhoto');

        var photo = userPhoto && userPhoto !== 'default' ? userPhoto : a.applicationPath + '/includes/sources/images/ava2.jpg';

        $('.bl-loggedUser.cloned .bl-photo img').attr('src', photo).attr('alt', userName).attr('title', userName);


        $('.bl-loggedUser.cloned .bl-info span.uName').substrUserInfo({value: userName});

        if (contactPhone) {
            $('.bl-loggedUser.cloned .bl-info p').substrUserInfo({
                text: $.messages.loggedUser.phone,
                value: contactPhone,
                innerSpan: 'uContactInfo'
            });
        } else if (contactEmail) {
            $('.bl-loggedUser.cloned .bl-info p').substrUserInfo({
                text: $.messages.loggedUser.email,
                value: contactEmail,
                innerSpan: 'uContactInfo',
                count: 16
            });

        } else {
            $('.bl-loggedUser.cloned .bl-info p').remove();
        }
        $('.bl-loggedUser.cloned').animate({
            opacity: 1
        }, 0, 'linear');

        $('#loginForm').hide();
        $('.loading').hide();
        $(".logout").off('click');
        $(".logout").on('click', function () {
            a.ajaxGetResponse("login.aspx?a=logoutajax", a.logout, $(this));
            return false;
        });
    };


    /**
     ************************************************* form methods ****************************************************
     */

    /**
     submit someone (all) form method
     */
    a.formSubmit = function (sForm, fType) {
        var email,
                err,
                mWindow = $($.htmlTags.mWindow).appendTo('body');
        $('p.error').remove();
        if (fType === 'passRecoverForm')
            email = sForm.find('input[type="email"]').val();
        if (fType !== 'loginForm')
            err = $($.htmlTags.p.err).prependTo(sForm);


        $.ajax({
            url: mapPath + sForm.attr('action'),
            method: "POST",
            data: sForm.serialize(),
            success: function (data) {
                if (fType === 'loginForm') {
                    /* only login form */
                    if (!data) {
                        data = {value: {result: "fail"}, messages: [{messageTitle: $.messages.error}]};
                    }
                    
                    if (data.result === 'Success') {
                        localStorage.setItem('isLogged', true);
                        a.ajaxGetResponse("login.aspx?a=getmyinfo", a.getUserInfo, false, mapPath + data.redirectUrl);
                    } else {
                        $('div.error').html(data.messages[0].messageTitle).show();
                    }
                } else if (fType === 'passRecoverForm') {
                    /* only password recover form */
                    err.empty().hide();
                    if (data.toString() === '[true]') {

                        sForm.after($.messages.passwordRecoverResult.success + email);
                        sForm.remove();
                    } else {
                        /* other forms */
                        err.empty().hide();

                        err.append($.messages.passwordRecoverResult.fail + email).show();
                    }
                } else {
                    err.empty().hide();
                    sForm.after($.messages.questSupportConfirm);
                    sForm.remove();
                }
                $.enableButton(sForm.find('*[type="submit"]'));

            },
            error: function () {
                err.empty().hide();
                err.html($.messages.formSubmitResult.failAjax).show();
            },
            beforeSend: function () {
                $.disableButton(sForm.find('*[type="submit"]'));
            }

        });
        return false;
    };

    /**
     render all form
     */

    a.renderForm = function (sForm, fType) {

        if (fType === 'loginForm') {
            /* only login form check password and login */
            var inputLogin = sForm.find('input[type="text"]');
            //inputLogin.focus();
            if (inputLogin.val() && inputLogin.val() !== '')
                sForm.find('input#password').focus();

            sForm.submit(function () {
                a.formSubmit(sForm, fType);
                return false;
            });

            if (window.localStorage) {
                var departmentId = localStorage.getItem("SelectedDepartmentId");
                $('#departmentId').val(departmentId);
            }


        } else {
            /* other forms */
            sForm.find('input, textarea').on('keyup', function () {
                a.validateForm(sForm);
            }).on('change', function () {
                a.validateForm(sForm);
            }).on('focusout', function () {
                a.validateForm(sForm);
            }).on('click', function () {
                a.validateForm(sForm);
            });
            sForm.find('input, textbox').each(function () {
                a.validateForm(sForm);
            });



            /* */
            if (sForm.find('input[type="file"]').length > 0) {
                sForm.ajaxForm({
                    beforeSend: function () {
                        a.validateForm(sForm);
                    },
                    success: function () {
                        sForm.after($.messages.questSupportConfirm);
                        sForm.remove();
                        console.log('Form submitted');
                    }
                });

            } else {
                sForm.find('*[type="submit"]').click(function () {
                    a.validateForm(sForm);
                    a.formSubmit(sForm, fType);
                    return false;
                });
            }

        }
    };

    /**
     validation form. only for form with required parametrs
     */

    a.validateForm = function (form) {
        var flag = true;
        form.find(' .required').each(function () {
            if ($(this).val() === '') {
                flag = false;
            } else {
                if ($(this).attr('type') === 'tel' && !NIKA_GLONASS.validation.isValidPhoneNumber($(this).val())) {
                    flag = false;
                }
                if ($(this).attr('type') === 'email' && !NIKA_GLONASS.validation.isValidEmailAddress($(this).val())) {
                    flag = false;
                }
            }

        });
        if (flag) {
            $.enableButton(form.find('*[type="submit"]'));
        } else {
            $.disableButton(form.find('*[type="submit"]'));
        }
    };

    a.getContactInfo = function (type, callback) {
        var params = {
            a: 'GetContacts',
            contactType: type
        };
        $.ajax({
            url: mapPath + 'services/settings.aspx',
            method: 'POST',
            data: params,
            success: function (response) {
                var contactData = '';
                for (var r = 0; r < response.length; r++) {
                    if (response[r].isConfirmed === true) {
                        contactData += response[r].value.toString() + '; ';
                    }
                }
                if ($('#client' + type).val() === '')
                    $('#client' + type).val($('#client' + type).val() + contactData);

                $('#clientInfo').val($('#clientInfo').val() + contactData);

                if (callback)
                    callback();
            },
            dataType: 'json'
        });

    };

    $(document).ready(function () {

        $.ajaxSetup({
            async: false
        });

        var regionalDateOptions = a.getRegionalDateOptions(),
                regionalDateOptionsString;

        var tooltipped = new Tooltipped({link: $('.hint')});
        tooltipped.init();

        $('a.hint').click(function () {
            return false;
        });
        /**
         check information about user
         */

        a.ajaxGetResponse("map.aspx?a=isalive", a.isAlive, false);

        if (localStorage.getItem('isLogged')) {
            a.fillUserForm();
        } else {
            $('.loading').hide();
            $('#loginForm').show();
        }

        if ($('.options').size() > 0) {
            var hash = location.hash.substring(1);
            $('html, body').animate({
                scrollTop: $('*[name="' + hash + '"]').offset().top
            }, 2000);
            $('*[name="' + hash + '"] h4').css('color', '#f00');
        }


        for (var key in regionalDateOptions)
            $('#' + key).val(JSON.stringify(regionalDateOptions[key]));

        regionalDateOptionsString = a.toQueryString(a.getRegionalDateOptions());

        $('.demoLoginLink').each(function (index, value) {
            value.href += '&' + regionalDateOptionsString;
        });

        /**
         render all forms on the page
         */
        $('form').each(function () {
            var _this = $(this);
            /* insert contact data if user logged on*/

            a.renderForm($(this), $(this).attr('data-fType'));

        });

        /**
         find elements with changed visible
         */

        if ($('.show_hide').size() > 0) {
            $('.show_hide').each(function () {
                a.changeVisible($(this), $(this).parent().find('table'), true);
            });
            $('.list').click(function () {
                $('.hidden').removeClass('hidden');
                $('.show_hide').attr('clicked', 'true');

                if (!$(this).hasClass('for_spoiler')) {
                    $('.show_hide').find('span').html($.messages.contentMessages.hideBlock);
                    $("html, body").animate({scrollTop: $(this).offset().top}, "slow");

                }
                return false;
            });

        }

        if ($('.show_hide_spoiler').size() > 0) {
            $('.show_hide_spoiler').each(function () {
                var sib = $(this).parents('div').eq(0).find('.spoiler');
                a.changeVisible($(this), sib, false);

            });

        }







        /*if ($('#need-to-control-fuel-level-checkbox').size() > 0) {
         if ($('#need-to-control-fuel-level-checkbox').prop("checked")) {
         $('.transport-type').removeClass('hidden');
         } else {
         $('.transport-type').addClass('hidden');
         }
         $('#need-to-control-fuel-level-checkbox').change(function () {
         if ($(this).prop("checked")) {
         $('.transport-type').removeClass('hidden');
         } else {
         $('.transport-type').addClass('hidden');
         $('.transport-type input').removeAttr('checked');
         }
         });
         }*/

    });


}(NIKA_GLONASS || {}, jQuery));
