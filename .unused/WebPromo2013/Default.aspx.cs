﻿namespace RU.NVG.Web.WebPromo2013
{
    public partial class Default : BasePage
    {
        /// <summary>
        /// AS: Обработка по умолчанию
        /// </summary>
        [PageAction("default")]
        public void DefaultProcess()
        {
            RenderLoginPage();
        }
    }
}
