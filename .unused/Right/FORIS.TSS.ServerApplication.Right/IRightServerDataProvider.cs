using FORIS.TSS.ServerApplication.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Right.Data.Right;

namespace FORIS.TSS.ServerApplication.Right
{
	public interface IRightServerDataProvider:
		IDatabaseProvider
	{
		OperatorServerData PrincipalData { get; }

		RightServerData RightData { get; }
	}
}
