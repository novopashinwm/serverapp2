using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right;
using System.Runtime.Remoting.Lifetime;
using System;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	/// <summary>
	/// ��������� ������ ��� ������ "�����"
	/// </summary>
	/// <remarks>
	/// ������ ����� �������� ������ ���������,
	/// ��� ������������� - ����������� ������,
	/// ���������� �������� � �.�. � �.�.
	/// 
	/// ������ ���� ����� ������ ������������
	/// ���������� ������ ��������������� � ��
	/// </remarks>
	public class RightServerData:
		RightData
	{
		#region Constructor & Dispose

		/// <summary>
		/// ����������� ��������� ������ � ��������� �������
		/// </summary>
		public RightServerData(
			IRightServerDataProvider serverDataProvider
			)
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new RightDatabaseDataSupplier( this.serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				RemotingServices.Disconnect( this );

				this.Destroy();
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// ���������� � ������� DataTreater
		/// ��� ���������� ������ � ��
		/// </summary>
		private readonly IRightServerDataProvider serverDataProvider;

		/// <summary>
		/// ������� ����� ������ DataTreater
		/// </summary>
		/// <param name="sessionInfo"></param>
		/// <returns>������ RightDataTreater</returns>
		public IRightDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new RightDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		[Obsolete("", false)]
		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			return lease;
		}
	}
}