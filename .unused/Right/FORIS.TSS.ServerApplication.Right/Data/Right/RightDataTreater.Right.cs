using System;
using System.Threading;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right.Security;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	partial class RightDataTreater
	{
		public void RightInsert( 
			string name,
			string description,
			string rightName
			)
		{
			#region Precondition

/**
			if (!(Thread.CurrentPrincipal is TssPrincipalInfo))
			{
				throw new SecurityException(
					"Thread principal type must be of type TssPrincipalInfo"
					);
			}

			TssPrincipalInfo currentPrincipal = (TssPrincipalInfo)Thread.CurrentPrincipal;

			if( currentPrincipal.PrincipalType != TssPrincipalType.Operator )
			{
				throw new SecurityException(
					"Operator only can be thread's current principal"
					);
			}
/**/

			#endregion // Precondition

			RightRow rightRow;

			lock ( this.Data )
			{
				try
				{
					rightRow = this.Data.Right.NewRow();

					rightRow.Name = name;
					rightRow.Description = description;
					rightRow.RightName = rightName;

					this.Data.Right.Rows.Add( rightRow );

					this.AcceptChanges();
				}
				catch ( Exception ex )
				{
					this.RejectChanges();

					throw;
				}

				//return;

				//Thread.Sleep( 5000 );

				#region Security

				/* ��������� ������ �������� ������������ 
				 * Read � AccessControl, �� �������, 
				 * ������� ������ ��� ������
				 */

				lock ( this.Data.SecurityData )
				{
					try
					{
						RightOperatorRow rightOperatorRow =
							this.Data.SecurityData.RightOperator.NewRow();

						rightOperatorRow.Right = rightRow.ID;
						rightOperatorRow.Operator = this.SessionInfo.OperatorInfo.OperatorID;
						rightOperatorRow.AllowMask =
							(int)RightRights.Read |
							(int)RightRights.AccessControl;
						rightOperatorRow.DenyMask = 0;

						this.Data.SecurityData.RightOperator.Rows.Add( rightOperatorRow );

						this.Data.SecurityData.AcceptChanges( Guid.NewGuid(), this.databaseDataSupplier );
					}
					catch ( Exception ex )
					{
						this.Data.SecurityData.RejectChanges();

						throw;
					}
				}

				#endregion // Security
			}
		}

		public void RightUpdate( 
			int rightId, 
			string name,
			string description,
			string rightName
			)
		{
			lock( this.Data )
			{
				try
				{
					RightRow rightRow = this.Data.Right.FindRow( rightId );

					rightRow.BeginEdit();

					rightRow.Name = name;
					rightRow.Description = description;
					rightRow.RightName = rightName;

					rightRow.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void RightDelete( int rightId )
		{
			lock( this.Data )
			{
				RightRow rightRow = this.Data.Right.FindRow( rightId );

				#region Preconditions

				if( rightRow == null )
				{
					throw new ApplicationException( 
						"Right row does not exist"
						);
				}

				#endregion // Preconditions

				#region Security

				/* ����� ��������� ������� ���������� 
				 * ������� ��� �������� �� ���� ����������
				 */

				lock( this.Data.SecurityData )
				{
					try
					{
						rightRow.ClearAcl();

						this.Data.SecurityData.AcceptChanges( new Guid(), this.databaseDataSupplier );
					}
					catch( Exception )
					{
						this.RejectChanges();

						throw;
					}
				}

				#endregion // Security

				try
				{
					rightRow.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}