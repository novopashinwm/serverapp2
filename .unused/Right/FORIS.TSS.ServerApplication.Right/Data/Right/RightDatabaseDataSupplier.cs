using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	public class RightDatabaseDataSupplier:
		DatabaseDataSupplier<IRightServerDataProvider>,
		IRightDataSupplier
	{
		public RightDatabaseDataSupplier( IRightServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{
			this.rightSecurityDatabaseDataSupplier =
				new RightSecurityDatabaseDataSupplier( databaseServerProvider );
		}

		private readonly RightSecurityDatabaseDataSupplier rightSecurityDatabaseDataSupplier;

		public IRightSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.rightSecurityDatabaseDataSupplier; }
		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerRight",
					new string[] { "RIGHT" }
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IRightDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}