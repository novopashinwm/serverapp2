using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Right.Data.Right;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	public partial class RightDataTreater:
		DataTreater<IRightData>,
		IRightDataTreater
	{
		public RightDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IRightData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{
		}
	}
}