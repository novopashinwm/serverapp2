using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	public class RightSecurityDatabaseDataSupplier:
		DatabaseDataSupplier<IRightServerDataProvider>,
		IRightSecurityDataSupplier
	{
		public RightSecurityDatabaseDataSupplier( IRightServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{

		}

		protected override FORIS.TSS.BusinessLogic.Data.DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerRightSecurity",
					new string[]
						{
							"R_RIGHT_OPERATORGROUP",
							"R_RIGHT_OPERATOR"
						}
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		IOperatorDataSupplier ISecurityDataSupplier<IOperatorDataSupplier>.PrincipalDataSupplier
		{
			get { return this.DatabaseProvider.PrincipalData; }
		}

		public IRightSecurityDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}
	}
}
