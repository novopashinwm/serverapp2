using System.Collections.Generic;
using System.Data;
using System.Threading;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.ServerApplication.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication.Right.Data.Right
{
	public class RightPersonalDataSupplier:
		PersonalDataSupplier<RightServerData, IRightDataTreater>,
		IRightDataSupplier
	{
		public RightPersonalDataSupplier(
			RightServerData dataSupplier,
			IPersonalServer personalServer
			)
			: base( dataSupplier, personalServer )
		{

		}

		public IRightSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.DataSupplier.SecurityDataSupplier; }
		}

		public override IRightDataTreater GetDataTreater()
		{
			return
				this.DataSupplier.GetTreater( this.PersonalServer.SessionInfo );
		}

		protected override DataInfo GetDataInfo()
		{
			DataInfo dataInfo = base.GetDataInfo();

            
            List<DataRow> victims = new List<DataRow>();

            foreach (DataRow dataRow in dataInfo.DataSet.Tables["RIGHT"].Rows)
            {
                RightRow rightRow = this.DataSupplier.Right.FindRow((int)dataRow["RIGHT_ID"]);

                if (!rightRow.TestRight((GeneralRights)RightRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataInfo.DataSet.Tables["RIGHT"].Rows.Remove(dataRow);
            }

			return dataInfo;
		}

		protected override void dataSupplier_DataChanged(DataChangedEventArgs e)
		{
			DataChangedEventArgs dataChangedEventArgs = e;

            List<DataRow> victims = new List<DataRow>();

            #region Added

            foreach (DataRow dataRow in dataChangedEventArgs.Added.Tables["RIGHT"].Rows)
            {
                RightRow rightRow = this.DataSupplier.Right.FindRow((int)dataRow["RIGHT_ID"]);

                if (!rightRow.TestRight((GeneralRights)RightRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Added.Tables["RIGHT"].Rows.Remove(dataRow);
            }

            #endregion  // Added

            #region Deleted

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Deleted.Tables["RIGHT"].Rows)
            {
                RightRow rightRow = this.DataSupplier.Right.FindRow((int)dataRow["RIGHT_ID"]);

                if (!rightRow.TestRight((GeneralRights)RightRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Deleted.Tables["RIGHT"].Rows.Remove(dataRow);
            }

            #endregion  // Deleted

            #region Modified

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Modified.Tables["RIGHT"].Rows)
            {
                RightRow rightRow = this.DataSupplier.Right.FindRow((int)dataRow["RIGHT_ID"]);

                if (!rightRow.TestRight((GeneralRights)RightRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Modified.Tables["RIGHT"].Rows.Remove(dataRow);
            }

            #endregion  // Modified

            #region Modified

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Modified.Tables["RIGHT"].Rows)
            {
                RightRow rightRow = this.DataSupplier.Right.FindRow((int)dataRow["RIGHT_ID"]);

                if (!rightRow.TestRight((GeneralRights)RightRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Modified.Tables["RIGHT"].Rows.Remove(dataRow);
            }

            #endregion  // Modified

			base.dataSupplier_DataChanged( dataChangedEventArgs );
		}
	}
}