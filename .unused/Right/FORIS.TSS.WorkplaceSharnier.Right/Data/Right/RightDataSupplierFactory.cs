using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Right;
using FORIS.TSS.WorkplaceSharnier.Right.Data.Right;

namespace FORIS.TSS.WorkplaceSharnier.Right.Data.Right
{
	public class RightDataSupplierFactory:
		ClientDataSupplierFactory<IRightClientDataProvider, RightClientData, DataParams>
	{
		public RightDataSupplierFactory( IRightClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override RightClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			RightClientData Supplier =new RightClientData( this.Client );

			return Supplier;
		}
	}
}