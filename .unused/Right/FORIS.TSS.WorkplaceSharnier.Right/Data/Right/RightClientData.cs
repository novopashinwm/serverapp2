using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right;

namespace FORIS.TSS.WorkplaceSharnier.Right.Data.Right
{
	/// <summary>
	/// ��������� ������ ������� �������
	/// ������ "������� ������"
	/// </summary>
	public class RightClientData:
		RightData
	{
		#region Constructor & Dispose

		public RightClientData( IRightClientDataProvider clientProvider )
		{
			this.dataSupplier = new RightClientDataSupplier( clientProvider );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// ������������ ��� ��������� ������ ������� DataTreater
		/// </summary>
		private readonly IRightDataSupplier dataSupplier;
	}
}