using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Right;

namespace FORIS.TSS.WorkplaceSharnier.Right.Data.Right
{
	internal class RightClientDataSupplier:
		ClientDataSupplier<IRightClientDataProvider, IRightDataSupplier, IRightDataTreater>,
		IRightDataSupplier
	{
		public RightClientDataSupplier( IRightClientDataProvider clientProvider )
			: base( clientProvider, clientProvider.GetRightDataSupplier() )
		{

		}

		public IRightSecurityDataSupplier SecurityDataSupplier
		{
			get
			{
				return
					new RightSecurityClientDataSupplier(
						this.ClientProvider,
						this.DataSupplier.SecurityDataSupplier
						);
				}
		}
	}

	internal class RightSecurityClientDataSupplier :
		ClientDataSupplier<IRightClientDataProvider, IRightSecurityDataSupplier, IRightSecurityDataTreater>,
		IRightSecurityDataSupplier
	{
		public RightSecurityClientDataSupplier(
			IRightClientDataProvider clientProvider,
			IRightSecurityDataSupplier dataSupplier
			)
			: base( clientProvider, dataSupplier )
		{
		}

		public IOperatorDataSupplier PrincipalDataSupplier
		{
		    get { return this.ClientProvider.GetOperatorClientData(); }		}
	}
}