using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Right.Data.Right;
using FORIS.TSS.BusinessLogic.Right.Data;

namespace FORIS.TSS.WorkplaceSharnier.Right
{
	public interface IRightClientDataProvider:
		IClientDataProvider,
		IOperatorClientDataProvider,
		IRightDataSupplierProvider
	{
		RightClientData GetRightClientData();

		ISecurityEditor SecurityEditor { get; }
	}
}
