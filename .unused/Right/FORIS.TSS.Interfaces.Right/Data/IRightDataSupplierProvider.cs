using FORIS.TSS.BusinessLogic.Right.Data.Right;

namespace FORIS.TSS.BusinessLogic.Right.Data
{
	public interface IRightDataSupplierProvider
	{
		IRightDataSupplier GetRightDataSupplier();
	}
}