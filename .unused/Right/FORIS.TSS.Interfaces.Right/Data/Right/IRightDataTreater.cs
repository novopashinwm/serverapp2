using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.BusinessLogic.Right.Data.Right
{
	public interface IRightDataTreater:
		IDataTreater
	{
		#region Right

		void RightInsert(
			string name,
			string description,
			string rightName
			);

		void RightUpdate(
			int rightId,
			string name,
			string description,
			string rightName
			);

		void RightDelete( 
			int rightId 
			);

		#endregion // Right
	}

	public interface IRightSecurityDataTreater:
		IDataTreater
	{
		
	}
}
