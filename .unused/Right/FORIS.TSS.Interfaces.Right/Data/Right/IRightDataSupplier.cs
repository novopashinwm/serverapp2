using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.BusinessLogic.Right.Data.Right
{
	public interface IRightDataSupplier:
		IDataSupplier<IRightDataTreater>,
		ISecureDataSupplier<IRightSecurityDataSupplier, IOperatorDataSupplier>
	{

	}

	public interface IRightSecurityDataSupplier:
		IDataSupplier<IRightSecurityDataTreater>,
		ISecurityDataSupplier<IOperatorDataSupplier>
	{
		
	}
}
