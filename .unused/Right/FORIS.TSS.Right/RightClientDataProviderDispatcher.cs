using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Right;

namespace FORIS.TSS.Right
{
	class RightClientDataProviderDispatcher:
		ClientDispatcher<IRightClientDataProvider>
	{
		public RightClientDataProviderDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public RightClientDataProviderDispatcher()
		{

		}

		private readonly RightClientDataProviderAmbassadorCollection ambassadors =
			new RightClientDataProviderAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new RightClientDataProviderAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IClientItem<IRightClientDataProvider>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
	}

	class RightClientDataProviderAmbassadorCollection:
		ClientAmbassadorCollection<IRightClientDataProvider>
	{
		public new RightClientDataProviderAmbassador this[int index]
		{
			get { return (RightClientDataProviderAmbassador)base[index]; }
		}
	}

	class RightClientDataProviderAmbassador:
		ClientAmbassador<IRightClientDataProvider>
	{

	}
}