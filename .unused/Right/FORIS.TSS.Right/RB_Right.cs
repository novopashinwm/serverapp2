using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.WorkplaceShadow;
using FORIS.TSS.WorkplaceShadow.Right.Data.Right;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Right;
using FORIS.TSS.WorkplaceShadow.Right.Controls.Right;

namespace FORIS.TSS.Right
{
	public class RB_Right:
		PluginForm,
		IClientItem<IRightClientDataProvider>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private RightClientDataProviderDispatcher clientDispatcher;
		private RightDataSupplier rightDataSupplier;
		private RightData rightData;
		private RightClientDataProviderAmbassador caRightDataSupplier;
		private RightTableControl ctrlRightTable;
		private RightDataDispatcher dataDispatcher;
		private RightDataAmbassador daRightEditorProvider;
		private RightDataAmbassador daRightTableControl;
		private RightClientDataProviderAmbassador caRightTableControl;
		private RightEditorProvider rightEditorProvider;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public RB_Right()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Forms Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.clientDispatcher = new FORIS.TSS.Right.RightClientDataProviderDispatcher( this.components );
			this.caRightDataSupplier = new FORIS.TSS.Right.RightClientDataProviderAmbassador();
			this.rightDataSupplier = new FORIS.TSS.WorkplaceShadow.Right.Data.Right.RightDataSupplier( this.components );
			this.rightData = new FORIS.TSS.Helpers.Right.Data.Right.RightData( this.components );
			this.dataDispatcher = new FORIS.TSS.Helpers.Right.Data.Right.RightDataDispatcher( this.components );
			this.daRightEditorProvider = new FORIS.TSS.Helpers.Right.Data.Right.RightDataAmbassador();
			this.rightEditorProvider = new FORIS.TSS.WorkplaceShadow.Right.Controls.Right.RightEditorProvider( this.components );
			this.daRightTableControl = new FORIS.TSS.Helpers.Right.Data.Right.RightDataAmbassador();
			this.ctrlRightTable = new FORIS.TSS.WorkplaceShadow.Right.Controls.Right.RightTableControl();
			this.caRightTableControl = new FORIS.TSS.Right.RightClientDataProviderAmbassador();
			this.SuspendLayout();
			// 
			// clientDispatcher
			// 
			this.clientDispatcher.Ambassadors.Add( this.caRightDataSupplier );
			this.clientDispatcher.Ambassadors.Add( this.caRightTableControl );
			// 
			// caRightDataSupplier
			// 
			this.caRightDataSupplier.Item = this.rightDataSupplier;
			// 
			// rightData
			// 
			this.rightData.DataSupplier = this.rightDataSupplier;
			this.rightData.InvokeBroker = this.invokeBroker;
			this.rightData.Loaded += new System.EventHandler( this.rightData_Loaded );
			this.rightData.Loading += new System.EventHandler( this.rightData_Loading );
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.Ambassadors.Add( this.daRightEditorProvider );
			this.dataDispatcher.Ambassadors.Add( this.daRightTableControl );
			// 
			// daRightEditorProvider
			// 
			this.daRightEditorProvider.Item = this.rightEditorProvider;
			// 
			// daRightTableControl
			// 
			this.daRightTableControl.Item = this.ctrlRightTable;
			// 
			// ctrlRightTable
			// 
			this.ctrlRightTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctrlRightTable.EditorProvider = this.rightEditorProvider;
			this.ctrlRightTable.Location = new System.Drawing.Point( 0, 0 );
			this.ctrlRightTable.Name = "ctrlRightTable";
			this.ctrlRightTable.Size = new System.Drawing.Size( 641, 381 );
			this.ctrlRightTable.TabIndex = 0;
			// 
			// caRightTableControl
			// 
			this.caRightTableControl.Item = this.ctrlRightTable;
			// 
			// RB_Right
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.ClientSize = new System.Drawing.Size( 641, 381 );
			this.Controls.Add( this.ctrlRightTable );
			this.Name = "RB_Right";
			this.ResumeLayout( false );

		}

		#endregion // Windows Forms Designer generated code

		#region IClientItem<IRightClientDataProvider> Members

		public IRightClientDataProvider Client
		{
			get { return this.clientDispatcher.Client; }
			set { this.clientDispatcher.Client = value; }
		}

		#endregion // IClientItem<IRightClientDataProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IRightClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		#region Handle rightData events

		private void rightData_Loading( object sender, System.EventArgs e )
		{
			this.dataDispatcher.Data = null;
		}

		private void rightData_Loaded( object sender, System.EventArgs e )
		{
			this.dataDispatcher.Data = this.rightData;
		}

		#endregion // Handle rightData events
	}
}
