using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightPropertiesForm: 
		TssUserForm,
		IDataItem<IRightData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components;
		private RightDataDispatcher dataDispatcher;
		private TableLayoutPanel tableLayoutPanelMain;
		private Label lblName;
		private TextBox edtDescription;
		private Label lblDescription;
		private Label lblRightName;
		private TextBox edtRightName;
		private TextBox edtName;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public RightPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( RightPropertiesForm ) );
			this.dataDispatcher = new FORIS.TSS.Helpers.Right.Data.Right.RightDataDispatcher( this.components );
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblName = new System.Windows.Forms.Label();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblRightName = new System.Windows.Forms.Label();
			this.edtRightName = new System.Windows.Forms.TextBox();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			resources.ApplyResources( this.panelFill, "panelFill" );
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.UpdateView += new System.EventHandler( this.dataDispatcher_UpdateView );
			this.dataDispatcher.BuildView += new System.EventHandler( this.dataDispatcher_BuildView );
			this.dataDispatcher.AfterSetData += new System.EventHandler( this.dataDispatcher_AfterSetData );
			this.dataDispatcher.DestroyView += new System.EventHandler( this.dataDispatcher_DestroyView );
			this.dataDispatcher.BeforeSetData += new System.EventHandler( this.dataDispatcher_BeforeSetData );
			// 
			// tableLayoutPanelMain
			// 
			resources.ApplyResources( this.tableLayoutPanelMain, "tableLayoutPanelMain" );
			this.tableLayoutPanelMain.Controls.Add( this.lblName, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.edtDescription, 1, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.lblDescription, 0, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.edtName, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.lblRightName, 0, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.edtRightName, 1, 2 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblName
			// 
			resources.ApplyResources( this.lblName, "lblName" );
			this.lblName.Name = "lblName";
			// 
			// edtDescription
			// 
			resources.ApplyResources( this.edtDescription, "edtDescription" );
			this.edtDescription.Name = "edtDescription";
			// 
			// lblDescription
			// 
			resources.ApplyResources( this.lblDescription, "lblDescription" );
			this.lblDescription.Name = "lblDescription";
			// 
			// edtName
			// 
			resources.ApplyResources( this.edtName, "edtName" );
			this.edtName.Name = "edtName";
			// 
			// lblRightName
			// 
			resources.ApplyResources( this.lblRightName, "lblRightName" );
			this.lblRightName.Name = "lblRightName";
			// 
			// edtRightName
			// 
			resources.ApplyResources( this.edtRightName, "edtRightName" );
			this.edtRightName.Name = "edtRightName";
			// 
			// RightPropertiesForm
			// 
			resources.ApplyResources( this, "$this" );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "RightPropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.RightPropertiesForm_FormClosing );
			this.Controls.SetChildIndex( this.panelFill, 0 );
			this.Controls.SetChildIndex( this.tableLayoutPanelMain, 0 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		#region IDataItem<IRightData> Members

		public IRightData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if (this.dataDispatcher.Data != null)
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if (this.dataDispatcher.Data != null)
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IRightData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idRight;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdRight
		{
			get { return this.idRight; }
			set
			{
				if( this.idRight != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idRight = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public string RightName
		{
			get { return this.edtName.Text; }
		}

		public string Description
		{
			get { return this.edtDescription.Text; }
		}

		public string RightRightName
		{
			get { return this.edtRightName.Text; }
		}

		#endregion // Properties

		#region Data

		private RightRow rowRight;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowRight != null )
			{
				this.rowRight.AfterChange -=  rowRight_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowRight != null )
			{
				this.rowRight.AfterChange += rowRight_AfterChange;
			}
		}

		private void rowRight_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowRight = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowRight = this.Data.Right.FindRow( this.idRight );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:

					if( this.rowRight != null )
					{
						this.edtName.Text = this.rowRight.Name;
						this.edtDescription.Text = this.rowRight.Description;
						this.edtRightName.Text = this.rowRight.RightName;
					}

					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.edtName.Text = string.Empty;
					this.edtDescription.Text = string.Empty;
					this.edtRightName.Text = string.Empty;

					break;

					#endregion // New
			}
		}

		#endregion // View

		#region Handle dataDispatcher events

		private void dataDispatcher_BeforeSetData( object sender, EventArgs e )
		{
			this.OnBeforeSetData();
		}

		private void dataDispatcher_AfterSetData( object sender, EventArgs e )
		{
			this.OnAfterSetData();
		}

		private void dataDispatcher_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}

		private void dataDispatcher_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}

		private void dataDispatcher_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle dataDispatcher events

		#region Handle controls' events

		private void RightPropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( "Right name required");
				}

				if( this.edtDescription.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( "Right description required" );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						"Data error(s):" + "\r\n" +
						Warnings.ToString()
						);
				}
			}
		}

		#endregion // Handle controls' events
	}
}