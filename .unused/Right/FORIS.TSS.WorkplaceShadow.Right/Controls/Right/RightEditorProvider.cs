using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightEditorProvider:
		EditorProvider<IRightDataTreater, IRightData>,
		IRightEditorProvider
	{
		public RightEditorProvider()
		{
			
		}
		public RightEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		protected override void New()
		{
			using( RightPropertiesForm propertiesForm = new RightPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRightDataTreater Treater =
							((IRightDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.RightInsert(
								propertiesForm.RightName,
								propertiesForm.Description,
								propertiesForm.RightRightName
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not create new Right" +
						"Exception: " + Ex.Message
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Properties( int idRight )
		{
			using( RightPropertiesForm propertiesForm = new RightPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.Edit;
				propertiesForm.IdRight = idRight;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IRightDataTreater Treater =
							( (IRightDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.RightUpdate(
								idRight,
								propertiesForm.RightName,
								propertiesForm.Description,
								propertiesForm.RightRightName
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not update Right" +
						"Exception: " + Ex.Message
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
		}

		protected override void Remove( int idRight )
		{
			if(
				MessageBox.Show(
					"Are you sure to delete Right",
					"Warning",
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IRightDataTreater Treater =
						( (IRightDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.RightDelete( idRight );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not delete Right" + "\r\n" +
						"Exception: " + Ex.Message
						);
				}
			}
		}

		protected override ISecureObject GetSecureObject( int idRight )
		{
			return this.Data.Right.FindRow( idRight );
		}
	}
}