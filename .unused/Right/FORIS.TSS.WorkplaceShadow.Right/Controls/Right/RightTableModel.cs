using System.ComponentModel;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightTableModel:
		DataTableModel<RightXpRow, RightRow, RightTable, IRightData>
	{
		public RightTableModel( IContainer container )
		{
			container.Add( this );
		}
		public RightTableModel()
		{

		}

		#region Rows

		private readonly RightXpRowCollection rows =
			new RightXpRowCollection();

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new RightXpRowCollection Rows
		{
			get { return this.rows; }
		}

		protected override DataXpRowCollection<RightXpRow, RightRow, RightTable, IRightData> GetRowCollection()
		{
			return this.rows;
		}

		#endregion // Rows

		#region Factory

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new RightXpRowFactory Factory
		{
			get { return (RightXpRowFactory)base.Factory; }
			set { base.Factory = value; }
		}

		#endregion // Factory

		protected override RightTable GetTable()
		{
			return this.Data.Right;
		}
		public class RightXpRowCollection:
			DataXpRowCollection<RightXpRow, RightRow, RightTable, IRightData>
		{

		}
	}
}
