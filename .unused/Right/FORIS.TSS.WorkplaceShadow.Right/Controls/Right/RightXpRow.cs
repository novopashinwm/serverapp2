using System;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightXpRow:
		DataXpRow<RightRow, RightTable, IRightData>
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellDescription;

		#endregion // Cells

		public RightXpRow( int idRight )
			: base( idRight )
		{
			this.cellName = new Cell();
			this.cellDescription = new Cell();

			base.Cells.AddRange(
				new FORIS.TSS.TransportDispatcher.XPTable.Models.Cell[]
					{
						this.cellName,
						this.cellDescription
					}
				);
		}

		protected override RightRow GetRow()
		{
			return this.Data.Right.FindRow( this.Id );
		}

		protected override void OnUpdateView()
		{
			if( this.Row != null )
			{
				this.cellName.Text = this.Row.Name;
				this.cellDescription.Text = this.Row.Description;
			}
			else
			{
				this.cellName.Text = String.Empty;
				this.cellDescription.Text = String.Empty;
			}
		}
	}
}