using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceSharnier.Right;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightTableControl: 
		UserControl,
		IClientItem<IRightClientDataProvider>,
		IDataItem<IRightData>
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private RightDataDispatcher dataDispatcher;
		private CommandManager commandManager;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel columnModel;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn columnCheck;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnName;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn columnDescription;
		private RightDataAmbassador daTableModel;
		private RightXpRowFactory rowFactory;
		private FORIS.TSS.Common.Commands.Command cmdNew;
		private FORIS.TSS.Common.Commands.Command cmdProperties;
		private FORIS.TSS.Common.Commands.Command cmdRemove;
		private FORIS.TSS.Common.Commands.Command cmdCheckAll;
		private FORIS.TSS.Common.Commands.Command cmdUncheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciNewToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciPropertiesToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciRemoveToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciCheckAllContextMenuItem;
		private ToolStripMenuItem tsmiCheckAll;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllToolbarButton;
		private FORIS.TSS.Common.Commands.CommandInstance ciUncheckAllContextMenuItem;
		private ContextMenuStrip cmsTable;
		private ToolStripMenuItem tsmiUncheckAll;
		private FORIS.TSS.TransportDispatcher.XPTable.Models.Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbCheckAll;
		private ToolBarButton tbbUncheckAll;
		private ToolBarButton separator1;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
		private ToolBarButton separator2;
		private ToolBarButton tbbRemove;
		private ToolBarButton tbsSecurity;
		private ToolBarButton tbbSecurity;
		private Command cmdSecurity;
		private CommandInstance ciSecurityToolbarButton;
		private Command cmdServerGC;
		private ToolBarButton tbsServerGC;
		private ToolBarButton tbbServerGC;
		private CommandInstance ciServerGCToolbarButton;
		private ToolBarButton tbsGC;
		private ToolBarButton tbbGC;
		private RightTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public RightTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RightTableControl));
            this.commandManager = new FORIS.TSS.Common.Commands.CommandManager(this.components);
            this.dataDispatcher = new FORIS.TSS.Helpers.Right.Data.Right.RightDataDispatcher(this.components);
            this.daTableModel = new FORIS.TSS.Helpers.Right.Data.Right.RightDataAmbassador();
            this.tableModel = new FORIS.TSS.WorkplaceShadow.Right.Controls.Right.RightTableModel(this.components);
            this.rowFactory = new FORIS.TSS.WorkplaceShadow.Right.Controls.Right.RightXpRowFactory();
            this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
            this.columnCheck = new FORIS.TSS.TransportDispatcher.XPTable.Models.CheckBoxColumn();
            this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.cmsTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdNew = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbNew = new System.Windows.Forms.ToolBarButton();
            this.cmdProperties = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbProperties = new System.Windows.Forms.ToolBarButton();
            this.cmdRemove = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbRemove = new System.Windows.Forms.ToolBarButton();
            this.cmdCheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciCheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbCheckAll = new System.Windows.Forms.ToolBarButton();
            this.ciCheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdUncheckAll = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciUncheckAllToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.tbbUncheckAll = new System.Windows.Forms.ToolBarButton();
            this.ciUncheckAllContextMenuItem = new FORIS.TSS.Common.Commands.CommandInstance();
            this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.separator1 = new System.Windows.Forms.ToolBarButton();
            this.separator2 = new System.Windows.Forms.ToolBarButton();
            this.tbsSecurity = new System.Windows.Forms.ToolBarButton();
            this.tbbSecurity = new System.Windows.Forms.ToolBarButton();
            this.tbsGC = new System.Windows.Forms.ToolBarButton();
            this.tbbGC = new System.Windows.Forms.ToolBarButton();
            this.cmdSecurity = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciSecurityToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdServerGC = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciServerGCToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmsTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.SuspendLayout();
            // 
            // dataDispatcher
            // 
            this.dataDispatcher.Ambassadors.Add(this.daTableModel);
            // 
            // daTableModel
            // 
            this.daTableModel.Item = this.tableModel;
            // 
            // tableModel
            // 
            this.tableModel.Factory = this.rowFactory;
            this.tableModel.SelectionAdjuster = null;
            // 
            // columnModel
            // 
            this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnCheck,
            this.columnName,
            this.columnDescription});
            // 
            // columnCheck
            // 
            this.columnCheck.Editor = null;
            resources.ApplyResources(this.columnCheck, "columnCheck");
            this.columnCheck.Width = 20;
            // 
            // columnName
            // 
            this.columnName.Editor = null;
            resources.ApplyResources(this.columnName, "columnName");
            this.columnName.Width = 200;
            // 
            // columnDescription
            // 
            this.columnDescription.Editor = null;
            resources.ApplyResources(this.columnDescription, "columnDescription");
            this.columnDescription.Width = 400;
            // 
            // cmsTable
            // 
            this.cmsTable.AccessibleDescription = null;
            this.cmsTable.AccessibleName = null;
            resources.ApplyResources(this.cmsTable, "cmsTable");
            this.cmsTable.BackgroundImage = null;
            this.cmsTable.Font = null;
            this.cmsTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCheckAll,
            this.tsmiUncheckAll});
            this.cmsTable.Name = "cmsTable";
            // 
            // tsmiCheckAll
            // 
            this.tsmiCheckAll.AccessibleDescription = null;
            this.tsmiCheckAll.AccessibleName = null;
            resources.ApplyResources(this.tsmiCheckAll, "tsmiCheckAll");
            this.tsmiCheckAll.BackgroundImage = null;
            this.tsmiCheckAll.Name = "tsmiCheckAll";
            this.tsmiCheckAll.ShortcutKeyDisplayString = null;
            // 
            // tsmiUncheckAll
            // 
            this.tsmiUncheckAll.AccessibleDescription = null;
            this.tsmiUncheckAll.AccessibleName = null;
            resources.ApplyResources(this.tsmiUncheckAll, "tsmiUncheckAll");
            this.tsmiUncheckAll.BackgroundImage = null;
            this.tsmiUncheckAll.Name = "tsmiUncheckAll";
            this.tsmiUncheckAll.ShortcutKeyDisplayString = null;
            // 
            // cmdNew
            // 
            this.cmdNew.Instances.Add(this.ciNewToolbarButton);
            this.cmdNew.Manager = this.commandManager;
            this.cmdNew.Execute += new System.EventHandler(this.cmdNew_Execute);
            // 
            // ciNewToolbarButton
            // 
            this.ciNewToolbarButton.Item = this.tbbNew;
            // 
            // tbbNew
            // 
            resources.ApplyResources(this.tbbNew, "tbbNew");
            this.tbbNew.Name = "tbbNew";
            // 
            // cmdProperties
            // 
            this.cmdProperties.Instances.Add(this.ciPropertiesToolbarButton);
            this.cmdProperties.Manager = this.commandManager;
            this.cmdProperties.Execute += new System.EventHandler(this.cmdProperties_Execute);
            this.cmdProperties.Update += new System.EventHandler(this.cmdProperties_Update);
            // 
            // ciPropertiesToolbarButton
            // 
            this.ciPropertiesToolbarButton.Item = this.tbbProperties;
            // 
            // tbbProperties
            // 
            resources.ApplyResources(this.tbbProperties, "tbbProperties");
            this.tbbProperties.Name = "tbbProperties";
            // 
            // cmdRemove
            // 
            this.cmdRemove.Instances.Add(this.ciRemoveToolbarButton);
            this.cmdRemove.Manager = this.commandManager;
            this.cmdRemove.Execute += new System.EventHandler(this.cmdRemove_Execute);
            this.cmdRemove.Update += new System.EventHandler(this.cmdRemove_Update);
            // 
            // ciRemoveToolbarButton
            // 
            this.ciRemoveToolbarButton.Item = this.tbbRemove;
            // 
            // tbbRemove
            // 
            resources.ApplyResources(this.tbbRemove, "tbbRemove");
            this.tbbRemove.Name = "tbbRemove";
            // 
            // cmdCheckAll
            // 
            this.cmdCheckAll.Instances.Add(this.ciCheckAllToolbarButton);
            this.cmdCheckAll.Instances.Add(this.ciCheckAllContextMenuItem);
            this.cmdCheckAll.Manager = this.commandManager;
            this.cmdCheckAll.Execute += new System.EventHandler(this.cmdCheckAll_Execute);
            // 
            // ciCheckAllToolbarButton
            // 
            this.ciCheckAllToolbarButton.Item = this.tbbCheckAll;
            // 
            // tbbCheckAll
            // 
            resources.ApplyResources(this.tbbCheckAll, "tbbCheckAll");
            this.tbbCheckAll.Name = "tbbCheckAll";
            // 
            // ciCheckAllContextMenuItem
            // 
            this.ciCheckAllContextMenuItem.Item = this.tsmiCheckAll;
            // 
            // cmdUncheckAll
            // 
            this.cmdUncheckAll.Instances.Add(this.ciUncheckAllToolbarButton);
            this.cmdUncheckAll.Instances.Add(this.ciUncheckAllContextMenuItem);
            this.cmdUncheckAll.Manager = this.commandManager;
            this.cmdUncheckAll.Execute += new System.EventHandler(this.cmdUncheckAll_Execute);
            // 
            // ciUncheckAllToolbarButton
            // 
            this.ciUncheckAllToolbarButton.Item = this.tbbUncheckAll;
            // 
            // tbbUncheckAll
            // 
            resources.ApplyResources(this.tbbUncheckAll, "tbbUncheckAll");
            this.tbbUncheckAll.Name = "tbbUncheckAll";
            // 
            // ciUncheckAllContextMenuItem
            // 
            this.ciUncheckAllContextMenuItem.Item = this.tsmiUncheckAll;
            // 
            // table
            // 
            this.table.AccessibleDescription = null;
            this.table.AccessibleName = null;
            this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            resources.ApplyResources(this.table, "table");
            this.table.BackColor = System.Drawing.Color.White;
            this.table.BackgroundImage = null;
            this.table.ColumnModel = this.columnModel;
            this.table.ContextMenuStrip = this.cmsTable;
            this.table.EnableHeaderContextMenu = false;
            this.table.Font = null;
            this.table.FullRowSelect = true;
            this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.table.HeaderContextMenu = null;
            this.table.Name = "table";
            this.table.NoItemsText = "";
            this.table.TableModel = this.tableModel;
            // 
            // globalToolbar
            // 
            this.globalToolbar.AccessibleDescription = null;
            this.globalToolbar.AccessibleName = null;
            resources.ApplyResources(this.globalToolbar, "globalToolbar");
            this.globalToolbar.BackgroundImage = null;
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbCheckAll,
            this.tbbUncheckAll,
            this.separator1,
            this.tbbNew,
            this.tbbProperties,
            this.separator2,
            this.tbbRemove,
            this.tbsSecurity,
            this.tbbSecurity,
            this.tbsGC,
            this.tbbGC});
            this.globalToolbar.Divider = false;
            this.globalToolbar.Font = null;
            this.globalToolbar.Name = "globalToolbar";
            // 
            // separator1
            // 
            resources.ApplyResources(this.separator1, "separator1");
            this.separator1.Name = "separator1";
            this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // separator2
            // 
            resources.ApplyResources(this.separator2, "separator2");
            this.separator2.Name = "separator2";
            this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbsSecurity
            // 
            resources.ApplyResources(this.tbsSecurity, "tbsSecurity");
            this.tbsSecurity.Name = "tbsSecurity";
            this.tbsSecurity.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbSecurity
            // 
            resources.ApplyResources(this.tbbSecurity, "tbbSecurity");
            this.tbbSecurity.Name = "tbbSecurity";
            // 
            // tbsGC
            // 
            resources.ApplyResources(this.tbsGC, "tbsGC");
            this.tbsGC.Name = "tbsGC";
            this.tbsGC.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbGC
            // 
            resources.ApplyResources(this.tbbGC, "tbbGC");
            this.tbbGC.Name = "tbbGC";
            // 
            // cmdSecurity
            // 
            this.cmdSecurity.Instances.Add(this.ciSecurityToolbarButton);
            this.cmdSecurity.Manager = this.commandManager;
            this.cmdSecurity.Execute += new System.EventHandler(this.cmdSecurity_Execute);
            this.cmdSecurity.Update += new System.EventHandler(this.cmdSecurity_Update);
            // 
            // ciSecurityToolbarButton
            // 
            this.ciSecurityToolbarButton.Item = this.tbbSecurity;
            // 
            // cmdServerGC
            // 
            this.cmdServerGC.Instances.Add(this.ciServerGCToolbarButton);
            this.cmdServerGC.Manager = this.commandManager;
            this.cmdServerGC.Execute += new System.EventHandler(this.cmdServerGC_Execute);
            this.cmdServerGC.Update += new System.EventHandler(this.cmdServerGC_Update);
            // 
            // ciServerGCToolbarButton
            // 
            this.ciServerGCToolbarButton.Item = this.tbbGC;
            // 
            // RightTableControl
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this.table);
            this.Controls.Add(this.globalToolbar);
            this.Font = null;
            this.Name = "RightTableControl";
            this.cmsTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region IClientItem<IRightClientDataProvider> Members

		private IRightClientDataProvider client;
		[Browsable( false )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRightClientDataProvider Client
		{
			get { return this.client; }
			set { this.client = value; }
		}

		#endregion // IClientItem<IRightClientDataProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set { this.Client = (IRightClientDataProvider)value; }
		}

		#endregion // IClientItem Members

		#region IDataItem<IRightData> Members

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IRightData Data
		{
			get { return this.dataDispatcher.Data; }
			set { this.dataDispatcher.Data = value; }
		}

		#endregion // IDataItem<IRightData> Members

		#region Properties

		private IRightEditorProvider editorProvider;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IRightEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public RightXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int RightId = ( (RightXpRow)this.table.SelectedItems[0] ).Id;

				if( this.editorProvider != null )
				{
					this.editorProvider.Properties( RightId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if (this.table.SelectedItems.Length == 1)
			{
				int RightId = ( (RightXpRow)this.table.SelectedItems[0] ).Id;

				if (this.editorProvider != null)
				{
					this.editorProvider.Remove( RightId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionSecurity()
		{
			if( this.table.SelectedItems.Length == 1 )
			{
				int rightId = ( (RightXpRow)this.table.SelectedItems[0] ).Id;

				if( this.Client == null )
				{
					throw new ApplicationException(
						"Security editor (client) must be specified"
						);
				}

				if( this.editorProvider == null )
				{
					throw new ApplicationException(
						"Editor provider must be specified"
						);
				}

				this.Client.SecurityEditor.Edit(
					this.editorProvider.GetSecureObject( rightId )
					);
			}
		}

		private void ActionServerGC()
		{
			if( this.Client == null )
			{
				throw new ApplicationException(
					"Client must be specified"
					);
			}

			try
			{
				this.Client.Session.ServerGC();
			}
			catch( Exception ex )
			{
				MessageBox.Show(
					"������: " + ex.Message
					);
			}
		}

		private void ActionCheckAll()
		{
			foreach( RightXpRow row in this.tableModel.Rows )
			{
				row.Checked = true;
			}
		}
		private void ActionUncheckAll()
		{
			foreach( RightXpRow row in this.tableModel.Rows )
			{
				row.Checked = false;
			}
		}

		#endregion // Actions

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdSecurity_Execute( object sender, EventArgs e )
		{
			this.ActionSecurity();
		}

		private void cmdServerGC_Execute(object sender, EventArgs e)
		{
			this.ActionServerGC();
		}

		private void cmdCheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionCheckAll();
		}

		private void cmdUncheckAll_Execute( object sender, EventArgs e )
		{
			this.ActionUncheckAll();
		}

		private void cmdProperties_Update( object sender, EventArgs e )
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdSecurity_Update( object sender, EventArgs e )
		{
			this.cmdSecurity.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdServerGC_Update(object sender, EventArgs e)
		{
			bool result = false;

			if (
				this.Data != null &&
				this.Data.Right.RowsByRightName.ContainsKey( "serverGC" )
				)
			{
				RightRow rightRow = this.Data.Right.RowsByRightName["serverGC"];

				result = rightRow.TestRight( (GeneralRights)RightRights.Read );
			}

			this.cmdServerGC.Enabled = result;
		}

		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		#endregion // Events
	}
}