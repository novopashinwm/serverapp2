using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightComboBox:
		DataComboBox<RightItem, RightRow, RightTable, IRightData>
	{

		protected override RightTable GetTable()
		{
			return this.Data.Right;
		}

		protected override RightItem CreateItem( int idRight )
		{
			return new RightItem( idRight );
		}
	}

	public class RightItem:
		DataItem<RightRow, RightTable, IRightData>
	{
		public RightItem( int idRight ): base( idRight )
		{
			
		}

		protected override RightRow GetRow()
		{
			return this.Data.Right.FindRow( this.Id );
		}

		protected override string GetText()
		{
			return this.row.Name;
		}
		
	}
}
