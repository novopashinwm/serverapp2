using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Right.Controls.Right
{
	public class RightXpRowFactory:
		DataXpRowFactory<RightXpRow, RightRow, RightTable, IRightData>
	{
		public override RightXpRow CreateXpRow( int idRight )
		{
			return new RightXpRow( idRight );
		}
	}
}
