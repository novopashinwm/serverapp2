using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Right;

namespace FORIS.TSS.WorkplaceShadow.Right.Data.Right
{
	/// <summary>
	/// ��������� ������ ������ ��� �������
	/// </summary>
	public class RightDataSupplier:
		ModuleDataSupplier<IRightClientDataProvider, IRightDataSupplier>,
		IRightDataSupplier
	{
		#region Constructor & Dispose

		public RightDataSupplier( IContainer container )
		{
			container.Add( this );
		}

		public RightDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

		public IRightSecurityDataSupplier SecurityDataSupplier
		{
			get
			{
				return
					this.dataSupplier != null
						? this.dataSupplier.SecurityDataSupplier
						: null;
			}
		}

		public IRightDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IRightDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetRightClientData();
		}
	}
}