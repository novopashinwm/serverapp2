using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Right.Data.Right.Security;

namespace FORIS.TSS.Helpers.Right.Data.Right
{
	public interface IRightData:
		IData<IRightDataTreater>,
		ISecureData<IRightSecurityData>
	{
		RightTable Right { get; }
	}
}