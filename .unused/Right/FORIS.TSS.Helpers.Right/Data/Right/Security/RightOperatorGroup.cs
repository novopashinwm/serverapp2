using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Right.Data.Right.Security
{
	public class RightOperatorGroupTable:
		SecurityTable<RightOperatorGroupRow, RightOperatorGroupTable, IRightSecurityData, IRightData, IOperatorData>
	{
		#region Properties

		public override string Name
		{
			get { return "R_RIGHT_OPERATORGROUP"; }
		}

		#endregion // Properties

		public RightOperatorGroupTable()
		{
			
		}
		public RightOperatorGroupTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#region Fields' properties

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int rightIndex;
		internal int RightIndex { get { return this.rightIndex; } }

		private int operatorGroupIndex;
		internal int OperatorGroupIndex { get { return this.operatorGroupIndex; } }

		private int allowMaskIndex;
		internal int AllowMaskIndex { get { return this.allowMaskIndex; } }

		private int denyMaskIndex;
		internal int DenyMaskIndex { get { return this.denyMaskIndex; } }

		#endregion // Fields' properties

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.rightIndex = -1;
			this.operatorGroupIndex = -1;
			this.allowMaskIndex = -1;
			this.denyMaskIndex = -1;
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.DataTable != null )
			{
				this.idIndex = this.DataTable.Columns.IndexOf( "R_RIGHT_OPERATORGROUP_ID" );
				this.rightIndex = this.DataTable.Columns.IndexOf( "RIGHT_ID" );
				this.operatorGroupIndex = this.DataTable.Columns.IndexOf( "OPERATORGROUP_ID" );
				this.allowMaskIndex = this.DataTable.Columns.IndexOf( "ALLOW_MASK" );
				this.denyMaskIndex = this.DataTable.Columns.IndexOf( "DENY_MASK" );
			}
		}

		protected override RightOperatorGroupRow OnCreateRow( System.Data.DataRow dataRow )
		{
			return new RightOperatorGroupRow( dataRow );
		}

		protected override void OnRowActionHide( RightOperatorGroupRow row )
		{
			#region RightRow.RowsRightOperatorGroup 

			RightRow rightRow = this.Data.ProtectedData.Right.FindRow( row.Right );

			rightRow.RowsRightOperatorGroup.Remove( row );

			#endregion // RightRow.RowsRightOperatorGroup

			base.OnRowActionHide( row );
		}

		protected override void OnRowActionShow( RightOperatorGroupRow row )
		{
			base.OnRowActionShow( row );

			#region RightRow.RowsRightOperatorGroup

			RightRow rightRow = this.Data.ProtectedData.Right.FindRow( row.Right );

			rightRow.RowsRightOperatorGroup.Add( row );

			#endregion // RightRow.RowsRightOperatorGroup
		}

		protected override string SecureObjectIdColumnName
		{
			get { return "RIGHT_ID"; }
		}

		protected override string PrincipalIdColumnName
		{
			get { return "OPERATORGROUP_ID"; }
		}
	}

	public class RightOperatorGroupRow:
		SecurityRow<RightOperatorGroupRow, RightOperatorGroupTable, IRightSecurityData, IRightData, IOperatorData>
	{
		#region Fields

		public int Right
		{
			get { return (int)this.DataRow[this.Table.RightIndex, DataRowVersion.Current ]; }
			set { this.DataRow[this.Table.RightIndex] = value; }
		}
		
		public int OperatorGroup
		{
			get { return (int)this.DataRow[this.Table.OperatorGroupIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OperatorGroupIndex] = value; }
		}

		public override int AllowMask
		{
			get { return (int)this.DataRow[this.Table.AllowMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.AllowMaskIndex] = value; }
		}

		public override int DenyMask
		{
			get { return (int)this.DataRow[this.Table.DenyMaskIndex, DataRowVersion.Current]; }
			set {this.DataRow[this.Table.DenyMaskIndex] = value;}
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Parent row

		private OperatorGroupRow rowOperatorGroup;

		#endregion // Parent row

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.rowOperatorGroup = null;
		}
		protected override void OnBuildView()
		{
			this.rowOperatorGroup = this.Data.PrincipalData.OperatorGroup.FindRow( this.OperatorGroup );

			base.OnBuildView();
		}


		public RightOperatorGroupRow( DataRow dataRow )
			: base( dataRow )
		{

		}

		protected override void OnBuildFields()
		{
			
		}

		protected override ITssPrincipal GetPrincipal()
		{
			return this.rowOperatorGroup;			
		}
	}
}
