using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Right.Data.Right.Security
{
	public class RightSecurityData:
		FORIS.TSS.Helpers.Data.Security.SecurityData<RightData, IRightSecurityData>,
		IRightSecurityData,
		IRightSecurityDataSupplier
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private RightSecurityDataDispatcher tablesDataDispatcher;
		private OperatorData principalData;

		private RightOperatorGroupTable rightOperatorGroup;
		private RightOperatorTable rightOperator;

		private RightSecurityDataAmbassador daRightOperatorGroup;
		private RightSecurityDataAmbassador daRightOperator;
		
		#endregion // Controls & Components 

		#region Constructor & Dispose

		private readonly IRightData protectedData;

		public RightSecurityData( RightData protectedData ):
			base( protectedData )
		{
			this.protectedData = protectedData;

			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new RightSecurityDataDispatcher(this.components);
			this.principalData = new OperatorData( this.components );

			this.rightOperatorGroup = new RightOperatorGroupTable( this.components );
			this.rightOperator = new RightOperatorTable( this.components );

			this.daRightOperatorGroup = new RightSecurityDataAmbassador();
			this.daRightOperator = new RightSecurityDataAmbassador();

			this.daRightOperatorGroup.Item = this.rightOperatorGroup;
			this.daRightOperator.Item = this.rightOperator;

			this.tablesDataDispatcher.Ambassadors.Add( this.daRightOperatorGroup );
			this.tablesDataDispatcher.Ambassadors.Add( this.daRightOperator );
		}

		#endregion // Component Designer generated code

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IRightSecurityDataSupplier DataSupplier
		{
			get { return (IRightSecurityDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override FORIS.TSS.Helpers.Data.ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;

			this.principalData.DataSupplier = null;
		}

		protected override void Build()
		{
			this.principalData.DataSupplier = this.DataSupplier.PrincipalDataSupplier;

			this.tablesDataDispatcher.Data = this;
		}


		#endregion // Build & Destroy

		#region Security

		protected override ISecurityTable GetSecurityTable( SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo )
		{
			switch( objectInfo.TableName )
			{
				case "RIGHT":
					switch( principalInfo.TableName )
					{
						case "OPERATORGROUP":
							return this.rightOperatorGroup;

						case "OPERATOR":
							return this.rightOperator;
					}
					break;
			}

			throw new ApplicationException();
		}

		public IRightData ProtectedData
		{
			get { return this.protectedData; }
		}

		public IOperatorData PrincipalData
		{
			get { return this.principalData; }
		}

		IOperatorDataSupplier ISecurityDataSupplier<IOperatorDataSupplier>.PrincipalDataSupplier
		{
			get { return this.principalData; }			
		}


		#endregion // Security

		#region Tables properties (IRightSecurityData Members)

		[Browsable(false)]
		public RightOperatorGroupTable RightOperatorGroup
		{
			get { return this.rightOperatorGroup; }
		}

		[Browsable(false)]
		public RightOperatorTable RightOperator
		{
			get { return this.rightOperator; }
		}

		#endregion // Tables properties (IRightSecurityData Members)

		public IRightSecurityDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}
	}

	public interface IRightSecurityData:
		ISecurityData<IRightSecurityData, IRightData, IOperatorData>
	{
		RightOperatorGroupTable RightOperatorGroup { get; }
		RightOperatorTable RightOperator { get; }
	}
}

