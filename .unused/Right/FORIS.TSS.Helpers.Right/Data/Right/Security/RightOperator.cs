using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Right.Data.Right.Security
{
	public class RightOperatorTable:
		SecurityTable<RightOperatorRow, RightOperatorTable, IRightSecurityData, IRightData, IOperatorData>
	{
		#region Properties

		public override string Name
		{
			get { return "R_RIGHT_OPERATOR"; }
		}

		#endregion // Properties

		public RightOperatorTable()
		{
		}

		public RightOperatorTable( IContainer container): this()
		{
			container.Add( this );
		}

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex { get { return this.idIndex; } }

		private int rightIndex;
		internal int RightIndex { get { return this.rightIndex; } }

		private int operatorIndex;
		internal int OperatorIndex { get { return this.operatorIndex; } }

		private int allowMaskIndex;
		internal int AllowMaskIndex { get { return this.allowMaskIndex; } }

		private int denyMaskIndex;
		internal int DenyMaskIndex { get { return this.denyMaskIndex; } }

		#endregion // Fields' indexes

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.rightIndex = -1;
			this.operatorIndex = -1;
			this.allowMaskIndex = -1;
			this.denyMaskIndex = -1;

			this.Data.ProtectedData.Right.Rows.Inserted -= this.RightRows_Inserted;
			this.Data.ProtectedData.Right.Rows.Removing -= this.RightRows_Removing;

			this.childRows.Clear();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.DataTable != null )
			{
				this.idIndex = this.DataTable.Columns.IndexOf( "R_RIGHT_OPERATOR_ID" );
				this.rightIndex = this.DataTable.Columns.IndexOf( "RIGHT_ID" );
				this.operatorIndex = this.DataTable.Columns.IndexOf( "OPERATOR_ID" );
				this.allowMaskIndex = this.DataTable.Columns.IndexOf( "ALLOW_MASK" );
				this.denyMaskIndex = this.DataTable.Columns.IndexOf( "DENY_MASK" );
			}

			this.Data.ProtectedData.Right.Rows.Inserted += this.RightRows_Inserted;
			this.Data.ProtectedData.Right.Rows.Removing += this.RightRows_Removing;
		}

		protected override RightOperatorRow OnCreateRow( DataRow dataRow )
		{
			return new RightOperatorRow( dataRow );
		}

		protected override void OnRowActionHide( RightOperatorRow row )
		{
			#region RightRow.RowsRightOperator

			lock ( this.childRows )
			{
				RightRow rightRow = this.Data.ProtectedData.Right.FindRow( row.Right );

				if ( rightRow != null )
				{
					rightRow.RowsRightOperator.Remove( row );
				}
				else
				{
					List<RightOperatorRow> list = this.childRows[row.Right];
					list.Remove( row );

					if ( list.Count == 0 )
					{
						this.childRows.Remove( row.Right );
					}
				}
			}

			#endregion // RightRow.RowsRightOperator

			base.OnRowActionHide( row );
		}

		protected override void OnRowActionShow( RightOperatorRow row )
		{
			base.OnRowActionShow( row );

			#region RightRow.RowsRightOperator

			lock( this.childRows )
			{
				RightRow rightRow = this.Data.ProtectedData.Right.FindRow( row.Right );

				if ( rightRow != null )
				{
					rightRow.RowsRightOperator.Add( row );
				}
				else
				{
					if ( this.childRows.ContainsKey( row.Right ) )
					{
						this.childRows[row.Right].Add( row );
					}
					else
					{
						List<RightOperatorRow> list = new List<RightOperatorRow>();
						list.Add( row );

						this.childRows.Add( row.Right, list );
					}
				}
			}

			#endregion // RightRow.RowsRightOperator
		}

		protected override string SecureObjectIdColumnName
		{
			get { return "RIGHT_ID"; }
		}

		protected override string PrincipalIdColumnName
		{
			get { return "OPERATOR_ID"; }
		}


		#region Delayed child rows

		private readonly Dictionary<int, List<RightOperatorRow>> childRows =
			new Dictionary<int, List<RightOperatorRow>>();

		void RightRows_Inserted( object sender, CollectionChangeEventArgs<RightRow> e )
		{
			lock ( this.childRows )
			{
				if ( this.childRows.ContainsKey( e.Item.ID ) )
				{
					foreach ( RightOperatorRow row in this.childRows[e.Item.ID] )
					{
						e.Item.RowsRightOperator.Add( row );
					}

					this.childRows.Remove( e.Item.ID );
				}
			}
		}

		void RightRows_Removing( object sender, CollectionChangeEventArgs<RightRow> e )
		{
			lock ( this.childRows )
			{
				if ( this.childRows.ContainsKey( e.Item.ID ) )
				{
					this.childRows.Remove( e.Item.ID );
				}
			}
		}

		#endregion  // Delayed child rows
	}

	public class RightOperatorRow:
		SecurityRow<RightOperatorRow, RightOperatorTable, IRightSecurityData, IRightData, IOperatorData>
	{
		#region Fields

		public int Right
		{
			get { return (int)this.DataRow[this.Table.RightIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RightIndex] = value; }
		}

		public int Operator
		{
			get { return (int)this.DataRow[this.Table.OperatorIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OperatorIndex] = value; }
		}

		public override int AllowMask
		{
			get { return (int)this.DataRow[this.Table.AllowMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.AllowMaskIndex] = value; }
		}

		public override int DenyMask
		{
			get { return (int)this.DataRow[this.Table.DenyMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DenyMaskIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Parent row

		private OperatorRow rowOperator;

		#endregion // Parent row

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.rowOperator = null;
		}
		protected override void OnBuildView()
		{
			this.rowOperator = this.Data.PrincipalData.Operator.FindRow( this.Operator );
			base.OnBuildView();
		}

		public RightOperatorRow( DataRow dataRow )
			: base( dataRow )
		{

		}

		protected override void OnBuildFields()
		{
			
		}

		protected override ITssPrincipal GetPrincipal()
		{
			return this.rowOperator;
		}
	}
}
