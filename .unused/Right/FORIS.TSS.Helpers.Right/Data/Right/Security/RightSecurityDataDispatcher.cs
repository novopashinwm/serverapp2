using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Right.Data.Right.Security;

namespace FORIS.TSS.Helpers.Right.Data.Right.Security
{
	public class RightSecurityDataDispatcher:
		DataDispatcher<IRightSecurityData>
	{
		public RightSecurityDataDispatcher( IContainer container ): this()
		{
			container.Add( this );
		}
		public RightSecurityDataDispatcher()
		{
			
		}

		private readonly RightSecurityDataAmbassadorCollection ambassadors =
			new RightSecurityDataAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new RightSecurityDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRightSecurityData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

	public class RightSecurityDataAmbassadorCollection:
		DataAmbassadorCollection<IRightSecurityData>
	{
		public new RightSecurityDataAmbassador this[int index ]
		{
			get { return (RightSecurityDataAmbassador)base[index]; }
		}
	}

	public class RightSecurityDataAmbassador:
		DataAmbassador<IRightSecurityData>
	{
		
	}
}