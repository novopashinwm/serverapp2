using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Right.Data.Right
{
	public class RightDataDispatcher:
		DataDispatcher<IRightData>
	{
		public RightDataDispatcher( IContainer container ): this()
		{
			container.Add( this );
		}
		public RightDataDispatcher()
		{
			
		}

		private readonly RightDataAmbassadorCollection ambassadors =
			new RightDataAmbassadorCollection();

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new RightDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IRightData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

	public class RightDataAmbassadorCollection:
		DataAmbassadorCollection<IRightData>
	{
		public new RightDataAmbassador this[int index ]
		{
			get { return (RightDataAmbassador)base[index]; }
		}
	}

	public class RightDataAmbassador:
		DataAmbassador<IRightData>
	{
		
	}
}