using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right.Security;
using System;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting;

namespace FORIS.TSS.Helpers.Right.Data.Right
{
	public class RightData:
		FORIS.TSS.Helpers.Data.Security.SecureData<IRightDataTreater>,
		IRightData,
		IRightDataSupplier
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private RightDataDispatcher tablesDataDispatcher;

		private RightTable right;

		private RightDataAmbassador daRight;
		
		#endregion // Controls & Components

		#region Constructor & Dispose

		public RightData( IContainer container ): this()
		{
			container.Add( this );
		}
		public RightData()
		{
			this.securityData = new RightSecurityData(this);
			
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.securityData.Dispose();

				if( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new RightDataDispatcher( this.components );

			this.right = new RightTable( this.components );

			this.daRight = new RightDataAmbassador();

			this.daRight.Item = this.right;

			this.tablesDataDispatcher.Ambassadors.Add( this.daRight );
		}

		#endregion // Component Designer generated code

		#region Properties

		public new IRightDataSupplier DataSupplier
		{
			get { return (IRightDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override FORIS.TSS.Helpers.Data.ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.securityData.DataSupplier = null;

			this.tablesDataDispatcher.Data = null;
		}

		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;

			this.securityData.DataSupplier = this.DataSupplier.SecurityDataSupplier;
		}

		#endregion // Build & Destroy

		#region Security

		private readonly RightSecurityData securityData;
		[Browsable( false )]
		public IRightSecurityData SecurityData
		{
			get { return this.securityData; }
		}

		public IRightSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.securityData; }
		}

		#endregion // Security

		#region Tables properties

		[Browsable(false)]
		public RightTable Right
		{
			get { return this.right; }
		}

		#endregion // Tables properties
	}
}