using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Right.Data.Right;
using FORIS.TSS.Helpers.Right.Data.Right.Security;
using FORIS.TSS.Helpers.Security;

namespace FORIS.TSS.Helpers.Right.Data.Right
{
	public class RightTable:
		SecureTable<RightRow, RightTable, IRightData, IRightSecurityData, IOperatorData>
	{
		public override string Name
		{
			get { return "RIGHT"; }
		}

		public RightTable()
		{
			
		}
		public RightTable( IContainer container )
		{
			container.Add( this );
		}

		#region Fields' indexes

		private int idIndex = -1;
		internal int IdIndex { get { return this.idIndex; } }

		private int nameIndex = -1;
		internal int NameIndex { get { return this.nameIndex; } }

		private int descriptionIndex = -1;
		internal int DescriptionIndex { get { return this.descriptionIndex; } }

		private int rightNameIndex = -1;
		internal int RightNameIndex { get { return this.rightNameIndex; } }


		#endregion // Fields' indexes

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex = -1;
			this.nameIndex = -1;
			this.descriptionIndex = -1;
			this.rightNameIndex = -1;
		}

		protected override void OnBuildView()
		{
			base.OnBuildView();

			if( this.DataTable != null )
			{
				this.idIndex = this.DataTable.Columns.IndexOf( "RIGHT_ID" );
				this.nameIndex = this.DataTable.Columns.IndexOf( "NAME" );
				this.descriptionIndex = this.DataTable.Columns.IndexOf( "DESCRIPTION" );
				this.rightNameIndex = this.DataTable.Columns.IndexOf( "RIGHT_NAME" );
			}
		}

		protected override RightRow OnCreateRow( DataRow dataRow )
		{
			return new RightRow( dataRow );
		}

		protected override void OnRowActionHide( RightRow row )
		{
			this.rowsByRightName.Remove( row.RightName );

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( RightRow row )
		{
			base.OnRowActionShow( row );

			this.rowsByRightName.Add( row.RightName, row );
		}

		private readonly Dictionary<string, RightRow> rowsByRightName =
			new Dictionary<string, RightRow>();

		public IDictionary<string, RightRow> RowsByRightName
		{
			get { return this.rowsByRightName; }
		}

		#region Implement SecureTable Members

		/* ���������������� ����� �� ����� �� ��������� 
		 * �� ������� �����, �� �������� ���� ������� ��,
		 * ��� ��� �� ������ ����������� ��������, 
		 * ����� ������ ��������� ������ �� ���
		 * 
		 */

		private static readonly AccessRight[] s_accessRights =
			new AccessRight[]
				{
					new AccessRight( (int)RightRights.FullControl, "Full Control" ),
					new AccessRight( (int)RightRights.Read, "Read" ),
					// new AccessRight( (int)RightRights.Update, "Update" ),
					// new AccessRight( (int)RightRights.Delete, "Delete" ),
					new AccessRight( (int)RightRights.AccessControl, "AccessRight Control" )
				};

		public override AccessRight[] AccessRights
		{
			get { return RightTable.s_accessRights; }
		}

		#endregion // Implement SecureTable Members
	}

	[Flags]
	public enum RightRights: int
	{
		FullControl = GeneralRights.FullControl,
		Read = GeneralRights.Read,
		// Update = GeneralRights.Update,
		// Delete = GeneralRights.Delete,
		AccessControl = GeneralRights.AccessControl
	}

	public class RightRow: 
		SecureRow<RightRow, RightTable, IRightData, IRightSecurityData, IOperatorData>
	{
		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Description
		{
			get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DescriptionIndex] = value; }
		}

		public string RightName
		{
			get { return (string)this.DataRow[this.Table.RightNameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.RightNameIndex] = value; }
		}

		#endregion // Fields

		#region Child rows

		//private readonly ChildRowCollection<RightOperatorGroupRow, IRightSecurityData> rowsRightOperatorGroup;

		//public ChildRowCollection<RightOperatorGroupRow, IRightSecurityData> RowsRightOperatorGroup
		//{
		//    get { return this.rowsRightOperatorGroup; }
		//}


		//private readonly ChildRowCollection<RightOperatorRow, IRightSecurityData> rowsRightOperator;

		//public ChildRowCollection<RightOperatorRow, IRightSecurityData> RowsRightOperator
		//{
		//    get { return this.rowsRightOperator; }
		//}


		#endregion // Child rows 

		#region Security child rows

		private readonly ChildRowCollection<RightOperatorGroupRow, RightOperatorGroupTable, IRightSecurityData> rowsRightOperatorGroup;
		public ChildRowCollection<RightOperatorGroupRow, RightOperatorGroupTable, IRightSecurityData> RowsRightOperatorGroup
		{
			get { return this.rowsRightOperatorGroup; }
		}

		private readonly ChildRowCollection<RightOperatorRow, RightOperatorTable, IRightSecurityData> rowsRightOperator;
		public ChildRowCollection<RightOperatorRow, RightOperatorTable, IRightSecurityData> RowsRightOperator
		{
			get { return this.rowsRightOperator; }
		}

		#endregion // Security child rows

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public RightRow( DataRow dataRow ): base( dataRow )
		{
			this.rowsRightOperatorGroup = new ChildRowCollection<RightOperatorGroupRow, RightOperatorGroupTable, IRightSecurityData>();
			this.rowsRightOperator = new ChildRowCollection<RightOperatorRow, RightOperatorTable, IRightSecurityData>();
		}

		#region ISecureObject



		#endregion // ISecureObject

		protected override void OnBuildFields()
		{

		}

		#region Implement SecureRow Members

		protected override string GetSecureObjectName()
		{
			return this.Name;
		}

		protected override ICollection<ISecureObject> GetSecureObjectParents()
		{
			return new List<ISecureObject>( 0 );
		}

		protected override IList<IAccess> GetSecureObjectAcl()
		{
			List<IAccess> ACL = new List<IAccess>( this.rowsRightOperatorGroup.Count + this.rowsRightOperator.Count );

			foreach( RightOperatorGroupRow rightOperatorGroupRow in this.rowsRightOperatorGroup )
			{
				ACL.Add( rightOperatorGroupRow );
			}

			foreach( RightOperatorRow rightOperatorRow in this.rowsRightOperator )
			{
				ACL.Add( rightOperatorRow );
			}

			return ACL;
		}

		public override void ClearAcl()
		{
			foreach( RightOperatorGroupRow rightOperatorGroupRow in this.rowsRightOperatorGroup.Clone() )
			{
				rightOperatorGroupRow.Delete();
			}
			foreach( RightOperatorRow rightOperatorRow in this.RowsRightOperator.Clone() )
			{
				rightOperatorRow.Delete();
			}
		}

		#endregion // Implement SecureRow Members
	}
}