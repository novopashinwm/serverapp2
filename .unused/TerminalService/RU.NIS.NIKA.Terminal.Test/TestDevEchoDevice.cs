﻿using System.Collections;
using System.Linq;
using System.Text;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NIS.NIKA.Terminal.Test
{
    public class TestDevEchoDevice : Device
    {
        private readonly byte[] _devNull = Encoding.ASCII.GetBytes("/dev/echo");

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;

            return new ArrayList {new ConfirmPacket(data.Take(count).ToArray())};
        }

        public override bool SupportData(byte[] data)
        {
            return data.StartsWith(_devNull);
        }
    }
}