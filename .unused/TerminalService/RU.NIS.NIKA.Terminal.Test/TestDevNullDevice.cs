﻿using System.Collections;
using System.Text;
using FORIS.TSS.Terminal;
using FORIS.TSS.Helpers;

namespace RU.NIS.NIKA.Terminal.Test
{
    public class TestDevNullDevice : Device
    {
        private readonly byte[] _devNull = Encoding.ASCII.GetBytes("/dev/null");

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            return new ArrayList();
        }

        public override bool SupportData(byte[] data)
        {
            return data.StartsWith(_devNull);
        }
    }
}
