﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.Order285
{
    public class Order285Device : Device, IHttpDevice
    {
        const string Prefix = "Order285";

        public bool CanProcess(IHttpRequest request)
        {
            return request.RequestUrl.Segments.Any(
                    s => string.Equals(s, Prefix, StringComparison.OrdinalIgnoreCase));
        }

        public IList<object> OnData(IHttpRequest request)
        {
            if (request.Method != HttpMethod.Post)
                return new object[] { HttpResponse.Forbidden() };

            var xmlString = request.Text;
            xmlString = xmlString.Replace(
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"">",
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">");

            xmlString = xmlString.Replace(
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"">",
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">");
            var document = new XmlDocument();
            document.LoadXml(xmlString);

            var objectId = document.SelectSingleNode("//ObjectID");
            if (objectId == null)
                return new object[] { HttpResponse.BadRequest("not supported") };

            var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
            ui.DeviceID = objectId.InnerText;
            if(!Manager.FillUnitInfo(ui))
                return new object[] { HttpResponse.Forbidden() };
            //if(ui.DeviceType != ControllerType.Names.Order285)
            //    return new object[] { HttpResponse.Forbidden() };

            var coord = document.SelectSingleNode("//Coord");
            if (coord == null)
                return new object[] { HttpResponse.BadRequest("not supported") };

            var timeAttr = coord.Attributes != null ? coord.Attributes["time"] : null;
            if (timeAttr == null)
                return new object[] { HttpResponse.Ok() };
            var lonAttr = coord.Attributes != null ? coord.Attributes["lon"] : null;
            var latAttr = coord.Attributes != null ? coord.Attributes["lat"] : null;
            var speedAttr = coord.Attributes != null ? coord.Attributes["speed"] : null;
            var dirAttr = coord.Attributes != null ? coord.Attributes["dir"] : null;
            var validAttr = coord.Attributes != null ? coord.Attributes["valid"] : null;

            MobilUnit mu = null;
            try
            {
                var time = XmlConvert.ToDateTime(timeAttr.InnerText, XmlDateTimeSerializationMode.Utc);
                var lon = lonAttr != null ? XmlConvert.ToDouble(lonAttr.InnerText) : MobilUnit.InvalidLongitude;
                var lat = latAttr != null ? XmlConvert.ToDouble(latAttr.InnerText) : MobilUnit.InvalidLatitude;
                var speed = speedAttr != null ? XmlConvert.ToInt32(speedAttr.InnerText) : (int?)null;
                var direction = dirAttr != null ? XmlConvert.ToInt32(dirAttr.InnerText) : (int?)null;
                var valid = validAttr != null && validAttr.InnerText == "1";
                mu = new MobilUnit(ui)
                {
                    Time = TimeHelper.GetSecondsFromBase(time),
                    Longitude = lon,
                    Latitude = lat,
                    Speed = speed,
                    Course = direction,
                    CorrectGPS = valid
                };
            }
            catch (Exception ex)
            {
                Trace.TraceError("{0} couldnt parse request {1} {2}", this, request, ex);
            }

            if (mu == null)
                return new object[] { HttpResponse.Ok() };
            
            return new List<object>
            {
                mu,
                HttpResponse.Ok()
            };
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            throw new NotSupportedException();
        }

        public override bool SupportData(byte[] data)
        {
            return false;
        }
    }
}
