﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using RU.NVG.NIKA.Terminal.Egts;

namespace RU.NVG.NIKA.Terminal.Granit
{
    public class GranitDevice : EgtsDevice
    {
        public GranitDevice()
            : base(null, new[] { "GRANIT_PERSONAL", "GRANIT_AUTO" })
        {
            
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            var result = base.OnData(data, count, stateData, out bufferRest);
            foreach (var unit in result.OfType<IMobilUnit>())
            {
                if (!unit.Properties.ContainsKey(DeviceProperty.Protocol))
                    unit.Properties.Add(DeviceProperty.Protocol, "GRANIT_AUTO");
                else
                    unit.Properties[DeviceProperty.Protocol] = "GRANIT_AUTO";
            }

            return result;
        }

        public override List<CommandStep> GetCommandSteps(IStdCommand command)
        {
            switch (command.Type)
            {
                case CmdType.Setup:
                    return GetSetupCommandSteps();
                default: 
                    return base.GetCommandSteps(command);
            }
        }

        private List<CommandStep> GetSetupCommandSteps()
        {
            var steps = new List<CommandStep>(4)
            {
                new SmsCommandStep("BB+SRV1PROT=EGTS"),
                new SmsCommandStep(string.Format("BB+SAPN={0},{1},{2}",
                    Manager.GetConstant(Constant.NetworkAPN),
                    Manager.GetConstant(Constant.NetworkAPNUser),
                    Manager.GetConstant(Constant.NetworkAPNPassword))),
                new SmsCommandStep(string.Format("BB+SRV1={0},{1},{2}", Manager.GetConstant(Constant.ServerIP),
                    Manager.GetConstant(Constant.AppDomain), Manager.GetConstant(Constant.ServerPort))),
                new SmsCommandStep("BB+RESET")
            };

            foreach (var step in steps)
                step.WaitAnswer = CmdType.Setup;

            return steps;
        }
    }
}
