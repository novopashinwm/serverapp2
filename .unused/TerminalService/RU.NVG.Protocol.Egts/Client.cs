﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using FORIS.TSS.Helpers;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Packet;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Auth;

namespace RU.NVG.Protocol.Egts
{
	public class Client : IDisposable
	{
		private readonly TimeSpan _timeOut = TimeSpan.FromSeconds(100);
		private readonly string   _host;
		private readonly int      _port;
		private readonly int?     _tid;
		private TcpClient         _client;
		private Stream            _stream;
		private EgtsReader        _reader;
		private EgtsWriter        _writer;
		private readonly Mutex    _mutex = new Mutex(true);

		private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

		public bool Connected
		{
			get { return _client.Connected; }
		}
		public Client(string host, int port, int? tid = null)
		{
			_client = new TcpClient();
			_host   = host;
			_port   = port;
			_tid    = tid;
			Task.Factory.StartNew(ReadAnswers, _cancellationTokenSource.Token, TaskCreationOptions.LongRunning);
		}
		private void ReadAnswers(object o)
		{
			var cancellationToken = (CancellationToken)o;
			while (!cancellationToken.IsCancellationRequested)
			{
				if (Connected)
				{
					// просто вычитываем и ничего не делаем
					var buf = new byte[1024];
					try
					{
						var readed = _reader.BaseStream.Read(buf, 0, buf.Length);
						var memoryStream = new MemoryStream(buf, 0, readed);
						var egtsReader = new EgtsReader(memoryStream);
						var response = egtsReader.ReadPacket();
						var responseDataRecord = response.ServiceFrame.Records
								.SelectMany(r => r.SubRecords)
								.FirstOrDefault(r => r.DataRecord is EgtsSrRecordResponse);
						if (responseDataRecord == null)
						{
							Trace.TraceWarning("{0}.ReadAnswers: invalid response - no EgtsSrRecordResponse", this);
							continue;
						}
						var responsePacket = (EgtsSrRecordResponse) responseDataRecord.DataRecord;
						if (responsePacket.Rst != ResponseResult.EGTS_PC_OK)
							Trace.TraceWarning("{0}.ReadAnswers: invalid response {1}", this, responsePacket.Rst);
					}
					catch (Exception e)
					{
						Trace.TraceWarning("{0}.ReadAnswers failed: {1}", this, e);
					}
				}
				else
				{
					_mutex.WaitOne(TimeSpan.FromSeconds(5));
				}
			}
		}
		public void Send(EgtsPtAppdata requestFrame)
		{
			ConnectIfNeeded();

			var requestPacket = new TransportPacket(requestFrame)
				{
					Priority = Priority.Mid
				};
			_writer.Write(requestPacket);
		}
		public void Dispose()
		{
			_cancellationTokenSource.Cancel();
			_client.Close();
		}
		protected void ConnectIfNeeded()
		{
			if (!_client.Connected)
			{
				_client = new TcpClient();
				_client.Connect(_host, _port, _timeOut);
				_stream = _client.GetStream();
				_stream.ReadTimeout = (int)_timeOut.TotalMilliseconds;
				_reader = new EgtsReader(_stream);
				_writer = new EgtsWriter(_stream);
				_mutex.ReleaseMutex();

				if (_tid.HasValue)
				{
					var requestFrame = EgtsPtAppdata.Create();
					requestFrame.Add(new EgtsSrTermIdentity
					{
						Tid = (uint) _tid.Value
					});
					var authPacket = new TransportPacket(requestFrame);
					_writer.Write(authPacket);
					var responsePacket = _reader.ReadPacket();
					var responseFrame = (EgtsPtResponse)responsePacket.ServiceFrame;
					var errors = responseFrame.GetErrors(requestFrame).ToArray();
					if (errors.Any())
					{
						var errorMessage = string.Join(";", errors.Select(e => string.Format("{0}-{1}", e.Crn, e.Rst)));
						throw new ArgumentException("client couldn't connect to " + _host + ":" + _port + " - " + errorMessage);
					}
				}
			}

			if(!_client.Connected)
				throw new ArgumentException("client couldn't connect to " + _host + ":" + _port);
		}
	}
}