﻿using System.Text;
using FORIS.TSS.Terminal.MJ;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class MJForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new MJ();
            if (!device.SupportData(hexData))
                return "Not supported";

            byte[] bufferRest;
            var items = device.OnData(hexData, hexData.Length, null, out bufferRest);
            var sb = new StringBuilder();
            foreach (var item in items)
            {
                var mu = item as IMobilUnit;
                if (mu == null)
                {
                    var positionMessage = item as FORIS.TSS.Terminal.Messaging.IPositionMessage;
                    if (positionMessage != null)
                        mu = positionMessage.MobilUnit;
                }
                if (mu == null)
                    continue;
                Helper.Append(sb, mu);
            }

            return sb.ToString();
        }
    }
}
