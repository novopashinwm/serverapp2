﻿using System.Text;
using FORIS.TSS.Terminal.NaviTech.VersionUtp4;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class NaviTechForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new NaviTechDevice();

            var sb = new StringBuilder();

            byte[] bufferRest;
            var parsedItems = device.OnData(hexData, hexData.Length, null, out bufferRest);

            if (parsedItems == null)
                return "No data";

            foreach (var item in parsedItems)
            {
                var mobilUnit = item as IMobilUnit;
                if (mobilUnit == null)
                    continue;

                Helper.Append(sb, mobilUnit);
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
