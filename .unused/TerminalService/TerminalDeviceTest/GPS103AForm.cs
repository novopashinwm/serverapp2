﻿using System.Text;
using FORIS.TSS.Terminal.GPS103A;

namespace TerminalDeviceTest
{
    public class GPS103AForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new GPS103ADevice();
            var sb = new StringBuilder();
            sb.AppendLine();

            if (!device.SupportData(hexData))
            {
                sb.AppendLine("Data not supported: " + s);
            }
            else
            {
                byte[] restBuffer;
                var res = device.OnData(hexData, 0, new object(), out restBuffer);

                foreach (object o in res)
                {
                    var mu = o as FORIS.TSS.MobilUnit.Unit.MobilUnit;
                    if (mu == null) continue;
                    Helper.Append(sb, mu);
                }
            }
            return sb.ToString();
        }
    }
}
