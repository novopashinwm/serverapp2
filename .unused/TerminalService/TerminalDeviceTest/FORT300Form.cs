﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.Terninal.FORT300;

namespace TerminalDeviceTest
{
    class FORT300Form : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new FORT300();
            byte[] bufferRest;
            var list = device.OnData(hexData, hexData.Length, null, out bufferRest);

            var sb = new StringBuilder();
            foreach (var o in list)
            {
                var mu = o as IMobilUnit;
                if (mu == null)
                    continue;
                Helper.Append(sb, mu);
            }

            return sb.ToString();
        }
    }
}
