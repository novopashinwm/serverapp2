﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using FORIS.TSS.Terminal.Symbian;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class Symbian : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var result = new StringBuilder();

            bool validCRC = S60V3FP.CheckCRC(hexData, hexData.Length);
            result.Append(validCRC ? "CRC is valid" : "CRC is invalid");
            result.AppendLine();
            IEnumerable items;
            
            try
            {
                var s60Device = new S60V3FP();
                items = s60Device.ParseAnyPacket(hexData, hexData.Length, null).ToArray();
            }
            catch (Exception)
            {
                items = null;
            }

            if (items == null)
            {
                byte[] bufferRest;
                items = new S60V3FP().OnData(hexData, hexData.Length, null, out bufferRest);
            }

            foreach (var item in items)
            {
                var mlpDevice = item as IMobilUnit;
                if (mlpDevice == null)
                    continue;
                result.Append(mlpDevice, typeof(S60V3FPSensor));
                result.AppendLine();
            }
            return result.ToString();
        }

        protected override string ParseFile(byte[] bytes)
        {
            var s60Device = new S60V3FP();
            var items = s60Device.ParseAnyPacket(bytes, 0, bytes.Length, null);

            var result = new StringBuilder();
            foreach (var item in items)
            {
                var mobilUnit = item as IMobilUnit;
                if (mobilUnit == null)
                    continue;
                Helper.Append(result, mobilUnit);
                result.AppendLine();
            }
            return result.ToString();
        }

        protected override IEnumerable GetDevices(string s, byte[] stringData, byte[] hexData, string originalInputString, string deviceID)
        {
            var s60Device = new S60V3FP();
            return s60Device.ParseAnyPacket(hexData, 0, hexData.Length, deviceID);
        }

    }
}
