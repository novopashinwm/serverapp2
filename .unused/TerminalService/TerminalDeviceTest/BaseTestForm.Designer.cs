﻿namespace TerminalDeviceTest
{
    partial class BaseTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonDataToDB = new System.Windows.Forms.Button();
            this.buttonFileToDB = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._phoneBox = new System.Windows.Forms.TextBox();
            this.deviceIDBox = new System.Windows.Forms.TextBox();
            this.buttonFromFile = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.inputBox = new System.Windows.Forms.TextBox();
            this.outputBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.buttonDataToDB);
            this.splitContainer1.Panel1.Controls.Add(this.buttonFileToDB);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this._phoneBox);
            this.splitContainer1.Panel1.Controls.Add(this.deviceIDBox);
            this.splitContainer1.Panel1.Controls.Add(this.buttonFromFile);
            this.splitContainer1.Panel1.Controls.Add(this.btnProcess);
            this.splitContainer1.Panel1.Controls.Add(this.inputBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.outputBox);
            this.splitContainer1.Size = new System.Drawing.Size(633, 609);
            this.splitContainer1.SplitterDistance = 211;
            this.splitContainer1.TabIndex = 0;
            // 
            // buttonDataToDB
            // 
            this.buttonDataToDB.Location = new System.Drawing.Point(12, 176);
            this.buttonDataToDB.Name = "buttonDataToDB";
            this.buttonDataToDB.Size = new System.Drawing.Size(187, 23);
            this.buttonDataToDB.TabIndex = 5;
            this.buttonDataToDB.Text = "Upload data to DB";
            this.buttonDataToDB.UseVisualStyleBackColor = true;
            this.buttonDataToDB.Click += new System.EventHandler(this.buttonFileToDB_Click);
            // 
            // buttonFileToDB
            // 
            this.buttonFileToDB.Location = new System.Drawing.Point(12, 152);
            this.buttonFileToDB.Name = "buttonFileToDB";
            this.buttonFileToDB.Size = new System.Drawing.Size(187, 23);
            this.buttonFileToDB.TabIndex = 5;
            this.buttonFileToDB.Text = "Upload file to DB";
            this.buttonFileToDB.UseVisualStyleBackColor = true;
            this.buttonFileToDB.Click += new System.EventHandler(this.buttonFileToDB_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Phone string";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "DeviceID String";
            // 
            // _phoneBox
            // 
            this._phoneBox.Location = new System.Drawing.Point(16, 126);
            this._phoneBox.Name = "_phoneBox";
            this._phoneBox.Size = new System.Drawing.Size(186, 20);
            this._phoneBox.TabIndex = 3;
            // 
            // deviceIDBox
            // 
            this.deviceIDBox.Location = new System.Drawing.Point(13, 80);
            this.deviceIDBox.Name = "deviceIDBox";
            this.deviceIDBox.Size = new System.Drawing.Size(186, 20);
            this.deviceIDBox.TabIndex = 3;
            // 
            // buttonFromFile
            // 
            this.buttonFromFile.Location = new System.Drawing.Point(13, 34);
            this.buttonFromFile.Name = "buttonFromFile";
            this.buttonFromFile.Size = new System.Drawing.Size(186, 23);
            this.buttonFromFile.TabIndex = 2;
            this.buttonFromFile.Text = "Read file";
            this.buttonFromFile.UseVisualStyleBackColor = true;
            this.buttonFromFile.Click += new System.EventHandler(this.buttonFromFile_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(13, 4);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(186, 23);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Обработать";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // inputBox
            // 
            this.inputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputBox.Location = new System.Drawing.Point(12, 205);
            this.inputBox.Multiline = true;
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(187, 390);
            this.inputBox.TabIndex = 0;
            this.inputBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.inputBox_KeyUp);
            // 
            // outputBox
            // 
            this.outputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputBox.Location = new System.Drawing.Point(12, 12);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputBox.Size = new System.Drawing.Size(394, 585);
            this.outputBox.TabIndex = 0;
            // 
            // BaseTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 609);
            this.Controls.Add(this.splitContainer1);
            this.Name = "BaseTestForm";
            this.Text = "BaseTestForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox inputBox;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button buttonFromFile;
        private System.Windows.Forms.Button buttonFileToDB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox deviceIDBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _phoneBox;
        private System.Windows.Forms.Button buttonDataToDB;
    }
}