﻿using System.Collections;
using System.Text;
using FORIS.TSS.Terminal.AzimuthGSM;

namespace TerminalDeviceTest
{
    class AzimuthForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new AzimuthGSM();
            var sb = new StringBuilder();
            sb.AppendLine();

            byte[] restBuffer;
            var res = device.OnData(stringData, stringData.Length, new object(), out restBuffer);

            foreach (object o in res)
            {
                var mu = o as FORIS.TSS.MobilUnit.Unit.MobilUnit;
                if (mu == null) continue;
                sb.Append(mu, typeof(Sensors));
            }
            return sb.ToString();
        }

        protected override IEnumerable GetDevices(string s, byte[] stringData, byte[] hexData, string originalInputString, string deviceID)
        {
            var device = new AzimuthGSM();
            byte[] restBuffer;
            return device.OnData(hexData, hexData.Length, new object(), out restBuffer);
        }
    }
}
