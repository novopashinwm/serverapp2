﻿using System.Text;
using FORIS.TSS.TerminalService.Interfaces;
using RU.NVG.NIKA.Terminal.Wialon;

namespace TerminalDeviceTest
{
    class WialonTestForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            if (!originalInputString.EndsWith("\r\n"))
                originalInputString += "\r\n";

            var bytes = Encoding.ASCII.GetBytes(originalInputString);

            byte[] bufferRest;
            var items = new WialonDevice().OnData(bytes, bytes.Length, null, out bufferRest);

            var result = new StringBuilder();
            foreach (var item in items)
            {
                var device = item as IMobilUnit;
                if (device == null)
                    continue;
                result.Append(device, typeof(WialonSensor));
                result.AppendLine();
            }
            return result.ToString();
        }
    }
}
