﻿namespace TerminalDeviceTest
{
    partial class AvtoGraf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcess = new System.Windows.Forms.Button();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtRes = new System.Windows.Forms.TextBox();
            this.btnPrecessV16 = new System.Windows.Forms.Button();
            this.btnInsertIntoDB = new System.Windows.Forms.Button();
            this.btnV16OnData = new System.Windows.Forms.Button();
            this._firmwareVersionBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._v16onDataText = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(12, 12);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(158, 26);
            this.btnProcess.TabIndex = 4;
            this.btnProcess.Text = "Обработка";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // txtInput
            // 
            this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInput.Location = new System.Drawing.Point(12, 80);
            this.txtInput.Multiline = true;
            this.txtInput.Name = "txtInput";
            this.txtInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInput.Size = new System.Drawing.Size(946, 80);
            this.txtInput.TabIndex = 3;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // txtRes
            // 
            this.txtRes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRes.Location = new System.Drawing.Point(12, 181);
            this.txtRes.Multiline = true;
            this.txtRes.Name = "txtRes";
            this.txtRes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRes.Size = new System.Drawing.Size(946, 199);
            this.txtRes.TabIndex = 2;
            this.txtRes.TextChanged += new System.EventHandler(this.txtRes_TextChanged);
            // 
            // btnPrecessV16
            // 
            this.btnPrecessV16.Location = new System.Drawing.Point(189, 12);
            this.btnPrecessV16.Name = "btnPrecessV16";
            this.btnPrecessV16.Size = new System.Drawing.Size(150, 26);
            this.btnPrecessV16.TabIndex = 4;
            this.btnPrecessV16.Text = "Обработка v16";
            this.btnPrecessV16.UseVisualStyleBackColor = true;
            this.btnPrecessV16.Click += new System.EventHandler(this.btnPrecessV16_Click);
            // 
            // btnInsertIntoDB
            // 
            this.btnInsertIntoDB.Location = new System.Drawing.Point(492, 12);
            this.btnInsertIntoDB.Name = "btnInsertIntoDB";
            this.btnInsertIntoDB.Size = new System.Drawing.Size(150, 26);
            this.btnInsertIntoDB.TabIndex = 4;
            this.btnInsertIntoDB.Text = "Вставить в базу";
            this.btnInsertIntoDB.UseVisualStyleBackColor = true;
            this.btnInsertIntoDB.Click += new System.EventHandler(this.btnInsertIntoDB_Click);
            // 
            // btnV16OnData
            // 
            this.btnV16OnData.Location = new System.Drawing.Point(346, 14);
            this.btnV16OnData.Name = "btnV16OnData";
            this.btnV16OnData.Size = new System.Drawing.Size(140, 23);
            this.btnV16OnData.TabIndex = 5;
            this.btnV16OnData.Text = "Обработка v16 OnData";
            this.btnV16OnData.UseVisualStyleBackColor = true;
            this.btnV16OnData.Click += new System.EventHandler(this.btnV16OnData_Click);
            // 
            // _firmwareVersionBox
            // 
            this._firmwareVersionBox.Location = new System.Drawing.Point(774, 12);
            this._firmwareVersionBox.Name = "_firmwareVersionBox";
            this._firmwareVersionBox.Size = new System.Drawing.Size(184, 20);
            this._firmwareVersionBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(671, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Версия прошивки";
            // 
            // _v16onDataText
            // 
            this._v16onDataText.Location = new System.Drawing.Point(346, 44);
            this._v16onDataText.Name = "_v16onDataText";
            this._v16onDataText.Size = new System.Drawing.Size(164, 23);
            this._v16onDataText.TabIndex = 8;
            this._v16onDataText.Text = "Обработка v16 onData text";
            this._v16onDataText.UseVisualStyleBackColor = true;
            this._v16onDataText.Click += new System.EventHandler(this.button1_Click);
            // 
            // AvtoGraf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 392);
            this.Controls.Add(this._v16onDataText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._firmwareVersionBox);
            this.Controls.Add(this.btnV16OnData);
            this.Controls.Add(this.btnInsertIntoDB);
            this.Controls.Add(this.btnPrecessV16);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.txtRes);
            this.Name = "AvtoGraf";
            this.Text = "AvtoGraf";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtRes;
        private System.Windows.Forms.Button btnPrecessV16;
        private System.Windows.Forms.Button btnInsertIntoDB;
        private System.Windows.Forms.Button btnV16OnData;
        private System.Windows.Forms.TextBox _firmwareVersionBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _v16onDataText;
    }
}