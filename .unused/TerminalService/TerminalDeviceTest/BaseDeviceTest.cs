﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TerminalDeviceTest
{
	public abstract class BaseDeviceTest<TDevice> where TDevice : class, IDevice, new()
	{
		private TDevice _device;

		protected TDevice Device
		{
			get
			{
				if (_device != null) 
					return _device;
				_device = new TDevice();
				_device.Manager = new TerminalManager();

				return _device;
			}
		}

		protected IDevice GetDevice(byte[] data)
		{
			var terminalManager = new TerminalManager();
			var device = terminalManager.GetDevice(data);
			Assert.IsNotNull(device);
			Assert.IsInstanceOfType(device, typeof(TDevice));

			return device;
		}

		protected void CheckSupportsData(string s)
		{
			var bytes = Encoding.ASCII.GetBytes(s);
			Assert.IsTrue(Device.SupportData(bytes));
			Assert.AreEqual(typeof (TDevice), GetDevice(bytes).GetType());
		}

		protected void CheckSupportsData(byte[] bytes)
		{
			Assert.IsTrue(Device.SupportData(bytes));
			Assert.AreEqual(typeof (TDevice), GetDevice(bytes).GetType());
		}

		protected IEnumerable<IMobilUnit> GetMobilUnits(params byte[][] packets)
		{
			if (packets == null || packets.Length == 0)
				yield break;

			byte[] bufferRest = null;
			object stateData = null;
			foreach (var packet in packets)
			{
				var data = packet;
				if (bufferRest != null && bufferRest.Length != 0)
					data = bufferRest.Concat(data).ToArray();

				var result = Device.OnData(data, data.Length, stateData, out bufferRest);

				if (result == null)
					throw new ArgumentOutOfRangeException("packets", packets, @"Value cannot be parsed");

				var storeToStateMessage = result.OfType<ReceiverStoreToStateMessage>().FirstOrDefault();
				if (storeToStateMessage != null)
					stateData = storeToStateMessage.StateData;

				foreach (var mobilUnit in result.OfType<IMobilUnit>())
					yield return mobilUnit;
			}
		}

		protected IEnumerable<IMobilUnit> GetMobilUnits(params string[] packets)
		{
			if (packets == null)
				yield break;

			var bytePackets = Array.ConvertAll(packets, p => Encoding.ASCII.GetBytes(p));

			foreach (var mu in GetMobilUnits(bytePackets))
				yield return mu;
		}

		protected IList OnData(byte[] data, object state = null)
		{
			byte[] bufferRest;
			return Device.OnData(data, data.Length, state, out bufferRest);
		}

		protected IList OnHexData(string hex, object state = null)
		{
			var data = StringHelper.HexStringToByteArray(hex);

			return OnData(data, state);
		}

		protected IList OnStringData(string hex, object state = null)
		{
			var data = Encoding.ASCII.GetBytes(hex);

			return OnData(data, state);
		}
	}
}