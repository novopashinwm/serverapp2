﻿using System;
using System.Collections;
using FORIS.TSS.Terminal.UltraCom;

namespace TerminalDeviceTest
{
    class DeerCollarForm : BaseTestForm
    {
        protected override Type GetSensorType()
        {
            return typeof (DeerCollarSensor);
        }

        protected override IEnumerable GetDevices(string s, byte[] stringData, byte[] hexData, string originalInputString, string deviceID)
        {
            var device = new DeerCollar();

            var res = new ArrayList();

            foreach (var line in originalInputString.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            {
                byte[] lineHexData;
                if (!Helper.TryParseAsHexString(line, out lineHexData))
                    continue;
                byte[] restBuffer;
                res.AddRange(device.OnData(lineHexData, lineHexData.Length, new object(), out restBuffer));
            }

            return res;
        }
    }
}
