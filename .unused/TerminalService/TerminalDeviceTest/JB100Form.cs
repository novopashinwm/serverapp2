﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal.JB100;

namespace TerminalDeviceTest
{
    class JB100Form : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new FORIS.TSS.Terminal.JB100.JB100();
            var isSupported = device.SupportData(stringData);
            if (!isSupported)
                return "Not supported";

            byte[] bufferRest;
            var parsedItems = device.OnData(stringData, stringData.Length, null, out bufferRest);

            var sb = new StringBuilder();

            foreach (MobilUnit mu in parsedItems)
            {
                sb.Append(mu, typeof(JB100Sensor));

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
