﻿using System.Text;
using FORIS.TSS.Terminal.GPS608.V2;

namespace TerminalDeviceTest
{
    public partial class GPS608V2 : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new FORIS.TSS.Terminal.GPS608.V2.GPS608();
            var sb = new StringBuilder();
            sb.AppendLine();

            if (!device.SupportData(hexData))
            {
                sb.AppendLine("Data not supported: " + s);
            }
            else
            {
                byte[] restBuffer;
                var res = device.OnData(hexData, 0, new object(), out restBuffer);

                foreach (object o in res)
                {
                    var mu = o as FORIS.TSS.MobilUnit.Unit.MobilUnit;
                    if (mu == null) continue;
                    sb.Append(mu, typeof(GPS608Sensors));
                }
            }
            return sb.ToString();
        }
    }
}
