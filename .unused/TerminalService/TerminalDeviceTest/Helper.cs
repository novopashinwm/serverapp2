﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
	public static class Helper
	{
		private static readonly DataBaseManager DBManager= new DataBaseManager();

		public static bool TryParseAsHexString(string packetString, out byte[] res)
		{
			byte[] data = StringHelper.HexStringToByteArray(packetString);
			res = data;
			return (res!=null);
		}

		public static byte[] ParseString(string packetString)
		{
			byte[] data;

			if(!TryParseAsHexString(packetString, out data))
				data = Encoding.ASCII.GetBytes(packetString);

			return data;
		}

		public static DateTime ConvertToUserLocalTime(DateTime date, int userCurrentUTCTimeOffset)
		{
			//Определяем разницу в смещении относительно UTC на сервере на текущий момент и на указанную дату (будем надеяться, что на сервере установлен русский регион)
			int serverOffsetDiff = (int)Math.Floor((date - date.ToLocalTime()).TotalHours) - (int)Math.Floor((DateTime.Now.ToUniversalTime() - DateTime.Now).TotalHours);

			return date.AddHours(userCurrentUTCTimeOffset + serverOffsetDiff);
		}

		public static void InsertIntoDB(IEnumerable aRes)
		{
			var mus = new List<IMobilUnit>();

			var terminalManager = new TerminalManager();
			foreach (var o in aRes)
			{
				var mu = o as IMobilUnit;
				
				if (terminalManager.FillUnitInfo(mu))
					mus.Add(mu);
			}

			const int bufferCapacity = 100;
			var buffer = new List<IMobilUnit>(bufferCapacity);
			foreach (var mu in mus)
			{
				buffer.Add(mu);

				if (buffer.Count == bufferCapacity)
				{
					DBManager.AddMonitoreeLog(buffer);
					buffer.Clear();
				}
			}
			if (buffer.Any())
				DBManager.AddMonitoreeLog(buffer);
		}

		public static void Append(this StringBuilder stringBuilder, IList onDataResults, Type sensorEnumType)
		{
			if (onDataResults == null || onDataResults.Count == 0) return;
			foreach (var onDataResult in onDataResults)
			{
				if (onDataResult is IMobilUnit)
				{
					Append(stringBuilder, onDataResult as IMobilUnit, sensorEnumType);
				}
				else if (onDataResult is ConfirmPacket)
				{
					Append(stringBuilder, onDataResult as ConfirmPacket);
				}
			}
		}

		public static void Append(this StringBuilder stringBuilder, IList onDataResults)
		{
			Append(stringBuilder, onDataResults, null);
		}

		public static void Append(this StringBuilder stringBuilder, ConfirmPacket confirmPacket)
		{
			if (confirmPacket == null) return;
			stringBuilder.Append("ConfirmPacket: ");
			stringBuilder.AppendLine(Encoding.ASCII.GetString(confirmPacket.Data));
		}

		public static void Append(this StringBuilder stringBuilder, IMobilUnit mobilUnit)
		{
			Append(stringBuilder, mobilUnit, null);
		}
			
		public static void Append(this StringBuilder stringBuilder, IMobilUnit mobilUnit, Type sensorEnumType)
		{
			if (mobilUnit == null) return;
			stringBuilder.Append("ID: ");
			stringBuilder.Append(mobilUnit.ID);
			stringBuilder.AppendLine();

			stringBuilder.Append("DeviceID: ");
			stringBuilder.AppendLine(mobilUnit.DeviceID ?? "<empty>");

			if (mobilUnit.Unique != 0)
			{
				stringBuilder.AppendFormat("Unique: {0}", mobilUnit.Unique);
				stringBuilder.AppendLine();
			}

			stringBuilder.Append("Firmware: ");
			stringBuilder.AppendLine(mobilUnit.Firmware);
			stringBuilder.Append("Latitude: ");
			stringBuilder.AppendLine(mobilUnit.Latitude.ToString(CultureInfo.InvariantCulture));
			stringBuilder.Append("Longitude: ");
			stringBuilder.AppendLine(mobilUnit.Longitude.ToString(CultureInfo.InvariantCulture));
			stringBuilder.Append("Time: ");
			stringBuilder.Append(TimeHelper.GetDateTimeUTC(mobilUnit.Time).ToLocalTime().ToString(CultureInfo.InvariantCulture));
			stringBuilder.AppendLine(" (" + mobilUnit.Time + ")");
			stringBuilder.Append("Speed: ");
			stringBuilder.Append(mobilUnit.Speed);
			stringBuilder.AppendLine();
			stringBuilder.Append("ValidPosition: ");
			stringBuilder.AppendLine(mobilUnit.ValidPosition.ToString());
			stringBuilder.Append("Satellites: ");
			stringBuilder.AppendLine(mobilUnit.Satellites.ToString(CultureInfo.InvariantCulture));

			if (mobilUnit.Course != null)
			{
				stringBuilder.Append("Course: ");
				stringBuilder.AppendLine(mobilUnit.Course.ToString());
			}

			if (mobilUnit.Radius != null)
			{
				stringBuilder.Append("Radius: ");
				stringBuilder.AppendLine(mobilUnit.Radius.ToString());
			}

			if (mobilUnit.SensorValues != null)
			{
				stringBuilder.AppendLine("Sensors");
				foreach (var pair in mobilUnit.SensorValues)
				{
					if (sensorEnumType != null)
					{
						var @sensorEnum = Enum.ToObject(sensorEnumType, pair.Key);
						stringBuilder.Append(@sensorEnum);
					}
					else
					{
						stringBuilder.Append(pair.Key);
					}
					stringBuilder.Append(": ");
					stringBuilder.Append(pair.Value);
					stringBuilder.AppendLine();
				}
			}

			if (mobilUnit.CellNetworks != null && mobilUnit.CellNetworks.Length != 0)
			{
				stringBuilder.Append("Cell networks: ");
				foreach (var cellNetwork in mobilUnit.CellNetworks)
				{
					stringBuilder.Append(TimeHelper.GetDateTimeUTC(mobilUnit.Time).ToLocalTime());
					stringBuilder.Append(" ");
					stringBuilder.Append(cellNetwork.Number);
					stringBuilder.Append(": ");
					stringBuilder.Append(cellNetwork.CountryCode);
					stringBuilder.Append(" / ");
					stringBuilder.Append(cellNetwork.NetworkCode);
					stringBuilder.Append(" / ");
					stringBuilder.Append(cellNetwork.LAC);
					stringBuilder.Append(" / ");
					stringBuilder.Append(cellNetwork.CellID);
					stringBuilder.Append(" / ");
					stringBuilder.Append(cellNetwork.SignalStrength);
					stringBuilder.AppendLine();
				}
			}

			if (mobilUnit.Properties != null)
			{
				stringBuilder.AppendLine("Properties:");
				foreach (var pair in mobilUnit.Properties)
				{
					stringBuilder.AppendFormat("{0}: {1}", pair.Key, pair.Value);
					stringBuilder.AppendLine();
				}
			}
		}
	}
}
