﻿using System;
using System.Windows.Forms;

namespace TerminalDeviceTest
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //DateTime dt = Helper.ConvertToUserLocalTime(new DateTime(2009, 10, 10, 11, 2, 0), -3);
            //dt = Helper.ConvertToUserLocalTime(new DateTime(2009, 11, 10, 11, 2, 0), -3);

            var f = new AvtoGraf();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var f = new TM4();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var f = new GlobalOrient();
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var f = new TM140();
            f.Show();
        }

        private void m2mButton_Click(object sender, EventArgs e)
        {
            var f = new M2M();
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new Mirep().Show();
        }

        private void symbianS60_Click(object sender, EventArgs e)
        {
            new Symbian().Show();
        }

        private void TR203Button_Click(object sender, EventArgs e)
        {
            new TR203Form().Show();
        }

        private void teltonikaButton_Click(object sender, EventArgs e)
        {
            new TeltonikaForm().Show();
        }

        private void MeiligaoButton_Click(object sender, EventArgs e)
        {
            new MeiligaoVT300().Show();
        }

        private void MeiligaoMVT380Button_Click(object sender, EventArgs e)
        {
            new MeiligaoMVT380().Show();
        }

        private void btnGV100_Click(object sender, EventArgs e)
        {
            new GV100().Show();
        }

        private void btnGPS680_Click(object sender, EventArgs e)
        {
            new GPS608().Show();
        }

        private void _naviTechButton_Click(object sender, EventArgs e)
        {
            new NaviTechForm().Show();
        }

        private void _buttonJB100_Click(object sender, EventArgs e)
        {
            new JB100Form().Show();
        }

        private void _buttonStarLine_Click(object sender, EventArgs e)
        {
            new StarLine().Show();
        }

        private void _btnNeva_Click(object sender, EventArgs e)
        {
            new NevaForm().Show();
        }

        private void _btnMJClick(object sender, EventArgs e)
        {
            new MJForm().Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new GPS608V2().Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new GPS103AForm().Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            new DeerCollarForm().Show();
        }

        private void _sonimButton_Click(object sender, EventArgs e)
        {
            new SonimForm().Show();
        }

        private void _fort300ButtonClick(object sender, EventArgs e)
        {
            new FORT300Form().Show();
        }

        private void _azimuthButton_Click(object sender, EventArgs e)
        {
            new AvtofonForm().Show();
        }

        private void _universalClick(object sender, EventArgs e)
        {
            new UniversalForm().Show();
        }

        private void _egtsButton_Click(object sender, EventArgs e)
        {
            new EgtsTestForm().Show();
        }

        private void _wialonButton_Click(object sender, EventArgs e)
        {
            new WialonTestForm().Show();
        }
    }
}
