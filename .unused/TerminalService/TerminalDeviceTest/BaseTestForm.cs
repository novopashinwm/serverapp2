﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    public partial class BaseTestForm : Form
    {
        public BaseTestForm()
        {
            InitializeComponent();
        }

        protected virtual string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            IEnumerable res;
            try
            {
                res = GetDevices(s, stringData, hexData, originalInputString, deviceIDBox.Text);
            }
            catch (NotImplementedException)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();
            var sensorType = GetSensorType();
            foreach (object o in res)
            {
                var mu = o as FORIS.TSS.MobilUnit.Unit.MobilUnit;
                if (mu == null) continue;

                sb.Append(mu, sensorType);
            }
            return sb.ToString();
        }

        protected virtual Type GetSensorType()
        {
            return null;
        }

        protected void Process(string originalInputString)
        {
            string text;

            if (!string.IsNullOrEmpty(originalInputString))
            {
                try
                {
                    byte[] parsedBytes = null;
                    byte[] unparsedBytes = null;
                    string inputString = originalInputString;
                    //Проверяем если введенный текст - это байты
                    if (Helper.TryParseAsHexString(inputString, out parsedBytes))
                    {
                        inputString = Encoding.ASCII.GetString(parsedBytes);
                        unparsedBytes = parsedBytes;
                    }
                    else //Если это был printable текст
                    {
                        unparsedBytes = Encoding.ASCII.GetBytes(inputString);
                        parsedBytes = unparsedBytes;
                    }

                    text = Parse(inputString, unparsedBytes, parsedBytes, originalInputString);
                }
                catch (Exception ex)
                {
                    text = ex.ToString();
                }
            }
            else
            {
                text = string.Empty;
            }
            outputBox.Text = text;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            Process(inputBox.Text);
        }

        private void inputBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !e.Shift)
            {
                Process(inputBox.Text);
            }
        }

        protected virtual string ParseFile(byte[] bytes)
        {
            return Parse(null, bytes, bytes, null);
        }

        private void buttonFromFile_Click(object sender, EventArgs e)
        {
            using (var fileDialog = new OpenFileDialog())
            {
                if (fileDialog.ShowDialog() != DialogResult.OK)
                    return;
                var bytes = File.ReadAllBytes(fileDialog.FileName);

                string text;
                try
                {
                    text = ParseFile(bytes);
                }
                catch (Exception ex)
                {
                    text = ex.ToString();
                }

                File.WriteAllText("output.txt", text);

                outputBox.Text = text;
            }
        }

        private void buttonFileToDB_Click(object sender, EventArgs e)
        {
            byte[] bytes;

            if (sender == buttonFileToDB)
            {
                using (var fileDialog = new OpenFileDialog())
                {
                    if (fileDialog.ShowDialog() != DialogResult.OK)
                        return;
                    bytes = File.ReadAllBytes(fileDialog.FileName);
                }
            }
            else if (sender == buttonDataToDB)
            {
                bytes = Encoding.ASCII.GetBytes(inputBox.Text);
            }
            else
            {
                throw new ArgumentOutOfRangeException("sender");
            }

            byte[] parsedBytes = null;
            byte[] unparsedBytes = null;
            string inputString = inputBox.Text;
            //Проверяем если введенный текст - это байты
            if (Helper.TryParseAsHexString(inputString, out parsedBytes))
            {
                inputString = Encoding.ASCII.GetString(parsedBytes);
                unparsedBytes = parsedBytes;
            }
            else //Если это был printable текст
            {
                unparsedBytes = Encoding.ASCII.GetBytes(inputString);
                parsedBytes = unparsedBytes;
            }

            var devices = GetDevices(
                inputString, 
                unparsedBytes,
                parsedBytes,
                inputBox.Text,
                deviceIDBox.Text)
                .Cast<object>().
                Select(o =>
                           {
                               var mu = o as IMobilUnit;
                               if (mu != null)
                                   mu.Telephone = _phoneBox.Text;

                               return o;
                           });
            Helper.InsertIntoDB(devices);
        }

        protected virtual IEnumerable GetDevices(string s, byte[] stringData, byte[] hexData, string originalInputString, string deviceID)
        {
            throw new NotImplementedException();
        }
    }
}
