﻿using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal.AvtoGRAF16;
using FORIS.TSS.TerminalService.Interfaces;
using AvtoGrafSensors = FORIS.TSS.Terminal.AvtoGRAF16.AvtoGrafSensors;


namespace TerminalDeviceTest
{
	public partial class AvtoGraf : Form
	{
		public AvtoGraf()
		{
			InitializeComponent();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			txtRes.Text = ProcessOld(txtInput.Text);
		}

		private byte[] ParseInputString()
		{
			byte[] data;
			string dataStr = txtInput.Text.Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
			if (!dataStr.StartsWith("#"))
				data = StringHelper.HexStringToByteArray(dataStr.Replace(":", "").Replace("-", ""));
			else
				data = Encoding.ASCII.GetBytes(dataStr);

			return data;
		}

		private void ParsePackageV16()
		{
			byte[] data = ParseInputString();
			var avt16 = new AvtoGRAF16();
			if (!avt16.SupportData(data))
			{
				txtRes.Text = "Data is not supported";
				return;
			}

			var dg = new AvtoGRAF16Datagram(data, _firmwareVersionBox.Text);

			if (!dg.isValid)
				txtRes.Text = "Datagram is invalid";
			else
			{
				txtRes.Text = "";
				txtRes.Text += "\r\n" + "=============================";
				txtRes.Text += "\r\n" + dg.ID;
				txtRes.Text += "\r\n" + "=============================";
				foreach (TracePoint point in dg.TracePoints)
				{
					txtRes.Text += "\r\n" + "=============================";
					txtRes.Text += "\r\n" + string.Format("ID={0}", dg.ID);
					txtRes.Text += "\r\n" + "Time=" + point.Time.ToString();
					txtRes.Text += "\r\n" + string.Format("lat={0}, lng={1}", point.Lat, point.Lng);
					txtRes.Text += "\r\n" + string.Format("Speed={0}, Course={1}", point.Speed, point.Course);
					txtRes.Text += "\r\n" + string.Format("CorrectGPS={0}", point.GPSCorrect);
					txtRes.Text += "\r\n" + string.Format("DigitalSensor1={0}, DigitalSensor2={1}", point.DigitalSensor1, point.DigitalSensor2);
					txtRes.Text += "\r\n" + string.Format("AnalogSensor1: {0}, AnalogSensor2: {1}, PowerVoltage: {2}",
														  point.AnalogSensor1, point.AnalogSensor2, point.PowerVoltage);
					txtRes.Text += "\r\n" + string.Format("DigitalCounter1: {0}, DigitalCounter2: {1}",
														  point.DigitalSensor1Counter,
														  point.DigitalSensor2Counter);
					txtRes.Text += "\r\n" + string.Format("DigitalCounter3: {0}, DigitalCounter4: {1}",
														  point.DigitalSensor3Counter,
														  point.DigitalSensor4Counter);
					txtRes.Text += "\r\n" + string.Format("LLS1: {0}, LLS2: {1}",
														  point.LLS1,
														  point.LLS2);
					txtRes.Text += "\r\n" + string.Format("LLS3: {0}, LLS4: {1}",
														  point.LLS3,
														  point.LLS4);
					txtRes.Text += "\r\n" + string.Format("LLS5: {0}, LLS6: {1}",
														  point.LLS5,
														  point.LLS6);
					txtRes.Text += "\r\n" + string.Format("LLS7: {0}, LLS8: {1}",
														  point.LLS7,
														  point.LLS8);

				}
			}

			txtRes.Text += "============Все записи==================";

			foreach (Rec rec in dg.Records)
			{
				txtRes.Text += "\r\n" + rec.ToString();
			}

		}

		private void ParsePackageV16AndCreateMobileUnits(byte[] data)
		{
			var device = new AvtoGRAF16();

			var result = new StringBuilder();

			if (!device.SupportData(data))
			{
				result.AppendLine("Not supported");
			}
			else
			{
				byte[] bufferRest;
				var list = device.OnData(data, -1, null, out bufferRest);
			
				foreach (var item in list)
				{
					var mobileUnit = item as IMobilUnit;
					if (mobileUnit != null) 
					{
						result.Append(mobileUnit, typeof(AvtoGrafSensors));
						continue;
					}
				
					var confirmPacket = item as ConfirmPacket;
					if (confirmPacket != null)
					{
						result.AppendLine();
						result.AppendLine("ConfirmPacket: " + Encoding.ASCII.GetString(confirmPacket.Data));
						continue;
					}
				}
			}
			txtRes.Text = result.ToString();
		}

		//CopyFromBaseForm
		protected string ProcessOld(string inputString)
		{
			string text;

			if (!string.IsNullOrEmpty(inputString))
			{
				try
				{
					byte[] parsedBytes = null;
					byte[] unparsedBytes = null;
					//Проверяем если введенный текст - это байты
					if (Helper.TryParseAsHexString(inputString, out parsedBytes))
					{
						inputString = Encoding.ASCII.GetString(parsedBytes);
						unparsedBytes = parsedBytes;
					}
					else //Если это был printable текст
					{
						unparsedBytes = Encoding.ASCII.GetBytes(inputString);
						parsedBytes = unparsedBytes;
					}

					text = ParseOld(inputString, unparsedBytes, parsedBytes);
				}
				catch (Exception ex)
				{
					text = ex.ToString();
				}
			}
			else
			{
				text = string.Empty;
			}

			return text;
		}

		protected string ParseOld(string s, byte[] stringData, byte[] hexData)
		{
			var device = new FORIS.TSS.Terminal.AvtoGRAF.AvtoGRAF();
			var sb = new StringBuilder();
			sb.AppendLine();

			if (!device.SupportData(hexData))
			{
				sb.AppendLine("Data not supported: " + s);
			}
			else
			{
				byte[] restBuffer;
				var res = device.OnData(hexData, 0, new object(), out restBuffer);
				var i = res.Count;

				foreach (object o in res)
				{
					if (o != null && o is FORIS.TSS.MobilUnit.Unit.MobilUnit)
					{
						sb.Append(o as FORIS.TSS.MobilUnit.Unit.MobilUnit, typeof(AvtoGrafSensors));
					}
				}
			}
			return sb.ToString();
		}

		/*
		private void ParsePackage()
		{
			byte[] data = ParseInputString();
			TreckerAF379Datagram dgTr = new TreckerAF379Datagram(data);
			AvtoGRAFDatagram dg = new AvtoGRAFDatagram(data);
			txtRes.Text = "";
			foreach (TracePoint point in dg.TracePoints)
			{
				txtRes.Text += "\r\n" + "=============================";
				txtRes.Text += "\r\n" + string.Format("ID={0}", dg.ID);
				txtRes.Text += "\r\n" + "Time=" + point.Time.ToString();
				txtRes.Text += "\r\n" + string.Format("lat={0}, lng={1}", point.Lat, point.Lng);
				txtRes.Text += "\r\n" + string.Format("Speed={0}, Course={1}", point.Speed, point.Course);
				txtRes.Text += "\r\n" + string.Format("CorrectGPS={0}", point.GPSCorrect);
				txtRes.Text += "\r\n" + string.Format("DigitalSensor1={0}, DigitalSensor2={1}", point.DigitalSensor1, point.DigitalSensor2Counter);
			}

			txtRes.Text += "============Все записи==================";

			foreach (Rec rec in dg.Records)
			{
				txtRes.Text += "\r\n" + rec.ToString();
			}
		}
		*/

		private void btnPrecessV16_Click(object sender, EventArgs e)
		{
			ParsePackageV16();
		}

		private void txtInput_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtRes_TextChanged(object sender, EventArgs e)
		{

		}

		private void btnInsertIntoDB_Click(object sender, EventArgs e)
		{
			byte[] data = ParseInputString();
			AvtoGRAF16 device = new AvtoGRAF16();
			byte[] buffRes;
			IList aRes = device.OnData(data, data.Length, null, out buffRes);
			Helper.InsertIntoDB(aRes);
		}

		private void btnV16OnData_Click(object sender, EventArgs e)
		{
			ParsePackageV16AndCreateMobileUnits(ParseInputString());
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ParsePackageV16AndCreateMobileUnits(Encoding.ASCII.GetBytes(txtInput.Text));
		}
	}
}