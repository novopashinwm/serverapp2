﻿namespace TerminalDeviceTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.m2mButton = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.symbianS60 = new System.Windows.Forms.Button();
            this.TR203Button = new System.Windows.Forms.Button();
            this.teltonikaButton = new System.Windows.Forms.Button();
            this.MeiligaoVT300Button = new System.Windows.Forms.Button();
            this.MeiligaoMVT380Button = new System.Windows.Forms.Button();
            this.btnGV100 = new System.Windows.Forms.Button();
            this.btnGPS680 = new System.Windows.Forms.Button();
            this._naviTechButton = new System.Windows.Forms.Button();
            this._buttonJB100 = new System.Windows.Forms.Button();
            this._buttonStarLine = new System.Windows.Forms.Button();
            this._btnNeva = new System.Windows.Forms.Button();
            this._btnMJ = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this._fort300Button = new System.Windows.Forms.Button();
            this._sonimButton = new System.Windows.Forms.Button();
            this._azimuthButton = new System.Windows.Forms.Button();
            this._universal = new System.Windows.Forms.Button();
            this._avtofonDevice = new System.Windows.Forms.Button();
            this._egtsButton = new System.Windows.Forms.Button();
            this._wialonButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "АвтоГРАФ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "ТМ4";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(28, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "GlobalOrient";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(28, 100);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "TM140";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // m2mButton
            // 
            this.m2mButton.Location = new System.Drawing.Point(28, 130);
            this.m2mButton.Name = "m2mButton";
            this.m2mButton.Size = new System.Drawing.Size(75, 23);
            this.m2mButton.TabIndex = 2;
            this.m2mButton.Text = "M2M";
            this.m2mButton.UseVisualStyleBackColor = true;
            this.m2mButton.Click += new System.EventHandler(this.m2mButton_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(28, 160);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "Mirep";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // symbianS60
            // 
            this.symbianS60.Location = new System.Drawing.Point(28, 189);
            this.symbianS60.Name = "symbianS60";
            this.symbianS60.Size = new System.Drawing.Size(75, 23);
            this.symbianS60.TabIndex = 4;
            this.symbianS60.Text = "Symbian S60";
            this.symbianS60.UseVisualStyleBackColor = true;
            this.symbianS60.Click += new System.EventHandler(this.symbianS60_Click);
            // 
            // TR203Button
            // 
            this.TR203Button.Location = new System.Drawing.Point(28, 219);
            this.TR203Button.Name = "TR203Button";
            this.TR203Button.Size = new System.Drawing.Size(75, 23);
            this.TR203Button.TabIndex = 5;
            this.TR203Button.Text = "TR203";
            this.TR203Button.UseVisualStyleBackColor = true;
            this.TR203Button.Click += new System.EventHandler(this.TR203Button_Click);
            // 
            // teltonikaButton
            // 
            this.teltonikaButton.Location = new System.Drawing.Point(142, 13);
            this.teltonikaButton.Name = "teltonikaButton";
            this.teltonikaButton.Size = new System.Drawing.Size(88, 23);
            this.teltonikaButton.TabIndex = 6;
            this.teltonikaButton.Text = "Teltonika";
            this.teltonikaButton.UseVisualStyleBackColor = true;
            this.teltonikaButton.Click += new System.EventHandler(this.teltonikaButton_Click);
            // 
            // MeiligaoVT300Button
            // 
            this.MeiligaoVT300Button.Location = new System.Drawing.Point(142, 42);
            this.MeiligaoVT300Button.Name = "MeiligaoVT300Button";
            this.MeiligaoVT300Button.Size = new System.Drawing.Size(88, 23);
            this.MeiligaoVT300Button.TabIndex = 6;
            this.MeiligaoVT300Button.Text = "Meiligao.vt300";
            this.MeiligaoVT300Button.UseVisualStyleBackColor = true;
            this.MeiligaoVT300Button.Click += new System.EventHandler(this.MeiligaoButton_Click);
            // 
            // MeiligaoMVT380Button
            // 
            this.MeiligaoMVT380Button.Location = new System.Drawing.Point(142, 71);
            this.MeiligaoMVT380Button.Name = "MeiligaoMVT380Button";
            this.MeiligaoMVT380Button.Size = new System.Drawing.Size(88, 23);
            this.MeiligaoMVT380Button.TabIndex = 6;
            this.MeiligaoMVT380Button.Text = "Meiligao.MV380";
            this.MeiligaoMVT380Button.UseVisualStyleBackColor = true;
            this.MeiligaoMVT380Button.Click += new System.EventHandler(this.MeiligaoMVT380Button_Click);
            // 
            // btnGV100
            // 
            this.btnGV100.Location = new System.Drawing.Point(142, 100);
            this.btnGV100.Name = "btnGV100";
            this.btnGV100.Size = new System.Drawing.Size(88, 23);
            this.btnGV100.TabIndex = 6;
            this.btnGV100.Text = "GV100";
            this.btnGV100.UseVisualStyleBackColor = true;
            this.btnGV100.Click += new System.EventHandler(this.btnGV100_Click);
            // 
            // btnGPS680
            // 
            this.btnGPS680.Location = new System.Drawing.Point(142, 130);
            this.btnGPS680.Name = "btnGPS680";
            this.btnGPS680.Size = new System.Drawing.Size(88, 23);
            this.btnGPS680.TabIndex = 6;
            this.btnGPS680.Text = "GPS680";
            this.btnGPS680.UseVisualStyleBackColor = true;
            this.btnGPS680.Click += new System.EventHandler(this.btnGPS680_Click);
            // 
            // _naviTechButton
            // 
            this._naviTechButton.Location = new System.Drawing.Point(142, 160);
            this._naviTechButton.Name = "_naviTechButton";
            this._naviTechButton.Size = new System.Drawing.Size(88, 23);
            this._naviTechButton.TabIndex = 7;
            this._naviTechButton.Text = "NaviTech";
            this._naviTechButton.UseVisualStyleBackColor = true;
            this._naviTechButton.Click += new System.EventHandler(this._naviTechButton_Click);
            // 
            // _buttonJB100
            // 
            this._buttonJB100.Location = new System.Drawing.Point(142, 188);
            this._buttonJB100.Name = "_buttonJB100";
            this._buttonJB100.Size = new System.Drawing.Size(88, 23);
            this._buttonJB100.TabIndex = 8;
            this._buttonJB100.Text = "JB100";
            this._buttonJB100.UseVisualStyleBackColor = true;
            this._buttonJB100.Click += new System.EventHandler(this._buttonJB100_Click);
            // 
            // _buttonStarLine
            // 
            this._buttonStarLine.Location = new System.Drawing.Point(142, 218);
            this._buttonStarLine.Name = "_buttonStarLine";
            this._buttonStarLine.Size = new System.Drawing.Size(88, 23);
            this._buttonStarLine.TabIndex = 9;
            this._buttonStarLine.Text = "StarLine";
            this._buttonStarLine.UseVisualStyleBackColor = true;
            this._buttonStarLine.Click += new System.EventHandler(this._buttonStarLine_Click);
            // 
            // _btnNeva
            // 
            this._btnNeva.Location = new System.Drawing.Point(263, 13);
            this._btnNeva.Name = "_btnNeva";
            this._btnNeva.Size = new System.Drawing.Size(75, 23);
            this._btnNeva.TabIndex = 10;
            this._btnNeva.Text = "Neva";
            this._btnNeva.UseVisualStyleBackColor = true;
            this._btnNeva.Click += new System.EventHandler(this._btnNeva_Click);
            // 
            // _btnMJ
            // 
            this._btnMJ.Location = new System.Drawing.Point(263, 42);
            this._btnMJ.Name = "_btnMJ";
            this._btnMJ.Size = new System.Drawing.Size(75, 23);
            this._btnMJ.TabIndex = 11;
            this._btnMJ.Text = "MJ";
            this._btnMJ.UseVisualStyleBackColor = true;
            this._btnMJ.Click += new System.EventHandler(this._btnMJClick);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(263, 71);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 12;
            this.button6.Text = "GPS608 V2";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(263, 100);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 13;
            this.button7.Text = "GPS103A";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(263, 130);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 14;
            this.button8.Text = "DeerCollar";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // _fort300Button
            // 
            this._fort300Button.Location = new System.Drawing.Point(263, 160);
            this._fort300Button.Name = "_fort300Button";
            this._fort300Button.Size = new System.Drawing.Size(75, 23);
            this._fort300Button.TabIndex = 14;
            this._fort300Button.Text = "FORT300";
            this._fort300Button.UseVisualStyleBackColor = true;
            this._fort300Button.Click += new System.EventHandler(this._fort300ButtonClick);
            // 
            // _sonimButton
            // 
            this._sonimButton.Location = new System.Drawing.Point(263, 189);
            this._sonimButton.Name = "_sonimButton";
            this._sonimButton.Size = new System.Drawing.Size(75, 23);
            this._sonimButton.TabIndex = 15;
            this._sonimButton.Text = "Sonim";
            this._sonimButton.UseVisualStyleBackColor = true;
            this._sonimButton.Click += new System.EventHandler(this._sonimButton_Click);
            // 
            // _azimuthButton
            // 
            this._azimuthButton.Location = new System.Drawing.Point(263, 218);
            this._azimuthButton.Name = "_azimuthButton";
            this._azimuthButton.Size = new System.Drawing.Size(75, 23);
            this._azimuthButton.TabIndex = 15;
            this._azimuthButton.Text = "AzimuthGSM";
            this._azimuthButton.UseVisualStyleBackColor = true;
            this._azimuthButton.Click += new System.EventHandler(this._azimuthButton_Click);
            // 
            // _universal
            // 
            this._universal.Location = new System.Drawing.Point(345, 13);
            this._universal.Name = "_universal";
            this._universal.Size = new System.Drawing.Size(75, 23);
            this._universal.TabIndex = 16;
            this._universal.Text = "Universal";
            this._universal.UseVisualStyleBackColor = true;
            this._universal.Click += new System.EventHandler(this._universalClick);
            // 
            // _avtofonDevice
            // 
            this._avtofonDevice.Location = new System.Drawing.Point(344, 42);
            this._avtofonDevice.Name = "_avtofonDevice";
            this._avtofonDevice.Size = new System.Drawing.Size(75, 23);
            this._avtofonDevice.TabIndex = 15;
            this._avtofonDevice.Text = "Avtofon";
            this._avtofonDevice.UseVisualStyleBackColor = true;
            this._avtofonDevice.Click += new System.EventHandler(this._azimuthButton_Click);
            // 
            // _egtsButton
            // 
            this._egtsButton.Location = new System.Drawing.Point(344, 71);
            this._egtsButton.Name = "_egtsButton";
            this._egtsButton.Size = new System.Drawing.Size(75, 23);
            this._egtsButton.TabIndex = 15;
            this._egtsButton.Text = "EGTS";
            this._egtsButton.UseVisualStyleBackColor = true;
            this._egtsButton.Click += new System.EventHandler(this._egtsButton_Click);
            // 
            // _wialonButton
            // 
            this._wialonButton.Location = new System.Drawing.Point(345, 100);
            this._wialonButton.Name = "_wialonButton";
            this._wialonButton.Size = new System.Drawing.Size(75, 23);
            this._wialonButton.TabIndex = 15;
            this._wialonButton.Text = "Wialon";
            this._wialonButton.UseVisualStyleBackColor = true;
            this._wialonButton.Click += new System.EventHandler(this._wialonButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 266);
            this.Controls.Add(this._universal);
            this.Controls.Add(this._egtsButton);
            this.Controls.Add(this._wialonButton);
            this.Controls.Add(this._avtofonDevice);
            this.Controls.Add(this._azimuthButton);
            this.Controls.Add(this._sonimButton);
            this.Controls.Add(this._fort300Button);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this._btnMJ);
            this.Controls.Add(this._btnNeva);
            this.Controls.Add(this._buttonStarLine);
            this.Controls.Add(this._buttonJB100);
            this.Controls.Add(this._naviTechButton);
            this.Controls.Add(this.btnGPS680);
            this.Controls.Add(this.btnGV100);
            this.Controls.Add(this.MeiligaoMVT380Button);
            this.Controls.Add(this.MeiligaoVT300Button);
            this.Controls.Add(this.teltonikaButton);
            this.Controls.Add(this.TR203Button);
            this.Controls.Add(this.symbianS60);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.m2mButton);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button m2mButton;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button symbianS60;
        private System.Windows.Forms.Button TR203Button;
        private System.Windows.Forms.Button teltonikaButton;
        private System.Windows.Forms.Button MeiligaoVT300Button;
        private System.Windows.Forms.Button MeiligaoMVT380Button;
        private System.Windows.Forms.Button btnGV100;
        private System.Windows.Forms.Button btnGPS680;
        private System.Windows.Forms.Button _naviTechButton;
        private System.Windows.Forms.Button _buttonJB100;
        private System.Windows.Forms.Button _buttonStarLine;
        private System.Windows.Forms.Button _btnNeva;
        private System.Windows.Forms.Button _btnMJ;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button _fort300Button;
        private System.Windows.Forms.Button _sonimButton;
        private System.Windows.Forms.Button _azimuthButton;
        private System.Windows.Forms.Button _universal;
        private System.Windows.Forms.Button _avtofonDevice;
        private System.Windows.Forms.Button _egtsButton;
        private System.Windows.Forms.Button _wialonButton;

    }
}

