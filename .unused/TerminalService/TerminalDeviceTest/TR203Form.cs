﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Terminal.GlobalSat.TR203;

namespace TerminalDeviceTest
{
    class TR203Form : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var tr203 = new TR203();
            byte[] data = hexData;
            if (!tr203.SupportData(data))
            {
                data = Encoding.ASCII.GetBytes(originalInputString);
                if (!tr203.SupportData(data))
                    return "Not supported data";

            }
            /*if (!TR203.CheckCRC(s))
                return "Invalid CRC";
             */ 

            byte[] buff;
            var res = tr203.OnData(data, data.Length, null, out buff);
            if (res == null)
                return "No parsed data";

            var sb = new StringBuilder(100);
            Helper.Append(sb, res, typeof(TR203Sensor));

            return sb.ToString();
        }
    }
}
