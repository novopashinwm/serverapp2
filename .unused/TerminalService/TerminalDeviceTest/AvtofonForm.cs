﻿using RU.NVG.Terminal.Avtofon;

namespace TerminalDeviceTest
{
    class AvtofonForm : BaseTestForm
    {
        protected override System.Collections.IEnumerable GetDevices(string s, byte[] stringData, byte[] hexData, string originalInputString, string deviceID)
        {
            var device = new AvtofonDevice();

            byte[] bufferRest;
            return device.OnData(hexData, hexData.Length, null, out bufferRest);
        }
    }
}
