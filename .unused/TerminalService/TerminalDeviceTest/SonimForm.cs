﻿using System;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Terminal.Sonim;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class SonimForm : BaseTestForm
    {
        private class DummyHttpRequest : IHttpRequest
        {
            private readonly DateTime _time = DateTime.UtcNow;
            private string _text;
            private static readonly Uri Uri = new Uri("http://www.nika-glonass.ru/terminal/sonim");

            public DummyHttpRequest(string text)
            {
                _text = text;
            }

            public Uri RequestUrl
            {
                get { return Uri; }
            }

            public HttpMethod Method
            {
                get { return HttpMethod.Get; }
            }

            public DateTime TimeReceived
            {
                get { return _time; }
            }

            public string Requestor
            {
                get { return null; }
            }

            public string Text
            {
                get { return _text; }
            }

            public string XNikaDevice { get { return null; } }
            public string XNikaDeviceID { get { return null; } }
        }

        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var sonim = new Sonim();
            var dummyHttpRequest = new DummyHttpRequest(s);
            if (!sonim.CanProcess(dummyHttpRequest))
                return "Can't process";


            var list = sonim.OnData(dummyHttpRequest);

            if (list == null)
                return "OnData returned null";

            var response = (HttpResponse)list.FirstOrDefault(o => o is HttpResponse);

            var sb = new StringBuilder();
            if (response != null)
            {
                sb.Append("Response string: ");
                sb.AppendLine(response.Text);
            }            
            foreach (var item in list )
            {
                var mu = item as IMobilUnit;
                if (mu == null)
                {
                    sb.Append("Not IMobilUnit: ");
                    sb.AppendLine(item.ToString());
                    continue;
                }

                sb.Append(mu, typeof(Sensors));
            }

            return sb.ToString();
        }
    }
}
