﻿using System;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal.GlobalOrient;

namespace TerminalDeviceTest
{
	public partial class GlobalOrient : Form
	{
		public GlobalOrient()
		{
			InitializeComponent();
		}

		private byte[] ParseInputString()
		{
			byte[] data;
			string dataStr = txtInput.Text.Replace(" ", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
			if (!dataStr.StartsWith("##"))
				data = StringHelper.HexStringToByteArray(dataStr.Replace(":", "").Replace("-", ""));
			else
				data = Encoding.ASCII.GetBytes(dataStr);

			return data;
		}

		private void btnPrecessV16_Click(object sender, EventArgs e)
		{
			byte[] data = ParseInputString();
			byte[] bufferRest;
			var ga = new FORIS.TSS.Terminal.GlobalOrient.GlobalOrient();
			ga.OnData(data, data.Length, new GlobalOrientState {DeviceID = Encoding.ASCII.GetString(new byte[] {12, 23, 34})},
								 out bufferRest);
		}
	}
}