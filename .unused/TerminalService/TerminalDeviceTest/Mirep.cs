﻿using System;
using System.Linq;
using System.Text;
using FORIS.TSS.Terminal.Mirep;

namespace TerminalDeviceTest
{
    class Mirep : BaseTestForm
    {
        private static readonly MirepReader Reader = new MirepReader();

        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var mirepDevice = new FORIS.TSS.Terminal.Mirep.Mirep();
            var sb = new StringBuilder();
            sb.AppendLine();

            if (!mirepDevice.SupportData(stringData))
            {
                sb.AppendLine("Data not supported: " + s);
            }
            else
            {
                foreach (var s1 in s.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    try
                    {
                        var datagram = Reader.Read(s1);
                        if (datagram == null)
                            continue;
                        var mu = datagram.ToMobilUnit();
                        Helper.Append(sb, mu);
                    }
                    catch (Exception ex)
                    {
                        sb.Append(ex);
                    }
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
    }
}
