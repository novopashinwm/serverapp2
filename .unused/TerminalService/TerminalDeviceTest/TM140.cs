﻿using System;
using System.Windows.Forms;
using FORIS.TSS.MobilUnit.Unit;

namespace TerminalDeviceTest
{
    public partial class TM140 : Form
    {
        public TM140()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            byte[] data = Helper.ParseString(txtInput.Text);
            var dev = new FORIS.TSS.Terminal.TM140.TM140();
            byte[] bufferRest;
            object state=null;

            if (!dev.SupportData(data))
            {
                txtRes.Text = "Data is not supported";
                return;
            }

            var list = dev.OnData(data, data.Length, state, out bufferRest);

            string res = "";
            if (list!=null && list.Count > 0)
            {
                foreach (object o in list)
                {
                    if (o is MobilUnit)
                    {
                        MobilUnit mu = o as MobilUnit;
                        res += string.Format("id={0}", mu.DeviceID) + "\r\n";
                    }
                }
            }
            else
            {
                res += "Нет данных";
            }
            res += "\r\n";
            txtRes.Text = res;

        }
    }
}
