﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TerminalDeviceTest
{
    public partial class GV100 : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new FORIS.TSS.Terminal.GV100.GV100();
            var sb = new StringBuilder();
            sb.AppendLine();

            if (!device.SupportData(hexData))
            {
                sb.AppendLine("Data not supported: " + s);
            }
            else
            {
                byte[] restBuffer;
                var res = device.OnData(hexData, 0, new object(), out restBuffer);
                var i = res.Count;

                foreach (object o in res)
                {
                    if (o != null && o is FORIS.TSS.MobilUnit.Unit.MobilUnit)
                    {
                        Helper.Append(sb, o as FORIS.TSS.MobilUnit.Unit.MobilUnit);
                    }
                }
            }
            return sb.ToString();
        }
    }
}
