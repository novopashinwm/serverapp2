﻿using System.Text;
using FORIS.TSS.Terminal.Teltonika.FM3101;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class TeltonikaForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            byte[] bufferRest;
            var fM3101Device = new TeltonikaDevice();
            var list = fM3101Device.Parse(hexData, out bufferRest);
            var sb = new StringBuilder(hexData.Length);
            foreach (var @object in list)
            {
                var mu = @object as IMobilUnit;
                if (mu == null)
                    continue;
                Helper.Append(sb, mu);
            }
            return sb.ToString();
        }
    }
}
