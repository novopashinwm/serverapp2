﻿using System.Text;

namespace TerminalDeviceTest
{
    public partial class GPS608 : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var device = new FORIS.TSS.Terminal.GPS608.JB101();
            var sb = new StringBuilder();
            sb.AppendLine();

            if (!device.SupportData(hexData))
            {
                sb.AppendLine("Data not supported: " + s);
            }
            else
            {
                byte[] restBuffer;
                var res = device.OnData(hexData, 0, new object(), out restBuffer);

                foreach (object o in res)
                {
                    var mu = o as FORIS.TSS.MobilUnit.Unit.MobilUnit;
                    if (mu == null) continue;
                    Helper.Append(sb, mu, typeof(FORIS.TSS.Terminal.GPS608.BaseDatagram.Sensors));
                }
            }
            return sb.ToString();
        }
    }
}
