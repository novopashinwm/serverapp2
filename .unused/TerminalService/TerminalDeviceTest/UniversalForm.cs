﻿using System.Linq;
using System.Text;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
	class UniversalForm : BaseTestForm
	{
		private IDevice _device;
		private object _stateData;
		private bool _useStringData;

		protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
		{
			byte[] data = _useStringData ? stringData : hexData;

			if (_device == null)
			{
				_device = FindDevice(stringData);

				if (_device != null)
				{
					data = stringData;
					_useStringData = true;
				}
				else
				{
					_device = FindDevice(hexData);
					if (_device == null)
						return "Device not found";
					data = hexData;
				}
			}

			var sb = new StringBuilder();

			sb.AppendLine(_device.GetType().Name);

			byte[] restBuffer;
			var res = _device.OnData(data, data.Length, _stateData, out restBuffer);

			if (restBuffer != null && restBuffer.Length > 0)
				sb.AppendFormat("restBuffer = {0}", StringHelper.SmartFormatBytes(restBuffer));

			if (res == null)
			{
				sb.Append("null");
			}
			else
			{
				foreach (object o in res)
				{
					var mu = o as MobilUnit;
					if (mu != null)
					{
						sb.Append(mu, _device.SensorEnumType);
						sb.AppendLine();
						continue;
					}
					var receiverStateMessage = o as ReceiverStoreToStateMessage;
					if (receiverStateMessage != null)
					{
						_stateData = receiverStateMessage.StateData;
						sb.AppendLine("State: " + _stateData);
						continue;
					}
					var confirm = o as ConfirmPacket;
					if (confirm != null)
					{
						sb.AppendLine("Confirm: " + StringHelper.SmartFormatBytes(confirm.Data));
						continue;
					}

					var notification = o as NotifyEventArgs;
					if (notification != null)
					{
						sb.AppendLine("Notification " + notification.Event + ": " + notification.Tag.OfType<CommandResult>().Select(cr => cr.ToString()).Join(", "));
					}
					sb.AppendLine("Unknown item type: " + o.GetType());
				}
			}
			return sb.ToString();
		}

		private IDevice FindDevice(byte[] hexData)
		{
			var terminalManager = new TerminalManager();
			var result = terminalManager.GetDevice(hexData);
			return result;
		}
	}
}
