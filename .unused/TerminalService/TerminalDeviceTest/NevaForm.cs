﻿using System.Text;
using FORIS.TSS.Terminal.Neva;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class NevaForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            if (Neva104GSM.IsRegistrationPacket(hexData))
            {
                return "Registration packet";
            }

            byte[] bufferRest;
            var items = new Neva104GSM().OnData(hexData, hexData.Length, null, out bufferRest);

            var result = new StringBuilder();
            foreach (var item in items)
            {
                var device = item as IMobilUnit;
                if (device == null)
                    continue;
                Helper.Append(result, device);
                result.AppendLine();
            }
            return result.ToString();
        }
    }
}
