﻿using System;
using System.Windows.Forms;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal.TM4;

namespace TerminalDeviceTest
{
	public partial class TM4 : Form
	{
		public TM4()
		{
			InitializeComponent();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			ParseTM4();
		}

		protected void ParseTM4()
		{
			/*
			07 
			3b830c00 - id
			6e8a5012 - time (sec) from 2000.1.1
			948bcd05 - lat
			b643e903 - lng
			000000 - add data
			08 - flags (0 bit - GPS correct)
			3700 - speed
			d00d - course
			d9050000 - add data
			00000000 - alarm button last time
			00 - digital sensors
			00
			00
			00
			00  
			00
			6a
			0c
			b1de - CRC                                  
			 */
			//byte[] = Convert.
			byte[] data = StringHelper.HexStringToByteArray(txtInput.Text.Replace(" ", ""));
			TM4Datagram dg = new TM4Datagram(data);
			txtRes.Text = "";
			txtRes.Text = string.Format("ID={0}", dg.ID);
			txtRes.Text += "\r\n" + "Time=" + FORIS.TSS.BusinessLogic.TimeHelper.GetDateTimeUTC(dg.Time).ToString();
			txtRes.Text += "\r\n" + string.Format("lat={0}, lng={1}", dg.Latitude, dg.Longitude);
			txtRes.Text += "\r\n" + string.Format("Speed={0}", dg.Speed);
			txtRes.Text += "\r\n" + string.Format("Course={0}", dg.Course);
			txtRes.Text += "\r\n" + string.Format("CorrectGPS={0}", dg.CorrectGPS);
			txtRes.Text += "\r\n" + string.Format("Alarm={0}", dg.Alarm);
			txtRes.Text += "\r\n" + string.Format("IsDigitalSensors={0}", dg.IsDigitalSensors);
		}
	}
}