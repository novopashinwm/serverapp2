﻿using System;
using System.Text;
using FORIS.TSS.Terminal.StarLine;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalDeviceTest
{
    class StarLine : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var starLineDevice = new StarLineM10();

            byte[] registrationPacket = null;
            if (hexData.Length == 19 || hexData.Length == 19 + 34) 
            {
                registrationPacket = new byte[19];
                Array.Copy(hexData, registrationPacket, registrationPacket.Length);
                if (!starLineDevice.SupportData(registrationPacket))
                    return "Data not supported";
            }
            var result = new StringBuilder();


            byte[] workingPacket;
            byte[] bufferRest;
            ReceiverStoreToStateMessage stateMessage = null;
            if (registrationPacket != null)
            {
                var headerParsedData = starLineDevice.OnData(registrationPacket, registrationPacket.Length, null, out bufferRest);
                foreach (var item in headerParsedData)
                {
                    stateMessage = item as ReceiverStoreToStateMessage;
                    if (stateMessage != null)
                        break;
                }

                if (19 < hexData.Length)
                {
                    workingPacket = new byte[34];
                    Array.Copy(hexData, 19, workingPacket, 0, workingPacket.Length);
                }
                else
                {
                    workingPacket = null;
                }
            }
            else
            {
                workingPacket = hexData;
            }

            if (workingPacket == null)
                return "No data";

            var items = starLineDevice.OnData(workingPacket, workingPacket.Length, stateMessage != null ? stateMessage.StateData : null, out bufferRest);

            if (items != null)
            {
                foreach (var item in items)
                {
                    var mobilUnit = item as IMobilUnit;
                    if (mobilUnit == null)
                        continue;
                    Helper.Append(result, mobilUnit);
                    result.AppendLine();
                }
            }
            return result.ToString();
        }
    }
}
