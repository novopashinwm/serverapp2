﻿using System.IO;
using System.Text;
using RU.NVG.Protocol.Egts.Helpers;

namespace TerminalDeviceTest
{
    class EgtsTestForm : BaseTestForm
    {
        protected override string Parse(string s, byte[] stringData, byte[] hexData, string originalInputString)
        {
            var memoryStream = new MemoryStream(stringData);
            var egtsReader = new EgtsReader(memoryStream);
            var packet = egtsReader.ReadPacket();

            var sb = new StringBuilder();
            sb.AppendLine(packet.ServiceFrame.Type.ToString());
            
            foreach (var record in packet.ServiceFrame.Records)
            {
                int i = 0;
                foreach (var subRecord in record.GetRecords())
                {
                    sb.AppendFormat("record[{0}].SubRecord = {1}\n", i, subRecord);
                    ++i;
                }
            }

            return sb.ToString();
        }
    }
}