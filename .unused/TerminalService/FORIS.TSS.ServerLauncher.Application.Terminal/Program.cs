﻿using System;

namespace FORIS.TSS.ServerLauncher.Application.Terminal
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			/// Приложение сервера конфигурируется при старте домена приложения сервера
			TrayIconApplicationContext.RunInteractive("Terminal Server", false, true, false);
		}
	}
}