﻿using System.Collections;
using FORIS.TSS.Common;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class F52Record
	{
		/// <summary>
		/// Статус охраны / режим тревожного оповещения
		/// </summary>
		private class ProtectionStatus
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// 0 – наблюдение (нет охраны);
			/// 1 - профиль охраны №1 (полная охрана); 
			/// 2 - профиль охраны №2.
			/// </summary>
			public byte ProtectionProfile
			{
				get { return (byte) ((_arr[1] ? 1 : 0) + (_arr[2] ? 2 : 0)); }
			}

			/// <summary>
			/// 0 – рабочий режим;
			/// 1 – тестовый режим
			/// </summary>
			public bool Mode1
			{
				get { return _arr[6]; }
			}

			/// <summary>
			/// 0 – штатная работа;
			/// 1 – режим тревожного оповещения
			/// </summary>
			public bool Mode2
			{
				get { return _arr[7]; }
			}

			public byte Value
			{
				get
				{
					var buf = new byte[1];
					_arr.CopyTo(buf, 0);
					return buf[0];
				}
				set
				{
					_arr = new BitArray(new byte[value]);
				}
			}
		}

		/// <summary>
		/// Текущий статус функциональных модулей
		/// </summary>
		private class FunctionalState
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// 0 – GSM модем выключен; 
			/// 1 –GSM – модем включен.
			/// </summary>
			public bool GsmModemState
			{
				get { return _arr[0]; }
			}

			/// <summary>
			/// 0 – USB не подключен; 
			/// 1 – USB подключен.
			/// </summary>
			public bool UsbState
			{
				get { return _arr[1]; }
			}

			/// <summary>
			/// 0 – часы не синхронизированы по SMS;
			/// 1- часы синхронизированы по SMS.
			/// </summary>
			public bool ClockSmsSyncState
			{
				get { return _arr[2]; }
			}

			/// <summary>
			/// 0 – часы не синхронизированы по GPS; 
			/// 1 – часы не синхронизированы по GPS.
			/// </summary>
			public bool ClockGpsSyncState
			{
				get { return _arr[3]; }
			}

			/// <summary>
			/// 0 – нет регистрации в сотовой сети; 
			/// 1 – есть регистрация в сотовой сети.
			/// </summary>
			public bool GsmRegistrationState
			{
				get { return _arr[5]; }
			}

			/// <summary>
			/// 0 – домашняя сотовая сеть; 
			/// 1 – роуминг.
			/// </summary>
			public bool RoamingState
			{
				get { return _arr[6]; }
			}

			/// <summary>
			/// 0 – зажигание выключено; 
			/// 1 – зажигание включено.
			/// </summary>
			public bool IgnitionState
			{
				get { return _arr[7]; }
			}

			public byte Value
			{
				get
				{
					var buf = new byte[1];
					_arr.CopyTo(buf, 0);
					return buf[0];
				}
				set
				{
					_arr = new BitArray(new byte[value]);
				}
			}
		}

		/// <summary>
		/// Битовое поле:
		/// 0: 1-й выход;
		/// 1: 2-ой выход;
		/// 2-7: количество спутников;
		/// 0- выход выключен;
		/// 1- выход работает.
		/// </summary>
		private class OutputAndSattelitesCount
		{
			public byte Value { get; set; }

			/// <summary>
			/// 0- выход выключен;
			/// 1- выход работает.
			/// </summary>
			public bool OutputLine1
			{
				get { return (Value & 0x1) > 0; }
			}

			/// <summary>
			/// 0- выход выключен;
			/// 1- выход работает.
			/// </summary>
			public bool OutputLine2
			{
				get { return (Value & 0x2) > 0; }
			}

			/// <summary>
			/// количество спутников;
			/// </summary>
			public byte Sattelites
			{
				get { return (byte) (Value & 0xfc); }
			}
		}

		/// <summary>
		/// 0- датчик в нормальном состоянии; 
		/// 1 — датчик сработал.
		/// </summary>
		private class SensorValues
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// вход зажигания;
			/// </summary>
			public bool Ignition
			{
				get { return _arr[0]; }
			}

			/// <summary>
			/// вход тревожной кнопки;
			/// </summary>
			public bool Alarm
			{
				get { return _arr[1]; }
			}

			/// <summary>
			/// вход датчика дверей;
			/// </summary>
			public bool Door
			{
				get { return _arr[2]; }
			}

			/// <summary>
			/// вход А1;
			/// </summary>
			public bool A1
			{
				get { return _arr[3]; }
			}

			/// <summary>
			/// вход А2;
			/// </summary>
			public bool A2
			{
				get { return _arr[4]; }
			}

			/// <summary>
			/// датчик слабого удара;
			/// </summary>
			public bool LowKick
			{
				get { return _arr[5]; }
			}

			/// <summary>
			/// датчик сильного удара;
			/// </summary>
			public bool HighKick
			{
				get { return _arr[6]; }
			}

			/// <summary>
			/// датчик наклона/перемещения;
			/// </summary>
			public bool Accel
			{
				get { return _arr[7]; }
			}

			public byte Value
			{
				get
				{
					var buf = new byte[1];
					_arr.CopyTo(buf, 0);
					return buf[0];
				}
				set
				{
					_arr = new BitArray(new byte[value]);
				}
			}
		}

		/// <summary>
		/// Разряды Значения
		/// 0 0 – навигационные приемник включен;
		/// 1 – навигационный приемник выключен.
		/// 1 0 – невалидная навигация;
		/// 1 – валидная навигация.
		/// 2..3 1 – GPS 2 – ГЛОНАСС 3 – ГЛОНАСС /GPS
		/// </summary>
		private class NavigationState
		{
			/// <summary>
			/// 0 – навигационные приемник включен;
			/// 1 – навигационный приемник выключен.
			/// </summary>
			public bool NavigationOn
			{
				get { return (Value & 0x1) == 0; }
			}

			/// <summary>
			/// 0 – невалидная навигация;
			/// 1 – валидная навигация.
			/// </summary>
			public bool NavigationValid
			{
				get { return (Value & 0x2) > 1; }
			}

			/// <summary>
			/// 1 – GPS
			/// 2 – ГЛОНАСС
			/// 3 – ГЛОНАСС /GPS
			/// </summary>
			public byte NavigationSystem
			{
				get { return (byte) (Value & 0xc); }
			}

			public byte Value { get; set; }
		}

		/// <summary>
		/// Сквозной номер записи в энергонезависимой памяти
		/// </summary>
		private uint _recordId;

		/// <summary>
		/// Код события, соответствующий данной записи
		/// </summary>
		private ushort _eventId;

		/// <summary>
		/// Время события (формирования записи) на бортовом устройстве
		/// Час 0 – 23
		/// </summary>
		private byte _hour;

		/// Минута 0 – 59
		private byte _minute;

		/// Секунда 0 – 59
		private byte _second;

		/// День 1 – 31
		private byte _day;

		/// Месяц 0 – 11
		private byte _month;

		/// Год 0 – 255 (с 2000 года)
		private byte _year;

		/// <summary>
		/// Статус охраны / режим тревожного оповещения
		/// </summary>
		private readonly ProtectionStatus _protectionStatus = new ProtectionStatus();

		/// <summary>
		/// Текущий статус функциональных модулей
		/// </summary>
		private readonly FunctionalState _functionalState = new FunctionalState();

		/// <summary>
		/// Уровень GSM
		/// </summary>
		private byte _gsmLevel;

		/// <summary>
		/// Текущее состояние выходных линий (2шт) / количество навигационных спутников
		/// </summary>
		private readonly OutputAndSattelitesCount _outputAndSattelites = new OutputAndSattelitesCount();
 
		/// <summary>
		/// Текущие показания датчиков (8 шт.)
		/// </summary>
		private readonly SensorValues _sensorValues = new SensorValues();

		/// <summary>
		/// Напряжение на основном источнике питания
		/// </summary>
		private ushort _mainAccVoltage;

		/// <summary>
		/// Напряжение на резервном источнике питания
		/// </summary>
		private ushort _secondAccVoltage;

		/// <summary>
		/// Напряжение на аналоговом входе 1 (A1)
		/// </summary>
		private ushort _analogueInput1Voltage;

		/// <summary>
		/// Напряжение на аналоговом входе 2 (A2)
		/// </summary>
		private ushort _analogueInput2Voltage;

		/// <summary>
		/// Показания счетчика импульсов 1 (I2)
		/// </summary>
		private uint _impulseCounter1;

		/// <summary>
		/// Показания счетчика импульсов 2 (I3)
		/// </summary>
		private uint _impulseCounter2;

		/// <summary>
		/// Состояние навигационного датчика
		/// </summary>
		private readonly NavigationState _navigationState = new NavigationState();

		/// <summary>
		/// Время и дата последних валидных данных:
		/// Час 0 – 23
		/// </summary>
		private byte _validPositionHour;

		/// Минута 0 – 59
		private byte _validPositionMinute;

		/// Секунда 0 – 59
		private byte _validPositionSecond;

		/// День 1 – 31
		private byte _validPositionDay;

		/// Месяц 0 – 11
		private byte _validPositionMonth;

		/// Год 0 – 255 (с 2000 года)
		private byte _validPositionYear;

		/// <summary>
		/// Последняя валидная широта
		/// </summary>
		private float _validLatitude;

		/// <summary>
		/// Последняя валидная долгота
		/// </summary>
		private float _validLongitude;

		/// <summary>
		/// Скорость, зафиксированная при 
		/// получении последних валидных координат. В км/ч
		/// </summary>
		private float _speed;

		/// <summary>
		/// Курс, зафиксированный при получении
		/// последних валидных координат. 0° … 360°
		/// </summary>
		private ushort _cource;

		/// <summary>
		/// Пробег, зафиксированный на момент события, вычисляющийся во время
		/// поступления валидных навигационных данных. В км.
		/// </summary>
		private float _odometer;

		/// <summary>
		/// Пробег, рассчитанный между данным событием и предыдущим. 
		/// (между двумя точками трека) В км.
		/// </summary>
		private float _lastDistance;

		/// <summary>
		/// Общее количество точек вычисления 
		/// навигационным приемником координат с темпом раз в секунду.
		/// </summary>
		private ushort _lastSeconds;

		/// <summary>
		/// Количество точек вычисления навигационным приемником координат
		/// с темпом раз в секунду при валидных навигационных данных.
		///  </summary>
		private ushort _lastDistanceSeconds;

		/// <summary>
		/// Значение частоты для цифрового датчика 1
		/// </summary>
		private ushort _fuelSensor1Freq;

		/// <summary>
		/// Значение температуры для цифрового датчика 1
		/// </summary>
		private byte _fuelSensor1Temperature;

		/// <summary>
		/// Значение относительного уровня для цифрового датчика 1
		/// </summary>
		private ushort _fuelSensor1Level;

		/// <summary>
		/// Значение частоты для цифрового датчика 2
		/// </summary>
		private ushort _fuelSensor2Freq;

		/// <summary>
		/// Значение температуры для цифрового датчика 2
		/// </summary>
		private byte _fuelSensor2Temperature;

		/// <summary>
		/// Значение относительного уровня для цифрового датчика 2
		/// </summary>
		private ushort _fuelSensor2Level;

		/// <summary>
		/// Значение частоты для цифрового датчика 3
		/// </summary>
		private ushort _fuelSensor3Freq;

		/// <summary>
		/// Значение температуры для цифрового датчика 3
		/// </summary>
		private byte _fuelSensor3Temperature;

		/// <summary>
		/// Значение относительного уровня для цифрового датчика 3
		/// </summary>
		private ushort _fuelSensor3Level;

		/// <summary>
		/// Температура с цифрового датчика 1 (в градусах Цельсия)
		/// -55°С … +125°С (-128 °С — датчик не подключен)
		/// </summary>
		private byte _temperatureSensor1;

		/// <summary>
		/// Температура с цифрового датчика 2 (в градусах Цельсия)
		/// -55°С … +125°С (-128 °С — датчик не подключен)
		/// </summary>
		private byte _temperatureSensor2;

		/// <summary>
		/// Температура с цифрового датчика 3 (в градусах Цельсия)
		/// -55°С … +125°С (-128 °С — датчик не подключен)
		/// </summary>
		private byte _temperatureSensor3;

		/// <summary>
		/// Температура с цифрового датчика 4 (в градусах Цельсия)
		/// -55°С … +125°С (-128 °С — датчик не подключен)
		/// </summary>
		private byte _temperatureSensor4;

		public void ReadFrom(DataReader reader)
		{
			_recordId = reader.ReadLittleEndian(4);
			_eventId = (ushort) reader.ReadLittleEndian(2);
			_hour = reader.ReadByte();
			_minute = reader.ReadByte();
			_second = reader.ReadByte();
			_day = reader.ReadByte();
			_month = reader.ReadByte();
			_year = reader.ReadByte();
			_protectionStatus.Value = reader.ReadByte();
			_functionalState.Value = reader.ReadByte();
			_gsmLevel = reader.ReadByte();
			_outputAndSattelites.Value = reader.ReadByte();
			_sensorValues.Value = reader.ReadByte();
			_mainAccVoltage = (ushort) reader.ReadLittleEndian(2);
			_secondAccVoltage = (ushort) reader.ReadLittleEndian(2);
			_analogueInput1Voltage = (ushort) reader.ReadLittleEndian(2);
			_analogueInput2Voltage = (ushort) reader.ReadLittleEndian(2);
			_impulseCounter1 = reader.ReadLittleEndian(4);
			_impulseCounter2 = reader.ReadLittleEndian(4);
			_navigationState.Value = reader.ReadByte();
			_validPositionHour = reader.ReadByte();
			_validPositionMinute = reader.ReadByte();
			_validPositionSecond = reader.ReadByte();
			_validPositionDay = reader.ReadByte();
			_validPositionMonth = reader.ReadByte();
			_validPositionYear = reader.ReadByte();
			_validLatitude = reader.ReadSingle();
			_validLongitude = reader.ReadSingle();
			_speed = reader.ReadSingle();
			_cource = (ushort) reader.ReadLittleEndian(2);
			_odometer = reader.ReadSingle();
			_lastDistance = reader.ReadSingle();
			_lastSeconds = (ushort) reader.ReadLittleEndian(2);
			_lastDistanceSeconds = (ushort) reader.ReadLittleEndian(2);
			_fuelSensor1Freq = (ushort) reader.ReadLittleEndian(2);
			_fuelSensor1Temperature = reader.ReadByte();
			_fuelSensor1Level = (ushort) reader.ReadLittleEndian(2);
			_fuelSensor2Freq = (ushort)reader.ReadLittleEndian(2);
			_fuelSensor2Temperature = reader.ReadByte();
			_fuelSensor2Level = (ushort)reader.ReadLittleEndian(2);
			_fuelSensor3Freq = (ushort)reader.ReadLittleEndian(2);
			_fuelSensor3Temperature = reader.ReadByte();
			_fuelSensor3Level = (ushort)reader.ReadLittleEndian(2);
			_temperatureSensor1 = reader.ReadByte();
			_temperatureSensor2 = reader.ReadByte();
			_temperatureSensor3 = reader.ReadByte();
			_temperatureSensor4 = reader.ReadByte();
		}
	}
}