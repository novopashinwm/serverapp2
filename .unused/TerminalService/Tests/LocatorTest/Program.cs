﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.Terminal;

namespace LocatorTest
{
    class Program
    {
        private static readonly string YandexApiKey =
            "AF-HjE0BAAAA7NIHZgMA3wF-Z_isw1A5-Ji2L2kc-xtnPAUAAAAAAAAAAAD-vRJsoKdRjBb1XKCRsi7hlNz-Uw==";

        private static readonly string Url = "http://api.lbs.yandex.net/geolocation";

        static void Main(string[] args)
        {
            var db = new DataBaseManager();

            var dataForNetmonitoring = db.GetDataForNetmonitoring();

            Console.WriteLine(dataForNetmonitoring.GetXmlForYandex(YandexApiKey).ToString());
        }

        private static void TestYandexLbs(DataBaseManager db)
        {
            var dataForLocator = db.GetDataForLocator();
            //dataForLocator.Cells = null;

            //var dataForLocator = new DataBaseManager.DataForLocator()
            //                         {
            //                             Wlans = new []
            //                                         {
            //                                             "B6577B0B20BC",
            //                                             "B6578D11A0B8",
            //                                             "B6578D1C40AA",
            //                                             "B6577B0B21BC",
            //                                             "B6578D11A1B8",
            //                                             "B6578D1C41AA",

            //                                         }.Select(mac => new WlanRecord {WlanMacAddress = mac}).ToList()
            //                         };

            var message = GetRequestBody(dataForLocator);

            Console.WriteLine(message);

            ServicePointManager.Expect100Continue = false;

            var httpRequest = (HttpWebRequest) WebRequest.CreateDefault(new Uri(Url));
            httpRequest.Headers.Add("Accept-Encoding", "identity");
            httpRequest.Proxy = WebRequest.DefaultWebProxy;
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Method = "POST";
            httpRequest.Timeout = 10000;

            var body = Encoding.ASCII.GetBytes(message);
            var requestStream = httpRequest.GetRequestStream();
            requestStream.Write(body, 0, body.Length);
            requestStream.Close();


            HttpWebResponse loWebResponse; //GetProxySettings(loHttp); //

            try
            {
                loWebResponse = (HttpWebResponse) httpRequest.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Message == "The remote server returned an error: (404) Not Found.")
                {
                    Console.WriteLine("Not found (404)");
                    return;
                }

                Console.WriteLine(e.ToString());
                return;
            }


            foreach (string key in loWebResponse.Headers.Keys)
                Console.WriteLine("{0}: {1}", key, loWebResponse.GetResponseHeader(key));

            string s;
            var responseStream = loWebResponse.GetResponseStream();

            if (responseStream == null)
                return;

            using (var stringReader = new StreamReader(responseStream))
                s = stringReader.ReadToEnd();


            var doc = XDocument.Parse(s);

            if (doc.Root == null)
                return;

            var position = doc.Root.Descendants("position").FirstOrDefault();

            if (position == null)
                return;

            var latitude = XmlConvert.ToDecimal(position.Descendants("latitude").First().Value);
            var longitude = XmlConvert.ToDecimal(position.Descendants("longitude").First().Value);
            var altitude = XmlConvert.ToDecimal(position.Descendants("altitude").First().Value);
            var precision = XmlConvert.ToDecimal(position.Descendants("precision").First().Value);
            var type = position.Descendants("type").First().Value;

            Console.WriteLine("Type: {0}", type);
            Console.WriteLine("Coordinates: {0} {1}", latitude, longitude);
            Console.WriteLine("Accuracy: {0}", precision);
        }

        private static string GetRequestBody(DataForLocator dataForLocator)
        {
            var doc = new XDocument(
                new XElement("ya_lbs_request",
                             new XElement("common",
                                          new XElement("version", "1.0"),
                                          new XElement("api_key", YandexApiKey)),
                             dataForLocator.Cells == null
                                 ? null
                                 : new XElement("gsm_cells",
                                                dataForLocator.Cells.Select(
                                                    c => new XElement("cell",
                                                                      new XElement("countrycode", c.CountryCode),
                                                                      new XElement("operatorid", c.NetworkCode),
                                                                      new XElement("cellid", c.CellID),
                                                                      new XElement("lac", c.LAC)
                                                             //, new XElement("signal_strength", (-c.SignalStrength).ToString(CultureInfo.InvariantCulture))
                                                             ))),
                             dataForLocator.Wlans == null
                                 ? null
                                 : new XElement("wifi_networks",
                                                dataForLocator.Wlans.Select(
                                                    w => new XElement("network",
                                                                      new XElement("mac", w.WlanMacAddress)
                                                             //, new XElement("signal_strength", (-w.SignalStrength).ToString(CultureInfo.InvariantCulture))
                                                             ))
                                       )));

            var message = "xml=" + doc;
            return message;
        }
    }
}
