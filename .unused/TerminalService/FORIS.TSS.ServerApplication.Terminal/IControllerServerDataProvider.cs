﻿using FORIS.TSS.ServerApplication.Terminal.Data.Controller;

namespace FORIS.TSS.ServerApplication.Terminal
{
	public interface IControllerServerDataProvider :
		IDatabaseProvider
	{
		ControllerServerData ControllerData { get; }
	}
}