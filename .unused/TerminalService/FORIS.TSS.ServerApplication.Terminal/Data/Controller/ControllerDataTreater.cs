﻿
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Terminal.Data.Controller;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
	partial class ControllerDataTreater :
		DataTreater<IControllerData>,
		IControllerDataTreater
	{
		public ControllerDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IControllerData data,
			ISessionInfo sessionInfo
			) : base(databaseDataSupplier, data, sessionInfo)
		{

		}
	}
}