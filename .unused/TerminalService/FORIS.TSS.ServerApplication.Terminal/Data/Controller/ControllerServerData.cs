﻿using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.Helpers.Terminal.Data.Controller;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
	public class ControllerServerData :
		ControllerData
	{
		#region Constructor & Dispose

		public ControllerServerData(TerminalServer server)
		{
			this.server = server;

			DataSupplier = new ControllerDatabaseDataSupplier(server);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				RemotingServices.Disconnect(this);
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		private readonly TerminalServer server;

		public override object InitializeLifetimeService()
		{
			/* Ссылку на этот объект получает клиентское приложение.
			 * Объект отключается от ремотинга при вызове Dispose()
			 */

			return null;
		}

		public IControllerDataTreater GetTreater(ISessionInfo sessionInfo)
		{
			return
				new ControllerDataTreater(
					server.Database,
					this,
					sessionInfo
					);
		}
	}
}