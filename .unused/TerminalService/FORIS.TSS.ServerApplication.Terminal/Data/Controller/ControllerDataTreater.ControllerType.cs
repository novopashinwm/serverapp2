using System;
using FORIS.TSS.Helpers.Terminal.Data.Controller;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
    partial class ControllerDataTreater
    {
		public void ControllerTypeInsert(
			string name
			)
		{
			lock( this.Data )
			{
				try
				{
					ControllerTypeRow row = this.Data.ControllerType.NewRow();

					row.Name = name;

					this.Data.ControllerType.Rows.Add( row );
					
					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ControllerTypeUpdate(
			int controllerTypeId,
			string name
			)
		{
			lock( this.Data )
			{
				try
				{
					ControllerTypeRow row = this.Data.ControllerType.FindRow( controllerTypeId );

					row.BeginEdit();
					
					row.Name = name;

					row.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ControllerTypeDelete(
			int controllerTypeId
			)
		{
			lock( this.Data )
			{
				try
				{
					ControllerTypeRow row = this.Data.ControllerType.FindRow( controllerTypeId );

					row.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
    }
}
