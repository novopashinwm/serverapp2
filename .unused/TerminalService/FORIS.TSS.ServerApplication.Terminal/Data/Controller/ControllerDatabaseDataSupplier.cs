﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
	class ControllerDatabaseDataSupplier :
		DatabaseDataSupplier<TerminalServer>,
		IControllerDataSupplier
	{
		public ControllerDatabaseDataSupplier(TerminalServer server) : base(server)
		{
		}
		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerController",
					new string[] { "CONTROLLER_TYPE", "CONTROLLER", "CONTROLLER_INFO" }
					);

			return new DataInfo(data, Guid.NewGuid());
		}
		public IControllerDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}