﻿using System;
using FORIS.TSS.Helpers.Terminal.Data.Controller;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
	partial class ControllerDataTreater
	{
		public void ControllerInsert(
			int controllerType,
			string phone,
			int number,
			string firmware
			)
		{
			lock (this.Data)
			{
				try
				{
					ControllerRow row = this.Data.Controller.NewRow();

					row.ControllerType = controllerType;
					row.Phone          = phone;
					row.Number         = number;
					row.Firmware       = firmware;

					this.Data.Controller.Rows.Add(row);

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ControllerUpdate(
			int controllerId,
			int controllerType,
			string phone,
			int number,
			string firmware
			)
		{
			lock (this.Data)
			{
				try
				{
					ControllerRow row = this.Data.Controller.FindRow(controllerId);

					row.BeginEdit();

					row.ControllerType = controllerType;
					row.Phone = phone;
					row.Number = number;
					row.Firmware = firmware;

					row.EndEdit();

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ControllerDelete(
			int controllerId
			)
		{
			lock (this.Data)
			{
				try
				{
					ControllerRow row = this.Data.Controller.FindRow(controllerId);

					row.Delete();

					this.AcceptChanges();
				}
				catch (Exception)
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}