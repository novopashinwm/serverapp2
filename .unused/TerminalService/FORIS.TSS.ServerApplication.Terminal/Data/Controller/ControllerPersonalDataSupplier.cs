﻿using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.ServerApplication.Data;

namespace FORIS.TSS.ServerApplication.Terminal.Data.Controller
{
	class ControllerPersonalDataSupplier :
		PersonalDataSupplier<ControllerServerData, IControllerDataTreater>,
		IControllerDataSupplier
	{
		public ControllerPersonalDataSupplier(
			ControllerServerData dataSupplier,
			IPersonalServer personalServer
			)
			: base(dataSupplier, personalServer)
		{
		}
		public override IControllerDataTreater GetDataTreater()
		{
			return DataSupplier.GetTreater(PersonalServer.SessionInfo);
		}
	}
}