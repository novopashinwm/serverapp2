﻿using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceShadow.Terminal.Data.Controller
{
	public class ControllerDataSupplierFactory :
		ClientDataSupplierFactory<IControllerClientDataProvider, ControllerClientData, DataParams>
	{
		public ControllerDataSupplierFactory(IControllerClientDataProvider client)
			: base(client)
		{
		}

		protected override ControllerClientData CreateDataSupplier(DataParams @params)
		{
			if (@params != DataParams.Empty)
			{
				throw new ArgumentException();
			}

			ControllerClientData Supplier = new ControllerClientData(this.Client);

			return Supplier;
		}
	}
}