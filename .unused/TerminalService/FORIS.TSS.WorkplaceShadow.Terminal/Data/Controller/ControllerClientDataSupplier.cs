﻿using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.WorkplaceSharnier.Data;

namespace FORIS.TSS.WorkplaceShadow.Terminal.Data.Controller
{
	public class ControllerClientDataSupplier :
		ClientDataSupplier<IControllerClientDataProvider, IControllerDataSupplier, IControllerDataTreater>,
		IControllerDataSupplier
	{
		public ControllerClientDataSupplier(IControllerClientDataProvider clientProvider)
			: base(clientProvider, clientProvider.GetControllerDataSupplier())
		{

		}
	}
}