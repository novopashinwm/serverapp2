﻿using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.Helpers.Terminal.Data.Controller;

namespace FORIS.TSS.WorkplaceShadow.Terminal.Data.Controller
{
	public class ControllerClientData :
		ControllerData
	{
		public ControllerClientData(IControllerClientDataProvider clientProvider)
		{
			this.dataSupplier = new ControllerClientDataSupplier(clientProvider);
			this.DataSupplier = this.dataSupplier;
		}

		private readonly IControllerDataSupplier dataSupplier;
	}
}