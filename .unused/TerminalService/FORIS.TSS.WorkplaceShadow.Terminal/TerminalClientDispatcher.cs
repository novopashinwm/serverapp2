﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Terminal
{
	public class TerminalClientDispatcher :
		ClientDispatcher<TerminalClient>
	{
		public TerminalClientDispatcher()
		{

		}

		public TerminalClientDispatcher(IContainer container) : this()
		{
			container.Add(this);
		}

		private readonly TerminalClientAmbassadorCollection ambassadors =
			new TerminalClientAmbassadorCollection();

		protected override AmbassadorCollection<IClientItem<TerminalClient>> GetAmbassadorCollection()
		{
			return ambassadors;
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new TerminalClientAmbassadorCollection Ambassadors
		{
			get { return ambassadors; }
		}
	}

	public class TerminalClientAmbassadorCollection :
		ClientAmbassadorCollection<TerminalClient>
	{
		public new TerminalClientAmbassador this[int index]
		{
			get { return (TerminalClientAmbassador)base[index]; }
		}
	}

	public class TerminalClientAmbassador :
		ClientAmbassador<TerminalClient>
	{

	}
}