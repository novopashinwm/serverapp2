﻿using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.WorkplaceShadow.Terminal.Data.Controller;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Terminal
{
	public interface IControllerClientDataProvider :
		IClientDataProvider,
		IControllerDataSupplierProvider
	{
		ControllerClientData GetControllerClientData();
	}
}