﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FORIS.TSS.WorkplaceShadow.Terminal {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FORIS.TSS.WorkplaceShadow.Terminal.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not create new Controller
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerEditorProvider_Exception_CanNotCreateNewController {
            get {
                return ResourceManager.GetString("ControllerEditorProvider_Exception_CanNotCreateNewController", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not delete Controller
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerEditorProvider_Exception_CanNotDeleteController {
            get {
                return ResourceManager.GetString("ControllerEditorProvider_Exception_CanNotDeleteController", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not update Controller
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerEditorProvider_Exception_CanNotUpdateController {
            get {
                return ResourceManager.GetString("ControllerEditorProvider_Exception_CanNotUpdateController", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete Controller.
        /// </summary>
        internal static string ControllerEditorProvider_Warning_AreYouSureToDeleteController {
            get {
                return ResourceManager.GetString("ControllerEditorProvider_Warning_AreYouSureToDeleteController", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controller firmware required.
        /// </summary>
        internal static string ControllerPropertiesForm_Warning_ControllerFirmwareRequired {
            get {
                return ResourceManager.GetString("ControllerPropertiesForm_Warning_ControllerFirmwareRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controller number required.
        /// </summary>
        internal static string ControllerPropertiesForm_Warning_ControllerNumberRequired {
            get {
                return ResourceManager.GetString("ControllerPropertiesForm_Warning_ControllerNumberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controller phone required.
        /// </summary>
        internal static string ControllerPropertiesForm_Warning_ControllerPhoneRequired {
            get {
                return ResourceManager.GetString("ControllerPropertiesForm_Warning_ControllerPhoneRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controller type required.
        /// </summary>
        internal static string ControllerPropertiesForm_Warning_ControllerTypeRequired {
            get {
                return ResourceManager.GetString("ControllerPropertiesForm_Warning_ControllerTypeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not create new ControllerType
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerTypeEditorProvider_Exception_CanNotCreateNewControllerType {
            get {
                return ResourceManager.GetString("ControllerTypeEditorProvider_Exception_CanNotCreateNewControllerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not delete ControllerType
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerTypeEditorProvider_Exception_CanNotDeleteControllerType {
            get {
                return ResourceManager.GetString("ControllerTypeEditorProvider_Exception_CanNotDeleteControllerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not update ControllerType
        ///Exception: {0}.
        /// </summary>
        internal static string ControllerTypeEditorProvider_Exception_CanNotUpdateControllerType {
            get {
                return ResourceManager.GetString("ControllerTypeEditorProvider_Exception_CanNotUpdateControllerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete ControllerType.
        /// </summary>
        internal static string ControllerTypeEditorProvider_Warning_AreYouSureToDeleteControllerType {
            get {
                return ResourceManager.GetString("ControllerTypeEditorProvider_Warning_AreYouSureToDeleteControllerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controller type name required.
        /// </summary>
        internal static string ControllerTypePropertiesForm_Warning_ControllerTypeNameRequired {
            get {
                return ResourceManager.GetString("ControllerTypePropertiesForm_Warning_ControllerTypeNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data error(s):
        ///{0}.
        /// </summary>
        internal static string Message_DataErrors {
            get {
                return ResourceManager.GetString("Message_DataErrors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No data for edit .
        /// </summary>
        internal static string NoDataForEdit {
            get {
                return ResourceManager.GetString("NoDataForEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        internal static string Warning {
            get {
                return ResourceManager.GetString("Warning", resourceCulture);
            }
        }
    }
}
