﻿using System;
using System.Text;

namespace FORIS.TSS.Helpers.Terminal
{
	///<summary>Реализация логики для поточного чтения данных из массива байт</summary>
	public class DataReader
	{
		protected readonly byte[] _data;
		private readonly int _endIndex;
		private int _index;
		private readonly int _startIndex;

		///<summary>Конструктор для чтения всех байт из массива</summary>
		///<param name="data">Массив байт</param>
		public DataReader(byte[] data) : this(data, 0, data.Length)
		{
		}

		/// <summary>Конструктор для чтения байт начиная со startIndex до конца</summary>
		public DataReader(byte[] data, int startIndex)
			: this(data, startIndex, data.Length)
		{
		}

		/// <summary>Конструктор для чтения байт начиная со startIndex по endIndex (не включая endIndex)</summary>
		public DataReader(byte[] data, int startIndex, int endIndex)
		{
			_data = data;
			_startIndex = startIndex;
			_index = startIndex;
			_endIndex = endIndex;
		}

		public int Position
		{
			get { return _index; }
		}

		/// <summary>
		/// Возвращает количество непрочитанных байт
		/// </summary>
		public int RemainingBytesCount
		{
			get { return _endIndex - _index; }
		}

		public int BytesRead
		{
			get { return _index - _startIndex; }
		}

		public int TotalBytes
		{
			get { return _endIndex; }
		}

		public byte[] GetRemainingBytes()
		{
			if (!Any())
				return null;
			return ReadBytes(RemainingBytesCount);
		}

		///<summary>Читает 1 байт</summary>
		public byte ReadByte()
		{
			if (_endIndex <= _index)
				throw new InvalidOperationException("Nothing to read");

			return _data[_index++];
		}

		/// <summary>Возвращает true, если есть непрочитанные данные</summary>
		public bool Any()
		{
			return _index != _endIndex;
		}

		/// <summary>Пропускает 1 байт</summary>
		public void Skip()
		{
			if (_index == _endIndex)
				throw new InvalidOperationException("Nothing to read");

			++_index;
		}

		/// <summary>
		/// Пропускает count байт
		/// </summary>
		public void Skip(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException("count", count, @"Too many data has been requested");
			_index += count;
		}

		/// <summary>
		/// Пропускает указанный символ или выбрасывает исколючение, если символ на текущей позиции отличается от указанного
		/// </summary>
		/// <param name="c">Символ</param>
		public void SkipChar(char c)
		{
			if (_data[_index] != c)
				throw new InvalidOperationException("Wrong symbol " + _data[_index] +  " at position " + _index + ", expected " + c);
			++_index;
		}

		/// <summary>Читает count байт как ASCII-строку</summary>
		public string ReadAsciiString(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException("count", count, @"Too many data has been requested");
			
			var result = Encoding.ASCII.GetString(_data, _index, count);
			_index += count;
			return result;
		}

		public void SkipAsciiString(string s)
		{
			foreach (var c in s)
				SkipChar(c);
		}


		/// <summary>Читает count байт</summary>
		public byte[] ReadBytes(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException("count", count, @"Too many data has been requested");
			var result = new byte[count];
			System.Array.Copy(_data, _index, result, 0, count);
			_index += count;
			return result;        
		}

		/// <summary>Читает count байт как десятичное положительное целое число, записанное в виде ASCII строки</summary>
		public int ReadAsciiNumber10(int count)
		{
			if (count <= 0)
				throw new ArgumentOutOfRangeException("count", count, @"Value could not be less or equal to zero");
			if (8 < count)
				throw new ArgumentOutOfRangeException("count", count, @"Value could not be greater than 8");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException("count", count, @"Too many data has been requested");
			var result = 0;
			for (var i = 0; i != count; ++i)
			{
				var @byte = ReadByte();
				if (@byte < '0' || '9' < @byte)
					throw new InvalidOperationException("ASCII digit sequence expected, but not " + @byte);
				result *= 10;
				result += @byte - '0';
			}

			return result;
		}

		public void Rewind()
		{
			_index = _startIndex;
		}

		public void Rewind(int count)
		{
			_index -= count;
		}

		public ulong ReadBigEndian64()
		{
			return ReadBigEndian64(8);
		}

		/// <summary>Читает count байт как целое беззнаковое число в big-endian</summary>
		public uint ReadBigEndian(int count)
		{
			if (count < 0 || 4 < count)
				throw new ArgumentOutOfRangeException("count", count, @"Value should be between 0 and 4");

			var result = ReadBigEndian(_data, _index, count);
			_index += count;
			return result;
		}

		public ulong ReadBigEndian64(int bytes)
		{
			ulong result = 0;
			for (var i = 0; i != bytes; ++i)
			{
				result <<= 8;
				result |= _data[_index];
				++_index;
			}
			return result;
		}

		public uint ReadLittleEndian(int bytes)
		{
			var result = ReadLittleEndian(_data, _index, bytes);
			_index += bytes;
			return result;
		}

		public ulong ReadLittleEndian64(int bytes)
		{
			ulong result = 0;
			for (var i = 0; i != bytes; ++i)
			{
				result |= (ulong)(_data[_index] << (i * 8));
				++_index;
			}
			return result;
		}

		public float ReadSingle()
		{
			var result = BitConverter.ToSingle(_data, _index);
			Skip(4);
			return result;
		}

		public static uint ReadLittleEndian(byte[] buffer, int offset, int length)
		{
			uint result = 0;
			for (var i = 0; i != length; ++i)
				result |= (uint)(buffer[i + offset] << (i * 8));

			return result;
		}

		public static uint ReadBigEndian(byte[] buffer, int offset, int length)
		{
			uint result = 0;
			for (var i = offset; i != length; ++i)
			{
				result <<= 8;
				result |= buffer[i];
			}

			return result;
		}
	}
}