using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Terminal.Data.Controller
{
    public class ControllerInfoTable:
        Table<ControllerInfoRow, ControllerInfoTable, IControllerData>
    {
		public override string Name
		{
			get { return "CONTROLLER_INFO"; }
		}

		public ControllerInfoTable( IContainer container ): this()
		{
			container.Add(this );
		}
		public ControllerInfoTable()
		{
			
		}

		protected override ControllerInfoRow OnCreateRow( DataRow dataRow )
		{
			return new ControllerInfoRow( dataRow );
		}
    }

    public class ControllerInfoRow :
        Row<ControllerInfoRow, ControllerInfoTable, IControllerData>
	{
		#region Fields

    	private const int CONTROLLER_ID = 0;

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[ControllerInfoRow.CONTROLLER_ID];
		}

		#endregion // Id

		public ControllerInfoRow( DataRow dataRow ): base( dataRow )
		{
			
		}

		protected override void OnBuildFields()
		{
			
		}
	}
}
