using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Terminal.Data.Controller
{
    public class ControllerDataDispatcher:
		DataDispatcher<IControllerData>
    {
		public ControllerDataDispatcher(IContainer container): this()
		{
			container.Add( this );
		}
		public ControllerDataDispatcher()
		{
			
		}

		private readonly ControllerDataAmbassadorCollection ambassadors =
			new ControllerDataAmbassadorCollection();

    	[Browsable(true)]
    	[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    	public new ControllerDataAmbassadorCollection Ambassadors
    	{
			get { return this.ambassadors; }
    	}

		protected override AmbassadorCollection<IDataItem<IControllerData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}
    }

	public class ControllerDataAmbassadorCollection:
		DataAmbassadorCollection<IControllerData>
	{
		public new ControllerDataAmbassador this[int index]
		{
			get { return (ControllerDataAmbassador)base[index]; }
		}
	}

	public class ControllerDataAmbassador:
		DataAmbassador<IControllerData>
	{

	}
}
