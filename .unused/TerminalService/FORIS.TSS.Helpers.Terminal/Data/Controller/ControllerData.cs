﻿using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Terminal.Data.Controller
{
	public class ControllerData:
		FORIS.TSS.Helpers.Data.Data<IControllerDataTreater>,
		IControllerData,
		IControllerDataSupplier
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private ControllerDataDispatcher tablesDataDispatcher;

		private ControllerTypeTable controllerType;
		private ControllerTable     controller;
		private ControllerInfoTable controllerInfo;

		private ControllerDataAmbassador daControllerType;
		private ControllerDataAmbassador daController;
		private ControllerDataAmbassador daControllerInfo;

		#endregion Controls & Components

		#region Constructor & Dispose

		public ControllerData( IContainer container ): this()
		{
			container.Add( this );
		}

		public ControllerData()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new ControllerDataDispatcher(this.components);

			this.controllerType = new ControllerTypeTable( this.components );
			this.controller = new ControllerTable(this.components );
			this.controllerInfo = new ControllerInfoTable( this.components );

			this.daControllerType = new ControllerDataAmbassador();
			this.daController = new ControllerDataAmbassador();
			this.daControllerInfo = new ControllerDataAmbassador();

			this.daControllerType.Item = this.controllerType;
			this.daController.Item = this.controller;
			this.daControllerInfo.Item = this.controllerInfo;

			this.tablesDataDispatcher.Ambassadors.Add( this.daControllerType );
			this.tablesDataDispatcher.Ambassadors.Add( this.daController );
			this.tablesDataDispatcher.Ambassadors.Add( this.daControllerInfo );
		}

		#endregion Component Designer generated code

		#region Tables order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion Table order

		#region Build & Destoy

		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;
		}

		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;
		}

		#endregion Build & Destroy

		#region Tables properties

		[Browsable(false)]
		public ControllerTypeTable ControllerType
		{
			get { return this.controllerType; }
		}

		[Browsable(false)]
		public ControllerTable Controller
		{
			get { return this.controller; }
		}

		[Browsable(false)]
		public ControllerInfoTable ControllerInfo
		{
			get { return this.controllerInfo; }
		}

		#endregion Tables properties
	}

	public interface IControllerData :
		IData<IControllerDataTreater>
	{
		ControllerTypeTable ControllerType { get; }
		ControllerTable     Controller     { get; }
		ControllerInfoTable ControllerInfo { get; }
	}
}