using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Terminal.Data.Controller
{
	public class ControllerTypeTable:
		Table<ControllerTypeRow, ControllerTypeTable, IControllerData>
	{
		#region Properties

		public override string Name
		{
			get { return "CONTROLLER_TYPE"; }
		}

		#endregion // Properties

		#region Constructor

		public ControllerTypeTable( IContainer container ):this()
		{
			container.Add( this );
		}
		public ControllerTypeTable()
		{
			
		}

		#endregion // Constructor

		protected override ControllerTypeRow OnCreateRow( DataRow dataRow )
		{
			return new ControllerTypeRow( dataRow );
		}
	}

	public class ControllerTypeRow:
		Row<ControllerTypeRow, ControllerTypeTable, IControllerData>
	{
		#region Fields

		private const int CONTROLLER_TYPE_ID = 0;
		private const int TYPE_NAME = 1;

		public string Name
		{
            get { return (string)this.DataRow[ControllerTypeRow.TYPE_NAME, DataRowVersion.Current]; }
			set { this.DataRow[ControllerTypeRow.TYPE_NAME] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[ControllerTypeRow.CONTROLLER_TYPE_ID];
		}

		#endregion // Id

		public ControllerTypeRow( DataRow dataRow ): base( dataRow )
		{
			
		}

		protected override void OnBuildFields()
		{
		
		}
	}
}
