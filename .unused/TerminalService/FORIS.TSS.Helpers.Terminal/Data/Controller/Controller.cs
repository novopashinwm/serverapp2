﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Terminal.Data.Controller
{
	public class ControllerTable :
		Table<ControllerRow, ControllerTable, IControllerData>
	{
		public override string Name
		{
			get { return "CONTROLLER"; }
		}
		public ControllerTable(IContainer container) : this()
		{
			container.Add(this);
		}
		public ControllerTable()
		{
		}
		protected override ControllerRow OnCreateRow(System.Data.DataRow dataRow)
		{
			return new ControllerRow(dataRow);
		}

		#region Row action

		protected override void OnRowActionHide(ControllerRow row)
		{
			this.rowsByNumber.Remove(row.Number);

			base.OnRowActionHide(row);
		}
		protected override void OnRowActionShow(ControllerRow row)
		{
			base.OnRowActionShow(row);

			this.rowsByNumber.Add(row.Number, row);
		}

		#endregion Row action

		private readonly Dictionary<int, ControllerRow> rowsByNumber =
			new Dictionary<int, ControllerRow>();

		public IDictionary<int, ControllerRow> RowsByNumber
		{
			get { return this.rowsByNumber; }
		}
	}

	public class ControllerRow :
		Row<ControllerRow, ControllerTable, IControllerData>
	{
		#region Fields

		private const int CONTROLLER_ID      = 0;
		private const int CONTROLLER_TYPE_ID = 1;
		[Obsolete]
		private const int VEHICLE_ID         = 2;
		private const int PHONE              = 3;
		private const int NUMBER             = 4;
		private const int FIRMWARE           = 5;

		public int ControllerType
		{
			get { return (int)this.DataRow[ControllerRow.CONTROLLER_TYPE_ID, DataRowVersion.Current]; }
			set { this.DataRow[ControllerRow.CONTROLLER_TYPE_ID] = value; }
		}
		public string Phone
		{
			get { return (string)this.DataRow[ControllerRow.PHONE, DataRowVersion.Current]; }
			set { this.DataRow[ControllerRow.PHONE] = value; }
		}
		public int Number
		{
			get { return (int)this.DataRow[ControllerRow.NUMBER, DataRowVersion.Current]; }
			set { this.DataRow[ControllerRow.NUMBER] = value; }
		}
		public string Firmware
		{
			get
			{
				string result = string.Empty;

				if (!this.DataRow.IsNull(ControllerRow.FIRMWARE))
				{
					result = (string)this.DataRow[ControllerRow.FIRMWARE, DataRowVersion.Current];
				}

				return result;
			}
			set { this.DataRow[ControllerRow.FIRMWARE] = value; }
		}

		#endregion Fields

		#region Parent row

		private ControllerTypeRow rowControllerType;
		[Browsable(false)]
		public ControllerTypeRow RowControllerType
		{
			get { return this.rowControllerType; }
		}

		#endregion Parent row

		protected override void OnDestroyView()
		{
			this.rowControllerType = null;

			base.OnDestroyView();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			this.rowControllerType = this.Data.ControllerType.FindRow(this.ControllerType);
		}
		public ControllerRow(DataRow dataRow) : base(dataRow)
		{
		}

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[ControllerRow.CONTROLLER_ID];
		}

		#endregion Id

		protected override void OnBuildFields()
		{
		}
		protected override int CompareTo(ControllerRow row)
		{
			ControllerRow controllerRow = (ControllerRow)row;

			int result;

			result = this.Number.CompareTo(controllerRow.Number);

			if (result != 0) return result;

			return base.CompareTo(row);
		}
	}
}