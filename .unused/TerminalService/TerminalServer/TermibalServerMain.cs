using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Text;
using System.Windows.Forms;
using FORIS.DataAccess;
using FORIS.TSS.TerminalService.Interfaces;

namespace TerminalServer
{
	public partial class TermibalServerMain : Component
	{
		public TermibalServerMain()
		{
			InitializeComponent();
		}

		public TermibalServerMain(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
		}

		static ITerminalManager terminalManager;

		/// <summary>
		/// ������ �������
		/// </summary>
		/// <param name="args"></param>
		public static void Main(string[] args)
		{
			try
			{
				RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);

				using (TermibalServerMain form = new TermibalServerMain())
				{
					form.notifyIcon1.Visible = true;

					//
					terminalManager = (ITerminalManager)Activator.GetObject(typeof(ITerminalManager), "tcp://localhost:11110/TerminalManager");

					terminalManager.CreateMainTerminal(Globals.AppSettings["Terminal"]);

					if (Globals.AppSettings["TerminalTCP"] != null)
						terminalManager.CreateMainTerminal(Globals.AppSettings["TerminalTCP"]);

					if (Globals.AppSettings["TerminalMSMS"] != null)
					{
						terminalManager.CreateMainTerminal(Globals.AppSettings["TerminalMSMS"]);
					}


					Application.Run();
				}
			}
			catch (Exception e)
			{
				Trace.WriteLine(e.ToString());
			}
		}

		private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			if(e.ClickedItem == toolStripMenuItem1)
			{
				this.notifyIcon1.Visible = false;
				Application.Exit();
			}
		}
	}
}
