﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.MTA
{
    public class MTA : IReceiverDevice
    {
        public static readonly StreamWriter swMTAData;

        static MTA()
        {
            swMTAData = new StreamWriter("C:\\mta.log", true) {AutoFlush = true};
        }

        public Datagram InputDatagram
        {
            get { throw new NotImplementedException(); }
        }

        #region IReceiverDevice Members

        public ITerminalReceiver GetReceiver(byte[] data, ITCPReceiver oldReceiver, Type TerminalType)
        {
            if (oldReceiver is MtaTcpReceiver)
                return null;
            if (typeof (TCPTerminal) == TerminalType)
            {
                ITerminalReceiver itr = new MtaTcpReceiver(oldReceiver.Client) {Device = this};
                SendReply(itr, data);
                return itr;
            }
            throw new Exception("Teriminal type is not supported yet. " + TerminalType);
        }

        public void Init(CmdType link, CmdType mode)
        {
            Mode = link;
        }

        public void Init(CmdType link, IStdCommand cmd)
        {
            Mode = link;
        }

        public bool Initialized
        {
            get { throw new NotImplementedException(); }
        }

        public CmdType Mode
        {
            get { return CmdType.Unspecified; }
            set { }
        }

        public byte[] InitSeq
        {
            get { return null; }
        }

        public byte[] UninitSeq
        {
            get { return null; }
        }

        public byte[] GetCmd(IStdCommand cmd)
        {
            return null;
        }

        public void OnLinkOpen(ILink lnk)
        {
        }

        public IList OnData(byte[] data, int count_, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            if (data == null || count_ == 0)
                return null;

            string strData = Encoding.UTF8.GetString(data);

            if (strData[0] == '@')
                return OnASCIIData(strData);

            swMTAData.WriteLine(DateTime.Now + " Receive data:");
            swMTAData.WriteLine(strData);
            swMTAData.WriteLine("data received");

            string idxstr;
            idxstr = strData.Substring(3, strData.IndexOf('&')-3);
            
            int n = strData.IndexOf("bin=");
            if (n < 0)
                return null;
            n += 4;
            if (data[n] == 50 && data.Length >= 44)
            {
                int shift = data[n + 1];
                n += shift;
                if (data[n] != 0xff)
                    return null;
                byte eventHeader = data[n + 1];

                int datastart = n + 1;
                if ((eventHeader & 0x40) == 0)
                    datastart++;

                float latitude;
                float longitude;
                float time;
                ParsePosition(data, datastart, out latitude, out longitude, out time);

                var mu = new MobilUnit.Unit.MobilUnit
                             {
                                 DeviceID = Encoding.ASCII.GetBytes(idxstr),
                                 Latitude = latitude*180/Math.PI,
                                 Longitude = longitude*180/Math.PI,
                                 Time = 0,
                                 Speed = 0,
                                 Satellites = 10,
                                 cmdType = CmdType.Trace,
                                 CorrectGPS = true
                };

                var aRes = new ArrayList(1) { mu };
                return aRes;
            }

            return null;
        }

        public void OnNoData()
        {
            throw new NotImplementedException();
        }

        public void OnError()
        {
            throw new NotImplementedException();
        }

        public void OnLinkClose()
        {
            throw new NotImplementedException();
        }

        public bool SupportData(byte[] data)
        {
            string strData = Encoding.UTF8.GetString(data);
            if (strData.Contains("Content-Length: ") || strData.Contains("id="))
                return true;
            if (strData[0] == '@')
                return true;
            return false;
        }

        public bool SupportData(Datagram dg)
        {
            throw new NotImplementedException();
        }

        public ITerminalManager Manager { get; set; }

        public ITerminal CreateTerminal(CmdType type)
        {
            throw new NotImplementedException();
        }

        public byte[] AdjustSettings()
        {
            throw new NotImplementedException();
        }

        public ITerminal CreateTerminal(Media type)
        {
            throw new NotImplementedException();
        }

        Datagram IDevice.InputDatagram
        {
            get { throw new NotImplementedException(); }
        }

        Datagram IDevice.OutputDatagram(byte[] data, out int begin)
        {
            throw new NotImplementedException();
        }

        #endregion

        private void SendReply(ITerminalReceiver itr, byte[] data)
        {
            string strData = Encoding.UTF8.GetString(data);
            byte[] replyData;
            if (strData.Contains("HTTP"))
            {
                replyData = Encoding.UTF8.GetBytes("HTTP/1.1 100 Continue\r\n\r\n\r\n");
                itr.Send(replyData, replyData.Length);

                swMTAData.WriteLine(DateTime.Now + " Send data:");
                swMTAData.WriteLine("HTTP/1.1 100 Continue\r\n");
                swMTAData.WriteLine("data sent");
            }
            if (strData.Contains("id="))
            {
                string[] strs = strData.Split(new[] {"bin="}, StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length >= 2)
                {
                    byte[] terminalPacket = Encoding.UTF8.GetBytes(strs[1]);

                    var replyList = new List<byte>();
                    replyList.AddRange(Encoding.ASCII.GetBytes("HTTP/1.1 200 OK\r\n\r\nContent-Length: 7\r\n\r\n#ACK#"));
                    replyList.Add(terminalPacket[0]);
                    replyList.Add(terminalPacket[2]);
                    //replyList.Add(0);

                    replyData = replyList.ToArray();
                    itr.Send(replyData, replyData.Length);

                    swMTAData.WriteLine(DateTime.Now + " Send data:");
                    swMTAData.WriteLine(Encoding.ASCII.GetString(replyData));
                    swMTAData.WriteLine("data sent");

                    Trace.WriteLine(GetType() + " Packet number: " + (int) terminalPacket[2]);
                }
            }
        }

        public Datagram OutputDatagram(byte[] data, out int begin)
        {
            throw new NotImplementedException();
        }

        private IList OnASCIIData(string strData)
        {
            try
            {
                strData = strData.Remove(0, 1);

                string[] strings = strData.Split(';');
                if (strings.Length < 5)
                    return null;

                for (int i = 0; i < strings.Length; i++)
                {
                    int idx = strings[i].IndexOf('=');
                    if (idx >= 0)
                    {
                        strings[i] = strings[i].Remove(0, idx + 1);
                    }
                }

                Int64 idsim;
                if (!Int64.TryParse(strings[0], out idsim))
                    return null;

                string[] strsStatus = strings[4].Split(',');
                int satellites = int.Parse(strsStatus[0]);
                bool validGPS = strsStatus[1].ToUpper() == "OK";
                int speed = int.Parse(strings[5]);
                int run = int.Parse(strings[6]);

                string[] strsLongLat = strings[7].Split(',');
                float latitude = float.Parse(strsLongLat[0], CultureInfo.InvariantCulture);
                float longitude = float.Parse(strsLongLat[1], CultureInfo.InvariantCulture);

                DateTime time = DateTime.ParseExact(strings[8], "DDMMYY,HHmmss", CultureInfo.InvariantCulture);

                var mu = new MobilUnit.Unit.MobilUnit
                             {
                                 DeviceID = Encoding.ASCII.GetBytes(idsim.ToString()),
                                 Latitude = latitude,
                                 Longitude = longitude,
                                 Time = ((int) (time - TimeHelper.year1970).TotalSeconds),
                                 Speed = speed,
                                 Satellites = satellites,
                                 cmdType = CmdType.Trace,
                                 CorrectGPS = (validGPS & satellites >= 3)
                             };

                var aRes = new ArrayList(1) {mu};
                return aRes;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + ": " + GetType() + ": " + ex.Message);
            }
            return null;
        }

        private static void ParsePosition(byte[] data, int pos, out float latitude, out float longitude, out float time)
        {
            UInt32 prevBits = 0xFFFFFFFF;
            pos += ParseCompressedValue(data, pos, ref prevBits, out latitude);
            pos += ParseCompressedValue(data, pos, ref prevBits, out longitude);
            pos += ParseCompressedValue(data, pos, ref prevBits, out time);
        }

        private static int ParseCompressedValue(byte[] data, int pos, ref UInt32 prevBits, out float value)
        {
            var b = new[] {data[pos + 3], data[pos + 2], data[pos + 1], data[pos + 0]};

            UInt32 bits = BitConverter.ToUInt32(b, 0);
            bits = bits << 2;
            int size = 0;

            UInt32 header = (bits & ((UInt32) 3 << 30)) >> 30;
            switch (header)
            {
                case 0:
                    size = 4;
                    break;
                case 1:
                    bits = prevBits & 0xFFFFFFFF & ((UInt32) ((data[pos]) << 2));
                    size = 1;
                    break;
                case 2:
                    UInt32 tmp = 0xFFFFFFFF & data[pos];
                    tmp <<= 8;
                    tmp &= data[pos + 1];
                    bits = prevBits & tmp;
                    break;
                case 3:
                    break;
            }
            value = BitConverter.ToSingle(BitConverter.GetBytes(bits), 0);
            prevBits = bits;

            return size;
        }
    }
}