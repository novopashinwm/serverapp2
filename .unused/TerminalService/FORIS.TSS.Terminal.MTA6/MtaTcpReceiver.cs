﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using FORIS.TSS.Terminal.Terminal;

namespace FORIS.TSS.Terminal.MTA
{
    class MtaTcpReceiver : TCPReceiver
    {
        public override event EventHandler OnData;
        public override event EventHandler OnProcessedData;

        public MtaTcpReceiver(TcpClient client) : base(client)
        {
            this.lastReceiveTime = DateTime.Now;

            InitReceiver();
        }

        private void InitReceiver()
        {
            this.OnData += MtaTcpReceiver_OnData;
        }

        void MtaTcpReceiver_OnData(object sender, EventArgs e)
        {
            try
            {
                if (this.Device != null)
                {
                    SendReply(((TcpOnDataEventArgs)e).Data);
                    byte[] bufferRest;
                    this.aRes = this.Device.OnData(((TcpOnDataEventArgs)e).Data, ((TcpOnDataEventArgs)e).Data.Length, m_stateDataForDevice, out bufferRest);


                    if (this.OnProcessedData != null)
                    {
                        TcpOnProcessedDataEventArgs args = new TcpOnProcessedDataEventArgs
                        {
                            aRes = this.aRes,
                            Address =
                                (m_client.Client.RemoteEndPoint as IPEndPoint)
                        };
                        foreach (Delegate dlgt in OnProcessedData.GetInvocationList())
                        {
                            ((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " " + this.GetType() + ".MtaTcpReceiver_OnData: " + ex.Message);
            }
        }

        private void SendReply(byte[] data)
        {
            string strData = Encoding.UTF8.GetString(data);
            byte[] replyData;
            if (strData.Contains("HTTP"))
            {
                replyData = Encoding.UTF8.GetBytes("HTTP/1.1 100 Continue\r\n");
                Send(replyData, replyData.Length);

                MTA.swMTAData.WriteLine(DateTime.Now + " Send data:");
                MTA.swMTAData.WriteLine("HTTP/1.1 100 Continue\r\n");
                MTA.swMTAData.WriteLine("data sent");
            }
            if (strData.Contains("id="))
            {
                string[] strs = strData.Split(new[] {"bin="}, StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length >= 2)
                {
                    byte[] terminalPacket = Encoding.UTF8.GetBytes(strs[1]);

                    List<byte> replyList = new List<byte>();
                    replyList.AddRange(Encoding.ASCII.GetBytes("HTTP/1.1 200 OK\r\nContent-Length: 7\r\n\r\n\r\n#ACK#"));
                    replyList.Add(terminalPacket[0]);
                    replyList.Add(terminalPacket[2]);
                    //replyList.Add(0);

                    replyData = replyList.ToArray();
                    Send(replyData, replyData.Length);

                    MTA.swMTAData.WriteLine(DateTime.Now + " Send data:");
                    MTA.swMTAData.WriteLine(Encoding.ASCII.GetString(replyData));
                    MTA.swMTAData.WriteLine("data sent");

                    Trace.WriteLine(this.GetType() + " Packet number: " + (int)terminalPacket[2]);
                }
            }
        }

        protected override void ReceiveData_Callback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.
                int bytesRead = 0;

                if (client.Connected)
                    bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    byte[] d = new byte[bytesRead];
                    Array.Copy(state.buffer, d, bytesRead);

                    if (OnData != null)
                    {
                        TcpOnDataEventArgs args = new TcpOnDataEventArgs
                        {
                            Address = (m_client.Client.RemoteEndPoint as IPEndPoint),
                            Data = d
                        };


                        foreach (Delegate dlgt in OnData.GetInvocationList())
                        {
                            ((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                        }
                        receive_counter++;
                    }
                    lastReceiveTime = DateTime.Now;


                    // Get the rest of the data.
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                        new AsyncCallback(ReceiveData_Callback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.
                    if (state.data.Count > 1)
                    {
                        if (OnData != null)
                        {
                            TcpOnDataEventArgs args = new TcpOnDataEventArgs
                            {
                                Address = (m_client.Client.RemoteEndPoint as IPEndPoint),
                                Data = state.data.ToArray()
                            };

                            state.data.Clear();

                            foreach (Delegate dlgt in OnData.GetInvocationList())
                            {
                                ((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                            }
                            receive_counter++;
                        }
                        lastReceiveTime = DateTime.Now;
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(this.GetType() + " " + e);
            }
        }
    }
}
