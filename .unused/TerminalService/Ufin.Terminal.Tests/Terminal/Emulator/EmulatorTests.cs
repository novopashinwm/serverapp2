﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Fakes;
using System.Linq;
using System.Reflection;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.ServerApplication.Terminal;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Emulator.Fakes;
using FORIS.TSS.Terminal.Fakes;
using FORIS.TSS.Terminal.Terminal.Emulator.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Terminal.Emulator
{
	[TestClass()]
	public class EmulatorTests
	{
		[TestMethod()]
		public void EmulatorGenerateDataTest()
		{
			var timStp = 60;
			//var edgTim = 60;             // Минута
			//var edgTim = 60 * 60;        // Час
			//var edgTim = 60 * 1440;      // Сутки
			var edgTim = 60 * 1440 * 02;      // Двое суток
			//var edgTim = 60 * 1440 * 30; // Месяц

			var utcNow = DateTime.UtcNow;
			var begNow = utcNow.Date.AddSeconds(-edgTim);
			var shmNow = begNow;
			//////////////////////////////////////////////////
			// Для эмуляции сохранения готовим словарь
			var lastFakeVehicleLogTime = new Dictionary<int, int>();
			using (ShimsContext.Create())
			{
				//////////////////////////////////////////////////
				// Делаем подмену текущего времени
				ShimDateTime.UtcNowGet = () => { return shmNow; };
				//////////////////////////////////////////////////
				// Делаем подмену данных из БД (как будто все сохранили)
				ShimEmulator.ShimEmulatedVehicle.AllInstances.LastLogTimeGet = (v) =>
				{
					if (!lastFakeVehicleLogTime.ContainsKey(v.FakeVehicleId))
						lastFakeVehicleLogTime[v.FakeVehicleId] = shmNow.ToLogTime() - timStp;
					return lastFakeVehicleLogTime[v.FakeVehicleId];
				};
				//////////////////////////////////////////////////
				// Создаем терминал с эмулятором
				var emulator = new FORIS.TSS.Terminal.Terminal.Emulator.Emulator(0);
				//////////////////////////////////////////////////
				// Подписываемся на событие терминала с эмулятором
				emulator.OnSendEvent += (mobilUnits) =>
				{
					if (0 < (mobilUnits?.Count() ?? 0))
					{
						foreach (var mobilUnit in mobilUnits)
						{
							var prevLogTime = default(int);
							if (!lastFakeVehicleLogTime.TryGetValue(mobilUnit.Unique, out prevLogTime))
								prevLogTime = mobilUnit.Time;

							// Сохраняем в словарь, эмулируя сохранение в БД
							lastFakeVehicleLogTime[mobilUnit.Unique] = mobilUnit.Time;
							//if (1000 < mobilUnit.Time - prevLogTime)
							// Трассируем сохранение
							(
								$"UtcNow = {shmNow:yyyy-MM-dd HH:mm:ss.fff}"                                                         +
								$" {nameof(mobilUnit.Unique)} = {mobilUnit.Unique}"                                               +
								$" {nameof(mobilUnit.Time)} = {mobilUnit.Time}({mobilUnit.Time.ToUtcDateTime():yyMMdd-HHmmss})" +
								//$" PrevLogTime = {prevLogTime}({prevLogTime.ToUtcDateTime():yyMMdd-HHmmss})" +
								$" DiffLogTime = {mobilUnit.Time - prevLogTime}"
								//+
								//$" {nameof(mobilUnit.Latitude)} = {mobilUnit.Latitude}"                                             +
								//$" {nameof(mobilUnit.Longitude)} = {mobilUnit.Longitude}"
							)
								.TraceInformation();

							//Trace.TraceInformation(string.Empty
							//	+ $"\n-------------------------------------------------------"
							//	+ $"\n{nameof(mobilUnit.Unique)}    = {mobilUnit.Unique}"
							//	+ $"\n{nameof(mobilUnit.Time)}      = {mobilUnit.Time}({mobilUnit.Time.ToUtcDateTime():yyMMdd-HHmmss.fff})"
							//	+ $"\n{nameof(mobilUnit.Latitude)}  = {mobilUnit.Latitude}"
							//	+ $"\n{nameof(mobilUnit.Longitude)} = {mobilUnit.Longitude}"
							//	+ $"\n-------------------------------------------------------");
						}
					}
					// Изменяем текущее время
					shmNow += TimeSpan.FromSeconds(timStp);
					//$"UtcNow = {shmNow:yyyy-MM-dd HH:mm:ss}"
					//	.TraceInformation();
				};
				//////////////////////////////////////////////////
				// Старт терминала с эмулятором
				emulator.Start();
				//////////////////////////////////////////////////
				// Ожидание необходимого времени работы терминала с эмулятором
				while (emulator.Running && (shmNow - begNow).TotalSeconds < 2 * edgTim)
					Thread.Sleep(1000);
				//////////////////////////////////////////////////
				// Остановка терминала с эмулятором
				emulator.Stop();
			}
		}
		[TestMethod()]
		public void TestPerformanceWithEmulators()
		{
			var timStp = 5;
			var edgTim = 30 * 1440 * 60; // Месяц
			var utcNow = DateTime.UtcNow;
			var begNow = utcNow.Date.AddSeconds(-edgTim);
			var shmNow = begNow;
			using (ShimsContext.Create())
			{
				//////////////////////////////////////////////////
				ShimDataBaseManager.ShimAddMonitoreeLogContext.AllInstances.AddMobilUnitIMobilUnit = (context, mobilUnit) =>
				{
					if (null == mobilUnit)
						return false;
					Trace.TraceInformation(string.Empty
						+ $"\n-------------------------------------------------------"
						+ $"\n{nameof(mobilUnit.Unique)}    = {mobilUnit.Unique}"
						+ $"\n{nameof(mobilUnit.Time)}      = {mobilUnit.Time}({mobilUnit.Time.ToUtcDateTime():yyMMdd-HHmmss.fff})"
						+ $"\n{nameof(mobilUnit.Latitude)}  = {mobilUnit.Latitude}"
						+ $"\n{nameof(mobilUnit.Longitude)} = {mobilUnit.Longitude}"
						+ $"\nSaveQueue Count               = {Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaveQueue).RawValue}"
						+ $"\n-------------------------------------------------------");
					return true;
				};
				//////////////////////////////////////////////////
				ShimDataBaseManager.ShimAddMonitoreeLogContext.AllInstances.UpdateInsertTimeInt32Int32 = (context, vehicleId, logTime) =>
				{
					//shmNow += TimeSpan.FromSeconds(timStp);
				};
				//////////////////////////////////////////////////
				// Делаем подмену данных из БД (как будто все сохранили)
				ShimEmulator.ShimEmulatedVehicle.AllInstances.LastLogTimeGet = (v) => { return shmNow.ToLogTime() - timStp; };
				ShimEmulatorTerminal.AllInstances.PublishMobileUnitIMobilUnitArray = (emulatorTerminal, mobilUnits) =>
				{
					ShimsContext.ExecuteWithoutShims(() =>
					{
						emulatorTerminal.GetType()
							?.GetMethod("PublishMobileUnit", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
							?.Invoke(emulatorTerminal, new[] { mobilUnits });
					});
					shmNow += TimeSpan.FromSeconds(timStp);
				};
				//////////////////////////////////////////////////
				using (var terminalServer = new TerminalServer(new TerminalSessionManager(), new NameValueCollection()))
				{
					// Стартуем при входе, останавливаем при выходе
					using (new UsingActions(((IServer)terminalServer).Start, ((IServer)terminalServer).Stop))
					{
						// Ожидаем состояния запуска
						while (((IServer)terminalServer).State != ServerState.Running)
							Thread.Sleep(100);
						var emulatorTerminal = terminalServer
							?.TerminalManager
							?.GetLiveTerminals(Media.Emulator)
							?.FirstOrDefault();
						if (null != emulatorTerminal)
						{
							while (shmNow < utcNow || 0 < Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaveQueue).RawValue)
							{
								Trace.TraceInformation(string.Empty
									+ $"\n-------------------------------------------------------"
									+ $"\n{nameof(shmNow)}      = {shmNow:yyMMdd-HHmmss.fff}"
									+ $"\n{nameof(utcNow)}      = {utcNow:yyMMdd-HHmmss.fff}"
									+ $"\nSaveQueue Count = {Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaveQueue).RawValue}"
									+ $"\n-------------------------------------------------------");
								Thread.Sleep(1000);
							}
							Trace.TraceInformation("End");
						}
					}
				}
			}
		}
	}
}