﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.ServerApplication.Terminal;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Terminal
{
	[TestClass]
	public class TerminalServerTests
	{
		private static readonly ConcurrentDictionary<int, MobilUnit> _terminalMobils = new ConcurrentDictionary<int, MobilUnit>();
		private static          TerminalServer                       _terminalServer = null;
		public TestContext TestContext { get; set; }
		#region Additional test attributes
		[ClassInitialize]
		public static void ClassInitialize(TestContext testContext)
		{
			foreach (var position in new DataBaseManager().GetLastPositions())
				_terminalMobils[position.VehicleID] = new MobilUnit()
				{
					Unique    = position.VehicleID,
					Time      = position.Time,
					Longitude = position.Longitude,
					Latitude  = position.Latitude,
					Speed     = position.Speed,
					Course    = position.Course,
				};
			_terminalServer = new TerminalServer(new TerminalSessionManager(), new NameValueCollection());
			((IServer)_terminalServer).Start();
		}
		[ClassCleanup]
		public static void ClassCleanup()
		{
			((IServer)_terminalServer).Stop();
			((IServer)_terminalServer).Dispose();
			_terminalServer = null;
		}
		//[TestInitialize]
		//public void TestInitialize() { }
		//[TestCleanup]
		//public void TestCleanup() { }
		#endregion Additional test attributes
		[TestMethod]
		public void TestPerformance()
		{
			var edgTim = 1
				* 60 // Минута
				* 60 // Час
				//* 24 // Сутки
				//* 30 // Месяц
				;
			var utcNow = DateTime.UtcNow;
			var begNow = utcNow.Date.AddSeconds(-edgTim);
			var shmNow = begNow;
			using (ShimsContext.Create())
			{
				//////////////////////////////////////////////////
				ShimDataBaseManager.ShimAddMonitoreeLogContext.AllInstances.AddMobilUnitIMobilUnit = (context, mobilUnit) =>
				{
					if (null == mobilUnit)
						return false;
					// Среднее время выполнения процедур сохранения (получено из статистики процедур)
					Thread.Sleep(1);
					//using (new Stopwatcher($@"GetConstant(Constant.ServerIP)", null, TimeSpan.Zero))
					//	_terminalServer.TerminalManager.GetConstant(Constant.ServerIP);
					return true;
				};
				//////////////////////////////////////////////////
				ShimDataBaseManager.ShimAddMonitoreeLogContext.AllInstances.UpdateInsertTimeInt32Int32 = (context, vehicleId, logTime) =>
				{
				};
				var groupMobilUnits = default(IEnumerable<MobilUnit[]>);
				using (new Stopwatcher($@"Generate data", null, TimeSpan.Zero))
				{
					groupMobilUnits = Enumerable.Range(0, edgTim)
					.Select(i => shmNow += TimeSpan.FromSeconds(1))
					.SelectMany(i => _terminalMobils.Values
						.Select(m => { m.Time = i.ToLogTime(); return m; }))
					.GroupBy(u => new { u.StorageId, u.Unique })
					.Select(g => g.ToArray())
					.ToArray();
				}
				var repeatCount = 1000000 / groupMobilUnits.Sum(g => g.Length);
				for (int i = 0; i < repeatCount; i++)
					foreach (var groupMobilUnit in groupMobilUnits)
					{
						using (new Stopwatcher($@"PublishMobileUnits(mobilUnits count={groupMobilUnit.Length})", null, TimeSpan.Zero))
							_terminalServer.TerminalManager.PublishMobileUnits(groupMobilUnit);
					}
				//////////////////////////////////////////////////
				/// Ищем все счетчики очередей
				var counters = Enumerable.Range(1, ConfigHelper.GetAppSettingAsInt32("TerminalManager.MobilUnitSaverCount", 4))
					.Select(i => Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaverTask, $@"{i:00}"))
					.Union(new[] { Counters.Instance.GetCounterReadOnly(CounterGroup.Count, Counters.Instances.SaveQueue) })
					.Where(c => null != c)
					.ToArray();
				/// Ждем обнуления всех счетчиков
				while (0 < counters.Sum(c => c.RawValue))
					Thread.Sleep(1000);
			}
		}
	}
}