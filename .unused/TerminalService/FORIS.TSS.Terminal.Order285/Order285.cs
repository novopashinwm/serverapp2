﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Order285
{
    public class Order285 : Device, IHttpDevice
    {
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            throw new NotSupportedException();
        }

        public override bool SupportData(byte[] data)
        {
            return false;
        }

        public bool CanProcess(IHttpRequest request)
        {
            return string.Equals(
                request.RequestUrl.PathAndQuery,
                "/Terminal/Order285SoapV1.4/",
                StringComparison.OrdinalIgnoreCase);
        }

        public IList<object> OnData(IHttpRequest request)
        {
            if (request.Text == null)
                return Error("Request body not found");

            var text = request.Text.Replace(
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"">",
                @"<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">");

            XDocument doc;
            try
            {
                doc = XDocument.Parse(text);
            }
            catch (Exception e)
            {
                Trace.TraceWarning("Unable to parse {0}: {1}", text, e);
                return Error("Error parsing xml");
            }

            if (doc.Root == null || doc.Root.Name.LocalName != "Envelope")
                return NodeNotFoundError("Envelope");

            var body = doc.Root.Nodes().FirstOrDefault(
                n => n.NodeType == XmlNodeType.Element && ((XElement)n).Name.LocalName == "Body") as XElement;
            if (body == null)
                return NodeNotFoundError("Body", doc.Root);

            var putCoord = body.Nodes().FirstOrDefault(
                n => n.NodeType == XmlNodeType.Element && ((XElement)n).Name.LocalName == "PutCoord") as XElement;
            if (putCoord == null)
                return NodeNotFoundError("PutCoord", body);

            var objectId = putCoord.Nodes().FirstOrDefault(
                n => n.NodeType == XmlNodeType.Element && ((XElement)n).Name.LocalName == "ObjectID") as XElement;
            if (objectId == null)
                return NodeNotFoundError("ObjectID", putCoord);

            var coord = putCoord.Nodes().FirstOrDefault(
                n => n.NodeType == XmlNodeType.Element && ((XElement)n).Name.LocalName == "Coord") as XElement;
            if (coord == null)
                return NodeNotFoundError("Coord", putCoord);

            var objectIdString = objectId.Value.Trim();

            var time = ParseAttribute(coord, "time", value => XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.Utc));

            var mu = UnitReceived();
            try
            {
                var lon = ParseAttribute(coord, "lon", XmlConvert.ToDouble);
                var lat = ParseAttribute(coord, "lat", XmlConvert.ToDouble);
                var speed = ParseAttribute(coord, "speed", XmlConvert.ToDouble);
                var dir = ParseAttribute(coord, "dir", XmlConvert.ToInt32);
                var valid = coord.Attribute("valid").Value == "1";

                mu.DeviceID = mu.DeviceID = objectIdString;
                mu.Time = TimeHelper.GetSecondsFromBase(time);
                mu.Longitude = lon;
                mu.Latitude = lat;
                mu.Speed = (int) speed;
                mu.Course = dir;
                mu.Satellites = valid ? 10 : 0;
                mu.CorrectGPS = valid;
            }
            catch (ParsingException e)
            {
                return Error(e.Message);
            }

            return new List<object>
                {
                    new HttpResponse
                        {
                            Text = GetSuccessResponseString(objectIdString)
                        },
                    mu
                };

        }

        private IList<object> NodeNotFoundError(string nodeName, XElement where = null)
        {
            var wherePathString = (@where == null ? "" : @where.AncestorsAndSelf().Select(x => x.Name.LocalName).Join("/"));
            return Error("Node " + nodeName + " not found at /" + wherePathString);
        }

        private IList<object> Error(string message)
        {
            const string formatString = @"<?xml version=""1.0"" encoding=""windows-1251""?>
<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">
<soapenv:Header/>
<soapenv:Body>
<ws:Fault>
<Reason>
<Text>{0}</Text>
</Reason>
</ws:Fault>
</soapenv:Body>
</soapenv:Envelope>";

            return new List<object>
                {
                    new HttpResponse
                        {
                            StatusCode = 200,
                            StatusDescription = "200 OK",
                            Text = string.Format(formatString, message)
                        }
                };
        }

        private T ParseAttribute<T>(XElement node, string name, Converter<string, T> parse)
        {
            var attribute = node.Attribute(name);
            if (attribute == null)
                throw new ParsingException("Attribute " + name + " not found at " + GetPath(node));

            try
            {
                return parse(attribute.Value);
            }
            catch (Exception ex)
            {
                throw new ParsingException("Error parsing attribute " + name + " at " + GetPath(node) + ": " + ex.Message, ex);
            }
        }

        private string GetPath(XElement node)
        {
            var result = new List<string>();

            while (node.Parent != null)
            {
                result.Add(node.Name.LocalName);
                node = node.Parent;
            }

            result.Reverse();
            return result.Join("/");
        }

        private class ParsingException : ApplicationException 
        {
            public ParsingException(string message) : base(message)
            {
            }

            public ParsingException(string message, Exception innerException) : 
                base(message, innerException)
            {
            }
        }

        private string GetSuccessResponseString(string objectIdString)
        {
            const string formatString = @"<?xml version=""1.0"" encoding=""windows-1251""?>
<soapenv:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope"" xmlns:ws=""tns"">
<soapenv:Header/>
<soapenv:Body>
<ws:PutCoordResponse>
<ObjectID>{0}</ObjectID>
</ws:PutCoordResponse>
</soapenv:Body>
</soapenv:Envelope>";

            return string.Format(formatString, objectIdString);
        }
    }
}
