using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Services;
using System.Diagnostics;

namespace FORIS.TSS.Terminal
{
#if DEBUG
    class Observer: ITrackingHandler
    {

        #region ITrackingHandler Members

        void ITrackingHandler.DisconnectedObject(object obj)
        {
            Debug.WriteLine("DisconnectedObject() " + obj.GetType().Name);
        }

        void ITrackingHandler.MarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
        {
            Debug.WriteLine("MarshaledObject() " + obj.GetType().Name);
        }

        void ITrackingHandler.UnmarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
        {
            Debug.WriteLine("UnmarshaledObject() " + obj.GetType().Name);
        }

        #endregion

        public Observer()
        {
            TrackingServices.RegisterTrackingHandler(this);
        }
        ~Observer()
        {
            TrackingServices.UnregisterTrackingHandler(this);
        }
    }
#endif
}
