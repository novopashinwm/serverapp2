﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.Generator
{
	/// <summary> Эмулирует поступление новых данных через их генерацию непосредственно в процессе приложения </summary>
	class GeneratingEmulatorTerminal : _Terminal
	{
		private readonly BackgroundGenerator _backgroundProcessor;

		class BackgroundGenerator : BackgroundProcessor
		{
			private readonly GeneratingEmulatorTerminal _terminal;
			private readonly int _startTime;
			private int? _lastTime;

			public BackgroundGenerator(GeneratingEmulatorTerminal terminal) : base(TimeSpan.FromSeconds(1))
			{
				_terminal = terminal;
				_startTime = TimeHelper.GetSecondsFromBase(new DateTime(2016, 03, 24, 0, 0, 0, DateTimeKind.Utc));
			}

			private GeoPoint GetPoint(int time)
			{
				var radians = MathHelper.DegreesToRadians(time);
				var ampLng = 0.1 + Math.Sin((2*radians+1)/60/60);
				var ampLat = 0.1 + Math.Sin(radians/60/60);
				return GeoPoint.CreateGeo(Math.Cos(radians / 60) * ampLng + 37.617734, Math.Sin(radians / 60) * ampLat + 55.751999);
			}

			protected override bool Do()
			{
				var currentTime = TimeHelper.GetSecondsFromBase();
				if (_lastTime == null)
					_lastTime = currentTime;

				var mobilUnits = new List<IMobilUnit>();
				while (_lastTime < currentTime)
				{
					++_lastTime;

					var mu = new MobilUnit.Unit.MobilUnit();
					mobilUnits.Add(mu);
					mu.DeviceID = "generator";
					mu.Time = _lastTime.Value;

					var time = mu.Time - _startTime;
					var currPoint = GetPoint(time);

					mu.Latitude  = currPoint.Y;
					mu.Longitude = currPoint.X;

					var prevPoint = GetPoint(time - 1);
					var metersPerSecond = GeoHelper.DistanceBetweenLocations(
						currPoint.Y, currPoint.X, 
						prevPoint.Y, prevPoint.X);

					mu.Speed = (int) MeasureUnitHelper.MpsToKmph(metersPerSecond);
					mu.Course = mu.CalcCourse(prevPoint.Y, prevPoint.X);

					var radians = MathHelper.DegreesToRadians(time%TimeSpan.FromHours(1).TotalSeconds);
					mu.Height = (int) (Math.Cos(radians)*25000);
					mu.CorrectGPS = true;
					mu.SensorValues = new Dictionary<int, long>();


					const int digitalSensorCount = 8;
					for (var i = 0; i != digitalSensorCount; ++i)
						mu.SensorValues.Add(i + 1, 0.5 < Math.Sin((radians + i)/(i + i)) ? 1 : 0);

					const int analogueSensorCount = 8;
					for (var i = 0; i != analogueSensorCount; ++i)
						mu.SensorValues.Add(i + 11, (long) (100*Math.Sin((radians + i)/(i + 1))));

					mu.Media = Media.Emulator;
				}
				_terminal.OnReceive(new ReceiveEventArgs(mobilUnits.ToArray()));

				return 1 < mobilUnits.Count;
			}
		}

		public GeneratingEmulatorTerminal(string name, ITerminalManager mgr) : base(name, mgr)
		{
			_backgroundProcessor = new BackgroundGenerator(this);
		}

		public override void Initialization(params string[] initStrings)
		{
			base.Initialization(initStrings);

			_backgroundProcessor.Start();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
				_backgroundProcessor.Stop();
		}
	}
}