﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.Locator
{
    /// <summary>
    /// Обеспечивает получение координат объектов по данным о запеленгованных сетях GSM/WiFi
    /// </summary>
    class YandexLocatorTerminal : _Terminal
    {
        private LocatorProcessor _locatorProcessor;
        private NetmonitoringProcessor _netmonitoringProcessor;

        public YandexLocatorTerminal(string name, ITerminalManager mgr) : base(name, mgr)
        {
        }

        public override void Initialization(params string[] initStrings)
        {
            base.Initialization(initStrings);

            var yandexApiKey = initStrings[1];

            _locatorProcessor = new LocatorProcessor(this, yandexApiKey);
            _locatorProcessor.Start();

            _netmonitoringProcessor = new NetmonitoringProcessor(yandexApiKey);
            _netmonitoringProcessor.Start();
        }

        public override void Dispose()
        {
            if (_locatorProcessor != null)
                _locatorProcessor.Stop();

            if (_netmonitoringProcessor != null)
                _netmonitoringProcessor.Stop();

            base.Dispose();
        }

        private class LocatorProcessor : BackgroundProcessor
        {
            private readonly string _yandexApiKey;
            private readonly Uri _url = new Uri("http://api.lbs.yandex.net/geolocation");

            private readonly YandexLocatorTerminal _terminal;
            private readonly DataBaseManager _db;

            public LocatorProcessor(YandexLocatorTerminal terminal, string yandexApiKey)
            {
                _terminal = terminal;
                _db = new DataBaseManager();
                _yandexApiKey = yandexApiKey;
            }

            protected override bool Do()
            {
                Result result;
                try
                {
                    result = ProcessOnePosition();
                }
                catch (ThreadAbortException)
                {
                    return true;
                }
                catch (ThreadInterruptedException)
                {
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError("{0}", ex);
                    result = Result.Error;
                }

                switch (result)
                {
                    case Result.Success:
                    case Result.PositionNotFound:
                        return true;
                    case Result.NoCandidates:
                        return false;
                    default:
                        return false;
                }
            }

            private enum Result
            {
                Success,
                NoCandidates,

                PositionNotFound,
                WebException,
                Error
            }

            private Result ProcessOnePosition()
            {
                var dataForLocator = _db.GetDataForLocator();

                if (dataForLocator == null ||
                    dataForLocator.Cells == null &&
                    dataForLocator.Wlans == null)
                {
                    return Result.NoCandidates;
                }

                string requestBodyString = null;
                string responseBodyString = null;

                MobilUnit.Unit.MobilUnit mu = null;
                try
                {
                    requestBodyString = GetRequestBody(dataForLocator);

                    byte[] requestBodyBytes;
                    var result = GetResponse(requestBodyString, out requestBodyBytes, out responseBodyString);

                    if (result != Result.Success)
                        return result;

                    result = ParseLocatorResponse(responseBodyString, dataForLocator, out mu);

                    if (result != Result.Success)
                        return result;
                }
                finally
                {
                    if (requestBodyString != null)
                    {
                        Trace.TraceInformation(
                            "YandexLocator requested position for vehicle {0}, time {1}, \nrequest:{2},response\n{3}",
                            dataForLocator.VehicleId,
                            mu != null ? TimeHelper.GetLocalTime(mu.Time, TimeZoneInfo.Local) : (DateTime?) null,
                            requestBodyString, 
                            responseBodyString);
                    }
                }

                _terminal.OnReceive(new ReceiveEventArgs(mu));

                return Result.Success;
            }

            private Result ParseLocatorResponse(string responseBodyString, DataForLocator dataForLocator,
                                              out MobilUnit.Unit.MobilUnit mu)
            {
                mu = null;
                var doc = XDocument.Parse(responseBodyString);

                if (doc.Root == null)
                {
                    Trace.TraceWarning("{0}: no root element");
                    return Result.Error;
                }

                var position = doc.Root.Descendants("position").FirstOrDefault();

                if (position == null)
                {
                    return Result.Error;
                }

                var latitude = XmlConvert.ToDecimal(position.Descendants("latitude").First().Value);
                var longitude = XmlConvert.ToDecimal(position.Descendants("longitude").First().Value);
                var altitude = XmlConvert.ToDecimal(position.Descendants("altitude").First().Value);
                var precision = XmlConvert.ToDecimal(position.Descendants("precision").First().Value);
                var type = position.Descendants("type").First().Value;

                int logTime;
                PositionType positionType;
                switch (type)
                {
                    case "gsm":
                        if (dataForLocator.Cells == null)
                        {
                            Trace.TraceError("Locator returned unexpected type of position {0}", type);
                            return Result.Error;
                        }
                        logTime = dataForLocator.Cells.First().LogTime;
                        positionType = PositionType.YandexGsm;
                        break;
                    case "wifi":
                        if (dataForLocator.Wlans == null)
                        {
                            Trace.TraceError("Locator returned unexpected type of position {0}", type);
                            return Result.Error;
                        }
                        logTime = dataForLocator.Wlans.First().LogTime;
                        positionType = PositionType.YandexWifi;
                        break;
                    default:
                        Trace.TraceError("Unknown locator position type: {0}", type);
                        return Result.Error;
                }

                mu = new MobilUnit.Unit.MobilUnit
                    {
                        Unique = dataForLocator.VehicleId,
                        Time = logTime,
                        Latitude = (double) latitude,
                        Longitude = (double) longitude,
                        Height = (int) altitude,
                        Radius = (int) precision,
                        TermID = _terminal.ID,
                        Type = positionType,
                        CorrectGPS = true,
                    };

                return Result.Success;
            }

            private Result GetResponse(string requestBodyString, out byte[] requestBodyBytes, out string responseBodyString)
            {
                var httpRequest = (HttpWebRequest) WebRequest.CreateDefault(_url);
                httpRequest.Headers.Add("Accept-Encoding", "identity");
                httpRequest.Proxy = WebRequest.DefaultWebProxy;
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.Method = "POST";
                httpRequest.Timeout = 10000;
                httpRequest.ServicePoint.Expect100Continue = false;

                requestBodyBytes = Encoding.ASCII.GetBytes(requestBodyString);

                var requestStream = httpRequest.GetRequestStream();
                requestStream.Write(requestBodyBytes, 0, requestBodyBytes.Length);
                requestStream.Close();

                HttpWebResponse loWebResponse;

                try
                {
                    loWebResponse = (HttpWebResponse) httpRequest.GetResponse();
                }
                catch (WebException e)
                {
                    responseBodyString = null;
                    //TODO: анализировать код ошибки, а не текст
                    if (e.Message == "The remote server returned an error: (404) Not Found.")
                    {
                        return Result.PositionNotFound;
                    }

                    Trace.TraceError("{0}", e);
                    return Result.WebException;
                }

                var responseStream = loWebResponse.GetResponseStream();

                if (responseStream == null)
                {
                    Trace.TraceWarning("{0}: no response string");
                    responseBodyString = null;
                    return Result.Error;
                }

                using (var stringReader = new StreamReader(responseStream))
                    responseBodyString = stringReader.ReadToEnd();

                return Result.Success;
            }

            private string GetRequestBody(DataForLocator dataForLocator)
            {
                var doc = new XDocument(
                    new XElement("ya_lbs_request",
                                 new XElement("common",
                                              new XElement("version", "1.0"),
                                              new XElement("api_key", _yandexApiKey)),
                                 dataForLocator.Cells == null
                                     ? null
                                     : new XElement("gsm_cells",
                                                    dataForLocator.Cells.Select(
                                                        c => new XElement("cell",
                                                                          new XElement("countrycode", c.CountryCode),
                                                                          new XElement("operatorid", c.NetworkCode),
                                                                          new XElement("cellid", c.CellID),
                                                                          new XElement("lac", c.LAC)
                                                            //, new XElement("signal_strength", (-c.SignalStrength).ToString(CultureInfo.InvariantCulture))
                                                                 ))),
                                 dataForLocator.Wlans == null
                                     ? null
                                     : new XElement("wifi_networks",
                                                    dataForLocator.Wlans.Select(
                                                        w => new XElement("network",
                                                                          new XElement("mac", w.WlanMacAddress)
                                                            //, new XElement("signal_strength", (-w.SignalStrength).ToString(CultureInfo.InvariantCulture))
                                                                 ))
                                           )));

                var message = "xml=" + doc;
                return message;
            }
        
        }

        private class NetmonitoringProcessor : BackgroundProcessor
        {
            private readonly string _yandexApiKey;
            private readonly DataBaseManager _db;
            private const string UriFormatString = "http://api.lbs.yandex.net/partners/wifipool?uuid={0}&ver=1.0";

            public NetmonitoringProcessor(string yandexApiKey)
            {
                _yandexApiKey = yandexApiKey;
                _db = new DataBaseManager();
            }

            private enum Result
            {
                Success,
                NoData,
                Error,
                UnexpectedError,
            }

            protected override bool Do()
            {
                Result result;
                try
                {
                    result = PublishOneVehicle();
                } 
                catch (Exception ex)
                {
                    Trace.TraceError("Unexpected error when publishing netmonitoring data: {0}", ex);
                    result = Result.UnexpectedError;
                }

                switch (result)
                {
                    case Result.Success:
                        return true;
                    case Result.NoData:
                        return false;
                    default:
                        return false;
                }
            }

            private Result PublishOneVehicle()
            {
                var dataForNetmonitoring = _db.GetDataForNetmonitoring();

                if (dataForNetmonitoring == null ||
                    dataForNetmonitoring.Positions == null ||
                    dataForNetmonitoring.Positions.Count == 0 ||
                    (dataForNetmonitoring.Cells == null || dataForNetmonitoring.Cells.Count == 0) &&
                    (dataForNetmonitoring.Wlans == null || dataForNetmonitoring.Wlans.Count == 0))
                {
                    return Result.NoData;
                }



                string requestBodyString = null;
                string responseBodyString = null;

                try
                {
                    requestBodyString = "data=" + dataForNetmonitoring.GetXmlForYandex(_yandexApiKey);
                    var requestUri =
                        new Uri(string.Format(UriFormatString,
                                              dataForNetmonitoring.Uuid.ToString(DataForNetmonitoring.UuidFormat)));

                    var httpRequest = (HttpWebRequest) WebRequest.CreateDefault(requestUri);
                    httpRequest.Headers.Add("Accept-Encoding", "identity");
                    httpRequest.Proxy = WebRequest.DefaultWebProxy;
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.Method = "POST";
                    httpRequest.Timeout = 10000;
                    httpRequest.ServicePoint.Expect100Continue = false;

                    var body = Encoding.ASCII.GetBytes(requestBodyString);
                    var requestStream = httpRequest.GetRequestStream();
                    requestStream.Write(body, 0, body.Length);
                    requestStream.Close();

                    var webResponse = (HttpWebResponse) httpRequest.GetResponse();

                    var responseStream = webResponse.GetResponseStream();

                    if (responseStream == null)
                    {
                        Trace.TraceError("{0} returned empty response stream when publishing data {1}",
                                         requestUri, requestBodyString);
                        return Result.Error;
                    }

                    using (var stringReader = new StreamReader(responseStream))
                        responseBodyString = stringReader.ReadToEnd();

                    if (!IsOK(responseBodyString))
                    {
                        Trace.TraceError("{0} returned unexpected result {1} when publishing {2}",
                                         requestUri, responseBodyString, requestBodyString);
                        return Result.Error;
                    }

                    return Result.Success;
                }
                finally
                {
                    Trace.TraceInformation(
                        "YandexLocator published positions \nrequest:{0},response\n{1}",
                        requestBodyString, responseBodyString);
                }
            }

            private bool IsOK(string s)
            {
                var doc = XDocument.Parse(s);
                if (doc.Root == null || doc.Root.Name != "wifipool")
                    return false;

                var error = doc.Root.Attribute("error");
                return error != null && error.Value == "0";
            }
        }
    }
}
