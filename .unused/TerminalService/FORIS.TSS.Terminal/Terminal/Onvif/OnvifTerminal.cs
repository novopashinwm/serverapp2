﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reactive.Disposables;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.TerminalService.Interfaces;
using kipods.api;
using odm.core;
using onvif.services;
using onvif.utils;
using utils;

namespace FORIS.TSS.Terminal.Terminal.Onvif
{
    /// <summary>
    /// Терминал для приёма событий по протоколу ONVIF
    /// </summary>
    public class OnvifTerminal : _Terminal
    {
        private CompositeDisposable _disposables;
        private string _onvifHost;
        private NvtSessionFactory _factory;
        private Uri _onvifUrl;
        private OnvifEventManager _onvifEventManager;


        public OnvifTerminal(string name, ITerminalManager mgr) : base(name, mgr)
        {
        }

        public override void Initialization(params string[] initStrings)
        {
            base.Initialization(initStrings);

            if (initStrings.Length < 2 || string.IsNullOrWhiteSpace(initStrings[1]))
                return;

            _onvifHost = initStrings[1];
            var onvifUrlString = string.Format(@"http://{0}/onvif/device_service", _onvifHost);

            if (!Uri.TryCreate(onvifUrlString, UriKind.Absolute, out _onvifUrl))
            {
                Trace.TraceError("Invalid ONVIF URL: {0}", onvifUrlString);
                return;
            }

            CreateSession();
        }

        public override bool CanHandle(IStdCommand cmd)
        {
            return cmd.Type == CmdType.CreateReplayUri;
        }

        public override bool SendCommand(IStdCommand cmd)
        {
            switch (cmd.Type)
            {
                case CmdType.CreateReplayUri:
                    CreateReplayUri(cmd, cmd.Target);
                    return true;
                default:
                    return base.SendCommand(cmd);
            }
        }

        private void CreateReplayUri(IStdCommand command, IUnitInfo target)
        {
            if (!itmManager.FillUnitInfo(target) || target.DeviceID == null || target.DeviceID.Length == 0)
                return;

            var token = target.DeviceID;

            _onvifEventManager.CreateReplayUri(command, token);
        }

        CancellationTokenSource _cancellationTokenSource;

        private async void CreateSession()
        {
            Trace.TraceInformation("{0}: create session started", GetType());

            _factory = new NvtSessionFactory(null);
            if (_disposables == null)
                _disposables = new CompositeDisposable();

            if (_cancellationTokenSource == null)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                _disposables.Add(_cancellationTokenSource);
            }

            INvtSession session;
            try
            {
                session = await _factory.CreateSession(new[] {_onvifUrl}).StartAsTask(_cancellationTokenSource.Token);
            }
            catch (Exception e)
            {
                Trace.TraceError("{0}", e);
                DisconnectNowAndReconnectLater();
                return;
            }

            _onvifEventManager = new OnvifEventManager(this, session, _cancellationTokenSource);
            _disposables.Add(_onvifEventManager);
        }

        private void CloseSession()
        {
            _onvifEventManager = null;
            _cancellationTokenSource = null;

            Trace.TraceInformation("{0}: CloseSession", GetType());

            var disposables = _disposables;
            if (disposables == null)
                return;
            _disposables = null;

            disposables.Dispose();
        }

        private void CreateSessionLater()
        {
            Trace.TraceInformation("{0}: CreateSessionLater", GetType());
            _disposables = new CompositeDisposable();
            _disposables.Add(
                new Timer(delegate
                    {
                        CreateSession();
                    },
                    null,
                    TimeSpan.FromMinutes(1), TimeSpan.FromMilliseconds(-1)));
        }

        private void DisconnectNowAndReconnectLater()
        {
            CloseSession();
            CreateSessionLater();
        }

        public override void Dispose()
        {
            CloseSession();
            base.Dispose();
        }

        class OnvifEventManager : IDisposable
        {
            public OnvifEventManager(OnvifTerminal terminal, INvtSession session, CancellationTokenSource cancellationTokenSource)
            {
                _terminal = terminal;
                // TODO: Complete member initialization
                _session = session;
                _cancellationTokenSource = cancellationTokenSource;

                Run();
            }

            private readonly INvtSession _session;
            private readonly CancellationTokenSource _cancellationTokenSource;
            private readonly CompositeDisposable _disposables = new CompositeDisposable();
            private readonly OnvifTerminal _terminal;
            private N6Client _chanelsClient;

            public void Dispose()
            {
                _disposables.Dispose();
            }

            private async void Run()
            {
                var odmSess = new OdmSession(_session);

                var uriBuild = new UriBuilder(_session.deviceUri);
                uriBuild.Path = @"/n6/v0";
                Uri nv6Uri = uriBuild.Uri;
                _chanelsClient = new N6Client(nv6Uri, _session.credentials);

                ChannelDesription[] channels;
                try
                {
                    channels = (await _chanelsClient.GetAllChannels(_cancellationTokenSource.Token)).Where(p => p.enbled).ToArray();
                }
                catch (Exception e)
                {
                    ProcessError(e);
                    return;
                }
                if (channels.IsEmpty())
                {
                    Trace.TraceError(@"{0}: Server has not any enabled channels!!", GetType());
                    _terminal.DisconnectNowAndReconnectLater();
                    return;
                }


                _disposables.Add(odmSess.GetPullPointEvents()
                                       .Subscribe(
                                           ProcessEvent, 
                                           ProcessError
                                    ));
            }

            private void ProcessError(Exception err)
            {
                Trace.TraceError("OnvifTerminal: {0}", err);
                _terminal.DisconnectNowAndReconnectLater();
            }

            private void ProcessEvent(OnvifEvent evnt)
            {
                //Parse onvif event here

                if (evnt.message == null ||
                    evnt.message.source == null ||
                    evnt.message.source.simpleItem == null) return;

                var deviceIdString =
                    evnt.GetSourceValue("VideoAnalyticsConfigurationToken");
                if (string.IsNullOrWhiteSpace(deviceIdString))
                    return;

                var eventType = evnt.GetDataValue("OriginalEventTopic");
                if (eventType == null && evnt.topic.Any.Length != 0)
                    eventType = evnt.topic.Any[0].Value;
                if (string.IsNullOrWhiteSpace(eventType))
                    return;

                var snapshotPath = evnt.GetDataValue("SnapshotPath");
                if (string.IsNullOrWhiteSpace(snapshotPath))
                    return;

                var time = evnt.message.utcTime;

                if (time.Kind == DateTimeKind.Local)
                {
                    time = TimeZoneInfo.ConvertTimeToUtc(time);
                }

                var mu = new PictureUnit
                    {
                        TermID = _terminal.ID,
                        DeviceID = deviceIdString,
                        Time = TimeHelper.GetSecondsFromBase(time),
                        Url = "http://" + _terminal._onvifHost + snapshotPath,
                        CorrectGPS = false
                    };

                if (!_terminal.itmManager.FillUnitInfo(mu))
                    return;

                var sensor = GetEventSensor(eventType);

                if (sensor == null)
                    return;

                var includedInputs = new HashSet<int>();

                if (mu.SensorMap == null)
                    return;

                foreach (var legendGrouping in mu.SensorMap)
                {
                    foreach (var map in legendGrouping.Value)
                    {
                        var input = map.InputNumber;
                        includedInputs.Add(input);
                    }
                }

                foreach (var input in includedInputs)
                {
                    mu.SetSensorValue(input, (int) sensor.Value == input ? 1 : 0);
                }

                if (!includedInputs.Contains((int) sensor.Value))
                    return;

                try
                {
                    using (var webClient = new WebClient())
                    {
                        mu.Bytes = webClient.DownloadData(mu.Url);
                    }
                }
                catch (Exception e)
                {
                    Trace.TraceError("Unable to get data from {0}: {1}", mu.Url, e);
                    return;
                }

                _terminal.OnReceive(new ReceiveEventArgs(mu));
            }

            private VideoAnaliticSensor? GetEventSensor(string eventType)
            {
                switch (eventType)
                {
                    case "tns1:VideoAnalytics/tnssns:MotionAlarm": return VideoAnaliticSensor.MotionAlarm;
                    case "tns1:RuleEngine/LineDetector/Crossed": return VideoAnaliticSensor.LineCrossed;
                    case "tns1:VideoAnalytics/FieldDetector/tnssns:LoiteringAlarm": return VideoAnaliticSensor.LoiteringAlarm;
                    case "tns1:RuleEngine/FieldDetector/tnssns:AbandonedAlarm": return VideoAnaliticSensor.AbandonedAlarm;
                    case "tns1:FaceDetection/tnssns:FaceDetected": return VideoAnaliticSensor.FaceDetected;
                    default:
                        return null;
                }
            }

            public async void CreateReplayUri(IStdCommand cmd, string token)
            {
                var recordingSession = _session as odm.onvif.IReplayAsync;
                ChannelDesription[] channels;
                try
                {
                    channels = (await _chanelsClient.GetAllChannels(_cancellationTokenSource.Token)).Where(p => p.enbled).ToArray();
                }
                catch (Exception e)
                {
                    Trace.TraceError("{0}", e);
                    return; //TODO: notify error
                }
                if (channels.Length == 0)
                    return; //TODO: notify not found
                var channel = channels.First(p => p.onvif.vac == token);
                var url = await recordingSession.GetReplayUri(channel.onvif.rec, CreateStreamSetup()).StartAsTask(_cancellationTokenSource.Token);
                RecordSummary summary = await _chanelsClient.GetRecordsSummary(channel.id, _cancellationTokenSource.Token);

                var fromValue = summary.GetFromValue();
                if (fromValue == null)
                    return; //TODO: notify empty stream

                Trace.TraceInformation("Replay uri created: {0} - start date {1}", url, fromValue);

                _terminal.OnNotify(new NotifyEventArgs(
                                       System.DateTime.UtcNow,
                                       TerminalEventMessage.VideoRecordReplayUrlCreated,
                                       null,
                                       Severity.Lvl0,
                                       NotifyCategory.Terminal,
                                       null,
                                       null,
                                       new CommandResult
                                           {
                                               CmdType = cmd.Type,
                                               Value = new VideoRecord
                                                   {
                                                       Uri = url,
                                                       Start = fromValue.Value
                                                   },
                                               Tag = cmd.Target
                                           }
                                       ));
            }

            StreamSetup CreateStreamSetup()
            {
                var strSetup = new StreamSetup();
                strSetup.stream = StreamType.rtpUnicast;
                strSetup.transport = new Transport();
                strSetup.transport.protocol = TransportProtocol.udp; //TODO: после исправления ошибки в KIPOD переключить на TCP
                strSetup.transport.tunnel = null;
                return strSetup;
            }
        }
    }
}
