﻿using kipods.api;

namespace FORIS.TSS.Terminal.Terminal.Onvif
{
    internal static class RecordSummaryEx
    {
        public static System.DateTime? GetFromValue(this RecordSummary inst)
        {
            System.DateTime dt;
            if (System.DateTime.TryParse(inst.from, out dt))
                return dt;
            return null;
        }
    }
}
