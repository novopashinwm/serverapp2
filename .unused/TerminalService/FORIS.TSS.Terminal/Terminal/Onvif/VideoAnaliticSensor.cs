﻿namespace FORIS.TSS.Terminal.Terminal.Onvif
{
    public enum VideoAnaliticSensor
    {
        MotionAlarm = 1,
        LineCrossed = 2,
        FaceDetected = 3,
        AbandonedAlarm = 4,
        LoiteringAlarm = 5
    }
}
