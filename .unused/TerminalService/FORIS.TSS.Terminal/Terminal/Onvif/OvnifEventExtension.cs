﻿using System.Linq;
using onvif.utils;

namespace FORIS.TSS.Terminal.Terminal.Onvif
{
    static class OvnifEventExtension
    {
        public static string GetSourceValue(this OnvifEvent @event, string key)
        {
            return @event.message.source.simpleItem.Where(
                x => x.name == key).Select(x => x.value).FirstOrDefault();
        }

        public static string GetDataValue(this OnvifEvent @event, string key)
        {
            return @event.message.data.simpleItem.Where(
                x => x.name == key).Select(x => x.value).FirstOrDefault();
        }
    }
}
