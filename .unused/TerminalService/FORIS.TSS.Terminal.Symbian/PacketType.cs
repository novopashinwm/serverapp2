﻿namespace FORIS.TSS.Terminal.Symbian
{
    /// <summary>
    /// Тип пакета данных от устройства
    /// </summary>
    enum PacketType
    {
        /// <summary>
        /// Данные от GPS и датчиков телефона (заряд батареи, тревожная кнопка)
        /// </summary>
        Data = 0x01,
        /// <summary>
        /// Нет данных, но есть GPRS-соединение. Передаются только значения датчиков.
        /// </summary>
        NoData = 0x02,
        /// <summary> Пакет с датчиками </summary>
        SensorBucket = 3,
        /// <summary>
        /// Пакет, содержащий отладочные данные
        /// </summary>
        Debug = 0x04,
        /// <summary>
        /// Пакет, содержащий информацию об ошибке
        /// </summary>
        Error = 0x08,
        
        /// <summary>
        /// Пакет, содержащий данные о сотовой сети вокруг устройства
        /// </summary>
        CellNetwork = 0x10,

        /// <summary>
        /// Пакет, содержащий данные о WiFi-сетях вокруг устройства
        /// </summary>
        WiFiNetwork = 0x20
    }
}
