﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Symbian
{
    // ReSharper disable once InconsistentNaming
    public class S60V3FP : Device, IHttpDevice
    {
        public S60V3FP()
            : base(null, new[] { ControllerType.Names.NikaDevice })
        {
            
        }

        public bool CheckDeviceId = true;

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            string deviceID = null;
            var solidPacket = false;
            if (IsDataValidRegistrationPacket(data, count))
            {
                var imeiLength = data[PacketPrefix.Length];
                deviceID = Encoding.ASCII.GetString(data, PacketPrefix.Length + 1, imeiLength);

                var confirm = GetConfirm(deviceID);

                var registrationPacketLength = PacketPrefix.Length + 1 + imeiLength + 4;
                if (registrationPacketLength < count)
                {
                    var userData = new byte[count - registrationPacketLength];
                    Array.Copy(data, registrationPacketLength, userData, 0, userData.Length);
                    data = userData;
                    count -= registrationPacketLength;
                    solidPacket = true;
                }
                else
                {
                    var result = new List<object> {GetConfirmPacket(confirm)};

                    if (confirm == Confirm.Success)
                        result.Add(new ReceiverStoreToStateMessage
                            {
                                StateData = new S60V3FPReceiverState
                                    {
                                        DeviceID = deviceID
                                    }
                            });

                    return result;
                }
            }

            var expectedBytesCount = new S60V3FPDataReader(data, 0, 4).ReadUint(4) + 8;  /*Length + CRC*/
            if (count < 4 || count < expectedBytesCount)
            {
                bufferRest = new byte[count];
                Array.Copy(data, 0, bufferRest, 0, count);
                return null;
            }

            if (!solidPacket && !CheckCRC(data, count))
            {
                return new List<object>
                           {
                               GetConfirmPacket(Confirm.WrongCRC)
                           };
            }

            if (deviceID == null && stateData != null)
                deviceID = ((S60V3FPReceiverState)stateData).DeviceID;

            if (deviceID == null)
                return new ArrayList {GetConfirmPacket(Confirm.UnknownDeviceId)};

            return ParseAnyPacket(data, count, deviceID).ToList();
        }

        private enum Confirm
        {
            UnknownDeviceId = 0,
            Success = 1,
            Blocked = 2,
            NoService = 3,
            WrongCRC = 4
        }

        private Confirm GetConfirm(string deviceID)
        {
            var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
            ui.DeviceID = deviceID;
            Manager.FillUnitInfo(ui);
            return GetConfirm(ui);
        }

        private static Confirm GetConfirm(UnitInfo ui)
        {
            if (ui.Unique < 1 && !string.IsNullOrEmpty(ui.DeviceID)) 
                return Confirm.UnknownDeviceId;

            var blockingDate = ui.BlockingDate;
            if (blockingDate != null)
                return Confirm.Blocked;

            if (!string.IsNullOrWhiteSpace(ui.Asid) && string.IsNullOrEmpty(ui.DeviceID))
                return Confirm.NoService;

            return Confirm.Success;
        }

        private static ConfirmPacket GetConfirmPacket(Confirm confirm)
        {
            return new ConfirmPacket {Data = new[] {(byte) confirm}};
        }

        private class AllPackets
        {
            public readonly Dictionary<int, PositionPacket> DataPackets = new Dictionary<int, PositionPacket>();
            public readonly Dictionary<int, ErrorPacket> ErrorPackets = new Dictionary<int, ErrorPacket>();
            public readonly Dictionary<int, List<CellNetworkRecord>> CellNetworkRecords = new Dictionary<int, List<CellNetworkRecord>>();
            public readonly Dictionary<int, List<WlanRecord>> WlanRecords = new Dictionary<int, List<WlanRecord>>();
            public readonly Dictionary<int, List<DebugPacket>> DebugPackets = new Dictionary<int, List<DebugPacket>>();
            public readonly Dictionary<int, SensorPacket> SensorPackets = new Dictionary<int, SensorPacket>();
        }

        public IEnumerable<object> ParseAnyPacket(byte[] data, int startIndex, int count, string deviceID)
        {
            var allPackets = ParseAnyPacket(data, startIndex, count);
            var allLogTimes = new SortedSet<int>(
                allPackets.DataPackets.Keys.
                    Union(allPackets.ErrorPackets.Keys).
                    Union(allPackets.CellNetworkRecords.Keys).
                    Union(allPackets.WlanRecords.Keys).
                    Union(allPackets.DebugPackets.Keys)).
                    Union(allPackets.SensorPackets.Keys);
            
            foreach (var logTime in allLogTimes)
            {
                var mu = UnitReceived();
                mu.Time = logTime;
                mu.DeviceID = deviceID;

                ErrorPacket error;
                if (allPackets.ErrorPackets.TryGetValue(logTime, out error))
                {
                    if (mu.SensorValues == null)
                        mu.SensorValues = new Dictionary<int, long>();

                    mu.SensorValues[(int) S60V3FPSensor.ErrorSource] = error.ErrorSource;
                    mu.SensorValues[(int) S60V3FPSensor.ErrorCode] = error.ErrorCode;
                }

                List<CellNetworkRecord> cellNetworkRecords;
                if (allPackets.CellNetworkRecords.TryGetValue(logTime, out cellNetworkRecords))
                    mu.CellNetworks = cellNetworkRecords.ToArray();

                List<WlanRecord> wlanRecords;
                if (allPackets.WlanRecords.TryGetValue(logTime, out wlanRecords))
                    mu.Wlans = wlanRecords.ToArray();

                List<DebugPacket> debugPackets;
                if (allPackets.DebugPackets.TryGetValue(logTime, out debugPackets) && debugPackets.Count != 0)
                {
                    if (mu.SensorValues == null)
                        mu.SensorValues = new Dictionary<int, long>();

                    foreach (var debugPacket in debugPackets)
                        mu.SensorValues[debugPacket.Key] = debugPacket.Value;
                }

                PositionPacket position;
                if (allPackets.DataPackets.TryGetValue(logTime, out position))
                {
                    mu.Latitude = position.Latitude;
                    mu.Longitude = position.Longitude;
                    mu.Speed = position.Speed;
                    mu.Radius = position.Radius;
                    mu.Height = position.Height;
                    mu.Satellites = position.Satellites;
                    mu.Course = position.Course;
                    mu.CorrectGPS = position.CorrectGPS;
                }
                else
                {
                    mu.CorrectGPS = false;
                }

                SensorPacket sensorPacket;
                if (allPackets.SensorPackets.TryGetValue(logTime, out sensorPacket))
                {
                    if (mu.SensorValues == null)
                        mu.SensorValues = new Dictionary<int, long>();
                    foreach (var record in sensorPacket.Records)
                    {
                        mu.SensorValues[record.Number] = (long) record.Value;
                    }
                }

                yield return mu;
            }

            yield return GetConfirmPacket(Confirm.Success);
        }

        private static AllPackets ParseAnyPacket(byte[] data, int startIndex, int count)
        {
            var totalBucketsRead = 0;
            var totalPacketsRead = 0;

            var reader = new S60V3FPDataReader(data, startIndex, count);
            var allPackets = new AllPackets();
            while (reader.Any())
            {
                var packetType = (PacketType)reader.ReadUint(1);
                var packetCount = reader.ReadUint(packetType == PacketType.SensorBucket ? 2 : 1);

                    switch (packetType)
                    {
                        case PacketType.Data:
                            for (var i = 0; i != packetCount; ++i)
                            {
                                var positionPacket = ParseDataPacket(reader);
                                allPackets.DataPackets[positionPacket.Time] = positionPacket;
                            }
                            break;
                        case PacketType.SensorBucket:
                            var packets = ParseSensorPacket(reader, packetCount);
                            foreach (var packet in packets)
                                allPackets.SensorPackets[(int) (packet.Time/1000)] = packet;
                            break;
                        case PacketType.NoData:
                            break;
                        case PacketType.Error:
                            for (var i = 0; i != packetCount; ++i)
                            {
                                var errorPacket = ParseErrorPacket(reader);
                                allPackets.ErrorPackets[errorPacket.Time] = errorPacket;
                            }
                            break;
                        case PacketType.CellNetwork:
                        {
                            if (packetCount != 0)
                            {
                                for (var i = 0; i != packetCount; ++i)
                                {
                                    var cellNetworkRecord = ParseNetworkRecord(reader, true);
                                    List<CellNetworkRecord> list;
                                    if (!allPackets.CellNetworkRecords.TryGetValue(cellNetworkRecord.LogTime, out list))
                                    {
                                        allPackets.CellNetworkRecords.Add(cellNetworkRecord.LogTime,
                                            list = new List<CellNetworkRecord>());
                                    }

                                    list.Add(cellNetworkRecord);
                                }
                            }
                        }
                            break;
                        case PacketType.WiFiNetwork:
                        {
                            if (packetCount != 0)
                            {
                                for (var i = 0; i != packetCount; ++i)
                                {
                                    var wlanRecord = ParseWlanRecord(reader);

                                    List<WlanRecord> list;
                                    if (!allPackets.WlanRecords.TryGetValue(wlanRecord.LogTime, out list))
                                        allPackets.WlanRecords.Add(wlanRecord.LogTime, list = new List<WlanRecord>());

                                    list.Add(wlanRecord);
                                }
                            }
                        }
                            break;
                        case PacketType.Debug:
                        {
                            for (var i = 0; i != packetCount; ++i)
                            {
                                var debugPacket = ParseDebugPacket(reader);

                                List<DebugPacket> list;
                                if (!allPackets.DebugPackets.TryGetValue(debugPacket.LogTime, out list))
                                    allPackets.DebugPackets.Add(debugPacket.LogTime, list = new List<DebugPacket>());

                                list.Add(debugPacket);
                            }
                        }
                            break;
                        default:
                            throw new FormatException("Packet type " + packetType + " is not supported at position " +
                                                      (reader.Position - 2) + " after " + totalBucketsRead +
                                                      " buckets read ( " + totalPacketsRead + " records read)");
                    }

                ++totalBucketsRead;
                totalPacketsRead += (int)packetCount;
            }
            return allPackets;
        }

        private static IEnumerable<SensorPacket> ParseSensorPacket(S60V3FPDataReader reader, uint recordCount)
        {
            SensorPacket result = null;

            for (var i = 0; i != recordCount; ++i)
            {
                var number = (ushort) reader.ReadUint(2);
                var size = reader.ReadByte();
                var value = reader.ReadUint64(size);
                if (number == 0)
                {
                    if (result != null && result.Records.Count != 0)
                        yield return result;
                    result = new SensorPacket((long) value);
                }

                if (result != null)
                {
                    result.Records.Add(new SensorRecord
                    {
                        Number = number,
                        Value = value
                    });
                }
            }

            if (result != null && result.Records.Count != 0)
                yield return result;
        }

        private static WlanRecord ParseWlanRecord(S60V3FPDataReader reader)
        {
            var logTime = (int) reader.ReadUint(4);
            var number = (int) reader.ReadByte();
            var macAddressLength = reader.ReadByte();
            var macAddress = reader.ReadBytesAsHexString(macAddressLength);
            
            var signalStrength = reader.ReadByte();
            var wlanSsid = reader.ReadAsciiString8();
            var channelNumber = reader.ReadByte();
            return new WlanRecord
                       {
                           LogTime = logTime,
                           Number = number,
                           WlanMacAddress = macAddress,
                           SignalStrength = signalStrength,
                           WlanSSID = wlanSsid,
                           ChannelNumber = channelNumber == 0xFF ? (byte?)null : channelNumber
                       };
        }

        private static CellNetworkRecord ParseNetworkRecord(S60V3FPDataReader reader, bool readNumber)
        {
            return new CellNetworkRecord
                       {
                           LogTime = (int)reader.ReadUint(4),
                           Number = readNumber ? (int)reader.ReadUint(1) : 0,
                           CountryCode = reader.ReadAsciiString8(),
                           NetworkCode = reader.ReadAsciiString8(),
                           CellID = (int)reader.ReadUint(4),
                           LAC = (int)reader.ReadUint(4),
                           SignalStrength = (int)reader.ReadUint(4)
                       };
        }

        public IEnumerable<object> ParseAnyPacket(byte[] data, int count, string deviceID)
        {
            return ParseAnyPacket(data, 4 /*Length*/, count - 4/*CRC*/, deviceID);
        }

        private class ErrorPacket
        {
            public int Time;
            public uint ErrorSource;
            public uint ErrorCode;
        }

        private static ErrorPacket ParseErrorPacket(S60V3FPDataReader reader)
        {
            var result = new ErrorPacket
            {
                Time = (int)reader.ReadUint(4),
                ErrorSource = reader.ReadUint(4),
                ErrorCode = reader.ReadUint(4),
            };

            return result;
        }

        private class DebugPacket
        {
            public int LogTime;
            public int Key;
            public int Value;
        }

        private static DebugPacket ParseDebugPacket(S60V3FPDataReader reader)
        {
            var logTime = (int)reader.ReadUint(4);
            var key = reader.ReadUint(4);
            var value = reader.ReadUint(4);
            return new DebugPacket
                       {
                           LogTime = logTime,
                           Key = (int) key,
                           Value = (int) value
                       };
        }

        private class PositionPacket
        {
            public int Time;
            public double Latitude;
            public double Longitude;
            public int Speed;
            public int Radius;
            public int Height;
            public int Satellites;
            public int Course;
            public bool CorrectGPS;
        }

        private class SensorPacket
        {
            /// <summary> Время в миллисекундах от Unix Epoch</summary>
            public readonly long Time;
            
            /// <summary> Датчики, значения которых были зарегистрированы в это время</summary>
            public readonly List<SensorRecord> Records = new List<SensorRecord>();

            public SensorPacket(long time)
            {
                Time = time;
            }
        }

        private struct SensorRecord
        {
            public ushort Number;
            public ulong Value;
        }

        private static PositionPacket ParseDataPacket(S60V3FPDataReader reader)
        {
            var position = new PositionPacket();
            position.Time = (int) reader.ReadUint(4);
            var rawLatitude = reader.ReadUint(4);
            position.Latitude = (rawLatitude*180.0/((((long) 1) << 32))) - 90;
            var rawLongitude = reader.ReadUint(4);
            position.Longitude = (rawLongitude*360.0/((((long) 1) << 32))) - 180;
            position.Speed = (int) reader.ReadUint(2);
            position.Radius = (int) reader.ReadUint(2);
            position.Height = (int) reader.ReadUint(2);
            position.Satellites = (int) reader.ReadUint(1);
            position.Course = (int) (reader.ReadUint(1)*360.0/(1 << 8));

            position.CorrectGPS = 4 <= position.Satellites &&
                -90 < position.Latitude && position.Latitude < 90 &&
                -180 <= position.Longitude && position.Longitude <= 180;

            return position;
        }

        public static bool CheckCRC(byte[] data, int count)
        {
            if (count < 5)
                return false;

            var receivedCRC = (UInt32)((data[count - 4] << 24) |
                                        (data[count - 3] << 16) |
                                        (data[count - 2] << 8) |
                                        (data[count - 1]));

            var computedCRC = GetCRC32(data, count - 4);

            return (receivedCRC == computedCRC);
        }

        private const string PacketString = "S60V3FP";
        private static readonly byte[] PacketPrefix = Encoding.ASCII.GetBytes(PacketString);


        public override bool SupportData(byte[] data)
        {
            return IsDataValidRegistrationPacket(data, data.Length);
        }

        private static bool IsDataValidRegistrationPacket(byte[] data, int count)
        {
            //PacketPrefix.Length байта - идентификатор протокола "S60V3FP"
            //Далее 1 байт - длина IMEI
            //Затем собственно IMEI
            //В конце 4 байта - контрольная сумма CRC32

            if (count < PacketPrefix.Length ||
                data.Length < PacketPrefix.Length)
                return false;

            for (var i = 0; i != PacketPrefix.Length; ++i)
            {
                if (PacketPrefix[i] != data[i])
                    return false;
            }

            var imeiLength = data[PacketPrefix.Length];
            if (count - 1 - PacketPrefix.Length - 4 < imeiLength)
                return false;

            if (!CheckCRC(data, PacketPrefix.Length + 1 + imeiLength + 4))
                return false;

            return true;
        }

        private static UInt32 GetCRC32(byte[] buf, int len)
        {
            int p = 0;
            int q = len - 1;
            uint crc = 0;
            while (p <= q)
            {
                crc = (crc >> 8) ^ CRCTable[(crc ^ buf[p]) & 0xff];
                ++p;
            }

            return crc;
        }

        bool IHttpDevice.CanProcess(IHttpRequest request)
        {
            return (request.XNikaDevice == PacketString);
        }

        public IList<object> ParseHttpRequestBody(string text)
        {
            if (text == null)
                return null;
            if (text.StartsWith("data="))
            {
                text = text.Substring("data=".Length);
                text = Uri.UnescapeDataString(text);
            }

            var data = Convert.FromBase64String(text);

            if (!CheckCRC(data, data.Length))
            {
                return new List<object>
                           {
                               GetConfirmPacket(Confirm.WrongCRC)
                           };
            }

            return ParseAnyPacket(data, data.Length, null).ToList();
        }

        IList<object> IHttpDevice.OnData(IHttpRequest request)
        {
            var items = ParseHttpRequestBody(request.Text);

            if (items == null)
                return null;

            foreach (var item in items)
            {
                var mu = item as IMobilUnit;
                if (mu == null)
                    continue;
                mu.DeviceID = GetDeviceID(request);
            }

            items.Add(new HttpResponse
            {
                Text = GetResponseString(request)
            });

            return items;
        }

        private static string GetDeviceID(IHttpRequest request)
        {
            return request.XNikaDeviceID;
        }

        private string GetResponseString(IHttpRequest request)
        {
            Confirm confirm;

            if (string.IsNullOrWhiteSpace(request.Requestor))
            {
                confirm = GetConfirm(GetDeviceID(request));

                if (confirm != Confirm.Success)
                    return ((int)confirm).ToString(CultureInfo.InvariantCulture) + " " + confirm;
            }
            else
            {
                var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
                ui.Asid = request.Requestor;

                Manager.FillUnitInfo(ui);

                confirm = GetConfirm(ui);

                if (confirm == Confirm.Success)
                    return "DeviceID=" + ui.DeviceID;

                return confirm.ToString();
            }

            return "200";
        }


        private static readonly uint[] CRCTable =
            new uint[]
                {
#region CRCTable
                    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,
                    0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
                    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
                    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
                    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
                    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
                    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,
                    0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
                    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
                    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
                    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
                    0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
                    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,
                    0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
                    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
                    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
                    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,
                    0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
                    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,
                    0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
                    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
                    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
                    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,
                    0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
                    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
                    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
                    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
                    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
                    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,
                    0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
                    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,
                    0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
                    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
                    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
                    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
                    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
                    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,
                    0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
                    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
                    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
                    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,
                    0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
                    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,
                    0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
                    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
                    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
                    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,
                    0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
                    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,
                    0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
                    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
                    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
                    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,
                    0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
                    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
                    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
                    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
                    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
                    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,
                    0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
                    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,
                    0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
                    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
                    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
#endregion
                };
    }
}
