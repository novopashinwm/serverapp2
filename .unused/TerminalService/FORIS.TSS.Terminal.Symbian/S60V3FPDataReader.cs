﻿using System;
using System.Text;

namespace FORIS.TSS.Terminal.Symbian
{
    class S60V3FPDataReader
    {
        private readonly byte[] _data;
        private readonly int _endIndex;
        private int _index;

        public S60V3FPDataReader(byte[] data, int startIndex, int endIndex)
        {
            _data = data;
            _index = startIndex;
            _endIndex = endIndex;
        }

        public int Position
        {
            get { return _index; }
        }

        public uint ReadUint(int length)
        {
            uint result = 0;
            for (var i = 0; i != length; ++i)
            {
                result <<= 8;
                result |= _data[_index];
                ++_index;
            }
            return result;
        }

        public byte ReadByte()
        {
            return _data[_index++];
        }

        /// <summary>
        /// Читает из массива строку, в формате [LENGTH, STRING]
        /// LENGTH - длина строки, 1 байт
        /// STRING - содержимое строки длиной LENGTH в формате ASCII
        /// </summary>
        public string ReadAsciiString8()
        {
            var length = ReadByte();
            var result = Encoding.ASCII.GetString(_data, _index, length);
            _index += length;
            return result;
        }

        public bool Any()
        {
            return _index != _endIndex;
        }

        public string ReadBytesAsHexString(int count)
        {
            const string hexTable = "0123456789ABCDEF"; 
            
            var result = new StringBuilder(count * 2);
            for (var i = 0; i != count; ++i)
            {
                result.Append(hexTable[_data[_index]/16]);
                result.Append(hexTable[_data[_index]%16]);
                ++_index;
            }

            return result.ToString();
        }

        public UInt64 ReadUint64(int length)
        {
            UInt64 result = 0;
            for (var i = 0; i != length; ++i)
            {
                result <<= 8;
                result |= _data[_index];
                ++_index;
            }
            return result;
        }
    }
}
