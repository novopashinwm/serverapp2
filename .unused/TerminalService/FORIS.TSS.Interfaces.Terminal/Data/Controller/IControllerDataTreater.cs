﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.BusinessLogic.Terminal.Data.Controller
{
	public interface IControllerDataTreater : IDataTreater
	{
		#region ControllerType

		void ControllerTypeInsert(string name);
		void ControllerTypeUpdate(int controllerTypeId, string name);
		void ControllerTypeDelete(int controllerTypeId);

		#endregion ControllerType

		#region Controller

		void ControllerInsert(int controllerType, string phone, int number, string firmware);
		void ControllerUpdate(int controllerId, int controllerType, string phone, int number, string firmware);
		void ControllerDelete(int controllerId);

		#endregion Controller
	}
}