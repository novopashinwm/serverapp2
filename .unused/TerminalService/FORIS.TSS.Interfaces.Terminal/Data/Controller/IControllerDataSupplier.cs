﻿using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.BusinessLogic.Terminal.Data.Controller
{
	public interface IControllerDataSupplier : IDataSupplier<IControllerDataTreater>
	{
	}
}