﻿using FORIS.TSS.BusinessLogic.Terminal.Data.Controller;

namespace FORIS.TSS.BusinessLogic.Terminal.Data
{
	public interface ITerminalDataSupplierProvider :
		IControllerDataSupplierProvider
	{
	}
	public interface IControllerDataSupplierProvider
	{
		IControllerDataSupplier GetControllerDataSupplier();
	}
}