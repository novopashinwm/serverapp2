﻿using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace Interfaces.Report.Data.Report
{
	public interface IReportDataSupplier :
		IDataSupplier<IReportDataTreater>,
		ISecureDataSupplier<IReportSecurityDataSupplier, IOperatorDataSupplier>
	{

	}

	public interface IReportSecurityDataSupplier :
		IDataSupplier<IReportSecurityDataTreater>,
		ISecurityDataSupplier<IOperatorDataSupplier>
	{

	}
}