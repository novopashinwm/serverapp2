﻿using System;
using FORIS.TSS.BusinessLogic.Data;

namespace Interfaces.Report.Data.Report
{
	public interface IReportDataTreater :
		IDataTreater
	{
		#region Report

		void ReportInsert(
			string name,
			string description,
			Guid reportGuid,
			string reportFileName,
            string settings
			);

		void ReportUpdate(
			int reportId,
			string name,
			string description,
			Guid reportGuid,
            string reportFileName,
            string settings
            );

		void ReportDelete(
			int reportId
			);

		#endregion // Report
	}

	public interface IReportSecurityDataTreater :
		IDataTreater
	{

	}
}