﻿using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace Interfaces.Report.Data
{
	public interface IGeneratedReportDataSupplierProvider
	{
        IGeneratedReportDataSupplier GetGeneratedReportDataSupplier();
	}
}