﻿using Interfaces.Report.Data.Report;

namespace Interfaces.Report.Data
{
	public interface IReportDataSupplierProvider
	{
		IReportDataSupplier GetReportDataSupplier();
	}
}