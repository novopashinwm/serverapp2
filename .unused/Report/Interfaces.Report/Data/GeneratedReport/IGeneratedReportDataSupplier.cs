﻿using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;

namespace Interfaces.Report.Data.GeneratedReport
{
	public interface IGeneratedReportDataSupplier :
		IDataSupplier<IGeneratedReportDataTreater>,
        ISecureDataSupplier<IGeneratedReportSecurityDataSupplier, IOperatorDataSupplier>
	{

	}

    public interface IGeneratedReportSecurityDataSupplier :
        IDataSupplier<IGeneratedReportSecurityDataTreater>,
		ISecurityDataSupplier<IOperatorDataSupplier>
	{

	}
}