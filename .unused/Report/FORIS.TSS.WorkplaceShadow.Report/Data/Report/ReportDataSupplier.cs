﻿using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceShadow.Report.Data.Report
{
	/// <summary>
	/// Поставщик данных правил для модулей
	/// </summary>
	public class ReportDataSupplier :
		ModuleDataSupplier<IReportClientDataProvider, IReportDataSupplier>,
		IReportDataSupplier
	{
		#region Constructor & Dispose

		public ReportDataSupplier( IContainer container ) : this()
		{
			container.Add( this );
		}
		public ReportDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

		public IReportSecurityDataSupplier SecurityDataSupplier
		{
			get
			{
				return
					this.dataSupplier != null
						? this.dataSupplier.SecurityDataSupplier
						: null;
			}
		}

		public IReportDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IReportDataSupplier GetDataSupplier()
		{
			return this.ClientProvider.GetReportClientData();
		}
	}
}