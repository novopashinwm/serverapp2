﻿using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;
using IReportSecurityDataSupplier=Interfaces.Report.Data.Report.IReportSecurityDataSupplier;

namespace FORIS.TSS.WorkplaceShadow.Report.Data.GeneratedReport
{
	/// <summary>
	/// Поставщик данных правил для модулей
	/// </summary>
	public class GeneratedReportDataSupplier :
        ModuleDataSupplier<IGeneratedReportClientDataProvider, IGeneratedReportDataSupplier>,
		IGeneratedReportDataSupplier
	{
		#region Constructor & Dispose

		public GeneratedReportDataSupplier( IContainer container ) : this()
		{
			container.Add( this );
		}
		public GeneratedReportDataSupplier()
		{

		}

		#endregion // Constructor & Dispose

        public IGeneratedReportSecurityDataSupplier SecurityDataSupplier
		{
			get
			{
				return
					this.dataSupplier != null
						? this.dataSupplier.SecurityDataSupplier
						: null;
			}
		}

        public IGeneratedReportDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		protected override IGeneratedReportDataSupplier GetDataSupplier()
		{
            return this.ClientProvider.GetGeneratedReportClientData();
		}
	}
}