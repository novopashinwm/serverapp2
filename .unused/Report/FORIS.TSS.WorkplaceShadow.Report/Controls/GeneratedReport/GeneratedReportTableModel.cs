using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport

{
	public class GeneratedReportTableModel :
		ViewObjectTableModel<GeneratedReportXpRow>
	{
		#region Constructor & Dispose

		public GeneratedReportTableModel( IContainer container )
		{
			container.Add( this );
		}
		public GeneratedReportTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}