﻿namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport

{
    partial class GeneratedReportsListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.tbbNew = new System.Windows.Forms.ToolBarButton();
            this.separator2 = new System.Windows.Forms.ToolBarButton();
            this.tbbReportView = new System.Windows.Forms.ToolBarButton();
            this.tableGeneratedReports = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.tabControlGeneratedReports = new System.Windows.Forms.TabControl();
            this.tabPageLastReports = new System.Windows.Forms.TabPage();
            this.tableLastReports = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.tabPageGeneratedReports = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.tableGeneratedReports)).BeginInit();
            this.tabControlGeneratedReports.SuspendLayout();
            this.tabPageLastReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableLastReports)).BeginInit();
            this.tabPageGeneratedReports.SuspendLayout();
            this.SuspendLayout();
            // 
            // globalToolbar
            // 
            this.globalToolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbNew,
            this.separator2,
            this.tbbReportView});
            this.globalToolbar.Divider = false;
            this.globalToolbar.DropDownArrows = true;
            this.globalToolbar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.globalToolbar.Location = new System.Drawing.Point(0, 0);
            this.globalToolbar.Name = "globalToolbar";
            this.globalToolbar.ShowToolTips = true;
            this.globalToolbar.Size = new System.Drawing.Size(585, 26);
            this.globalToolbar.TabIndex = 4;
            // 
            // tbbNew
            // 
            this.tbbNew.ImageIndex = 0;
            this.tbbNew.Name = "tbbNew";
            this.tbbNew.ToolTipText = "Обновить";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbReportView
            // 
            this.tbbReportView.ImageIndex = 210;
            this.tbbReportView.Name = "tbbReportView";
            this.tbbReportView.ToolTipText = "Просмотр отчёта";
            // 
            // tableGeneratedReports
            // 
            this.tableGeneratedReports.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            this.tableGeneratedReports.BackColor = System.Drawing.Color.White;
            this.tableGeneratedReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableGeneratedReports.EnableHeaderContextMenu = false;
            this.tableGeneratedReports.FullRowSelect = true;
            this.tableGeneratedReports.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.tableGeneratedReports.HeaderContextMenu = null;
            this.tableGeneratedReports.Location = new System.Drawing.Point(3, 3);
            this.tableGeneratedReports.Name = "tableGeneratedReports";
            this.tableGeneratedReports.NoItemsText = "";
            this.tableGeneratedReports.Padding = new System.Windows.Forms.Padding(5);
            this.tableGeneratedReports.Size = new System.Drawing.Size(571, 199);
            this.tableGeneratedReports.TabIndex = 5;
            // 
            // tabControlGeneratedReports
            // 
            this.tabControlGeneratedReports.Controls.Add(this.tabPageLastReports);
            this.tabControlGeneratedReports.Controls.Add(this.tabPageGeneratedReports);
            this.tabControlGeneratedReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGeneratedReports.Location = new System.Drawing.Point(0, 26);
            this.tabControlGeneratedReports.Name = "tabControlGeneratedReports";
            this.tabControlGeneratedReports.SelectedIndex = 0;
            this.tabControlGeneratedReports.Size = new System.Drawing.Size(585, 231);
            this.tabControlGeneratedReports.TabIndex = 6;
            // 
            // tabPageLastReports
            // 
            this.tabPageLastReports.Controls.Add(this.tableLastReports);
            this.tabPageLastReports.Location = new System.Drawing.Point(4, 22);
            this.tabPageLastReports.Name = "tabPageLastReports";
            this.tabPageLastReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLastReports.Size = new System.Drawing.Size(577, 205);
            this.tabPageLastReports.TabIndex = 0;
            this.tabPageLastReports.Text = "Последние";
            this.tabPageLastReports.UseVisualStyleBackColor = true;
            // 
            // tableLastReports
            // 
            this.tableLastReports.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            this.tableLastReports.BackColor = System.Drawing.Color.White;
            this.tableLastReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLastReports.EnableHeaderContextMenu = false;
            this.tableLastReports.FullRowSelect = true;
            this.tableLastReports.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.tableLastReports.HeaderContextMenu = null;
            this.tableLastReports.Location = new System.Drawing.Point(3, 3);
            this.tableLastReports.Name = "tableLastReports";
            this.tableLastReports.NoItemsText = "";
            this.tableLastReports.Padding = new System.Windows.Forms.Padding(5);
            this.tableLastReports.Size = new System.Drawing.Size(571, 199);
            this.tableLastReports.TabIndex = 6;
            // 
            // tabPageGeneratedReports
            // 
            this.tabPageGeneratedReports.Controls.Add(this.tableGeneratedReports);
            this.tabPageGeneratedReports.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneratedReports.Name = "tabPageGeneratedReports";
            this.tabPageGeneratedReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneratedReports.Size = new System.Drawing.Size(577, 205);
            this.tabPageGeneratedReports.TabIndex = 1;
            this.tabPageGeneratedReports.Text = "Архив";
            this.tabPageGeneratedReports.UseVisualStyleBackColor = true;
            // 
            // GeneratedReportsListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tabControlGeneratedReports);
            this.Controls.Add(this.globalToolbar);
            this.Name = "GeneratedReportsListControl";
            this.Size = new System.Drawing.Size(585, 257);
            ((System.ComponentModel.ISupportInitialize)(this.tableGeneratedReports)).EndInit();
            this.tabControlGeneratedReports.ResumeLayout(false);
            this.tabPageLastReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableLastReports)).EndInit();
            this.tabPageGeneratedReports.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GlobalToolbar globalToolbar;
        private System.Windows.Forms.ToolBarButton tbbReportsGet;
        private System.Windows.Forms.ToolBarButton separator2;
        private System.Windows.Forms.ToolBarButton tbbReportView;
        private FORIS.TSS.TransportDispatcher.XPTable.Models.Table tableGeneratedReports;
        private System.Windows.Forms.ToolBarButton tbbNew;
        private System.Windows.Forms.TabControl tabControlGeneratedReports;
        private System.Windows.Forms.TabPage tabPageLastReports;
        private System.Windows.Forms.TabPage tabPageGeneratedReports;
        private FORIS.TSS.TransportDispatcher.XPTable.Models.Table tableLastReports;
    }
}
