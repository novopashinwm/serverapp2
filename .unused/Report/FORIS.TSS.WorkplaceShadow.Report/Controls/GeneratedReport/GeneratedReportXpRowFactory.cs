using FORIS.TSS.Helpers.Report.Data.GeneratedReport;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport

{
	public class GeneratedReportXpRowFactory :
        ViewObjectFactory<GeneratedReportRow, GeneratedReportXpRow, IGeneratedReportData>
	{
		public override GeneratedReportXpRow CreateViewObject( 
			int viewObjectId,
            GeneratedReportRow reportRow 
			)
		{
			return
				new GeneratedReportXpRow(
					viewObjectId,
					reportRow.Name,
					reportRow.Description
					);
		}
	}
}