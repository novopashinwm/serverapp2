﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.WorkplaceShadow.Tsp;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport
{
    public partial class GeneratedReportsListControl :
        TspUserControl,
        IDataItem<IReportData>
    {
        public GeneratedReportsListControl()
        {
            InitializeComponent();
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReportData Data
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}
