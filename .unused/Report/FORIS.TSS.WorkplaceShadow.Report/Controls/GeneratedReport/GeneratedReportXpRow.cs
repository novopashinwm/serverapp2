using System;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport

{
	public class GeneratedReportXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellDescription;
        private readonly Cell cellMobile;
        private readonly Cell cellDateTime;
        private readonly Cell cellTimeFrom;
        private readonly Cell cellTimeTo;
        private readonly Cell cellState;

		#endregion  // Cells

		private readonly int ID;

		public GeneratedReportXpRow( 
			int id,
			string name,
            //int mobile,
            //DateTime date,
            //DateTime dateFrom,
            //DateTime dateTo,
            //ReportStateEnum state,
			string description
			)
		{
			this.ID = id;

			#region Cells

            this.cellName = new Cell();
            this.cellMobile = new Cell();
            this.cellDateTime = new Cell();
            this.cellTimeFrom = new Cell();
            this.cellTimeTo = new Cell();
            this.cellState = new Cell();
            this.cellDescription = new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellName,
                        this.cellMobile,
                        this.cellDateTime,
                        this.cellTimeFrom,
                        this.cellTimeTo,
                        this.cellState,
						this.cellDescription
					}
				);

			#endregion  // Cells

			this.cellName.Text			= name;
            //this.cellMobile.Text = mobile.ToString();
            //this.cellDateTime.Text = date.ToShortTimeString(); 
            //this.cellTimeFrom.Text = dateFrom.ToShortTimeString(); 
            //this.cellTimeTo.Text = dateTo.ToShortTimeString();
            //this.cellState.Text = state.ToString();
			this.cellDescription.Text	= description;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}