using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Commands;
using FORIS.TSS.TransportDispatcher;
using FORIS.TSS.TransportDispatcher.XPTable.Events;
using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceSharnier.Report;
using FORIS.TSS.WorkplaceSharnier;
using Command=FORIS.TSS.Common.Commands.Command;
using CommandManager=FORIS.TSS.Common.Commands.CommandManager;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport
{
	public class GeneratedReportTableControl :
		UserControl,
		IViewObjectControl<GeneratedReportXpRow>,
		IClientItem<IReportClientDataProvider>
	{
		#region Controls & Components

		private IContainer components;
		private CommandManager commandManager;
		private ColumnModel columnModel;
		private TextColumn columnName;
		private TextColumn columnDescription;
		private Command cmdNew;
		private Command cmdProperties;
		private Command cmdRemove;
		private CommandInstance ciNewToolbarButton;
		private CommandInstance ciPropertiesToolbarButton;
		private CommandInstance ciRemoveToolbarButton;
		private ContextMenuStrip cmsTable;
		private Table table;
		private GlobalToolbar globalToolbar;
		private ToolBarButton tbbNew;
		private ToolBarButton tbbProperties;
        private ToolBarButton separator2;
		private ToolBarButton tbbLoad;
		private Command cmdSecurity;
        private CommandInstance ciSecurityToolbarButton;
        private Command cmdBuild;
        private CommandInstance ciBuildToolbarButton;
        private TextColumn columnMobile;
        private TextColumn columnDateTime;
        private TextColumn columnTimeFrom;
        private TextColumn columnTimeTo;
        private TextColumn columnState;
        private ToolBarButton tbbSecurity;
		private GeneratedReportTableModel tableModel;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public GeneratedReportTableControl()
		{
			InitializeComponent();

			this.Padding = new Padding( 0 );
		}

		#endregion // Constructor & Dispose

	    //private ReportParamsFactory reportParamsFactory = new ReportParamsFactory();

		#region Component Designer generated code

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneratedReportTableControl));
            this.cmsTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.table = new FORIS.TSS.TransportDispatcher.XPTable.Models.Table();
            this.columnModel = new FORIS.TSS.TransportDispatcher.XPTable.Models.ColumnModel();
            this.columnName = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnDescription = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.tableModel = new FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport.GeneratedReportTableModel(this.components);
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.tbbNew = new System.Windows.Forms.ToolBarButton();
            this.separator2 = new System.Windows.Forms.ToolBarButton();
            this.tbbSecurity = new System.Windows.Forms.ToolBarButton();
            this.tbbProperties = new System.Windows.Forms.ToolBarButton();
            this.commandManager = new FORIS.TSS.Common.Commands.CommandManager(this.components);
            this.cmdNew = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciNewToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdProperties = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciPropertiesToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdRemove = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciRemoveToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdSecurity = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciSecurityToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cmdBuild = new FORIS.TSS.Common.Commands.Command(this.components);
            this.ciBuildToolbarButton = new FORIS.TSS.Common.Commands.CommandInstance();
            this.columnMobile = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnDateTime = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnTimeFrom = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnTimeTo = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            this.columnState = new FORIS.TSS.TransportDispatcher.XPTable.Models.TextColumn();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.SuspendLayout();
            // 
            // cmsTable
            // 
            this.cmsTable.Name = "cmsTable";
            resources.ApplyResources(this.cmsTable, "cmsTable");
            // 
            // table
            // 
            this.table.AlternatingRowColor = System.Drawing.Color.WhiteSmoke;
            this.table.BackColor = System.Drawing.Color.White;
            this.table.ColumnModel = this.columnModel;
            this.table.ContextMenuStrip = this.cmsTable;
            resources.ApplyResources(this.table, "table");
            this.table.EnableHeaderContextMenu = false;
            this.table.FullRowSelect = true;
            this.table.GridLines = FORIS.TSS.TransportDispatcher.XPTable.Models.GridLines.Both;
            this.table.HeaderContextMenu = null;
            this.table.Name = "table";
            this.table.NoItemsText = "";
            this.table.TableModel = this.tableModel;
            this.table.SelectionChanged += new FORIS.TSS.TransportDispatcher.XPTable.Events.SelectionEventHandler(this.table_SelectionChanged);
            // 
            // columnModel
            // 
            this.columnModel.Columns.AddRange(new FORIS.TSS.TransportDispatcher.XPTable.Models.Column[] {
            this.columnName,
            this.columnMobile,
            this.columnDateTime,
            this.columnTimeFrom,
            this.columnTimeTo,
            this.columnState,
            this.columnDescription});
            // 
            // columnName
            // 
            this.columnName.Editor = null;
            resources.ApplyResources(this.columnName, "columnName");
            this.columnName.Width = 200;
            // 
            // columnDescription
            // 
            this.columnDescription.Editor = null;
            resources.ApplyResources(this.columnDescription, "columnDescription");
            this.columnDescription.Width = 400;
            // 
            // tableModel
            // 
            this.tableModel.SelectionAdjuster = null;
            // 
            // globalToolbar
            // 
            resources.ApplyResources(this.globalToolbar, "globalToolbar");
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tbbNew,
            this.separator2,
            this.tbbSecurity,
            this.tbbProperties});
            this.globalToolbar.Divider = false;
            this.globalToolbar.Name = "globalToolbar";
            // 
            // tbbNew
            // 
            resources.ApplyResources(this.tbbNew, "tbbNew");
            this.tbbNew.Name = "tbbNew";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbSecurity
            // 
            resources.ApplyResources(this.tbbSecurity, "tbbSecurity");
            this.tbbSecurity.Name = "tbbSecurity";
            // 
            // tbbProperties
            // 
            resources.ApplyResources(this.tbbProperties, "tbbProperties");
            this.tbbProperties.Name = "tbbProperties";
            // 
            // cmdNew
            // 
            this.cmdNew.Instances.Add(this.ciNewToolbarButton);
            this.cmdNew.Manager = this.commandManager;
            this.cmdNew.Execute += new System.EventHandler(this.cmdNew_Execute);
            // 
            // ciNewToolbarButton
            // 
            this.ciNewToolbarButton.Item = this.tbbNew;
            // 
            // cmdProperties
            // 
            this.cmdProperties.Instances.Add(this.ciPropertiesToolbarButton);
            this.cmdProperties.Manager = this.commandManager;
            this.cmdProperties.Execute += new System.EventHandler(this.cmdProperties_Execute);
            this.cmdProperties.Update += new System.EventHandler(this.cmdProperties_Update);
            // 
            // ciPropertiesToolbarButton
            // 
            this.ciPropertiesToolbarButton.Item = this.tbbProperties;
            // 
            // cmdRemove
            // 
            this.cmdRemove.Instances.Add(this.ciRemoveToolbarButton);
            this.cmdRemove.Manager = this.commandManager;
            this.cmdRemove.Execute += new System.EventHandler(this.cmdRemove_Execute);
            this.cmdRemove.Update += new System.EventHandler(this.cmdRemove_Update);
            // 
            // cmdSecurity
            // 
            this.cmdSecurity.Instances.Add(this.ciSecurityToolbarButton);
            this.cmdSecurity.Manager = this.commandManager;
            this.cmdSecurity.Execute += new System.EventHandler(this.cmdSecurity_Execute);
            this.cmdSecurity.Update += new System.EventHandler(this.cmdSecurity_Update);
            // 
            // ciSecurityToolbarButton
            // 
            this.ciSecurityToolbarButton.Item = this.tbbSecurity;
            // 
            // cmdBuild
            // 
            this.cmdBuild.Instances.Add(this.ciBuildToolbarButton);
            this.cmdBuild.Manager = this.commandManager;
            this.cmdBuild.Execute += new System.EventHandler(this.cmdBuild_Execute);
            this.cmdBuild.Update += new System.EventHandler(this.cmdBuild_Update);
            // 
            // columnMobile
            // 
            this.columnMobile.Editor = null;
            resources.ApplyResources(this.columnMobile, "columnMobile");
            this.columnMobile.Width = 40;
            // 
            // columnDateTime
            // 
            this.columnDateTime.Editor = null;
            resources.ApplyResources(this.columnDateTime, "columnDateTime");
            // 
            // columnTimeFrom
            // 
            this.columnTimeFrom.Editor = null;
            resources.ApplyResources(this.columnTimeFrom, "columnTimeFrom");
            // 
            // columnTimeTo
            // 
            this.columnTimeTo.Editor = null;
            resources.ApplyResources(this.columnTimeTo, "columnTimeTo");
            // 
            // columnState
            // 
            this.columnState.Editor = null;
            resources.ApplyResources(this.columnState, "columnState");
            // 
            // GeneratedReportTableControl
            // 
            this.Controls.Add(this.table);
            this.Controls.Add(this.globalToolbar);
            this.Name = "GeneratedReportTableControl";
            resources.ApplyResources(this, "$this");
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion // Component Designer generated code

		#region IClientItem<IReportClientDataProvider> Members

		private IReportClientDataProvider client;
		[Browsable( false )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IReportClientDataProvider Client
		{
			get { return this.client; }
			set { this.client = value; }
		}

		#endregion // IClientItem<IReportClientDataProvider> Members

		#region IClientItem Members

		IClient IClientItem.Client
		{
			get { return this.Client; }
			set 
            { 
                this.Client = (IReportClientDataProvider)value; 
            }
		}

		#endregion // IClientItem Members

		#region Properties

        private int selectedReport;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectedReport
        {
            get { return this.selectedReport; }
            set
            {
                if (this.tableModel.ContainsId(value))
                {
                    this.tableModel.Selections.SelectCell(
                        this.tableModel.IndexOf(value),
                        0
                        );

                    this.selectedReport = value;
                }
                else
                {
                    this.tableModel.Selections.Clear();
                }
            }
        }

        private IGeneratedReportEditorProvider editorProvider;
		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public IGeneratedReportEditorProvider EditorProvider
		{
			get { return this.editorProvider; }
			set { this.editorProvider = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public TableModel.Selection Selections
		{
			get { return this.tableModel.Selections; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public GeneratedReportXpRow[] SelectedRows
		{
			get { return this.tableModel.SelectedRows; }
		}

		#endregion // Properties

		#region Actions

		private void ActionNew()
		{
			if ( this.editorProvider != null )
			{
				this.editorProvider.New();
			}
			else
				throw new ApplicationException(
					"Editor provider must be specified"
					);
		}

		private void ActionProperties()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int ReportId = ( (GeneratedReportXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Properties( ReportId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionRemove()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int ReportId = ( (GeneratedReportXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.editorProvider != null )
				{
					this.editorProvider.Remove( ReportId );
				}
				else
					throw new ApplicationException(
						"Editor provider must be specified"
						);
			}
		}

		private void ActionSecurity()
		{
			if ( this.table.SelectedItems.Length == 1 )
			{
				int rightId = ( (GeneratedReportXpRow)this.table.SelectedItems[0] ).GetId();

				if ( this.Client == null )
				{
					throw new ApplicationException(
						"Security editor (client) must be specified"
						);
				}

				if ( this.editorProvider == null )
				{
					throw new ApplicationException(
						"Editor provider must be specified"
						);
				}

				this.Client.SecurityEditor.Edit(
					this.editorProvider.GetSecureObject( rightId )
					);
			}
		}

        private void ActionBuild()
        {
            if (this.table.SelectedItems.Length != 1)
            {
                return;
            }

            if (this.Client == null)
            {
                throw new ApplicationException(
                    "Security editor (client) must be specified"
                    );
            }

            if (this.editorProvider == null)
            {
                throw new ApplicationException(
                    "Editor provider must be specified"
                    );
            }

            int ReportId = ((GeneratedReportXpRow)this.table.SelectedItems[0]).GetId();

            this.Client.SecurityEditor.Edit(
                    this.editorProvider.GetSecureObject(ReportId)
                    );
        }

		#endregion // Actions

		#region Handle controls' events

		private void cmdNew_Execute( object sender, EventArgs e )
		{
			this.ActionNew();
		}

		private void cmdProperties_Execute( object sender, EventArgs e )
		{
			this.ActionProperties();
		}

		private void cmdRemove_Execute( object sender, EventArgs e )
		{
			this.ActionRemove();
		}

		private void cmdSecurity_Execute( object sender, EventArgs e )
		{
			this.ActionSecurity();
		}

        private void cmdBuild_Execute(object sender, EventArgs e)
        {
            this.ActionBuild();
        }

        private void cmdProperties_Update(object sender, EventArgs e)
		{
			this.cmdProperties.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdRemove_Update( object sender, EventArgs e )
		{
			this.cmdRemove.Enabled = this.table.SelectedItems.Length == 1;
		}

		private void cmdSecurity_Update( object sender, EventArgs e )
		{
			this.cmdSecurity.Enabled = this.table.SelectedItems.Length == 1;
		}

        private void cmdBuild_Update(object sender, EventArgs e)
        {
            this.cmdBuild.Enabled = this.table.SelectedItems.Length == 1;
        }

		#endregion // Handle controls events

		#region Events

		public event SelectionEventHandler SelectionChanged
		{
			add { this.tableModel.SelectionChanged += value; }
			remove { this.tableModel.SelectionChanged -= value; }
		}

		#endregion // Events

		#region IViewObjectControl<GeneratedReportXpRow> Members

		public void RowShow( ViewObjectEventArgs<GeneratedReportXpRow> e )
		{
			this.tableModel.RowShow( e );
		}

		public void RowHide( ViewObjectEventArgs<GeneratedReportXpRow> e )
		{
			this.tableModel.RowHide( e );
		}

		public void RowsClear( object sender, EventArgs e )
		{
			this.tableModel.RowsClear( sender, e );
		}

		#endregion

        private void table_SelectionChanged(object sender, SelectionEventArgs e)
        {
            if (this.table.SelectedItems.Length == 1)
            {
                this.selectedReport = ((GeneratedReportXpRow)this.table.SelectedItems[0]).GetId();
            }
            else
            {
                this.selectedReport = 0;
            }
        }
	}
}