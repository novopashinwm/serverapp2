﻿using FORIS.TSS.Helpers.Report.Data.Report;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.GeneratedReport
{
    partial class GeneratedReportSettingsContorl
    {
        #region Controls & Components
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private FORIS.TSS.UI.Controls.PropertyGrid.PropertyGridEx propertyGridEx;

        #endregion  // Controls & Components

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.propertyGridEx = new FORIS.TSS.UI.Controls.PropertyGrid.PropertyGridEx();
            this.dataDispatcher = new FORIS.TSS.Helpers.Report.Data.Report.ReportDataDispatcher(this.components);
            this.globalToolbar = new System.Windows.Forms.GlobalToolbar(this.components);
            this.s1 = new System.Windows.Forms.ToolBarButton();
            this.tbbBuild = new System.Windows.Forms.ToolBarButton();
            this.cmdBuild = new FORIS.TSS.Common.Commands.Command(this.components);
            this.cInstance_tbbBuild = new FORIS.TSS.Common.Commands.CommandInstance();
            this.cbxReportFormat = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // propertyGridEx
            // 
            // 
            // 
            // 
            this.propertyGridEx.DocCommentDescription.AutoEllipsis = true;
            this.propertyGridEx.DocCommentDescription.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertyGridEx.DocCommentDescription.Location = new System.Drawing.Point(3, 18);
            this.propertyGridEx.DocCommentDescription.Name = "";
            this.propertyGridEx.DocCommentDescription.Size = new System.Drawing.Size(217, 37);
            this.propertyGridEx.DocCommentDescription.TabIndex = 1;
            this.propertyGridEx.DocCommentImage = null;
            // 
            // 
            // 
            this.propertyGridEx.DocCommentTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertyGridEx.DocCommentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.propertyGridEx.DocCommentTitle.Location = new System.Drawing.Point(3, 3);
            this.propertyGridEx.DocCommentTitle.Name = "";
            this.propertyGridEx.DocCommentTitle.Size = new System.Drawing.Size(217, 15);
            this.propertyGridEx.DocCommentTitle.TabIndex = 0;
            this.propertyGridEx.DocCommentTitle.UseMnemonic = false;
            this.propertyGridEx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridEx.Location = new System.Drawing.Point(0, 26);
            this.propertyGridEx.Name = "propertyGridEx";
            this.propertyGridEx.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyGridEx.Size = new System.Drawing.Size(223, 200);
            this.propertyGridEx.TabIndex = 0;
            this.propertyGridEx.ToolbarVisible = false;
            // 
            // 
            // 
            this.propertyGridEx.ToolStrip.AccessibleName = "ToolBar";
            this.propertyGridEx.ToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.propertyGridEx.ToolStrip.AllowMerge = false;
            this.propertyGridEx.ToolStrip.AutoSize = false;
            this.propertyGridEx.ToolStrip.CanOverflow = false;
            this.propertyGridEx.ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.propertyGridEx.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.propertyGridEx.ToolStrip.Location = new System.Drawing.Point(0, 1);
            this.propertyGridEx.ToolStrip.Name = "";
            this.propertyGridEx.ToolStrip.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.propertyGridEx.ToolStrip.Size = new System.Drawing.Size(223, 25);
            this.propertyGridEx.ToolStrip.TabIndex = 1;
            this.propertyGridEx.ToolStrip.TabStop = true;
            this.propertyGridEx.ToolStrip.Text = "PropertyGridToolBar";
            this.propertyGridEx.ToolStrip.Visible = false;
            // 
            // globalToolbar
            // 
            this.globalToolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.globalToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.s1,
            this.tbbBuild});
            this.globalToolbar.Divider = false;
            this.globalToolbar.DropDownArrows = true;
            this.globalToolbar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.globalToolbar.Location = new System.Drawing.Point(0, 0);
            this.globalToolbar.Name = "globalToolbar";
            this.globalToolbar.ShowToolTips = true;
            this.globalToolbar.Size = new System.Drawing.Size(223, 26);
            this.globalToolbar.TabIndex = 4;
            // 
            // s1
            // 
            this.s1.Name = "s1";
            this.s1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // tbbBuild
            // 
            this.tbbBuild.ImageIndex = 1;
            this.tbbBuild.Name = "tbbBuild";
            this.tbbBuild.ToolTipText = "Построить отчет";
            // 
            // cmdBuild
            // 
            this.cmdBuild.Instances.Add(this.cInstance_tbbBuild);
            this.cmdBuild.Manager = this.commandManager;
            this.cmdBuild.Execute += new System.EventHandler(this.cmdBuild_Execute);
            this.cmdBuild.Update += new System.EventHandler(this.cmdBuild_Update);
            // 
            // cInstance_tbbBuild
            // 
            this.cInstance_tbbBuild.Item = this.tbbBuild;
            // 
            // cbxReportFormat
            // 
            this.cbxReportFormat.Items.AddRange(new object[] {
            "Формат PDF",
            "Формат XLS"});
            this.cbxReportFormat.Location = new System.Drawing.Point(31, 3);
            this.cbxReportFormat.Name = "cbxReportFormat";
            this.cbxReportFormat.Size = new System.Drawing.Size(121, 21);
            this.cbxReportFormat.TabIndex = 5;
            this.cbxReportFormat.Text = "Формат PDF";
            // 
            // ReportSettingsContorl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxReportFormat);
            this.Controls.Add(this.propertyGridEx);
            this.Controls.Add(this.globalToolbar);
            this.Name = "ReportSettingsContorl";
            this.Size = new System.Drawing.Size(223, 226);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GlobalToolbar globalToolbar;
        private System.Windows.Forms.ToolBarButton tbbBuild;
        private FORIS.TSS.Common.Commands.Command cmdBuild;
        private System.Windows.Forms.ToolBarButton s1;
        private FORIS.TSS.Common.Commands.CommandInstance cInstance_tbbBuild;
        private System.Windows.Forms.ComboBox cbxReportFormat;

    }
}
