﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.WorkplaceShadow.Controls;
using FORIS.TSS.WorkplaceShadow.Tsp;
using FORIS.TSS.WorkplaceSharnier;
using FORIS.TSS.WorkplaceSharnier.Report;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
    public partial class ReportSettingsContorl :
        TspUserControl,
        IDataItem<IReportData>
    {

        private IReportSettings reportSettings;
        private ReportDataDispatcher dataDispatcher;

        public ReportSettingsContorl()
        {
            InitializeComponent();
        }


        #region IClientItem Members

        public IReportData Data
        {
            get { return this.dataDispatcher.Data; }
            set
            {
                if (this.Data != null)
                {
                    this.OnBeforeSetData();

                    this.OnDestroyView();
                }

                this.dataDispatcher.Data = value;

                if (this.Data != null)
                {
                    this.OnBuildView();
                    this.OnUpdateView();

                    this.OnAfterSetData();
                }
            }
        }

        #endregion // IClientItem Members

        #region Data

        private ReportRow rowReport;

        protected virtual void OnBeforeSetData()
        {
            if (this.rowReport != null)
            {
                this.rowReport.AfterChange -= rowReport_AfterChange;
            }
        }
        protected virtual void OnAfterSetData()
        {
            if (this.rowReport != null)
            {
                this.rowReport.AfterChange += rowReport_AfterChange;
            }
        }

        private void rowReport_AfterChange(object sender, EventArgs e)
        {
            this.OnUpdateView();
        }

        #endregion // Data

        #region View

        protected virtual void OnDestroyView()
        {
            //this.Enabled = false;

            //#region Clear controls
            //#endregion  // Clear controls
        }

        protected virtual void OnBuildView()
        {
            // TODO: Почему вызывается обновление этого контрола при первой загрузке, когда отчёт ещё не выбран в списке отчётов???
            if (this.idReport <= 0)
                return;

            this.rowReport = this.Data.Report.FindRow(this.idReport);

            if(this.rowReport == null)
                Debug.Assert(this.rowReport != null, "Невозможно отобразить параметры отчёта! Отчёт не существует в контейнере! ID = " + idReport);

            // TODO: Получаем объект с данными о параметрах отчёта
            // временно получаем заранее созданные объекты для каждого отчёта
            // сл. итерация - считывание параметров из БД

            reportSettings = ReportSettings.GetReportSettingsObject(this.rowReport.ReportGuid, this.rowReport.Name);

            if(reportSettings != null)
            {
                reportSettings.Client = this.Client;
                reportSettings.FillSettings(this.propertyGridEx);
            }

        }

        private void OnUpdateView()
        {
        }

        #endregion // View

        #region Properties

        private DataItemControlMode mode = DataItemControlMode.View;
        [Browsable(false)]
        [DefaultValue(DataItemControlMode.View)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataItemControlMode Mode
        {
            get { return this.mode; }
            set
            {
                if (this.mode != value)
                {
                    this.OnDestroyView();

                    this.mode = value;

                    this.OnBuildView();
                    this.OnUpdateView();
                }
            }
        }

        private int idReport;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ReportId
        {
            get { return this.idReport; }
            set
            {
                if (this.idReport != value)
                {
                    if (this.idReport != 0)
                    {
                        this.OnDestroyView();
                    }

                    this.idReport = value;

                    if (this.idReport != 0)
                    {
                        this.OnBuildView();
                        this.OnUpdateView();
                    }
                }
            }
        }

        #endregion  // Properties

        private void cmdBuild_Execute(object sender, EventArgs e)
        {
            this.Client.Session.ReportMake(reportSettings.Guid, reportSettings.ReportParams, reportSettings.FormatType, "");
        }

        private void cmdBuild_Update(object sender, EventArgs e)
        {
            tbbBuild.Enabled = rowReport != null;
        }

    }
}
