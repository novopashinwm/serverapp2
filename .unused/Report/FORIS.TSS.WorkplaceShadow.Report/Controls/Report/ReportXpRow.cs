using FORIS.TSS.TransportDispatcher.XPTable.Models;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportXpRow : 
		Row,
		IViewObject
	{
		#region Cells

		private readonly Cell cellName;
		private readonly Cell cellDescription;

		#endregion  // Cells

		private readonly int ID;

		public ReportXpRow( 
			int id,
			string name,
			string description
			)
		{
			this.ID = id;

			#region Cells

            this.cellName			= new Cell();
            this.cellDescription	= new Cell();

			base.Cells.AddRange(
				new Cell[]
					{
						this.cellName,
						this.cellDescription
					}
				);

			#endregion  // Cells

			this.cellName.Text			= name;
			this.cellDescription.Text	= description;
		}

		public int GetId()
		{
			return this.ID;
		}
	}
}