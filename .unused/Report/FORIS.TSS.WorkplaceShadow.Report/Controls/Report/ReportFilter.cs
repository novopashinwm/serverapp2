using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportFilter :
		Filter<ReportRow, IReportData>
	{
		#region Data

		protected override void OnBeforeSetData()
		{
			this.Data.Report.Rows.Inserted -= this.ReportRows_Inserted;
			this.Data.Report.Rows.Removing -= this.ReportRows_Removing;
		}
		protected override void OnAfterSetData()
		{
			this.Data.Report.Rows.Inserted += this.ReportRows_Inserted;
			this.Data.Report.Rows.Removing += this.ReportRows_Removing;
		}

		#endregion  // Data

		#region Handle Data events

		void ReportRows_Inserted( object sender, CollectionChangeEventArgs<ReportRow> e )
		{
			this.dataObjects.Add( e.Item.ID, e.Item );
		}
		void ReportRows_Removing( object sender, CollectionChangeEventArgs<ReportRow> e )
		{
			this.dataObjects.Remove( e.Item.ID );
		}

		#endregion

		#region View

		protected override void OnDestroyView()
		{
			this.dataObjects.Clear();
		}
		protected override void OnBuildView()
		{
			foreach ( ReportRow row in this.Data.Report.Rows )
			{
                // TODO: �������� 
                // 1. ������ ��� WEB �� ������ ���� ����� � WIN
                // 2. � ���������� �� ������ ���� ������� ����� �������� WEB � WIN
                
                bool show = true;
                if(row.ReportGuid.ToString().ToUpper() ==
                    "9666515E-77FC-4EFE-875D-C928EE61FFA7")
                    show = false;

                if(show)
				    this.dataObjects.Add( row.ID, row );
			}
		}

        #endregion  // View
	}
}