﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic.Tsp;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.Interfaces.ReportService;
using FORIS.TSS.UI.Controls.PropertyGrid;
using FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors;
using FORIS.TSS.WorkplaceShadow.Tsp;
using FORIS.TSS.WorkplaceSharnier;
using Interfaces.Web;
using Microsoft.VisualBasic;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
    public interface IReportSettings : IClientItem<IClient>
    {
        void FillSettings(PropertyGridEx grid);

        Guid Guid { get; }

        PARAMS ReportParams { get; }

        ReportTypeEnum FormatType { get; }
    }

    public abstract class ReportSettings : IReportSettings
    {
        protected PropertyGridEx grid;
        private IClient client;
        private Guid guid;
        //private PARAMS reportParams;
        private ReportTypeEnum formatType;
        private string name;
        /// <summary>
        /// Список параметров отчёта для редактирования
        /// </summary>
        protected ReportParamsDictionary reportParams;

        public ReportSettings(Guid guid, string name)
        {
            this.guid = guid;
            this.name = name;
        }

        IClient IClientItem<IClient>.Client
        {
            get { return client; }
            set { client = value; }
        }

        public IClient Client
        {
            get { return client; }
            set { client = value; }
        }

        public static IReportSettings GetReportSettingsObject(Guid guid, string name)
        {
            IReportSettings settings = null;
            
            switch (guid.ToString().ToUpper())
            {
                case "E2D96FFD-097B-4E58-8707-28811695E5E7":
                    settings = new ReportTspMoveHistorySettings(guid, name);
                    break;

                case "9666515E-77FC-4EFE-875D-C928EE61FFA7":
                    Debug.Assert(false, "Can't create report 9666515E-77FC-4EFE-875D-C928EE61FFA7 !!!");
                    break;

                case "6E2DE7AD-2656-4812-BC1C-0EEA32613662":
                    settings = new ReportZoneOverSettings(guid, name);
                    break;
            }
        
            return settings;
        }

        public void FillSettings(PropertyGridEx grid)
        {
            this.grid = grid;

            this.grid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(grid_PropertyValueChanged);

            // очищаем таблицу свойств
            this.grid.ShowCustomProperties = true;
            this.grid.Item.Clear();

            TspClient client = null;
            if (this.Client is TspClient)
            {
                client = ((TspClient)this.Client);
                reportParams = ((ITspPersonalServer)this.Client.Session).GetReportParams(this.guid);
            }

            // заполняем таблицу свойств
            FillSettings();

            // обновляем таблицу свойств
            this.grid.Refresh();
        }

        protected abstract void grid_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e);

        public string Name
        {
            get { return name; }
        }

        public Guid Guid
        {
            get { return guid; }
        }

        public PARAMS ReportParams
        {
            get { return GetReportParams(); }
        }

        public ReportTypeEnum FormatType
        {
            get { return formatType; }
        }

        protected virtual void FillSettings()
        {
            if (reportParams != null && reportParams.Count > 0)
                foreach (KeyValuePair<string, IReportParams> kvp in reportParams)
                {
                    String rpName = kvp.Key; 
                    IReportParams rp = kvp.Value;

                    CustomProperty cp = new CustomProperty(rp.Name, rp.Value, rp.ReadOnly, rp.Category, rp.Description, rp.Visable, rp.Order);

                    this.grid.Item.Add(cp);
                }
        }
        protected abstract PARAMS GetReportParams();
    }

    public class ReportTspMoveHistorySettings : ReportSettings
    {
        MobileEditor mobileEditor;
        CustomProperty cpInterval;
        CustomProperty cpCount;
        CustomProperty cpFromDate;
        CustomProperty cpFromTime;
        CustomProperty cpToDate;
        CustomProperty cpToTime;
        CustomProperty cpMobile;
        CustomProperty cpShowAdress;

        public ReportTspMoveHistorySettings(Guid guid, string name) : base(guid, name) 
        {
        }

        protected override void FillSettings()
        {
            mobileEditor = new MobileEditor(Client);

            this.grid.Item.Add("Название", Name, true, "Отчёт", "Название отчёта", true);
            this.grid.Item.Add("GUID", Guid.ToString(), true, "Отчёт", "Уникальный идентификатор отчёта", true);

            cpInterval = new CustomProperty("Интервал, с", 10, false, "Параметры отчёта", "Минимальный интервал между позициями истории движения.", true, 0);
            cpCount    = new CustomProperty("Количество позиций", 1000, false, "Параметры отчёта", "Максимальное количество позиций истории движения, которое будет запрошено из базы данных.", true, 1);
            cpFromDate = new CustomProperty("Дата начала периода", new DateTime(DateAndTime.Today.Ticks), false, "Параметры отчёта", "Дата начала периода, за который формируется отчёт.", true, 2);
            cpFromTime = new CustomProperty("Время начала периода", DateTime.Now.AddHours(-2).ToString("HH:mm"), false, "Параметры отчёта", "Время начала периода, за который формируется отчёт.", true, 3);
            cpToDate   = new CustomProperty("Дата окончания периода", new DateTime(DateAndTime.Today.Ticks), false, "Параметры отчёта", "Дата окончания периода, за который формируется отчёт.", true, 4);
            cpToTime = new CustomProperty("Время окончания периода", DateTime.Now.ToString("HH:mm"), false, "Параметры отчёта", "Время окончания периода, за который формируется отчёт.", true, 5);
            cpMobile   = new CustomProperty("Объект слежения", mobileEditor.Mobiles.Array[0].Text, false, "Параметры отчёта", "Номер объекта слежения.", true, 6);
            cpMobile.ValueMember = "Value";
            cpMobile.DisplayMember = "Text";
            cpMobile.Datasource = mobileEditor.Mobiles.Array;

            // TODO : доделать редактирвоание булевых значений
            //IDictionary<int, string> showAdress = new Dictionary<int, string>(2);
            //showAdress.Add(0, "нет");
            //showAdress.Add(1, "да");
            IList<string> showAdress = new List<string>(2);
            showAdress.Add("нет");
            showAdress.Add("да");
            cpShowAdress = new CustomProperty("Искать адрес", "нет", false, "Параметры отчёта", "Флаг отображения адреса для каждой исторической позиции", true, 7);
            cpShowAdress.Datasource = showAdress;

            // Simple properties
            this.grid.Item.Add(cpInterval);
            this.grid.Item.Add(cpCount);
            this.grid.Item.Add(cpFromDate);
            this.grid.Item.Add(cpFromTime);
            this.grid.Item.Add(cpToDate);
            this.grid.Item.Add(cpToTime);
            this.grid.Item.Add(cpMobile);
            this.grid.Item.Add(cpShowAdress);
        }

        protected override void grid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Trace.Write("");            
        }

        protected override PARAMS GetReportParams()
        {
            // Параметры для построения
            PARAMS paramsReport = new PARAMS(true);

            paramsReport["Mo"] = (string)cpMobile.Value;
            paramsReport["ControllerID"] = mobileEditor.GetControllerID((string)cpMobile.Value);
            paramsReport["DateFrom"] = (DateTime)cpFromDate.Value;
            paramsReport["TimeFrom"] = (string)cpFromTime.Value;
            paramsReport["DateTo"] = (DateTime)cpToDate.Value;
            paramsReport["TimeTo"] = (string)cpToTime.Value;
            paramsReport["ShowAddress"] = (string)(cpShowAdress.Value) == "да"; 

            //paramsReport["ShowAddress"] = true;
            paramsReport["MapGuid"] = "86AA79B0-461F-41fc-8B87-B7879CA3781A";
            
            return paramsReport;
        }
    }

    public class ReportZoneOverSettings : ReportSettings
    {
        MobileEditor mobileEditor;
        CustomProperty cpInterval;
        CustomProperty cpCount;
        CustomProperty cpFromDate;
        CustomProperty cpFromTime;
        CustomProperty cpToDate;
        CustomProperty cpToTime;
        CustomProperty cpMobile;
        CustomProperty cpShowAdress;

        public ReportZoneOverSettings(Guid guid, string name)
            : base(guid, name) 
        {
        }

        protected override void FillSettings()
        {
            mobileEditor = new MobileEditor(Client);

            this.grid.Item.Add("Название", Name, true, "Отчёт", "Название отчёта", true);
            this.grid.Item.Add("GUID", Guid.ToString(), true, "Отчёт", "Уникальный идентификатор отчёта", true);

            cpInterval = new CustomProperty("Интервал, с", 10, false, "Параметры отчёта", "Минимальный интервал между позициями истории движения.", true, 0);
            cpCount    = new CustomProperty("Количество позиций", 1000, false, "Параметры отчёта", "Максимальное количество позиций истории движения, которое будет запрошено из базы данных.", true, 1);
            cpFromDate = new CustomProperty("Дата начала периода", new DateTime(DateAndTime.Today.Ticks), false, "Параметры отчёта", "Дата начала периода, за который формируется отчёт.", true, 2);
            cpFromTime = new CustomProperty("Время начала периода", DateTime.Now.AddHours(-2).ToString("HH:mm"), false, "Параметры отчёта", "Время начала периода, за который формируется отчёт.", true, 3);
            cpToDate   = new CustomProperty("Дата окончания периода", new DateTime(DateAndTime.Today.Ticks), false, "Параметры отчёта", "Дата окончания периода, за который формируется отчёт.", true, 4);
            cpToTime = new CustomProperty("Время окончания периода", DateTime.Now.ToString("HH:mm"), false, "Параметры отчёта", "Время окончания периода, за который формируется отчёт.", true, 5);
            cpMobile   = new CustomProperty("Объект слежения", mobileEditor.Mobiles.Array[0].Text, false, "Параметры отчёта", "Номер объекта слежения.", true, 6);
            cpMobile.ValueMember = "Value";
            cpMobile.DisplayMember = "Text";
            cpMobile.Datasource = mobileEditor.Mobiles.Array;

            // TODO : доделать редактирвоание булевых значений
            //IDictionary<int, string> showAdress = new Dictionary<int, string>(2);
            //showAdress.Add(0, "нет");
            //showAdress.Add(1, "да");
            IList<string> showAdress = new List<string>(2);
            showAdress.Add("нет");
            showAdress.Add("да");
            cpShowAdress = new CustomProperty("Искать адрес", "нет", false, "Параметры отчёта", "Флаг отображения адреса для каждой исторической позиции", true, 7);
            cpShowAdress.Datasource = showAdress;

            // Simple properties
            this.grid.Item.Add(cpInterval);
            this.grid.Item.Add(cpCount);
            this.grid.Item.Add(cpFromDate);
            this.grid.Item.Add(cpFromTime);
            this.grid.Item.Add(cpToDate);
            this.grid.Item.Add(cpToTime);
            this.grid.Item.Add(cpMobile);
            this.grid.Item.Add(cpShowAdress);
        }

        protected override void grid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Trace.Write("");            
        }

        protected override PARAMS GetReportParams()
        {
            // Параметры для построения
            PARAMS paramsReport = new PARAMS(true);

            paramsReport["Mo"] = (string)cpMobile.Value;
            paramsReport["ControllerID"] = mobileEditor.GetControllerID((string)cpMobile.Value);
            paramsReport["DateFrom"] = (DateTime)cpFromDate.Value;
            paramsReport["TimeFrom"] = (string)cpFromTime.Value;
            paramsReport["DateTo"] = (DateTime)cpToDate.Value;
            paramsReport["TimeTo"] = (string)cpToTime.Value;
            paramsReport["ShowAddress"] = (string)(cpShowAdress.Value) == "да"; 

            //paramsReport["ShowAddress"] = true;
            paramsReport["MapGuid"] = "86AA79B0-461F-41fc-8B87-B7879CA3781A";
            
            return paramsReport;
        }
    }
}
