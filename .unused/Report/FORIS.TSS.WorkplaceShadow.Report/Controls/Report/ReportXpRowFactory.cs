using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportXpRowFactory :
		ViewObjectFactory<ReportRow, ReportXpRow, IReportData>
	{
		public override ReportXpRow CreateViewObject( 
			int viewObjectId, 
			ReportRow reportRow 
			)
		{
			return
				new ReportXpRow(
					viewObjectId,
					reportRow.Name,
					reportRow.Description
					);
		}
	}
}