using System.ComponentModel;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportTableModel :
		ViewObjectTableModel<ReportXpRow>
	{
		#region Constructor & Dispose

		public ReportTableModel( IContainer container )
		{
			container.Add( this );
		}
		public ReportTableModel()
		{

		}

		#endregion  // Constructor & Dispose
	}
}