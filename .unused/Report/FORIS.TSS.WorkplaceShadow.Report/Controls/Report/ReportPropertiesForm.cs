using System;
using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.WorkplaceShadow.Controls;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportPropertiesForm: 
		TssUserForm,
		IDataItem<IReportData>
	{
		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components;
		private ReportDataDispatcher dataDispatcher;
		private TableLayoutPanel tableLayoutPanelMain;
		private Label lblName;
		private TextBox edtDescription;
		private Label lblDescription;
		private Label lblReportName;
		private TextBox edtReportName;
		private TextBox edtName;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public ReportPropertiesForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ReportPropertiesForm ) );
			this.dataDispatcher = new FORIS.TSS.Helpers.Report.Data.Report.ReportDataDispatcher( this.components );
			this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
			this.lblName = new System.Windows.Forms.Label();
			this.edtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.edtName = new System.Windows.Forms.TextBox();
			this.lblReportName = new System.Windows.Forms.Label();
			this.edtReportName = new System.Windows.Forms.TextBox();
			this.tableLayoutPanelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelFill
			// 
			resources.ApplyResources( this.panelFill, "panelFill" );
			// 
			// dataDispatcher
			// 
			this.dataDispatcher.UpdateView += new System.EventHandler( this.dataDispatcher_UpdateView );
			this.dataDispatcher.BuildView += new System.EventHandler( this.dataDispatcher_BuildView );
			this.dataDispatcher.AfterSetData += new System.EventHandler( this.dataDispatcher_AfterSetData );
			this.dataDispatcher.DestroyView += new System.EventHandler( this.dataDispatcher_DestroyView );
			this.dataDispatcher.BeforeSetData += new System.EventHandler( this.dataDispatcher_BeforeSetData );
			// 
			// tableLayoutPanelMain
			// 
			resources.ApplyResources( this.tableLayoutPanelMain, "tableLayoutPanelMain" );
			this.tableLayoutPanelMain.Controls.Add( this.lblName, 0, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.edtDescription, 1, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.lblDescription, 0, 1 );
			this.tableLayoutPanelMain.Controls.Add( this.edtName, 1, 0 );
			this.tableLayoutPanelMain.Controls.Add( this.lblReportName, 0, 2 );
			this.tableLayoutPanelMain.Controls.Add( this.edtReportName, 1, 2 );
			this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
			// 
			// lblName
			// 
			resources.ApplyResources( this.lblName, "lblName" );
			this.lblName.Name = "lblName";
			// 
			// edtDescription
			// 
			resources.ApplyResources( this.edtDescription, "edtDescription" );
			this.edtDescription.Name = "edtDescription";
			// 
			// lblDescription
			// 
			resources.ApplyResources( this.lblDescription, "lblDescription" );
			this.lblDescription.Name = "lblDescription";
			// 
			// edtName
			// 
			resources.ApplyResources( this.edtName, "edtName" );
			this.edtName.Name = "edtName";
			// 
			// lblReportName
			// 
			resources.ApplyResources( this.lblReportName, "lblReportName" );
			this.lblReportName.Name = "lblReportName";
			// 
			// edtReportName
			// 
			resources.ApplyResources( this.edtReportName, "edtReportName" );
			this.edtReportName.Name = "edtReportName";
			// 
			// ReportPropertiesForm
			// 
			resources.ApplyResources( this, "$this" );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add( this.tableLayoutPanelMain );
			this.Name = "ReportPropertiesForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.ReportPropertiesForm_FormClosing );
			this.Controls.SetChildIndex( this.panelFill, 0 );
			this.Controls.SetChildIndex( this.tableLayoutPanelMain, 0 );
			this.tableLayoutPanelMain.ResumeLayout( false );
			this.tableLayoutPanelMain.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		#region IDataItem<IReportData> Members

		public IReportData Data
		{
			get { return this.dataDispatcher.Data; }
			set
			{
				if (this.dataDispatcher.Data != null)
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.dataDispatcher.Data = value;

				if (this.dataDispatcher.Data != null)
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // IDataItem<IReportData> Members

		#region Properties

		private DataItemControlMode mode = DataItemControlMode.Undefined;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public DataItemControlMode Mode
		{
			get { return this.mode; }
			set
			{
				if( this.Data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.mode = value;

				if( this.Data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		private int idReport;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int IdReport
		{
			get { return this.idReport; }
			set
			{
				if( this.idReport != value )
				{
					if( this.Data != null )
					{
						this.OnBeforeSetData();

						this.OnDestroyView();
					}

					this.idReport = value;

					if( this.Data != null )
					{
						this.OnBuildView();
						this.OnUpdateView();

						this.OnAfterSetData();
					}
				}
			}
		}

		public string ReportName
		{
			get { return this.edtName.Text; }
		}

		public string Description
		{
			get { return this.edtDescription.Text; }
		}

		public string ReportReportName
		{
			get { return this.edtReportName.Text; }
		}

		#endregion // Properties

		#region Data

		private ReportRow rowReport;

		protected virtual void OnBeforeSetData()
		{
			if( this.rowReport != null )
			{
				this.rowReport.AfterChange -=  rowReport_AfterChange ;
			}
		}
		protected virtual void OnAfterSetData()
		{
			if( this.rowReport != null )
			{
				this.rowReport.AfterChange += rowReport_AfterChange;
			}
		}

		private void rowReport_AfterChange( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.rowReport = null;
		}

		protected virtual void OnBuildView()
		{
			this.rowReport = this.Data.Report.FindRow( this.idReport );
		}
		protected virtual void OnUpdateView()
		{
			switch( this.Mode )
			{
					#region Edit

				case DataItemControlMode.Edit:

					if( this.rowReport != null )
					{
						this.edtName.Text = this.rowReport.Name;
						this.edtDescription.Text = this.rowReport.Description;
						this.edtReportName.Text = this.rowReport.Name;
					}

					break;

					#endregion // Edit

					#region New

				case DataItemControlMode.New:

					this.edtName.Text = string.Empty;
					this.edtDescription.Text = string.Empty;
					this.edtReportName.Text = string.Empty;

					break;

					#endregion // New
			}
		}

		#endregion // View

		#region Handle dataDispatcher events

		private void dataDispatcher_BeforeSetData( object sender, EventArgs e )
		{
			this.OnBeforeSetData();
		}

		private void dataDispatcher_AfterSetData( object sender, EventArgs e )
		{
			this.OnAfterSetData();
		}

		private void dataDispatcher_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}

		private void dataDispatcher_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}

		private void dataDispatcher_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();
		}

		#endregion // Handle dataDispatcher events

		#region Handle controls' events

		private void ReportPropertiesForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( this.DialogResult == DialogResult.OK )
			{
				StringCollection Warnings = new StringCollection();

				if( this.edtName.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( "Report name required");
				}

				if( this.edtDescription.Text.Length == 0 )
				{
					e.Cancel = true;

					Warnings.Add( "Report description required" );
				}

				if( Warnings.Count > 0 )
				{
					MessageBox.Show(
						"Data error(s):" + "\r\n" +
						Warnings.ToString()
						);
				}
			}
		}

		#endregion // Handle controls' events
	}
}