﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
    public class ReportParamsFactory
    {
        // Параметры отчетов по GUID-ам 
        private readonly Dictionary<Guid, Object> GuidReportParams = new Dictionary<Guid, Object>();

        public ReportParamsFactory()
        {
            this.Params_Load();
        }

        private void Params_Load()
        {
            Trace.WriteLine("reports: ReportsLoad()");

            try
            {
                //Просмотр файлов происходит в текущей директории приложения.
                DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\Reports");
                //Просматриваются только .dll отчетов.
                FileInfo[] iAssembleFiles = di.GetFiles("*Report*.dll");

                Trace.WriteLine("reports: ReportsLoad()->iAssembleFiles.Length = " + iAssembleFiles.Length);

                //Просмотр последовательно всех файлов сборки.
                foreach (FileInfo fliProccessingFileInfo in iAssembleFiles)
                {
                    //Пропускаются файлы, заведомо являющиеся частью сборки "Report".
                    if (fliProccessingFileInfo.FullName.EndsWith("Report.dll") || fliProccessingFileInfo.FullName.EndsWith("Reports.dll"))
                    {
                        continue;
                    }

                    //Создается экземпляр рассматриваемой .dll.
                    Assembly assProcessingAssembly = Assembly.LoadFile(fliProccessingFileInfo.FullName);

                    Trace.WriteLine("reports: ReportsLoad()->Assembly.LoadFile(fliProccessingFileInfo.FullName) " + fliProccessingFileInfo.FullName);

                    //Сохраняется имя рассматриваемой .dll.
                    string strAssembleName = assProcessingAssembly.FullName;
                    //Массив для хранения списка типов в рассматриваемой .dll.
                    //Получение списка типов в рассматриваемой .dll.
                    Type[] typeList = assProcessingAssembly.GetTypes();

                    //Последовательный перебор всех типов в рассматриваемой .dll. - поиск класса отчета
                    foreach (Type type in typeList)
                    {
                        //Последовательный перебор всех интерфейсов в рассматриваемом типе.
                        if (type.BaseType != null && type.BaseType.Name == "ReportParameters") //typeof(TssReportBase).Name)
                        {
                            Trace.WriteLine("reports: before Activator.CreateInstance");

                            ObjectHandle h = Activator.CreateInstance(strAssembleName, type.ToString());
                            //ReportParameters reportParameters = h.Unwrap() as ReportParameters;
                            Object reportParameters = h.Unwrap();

                            Trace.WriteLine("reports: after Activator.CreateInstance");

                            this.GuidReportParams.Add(new Guid(), reportParameters);
                        }
                    }
                }
            }
            catch (ReflectionTypeLoadException e)
            {
                foreach (Exception exc in e.LoaderExceptions)
                    Trace.WriteLine(exc.Message);
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.WriteLine("В функции " + ex.Source + " произошла ошибка: " + ex.Message);
#endif
            }
        }

        public Object GetReportParams(Guid reportGuid)
        {
            if(this.GuidReportParams.ContainsKey(reportGuid))
                return this.GuidReportParams[reportGuid];
            return null;
        }
    }
}
