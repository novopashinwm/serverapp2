using System.ComponentModel;
using System.Windows.Forms;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.WorkplaceShadow.Controls;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report
{
	public class ReportEditorProvider:
		EditorProvider<IReportDataTreater, IReportData>,
		IReportEditorProvider
	{
		public ReportEditorProvider()
		{
			
		}
		public ReportEditorProvider( IContainer container )
			: this()
		{
			container.Add( this );
		}

		protected override void New()
		{
			MessageBox.Show( 
				"� ������!", 
				"������", 
				MessageBoxButtons.OK, 
				MessageBoxIcon.Stop );

			#region Creating new
			/**
			using ( ReportPropertiesForm propertiesForm = new ReportPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.New;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IReportDataTreater Treater =
							((IReportDataSupplier)this.Data).GetDataTreater();

						using( Treater )
						{
							Treater.ReportInsert(
								propertiesForm.ReportName,
								propertiesForm.Description,
								propertiesForm.ReportGuid,
								propertiesForm.ReportFileName
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not create new Report" +
						"Exception: " + Ex.Message
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
			/**/
			#endregion  // Creating new
		}

		protected override void Properties( int idReport )
		{
			MessageBox.Show(
				"� ������!",
				"������",
				MessageBoxButtons.OK,
				MessageBoxIcon.Stop );

			#region Modifying existing 
			/**
			using ( ReportPropertiesForm propertiesForm = new ReportPropertiesForm() )
			{
				propertiesForm.Mode = DataItemControlMode.Edit;
				propertiesForm.IdReport = idReport;

				this.dataDispatcher.Ambassadors.Add( propertiesForm );

				try
				{
					if( propertiesForm.ShowDialog() == DialogResult.OK )
					{
						IReportDataTreater Treater =
							( (IReportDataSupplier)this.Data ).GetDataTreater();

						using( Treater )
						{
							Treater.ReportUpdate(
								idReport,
								propertiesForm.ReportName,
								propertiesForm.Description,
								propertiesForm.ReportGuid,
								propertiesForm.ReportFileName
								);
						}
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not update Report" +
						"Exception: " + Ex.Message
						);
				}
				finally
				{
					this.dataDispatcher.Ambassadors.Remove( propertiesForm );
				}
			}
			/**/
			#endregion  // Modifying existing
		}

		protected override void Remove( int idReport )
		{
			MessageBox.Show(
				"� ������!",
				"������",
				MessageBoxButtons.OK,
				MessageBoxIcon.Stop );

			#region Deleting
			/**
			if (
				MessageBox.Show(
					"Are you sure to delete Report",
					"Warning",
					MessageBoxButtons.YesNo
					) == DialogResult.Yes
				)
			{
				try
				{
					IReportDataTreater Treater =
						( (IReportDataSupplier)this.Data ).GetDataTreater();

					using( Treater )
					{
						Treater.ReportDelete( idReport );
					}
				}
				catch( Exception Ex )
				{
					MessageBox.Show(
						"Can not delete Report" + "\r\n" +
						"Exception: " + Ex.Message
						);
				}
			}
			/**/
			#endregion  // Deleting
		}

		protected override ISecureObject GetSecureObject( int idReport )
		{
			return this.Data.Report.FindRow( idReport );
		}
	}
}