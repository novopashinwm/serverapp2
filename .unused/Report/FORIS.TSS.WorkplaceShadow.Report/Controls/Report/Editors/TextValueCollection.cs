﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors
{
    /// <summary>
    /// Коллекция пар Строка-Значение
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TextValueCollection<T>
    {
        TextValue <String, T>[] array = null;

        public TextValue<String, T>[] Array 
        {
            get { return array; }
            set { array = value;  } 
        }
    }
}
