﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors
{
    /// <summary>
    /// Интерфейс Строка-Значение
    /// </summary>
    /// <typeparam name="String"></typeparam>
    /// <typeparam name="T"></typeparam>
    public class TextValue<String, T>
    {
        string text = System.String.Empty;
        T value = default(T);

        public TextValue(string text, T value)
        {
            this.text = text;
            this.value = value;
        }

        public string Text 
        {
            get { return text; }
            set { text = value; } 
        }

        public T Value 
        {
            get { return this.value;  }
            set { this.value = value; } 
        }
    }
}
