﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.Helpers.RuleManager.Data.Rule;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.WorkplaceShadow.Tsp;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors
{
    public class MobileEditor : ReportSettingsEditorBase
    {
        public MobileEditor(IClient client)
            : base(client)
        {
        }

        public TextValueCollection<Int32> Mobiles = new TextValueCollection<Int32>();

        protected override void FillCollections()
        {
            TspClient client = null;
            if (this.Client is TspClient)
                client = ((TspClient)this.Client);

            Trace.Assert(client != null, "Клиент не может быть равным NULL!");

            int count = client.GetRuleClientData().Mobile.Rows.Count;
            this.Mobiles.Array = new TextValue<string, int>[count];

            int i = 0;
            foreach (MobileRow row in client.GetRuleClientData().Mobile.Rows)
            {
                Mobiles.Array[i++] = new TextValue<string, int>(row.Name.ToString(), row.ID);
            }
        }

        public int GetControllerID(string mobileName)
        {
            MobileRow rowMobile = null;

            foreach (TextValue<string, int> tv in Mobiles.Array)
            {
                if (tv.Text == mobileName)
                {
                    rowMobile = ((TspClient)this.Client).GetRuleClientData().Mobile.FindRow(tv.Value);
                }
            }

            return rowMobile != null ? rowMobile.Controller.Value : 0;
        }
    }
}
