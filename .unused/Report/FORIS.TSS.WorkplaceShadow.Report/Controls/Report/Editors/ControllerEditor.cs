﻿using System;
using System.Diagnostics;
using FORIS.TSS.Helpers.TspTerminal.Data.Controller;
using FORIS.TSS.WorkplaceShadow.Tsp;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors
{
    public class ControllerEditor : ReportSettingsEditorBase
    {
        public ControllerEditor(IClient client)
            : base(client)
        {
        }

        public TextValueCollection<Int32> Controllers = new TextValueCollection<Int32>();

        protected override void FillCollections()
        {
            TspClient client = null;
            if (this.Client is TspClient)
                client = ((TspClient)this.Client);

            Trace.Assert(client != null, "Клиент не может быть равным NULL!");

            int count = client.GetControllerClientData().Controller.Rows.Count;
            this.Controllers.Array = new TextValue<string, int>[count];

            int i=0;
            foreach (ControllerRow row in client.GetControllerClientData().Controller.Rows)
            {
                Controllers.Array[i++] = new TextValue<string, int>(row.Number.ToString(), row.ID);
            }
        }
    }
}