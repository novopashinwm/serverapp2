﻿using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Report.Controls.Report.Editors
{
    /// <summary>
    /// Базовый класс, описывающий поведение редакторов настроек отчётов
    /// </summary>
    public abstract class ReportSettingsEditorBase : IReportSettingsEditor
    {
        private IClient client;

        IClient IClientItem<IClient>.Client
        {
            get { return client; }
            set { client = value; }
        }

        public IClient Client
        {
            get { return client; }
            set { client = value; }
        }

        /// <summary>
        /// Базовый класс, описывающий поведение редакторов настроек отчётов
        /// </summary>
        /// <param name="client"></param>
        public ReportSettingsEditorBase(IClient client)
        {
            this.client = client;

            this.FillCollections();
        }

        protected ReportSettingsEditorBase()
        {
        }

        protected abstract void FillCollections();
    }
}
