using System;
using FORIS.TSS.Helpers.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	partial class ReportDataTreater
	{
		public void ReportInsert( 
			string name,
			string description,
			Guid reportGuid,
			string reportFileName,
            string settings
			)
		{
			ReportRow reportRow;

			lock ( this.Data )
			{
				try
				{
					reportRow = this.Data.Report.NewRow();

					reportRow.Name = name;
					reportRow.Description = description;
					reportRow.ReportGuid = reportGuid;
					reportRow.ReportFileName = reportFileName;
                    reportRow.Settings = settings;

					this.Data.Report.Rows.Add( reportRow );

					this.AcceptChanges();
				}
				catch ( Exception ex )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ReportUpdate( 
			int reportId, 
			string name,
			string description,
			Guid reportGuid,
            string reportFileName,
            string settings
            )
		{
			lock( this.Data )
			{
				try
				{
					ReportRow reportRow = this.Data.Report.FindRow( reportId );

					reportRow.BeginEdit();

					reportRow.Name = name;
					reportRow.Description = description;
					reportRow.ReportGuid = reportGuid;
					reportRow.ReportFileName = reportFileName;
                    reportRow.Settings = settings;

					reportRow.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ReportDelete( int reportId )
		{
			lock( this.Data )
			{
				ReportRow reportRow = this.Data.Report.FindRow( reportId );

				#region Preconditions

				if( reportRow == null )
				{
					throw new ApplicationException( 
						"Report row does not exist"
						);
				}

				#endregion // Preconditions

				#region Security

				/* ����� ��������� ������� ���������� 
				 * ������� ��� �������� �� ���� ����������
				 */

				lock( this.Data.SecurityData )
				{
					try
					{
						reportRow.ClearAcl();

						this.Data.SecurityData.AcceptChanges( new Guid(), this.databaseDataSupplier );
					}
					catch( Exception )
					{
						this.RejectChanges();

						throw;
					}
				}

				#endregion // Security

				try
				{
					reportRow.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}