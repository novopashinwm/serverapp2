using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Data;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	public class ReportSecurityDatabaseDataSupplier:
		DatabaseDataSupplier<IReportServerDataProvider>,
		IReportSecurityDataSupplier
	{
		public ReportSecurityDatabaseDataSupplier( 
			IReportServerDataProvider databaseServerProvider 
			)
			: base( databaseServerProvider )
		{

		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerReportSecurity",
					new string[]
						{
							"R_REPORT_OPERATORGROUP",
							"R_REPORT_OPERATOR"
						}
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		IOperatorDataSupplier ISecurityDataSupplier<IOperatorDataSupplier>.PrincipalDataSupplier
		{
			get { return this.DatabaseProvider.PrincipalData; }
		}

		public IReportSecurityDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}
	}
}