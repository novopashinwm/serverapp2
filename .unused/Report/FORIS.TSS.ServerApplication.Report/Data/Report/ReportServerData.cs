﻿using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Report.Data.Report;
using System.Runtime.Remoting.Lifetime;
using System;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	/// <summary>
	/// Поставщик данных для модуля "Отчеты"
	/// </summary>
	/// <remarks>
	/// Всегда можно изменить способ получения,
	/// при необходимости - кэширования данных,
	/// выполнения операций и т.д. и т.п.
	/// 
	/// Именно этот самый объект осуществляет
	/// соединение данных непосредственно с БД
	/// </remarks>
	public class ReportServerData :
		ReportData
	{
		#region Constructor & Dispose

		/// <summary>
		/// Конструктор загружает данные и формирует обертку
		/// </summary>
		public ReportServerData( IReportServerDataProvider serverDataProvider )
		{
			this.serverDataProvider = serverDataProvider;

			this.DataSupplier = new ReportDatabaseDataSupplier( this.serverDataProvider );
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				RemotingServices.Disconnect( this );

				this.Destroy();
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		/// <summary>
		/// Передается в объекты DataTreater
		/// для обновления данных в БД
		/// </summary>
		private readonly IReportServerDataProvider serverDataProvider;

		/// <summary>
		/// Создает новый объект DataTreater
		/// </summary>
		/// <param name="sessionInfo"></param>
		/// <returns>объект ReportDataTreater</returns>
		public IReportDataTreater GetTreater( ISessionInfo sessionInfo )
		{
			return
				new ReportDataTreater(
					this.serverDataProvider.Database,
					this,
					sessionInfo
					);
		}

		[Obsolete( "", false )]
		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			return lease;
		}
	}
}