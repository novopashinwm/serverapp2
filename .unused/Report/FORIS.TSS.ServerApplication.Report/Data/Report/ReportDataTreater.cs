using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Report.Data.Report;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	public partial class ReportDataTreater:
		DataTreater<IReportData>,
		IReportDataTreater
	{
		public ReportDataTreater(
			IDatabaseDataSupplier databaseDataSupplier,
			IReportData data,
			ISessionInfo sessionInfo
			)
			: base( databaseDataSupplier, data, sessionInfo )
		{
		}
	}
}