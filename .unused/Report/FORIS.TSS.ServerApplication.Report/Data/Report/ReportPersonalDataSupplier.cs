using System.Collections.Generic;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.ServerApplication.Data;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	public class ReportPersonalDataSupplier:
		PersonalDataSupplier<ReportServerData, IReportDataTreater>,
		IReportDataSupplier
	{
		public ReportPersonalDataSupplier(
			ReportServerData serverData,
			IPersonalServer personalServer
			)
			: base( serverData, personalServer )
		{
		}

		public IReportSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.DataSupplier.SecurityDataSupplier; }
		}

		public override IReportDataTreater GetDataTreater()
		{
			return
				this.DataSupplier.GetTreater( this.PersonalServer.SessionInfo );
		}

		protected override DataInfo GetDataInfo()
		{
			DataInfo dataInfo = base.GetDataInfo();

            List<DataRow> victims = new List<DataRow>();

            foreach (DataRow dataRow in dataInfo.DataSet.Tables["REPORT"].Rows)
            {
                ReportRow reportRow = this.DataSupplier.Report.FindRow((int)dataRow["REPORT_ID"]);

                if (!reportRow.TestRight((GeneralRights)ReportRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataInfo.DataSet.Tables["REPORT"].Rows.Remove(dataRow);
            }

			return dataInfo;
		}

		protected override void dataSupplier_DataChanged( DataChangedEventArgs e )
		{
			DataChangedEventArgs dataChangedEventArgs = e;

            List<DataRow> victims = new List<DataRow>();

            #region Added

            foreach (DataRow dataRow in dataChangedEventArgs.Added.Tables["REPORT"].Rows)
            {
                ReportRow reportRow = this.DataSupplier.Report.FindRow((int)dataRow["REPORT_ID"]);

                if (!reportRow.TestRight((GeneralRights)ReportRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Added.Tables["REPORT"].Rows.Remove(dataRow);
            }

            #endregion  // Added

            #region Deleted

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Deleted.Tables["REPORT"].Rows)
            {
                ReportRow reportRow = this.DataSupplier.Report.FindRow((int)dataRow["REPORT_ID"]);

                if (!reportRow.TestRight((GeneralRights)ReportRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Deleted.Tables["REPORT"].Rows.Remove(dataRow);
            }

            #endregion  // Deleted

            #region Modified

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Modified.Tables["REPORT"].Rows)
            {
                ReportRow reportRow = this.DataSupplier.Report.FindRow((int)dataRow["REPORT_ID"]);

                if (!reportRow.TestRight((GeneralRights)ReportRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Modified.Tables["REPORT"].Rows.Remove(dataRow);
            }

            #endregion  // Modified

            #region Modified

            victims.Clear();

            foreach (DataRow dataRow in dataChangedEventArgs.Modified.Tables["REPORT"].Rows)
            {
                ReportRow reportRow = this.DataSupplier.Report.FindRow((int)dataRow["REPORT_ID"]);

                if (!reportRow.TestRight((GeneralRights)ReportRights.Read))
                {
                    victims.Add(dataRow);
                }
            }

            foreach (DataRow dataRow in victims)
            {
                dataChangedEventArgs.Modified.Tables["REPORT"].Rows.Remove(dataRow);
            }

            #endregion  // Modified

			base.dataSupplier_DataChanged( dataChangedEventArgs );
		}
	}
}