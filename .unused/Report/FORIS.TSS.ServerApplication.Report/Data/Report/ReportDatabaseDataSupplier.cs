using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.ServerApplication.Data;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.Report
{
	public class ReportDatabaseDataSupplier:
		DatabaseDataSupplier<IReportServerDataProvider>,
		IReportDataSupplier
	{
		public ReportDatabaseDataSupplier( IReportServerDataProvider databaseServerProvider )
			: base( databaseServerProvider )
		{
			this.reportSecurityDatabaseDataSupplier =
				new ReportSecurityDatabaseDataSupplier( databaseServerProvider );
		}

		private readonly ReportSecurityDatabaseDataSupplier reportSecurityDatabaseDataSupplier;

		public IReportSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.reportSecurityDatabaseDataSupplier; }
		}

		protected override DataInfo GetData()
		{
			DataSet data =
				this.DatabaseProvider.Database.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetContainerReport",
					new string[] { "REPORT" }
					);

			return new DataInfo( data, Guid.NewGuid() );
		}

		public IReportDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}