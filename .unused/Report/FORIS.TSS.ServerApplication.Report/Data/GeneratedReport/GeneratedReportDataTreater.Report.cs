using System;
using FORIS.TSS.Helpers.Report.Data.GeneratedReport;
using FORIS.TSS.Helpers.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.GeneratedReport
{
    partial class GeneratedReportDataTreater
	{
		public void ReportInsert( 
			string name,
			string description,
			Guid reportGuid,
			string reportFileName,
            string settings
			)
		{
            GeneratedReportRow reportRow;

			lock ( this.Data )
			{
				try
				{
                    reportRow = this.Data.GeneratedReport.NewRow();

					reportRow.Name = name;
					reportRow.Description = description;
					reportRow.ReportGuid = reportGuid;
					reportRow.ReportFileName = reportFileName;
                    reportRow.Settings = settings;

                    this.Data.GeneratedReport.Rows.Add(reportRow);

					this.AcceptChanges();
				}
				catch ( Exception ex )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ReportUpdate( 
			int reportId, 
			string name,
			string description,
			Guid reportGuid,
            string reportFileName,
            string settings
            )
		{
			lock( this.Data )
			{
				try
				{
                    GeneratedReportRow reportRow = this.Data.GeneratedReport.FindRow(reportId);

					reportRow.BeginEdit();

					reportRow.Name = name;
					reportRow.Description = description;
					reportRow.ReportGuid = reportGuid;
					reportRow.ReportFileName = reportFileName;
                    reportRow.Settings = settings;

					reportRow.EndEdit();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}

		public void ReportDelete( int reportId )
		{
			lock( this.Data )
			{
                GeneratedReportRow reportRow = this.Data.GeneratedReport.FindRow(reportId);

				#region Preconditions

				if( reportRow == null )
				{
					throw new ApplicationException( 
						"Report row does not exist"
						);
				}

				#endregion // Preconditions

				#region Security

				/* ����� ��������� ������� ���������� 
				 * ������� ��� �������� �� ���� ����������
				 */

				lock( this.Data.SecurityData )
				{
					try
					{
						reportRow.ClearAcl();

						this.Data.SecurityData.AcceptChanges( new Guid(), this.databaseDataSupplier );
					}
					catch( Exception )
					{
						this.RejectChanges();

						throw;
					}
				}

				#endregion // Security

				try
				{
					reportRow.Delete();

					this.AcceptChanges();
				}
				catch( Exception )
				{
					this.RejectChanges();

					throw;
				}
			}
		}
	}
}