using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.ServerApplication.Data;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.GeneratedReport
{
	public class GeneratedReportDatabaseDataSupplier:
		DatabaseDataSupplier<IGeneratedReportServerDataProvider>,
		IGeneratedReportDataSupplier
	{
        public GeneratedReportDatabaseDataSupplier(IGeneratedReportServerDataProvider databaseServerProvider)
			: base( databaseServerProvider )
		{
			this.reportSecurityDatabaseDataSupplier =
                new GeneratedReportSecurityDatabaseDataSupplier(databaseServerProvider);
		}

        private readonly GeneratedReportSecurityDatabaseDataSupplier reportSecurityDatabaseDataSupplier;

        public IGeneratedReportSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.reportSecurityDatabaseDataSupplier; }
		}

		protected override DataInfo GetData()
		{
            DataSet dataSet = this.DatabaseProvider.GetGeneratedReportData();

            return new DataInfo(dataSet, Guid.NewGuid());
		}

        public IGeneratedReportDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}
	}
}