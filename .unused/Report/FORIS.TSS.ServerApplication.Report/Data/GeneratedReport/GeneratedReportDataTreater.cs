using System;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Report.Data.GeneratedReport;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.Interfaces.ReportService;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report.Data.GeneratedReport
{
	public partial class GeneratedReportDataTreater:
		DataTreater<IGeneratedReportData>,
		IGeneratedReportDataTreater
	{
        public GeneratedReportDataTreater(
            IGeneratedReportData data,
			ISessionInfo sessionInfo
			)
			: base( null, data, sessionInfo )
		{

		}

        public void GeneratedReportInsert(
            CreatedReportEventArgs args
            )
        {
            lock (this.Data)
            {
                try
                {
                    GeneratedReportRow row = this.Data.GeneratedReport.NewRow();

                    row.Name = "todo:name";
                    row.ReportGuid = args.ReportGuid;
                    row.ReportFileName = "todo:fileName";
                    row.Settings = "todo:settings";

                    this.Data.GeneratedReport.Rows.Add(row);

                    this.AcceptChanges();

                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void OwnerMobileUpdate(
            int ownerMobleId,
            int ownerId,
            int mobileId
            )
        {
            lock (this.Data)
            {
                try
                {
                    //OwnerMobileRow ownerMobileRow = this.Data.OwnerMobile.FindRow(ownerMobleId);

                    //ownerMobileRow.BeginEdit();

                    //ownerMobileRow.Owner = ownerId;
                    //ownerMobileRow.Mobile = mobileId;

                    //ownerMobileRow.EndEdit();

                    //this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }

        public void OwnerMobileDelete(int ownerMobleId)
        {
            lock (this.Data)
            {
                try
                {
                    //OwnerMobileRow ownerMobileRow = this.Data.OwnerMobile.FindRow(ownerMobleId);

                    //ownerMobileRow.Delete();

                    //this.AcceptChanges();
                }
                catch (Exception)
                {
                    this.RejectChanges();

                    throw;
                }
            }
        }
	}
}