﻿using FORIS.TSS.ServerApplication.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report
{
	public interface IReportServerDataProvider :
		IDatabaseProvider
	{
		OperatorServerData PrincipalData { get; }

		ReportServerData ReportData { get; }
	}
}