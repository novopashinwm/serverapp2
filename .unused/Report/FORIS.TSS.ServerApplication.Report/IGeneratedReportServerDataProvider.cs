﻿using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.ServerApplication.Operator.Data.Operator;
using FORIS.TSS.ServerApplication.Report.Data.GeneratedReport;
using FORIS.TSS.ServerApplication.Report.Data.Report;

namespace FORIS.TSS.ServerApplication.Report
{
	public interface IGeneratedReportServerDataProvider :
		IDatabaseProvider
	{
		OperatorServerData PrincipalData { get; }

        GeneratedReportServerData ReportData { get; }

        DataSet GetGeneratedReportData();
	}
}