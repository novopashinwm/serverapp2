﻿using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Report.Data.GeneratedReport.Security;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport
{
	public class GeneratedReportData :
		SecureData<IGeneratedReportDataTreater>,
		IGeneratedReportData,
		IGeneratedReportDataSupplier
	{
		#region Controls & Components

		private IContainer components;
        private GeneratedReportDataDispatcher tablesDataDispatcher;

		private GeneratedReportTable report;

		private GeneratedReportDataAmbassador daReport;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public GeneratedReportData( IContainer container )
			: this()
		{
			container.Add( this );
		}
        public GeneratedReportData()
		{
            this.securityData = new GeneratedReportSecurityData(this);

			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				this.securityData.Dispose();

				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
            this.tablesDataDispatcher = new GeneratedReportDataDispatcher(this.components);

            this.report = new GeneratedReportTable(this.components);

            this.daReport = new GeneratedReportDataAmbassador();

			this.daReport.Item = this.report;

			this.tablesDataDispatcher.Ambassadors.Add( this.daReport );
		}

		#endregion // Component Designer generated code

		#region Properties

        public new IGeneratedReportDataSupplier DataSupplier
		{
            get { return (IGeneratedReportDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.securityData.DataSupplier = null;

			this.tablesDataDispatcher.Data = null;
		}

		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;

			this.securityData.DataSupplier = this.DataSupplier.SecurityDataSupplier;
		}

		#endregion // Build & Destroy

		#region Security

        private readonly GeneratedReportSecurityData securityData;
		[Browsable( false )]
        public IGeneratedReportSecurityData SecurityData
		{
			get { return this.securityData; }
		}

        public IGeneratedReportSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.securityData; }
		}

		#endregion // Security

	    IGeneratedReportSecurityData ISecureData<IGeneratedReportSecurityData>.SecurityData
	    {
	        get { throw new System.NotImplementedException(); }
	    }

        [Browsable(false)]
        public GeneratedReportTable GeneratedReport
	    {
            get { return this.report; }
        }

	    IGeneratedReportSecurityDataSupplier ISecureDataSupplier<IGeneratedReportSecurityDataSupplier, IOperatorDataSupplier>.SecurityDataSupplier
	    {
	        get { throw new System.NotImplementedException(); }
	    }
	}
}