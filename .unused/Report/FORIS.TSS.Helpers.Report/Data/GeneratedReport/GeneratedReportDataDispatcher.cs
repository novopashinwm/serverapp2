﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport
{
	public class GeneratedReportDataDispatcher :
        DataDispatcher<IGeneratedReportData>
	{
		public GeneratedReportDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
        public GeneratedReportDataDispatcher()
		{

		}

        private readonly GeneratedReportDataAmbassadorCollection ambassadors =
            new GeneratedReportDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public new GeneratedReportDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

        protected override AmbassadorCollection<IDataItem<IGeneratedReportData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

    public class GeneratedReportDataAmbassadorCollection :
        DataAmbassadorCollection<IGeneratedReportData>
	{
        public new GeneratedReportDataAmbassador this[int index]
		{
            get { return (GeneratedReportDataAmbassador)base[index]; }
		}
	}

    public class GeneratedReportDataAmbassador :
        DataAmbassador<IGeneratedReportData>
	{

	}
}