﻿using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Report.Data.GeneratedReport.Security;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport
{
	public interface IGeneratedReportData :
        IData<IGeneratedReportDataTreater>,
        ISecureData<IGeneratedReportSecurityData>
	{
        GeneratedReportTable GeneratedReport { get; }
	}
}