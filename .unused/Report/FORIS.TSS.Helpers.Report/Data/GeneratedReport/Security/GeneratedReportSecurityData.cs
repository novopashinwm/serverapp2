﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data.Security;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport.Security
{
	public class GeneratedReportSecurityData :
		SecurityData<GeneratedReportData, IGeneratedReportSecurityData>,
		IGeneratedReportSecurityData,
		IGeneratedReportSecurityDataSupplier
	{
		#region Controls & Components

		private IContainer components;
        private GeneratedReportSecurityDataDispatcher tablesDataDispatcher;
		private OperatorData principalData;

		private ReportOperatorGroupTable reportOperatorGroup;
		private ReportOperatorTable reportOperator;

        private ReportSecurityDataAmbassador daReportOperatorGroup;
        private ReportSecurityDataAmbassador daReportOperator;

		#endregion // Controls & Components

		#region Constructor & Dispose

		private new readonly IGeneratedReportData protectedData;

        public GeneratedReportSecurityData(GeneratedReportData protectedData) :
			base( protectedData )
		{
			this.protectedData = protectedData;

			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
            this.tablesDataDispatcher = new GeneratedReportSecurityDataDispatcher(this.components);
			this.principalData = new OperatorData( this.components );

			this.reportOperatorGroup = new ReportOperatorGroupTable( this.components );
			this.reportOperator = new ReportOperatorTable( this.components );

            this.daReportOperatorGroup = new ReportSecurityDataAmbassador();
            this.daReportOperator = new ReportSecurityDataAmbassador();

            //this.daReportOperatorGroup.Item = this.reportOperatorGroup;
            //this.daReportOperator.Item = this.reportOperator;

            //this.tablesDataDispatcher.Ambassadors.Add( this.daReportOperatorGroup );
            //this.tablesDataDispatcher.Ambassadors.Add( this.daReportOperator );
		}

		#endregion // Component Designer generated code

		#region Properties

		[Browsable( true )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public new IGeneratedReportSecurityDataSupplier DataSupplier
		{
            get { return (IGeneratedReportSecurityDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.tablesDataDispatcher.Data = null;

			this.principalData.DataSupplier = null;
		}

		protected override void Build()
		{
			this.principalData.DataSupplier = this.DataSupplier.PrincipalDataSupplier;

			this.tablesDataDispatcher.Data = this;
		}


		#endregion // Build & Destroy

		#region Security

		protected override ISecurityTable GetSecurityTable( SecureObjectInfo objectInfo, TssPrincipalInfo principalInfo )
		{
			switch ( objectInfo.TableName )
			{
				case "REPORT":
					switch ( principalInfo.TableName )
					{
						case "OPERATORGROUP":
							return this.reportOperatorGroup;

						case "OPERATOR":
							return this.reportOperator;
					}
					break;
			}

			throw new ApplicationException();
		}

        public IGeneratedReportData ProtectedData
		{
			get { return this.protectedData; }
		}

		public IOperatorData PrincipalData
		{
			get { return this.principalData; }
		}

		IOperatorDataSupplier ISecurityDataSupplier<IOperatorDataSupplier>.PrincipalDataSupplier
		{
			get { return this.principalData; }
		}


		#endregion // Security

		#region Tables properties (IReportSecurityData Members)

		[Browsable( false )]
		public ReportOperatorGroupTable ReportOperatorGroup
		{
			get { return this.reportOperatorGroup; }
		}

		[Browsable( false )]
		public ReportOperatorTable ReportOperator
		{
			get { return this.reportOperator; }
		}

		#endregion // Tables properties (IReportSecurityData Members)

        public IGeneratedReportSecurityDataTreater GetDataTreater()
		{
			throw new NotImplementedException();
		}
	}
}