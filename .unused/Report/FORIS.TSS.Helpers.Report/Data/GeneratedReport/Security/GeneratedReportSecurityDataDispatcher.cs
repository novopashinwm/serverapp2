﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Report.Data.Report.Security;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport.Security
{
	public class GeneratedReportSecurityDataDispatcher :
		DataDispatcher<IGeneratedReportSecurityData>
	{
		public GeneratedReportSecurityDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
        public GeneratedReportSecurityDataDispatcher()
		{

		}

        private readonly GeneratedReportSecurityDataAmbassadorCollection ambassadors =
            new GeneratedReportSecurityDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public new GeneratedReportSecurityDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

        protected override AmbassadorCollection<IDataItem<IGeneratedReportSecurityData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

    public class GeneratedReportSecurityDataAmbassadorCollection :
        DataAmbassadorCollection<IGeneratedReportSecurityData>
	{
        public new GeneratedReportSecurityDataAmbassador this[int index]
		{
            get { return (GeneratedReportSecurityDataAmbassador)base[index]; }
		}
	}

    public class GeneratedReportSecurityDataAmbassador :
        DataAmbassador<IGeneratedReportSecurityData>
	{

	}
}