﻿using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Report.Data.Report.Security;

namespace FORIS.TSS.Helpers.Report.Data.GeneratedReport.Security
{

    public interface IGeneratedReportSecurityData :
        ISecurityData<IGeneratedReportSecurityData, IGeneratedReportData, IOperatorData>
	{
		ReportOperatorGroupTable	ReportOperatorGroup { get; }
		ReportOperatorTable			ReportOperator { get; }
	}
}