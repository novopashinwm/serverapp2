﻿using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.Helpers.Report.Data.Report
{
	public interface IReportData :
		IData<IReportDataTreater>,
		ISecureData<IReportSecurityData>
	{
		ReportTable Report { get; }
	}
}