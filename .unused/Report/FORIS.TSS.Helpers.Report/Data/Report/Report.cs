﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Helpers.Report.Data.Report;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using FORIS.TSS.Helpers.Security;
using FORIS.TSS.BusinessLogic.Security;

namespace FORIS.TSS.Helpers.Report.Data.Report
{
	public class ReportTable :
		SecureTable<ReportRow, ReportTable, IReportData, IReportSecurityData, IOperatorData>
	{
		#region Properties

		public override string Name
		{
			get { return "REPORT"; }
		}

		#endregion  // Properties

		#region Constructor & Dispose

		public ReportTable()
		{

		}
		public ReportTable( IContainer container )
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		#region Fields' indexes

		private int idIndex = -1;
		internal int IdIndex				{ get { return this.idIndex; } }

		private int nameIndex = -1;
		internal int NameIndex				{ get { return this.nameIndex; } }

		private int descriptionIndex = -1;
		internal int DescriptionIndex		{ get { return this.descriptionIndex; } }

		private int reportGuidIndex = -1;
		internal int ReportGuidIndex		{ get { return this.reportGuidIndex; } }

		private int reportFileNameIndex = -1;
		internal int ReportFileNameIndex	{ get { return this.reportFileNameIndex; } }

        private int settingsIndex = -1;
        internal int SettingsIndex { get { return this.settingsIndex; } }

		#endregion // Fields' indexes

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex				= -1;
			this.nameIndex				= -1;
			this.descriptionIndex		= -1;
			this.reportGuidIndex		= -1;
			this.reportFileNameIndex	= -1;
            this.settingsIndex = -1;
        }
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex				= this.DataTable.Columns.IndexOf( "REPORT_ID" );
				this.nameIndex				= this.DataTable.Columns.IndexOf( "NAME" );
				this.descriptionIndex		= this.DataTable.Columns.IndexOf( "DESCRIPTION" );
				this.reportGuidIndex		= this.DataTable.Columns.IndexOf( "REPORT_GUID" );
				this.reportFileNameIndex	= this.DataTable.Columns.IndexOf( "REPORT_FILENAME" );
                this.settingsIndex          = this.DataTable.Columns.IndexOf( "SETTINGS" );
            }
		}

		#endregion  // View

		protected override ReportRow OnCreateRow( DataRow dataRow )
		{
			return new ReportRow( dataRow );
		}

		protected override void OnRowActionHide( ReportRow row )
		{
			this.rowsByReportGuid.Remove( row.ReportGuid );

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( ReportRow row )
		{
			base.OnRowActionShow( row );

			this.rowsByReportGuid.Add( row.ReportGuid, row );
		}

		private readonly Dictionary<Guid, ReportRow> rowsByReportGuid =
			new Dictionary<Guid, ReportRow>();

		public IDictionary<Guid, ReportRow> RowsByReportGuid
		{
			get { return this.rowsByReportGuid; }
		}

		#region Implement SecureTable Members

		private static readonly AccessRight[] s_accessRights =
			new AccessRight[]
				{
					new AccessRight( (int)ReportRights.FullControl, "Full Control" ),
					new AccessRight( (int)ReportRights.Read, "Read" ),
					new AccessRight( (int)ReportRights.AccessControl, "AccessRight Control" )
				};

		public override AccessRight[] AccessRights
		{
			get { return ReportTable.s_accessRights; }
		}

		#endregion // Implement SecureTable Members
	}

	[Flags]
	public enum ReportRights
	{
		FullControl		= GeneralRights.FullControl,
		Read			= GeneralRights.Read,
		AccessControl	= GeneralRights.AccessControl
	}

	public class ReportRow :
		SecureRow<ReportRow, ReportTable, IReportData, IReportSecurityData, IOperatorData>
	{
		#region Fields

		public string Name
		{
			get { return (string)this.DataRow[this.Table.NameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.NameIndex] = value; }
		}

		public string Description
		{
			get { return (string)this.DataRow[this.Table.DescriptionIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DescriptionIndex] = value; }
		}

		public Guid ReportGuid
		{
			get { return (Guid)this.DataRow[this.Table.ReportGuidIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReportGuidIndex] = value; }
		}

		public string ReportFileName
		{
			get { return (string)this.DataRow[this.Table.ReportFileNameIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReportFileNameIndex] = value; }
		}

        public string Settings
        {
            get 
            { 
                return (string)this.DataRow[this.Table.SettingsIndex, DataRowVersion.Current]; 
            }
            set 
            {
                this.DataRow[this.Table.SettingsIndex] = value; 
            }
        }


		#endregion // Fields

		#region Security child rows

		private readonly ChildRowCollection<ReportOperatorGroupRow, ReportOperatorGroupTable, IReportSecurityData> rowsReportOperatorGroup;
		public ChildRowCollection<ReportOperatorGroupRow, ReportOperatorGroupTable, IReportSecurityData> RowsReportOperatorGroup
		{
			get { return this.rowsReportOperatorGroup; }
		}

		private readonly ChildRowCollection<ReportOperatorRow, ReportOperatorTable, IReportSecurityData> rowsReportOperator;
		public ChildRowCollection<ReportOperatorRow, ReportOperatorTable, IReportSecurityData> RowsReportOperator
		{
			get { return this.rowsReportOperator; }
		}

		#endregion // Security child rows

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		public ReportRow( DataRow dataRow )
			: base( dataRow )
		{
			this.rowsReportOperatorGroup = new ChildRowCollection<ReportOperatorGroupRow, ReportOperatorGroupTable, IReportSecurityData>();
			this.rowsReportOperator = new ChildRowCollection<ReportOperatorRow, ReportOperatorTable, IReportSecurityData>();
		}

		#region ISecureObject



		#endregion // ISecureObject

		protected override void OnBuildFields() {}

		#region Implement SecureRow Members

		protected override string GetSecureObjectName()
		{
			return this.Name;
		}

		protected override ICollection<ISecureObject> GetSecureObjectParents()
		{
			return new List<ISecureObject>( 0 );
		}

		protected override IList<IAccess> GetSecureObjectAcl()
		{
			List<IAccess> ACL = new List<IAccess>( this.rowsReportOperatorGroup.Count + this.rowsReportOperator.Count );

			foreach ( ReportOperatorGroupRow reportOperatorGroupRow in this.rowsReportOperatorGroup )
			{
				ACL.Add( reportOperatorGroupRow );
			}

			foreach ( ReportOperatorRow reportOperatorRow in this.rowsReportOperator )
			{
				ACL.Add( reportOperatorRow );
			}

			return ACL;
		}

		public override void ClearAcl()
		{
			foreach ( ReportOperatorGroupRow reportOperatorGroupRow in this.rowsReportOperatorGroup.Clone() )
			{
				reportOperatorGroupRow.Delete();
			}
			foreach ( ReportOperatorRow reportOperatorRow in this.RowsReportOperator.Clone() )
			{
				reportOperatorRow.Delete();
			}
		}

		#endregion // Implement SecureRow Members
	}
}