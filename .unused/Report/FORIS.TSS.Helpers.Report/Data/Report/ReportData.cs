﻿using System.ComponentModel;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Report.Data.Report.Security;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.Helpers.Report.Data.Report
{
	public class ReportData :
		SecureData<IReportDataTreater>,
		IReportData,
		IReportDataSupplier
	{
		#region Controls & Components

		private IContainer components;
		private ReportDataDispatcher tablesDataDispatcher;

		private ReportTable report;

		private ReportDataAmbassador daReport;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public ReportData( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public ReportData()
		{
			this.securityData = new ReportSecurityData( this );

			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				this.securityData.Dispose();

				if ( this.components != null )
				{
					this.components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tablesDataDispatcher = new ReportDataDispatcher( this.components );

			this.report = new ReportTable( this.components );

			this.daReport = new ReportDataAmbassador();

			this.daReport.Item = this.report;

			this.tablesDataDispatcher.Ambassadors.Add( this.daReport );
		}

		#endregion // Component Designer generated code

		#region Properties

		public new IReportDataSupplier DataSupplier
		{
			get { return (IReportDataSupplier)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}

		#endregion // Properties

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return this.tablesDataDispatcher; }
		}

		#endregion // Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			this.securityData.DataSupplier = null;

			this.tablesDataDispatcher.Data = null;
		}

		protected override void Build()
		{
			this.tablesDataDispatcher.Data = this;

			this.securityData.DataSupplier = this.DataSupplier.SecurityDataSupplier;
		}

		#endregion // Build & Destroy

		#region Security

		private readonly ReportSecurityData securityData;
		[Browsable( false )]
		public IReportSecurityData SecurityData
		{
			get { return this.securityData; }
		}

		public IReportSecurityDataSupplier SecurityDataSupplier
		{
			get { return this.securityData; }
		}

		#endregion // Security

		#region Tables properties

		[Browsable( false )]
		public ReportTable Report
		{
			get { return this.report; }
		}

		#endregion // Tables properties
	}
}