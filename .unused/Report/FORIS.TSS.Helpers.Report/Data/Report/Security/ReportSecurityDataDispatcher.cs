﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Report.Data.Report.Security;

namespace FORIS.TSS.Helpers.Report.Data.Report.Security
{
	public class ReportSecurityDataDispatcher :
		DataDispatcher<IReportSecurityData>
	{
		public ReportSecurityDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public ReportSecurityDataDispatcher()
		{

		}

		private readonly ReportSecurityDataAmbassadorCollection ambassadors =
			new ReportSecurityDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new ReportSecurityDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IReportSecurityData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

	public class ReportSecurityDataAmbassadorCollection :
		DataAmbassadorCollection<IReportSecurityData>
	{
		public new ReportSecurityDataAmbassador this[int index]
		{
			get { return (ReportSecurityDataAmbassador)base[index]; }
		}
	}

	public class ReportSecurityDataAmbassador :
		DataAmbassador<IReportSecurityData>
	{

	}
}