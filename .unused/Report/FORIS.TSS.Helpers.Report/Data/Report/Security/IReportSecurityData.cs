﻿using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Report.Data.Report.Security
{

	public interface IReportSecurityData :
		ISecurityData<IReportSecurityData, IReportData, IOperatorData>
	{
		ReportOperatorGroupTable	ReportOperatorGroup { get; }
		ReportOperatorTable			ReportOperator { get; }
	}
}