﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Report.Data.Report.Security
{
	public class ReportOperatorTable :
		SecurityTable<ReportOperatorRow, ReportOperatorTable, IReportSecurityData, IReportData, IOperatorData>
	{
		#region Properties

		public override string Name
		{
			get { return "R_REPORT_OPERATOR"; }
		}

		#endregion // Properties

		#region Constructor & Dispose

		public ReportOperatorTable()
		{
		}
		public ReportOperatorTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		#region Fields' indexes

		private int idIndex;
		internal int IdIndex			{ get { return this.idIndex; } }

		private int reportIdIndex;
		internal int ReportIdIndex		{ get { return this.reportIdIndex; } }

		private int operatorIdIndex;
		internal int OperatorIdIndex	{ get { return this.operatorIdIndex; } }

		private int allowMaskIndex;
		internal int AllowMaskIndex		{ get { return this.allowMaskIndex; } }

		private int denyMaskIndex;
		internal int DenyMaskIndex		{ get { return this.denyMaskIndex; } }

		#endregion // Fields' indexes

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex			= -1;
			this.reportIdIndex		= -1;
			this.operatorIdIndex	= -1;
			this.allowMaskIndex		= -1;
			this.denyMaskIndex		= -1;

			this.Data.ProtectedData.Report.Rows.Inserted -= this.ReportRows_Inserted;
			this.Data.ProtectedData.Report.Rows.Removing -= this.ReportRows_Removing;

			this.childRows.Clear();
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex			= this.DataTable.Columns.IndexOf( "R_REPORT_OPERATOR_ID" );
				this.reportIdIndex		= this.DataTable.Columns.IndexOf( "REPORT_ID" );
				this.operatorIdIndex	= this.DataTable.Columns.IndexOf( "OPERATOR_ID" );
				this.allowMaskIndex		= this.DataTable.Columns.IndexOf( "ALLOW_MASK" );
				this.denyMaskIndex		= this.DataTable.Columns.IndexOf( "DENY_MASK" );
			}

			this.Data.ProtectedData.Report.Rows.Inserted += this.ReportRows_Inserted;
			this.Data.ProtectedData.Report.Rows.Removing += this.ReportRows_Removing;
		}

		#endregion  // View

		protected override ReportOperatorRow OnCreateRow( DataRow dataRow )
		{
			return new ReportOperatorRow( dataRow );
		}

		protected override void OnRowActionHide( ReportOperatorRow row )
		{
			#region ReportRow.RowsReportOperator

			lock ( this.childRows )
			{
				ReportRow reportRow = this.Data.ProtectedData.Report.FindRow( row.Report );

				if ( reportRow != null )
				{
					reportRow.RowsReportOperator.Remove( row );
				}
				else
				{
					List<ReportOperatorRow> list = this.childRows[row.Report];
					list.Remove( row );

					if ( list.Count == 0 )
					{
						this.childRows.Remove( row.Report );
					}
				}
			}

			#endregion // ReportRow.RowsReportOperator

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( ReportOperatorRow row )
		{
			base.OnRowActionShow( row );

			#region ReportRow.RowsReportOperator

			lock ( this.childRows )
			{
				ReportRow reportRow = this.Data.ProtectedData.Report.FindRow( row.Report );

				if ( reportRow != null )
				{
					reportRow.RowsReportOperator.Add( row );
				}
				else
				{
					if ( this.childRows.ContainsKey( row.Report ) )
					{
						this.childRows[row.Report].Add( row );
					}
					else
					{
						List<ReportOperatorRow> list = new List<ReportOperatorRow>();
						list.Add( row );

						this.childRows.Add( row.Report, list );
					}
				}
			}

			#endregion // ReportRow.RowsReportOperator
		}

		protected override string SecureObjectIdColumnName
		{
			get { return "REPORT_ID"; }
		}
		protected override string PrincipalIdColumnName
		{
			get { return "OPERATOR_ID"; }
		}

		#region Delayed child rows

		private readonly Dictionary<int, List<ReportOperatorRow>> childRows =
			new Dictionary<int, List<ReportOperatorRow>>();

		void ReportRows_Inserted( object sender, CollectionChangeEventArgs<ReportRow> e )
		{
			lock ( this.childRows )
			{
				if ( this.childRows.ContainsKey( e.Item.ID ) )
				{
					foreach ( ReportOperatorRow row in this.childRows[e.Item.ID] )
					{
						e.Item.RowsReportOperator.Add( row );
					}

					this.childRows.Remove( e.Item.ID );
				}
			}
		}

		void ReportRows_Removing( object sender, CollectionChangeEventArgs<ReportRow> e )
		{
			lock ( this.childRows )
			{
				if ( this.childRows.ContainsKey( e.Item.ID ) )
				{
					this.childRows.Remove( e.Item.ID );
				}
			}
		}

		#endregion  // Delayed child rows
	}

	public class ReportOperatorRow :
		SecurityRow<ReportOperatorRow, ReportOperatorTable, IReportSecurityData, IReportData, IOperatorData>
	{
		#region Fields

		public int Report
		{
			get { return (int)this.DataRow[this.Table.ReportIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReportIdIndex] = value; }
		}

		public int Operator
		{
			get { return (int)this.DataRow[this.Table.OperatorIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OperatorIdIndex] = value; }
		}

		public override int AllowMask
		{
			get { return (int)this.DataRow[this.Table.AllowMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.AllowMaskIndex] = value; }
		}

		public override int DenyMask
		{
			get { return (int)this.DataRow[this.Table.DenyMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DenyMaskIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Parent row

		private OperatorRow rowOperator;

		#endregion // Parent row

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.rowOperator = null;
		}
		protected override void OnBuildView()
		{
			this.rowOperator = this.Data.PrincipalData.Operator.FindRow( this.Operator );
			base.OnBuildView();
		}

		#endregion  // View

		public ReportOperatorRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() {}

		protected override ITssPrincipal GetPrincipal()
		{
			return this.rowOperator;
		}
	}
}