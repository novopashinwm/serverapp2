﻿using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Helpers.Data.Security;
using FORIS.TSS.Helpers.Operator.Data.Operator;

namespace FORIS.TSS.Helpers.Report.Data.Report.Security
{
	public class ReportOperatorGroupTable :
		SecurityTable<ReportOperatorGroupRow, ReportOperatorGroupTable, IReportSecurityData, IReportData, IOperatorData>
	{
		#region Properties

		public override string Name
		{
			get { return "R_REPORT_OPERATORGROUP"; }
		}

		#endregion // Properties

		#region Constructor & Dispose

		public ReportOperatorGroupTable()
		{

		}
		public ReportOperatorGroupTable( IContainer container )
			: this()
		{
			container.Add( this );
		}

		#endregion  // Constructor & Dispose

		#region Fields' properties

		private int idIndex;
		internal int IdIndex				{ get { return this.idIndex; } }

		private int reportIdIndex;
		internal int ReportIdIndex			{ get { return this.reportIdIndex; } }

		private int operatorGroupIdIndex;
		internal int OperatorGroupIdIndex	{ get { return this.operatorGroupIdIndex; } }

		private int allowMaskIndex;
		internal int AllowMaskIndex			{ get { return this.allowMaskIndex; } }

		private int denyMaskIndex;
		internal int DenyMaskIndex			{ get { return this.denyMaskIndex; } }

		#endregion // Fields' properties

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.idIndex				= -1;
			this.reportIdIndex			= -1;
			this.operatorGroupIdIndex	= -1;
			this.allowMaskIndex			= -1;
			this.denyMaskIndex			= -1;
		}
		protected override void OnBuildView()
		{
			base.OnBuildView();

			if ( this.DataTable != null )
			{
				this.idIndex				= this.DataTable.Columns.IndexOf( "R_REPORT_OPERATORGROUP_ID" );
				this.reportIdIndex			= this.DataTable.Columns.IndexOf( "REPORT_ID" );
				this.operatorGroupIdIndex	= this.DataTable.Columns.IndexOf( "OPERATORGROUP_ID" );
				this.allowMaskIndex			= this.DataTable.Columns.IndexOf( "ALLOW_MASK" );
				this.denyMaskIndex			= this.DataTable.Columns.IndexOf( "DENY_MASK" );
			}
		}

		#endregion  // View

		protected override ReportOperatorGroupRow OnCreateRow( DataRow dataRow )
		{
			return new ReportOperatorGroupRow( dataRow );
		}

		protected override void OnRowActionHide( ReportOperatorGroupRow row )
		{
			#region ReportRow.RowsReportOperatorGroup

			ReportRow rightRow = this.Data.ProtectedData.Report.FindRow( row.Report );

			rightRow.RowsReportOperatorGroup.Remove( row );

			#endregion // ReportRow.RowsReportOperatorGroup

			base.OnRowActionHide( row );
		}
		protected override void OnRowActionShow( ReportOperatorGroupRow row )
		{
			base.OnRowActionShow( row );

			#region ReportRow.RowsReportOperatorGroup

			ReportRow rightRow = this.Data.ProtectedData.Report.FindRow( row.Report );

			rightRow.RowsReportOperatorGroup.Add( row );

			#endregion // ReportRow.RowsReportOperatorGroup
		}

		protected override string SecureObjectIdColumnName
		{
			get { return "REPORT_ID"; }
		}
		protected override string PrincipalIdColumnName
		{
			get { return "OPERATORGROUP_ID"; }
		}
	}

	public class ReportOperatorGroupRow :
		SecurityRow<ReportOperatorGroupRow, ReportOperatorGroupTable, IReportSecurityData, IReportData, IOperatorData>
	{
		#region Fields

		public int Report
		{
			get { return (int)this.DataRow[this.Table.ReportIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.ReportIdIndex] = value; }
		}

		public int OperatorGroup
		{
			get { return (int)this.DataRow[this.Table.OperatorGroupIdIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.OperatorGroupIdIndex] = value; }
		}

		public override int AllowMask
		{
			get { return (int)this.DataRow[this.Table.AllowMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.AllowMaskIndex] = value; }
		}

		public override int DenyMask
		{
			get { return (int)this.DataRow[this.Table.DenyMaskIndex, DataRowVersion.Current]; }
			set { this.DataRow[this.Table.DenyMaskIndex] = value; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[this.Table.IdIndex];
		}

		#endregion // Id

		#region Parent row

		private OperatorGroupRow rowOperatorGroup;

		#endregion // Parent row

		#region View

		protected override void OnDestroyView()
		{
			base.OnDestroyView();

			this.rowOperatorGroup = null;
		}
		protected override void OnBuildView()
		{
			this.rowOperatorGroup = this.Data.PrincipalData.OperatorGroup.FindRow( this.OperatorGroup );

			base.OnBuildView();
		}

		#endregion  // View

		public ReportOperatorGroupRow( DataRow dataRow ) : base( dataRow ) {}

		protected override void OnBuildFields() {}

		protected override ITssPrincipal GetPrincipal()
		{
			return this.rowOperatorGroup;
		}
	}
}