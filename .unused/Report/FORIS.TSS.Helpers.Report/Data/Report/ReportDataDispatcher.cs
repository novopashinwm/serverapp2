﻿using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.Helpers.Report.Data.Report
{
	public class ReportDataDispatcher :
		DataDispatcher<IReportData>
	{
		public ReportDataDispatcher( IContainer container )
			: this()
		{
			container.Add( this );
		}
		public ReportDataDispatcher()
		{

		}

		private readonly ReportDataAmbassadorCollection ambassadors =
			new ReportDataAmbassadorCollection();

		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public new ReportDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem<IReportData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

	}

	public class ReportDataAmbassadorCollection :
		DataAmbassadorCollection<IReportData>
	{
		public new ReportDataAmbassador this[int index]
		{
			get { return (ReportDataAmbassador)base[index]; }
		}
	}

	public class ReportDataAmbassador :
		DataAmbassador<IReportData>
	{

	}
}