using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Report.Data.GeneratedReport;
using FORIS.TSS.WorkplaceSharnier.Report.Data.Report;
using Interfaces.Report.Data;

namespace FORIS.TSS.WorkplaceSharnier.Report
{
	public interface IGeneratedReportClientDataProvider:
		IClientDataProvider,
		IOperatorClientDataProvider,
		IGeneratedReportDataSupplierProvider
	{
        GeneratedReportClientData GetGeneratedReportClientData();

		ISecurityEditor SecurityEditor { get; }

        //IReportFactory ReportFactory { get; }
	}
}