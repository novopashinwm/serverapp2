using FORIS.TSS.WorkplaceSharnier.Operator;
using FORIS.TSS.WorkplaceSharnier.Report.Data.Report;
using Interfaces.Report.Data;

namespace FORIS.TSS.WorkplaceSharnier.Report
{
	public interface IReportClientDataProvider:
		IClientDataProvider,
		IOperatorClientDataProvider,
		IReportDataSupplierProvider
	{
		ReportClientData GetReportClientData();

		ISecurityEditor SecurityEditor { get; }

        //IReportFactory ReportFactory { get; }
	}
}