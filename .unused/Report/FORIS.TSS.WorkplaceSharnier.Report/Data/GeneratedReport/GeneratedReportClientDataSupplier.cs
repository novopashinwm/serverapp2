using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.GeneratedReport
{
    internal class GeneratedReportClientDataSupplier :
        ClientDataSupplier<IGeneratedReportClientDataProvider, IGeneratedReportDataSupplier, IGeneratedReportDataTreater>,
        IGeneratedReportDataSupplier
    {
        public GeneratedReportClientDataSupplier(IGeneratedReportClientDataProvider clientProvider)
            : base(clientProvider, clientProvider.GetGeneratedReportDataSupplier())
        {

        }

        public IGeneratedReportSecurityDataSupplier SecurityDataSupplier
        {
            get
            {
                return
                    new GeneratedReportSecurityClientDataSupplier(
                        this.ClientProvider,
                        this.DataSupplier.SecurityDataSupplier
                        );
            }
        }
    }

    internal class GeneratedReportSecurityClientDataSupplier :
        ClientDataSupplier<IGeneratedReportClientDataProvider, IGeneratedReportSecurityDataSupplier, IGeneratedReportSecurityDataTreater>,
        IGeneratedReportSecurityDataSupplier
    {
        public GeneratedReportSecurityClientDataSupplier(
            IGeneratedReportClientDataProvider clientProvider,
            IGeneratedReportSecurityDataSupplier dataSupplier
            )
            : base(clientProvider, dataSupplier)
        {
        }

        public IOperatorDataSupplier PrincipalDataSupplier
        {
            get { return this.ClientProvider.GetOperatorClientData(); }
        }
    }
}