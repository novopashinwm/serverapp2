using FORIS.TSS.Helpers.Report.Data.GeneratedReport;
using FORIS.TSS.Helpers.Report.Data.Report;
using Interfaces.Report.Data.GeneratedReport;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.GeneratedReport
{
	/// <summary>
	/// ��������� ������ ������� �������
	/// ������ "������"
	/// </summary>
	public class GeneratedReportClientData:
		GeneratedReportData
	{
		#region Constructor & Dispose

        public GeneratedReportClientData(IGeneratedReportClientDataProvider clientProvider)
		{
            this.dataSupplier = new GeneratedReportClientDataSupplier(clientProvider);

			this.DataSupplier = this.dataSupplier;
		}

		#endregion  // Constructor & Dispose

		/// <summary>
		/// ������������ ��� ��������� ������ ������� DataTreater
		/// </summary>
        private readonly IGeneratedReportDataSupplier dataSupplier;
	}
}