using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using FORIS.TSS.WorkplaceSharnier.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.GeneratedReport
{
	public class GeneratedReportDataSupplierFactory:
		ClientDataSupplierFactory<IGeneratedReportClientDataProvider, GeneratedReportClientData, DataParams>
	{
        public GeneratedReportDataSupplierFactory(IGeneratedReportClientDataProvider clientProvider)
			: base( clientProvider )
		{

		}

        protected override GeneratedReportClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

            GeneratedReportClientData Supplier = new GeneratedReportClientData(this.Client);

			return Supplier;
		}
	}
}