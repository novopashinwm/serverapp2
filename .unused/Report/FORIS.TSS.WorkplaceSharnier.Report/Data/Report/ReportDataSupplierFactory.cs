using System;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using FORIS.TSS.WorkplaceSharnier.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.Report
{
	public class ReportDataSupplierFactory:
		ClientDataSupplierFactory<IReportClientDataProvider, ReportClientData, DataParams>
	{
		public ReportDataSupplierFactory( IReportClientDataProvider clientProvider )
			: base( clientProvider )
		{

		}

		protected override ReportClientData CreateDataSupplier(
			DataParams @params
			)
		{
			if( @params != DataParams.Empty )
			{
				throw new ApplicationException();
			}

			ReportClientData Supplier =new ReportClientData( this.Client );

			return Supplier;
		}
	}
}