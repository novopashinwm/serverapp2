using FORIS.TSS.Interfaces.Operator.Data.Operator;
using FORIS.TSS.WorkplaceSharnier.Data;
using FORIS.TSS.WorkplaceSharnier.Report;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.Report
{
	internal class ReportClientDataSupplier:
		ClientDataSupplier<IReportClientDataProvider, IReportDataSupplier, IReportDataTreater>,
		IReportDataSupplier
	{
		public ReportClientDataSupplier( IReportClientDataProvider clientProvider )
			: base( clientProvider, clientProvider.GetReportDataSupplier() )
		{

		}

		public IReportSecurityDataSupplier SecurityDataSupplier
		{
			get
			{
				return
					new ReportSecurityClientDataSupplier(
						this.ClientProvider,
						this.DataSupplier.SecurityDataSupplier
						);
				}
		}
	}

	internal class ReportSecurityClientDataSupplier :
		ClientDataSupplier<IReportClientDataProvider, IReportSecurityDataSupplier, IReportSecurityDataTreater>,
		IReportSecurityDataSupplier
	{
		public ReportSecurityClientDataSupplier(
			IReportClientDataProvider clientProvider,
			IReportSecurityDataSupplier dataSupplier
			)
			: base( clientProvider, dataSupplier )
		{
		}

		public IOperatorDataSupplier PrincipalDataSupplier
		{
		    get { return this.ClientProvider.GetOperatorClientData(); }	
		}
	}
}