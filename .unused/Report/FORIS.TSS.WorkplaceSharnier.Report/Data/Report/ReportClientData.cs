using FORIS.TSS.Helpers.Report.Data.Report;
using Interfaces.Report.Data.Report;

namespace FORIS.TSS.WorkplaceSharnier.Report.Data.Report
{
	/// <summary>
	/// ��������� ������ ������� �������
	/// ������ "������"
	/// </summary>
	public class ReportClientData:
		ReportData
	{
		#region Constructor & Dispose

		public ReportClientData( IReportClientDataProvider clientProvider )
		{
			this.dataSupplier = new ReportClientDataSupplier( clientProvider );

			this.DataSupplier = this.dataSupplier;
		}

		#endregion  // Constructor & Dispose

		/// <summary>
		/// ������������ ��� ��������� ������ ������� DataTreater
		/// </summary>
		private readonly IReportDataSupplier dataSupplier;
	}
}