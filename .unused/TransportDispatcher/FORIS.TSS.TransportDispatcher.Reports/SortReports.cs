﻿using System.Collections;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	public class SortReports : IComparer
	{
		int IComparer.Compare(object oReport1, object oReport2)
		{
			TssReportBase Report1 = (TssReportBase)oReport1;
			TssReportBase Report2 = (TssReportBase)oReport2;
			return Report1.ReportNameGet().CompareTo(Report2.ReportNameGet());
		}
	}
}