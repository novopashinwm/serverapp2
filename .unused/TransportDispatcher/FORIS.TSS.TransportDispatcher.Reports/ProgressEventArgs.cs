﻿using System;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Объявление параметров обработчика событий для прогрессбара в отчетах./ </summary>
	public class ProgressEventArgs:EventArgs
	{
		public string message;
		public int count;
		public int value;
		
		/// <summary> Параметры обработчика событий для прогрессбара в отчетах. </summary>
		/// <param name="message">Текст, отображаемый в прогрессбаре на текущей стадии выполнения</param>
		/// <param name="count">Максимальное значение шкалы прогрессбара</param>
		/// <param name="value">Текущее положение движения прогрессбара</param>
		public ProgressEventArgs(string message, int count, int value)
		{
			this.message=message;
			this.count=count;
			this.value=value;
		}
	}
}