﻿namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Объявление делегата обработчика событий для прогрессбара в отчетах. </summary>
	public delegate void ProgressEventHandler(object sender, ProgressEventArgs args);
}