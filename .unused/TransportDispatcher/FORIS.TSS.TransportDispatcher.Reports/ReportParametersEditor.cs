﻿using System.ComponentModel;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
	/// <summary> Класс визуального редактора параметров отчёта для UI </summary>
	public class ReportParametersEditor
	{
		ReportParameters reportParameters;

		public ReportParametersEditor(ReportParameters reportParameters)
		{
			this.reportParameters = reportParameters;
		}

		[Browsable(false)]
		public ReportParameters ReportParameters
		{
			get { return this.reportParameters; }
			set { this.reportParameters = value;  }
		}

		[DisplayName("EmpryReportDN", true), Category("ReportParsCat"), Description("EmpryReportDesc")]
		public bool ShowBlankReportProp
		{
			get
			{
				return reportParameters.ShowBlankReportProp;
			}
			set
			{
				reportParameters.ShowBlankReportProp = value;
			}
		}
	}
}