using System;

namespace FORIS.TSS.ServerApplication.Helpers
{
	public interface IDatePeriod
	{
		int ID { get; }

		DateTime DateBegin { get; }
		DateTime DateEnd { get; }

		bool Contains( DateTime Date );

		IDatePeriod Clone();

		void SetData( IDatePeriod Period );

		bool EssentialEquals( IDatePeriod Period );
	}
}
